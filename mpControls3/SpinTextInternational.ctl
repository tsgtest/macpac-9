VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.UserControl SpinTextInternational 
   ClientHeight    =   900
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2850
   LockControls    =   -1  'True
   ScaleHeight     =   900
   ScaleWidth      =   2850
   ToolboxBitmap   =   "SpinTextInternational.ctx":0000
   Begin VB.TextBox txtText 
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   1
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   720
   End
   Begin MSComCtl2.UpDown spnText 
      Height          =   315
      Left            =   720
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   240
      _ExtentX        =   450
      _ExtentY        =   556
      _Version        =   393216
      OrigLeft        =   1680
      OrigTop         =   405
      OrigRight       =   1920
      OrigBottom      =   720
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtText 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Index           =   0
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   720
   End
End
Attribute VB_Name = "SpinTextInternational"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private m_IncrementValue As Currency
Private m_MinValue As Currency
Private m_MaxValue As Currency
Private m_AppendSymbol As Boolean
Private m_Value As Currency
Private m_Symbol As String
Private m_ValueUnit As stbUnits
Private m_DisplayUnit As stbUnits
Private m_DisplayValue As Currency
Private m_Converter As Double
Private m_IncrementDisplay As Currency
Private m_MaxDisplay As Currency
Private m_MinDisplay As Currency
Private m_bInit As Boolean
Private m_bNoChange As Boolean
Private m_AllowFractions As Boolean
Private m_DecimalPlaces As Integer
Private m_Appearance As stbAppearance

Public Enum stbUnits
    stbUnit_Inches = 0
    stbUnit_Centimeters = 1
    stbUnit_Millimeters = 2
    stbUnit_Points = 3
    stbUnit_Picas = 4
End Enum

Public Enum stbAppearance
    Flat = 0
    ThreeDimensional = 1
End Enum

Private Const mpUnitsPerBase_Inches As Currency = 1
Private Const mpUnitsPerBase_Centimeters As Currency = 2.54
Private Const mpUnitsPerBase_Millimeters As Currency = 25.4
Private Const mpUnitsPerBase_Points As Currency = 72
Private Const mpUnitsPerBase_Picas As Currency = 6

Public Event KeyDown(KeyCode As Integer, Shift As Integer)
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event Change()
Public Event SpinUp()
Public Event SpinDown()
Public Event Click()

'*************************************************************************
'   Properties
'*************************************************************************

Public Property Let Visible(ByVal NewValue As Boolean)
    UserControl.Extender.Visible = NewValue
    PropertyChanged "Visible"
End Property

Public Property Get Visible() As Boolean
    Visible = UserControl.Extender.Visible
End Property

Public Property Let BackColor(ByVal NewValue As OLE_COLOR)
    UserControl.txtText(m_Appearance).BackColor = NewValue
    PropertyChanged "BackColor"
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.txtText(m_Appearance).BackColor
End Property

Public Property Get Enabled() As Boolean
    Enabled = UserControl.Enabled
End Property

Public Property Let Enabled(ByVal NewValue As Boolean)
    UserControl.Enabled = NewValue
    UserControl.txtText(m_Appearance).Enabled = NewValue
    PropertyChanged "Enabled"
End Property

Public Property Get Appearance() As stbAppearance
    Appearance = m_Appearance
End Property

Public Property Let Appearance(ByVal NewValue As stbAppearance)
    Dim iNon As Integer
    If NewValue <> m_Appearance Then
        m_Appearance = NewValue
        iNon = Abs(Not CBool(m_Appearance))
        With txtText(m_Appearance)
            .Visible = True
            .Enabled = txtText(iNon).Enabled
            .BackColor = txtText(iNon).BackColor
            .Text = txtText(iNon).Text
            .SelLength = txtText(iNon).SelLength
            .SelStart = txtText(iNon).SelStart
            .SelText = txtText(iNon).SelText
        End With
        txtText(iNon).Visible = False
        PropertyChanged "Appearance"
    End If
End Property

Public Property Let AllowFractions(ByVal bNew As Boolean)
    If bNew <> m_AllowFractions Then
        m_AllowFractions = bNew
        If m_AllowFractions = False Then
            m_DecimalPlaces = 0
'           if fractions not allowed, force increment to 1
            If CLng(m_IncrementValue) <> m_IncrementValue Then _
                m_IncrementValue = 1
        Else
            m_DecimalPlaces = 2
        End If
        If Not m_bInit Then _
            SetDisplayUnit
        PropertyChanged "AllowFractions"
    End If
End Property

Public Property Get AllowFractions() As Boolean
    AllowFractions = m_AllowFractions
End Property

Public Property Let IncrementValue(ByVal dNew As Currency)
    On Error GoTo ProcError
    If dNew <> m_IncrementValue Then
        m_IncrementValue = dNew
'       if increment isn't whole, allow fractions
        If CLng(m_IncrementValue) <> m_IncrementValue Then
            m_AllowFractions = True
            m_DecimalPlaces = 2
        End If
        If Not m_bInit Then _
            SetDisplayUnit
        PropertyChanged "IncrementValue"
    End If
    Exit Property
ProcError:
    RaiseError Err.Number, Err.Description, "IncrementValue"
    Exit Property
End Property

Public Property Get IncrementValue() As Currency
    IncrementValue = m_IncrementValue
End Property

Public Property Let MinValue(ByVal dNew As Currency)
    On Error GoTo ProcError
    If dNew <> m_MinValue Then
        m_MinValue = dNew
        If Not m_bInit Then _
            SetDisplayUnit
        PropertyChanged "MinValue"
    End If
    Exit Property
ProcError:
    RaiseError Err.Number, Err.Description, "MinValue"
    Exit Property
End Property

Public Property Get MinValue() As Currency
    MinValue = m_MinValue
End Property
Public Property Let MaxValue(ByVal dNew As Currency)
    On Error GoTo ProcError
    If dNew <> m_MaxValue Then
        m_MaxValue = dNew
        If Not m_bInit Then _
            SetDisplayUnit
        PropertyChanged "MaxValue"
    End If
    Exit Property
ProcError:
    RaiseError Err.Number, Err.Description, "MaxValue"
    Exit Property
End Property

Public Property Get MaxValue() As Currency
    MaxValue = m_MaxValue
End Property

Public Property Let ValueUnit(ByVal iUnit As stbUnits)
    On Error GoTo ProcError
    If iUnit <> m_ValueUnit Then
        m_ValueUnit = iUnit
        If Not m_bInit Then _
            SetDisplayUnit
        PropertyChanged "ValueUnit"
    End If
    Exit Property
ProcError:
    RaiseError Err.Number, Err.Description, "ValueUnit"
    Exit Property
End Property

Public Property Get ValueUnit() As stbUnits
    ValueUnit = m_ValueUnit
End Property

Public Property Let DisplayUnit(ByVal iUnit As stbUnits)
    On Error GoTo ProcError
    If iUnit <> m_DisplayUnit Then
        m_DisplayUnit = iUnit
        If Not m_bInit Then _
            SetDisplayUnit
        PropertyChanged "DisplayUnit"
    End If
    Exit Property
ProcError:
    RaiseError Err.Number, Err.Description, "DisplayUnit"
    Exit Property
End Property

Public Property Get DisplayUnit() As stbUnits
    DisplayUnit = m_DisplayUnit
End Property

Public Property Let AppendSymbol(ByVal bNew As Boolean)
    On Error GoTo ProcError
    If bNew <> m_AppendSymbol Then
        m_AppendSymbol = bNew
        m_bNoChange = True
        If bNew Then
            txtText(m_Appearance).Text = AddSymbol(txtText(m_Appearance).Text)
        Else
            txtText(m_Appearance).Text = _
                Trim(xSubstitute(txtText(m_Appearance).Text, m_Symbol, ""))
        End If
        m_bNoChange = False
        PropertyChanged "AppendSymbol"
    End If
    Exit Property
ProcError:
    RaiseError Err.Number, Err.Description, "AppendSymbol"
    Exit Property
End Property

Public Property Get AppendSymbol() As Boolean
    AppendSymbol = m_AppendSymbol
End Property

Public Property Let Value(ByVal dNew As Currency)
    On Error GoTo ProcError
    If dNew <> m_Value Then
        m_Value = dNew
        m_bNoChange = True
        m_DisplayValue = Round(m_Converter * m_Value, m_DecimalPlaces)
        txtText(m_Appearance).Text = m_DisplayValue
        If Me.AppendSymbol Then _
            txtText(m_Appearance).Text = AddSymbol(txtText(m_Appearance).Text)
        m_bNoChange = False
        PropertyChanged "Value"
    End If
    Exit Property
ProcError:
    RaiseError Err.Number, Err.Description, "Value"
    Exit Property
End Property

Public Property Get Value() As Currency
    Value = m_Value
End Property

Public Property Get DisplayValue() As Currency
    DisplayValue = m_DisplayValue
End Property

Public Property Let DisplayText(ByVal xNew As String)
    m_bNoChange = True
    UserControl.txtText(m_Appearance).Text = xNew
    m_bNoChange = False
End Property

Public Property Get DisplayText() As String
    DisplayText = UserControl.txtText(m_Appearance).Text
End Property

Public Property Let SelLength(ByVal lNew As Long)
    UserControl.txtText(m_Appearance).SelLength = lNew
End Property

Public Property Get SelLength() As Long
    SelLength = UserControl.txtText(m_Appearance).SelLength
End Property

Public Property Let SelStart(ByVal lNew As Long)
    UserControl.txtText(m_Appearance).SelStart = lNew
End Property

Public Property Get SelStart() As Long
    SelStart = UserControl.txtText(m_Appearance).SelStart
End Property

Public Property Let SelText(ByVal xNew As String)
    UserControl.txtText(m_Appearance).SelText = xNew
End Property

Public Property Get SelText() As String
    SelText = UserControl.txtText(m_Appearance).SelText
End Property


'******************************************************************
'---internal functions
'******************************************************************

Private Sub spnText_DownClick()
    Dim xTemp As String
    
    On Error GoTo ProcError

'   remove symbol
    xTemp = Trim(xSubstitute(txtText(m_Appearance).Text, m_Symbol, ""))
    
    If Not IsNumeric(xTemp) Then
'       check for symbol of different unit
        xTemp = ConvertToDisplayUnit(xTemp)
        
'       remove symbol
        xTemp = Trim(xSubstitute(xTemp, m_Symbol, ""))
    End If

    If Not IsNumeric(xTemp) Then
'       set to min value
        m_DisplayValue = Max(m_MinDisplay, 0)
    ElseIf CCur(xTemp) > m_MinDisplay Then
'       decrement
        m_DisplayValue = Max(NextIncrement(CCur(xTemp), False), _
            m_MinDisplay)
    Else
'       at or below min - leave as is
        m_DisplayValue = CCur(xTemp)
    End If
    
'   update textbox
    txtText(m_Appearance).Text = m_DisplayValue
    If Me.AppendSymbol Then _
        txtText(m_Appearance).Text = AddSymbol(txtText(m_Appearance).Text)

'   update actual value
    m_Value = m_DisplayValue / m_Converter
    
    RaiseEvent SpinDown
    RaiseEvent Click
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "spnText_DownClick"
    Exit Sub
End Sub

Private Sub spnText_UpClick()
    Dim xTemp As String
    
    On Error GoTo ProcError

'   remove symbol
    xTemp = Trim(xSubstitute(txtText(m_Appearance).Text, m_Symbol, ""))
    
    If Not IsNumeric(xTemp) Then
'       check for symbol of different unit
        xTemp = ConvertToDisplayUnit(xTemp)
        
'       remove symbol
        xTemp = Trim(xSubstitute(xTemp, m_Symbol, ""))
    End If

    If Not IsNumeric(xTemp) Then
'       set to min value
        m_DisplayValue = Max(m_MinDisplay, 0)
    ElseIf CCur(xTemp) < m_MaxDisplay Then
'       increment
        m_DisplayValue = Min(NextIncrement(CCur(xTemp), True), _
            m_MaxDisplay)
    Else
'       at or above max - leave as is
        m_DisplayValue = CCur(xTemp)
    End If
    
'   update textbox
    txtText(m_Appearance).Text = m_DisplayValue
    If Me.AppendSymbol Then _
        txtText(m_Appearance).Text = AddSymbol(txtText(m_Appearance).Text)
        
'   update actual value
    m_Value = m_DisplayValue / m_Converter
    
    RaiseEvent SpinUp
    RaiseEvent Click
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "spnText_UpClick"
    Exit Sub
End Sub

Private Sub txtText_Change(Index As Integer)
    If Not m_bNoChange Then
        RaiseEvent Change
    End If
End Sub

Private Sub txtText_GotFocus(Index As Integer)
    txtText(Index).SelStart = 0
    txtText(Index).SelLength = Len(txtText(Index).Text)
End Sub

Private Sub txtText_KeyDown(Index As Integer, KeyCode As Integer, _
                            Shift As Integer)
    If KeyCode = 40 Then
        spnText_DownClick
    ElseIf KeyCode = 38 Then
        spnText_UpClick
    End If
End Sub

Private Function AddSymbol(ByVal xText As String) As String
    Dim xTemp As String
    On Error GoTo ProcError
    If xText = "" Then Exit Function
    xTemp = Trim(xSubstitute(xText, Trim(m_Symbol), ""))
    xTemp = xTemp & m_Symbol
    AddSymbol = xTemp
    Exit Function
ProcError:
    RaiseError Err.Number, Err.Description, "AddSymbol"
    Exit Function
End Function

Public Function IsValid(Optional bDisplayMessage As Boolean = True) As Boolean
    Dim xTemp As String
    Dim xSymbol As String
    Dim xSubject As String

    On Error GoTo ProcError

'   remove symbol
    xTemp = Trim(xSubstitute(txtText(m_Appearance).Text, m_Symbol, ""))

    If Not IsNumeric(xTemp) Then
'       check for symbol of different unit
        xTemp = ConvertToDisplayUnit(xTemp)
        
'       remove symbol
        xTemp = Trim(xSubstitute(xTemp, m_Symbol, ""))
    End If
    
'   empty will be changed to 0 on lost focus
    If xTemp = "" Then
        IsValid = True
        Exit Function
    End If

'   if displaying symbol, include in message
    If m_AppendSymbol Then
        xSymbol = m_Symbol
        xSubject = "measurement"
    Else
        xSubject = "value"
    End If
    
    If (Not IsNumeric(xTemp)) Or _
            SeparatorIsInvalid(xTemp) Then
'       display message
        If bDisplayMessage Then
            MsgBox "This is not a valid " & xSubject & ".", _
                vbExclamation, App.Title
        End If
    ElseIf (CCur(xTemp) > m_MaxDisplay) Or _
            (CCur(xTemp) < m_MinDisplay) Then
'       display message
        If bDisplayMessage Then
            MsgBox "The " & xSubject & " must be between " & m_MinDisplay & _
                xSymbol & " and " & m_MaxDisplay & xSymbol & ".", _
                vbExclamation, App.Title
        End If
    ElseIf (m_AllowFractions = False) And _
            (CLng(xTemp) <> CCur(xTemp)) Then
'       display message
        If bDisplayMessage Then
            MsgBox "The " & xSubject & " must be a whole number between " & _
                m_MinDisplay & xSymbol & " and " & m_MaxDisplay & xSymbol & ".", _
                vbExclamation, App.Title
        End If
    Else
        IsValid = True
        Exit Function
    End If
    
'   select entire text
    txtText(m_Appearance).SelStart = 0
    txtText(m_Appearance).SelLength = Len(txtText(m_Appearance).Text)

    Exit Function
ProcError:
    RaiseError Err.Number, Err.Description, "IsValid"
    Exit Function
End Function

Private Sub txtText_LostFocus(Index As Integer)
    Dim xTemp As String

    On Error GoTo ProcError

'   remove symbol
    xTemp = Trim(xSubstitute(txtText(Index).Text, m_Symbol, ""))

    If Not IsNumeric(xTemp) Then
'       check for symbol of different unit
        xTemp = ConvertToDisplayUnit(xTemp)
        
'       remove symbol
        xTemp = Trim(xSubstitute(xTemp, m_Symbol, ""))
    End If
    
'   change empty to zero or minimum
    If xTemp = "" Then
        xTemp = Trim(CStr(Max(m_MinDisplay, 0)))
    End If

'   the form may not be doing validation
    If (Not IsNumeric(xTemp)) Or _
            SeparatorIsInvalid(xTemp) Then
        Exit Sub
    ElseIf (CCur(xTemp) > m_MaxDisplay) Or _
            (CCur(xTemp) < m_MinDisplay) Then
        Exit Sub
    End If

'   update display value and textbox
    m_bNoChange = True
    m_DisplayValue = xTemp
    txtText(Index).Text = m_DisplayValue
    If Me.AppendSymbol Then _
        txtText(Index).Text = AddSymbol(txtText(Index).Text)
    m_bNoChange = False
        
'   update actual value
    m_Value = m_DisplayValue / m_Converter
    
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "txtText_LostFocus"
    Exit Sub
End Sub

Private Function UnitsPerBase(ByVal iUnit As stbUnits) As Currency
    On Error GoTo ProcError
    Select Case iUnit
        Case stbUnit_Inches
            UnitsPerBase = mpUnitsPerBase_Inches
        Case stbUnit_Centimeters
            UnitsPerBase = mpUnitsPerBase_Centimeters
        Case stbUnit_Millimeters
            UnitsPerBase = mpUnitsPerBase_Millimeters
        Case stbUnit_Points
            UnitsPerBase = mpUnitsPerBase_Points
        Case stbUnit_Picas
            UnitsPerBase = mpUnitsPerBase_Picas
        Case Else
            UnitsPerBase = mpUnitsPerBase_Points
    End Select
    Exit Function
ProcError:
    RaiseError Err.Number, Err.Description, "UnitsPerBase"
    Exit Function
End Function

Private Function GetSymbol(ByVal iUnit As stbUnits) As String
    On Error GoTo ProcError
    Select Case iUnit
        Case stbUnit_Inches
            GetSymbol = Chr(34)
        Case stbUnit_Centimeters
            GetSymbol = " cm"
        Case stbUnit_Millimeters
            GetSymbol = " mm"
        Case stbUnit_Points
            GetSymbol = " pt"
        Case stbUnit_Picas
            GetSymbol = " pi"
        Case Else
            GetSymbol = " pt"
    End Select
    Exit Function
ProcError:
    RaiseError Err.Number, Err.Description, "GetSymbol"
    Exit Function
End Function

Private Sub SetDisplayUnit()
'sets module level variables for display unit
    Dim curDisplay As Currency
    Dim curValue As Currency
    
    On Error GoTo ProcError

'   converter
    curValue = UnitsPerBase(m_ValueUnit)
    curDisplay = UnitsPerBase(m_DisplayUnit)
    m_Converter = curDisplay / curValue
    
'   symbol
    m_Symbol = GetSymbol(m_DisplayUnit)
        
'   max/min
    m_MaxDisplay = Round(m_Converter * m_MaxValue, m_DecimalPlaces)
    m_MinDisplay = Round(m_Converter * m_MinValue, m_DecimalPlaces)
    
''   increment
'    If m_ValueUnit < 2 And m_DisplayUnit > 1 Then
''       adjust increment for smaller unit
'        m_IncrementDisplay = m_IncrementValue * 10
'    Else
'        m_IncrementDisplay = m_IncrementValue
'    End If
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "SetDisplayUnit"
    Exit Sub
End Sub

Private Function NextIncrement(ByVal curValue As Currency, _
                               ByVal bUp As Boolean) As Currency
    Dim curTemp As Currency
    Dim lTemp As Long
    Dim curTest As Currency
      
    On Error GoTo ProcError

'   round to nearest increment
    curTemp = curValue / m_IncrementValue
    lTemp = CLng(curTemp)
       
    If lTemp = curTemp Then
         curTemp = curValue
    Else
         curTemp = lTemp * m_IncrementValue
    End If
    
'   increment/decrement
    If bUp Then
        If curTemp > curValue Then
            NextIncrement = curTemp
        Else
            NextIncrement = curTemp + m_IncrementValue
        End If
    Else
        If curTemp < curValue Then
            NextIncrement = curTemp
        Else
            NextIncrement = curTemp - m_IncrementValue
        End If
    End If

    Exit Function
ProcError:
    RaiseError Err.Number, Err.Description, "NextIncrement"
    Exit Function
End Function

Private Function ConvertToDisplayUnit(ByVal xText As String) As String
    Dim iUnit As stbUnits
    Dim xTemp As String
    Dim curValue As Currency
    Dim curDisplay As Currency
    Dim curUnit As Currency
    Dim xSymbol As String
    
    On Error GoTo ProcError

'   identify unit by looking
'   for one of the standard symbols
    xTemp = xText
    If InStr(xTemp, Chr(34)) Then
        iUnit = stbUnit_Inches
    ElseIf InStr(UCase(xTemp), "CM") Then
        iUnit = stbUnit_Centimeters
    ElseIf InStr(UCase(xTemp), "MM") Then
        iUnit = stbUnit_Millimeters
    ElseIf InStr(UCase(xTemp), "PT") Then
        iUnit = stbUnit_Points
    ElseIf InStr(UCase(xTemp), "PI") Then
        iUnit = stbUnit_Picas
    Else
'       no symbol found - return unchanged
        ConvertToDisplayUnit = xTemp
        Exit Function
    End If
    
'   remove symbol
    xSymbol = GetSymbol(iUnit)
    xTemp = Trim(xSubstitute(xText, Trim(xSymbol), ""))
    
    If Not IsNumeric(xTemp) Then
'       return unchanged
        ConvertToDisplayUnit = xTemp
        Exit Function
    End If
    
'   convert to display unit
    curDisplay = UnitsPerBase(m_DisplayUnit)
    curUnit = UnitsPerBase(iUnit)
    curValue = Round((CCur(xTemp) * (curDisplay / curUnit)), m_DecimalPlaces)
    xTemp = Trim(CStr(curValue))
    xTemp = AddSymbol(xTemp)
    ConvertToDisplayUnit = xTemp
    Exit Function
ProcError:
    RaiseError Err.Number, Err.Description, "ConvertToDisplayUnit"
    Exit Function
End Function

'****************************************************************
'---methods
'****************************************************************

Private Sub UserControl_InitProperties()
    On Error GoTo ProcError
    Appearance = Flat
    BackColor = vbWhite
    Enabled = True
    Visible = True
    IncrementValue = 1
    MaxValue = 10
    MinValue = 0
    AppendSymbol = False
    Value = 0
    ValueUnit = stbUnit_Points
    DisplayUnit = stbUnit_Points
    AllowFractions = True
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "UserControl_InitProperties"
    Exit Sub
End Sub

Private Sub UserControl_Resize()
    On Error GoTo ProcError
    With UserControl
        .txtText(0).Width = UserControl.Width - .spnText.Width - 15
        .txtText(0).Height = UserControl.Height
        .txtText(1).Width = .txtText(0).Width
        .txtText(1).Height = .txtText(0).Height
        .spnText.Height = UserControl.Height
        .spnText.Left = .txtText(0).Width + 15
    End With
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "UserControl_Resize"
    Exit Sub
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    m_bInit = True
    Appearance = PropBag.ReadProperty("Appearance", 0)
    BackColor = PropBag.ReadProperty("BackColor", vbWhite)
    Enabled = PropBag.ReadProperty("Enabled", True)
    Visible = PropBag.ReadProperty("Visible", True)
    IncrementValue = PropBag.ReadProperty("IncrementValue", 1)
    MaxValue = PropBag.ReadProperty("MaxValue", 10)
    MinValue = PropBag.ReadProperty("MinValue", 0)
    AppendSymbol = PropBag.ReadProperty("AppendSymbol", False)
    Value = PropBag.ReadProperty("Value", 0)
    ValueUnit = PropBag.ReadProperty("ValueUnit", stbUnit_Points)
    DisplayUnit = PropBag.ReadProperty("DisplayUnit", stbUnit_Points)
    AllowFractions = PropBag.ReadProperty("AllowFractions", True)
    m_bInit = False
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "UserControl_ReadProperties"
    Exit Sub
End Sub

Private Sub UserControl_Show()
    On Error GoTo ProcError
    m_bNoChange = True
    SetDisplayUnit
    m_DisplayValue = Round(m_Converter * m_Value, m_DecimalPlaces)
    txtText(m_Appearance).Text = m_DisplayValue
    If Me.AppendSymbol Then _
        txtText(m_Appearance).Text = AddSymbol(txtText(m_Appearance).Text)
    m_bNoChange = False
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "UserControl_Show"
    Exit Sub
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    PropBag.WriteProperty "Appearance", Me.Appearance, 0
    PropBag.WriteProperty "BackColor", Me.BackColor, vbWhite
    PropBag.WriteProperty "Enabled", Me.Enabled, True
    PropBag.WriteProperty "Visible", Me.Visible, True
    PropBag.WriteProperty "IncrementValue", Me.IncrementValue, 1
    PropBag.WriteProperty "MinValue", Me.MinValue, 0
    PropBag.WriteProperty "MaxValue", Me.MaxValue, 10
    PropBag.WriteProperty "AppendSymbol", Me.AppendSymbol, False
    PropBag.WriteProperty "Value", Me.Value, 0
    PropBag.WriteProperty "ValueUnit", Me.ValueUnit, stbUnit_Points
    PropBag.WriteProperty "DisplayUnit", Me.DisplayUnit, stbUnit_Points
    PropBag.WriteProperty "AllowFractions", Me.AllowFractions, True
    Exit Sub
ProcError:
    RaiseError Err.Number, Err.Description, "UserControl_WriteProperties"
    Exit Sub
End Sub

Sub RaiseError(lErr As Long, xDescription As String, xSource As String)
    MsgBox "The following error occurred in mpControls3.SpinTextInternational:" & _
        vbCr & vbCr & "Number: " & lErr & vbCr & "Source: " & xSource & vbCr & _
        "Description: " & xDescription, vbCritical, App.Title
End Sub

Public Sub Refresh()
'refreshes display
    txtText(m_Appearance).Text = m_DisplayValue
    If Me.AppendSymbol Then _
        txtText(m_Appearance).Text = AddSymbol(txtText(m_Appearance).Text)
End Sub

Private Function SeparatorIsInvalid(xText As String) As Boolean
    Dim xDecimal As String

    xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
    If xDecimal = "." Then
        If InStr(xText, ",") Then _
            SeparatorIsInvalid = True
    Else
        If InStr(xText, ".") Then _
            SeparatorIsInvalid = True
    End If
End Function
