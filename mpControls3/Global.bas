Attribute VB_Name = "Global"
Option Explicit

Public Function Max(i As Currency, j As Currency) As Currency
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Public Function Min(i As Currency, j As Currency) As Currency
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Public Function xSubstitute(ByVal xString As String, _
                     ByVal xSearch As String, _
                     ByVal xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE
    Dim lSearchPos As Long
    Dim xNewString As String
    
'   if xSearch is empty, InStr() will always return 1,
'   resulting in an infinite loop; return string as is;
'   this is a fix for Macpac 9 generic log item #1726
    If xSearch = "" Then
        xSubstitute = xString
        Exit Function
    End If
    
'   get first char pos
    lSearchPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While lSearchPos
        xNewString = xNewString & _
            Left(xString, lSearchPos - 1) & _
            xReplace
        xString = Mid(xString, lSearchPos + Len(xSearch))
        lSearchPos = InStr(UCase(xString), _
                          UCase(xSearch))
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function

