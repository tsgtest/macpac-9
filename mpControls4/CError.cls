VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum ciErrs
    ciErr_MissingOrInvalidINIKey = vbError + 512 + 1
    ciErr_InvalidUNID
    ciErr_InvalidFilterField
    ciErr_NotImplemented
    ciErr_BackendLogonCancelled
    ciErr_CouldNotSetFilterField
    ciErr_MissingFile
    ciErr_InvalidListing
    ciErr_InvalidUserINI
    ciErr_InvalidAddress
    ciErr_CouldNotAddToCollection
    ciErr_InvalidContactNumberType
    ciErr_InvalidContactDetailFormatString
    ciErr_DefaultFolderDoesNotExist
    ciErr_NoConnectedBackends
    ciErr_InvalidColumnParameter
    ciErr_ODBCError
    ciErr_CouldNotConnectToBackend
End Enum
    
Public Sub RaiseError(ByVal xNewSource As String)
'raises the current error, appending the source
    With Err
        If InStr(.Source, ".") Then
'           an originating source has already been specified - keep it
            .Raise .Number, .Source, .Description
        Else
'           no originating source has been specified - use the one supplied
            .Raise .Number, xNewSource & "." & Erl, .Description
        End If
    End With
End Sub

Sub ShowError()
    MsgBox "The following unexpected error occurred: " & vbCrLf & vbCrLf & _
        "Number:  " & Err.Number & vbCr & _
        "Description:  " & Err.Description & vbCr & _
        "Source:  " & Err.Source, vbExclamation, App.Title
End Sub


