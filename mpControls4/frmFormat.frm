VERSION 5.00
Begin VB.Form frmFormat 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Format Re Line"
   ClientHeight    =   3390
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   2235
   ClipControls    =   0   'False
   Icon            =   "frmFormat.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3390
   ScaleWidth      =   2235
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chkCarrierFlushLeft 
      Appearance      =   0  'Flat
      Caption         =   "Underline &Last Line"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":014A
      TabIndex        =   9
      Top             =   2535
      Width           =   2070
   End
   Begin VB.CheckBox chkRelineFlushLeft 
      Appearance      =   0  'Flat
      Caption         =   "Underline &Last Line"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":048C
      TabIndex        =   4
      Top             =   1155
      Width           =   2070
   End
   Begin VB.CheckBox chkCarrierBold 
      Appearance      =   0  'Flat
      Caption         =   "&Bold"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":07CE
      TabIndex        =   6
      Top             =   1755
      Width           =   2070
   End
   Begin VB.CheckBox chkCarrierItalic 
      Appearance      =   0  'Flat
      Caption         =   "&Italic"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":08D0
      TabIndex        =   7
      Top             =   2015
      Width           =   2070
   End
   Begin VB.CheckBox chkCarrierUnderScored 
      Appearance      =   0  'Flat
      Caption         =   "&Underline"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":0C12
      TabIndex        =   8
      Top             =   2275
      Width           =   2070
   End
   Begin VB.CheckBox chkRelineBold 
      Appearance      =   0  'Flat
      Caption         =   "&Bold"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":0F54
      TabIndex        =   1
      Top             =   360
      Width           =   2070
   End
   Begin VB.CheckBox chkRelineItalic 
      Appearance      =   0  'Flat
      Caption         =   "&Italic"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":1056
      TabIndex        =   2
      Top             =   625
      Width           =   2070
   End
   Begin VB.CheckBox chkRelineUnderscored 
      Appearance      =   0  'Flat
      Caption         =   "&Underline"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   150
      Picture         =   "frmFormat.frx":1398
      TabIndex        =   3
      Top             =   890
      Width           =   2070
   End
   Begin VB.CommandButton btnFormatCancel 
      Appearance      =   0  'Flat
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   1275
      TabIndex        =   11
      Top             =   2970
      Width           =   850
   End
   Begin VB.CommandButton btnFormatOK 
      Appearance      =   0  'Flat
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   315
      Left            =   90
      TabIndex        =   10
      Top             =   2970
      Width           =   850
   End
   Begin VB.Label lblRelineFormats 
      BackStyle       =   0  'Transparent
      Caption         =   "Re Line Formats:"
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   105
      Width           =   1275
   End
   Begin VB.Label lblCarrierFormats 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Carrier Formats:"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   5
      Top             =   1500
      Width           =   1275
   End
End
Attribute VB_Name = "frmFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_xPrefillList As String



Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let PrefillList(xNew As String)
    m_xPrefillList = xNew
End Property

Public Property Get PrefillList() As String
    PrefillList = m_xPrefillList
End Property

Private Sub btnFormatCancel_Click()
    Unload Me
End Sub

Private Sub btnFormatOK_Click()
    Me.Cancelled = False
    CollectFormatValues
    
    Me.Hide
End Sub


Private Sub chkRelineFlushLeft_Click()
    If chkRelineFlushLeft = vbChecked Then
        If chkRelineUnderscored = vbChecked Then
            chkRelineUnderscored = vbUnchecked
        End If
    End If

End Sub

Private Sub chkRelineUnderscored_Click()
    If chkRelineUnderscored = vbChecked Then
        If chkRelineFlushLeft = vbChecked Then
            chkRelineFlushLeft = vbUnchecked
        End If
    End If
End Sub

Private Sub chkCarrierFlushLeft_Click()
    If chkCarrierFlushLeft = vbChecked Then
        If chkCarrierUnderScored = vbChecked Then
            chkCarrierUnderScored = vbUnchecked
        End If
    End If

End Sub

Private Sub chkCarrierUnderscored_Click()
    If chkCarrierUnderScored = vbChecked Then
        If chkCarrierFlushLeft = vbChecked Then
            chkCarrierFlushLeft = vbUnchecked
        End If
    End If
End Sub

Private Sub Form_Activate()
    '---configure form
    DisplayControls
    
    PositionControls
    
    '---load values on reuse of form
    InitializeControls
    
    ResizeForm
End Sub

Private Sub Form_Initialize()
    Me.Cancelled = True
End Sub

Private Sub Form_Load()
    On Error GoTo ProcError
        
    Exit Sub
ProcError:
    g_oError.RaiseError "Reline.frmFormat:Form_Load"
End Sub


Private Sub DisplayControls()
    
    With Me
        
        If g_bAllowCarrier Then
            .chkCarrierBold.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_CarrierBoldVisible))
            .chkCarrierItalic.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_CarrierItalicVisible))
            .chkCarrierUnderScored.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_CarrierUnderscoredVisible))
            .chkCarrierFlushLeft.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_CarrierUnderscoreLastVisible))
            .lblCarrierFormats.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_CarrierLabelVisible))
        Else
            .chkCarrierBold.Visible = False
            .chkCarrierItalic.Visible = False
            .chkCarrierUnderScored.Visible = False
            .chkCarrierFlushLeft.Visible = False
            .lblCarrierFormats.Visible = False
        End If
    
        If g_bAllowReline Then
            .chkRelineBold.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_RelineBoldVisible))
            .chkRelineItalic.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_RelineItalicVisible))
            .chkRelineUnderscored.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_RelineUnderscoredVisible))
            .chkRelineFlushLeft.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_RelineUnderscoreLastVisible))
            .lblRelineFormats.Visible = Abs((g_lFormatFormConfigValues And mpRelineFF_RelineLabelVisible))
        Else
            .chkRelineBold.Visible = False
            .chkRelineItalic.Visible = False
            .chkRelineUnderscored.Visible = False
            .chkRelineFlushLeft.Visible = False
            .lblRelineFormats.Visible = False
        End If
    
    End With


End Sub

Private Sub PositionControls()
    Dim i As Integer
    Dim oCtl As Control
    Dim lAdjust As Long
    Dim xAR As XArrayDB
    Dim iCount As Integer

'---create array of control name and tab index props, then sort
    Set xAR = New XArrayDB
    
    xAR.ReDim 0, Me.Controls.Count - 1, 0, 2

    For Each oCtl In Me.Controls
        xAR.Value(iCount, 0) = oCtl.Name
        xAR.Value(iCount, 1) = oCtl.TabIndex
        xAR.Value(iCount, 2) = oCtl.Top
        iCount = iCount + 1
    Next
    
    xAR.QuickSort 0, xAR.UpperBound(1), 1, XORDER_ASCEND, XTYPE_LONG
    
'---Check visibility, then reset position
    For i = 0 To xAR.UpperBound(1) - 2
        Set oCtl = Me.Controls(xAR.Value(i, 0))
        If oCtl.Visible = False Then
            lAdjust = lAdjust + (xAR.Value(i + 1, 2) - oCtl.Top)
        End If
        oCtl.Top = oCtl.Top - lAdjust
    Next


End Sub



Private Sub CollectFormatValues()
    Dim lValues As Long
    On Error GoTo ProcError
    
    With Me
'---carrier formats
        If g_bAllowCarrier Then
            If .chkCarrierBold.Value = vbChecked Then
                lValues = lValues Or mpRelineVI_CarrierBold
            End If
            If .chkCarrierItalic.Value = vbChecked Then
                lValues = lValues Or mpRelineVI_CarrierItalic
            End If
            If .chkCarrierUnderScored.Value = vbChecked Then
                lValues = lValues Or mpRelineVI_CarrierUnderscored
            End If
            If .chkCarrierFlushLeft.Value = vbChecked Then
                lValues = lValues Or mpRelineVI_CarrierUnderscoreLast
            End If
        End If
        
'---reline formats
        If .chkRelineBold.Value = vbChecked Then
            lValues = lValues Or mpRelineVI_RelineBold
        End If
        If .chkRelineItalic.Value = vbChecked Then
            lValues = lValues Or mpRelineVI_RelineItalic
        End If
        If .chkRelineUnderscored.Value = vbChecked Then
            lValues = lValues Or mpRelineVI_RelineUnderscored
        End If
        If .chkRelineFlushLeft.Value = vbChecked Then
            lValues = lValues Or mpRelineVI_RelineUnderscoreLast
        End If

    End With

    g_bFormatChanged = g_lFormatValues <> lValues
    
    g_lFormatValues = lValues
    
    Exit Sub
ProcError:
    g_oError.RaiseError "Reline.frmFormat:CollectFormatValues"
End Sub


Private Sub InitializeControls()
    On Error GoTo ProcError
'---carrier values
    Me.chkCarrierBold.Value = Abs((g_lFormatValues And mpRelineVI_CarrierBold) = mpRelineVI_CarrierBold)
    Me.chkCarrierUnderScored.Value = Abs((g_lFormatValues And mpRelineVI_CarrierUnderscored) = mpRelineVI_CarrierUnderscored)
    Me.chkCarrierItalic.Value = Abs((g_lFormatValues And mpRelineVI_CarrierItalic) = mpRelineVI_CarrierItalic)
    Me.chkCarrierFlushLeft.Value = Abs((g_lFormatValues And mpRelineVI_CarrierUnderscoreLast) = mpRelineVI_CarrierUnderscoreLast)

'---reline values
    Me.chkRelineBold.Value = Abs((g_lFormatValues And mpRelineVI_RelineBold) = mpRelineVI_RelineBold)
    Me.chkRelineUnderscored.Value = Abs((g_lFormatValues And mpRelineVI_RelineUnderscored) = mpRelineVI_RelineUnderscored)
    Me.chkRelineItalic.Value = Abs((g_lFormatValues And mpRelineVI_RelineItalic) = mpRelineVI_RelineItalic)
    Me.chkRelineFlushLeft.Value = Abs((g_lFormatValues And mpRelineVI_RelineUnderscoreLast) = mpRelineVI_RelineUnderscoreLast)
    Exit Sub

ProcError:
    g_oError.RaiseError "Reline.frmFormat:InitializeControls"
End Sub


Private Sub ResizeForm()
    Dim ctl As Control
    Dim iAdjust As Integer
    For Each ctl In Me.Controls
        If ctl.Visible = False Then
            iAdjust = iAdjust + ctl.Height
        End If
    Next
    Me.Height = Me.Height - iAdjust
    '---adjust buttons
    If iAdjust <> 0 Then
        Me.btnFormatCancel.Top = Me.Height - btnFormatCancel.Height - 585
        btnFormatOK.Top = btnFormatCancel.Top
    End If
End Sub
