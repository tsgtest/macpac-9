VERSION 5.00
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Begin VB.UserControl RelineControl 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BackStyle       =   0  'Transparent
   ClientHeight    =   5508
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6936
   ScaleHeight     =   5508
   ScaleWidth      =   6936
   Begin VB.CheckBox chkCarrier 
      Appearance      =   0  'Flat
      Caption         =   "Spe&cial"
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   60
      TabIndex        =   3
      Top             =   795
      Width           =   3210
   End
   Begin VB.CheckBox chkReline 
      Appearance      =   0  'Flat
      Caption         =   "&Reline"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   375
      TabIndex        =   2
      Top             =   1665
      Visible         =   0   'False
      Width           =   780
   End
   Begin VB.TextBox txtReline 
      Appearance      =   0  'Flat
      Height          =   765
      Left            =   1320
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   1260
      Width           =   3690
   End
   Begin TrueDBGrid60.TDBGrid grdCarrier 
      Height          =   765
      Left            =   0
      OleObjectBlob   =   "ReLine.ctx":0000
      TabIndex        =   0
      Top             =   0
      Width           =   3675
   End
   Begin VB.Menu mnuCP 
      Caption         =   "&Carrier Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuCPClearRow 
         Caption         =   "&Clear Row"
      End
      Begin VB.Menu mnuCPDeleteRow 
         Caption         =   "&Delete Row"
      End
      Begin VB.Menu mnuCPDeleteAll 
         Caption         =   "Delete &All"
      End
      Begin VB.Menu mnuCPInsertRow 
         Caption         =   "Insert &Row"
      End
      Begin VB.Menu mnuCPSep1 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuCPPrefill 
         Caption         =   "Prefill1"
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu mnuCPSep2 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuCPFormat 
         Caption         =   "&Format Re Line"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuRLP 
      Caption         =   "Reline Popup"
      Visible         =   0   'False
      Begin VB.Menu mnuRLPCut 
         Caption         =   "Cut"
      End
      Begin VB.Menu mnuRLPCopy 
         Caption         =   "Copy"
      End
      Begin VB.Menu mnuRLPPaste 
         Caption         =   "Paste"
      End
      Begin VB.Menu mnuRLPDelete 
         Caption         =   "Delete"
      End
      Begin VB.Menu mnuRLPSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRLPClear 
         Caption         =   "&Clear All"
      End
      Begin VB.Menu mnuRLPSep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRLPFormat 
         Caption         =   "&Format Re Line"
      End
   End
End
Attribute VB_Name = "RelineControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Event TogglePane()
Public Event FormatChanged()

'---Default Property Values:
Const m_def_lFormatFormConfigValues As Long = 1023 '---this value displays all controls in reline format form
Const m_def_lConfigValues As Long = 4 '---default = allow both w/ reline as default
Const m_def_lGridRows As Long = 3

Public Enum mpRelineDefaultTab
    [Default to Reline] = 0
    [Default to Grid] = 1
End Enum

Public Enum mpRelineVisiblePane
    [Reline Textbox] = 0
    [Reline Grid] = 1
End Enum

'---reline formatting values enumeration
Public Enum mpRelineControlValueItems
    mpRelineVI_RelineUnderscoreLast = 1
    mpRelineVI_RelineBold = 2
    mpRelineVI_RelineItalic = 4
    mpRelineVI_RelineUnderscored = 8
    mpRelineVI_CarrierUnderscoreLast = 16
    mpRelineVI_CarrierBold = 32
    mpRelineVI_CarrierItalic = 64
    mpRelineVI_CarrierUnderscored = 128
End Enum

'---Format dialog configuration enumeration
Public Enum mpRelineFormatFormConfigItems
    mpRelineFF_RelineBoldVisible = 1
    mpRelineFF_RelineItalicVisible = 2
    mpRelineFF_RelineUnderscoredVisible = 4
    mpRelineFF_RelineUnderscoreLastVisible = 8
    mpRelineFF_RelineLabelVisible = 16
    mpRelineFF_CarrierBoldVisible = 32
    mpRelineFF_CarrierItalicVisible = 64
    mpRelineFF_CarrierUnderscoredVisible = 128
    mpRelineFF_CarrierUnderscoreLastVisible = 256
    mpRelineFF_CarrierLabelVisible = 512
End Enum


'---not used -- could be implemented for controlling popup menu picks
'Public Enum mpRelineControlMenuConfigItems
'    mpRelineCMCI_ClearRow = 1
'    mpRelineCMCI_DeleteRow = 2
'    mpRelineCMCI_InsertRow = 4
'    mpRelineCMCI_DeleteAll = 8
'    mpRelineCMCI_Sep1Visible = 16
'    mpRelineCMCI_Sep2Visible = 32
'    mpRelineCMCI_Sep3Visible = 64
'    mpRelineCMCI_PrefillsVisible = 128
'End Enum

'---general control config
Public Enum mpRelineConfigurationItems
    [Show Reline Only] = 1
    [Show Grid Only] = 2
    [Show Both] = 4
'    mpReline_AllowAllCarrier = 8
End Enum


'---grid row display

Public Enum mpRelineGridRows
    mpReline_Gridrows1 = 1
    mpReline_Gridrows2 = 2
    mpReline_Gridrows3 = 3
    mpReline_Gridrows4 = 4
End Enum

'---service exclusive carrier/reline buttons
Private m_bButtonFlag As Boolean
Private m_bAllowCarrier As Boolean
Private m_bAllowReline As Boolean
Private m_bLoadDefault As Boolean

Private m_xARCarrierValues As XArray

Private m_xCarrierValues As String
Private m_xRelineValues As String

Private m_bIncludeSpecialReline As Boolean
Private m_lConfigValues As Long
Private m_lGridRows As Long
Private m_iDefaultTab As mpRelineDefaultTab
Private m_ofont As StdFont
Private m_iVisiblePane As mpRelineVisiblePane



'***************************
'---props
'***************************

Public Property Let BackColor(ByVal NewValue As OLE_COLOR)
Attribute BackColor.VB_Description = "sets control back color"
    UserControl.grdCarrier.BackColor = NewValue
    UserControl.txtReline.BackColor = NewValue
    PropertyChanged "BackColor"
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.grdCarrier.BackColor
End Property

Public Property Let Caption(ByVal NewValue As String)
    UserControl.chkCarrier.Caption = NewValue
    PropertyChanged "Caption"
End Property

Public Property Get Caption() As String
    Caption = UserControl.chkCarrier.Caption
End Property

Public Property Set Font(ByVal NewFont As StdFont)
    Set m_ofont = NewFont

    With m_ofont
        .Bold = NewFont.Bold
        .Name = NewFont.Name
        .Italic = NewFont.Italic
        .Size = NewFont.Size
        .Underline = NewFont.Underline
    End With
    
    Set UserControl.grdCarrier.Font = m_ofont
    Set UserControl.txtReline.Font = m_ofont
    
    With grdCarrier.Font
        .Bold = m_ofont.Bold
        .Name = m_ofont.Name
        .Italic = m_ofont.Italic
        .Size = m_ofont.Size
        .Underline = m_ofont.Underline
    End With
    
'    Set UserControl.lstAttys.Font = NewFont
    With txtReline.Font
        .Bold = m_ofont.Bold
        .Name = m_ofont.Name
        .Italic = m_ofont.Italic
        .Size = m_ofont.Size
        .Underline = m_ofont.Underline
    End With
    
    Refresh
    
    PropertyChanged "Font"
End Property

Public Property Get Font() As StdFont
    Set Font = m_ofont
End Property

Public Property Let Visible(ByVal NewValue As Boolean)
    UserControl.Extender.Visible = NewValue
    PropertyChanged "Visible"
End Property

Public Property Get Visible() As Boolean
    Visible = UserControl.Extender.Visible
End Property

Public Property Let LoadDefault(ByVal NewValue As Boolean)
    m_bLoadDefault = NewValue
    PropertyChanged "LoadDefault"
End Property

Public Property Get LoadDefault() As Boolean
    LoadDefault = m_bLoadDefault
End Property

Public Property Let CarrierPrefillLists(xARNew As XArray)
Attribute CarrierPrefillLists.VB_Description = "xArray containing delimited strings of carrier prefill lists -- used to build popup menu selection of available prefills "
    Set g_xARCarrierPrefillLists = xARNew
    ConfigurePopupMenu
End Property

Public Property Get CarrierPrefillLists() As XArray
    Set CarrierPrefillLists = g_xARCarrierPrefillLists
End Property
Public Property Let FormatValues(lNew As Long)
    If lNew <> g_lFormatValues Then
        RaiseEvent FormatChanged
    End If
    g_lFormatValues = lNew
    PropertyChanged "FormatValues"
End Property

Public Property Get FormatValues() As Long
Attribute FormatValues.VB_Description = "Bitwise integer reflecting state of dedicated formatting controls"
Attribute FormatValues.VB_MemberFlags = "400"
    FormatValues = g_lFormatValues
End Property

''''Public Property Let DefaultCarrierList(lNew As Integer)
''''    m_iDefaultCarrierList = lNew
''''    PropertyChanged "DefaultCarrierList"
''''End Property
''''
''''Public Property Get DefaultCarrierList() As Integer
''''    DefaultCarrierList = m_iDefaultCarrierList
''''End Property
''''
Public Property Let FormatMenuConfiguration(iNew As Integer)
Attribute FormatMenuConfiguration.VB_Description = "Bitwise integer hides/displays checkboxes in reline format dialog.  See mpControlItems enumerations for valid value combinations.  Default value of 1023 displays all checkboxes"
    Dim iBit As Integer
    g_lFormatFormConfigValues = iNew
    '---hide the menu picks if config = 0
    UserControl.mnuCPSep2.Visible = Me.FormatMenuConfiguration <> 0
    UserControl.mnuCPFormat.Visible = Me.FormatMenuConfiguration <> 0
    UserControl.mnuRLPSep2.Visible = Me.FormatMenuConfiguration <> 0
    UserControl.mnuRLPFormat.Visible = Me.FormatMenuConfiguration <> 0
    
    PropertyChanged "FormatMenuConfiguration"
End Property

Public Property Get FormatMenuConfiguration() As Integer
    FormatMenuConfiguration = g_lFormatFormConfigValues
End Property

Public Property Let RelineValues(xNew As String)
    m_xRelineValues = xNew
    UserControl.txtReline.Text = xNew
    PropertyChanged "RelineValues"
End Property

Public Property Get RelineValues() As String
Attribute RelineValues.VB_Description = "String containing text input to reline textbox"
Attribute RelineValues.VB_MemberFlags = "400"
    RelineValues = m_xRelineValues
End Property

Public Property Let IncludeSpecialReline(bNew As Boolean)
    m_bIncludeSpecialReline = bNew
End Property
Public Property Get IncludeSpecialReline() As Boolean
    IncludeSpecialReline = m_bIncludeSpecialReline
End Property
Public Property Let CarrierValues(xNew As String)
    Dim xARTemp As XArray
    On Error GoTo ProcError
''''    Set xARTemp = New XArray
    
    If m_xCarrierValues <> xNew And xNew <> "" Then
        '---refresh the carrier grid
        On Error Resume Next
        Set xARTemp = xarStringToxArray(xNew, 2, "|")
        grdCarrier.Bookmark = Null
        grdCarrier.Array = xARTemp
        grdCarrier.ClearFields
        grdCarrier.ReBind
        grdCarrier.Refresh
        If xNew <> "" Then
            grdCarrier.Col = 1
        End If
        grdCarrier.EditActive = True
    End If
    
    m_xCarrierValues = xNew
    PropertyChanged "CarrierValues"
    Exit Property
ProcError:
    g_oError.RaiseError "Reline:CarrierValues"
End Property

Public Property Get CarrierValues() As String
Attribute CarrierValues.VB_Description = "Delimited string holding values input in carrier grid -- read/write"
Attribute CarrierValues.VB_MemberFlags = "400"
    Dim xTemp As String
    Dim i, j, iTrim As Integer
    Dim xARTemp As XArray
    Dim xTest As String
    
    Set xARTemp = grdCarrier.Array
    
    For i = xARTemp.UpperBound(1) To 0 Step -1
        For j = 0 To xARTemp.UpperBound(2)
            xTest = xTest & xARTemp.Value(i, j)
            If xTest = "" Then
                iTrim = iTrim + 1
            End If
        Next j
    Next
    
    iTrim = iTrim / 2
    
    If xTest = "" Then
        CarrierValues = ""
        Exit Property
    End If
    
    '---trim extra rows from xArray if any
    
    If iTrim > 0 Then
        xARTemp.ReDim 0, xARTemp.UpperBound(1) - iTrim, 0, 1
    End If
    
    
    xTemp = XArrayToString(xARTemp, , , True, "~")
    If xTemp = "|" Then
        CarrierValues = ""
        Exit Property
    End If
        
    While InStr(Mid(xTemp, Len(xTemp) - 2, Len(xTemp)), "~|") <> 0
        xTemp = Left(xTemp, Len(xTemp) - 2)
        If xTemp = "|" Then
            CarrierValues = ""
            Exit Property
        End If
    Wend
    CarrierValues = xTemp 'XArrayToString(grdCarrier.Array, , , True, "~")
End Property

Public Property Let Style(iNew As mpRelineConfigurationItems)
Attribute Style.VB_Description = "Enumeration controlling control appearance and available features"
    
    '---sets up control appearance/functionality
    If iNew = 0 Then Exit Property
    m_lConfigValues = iNew
    SetupDisplay
    PropertyChanged "Style"
    UserControl_Resize
End Property

Public Property Get Style() As mpRelineConfigurationItems
    Style = m_lConfigValues
End Property

Public Property Let DefaultTab(iNew As mpRelineDefaultTab)
    
    If Me.Style = mpRelineConfigurationItems.[Show Both] Then
        m_iDefaultTab = iNew
    ElseIf Me.Style = mpRelineConfigurationItems.[Show Grid Only] Then
        m_iDefaultTab = mpRelineDefaultTab.[Default to Grid]
    Else
        m_iDefaultTab = mpRelineDefaultTab.[Default to Reline]
    End If
    SetupDisplay
    PropertyChanged "DefaultTab"
End Property
Public Property Get DefaultTab() As mpRelineDefaultTab
    DefaultTab = m_iDefaultTab
End Property

Public Property Let VisiblePane(iNew As mpRelineVisiblePane)
    m_iVisiblePane = iNew
End Property
Public Property Get VisiblePane() As mpRelineVisiblePane
    VisiblePane = m_iVisiblePane
End Property

Public Property Let GridRows(lNew As mpRelineGridRows)
Attribute GridRows.VB_Description = "Sets number of rows the grid will display.  Valid range = 1 to 4"
Attribute GridRows.VB_ProcData.VB_Invoke_PropertyPut = ";Appearance"
    On Error GoTo ProcError
    m_lGridRows = Max(CDbl(lNew), 1)
    m_lGridRows = Min(CDbl(m_lGridRows), 4)
    m_xARCarrierValues.ReDim 0, m_lGridRows - 1, 0, 1
    
    With grdCarrier
        .Array = m_xARCarrierValues
        .ReBind
        .Bookmark = 0
        .RowHeight = (.Height / Me.GridRows) - 5
    End With
    
    PropertyChanged "GridRows"
    Exit Property
ProcError:
    g_oError.RaiseError "Reline:GridRows"
End Property

Public Property Get GridRows() As mpRelineGridRows
    GridRows = m_lGridRows
End Property


'***************************
'---control event code
'***************************

Private Sub chkReline_Click()
    On Error GoTo ProcError
    If m_bButtonFlag Then Exit Sub
    m_bButtonFlag = True
    chkCarrier.Value = 1 - chkReline.Value
    
    txtReline.ZOrder (chkCarrier.Value)
    txtReline.Enabled = chkReline = vbChecked
    grdCarrier.Enabled = chkCarrier = vbChecked
    
    On Error Resume Next
    If chkCarrier.Value = vbChecked Then
        grdCarrier.SetFocus
        Me.VisiblePane = [Reline Grid]
    Else
        txtReline.SetFocus
        Me.VisiblePane = [Reline Textbox]
    End If
    RaiseEvent TogglePane
    m_bButtonFlag = False
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub chkCarrier_Click()
    On Error GoTo ProcError
    If m_bButtonFlag Then Exit Sub
    m_bButtonFlag = True
    chkReline.Value = 1 - chkCarrier.Value
    If chkCarrier.Value = vbChecked Then
        Me.VisiblePane = [Reline Grid]
    Else
        Me.VisiblePane = [Reline Textbox]
    End If
    
    txtReline.ZOrder (chkCarrier.Value)
    grdCarrier.ZOrder (chkReline.Value)
    txtReline.Enabled = chkReline = vbChecked
    grdCarrier.Enabled = chkCarrier = vbChecked
    
    On Error Resume Next

    If chkCarrier.Value = vbChecked Then
        grdCarrier.SetFocus
        If Me.CarrierValues = "" Then
            If Me.CarrierPrefillLists.Count(1) > -1 And Me.LoadDefault = True Then
                If GetDefaultPrefill <> -1 Then
                    mnuCPPrefill_Click GetDefaultPrefill
                End If
            End If
        End If
        Me.IncludeSpecialReline = True
    Else
        txtReline.SetFocus
        Me.IncludeSpecialReline = False
    End If
    RaiseEvent TogglePane
    m_bButtonFlag = False
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub chkCarrier_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        chkCarrier.Value = 1 - chkCarrier.Value
    End If
End Sub

Private Sub chkReline_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then
        chkReline.Value = 1 - chkReline.Value
    End If
End Sub

Private Sub grdCarrier_AfterColEdit(ByVal ColIndex As Integer)
    grdCarrier.Update
    'Me.CarrierValues = XArrayToString(grdCarrier.Array)
    m_xCarrierValues = XArrayToString(grdCarrier.Array, , , True, "~")
End Sub

Private Sub grdCarrier_AfterColUpdate(ByVal ColIndex As Integer)
    grdCarrier.Update
    'Me.CarrierValues = XArrayToString(grdCarrier.Array)
    m_xCarrierValues = XArrayToString(grdCarrier.Array, , , True, "~")
End Sub

Private Sub grdCarrier_ColResize(ByVal ColIndex As Integer, Cancel As Integer)

    'grdCarrier.Columns(1).Width = UserControl.Width - grdCarrier.Columns(0).Width - 200
    grdCarrier.Columns(1).Width = (UserControl.Width - grdCarrier.Columns(0).Width) - 290

End Sub

Private Sub grdCarrier_GotFocus()
    grdCarrier.TabAction = 2
End Sub

Private Sub grdCarrier_KeyDown(KeyCode As Integer, Shift As Integer)


    If KeyCode = 13 And grdCarrier.Col = 1 Then    '---move down column 1
        KeyCode = 0
        GridMoveNext UserControl.grdCarrier
    End If

    If KeyCode = 9 Then    'tab out of grid if last col/last row
        If grdCarrier.Bookmark <= 0 And Shift <> 0 And grdCarrier.Col = 0 Then
            grdCarrier.TabAction = 0
            Exit Sub
        End If

        If grdCarrier.RowBookmark(grdCarrier.Row) = grdCarrier.Array.UpperBound(1) Or _
            IsNull(grdCarrier.RowBookmark(grdCarrier.Row)) Then
            If grdCarrier.Col = 1 Then
                '---change grid navigation to enable tab out
                If Shift = 0 Then
                    grdCarrier.TabAction = 1
                End If
            End If
        End If

    End If

End Sub


Private Sub grdCarrier_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        UserControl.PopupMenu mnuCP
    End If
End Sub


Private Sub mnuCPClearRow_Click()
    Dim iRow As Integer
    Dim iBookmark As Integer
    Dim iRowBookmark As Integer
    Dim i As Integer
    
    With UserControl.grdCarrier
        iBookmark = .Bookmark
        iRow = .Row
        iRowBookmark = .RowBookmark(.Row)
        For i = 0 To .Array.UpperBound(2)
            .Array.Value(iRowBookmark, i) = ""
        Next
        .ReBind
        .Bookmark = iRowBookmark
        .EditActive = True
    End With
End Sub

Private Sub mnuCPDeleteAll_Click()
    On Error Resume Next
    With grdCarrier
        .Array.ReDim 0, -1, 0, -1
        .ReBind
        .Array.ReDim 0, 1, 0, 1
        .ReBind
        .Refresh
    End With
    
    Me.CarrierValues = ""
    grdCarrier.Bookmark = Null

    Dim i As Integer
    For i = 0 To mnuCPPrefill.Count
        mnuCPPrefill(i).Checked = False
    Next

End Sub

Private Sub mnuCPDeleteRow_Click()
    Dim iBookmark As Integer
    On Error Resume Next
    With grdCarrier
        
        .Delete
        
        If .Array.UpperBound(1) = -1 Then
            .Array.ReDim 0, -1, 0, -1
            .ReBind
            .Refresh
            .Array.ReDim 0, 0, 0, 1
            .ReBind
            .Refresh
        End If
    
    End With
    
    grdCarrier.EditActive = True
    grdCarrier.Bookmark = Null
End Sub

Private Sub mnuCPFormat_Click()
    ShowFormatMenu
End Sub

Private Sub mnuCPInsertRow_Click()
    Dim iRow As Integer
    Dim iBookmark As Integer
    Dim iRowBookmark As Integer
    Dim i As Integer
    On Error Resume Next
    With UserControl.grdCarrier
        .Update
        iBookmark = .Bookmark
        iRow = .Row
        iRowBookmark = .RowBookmark(.Row)
        .Array.Insert 1, iRowBookmark
        
        .ReBind
''''        .Bookmark = Max(CDbl(iRowBookmark + 1), 0)
        .EditActive = True
        .Bookmark = Null
    End With

End Sub

Private Sub mnuCPPrefill_Click(Index As Integer)
    Dim i As Integer
    On Error Resume Next
    Me.CarrierValues = mnuCPPrefill(Index).Tag
'    If Err Then Exit Sub
    For i = 0 To Me.CarrierPrefillLists.Count(1) - 1
        mnuCPPrefill(i).Checked = False
    Next i
    If mnuCPPrefill(Index).Caption <> "Reset to Defaults" Then
        mnuCPPrefill(Index).Checked = True
    End If
End Sub

Private Sub mnuRLPClear_Click()
    UserControl.txtReline = ""
    Me.RelineValues = ""
End Sub

Private Sub mnuRLPFormat_Click()
    ShowFormatMenu
End Sub

Private Sub mnuRLPDelete_Click()
    UserControl.txtReline.SelText = ""
End Sub

Private Sub mnuRLPCopy_Click()
    Clipboard.Clear
    Clipboard.SetText UserControl.txtReline.SelText
End Sub

Private Sub mnuRLPCut_Click()
'   First do the same as a copy.
    mnuRLPCopy_Click
'   clear contents of active control.
    UserControl.txtReline.SelText = ""
End Sub

Private Sub mnuRLPPaste_Click()
    UserControl.txtReline.SelText = Clipboard.GetText()
End Sub

Private Sub txtReline_LostFocus()
    Me.RelineValues = txtReline.Text
End Sub

Private Sub txtReline_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oCtl As Control
    On Error Resume Next
    If Button = 2 Then
        '---suppress default right click menu
        LockWindowUpdate txtReline.hWnd
        'UserControl.chkCarrier.SetFocus
        Set oCtl = txtReline
        txtReline.Enabled = False
        DoEvents
        UserControl.PopupMenu mnuRLP
        '---return focus and unlock
        txtReline.Enabled = True
        LockWindowUpdate 0&
        txtReline.SetFocus
        Set oCtl = Nothing
    End If

End Sub

'********************************
'---user control event code
'********************************

Private Sub UserControl_Initialize()
    On Error GoTo ProcError
    '---create error object
    Set g_oError = New CError
    '---set reline button on
    m_bButtonFlag = True
    UserControl.chkReline.Value = vbChecked
    m_bButtonFlag = False
    '---initialize array for carrier grid
    Set m_xARCarrierValues = New XArray
    m_xARCarrierValues.ReDim 0, m_def_lGridRows, 0, 1
    
    Set g_xARCarrierPrefillLists = New XArray
    g_xARCarrierPrefillLists.ReDim 0, 0, 0, 3
    grdCarrier.Array = m_xARCarrierValues
    grdCarrier.ReBind
    grdCarrier.Bookmark = 0
     
    Set m_ofont = New StdFont
   
   
    Exit Sub
ProcError:
    g_oError.RaiseError "Reline:Initialize"
End Sub

Private Sub UserControl_InitProperties()
    g_lFormatFormConfigValues = m_def_lFormatFormConfigValues
    m_lConfigValues = m_def_lConfigValues
    m_lGridRows = m_def_lGridRows
End Sub

Private Sub UserControl_Resize()
     
     On Error GoTo ProcError
    
    
    '---align text boxes
    With UserControl
        
        .txtReline.Width = UserControl.Width
        .grdCarrier.Width = UserControl.Width
        .txtReline.Left = 0
        .grdCarrier.Left = 0
        .txtReline.Top = 0
        
        If Me.Style = mpRelineConfigurationItems.[Show Both] Then
            .chkCarrier.Top = .Height - .chkCarrier.Height + 60
            .grdCarrier.Height = .Height - .chkCarrier.Height '  - 15
        Else
            .grdCarrier.Height = .Height
        End If
        
        
        .txtReline.Height = .grdCarrier.Height
       
        
        
        
        With .grdCarrier
            .Top = 0
            .Columns(0).Width = (UserControl.Width / 2) - 420
            .Columns(1).Width = (UserControl.Width - .Columns(0).Width) - 290
'---resize event runs before read properties on init, so we divide by at least 1
            .RowHeight = (.Height / Max(CDbl(Me.GridRows), 1)) - 5
        End With
        
        .chkReline.Top = .chkCarrier.Top
        
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "Reline:Resize"
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    Call PropBag.WriteProperty("Style", Me.Style, 4)
    Call PropBag.WriteProperty("BackColor", Me.BackColor, vbWhite)
    Call PropBag.WriteProperty("LoadDefault", Me.LoadDefault, True)
    Call PropBag.WriteProperty("FormatMenuConfiguration", Me.FormatMenuConfiguration, 1023)
    Call PropBag.WriteProperty("GridRows", Me.GridRows, m_def_lGridRows)
    Call PropBag.WriteProperty("DefaultTab", Me.DefaultTab, 0)
    Call PropBag.WriteProperty("Caption", Me.Caption, "Spe&cial")
    With m_ofont
        PropBag.WriteProperty "FontName", .Name, "MS Sans Serif"
        PropBag.WriteProperty "FontBold", .Bold
        PropBag.WriteProperty "FontSize", .Size, 8
        PropBag.WriteProperty "FontItalic", .Italic
        PropBag.WriteProperty "FontUnderline", .Underline
    End With
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error Resume Next
    LoadDefault = PropBag.ReadProperty("LoadDefault", True)
    Style = PropBag.ReadProperty("Style", m_def_lConfigValues)
    FormatMenuConfiguration = PropBag.ReadProperty("FormatMenuConfiguration", m_def_lFormatFormConfigValues)
    BackColor = PropBag.ReadProperty("BackColor", vbWhite)
    GridRows = PropBag.ReadProperty("GridRows", m_def_lGridRows)
    DefaultTab = PropBag.ReadProperty("DefaultTab", 0)
    Caption = PropBag.ReadProperty("Caption", "Spe&cial")
'---handle font object
    With m_ofont
        .Name = PropBag.ReadProperty("FontName", "MS San Serif")
        .Size = PropBag.ReadProperty("FontSize", 8)
        .Bold = PropBag.ReadProperty("FontBold")
        .Underline = PropBag.ReadProperty("FontUnderline")
        .Italic = PropBag.ReadProperty("FontItalic")
    End With
    Set Me.Font = m_ofont
End Sub

Friend Sub ShowFormatMenu()
    Dim frmFormat As Form
    On Error GoTo ProcError
    Set frmFormat = New frmFormat
    
    With frmFormat
        '---account for screen position of parent form containing control
        .Top = UserControl.Parent.Top + UserControl.Extender.Top + 450 '350
        .Left = UserControl.Parent.Left + UserControl.Extender.Left '+ txtReline.Width - 190
        .Show vbModal, UserControl
    End With
    
    If g_bFormatChanged Then
        RaiseEvent FormatChanged
    End If
    
    Unload frmFormat
    Set frmFormat = Nothing
    Exit Sub
ProcError:
    g_oError.RaiseError "ReLine Control:ShowFormatMenu"
End Sub


'***************************
'---methods
'***************************


Private Sub ConfigurePopupMenu()
    On Error GoTo ProcError
    '---initialize popup menu
    Dim i As Integer
    
    mnuCPSep1.Visible = True
    
    '---set first item in array index to visible
    '---this indexed item has already been created at design time
    
    With mnuCPPrefill(0)
        .Visible = True
        If Me.CarrierPrefillLists.UpperBound(1) = 0 Then
            .Caption = "Reset to Defaults"
        Else
            .Caption = Me.CarrierPrefillLists.Value(0, 1)
        End If
        '---store the delimited string prefill in control tag
        .Tag = Me.CarrierPrefillLists.Value(i, 2)
    End With
    For i = 1 To Me.CarrierPrefillLists.UpperBound(1)
        '---use the load method to add new menu items for each prefill record in tblCarrierPrefills
        Load mnuCPPrefill(i)
        With mnuCPPrefill(i)
            .Visible = True
            .Caption = Me.CarrierPrefillLists.Value(i, 1)
            '---store the delimited string prefill list in control tag
            .Tag = Me.CarrierPrefillLists.Value(i, 2)
        End With
    Next i
    
''''    If mnuCPPrefill.Count > 0 Then
''''        mnuCPPrefill_Click (0)
''''    End If
    
        '---hide the menu picks if config = 0
        UserControl.mnuCPSep2.Visible = Me.FormatMenuConfiguration <> 0
        UserControl.mnuCPFormat.Visible = Me.FormatMenuConfiguration <> 0
    
    
    
    Exit Sub
ProcError:
    g_oError.RaiseError "Reline:ConfigurePopupMenu"
End Sub

Private Sub SetupDisplay()
    Dim sAdjust As Single
    Dim bShowButton As Boolean
    Dim bShowGrid As Boolean
    
    bShowButton = m_lConfigValues = mpRelineConfigurationItems.[Show Both]
    
    bShowGrid = (m_lConfigValues = mpRelineConfigurationItems.[Show Both] And m_iDefaultTab = 1) Or _
                m_lConfigValues = mpRelineConfigurationItems.[Show Grid Only]
    
    m_bAllowCarrier = m_lConfigValues <> mpRelineConfigurationItems.[Show Reline Only]
    m_bAllowReline = m_lConfigValues <> mpRelineConfigurationItems.[Show Grid Only]
    
    '---temporary - move global vars into frmFormat props
''''    g_bAllowCarrier = m_bAllowCarrier
'    g_bAllowReline = m_bAllowReline
    
    '---temporary only - for mp90 9.7.2 we're only exposing formatting for entire block
    g_bAllowReline = m_bAllowReline Or m_bAllowCarrier
    
    UserControl.chkCarrier.Visible = bShowButton
'    UserControl.chkReline.Visible = bShowButton
    
    If bShowButton = True Then
        If bShowGrid Then
            UserControl.txtReline.ZOrder (1)
            UserControl.txtReline.Enabled = False
            UserControl.grdCarrier.ZOrder (0)
            UserControl.grdCarrier.Enabled = True
            UserControl.chkCarrier.Value = vbChecked
            UserControl.chkReline.Value = vbUnchecked
            Me.VisiblePane = [Reline Grid]
        Else
            UserControl.txtReline.ZOrder (0)
            UserControl.txtReline.Enabled = True
            UserControl.grdCarrier.ZOrder (1)
            UserControl.grdCarrier.Enabled = False
            UserControl.chkCarrier.Value = vbUnchecked
            UserControl.chkReline.Value = vbChecked
            Me.VisiblePane = [Reline Textbox]
        End If
    Else
        If Me.DefaultTab = [Default to Grid] Then
            Me.VisiblePane = [Reline Grid]
        Else
            Me.VisiblePane = [Reline Textbox]
        End If
        
        UserControl.txtReline.ZOrder (Abs(CInt(bShowGrid = True)))
        UserControl.txtReline.Enabled = bShowGrid = False
        UserControl.grdCarrier.ZOrder (Abs(CInt(bShowGrid = False)))
        UserControl.grdCarrier.Enabled = bShowGrid = True
    
        If m_lConfigValues = mpRelineConfigurationItems.[Show Grid Only] Then
            If Me.CarrierValues = "" Then
                If Me.CarrierPrefillLists.Count(1) > -1 And Me.LoadDefault = True Then
                    If GetDefaultPrefill <> -1 Then
                        mnuCPPrefill_Click GetDefaultPrefill
                    End If
                End If
            End If
        End If
    
    End If
    
''''    If bShowButton Then
            sAdjust = UserControl.chkCarrier.Height + 60
''''    End If

'    With UserControl
'        .txtReline.Height = .Height - sAdjust
'        .grdCarrier.Height = .txtReline.Height
'        .chkCarrier.Top = .Height - .chkCarrier.Height
'        .chkReline.Top = .chkCarrier.Top
'    End With

''''    Me.RowHeight = m_iRowHeight

End Sub


Private Function GetDefaultPrefill() As Integer
    Dim i, j As Integer
        j = -1
    For i = 0 To g_xARCarrierPrefillLists.UpperBound(1)
        If g_xARCarrierPrefillLists.Value(i, 3) = True Then
            j = i
            Exit For
        End If
    Next i
    GetDefaultPrefill = j
End Function

