Attribute VB_Name = "mdlUtilities"
Option Explicit

Public Sub GridTabOut(grdGrid As TDBGrid, bEnter As Boolean, iColtoCheck As Integer)
    '---this sub allows a tabout from grid when navigation param is set to grid navigation
    '---normally tabbing halts at last item in grid
    On Error Resume Next
    Select Case bEnter
        Case True
            grdGrid.TabAction = 2
        Case False
'            Debug.Print "Row " & grdGrid.RowBookmark(grdGrid.Row)
'            Debug.Print "UpperBound " & grdGrid.Array.UpperBound(1)
'            Debug.Print "LowerBound " & grdGrid.Array.LowerBound(1)
'            Debug.Print "iColtoCheck " & iColtoCheck
            If grdGrid.RowBookmark(grdGrid.Row) = grdGrid.Array.UpperBound(1) Or _
                    grdGrid.RowBookmark(grdGrid.Row) = grdGrid.Array.LowerBound(1) Or _
                    IsNull(grdGrid.RowBookmark(grdGrid.Row)) Then
                If grdGrid.Col = iColtoCheck Then
                    '---change grid navigation to enable tab out
                    grdGrid.TabAction = 1
                End If
            End If
    Case Else
    End Select
End Sub

'Public Function GridMoveNext(grdCur As TDBGrid, Optional bNextRecord As Boolean = False)
'    ' Move to next column or row
'    ' Redim array and add blank row if necessary
'    On Error Resume Next
'    With grdCur
'        If Not GridCurrentRowIsEmpty(grdCur) Then
'            Dim lCol As Long
'            lCol = .Col
'            .Update
'            .Array.ReDim 0, Max(CDbl(.Bookmark + 1), .Array.UpperBound(1)), 0, .Columns.Count - 1
'            .ReBind
'            If lCol = 0 Then
'                .Col = 1
'            Else
'                If .Bookmark < grdCur.Array.UpperBound(1) Then
'                    .MoveNext
'                Else
'                    .MoveLast
'                End If
'                .Col = 0
'            End If
'            .EditActive = True
'        End If
'        .EditActive = True
'    End With
'End Function


Public Function GridMoveNext(grdCur As TDBGrid, Optional bNextRecord As Boolean = False)
    ' Move to next column or row
    ' Redim array and add blank row if necessary
    On Error Resume Next
    With grdCur
        If .Bookmark < grdCur.Array.UpperBound(1) Then
            '---
            .MoveNext
        ElseIf Not GridCurrentRowIsEmpty(grdCur) Then
            .Update
            .Array.ReDim 0, .Bookmark + 1, 0, .Columns.Count - 1
            .ReBind
            .MoveLast
            .Col = 0
            .EditActive = True
        End If
    End With
End Function




Public Function GridCurrentRowIsEmpty(grdTest As TDBGrid) As Boolean
    Dim i As Integer
    On Error Resume Next
    GridCurrentRowIsEmpty = True
    With grdTest
        For i = 0 To .Columns.Count - 1
            If .Columns(i).Text <> "" Then
                GridCurrentRowIsEmpty = False
                Exit Function
            End If
        Next i
    End With
End Function


Function xarStringToxArray(ByVal xString As String, _
                        Optional iNumCols As Integer = 1, _
                        Optional xSep As String = "|") As XArray

    Dim iNumSeps As Integer
    Dim iNumEntries As Integer
    Dim iSepPos As Integer
    Dim xEntry As String
    Dim i As Integer
    Dim j As Integer
    Dim xarP As XArray
    
'   get ubound of arrP - count delimiter
'   then divide by iNumCols
    iNumSeps = lCountChrs(xString, xSep)
    iNumEntries = (iNumSeps + 1) / iNumCols
    
    Set xarP = New XArray
    
    xarP.ReDim 0, iNumEntries - 1, 0, iNumCols - 1
    
    For i = 0 To iNumEntries - 1
        For j = 0 To iNumCols - 1
'           get next entry & store
            iSepPos = InStr(xString, xSep)
            If iSepPos Then
                xEntry = Left(xString, iSepPos - 1)
            Else
                xEntry = xString
            End If
            xarP.Value(i, j) = xEntry
            
'           remove entry from xstring
            If iSepPos Then _
                xString = Mid(xString, iSepPos + Len(xSep))
        Next j
    Next i
    
    Set xarStringToxArray = xarP
End Function

Function XArrayToString(xarP As XArray, _
                        Optional ByVal xSep As String = "|", _
                        Optional ByVal iColumn As Integer = -1, _
                        Optional bIncludeLF As Boolean = False, _
                        Optional ByVal xLFSep As String = vbCrLf) As String
'converts an XArray to a string delimited by xSep and xLFSep-
'convertys only the specified array column iColumn if specified
    Dim i As Integer
    Dim j As Integer
    Dim xString As String
    
    On Error Resume Next
    
'   concatenate array items to string
    For i = xarP.LowerBound(1) To xarP.UpperBound(1)
        If iColumn > -1 Then
            j = iColumn
            xString = xString & xarP.Value(i, j) & xSep
        Else
            For j = xarP.LowerBound(2) To xarP.UpperBound(2)
               If bIncludeLF Then   '---unzip array w/ record delimiters
                   If j < xarP.UpperBound(2) Then
                        xString = xString & xarP.Value(i, j) & xSep
                    Else
                        xString = xString & xarP.Value(i, j) & xLFSep
                    End If
                Else
                    xString = xString & xarP.Value(i, j) & xSep
                End If
            Next j
        End If
    Next i
    
'   trim final separator
    If Len(xSep) <= Len(vbCrLf) Then
        xString = Left(xString, Len(xString) - Len(xSep))
    Else
        If Right(xString, 1) = vbLf Then
            xString = Left(xString, Len(xString) - 1)
        End If
        If Right(xString, 1) = vbCr Then
            xString = Left(xString, Len(xString) - 1)
        End If
    End If
    
    XArrayToString = xString
End Function

Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function


Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xarP As XArray
    Dim iRows As Integer
    On Error Resume Next
    With tdbCBX
        Set xarP = tdbCBX.Array
        iRows = Min(xarP.Count(1), CDbl(iMaxRows))
        .DropdownHeight = (iRows * .RowHeight)
        If Err.Number = 6 Then
 '---use hard value if there's a processor problem -- err 6 is overflow
            .DropdownHeight = (iRows * 239.811)
        End If
    End With
End Sub

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function


