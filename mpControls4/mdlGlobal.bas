Attribute VB_Name = "mdlGlobal"
Option Explicit

Public g_oError As CError

'---These global vars service props that are passed to and refreshed by frmFormat
Public g_lFormatFormConfigValues As Long
Public g_lFormatValues As Long
Public g_bAllowCarrier As Boolean
Public g_bAllowReline As Boolean
Public g_bFormatChanged As Boolean

'---temp dim as XArray because it's set from DB.Lists
Public g_xARCarrierPrefillLists As XArray
Public g_xCarrierPrefill As String

'---we need this to supress windows default right click menu in text boxes
Public Declare Function LockWindowUpdate Lib "user32" ( _
    ByVal hwndLock As Long) As Long


