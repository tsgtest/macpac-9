VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{76ACB981-5B88-45A3-8021-6C736CAC7713}#7.2#0"; "mpControls4.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4560
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9030
   LinkTopic       =   "Form1"
   ScaleHeight     =   4560
   ScaleWidth      =   9030
   StartUpPosition =   3  'Windows Default
   Begin mpControls4.RelineControl RelineControl1 
      Height          =   1575
      Left            =   540
      TabIndex        =   1
      Top             =   420
      Width           =   4230
      _ExtentX        =   7461
      _ExtentY        =   2778
      BackColor       =   -2147483643
      LoadDefault     =   0   'False
      FontName        =   "Arial"
      FontBold        =   0   'False
      FontSize        =   8.25
      FontItalic      =   0   'False
      FontUnderline   =   0   'False
   End
   Begin TrueDBList60.TDBList TDBList1 
      Height          =   1110
      Left            =   5490
      OleObjectBlob   =   "Tester.frx":0000
      TabIndex        =   0
      Top             =   2655
      Width           =   2220
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click()
    Me.RelineControl1.Style = mpRelineConfigurationItems.[Show Both]
End Sub

Private Sub Form_Load()
'    Dim i As mprelinecontrolitems
'    Dim iFormat As Integer
    
'    iFormat = iFormat Or mprelinecontrolitems.mpRelineControlItem_FlushLeft
'
'    iFormat = iFormat Or mprelinecontrolitems.mpRelineControlItem_Italic
'
'    Me.RelineControl1.FormatValues = 2
'    Me.RelineControl1.Configuration = Me.RelineControl1.Configuration - mpRelineCCI_RelineBoldVisible - mpRelineCCI_CarrierItalicVisible - mpRelineCCI_CarrierBoldVisible
    Me.RelineControl1.FormatValues = 3
'    Dim X As mpDB.CDatabase
'    Dim xList As String
    
'    Set X = New mpDB.CDatabase
    
'    xList = X.Lists("CarrierRelinePrefills").ListItems.Source.Value(0, 2)

''''    Me.RelineControl1.CarrierValues = "Insured:|name& vbLf & Date of Loss:|date|Claimant:|claimaint|Your File Number:|file number|Another Thing|thing"
    
    Dim xARTemp As XArrayDB
    Set xARTemp = New XArrayDB
    
    xARTemp.ReDim 0, 1, 0, 3

    With xARTemp
        .Value(0, 0) = ""
        .Value(0, 1) = "Insurance"
        .Value(0, 2) = "Insured:||Date of Loss:||Claimant:||Your File Number:|"
        .Value(0, 3) = 0
        .Value(1, 0) = ""
        .Value(1, 1) = "Asbestos"
        .Value(1, 2) = "Claimant:||Date of Claim:||Company Name:||Action Number:|"
        .Value(1, 3) = 1

    End With


'''    xARTemp.ReDim 0, 0, 0, 3
'''
'''    With xARTemp
'''        .Value(0, 0) = ""
'''        .Value(0, 1) = "Insurance"
'''        .Value(0, 2) = "Insured:||Date of Loss:||Claimant:||Your File Number:|"
'''        .Value(0, 3) = 0
'''    End With

    Me.RelineControl1.CarrierPrefillLists = xARTemp
    'Me.RelineControl1.CarrierPrefillLists = X.Lists("CarrierRelinePrefills").ListItems.Source
    
'    Me.RelineControl1.AllowCarrier = False
    Me.RelineControl1.Style = [Show Grid Only]

    
    Dim xARTemp2 As XArrayDB
    Set xARTemp2 = New XArrayDB
    xARTemp2.ReDim 0, 3, 0, 1
    
    With xARTemp2
        .Value(0, 0) = mpRelineConfigurationItems.[Show Reline Only]
        .Value(0, 1) = "Reline Only"
        .Value(1, 0) = mpRelineConfigurationItems.[Show Grid Only]
        .Value(1, 1) = "Carrier Reline Only"
        .Value(2, 0) = mpRelineConfigurationItems.[Show Both]
        .Value(2, 1) = "Both"
    End With

    Me.TDBList1.Array = xARTemp2
'    Me.RelineControl1.FormatMenuConfiguration = 0

End Sub

Private Sub RelineControl1_FormatChanged()
    MsgBox "FormatChanged"
End Sub

Private Sub RelineControl1_LostFocus()
'    Me.Text1.Text = Me.RelineControl1.FormatValues
'    MsgBox RelineControl1.RelineValues
'    MsgBox RelineControl1.CarrierValues
'    Debug.Print RelineControl1.CarrierValues
    Debug.Print RelineControl1.CarrierValues
End Sub

Private Sub TDBList1_Click()
    Me.RelineControl1.Style = Val(TDBList1.BoundText)
End Sub

