
GENERAL CODING NOTES
* call MPO.CDocument.GetVar & MPO.CDocument.SetVar when dealing with variables
* Always set Attachment argument to False when calling the .InsertFile  method
* Never use the Information method on a range object 
* When adding custom controls to forms where users can type text, the font property should be set to 'MS Sans Serif'
* When referring to Native Word styles in code, use the VB constants instead of the literal text (ie, use '.Styles(wdStyleNormal)', .Styles(wdStyleBodyText), instead of .Styles("Normal")/.Styles("Body Text")

WORD XP ISSUES
* use range object in Header/Footer
* if deleting contents of header/footer, you need to select the contents afterwards
* Don't use .information(wdinTable) 
* use if .exists as opposed to blindly cycling through collections or using 'on error resume next'
* if removing the contents of the header and footer, you need to reselect the range (i.e., the empty header or
footer) before making any further changes such as inserting something else.

GENERIC CHECKLIST
* remove all 'Stop/Resume' code