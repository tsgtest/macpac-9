Attribute VB_Name = "mdlGlobal"
Option Explicit

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

Public g_oLog As Scripting.TextStream

Sub Main()
    Load frmBackup
End Sub

Public Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise mpError.mpError_NullValueNotAllowed, , _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    Set oStr = New mpbase2.CStrings
    GetUserVarPath = oStr.xSubstitute(xPath, "<UserName>", xBuf)
        
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "mpBackup.mdlGlobal.GetUserVarPath", Err.Source, _
        Err.Description
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    Set oStr = New mpbase2.CStrings
    
    'deal with % delimited environ tokens (we are assuming there's only one token)
    xPath = oStr.xSubstituteFirst(xPath, "%", "<")
    xPath = oStr.xSubstituteFirst(xPath, "%", ">")
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    End If
    
    If xValue <> "" Then
        xPath = oStr.xSubstitute(xPath, "<" & xToken & ">", xValue)
    End If
    
    GetEnvironVarPath = xPath
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpBackup.mdlGlobal.GetEnvironVarPath"
    Exit Function
End Function
