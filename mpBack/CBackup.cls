VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBackup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'****************************************************
'   CBackup Class
'   created 4/20/00 by Daniel Fisherman
'   Contains properties and methods that
'   define the CBackup Class - those
'   procedures that backup MacPac files
'****************************************************
Option Explicit
Private lRet As Long
Private goError As New mpError.CError

Public Property Let LastBackup(dNew As Date)
    mpbase2.SetUserIni "General", "LastBackup", CDbl(dNew)
End Property

Public Property Get LastBackup() As Date
    On Error Resume Next
    LastBackup = mpbase2.GetUserIni("General", "LastBackup")
End Property

Public Function Execute() As Long
    Dim xMsg As String
    Dim bDoPost As Boolean
    Dim xNetDir As String
    Dim xHomeDir As String
    Dim xHomeVar As String
    Dim xLocalDir As String
    Dim xSourceDir As String
    Dim iUser As Integer
    Dim xDestDir As String
    Dim o As Scripting.FileSystemObject
    Dim i As Integer
    Dim xLog As String
    Dim xLogPath As String
    Dim f As File
    Dim bWarn As Boolean

    On Error GoTo ProcError

'   determine if files should be posted
    bDoPost = CBool(GetMacPacIni("Post", "Post"))
    Dim xSkipWarn As String
    xSkipWarn = GetMacPacIni("Post", "SkipPostWarning")
    bWarn = UCase(xSkipWarn) <> "TRUE"

    If bDoPost Then
'       create log file for output
        Set o = New Scripting.FileSystemObject

''       get path for log - if you want a history,
''       don't overwrite previous log
'        xLogPath = mpbase2.ApplicationDirectory & "\BackupLogs"
'        On Error Resume Next
'        MkDir xLogPath
'        On Error GoTo ProcError
'        i = 1
'        xLog = xLogPath & "\mpBackup1.log"
'        While Dir(xLog) <> ""
'            i = i + 1
'            xLog = xLogPath & "\mpBackup" & i & ".log"
'        Wend

'       create log
'        xLog = mpbase2.ApplicationDirectory & "\mpBackup.log"
'        Set g_oLog = o.CreateTextFile(xLog, True)
'        With g_oLog
'            .WriteLine String(81, "*")
'            .WriteLine "EXECUTE START: " & Format(Now, "MM/d/yy hh:mm:ss")
'            .WriteLine String(81, "*")
'        End With
'****NEW CODE****
        xLog = mpbase2.ApplicationDirectory & "\mpBackup.log"
        Set g_oLog = o.OpenTextFile(xLog, ForAppending, True)
        Set f = o.GetFile(xLog)
        If f.Size > 1000000 Then 'file is too big, start new one
            g_oLog.Close
            Set g_oLog = o.CreateTextFile(xLog, True)
        End If
        With g_oLog
            .WriteLine String(81, "*")
            .WriteLine "EXECUTE START: " & Format(Now, "MM/d/yy hh:mm:ss")
            .WriteLine String(81, "*")
        End With

'****END OF NEW*****
'       get source dir
        xSourceDir = xTrimTrailingChrs(mpbase2.UserFilesDirectory, "\")

'       get dest dir
        xNetDir = GetPostDirectory()

'        xHomeVar = GetMacPacIni("Post", "HomePathVariable")
'
'        If xHomeVar <> "" Then
'            xHomeDir = Environ(xHomeVar)
'            If Right(xNetDir, 1) <> "\" And Left(xHomeDir, 1) <> "\" Then
'                xHomeDir = "\" & xHomeDir
'            ElseIf Right(xNetDir, 1) = "\" And Left(xHomeDir, 1) = "\" Then
'                xHomeDir = Mid(xHomeDir, 2)
'            End If
'
'            xNetDir = xNetDir & xHomeDir & "\MacPac"
'        End If

        xLocalDir = xSourceDir & "_zzmpTemp"

'       check for dest dir availability
        On Error Resume Next
        If Dir(xNetDir & "\*.*") = "" Then
            If Err.Number = 68 Then 'device not available
'               prompt to copy locally
                g_oLog.WriteLine "ERROR: num = " & Err.Number & _
                    "; source = " & Err.Source & "; desc = " & Err.Description
                xMsg = "The network directory on which your " & _
                       "personal settings are stored is currently " & _
                       "unavailable.  Settings will be stored locally " & _
                       "until the network drive becomes functional."
                If bWarn Then _
                    MsgBox xMsg, vbExclamation, "MacPac 9"

'               post to local personal dir as files -
'               will err if dir exists
                xDestDir = xLocalDir
                MkDir xLocalDir
                On Error GoTo ProcError
                g_oLog.WriteLine "***POSTING LOCALLY***"
                lRet = lPostToDir(xSourceDir, xLocalDir)
            Else
                '---check if the directory is there
                xDestDir = xNetDir
                MkDir xNetDir
                'GLOG : 5033 : CEH
                If Err.Number = 75 Then
                    '---directory exists, you can post!
                    g_oLog.WriteLine "***POSTING TO " & xNetDir & "***"
                    lRet = lPostToDir(xNetDir)
                ElseIf Err.Number = 0 Then
                    '---directory newly created, you can post!
                    g_oLog.WriteLine "***POSTING TO " & xNetDir & "***"
                    lRet = lPostToDir(xNetDir, , True)
                Else
                    If Err.Number = 76 Then
                        g_oLog.WriteLine "ERROR: Could not find " & xNetDir
                    Else
                        g_oLog.WriteLine "ERROR: num = " & Err.Number & _
                            "; source = " & Err.Source & "; desc = " & Err.Description
                    End If
                    g_oLog.WriteLine "***POST WAS UNSUCCESSFUL***"
                    xMsg = "MacPac cannot backup all of your default option settings to the network" & _
                            " at this time.  You may lose any changes" & _
                            " to your normal template and default options made" & _
                            " during this Word session only." & _
                            "  Please contact your System Administrator if this is a problem."

    '               not available, warn and do...
                    If bWarn Then _
                        MsgBox xMsg, vbInformation, App.Title
                    GoTo ProcLogEnd
                End If
            End If
        Else
'           post
            xDestDir = xNetDir
            g_oLog.WriteLine "***POSTING TO " & xNetDir & "***"
            lRet = lPostToDir(xNetDir)
        End If

'       check for success
        If lRet Then
'           warn user
            g_oLog.WriteLine "***POST WAS UNSUCCESSFUL***"
            xMsg = "MacPac cannot backup all of your default option settings to " & _
            xDestDir & " at this time.  You may lose any changes to your " & _
            "normal template and default options made during this Word session only.  " & _
            "Please contact your Systems Administrator if this is a problem."
            If bWarn Then _
                MsgBox xMsg, vbInformation, App.Title
        Else
'           mark time of last backup
            g_oLog.WriteLine "***POSTED FILES SUCCESSFULLY***"
            Me.LastBackup = Now
        End If

ProcLogEnd:
'       write "END" to log
        With g_oLog
            .WriteLine String(81, "*")
            .WriteLine "EXECUTE END: " & Format(Now, "MM/d/yy hh:mm:ss")
            .WriteLine String(81, "*")
            .Close
        End With
    End If

    Execute = lRet
    Exit Function
ProcError:
    goError.Raise Err.Number, "mpBackup.CBackup.Execute"
    Exit Function
End Function

Private Function lPostToDir(ByVal xDestDirRoot As String, _
                            Optional ByVal bCreateTempPersonal As Boolean = False, _
                            Optional ByVal bForceCopy As Boolean = False) As Long
    
    Dim xFullName As String
    Dim xFile As String
    Dim docNormal As Word.Document
    Dim xMPFiles() As String
    Dim lNumItems As Long
    Dim CopyError As Long
    Dim i As Integer
    Dim x As String
    Dim xDefSource As String
    Dim bExists As Boolean
    Dim xChild As String
    Dim xDestDir As String
    Dim xFileSpec As String
    Dim oFSO As FileSystemObject
    
    Set oFSO = New FileSystemObject
    
    On Error Resume Next
    Application.StatusBar = "Saving Personal Settings. Please wait..."
    g_oLog.WriteLine "Last backup was " & Format(Me.LastBackup, "MM/d/yy hh:mm:ss")
    
    xDefSource = mpbase2.UserFilesDirectory
    
'   get items to backup from MacPac.ini
    x = mpbase2.GetIniSection("Backup Files", mpbase2.ApplicationDirectory & "\MacPac.ini")
    lNumItems = mpbase2.lCountChrs(x, Chr(0))
    
    ReDim xMPFiles(lNumItems - 1)
    For i = 1 To lNumItems
        xMPFiles(i - 1) = GetMacPacIni("Backup Files", "File" & i)
    Next i
    
    DoEvents
'   enumerate files
    For i = 0 To lNumItems - 1
        If Not (xMPFiles(i) = Empty) Then
'           if item has "\" in name, get source dir
            If InStr(xMPFiles(i), "\") Then
                If Left(xMPFiles(i), 1) = "<" Then
'                   variable supplied for path - get path
                    xFileSpec = xGetFileSpec(xMPFiles(i))
                Else
'                   entire hard coded path is supplied
                    xFileSpec = xMPFiles(i)
                End If
            Else
'               no path supplied, use default source
                xFileSpec = Dir(xDefSource & "\" & xMPFiles(i))
            End If
'           get subdirectory of destination dir
            xChild = xGetDestChildDir(xMPFiles(i))
            
'           check if file exists
            On Error GoTo 0
            xFile = Empty
            xFile = Dir(xFileSpec)
            bExists = (xFile <> Empty)
            
            While bExists
                'get full name
                xFullName = oFSO.GetParentFolderName(xFileSpec) & "\" & xFile
                'write to log
                g_oLog.WriteLine xFullName & " was last modified " & _
                        Format(oFSO.GetFile(xFullName) _
                        .DateLastModified, "MM/d/yy hh:mm:ss")
                'get destination dir
                xDestDir = xDestDirRoot & "\" & xChild
                On Error Resume Next
                MkDir xDestDir
                On Error GoTo 0
                'copy file if any of these is true:
                '- last modified date is newer than last backup
                '- file is USER.INI
                '- Force Copy = True (POST folder didn't exist)
                '- file doesn't exist in POST location
                If (oFSO.GetFile(xFullName).DateLastModified > Me.LastBackup) Or _
                (UCase(xFile) = "USER.INI") Or _
                bForceCopy Or _
                (Not oFSO.FileExists(xDestDir & "\" & xFile)) Then
'                   file has been modified since last backup - backup file
                    On Error Resume Next
'                   copy the file from source to dest
                    FileCopy xFullName, xDestDir & "\" & xFile
                    If Err.Number <> 0 Then
                        CopyError = Err.Number
                        g_oLog.WriteLine "Error while copying: " & xFullName & _
                            "; num = " & CopyError & "; desc = " & Err.Description
                    Else
                        g_oLog.WriteLine "Copied file: " & xFullName & _
                            " to " & vbCrLf & "             " & xDestDir & "\" & xFile
                    End If
                End If
                xFile = Empty
                xFile = Dir()
                bExists = (xFile <> Empty)
            Wend
        End If
    Next i
    lPostToDir = CopyError
    Application.StatusBar = ""
    Exit Function
ProcError:
    goError.Raise Err.Number, "mpBackup.CBackup.lPostToDir"
    Exit Function
End Function

Private Function xGetDestChildDir(ByVal xFileSpec As String) As String
'returns the name of the destination subdirectory
'to which the file should be copied
    Dim lPos As Long
    Dim xTemp As String
    
    If InStr(xFileSpec, "<") Then
        lPos = InStr(xFileSpec, ">")
        xTemp = Mid(xFileSpec, 2, lPos - 2)
        xGetDestChildDir = xTemp
    ElseIf InStr(xFileSpec, "\") Then
        xGetDestChildDir = "Docs"
    Else
        xGetDestChildDir = "<MacPacUser>"
    End If
End Function

Private Function xGetFileSpec(ByVal xSoftFileSpec As String) As String
'returns the full name of the file specified by xFileSpec
    Dim x As String
    Dim xStartup As String
    Dim xWkgroup As String
    Dim xUser As String
    Dim xCurrentWordVersion As String
    Dim xPolicies_User As String
    Dim xPolicies_Startup As String
    Dim xPolicies_Wkgroup As String
    Dim xPath As String
    Dim xTemp As String
    
    Dim oReg As mpbase2.CRegistry
    Set oReg = New CRegistry
    
    oReg.GetValue HKEY_LOCAL_MACHINE, "Software\Microsoft\Windows\CurrentVersion\App Paths\Winword.exe", "Path", xCurrentWordVersion
    
    If InStr(UCase(xCurrentWordVersion), "OFFICE14") Then  'Office 2010
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\14.0\Word\Options", "STARTUP-PATH", xStartup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\14.0\Common\General", "SharedTemplates", xWkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\14.0\Common\General", "UserTemplates", xUser
        'policies location
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\14.0\Word\Options", "STARTUP-PATH", xPolicies_Startup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\14.0\Common\General", "SharedTemplates", xPolicies_Wkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\14.0\Common\General", "UserTemplates", xPolicies_User
    ElseIf InStr(UCase(xCurrentWordVersion), "OFFICE12") Then  'Office 2007
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\12.0\Word\Options", "STARTUP-PATH", xStartup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\12.0\Common\General", "SharedTemplates", xWkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\12.0\Common\General", "UserTemplates", xUser
        'policies location
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\12.0\Word\Options", "STARTUP-PATH", xPolicies_Startup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\12.0\Common\General", "SharedTemplates", xPolicies_Wkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\12.0\Common\General", "UserTemplates", xPolicies_User
    ElseIf InStr(UCase(xCurrentWordVersion), "OFFICE11") Then  'Office 2003
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\11.0\Word\Options", "STARTUP-PATH", xStartup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\11.0\Common\General", "SharedTemplates", xWkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\11.0\Common\General", "UserTemplates", xUser
        'policies location
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\11.0\Word\Options", "STARTUP-PATH", xPolicies_Startup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\11.0\Common\General", "SharedTemplates", xPolicies_Wkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\11.0\Common\General", "UserTemplates", xPolicies_User
    ElseIf InStr(UCase(xCurrentWordVersion), "OFFICE10") Then  'Office XP
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\10.0\Word\Options", "STARTUP-PATH", xStartup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\10.0\Common\General", "SharedTemplates", xWkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\10.0\Common\General", "UserTemplates", xUser
        'policies location
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\10.0\Word\Options", "STARTUP-PATH", xPolicies_Startup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\10.0\Common\General", "SharedTemplates", xPolicies_Wkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\10.0\Common\General", "UserTemplates", xPolicies_User
    ElseIf InStr(UCase(xCurrentWordVersion), "\OFFICE\") Then  'Office 2000
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\9.0\Word\Options", "STARTUP-PATH", xStartup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\9.0\Common\General", "SharedTemplates", xWkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Microsoft\Office\9.0\Common\General", "UserTemplates", xUser
        'policies location
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\9.0\Word\Options", "STARTUP-PATH", xPolicies_Startup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\9.0\Common\General", "SharedTemplates", xPolicies_Wkgroup
        oReg.GetValue HKEY_CURRENT_USER, "Software\Policies\Microsoft\Office\9.0\Common\General", "UserTemplates", xPolicies_User
    Else
    End If
    
    'Startup path
    If xStartup <> "" Then
        xPath = xStartup
    ElseIf xPolicies_Startup <> "" Then
        xPath = xPolicies_Startup
    Else
    End If
    x = xSubstitute(xSoftFileSpec, "<WordStartup>", xPath)
    
    xPath = ""
    
    'Workgroup path
    If xWkgroup <> "" Then
        xPath = xWkgroup
    ElseIf xPolicies_Wkgroup <> "" Then
        xPath = xPolicies_Wkgroup
    Else
    End If
    x = xSubstitute(x, "<WordWorkgroupTemplates>", xPath)
    
    xPath = ""
    
    'User templates path
    If xUser <> "" Then
        xPath = xUser
    ElseIf xPolicies_User <> "" Then
        xPath = xPolicies_User
    Else
        'if UserTemplates path is set to default path, ie: c:\documents and settings\%username%\application data\microsoft\templates
        'then UserTemplates registry key will be empty
        xPath = GetEnvironVarPath("<AppData>\Microsoft\Templates")
    End If
    x = xSubstitute(x, "<WordUserTemplates>", xPath)
    
    
    x = xSubstitute(x, "<MacPacTemplates>", mpbase2.TemplatesDirectory)
    x = xSubstitute(x, "<MacPacBoilerplate>", mpbase2.BoilerplateDirectory)
    x = xSubstitute(x, "<MacPacUser>", mpbase2.UserFilesDirectory)
    x = xSubstitute(x, "<MacPacUserBoilerplate>", mpbase2.UserBoilerplateDirectory)
    x = GetEnvironVarPath(x)
    'deal with network location
    If Left(x, 2) = "\\" Then
        xTemp = Mid(x, 3, Len(x) - 2)
        xTemp = xSubstitute(xTemp, "\\", "\")
        xTemp = "\\" & xTemp
    Else
        xTemp = xSubstitute(x, "\\", "\")
    End If
    xGetFileSpec = xTemp
End Function

Public Function PrivateDBIsAccessible() As Boolean
    On Error Resume Next
    
    FileCopy mpbase2.UserFilesDirectory & "\mpPrivate.mdb", _
        mpbase2.UserFilesDirectory & "\mpPrivatex.mdb"
    If Err.Number = 70 Then
        Err.Clear
        Exit Function
    End If
    
    Kill mpbase2.UserFilesDirectory & "\mpPrivatex.mdb"
    PrivateDBIsAccessible = True
End Function
Private Function GetPostDirectory() As String
    Dim xTemp As String
    Dim oIni As CIni
    Dim iCounter As Integer
    Dim iTokenPos As Integer
    Static xDir As String
    
    If xDir <> Empty Then
'       value has been retrieved already - exit
        GetPostDirectory = xDir
        Exit Function
    End If
    
    Set oIni = New CIni
    
    On Error Resume Next
'   get path from ini
    xTemp = mpbase2.GetMacPacIni("Post", "PostDir")
    On Error GoTo ProcError

    Do While InStr(xTemp, "<")
        iCounter = InStr(xTemp, "<")
'           substitute real path for user var if it exists in string
        If (InStr(UCase(xTemp), "<USERNAME>") > 0) Then
'               use API to get Windows user name
            xTemp = GetUserVarPath(xTemp)
        End If
'           check for environmental variables
        xTemp = GetEnvironVarPath(xTemp)
        If InStr(xTemp, "<") = iCounter Then    'environment variable was not replaced
            Exit Do
        End If
    Loop

'   raise error if path is invalid
'    If Not DirExists(xTemp) Then
'        Err.Raise mpError_InvalidMacPacUserFilesPath
'    End If
    
'   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetPostDirectory = xTemp
    
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpBackup.cBackup.GetPostDirectory", _
              goError.Desc(Err.Number)
    Exit Function
End Function
