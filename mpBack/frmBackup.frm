VERSION 5.00
Begin VB.Form frmBackup 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Backing Up MacPac User Files..."
   ClientHeight    =   45
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2925
   ControlBox      =   0   'False
   Icon            =   "frmBackup.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   45
   ScaleWidth      =   2925
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer timBackup 
      Interval        =   500
      Left            =   45
      Top             =   30
   End
End
Attribute VB_Name = "frmBackup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim m_iTries As Integer
Dim m_vNow

Private Sub timBackup_Timer()
    Dim oBack As CBackup
    Dim oWord As Word.Application
    Dim bDoPost As Boolean
    Dim xLog As String
    Dim xLogPath As String
    Dim f As File
    
    Static iSecs As Integer
 
'   do only if word has shut down
    On Error Resume Next
    Set oWord = Nothing
    Set oWord = Word.Application
    
    On Error GoTo 0
    bDoPost = CBool(GetMacPacIni("Post", "Post"))
    If oWord Is Nothing And bDoPost Then
        Set oBack = New CBackup
        If oBack.PrivateDBIsAccessible Then
            Me.timBackup.Enabled = False
            oBack.Execute
            Unload Me
        Else
            If m_vNow = Empty Then
                m_vNow = Now
                
'               get timeout value - default to 5 secs
'               if no timeout has been specified
                On Error Resume Next
                iSecs = GetMacPacIni("Post", "PostTimeout")
                On Error GoTo 0
                If iSecs = 0 Then
                    iSecs = 5
                End If
            ElseIf Now > m_vNow + iSecs / 86400 Then
'               stop timer from firing and alert if specified
                Dim bWarn As Boolean
                Dim xWarn As String
                
                Me.timBackup.Enabled = False
                
'               check if alert should be displayed
                xWarn = GetMacPacIni("Post", "SkipPostWarning")
                bWarn = UCase(xWarn) <> "TRUE"
                
                Dim o As Scripting.FileSystemObject
                Set o = New Scripting.FileSystemObject
                
                xLog = mpbase2.ApplicationDirectory & "\mpBackup.log"
                Set g_oLog = o.OpenTextFile(xLog, ForAppending, True)
                Set f = o.GetFile(xLog)
                If f.Size > 1000000 Then 'file is too big, start new one
                    g_oLog.Close
                    Set g_oLog = o.CreateTextFile(xLog, True)
                End If
                With g_oLog
                    .WriteLine String(81, "*")
                    .WriteLine "EXECUTE FAILED : " & Format(Now, "MM/d/yy hh:mm:ss")
                    .WriteLine "PRIVATE DB NOT ACCESSIBLE"
                    .WriteLine String(81, "*")
                End With
                
                If bWarn Then
                    MsgBox "MacPac cannot backup mpPrivate.mdb " & _
                        "at this time.  You may lose any changes to your " & _
                        "personal settings made during this Word session only.  " & _
                        "Please contact your Systems Administrator if this is a problem.", _
                        vbInformation, "MacPac 9"
                End If
                Unload Me
            End If
        End If
    Else
        Unload Me
    End If
End Sub
