Attribute VB_Name = "mdlFunctions"
Option Explicit

'API declarations for LockWindowUpdate() and FindWindow()
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Private Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Sub EchoOff()
    Call LockWindowUpdate(FindWindow("OpusApp", 0&))
End Sub

Sub EchoOn()
    Call LockWindowUpdate(0&)
End Sub

Function bFileExists(xPath As String) As Boolean
    On Error GoTo EndIt
    bFileExists = False
    GetAttr xPath
    bFileExists = True
EndIt:
End Function
Function sGetPathFromClassID(sClassName As String, Optional bEXE As Boolean = False)
    Dim vPath As Variant
    Dim sClass As String
    
    sClass = GetKeyValue(HKEY_CLASSES_ROOT, sClassName & "\Clsid", "")
    If sClass <> "" Then
        If Not bEXE Then
            vPath = GetKeyValue(HKEY_CLASSES_ROOT, "CLSID\" & sClass & "\InProcServer32", "")
        Else
            vPath = GetKeyValue(HKEY_CLASSES_ROOT, "CLSID\" & sClass & "\LocalServer32", "")
        End If
        sGetPathFromClassID = GetEnvironVarPath(CStr(vPath))
    Else
        sGetPathFromClassID = ""
    End If

End Function
Function sGetCLSIDFromProgID(sClassName As String) As String
    On Error Resume Next
    sGetCLSIDFromProgID = GetKeyValue(HKEY_CLASSES_ROOT, sClassName & "\Clsid", "")
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim xDelimiter As String
    
    On Error GoTo ProcError
    xDelimiter = "%"
    iPosStart = InStr(xPath, xDelimiter)
    iPosEnd = InStr(Mid(xPath, iPosStart + 1, Len(xPath)), xDelimiter)
    If iPosEnd Then
        iPosEnd = iPosEnd + iPosStart
    End If
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    End If
    
    If xValue <> "" Then
        GetEnvironVarPath = xSubstitute(xPath, xDelimiter & xToken & xDelimiter, xValue)
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Exit Function
End Function

