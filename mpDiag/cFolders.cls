VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFolders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'*********************************************************
'Class Name:    cFolders
'Author:        Charlie Homo
'Date:          10/6/1999
'Description:   Maintains the collection of cFolder objects
'*********************************************************

Private m_colFolders As Collection

'************
'CLASS EVENTS
'************

Private Sub Class_Initialize()
'***create the collection object
    Set m_colFolders = New Collection
End Sub

Private Sub Class_Terminate()
'***clear the reference
    Set m_colFolders = Nothing
End Sub

'*****************
'PUBLIC PROPERTIES
'*****************

Public Property Get Count() As Long
    Count = m_colFolders.Count
End Property

'**************
'PUBLIC METHODS
'**************

Public Function Add(xName As String, _
                    xPath As String) As cFolder
    
    Dim newFolder As cFolder
    
    Set newFolder = New cFolder
    
    With newFolder
        .Name = xName
        .Path = xPath
'***    add member to collection
        m_colFolders.Add newFolder, .Name
    End With
    
'***return the added file obj
    Set Add = newFolder
    
End Function

Public Function Item(vKey As Variant) As cFolder
Attribute Item.VB_UserMemId = 0
    Set Item = m_colFolders.Item(vKey)
End Function

Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_colFolders.[_NewEnum]
End Function
Public Function Fill(xPath As String, Optional xParent As String = "")
    Dim xFolder As String
    Dim xFolderPath As String
    Dim newFolder As cFolder
    Dim xStartupPath As String
    Dim bStartupFound As Boolean
    
    On Error GoTo ProcError
    
    If InStr(UCase(Application.Options.DefaultFilePath(wdStartupPath)), _
        UCase(xPath)) = 1 Then _
        bStartupFound = True
    If xParent <> "" And Right(xParent, 1) <> "\" Then _
        xParent = xParent & "\"
'***cycle through each folder in root Path
    xFolder = Dir(xPath, vbDirectory)
    Do While xFolder <> ""
        If xFolder <> "." And xFolder <> ".." Then
            If (GetAttr(xPath & xFolder) And vbDirectory) = vbDirectory Then
                'v9.8.1010 - #4320
                If UCase(xFolder) = "BOILERPLATE" And g_bNoBP Then
                    GoTo NextFolder
                End If
                
                xFolderPath = xPath & xFolder
        
                Set newFolder = New cFolder
                
                With newFolder
                    .Path = xFolderPath
                    .Name = xParent & xFolder
                    .IsMacPac = bIsMacPac(xFolder)
                
                    m_colFolders.Add newFolder, .Name
                
                End With
            End If  ' it represents a directory.
NextFolder:
        End If
'***    get next entry
        xFolder = Dir()
    Loop
    ' Add Startup folder if it's not included in MacPac
    If xParent = "" And Not bStartupFound Then
        xStartupPath = Word.Application.Options.DefaultFilePath(wdStartupPath)
        If (GetAttr(xStartupPath) And vbDirectory) = vbDirectory Then
            Set newFolder = New cFolder
            
            With newFolder
                .Path = xStartupPath
                .Name = "Word Startup"
                .IsMacPac = bIsMacPac(xFolder)
            
                On Error Resume Next
                m_colFolders.Add newFolder, "Word Startup"
                On Error GoTo ProcError
            End With
        End If  ' it represents a directory.
    End If
Exit Function
ProcError:
Stop
Resume
    If Err.Number Then
        MsgBox "An error occurred filling the list of folders" & _
        vbCr & "Err : " & Err.Number & " (" & Err.Description, vbCritical
    End If
    Exit Function
End Function

Private Function bIsMacPac(xName As String) As Boolean

    Select Case UCase(xName)
        Case "TEMPLATES", "BOILERPLATE", "PERSONAL", "STARTUP", "APP", "CI"
            bIsMacPac = True
        Case Else
            bIsMacPac = False
    End Select
    
End Function
