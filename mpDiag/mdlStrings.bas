Attribute VB_Name = "mdlStrings"
Option Explicit

Public Function xInitialCap(xString As String) As String
'***Takes any string and returns it Initial Capped

    Dim xTemp As String
    Dim xFinal As String
    Dim iCount As Integer
    Dim I As Integer
    Dim bSpace As Boolean
    
    xTemp = LCase(xString)
    iCount = Len(xTemp)
    
    xFinal = UCase(Left(xTemp, 1))
    
    For I = 1 To iCount - 1
    
        xTemp = Right(xTemp, iCount - I)
        If Left(xTemp, 1) = Chr(32) Then bSpace = True
        
        If Left(xTemp, 1) <> Chr(32) And bSpace Then
            xFinal = xFinal + UCase(Left(xTemp, 1))
            bSpace = False
        Else
            xFinal = xFinal + Left(xTemp, 1)
        End If
        
    Next I
    
    xInitialCap = xFinal
    
End Function

Public Function xGetNumeric(xString As String) As String

    'Trims all non numeric chars from start of string
    'made for Attorney Bar ID number field that contains
    'abbreviated state info

    Dim iLength As Long
    Dim I As Long
    Dim iPos As Long
    Dim xChr As String
    
    iLength = Len(xString)
    iPos = 0
    
    For I = 1 To iLength
        xChr = Mid(xString, I, 1)
        If IsNumeric(xChr) Then
            iPos = I
            Exit For
        End If
    Next I
    
    If iPos <> 0 Then
        xGetNumeric = Right(xString, iLength - (iPos - 1))
    Else
        xGetNumeric = ""
    End If
End Function


Function xSplit(xText As String, _
                xSplitAfter As String, _
                Optional bIncludeComma As Boolean = True) As String
'inserts new line chr in xText after xSplitAfter-
'if bIncludeComma then leave trailing char at end
'of first line

    Dim iSplitPos As Integer
    Dim iSplitLength As Integer
    Dim xFirstLine As String
    Dim xSecondLine As String
    
    iSplitPos = InStr(xText, xSplitAfter)
    
    If iSplitPos Then
        iSplitLength = Len(xSplitAfter)
        xFirstLine = Trim(Left(xText, iSplitPos - 1))
        If Not bIncludeComma Then
            If Right(xFirstLine, 1) = "," Then
                xFirstLine = Left(xFirstLine, Len(xFirstLine) - 1)
            End If
        End If
        xSecondLine = Trim(Mid(xText, iSplitPos + iSplitLength))
        xSplit = xFirstLine & Chr(11) & xSecondLine
    Else
        xSplit = xText
    End If
End Function


Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function
Function xNulltoString(vValue As Variant) As String
    If Not IsNull(vValue) Then _
        xNulltoString = CStr(vValue)
End Function
Function xStringToArray(ByVal xString As String, _
                        xArray() As String, _
                        Optional iNumCols As Integer = 1, _
                        Optional xSep As String = ",") As String

    Dim iNumSeps As Integer
    Dim iNumEntries As Integer
    Dim iSepPos As Integer
    Dim xEntry As String
    Dim I As Integer
    Dim j As Integer
    
'   get ubound of xArray - count delimiter
'   then divide by iNumCols
    iNumSeps = lCountChrs(xString, xSep)
    iNumEntries = (iNumSeps + 1) / iNumCols
    
    ReDim xArray(iNumCols - 1, iNumEntries - 1)
    
    For I = 0 To iNumEntries - 1
        For j = 0 To iNumCols - 1
'           get next entry & store
            iSepPos = InStr(xString, xSep)
            If iSepPos Then
                xEntry = Left(xString, iSepPos - 1)
            Else
                xEntry = xString
            End If
            xArray(j, I) = xEntry
            
'           remove entry from xstring
            If iSepPos Then _
                xString = Mid(xString, iSepPos + Len(xSep))
        Next j
    Next I
End Function
Function xTrimTrailingChrs(ByVal xText As String, _
                           Optional ByVal xChr As String = "", _
                           Optional bTrimStart As Boolean = False, _
                           Optional bTrimEnd As Boolean = True) As String
                           
'   removes trailing xChr from xText -
'   if xchr = "" then trim last char

    If xChr <> "" Then
        If bTrimStart Then
            While Left(xText, Len(xChr)) = xChr
                xText = Mid(xText, Len(xChr) + 1)
            Wend
        End If
        If bTrimEnd Then
            While Right(xText, Len(xChr)) = xChr
                xText = Left(xText, Len(xText) - Len(xChr))
            Wend
        End If
    Else
        xText = Left(xText, Len(xText) - 1)
    End If
        
    xTrimTrailingChrs = xText
End Function

Function bParseAddress(xEntry As String, _
                       xName As String, _
                       xAddress As String) As Boolean
                         
'fills xName and xAddress
'after parsing xEntry

    Dim iParaPos As String
    
    On Error Resume Next
    
    iParaPos = InStr(xEntry, vbCr)
    xName = Left(xEntry, iParaPos - 1)
    xAddress = Right(xEntry, Len(xEntry) - iParaPos)
    
    While Right(xAddress, 1) = vbCr
        xAddress = Left(xAddress, Len(xAddress) - 1)
    Wend
    
    bParseAddress = True
End Function



Function xSubstitute(ByVal xString As String, _
                     xSearch As String, _
                     xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Integer
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While iSeachPos
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
        iSeachPos = InStr(UCase(xString), _
                          UCase(xSearch))
    
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function
Function xGetMonth(iMonth As Integer, xFormat As String) As String
                   
    Dim I As Integer
    Dim MonthArray(11) As String
    
    Select Case xFormat
        Case "mmmm"
            MonthArray(0) = "January"
            MonthArray(1) = "February"
            MonthArray(2) = "March"
            MonthArray(3) = "April"
            MonthArray(4) = "May"
            MonthArray(5) = "June"
            MonthArray(6) = "July"
            MonthArray(7) = "August"
            MonthArray(8) = "September"
            MonthArray(9) = "October"
            MonthArray(10) = "November"
            MonthArray(11) = "December"
        Case "mmm"
            MonthArray(0) = "Jan"
            MonthArray(1) = "Feb"
            MonthArray(2) = "Mar"
            MonthArray(3) = "Apr"
            MonthArray(4) = "May"
            MonthArray(5) = "Jun"
            MonthArray(6) = "Jul"
            MonthArray(7) = "Aug"
            MonthArray(8) = "Sept"
            MonthArray(9) = "Oct"
            MonthArray(10) = "Nov"
            MonthArray(11) = "Dec"
        Case Else
            For I = 0 To 11
                MonthArray(I) = LTrim(Str(I + 1))
            Next I
    End Select
        xGetMonth = MonthArray(iMonth - 1)
End Function

Function xTrimSpaces(ByVal xString As String) As String

    xTrimSpaces = xSubstitute(xString, _
                              Chr$(32), _
                              "")
End Function
Function UCaseName(xName As String) As String
    Dim I As Integer
    Dim xInputString As String
    xInputString = UCase(xName)
    I = InStr(xInputString, " MC")
    If I > 0 Then
        xInputString = Mid(xInputString, 1, I + 1) & _
            "c" & Mid(xInputString, I + 3)
    End If
    UCaseName = xInputString
End Function


Function xArrayToString(xArray() As String, Optional xSep As String = "||") As String
    Dim I As Long
    Dim j As Long
    Dim xString As String
    Dim lNumEntries As Long
    
'   test for dimensioned array
    On Error Resume Next
    lNumEntries = UBound(xArray, 2) + 1
    On Error GoTo 0
    
'   do if array has been dimensioned
    If lNumEntries Then
        For I = LBound(xArray, 2) To UBound(xArray, 2)
            For j = LBound(xArray, 1) To UBound(xArray, 1)
                xString = xString & xSep & xArray(j, I)
            Next j
        Next I
        
        xArrayToString = Mid(xString, Len(xSep) + 1)
    End If
End Function



