Attribute VB_Name = "mdlTypeLib"
Option Explicit
Private oTLIApp As Object
Private xDetails As String
Function bCheckClassInfo(xBasePath As String) As Boolean
    
    Dim sPath As String
    Dim rc As Long
    ' TypeLibInfo objects are created using late binding
    ' to avoid compatibility issues with different versions of tlbinf32.dll
        
    
    On Error Resume Next
    
    Set oTLIApp = CreateObject("TLI.TLIApplication")
    
    If oTLIApp Is Nothing Then
        ' If tlbinf32.dll is not registered, look for a copy in the
        ' same folder as mpDiagnose.exe and register if found
        If Dir(App.Path & "\tlbinf32.dll") <> "" Then
            sPath = Space$(256)
            rc = GetSystemDirectory(sPath, Len(sPath))
            sPath = LCase$(Mid$(sPath, 1, InStr(sPath, Chr(0)) - 1))
            Shell sPath & "\regsvr32.exe " & Chr(34) & App.Path & "\tlbinf32.dll" & Chr(34) & " /s"
            DoEvents
        End If
        Set oTLIApp = CreateObject("TLI.TLIApplication")
    End If
        
    'Get information from type library
    If Not oTLIApp Is Nothing Then
        
        If Word.Documents.Count = 0 Then
            Set m_oSource = Word.Documents.Add()
        Else
            With m_oSource.ActiveWindow.Selection
                .EndKey wdStory
                .InsertBreak wdSectionBreakNextPage
                .EndKey wdStory
            End With
        End If
    
        xDetails = "Library/Classes" & vbTab & "GUID from File on Disk" & vbCr
        
        bReadAXFiles xBasePath & "MacPac90\App\"
        bReadAXFiles xBasePath & "Numbering\App\"
        bReadAXFiles xBasePath & "CI\"
        bReadAXFiles xBasePath & "CI\App"
        Set oTLIApp = Nothing
        bCreateTypeLibInfoTableReport xDetails
    End If
End Function

Function bCreateTypeLibInfoTableReport(xString As String) As Boolean
    Dim rngStart As Word.Range
    Dim rngTitle As Word.Range
    Dim cCol As Word.Column
    Dim oTbl As Word.Table
    Dim rngFooter As Word.Range
    

    With m_oSource.ActiveWindow.Selection
        .EndOf
        .Bookmarks.Add "GUIDList"
        .InsertAfter "GUIDs of Installed MacPac Files and Classes"
        .ParagraphFormat.Alignment = wdAlignParagraphCenter
        .Font.SmallCaps = True
        .ParagraphFormat.SpaceAfter = 0
        .Font.Size = 14
        .InsertParagraphAfter
        .EndOf
        .InsertAfter "(as of " & Format(Now(), "MMMM d, yyyy") & ")"
        .Font.Size = 9
        .Font.SmallCaps = False
        .ParagraphFormat.SpaceAfter = 12
        .EndOf
        .InsertParagraphAfter
        .EndOf
        .ParagraphFormat.Alignment = wdAlignParagraphLeft
        .ParagraphFormat.SpaceAfter = 0
        .Font.Size = 10
        If xString <> "" Then
            .InsertAfter xString
            Set oTbl = .Range.ConvertToTable(Separator:=wdSeparateByTabs, _
                              NumColumns:=2, _
                              AutoFit:=True)
        Else
            .InsertAfter "TLIBINF32.DLL IS NOT INSTALLED." & vbCr & _
                "TYPE LIBRARY INFORMATION NOT AVAILABLE."
            Exit Function
        End If
    End With
    
'***format table
    With oTbl
        .Rows(1).HeadingFormat = True
        With .Rows(1).Range
            .Font.Size = 12
            .Bold = True
            .Italic = True
            .ParagraphFormat.SpaceBefore = 3
            .ParagraphFormat.SpaceAfter = 3
        End With
        .Columns.AutoFit
        .Rows.Alignment = wdAlignRowCenter
        With Word.Selection
            .SetRange oTbl.Range.Start, oTbl.Range.Start
            ' Mark entries with unmatched GUIDs
            With .Find
                .Text = "Registry Value:"
                .Forward = True
                .Wrap = wdFindStop
                .Execute
                While .Found
                    Selection.Cells(1).Range.Font.ColorIndex = wdRed
                    .Execute
                Wend
            End With
        End With
    End With
    
End Function
Function bReadAXFiles(xPath As String) As Boolean
    Dim oTLI As Object ' TLI.TypeLibInfo object
    Dim oCoClasses As Object
    Dim oFSO As Object
    Dim oFolder As Object
    Dim oFile As Object
    Dim xTestGUID As String
    Dim i As Integer
    
    On Error Resume Next
    Set oFSO = CreateObject("Scripting.FileSystemObject")
    
    Set oFolder = oFSO.GetFolder(xPath)
    
    If Not (oFolder Is Nothing) Then
        For Each oFile In oFolder.Files
            With oFile
                If (Right(UCase(.Name), 4) = ".OCX" Or _
                    Right(UCase(.Name), 4) = ".DLL" Or _
                    Right(UCase(.Name), 4) = ".EXE") Then
                    Set oTLI = Nothing
                    ' Get TypeLibInfo Object
                    Set oTLI = oTLIApp.TypeLibInfoFromFile(oFile.Path)
                    If Not oTLI Is Nothing Then
                        xDetails = xDetails & UCase(oFile.Name) & " (" & oFile.DateLastModified & ")" & vbTab & oTLI.Guid & vbCr
                        Set oCoClasses = oTLI.CoClasses
                        ' Iterate through classes within ActiveX file
                        For i = 1 To oCoClasses.Count
                            xDetails = xDetails & "     " & oCoClasses.Item(i).Parent & "." & oCoClasses.Item(i).Name & vbTab & oCoClasses.Item(i).Guid & vbCr
                            ' Check if CLSID in registry matches file on disk
                            xTestGUID = sGetCLSIDFromProgID(oCoClasses.Item(i).Parent & "." & oCoClasses.Item(i).Name)
                            If xTestGUID = "" Then _
                                xTestGUID = "NOT FOUND"
                            If UCase(xTestGUID) <> UCase(oCoClasses.Item(i).Guid) Then
                                xDetails = xDetails & vbTab & "Registry Value: " & xTestGUID & vbCr
                            End If
                        Next i
                    End If
                End If
            End With
        Next oFile
    End If
    Set oCoClasses = Nothing
    Set oTLI = Nothing
    Set oFSO = Nothing
End Function
