VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cSysFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarStrucVer As String 'local copy
Private mvarFileVer As String 'local copy
Private mvarProdVer As String 'local copy
Private mvarFileFlags As String 'local copy
Private mvarFileOS As String 'local copy
Private mvarFileType As String 'local copy
Private mvarFileSubType As String 'local copy
'local variable(s) to hold property value(s)
Private mvarFileName As String 'local copy
Private mvarFilePath As String 'local copy
'local variable(s) to hold property value(s)
Private mvarFileSize As Long 'local copy
Private mvarFileDate As String 'local copy
Public Property Let FileDate(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileDate = 5
    mvarFileDate = vData
End Property


Public Property Get FileDate() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileDate
    FileDate = mvarFileDate
End Property



Public Property Let FileSize(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileSize = 5
    mvarFileSize = vData
End Property


Public Property Get FileSize() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileSize
    FileSize = mvarFileSize
End Property



Public Function FullName() As String
    FullName = Me.FilePath & Me.FileName
End Function

Public Property Let FilePath(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FilePath = 5
    If Right(vData, 1) <> "\" Then _
        vData = vData & "\"
    mvarFilePath = vData
End Property


Public Property Get FilePath() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FilePath
    FilePath = mvarFilePath
End Property



Friend Property Let FileName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileName = 5
    mvarFileName = vData
End Property


Friend Property Get FileName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileName
    FileName = mvarFileName
End Property



Public Property Let FileSubType(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileSubType = 5
    mvarFileSubType = vData
End Property


Public Property Get FileSubType() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileSubType
    FileSubType = mvarFileSubType
End Property



Public Property Let FileType(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileType = 5
    mvarFileType = vData
End Property


Public Property Get FileType() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileType
    FileType = mvarFileType
End Property



Public Property Let FileOS(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileOS = 5
    mvarFileOS = vData
End Property


Public Property Get FileOS() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileOS
    FileOS = mvarFileOS
End Property



Public Property Let FileFlags(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileFlags = 5
    mvarFileFlags = vData
End Property


Public Property Get FileFlags() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileFlags
    FileFlags = mvarFileFlags
End Property



Public Property Let ProdVer(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ProdVer = 5
    mvarProdVer = vData
End Property


Public Property Get ProdVer() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ProdVer
    ProdVer = mvarProdVer
End Property



Public Property Let FileVer(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FileVer = 5
    mvarFileVer = vData
End Property


Public Property Get FileVer() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FileVer
    FileVer = mvarFileVer
End Property



Public Property Let StrucVer(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.StrucVer = 5
    mvarStrucVer = vData
End Property


Public Property Get StrucVer() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.StrucVer
    StrucVer = mvarStrucVer
End Property

