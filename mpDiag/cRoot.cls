VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cRoot"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_xPath As String

'******************************
'*****PUBLIC PROPERTIES********
'******************************
Public Property Get Path() As String
    Path = m_xPath
End Property

Public Property Let Path(xNew As String)
    m_xPath = xNew
End Property

