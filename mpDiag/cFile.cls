VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cFile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_xPath As String
Private m_xName As String
Private m_dDate As Date
Private m_lSize As Long
Private m_xType As String

'******************************
'*****PUBLIC PROPERTIES********
'******************************
Public Property Get Path() As String
    Path = m_xPath
End Property

Public Property Let Path(xNew As String)
    m_xPath = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get FileType() As String
    FileType = m_xType
End Property

Public Property Let FileType(xNew As String)
    m_xType = xNew
End Property

Public Property Get Size() As Long
    Size = m_lSize
End Property

Public Property Let Size(lNew As Long)
    Dim ltemp As Long
    ltemp = mpMax(lNew / 1024, _
             (lNew / 1024) + 1)
    m_lSize = ltemp
End Property

Public Property Get ModifiedDate() As Date
    ModifiedDate = m_dDate
End Property

Public Property Let ModifiedDate(dNew As Date)
    m_dDate = dNew
End Property

