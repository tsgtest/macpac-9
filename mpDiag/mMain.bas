Attribute VB_Name = "mdlMain"
Option Explicit

Public g_xPath As String
Public g_xFirmName As String
Public g_bAllRootFolders As Boolean

Public Const mpdAppName = "MacPac Diagnostic Tool"

'Messages
Public Const mpdMsgFooter = "* Personal folder file dates may vary because users can modify these files" & vbCr & _
                            "** file date may vary due to frequent updates of the People table" & vbCr & _
                            "*** file dates may vary as MacPac typically resets the file paths"
Public Const mpdMsgRunning = "Running MacPac diagnostic. Please wait..."

Dim wdApp As Word.Application
Sub Main()
    Dim xPath As String
    Dim xArgument As String
    Dim xStartupPath As String
    Dim i As Integer
    
    On Error GoTo Main_Error
    
'***check command line arguments
    xArgument = xSubstitute(Command(), Chr(34), "")
    g_bAllRootFolders = True
    If xArgument <> "" Then
        If Not bFileExists(xArgument) Then
            MsgBox """" & xArgument & """" & " is not a valid path.", , mpdAppName
            Exit Sub
        End If
    End If
        
'***get Word's startup path (still will also fire up Word if necessary)
    xStartupPath = Word.Application.Options. _
            DefaultFilePath(wdStartupPath)

    If xStartupPath = "" And xArgument = "" Then
        MsgBox "Word's Startup Path is not valid.", , mpdAppName
        If Not (wdApp Is Nothing) Then
            wdApp.Quit
            Set wdApp = Nothing
        End If
        Exit Sub
    End If
    
    Select Case xArgument
        Case ""
            xPath = App.Path
            ' If running from App folder, set base to parent MacPac Folder
            i = InStr(UCase(xPath), "\MACPAC90\")
            If i > 0 Then _
                xPath = Left(xPath, i - 1)
        Case Else
            xPath = xArgument
    End Select
    
'***set global vars
    If Right(xPath, 1) <> "\" Then _
        xPath = xPath & "\"
        
    g_xPath = xPath
'    g_xFirmName = System.PrivateProfileString _
'                  (xPath & "MacPac90\App\MacPac.ini", "Firm", "Name")

'***run diagnosis
    Word.Documents.Add
    Word.Application.StatusBar = mpdMsgRunning
    
    Application.ScreenUpdating = False
    EchoOff

'***change page setup
    With ActiveDocument
        With .PageSetup
            .DifferentFirstPageHeaderFooter = False
            .LeftMargin = InchesToPoints(0.5)
            .RightMargin = .LeftMargin
            .TopMargin = InchesToPoints(0.5)
        End With
        With .Styles("Normal")
            .Font.Size = 10
            .LanguageID = wdNoProofing
        End With
    End With
    
    bDiagnose
    bCheckSysFiles
    bCheckClassInfo xPath
    
    Word.Selection.HomeKey wdStory
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Word.Application.StatusBar = "Finished!"
    Word.Application.Activate
    
    On Error Resume Next
    Set wdApp = Nothing
    
    Exit Sub
    
Main_Error:
    Select Case Err.Number
        Case 429    'activex
            Set wdApp = New Word.Application
            With wdApp
                .Visible = True
                .WindowState = wdWindowStateMaximize
            End With
            Resume
        Case Else
            MsgBox "Err: " & _
                    Err.Number & " - " & _
                    Err.Description, _
                    vbInformation, mpdAppName
            On Error Resume Next
            Set wdApp = Nothing
    End Select
    Exit Sub
End Sub
