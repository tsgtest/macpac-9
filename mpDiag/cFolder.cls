VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFolder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_xPath As String
Private m_xName As String
Private m_bIsMacPac As Boolean
Private m_colFiles As cFiles
Private m_colFolders As cFolders

'************
'CLASS EVENTS
'************

Private Sub Class_Initialize()
'***create collection object
    Set m_colFiles = New cFiles
    Set m_colFolders = New cFolders
End Sub

Private Sub Class_Terminate()
'***release object reference
    Set m_colFiles = Nothing
    Set m_colFolders = Nothing
End Sub

'******************************
'*****PUBLIC PROPERTIES********
'******************************
Public Property Get Path() As String
    Path = m_xPath
End Property

Public Property Let Path(xNew As String)
    m_xPath = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get IsMacPac() As Boolean
    IsMacPac = m_bIsMacPac
End Property

Public Property Let IsMacPac(bNew As Boolean)
    m_bIsMacPac = bNew
End Property

Public Function Files() As cFiles
    m_colFiles.GetFiles m_xPath
    Set Files = m_colFiles
End Function

Public Function Folders() As cFolders
    Set Folders = m_colFolders
End Function

