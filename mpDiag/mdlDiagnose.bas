Attribute VB_Name = "mdlDiagnose"
Option Explicit

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

Public m_oSource As Word.Document

Function bDiagnose() As Boolean
    Dim Folders As cFolders
    Dim NestedFolders As cFolders
    Dim fldFolder As cFolder
    Dim fldNestedFolder As cFolder
    Dim fFile As cFile
    Dim xDetails As String
    Dim i As Integer
    Dim xPath As String
    
    
    Set Folders = New cFolders
    
'***get info string
    xDetails = xDetails & "Folders" & vbTab & _
               "Files" & vbTab & _
               "Type" & vbTab & _
               "Modified Date" & vbTab & _
               "Size" & vbTab
      
'***get root folders collection
    Folders.Fill g_xPath
    
    For Each fldFolder In Folders
        i = i + 1
        If g_bAllRootFolders Or _
        (Not g_bAllRootFolders And fldFolder.IsMacPac) Then
        
            For Each fFile In fldFolder.Files
            
                xDetails = xDetails & fldFolder.Name & vbTab
                With fFile
                    If fldFolder.Name = "Personal" Then
                        xDetails = xDetails & .Name & "*" & vbTab
                    ElseIf UCase(.Name) = "MPPUBLIC.MDB" Then
                        xDetails = xDetails & .Name & "**" & vbTab
                    ElseIf UCase(.Name) = "MACPAC.INI" Or _
                           UCase(.Name) = "CI.INI" Or _
                           UCase(.Name) = "MPN90.INI" Then
                        xDetails = xDetails & .Name & "***" & vbTab
                    Else
                        xDetails = xDetails & .Name & vbTab
                    End If
                    xDetails = xDetails & .FileType & vbTab
                    xDetails = xDetails & .ModifiedDate & vbTab
                    xDetails = xDetails & Format(.Size, "#,##0") & "KB" & vbCr
                End With
                
            Next fFile
            
            Set NestedFolders = New cFolders
            NestedFolders.Fill fldFolder.Path & "\", fldFolder.Name & "\"
            
            For Each fldNestedFolder In NestedFolders
                For Each fFile In fldNestedFolder.Files
                    xDetails = xDetails & fldNestedFolder.Name & vbTab
                    With fFile
                        xDetails = xDetails & .Name & vbTab
                        xDetails = xDetails & .FileType & vbTab
                        xDetails = xDetails & .ModifiedDate & vbTab
                        xDetails = xDetails & Format(.Size, "#,##0") & "KB" & vbCr
                    End With
                Next fFile
            Next fldNestedFolder
            
        End If

    Next fldFolder
    
    If mpbase2.ApplicationDirectory <> mpbase2.ProgramDirectory Then
    '***get root folders collection from PUBLIC docments
        Set Folders = Nothing
        Set Folders = New cFolders
        
        xPath = mpbase2.ApplicationDirectory & "\"
        
        ' If running from App folder, set base to parent MacPac Folder
        i = InStrRev(UCase(xPath), "\APP\")
        If i > 0 Then _
            xPath = Left(xPath, i - 1)

        
        Folders.Fill xPath & "\"
        
        For Each fldFolder In Folders
            i = i + 1
            If g_bAllRootFolders Or _
            (Not g_bAllRootFolders And fldFolder.IsMacPac) Then
            
                For Each fFile In fldFolder.Files
                
                    xDetails = xDetails & fldFolder.Name & vbTab
                    With fFile
                        If fldFolder.Name = "Personal" Then
                            xDetails = xDetails & .Name & "*" & vbTab
                        ElseIf UCase(.Name) = "MPPUBLIC.MDB" Then
                            xDetails = xDetails & .Name & "**" & vbTab
                        ElseIf UCase(.Name) = "MACPAC.INI" Or _
                               UCase(.Name) = "CI.INI" Or _
                               UCase(.Name) = "MPN90.INI" Then
                            xDetails = xDetails & .Name & "***" & vbTab
                        Else
                            xDetails = xDetails & .Name & vbTab
                        End If
                        xDetails = xDetails & .FileType & vbTab
                        xDetails = xDetails & .ModifiedDate & vbTab
                        xDetails = xDetails & Format(.Size, "#,##0") & "KB" & vbCr
                    End With
                    
                Next fFile
                
                Set NestedFolders = New cFolders
                NestedFolders.Fill fldFolder.Path & "\", fldFolder.Name & "\"
                
                For Each fldNestedFolder In NestedFolders
                    For Each fFile In fldNestedFolder.Files
                        xDetails = xDetails & fldNestedFolder.Name & vbTab
                        With fFile
                            xDetails = xDetails & .Name & vbTab
                            xDetails = xDetails & .FileType & vbTab
                            xDetails = xDetails & .ModifiedDate & vbTab
                            xDetails = xDetails & Format(.Size, "#,##0") & "KB" & vbCr
                        End With
                    Next fFile
                Next fldNestedFolder
                
            End If
    
        Next fldFolder
    End If
    
'***create new document and insert table
    bCreateTableReport xDetails
End Function

Function bCreateTableReport(xString As String) As Boolean
    Dim rngStart As Word.Range
    Dim rngTitle As Word.Range
    Dim cCol As Word.Column
    Dim rngFooter As Word.Range
    
    m_oSource.Range.Text = ""
    m_oSource.Range.Font.Reset
    m_oSource.Range(0, 0).Select
    With m_oSource.ActiveWindow.Selection
        .InsertAfter vbCr
        .EndOf
        .InsertAfter xString
        .Range.ConvertToTable Separator:=wdSeparateByTabs, _
                              NumColumns:=5, _
                              AutoFit:=True
    End With
    
'***format table
    With m_oSource.Tables(1)
        .Rows(1).HeadingFormat = True
        With .Rows(1).Range
            .Font.Size = 12
            .Bold = True
            .Italic = True
            .ParagraphFormat.SpaceBefore = 3
            .ParagraphFormat.SpaceAfter = 3
        End With
        .Sort Excludeheader:=True, _
              FieldNumber:="Column 1", _
              SortFieldType:=wdSortFieldAlphanumeric, _
              SortOrder:=wdSortOrderAscending, _
              FieldNumber2:="Column 2", _
              SortFieldType2:=wdSortFieldAlphanumeric, _
              SortOrder2:=wdSortOrderAscending
    End With

    Set rngTitle = m_oSource.Range(0, 0)
    
    With rngTitle
        .InsertAfter "MacPac File List"
        .ParagraphFormat.Alignment = wdAlignParagraphCenter
        .Font.SmallCaps = True
        .ParagraphFormat.SpaceAfter = 0
        .Font.Size = 14
        .InsertParagraphAfter
        .EndOf
        .InsertAfter "(file dates as of " & Format(Now(), "MMMM d, yyyy") & ")"
        .Font.Size = 9
        .Font.SmallCaps = False
        .ParagraphFormat.SpaceAfter = 12
        .EndOf
        .InsertParagraphAfter
        .EndOf
        .InsertAfter "Word version: " & Word.Application.Version & Chr(13)
        .InsertAfter "Operating System: " & System.OperatingSystem & " " & System.Version & Chr(13)
        .InsertAfter "File Locations:" & Chr(13)
        If mpbase2.ApplicationDirectory <> mpbase2.ProgramDirectory Then
            .InsertAfter String(4, " ") & "Program Location: " & g_xPath & Chr(13)
            .InsertAfter String(4, " ") & "Application Data Location: " & mpbase2.ApplicationDirectory & Chr(13)
        Else
            .InsertAfter String(4, " ") & "MacPac Location: " & g_xPath & Chr(13)
        End If
        .InsertAfter String(4, " ") & "Startup: " & Word.Application.Options.DefaultFilePath(wdStartupPath) & Chr(13)
        .InsertAfter String(4, " ") & "User Templates: " & Word.Application.Options.DefaultFilePath(wdUserTemplatesPath) & Chr(13)
        .InsertAfter String(4, " ") & "Workgroup Templates: " & Word.Application.Options.DefaultFilePath(wdWorkgroupTemplatesPath) & Chr(13)
        .InsertAfter String(4, " ") & "Installed Add-Ins: " & vbTab & GetLoadedAddins() & Chr(13)
        Dim x As String
        x = GetCOMAddins()
        If x <> "" Then
            .InsertAfter String(4, " ") & "Loaded COM Add-ins: " & vbTab & x
        End If
        .Font.Size = 11
        .Font.SmallCaps = False
        .ParagraphFormat.SpaceAfter = 0
        .ParagraphFormat.Alignment = wdAlignParagraphLeft
        .ParagraphFormat.LeftIndent = InchesToPoints(2)
        .ParagraphFormat.FirstLineIndent = InchesToPoints(-2)
        .InsertParagraphAfter
        .EndOf

    End With
    
'***insertfooter language
    Set rngFooter = m_oSource.Sections(1).Footers(wdHeaderFooterPrimary).Range
        
    With rngFooter
        .SetRange rngFooter.End, rngFooter.End
        .MoveStart wdCharacter, -1
        If .Characters(1) = vbCr Then
            .MoveStart wdCharacter, 1
        Else
            .MoveStart wdCharacter, 1
            .InsertParagraphAfter
            .SetRange rngFooter.End, rngFooter.End
        End If
        .InsertAfter mpdMsgFooter
        .Paragraphs(1).SpaceBefore = 6
    End With
    
    With m_oSource
        .Tables(1).Columns.AutoFit
        .Tables(1).Rows.Alignment = wdAlignRowCenter
    End With


End Function
Function GetLoadedAddins() As String
    Dim o As Word.AddIn
    Dim x As String
    For Each o In Word.Application.AddIns
        If o.Installed Then
            x = x & Chr(11) & o.Path & "\" & o.Name
        End If
    Next o
    If x <> "" Then
        x = Mid(x, 2)
    End If
    GetLoadedAddins = x
End Function
Function GetCOMAddins() As String
    Dim o As Object
    Dim i As Integer
    Dim x As String
    Set o = Word.Application
    On Error GoTo WrongVersion
    If o.COMAddins.Count > 0 Then
        For i = 1 To o.COMAddins.Count
            If o.COMAddins.Item(i).Connect Then
                x = x & Chr(11) & o.COMAddins.Item(i).Description
            End If
        Next i
    End If
    If x <> "" Then
        x = Mid(x, 2)
    End If
    GetCOMAddins = x
WrongVersion:
End Function
Sub ListIniFiles()
    Dim xDetails As String
    On Error GoTo ErrorProc
    
    If Word.Documents.Count = 0 Then
        Set m_oSource = Word.Documents.Add()
    Else
        With m_oSource.ActiveWindow.Selection
            .EndKey wdStory
            .InsertBreak wdSectionBreakNextPage
            .EndKey wdStory
            With .Sections(1).Footers(wdHeaderFooterPrimary)
                .LinkToPrevious = False
                .Range.Delete
            End With
        End With
    End If
        
    With m_oSource.ActiveWindow.Selection
        .EndOf
        .InsertAfter "Contents of Ini Files" & vbCr
        .ParagraphFormat.Alignment = wdAlignParagraphCenter
        .Font.SmallCaps = True
        .ParagraphFormat.SpaceAfter = 0
        .Font.Size = 14
        .EndOf
        .InsertAfter "(as of " & Format(Now(), "MMMM d, yyyy") & ")" & vbCr
        .Font.Size = 9
        .Font.SmallCaps = False
        .ParagraphFormat.Alignment = wdAlignParagraphCenter
        .ParagraphFormat.SpaceAfter = 12
        .EndOf
        .ParagraphFormat.Alignment = wdAlignParagraphLeft
        .ParagraphFormat.SpaceAfter = 0
        .Font.Reset
        .Bookmarks.Add "MacPac_Ini"
        If Dir(mpbase2.ApplicationDirectory & "\MacPac.ini") <> "" Then
            .InsertAfter mpbase2.ApplicationDirectory & "\MacPac.ini" & vbCr
            .Font.Size = 13
            .Font.Bold = True
            .Font.Underline = wdUnderlineSingle
            .EndOf
            .Font.Reset
            .InsertParagraphAfter
            .EndOf
            .Font.Size = 11
            .InsertAfter xFileContents(mpbase2.ApplicationDirectory & "\MacPac.ini")
            .EndOf
        Else
            .InsertAfter "MACPAC.INI WAS NOT FOUND" & vbCr
            .Font.Bold = True
            .EndOf
            .Font.Reset
        End If
        .InsertBreak wdPageBreak
        .EndOf
        .Bookmarks.Add "User_Ini"
        If mpbase2.UserFilesDirectory <> "" Then
            If Dir(mpbase2.UserFilesDirectory & "User.ini") <> "" Then
                .InsertAfter mpbase2.UserFilesDirectory & "User.ini" & vbCr
                .Font.Bold = True
                .Font.Size = 13
                .Font.Underline = wdUnderlineSingle
                .EndOf
                .Font.Reset
                .InsertParagraphAfter
                .EndOf
                .Font.Size = 11
                .InsertAfter xFileContents(mpbase2.UserFilesDirectory & "User.ini")
                .EndOf
            Else
                .InsertAfter "USER.INI WAS NOT FOUND" & vbCr
                .Font.Bold = True
                .EndOf
                .Font.Reset
            End If
        Else
            .InsertAfter "USER.INI WAS NOT FOUND" & vbCr
            .Font.Bold = True
            .EndOf
            .Font.Reset
        End If
        .InsertBreak wdPageBreak
        .EndOf
        .Bookmarks.Add "Numbering_Ini"
        If Dir(mpbase2.ApplicationDirectoryNumbering & "\mpN90.ini") <> "" Then
            .InsertAfter mpbase2.ApplicationDirectoryNumbering & "\mpN90.ini" & vbCr
            .Font.Bold = True
            .Font.Size = 13
            .Font.Underline = wdUnderlineSingle
            .EndOf
            .Font.Reset
            .InsertParagraphAfter
            .EndOf
            .Font.Size = 11
            .InsertAfter xFileContents(mpbase2.ApplicationDirectoryNumbering & "\mpN90.ini")
            .EndOf
        Else
            .InsertAfter "MPN90.INI WAS NOT FOUND" & vbCr
            .Font.Bold = True
            .EndOf
            .Font.Reset
        End If
        .InsertBreak wdPageBreak
        .EndOf
        .Bookmarks.Add "NumTOC_Ini"
        If mpbase2.UserFilesDirectory <> "" Then
            If Dir(mpbase2.UserFilesDirectory & "NumTOC.ini") <> "" Then
                .InsertAfter mpbase2.UserFilesDirectory & "NumTOC.ini" & vbCr
                .Font.Bold = True
                .Font.Size = 13
                .Font.Underline = wdUnderlineSingle
                .EndOf
                .Font.Reset
                .InsertParagraphAfter
                .EndOf
                .Font.Size = 11
                .InsertAfter xFileContents(mpbase2.UserFilesDirectory & "NumTOC.ini")
                .EndOf
            Else
                .InsertAfter "NUMTOC.INI WAS NOT FOUND" & vbCr
                .Font.Bold = True
                .EndOf
                .Font.Reset
            End If
        Else
            .InsertAfter "NUMTOC.INI WAS NOT FOUND" & vbCr
            .Font.Bold = True
            .EndOf
            .Font.Reset
        End If
        .InsertBreak wdPageBreak
        .EndOf
        .Bookmarks.Add "CI_Ini"
        If Dir(mpbase2.ApplicationDirectoryCI & "\CI.ini") <> "" Then
            .InsertAfter mpbase2.ApplicationDirectoryCI & "\CI.ini" & vbCr
            .Font.Bold = True
            .Font.Size = 13
            .Font.Underline = wdUnderlineSingle
            .EndOf
            .Font.Reset
            .InsertParagraphAfter
            .EndOf
            .Font.Size = 11
            .InsertAfter xFileContents(mpbase2.ApplicationDirectoryCI & "\CI.ini")
            .EndOf
        ElseIf Dir(mpbase2.ApplicationDirectoryCI & "\mpCI.ini") <> "" Then
            .InsertAfter mpbase2.ApplicationDirectoryCI & "\mpCI.ini" & vbCr
            .Font.Bold = True
            .Font.Size = 13
            .Font.Underline = wdUnderlineSingle
            .EndOf
            .Font.Reset
            .InsertParagraphAfter
            .EndOf
            .Font.Size = 11
            .InsertAfter xFileContents(mpbase2.ApplicationDirectoryCI & "\mpCI.ini")
            .EndOf
        Else
            .InsertAfter "CI.INI WAS NOT FOUND" & vbCr
            .Font.Bold = True
            .EndOf
            .Font.Reset
        End If
        .InsertBreak wdPageBreak
        .EndOf
        .Bookmarks.Add "CIUser_Ini"
        If mpbase2.UserFilesDirectory <> "" Then
            If Dir(mpbase2.UserFilesDirectory & "CIUser.ini") <> "" Then
                .InsertAfter mpbase2.UserFilesDirectory & "CIUser.ini" & vbCr
                .Font.Bold = True
                .Font.Size = 13
                .Font.Underline = wdUnderlineSingle
                .EndOf
                .Font.Reset
                .InsertParagraphAfter
                .EndOf
                .Font.Size = 11
                .InsertAfter xFileContents(mpbase2.UserFilesDirectory & "CIUser.ini")
                .EndOf
            ElseIf Dir(mpbase2.UserFilesDirectory & "mpciUser.ini") <> "" Then
                .InsertAfter mpbase2.UserFilesDirectory & "mpciUser.ini" & vbCr
                .Font.Bold = True
                .Font.Size = 13
                .Font.Underline = wdUnderlineSingle
                .EndOf
                .Font.Reset
                .InsertParagraphAfter
                .EndOf
                .Font.Size = 11
                .InsertAfter xFileContents(mpbase2.UserFilesDirectory & "mpciUser.ini")
                .EndOf
            Else
                .InsertAfter "CIUSER.INI WAS NOT FOUND" & vbCr
                .Font.Bold = True
                .EndOf
                .Font.Reset
            End If
        Else
            .InsertAfter "CIUSER.INI WAS NOT FOUND" & vbCr
            .Font.Bold = True
            .EndOf
            .Font.Reset
        End If
    End With
    Exit Sub
ErrorProc:
    If Err.Number Then
        MsgBox "An error occurred reading the Ini Files", vbCritical
    End If
End Sub
Function xFileContents(xPath As String) As String
    Dim h As Long
    On Error GoTo ProcError
    h = FreeFile
    Open xPath For Input As #h
    xFileContents = Input(LOF(h), h)
    Close #h
    Exit Function
ProcError:
    xFileContents = xPath & " could not be read."
End Function
Function GetUserDir() As String
    Dim xPath As String
    On Error Resume Next
    xPath = System.PrivateProfileString(g_xPath & "MacPac90\App\MacPac.ini", "File Paths", "UserDir")
    If xPath = "" Then
        xPath = System.PrivateProfileString(g_xPath & "MacPac90\App\MacPac.ini", "General", "UserDir")
    End If
    If xPath <> "" Then
    '   substitute real path for user var if it exists in string
        If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xPath = GetUserVarPath(xPath)
        Else
    '       use environmental variable
            xPath = GetEnvironVarPath(xPath)
        End If
        If Right(xPath, 1) <> "\" Then _
            xPath = xPath & "\"
        GetUserDir = xPath
    End If
End Function
Public Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
        GetUserVarPath = xPath
        Exit Function
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    GetUserVarPath = Replace(xPath, "<UserName>", xBuf, , , vbTextCompare)
        
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "mpBase2.mdlGlobal.GetUserVarPath", Err.Source, _
        Err.Description
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    
    On Error GoTo ProcError
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    End If
    
    If xValue <> "" Then
        GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue, , , vbTextCompare)
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Exit Function
End Function


