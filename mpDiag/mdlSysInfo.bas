Attribute VB_Name = "mdlSysInfo"
Option Explicit
Const InputFile As String = "\SysFiles.lst"
'Const OutputFile As String = "\SysVerInfo.txt"

Private SysFiles() As String

Function bCheckSysFiles() As Boolean
    Dim sPath As String
    Dim sFile As String
    Dim rc As Long
    Dim i As Integer
    Dim sf() As cSysFile
    Dim sMDACDll As String
    Dim sDAODll As String
    Dim sCDODll As String '* 9.5.0
    ReDim sf(3)
    ' *** Get MDAC DLL ***
    sMDACDll = sGetPathFromClassID("ADODB.Connection")
    Set sf(0) = New cSysFile
    If sMDACDll <> "" Then
        i = InStrRev(sMDACDll, "\")
        sf(0).FileName = Mid(sMDACDll, i + 1)
        sf(0).FilePath = Left(sMDACDll, i)
        LoadVerInfo sf(0)
    Else
        sf(0).FileName = "MDAC"
        sf(0).FileVer = "Not Installed"
    End If
    
    ' *** Get DAO 3.6 DLL ***
    sDAODll = sGetPathFromClassID("DAO.DBEngine.36")
    Set sf(1) = New cSysFile
    If sDAODll <> "" Then
        i = InStrRev(sDAODll, "\")
        sf(1).FileName = Mid(sDAODll, i + 1)
        sf(1).FilePath = Left(sDAODll, i)
        LoadVerInfo sf(1)
    Else
        sf(1).FileName = "DAO360.DLL"
        sf(1).FileVer = "Not Installed"
    End If
           
'* 9.5.0
    ' *** Get CDO DLL ***
    sCDODll = sGetPathFromClassID("ActMsg.Session")
    Set sf(2) = New cSysFile
    If sCDODll <> "" Then
        i = InStrRev(sCDODll, "\")
        sf(2).FileName = Mid(sCDODll, i + 1)
        sf(2).FilePath = Left(sCDODll, i)
        LoadVerInfo sf(2)
    Else
        sf(2).FileName = "CDO.DLL"
        sf(2).FileVer = "Not Installed"
    End If
    
'    ' *** Get DAO 3.5 DLL ***
'    sDAODll = ""
'    sDAODll = sGetPathFromClassID("DAO.DBEngine.35")
'    Set sf(2) = New cSysFile
'    If sDAODll <> "" Then
'        I = InStrRev(sDAODll, "\")
'        sf(2).FileName = Mid(sDAODll, I + 1)
'        sf(2).FilePath = Left(sDAODll, I)
'        LoadVerInfo sf(2)
'    Else
'        sf(2).FileName = "DAO350.DLL"
'        sf(2).FileVer = "Not Installed"
'    End If

'* End 9.5.0
  
   ' **** Set Default Dir to Windows System Subdirectory ****
    sPath = xGetSystemFolder()
        
    If Dir(App.Path & InputFile) <> "" Then
         LoadFileList
        
         For i = 0 To UBound(SysFiles())
             ReDim Preserve sf(3 + i)
             Set sf(i + 3) = New cSysFile
             sf(i + 3).FilePath = sPath
             If SysFiles(i) <> "" Then
                 sf(i + 3).FileName = SysFiles(i)
                 If Dir(sf(i + 3).FullName) <> "" Then
                     LoadVerInfo sf(i + 3)
                 Else
                     sf(i + 3).FileVer = "MISSING"
                 End If
             End If
         Next i
    Else
        ReDim Preserve sf(4)
        Set sf(3) = New cSysFile
        sf(3).FileName = App.Path & InputFile & " not found."
        Set sf(4) = New cSysFile
        sf(4).FileName = "Complete system file version report could not be generated."
    End If
    WriteVerInfo sf()
End Function
Private Sub LoadVerInfo(oFile As cSysFile)

    Dim rc                As Long
    Dim sFileName As String
    Dim lDummy            As Long
    Dim sBuffer()         As Byte
    Dim lBufferLen        As Long
    Dim lVerPointer       As Long
    Dim udtVerBuffer      As VS_FIXEDFILEINFO
    Dim lVerbufferLen     As Long
    
    sFileName = oFile.FullName
    '*** Get size ****
    oFile.FileDate = VBA.FileDateTime(sFileName)
    oFile.FileSize = VBA.FileLen(sFileName)
    lBufferLen = GetFileVersionInfoSize(sFileName, lDummy)
    If lBufferLen < 1 Then
       Exit Sub
    End If    '**** Store info to udtVerBuffer struct ****
    
    ReDim sBuffer(lBufferLen)
    
    rc = GetFileVersionInfo(sFileName, 0&, lBufferLen, sBuffer(0))
    rc = VerQueryValue(sBuffer(0), "\", lVerPointer, lVerbufferLen)
    MoveMemory udtVerBuffer, lVerPointer, Len(udtVerBuffer)
    With oFile
        '**** Determine Structure Version number - NOT USED ****
        .StrucVer = Format$(udtVerBuffer.dwStrucVersionh) & "." & _
           Format$(udtVerBuffer.dwStrucVersionl)
        '**** Determine File Version number ****
        .FileVer = Format$(udtVerBuffer.dwFileVersionMSh) & "." & _
           Format$(udtVerBuffer.dwFileVersionMSl) & "." & _
           Format$(udtVerBuffer.dwFileVersionLSh) & "." & _
           Format$(udtVerBuffer.dwFileVersionLSl)
        '**** Determine Product Version number ****
        .ProdVer = Format$(udtVerBuffer.dwProductVersionMSh) & "." & _
           Format$(udtVerBuffer.dwProductVersionMSl) & "." & _
           Format$(udtVerBuffer.dwProductVersionLSh) & "." & _
           Format$(udtVerBuffer.dwProductVersionLSl)
        '**** Determine Boolean attributes of File ****
        .FileFlags = ""
        If udtVerBuffer.dwFileFlags And VS_FF_DEBUG _
           Then .FileFlags = "Debug "
        If udtVerBuffer.dwFileFlags And VS_FF_PRERELEASE _
           Then .FileFlags = .FileFlags & "PreRel "
        If udtVerBuffer.dwFileFlags And VS_FF_PATCHED _
           Then .FileFlags = .FileFlags & "Patched "
        If udtVerBuffer.dwFileFlags And VS_FF_PRIVATEBUILD _
           Then .FileFlags = .FileFlags & "Private "
        If udtVerBuffer.dwFileFlags And VS_FF_INFOINFERRED _
           Then .FileFlags = .FileFlags & "Info "
        If udtVerBuffer.dwFileFlags And VS_FF_SPECIALBUILD _
           Then .FileFlags = .FileFlags & "Special "
        If udtVerBuffer.dwFileFlags And VFT2_UNKNOWN _
           Then .FileFlags = .FileFlags + "Unknown "
        '**** Determine OS for which file was designed ****
        Select Case udtVerBuffer.dwFileOS
           Case VOS_DOS_WINDOWS16
             .FileOS = "DOS-Win16"
           Case VOS_DOS_WINDOWS32
             .FileOS = "DOS-Win32"
           Case VOS_OS216_PM16
             .FileOS = "OS/2-16 PM-16"
           Case VOS_OS232_PM32
             .FileOS = "OS/2-16 PM-32"
           Case VOS_NT_WINDOWS32
             .FileOS = "NT-Win32"
           Case Else
             .FileOS = "Unknown"
        End Select
        Select Case udtVerBuffer.dwFileType
           Case VFT_APP
              .FileType = "App"
           Case VFT_DLL
              .FileType = "DLL"
           Case VFT_DRV
              .FileType = "Driver"
              Select Case udtVerBuffer.dwFileSubtype
                 Case VFT2_DRV_PRINTER
                    .FileSubType = "Printer drv"
                 Case VFT2_DRV_KEYBOARD
                    .FileSubType = "Keyboard drv"
                 Case VFT2_DRV_LANGUAGE
                    .FileSubType = "Language drv"
                 Case VFT2_DRV_DISPLAY
                    .FileSubType = "Display drv"
                 Case VFT2_DRV_MOUSE
                    .FileSubType = "Mouse drv"
                 Case VFT2_DRV_NETWORK
                    .FileSubType = "Network drv"
                 Case VFT2_DRV_SYSTEM
                    .FileSubType = "System drv"
                 Case VFT2_DRV_INSTALLABLE
                    .FileSubType = "Installable"
                 Case VFT2_DRV_SOUND
                    .FileSubType = "Sound drv"
                 Case VFT2_DRV_COMM
                    .FileSubType = "Comm drv"
                 Case VFT2_UNKNOWN
                    .FileSubType = "Unknown"
              End Select
           Case VFT_FONT
              .FileType = "Font"
              Select Case udtVerBuffer.dwFileSubtype
                 Case VFT2_FONT_RASTER
                    .FileSubType = "Raster Font"
                 Case VFT2_FONT_VECTOR
                    .FileSubType = "Vector Font"
                 Case VFT2_FONT_TRUETYPE
                    .FileSubType = "TrueType Font"
              End Select
           Case VFT_VXD
              .FileType = "VxD"
           Case VFT_STATIC_LIB
              .FileType = "Lib"
           Case Else
              .FileType = "Unknown"
        End Select
    End With
End Sub
Private Sub WriteVerInfo(oFiles() As cSysFile)
    Dim xDetails As String
    On Error GoTo ErrorProc
    
    If Word.Documents.Count = 0 Then
        Set m_oSource = Word.Documents.Add()
    Else
        With m_oSource.ActiveWindow.Selection
            .EndKey wdStory
            .InsertBreak wdSectionBreakNextPage
            .EndKey wdStory
            With .Sections(1).Footers(wdHeaderFooterPrimary)
                .LinkToPrevious = False
                .Range.Delete
            End With
        End With
    End If
        
    xDetails = "File Name" & vbTab & "Version" & vbTab & "Date" & vbTab & "Size" & vbCr
    
    Dim i As Integer
    For i = 0 To UBound(oFiles())
        With oFiles(i)
            xDetails = xDetails & .FileName & vbTab & .FileVer & vbTab & Format(.FileDate, "MM/dd/yyyy hh:mm am/pm") & vbTab & Format(.FileSize, "#,###") & vbCr
        End With
    Next i
    bCreateSysInfoTableReport xDetails
    Err.Clear
ErrorProc:
    If Err.Number Then
        MsgBox "An error occurred creating the list of System Files", vbCritical
    End If
End Sub
Private Sub LoadFileList()
    Dim i As Integer
    On Error GoTo ReadError
    Open App.Path & InputFile For Input As #1
        
    i = 0
    Do While Not EOF(1)
        ReDim Preserve SysFiles(i)
        Line Input #1, SysFiles(i)
        i = i + 1
    Loop
    Err.Clear
ReadError:
    If Err.Number = 53 Then
        ReDim SysFiles(0)
    End If
    Close #1
End Sub
Function bCreateSysInfoTableReport(xString As String) As Boolean
    Dim rngStart As Word.Range
    Dim rngTitle As Word.Range
    Dim cCol As Word.Column
    Dim oTbl As Word.Table
    Dim rngFooter As Word.Range
    

    With m_oSource.ActiveWindow.Selection
        .EndOf
        .Bookmarks.Add "SystemFiles"
        .InsertAfter "Workstation System Files"
        .ParagraphFormat.Alignment = wdAlignParagraphCenter
        .Font.SmallCaps = True
        .ParagraphFormat.SpaceAfter = 0
        .Font.Size = 14
        .InsertParagraphAfter
        .EndOf
        .InsertAfter "(as of " & Format(Now(), "MMMM d, yyyy") & ")"
        .Font.Size = 9
        .Font.SmallCaps = False
        .ParagraphFormat.SpaceAfter = 12
        .EndOf
        .InsertParagraphAfter
        .EndOf
        .InsertAfter "MacPac relies on the system files listed below to function properly.  " & _
            "The MacPac Setup.exe may or may not have installed these files.  " & _
            "Since other applications also install and/or rely on many of these same system " & _
            "files, there may be different versions on different workstations.  This listing is " & _
            "provided to help you troubleshoot MacPac by comparing two reports, one on a workstation " & _
            "where MacPac is functioning properly and one where MacPac may be erroring." & vbCr & _
            "In the following list, msado15.dll should be in Program Files\Common files\System\ADO, dao360.dll in Program Files\Common Files\Microsoft Shared\DAO " & _
            " and cdo.dll in Program Files\Common Files\System\MSMapi\1033." & vbCr & _
            "All other files should be installed in " & xGetSystemFolder & "." & vbCr
        .ParagraphFormat.Alignment = wdAlignParagraphLeft
        .ParagraphFormat.LeftIndent = InchesToPoints(0.5)
        .ParagraphFormat.RightIndent = InchesToPoints(0.5)
        .Font.Size = 11
        .EndOf
        .ParagraphFormat.Reset
        .Font.Reset
        .Font.Size = 11
        .InsertAfter xString
        Set oTbl = .Range.ConvertToTable(Separator:=wdSeparateByTabs, _
                              NumColumns:=4, _
                              AutoFit:=True)
    End With
    
'***format table
    With oTbl
        .Rows(1).HeadingFormat = True
        With .Rows(1).Range
            .Font.Size = 12
            .Bold = True
            .Italic = True
            .ParagraphFormat.SpaceBefore = 3
            .ParagraphFormat.SpaceAfter = 3
        End With
        .Columns.AutoFit
        .Rows.Alignment = wdAlignRowCenter
    End With

End Function
Public Function xGetSystemFolder() As String
    Dim sPath As String
    Dim rc As Integer
    On Error Resume Next
    sPath = Space$(256)
    rc = GetSystemDirectory(sPath, Len(sPath))
    sPath = LCase$(Mid$(sPath, 1, InStr(sPath, Chr(0)) - 1))
    xGetSystemFolder = sPath
End Function
