VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cFiles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'*********************************************************
'Class Name:    cFiles
'Author:        Charlie Homo
'Date:          10/6/1999
'Description:   Maintains the collection of cFile objects
'*********************************************************

Private m_colFiles As Collection

'************
'CLASS EVENTS
'************

Private Sub Class_Initialize()
'***create the collection object
    Set m_colFiles = New Collection
End Sub

Private Sub Class_Terminate()
'***clear the reference
    Set m_colFiles = Nothing
End Sub

'*****************
'PUBLIC PROPERTIES
'*****************

Public Property Get Count() As Long
    Count = m_colFiles.Count
End Property


'**************
'PUBLIC METHODS
'**************

Public Function Add(xName As String, _
                    dDate As Date, _
                    lSize As Long, _
                    xPath As String, _
                    xType As String) As cFile
    
    Dim newFile As cFile
    
    Set newFile = New cFile
    
    With newFile
        .Name = xName
        .Path = xPath
        .Size = lSize
        .ModifiedDate = dDate
        .FileType = xType
'***    add member to collection
        m_colFiles.Add newFile, .Name
    End With
    
'***return the added file obj
    Set Add = newFile
    
End Function

Public Function Item(vKey As Variant) As cFile
Attribute Item.VB_UserMemId = 0
    Set Item = m_colFiles.Item(vKey)
End Function

Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_colFiles.[_NewEnum]
End Function

Public Function GetFiles(xPath)
    Dim xFile As String
    Dim xFilePath As String
    Dim newFile As cFile
    Dim xExtension As String

'***cycle through each file in Folder Path
    xFile = Dir(xPath & "\*.*")
    Do While xFile <> ""
        xExtension = Right(xFile, 4)
        Select Case UCase(xExtension)
            Case ".LDB", ".LIB", ".EXP", ".OCA"
'***            do nothing
            Case Else
                xFilePath = xPath & "\" & xFile
'***            check for folders/directories
'                If (GetAttr(xFilePath) And vbDirectory) <> vbDirectory Then
                    Set newFile = New cFile
                    
                    With newFile
                        .Path = xFilePath
                        .ModifiedDate = VBA.FileSystem.FileDateTime(xFilePath)
                        .Name = xFile
                        .Size = VBA.FileSystem.FileLen(xFilePath)
                        .FileType = xGetFileType(xExtension, xFile)
                        m_colFiles.Add newFile, .Name
                    End With
'                End If
        End Select
'***    get next entry
        xFile = Dir()
    Loop
   
End Function

Private Function xGetFileType(xExt, xFile) As String
    Dim xRegName As String
    Dim xTemp As String
    
    xRegName = System.PrivateProfileString("", "HKEY_CLASSES_ROOT\" & xExt, "")

    If xRegName <> "" Then
        xTemp = System.PrivateProfileString("", "HKEY_CLASSES_ROOT\" & xRegName, "")
    Else
        Select Case UCase(xExt)
            Case ".STY"
                xTemp = "MacPac Style"
            Case ".ATE"
                xTemp = "MacPac Boilerplate"
            Case ".MPB"
                xTemp = "MacPac Bitmap"
            Case Else
                If InStr(xFile, ".sty") Then
                    xTemp = "MacPac Style"
                Else
                    xTemp = UCase(Right(xExt, 3))
                End If
        End Select
        xTemp = xTemp & " file"
    End If
    
    xGetFileType = xTemp
End Function
