Attribute VB_Name = "mdlPleadingPaper"
Option Explicit
Function bDetail(iPleadingPaperType As Integer, _
                 Optional xBPFile As String, _
                 Optional xBPRange As String, _
                 Optional xBPFooter As String, _
                 Optional sTopMargin As Single, _
                 Optional sBottomMargin As Single, _
                 Optional sLeftMargin As Single, _
                 Optional sRightMargin As Single, _
                 Optional sHeaderDistance As Single, _
                 Optional sFooterDistance As Single) As Boolean
                         
'fills args with appropriate values
'for type iPleadingPaperType -
'**************************************************
'   THIS FUNCTION SHOULD REALLY REFERENCE THE
'   PUBLIC DB TO GET THESE VALUES - IN THE FUTURE
'   SOMEONE SHOULD CODE THIS AS SUCH
'**************************************************
    Select Case iPleadingPaperType
        Case mpPleadingPaperNone
            xBPRange = "zzmpTOCHeader_Primary"
            sTopMargin = mpTopMarginTOC
            sBottomMargin = mpBottomMarginTOC
            sLeftMargin = mpLeftMarginTOC
            sRightMargin = mpRightMarginTOC
            sHeaderDistance = mpHeaderDistTOC
            sFooterDistance = mpFooterDistTOC
        
        Case mpPleadingPaper28Line
            xBPRange = "zzmpPleadingLineNo_TOHeader"
            sTopMargin = mpPleadingTOCTopMargin
            sBottomMargin = 0.5
            sLeftMargin = mpPleading28LeftMargin
            sRightMargin = mpPleading28RightMargin
            sHeaderDistance = mpPleadingHeaderDistance
            sFooterDistance = mpPleadingFooterDistance
            
        Case mpPleadingPaper28LineFNLand
            xBPRange = "zzmpPleadingLineNo_TOHeaderFN"
            sTopMargin = mpPleadingTOCTopMargin
            sBottomMargin = 0.5
            sLeftMargin = mpPleading28LeftMargin
            sRightMargin = mpPleading28RightMargin
            sHeaderDistance = mpPleadingHeaderDistance
            sFooterDistance = mpPleadingFooterDistance
        
        Case mpPleadingPaper28LineFNPort
            xBPRange = "zzmpPleadingLineNo_TOHeader"
            sTopMargin = mpPleadingTOCTopMargin
            sBottomMargin = 0.5
            sLeftMargin = mpPleading28LeftMargin
            sRightMargin = mpPleading28RightMargin
            sHeaderDistance = mpPleadingHeaderDistance
            sFooterDistance = mpPleadingFooterDistance
        
        Case mpPleadingPaper26Line
            xBPRange = "zzmpPleadingLineNo26_TOHeader"
            sTopMargin = mpPleading26TOCTopMargin
            sBottomMargin = mpPleading26BotMargin
            sLeftMargin = mpPleading26LeftMargin
            sRightMargin = mpPleading26RightMargin
            sHeaderDistance = mpPleadingHeaderDistance
            sFooterDistance = mpPleadingFooterDistance
        
        Case mpPleadingPaper26LineFNLand
            xBPRange = "zzmpPleadingLineNo26_TOHeaderFN"
            sTopMargin = mpPleading26TOCTopMargin
            sBottomMargin = mpPleading26BotMargin
            sLeftMargin = mpPleading26LeftMargin
            sRightMargin = mpPleading26RightMargin
            sHeaderDistance = mpPleadingHeaderDistance
            sFooterDistance = mpPleadingFooterDistance
        
        Case mpPleadingPaper26LineFNPort
            xBPRange = "zzmpPleadingLineNo26_TOHeader"
            sTopMargin = mpPleading26TOCTopMargin
            sBottomMargin = mpPleading26BotMargin
            sLeftMargin = mpPleading26LeftMargin
            sRightMargin = mpPleading26RightMargin
            sHeaderDistance = mpPleadingHeaderDistance
            sFooterDistance = mpPleadingFooterDistance
    End Select

'---add pleading footer table if pleading document
    If Not g_oPleadingPaper Is Nothing Then
        '9.9.5005 - no reason to insert boilerplate footers if they're
        'going to get wiped out by mp2k pleading paper
        xBPFooter = "zzmpNothing"
    ElseIf bIsPleadingDocument(ActiveDocument) Then
        If bIsSpecifiedTemplate(ActiveDocument, "Pleading Appellate") Then
            xBPFooter = "zzmpPleadingFooter_AppTOC"
        ElseIf bIsSpecifiedTemplate(ActiveDocument, "Pleading CA") And _
                bIsStateCourt Then
            xBPFooter = "zzmpPleadingFooterTOC"
        Else
            xBPFooter = "zzmpPleadingFooterTOC_US"
        End If
    Else
        xBPFooter = "zzmpTOCFooter"
    End If
    
    

End Function
Public Function iPleadingPaperType(rngHeader As Word.Range) As Integer
    With rngHeader.Find
        .Text = "26" & Chr(11) & "27" & Chr(11) & "28"
        .Execute
        If .Found Then
            iPleadingPaperType = 28
        Else
            .Text = "24" & Chr(11) & "25" & Chr(11) & "26"
            .Execute
            If .Found Then
                iPleadingPaperType = 26
            Else
                iPleadingPaperType = 0
            End If
        End If
    End With
End Function
Public Function rngSetFirmNameTarget(rngTarget As Word.Range, _
                            secSection As Word.Section) As Word.Range
    
    Dim iNumHeaderCols As Integer
    Dim iNumFooterCols As Integer
    
    Dim iFirmNameType
    
'---determine if landscaped header col exists
    iNumHeaderCols = iFooterType(secSection _
                        .Headers(wdHeaderFooterPrimary))
                            
    If iNumHeaderCols <> 3 Then    'firm name in footer
'---        check the footer
        iNumFooterCols = iFooterType(secSection _
                        .Footers(wdHeaderFooterPrimary))
                
        If iNumFooterCols = 0 Then     'no table, no firm name
            Set rngSetFirmNameTarget = Nothing
        Else
            Set rngSetFirmNameTarget = rngTarget.Tables(1) _
                                .Cell(Row:=1, Column:=1).Range
        End If
    
    ElseIf iNumHeaderCols = 3 Then  'firm name in header
        Set rngSetFirmNameTarget = rngTarget.Tables(1) _
                                    .Cell(1, 1).Range
    End If
            
End Function
Public Function iGetPleadingPaperType(docDocument As Word.Document) As Integer
    Dim xPPaper As String
    Dim xPrefix As String
    Dim xSuffix As String
    Dim iLines As Integer
    Dim rngHeader As Word.Range
    Dim rngFooter As Word.Range
    Dim iNumHeaderCols As Integer
    Dim iNumFooterCols As Integer
    Dim rngFNTarget As Word.Range
    Dim secScope As Word.Section
    Dim iTestLen As Integer
    Dim xTest As String
    Dim dVar As Variable
    Dim bDVExists As Boolean
    
'---First check for PP doc var
    For Each dVar In docDocument.Variables
        If dVar.Name = "zzmpFixed_PleadingPaperType" Then
            bDVExists = True
            Exit For
        End If
    Next
    
    If bDVExists Then
        xPPaper = docDocument.Variables("zzmpFixed_PleadingPaperType")
    Else
'---See if there's pleading paper at all
        Set secScope = docDocument.Sections.First
        Set rngHeader = docDocument.Sections.First.Headers _
                                (wdHeaderFooterPrimary).Range
        Set rngFooter = docDocument.Sections.First.Footers _
                                (wdHeaderFooterPrimary).Range
        
        iLines = iPleadingPaperType(rngHeader)
    
        If iLines = 0 Then      'no pleading paper
            iGetPleadingPaperType = mpPleadingPaperNone
            Exit Function
        Else                    'determine paper type
            Select Case iLines
                Case 26
                    xPrefix = "PP26"
                
                Case 28
                    xPrefix = "PP28"
                
                Case Else
            End Select
        '---now check for a) three cols in header
            iNumHeaderCols = iFooterType(secScope _
                            .Headers(wdHeaderFooterPrimary))
            
            If iNumHeaderCols = 3 Then  'check for firm name text in first col
                
                Set rngFNTarget = rngSetFirmNameTarget(rngHeader, _
                                                    secScope)
            '---find out if there's any text
                iTestLen = Len(rngFNTarget.Text)
                If iTestLen < 5 Then
                    xSuffix = "None"
                Else
                    xSuffix = "FNLandscaped"
                End If
            
            Else   'check for footer table
             
                iNumFooterCols = iFooterType(secScope _
                                .Footers(wdHeaderFooterPrimary))
                If iNumFooterCols <> 4 Then  'it's not a pleading footer table
                    xSuffix = "None"
                Else
                    Set rngFNTarget = rngSetFirmNameTarget(rngFooter, _
                                                        secScope)
                '---find out if there's any text
                    iTestLen = Len(rngFNTarget.Text)
                    If iTestLen < 5 Then
                        xSuffix = "None"
                    Else
                        xSuffix = "FNPortrait"
                    End If
            
                End If
            
            End If
        
        End If
    
    xPPaper = xPrefix & xSuffix
    
    End If
                                        
    '---now assign constant determined by PPtype string
    
    Select Case xPPaper
        Case "PP26None"
            iGetPleadingPaperType = mpPleadingPaper26Line
        Case "PP26FNPortrait"
            iGetPleadingPaperType = mpPleadingPaper26LineFNPort
        Case "PP26FNLandscaped"
            iGetPleadingPaperType = mpPleadingPaper26LineFNLand
        Case "PP28None"
            iGetPleadingPaperType = mpPleadingPaper28Line
        Case "PP28FNPortrait"
            iGetPleadingPaperType = mpPleadingPaper28LineFNPort
        Case "PP28FNLandscaped"
            iGetPleadingPaperType = mpPleadingPaper28LineFNLand
                                        
        Case Else
    End Select
                                        
End Function
Function bIsStateCourt() As Boolean
    Dim xCourtVar As String
    On Error Resume Next
    xCourtVar = UCase(ActiveDocument.Variables("lstCourts"))
    Select Case xCourtVar
        Case "SUPERIOR", "MUNICIPAL", "OTHER", ""
            bIsStateCourt = True
        Case Else
            bIsStateCourt = False
    End Select
    
End Function
Public Function bIsPleadingDocument(docDoc As Word.Document) As Boolean
    With docDoc
        If InStr(UCase(.AttachedTemplate), "PLEAD") Or _
                InStr(UCase(.AttachedTemplate), "PROOF") Or _
                InStr(UCase(.AttachedTemplate), "VER") Then
            bIsPleadingDocument = True
        End If
    End With
End Function
Function bIsSpecifiedTemplate(docDoc As Word.Document, _
                              xTemplateName As String) As Boolean
'returns true if name of attached template
'of docDoc is xTemplateName
    Dim bTemp As Boolean
    
    bTemp = (InStr(UCase(docDoc.AttachedTemplate), _
                   UCase(xTemplateName)) > 0)
    bIsSpecifiedTemplate = bTemp
End Function
Function iFooterType(ftrFooter As HeaderFooter) As Integer
'returns 0 if not a MacPac
'footer (zzmpFooter style) -
'else, returns an integer
'representing # of columns

    Dim tFtrTable As Word.Table
    Dim xStylePrefix As String
    On Error Resume Next
'   cycle through all tables in footer
    For Each tFtrTable In ftrFooter.Range.Tables
        With tFtrTable.Range
            xStylePrefix = Left(.Style, 10)
            If InStr(xStylePrefix, "Footer") Then
                iFooterType = .Columns.Count
                Exit Function
            Else
                iFooterType = .Columns.Count
            End If
        End With
    Next tFtrTable
End Function

