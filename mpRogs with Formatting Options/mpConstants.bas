Attribute VB_Name = "mpConstantsVariables"
'CONSTANTS

Public Const mpAppName As String = "MacPac"

'VARIABLES

Public xRogTitlesFile As String
'misc
Public bRet As Variant
Public lRet As Long
Public xMsg As String
Public bPreviouslyInitialized As Boolean

'ids match fldType in mpPublic.tblInterrogatoryStyles.fldType
Public Enum mpRogStyles
    mpRogStyles_QuestionHeading = 0
    mpRogStyles_QuestionText = 1
    mpRogStyles_ResponseHeading = 2
    mpRogStyles_ResponseText = 3
    mpRogStyles_RuninQuestion = 4
    mpRogStyles_RuninResponse = 5
End Enum

Public Enum mpRogTypes
    mpRogTypes_Question = 0
    mpRogTypes_Response = 1
End Enum

'dialogs/shift-tabbing
'Dialog
Public g_bInVBALostFocus As Boolean
Public g_ActivatingTabProgrammatically As Boolean
Public g_VShiftDown As Boolean

Public Sub SetDocVars(frmP As Form, _
                      Optional bMarkAsRestarted As Boolean = True)
    
    Dim ctlControl As VB.Control
    Dim xName As String
    Dim vValue As Variant
    Dim varReuse As Variable
    
    For Each ctlControl In frmP.Controls
        If ControlHasValue(ctlControl) Then
                On Error Resume Next
'---            assign true false values for chkboxes, tglbuttons, Optionbuttons
                xName = ctlControl.Name
                If InStr(xName, "spn") Then
                    vValue = ctlControl.Value
                Else
                    vValue = ctlControl
                End If
                If InStr(xName, "chk") Or InStr(xName, "tgl") Or InStr(xName, "opt") Then
                    If vValue = False Then
                        vValue = "False"
                    Else
                        vValue = "True"
                    End If
                End If
                
'               if doc var exists, set value, else add doc var
                With ActiveDocument
                    On Error Resume Next
                    Set varReuse = Nothing
                    Set varReuse = .Variables(xName)
                    On Error GoTo 0

                    If varReuse Is Nothing Then
                        .Variables.Add xName, xNullToString(vValue)
                    Else
                        varReuse.Value = xNullToString(vValue)
                    End If
                End With
        End If
    Next ctlControl
    
'   mark as restarted
    If bMarkAsRestarted Then _
        ActiveDocument.Variables("Restarted") = True
End Sub
Public Sub GetDocVars(frmP As Form)
'assigns stored doc variable values to
'controls of same name - forces author list
'controls to evaluate first to avoid change
'events of those controls from affecting
'the values of other controls

    Dim ctlControl As Control
    Dim vCurValue As Variant
    Dim xPriorityCtls(1 To 9) As String
    Dim bIsPriorityControl As Boolean
    Dim i As Integer
    Dim xName As String
    Dim SkipListUpdate As Boolean
    
'   enumerate trouble controls - this is the only part,
'   if any, that should require modification
    xPriorityCtls(1) = "cmbAuthorLists"
    xPriorityCtls(2) = "lstAttyList"
    xPriorityCtls(3) = "cmbAuthor"
    xPriorityCtls(4) = "cmbPrefLists"
    xPriorityCtls(5) = "cmbSetAuthorPref"
    xPriorityCtls(6) = "cmbOptOffices"  'needed for Verification
    xPriorityCtls(7) = "cmbRecipient"
    xPriorityCtls(8) = "cmbRecipientLists"
    xPriorityCtls(9) = "cmbState"  'needed for Certificate of Service
    
'   run through priority controls, setting their values-
'   remember, change events will occur, but they should
'   be irrelevant after non-priority controls are set below
    For i = 1 To UBound(xPriorityCtls)
        On Error Resume Next
        Set ctlControl = frmP.Controls(xPriorityCtls(i))
        vCurValue = ActiveDocument.Variables(ctlControl.Name)
        If Not IsEmpty(vCurValue) Then
            SkipListUpdate = True
            ctlControl = vCurValue
        End If
    Next i
    
    On Error GoTo 0
    
'   run through non-priority controls,
'   setting their values
    For Each ctlControl In frmP.Controls
        If ControlHasValue(ctlControl) Then
            xName = ctlControl.Name
            
            bIsPriorityControl = _
                (xName = xPriorityCtls(1)) Or _
                (xName = xPriorityCtls(2)) Or _
                (xName = xPriorityCtls(3)) Or _
                (xName = xPriorityCtls(4)) Or _
                (xName = xPriorityCtls(5)) Or _
                (xName = xPriorityCtls(6)) Or _
                (xName = xPriorityCtls(7)) Or _
                (xName = xPriorityCtls(8)) Or _
                (xName = xPriorityCtls(9))
                
            If Not bIsPriorityControl Then
                On Error Resume Next
                    vCurValue = ActiveDocument.Variables(ctlControl.Name)
                If Not IsEmpty(vCurValue) Then
                    SkipListUpdate = True
                    If InStr(xName, "chk") Then
                        ctlControl = Abs(CBool(vCurValue))
                    ElseIf InStr(xName, "spn") Then
                        ctlControl.Value = _
                            mpbase2.xLocalizeNumericString(CStr(vCurValue))
                    Else
                        ctlControl = vCurValue
                    End If
                End If
            End If
        End If
    Next ctlControl
End Sub


