VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Begin VB.Form frmInterrogs 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create Discovery Questions and Responses"
   ClientHeight    =   7008
   ClientLeft      =   3348
   ClientTop       =   1812
   ClientWidth     =   4872
   Icon            =   "frmInterrogs.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7008
   ScaleWidth      =   4872
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDefaultHelp 
      Caption         =   "Help"
      Height          =   370
      Left            =   60
      TabIndex        =   57
      Top             =   6570
      Width           =   720
   End
   Begin VB.CommandButton btnUpdate 
      Caption         =   "U&pdate Field Codes\Styles"
      Height          =   370
      Left            =   840
      TabIndex        =   56
      ToolTipText     =   "Click to update automatic numbering field codes"
      Top             =   6570
      Width           =   2340
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   370
      Left            =   3264
      TabIndex        =   54
      ToolTipText     =   "Click to create heading"
      Top             =   6570
      Width           =   720
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   370
      Left            =   4035
      TabIndex        =   55
      Top             =   6570
      Width           =   720
   End
   Begin TabDlg.SSTab MultiPage1 
      Height          =   6360
      Left            =   30
      TabIndex        =   59
      TabStop         =   0   'False
      Top             =   90
      Width           =   4755
      _ExtentX        =   8382
      _ExtentY        =   11218
      _Version        =   393216
      Style           =   1
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "&1-Main"
      TabPicture(0)   =   "frmInterrogs.frx":000C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lblQuestion"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblResponse"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblPositionMessage"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblSet"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblStartNumberAt"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblType"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lstInterrogatories"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "obtnCustomRog"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "obtnCustomRogResponse"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "txtNumberRog"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "txtHeadingSetRogs"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "btnBSSetClear"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "txtCustomRog"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtCustomRogResponse"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "btnClear"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "chkRepeatPrevious"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "chkAutoNumRog"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "chkAlternate"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "fraStyDirect"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).ControlCount=   19
      TabCaption(1)   =   "&2-Styles"
      TabPicture(1)   =   "frmInterrogs.frx":0028
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "pnlStyCharacter"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "pnlStyParagraph"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "spnStyHLeftMargin"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "btnStySave"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "btnStyReset"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Frame2"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "&3-Direct"
      TabPicture(2)   =   "frmInterrogs.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "pnlBSParagraph"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "pnlBSCharacter"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      Begin VB.Frame fraStyDirect 
         Height          =   495
         Left            =   -74880
         TabIndex        =   60
         Top             =   345
         Width           =   4470
         Begin VB.OptionButton optDirect 
            Appearance      =   0  'Flat
            Caption         =   "Use &Direct Formatting"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   2025
            TabIndex        =   62
            Top             =   165
            Width           =   2100
         End
         Begin VB.OptionButton optStyles 
            Appearance      =   0  'Flat
            Caption         =   "Use St&yles"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   255
            TabIndex        =   61
            Top             =   165
            Width           =   1350
         End
      End
      Begin VB.CheckBox chkAlternate 
         Appearance      =   0  'Flat
         Caption         =   "&Alternate Between Questions && Responses"
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74850
         TabIndex        =   10
         Top             =   4440
         Value           =   1  'Checked
         Width           =   3480
      End
      Begin VB.CheckBox chkAutoNumRog 
         Appearance      =   0  'Flat
         Caption         =   "&Use Automatic Numbering (Field Codes)"
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74850
         TabIndex        =   11
         ToolTipText     =   "Click to have Word automatically number the interrogatory headings; clear to enter numbers manually"
         Top             =   4770
         Value           =   1  'Checked
         Width           =   4284
      End
      Begin VB.CheckBox chkRepeatPrevious 
         Appearance      =   0  'Flat
         Caption         =   "Repeat Pre&vious Number"
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   -74850
         TabIndex        =   12
         Top             =   5115
         Width           =   2160
      End
      Begin VB.Frame Frame2 
         Height          =   1710
         Left            =   105
         TabIndex        =   19
         Top             =   375
         Width           =   4530
         Begin VB.OptionButton obtnStySep 
            Appearance      =   0  'Flat
            Caption         =   "D&iscovery Heading On Its Own Line"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   240
            TabIndex        =   20
            Top             =   180
            Value           =   -1  'True
            Width           =   3225
         End
         Begin VB.OptionButton obtnStyRunin 
            Appearance      =   0  'Flat
            Caption         =   "Run-in Discover&y Heading"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   240
            TabIndex        =   21
            Top             =   525
            Width           =   2535
         End
         Begin MSForms.ComboBox cmbStyStyles 
            Height          =   315
            Left            =   240
            TabIndex        =   23
            Top             =   1185
            Width           =   2295
            VariousPropertyBits=   746608667
            BorderStyle     =   1
            DisplayStyle    =   7
            Size            =   "4048;556"
            MatchEntry      =   1
            ShowDropButtonWhen=   2
            SpecialEffect   =   0
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
         End
         Begin VB.Label lblStyList 
            Caption         =   "S&tyles:"
            Height          =   240
            Left            =   285
            TabIndex        =   22
            Top             =   945
            Width           =   660
         End
      End
      Begin VB.Frame pnlBSCharacter 
         Caption         =   "Character:"
         Height          =   2250
         Left            =   -74910
         TabIndex        =   42
         Top             =   525
         Width           =   4545
         Begin VB.CheckBox chkBSUnderlined 
            Appearance      =   0  'Flat
            Caption         =   "&Underlined"
            ForeColor       =   &H80000008&
            Height          =   437
            Left            =   2310
            TabIndex        =   46
            Top             =   780
            Value           =   1  'Checked
            Width           =   1126
         End
         Begin VB.CheckBox chkBSBold 
            Appearance      =   0  'Flat
            Caption         =   "&Bold Text"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   2310
            TabIndex        =   45
            Top             =   442
            Value           =   1  'Checked
            Width           =   1126
         End
         Begin VB.CheckBox chkBSSmallCaps 
            Appearance      =   0  'Flat
            Caption         =   "&Small Caps"
            ForeColor       =   &H80000008&
            Height          =   437
            Left            =   713
            TabIndex        =   44
            Top             =   780
            Width           =   1126
         End
         Begin VB.CheckBox chkBSAllCaps 
            Appearance      =   0  'Flat
            Caption         =   "&All Caps"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   713
            TabIndex        =   43
            Top             =   442
            Value           =   1  'Checked
            Width           =   975
         End
      End
      Begin VB.Frame pnlBSParagraph 
         Caption         =   "Paragraph:"
         Height          =   2820
         Left            =   -74925
         TabIndex        =   47
         Top             =   2910
         Width           =   4560
         Begin VB.OptionButton obtnBSRunIn 
            Appearance      =   0  'Flat
            Caption         =   "&Run-in Interrogatory Heading"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   435
            TabIndex        =   49
            Top             =   825
            Width           =   2535
         End
         Begin VB.OptionButton obtnBSSep 
            Appearance      =   0  'Flat
            Caption         =   "&Interrogatory Heading On Its Own Line"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   435
            TabIndex        =   48
            Top             =   450
            Value           =   -1  'True
            Width           =   3225
         End
         Begin mpControls3.SpinTextInternational spnHLeftMargin 
            Height          =   315
            Left            =   2160
            TabIndex        =   51
            Top             =   1500
            Width           =   1000
            _ExtentX        =   1778
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   8.5
            AppendSymbol    =   -1  'True
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin mpControls3.SpinTextInternational spnLeftMargin 
            Height          =   315
            Left            =   2160
            TabIndex        =   53
            Top             =   2015
            Width           =   1000
            _ExtentX        =   1778
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   8.5
            AppendSymbol    =   -1  'True
            Value           =   0.5
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin VB.Label lbBSHIndent 
            Caption         =   "Indent (in inc&hes) For Interrogatory Heading:"
            Height          =   480
            Left            =   435
            TabIndex        =   50
            Top             =   1425
            Width           =   1695
         End
         Begin VB.Label lblLeftMargin 
            Caption         =   "Ind&ent (in inches) For Following Paragraph:"
            Height          =   480
            Left            =   435
            TabIndex        =   52
            Top             =   1940
            Width           =   1560
         End
      End
      Begin VB.CommandButton btnStyReset 
         Caption         =   "&Reset"
         Height          =   420
         Left            =   3600
         TabIndex        =   40
         Top             =   5790
         Width           =   855
      End
      Begin VB.CommandButton btnStySave 
         Caption         =   "Sa&ve"
         Height          =   420
         Left            =   2655
         TabIndex        =   39
         Top             =   5790
         Width           =   855
      End
      Begin mpControls3.SpinTextInternational spnStyHLeftMargin 
         Height          =   312
         Left            =   2412
         TabIndex        =   31
         Top             =   3996
         Width           =   888
         _ExtentX        =   1566
         _ExtentY        =   550
         IncrementValue  =   0.1
         MaxValue        =   8.5
         AppendSymbol    =   -1  'True
         ValueUnit       =   0
         DisplayUnit     =   0
      End
      Begin VB.Frame pnlStyParagraph 
         Caption         =   "Paragraph Formats"
         Height          =   2025
         Left            =   105
         TabIndex        =   29
         Top             =   3645
         Width           =   4545
         Begin mpControls3.SpinTextInternational spnStySAfter 
            Height          =   300
            Left            =   3516
            TabIndex        =   38
            Top             =   1416
            Width           =   756
            _ExtentX        =   1334
            _ExtentY        =   529
            IncrementValue  =   6
            MaxValue        =   1584
            AppendSymbol    =   -1  'True
         End
         Begin VB.OptionButton optStySingle 
            Appearance      =   0  'Flat
            Caption         =   "Si&ngle"
            ForeColor       =   &H80000008&
            Height          =   330
            Left            =   960
            TabIndex        =   33
            Top             =   892
            Value           =   -1  'True
            Width           =   810
         End
         Begin VB.OptionButton optStyDouble 
            Appearance      =   0  'Flat
            Caption         =   "&Double"
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   1776
            TabIndex        =   34
            Top             =   915
            Width           =   915
         End
         Begin mpControls3.SpinTextInternational spnStySBefore 
            Height          =   312
            Left            =   1404
            TabIndex        =   36
            Top             =   1392
            Width           =   756
            _ExtentX        =   1334
            _ExtentY        =   550
            IncrementValue  =   6
            MaxValue        =   1584
            AppendSymbol    =   -1  'True
            AllowFractions  =   0   'False
         End
         Begin VB.Label lblStyHIndent 
            Caption         =   "First &Line Indent (inches):"
            Height          =   288
            Left            =   264
            TabIndex        =   30
            Top             =   372
            Width           =   1968
         End
         Begin VB.Label lblStySAfter 
            Caption         =   "Spa&ce After:"
            Height          =   252
            Left            =   2484
            TabIndex        =   37
            Top             =   1440
            Width           =   936
         End
         Begin VB.Label lblStySpacing 
            Caption         =   "Spacing:"
            Height          =   312
            Left            =   264
            TabIndex        =   32
            Top             =   936
            Width           =   648
         End
         Begin VB.Label lblStySBefore 
            Caption         =   "Space B&efore:"
            Height          =   252
            Left            =   240
            TabIndex        =   35
            Top             =   1440
            Width           =   1068
         End
      End
      Begin VB.Frame pnlStyCharacter 
         Caption         =   "Character Formats"
         Height          =   1365
         Left            =   105
         TabIndex        =   24
         Top             =   2130
         Width           =   4545
         Begin VB.CheckBox chkStyUnderlined 
            Appearance      =   0  'Flat
            Caption         =   "&Underlined"
            ForeColor       =   &H80000008&
            Height          =   437
            Left            =   2310
            TabIndex        =   28
            Top             =   675
            Width           =   1126
         End
         Begin VB.CheckBox chkStyBold 
            Appearance      =   0  'Flat
            Caption         =   "&Bold Text"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   2310
            TabIndex        =   27
            Top             =   330
            Width           =   1126
         End
         Begin VB.CheckBox chkStySmallCaps 
            Appearance      =   0  'Flat
            Caption         =   "&Small Caps"
            ForeColor       =   &H80000008&
            Height          =   437
            Left            =   675
            TabIndex        =   26
            Top             =   675
            Width           =   1126
         End
         Begin VB.CheckBox chkStyAllCaps 
            Appearance      =   0  'Flat
            Caption         =   "&All Caps"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   675
            TabIndex        =   25
            Top             =   330
            Width           =   975
         End
      End
      Begin VB.CommandButton btnClear 
         Caption         =   "&Clear ^"
         Height          =   340
         Left            =   -71205
         TabIndex        =   9
         Top             =   4275
         Width           =   810
      End
      Begin VB.TextBox txtCustomRogResponse 
         Appearance      =   0  'Flat
         Height          =   331
         Left            =   -73725
         TabIndex        =   8
         ToolTipText     =   "Enter Custom Discovery Response heading here"
         Top             =   3840
         Width           =   3345
      End
      Begin VB.TextBox txtCustomRog 
         Appearance      =   0  'Flat
         Height          =   331
         Left            =   -73725
         TabIndex        =   5
         ToolTipText     =   "Enter Custom Discovery heading here"
         Top             =   3390
         Width           =   3345
      End
      Begin VB.CommandButton btnBSSetClear 
         Caption         =   "< C&lear"
         Height          =   370
         Left            =   -71220
         TabIndex        =   17
         Top             =   5835
         Width           =   840
      End
      Begin VB.TextBox txtHeadingSetRogs 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   -72255
         TabIndex        =   16
         ToolTipText     =   "Enter a number here greater than starting number to generate a set of question/response headings in your document."
         Top             =   5865
         Width           =   675
      End
      Begin VB.TextBox txtNumberRog 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   -72255
         TabIndex        =   14
         Top             =   5415
         Width           =   675
      End
      Begin VB.OptionButton obtnCustomRogResponse 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74025
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   3855
         Width           =   195
      End
      Begin VB.OptionButton obtnCustomRog 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -74025
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   3420
         Width           =   225
      End
      Begin MSForms.ListBox lstInterrogatories 
         Height          =   1800
         Left            =   -74895
         TabIndex        =   1
         Top             =   1125
         Width           =   4500
         VariousPropertyBits=   746586139
         BackColor       =   -2147483644
         BorderStyle     =   1
         ScrollBars      =   3
         DisplayStyle    =   2
         Size            =   "7937;3175"
         BoundColumn     =   2
         ColumnCount     =   4
         cColumnInfo     =   4
         MatchEntry      =   0
         ListStyle       =   1
         SpecialEffect   =   0
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
         Object.Width           =   "141;0;0;0"
      End
      Begin VB.Label lblType 
         Caption         =   "&Type:"
         Height          =   255
         Left            =   -74865
         TabIndex        =   0
         Top             =   870
         Width           =   1350
      End
      Begin VB.Label lblStartNumberAt 
         Caption         =   "Start &Numbering At:"
         Height          =   270
         Left            =   -73830
         TabIndex        =   13
         Top             =   5490
         Width           =   1455
      End
      Begin VB.Label lblSet 
         Alignment       =   1  'Right Justify
         Caption         =   "&Generate Heading/Response Set from Starting Number Through:"
         Height          =   480
         Left            =   -74880
         TabIndex        =   15
         Top             =   5805
         Width           =   2490
      End
      Begin VB.Label lblPositionMessage 
         Alignment       =   2  'Center
         Caption         =   "(Heading inserted at cursor position)"
         Height          =   240
         Left            =   -74850
         TabIndex        =   2
         Top             =   2985
         Width           =   4260
      End
      Begin VB.Label lblResponse 
         Caption         =   "Custom &Response:"
         Height          =   435
         Left            =   -74865
         TabIndex        =   6
         Top             =   3810
         Width           =   765
      End
      Begin VB.Label lblQuestion 
         Caption         =   "Custom &Question:"
         Height          =   420
         Left            =   -74865
         TabIndex        =   3
         Top             =   3225
         Width           =   720
      End
   End
   Begin VB.PictureBox picCycleTab1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   200
      Left            =   400
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   18
      Top             =   6010
      Width           =   200
   End
   Begin VB.PictureBox picCycleTab2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   200
      Left            =   600
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   41
      Top             =   6010
      Width           =   200
   End
   Begin VB.PictureBox picCycleTab3 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   200
      Left            =   735
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   58
      TabStop         =   0   'False
      Top             =   6045
      Width           =   200
   End
End
Attribute VB_Name = "frmInterrogs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bCustomDefined As Boolean
Dim bFormInit As Boolean
Private vbControls() As String
Public m_bFinished As Boolean
Private m_xRunInStyles() As String
Private m_xNonRunInStyles() As String
Private m_xStyles() As String
Private m_bIsDirty As Boolean
Private m_bIsFormatOnly As Boolean

Private Sub btnCancel_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnCancel_Click
End Sub

Private Sub btnFinish_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnFinish_Click
End Sub

Private Sub btnStyReset_Click()
    With Me.cmbStyStyles
        Me.chkStyBold.Value = Abs(CInt(GetValue(.Text, "fldBold")))
        Me.spnStyHLeftMargin.Value = GetValue(.Text, "fldFirstLineIndent")
        If GetValue(.Text, "fldLineSpacing") = "Single" Then
            Me.optStySingle = True
        Else
            Me.optStyDouble = True
        End If
        Me.spnStySBefore.Value = GetValue(.Text, "fldSpacebefore")
        Me.spnStySAfter.Value = GetValue(.Text, "fldSpaceAfter")
        Me.chkStyUnderlined = Abs(CInt(GetValue(.Text, "fldUnderline")))    '9.7.1 - #4184
        Me.chkStyAllCaps.Value = Abs(CBool(GetValue(.Text, "fldAllCaps")))
        Me.chkStySmallCaps.Value = Abs(CBool(GetValue(.Text, "fldSmallCaps")))
    End With
End Sub

Private Sub btnStySave_Click()
    SetIniSettings
    UpdateDocumentStyle     '9.7.1 - #4123
    Me.btnStySave.Enabled = False
End Sub
Private Sub btnStySave_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnStySave_Click
End Sub

Private Sub btnUpdate_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnUpdate_Click
End Sub

Private Sub chkBSAllCaps_Click()
    On Error Resume Next
    If chkBSAllCaps = 1 Then _
        Me.chkBSSmallCaps = 0
End Sub

Private Sub chkStyBold_Click()
    Me.btnStySave.Enabled = True
End Sub
Private Sub chkBSSmallCaps_Click()
    On Error Resume Next
    If chkBSSmallCaps = 1 Then _
        Me.chkBSAllCaps = 0
End Sub

Private Sub chkStyUnderlined_Click()
    Me.btnStySave.Enabled = True
End Sub

Private Sub chkStyAllCaps_Click()
    On Error Resume Next
    If chkStyAllCaps = 1 Then _
        Me.chkStySmallCaps = 0
    Me.btnStySave.Enabled = True
End Sub

Private Sub chkStySmallCaps_Click()
    On Error Resume Next
    If chkStySmallCaps = 1 Then _
        Me.chkStyAllCaps = 0
    Me.btnStySave.Enabled = True
End Sub

Private Sub cmbStyStyles_Change()
    Dim bEnable As Boolean
    Select Case GetValue(Me.cmbStyStyles.Text, "fldType")
        Case mpRogStyles_QuestionText, mpRogStyles_ResponseText
            bEnable = False
        Case Else
            bEnable = True
    End Select
    Me.pnlStyCharacter.Enabled = bEnable
    Me.chkStyAllCaps.Enabled = bEnable
    Me.chkStyBold.Enabled = bEnable
    Me.chkStySmallCaps.Enabled = bEnable
    Me.chkStyUnderlined.Enabled = bEnable
    
    GetIniSettings True
End Sub

Private Sub cmdDefaultHelp_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    cmdDefaultHelp_Click
End Sub

Private Sub lstInterrogatories_GotFocus()
    bEnsureSelectedContent lstInterrogatories
End Sub

Private Sub obtnBSRunIn_Click()
    lblLeftMargin.Enabled = False
    spnLeftMargin.Enabled = False
End Sub

Private Sub obtnBSSep_Click()
    lblLeftMargin.Enabled = True
    spnLeftMargin.Enabled = True
End Sub

Private Sub obtnCustomRogResponse_GotFocus()
    SendKeys "+{tab}"
End Sub

Private Sub obtnCustomRogResponse_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    SendKeys "{tab}{tab}", 1
End Sub

Private Sub optDirect_Click()
    EnableTabs
End Sub

Private Sub optStyDouble_Click()
    Me.btnStySave.Enabled = True
End Sub

Private Sub optStyles_Click()
    EnableTabs
End Sub

Private Sub optStySingle_Click()
    Me.btnStySave.Enabled = True
End Sub
Private Sub picCycleTab2_GotFocus()
    CycleTabs 2, vbControls()
End Sub
Private Sub picCycleTab1_GotFocus()
    CycleTabs 1, vbControls()
End Sub
Private Sub picCycleTab3_GotFocus()
    CycleTabs 3, vbControls()
End Sub

Private Sub spnLeftMargin_LostFocus()
    VBACtlLostFocus Me.spnLeftMargin, vbControls()
End Sub

Private Sub spnLeftMargin_Validate(Cancel As Boolean)
    If Not spnLeftMargin.IsValid Then _
        Cancel = True
End Sub
Private Sub spnStyHLeftMargin_Change()
    Me.btnStySave.Enabled = True
End Sub

Private Sub spnStyHLeftMargin_LostFocus()
    VBACtlLostFocus Me.spnStyHLeftMargin, vbControls()
End Sub

Private Sub spnStyHLeftMargin_Validate(Cancel As Boolean)
    If Not spnStyHLeftMargin.IsValid Then _
        Cancel = True
End Sub

Private Sub spnHLeftMargin_LostFocus()
    VBACtlLostFocus Me.spnHLeftMargin, vbControls()
End Sub

Private Sub spnHLeftMargin_Validate(Cancel As Boolean)
    If Not spnHLeftMargin.IsValid Then _
        Cancel = True
End Sub

Private Sub spnStySAfter_Change()
    Me.btnStySave.Enabled = True
End Sub

Private Sub spnStySAfter_LostFocus()
    VBACtlLostFocus Me.spnStySAfter, vbControls()
End Sub

Private Sub spnStySAfter_Validate(Cancel As Boolean)
    If Not spnStySAfter.IsValid Then _
        Cancel = True
End Sub

Private Sub spnStySBefore_Change()
    Me.btnStySave.Enabled = True
End Sub

Private Sub spnStySBefore_LostFocus()
    VBACtlLostFocus Me.spnStySBefore, vbControls()
End Sub

Private Sub spnStySBefore_Validate(Cancel As Boolean)
    If Not spnStySBefore.IsValid Then _
        Cancel = True
End Sub

Private Sub lstInterrogatories_LostFocus()
    VBACtlLostFocus Me.lstInterrogatories, vbControls()
End Sub
Private Sub MultiPage1_Click(PreviousTab As Integer)

    If MultiPage1.Tab <> 1 And Not bFormInit Then
        If Me.btnStySave.Enabled Then PromptToSave
    End If
    
    If Not g_ActivatingTabProgrammatically Then
        DisableInactiveTabs Me
        SelectFirstCtlOnTab Me.MultiPage1, vbControls()
    End If
    
    If MultiPage1.Tab = 1 Then
        Me.btnStySave.Enabled = False
    ElseIf MultiPage1.Tab = 0 Then
       Me.chkRepeatPrevious.Enabled = False
    End If

End Sub


Private Sub txtCustomRog_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then _
        btnFinish_Click
End Sub

Private Sub txtCustomRog_LostFocus()
    'If txtCustomRog = "" Then _
        obtnCustomRog.Value = False
End Sub

Private Sub txtCustomRogResponse_GotFocus()
    bEnsureSelectedContent Me.txtCustomRogResponse
    obtnCustomRogResponse = True
    If txtCustomRogResponse <> "" Then
        bCustomDefined = True
    End If
End Sub
Private Sub txtCustomRog_GotFocus()
    obtnCustomRog.Value = True
    bEnsureSelectedContent Me.txtCustomRog
End Sub

Private Sub txtCustomRogResponse_LostFocus()
    If txtCustomRogResponse = "" And (Not Me.obtnCustomRogResponse) Then
        chkRepeatPrevious.Enabled = False
    Else
        bCustomDefined = True
    End If
End Sub

Private Sub txtHeadingSetRogs_GotFocus()
    bEnsureSelectedContent Me.txtHeadingSetRogs
End Sub
Private Sub txtNumberRog_GotFocus()
    bEnsureSelectedContent Me.txtNumberRog
End Sub

Function bIsEven(iInteger As Double) As Boolean
'---this function s/b ported to mpNumbers
    
    Dim iCompare As Double
    iCompare = iInteger / 2
    If Fix(iCompare) = iInteger / 2 Then
        bIsEven = True
        Exit Function
    End If
End Function

Function bWriteInterrogs() As Boolean
    Dim rngLocation As Word.Range
    Dim xRogHeading As String
    Dim xSeqName As String
    Dim bViewFields As Boolean
    Dim iReset As Integer
    Dim iEnd As Integer
    Dim iStart As Integer
    Dim i As Double
    Dim iCount As Double
    Dim iTrim As Integer
    Dim bExactSpace As Boolean
    Dim bRepeatPrevious As Boolean
    Dim xPreviousSeq As String
    
    With Word.ActiveDocument.ActiveWindow.View
        bViewFields = .ShowFieldCodes
        .ShowFieldCodes = False
    End With
    
    bRepeatPrevious = Me.chkRepeatPrevious
    
    If ActiveDocument.Styles(wdStyleNormal).ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
        bExactSpace = True
    End If
    
    Set rngLocation = Selection.Range
    If Right(rngLocation.Text, 1) = vbCr Then
        rngLocation.MoveEnd wdCharacter, -1
    End If
    On Error Resume Next
    iEnd = Val(Me.txtHeadingSetRogs)
    iStart = Val(Me.txtNumberRog)
    
    SetDocVars Me
    
    If iEnd = 0 Then
        iStart = 1
        iEnd = 1
    Else
        iEnd = ((iEnd - iStart) + 1)
        If chkAlternate Then _
            iEnd = iEnd * 2
        iStart = 1
    End If
    
    With rngLocation
        .Text = Empty
        .MoveEnd wdCharacter
        Select Case Right(.Text, 1)
            Case vbCr
                .MoveEnd wdCharacter, -1
            Case Else
                .MoveEnd wdCharacter, -1
                If iEnd < 2 Then
                    .InsertAfter " <mptemp>"
                    .Collapse wdCollapseStart
                Else
                    .InsertAfter vbCr
                    .Collapse wdCollapseStart
                End If
        End Select
    End With
    iCount = Val(Me.txtNumberRog)
    'If iCount = 0 Then iCount = 1
    
    For i = iStart To iEnd
        iReset = Val(ActiveDocument.Variables("iRogReset"))
        
        If lstInterrogatories.ListIndex = -1 Then
            If obtnCustomRog Then
                xRogHeading = txtCustomRog
                If chkAlternate Then _
                    obtnCustomRogResponse = 1
            Else
                xRogHeading = txtCustomRogResponse
                If chkAlternate Then _
                    obtnCustomRog = 1
            End If
            xSeqName = UCase(mpbase2.xTrimSpaces(xRogHeading))
        Else
            If iStart <> iEnd Then
                If chkAlternate And bIsEven(i) Then
                    xRogHeading = mpbase2.xTrimTrailingChrs(lstInterrogatories.List(lstInterrogatories.ListIndex + 1, 0), " ", True, False)
                Else
                    xRogHeading = mpbase2.xTrimTrailingChrs(lstInterrogatories.List(lstInterrogatories.ListIndex, 0), " ", True, False)
                End If
            Else
                xRogHeading = mpbase2.xTrimTrailingChrs(lstInterrogatories.List(lstInterrogatories.ListIndex, 0), " ", True, False)
            End If
            'get sequence field value
            If chkAlternate And bIsEven(i) Then
                xSeqName = UCase(Me.lstInterrogatories.List(Me.lstInterrogatories.ListIndex + 1, 2))
            Else
                xSeqName = UCase(Me.lstInterrogatories.List(Me.lstInterrogatories.ListIndex, 2))
            End If
        End If
        
        
'---rog formatting here
        With rngLocation
            
            With .ParagraphFormat
'---check if line spacing is double or exactly single and set accordingly
                If bExactSpace Then
                    .LineSpacingRule = wdLineSpaceExactly
                    .LineSpacing = 24
                Else
                    .LineSpacingRule = wdLineSpaceDouble
                End If
                .KeepTogether = True
                .KeepWithNext = True
                .FirstLineIndent = InchesToPoints(Me.spnHLeftMargin.Value)
            End With
            
            .InsertAfter xRogHeading
            If Me.chkAutoNumRog = 1 Then
                .InsertAfter " "
            End If
            
            With .Font
                .AllCaps = Me.chkBSAllCaps
                .SmallCaps = Me.chkBSSmallCaps
                .Bold = Me.chkBSBold
                If Me.chkBSUnderlined Then
                    .Underline = wdUnderlineSingle
                End If
            End With
            
            If Me.chkAutoNumRog = 1 Then
                If txtNumberRog <> "" Or Me.txtHeadingSetRogs <> "" Then
                    If iReset < 3 And iReset > 0 Then
                        xSeqName = xSeqName & "\R" & LTrim(txtNumberRog)
                    End If
                    If chkAlternate Then
                        If iReset > 0 Then iReset = iReset + 1
                        If iReset = 3 Then iReset = 0
                    Else
                        iReset = 0
                    End If
                    With ActiveDocument
                        On Error Resume Next
                        .Variables.Add "iRogReset", iReset
                        .Variables("iRogReset") = iReset
                    End With
                ElseIf bRepeatPrevious Then
                    xPreviousSeq = xGetPreviousSeq()
                    If xPreviousSeq <> "" Then
                        xSeqName = xPreviousSeq & " \C"
                    Else
                        MsgBox "Could not find previous Sequence Field", vbExclamation
                        Exit Function
                    End If
                End If
                .Collapse wdCollapseEnd
                .Fields.Add Range:=rngLocation, _
                            Type:=wdFieldSequence, _
                            Text:=xSeqName
                .MoveEnd Unit:=wdWord, Count:=1
            Else
                If iCount = 0 Then
                    .InsertAfter ""
                Else
                    .InsertAfter " " & LTrim(Str(iCount))
                End If
            End If
            
            If .Characters.Last.Text = " " Then .Characters.Last.Delete     '*c
            .Collapse wdCollapseEnd
            .InsertAfter ":"
            .Font.Underline = wdUnderlineNone
            If Me.obtnBSSep Then
                .InsertAfter vbCr
            Else
                .InsertAfter "  "
            End If
            .Collapse wdCollapseEnd
            
            With .ParagraphFormat
                .KeepTogether = False
                .KeepWithNext = False
                If Me.obtnBSSep Then _
                    .FirstLineIndent = InchesToPoints(Me.spnLeftMargin.Value)
            End With
            If iEnd > 1 Then
                If i <> iEnd Then
                        If Me.obtnBSSep Then
                            .InsertAfter vbCr
                            .Collapse wdCollapseEnd
                            With .Font
                                .Reset
                                .AllCaps = False
                                .Bold = False
                            End With
                        Else
                           .Collapse wdCollapseEnd
'                       .InsertAfter " "
                            With .Font
                                .Reset
                                .AllCaps = False
                                .Bold = False
                            End With
                            .InsertAfter vbCr
                    End If
                End If
                .Collapse wdCollapseEnd
            Else
                If Me.chkAutoNumRog Then
                    .MoveEnd wdCharacter, Len("<mptemp>")
                    If UCase(.Text) = "<MPTEMP>" Then
                        .Text = ""
                    End If
                Else
                    .MoveEnd wdCharacter, Len(" <mptemp>")
                    If UCase(.Text) = " <MPTEMP>" Then
                        .Text = ""
                    End If
                End If
                .Collapse wdCollapseStart
                .Select
                With Selection.Font
                    .Reset
                    .AllCaps = False
                    .Bold = False
                End With
            End If
        End With
        
        If bIsEven(i) Then
            iCount = iCount + 1
        Else
            If chkAlternate = vbUnchecked Then
                iCount = iCount + 1
            End If
        End If
        
    Next i

    Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = bViewFields

End Function

Function bWriteStyInterrogs() As Boolean
    Dim rngLocation As Word.Range
    Dim xRogHeading As String
    Dim xSeqName As String
    Dim bViewFields As Boolean
    Dim iReset As Integer
    Dim iEnd As Integer
    Dim iStart As Integer
    Dim i As Double
    Dim iCount As Double
    Dim iTrim As Integer
    Dim bIsAnswer As Boolean
    Dim xPreviousSeq As String
    Dim bRepeatPrevious As Boolean
    
    Dim bExactSpace As Boolean
        
    With Word.ActiveDocument.ActiveWindow.View
        bViewFields = .ShowFieldCodes
        .ShowFieldCodes = False
    End With
    
    bRepeatPrevious = Me.chkRepeatPrevious
    
    If ActiveDocument.Styles(wdStyleNormal).ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
        bExactSpace = True
    End If
    
    Set rngLocation = Selection.Range
    If Right(rngLocation.Text, 1) = vbCr Then
        rngLocation.MoveEnd wdCharacter, -1
    End If
    On Error Resume Next
    iEnd = Val(Me.txtHeadingSetRogs)
    iStart = Val(Me.txtNumberRog)
    
    SetDocVars Me
    
    If iEnd = 0 Then
        iStart = 1
        iEnd = 1
    Else
        iEnd = ((iEnd - iStart) + 1)
        If chkAlternate Then _
            iEnd = iEnd * 2
        iStart = 1
    End If
'9.7.1 - #4190
    With rngLocation
        .Text = Empty
        .MoveEnd wdCharacter
        Select Case Right(.Text, 1)
            Case vbCr
                .MoveEnd wdCharacter, -1
            Case Else
                .MoveEnd wdCharacter, -1
                If iEnd < 2 Then
                        .InsertAfter " <mptemp>"
                        .Collapse wdCollapseStart
                Else
                    .InsertAfter vbCr
                    .Collapse wdCollapseStart
                End If
        End Select
    End With
    
    iCount = Val(Me.txtNumberRog)
    
    For i = iStart To iEnd
        iReset = Val(ActiveDocument.Variables("iRogReset"))
        
        If lstInterrogatories.ListIndex = -1 Then
            If obtnCustomRog Then
                xRogHeading = txtCustomRog
                If chkAlternate Then _
                    obtnCustomRog = 0
                bIsAnswer = False
            Else
                xRogHeading = txtCustomRogResponse
                If chkAlternate Then _
                    obtnCustomRog = 1
                bIsAnswer = True
            End If
            'get sequence value
            xSeqName = UCase(mpbase2.xTrimSpaces(xRogHeading))
        Else
            If iStart <> iEnd Then
                If chkAlternate And bIsEven(i) Then
                    xRogHeading = mpbase2.xTrimTrailingChrs(lstInterrogatories.List(lstInterrogatories.ListIndex + 1, 0), " ", True, False)
                Else
                    xRogHeading = mpbase2.xTrimTrailingChrs(lstInterrogatories.List(Me.lstInterrogatories.ListIndex, 0), " ", True, False)
                End If
            Else
                xRogHeading = mpbase2.xTrimTrailingChrs(lstInterrogatories.List(Me.lstInterrogatories.ListIndex, 0), " ", True, False)
            End If
            
            If Me.lstInterrogatories.List(Me.lstInterrogatories.ListIndex, 3) = mpRogTypes_Response Then
                bIsAnswer = True
            Else
                bIsAnswer = False
            End If
            
            'get sequence field value
            If chkAlternate And bIsEven(i) Then
                bIsAnswer = Not bIsAnswer
                xSeqName = UCase(Me.lstInterrogatories.List(Me.lstInterrogatories.ListIndex + 1, 2))
            Else
                xSeqName = UCase(Me.lstInterrogatories.List(Me.lstInterrogatories.ListIndex, 2))
            End If
        End If
        
        
'---rog formatting here
        With rngLocation
            
            .InsertAfter xRogHeading
            If Me.chkAutoNumRog = 1 Then
                .InsertAfter " "
            End If

            
            If Me.chkAutoNumRog = 1 Then
                If txtNumberRog <> "" Or Me.txtHeadingSetRogs <> "" Then
                    If iReset < 3 And iReset > 0 Then
                        xSeqName = xSeqName & "\R" & LTrim(txtNumberRog)
                    End If
                    If chkAlternate Then
                        If iReset > 0 Then iReset = iReset + 1
                        If iReset = 3 Then iReset = 0
                    Else
                        iReset = 0
                    End If
                    With ActiveDocument
                        On Error Resume Next
                        .Variables.Add "iRogReset", iReset
                        .Variables("iRogReset") = iReset
                    End With
                ElseIf bRepeatPrevious Then
                    xPreviousSeq = xGetPreviousSeq()
                    If xPreviousSeq <> "" Then
                        xSeqName = xPreviousSeq & " \C"
                    Else
                        MsgBox "Could not find previous Sequence Field", vbExclamation
                        Exit Function
                    End If
                End If
                .Collapse wdCollapseEnd
                .Fields.Add Range:=rngLocation, _
                            Type:=wdFieldSequence, _
                            Text:=xSeqName
                .MoveEnd Unit:=wdWord, Count:=1
            Else
                If iCount = 0 Then
                    .InsertAfter ""
                Else
                    .InsertAfter " " & LTrim(Str(iCount))
                End If
            End If
'9.7.1 - #4190
            If .Characters.Last.Text = " " Then .Characters.Last.Delete
            .Collapse wdCollapseEnd
            .InsertAfter ":"
            .Font.Underline = wdUnderlineNone
            If Me.obtnStySep Then
                If bIsAnswer Then
                    .Paragraphs(1).Style = GetStyleName(mpRogStyles_ResponseHeading)
                Else
                    .Paragraphs(1).Style = GetStyleName(mpRogStyles_QuestionHeading)
                End If
                .InsertAfter vbCr
            Else
                If bIsAnswer Then
                    .Paragraphs(1).Style = GetStyleName(mpRogStyles_RuninResponse)
                Else
                    .Paragraphs(1).Style = GetStyleName(mpRogStyles_RuninQuestion)
                End If
                .Collapse wdCollapseEnd
                .InsertAfter "  "
                With .Font
                    .Bold = False
                    .AllCaps = False
                    .SmallCaps = False
                    .Underline = wdUnderlineNone
                End With
            End If
            .Collapse wdCollapseEnd
            
            If Me.obtnStySep Then
                If bIsAnswer Then
                    .Paragraphs(1).Style = GetStyleName(mpRogStyles_ResponseText)
                Else
                    .Paragraphs(1).Style = GetStyleName(mpRogStyles_QuestionText)
                End If
            Else
            End If
            If iEnd > 1 Then
'9.7.1 - #4190
'                If i = iEnd Then
'                    If Me.chkAutoNumRog Then
'                        .MoveEnd wdCharacter, Len("<mptemp>")
'                        If UCase(.Text) = "<MPTEMP>" Then
'                            .Text = ""
'                        End If
'                    Else
'                        .MoveEnd wdCharacter, Len(" <mptemp>")
'                        If UCase(.Text) = " <MPTEMP>" Then
'                            .Text = ""
'                        End If
'                    End If
'                End If
                
                If i <> iEnd Then
                    If Me.obtnStySep Then
                        .InsertAfter vbCr
                    Else
                       .Collapse wdCollapseEnd
    '                   .InsertAfter " "
                        .InsertAfter vbCr
                    End If
                End If
                .Collapse wdCollapseEnd
            Else
'9.7.1 - #4190
                If Me.chkAutoNumRog Then
                    .MoveEnd wdCharacter, Len("<mptemp>")
                    If UCase(.Text) = "<MPTEMP>" Then
                        .Text = ""
                    End If
                Else
                    .MoveEnd wdCharacter, Len(" <mptemp>")
                    If UCase(.Text) = " <MPTEMP>" Then
                        .Text = ""
                    End If
                End If
                .Collapse wdCollapseStart
                .Select
            End If
        End With
        If bIsEven(i) Then
            iCount = iCount + 1
        Else
            If chkAlternate = vbUnchecked Then
                iCount = iCount + 1
            End If
        End If
        
    Next i

    Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = bViewFields
End Function

Private Sub btnBSSetClear_Click()
    txtNumberRog = ""
    txtHeadingSetRogs = ""
    txtNumberRog.SetFocus
End Sub

Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnClear_Click()
    With Me
        .txtCustomRog = ""
        .txtCustomRogResponse = ""
        .txtCustomRog.SetFocus
        bCustomDefined = False
    End With
End Sub

Private Sub btnFinish_Click()

    If Me.btnStySave.Enabled Then
        PromptToSave
    End If
    
'   alternate custom heading set and either is missing
    On Error GoTo ProcError
    If (Me.obtnCustomRog Or Me.obtnCustomRogResponse = 1) And _
            (Me.txtCustomRog = "" Or Me.txtCustomRogResponse = "") And _
            chkAlternate And txtHeadingSetRogs <> "" Then
        MsgBox "You must enter a custom heading and response.", vbInformation
        If txtCustomRog = "" Then
            txtCustomRog.SetFocus
        Else
            txtCustomRogResponse.SetFocus
        End If
        Exit Sub
    End If
    
'   custom heading with missing text
    If Me.obtnCustomRog And Me.txtCustomRog = "" Then
        MsgBox "You must enter a custom heading.", vbInformation
        txtCustomRog.SetFocus
        Exit Sub
    ElseIf Me.obtnCustomRogResponse And Me.txtCustomRogResponse = "" Then
        MsgBox "You must enter a custom response.", vbInformation
        txtCustomRogResponse.SetFocus
        Exit Sub
    End If
    
'   ending number is less than starting number
    If txtHeadingSetRogs <> "" Then
        If Val(Me.txtNumberRog) > Val(Me.txtHeadingSetRogs) Then
            MsgBox "Ending number must be greater than starting number", vbInformation
            txtHeadingSetRogs.SetFocus
            Exit Sub
        ElseIf Me.txtNumberRog = "" Then
            Me.txtNumberRog = "1"
        End If
    End If
    
    Debug.Print Me.lstInterrogatories.Value
    
'   set variables
    On Error Resume Next
    If txtNumberRog <> "" Or txtHeadingSetRogs <> "" Then
        'restart numbering
        With ActiveDocument
            .Variables.Add "iRogReset", "1"
            .Variables("iRogReset") = "1"
        End With
        If txtHeadingSetRogs <> "" Then
            'when macro is rerun, don't change list selection
            With ActiveDocument
                .Variables.Add "bWasHeadingSet", "True"
                .Variables("bWasHeadingSet") = "True"
            End With
        Else
            Dim bResetResponseNumbering As Boolean
            bResetResponseNumbering = _
                ActiveDocument.Variables("bResetResponseNumbering")
            If bResetResponseNumbering Then
                ActiveDocument.Variables("bResetResponseNumbering") = "False"
            Else
                If txtNumberRog <> "" And chkAlternate Then
                    'when macro is rerun, don't clear "start at" text box
                    With ActiveDocument
                        .Variables.Add "bResetResponseNumbering", "True"
                        .Variables("bResetResponseNumbering") = "True"
                    End With
                End If
            End If
        End If
    End If
    On Error GoTo ProcError
    
    Me.Hide
    Screen.MousePointer = vbHourglass
    DoEvents
    Word.Application.ScreenUpdating = False
    If Me.optDirect Then
        bWriteInterrogs
    Else
        SetupStyles
        DoEvents
        bWriteStyInterrogs
    End If
    Application.Activate
    Screen.MousePointer = vbDefault
    Unload Me
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err, "Could not successfully insert interrogatories."
    Exit Sub
End Sub

Private Sub btnUpdate_Click()
    ActiveDocument.Fields.Update
    UpdateDocumentStyle     '9.7.1 - #4123
    Unload Me
End Sub

Private Sub chkAutoNumRog_Click()
    If chkAutoNumRog = False Then
        lblStartNumberAt.Caption = "Enter &Number Here:"
    Else
        lblStartNumberAt.Caption = "Start &Numbering At:"
    End If
End Sub

Private Sub cmdDefaultHelp_Click()
    Dim message, helpheading
    Select Case MultiPage1.Tab
        Case 0      '---first tab
            helpheading = "Discovery Questions and Responses"
            message = "Select a type of Question and Response from the list " & _
                "or type a custom Question and Response label in the " & _
                "textboxes provided.  Once you have selected a type, the macro " & _
                "will alternate between the Question and corresponding Response " & _
                "each time it is run." & vbCr & vbCr & _
                "Headings will be numbered automatically using field codes " & _
                "unless the ""Use Automatic Numbering"" checkbox is unselected, " & _
                "in which case the numbers will be inserted as text.  A range of " & _
                "paired questions and responses can be inserted at one time by " & _
                "entering a starting and ending number in the boxes provided." & vbCr & vbCr & _
                "The Update Field Codes button updates/renumbers the automatic numbering " & _
                "of existing Discovery Questions and Responses in a document. " & _
                "Use this feature when a discovery list using automatic numbering " & _
                "has been edited to change the order and the numbering needs to be reset. " & _
                "This button will also save changes to existing document styles."
        Case 1      '---Styles tab
            helpheading = "Setting Formatting Options for Discovery Headings using Styles"
            message = "Select heading style to edit." + String$(2, 10) + _
            "Save settings for each style or reset formatting to firm defaults."
        Case 2      '---Direct Formatting tab
            helpheading = "Setting Direct Formatting Options for Discovery Headings"
            message = "Select the character formatting of the discovery " & _
            "headings using the checkboxes at the top." + String$(2, 10) + _
            "Select option to insert the heading on a line by itself with the " + _
            "discovery text following in a separate paragraph, or use the run-in " & _
            "option to have the text following immediately after the heading on the same line.  " & _
            "Indents for headings and following paragraphs can be set using the controls provided."
        End Select
    
    Call HelpBalloon(helpheading, message)
End Sub

Private Sub lstInterrogatories_Click()
    obtnCustomRog = 0
    obtnCustomRogResponse = 0
    EnableRepeatPrevious

End Sub

Private Sub lstInterrogatories_KeyPress(KeyAscii As MSForms.ReturnInteger)
    If KeyAscii = 13 Then _
        btnFinish_Click
End Sub

Private Sub obtnStyRunIn_Click()
    bCBXList Me.cmbStyStyles, m_xRunInStyles()
    Me.cmbStyStyles.ListIndex = 0
End Sub

Private Sub obtnStySep_Click()
    bCBXList Me.cmbStyStyles, m_xNonRunInStyles()
    Me.cmbStyStyles.ListIndex = 0
End Sub

Private Sub obtnCustomRogResponse_Click()
    'Me.chkQuestionBeforeResponse = 1
    If obtnCustomRogResponse = True Then
        lstInterrogatories.ListIndex = -1
    End If
'    Me.txtCustomRogResponse.SetFocus
    EnableRepeatPrevious

End Sub

Private Sub obtnCustomRog_Click()
    'Me.chkQuestionBeforeResponse = 1
    If obtnCustomRog = True Then _
        lstInterrogatories.ListIndex = -1
    EnableRepeatPrevious
End Sub

Private Sub txtCustomRog_KeyDown(KeyCode As Integer, Shift As Integer)
    obtnCustomRog.Value = 1
End Sub
Private Sub txtCustomRog_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    obtnCustomRog.Value = 1
End Sub


Private Sub txtCustomRogResponse_KeyPress(KeyAscii As Integer)
    If bCustomDefined Then
        obtnCustomRogResponse = 1
    End If
    
    If KeyAscii = 13 Then _
        btnFinish_Click

End Sub

Private Sub txtCustomRogResponse_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    obtnCustomRogResponse = 1
End Sub

Private Sub Form_Activate()
    If Me.obtnCustomRog Then
        Me.txtCustomRog.SetFocus
    ElseIf Me.obtnCustomRogResponse Then
        Me.txtCustomRogResponse.SetFocus
    End If
    
    SetInterfaceControls
    
    If bDocIsRestarted(ActiveDocument) Then
        '9.7.1 - #4122
        If Me.obtnStySep.Value = True Then
            obtnStySep_Click
        Else
            obtnStyRunIn_Click
        End If
    Else
        Me.obtnStySep.Value = True
        If Not m_bIsFormatOnly Then
            Me.optStyles = True
        End If
        obtnStySep_Click
    End If
    
    bFormInit = False

    Me.chkRepeatPrevious.Enabled = False
End Sub

Private Sub Form_Load()
    Dim iRetry As Integer
    Dim i As Integer
    Dim iListCount As Integer
    Dim iListFields As Integer
    Dim xInterrogs() As String
    Dim bCustom As Boolean
    Dim bWasHeadingSet As Boolean
    Dim bResetResponseNumbering As Boolean
    Dim xBSIndent As String
    
    Me.m_bFinished = False
    GetTabOrder Me, vbControls()
    DisableInactiveTabs Me
    
    GetRogsLists
    
    bFormInit = True
    
'   get non-dbox control variables
    On Error Resume Next
    With ActiveDocument
        bWasHeadingSet = .Variables("bWasHeadingSet")
        bResetResponseNumbering = .Variables("bResetResponseNumbering")
    End With
    
'---reload global arrays if link
'---dropped for any reason
    On Error GoTo ReInitialize

    m_bIsFormatOnly = UCase(mpbase2.GetMacPacIni("Interrogs", "DirectFormatOnly")) = "TRUE"

'---get template specific lists
    
    bRet = bGetRogTitles(xInterrogs())


'---assign arrays to list boxes
    With Me
        bLSTList .lstInterrogatories, xInterrogs()

'---assign initial values
        If .lstInterrogatories.ListCount Then .lstInterrogatories.ListIndex = 0

'---use Word measurement unit for spinners
        With Word.Options
            Me.spnStyHLeftMargin.DisplayUnit = .MeasurementUnit
            Me.spnLeftMargin.DisplayUnit = .MeasurementUnit
            Me.spnHLeftMargin.DisplayUnit = .MeasurementUnit
        End With
    End With
        
100:
    On Error Resume Next

'---restart options
    If bDocIsRestarted(ActiveDocument) Then

'--- initialize control values
        GetDocVars Me
                
        Debug.Print "frmInterrogs.Load"
                
'--- toggle list choice
        Dim iIndex As Integer
        iIndex = lstInterrogatories.ListIndex
        Debug.Print "lstinterrogs.listindex=" & iIndex
        With ActiveDocument
            If .Variables("obtnCustomRog") = "True" Then bCustom = True
            If .Variables("obtnCustomRogResponse") = "True" Then bCustom = True
        End With
        
        Err.Clear
        
        If Not bResetResponseNumbering Then _
            txtNumberRog = ""
        txtHeadingSetRogs = ""
        
        If bCustom = False Then
            If chkAlternate And Not bWasHeadingSet Then
                bRet = bIsEven(iIndex + 1)
                If bRet = True Then
                    lstInterrogatories.ListIndex = iIndex - 1
                Else
                    lstInterrogatories.ListIndex = iIndex + 1
                End If
            End If
            obtnCustomRog = False
            obtnCustomRogResponse = False
        Else
            If chkAlternate And Not bWasHeadingSet Then
                With ActiveDocument
                    If .Variables("obtnCustomRog") Then
                        If Err <> 5825 Then
                            obtnCustomRog = False
                            obtnCustomRogResponse = True
                        End If
                        Err = 0
                    ElseIf .Variables("obtnCustomRogResponse") = "True" Then
                        If Err <> 5825 Then
                            obtnCustomRog = True
                            obtnCustomRogResponse = False
                        End If
                        Err = 0
                    End If
                End With
            End If
        End If      'bCustom
   Else
        obtnBSSep_Click
        
'       default to default Body Text indent as set in the MacPac.ini
        xBSIndent = mpbase2.GetMacPacIniNumeric("Interrogs", "BodyTextIndent")

        If xBSIndent = "" Then xBSIndent = "0.5"
        
        Me.spnHLeftMargin.Value = xBSIndent
        Me.spnStyHLeftMargin.Value = xBSIndent
        
   End If      'bDocIsRestarted
    
    If Me.txtCustomRog <> "" And Me.txtCustomRogResponse <> "" Then _
            bCustomDefined = True
            
'   set non-dbox control variables
    On Error Resume Next
    With ActiveDocument
        .Variables.Add "bWasHeadingSet", "False"
        .Variables("bWasHeadingSet") = "False"
    End With
        
    MultiPage1.Tab = 0
    
    If Not bDocIsRestarted(ActiveDocument) Then
        cmbStyStyles_Change
    End If
    Exit Sub

ReInitialize:
    Select Case Err
        Case Else
            Beep
            MsgBox "Error " & Str(Err) & vbCr & _
                Error(Err), vbExclamation
        
    End Select

End Sub

Private Sub Form_Unload(Cancel As Integer)
'    Set frmInterrogs = Nothing
End Sub

Private Sub GetRogsLists()
        
    On Error GoTo ProcError

    ReDim m_xNonRunInStyles(3)
    m_xNonRunInStyles(0) = GetStyleName(mpRogStyles_QuestionHeading)
    m_xNonRunInStyles(1) = GetStyleName(mpRogStyles_QuestionText)
    m_xNonRunInStyles(2) = GetStyleName(mpRogStyles_ResponseHeading)
    m_xNonRunInStyles(3) = GetStyleName(mpRogStyles_ResponseText)
    
    ReDim m_xRunInStyles(1)
    m_xRunInStyles(0) = GetStyleName(mpRogStyles_RuninQuestion)
    m_xRunInStyles(1) = GetStyleName(mpRogStyles_RuninResponse)

    ReDim m_xStyles(6)
    m_xStyles(0) = m_xNonRunInStyles(0)
    m_xStyles(1) = m_xNonRunInStyles(1)
    m_xStyles(2) = m_xNonRunInStyles(2)
    m_xStyles(3) = m_xNonRunInStyles(3)
    m_xStyles(4) = m_xRunInStyles(0)
    m_xStyles(5) = m_xRunInStyles(1)
    
ProcError:
End Sub

'9.7.1 - #4123
Private Sub UpdateDocumentStyle()
    Dim oStyle As Word.Style
    Dim oStyNormal As Word.Style
    Dim i As Integer
    Dim s As Single
    Dim xLineSpacing As String
    Dim xFirstLineIndent As String
    Dim xSpaceBefore As String
    Dim xSpaceAfter As String
    Dim xBold As String
    Dim xUnderline As String
    Dim xAllCaps As String
    Dim xSmallCaps As String
        
    On Error GoTo ProcError
        
    With Me.cmbStyStyles
        If .Text <> "" Then
            Set oStyNormal = ActiveDocument.Styles(wdStyleNormal)
            
            On Error Resume Next
            Set oStyle = ActiveDocument.Styles(.Text)
            On Error GoTo ProcError
             
             If oStyle Is Nothing Then
                 Set oStyle = ActiveDocument.Styles.Add(.Text, wdStyleTypeParagraph)
             End If
             
             With oStyle
                .BaseStyle = wdStyleNormal
                 
                If Me.optStyDouble Then
                    If oStyNormal.ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
                        .ParagraphFormat.LineSpacing = 24
                    Else
                        .ParagraphFormat.LineSpacingRule = wdLineSpaceDouble
                    End If
                Else
                    If oStyNormal.ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
                        .ParagraphFormat.LineSpacing = 12
                    Else
                        .ParagraphFormat.LineSpacingRule = wdLineSpaceSingle
                    End If
                End If
                    
                On Error Resume Next
                
                With .ParagraphFormat
                    xFirstLineIndent = Me.spnStyHLeftMargin.Value
                    If xFirstLineIndent <> "" Then
                        .FirstLineIndent = InchesToPoints(Me.spnStyHLeftMargin.Value)
                    Else
                        .FirstLineIndent = InchesToPoints(GetValue(oStyle.NameLocal, "fldFirstLineIndent"))
                    End If
                    
                    xSpaceAfter = Me.spnStySAfter.Value
                    If xSpaceAfter <> "" Then
                        .SpaceAfter = Me.spnStySAfter.Value
                    Else
                        .SpaceAfter = GetValue(oStyle.NameLocal, "fldSpaceAfter")
                    End If
                   
                    xSpaceBefore = Me.spnStySBefore.Value
                    If xSpaceBefore <> "" Then
                        .SpaceBefore = Me.spnStySBefore.Value
                    Else
                        .SpaceBefore = GetValue(oStyle.NameLocal, "fldSpaceBefore")
                    End If
                End With
                    
                For i = 0 To UBound(m_xStyles) - 1
                    If m_xStyles(i) = .NameLocal Then
                        Select Case i
                           Case mpRogStyles_QuestionText, mpRogStyles_ResponseText
                                .ParagraphFormat.KeepTogether = False
                                .ParagraphFormat.KeepWithNext = False
                                .NextParagraphStyle = m_xStyles(i)
                           Case Else
                                .NextParagraphStyle = wdStyleBodyText
                                .ParagraphFormat.KeepTogether = True
                                .ParagraphFormat.KeepWithNext = True
                               
                                .Font.Bold = CBool(Me.chkStyBold)
                               
                                If CBool(Me.chkStyUnderlined) Then
                                   .Font.Underline = wdUnderlineSingle
                                Else
                                   .Font.Underline = wdUnderlineNone
                                End If
                                
                                .Font.AllCaps = CBool(Me.chkStyAllCaps)
                                .Font.SmallCaps = CBool(Me.chkStySmallCaps)
                                   
                        End Select
                        Exit For
                    End If
                Next i
                
            End With
        End If
    End With
        
    Word.Application.ScreenUpdating = True
    Word.Application.ScreenRefresh

    
    Exit Sub
ProcError:
    MsgBox Error(Err), vbExclamation
    Exit Sub
End Sub


Private Sub SetupStyles()
    Dim oStyle As Word.Style
    Dim oStyNormal As Word.Style
    Dim i As Integer
    Dim s As Single
        
    Set oStyNormal = ActiveDocument.Styles(wdStyleNormal)
    
    For i = 0 To UBound(m_xStyles) - 1
         On Error Resume Next
         Set oStyle = Nothing
         Set oStyle = ActiveDocument.Styles(m_xStyles(i))
         On Error GoTo ProcError
         
         If oStyle Is Nothing Then
             Set oStyle = ActiveDocument.Styles.Add(m_xStyles(i), wdStyleTypeParagraph)
         End If
         With oStyle
             .BaseStyle = wdStyleNormal
             
                Dim xLineSpacing As String
                Dim xFirstLineIndent As String
                Dim xSpaceBefore As String
                Dim xSpaceAfter As String
                Dim xBold As String
                Dim xUnderline As String
                Dim xAllCaps As String
                Dim xSmallCaps As String
                
                xLineSpacing = mpbase2.GetUserIni(.NameLocal, "LineSpacing")
                If xLineSpacing = "" Then
                    xLineSpacing = GetValue(.NameLocal, "fldLineSpacing")
                End If

                If UCase(xLineSpacing) = "DOUBLE" Then
                    If oStyNormal.ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
                        .ParagraphFormat.LineSpacing = 24
                    Else
                        .ParagraphFormat.LineSpacingRule = wdLineSpaceDouble
                    End If
                Else
                    If oStyNormal.ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
                        .ParagraphFormat.LineSpacing = 12
                    Else
                        .ParagraphFormat.LineSpacingRule = wdLineSpaceSingle
                    End If
                End If
                
                
                On Error Resume Next
                With .ParagraphFormat
                    xFirstLineIndent = mpbase2.GetUserIniNumeric(oStyle.NameLocal, "FirstLineIndent")
                    If xFirstLineIndent <> "" Then
                        .FirstLineIndent = InchesToPoints(mpbase2.GetUserIniNumeric(oStyle.NameLocal, "FirstLineIndent"))
                    Else
                        .FirstLineIndent = InchesToPoints(GetValue(oStyle.NameLocal, "fldFirstLineIndent"))
                    End If
                    
                    xSpaceAfter = mpbase2.GetUserIni(oStyle.NameLocal, "SpaceAfter")
                    If xSpaceAfter <> "" Then
                        .SpaceAfter = mpbase2.GetUserIni(oStyle.NameLocal, "SpaceAfter")
                    Else
                        .SpaceAfter = GetValue(oStyle.NameLocal, "fldSpaceAfter")
                    End If
                   
                    xSpaceBefore = mpbase2.GetUserIni(oStyle.NameLocal, "SpaceBefore")
                    If xSpaceBefore <> "" Then
                        .SpaceBefore = mpbase2.GetUserIni(oStyle.NameLocal, "SpaceBefore")
                    Else
                        .SpaceBefore = GetValue(oStyle.NameLocal, "fldSpaceBefore")
                    End If
                End With
                
                Select Case i    'm_xStyles(i)
                   Case mpRogStyles_QuestionText, mpRogStyles_ResponseText
                        .ParagraphFormat.KeepTogether = False
                        .ParagraphFormat.KeepWithNext = False
                        .NextParagraphStyle = m_xStyles(i)
                   Case Else
                        .NextParagraphStyle = wdStyleBodyText
                        .ParagraphFormat.KeepTogether = True
                        .ParagraphFormat.KeepWithNext = True
                       
                        xBold = mpbase2.GetUserIni(.NameLocal, "Bold")
                        If xBold <> "" Then
                            .Font.Bold = CBool(mpbase2.GetUserIni(.NameLocal, "Bold"))
                        Else
                            .Font.Bold = CBool(GetValue(.NameLocal, "fldBold"))
                        End If
                       
                        xUnderline = mpbase2.GetUserIni(.NameLocal, "Underline")
                        If xUnderline <> "" Then
                            If mpbase2.GetUserIni(.NameLocal, "Underline") = "True" Then
                               .Font.Underline = wdUnderlineSingle
                            Else
                               .Font.Underline = wdUnderlineNone
                            End If
                        Else
                            If CBool(GetValue(.NameLocal, "fldUnderline")) Then
                               .Font.Underline = wdUnderlineSingle
                            Else
                               .Font.Underline = wdUnderlineNone
                            End If
                        End If
                        
                        xAllCaps = mpbase2.GetUserIni(.NameLocal, "AllCaps")
                        If xAllCaps <> "" Then
                            .Font.AllCaps = CBool(mpbase2.GetUserIni(.NameLocal, "AllCaps"))
                        Else
                            .Font.AllCaps = CBool(GetValue(.NameLocal, "fldAllCaps"))
                        End If
                       
                        xSmallCaps = mpbase2.GetUserIni(.NameLocal, "SmallCaps")
                        If xSmallCaps <> "" Then
                            .Font.SmallCaps = CBool(mpbase2.GetUserIni(.NameLocal, "SmallCaps"))
                        Else
                            .Font.SmallCaps = CBool(GetValue(.NameLocal, "fldSmallCaps"))
                        End If
                       
                End Select
'            End If  'active style
        End With
    Next i
    
    Exit Sub
ProcError:
    MsgBox Error(Err), vbExclamation
    Exit Sub
End Sub

Private Sub GetIniSettings(Optional bForceUpdate As Boolean = False)
    Dim xFirstLineIndent As String
    Dim xSpaceAfter As String
    Dim xSpaceBefore As String
    Dim xBold As String
    Dim xUnderline As String
    Dim xAllCaps As String
    Dim xSmallCaps As String
    Dim xLineSpacing As String
    
    If bFormInit Or bForceUpdate Then
        With Me.cmbStyStyles
        
            If .Text <> "" Then
        
                xBold = mpbase2.GetUserIni(.Text, "Bold")
                If xBold = "" Then
                    Me.chkStyBold.Value = Abs(CInt(GetValue(.Value, "fldBold")))
                Else
                    If UCase(xBold) = "TRUE" Then
                        Me.chkStyBold = 1
                    Else
                        Me.chkStyBold = 0
                    End If
                End If
                xUnderline = mpbase2.GetUserIni(.Text, "Underline")
                If xUnderline = "" Then
                    Me.chkStyUnderlined = Abs(CInt(GetValue(.Value, "fldUnderline")))
                Else
                    If UCase(xUnderline) = "TRUE" Then
                        Me.chkStyUnderlined = 1
                    Else
                        Me.chkStyUnderlined = 0
                    End If
                End If
                
                xAllCaps = mpbase2.GetUserIni(.Text, "AllCaps")
                If xAllCaps = "" Then
                    Me.chkStyAllCaps = Abs(CInt(GetValue(.Value, "fldAllCaps")))
                Else
                    If UCase(xAllCaps) = "TRUE" Then
                        Me.chkStyAllCaps = 1
                    Else
                        Me.chkStyAllCaps = 0
                    End If
                End If
                
                xSmallCaps = mpbase2.GetUserIni(.Text, "SmallCaps")
                If xSmallCaps = "" Then
                    Me.chkStySmallCaps = Abs(CInt(GetValue(.Value, "fldSmallCaps")))
                Else
                    If UCase(xSmallCaps) = "TRUE" Then
                        Me.chkStySmallCaps = 1
                    Else
                        Me.chkStySmallCaps = 0
                    End If
                End If
                xFirstLineIndent = mpbase2.GetUserIniNumeric(.Text, "FirstLineIndent")
                If Not (xFirstLineIndent = "") Then
                    Me.spnStyHLeftMargin.Value = xFirstLineIndent
                Else
                    Me.spnStyHLeftMargin.Value = GetValue(.Text, "fldFirstLineIndent")
                End If
                
                xSpaceBefore = mpbase2.GetUserIniNumeric(.Text, "SpaceBefore")
                If Not (xSpaceBefore = "") Then
                    Me.spnStySBefore.Value = xSpaceBefore
                Else
                    Me.spnStySBefore.Value = GetValue(.Text, "fldSpaceBefore")
                End If
                
                xSpaceAfter = mpbase2.GetUserIniNumeric(.Text, "SpaceAfter")
                If Not (xSpaceAfter = "") Then
                    Me.spnStySAfter.Value = xSpaceAfter
                Else
                    Me.spnStySAfter.Value = GetValue(.Text, "fldSpaceAfter")
                End If
                
                xLineSpacing = mpbase2.GetUserIni(.Text, "LineSpacing")
                If xLineSpacing = "" Then
                    xLineSpacing = GetValue(.Value, "fldLineSpacing")
                End If
                
                If UCase(xLineSpacing) = "DOUBLE" Then
                    Me.optStyDouble = True
                Else
                    Me.optStySingle = True
                End If
            End If
        End With
        Me.btnStySave.Enabled = False
    End If
    
End Sub

Private Sub SetIniSettings()
    With Me.cmbStyStyles
        If .Text <> "" Then
            If Me.optStyDouble Then
                mpbase2.SetUserIni .Text, "LineSpacing", "Double"
            Else
                mpbase2.SetUserIni .Text, "LineSpacing", "Single"
            End If
            mpbase2.SetUserIni .Text, "FirstLineIndent", Me.spnStyHLeftMargin.Value
            mpbase2.SetUserIni .Text, "SpaceAfter", Me.spnStySAfter.Value
            mpbase2.SetUserIni .Text, "SpaceBefore", Me.spnStySBefore.Value
            mpbase2.SetUserIni .Text, "Bold", CBool(Me.chkStyBold)
            mpbase2.SetUserIni .Text, "Underline", CBool(Me.chkStyUnderlined)
            mpbase2.SetUserIni .Text, "AllCaps", CBool(Me.chkStyAllCaps)
            mpbase2.SetUserIni .Text, "SmallCaps", CBool(Me.chkStySmallCaps)
        End If
    End With
End Sub

Private Sub SetInterfaceControls()

    If m_bIsFormatOnly Then
        Me.fraStyDirect.Visible = False
        Me.MultiPage1.TabCaption(2) = "&2-Format"
        Me.optStyles.Visible = False
        Me.optDirect.Visible = False
        Me.MultiPage1.TabVisible(1) = False
        Me.lblType.Top = Me.lblType.Top - 50
        Me.lstInterrogatories.Top = Me.lstInterrogatories.Top - 300
        Me.lblPositionMessage.Top = Me.lblPositionMessage.Top - 300
        Me.lblType.Top = Me.lblType.Top - 300
        Me.lblQuestion.Top = Me.lblQuestion.Top - 300
        Me.lblResponse.Top = Me.lblResponse.Top - 300
        Me.obtnCustomRog.Top = Me.obtnCustomRog.Top - 300
        Me.obtnCustomRogResponse.Top = Me.obtnCustomRogResponse.Top - 300
        Me.txtCustomRog.Top = Me.txtCustomRog.Top - 300
        Me.txtCustomRogResponse.Top = Me.txtCustomRogResponse.Top - 300
        Me.chkAlternate.Top = Me.chkAlternate.Top - 300
        Me.btnClear.Top = Me.btnClear.Top - 300
        Me.chkAutoNumRog.Top = Me.chkAutoNumRog.Top - 300
        Me.lblStartNumberAt.Top = Me.lblStartNumberAt.Top - 300
        Me.txtHeadingSetRogs.Top = Me.txtHeadingSetRogs.Top - 300
        Me.lblSet.Top = Me.lblSet.Top - 300
        Me.txtNumberRog.Top = Me.txtNumberRog.Top - 300
        Me.btnBSSetClear.Top = Me.btnBSSetClear.Top - 300
        Me.chkRepeatPrevious.Top = Me.chkRepeatPrevious.Top - 300
        
    Else
'        Me.optStyles = True
    End If

End Sub

Private Sub EnableTabs()
    Me.MultiPage1.TabEnabled(2) = (optStyles.Value = 0)
    Me.MultiPage1.TabEnabled(1) = (optDirect.Value = 0)
End Sub

Private Sub EnableRepeatPrevious()
    Me.chkRepeatPrevious.Enabled = obtnCustomRogResponse
    If Me.obtnCustomRogResponse = False Then
        Me.chkRepeatPrevious = 0
    End If
End Sub

Private Function xGetPreviousSeq() As String
    Dim oRange As Word.Range
    Dim bShowFieldCodes As Boolean
    Dim xTemp As String
    Dim iPos As Integer
    
    bShowFieldCodes = Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes

    Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = True
    
    Set oRange = ActiveDocument.Range
    
    With oRange.Find
        .ClearFormatting
        .Text = "^d"
        .Forward = False
        .Execute
        If .Found Then
            xTemp = Mid(oRange.Fields(1).Code.Text, 5)
            iPos = InStr(1, xTemp, "\")
            If iPos Then
                xTemp = Left(xTemp, iPos - 1)
            End If
            xTemp = mpbase2.xTrimTrailingChrs(xTemp, " ")
        End If
    
    End With
    
    Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = bShowFieldCodes
    
    xGetPreviousSeq = xTemp
    
End Function

Private Function PromptToSave() As Boolean
    Dim xMsg As String
    Dim iChoice As Integer
    
    On Error GoTo ProcError
    xMsg = "Save current entry?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        btnStySave_Click
        '---9.6.2
        PromptToSave = True
    End If
    Exit Function
ProcError:
    g_oError.Raise "mpRogs.frmInterrogs.PromptToSave"
    Exit Function
End Function

