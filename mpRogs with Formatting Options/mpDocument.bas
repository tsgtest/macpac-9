Attribute VB_Name = "mpDocument"
Function bDocIsRestarted(docDoc As Word.Document) As Boolean
'returns true for documents
'whose macros have been run already
    On Error GoTo DocIsRestarted_Error

    bDocIsRestarted = docDoc.Variables("Restarted")
    Exit Function

DocIsRestarted_Error:
    bDocIsRestarted = False
    Exit Function
End Function
Function mpMax(i As Double, j As Double) As Double
    If i > j Then
        mpMax = i
    Else
        mpMax = j
    End If
End Function

Function mpMin(i As Double, j As Double) As Double
    If i > j Then
        mpMin = j
    Else
        mpMin = i
    End If
End Function

Public Function GetStyleName(InterrogStyleType As mpRogStyles) As String
    Dim oDB As DAO.Database
    Dim oRS As DAO.Recordset
    Dim xSQL As String

'  get db object
    Set oDB = DBEngine(0).OpenDatabase(mpBase2.PublicDB)

    xSQL = "SELECT * FROM tblInterrogatoryStyles WHERE fldType = " & InterrogStyleType
           
'   open the rs
    Set oRS = oDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
    If oRS.RecordCount Then
        GetStyleName = oRS.Fields("fldName").Value
    End If
    
    Set oRS = Nothing
    
    Exit Function

End Function

Public Function GetValue(xStyleName As String, xDefField As String) As Variant
    Dim oDB As DAO.Database
    Dim oRS As DAO.Recordset
    Dim xSQL As String

'  get db object
    Set oDB = DBEngine(0).OpenDatabase(mpBase2.PublicDB)

    xSQL = "SELECT * FROM tblInterrogatoryStyles WHERE fldName = '" & xStyleName & "'"
           
'   open the rs
    Set oRS = oDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
    If oRS.RecordCount Then
        GetValue = oRS.Fields(xDefField).Value
    End If
    
    Set oRS = Nothing
    
    Exit Function
End Function

Function bGetRogTitles(xlistarray() As String) As Boolean
    Dim oDB As DAO.Database
    Dim oRS As DAO.Recordset
    Dim xSQL As String
    Dim xName As String
    Dim xSeq As String
    Dim iType As Integer
    Dim xTemp As String
    
'  get db object
    Set oDB = DBEngine(0).OpenDatabase(mpBase2.PublicDB)

    xSQL = "SELECT * FROM tblInterrogatories ORDER BY fldID"
           
'   open the rs
    Set oRS = oDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
    If oRS.RecordCount Then
        With oRS
            Dim iNumFields As Integer
            iNumFields = .Fields.Count
'           fill listitems collection if recordset is not empty
            If Not (.BOF And .EOF) Then
                While Not .EOF
                    On Error Resume Next
                    xSeq = .Fields("fldSequence")
                    xName = .Fields("fldName")
                    iType = CInt(.Fields("fldType"))
                    On Error GoTo 0

'                   add record to string - need 2 columns for name - buggy otherwise
                    xString = xString & xName & "," & xName & "," & xSeq & "," & iType & ","
                    .MoveNext
                Wend
            End If
            .Close
'            mpbase2.xStringToArray xlistarray, 3
            mpbase2.xStringToArray mpbase2.xTrimTrailingChrs(xString, ","), xlistarray(), 4
        End With
    End If
    
End Function


