VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CInterrogatories"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Sub Insert()
    Dim frmInter As frmInterrogs
    
    On Error GoTo ProcError
    
    Set frmInter = New frmInterrogs
    
    frmInter.Show vbModal
    
    Unload frmInter
    
    Set frmInter = Nothing
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not successfully insert Discovery Questions and Responses"
End Sub

