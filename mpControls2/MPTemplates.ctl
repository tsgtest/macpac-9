VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.UserControl MPTemplates 
   ClientHeight    =   4500
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7788
   KeyPreview      =   -1  'True
   ScaleHeight     =   4500
   ScaleWidth      =   7788
   ToolboxBitmap   =   "MPTemplates.ctx":0000
   Begin VB.CommandButton btnResetTemplatesView 
      Height          =   360
      Left            =   7335
      Picture         =   "MPTemplates.ctx":00FA
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Expand Templates' Width (Ctrl+W)"
      Top             =   1380
      Width           =   425
   End
   Begin MSComctlLib.ListView lstTemplates 
      Height          =   4260
      Left            =   2310
      TabIndex        =   3
      Top             =   240
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   7514
      Arrange         =   2
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      PictureAlignment=   4
      _Version        =   393217
      Icons           =   "ilNormal"
      SmallIcons      =   "ilSmall"
      ForeColor       =   -2147483640
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.CheckBox chkFind 
      Height          =   360
      Left            =   7335
      Picture         =   "MPTemplates.ctx":0304
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Find Templates (Alt+F)"
      Top             =   990
      Width           =   425
   End
   Begin VB.CommandButton btnViewIcons 
      Height          =   360
      Left            =   7335
      Picture         =   "MPTemplates.ctx":0406
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Show Templates with Large Icons (Alt+I)"
      Top             =   240
      Width           =   425
   End
   Begin VB.CommandButton btnViewDescriptions 
      Height          =   360
      Left            =   7335
      Picture         =   "MPTemplates.ctx":04F8
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Show Templates with Description (Alt+D)"
      Top             =   615
      Width           =   425
   End
   Begin MSComctlLib.ImageList ilNormal 
      Left            =   7170
      Top             =   1815
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":05EA
            Key             =   "Winword Normal"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":0904
            Key             =   "Excel Normal"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":0C1E
            Key             =   "Powerpoint Normal"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":0F38
            Key             =   "Other Normal"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ilSmall 
      Left            =   7170
      Top             =   2415
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":1252
            Key             =   "Winword Small"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":13AC
            Key             =   "Excel Small"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":1506
            Key             =   "Powerpoint Small"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MPTemplates.ctx":1660
            Key             =   "Other Small"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwCategories 
      Height          =   4260
      Left            =   0
      TabIndex        =   1
      Top             =   240
      Width           =   2265
      _ExtentX        =   4001
      _ExtentY        =   7514
      _Version        =   393217
      Indentation     =   441
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Checkboxes      =   -1  'True
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblTemplates 
      BackStyle       =   0  'Transparent
      Caption         =   "&Templates:"
      Height          =   255
      Left            =   2370
      TabIndex        =   2
      Top             =   15
      Width           =   1170
   End
   Begin VB.Label lblGroups 
      BackStyle       =   0  'Transparent
      Caption         =   "&Groups:"
      Height          =   255
      Left            =   30
      TabIndex        =   0
      Top             =   15
      Width           =   645
   End
   Begin VB.Menu mnuTemplates 
      Caption         =   "Templates"
      Begin VB.Menu mnuTemplates_ViewDescriptions 
         Caption         =   "Show &Descriptions"
      End
      Begin VB.Menu mnuTemplates_ViewIcons 
         Caption         =   "Show Large &Icons"
      End
      Begin VB.Menu mnuTemplates_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuTemplates_AddFavorite 
         Caption         =   "Add Fa&vorite"
      End
      Begin VB.Menu mnuTemplates_DeleteFavorite 
         Caption         =   "Delete Fa&vorite"
      End
   End
End
Attribute VB_Name = "MPTemplates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Enum mpItemSources
    mpItemSource_Templates = 1
    mpItemSource_Segments = 2
End Enum

Enum mpTemplateGroupCategories
    mpTemplateGroupCategory_Favorites = 1
    mpTemplateGroupCategory_Groups = 2
End Enum

Private m_bGroupsNodeExpanded As Boolean

Private m_bItemClicked As Boolean
Private m_oDB As mpDB.CDatabase
Private m_oError As mpError.CError
Private m_iCurView As MSComctlLib.ListViewConstants
Private m_iItemSource As mpItemSources
Private m_xIniSecPrefix As String
Private m_bInFindMode As Boolean 'if TRUE, the form is in 'FIND' mode
Private m_oPreFindSelNode As MSComctlLib.node

Public Event Selection(ByVal SelID As String)
Public Event SelDoubleClick(ByVal SelID As String)
Public Event GroupCategoryChange(ByVal iCategory As mpTemplateGroupCategories)

Public Property Set DBObj(oNew As mpDB.CDatabase)
    Set m_oDB = oNew


    If Me.ItemSource = mpItemSource_Segments Then
        InitializeSegments
    Else
        InitializeTemplates
    End If

End Property

Public Property Let ItemSource(iNew As mpItemSources)
    On Error GoTo ProcError
    If m_iItemSource <> iNew Then
'       store in private var
        m_iItemSource = iNew

        If Me.ItemSource = mpItemSource_Segments Then
            m_xIniSecPrefix = "Segment"
        Else
            m_xIniSecPrefix = "Template"
        End If
        
'       reset controls to show specified source
        SetItemSource iNew
    End If
    Exit Property
ProcError:
    m_oError.Raise "mpControls2.MPTemplates.ItemSource"
    Exit Property
End Property

Public Property Get ItemSource() As mpItemSources
    ItemSource = m_iItemSource
End Property

Private Sub SetItemSource(iSource As mpItemSources)
'sets the item source according to ItemSource Prop
    On Error GoTo ProcError
    With UserControl
        .lstTemplates.ListItems.Clear
        .tvwCategories.Nodes.Clear
        .mnuTemplates_AddFavorite.Visible = (iSource = mpItemSource_Templates)
        .mnuTemplates_DeleteFavorite.Visible = (iSource = mpItemSource_Templates)
        .mnuTemplates_Sep1.Visible = (iSource = mpItemSource_Templates)
    End With
    
'    If iSource = mpItemSource_Segments Then
'        InitializeSegments
'    Else
'        InitializeTemplates
'    End If
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.SetItemSource"
    Exit Sub
End Sub

Public Property Get CurrentView() As MSComctlLib.ListViewConstants
    CurrentView = UserControl.lstTemplates.View
End Property

Public Property Get SelectedID() As String
'   return id of current selection - empty
'   string if there is no current selection
    On Error GoTo ProcError
    On Error Resume Next
    SelectedID = lstTemplates.SelectedItem.Key
    Exit Property
ProcError:
    m_oError.Raise "mpControls2.MPTemplates.SelectedID"
    Exit Property
End Property

Private Function FindTemplates() As Boolean
'displays "Find" form and finds templates with specified text -
'returns TRUE if in FIND mode, else FALSE

    Dim oForm As frmFind
    Set oForm = New frmFind
    
'   show form
    With oForm
        .Show vbModal
    
        If Not .Cancelled Then
            Dim oTDef As mpDB.CTemplateDefs
            UserControl.lstTemplates.ListItems.Clear
            ShowTemplates , .ItemName, _
                            .SearchNameColumn, _
                            .SearchDescriptionColumn
        End If
    End With
    
    With UserControl
        If oForm.Cancelled Then
'           reset dialog to non-find mode
            m_bInFindMode = False
            .chkFind.Value = vbUnchecked
            .tvwCategories.Enabled = True
            .lblGroups.Enabled = True
            If .lstTemplates.ListItems.Count Then
                .lstTemplates.SetFocus
                .lstTemplates.SelectedItem = .lstTemplates.ListItems(1)
            End If
        Else
'           find templates
            m_bInFindMode = True
            .chkFind.Value = vbChecked
            .tvwCategories.Enabled = False
            .lblGroups.Enabled = False
            
'           collapse groups node
            If Not Me.GroupsNode Is Nothing Then
                Me.GroupsNode.Checked = vbUnchecked
                m_bGroupsNodeExpanded = Me.GroupsNode.Expanded
                Me.GroupsNode.Expanded = False
            End If
            
            If Not Me.FavoritesNode Is Nothing Then
                Me.FavoritesNode.Checked = vbUnchecked
            End If
        End If
    End With
    FindTemplates = m_bInFindMode
    Exit Function
ProcError:
    FindTemplates = m_bInFindMode
    m_oError.Raise "mpControls2.MPTemplates.FindTemplates"
    Exit Function
End Function

Private Function FindSegments() As Boolean
'displays "Find" form and finds templates with specified text-
'returns TRUE if in FIND mode, else FALSE
    Dim oForm As frmFind
    Set oForm = New frmFind
    
'   show form
    With oForm
        .Show vbModal
    
        If Not .Cancelled Then
            UserControl.lstTemplates.ListItems.Clear
            ShowSegments , .ItemName, _
                            .SearchNameColumn, _
                            .SearchDescriptionColumn
        End If
    End With
    
    With UserControl
        If oForm.Cancelled Then
'           reset dialog to non-find mode
            m_bInFindMode = False
            .chkFind.Value = vbUnchecked
            .tvwCategories.Enabled = True
            .lblGroups.Enabled = True
        Else
'           find templates
            m_bInFindMode = True
            .chkFind.Value = vbChecked
            .tvwCategories.Enabled = False
            .lblGroups.Enabled = False
            
'           collapse groups node
            If Not Me.GroupsNode Is Nothing Then
                Me.GroupsNode.Checked = vbUnchecked
                Me.GroupsNode.Expanded = False
            End If
        End If
    End With
    
    FindSegments = m_bInFindMode
    Exit Function
    
ProcError:
    FindSegments = m_bInFindMode
    m_oError.Raise "mpControls2.MPTemplates.FindTemplates"
    Exit Function
End Function

Private Sub btnResetTemplatesView_Click()
    On Error GoTo ProcError
    ResizeControls True
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub btnViewIcons_Click()
    On Error GoTo ProcError
    ViewIcons
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub btnViewDescriptions_Click()
    On Error GoTo ProcError
    ViewDescriptions
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub ViewIcons()
    On Error GoTo ProcError
    With UserControl
        .lstTemplates.View = lvwIcon
        .mnuTemplates_ViewDescriptions.Visible = True
        .mnuTemplates_ViewIcons.Visible = False
    End With
    Exit Sub
ProcError:
    With m_oError
        .Source "mpControls2.MPTemplates.ViewIcons", Err.Source
        .Show Err
    End With
    Exit Sub
End Sub

Private Sub ViewDescriptions()
    On Error GoTo ProcError
    With UserControl
        .lstTemplates.View = lvwReport
        .mnuTemplates_ViewIcons.Visible = True
        .mnuTemplates_ViewDescriptions.Visible = False
    End With
    Exit Sub
ProcError:
    With m_oError
        .Source "mpControls2.MPTemplates.ViewDescriptions", Err.Source
        .Show Err
    End With
    Exit Sub
End Sub

Private Sub chkFind_Click()
'enter/exit FIND mode if necessary
    On Error GoTo ProcError
    With UserControl.chkFind
        If .Value = vbChecked And Not m_bInFindMode Then
'           the find button has been checked, and the form
'           is not currently in find mode - put in find mode
            EnterFindMode
        ElseIf .Value = vbUnchecked And m_bInFindMode Then
'           the find button has been unchecked, and the form
'           is currently in find mode - take out of find mode
            ExitFindMode
        End If
    End With
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub ExitFindMode()
'takes the form out of FIND mode
    On Error GoTo ProcError
'   "find" button was unchecked - return to groups
    If Not m_oPreFindSelNode Is Nothing Then
        m_oPreFindSelNode.Checked = True
    End If
    If Not Me.GroupsNode Is Nothing Then
        If Me.GroupsNode.Checked Then
'           show templates in checked groups
            SetTemplatesList Me.GroupsNode
            Me.GroupsNode.Expanded = m_bGroupsNodeExpanded
        Else
'           show favorites
            SetTemplatesList Me.FavoritesNode
        End If
    Else
        UserControl.lstTemplates.ListItems.Clear
        ShowSelectedGroups
    End If
    
    m_bInFindMode = False
    
'   enable groups controls
    With UserControl
        .tvwCategories.Enabled = True
        .lblGroups.Enabled = True
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.ExitFindMode"
    Exit Sub
End Sub

Private Sub EnterFindMode()
'puts the form into FIND mode
    Dim oNode As MSComctlLib.node

    On Error GoTo ProcError
    Set oNode = Me.GroupsNode
    
    If Not oNode Is Nothing Then
'       get node that is currently checked - findTemplates
'       will uncheck both Faves and Groups - when user
'       unchecks Find button, this node should be rechecked
        If oNode.Checked Then
            Set m_oPreFindSelNode = oNode
        Else
            Set m_oPreFindSelNode = Me.FavoritesNode
        End If
    End If
    
'   "find" button was pressed - find templates
    If Me.ItemSource = mpItemSource_Templates Then
        FindTemplates
    Else
        FindSegments
    End If
    
    If m_bInFindMode Then
'       disable groups controls
        With UserControl
            .tvwCategories.Enabled = False
            .lblGroups.Enabled = False
        End With
        
        If UserControl.lstTemplates.ListItems.Count Then
            lstTemplates_ItemClick UserControl.lstTemplates.ListItems(1)
        Else
            Dim xMsg As String
            UserControl.chkFind.Value = vbUnchecked
            xMsg = "No matching items were found."
            MsgBox xMsg, vbExclamation, App.Title
        End If
    End If
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.EnterFindMode"
    Exit Sub
End Sub
Private Sub lstTemplates_DblClick()
    On Error GoTo ProcError
    If m_bItemClicked Then
        m_bItemClicked = False
    End If

    RaiseEvent SelDoubleClick(Me.SelectedID)
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub lstTemplates_ItemClick(ByVal Item As MSComctlLib.ListItem)
    On Error GoTo ProcError
    m_bItemClicked = True

'   broadcast that a selection was made
    RaiseEvent Selection(Me.SelectedID)
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub lstTemplates_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error GoTo ProcError
    If Button = 2 Then
        ShowMenu X, Y
    End If
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Public Sub ShowMenu(X As Single, Y As Single)
    Dim oItem As ListItem

    On Error GoTo ProcError
    With lstTemplates
        On Error Resume Next
        
'       exit if an icon was not clicked
        Set oItem = .HitTest(X, Y)
        If (oItem Is Nothing) Then
            Exit Sub
        End If
        
'       ensure that icon that has been
'       right-clicked on is selected
        oItem.Selected = True
        
'       broadcast that a selection was made
        RaiseEvent Selection(Me.SelectedID)
    End With
    
'   show context-sensitive menu
    UserControl.PopupMenu UserControl.mnuTemplates
    Exit Sub
ProcError:
    m_oError.Raise "mpControls2.MPTemplates.ShowMenu"
    Exit Sub
End Sub

Private Sub mnuTemplates_AddFavorite_Click()
    On Error GoTo ProcError
    AddFavorite Me.SelectedID
    Exit Sub
ProcError:
    m_oError.Show Err, "Could not successfully add to favorites."
    Exit Sub
End Sub

Private Sub mnuTemplates_DeleteFavorite_Click()
    On Error GoTo ProcError
    DeleteFavorite Me.SelectedID
    Exit Sub
ProcError:
    m_oError.Show Err, "Could not successfully remove from favorites."
    Exit Sub
End Sub

Public Sub AddFavorite(ByVal xSelID As String)
    On Error GoTo ProcError
    m_oDB.TemplateDefinitions.AddToFavorites xSelID
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.AddFavorite"
    Exit Sub
End Sub

Public Sub DeleteFavorite(ByVal xSelID As String)
    On Error GoTo ProcError
    With lstTemplates
        If .ListItems.Count Then
            m_oDB.TemplateDefinitions _
                .RemoveFromFavorites xSelID
        
'           remove item from list
            .ListItems.Remove xSelID
            
            If Not .SelectedItem Is Nothing Then
                .SelectedItem.Selected = True
            End If
        End If
    End With
    
'   broadcast that a selection was made
    RaiseEvent Selection(Me.SelectedID)
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.DeleteFavorite"
    Exit Sub
End Sub

Private Sub mnuTemplates_ViewDescriptions_Click()
    ViewDescriptions
End Sub

Private Sub mnuTemplates_ViewIcons_Click()
    On Error GoTo ProcError
    ViewIcons
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub tvwCategories_NodeCheck(ByVal node As MSComctlLib.node)
    Dim oChild As MSComctlLib.node
    
    On Error GoTo ProcError
'benchmark
'Dim l As Long
'l = mpbase2.CurrentTick

'   check/uncheck all children
    If node.Children And node.Key <> "xGroups" Then
        CheckSubNodes node
    End If
    
'   expand node if it's being checked
    If node.Checked Then
        node.Expanded = True
    End If
    
'   we're done checking all children nodes
    SetTemplatesList node

'   set sticky field value
    SetUserIni m_xIniSecPrefix & "Groups", _
                node.Key, _
                node.Checked
                
'MsgBox mpbase2.ElapsedTime(l)
    DoEvents
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub CheckSubNodes(ByVal oNode As MSComctlLib.node)
'checks and expands all children nodes of oNode
    Dim oChild As MSComctlLib.node
    
    Set oChild = oNode.Child
    While Not oChild Is Nothing
        oChild.Checked = oNode.Checked
        If oChild.Checked Then
            oChild.Expanded = True
        End If
        
'       set sticky field value for key
        SetUserIni m_xIniSecPrefix & "Groups", _
                    oChild.Key, _
                    oChild.Checked
                    
        If oChild.Children Then
'           recurse to get to deeper nodes
            CheckSubNodes oChild
        End If
        
        Set oChild = oChild.Next
    Wend
End Sub

Private Sub SetTemplatesList(ByVal node As MSComctlLib.node)
    Static bOnce As Boolean
    Dim oNodes As MSComctlLib.Nodes
    Dim oGroups As MSComctlLib.node
    Dim oFaves As MSComctlLib.node

    On Error GoTo ProcError
'   prevent recursion
    If bOnce Then
        Exit Sub
    Else
        bOnce = True
    End If

    Set oNodes = tvwCategories.Nodes
    On Error Resume Next
    Set oGroups = Me.GroupsNode
    Set oFaves = Me.FavoritesNode
    On Error GoTo ProcError

    If node Is oGroups Then
'       clear current list
        lstTemplates.ListItems.Clear

'       user clicked groups
        oFaves.Checked = Not oGroups.Checked
        If oGroups.Checked Then
'           show groups
            ShowSelectedGroups
            RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Groups)
        Else
'           show favorites
            ShowFavorites
            RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Favorites)
        End If
    ElseIf node Is oFaves Then
'       clear current list
        lstTemplates.ListItems.Clear

'       user clicked favorites
        oGroups.Checked = Not oFaves.Checked
        If oFaves.Checked Then
'           show favorites
            ShowFavorites
            RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Favorites)
        Else
'           show groups
            ShowSelectedGroups
            RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Groups)
        End If
    ElseIf (oGroups Is Nothing) Then
'       groups node does not exist -
'       show templates from specified group
'       clear current list
        lstTemplates.ListItems.Clear

'       user clicked a group and groups are shown
        ShowSelectedGroups
'        RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Groups)
    ElseIf oGroups.Checked Then
'       groups node does not exist -
'       show templates from specified group
'       clear current list
        lstTemplates.ListItems.Clear

'       user clicked a group and groups are shown
        ShowSelectedGroups
        RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Groups)
    End If

    SetBtnsMenus
    bOnce = False
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub SetBtnsMenus()
'   enable/disable menu items as appropriate
    On Error GoTo ProcError
    With tvwCategories.Nodes
        On Error Resume Next
        UserControl.mnuTemplates_AddFavorite.Enabled = _
            Me.GroupsNode.Checked
        UserControl.mnuTemplates_DeleteFavorite.Enabled = _
            Me.FavoritesNode.Checked
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.SetBtnsMenus"
    Exit Sub
End Sub

Private Sub ShowFavorites()
    On Error GoTo ProcError
    ShowTemplates -1
    
'   select first item or disable OK btn
    If lstTemplates.ListItems.Count Then
        lstTemplates.ListItems(1).Selected = True
    End If

'   broadcast that a selection was made
    RaiseEvent Selection(Me.SelectedID)
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.ShowFavorites"
    Exit Sub
End Sub

Private Sub ShowSelectedGroups()
'shows all templates in selected groups in dlg
    Dim oNode As node
    
'   show templates of each selected group
    If Me.ItemSource = mpItemSource_Templates Then
        For Each oNode In tvwCategories.Nodes
'           exclude "Groups" and "Favorites" nodes
            If Not oNode.Parent Is Nothing Then
                If oNode.Checked Then
                    ShowTemplates Mid(oNode.Key, 2)
                End If
            End If
        Next oNode
    Else
        For Each oNode In tvwCategories.Nodes
            If oNode.Checked Then
                ShowSegments Mid(oNode.Key, 2)
            End If
        Next oNode
    End If
    
'   select first item
    With lstTemplates.ListItems
        If .Count Then
            .Item(1).Selected = True
        End If
        
'       broadcast that a selection was made
        RaiseEvent Selection(Me.SelectedID)
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "MPO.frmNewDocument.ShowSelectedGroups"
    Exit Sub
End Sub

Private Sub ShowTemplates(Optional ByVal lGroupID As Long, _
        Optional ByVal xFilterText As String, _
        Optional ByVal bSearchNameCol As Boolean, _
        Optional ByVal bSearchDescriptionCol As Boolean)
    Dim i As Integer
    Dim iRefs As Integer
    Dim xID As String
    Dim oListItem As ListItem
    
    On Error GoTo ProcError
    With m_oDB.TemplateDefinitions
'       filter templates
        .Filter xFilterText, lGroupID, bSearchNameCol, bSearchDescriptionCol
        
'       add each template to list
        For i = 1 To .Count
            With .Item(i)
                xID = .FileName
                On Error Resume Next
                Set oListItem = lstTemplates.ListItems.Add(Key:=xID, _
                                              Text:=.ShortName, _
                                              Icon:=ilNormal.ListImages.Item(.Application + 1).Key, _
                                              SmallIcon:=ilSmall.ListImages.Item(.Application + 1).Key)
                oListItem.ListSubItems.Add Text:=.Description
            
'               increment the number of references to this icon
                With lstTemplates.ListItems(xID)
                    iRefs = 0
                    iRefs = .Tag
                    If iRefs = Empty Then
                        .Tag = 1
                    Else
                        .Tag = iRefs + 1
                    End If
                End With
            End With
        Next i
        lstTemplates.Sorted = True
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.ShowTemplates"
    Exit Sub
End Sub

Private Sub ShowSegments(Optional ByVal lGroupID As Long, _
        Optional ByVal xFilterText As String, _
        Optional ByVal bSearchNameCol As Boolean, _
        Optional ByVal bSearchDescriptionCol As Boolean)
    Dim i As Integer
    Dim iRefs As Integer
    Dim xID As String
    Dim oListItem As ListItem
    Dim oWord As Word.Application
    
    On Error GoTo ProcError
    With m_oDB.SegmentDefinitions
'       filter Segments
        On Error Resume Next
        Set oWord = GetObject(, "Word.application")
        On Error GoTo ProcError
        
        If oWord Is Nothing Then
            If UserControl.Ambient.UserMode = True Then
'               only show this message at run-time
                MsgBox "Can't show documents because Word is not open.  " & _
                    "Please launch Word, then try again.", vbExclamation, App.Title
            End If
            Exit Sub
        End If
        
'        .Refresh oWord.ActiveDocument.AttachedTemplate
        .Filter oWord.ActiveDocument.AttachedTemplate, xFilterText, _
            lGroupID, bSearchNameCol, bSearchDescriptionCol
        
'       add each Segment to list
        For i = 0 To .Count - 1
            With .ItemAtIndex(i)
                xID = "_" & .ID
                On Error Resume Next
                Set oListItem = lstTemplates.ListItems.Add(Key:=xID, _
                                              Text:=.Name, _
                                              Icon:=ilNormal.ListImages.Item(1).Key, _
                                              SmallIcon:=ilSmall.ListImages.Item(1).Key)
                oListItem.ListSubItems.Add Text:=.Description
                
'               increment the number of references to this icon
                With lstTemplates.ListItems(xID)
                    iRefs = 0
                    iRefs = .Tag
                    If iRefs = Empty Then
                        .Tag = 1
                    Else
                        .Tag = iRefs + 1
                    End If
                End With
            End With
        Next i
        lstTemplates.Sorted = True
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.ShowSegments"
    Exit Sub
End Sub

Private Sub HideTemplates(lGroupID As Long)
    Dim i As Integer
    Dim xID As String
    
    On Error GoTo ProcError
    With m_oDB.TemplateDefinitions
'       filter templates with specified group
        .Group = lGroupID
        .Refresh
        
'       decrement icon references of each template in collection
        For i = 1 To .Count
            xID = .Item(i).FileName
            On Error Resume Next
'           decrement the number of references to this icon
            With lstTemplates.ListItems(xID)
                .Tag = Max(.Tag - 1, 0)
'               if there are no refs left, remove icon from list
                If .Tag = 0 Then
                    lstTemplates.ListItems.Remove xID
                End If
            End With
        Next i
        lstTemplates.Sorted = True
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.HideTemplates"
    Exit Sub
End Sub

Private Sub tvwCategories_NodeClick(ByVal node As MSComctlLib.node)
    On Error GoTo ProcError
    node.Selected = False
    tvwCategories.Nodes(1).Selected = False
    
'   broadcast that a selection was made
    RaiseEvent Selection(Me.SelectedID)
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub InitializeTemplates()
'sets up the control for MacPac Templates
    Dim bExpand As Boolean
    Dim bSelFaves As Boolean
    Dim iView As MSComctlLib.ListViewConstants
    
    On Error GoTo ProcError
    
'    If m_oDB Is Nothing Then
'        Set m_oDB = New CDatabase
'    End If
    
    Set m_oError = New CError
    
    If m_oDB.TemplateGroups.Count = 0 Then
        Err.Raise mpError_NoTemplatesSpecified
    End If

'   set string prefix for later use
    m_xIniSecPrefix = "Template"
'   add favorites and groups
    With tvwCategories.Nodes
        .Add , , "xFavorites", "Favorites"
        .Add , , "xGroups", "Groups"
        On Error Resume Next
        bExpand = GetUserIni("TemplateGroups", "Expanded")
        Me.GroupsNode.Expanded = bExpand
    End With
    
'   add nodes to tree
    AddCategories m_oDB.TemplateGroups
    
'   get branch to show - faves or groups
    If UCase(GetUserIni("TemplateGroups", "FavesSelected")) = "TRUE" Then
        bSelFaves = True
    End If
    
    If bSelFaves Then
        '---9.4.1 hot fix; move this call to end of UserForm.Show
'       show faves
'        Me.FavoritesNode.Checked = True
'        tvwCategories_NodeCheck Me.FavoritesNode
'        RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Favorites)
    Else
'       show groups
        Me.GroupsNode.Checked = True
        RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Groups)
        ShowSelectedGroups
    End If
    
'   get view - ignore if non-valid value (might not yet be written to ini)
    On Error Resume Next
    iView = GetUserIni("Templates", "View")
    On Error GoTo ProcError
    
    If iView = lvwReport Then
        ViewDescriptions
    Else
        ViewIcons
    End If
    
    UserControl.lblTemplates.Caption = "&Templates:"
    
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub InitializeSegments()
'initializes the control for MacPac Segments
    Dim iView As MSComctlLib.ListViewConstants
    
    On Error GoTo ProcError
    
'    If m_oDB Is Nothing Then
'        Set m_oDB = New CDatabase
'    End If
    
    Set m_oError = New CError
    
    If m_oDB.SegmentGroups.Count = 0 Then
        Err.Raise mpError_NoTemplatesSpecified
    End If

'   set string prefix for later use
    m_xIniSecPrefix = "Segment"

'   add segment categories to tree
    AddCategories m_oDB.SegmentGroups

'   show templates in selected groups
    ShowSelectedGroups
    
'   get view - ignore if non-valid value
'   (might not yet be written to ini)
    On Error Resume Next
    iView = GetUserIni("Segments", "View")
    On Error GoTo ProcError
    If iView = lvwReport Then
        ViewDescriptions
    Else
        ViewIcons
    End If
    
    UserControl.lblTemplates.Caption = "Docu&ments:"
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub AddCategories(oGroups As Object)
'adds all categories to tree - oGroups is late
'bound to handle both Template and Segment groups
    Dim i As Integer
    
    On Error GoTo ProcError
    With oGroups
        For i = 1 To .Count
            AddCategory .ItemFromIndex(i), oGroups
        Next i
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, _
        "mpControls2.MPTemplates.AddCategories"
    Exit Sub
End Sub

Private Sub AddCategory(oGroup As Object, oGroups As Object)
'add category nodes to tree - late bound to handle both
'templateGroups and SegmentGroups - oGroup is the item (Segment or Template)
'oGroups is the collection of items
    Dim i As Integer
    Dim oNode As node
    Dim xGroup As String
    Dim oParent As Object
    Dim xName As String
    Dim lID As Long
    Dim lParentID As Long
    
    On Error GoTo ProcError
    With oGroup
'       get id of parent node
        lParentID = .ParentID
        
        If lParentID Then
'           check for existence of parent
            On Error Resume Next
            Set oParent = oGroups.Item(lParentID)
            
            If oParent Is Nothing Then
                Dim xMsg As String
                xMsg = "Invalid parent group. There is no group with ID = " & lParentID & _
                    "Please check tblSegmentGroups and tblTemplateGroups in mpPublic."
                Err.Raise mpError_InvalidMember, , xMsg
            End If
            
'           recurse to add parent if necessary
            AddCategory oGroups.Item(lParentID), oGroups
            
'           get id string for parent
            xGroup = "x" & lParentID
        ElseIf Not Me.GroupsNode Is Nothing Then
'           group has no parent - place as child of "Groups" node
            xGroup = "xGroups"
        End If
        
        If Len(xGroup) Then
'           add node as child of groups node - skip if
'           node already exists - nodes.add will err in that case
            On Error Resume Next
            Set oNode = tvwCategories.Nodes.Add( _
                        Key:="x" & .ID, Text:=.Name, _
                        Relative:=xGroup, _
                        Relationship:=tvwChild)
            On Error GoTo ProcError
        Else
'           add node at top level
            On Error Resume Next
            Set oNode = tvwCategories.Nodes.Add( _
                        Key:="x" & .ID, Text:=.Name)
            On Error GoTo ProcError
        End If
        
'       value of checkbox is determined by sticky field
        SetNodeValue oGroups, oNode, .ID
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.AddCategory"
    Exit Sub
End Sub

Private Sub SetNodeValue(oGroups As Object, oNode As MSComctlLib.node, lID As Long)
    Dim xIniSec As String
    
'   value of checkbox is determined by sticky field
    On Error Resume Next
    oNode.Checked = GetUserIni(m_xIniSecPrefix & "Groups", "x" & lID)
    If oNode.Parent.Key <> "xGroups" Then
'       expand the parent if the child is checked
        If oNode.Checked Then
            oNode.Parent.Expanded = True
        End If
    End If
    On Error GoTo ProcError

    Exit Sub
ProcError:
    m_oError.Raise "mpControls2.MPTemplates.GetNodeValue"
    Exit Sub
End Sub

Private Sub SetColumns()
'sets the list view columns for lstTemplates - width, names
    Dim xSource As String
    
    If Me.ItemSource = mpItemSource_Segments Then
        xSource = "Segments"
    Else
        xSource = "Templates"
    End If
    
    With UserControl.lstTemplates
        .ColumnHeaders.Add , "Name", "Name"
        .ColumnHeaders.Add , "Description", "Description"
        With UserControl.lstTemplates.ColumnHeaders
            On Error Resume Next
            Dim xCol1 As String
            xCol1 = GetUserIni(xSource, "Col1Width")
            If Len(xCol1) Then
'               there are user values for the column widths - use them
                .Item("Name").Width = GetUserIni(xSource, "Col1Width")
                .Item("Description").Width = GetUserIni(xSource, "Col2Width")
            Else
'               user values have not yet been set - set default
                .Item("Name").Width = 0.4 * UserControl.lstTemplates.Width
                .Item("Description").Width = 0.5 * UserControl.lstTemplates.Width
            End If
            On Error GoTo ProcError
        End With
    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.SetColumns"
End Sub

Private Sub UserControl_Initialize()
    On Error GoTo ProcError
'   set up list view columns
    SetColumns
    
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)
'sets hotkeys for Alt+I (View Icons), Alt+F (Find), and Alt+D (View Descriptions)
    On Error GoTo ProcError
    If KeyCode = vbKeyF And Shift = 4 Then '4= alt
        If UserControl.chkFind.Value = vbChecked Then
            UserControl.chkFind.Value = vbUnchecked
        Else
            UserControl.chkFind.Value = vbChecked
        End If
    ElseIf KeyCode = vbKeyI And Shift = 4 Then
        ViewIcons
    ElseIf KeyCode = vbKeyD And Shift = 4 Then
        ViewDescriptions
    'GLOG : 5486 : ceh
    ElseIf IsPressed(vbKeyControl) And (KeyCode = vbKeyW) Then
        ResizeControls True
    End If
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    Me.ItemSource = PropBag.ReadProperty("ItemSource", mpItemSource_Templates)
End Sub

Private Sub UserControl_Resize()
    On Error GoTo ProcError
    ResizeControls
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

'GLOG : 5486 : ceh
Private Sub ResizeControls(Optional ResetTemplatesList As Boolean = False)
    Dim oList As ListView
    Dim bExpanded As Boolean
    
    On Error GoTo ProcError
    Const TREE_WIDTH_PERCENT As Single = 2265 / 7290
    Const TREE_HEIGHT_PERCENT As Single = 4260 / 4500
    Const LIST_WIDTH_PERCENT As Single = 4980 / 7290
    Const LIST_HEIGHT_PERCENT As Single = 4260 / 4500
    
    On Error Resume Next
    bExpanded = GetUserIni("Templates", "Expanded")
    On Error GoTo ProcError
    
    With UserControl
        Set oList = .lstTemplates
        
        .tvwCategories.Height = .Height - .tvwCategories.Top
        .tvwCategories.Width = TREE_WIDTH_PERCENT * .Width
        
        oList.Height = .Height - oList.Top
        
        If ResetTemplatesList Then
            If oList.Left = 0 Then
                .btnResetTemplatesView.ToolTipText = "Expand Templates' Width (Ctrl+W)"
                .lblGroups.Visible = True
                oList.Left = .tvwCategories.Width + 45
        '       leave space at right for "VIEWS"
                oList.Width = .Width - oList.Left - .btnViewIcons.Width - 30
                bExpanded = False
            Else
                .btnResetTemplatesView.ToolTipText = "Reduce Templates' Width (Ctrl+W)"
                .lblGroups.Visible = False
                oList.Left = 0
        '       leave space at right for "VIEWS"
                oList.Width = (.Width - .btnViewIcons.Width - 30)
                bExpanded = True
            End If
        ElseIf bExpanded Then
            .btnResetTemplatesView.ToolTipText = "Reduce Templates' Width (Ctrl+W)"
            .lblGroups.Visible = False
            oList.Left = 0
    '       leave space at right for "VIEWS"
            oList.Width = (.Width - .btnViewIcons.Width - 30)
        End If
        
        .lblTemplates.Left = oList.Left
        
'       put "VIEWS" buttons at right
        .btnViewIcons.Left = .Width - .btnViewIcons.Width
        .btnViewDescriptions.Left = .btnViewIcons.Left
        .btnResetTemplatesView.Left = .btnViewIcons.Left
        .chkFind.Left = .btnViewIcons.Left
        
        SetUserIni "Templates", "Expanded", bExpanded

    End With
    Exit Sub
ProcError:
    m_oError.Raise Err.Number, "mpControls2.MPTemplates.ResizeControls"
    Exit Sub
End Sub

Private Sub UserControl_Show()
    Dim bSelFaves As Boolean
    
    On Error GoTo ProcError
    lstTemplates.SetFocus
    
'   add template groups to tree
    If UCase(GetUserIni(m_xIniSecPrefix & "Groups", "FavesSelected")) = "TRUE" Then
        bSelFaves = True
    End If
    
'   notify whether favorites or groups is selected
    If bSelFaves Then
        
'       show faves
        Me.FavoritesNode.Checked = True
        tvwCategories_NodeCheck Me.FavoritesNode
        
        RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Favorites)
    Else
        RaiseEvent GroupCategoryChange(mpTemplateGroupCategory_Groups)
    End If
    
    SetBtnsMenus
    
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub UserControl_Terminate()
'   write current settings to user.ini
    On Error GoTo ProcError
    If Not Me.GroupsNode Is Nothing Then
        SetUserIni m_xIniSecPrefix & "Groups", "Expanded", _
            Me.GroupsNode.Expanded
    End If
    
    If Not Me.FavoritesNode Is Nothing Then
        SetUserIni m_xIniSecPrefix & "Groups", "FavesSelected", _
            Me.FavoritesNode.Checked
    End If
    Dim xSource As String
    If Me.ItemSource = mpItemSource_Templates Then
        xSource = "Templates"
    Else
        xSource = "Segments"
    End If
    
    With UserControl.lstTemplates
        SetUserIni xSource, "View", .View
        SetUserIni xSource, "Col1Width", .ColumnHeaders("Name").Width
        SetUserIni xSource, "Col2Width", .ColumnHeaders("Description").Width
    End With
    
    '---9.4.1
    If Not m_oDB Is Nothing Then
        With m_oDB.TemplateDefinitions
            .Group = 0
            .Refresh
        End With
    End If
    '---end 9.4.1
    
'    Set m_oDB = Nothing
    Exit Sub
ProcError:
    m_oError.Show Err
    Exit Sub
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    PropBag.WriteProperty "ItemSource", Me.ItemSource
End Sub

Public Property Get GroupsNode() As MSComctlLib.node
'get groups node - check once, then return object
    Static oNode As MSComctlLib.node
    Static bChecked As Boolean
    
    If Not bChecked Or oNode Is Nothing Then
        On Error Resume Next
        Set oNode = UserControl.tvwCategories.Nodes.Item("xGroups")
        bChecked = True
    End If
    Set GroupsNode = oNode
End Property

Public Property Get FavoritesNode() As MSComctlLib.node
'get favorites node - check once
'return object on subsequent calls
    Static oNode As MSComctlLib.node
    Static bChecked As Boolean
    
    If Not bChecked Then
        On Error Resume Next
        Set oNode = UserControl.tvwCategories.Nodes.Item("xFavorites")
        bChecked = True
    End If
    Set FavoritesNode = oNode
End Property

