VERSION 5.00
Begin VB.Form frmFind 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Find Template"
   ClientHeight    =   1560
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5025
   Icon            =   "frmFind.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1560
   ScaleWidth      =   5025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkSearchNameCol 
      Appearance      =   0  'Flat
      Caption         =   "Search &Template Names"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   180
      TabIndex        =   2
      Top             =   900
      Value           =   1  'Checked
      Width           =   2505
   End
   Begin VB.CheckBox chkSearchDescCol 
      Appearance      =   0  'Flat
      Caption         =   "Search Template &Descriptions"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   180
      TabIndex        =   3
      Top             =   1185
      Width           =   2505
   End
   Begin VB.CommandButton txtCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3885
      TabIndex        =   5
      Top             =   1020
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   400
      Left            =   2790
      TabIndex        =   4
      Top             =   1020
      Width           =   1000
   End
   Begin VB.TextBox txtFind 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   135
      MaxLength       =   50
      TabIndex        =   1
      Top             =   375
      Width           =   4755
   End
   Begin VB.Label lblFind 
      Caption         =   "&Name:"
      Height          =   195
      Left            =   165
      TabIndex        =   0
      Top             =   150
      Width           =   510
   End
End
Attribute VB_Name = "frmFind"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xName As String
Private m_bCancelled As Boolean
Private m_bSearchName As Boolean
Private m_bSearchDescription As Boolean

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get SearchNameColumn() As Boolean
    SearchNameColumn = m_bSearchName
End Property

Public Property Let SearchNameColumn(bNew As Boolean)
    m_bSearchName = bNew
End Property

Public Property Get SearchDescriptionColumn() As Boolean
    SearchDescriptionColumn = m_bSearchDescription
End Property

Public Property Let SearchDescriptionColumn(bNew As Boolean)
    m_bSearchDescription = bNew
End Property

Public Property Get ItemName() As String
    ItemName = m_xName
End Property

Public Property Let ItemName(xNew As String)
    m_xName = xNew
End Property

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.ItemName = Me.txtFind
    Me.SearchDescriptionColumn = Me.chkSearchDescCol
    Me.SearchNameColumn = Me.chkSearchNameCol
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    m_bCancelled = True
End Sub

Private Sub txtCancel_Click()
    m_bCancelled = True
    Me.ItemName = Empty
    Me.Hide
    DoEvents
End Sub

Private Sub txtFind_Change()
'   enable OK btn only when there
'   is text in the Name txt box
    If txtFind.Text = "" Then
        btnOK.Enabled = False
    Else
        btnOK.Enabled = True
    End If
End Sub

Private Sub txtFind_GotFocus()
    With Me.txtFind
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
