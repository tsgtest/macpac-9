VERSION 5.00
Begin VB.Form frmAddItem 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   1635
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5025
   Icon            =   "frmAddItem.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1635
   ScaleWidth      =   5025
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton txtCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3885
      TabIndex        =   3
      Top             =   1140
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   400
      Left            =   2790
      TabIndex        =   2
      Top             =   1140
      Width           =   1000
   End
   Begin VB.TextBox txtItemName 
      Height          =   360
      Left            =   135
      MaxLength       =   50
      TabIndex        =   1
      Top             =   435
      Width           =   4755
   End
   Begin VB.Label Label1 
      Caption         =   "&Name:"
      Height          =   195
      Left            =   165
      TabIndex        =   0
      Top             =   225
      Width           =   510
   End
End
Attribute VB_Name = "frmAddItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xName As String
Private m_bCancelled As Boolean

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get ItemName() As String
    ItemName = m_xName
End Property

Public Property Let ItemName(xNew As String)
    m_xName = xNew
End Property

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.ItemName = Me.txtItemName
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    m_bCancelled = True
End Sub

Private Sub txtCancel_Click()
    m_bCancelled = True
    Me.ItemName = Empty
    Me.Hide
    DoEvents
End Sub

Private Sub txtItemName_Change()
'   enable OK btn only when there
'   is text in the Name txt box
    If txtItemName.Text = "" Then
        btnOK.Enabled = False
    Else
        btnOK.Enabled = True
    End If
End Sub

Private Sub txtItemName_GotFocus()
    With Me.txtItemName
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
