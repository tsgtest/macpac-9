Attribute VB_Name = "mdlGlobal"
Option Explicit

Public Declare Function GetKeyState Lib "user32" ( _
    ByVal nVirtKey As Long) As Integer

Function IsPressed(lKey As Long) As Boolean
    IsPressed = (GetKeyState(lKey) < 0)
End Function


