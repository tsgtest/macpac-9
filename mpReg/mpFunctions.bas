Attribute VB_Name = "mpFunctions"
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long

Private oFSO As New FileSystemObject

Function bRegister(Optional bIncludeCI As Boolean = True, _
                   Optional bIncludeNum As Boolean = True)

    Dim xMP90Path As String
    Dim xCIPath As String
    Dim xCIPath_OLD As String
    Dim xCommonPath As String
    Dim xCustTempWizardPath As String
    Dim xNumPath As String
    Dim xMPPUPCLIPath As String
    Dim I As Integer
    Dim oFile As File
    Dim oFolder As Folder
    Dim oFolder_OLD As Folder
    Dim xCommand As String
    Dim bSilent As Boolean
    Dim bUnregister As Boolean
    Dim xMessage As String
    
    On Error GoTo ProcError

    
    xCommand = Command()
    
    bSilent = InStr(UCase(xCommand), "/S")
    bUnregister = InStr(UCase(xCommand), "/U")

'   Get paths
    xMP90Path = App.Path & "\"
    
'   create MacPac lock file - this indicates that
'   this register function is currently executing
'   it will get deleted at end of function
    I = FreeFile
    Open xMP90Path & "mpLock.txt" For Output As #I
    Close #I
    
    If bIncludeCI Then
        xCIPath = Replace(UCase(xMP90Path), "MACPAC90\APP\", "CI\APP")
        xCIPath_OLD = Replace(UCase(xMP90Path), "MACPAC90\APP\", "CI")
    End If
    
'   get MacPac\Common folder files path
    xCommonPath = Replace(UCase(xMP90Path), "MACPAC90\APP\", "COMMON")
    
'   get MacPac\Custom Template Wizard folder files path
    xCustTempWizardPath = Replace(UCase(xMP90Path), "MACPAC90\APP\", "MACPAC90\TOOLS\ADMINISTRATIVE\CUSTOM TEMPLATE WIZARD")
    
    If bIncludeNum Then
        xNumPath = Replace(UCase(xMP90Path), "MACPAC90\APP\", "NUMBERING\APP")
    End If
        
    xMPPUPCLIPath = GetIni("mpPupCli", "ServerDir", App.Path & "\MacPac.ini")
        
'   register MacPac90 files
    Set oFolder = oFSO.GetFolder(App.Path)
    
    If Not (oFolder Is Nothing) Then
        For Each oFile In oFolder.Files
            With oFile
                If (Right(UCase(.Name), 4) = ".OCX" Or _
                Right(UCase(.Name), 4) = ".DLL" Or _
                Right(UCase(.Name), 4) = ".EXE") And _
                (UCase(.Name) <> "MPVER90.DLL" And _
                UCase(.Name) <> "MPVER80.DLL" And _
                UCase(.Name) <> "PCDCLIENT.DLL" And _
                UCase(.Name) <> "UPFUSION.DLL") Then
                    If bUnregister Then
                        bUnRegisterServer xMP90Path & .Name
                    Else
                        bReRegisterServer xMP90Path & .Name
                    End If
                End If
            End With
        Next oFile
    End If
    
'   register Common files
    On Error Resume Next
    Set oFolder = oFSO.GetFolder(xCommonPath)
    On Error GoTo ProcError
    
    If Not (oFolder Is Nothing) Then
        For Each oFile In oFolder.Files
            With oFile
                If (Right(UCase(.Name), 4) = ".OCX" Or _
                Right(UCase(.Name), 4) = ".DLL" Or _
                Right(UCase(.Name), 4) = ".EXE") Then
                    If bUnregister Then
                        bUnRegisterServer xCommonPath & "\" & .Name
                    Else
                        bReRegisterServer xCommonPath & "\" & .Name
                    End If
                End If
            End With
        Next oFile
    End If

'   register Custom Template Wizard files
    On Error Resume Next
    Set oFolder = oFSO.GetFolder(xCustTempWizardPath)
    On Error GoTo ProcError
    
    If Not (oFolder Is Nothing) Then
        For Each oFile In oFolder.Files
            With oFile
                If (Right(UCase(.Name), 4) = ".OCX" Or _
                Right(UCase(.Name), 4) = ".DLL" Or _
                Right(UCase(.Name), 4) = ".EXE") Then
                    If bUnregister Then
                        bUnRegisterServer xCustTempWizardPath & "\" & .Name
                    Else
                        bReRegisterServer xCustTempWizardPath & "\" & .Name
                    End If
                End If
            End With
        Next oFile
    End If

'   register mppupcli.exe
    On Error Resume Next
    Set oFolder = oFSO.GetFolder(xMPPUPCLIPath)
    On Error GoTo ProcError
    
    If Not (oFolder Is Nothing) Then
        For Each oFile In oFolder.Files
            With oFile
                If (Right(UCase(.Name), 4) = ".OCX" Or _
                Right(UCase(.Name), 4) = ".DLL" Or _
                Right(UCase(.Name), 4) = ".EXE") Then
                    If bUnregister Then
                        bUnRegisterServer xMPPUPCLIPath & "\" & .Name
                    Else
                        bReRegisterServer xMPPUPCLIPath & "\" & .Name
                    End If
                End If
            End With
        Next oFile
    End If

'   register CI files
    If bIncludeCI Then
        On Error Resume Next
        Set oFolder = oFSO.GetFolder(xCIPath)
        Set oFolder_OLD = oFSO.GetFolder(xCIPath_OLD)
        On Error GoTo ProcError
        
        If Not (oFolder Is Nothing) Then
            For Each oFile In oFolder.Files
                With oFile
                    If (Right(UCase(.Name), 4) = ".OCX" Or _
                    Right(UCase(.Name), 4) = ".DLL" Or _
                    Right(UCase(.Name), 4) = ".EXE") Then
                        If bUnregister Then
                            bUnRegisterServer xCIPath & "\" & .Name
                        Else
                            bReRegisterServer xCIPath & "\" & .Name
                        End If
                    End If
                End With
            Next oFile
        ElseIf Not (oFolder_OLD Is Nothing) Then
            For Each oFile In oFolder_OLD.Files
                With oFile
                    If (Right(UCase(.Name), 4) = ".OCX" Or _
                    Right(UCase(.Name), 4) = ".DLL" Or _
                    Right(UCase(.Name), 4) = ".EXE") Then
                        If bUnregister Then
                            bUnRegisterServer xCIPath_OLD & "\" & .Name
                        Else
                            bReRegisterServer xCIPath_OLD & "\" & .Name
                        End If
                    End If
                End With
            Next oFile
        End If
    End If
    
'   register Numbering files
    If bIncludeNum Then
        On Error Resume Next
        Set oFolder = oFSO.GetFolder(xNumPath)
        On Error GoTo ProcError
        
        If Not (oFolder Is Nothing) Then
            For Each oFile In oFolder.Files
                With oFile
                    If (Right(UCase(.Name), 4) = ".OCX" Or _
                    Right(UCase(.Name), 4) = ".DLL" Or _
                    Right(UCase(.Name), 4) = ".EXE") And _
                    (UCase(.Name) <> "MPWD80.DLL" And _
                    UCase(.Name) <> "MPWD90.DLL") Then
                        If bUnregister Then
                            bUnRegisterServer xNumPath & "\" & .Name
                        Else
                            bReRegisterServer xNumPath & "\" & .Name
                        End If
                    End If
                End With
            Next oFile
        End If
    End If
    
    bRegister = True
                
'   delete lock file
    Kill xMP90Path & "mpLock.txt"
    
    If Not bSilent Then
        If bUnregister Then
            xMessage = "All the MacPac components were unregistered."
        Else
            xMessage = "All the MacPac components were registered."
        End If
        MsgBox xMessage, vbInformation, App.Title
    End If
    
    Exit Function
ProcError:
    If Err.Number <> 0 Then
        MsgBox "Unable to register some of the MacPac components.  " & _
                "Please contact your system administrator." & vbCr & vbCr & _
                "ERROR " & Err.Number & ": " & Err.Description, vbExclamation, App.Title
                
'       delete lock file
        Kill xMP90Path & "mpLock.txt"
    
        End
    End If
End Function

Function bReRegisterServer(sRegister As String, Optional sUnregister As String = "") As Boolean
    
    On Error GoTo Register_Error
    Static strRegCmd As String
    Static strUnRegCmd As String
    
    If strRegCmd = "" Then
        strRegCmd = oFSO.GetSpecialFolder(SystemFolder) & "\Regsvr32.exe"
        If Dir(strRegCmd) = "" Then
            strRegCmd = oFSO.GetSpecialFolder(WindowsFolder) & "\Regsvr32.exe"
            If Dir(strRegCmd) = "" Then
                MsgBox "Unable to register MacPac components, Regsvr32.exe not found" & vbCr & _
                       "Please contact your system administrator", vbExclamation, App.Title
                Set oFSO = Nothing
                End
            End If
        End If
#If bDebug Then
        strRegCmd = strRegCmd & " "
#Else
        strRegCmd = strRegCmd & " /s "
#End If
        strUnRegCmd = strRegCmd & "/u "
    End If
    ' Register DLLs and unregister existing versions if they exist
    If sUnregister <> "" And Dir(sUnregister) <> "" Then
        If InStr(UCase(sUnregister), ".EXE") > 0 Then
            Shell sUnregister & " /unregserver"
        Else
            Shell strUnRegCmd & Chr(34) & sUnregister & Chr(34)
        End If
        DoEvents
    End If
    
    If Dir(sRegister) <> "" Then
        If InStr(UCase(sRegister), ".EXE") > 0 Then
            Shell sRegister & " /regserver"
        Else
            Shell strRegCmd & Chr(34) & sRegister & Chr(34)
        End If
        DoEvents
    End If
    
    bReRegisterServer = True
    Exit Function
    
Register_Error:
    bReRegisterServer = False
End Function
Function sGetPathFromClassID(sClassName As String, Optional bEXE As Boolean = False)
    
    Dim sClass As String
    
    sClass = GetKeyValue(HKEY_CLASSES_ROOT, sClassName & "\Clsid", "")
    If sClass <> "" Then
        If Not bEXE Then
            sGetPathFromClassID = GetKeyValue(HKEY_CLASSES_ROOT, "CLSID\" & sClass & "\InProcServer32", "")
        Else
            sGetPathFromClassID = GetKeyValue(HKEY_CLASSES_ROOT, "CLSID\" & sClass & "\LocalServer32", "")
        End If
    Else
        sGetPathFromClassID = ""
    End If

End Function

Function bUnRegisterServer(sUnregister As String) As Boolean
    
    On Error GoTo Register_Error
    Static strRegCmd As String
    Static strUnRegCmd As String
    
    If strRegCmd = "" Then
        strRegCmd = oFSO.GetSpecialFolder(SystemFolder) & "\Regsvr32.exe"
        If Dir(strRegCmd) = "" Then
            strRegCmd = oFSO.GetSpecialFolder(WindowsFolder) & "\Regsvr32.exe"
            If Dir(strRegCmd) = "" Then
                MsgBox "Unable to unregister MacPac components, Regsvr32.exe not found" & vbCr & _
                       "Please contact your system administrator", vbExclamation, App.Title
                Set oFSO = Nothing
                End
            End If
        End If
#If bDebug Then
        strRegCmd = strRegCmd & " "
#Else
        strRegCmd = strRegCmd & " /s "
#End If
        strUnRegCmd = strRegCmd & "/u "
    End If
    'unregister existing versions if they exist
    If sUnregister <> "" And Dir(sUnregister) <> "" Then
        If InStr(UCase(sUnregister), ".EXE") > 0 Then
            Shell sUnregister & " /unregserver"
        Else
            Shell strUnRegCmd & Chr(34) & sUnregister & Chr(34)
        End If
        DoEvents
    End If
        
    bUnRegisterServer = True
    Exit Function
    
Register_Error:
    bUnRegisterServer = False
End Function

Function GetIni(xSection As String, _
                xKey As String, _
                xIni As String) As Variant
                    
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function


