VERSION 5.00
Begin VB.Form frmTimer 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "frmTimer"
   ClientHeight    =   735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1560
   Icon            =   "frmTimer.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   735
   ScaleWidth      =   1560
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   200
      Left            =   225
      Top             =   105
   End
End
Attribute VB_Name = "frmTimer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Enum puErrors
    puError_MissingFile = vbError + 512 + 1
    puError_DBConnectionFailed
    puError_CouldNotCompact
End Enum
    
Private Sub Timer1_Timer()
    Dim iChannel As Integer
    
    On Error GoTo ProcError

'   stop timer
    Me.Timer1.Enabled = False
    
'   run update
    Dim o As mpPupCli.CPup
    Set o = New mpPupCli.CPup
    o.Execute
    
'   close down
    Unload Me
    Exit Sub
ProcError:
    Unload Me
End Sub
