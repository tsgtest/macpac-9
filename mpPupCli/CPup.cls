VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CPup Class
'   created 8/10/01 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods of
'   MacPac People Updater Client Class
'   Exists to allow synchronous updating
'   of people information - to run asynchronously,
'   shell on mpPupCli.exe
'**********************************************************
Option Explicit

Const SPACER As String = "     "


Public Property Let LastUpdate(dNew As Date)
    mpBase2.SetUserIni "General", "LastPeopleUpdate", CDbl(dNew)
End Property

Private Property Get LastUpdate() As Date
     On Error Resume Next
     LastUpdate = mpBase2.GetUserIni("General", "LastPeopleUpdate")
End Property

Public Sub Execute()
'Updates people information in mpPeople.mdb
    Dim oIni As mpBase2.CIni
    Dim xSource As String
    Dim xDest As String
    Dim xSQL As String
    Dim oDBDest As DAO.Database
    Dim oDBSource As DAO.Database
    Dim xTables(2) As String
    Dim i As Integer
    Dim xDestTemp As String
    Dim xErr As String
    Dim t0 As Long
    Dim iAttempt As Integer
    Dim xExecute As String
    Dim iChannel As Integer
    Dim xOnGoingLog As String
    Dim bOnGoingLog As Boolean
    
    On Error GoTo ProcError

    '9.7.2010
    'we need this in case exe is run outside of code
    xExecute = mpBase2.GetMacPacIni("mpPupCli", "Execute")
    

    
    If xExecute = "0" Then
        Exit Sub
    End If

    On Error Resume Next
    
    '*c deal with log
    xOnGoingLog = GetMacPacIni("mpPupCli", "OnGoingLog")
    bOnGoingLog = UCase(xOnGoingLog) = "TRUE"

    If Not bOnGoingLog Then
        Kill mpBase2.ApplicationDirectory & "\" & "mpPupCli.log"
    End If
    
    On Error GoTo ProcError
'   create new log
    iChannel = FreeFile()
    Open mpBase2.ApplicationDirectory & "\" & "mpPupCli.log" For Append As #iChannel
    
'   write start time
    Print #iChannel, vbCrLf & Now & SPACER & "Start"
    

'   get destination db
    xDest = mpBase2.PeopleDB()
    Print #iChannel, Now & SPACER & "Destination File is: " & xDest
    '*c NEW backup db
    '   write start time
    Print #iChannel, Now & SPACER & "Creating backup people database"
    FileCopy xDest, xDest & ".bak"
    If Dir(xDest) = Empty Then
        Err.Raise puError_MissingFile, , _
            "Could not find '" & xDest & "'."
    End If


    On Error Resume Next
    Set oDBDest = DBEngine(0).OpenDatabase(xDest, , False)
    If oDBDest Is Nothing Then
        Err.Raise puError_DBConnectionFailed, , _
            "Could not open '" & xDest & "'."
    End If

'   get source db
    '9.7.2010
    xSource = mpBase2.GetMacPacIni("mpPupCli", "ServerDir")
    
    'add trailing slash if necessary
    xSource = xSource & IIf(Right$(xSource, 1) <> "\", "\", "") & "mpPeople.mdb"

    Print #iChannel, Now & SPACER & "Source is: " & xSource

    If Dir(xSource) = Empty Then
        Err.Raise puError_MissingFile, , _
            "Could not find '" & xSource & "'."
    End If

    On Error Resume Next
    Set oDBSource = DBEngine(0).OpenDatabase(xSource, , True)
    On Error GoTo ProcError
    If oDBSource Is Nothing Then
        Err.Raise puError_DBConnectionFailed, , _
            "Could not open '" & xSource & "'."
    End If

    xTables(0) = "tblPeopleHub"
    xTables(1) = "tblPeopleMP"
    xTables(2) = "tblAttyLicenses"

    Print #iChannel, Now & SPACER & "Delete tables"

    For i = 0 To UBound(xTables)
'       delete table in destination
        xSQL = "DROP TABLE " & xTables(i)
        On Error Resume Next
        oDBDest.Execute xSQL
        
        iAttempt = 0
        
        While Err.Number > 0 And iAttempt < 5
            'drop table failed - wait and try again (up to 5x)
            On Error GoTo ProcError
            iAttempt = iAttempt + 1
            
            'wait 3 seconds
            t0 = CurrentTick()
            While (CurrentTick() - t0) < 3000
            Wend
            
            'try again
            On Error Resume Next
            oDBDest.Execute xSQL
        Wend
        
        If Err.Number > 0 Then
            'try one last time, allowing error to be handled
            On Error GoTo ProcError
            oDBDest.Execute xSQL
        End If
    Next i

    '*c NEW allow new errors to be handled
    On Error GoTo ProcError
    
    Print #iChannel, Now & SPACER & "Recreate tables"
    
    For i = 0 To UBound(xTables)
'       create table in destination
        xSQL = "SELECT * INTO " & xTables(i) & " IN '" _
             & xDest & "' FROM  " & xTables(i)
        Print #iChannel, Now & SPACER & "Recreate " & xTables(i)
        oDBSource.Execute xSQL
    Next i

    Print #iChannel, Now & SPACER & "Create indexes"
    
    For i = 0 To UBound(xTables)
'       create hub table primary key in destination
        xSQL = "CREATE INDEX fldID ON " & xTables(i) & _
            " (fldID) WITH PRIMARY"
        Print #iChannel, Now & SPACER & "Create indexes in " & xTables(i)
        oDBDest.Execute xSQL
    Next i

    Print #iChannel, Now & SPACER & "record update time in user.ini"
'   record update time in user.ini
    Me.LastUpdate = CDbl(Date)

'   close destination db
    oDBDest.Close

    Print #iChannel, Now & SPACER & "Compact Destination db"

'   compact destination db
    xDestTemp = xDest & ".tmp"

    On Error Resume Next
    DBEngine.CompactDatabase xDest, xDestTemp
    xErr = Err.Description
    On Error GoTo ProcError

    If Dir(xDestTemp, vbNormal) <> Empty Then
'       replace current db with compacted db
        Kill xDest
        Name xDestTemp As xDest
    Else
'       could not compact db - alert
        Err.Raise puError_CouldNotCompact, , _
            "Could not compact file '" & xDest & _
            "'.  " & xErr
    End If
    
    Print #iChannel, Now & SPACER & "Delete backup database '" & xDest & ".bak'"

    '*c NEW delete backup - if we got here, we were successful
    Kill xDest & ".bak"

    Set oDBDest = Nothing

'   close source db
    oDBSource.Close
    Set oDBSource = Nothing

    Print #iChannel, Now & SPACER & "mpPupCli successfully updated!"
    Print #iChannel, Now & SPACER & "END"

    Exit Sub
ProcError:
    Print #iChannel, Now & SPACER & "ERROR: " & Err.Number & " - " & Err.Description
    
    If Not (oDBDest Is Nothing) Then
        oDBDest.Close
        Set oDBDest = Nothing
    End If
    
    If Not (oDBSource Is Nothing) Then
        oDBSource.Close
        Set oDBSource = Nothing
    End If
    
    '*c NEW restore backups - something's wrong
    FileCopy xDest & ".bak", xDest
    Kill xDest & ".bak"
    
    Print #iChannel, Now & SPACER & "Restored original copy of database 'mpPeople.mdb'"
    Print #iChannel, Now & SPACER & "END"

'    Err.Raise Err.Number, "mpPUPCli.CPup.Execute", Err.Description
End Sub

