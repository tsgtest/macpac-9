VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUpdateStyles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CUpdateStyles Class
'   created 2/24/01 by Doug Miller -
'   based on CNormal Class
'   created 3/29/00  by Charlie Homo -
'   charliehomo@earthlink.net

'   Contains methods to update the styles
'   in user templates upon initialization

'**********************************************************

Private m_oFSO As Scripting.FileSystemObject
Dim xMappings() As String

Private Sub GetStyleMappings()
    Dim xValue As String
    Dim iCount As Integer
    Dim i As Integer
    
    On Error Resume Next
        
    xValue = "x"
    
    While xValue <> ""
        iCount = iCount + 1
        xValue = mpBase2.GetMacPacIni("Update Styles", _
            "Source" & iCount)
    Wend
    
    iCount = iCount - 1
    
    If iCount > 0 Then
        ReDim xMappings(iCount - 1, 1)
        For i = 0 To iCount - 1
            xMappings(i, 0) = mpBase2.GetMacPacIni("Update Styles", _
                "Source" & (i + 1))
            xMappings(i, 1) = mpBase2.GetMacPacIni("Update Styles", _
                "Destination" & (i + 1))
        Next i
    Else
        ReDim xMappings(0, 1)
    End If
End Sub

Private Function OpenAsDocument(xPath As String) As Word.Document
    Set OpenAsDocument = Word.Documents.Open(xPath)
End Function

Private Function UpdateRequired(xSource As String, xDestination As String) As Boolean
'   returns TRUE if a new sty file has been distributed
    Dim bNewSty As Boolean
    Dim oFile As Scripting.File
    Dim xOldStyVersion As String
    Dim xNewStyVersion As String
    Dim xMsg As String
    Dim xSourcePath As String
    Dim xDestPath As String
    
    On Error GoTo ProcError
        
    xSourcePath = mpBase2.ApplicationDirectory & "\" & xSource
    xDestPath = Word.Application.Options _
        .DefaultFilePath(wdUserTemplatesPath) & _
        "\" & xDestination
        
    On Error Resume Next
    Set oFile = m_oFSO.GetFile(xSourcePath)
    On Error GoTo ProcError
    
    If oFile Is Nothing Then
        Err.Clear
        xMsg = "The following file required by MacPac could not be found: '" & _
            xSourcePath & "'.  Please contact your administrator."
        MsgBox xMsg, vbExclamation, App.Title
        Exit Function
    End If
    
'   get last edit and open time of this file-
'   the concatenated string is the "version id"
    xNewStyVersion = CDbl(oFile.DateLastModified)
    
'   the old version id was written to the ini when the
'   template defaults were set
    xOldStyVersion = GetUserIniNumeric(xDestination, "StyVersion") '*c
    
'   an updated Normal.sty has arrived if two ids don't match
    bNewSty = (xNewStyVersion <> xOldStyVersion)
    
    UpdateRequired = bNewSty
    
    Exit Function
ProcError:
    RaiseError "MPO.CUpdateStyles.UpdateRequired"
    Exit Function
End Function

Public Sub ResetStyles()
'updates styles in user templates to defaults in sty files
    Dim docDest As Word.Document
    Dim oFile As Scripting.File
    Dim xMsg As String
    Dim i As Integer
    Dim xSourcePath As String
    Dim xDestPath As String
    
    On Error GoTo ProcError
    
    GetStyleMappings
    If xMappings(0, 0) = "" Then Exit Sub
    
    Application.ScreenUpdating = False
        
    For i = 0 To UBound(xMappings)
        xSourcePath = mpBase2.ApplicationDirectory & "\" & xMappings(i, 0)
        xDestPath = Word.Application.Options _
            .DefaultFilePath(wdUserTemplatesPath) & _
            "\" & xMappings(i, 1)
            
'       ensure existence of destination file
        On Error Resume Next
        Set oFile = m_oFSO.GetFile(xDestPath)
        On Error GoTo ProcError
        
        If oFile Is Nothing Then
            Err.Clear
            
            'skip if normal.dot isn't present
            If UCase(xDestPath) Like "*\NORMAL.DOT*" Then
                GoTo labNext
            End If
            
            xMsg = "The following file required by MacPac could not be found: '" & _
                xDestPath & "'.  Please contact your administrator."
            MsgBox xMsg, vbExclamation, App.Title
            GoTo labNext
        End If
        
'       ensure existence of source file
        On Error Resume Next
        Set oFile = m_oFSO.GetFile(xSourcePath)
        On Error GoTo ProcError
        
        If oFile Is Nothing Then
            Err.Clear
            xMsg = "The following file required by MacPac could not be found: '" & _
                xSourcePath & "'.  Please contact your administrator."
            MsgBox xMsg, vbExclamation, App.Title
            GoTo labNext
        End If
        
        If UpdateRequired(xMappings(i, 0), xMappings(i, 1)) Then
            Application.StatusBar = "Resetting styles in " & xMappings(i, 1) & _
                                    "  Please wait..."
            EchoOff
            Set docDest = OpenAsDocument(xDestPath)
            With docDest
                .CopyStylesFromTemplate xSourcePath
                .Save
                .Close
            End With
            EchoOn
            
'           mark version of source file
            SetUserIni xMappings(i, 1), "StyVersion", _
                CDbl(oFile.DateLastModified)
        End If
labNext:
    Next i

    Application.ScreenUpdating = True
    
    Exit Sub
ProcError:
    EchoOn
    RaiseError "MPO.CUpdateStyles.ResetStyles"
    Exit Sub
End Sub

Private Sub Class_Initialize()
    Set m_oFSO = New Scripting.FileSystemObject
End Sub

Private Sub Class_Terminate()
    Set m_oFSO = Nothing
End Sub


