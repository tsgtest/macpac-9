VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C846A582-6451-4CF3-8DF4-1B918B91AFD5}#17.0#0"; "mpControls2.ocx"
Begin VB.Form frmInsertSegmentE 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert into Current Document"
   ClientHeight    =   5625
   ClientLeft      =   45
   ClientTop       =   240
   ClientWidth     =   7830
   Icon            =   "frmInsertSegmentE.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5625
   ScaleWidth      =   7830
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin mpControls2.MPTemplates MPTemplates1 
      Height          =   4680
      Left            =   120
      TabIndex        =   5
      Top             =   60
      Width           =   7680
      _ExtentX        =   13547
      _ExtentY        =   8255
      ItemSource      =   2
   End
   Begin VB.CheckBox chkUseExistingPageSetup 
      Appearance      =   0  'Flat
      Caption         =   "&Use Existing Page Setup (Headers/Footers/Margins...)"
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   165
      TabIndex        =   2
      ToolTipText     =   "If checked, the inserted document will use the page setup values of the previous section in the document"
      Top             =   5265
      Width           =   4275
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   6300
      TabIndex        =   4
      Top             =   4950
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   5220
      TabIndex        =   3
      Top             =   4950
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbLocation 
      Height          =   600
      Left            =   840
      OleObjectBlob   =   "frmInsertSegmentE.frx":058A
      TabIndex        =   1
      Top             =   4905
      Width           =   3810
   End
   Begin MSComctlLib.ImageList ilNormal 
      Left            =   255
      Top             =   6255
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmInsertSegmentE.frx":27A5
            Key             =   "Normal Document"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblLocation 
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Location:"
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   135
      TabIndex        =   0
      Top             =   4965
      Width           =   1455
   End
End
Attribute VB_Name = "frmInsertSegmentE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private m_bCancelled As Boolean
Private m_bItemClicked As Boolean
Private m_oDefs As mpDB.CSegmentDefs

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Get SelectedItem() As Long
    Dim xID As String
    xID = Me.MPTemplates1.SelectedID

    '---9.4.1
    If Len(xID) = 0 Then
        Exit Property
    End If

'   trim prefixed char
    SelectedItem = Mid(Me.MPTemplates1.SelectedID, 2)

End Property

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    m_bCancelled = True
End Sub

Private Sub btnOK_Click()
    Me.Hide
    DoEvents
    m_bCancelled = False
End Sub

Private Sub cmbLocation_ItemChange()
    Me.chkUseExistingPageSetup.Enabled = Me.cmbLocation.Array.Value(Me.cmbLocation.SelectedItem, 2)
End Sub

Private Sub cmbLocation_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLocation, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLocation_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If bAuthorIsDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbLocation)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    On Error Resume Next
    Me.MPTemplates1.SetFocus
End Sub

Private Sub Form_Initialize()
    Set Me.MPTemplates1.DBObj = g_oDBs
End Sub

Private Sub Form_Load()
    Set m_oDefs = g_oDBs.SegmentDefinitions
    m_oDefs.Refresh Word.ActiveDocument.AttachedTemplate
    GetAllowableOptions
    m_bCancelled = True
End Sub

Private Sub GetAllowableOptions()
'fills cmbLocations with available insertion
'locations for the selected Segment type

'   get location options
    Dim oArray As XArray
    Dim oDef As mpDB.CSegmentDef
    Dim iCurItem As Integer
    Dim xCurItem As String
    
    xCurItem = Me.cmbLocation.BoundText
    
    On Error Resume Next
    If Me.SelectedItem > 0 Then
        Set oDef = m_oDefs.Item(Me.SelectedItem)
    End If
    On Error GoTo ProcError
    
    If oDef Is Nothing Then
'       segment types may need to be refreshed
        m_oDefs.Refresh Word.ActiveDocument.AttachedTemplate
    
'       try again
        On Error Resume Next
'        Set oDef = m_oDefs.Item(Me.SelectedItem)
        
        If Me.SelectedItem > 0 Then
            Set oDef = m_oDefs.Item(Me.SelectedItem)
        End If
        
        
        On Error GoTo ProcError
    
        If oDef Is Nothing Then
            Me.btnOK.Enabled = False
            Exit Sub
        End If
    End If

'   create an empty xarray
    Set oArray = New XArray
    oArray.ReDim 0, 5, 0, 2
    Dim iUpper As Integer
    iUpper = -1
'   add items if definition allows for it - column 2
'   holds boolean to enable/disable "Use Page Setup of adjacent section"
    With oArray
        If oDef.AllowInStartSection Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "Start of Document in Separate Section"
            .Value(iUpper, 0) = mpSegmentLocations_NewFirstSection
            .Value(iUpper, 2) = False
        End If
        If oDef.AllowInEndSection Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "End of Document in Separate Section"
            .Value(iUpper, 0) = mpSegmentLocations_NewLastSection
            .Value(iUpper, 2) = oDef.AllowExistingHeaderFooters
        End If
        If oDef.AllowInCursorSection Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "At Insertion Point in Separate Section"
            .Value(iUpper, 0) = mpSegmentLocations_NewSectionAtSelection
            .Value(iUpper, 2) = oDef.AllowExistingHeaderFooters
        End If
        If oDef.AllowAtCursor Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "Insertion Point"
            .Value(iUpper, 0) = mpSegmentLocations_InsertionPoint
            .Value(iUpper, 2) = False
        End If
        If oDef.AllowAtEndOfDocument Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "End Of Document"
            .Value(iUpper, 0) = mpSegmentLocations_EndOfDocument
            .Value(iUpper, 2) = False
        End If
        If oDef.AllowAtStartOfDocument Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "Start Of Document"
            .Value(iUpper, 0) = mpSegmentLocations_StartOfDocument
            .Value(iUpper, 2) = False
        End If
        .ReDim 0, iUpper, 0, 2
    End With
    If oArray.Count(1) = 0 Then
        xDesc = "No allowable locations have been specified for this segment." & _
            "  Please check tblSegmentTypes in mpPublic.mdb."
        Err.Raise mpError_InvalidList
    End If
    
'   bind to control
    With Me.cmbLocation
        .Array = oArray
        .Rebind
        
'       attempt to select previous selection
        .BoundText = xCurItem
        
'       if attempt was not successful, select first item
        If .BoundText = "" Then
            .SelectedItem = 0
        End If
    End With
    
    ResizeTDBCombo Me.cmbLocation, 5

'   setup checkbox
    If Not oDef.AllowExistingHeaderFooters Then
        Me.chkUseExistingPageSetup.Value = False
    End If
    Me.chkUseExistingPageSetup.Enabled = _
        oDef.AllowExistingHeaderFooters
    Exit Sub
ProcError:
    RaiseError "MPO.frmInsertSegment.GetAllowableOptions", xDesc
End Sub

Private Sub MPTemplates1_SelDoubleClick(ByVal SelID As String)
    On Error GoTo ProcError
    GetAllowableOptions
    Me.btnOK.Enabled = Len(SelID)
    btnOK_Click
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub MPTemplates1_Selection(ByVal SelID As String)
    On Error GoTo ProcError
    GetAllowableOptions
    Me.btnOK.Enabled = Len(SelID)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


