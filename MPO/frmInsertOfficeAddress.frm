VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmOfficeAddresses 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Office Address"
   ClientHeight    =   3390
   ClientLeft      =   3825
   ClientTop       =   1080
   ClientWidth     =   5055
   Icon            =   "frmInsertOfficeAddress.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3390
   ScaleWidth      =   5055
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnSetDefault 
      Cancel          =   -1  'True
      Caption         =   "&Set Default"
      Height          =   400
      Left            =   1755
      TabIndex        =   7
      Top             =   2910
      Width           =   1050
   End
   Begin VB.OptionButton optSentence 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "Se&ntence"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   3540
      TabIndex        =   4
      Top             =   2430
      Width           =   1170
   End
   Begin VB.OptionButton optBlock 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "&Block"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   2310
      TabIndex        =   3
      Top             =   2430
      Width           =   810
   End
   Begin TrueDBList60.TDBList lstOffices 
      Height          =   2130
      Left            =   1800
      OleObjectBlob   =   "frmInsertOfficeAddress.frx":000C
      TabIndex        =   1
      Top             =   120
      Width           =   3135
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3975
      TabIndex        =   6
      Top             =   2910
      Width           =   1000
   End
   Begin VB.CommandButton btnInsert 
      Caption         =   "&Insert"
      Default         =   -1  'True
      Height          =   400
      Left            =   2910
      TabIndex        =   5
      ToolTipText     =   "Address inserted at cursor position"
      Top             =   2910
      Width           =   1000
   End
   Begin VB.Label lblStyle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Style:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   2
      Top             =   2460
      Width           =   1455
   End
   Begin VB.Label lblOffices 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Offices:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   0
      Top             =   165
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H80000003&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   4000
      Left            =   -15
      Top             =   -120
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmOfficeAddresses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oAuthor As mpDB.CPerson
Private m_bInit As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property


'**********************************************************
'   Internal Procedures
'**********************************************************

Private Sub Form_Activate()
    Me.Initializing = False
End Sub

Private Sub Form_Load()
    Me.Initializing = True
    
    With Me.lstOffices
        .Array = g_oDBs.Offices.ListSource
'        .Bookmark = g_oDBs.Offices.Default.ID - 1
'        .SelectedItem = .Bookmark
        DoEvents
    End With
    
    Me.optBlock.Value = True
    
End Sub

Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnInsert_Click()
    Dim rngInserted As Word.Range
    Dim xSep As String
    Dim xAddress As String
    Dim oDoc As New CDocument
    
    Me.Hide
    
    Application.ScreenRefresh
    
'*  get separator
    If Me.optSentence Then
        xSep = ", "
    Else
        xSep = vbCr
    End If

'*  get office address string
    With g_oDBs.Offices(Me.lstOffices.Bookmark + 1)
        xAddress = GetMacPacIni("Firm", "Name") & _
                                xSep & .Address(xSep)
    End With
    
'*  insert in doc, creating space if necessary
    oDoc.InsertTextAfter xAddress
    
    Set oDoc = Nothing
    
    Unload Me
End Sub

Private Sub btnSetDefault_Click()
    With Me
    
        SetMacPacIni "Firm", _
                     "Default Office", _
                     .lstOffices.Bookmark + 1
                     
        .btnCancel.Caption = "Close"
        .btnInsert.SetFocus
    End With
End Sub
