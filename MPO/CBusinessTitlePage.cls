VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessTitlePage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTitlePage Class
'   created 3/21/00 by Jeffrey Sweetland
'   edited 4/4/06 by Charlie Homo
'
'**********************************************************
Const mpTitlePageBookmark As String = "zzmpFIXED_TitlePage"
Const mpDocSegment As String = "TitlePage"
Private m_oProps As MPO.CCustomProperties   '*c for Hanson

Private m_xBookmarkName As String
Private m_oDoc As CDocument
Private m_oDef As CBusinessTitlePageDef

'**********************************************************
'   Properties
'**********************************************************
Public Property Get BusinessTypeID() As Long
    BusinessTypeID = m_oDoc.RetrieveItem("BusinessTypeID", "Business")
End Property

Public Property Get DocumentTitle() As String
'   check for existing Title Page value
    DocumentTitle = m_oDoc.RetrieveItem("DocumentTitle", mpDocSegment)
    If DocumentTitle = "" Then
'           check for document title
        DocumentTitle = m_oDoc.RetrieveItem("DocumentTitle", "Business")
    End If
End Property
Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function
Public Property Let DocumentTitle(xNew As String)
    If g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpTitlePageDocumentTitle", xNew
        m_oDoc.SaveItem "DocumentTitle", mpDocSegment, , xNew
    End If
End Property

Public Property Get PartyName() As String
    PartyName = m_oDoc.RetrieveItem("PartyName", mpDocSegment)
End Property

Public Property Let PartyName(xNew As String)
    If g_bForceItemUpdate Then
        m_oDoc.SaveItem "PartyName", mpDocSegment, , xNew
        m_oDoc.EditItem "zzmpTitlePagePartyName", xNew
    End If
End Property

Public Property Get PartyName2() As String
    PartyName2 = m_oDoc.RetrieveItem("PartyName2", mpDocSegment)
End Property

Public Property Let PartyName2(xNew As String)
    If g_bForceItemUpdate Then
        m_oDoc.SaveItem "PartyName2", mpDocSegment, , xNew
        m_oDoc.EditItem "zzmpTitlePagePartyName2", xNew
    End If
End Property

Public Property Get PartyName3() As String
    PartyName3 = m_oDoc.RetrieveItem("PartyName3", mpDocSegment)
End Property

Public Property Let PartyName3(xNew As String)
    If g_bForceItemUpdate Then
        m_oDoc.SaveItem "PartyName3", mpDocSegment, , xNew
        m_oDoc.EditItem "zzmpTitlePagePartyName3", xNew
    End If
End Property

Public Property Let DateType(xNew As String)
    If Me.DateType <> xNew Or g_bForceItemUpdate Then
        If xNew = "None" Then
            m_oDoc.EditItem "zzmpTitlePageDate", "", , True, , , True
        Else
            m_oDoc.InsertDate "zzmpTitlePageDate", xNew, True
        End If
        m_oDoc.SaveItem "DateType", mpDocSegment, , xNew
    End If

End Property

Public Property Get DateType() As String
    On Error Resume Next
    DateType = m_oDoc.RetrieveItem("DateType", mpDocSegment)
End Property

Public Property Let Location(iNew As mpDocPositions)
    If g_bForceItemUpdate Then
        m_oDoc.SaveItem "Location", mpDocSegment, , iNew
    End If
End Property

Public Property Get Location() As mpDocPositions
    Location = m_oDoc.RetrieveItem("Location", mpDocSegment)
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Definition.Boilerplate
End Property

Public Property Let TypeID(lNew As Long)
    Dim xDesc As String

    On Error GoTo ProcError
    
    If Me.TypeID <> lNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "TypeID", mpDocSegment, , lNew
        Set m_oDef = g_oDBs.BusinessTitlePageDefs.Item(lNew)

    '       alert and exit if not a valid Type
        If (m_oDef Is Nothing) Then
            Err.Raise mpError_InvalidMember
        End If
    End If
    
    Exit Property

ProcError:
    If Err.Number = mpError_InvalidMember Then
        xDesc = "Invalid Business Type TypeID."
    Else
        xDesc = g_oError.Desc(Err.Number)
    End If
    RaiseError "MPO.CBusinessTitlePage.TypeID", xDesc
End Property

Public Property Get TypeID() As Long
    On Error Resume Next
    TypeID = m_oDoc.RetrieveItem("TypeID", mpDocSegment, , mpItemType_Integer)
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Function Definition() As mpDB.CBusinessTitlePageDef
    If (m_oDef Is Nothing) Then
        Set m_oDef = g_oDBs.BusinessTitlePageDefs.Item(Me.TypeID)
    End If
    Set Definition = m_oDef
End Function

Public Function BoilerplateExists() As Boolean
    If m_oDoc.File.Bookmarks.Exists(m_xBookmarkName) Then
        BoilerplateExists = True
    Else
        BoilerplateExists = False
    End If
End Function

Public Sub Finish()
    Dim bk As Word.Bookmark
    Dim oRng As Word.Range
    Dim bSaved As Boolean
    Dim oSel As Word.Range
    Dim oFT, oFT1, oAT As Word.AutoTextEntry
    Dim lview As Long
    
    On Error GoTo ProcError
    
    With m_oDoc
        If Not BoilerplateExists Then
        
''***        new code
            bSaved = Word.NormalTemplate.Saved
            lview = .File.ActiveWindow.View
    '       insert at eof, then move to final location later-
    '       this is the only way that Word will bring
    '       in headers and footers
            Set oSel = Selection.Range
            If Me.Location <> mpDocPosition_EOF Then
                .Range.InsertParagraphAfter
                .Range.Paragraphs.Last.Range.Style = wdStyleNormal
            End If
            .CreateSection mpDocPosition_EOF
            .Selection.EndOf wdStory
    '       Insert bp
            .InsertBoilerplate m_oDef.Boilerplate, ""
            .File.Characters.Last.Delete
            
    '       create temp autotext
            Set oRng = Word.ActiveDocument.Sections.Last.Range
            Set oAT = Word.NormalTemplate.AutoTextEntries.Add("zzmpTempTitlePage", oRng)
            If Me.Location <> mpDocPosition_EOF Then
                Set oFT = Word.NormalTemplate.AutoTextEntries.Add("zzmpTempTitlePageFooterPrimary", _
                    .File.Sections.Last.Footers(wdHeaderFooterPrimary).Range)
                If .File.Sections.Last.PageSetup.DifferentFirstPageHeaderFooter Then
                    Set oFT1 = Word.NormalTemplate.AutoTextEntries.Add("zzmpTempTitlePageFooterFirst", _
                        .File.Sections.Last.Footers(wdHeaderFooterPrimary).Range)
                End If
    '           Link last Headers and Footer to previous
    '           section then unlink, otherwise existing text
    '           will get zapped
                Set oRng = .File.Sections.Last.Range
                .LinkHeadersFooters oRng.Sections.Last
                .UnlinkHeadersFooters oRng.Sections.Last
                oRng.MoveStart wdCharacter, -1
                oRng.Delete
            End If
    '       create actual location if necessary
            Select Case Me.Location
                Case mpDocPosition_BOF
    '               insert at eof, then move to bof later-
    '               this is the only way that Word will bring
    '               in headers and footers
                    .CreateSection mpDocPosition_BOF
                    Set oRng = Word.ActiveDocument.Content
                    oRng.StartOf
                    oRng.Sections(1).PageSetup.TextColumns.SetCount 1   '9.7.1 - 3013
                    Word.NormalTemplate.AutoTextEntries("zzmpTempTitlePage").Insert oRng, True
                    On Error Resume Next
                    Word.NormalTemplate.AutoTextEntries("zzmpTempTitlePageFooterPrimary").Insert .File.Sections(1).Footers(wdHeaderFooterPrimary).Range, True
                    ' Fill in Footers from AutoText
                    .File.Sections(1).Footers(wdHeaderFooterPrimary).Range.Characters.Last.Delete
                    If .File.Sections(1).PageSetup.DifferentFirstPageHeaderFooter Then
                        Word.NormalTemplate.AutoTextEntries("zzmpTempTitlePageFooterFirst").Insert .File.Sections(1).Footers(wdHeaderFooterFirstPage).Range, True
                        .File.Sections(1).Footers(wdHeaderFooterFirstPage).Range.Characters.Last.Delete
                    End If
                    oRng.EndOf wdSection
                    'style section break with Normal style  #2255
                    oRng.Style = wdStyleNormal
                    oRng.Move wdCharacter, -1
                    oRng.Style = wdStyleNormal
                    '#3434
                    With ActiveDocument.Sections(2).Footers(wdHeaderFooterFirstPage) _
                        .PageNumbers
                            .RestartNumberingAtSection = True
                            .StartingNumber = 1
                    End With
                Case mpDocPosition_EOF
                    Set oRng = Word.ActiveDocument.Content
                    oRng.InsertParagraphAfter
                    oRng.EndOf
                    oRng.Sections(1).PageSetup.TextColumns.SetCount 1   '9.7.1 - 3013
                    oRng.Style = wdStyleNormal
                Case mpDocPosition_Selection
                    ' Create Section break on each end of Exhibits
                    oSel.Select
                    .CreateSection mpDocPosition_Selection
                    ' Preserve Header/footers of following section
                    .UnlinkHeadersFooters .Selection.Sections(1)
                    .Selection.Move wdCharacter, -1
                    .CreateSection mpDocPosition_Selection
                    ' Clear and Unlink headers for Exhibit Section
                    .ClearHeaders .Selection.Sections(1), True
                    .ClearFooters .Selection.Sections(1), True
                    ' Reset style of section break
                    .Selection.InsertParagraphAfter
                    .Selection.Style = wdStyleNormal
                    Set oRng = .Selection.Sections(1).Range
                    oRng.StartOf
                    Word.NormalTemplate.AutoTextEntries("zzmpTempTitlePage").Insert oRng, True
                    On Error Resume Next
                    Word.NormalTemplate.AutoTextEntries("zzmpTempTitlePageFooterPrimary").Insert oRng.Sections(1).Footers(wdHeaderFooterPrimary).Range, True
                    ' Fill in Footers from AutoText
                    oRng.Sections(1).Footers(wdHeaderFooterPrimary).Range.Characters.Last.Delete
                    If oRng.Sections(1).PageSetup.DifferentFirstPageHeaderFooter Then
                        Word.NormalTemplate.AutoTextEntries("zzmpTempTitlePageFooterFirst").Insert oRng.Sections(1).Footers(wdHeaderFooterFirstPage).Range, True
                        oRng.Sections(1).Footers(wdHeaderFooterFirstPage).Range.Characters.Last.Delete
                    End If
                    oRng.Sections(1).PageSetup.TextColumns.SetCount 1   '9.7.1 - 3013
                    oRng.EndOf wdSection
                    oRng.Move wdCharacter, -1
            End Select

            With Word.ActiveDocument.ActiveWindow
                If .View.SplitSpecial <> wdPaneNone Then
                    .View.SplitSpecial = wdPaneNone
                    .View = lview
                End If
            End With
            
            With oRng.Sections(1).Footers(wdHeaderFooterFirstPage).PageNumbers
                .RestartNumberingAtSection = True
                .StartingNumber = 1
            End With
                        
            On Error Resume Next
    '       delete autotext
            With Word.NormalTemplate.AutoTextEntries
                .Item("zzmpTempTitlePage").Delete
                .Item("zzmpTempTitlePageFooterPrimary").Delete
                .Item("zzmpTempTitlePageFooterFirst").Delete
            End With

    '       return normal template dirt
            Word.NormalTemplate.Saved = bSaved
        Else
            Set oRng = .File.Bookmarks(m_xBookmarkName).Range
            With oRng
'**************************************
'per log item 1819 - df - 010820
                If .Characters.Last = vbCr Then
                    .MoveEnd wdCharacter, -1
                End If
'**************************************
                .Delete
                .Select
            End With
            m_oDoc.InsertBoilerplate Me.BoilerplateFile, mpTitlePageBookmark
        End If
        
        On Error Resume Next
        Set oRng = ActiveDocument.Bookmarks(m_xBookmarkName).Range
        On Error GoTo ProcError
        
        If Not (oRng Is Nothing) Then
'           apply page setup properties
            With oRng.Sections(1).PageSetup
                
'               apply orientation
                .Orientation = m_oDef.Orientation
        
'               apply margins
                .PageHeight = m_oDef.PageHeight * 72
                .PageWidth = m_oDef.PageWidth * 72
                
                .LeftMargin = m_oDef.LeftMargin * 72
                .RightMargin = m_oDef.RightMargin * 72
                .TopMargin = m_oDef.TopMargin * 72
                .BottomMargin = m_oDef.BottomMargin * 72
        
        '       apply vertical alignment
                .VerticalAlignment = m_oDef.VerticalAlignment
            End With
        End If
        
        Refresh
        
'       remove extra para and select start of Title Page
        If m_oDoc.File.Bookmarks.Exists("zzmpFixed_TitlePage") Then
            Set bk = m_oDoc.File.Bookmarks("zzmpFixed_TitlePage")
            With bk.Range
                .MoveEnd wdParagraph, 1
                .Paragraphs.Last.Range.Delete
                .Collapse wdCollapseStart
                .Select
            End With
        End If
        
'      delete any hidden text
        .DeleteHiddenText
        .RemoveFormFields
        .ClearBookmarks
        
    End With
    Exit Sub
    
ProcError:
    g_bForceItemUpdate = False
    RaiseError "MPO.CBusinessTitlePage.Finish"
    Exit Sub
End Sub

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
    Set m_oProps = New MPO.CCustomProperties
    m_oProps.Category = "TitlePage"
    'set bookmark name
    m_xBookmarkName = mpTitlePageBookmark
    If m_oDoc.bHideFixedBookmarks Then
        m_oDoc.HideBookmark (m_xBookmarkName)
        m_xBookmarkName = "_" & m_xBookmarkName
    End If
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
    Set m_oProps = Nothing
End Sub

Private Sub Refresh()
    g_bForceItemUpdate = True
    With Me
        .DocumentTitle = .DocumentTitle
        .DateType = .DateType
        .Location = .Location
        .PartyName = .PartyName
        .PartyName2 = .PartyName2
        .PartyName3 = .PartyName3
        .CustomProperties.RefreshValues
End With
    g_bForceItemUpdate = False
End Sub
