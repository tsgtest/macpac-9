VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CExhibits"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CExhibits Class
'   created 6/30/00 by Jeffrey Sweetland
'
'**********************************************************
Const mpExhibitsBPFile As String = "BusinessExhibits.mbp"
Const mpSegment As String = "Exhibits"

Private m_colExhibits As Collection
Private m_iLocation As Integer
Private m_bFormatPageNumber As Boolean
Private m_oDoc As CDocument

Public Property Let Location(iNew As mpDocPositions)
    m_iLocation = iNew
    m_oDoc.SaveItem "Location", mpSegment, , m_iLocation
End Property
Public Property Get Location() As mpDocPositions
    On Error Resume Next
    If m_iLocation = 0 Then _
        m_iLocation = m_oDoc.RetrieveItem("Location", mpSegment)
    Location = m_iLocation
End Property
Public Property Get BusinessTypeID() As Long
    BusinessTypeID = m_oDoc.RetrieveItem("BusinessTypeID", "Business")
End Property
Public Property Let FormatPageNumber(bNew As Boolean)
    m_bFormatPageNumber = bNew
    m_oDoc.SaveItem "FormatPageNumber", mpSegment, , m_bFormatPageNumber
End Property

Public Property Get FormatPageNumber() As Boolean
    m_bFormatPageNumber = m_oDoc.RetrieveItem("FormatPageNumber", mpSegment)
    FormatPageNumber = m_bFormatPageNumber
End Property
Public Function AddItem(vNumber, vTitle)
    Dim oItem As CMPItem
    Set oItem = New CMPItem
    
    oItem.Value = vNumber
    oItem.Description = vTitle
    
    m_colExhibits.Add oItem
    
    Set oItem = Nothing
End Function
Public Property Get Item(vKey) As CMPItem
Attribute Item.VB_UserMemId = 0
    On Error GoTo ProcError
    Set Item = m_colExhibits(vKey)
    Exit Property
ProcError:
    g_oError.Raise mpError_InvalidItemIndex, "CExhibits.Item"
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "440"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = m_colExhibits.[_NewEnum]
End Property
Public Sub Finish()
    Dim oAT As Word.AutoTextEntry
    Dim oFT As Word.AutoTextEntry
    Dim oFT1 As Word.AutoTextEntry
    Dim bSaved As Boolean
    Dim oItem As CMPItem
    Dim oRng As Word.Range
    Dim oSel As Word.Range
    Dim i As Integer
    Dim oPS As Word.PageSetup
    Dim oBusDef As mpDB.CBusinessDef
    Dim oRngFind As Word.Range
    Dim xTemp As String
    
    If Me.Count = 0 Then Exit Sub
    
    bSaved = Word.NormalTemplate.Saved
    On Error GoTo ProcError
    With m_oDoc
'       insert at eof, then move to final location later-
'       this is the only way that Word will bring
'       in headers and footers
        Set oSel = Selection.Range
'        If Me.Location <> mpDocPosition_EOF Then
            .Range.InsertParagraphAfter
            .Range.Paragraphs.Last.Range.Style = wdStyleNormal
'        End If
        .CreateSection mpDocPosition_EOF
        .Selection.EndOf wdStory
'       Insert bp
        .InsertBoilerplate mpExhibitsBPFile, ""
        .EditItem "zzmpExhibitNumber", Item(1).Value, , False
        .EditItem "zzmpExhibitTitle", Item(1).Description, , False

'       dirty fix to remove extra space after exhibit title
        Dim r As Word.Range
        Set r = m_oDoc.File.Bookmarks("zzmpExhibitTitle").Range
        If Not (r Is Nothing) Then
            With r
                .Delete
                .MoveEnd
                If .Text = " " Then
                    .Text = ""
                Else
                    .MoveEnd wdCharacter, -1
                End If
'               insert text
                .InsertAfter xSubstitute(Item(1).Description, vbCrLf, Chr(11))
'               reset bookmark
                m_oDoc.File.Bookmarks.Add "zzmpExhibitTitle", r
            End With
        End If

'       create temp autotext
        Set oRng = Word.ActiveDocument.Sections.Last.Range
        If Right(oRng.Text, 3) = vbCr & vbCr & vbCr Then
            oRng.Characters.Last.Delete
        End If
        Set oAT = Word.NormalTemplate.AutoTextEntries.Add("zzmpTempExhibit", oRng)
        If Me.Location <> mpDocPosition_EOF Then
            Set oFT = Word.NormalTemplate.AutoTextEntries.Add("zzmpTempExhibitFooterPrimary", _
                .File.Sections.Last.Footers(wdHeaderFooterPrimary).Range)
            If .File.Sections.Last.PageSetup.DifferentFirstPageHeaderFooter Then
                Set oFT1 = Word.NormalTemplate.AutoTextEntries.Add("zzmpTempExhibitFooterFirst", _
                    .File.Sections.Last.Footers(wdHeaderFooterPrimary).Range)
            End If
'           Link last Headers and Footer to previous
'           section then unlink, otherwise existing text
'           will get zapped
            Set oRng = .File.Sections.Last.Range
            .LinkHeadersFooters oRng.Sections.Last
            .UnlinkHeadersFooters oRng.Sections.Last
            oRng.MoveStart wdCharacter, -1
            oRng.Delete
        End If
        On Error GoTo ProcError
'       create actual location if necessary
        Select Case Me.Location
            Case mpDocPosition_BOF
'               insert at eof, then move to bof later-
'               this is the only way that Word will bring
'               in headers and footers
                .CreateSection mpDocPosition_BOF
                Set oRng = Word.ActiveDocument.Content
                oRng.StartOf
                oRng.Sections(1).PageSetup.TextColumns.SetCount 1   '9.7.1 - 3013
                Word.NormalTemplate.AutoTextEntries("zzmpTempExhibit").Insert oRng, True
                On Error Resume Next
                Word.NormalTemplate.AutoTextEntries("zzmpTempExhibitFooterPrimary").Insert .File.Sections(1).Footers(wdHeaderFooterPrimary).Range, True
                ' Fill in Footers from AutoText
                .File.Sections(1).Footers(wdHeaderFooterPrimary).Range.Characters.Last.Delete
                .File.Sections(1).PageSetup.VerticalAlignment = wdAlignVerticalTop   '9.7.2010 #4731

                If .File.Sections(1).PageSetup.DifferentFirstPageHeaderFooter Then
                    Word.NormalTemplate.AutoTextEntries("zzmpTempExhibitFooterPrimary").Insert .File.Sections(1).Footers(wdHeaderFooterFirstPage).Range, True
                    .File.Sections(1).Footers(wdHeaderFooterFirstPage).Range.Characters.Last.Delete
                End If
                oRng.EndOf wdSection
                oRng.Move wdCharacter, -1
            Case mpDocPosition_EOF
                Set oRng = Word.ActiveDocument.Content
                With oRng
                    .InsertParagraphAfter
                    .EndOf
                    With .Sections(1).PageSetup
                        .TextColumns.SetCount 1   '9.7.1 - 3013
                        .VerticalAlignment = wdAlignVerticalTop   '9.7.3 #4673
                    End With
                    .Style = wdStyleNormal
                End With
            Case mpDocPosition_Selection
                ' Create Section break on each end of Exhibits
                oSel.Select
                .CreateSection mpDocPosition_Selection
                ' Preserve Header/footers of following section
                .UnlinkHeadersFooters .Selection.Sections(1)
                .Selection.Move wdCharacter, -1
                .CreateSection mpDocPosition_Selection
                ' Clear and Unlink headers for Exhibit Section
                .ClearHeaders .Selection.Sections(1), True
                .ClearFooters .Selection.Sections(1), True
                ' Reset style of section break
                .Selection.InsertParagraphAfter
                .Selection.Style = wdStyleNormal
                Set oRng = .Selection.Sections(1).Range
                oRng.StartOf
                oRng.Sections(1).PageSetup.TextColumns.SetCount 1   '9.7.1 - 3013
                Word.NormalTemplate.AutoTextEntries("zzmpTempExhibit").Insert oRng, True
                On Error Resume Next
                Word.NormalTemplate.AutoTextEntries("zzmpTempExhibitFooterPrimary").Insert oRng.Sections(1).Footers(wdHeaderFooterPrimary).Range, True
                ' Fill in Footers from AutoText
                oRng.Sections(1).Footers(wdHeaderFooterPrimary).Range.Characters.Last.Delete
                If oRng.Sections(1).PageSetup.DifferentFirstPageHeaderFooter Then
                    Word.NormalTemplate.AutoTextEntries("zzmpTempExhibitFooterPrimary").Insert oRng.Sections(1).Footers(wdHeaderFooterFirstPage).Range, True
                    oRng.Sections(1).Footers(wdHeaderFooterFirstPage).Range.Characters.Last.Delete
                End If
                oRng.EndOf wdSection
                oRng.Move wdCharacter, -1
        End Select
        
        If Word.ActiveWindow.View.SplitSpecial <> wdPaneNone Then
            Word.ActiveWindow.View.SplitSpecial = wdPaneNone
        End If
        
        With oRng.Sections(1).Footers(wdHeaderFooterFirstPage).PageNumbers
            .RestartNumberingAtSection = True
            .StartingNumber = 1
            .NumberStyle = wdPageNumberStyleArabic      '#3981
        End With

        On Error Resume Next
        Set oBusDef = g_oDBs.BusinessDefs(Me.BusinessTypeID)
        On Error GoTo ProcError

        If Not (oBusDef Is Nothing) Then
            Set oPS = oRng.Sections(1).PageSetup
            With oBusDef
        '       apply orientation & margins
                oPS.Orientation = .Orientation
                oPS.LeftMargin = .LeftMargin * 72
                oPS.RightMargin = .RightMargin * 72
                oPS.TopMargin = .TopMargin * 72
                oPS.BottomMargin = .BottomMargin * 72
                oPS.DifferentFirstPageHeaderFooter = .DifferentPage1HeaderFooter
                oPS.PageHeight = .PageHeight * 72
                oPS.PageWidth = .PageWidth * 72
                
            End With
        End If
                
        If m_bFormatPageNumber Then
            Dim fldPageNum As Word.Field
            Dim rngPageNum As Word.Range
            Dim oPageNum As Word.PageNumber
            Dim oDoc As New CDocument
            Dim xStr1 As String
            Dim xStr2 As String
            Dim oArray As XArray

            'put Exhibits into an XArray
            Set oArray = New XArray
            With oArray
                .ReDim 0, Me.Count - 1, 0, 0
                For i = 1 To Me.Count
                    mpBase2.xSplitString Item(i).Value, xStr1, xStr2, " "
                   .Value(i - 1, 0) = xStr2
                Next i
            End With

            '9.7.1 #4309
            For Each fldPageNum In oRng.Sections(1).Footers(wdHeaderFooterFirstPage).Range.Fields
                If fldPageNum.Type = wdFieldPage Then
                    Set rngPageNum = rngGetField(fldPageNum.Code)
                    With rngPageNum
                        'cleanup before page number
                        .MoveStart , -1
                        If Left(.Text, 1) <> vbTab Then
                            If Left(.Text, 1) = " " Then
                                .Characters.First.Delete
                                .MoveStart , -1
                            End If
                            If Left(.Text, 1) = "-" Then
                                .Characters.First.Delete
                            End If
                        Else
                            .MoveStart
                        End If
                        'cleanup after page number
                        .MoveEnd , 2
                        If Right(.Text, 1) = " " Then
                            .Characters.Last.Delete
                            .MoveEnd
                        End If
                        If Right(.Text, 1) = "-" Then
                            .Characters.Last.Delete
                        End If
                        .InsertBefore oArray(0, 0) & mpBase2.GetMacPacIni("Exhibits", "PageNumberSeparator")
                        .Style = m_oDoc.File.Styles(wdStylePageNumber) '***4452
                    End With
                    Exit For
                End If
            Next fldPageNum
            Set fldPageNum = Nothing
            For Each fldPageNum In oRng.Sections(1).Footers(wdHeaderFooterPrimary).Range.Fields
                If fldPageNum.Type = wdFieldPage Then
                    Set rngPageNum = rngGetField(fldPageNum.Code)
                    With rngPageNum
                        'cleanup before page number
                        .MoveStart , -1
                        If Left(.Text, 1) <> vbTab Then
                            If Left(.Text, 1) = " " Then
                                .Characters.First.Delete
                                .MoveStart , -1
                            End If
                            If Left(.Text, 1) = "-" Then
                                .Characters.First.Delete
                            End If
                        Else
                            .MoveStart
                        End If
                        'cleanup after page number
                        .MoveEnd , 2
                        If Right(.Text, 1) = " " Then
                            .Characters.Last.Delete
                            .MoveEnd
                        End If
                        If Right(.Text, 1) = "-" Then
                            .Characters.Last.Delete
                        End If
                        .InsertBefore oArray(0, 0) & mpBase2.GetMacPacIni("Exhibits", "PageNumberSeparator")
                        .Style = m_oDoc.File.Styles(wdStylePageNumber) '***4452
                    End With
                End If
            Next fldPageNum


        End If

        For i = 2 To m_colExhibits.Count
            .ClearBookmarks
            oRng.Style = wdStyleNormal
            
'            oRng.InsertBefore Chr(12)
'           insert section break (not page break as previously implemented)
            Dim oRngDupe As Word.Range
            Set oRngDupe = oRng.Duplicate
            oRngDupe.StartOf
            oRngDupe.InsertBreak wdSectionBreakNextPage
'***
            If m_bFormatPageNumber Then
                m_oDoc.UnlinkHeadersFooters oRngDupe.Sections.Last
            End If
            oRng.MoveStart wdCharacter
            Word.NormalTemplate.AutoTextEntries("zzmpTempExhibit").Insert oRng, True
            .EditItem "zzmpExhibitNumber", Item(i).Value, , False
            .EditItem "zzmpExhibitTitle", Item(i).Description, , False
            
            If m_bFormatPageNumber Then
                mpBase2.xSplitString Item(i).Value, xStr1, xStr2, " "
                
                'first footers
                Set oRngFind = oRngDupe.Sections.Last.Footers(wdHeaderFooterFirstPage).Range
                oRngFind.WholeStory
                With oRngFind.Find
                    .ClearFormatting
                    .Text = oArray(i - 2, 0)
                    .Replacement.Text = oArray(i - 1, 0)
                    .Execute Replace:=wdReplaceOne '***4453
                End With
    
                'do primary footers
                Set oRngFind = oRngDupe.Sections.Last.Footers(wdHeaderFooterPrimary).Range
                oRngFind.WholeStory
                With oRngFind.Find
                    .ClearFormatting
                    .Text = oArray(i - 2, 0)
                    .Replacement.Text = oArray(i - 1, 0)
                    .Execute Replace:=wdReplaceOne '***4453
                End With
            End If
            
            oRng.Sections(1).PageSetup.VerticalAlignment = wdAlignVerticalTop   '9.7.2010 #4731
            
            oRng.EndOf wdSection
            If oRng.Characters.First = Chr(12) Then
                oRng.Move wdCharacter, -1
            End If
        Next i
        
        If oRng.Characters.First = vbCr Then _
            oRng.Characters.First.Delete
            
'       set "Exhibit Heading" style's name property to match Normal's
        With .File.Styles("Exhibit Heading").Font
            .Name = m_oDoc.File.Styles(wdStyleNormal).Font.Name        '#3854/#3997 - 9.6.2
            .Size = m_oDoc.File.Styles(wdStyleNormal).Font.Size
        End With

'9.7.1 #4452
''       set "Page Number" style's name property to match Footer's
'        .File.Styles(wdStylePageNumber).Font.Size = _
'            .File.Styles(wdStyleFooter).Font.Size        '#3998 - 9.6.2

'      delete any hidden text
        .DeleteHiddenText , True, , True
        .RemoveFormFields
        .ClearBookmarks
        
        On Error Resume Next
'       delete autotext
        Word.NormalTemplate.AutoTextEntries("zzmpTempExhibit").Delete
        Word.NormalTemplate.AutoTextEntries("zzmpTempExhibitFooterPrimary").Delete
        Word.NormalTemplate.AutoTextEntries("zzmpTempExhibitFooterFirst").Delete
        
        On Error GoTo ProcError
        If Me.Location = mpDocPosition_BOF Then
            Selection.StartOf wdStory
        Else
            Selection.StartOf wdSection
        End If
        
        '#3646 & 9.7.1 - #4191
        With Selection
            .Move wdParagraph
            'force body text style
            .Paragraphs(1).Style = wdStyleBodyText
            '****NEW LINES****
            ' clean up trailing empty paragraph if necessary
            If Me.Location = mpDocPosition_EOF Then
                With ActiveDocument.Sections.Last.Range
                    If .Paragraphs.Last.Range.Text = vbCr And _
                        .Paragraphs.Last.Range.Style = Word.ActiveDocument.Styles(wdStyleNormal) Then
                        .Paragraphs.Last.Range.Delete
                    End If
                End With
            End If
            '****END NEW LINES****
        End With
        
        
'       return normal template dirt
        Word.NormalTemplate.Saved = bSaved
    End With
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, "CExhibits.Finish"

End Sub
Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
    Set m_colExhibits = New Collection
End Sub
Private Sub Class_Terminate()
    Set m_oDoc = Nothing
    Set m_colExhibits = Nothing
End Sub
Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function
Public Function Count() As Integer
    Count = m_colExhibits.Count
End Function
Private Function rngGetField(rngFieldCode As Word.Range) As Word.Range
'returns the range of the field
'whose code is rngFieldCode

    Dim lStartPos As Long
    Dim lEndPos As Long
    
    With rngFieldCode
        lStartPos = .Start
        lEndPos = .End
    End With
    
    rngFieldCode.SetRange lStartPos - 1, lEndPos + 1
    Set rngGetField = rngFieldCode
End Function
