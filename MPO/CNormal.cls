VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNormal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Normal Class
'   created 3/29/00  by Charlie Homo -
'   charliehomo@pacbell.net

'   Contains properties and methods that
'   define the Normal Template

'**********************************************************

Private m_oFSO As Scripting.FileSystemObject

'************
'Properties
'************

Public Property Get Path() As String
    Path = Word.NormalTemplate.FullName
End Property

Public Property Get StyPath() As String
    StyPath = mpBase2.ApplicationDirectory & "\Normal.sty"
End Property

'************
'Methods
'************

Public Function OpenAsDocument() As Word.Document
    Set OpenAsDocument = Word.Documents.Open(Me.Path)
End Function


Public Function UpdateRequired()
'   returns TRUE if a new Normal.sty has been distributed

    Dim bNewSty As Boolean
    Dim oFile As Scripting.File
    Dim xOldStyVersion As String
    Dim xNewStyVersion As String
    Dim xMsg As String
    
    On Error GoTo ProcError
        
    On Error Resume Next
    Set oFile = m_oFSO.GetFile(Me.StyPath)
    On Error GoTo ProcError
    
    If oFile Is Nothing Then
        Err.Clear
        xMsg = "The following file required by MacPac could not be found: '" & Me.StyPath & _
               "'.  Please contact your administrator"
        MsgBox xMsg, vbExclamation, App.Title
        Exit Function
    End If
    
'   get last edit and open time of this file-
'   the concatenated string is the "version id"
    xNewStyVersion = CDbl(oFile.DateLastModified)
    
'   the old version id was written to the ini when the
'   template defaults were set
    xOldStyVersion = GetUserIniNumeric(NormalTemplate.Name, "StyVersion") '*c
    
'   an updated Normal.sty has arrived if two ids don't match
    bNewSty = (xNewStyVersion <> xOldStyVersion)
    
    UpdateRequired = bNewSty
    
    Exit Function
ProcError:
    RaiseError "MPO.CNormal.UpdateRequired"
    Exit Function
End Function

Public Sub ResetStyles()
'updates Normal.dot's styles to defaults in 'Normal.sty'

    Dim docNormal As Word.Document
    Dim oFile As Scripting.File
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    On Error Resume Next
    Set oFile = m_oFSO.GetFile(Me.StyPath)
    On Error GoTo ProcError
    
    If oFile Is Nothing Then
        Err.Clear
        xMsg = "The following file required by MacPac could not be found: '" & Me.StyPath & _
               "'.  Please contact your administrator"
        MsgBox xMsg, vbExclamation, App.Title
        Exit Sub
    End If
    
    Application.ScreenUpdating = False
    
    Application.StatusBar = "Resetting styles in the Normal template.  " & _
                            "Please wait..."

    EchoOff
    Set docNormal = Me.OpenAsDocument
    With docNormal
        .CopyStylesFromTemplate Me.StyPath
        'retro0705
        .Save
        .Close
    End With
    EchoOn
    
'   mark version of Normal.sty file
    SetUserIni NormalTemplate.Name, "StyVersion", _
        CDbl(oFile.DateLastModified)

    Application.ScreenUpdating = True
    
    Exit Sub
ProcError:
    EchoOn
    RaiseError "MPO.CNormal.ResetStyles"
    Exit Sub
End Sub


Private Sub Class_Initialize()
    Set m_oFSO = New Scripting.FileSystemObject
End Sub

Private Sub Class_Terminate()
    Set m_oFSO = Nothing
End Sub
