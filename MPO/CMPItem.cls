VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMPItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CMPItem Class
'   created 12/21/99 by Jeffrey Sweetland
'   Contains properties and methods that
'   define a generic item Class -
'   meant to be used through a collection class for any
'   variable size group if items consisting of a label or
'   or description and a value (Recipient fields, Pleading
'   Caption items, etc.)
'
'**********************************************************
Public Event Changed(xField As String)

Private m_xDescription As String
Private m_vValue As Variant
Public Property Get Description() As String
    Description = m_xDescription
End Property
Public Property Let Description(xNew As String)
    If Description <> xNew Then
        m_xDescription = xNew
        RaiseEvent Changed("Description")
    End If
End Property
Public Property Get Value() As Variant
Attribute Value.VB_UserMemId = 0
    If IsObject(m_vValue) Then
        Set Value = m_vValue
    Else
        Value = m_vValue
    End If
End Property
Public Property Let Value(vNew)
    If m_vValue <> vNew Then
        m_vValue = vNew
        RaiseEvent Changed("Value")
    End If
End Property
