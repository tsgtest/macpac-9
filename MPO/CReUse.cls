VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CReUse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CReUse Class
'   created 11/16/99 by Daniel Fisherman
'   Contains properties and methods that
'   define the CReUse Class - those
'   procedures that implement MacPac ReUse functionality
'**********************************************************

Public Enum mpReUseAction
    mpReUseAction_Create = 1
    mpReUseAction_Replace = 2
    mpReUseAction_Append = 3
End Enum

Private Const mpDocVarCopyTag As String = "ReuseCopy_"
Private Const mpAutoTextCopyTag As String = "zzmpReuse"
Private Const mpReuseSelection As String = "zzmpReuseText"

Private m_iAction As mpReUseAction
Private m_oDoc As MPO.CDocument
Private m_bSaved As Boolean
Private m_bIncludeSelText As Boolean
Private m_bSkipStyles As Boolean

Public Event AfterSetupDocument()
Public Event BeforeSetupDocument()
Public Event AfterCleanup()
Public Event BeforeCleanup()
Public Event AfterRestoreDocument()
Public Event BeforeRestoreDocument()

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Action() As mpReUseAction
    If m_iAction = 0 Then
        m_iAction = mpReUseAction_Create
    End If
    Action = m_iAction
End Property

Public Property Let Action(iNew As mpReUseAction)
    m_iAction = iNew
End Property

Public Property Get IncludeSelectedText() As Boolean
    IncludeSelectedText = m_bIncludeSelText
End Property

Public Property Let IncludeSelectedText(bNew As Boolean)
    m_bIncludeSelText = bNew
End Property

Public Property Get SkipStyles() As Boolean
    SkipStyles = m_bSkipStyles
End Property

Public Property Let SkipStyles(bNew As Boolean)
    m_bSkipStyles = bNew
End Property


'**********************************************************
'   Methods
'**********************************************************
Public Function Document() As CDocument
    Set Document = m_oDoc
End Function

Public Function GetReuseAction() As Boolean
'displays the Reuse dialog-
'returns true if user did not cancel
    Dim vbRet As VbMsgBoxResult
    Dim bWarn As Boolean
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If
    
    With frmReUse
        .Show vbModal
        If Not .Cancelled Then
            If .optReuse(ReuseOptCtls_Append) Then
                Me.Action = mpReUseAction_Append
            ElseIf .optReuse(ReuseOptCtls_Replace) Then
                On Error Resume Next
                bWarn = mpBase2.GetMacPacIni("General", "ReUseWarning")
                If bWarn Then
                    vbRet = MsgBox("Are you sure you want to replace the contents of this document?", _
                                   vbYesNo, _
                                   App.Title)
                    If vbRet = vbNo Then GoTo FunctionEnd
                End If
                Me.Action = mpReUseAction_Replace
            Else
                Me.Action = mpReUseAction_Create
            End If
            Me.IncludeSelectedText = .chkIncludeSelText
            GetReuseAction = True
        End If
    End With
FunctionEnd:
    Unload frmReUse
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

End Function

Public Function SetupDocument() As Boolean
'sets up reuse document based on type of action
'and whether to include selected text in reuse output
    
'   get initial dirt status
    m_bSaved = Me.Document.Saved
    
    RaiseEvent BeforeSetupDocument
    
'   store selected text as autotext if specified
    If Me.IncludeSelectedText Then
        Me.Document.SaveSelectionAsAutoText mpReuseSelection
    End If
    
'   executes specified Action
    Select Case Me.Action
        Case mpReUseAction_Replace
            SetupDocument = SetupDocumentReplace()
        Case mpReUseAction_Append
            SetupDocument = SetupDocumentAppend()
        Case Else
            SetupDocument = SetupDocumentCreate()
    End Select
    
    RaiseEvent AfterSetupDocument
End Function

Public Function RestoreDocument()
'executes specified Action

    RaiseEvent BeforeRestoreDocument
    
    Select Case Me.Action
        Case mpReUseAction_Replace
            RestoreDocumentReplace
        Case mpReUseAction_Append
            RestoreDocumentAppend
        Case Else
            RestoreDocumentCreate
    End Select
    
'   delete saved autotext if necessary
    If Me.IncludeSelectedText Then
        Me.Document _
            .DeleteNormalAutoText mpReuseSelection
    End If
    
    RaiseEvent AfterRestoreDocument
    
'   restore initial dirt status
    Me.Document.Saved = m_bSaved
End Function

Public Function Cleanup()
    Dim oRange As Word.Range
    
'   cleans up the reuse document output
    RaiseEvent BeforeCleanup
    Select Case Me.Action
        Case mpReUseAction_Replace
            CleanupReplace
        Case mpReUseAction_Append
            CleanupAppend
        Case Else
            CleanupCreate
    End Select
    
'   insert original selected text at
'   start position ("Begin Typing Here")
'   if specified - then delete autotext
    If Me.IncludeSelectedText Then
        With Me.Document
            .SelectStartPosition , , , , True
            .InsertAutoText mpReuseSelection
            .DeleteNormalAutoText mpReuseSelection
                        
'           get rid of extra para created by insertion above
            If Word.Selection.Paragraphs(1) _
            .Range.Characters.Count = 1 Then
                Word.Selection.Delete
            End If
            
'           select the start of the current section
            Set oRange = Word.Selection.Sections(1).Range
            With oRange
                .StartOf
                .Select
            End With
        End With
    End If
    
    RaiseEvent AfterCleanup
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Function SetupDocumentCreate() As Boolean
'create a copy of source doc
    Dim xTemplate As String
    Dim oSource As Word.Document
    Dim xSource As String
    Dim oNum As New CNumbering
    Dim rngSelection As Word.Range
    
    On Error GoTo ProcError

'   get selection
    Set rngSelection = Selection.Range
    
'   create new doc of same template
    Set oSource = Word.ActiveDocument
'   get doc name using wordbasic - this bypasses ODMA
    xSource = WordBasic.FileName$()
    xTemplate = oSource.AttachedTemplate.FullName
    Word.Documents.Add xTemplate
    DoEvents
    
'   copy the doc vars from the
'   original doc to the new doc
    Me.Document.CopyDocVars "", _
        xSource, Word.ActiveDocument
        
'   copy numbering styles - this will prevent
'   schemes from becoming unlinked
    If Me.IncludeSelectedText And _
            (rngSelection.ListParagraphs.Count > 0) Then
        'Organizer Style Bug
        If Not Me.SkipStyles Then
            oNum.CopyNumberingStyles xSource
        End If
    Else
'       delete active scheme variables
        On Error Resume Next
        ActiveDocument.Variables("zzmpFixedCurScheme").Delete
        ActiveDocument.Variables("zzmpFixedCurScheme_9.0").Delete
        On Error GoTo ProcError
    End If
    
'   delete Numbering session variable
    On Error Resume Next
    ActiveDocument.Variables("zzmpNSession").Delete
    
'   delete all trailer variables
    ActiveDocument.Variables("NewDocStampType").Delete
    ActiveDocument.Variables("MPDocID").Delete
    ActiveDocument.Variables("MPDocIDTemplate").Delete
    ActiveDocument.Variables("MPDocIDTemplateDefault").Delete
    ActiveDocument.Variables("85TrailerType").Delete
    On Error GoTo ProcError
    
    SetupDocumentCreate = True
    Exit Function
    
ProcError:
    RaiseError "MPO.CReuse.SetupDocumentCreate"
    Exit Function
End Function

Private Function SetupDocumentReplace() As Boolean
'   create new section or new document
    Dim bSmart As Boolean
    
    On Error GoTo ProcError
    With m_oDoc
'       added 000721 - convert doc vars whose prefix is
'       'Main' to have a prefix of mpSegment - this is useful
'       for insert segment functionality that was added this July - df
        Dim xTemplate As String
        
        '*c - O12
        m_oDoc.ConvertMainVars Me.Document.DocumentType
            
'       back up doc vars in case user cancels procedure
        .CopyDocVars mpDocVarCopyTag
        
'       back up doc content in case user cancels procedure
        .StoreContentsAsAutoText mpAutoTextCopyTag
    
'       clean out document
        '.ClearContent
        '9.7.1 - #4073
        bSmart = Application.Options.SmartCutPaste
        If bSmart Then _
            Application.Options.SmartCutPaste = False
            
        .ClearMainTextStory
        
        If bSmart Then _
            Application.Options.SmartCutPaste = True
    End With
    
    SetupDocumentReplace = True
    Exit Function
ProcError:
    RaiseError "MPO.CReUse.SetupDocumentReplace"
    Exit Function
End Function

Private Function SetupDocumentAppend() As Boolean
'add new section for reuse document and
'back up doc vars in case user cancels at some point
    Dim oBkmk As Word.Bookmark
    Dim oRng As Word.Range
    
    On Error GoTo ProcError
    With m_oDoc
'       delete any fixed bookmarks - we need to allow
'       new boilerplate to insert possibly the same bookmarks
        For Each oBkmk In Word.ActiveDocument.Bookmarks
            If UCase(Left(oBkmk.Name, 9)) = "ZZMPFIXED" Then
                oBkmk.Delete
            End If
        Next oBkmk
        
'       add new section at eof
        .CreateSection mpDocPosition_EOF, True, True

        Set oRng = Word.ActiveDocument.Content
        oRng.EndOf
        oRng.Select
        
'       back up doc vars in case user cancels procedure
        .CopyDocVars mpDocVarCopyTag
    End With
    SetupDocumentAppend = True
    Exit Function
ProcError:
    RaiseError "MPO.CReUse.SetupDocumentAppend"
    Exit Function
End Function

Private Function RestoreDocumentReplace()
'restores the active document to it's pre-reuse state
    With m_oDoc
'       restore previously copied doc vars
        .RestoreDocVars mpDocVarCopyTag
        
'       restore previous doc content
        .RetoreContentsFromAutoText mpAutoTextCopyTag
    End With
End Function

Private Function RestoreDocumentCreate()
'close new doc without saving
    m_oDoc.CloseDoc False
End Function

Private Function RestoreDocumentAppend()
'restores the active document to it's pre-reuse state
    Dim oRange As Word.Range
    
'   delete the section that was last added
    On Error GoTo ProcError
    With m_oDoc.File.Sections
'       link, then unlink headers - this will guarantee
'       that the headers in the previous section remain
'       as they were when the reuse started
        m_oDoc.LinkHeadersFooters .Last
        m_oDoc.UnlinkHeadersFooters .Last
        With .Last.Range
'           include section break in deletion range
            .MoveStart wdCharacter, -1
            .Delete
        End With
        
'       restore previously copied doc vars
        m_oDoc.RestoreDocVars mpDocVarCopyTag
    End With
    Exit Function
ProcError:
    RaiseError "MPO.CReUse.RestoreDocumentAppend"
    Exit Function
End Function

Private Function CleanupReplace()
'cleans up document after reuse has been fully executed
    On Error GoTo ProcError
    With m_oDoc
        .DeleteDocVars mpDocVarCopyTag
        .DeleteDocContentsAutoText mpAutoTextCopyTag
        .ResetPageNumberFormat .File.Sections.First
    End With
    Exit Function
ProcError:
    RaiseError "MPO.CReUse.CleanupReplace"
    Exit Function
End Function

Private Function CleanupCreate()
'cleans up document after reuse has been fully executed
'nothing to do
End Function

Private Function CleanupAppend()
'cleans up document after reuse has been fully executed
    On Error GoTo ProcError
    m_oDoc.DeleteDocVars mpDocVarCopyTag
    Exit Function
ProcError:
    RaiseError "MPO.CReUse.CleanupAppend"
    Exit Function
End Function

'**********************************************************
'   Class Events
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
End Sub


