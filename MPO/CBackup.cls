VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBackup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'****************************************************
'   CBackup Class
'   created 4/20/00 by Daniel Fisherman
'   Contains properties and methods that
'   define the CBackup Class - those
'   procedures that backup MacPac files
'****************************************************
Option Explicit
Public Property Let LastBackup(dNew As Date)
    mpBase2.SetUserIni "Backup", "Last", CDbl(dNew)
End Property

Public Property Get LastBackup() As Date
    On Error Resume Next
    LastBackup = mpBase2.GetUserIni("Backup", "Last")
End Property

Public Function Execute() As Long
    Dim xMsg As String
    Dim bDoPost As Boolean
    Dim xNetDir As String
    Dim xHomeDir As String
    Dim xHomeVar As String
    Dim xLocalDir As String
    Dim xSourceDir As String
    Dim iUser As Integer
    Dim xDestDir As String
       
    On Error GoTo ProcError
    
'   determine if files should be posted
    bDoPost = CBool(GetMacPacIni("General", "Post"))
                                          
    If bDoPost Then
'       get source dir
        xSourceDir = xTrimTrailingChrs(mpBase2.UserFilesDirectory, "\")
    
'       get dest dir
        xNetDir = GetMacPacIni("General", "PostDir")
        
        xHomeVar = GetMacPacIni("General", "HomePathVariable")
        
        If xHomeVar <> "" Then
            xHomeDir = Environ(xHomeVar)
            If Right(xNetDir, 1) <> "\" And Left(xHomeDir, 1) <> "\" Then
                xHomeDir = "\" & xHomeDir
            ElseIf Right(xNetDir, 1) = "\" And Left(xHomeDir, 1) = "\" Then
                xHomeDir = Mid(xHomeDir, 2)
            End If
            
            xNetDir = xNetDir & xHomeDir & "\MacPac"
        End If
            
        xLocalDir = xSourceDir & "_zzmpTemp"
        
'       mark time of last backup
        Me.LastBackup = Now

'       check for dest dir availability
        On Error Resume Next
        If Dir(xNetDir & "\*.*") = "" Then
           
            If Err.Number = 68 Then 'device not available
'               prompt to copy locally
                xMsg = "The network directory on which your " & _
                       "personal settings are stored is currently " & _
                       "unavailable.  Settings will be stored locally " & _
                       "until the network drive becomes functional."
                MsgBox xMsg, vbExclamation, "The Legal MacPac"

'               post to local personal dir as files -
'               will err if dir exists
                xDestDir = xLocalDir
                MkDir xLocalDir
                On Error GoTo ProcError
                lRet = lPostToDir(xSourceDir, xLocalDir)
            Else
                '---check if the directory is there
                xDestDir = xNetDir
                MkDir xNetDir
                If Err.Number = 75 Or Err.Number = 0 Then
                '---directory exists, you can post!
                    lRet = lPostToDir(xNetDir)
                Else
                    xMsg = "MacPac cannot backup your normal.dot" & _
                            " and default option settings to the network" & _
                            " at this time.  You may lose any changes" & _
                            " to your normal and default options made" & _
                            " during this Word session only." & _
                            "  Please contact your System Administrator if this is a problem."
                            
    '               not available, warn and do...
                    MsgBox xMsg, vbInformation, App.Title
                End If
            End If
        Else
'           post
            xDestDir = xNetDir
            lRet = lPostToDir(xNetDir)
        End If
'       check for success
        If lRet <> 0 Then
'           warn user
            xMsg = "MacPac cannot backup all of your default option settings to " & _
            xDestDir & " at this time.  You may lose any changes to your " & _
            "normal template and default options made during this Word session only.  " & _
            "Please contact your Systems Administrator if this is a problem."
            MsgBox xMsg, vbInformation, App.Title
        End If
    End If
    Execute = lRet
    Exit Function
ProcError:
    RaiseError "MPO.CBackup.Execute"
    Exit Function
End Function

Private Function lPostToDir(ByVal xDestDirRoot As String, _
                            Optional ByVal bCreateTempPersonal As Boolean = False) As Long
    
    Dim xFullName As String
    Dim xFile As String
    Dim docNormal As Word.Document
    Dim xMPFiles() As String
    Dim lNumItems As Long
    Dim CopyError As Long
    Dim i As Integer
    Dim x As String
    Dim xDefSource As String
    Dim bExists As Boolean
    Dim xChild As String
    Dim xDestDir As String
    Dim xFileSpec As String
    Dim oFSO As FileSystemObject
    
    Set oFSO = New FileSystemObject
    
    On Error Resume Next
    Application.StatusBar = "Saving Personal Settings. Please wait..."
    
    xDefSource = mpBase2.UserFilesDirectory
    
'   get items to backup from MacPac.ini
    x = mpBase2.GetIniSection("Backup Files", App.Path & "\MacPac.ini")
    lNumItems = mpBase2.lCountChrs(x, Chr(0))
    
    ReDim xMPFiles(lNumItems - 1)
    For i = 1 To lNumItems
        xMPFiles(i - 1) = GetMacPacIni("Backup Files", "File" & i)
    Next i
    
    DoEvents
    
    g_oDBs.PrivateDB.Close
    g_oDBs.PublicDB.Close
    
    DoEvents

'   enumerate files
    For i = 0 To lNumItems - 1
        If Not (xMPFiles(i) = Empty) Then
'           if item has "\" in name, get source dir
            If InStr(xMPFiles(i), "\") Then
                If Left(xMPFiles(i), 1) = "<" Then
'                   variable supplied for path - get path
                    xFileSpec = xGetFileSpec(xMPFiles(i))
                Else
'                   entire hard coded path is supplied
                    xFileSpec = xMPFiles(i)
                End If
            Else
'               no path supplied, use default source
                xFileSpec = Dir(xDefSource & "\" & xMPFiles(i))
            End If
            
'           get subdirectory of destination dir
            xChild = xGetDestChildDir(xMPFiles(i))
            
'           check if file exists
            On Error GoTo 0
            xFile = Empty
            xFile = Dir(xFileSpec)
            bExists = (xFile <> Empty)
            
            While bExists
                xFullName = oFSO.GetParentFolderName(xFileSpec) & "\" & xFile
                xDestDir = xDestDirRoot & "\" & xChild
                On Error Resume Next
                MkDir xDestDir
                On Error GoTo 0
                
                If Right(UCase(xFullName), 11) = "\NORMAL.DOT" Then
'                   file is Normal.dot - to save, we run a save as
                    On Error Resume Next
                    EchoOff
                    Set docNormal = Word.Documents.Open( _
                        xFullName, , , False)
                    With docNormal
                        .SaveAs xDestDir & "\" & xFile, , , , False
                        .Close wdDoNotSaveChanges
                    End With
                    
                    If Err Then
                        CopyError = Err.Number
                    End If
                    EchoOn
                ElseIf oFSO.GetFile(xFullName).DateLastModified > Me.LastBackup Then
'                   file has been modified since last backup - backup file
                    On Error Resume Next
'                   copy the file from source to dest
                    FileCopy xFullName, xDestDir & "\" & xFile
                    If Err.Number <> 0 Then
                        CopyError = Err.Number
                    End If
                End If
                xFile = Empty
                xFile = Dir()
                bExists = (xFile <> Empty)
            Wend
        End If
    Next i
    lPostToDir = CopyError
    EchoOn
    Application.StatusBar = ""
    Exit Function
ProcError:
    RaiseError "MPO.CBackup.lPostToDir"
    Exit Function
End Function

Private Function xGetDestChildDir(ByVal xFileSpec As String) As String
'returns the name of the destination subdirectory
'to which the file should be copied
    Dim lPos As Long
    Dim xTemp As String
    
    If InStr(xFileSpec, "<") Then
        lPos = InStr(xFileSpec, ">")
        xTemp = Mid(xFileSpec, 2, lPos - 2)
        xGetDestChildDir = xTemp
    ElseIf InStr(xFileSpec, "\") Then
        xGetDestChildDir = "Docs"
    Else
        xGetDestChildDir = "<MacPacUser>"
    End If
End Function

Private Function xGetFileSpec(ByVal xSoftFileSpec As String) As String
'returns the full name of the file specified by xFileSpec
    Dim x As String
    With Word.Options
        x = xSubstitute(xSoftFileSpec, "<WordStartup>", Word.Application.StartupPath)
        x = xSubstitute(x, "<WordWorkgroupTemplates>", .DefaultFilePath(wdWorkgroupTemplatesPath))
        x = xSubstitute(x, "<WordUserTemplates>", .DefaultFilePath(wdUserTemplatesPath))
        x = xSubstitute(x, "<MacPacTemplates>", mpBase2.TemplatesDirectory)
        x = xSubstitute(x, "<MacPacBoilerplate>", mpBase2.BoilerplateDirectory)
        x = xSubstitute(x, "<MacPacUser>", mpBase2.UserFilesDirectory)
        x = xSubstitute(x, "\\", "\")
        xGetFileSpec = x
    End With
End Function

'Private Function lPostToDir(ByVal xSourceDir As String, _
'                            ByVal xDestDir As String, _
'                            Optional ByVal bCreateTempPersonal As Boolean = False) As Long
'
'    Dim xFile As String
'    Dim docNormal As Word.Document
'    Dim xMPFiles() As String
'    Dim bCopyMacPacOnly As Boolean
'    Dim lNumItems As Long
'    Dim CopyError As Long
'    Dim i As Integer
'
'    bCopyMacPacOnly = GetMacPacIni("General", "PostMacPacOnly") = True
'
'    On Error Resume Next
'
'    If bCopyMacPacOnly Then
'        lNumItems = GetMacPacIni("Backup Files", "FileCount")
'        ReDim xMPFiles(lNumItems - 1)
'        For i = 1 To lNumItems
'            xMPFiles(i - 1) = GetMacPacIni("Backup Files", "File" & i)
'        Next i
'    Else
'        ReDim xMPFiles(0)
'    End If
'
'    Application.StatusBar = "Saving Personal Settings. Please wait..."
'
'    DoEvents
'
'    g_oDBs.PrivateDB.Close
'    g_oDBs.PublicDB.Close
'
'    DoEvents
'
''   enumerate personal files
'    If bCopyMacPacOnly Then
'        For i = 0 To lNumItems - 1
''           if item has "\" in name
'            xFile = Dir(xSourceDir & "\" & xMPFiles(i))
'            While xFile <> ""
'                If UCase(xFile) = "NORMAL.DOT" Then
'                    On Error GoTo SkipNormal
'                    EchoOff
'                    Set docNormal = Word.Documents.Open( _
'                        xSourceDir & "\Normal.dot", , , False)
'                    With docNormal
'                        .SaveAs xDestDir & "\" & xFile, , , , False
'                        .Close wdDoNotSaveChanges
'                    End With
'SkipNormal:
'                    On Error Resume Next
'                    If Err.Number <> 0 Then _
'                        CopyError = Err.Number
'                    EchoOn
'                Else
'                    FileCopy xSourceDir & "\" & xFile, _
'                             xDestDir & "\" & xFile
'                    If Err.Number <> 0 Then _
'                        CopyError = Err.Number
'                End If
'                xFile = Dir()
'            Wend
'        Next i
'    Else
'        xFile = Dir(xSourceDir & "\*.*")
'
'        While xFile <> ""
'            If UCase(xFile) = "NORMAL.DOT" Then
'                On Error GoTo SkipNormal2
'                EchoOff
'                Set docNormal = Application.NormalTemplate.OpenAsDocument()
'                docNormal.SaveAs xDestDir & "\" & xFile
'                docNormal.Close wdDoNotSaveChanges
'SkipNormal2:
'                On Error Resume Next
'                If Err.Number <> 0 Then _
'                    CopyError = Err.Number
'                EchoOn
'            Else
'                FileCopy xSourceDir & "\" & xFile, _
'                         xDestDir & "\" & xFile
'                If Err.Number <> 0 Then _
'                    CopyError = Err.Number
'            End If
'            xFile = Dir()
'        Wend
'    End If
'    lPostToDir = CopyError
'    EchoOn
'    Application.StatusBar = ""
'
'End Function



