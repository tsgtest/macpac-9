VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Begin VB.Form frmPeople 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Manage Authors"
   ClientHeight    =   5484
   ClientLeft      =   4152
   ClientTop       =   2712
   ClientWidth     =   7788
   Icon            =   "frmPeople.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5484
   ScaleWidth      =   7788
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdEditBarIDs 
      Caption         =   "&Bar IDs..."
      Height          =   400
      Left            =   1950
      TabIndex        =   12
      Top             =   5010
      Width           =   900
   End
   Begin TrueDBList60.TDBList lstDetail 
      Height          =   4350
      Left            =   3735
      OleObjectBlob   =   "frmPeople.frx":058A
      TabIndex        =   8
      Top             =   510
      Width           =   3975
   End
   Begin VB.CommandButton btnCopy 
      Caption         =   "Cop&y..."
      Height          =   400
      Left            =   3900
      TabIndex        =   9
      Top             =   5010
      Width           =   900
   End
   Begin TrueDBGrid60.TDBGrid lstPeople 
      Height          =   4485
      Left            =   15
      OleObjectBlob   =   "frmPeople.frx":CE3F
      TabIndex        =   0
      Top             =   420
      Width           =   3585
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "&Delete..."
      Height          =   400
      Left            =   5835
      TabIndex        =   7
      Top             =   5010
      Width           =   900
   End
   Begin VB.CommandButton btnAddPerson 
      Caption         =   "&Add..."
      Height          =   400
      Left            =   2940
      TabIndex        =   4
      Top             =   5010
      Width           =   900
   End
   Begin VB.CommandButton btnSort 
      Caption         =   "&Sort"
      Height          =   400
      Left            =   90
      TabIndex        =   2
      Top             =   5010
      Width           =   900
   End
   Begin VB.CommandButton btnEdit 
      Caption         =   "&Edit..."
      Height          =   400
      Left            =   4875
      TabIndex        =   1
      Top             =   5010
      Width           =   900
   End
   Begin VB.CommandButton btnClose 
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Default         =   -1  'True
      Height          =   400
      Left            =   6810
      TabIndex        =   3
      Top             =   5010
      Width           =   900
   End
   Begin VB.Line Line3 
      BorderColor     =   &H00808080&
      X1              =   0
      X2              =   0
      Y1              =   405
      Y2              =   4900
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00808080&
      X1              =   0
      X2              =   7800
      Y1              =   4905
      Y2              =   4905
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00808080&
      X1              =   20
      X2              =   7800
      Y1              =   405
      Y2              =   405
   End
   Begin VB.Label Label2 
      Caption         =   "Name"
      Height          =   210
      Left            =   765
      TabIndex        =   11
      Top             =   165
      Width           =   675
   End
   Begin VB.Label Label1 
      Caption         =   "Favorite"
      Height          =   210
      Left            =   60
      TabIndex        =   10
      Top             =   165
      Width           =   675
   End
   Begin VB.Label lblSource 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "##"
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   2355
      TabIndex        =   6
      Top             =   165
      Width           =   1200
   End
   Begin VB.Label lblDetailHeading 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "##"
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   4845
      TabIndex        =   5
      Top             =   165
      Width           =   2820
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Main"
      Visible         =   0   'False
   End
End
Attribute VB_Name = "frmPeople"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_PersonsForm As MPO.CPersonsForm
Private m_psnP As mpDB.CPerson
Private SourceCheckboxClicked As Boolean
Private m_bRefreshRequired As Boolean
Private m_oArray As XArray

'**********************************************************
'   Properties
'**********************************************************
Private Property Let RefreshRequired(bNew As Boolean)
    m_bRefreshRequired = bNew
End Property

Public Property Get RefreshRequired() As Boolean
    RefreshRequired = m_bRefreshRequired
End Property

Public Property Let SelPerson(psnNew As mpDB.CPerson)
    Set m_psnP = psnNew
End Property

Public Property Get SelPerson() As mpDB.CPerson
    Set SelPerson = m_psnP
End Property

'**********************************************************
'   Internal Event Procedures
'**********************************************************
Private Sub btnDelete_Click()
    Dim bDel As Boolean
    Dim lSelID As Long
    
    bDel = m_PersonsForm.DeletePerson(Me.SelPerson)
    If bDel Then
        With Me.lstPeople
            If .SelBookmarks(0) + 1 = .Array.Count(1) + 1 Then
'               deleted item was last in list - select new last item
                lSelID = .Array.Value(.SelBookmarks(0) - 1, 2)
            Else
'               deleted item was not last in list -
'               select next item in list
                lSelID = .Array.Value(.SelBookmarks(0), 2)
            End If
        End With
        RefreshPeople lSelID
    End If
End Sub

Private Sub btnEdit_Click()
    Dim bEdited As Boolean
    bEdited = m_PersonsForm.EditPerson(Me.SelPerson)
    If bEdited Then
        RefreshPeople Me.SelPerson.ID
    End If
End Sub

Private Sub btnAddPerson_Click()
    Dim lID As Long
    lID = m_PersonsForm.AddPerson()
    If lID Then
        RefreshPeople lID
    End If
    m_bRefreshRequired = False
End Sub

Private Sub btnClose_Click()
    Me.Hide
    DoEvents
    If Me.RefreshRequired Then
        RefreshPeople
        g_oDBs.Attorneys.Refresh
    End If
    Unload Me
End Sub

Private Sub btnSort_Click()
    Screen.MousePointer = vbHourglass
    Me.lstPeople.FirstRow = 0
    RefreshPeople Me.SelPerson.ID
    Screen.MousePointer = vbDefault
End Sub

Private Sub btnCopy_Click()
    Dim oCopy As mpDB.CPerson
    If Not (Me.SelPerson Is Nothing) Then
        Set oCopy = m_PersonsForm.CopyPerson(Me.SelPerson)
        If Not (oCopy Is Nothing) Then
            RefreshPeople oCopy.ID
        End If
    End If
End Sub

Private Sub cmdEditBarIDs_Click()
    Dim oDlg As frmAttorneyLicenses
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean

    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

    Set oDlg = New frmAttorneyLicenses
    oDlg.Attorney = Me.SelPerson
    oDlg.cmbAuthor.BoundText = Me.lstPeople.Columns("fldID")
    oDlg.Show vbModal
    If Not oDlg.Cancelled Then
        RefreshPeople Me.lstPeople.Columns("fldID")
    End If
    Unload oDlg
    Set oDlg = Nothing
    
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

End Sub
Private Sub Form_Activate()
    If Not (Me.SelPerson Is Nothing) Then
        SelectPerson Me.SelPerson.ID
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_PersonsForm = Nothing
    Set m_psnP = Nothing
    Set m_oArray = Nothing
End Sub

Private Sub lstPeople_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim i As Integer
    Select Case KeyCode
        Case 32
            ToggleSource
        Case vbKeyPageUp
'           page down - ensure that all selections are
'           cleared if the current row is not the first item
            With Me.lstPeople
                If .RowBookmark(.Row) > 0 Then
                    For i = 0 To .SelBookmarks.Count - 1
                        .SelBookmarks.Remove i
                    Next i
                End If
            End With
        Case vbKeyPageDown
'           page down - ensure that all selections are
'           cleared if the current row is not the last item
            With Me.lstPeople
                If .RowBookmark(.Row) <> .Array.Count(1) - 1 Then
                    For i = 0 To .SelBookmarks.Count - 1
                        .SelBookmarks.Remove i
                    Next i
                End If
            End With
        Case vbKeyEscape
            Unload Me
        Case Else
            AutoCompleteInXArrayList KeyCode, Me.lstPeople
    End Select
End Sub

Private Sub lstPeople_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    SourceCheckboxClicked = (x > 270 And x < 420) And _
        True

    If Me.lstPeople.RowContaining(Y) = Me.lstPeople.Row Then
        If SourceCheckboxClicked Then
            ToggleSource
        End If
    End If
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Public Sub SelectPerson(lID As Long, Optional vFirstRow As Variant)
'selects the row in tdb control whose fldID  = lID
    Dim lRow As Long
    Dim xarP As XArray
    
    Set xarP = Me.lstPeople.Array
    
'   loop until a match is found in the xarray or until
'   all rows in xarray have been passed
    While lID <> xarP.Value(lRow, 2) And lRow <= xarP.Count(1)
        lRow = lRow + 1
    Wend
    
'   select first row if match was not found
    If lRow > xarP.Count(1) Then
        lRow = 0
    End If
    
    If Not IsMissing(vFirstRow) Then
'       set first row
        Me.lstPeople.FirstRow = vFirstRow
    End If
    
'   select row
    With Me.lstPeople
        .SelBookmarks.Add lRow
        .Bookmark = lRow
    End With
End Sub

Public Function ToggleSource()
    SourceCheckboxClicked = False
    With Me.SelPerson
        .Favorite = Not .Favorite
        Me.lstPeople.Columns("fldFavorite").Value = .Favorite
        Me.lstPeople.Update
    End With
End Function

Public Sub RefreshPeople(Optional vID As Variant)
    Dim lFRow As Long
    On Error Resume Next
    lFRow = Me.lstPeople.FirstRow
    On Error GoTo 0
    Screen.MousePointer = vbHourglass
    g_oDBs.People.Refresh
    g_oDBs.Attorneys.Refresh '9.7.1020 #4511
    With Me.lstPeople
        .Rebind
        On Error Resume Next
        If Not IsMissing(vID) Then
            SelectPerson CLng(vID), lFRow
        End If
        .SetFocus
    End With
    Screen.MousePointer = vbDefault
End Sub

Public Sub SelectCurrentRow()
    Dim lFirstRow As Long
    
    On Error GoTo ProcError
    
    With Me.lstPeople
'       get first visible row number
        If Not IsNull(.FirstRow) Then
            lFirstRow = .FirstRow
        End If
        
'       select current row
        .SelBookmarks.Add .Bookmark
        .SetFocus
        
'       set first row back to what it was
        If lFirstRow Then
            .FirstRow = lFirstRow
        End If
    End With
    
'   update SelPerson to reflect change
    Me.SelPerson = g_oDBs.People.Item( _
        Me.lstPeople.Columns("fldID"))
    
'   enable/disable delete button if
'   not a public list person
    Me.btnDelete.Enabled = _
        (Me.SelPerson.Source <> mpPeopleSourceList_People)
    Me.btnEdit.Enabled = _
        (Me.SelPerson.Source <> mpPeopleSourceList_People)

    UpdateForm
    
    If SourceCheckboxClicked Then
        ToggleSource
        DoEvents
    End If
    Exit Sub
    
ProcError:
    RaiseError "MPO.frmPeople.SelectCurrentRow"
    Exit Sub
End Sub

Private Sub UpdateForm()
    Dim xCaption As String
    Dim i As Integer
    Dim iIndex As Integer
    Dim vValue As Variant
    Dim xBarIDs As String
    
    With Me.SelPerson
        Me.lblDetailHeading = "Detail for " & .FirstName & " " & .LastName
        If .Source = mpPeopleSourceList_PeopleInput Then
            Me.lblSource.Caption = "Private Author"
            Me.lblSource.ForeColor = &HC0&
        Else
            Me.lblSource.Caption = "Public Author"
            Me.lblSource.ForeColor = &H0&
        End If
    End With
        
    With m_oArray
        .Value(0, 1) = Me.SelPerson.FirstName
        .Value(0, 3) = Me.SelPerson.MiddleName
        .Value(0, 5) = Me.SelPerson.LastName
        .Value(0, 7) = Me.SelPerson.FullName
        .Value(0, 9) = Me.SelPerson.Initials
        .Value(0, 11) = Me.SelPerson.Office.DisplayName
    End With
    
'   enable BarID if necessary
    Me.cmdEditBarIDs.Enabled = Me.SelPerson.IsAttorney
    
'   load custom properties
    With g_oDBs.PersonDetailControls
        For i = 1 To .Count
            iIndex = 11 + (i * 2)
            Err.Clear
            On Error Resume Next
'           get value - an error will be generated if the linked
'           property is a custom author property
            vValue = Empty
            vValue = CallByName(Me.SelPerson, .Item(i).LinkedProperty, VbGet)
            If Err.Number And Err.Number <> 94 Then
                On Error GoTo ProcError
'               try custom people properties
                vValue = Me.SelPerson.CustomProperties.Item(.Item(i).LinkedProperty)
            End If
            On Error GoTo ProcError
            If .Item(i).ControlType = mpPersonDetailCtl_CheckBox Then
                If vValue Then
                    m_oArray.Value(0, iIndex) = "Yes"
                Else
                    m_oArray.Value(0, iIndex) = "No"
                End If
            Else
                m_oArray.Value(0, iIndex) = vValue
            End If
        Next i
    End With
    
'   bar ids #3096
    For i = 1 To Me.SelPerson.Licenses.Count
        xBarIDs = xBarIDs & Me.SelPerson.Licenses(, i).Number & ", "
    Next i
    m_oArray.Value(0, iIndex + 2) = mpBase2.xTrimTrailingChrs(xBarIDs, ", ")
        

    With Me.lstDetail
        .Array = m_oArray
        .Rebind
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmPeople.UpdateForm"
    Exit Sub
End Sub

Private Sub lstPeople_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    SelectCurrentRow
    With Me.lstPeople.Columns
        .Item(0).Refresh
        .Item(1).Refresh
        DoEvents
    End With
End Sub

'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Form_Initialize()
    Set m_PersonsForm = New MPO.CPersonsForm
End Sub

Private Sub Form_Load()
    Dim iCols As Integer
    Dim iIndex As Integer
    Dim i As Integer
    
    Me.lstPeople.Array = g_oDBs.People.ListSource
    
'   add labels to detail list
    Set m_oArray = New XArray
        
    iCols = 11 + (g_oDBs.PersonDetailControls.Count * 2) + 2   'last 2 for bar ids
    
    With m_oArray
        .ReDim 0, 0, 0, iCols
        .Value(0, 0) = "First Name:"
        .Value(0, 2) = "Middle Name:"
        .Value(0, 4) = "Last Name:"
        .Value(0, 6) = "Full Name:"
        .Value(0, 8) = "Initials:"
        .Value(0, 10) = "Office:"
        
'       load custom properties
        With g_oDBs.PersonDetailControls
            For i = 1 To .Count
                iIndex = 10 + (i * 2)
                m_oArray.Value(0, iIndex) = _
                    xSubstitute(.Item(i).Caption, "&", "") & ":"
            Next i
        End With
    
'       bar ids
        .Value(0, iIndex + 2) = "Bar IDs:"
    End With


'   show/hide vertical scroll bar when necessary
    If iCols > 27 Then
        Me.lstDetail.ScrollBars = 2
    Else
        Me.lstDetail.ScrollBars = 0
    End If
End Sub

Private Sub Form_Terminate()
    Set m_PersonsForm = Nothing
End Sub

