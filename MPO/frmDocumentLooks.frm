VERSION 5.00
Object = "{3B008041-905A-11D1-B4AE-444553540000}#1.0#0"; "VSOCX6.OCX"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{EB93698E-EEED-11D3-A2AA-8AF5FFE8C00E}#1.2#0"; "MPCONT~1.OCX"
Begin VB.Form frmDocumentLooks 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit Document Types"
   ClientHeight    =   5235
   ClientLeft      =   3825
   ClientTop       =   1080
   ClientWidth     =   5475
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDocumentLooks.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5235
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnRetrieve 
      Caption         =   "Retrieve from Document"
      Height          =   400
      Left            =   1905
      TabIndex        =   42
      Top             =   4170
      Width           =   1965
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4935
      Picture         =   "frmDocumentLooks.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   45
      ToolTipText     =   "Delete Document Type (Alt+D)"
      Top             =   4170
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4455
      Picture         =   "frmDocumentLooks.frx":0196
      Style           =   1  'Graphical
      TabIndex        =   44
      ToolTipText     =   "Save Document Type (Alt+S)"
      Top             =   4170
      Width           =   435
   End
   Begin VB.CommandButton btnNew 
      Cancel          =   -1  'True
      Height          =   400
      Left            =   3960
      Picture         =   "frmDocumentLooks.frx":0314
      Style           =   1  'Graphical
      TabIndex        =   43
      ToolTipText     =   "Create a New Document Type (Alt+N)"
      Top             =   4170
      Width           =   435
   End
   Begin VB.CommandButton btnClose 
      Caption         =   "Close"
      Height          =   400
      Left            =   4380
      TabIndex        =   47
      Top             =   4785
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbDocType 
      Height          =   600
      Left            =   1860
      OleObjectBlob   =   "frmDocumentLooks.frx":03F6
      TabIndex        =   3
      Top             =   165
      Width           =   3585
   End
   Begin vsOcx6LibCtl.vsIndexTab vsIndexTab1 
      Height          =   5250
      Left            =   -15
      TabIndex        =   46
      Top             =   -45
      Width           =   5910
      _ExtentX        =   10425
      _ExtentY        =   9260
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   1
      MousePointer    =   0
      _ConvInfo       =   1
      Version         =   600
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "Layout|Format|Page Number"
      Align           =   0
      Appearance      =   1
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      Begin VB.Frame Frame3 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4740
         Left            =   6855
         TabIndex        =   50
         Top             =   45
         Width           =   5820
         Begin VB.TextBox txtPgNumBefore 
            Appearance      =   0  'Flat
            Height          =   317
            Left            =   1815
            TabIndex        =   34
            Top             =   1140
            Width           =   3585
         End
         Begin VB.TextBox txtPgNumAfter 
            Appearance      =   0  'Flat
            Height          =   317
            Left            =   1815
            TabIndex        =   36
            Top             =   1605
            Width           =   3585
         End
         Begin VB.CheckBox chkPgNumFirstPage 
            Appearance      =   0  'Flat
            BackColor       =   &H80000004&
            Caption         =   "Insert on first page of section"
            ForeColor       =   &H80000008&
            Height          =   360
            Left            =   1860
            TabIndex        =   41
            Top             =   3045
            Width           =   2865
         End
         Begin TrueDBList60.TDBCombo cmbPgNumFormat 
            Height          =   600
            Left            =   1800
            OleObjectBlob   =   "frmDocumentLooks.frx":2600
            TabIndex        =   32
            Top             =   690
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbPgNumLocation 
            Height          =   600
            Left            =   1815
            OleObjectBlob   =   "frmDocumentLooks.frx":4C9E
            TabIndex        =   38
            Top             =   2070
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbPgNumAlignment 
            Height          =   600
            Left            =   1830
            OleObjectBlob   =   "frmDocumentLooks.frx":733E
            TabIndex        =   40
            Top             =   2565
            Width           =   3585
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "T&ype:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   2
            Top             =   225
            Width           =   1275
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00FFFFFF&
            X1              =   -105
            X2              =   5500
            Y1              =   4725
            Y2              =   4725
         End
         Begin VB.Label lblBefore 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Text &Before:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   120
            TabIndex        =   33
            Top             =   1170
            Width           =   1500
         End
         Begin VB.Label lblAfter 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Text A&fter:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   120
            TabIndex        =   35
            Top             =   1635
            Width           =   1500
         End
         Begin VB.Label lblNumberFormat 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Format:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   90
            TabIndex        =   31
            Top             =   750
            Width           =   1500
         End
         Begin VB.Label lblLocation 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Location:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   37
            Top             =   2115
            Width           =   1500
         End
         Begin VB.Label lblAlignment 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Alignment:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   39
            Top             =   2625
            Width           =   1500
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H80000003&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H80000003&
            FillColor       =   &H80000003&
            Height          =   7260
            Left            =   15
            Top             =   -15
            Width           =   1770
         End
      End
      Begin VB.Frame Frame2 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4740
         Left            =   6555
         TabIndex        =   49
         Top             =   45
         Width           =   5820
         Begin mpControls.SpinnerTextBox spnFontSize 
            Height          =   315
            Left            =   1830
            TabIndex        =   20
            Top             =   1290
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            MinValue        =   6
            MaxValue        =   72
         End
         Begin mpControls.SpinnerTextBox spnFirstLineIndent 
            Height          =   315
            Left            =   1845
            TabIndex        =   24
            Top             =   2385
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            IncrementValue  =   0.25
            MaxValue        =   4
            SupplyQuotes    =   -1  'True
         End
         Begin mpControls.SpinnerTextBox spnSpaceBefore 
            Height          =   315
            Left            =   1845
            TabIndex        =   28
            Top             =   3240
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            IncrementValue  =   6
            MaxValue        =   108
         End
         Begin mpControls.SpinnerTextBox spnSpaceAfter 
            Height          =   315
            Left            =   1845
            TabIndex        =   30
            Top             =   3720
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            IncrementValue  =   6
            MaxValue        =   108
         End
         Begin TrueDBList60.TDBCombo cmbFont 
            Height          =   600
            Left            =   1830
            OleObjectBlob   =   "frmDocumentLooks.frx":99DF
            TabIndex        =   18
            Top             =   810
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbLineSpacing 
            Height          =   600
            Left            =   1845
            OleObjectBlob   =   "frmDocumentLooks.frx":BBE6
            TabIndex        =   26
            Top             =   2835
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbAlignment 
            Height          =   600
            Left            =   1845
            OleObjectBlob   =   "frmDocumentLooks.frx":DDF4
            TabIndex        =   22
            Top             =   1920
            Width           =   3585
         End
         Begin VB.Label lblParaAlignment 
            BackStyle       =   0  'Transparent
            Caption         =   "NORMAL"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   2
            Left            =   135
            TabIndex        =   52
            Top             =   555
            Width           =   990
         End
         Begin VB.Label lblParaAlignment 
            BackStyle       =   0  'Transparent
            Caption         =   "BODY TEXT"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   1
            Left            =   135
            TabIndex        =   51
            Top             =   1710
            Width           =   990
         End
         Begin VB.Line Line4 
            BorderColor     =   &H00FFFFFF&
            X1              =   -45
            X2              =   5560
            Y1              =   4725
            Y2              =   4725
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "T&ype:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   1
            Top             =   225
            Width           =   1275
         End
         Begin VB.Label lblSpaceAfter 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Space Af&ter:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   270
            TabIndex        =   29
            Top             =   3750
            Width           =   1275
         End
         Begin VB.Label lblSpaceBefore 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Space &Before:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   270
            TabIndex        =   27
            Top             =   3300
            Width           =   1275
         End
         Begin VB.Label lblLineSpace 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Line S&pacing:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   270
            TabIndex        =   25
            Top             =   2850
            Width           =   1275
         End
         Begin VB.Label lblFirstLine 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "First Line &Indent:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   270
            TabIndex        =   23
            Top             =   2415
            Width           =   1275
         End
         Begin VB.Label lblParaAlignment 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Alignment:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   0
            Left            =   270
            TabIndex        =   21
            Top             =   1965
            Width           =   1275
         End
         Begin VB.Label lblFontName 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Font &Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   17
            Top             =   885
            Width           =   1275
         End
         Begin VB.Label lblFontSize 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Font &Size:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   19
            Top             =   1335
            Width           =   1275
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H80000003&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H80000003&
            FillColor       =   &H80000003&
            Height          =   7260
            Left            =   15
            Top             =   -15
            Width           =   1770
         End
      End
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   4740
         Left            =   45
         TabIndex        =   48
         Top             =   45
         Width           =   5820
         Begin VB.CheckBox chkPage1HF 
            Caption         =   "Different First Page Header/Footer"
            Height          =   285
            Left            =   1890
            TabIndex        =   16
            Top             =   3405
            Width           =   2835
         End
         Begin mpControls.SpinnerTextBox spnTopMargin 
            Height          =   315
            Left            =   1845
            TabIndex        =   5
            Top             =   720
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            BackColor       =   -2147483643
            IncrementValue  =   0.25
            MaxValue        =   3
            SupplyQuotes    =   -1  'True
         End
         Begin mpControls.SpinnerTextBox spnBottomMargin 
            Height          =   315
            Left            =   1845
            TabIndex        =   7
            Top             =   1140
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            BackColor       =   -2147483643
            IncrementValue  =   0.25
            MaxValue        =   3
            SupplyQuotes    =   -1  'True
         End
         Begin mpControls.SpinnerTextBox spnLeftMargin 
            Height          =   315
            Left            =   1845
            TabIndex        =   9
            Top             =   1560
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            BackColor       =   -2147483643
            IncrementValue  =   0.25
            MaxValue        =   3
            SupplyQuotes    =   -1  'True
         End
         Begin mpControls.SpinnerTextBox spnRightMargin 
            Height          =   315
            Left            =   1845
            TabIndex        =   11
            Top             =   1950
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            BackColor       =   -2147483643
            IncrementValue  =   0.25
            MaxValue        =   3
            SupplyQuotes    =   -1  'True
         End
         Begin mpControls.SpinnerTextBox spnSpaceBetweenCols 
            Height          =   315
            Left            =   1845
            TabIndex        =   15
            Top             =   3000
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            BackColor       =   -2147483643
            IncrementValue  =   0.25
            MaxValue        =   2
            SupplyQuotes    =   -1  'True
         End
         Begin mpControls.SpinnerTextBox spnNumCols 
            Height          =   315
            Left            =   1845
            TabIndex        =   13
            Top             =   2595
            Width           =   3585
            _ExtentX        =   6324
            _ExtentY        =   556
            BackColor       =   -2147483643
            MinValue        =   1
            MaxValue        =   4
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00FFFFFF&
            X1              =   -15
            X2              =   5590
            Y1              =   4725
            Y2              =   4725
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000005&
            X1              =   -15
            X2              =   6140
            Y1              =   4755
            Y2              =   4755
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "T&ype:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   0
            Top             =   225
            Width           =   1275
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00FFFFFF&
            Visible         =   0   'False
            X1              =   -120
            X2              =   5550
            Y1              =   6960
            Y2              =   6960
         End
         Begin VB.Label lblNumCols 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Number of &Columns:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   120
            TabIndex        =   12
            Top             =   2655
            Width           =   1560
         End
         Begin VB.Label lblSpaceBetweenCols 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Bet&ween Columns:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   165
            TabIndex        =   14
            Top             =   3030
            Width           =   1515
         End
         Begin VB.Label lblBottomMargin 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Bottom Margin:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   6
            Top             =   1155
            Width           =   1275
         End
         Begin VB.Label lblLeftMargin 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Left Margin:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   8
            Top             =   1560
            Width           =   1275
         End
         Begin VB.Label lblRightMargin 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Right Margin:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   225
            TabIndex        =   10
            Top             =   1965
            Width           =   1275
         End
         Begin VB.Label lblTopMargin 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Top &Margin:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   4
            Top             =   750
            Width           =   1275
         End
         Begin VB.Shape Shape4 
            BackColor       =   &H80000003&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H80000003&
            FillColor       =   &H80000003&
            Height          =   7260
            Left            =   15
            Top             =   -30
            Width           =   1770
         End
      End
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmDocumentLooks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_oDefs As CDocumentLookDefs
Private m_oDef As CDocumentLookDef
Private m_bCancelled As Boolean

'**********************************************************
'   Properties
'**********************************************************
Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Private Sub btnClose_Click()
    Save
    Unload Me
End Sub

Private Sub btnDelete_Click()
    Dim xMsg As String
    Dim iRet As VbMsgBoxResult
    Dim lIndex As Long
    Dim lID As Long
    
    On Error GoTo ProcError
        
    If Me.cmbDocType.BoundText = Empty Then
        Exit Sub
    End If
    
    xMsg = "Delete this Document Type?"
    iRet = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
    
    If iRet = vbYes Then
'       delete the doc type
        m_oDefs.Delete Me.cmbDocType.BoundText
        
        With Me.cmbDocType
            If m_oDefs.Count Then
'               get currently selected item
                lIndex = .SelectedItem
            
'               get id of next item in list
                If lIndex = .Array.Count(1) - 1 Then
                    lID = .Array.Value(lIndex - 1, 0)
                Else
                    lID = .Array.Value(lIndex + 1, 0)
                End If
            End If
            
'           refresh list
            .Array.Clear
            .Array = m_oDefs.ListSource
            .Rebind
            
            If m_oDefs.Count Then
                .BoundText = lID
            Else
                .Text = Empty
                SetCtlEnable False
            End If
        End With
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.CDocumentLookDefs.Delete"
    Exit Sub
End Sub

Private Sub btnNew_Click()
'create a new document look
    CreateNewDocLook
End Sub

Private Function CreateNewDocLook() As Boolean
'returns TRUE if new look was created
'create a new document look
    Dim oDef As mpDB.CDocumentLookDef
    Dim oDlg As frmAddItem
    
    On Error GoTo ProcError
    
    Set oDlg = New frmAddItem
    
    With oDlg
'       show dlg to add new name
        .Caption = "Create a New Document Type"
        .Show vbModal, Me
        
        If Not .Cancelled Then
'           add new member to collection
            Set m_oDef = m_oDefs.Add(oDlg.ItemName)
            
'           refresh list source
            Me.cmbDocType.Array = m_oDefs.ListSource
            Me.cmbDocType.Rebind
            
'           select new member
            Me.cmbDocType.BoundText = m_oDef.ID
            
            CreateNewDocLook = True
        End If
    End With
    
'   enable controls if this is the first
'   document look def in the list
    If m_oDefs.Count = 1 Then
        SetCtlEnable True
    End If
    Exit Function
ProcError:
    RaiseError "MPO.frmDocumentLooks.CreateNewDocLook"
End Function

Private Sub btnRetrieve_Click()
    Dim bRet As Boolean
    Dim oVar As Word.Variable
    Dim iRet As VbMsgBoxResult
    Dim xName As String
    
'   check for an existing doc look

    On Error Resume Next
    Set oVar = Word.ActiveDocument.Variables.Item("DocLook_Alignment")
    xName = oVar.Name
    On Error GoTo ProcError
    
    If (xName = Empty) Then
'       alert and prompt for use of document values
        xMsg = "No MacPac document type has been applied to this document.  " & _
            "Would you like MacPac to analyze this document's format? " & vbCr & _
            "If so, please review the dialog values when you are done."
        iRet = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
'       use doc values if user says OK
        If iRet = vbYes Then
'           create new doc look
            bRet = CreateNewDocLook()
            If bRet Then
                Dim oPgSet As Word.PageSetup
                Dim oPara As Word.ParagraphFormat
                Dim oFont As Word.Font
                Dim oSec As Word.Section
                
                Set oSec = Selection.Sections.First
                Set oPgSet = oSec.PageSetup
                Set oPara = Word.ActiveDocument.Styles("Body Text").ParagraphFormat
                Set oFont = Word.ActiveDocument.Styles("Normal").Font
                
                Me.cmbAlignment.BoundText = oPara.Alignment
                Me.spnBottomMargin.Value = oPgSet.BottomMargin / 72
                Me.spnNumCols.Value = oPgSet.TextColumns.Count
                Me.chkPage1HF = oPgSet.DifferentFirstPageHeaderFooter
                Me.spnFirstLineIndent.Value = oPara.FirstLineIndent / 72
                Me.cmbFont.BoundText = oFont.Name
                Me.spnFontSize.Value = oFont.Size
                Me.spnLeftMargin.Value = oPgSet.LeftMargin / 72
                Me.cmbLineSpacing.BoundText = oPara.LineSpacingRule
                Me.cmbPgNumAlignment.BoundText = 1
                Me.chkPgNumFirstPage = 0
                Me.cmbPgNumFormat.BoundText = 1
                Me.cmbPgNumLocation.BoundText = 2
                Me.txtPgNumAfter = ""
                Me.txtPgNumBefore = "."
                Me.spnRightMargin.Value = oPgSet.RightMargin / 72
                Me.spnSpaceAfter.Value = oPara.SpaceAfter
                Me.spnSpaceBefore.Value = oPara.SpaceBefore
                If oPgSet.TextColumns.Spacing = 9999999 Then
                    Me.spnSpaceBetweenCols.Value = 0.25
                Else
                    Me.spnSpaceBetweenCols.Value = oPgSet.TextColumns.Spacing
                End If
                Me.spnTopMargin.Value = oPgSet.TopMargin / 72
                Save
            End If
        End If
    Else
'       create new doc look
        bRet = CreateNewDocLook()
    
        If bRet Then
'           set values to those stored in document
            On Error Resume Next
            With Word.ActiveDocument.Variables
                Me.cmbAlignment.BoundText = .Item("DocLook_Alignment")
                Me.spnBottomMargin.Value = .Item("DocLook_BottomMargin")
                Me.spnNumCols.Value = .Item("DocLook_Columns")
                Me.chkPage1HF = Abs(.Item("DocLook_DifferentPage1HeaderFooter"))
                Me.spnFirstLineIndent.Value = .Item("DocLook_FirstLineIndent")
                Me.cmbFont.BoundText = .Item("DocLook_FontName")
                Me.spnFontSize.Value = .Item("DocLook_FontSize")
                Me.spnLeftMargin.Value = .Item("DocLook_LeftMargin")
                Me.cmbLineSpacing.BoundText = .Item("DocLook_LineSpacing")
                Me.cmbPgNumAlignment.BoundText = .Item("DocLook_PageNumAlignment")
                Me.chkPgNumFirstPage = Abs(.Item("DocLook_PageNumFirstPage"))
                Me.cmbPgNumFormat.BoundText = .Item("DocLook_PageNumFormat")
                Me.cmbPgNumLocation.BoundText = .Item("DocLook_PageNumLocation")
                Me.txtPgNumAfter = .Item("DocLook_PageNumTextAfter")
                Me.txtPgNumBefore = .Item("DocLook_PageNumTextBefore")
                Me.spnRightMargin.Value = .Item("DocLook_RightMargin")
                Me.spnSpaceAfter.Value = .Item("DocLook_SpaceAfter")
                Me.spnSpaceBefore.Value = .Item("DocLook_SpaceBefore")
                Me.spnSpaceBetweenCols.Value = .Item("DocLook_SpaceBetweenColumns")
                Me.spnTopMargin.Value = .Item("DocLook_TopMargin")
            End With
            On Error GoTo ProcError
            Save
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnSave_Click()
    Save
End Sub

Private Sub Save()
    On Error GoTo ProcError
    UpdateObject
    m_oDefs.Save
    Exit Sub
ProcError:
    g_oError.Show Err, _
        "Could not successfully save the document type."
    Exit Sub
End Sub

Private Sub cmbDocType_ItemChange()
    On Error GoTo ProcError
    
'   get definition
    Set m_oDef = m_oDefs(Me.cmbDocType.BoundText)
    
'   show definition values
    UpdateForm
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateForm()
    On Error GoTo ProcError
    With m_oDef
'       do layout
        Me.spnBottomMargin.Value = .BottomMargin
        Me.spnTopMargin.Value = .TopMargin
        Me.spnLeftMargin.Value = .LeftMargin
        Me.spnRightMargin.Value = .RightMargin
        Me.spnNumCols.Value = .Columns
        Me.spnSpaceBetweenCols.Value = .SpaceBetweenColumns
        Me.chkPage1HF = Abs(.DifferentPage1HeaderFooter)
        
'       do format
        Me.cmbFont.BoundText = .FontName
        Me.spnFontSize.Value = .FontSize
        
        Me.cmbAlignment.BoundText = .Alignment
        Me.spnFirstLineIndent.Value = .FirstLineIndent
        Me.cmbLineSpacing.BoundText = .LineSpacing
        Me.spnSpaceBefore.Value = .SpaceBefore
        Me.spnSpaceAfter.Value = .SpaceAfter
        
'       do page number
        Me.cmbPgNumFormat.BoundText = .PageNumFormat
        Me.cmbPgNumAlignment.BoundText = .PageNumAlignment
        Me.cmbPgNumLocation.BoundText = .PageNumLocation
        Me.txtPgNumAfter = .PageNumTextAfter
        Me.txtPgNumBefore = .PageNumTextBefore
        Me.chkPgNumFirstPage = Abs(.PageNumFirstPage)
      End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub UpdateObject()
    On Error GoTo ProcError
    With m_oDef
        .BottomMargin = Me.spnBottomMargin.Value
        .TopMargin = Me.spnTopMargin.Value
        .LeftMargin = Me.spnLeftMargin.Value
        .RightMargin = Me.spnRightMargin.Value
        .Columns = Me.spnNumCols.Value
        .SpaceBetweenColumns = Me.spnSpaceBetweenCols.Value
        .DifferentPage1HeaderFooter = Me.chkPage1HF
        
        .FontName = Me.cmbFont.BoundText
        .FontSize = Me.spnFontSize.Value
        
        .Alignment = Me.cmbAlignment.BoundText
        .FirstLineIndent = Me.spnFirstLineIndent.Value
        .LineSpacing = Me.cmbLineSpacing.BoundText
        .SpaceBefore = Me.spnSpaceBefore.Value
        .SpaceAfter = Me.spnSpaceAfter.Value
        
        .PageNumAlignment = Me.cmbPgNumAlignment.BoundText
        .PageNumFirstPage = Me.chkPgNumFirstPage
        .PageNumFormat = Me.cmbPgNumFormat.BoundText
        .PageNumLocation = Me.cmbPgNumLocation.BoundText
        .PageNumTextAfter = Me.txtPgNumAfter
        .PageNumTextBefore = Me.txtPgNumBefore
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub SetCtlEnable(bEnable As Boolean)
    Dim oCtl As Control
    On Error GoTo ProcError
    For Each oCtl In Me.Controls
        If TypeOf oCtl Is VB.TextBox Then
            oCtl.Enabled = bEnable
            oCtl.Text = Empty
        ElseIf TypeOf oCtl Is mpControls.SpinnerTextBox Then
            oCtl.Enabled = bEnable
            oCtl.Value = Empty
        ElseIf TypeOf oCtl Is VB.CheckBox Then
            oCtl.Enabled = bEnable
            oCtl.Value = False
        ElseIf TypeOf oCtl Is TrueDBList60.TDBCombo Then
            oCtl.Enabled = bEnable
            oCtl.Text = Empty
        ElseIf TypeOf oCtl Is TrueDBList60.TDBList Then
            oCtl.Enabled = bEnable
            oCtl.Text = Empty
        End If
    Next oCtl
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Command1_Click()

End Sub

Private Sub Form_Activate()
    Dim lID As Long
    On Error GoTo ProcError
    If m_bInit Then
        If m_oDefs.Count Then
            Me.cmbDocType.SelectedItem = 0
            lID = Me.cmbDocType.BoundText
            Set m_oDef = m_oDefs(lID)
        End If
        Me.Initializing = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oArr As XArray
    Dim oAlignments As XArray
    Dim oLocs As XArray
    Dim oScopes As XArray
    
    Set oAlignments = New XArray
    Set oLocs = New XArray
    Set oScopes = New XArray
        
    On Error GoTo ProcError
    
'   build line spacing array
    Set oArr = mpBase2.xarStringToxArray("Single|0|1.5|1|Double|2", 2)
    
    Me.Cancelled = True
    
    Me.Initializing = True
    Set m_oDefs = g_oDBs.PrivateDocumentLookDefs
    If m_oDefs.Count Then
        Me.cmbDocType.Array = m_oDefs.ListSource
        ResizeTDBCombo Me.cmbDocType, 6
    Else
        SetCtlEnable False
    End If
    Me.cmbAlignment.Array = g_oLists("Alignments").ListItems.Source
    Me.cmbFont.Array = g_oLists("Fonts").ListItems.Source
    Me.cmbLineSpacing.Array = oArr
    
    ResizeTDBCombo Me.cmbAlignment, 3
    ResizeTDBCombo Me.cmbFont, 6
    ResizeTDBCombo Me.cmbLineSpacing, 3
    
'   set alignments list
    With oAlignments
        .ReDim 0, 2, 0, 1
        .Value(0, 1) = "Left"
        .Value(0, 0) = mpPageNumberLocation_Left
        .Value(1, 1) = "Center"
        .Value(1, 0) = mpPageNumberLocation_Center
        .Value(2, 1) = "Right"
        .Value(2, 0) = mpPageNumberLocation_Right
    End With
    
'   set location list
    With oLocs
        .ReDim 0, 2, 0, 1
        .Value(0, 1) = "Header"
        .Value(0, 0) = mpPageNumberLocation_Header
        .Value(1, 1) = "Footer"
        .Value(1, 0) = mpPageNumberLocation_Footer
        .Value(2, 1) = "Cursor Position"
        .Value(2, 0) = mpPageNumberLocation_Cursor
    End With
    
'   set number format list
    Me.cmbPgNumFormat.Array = g_oDBs.Lists("PageNumberFormats").ListItems.Source
    
'   assign lists to controls
    Me.cmbPgNumAlignment.Array = oAlignments
    Me.cmbPgNumLocation.Array = oLocs
    
'   resize control dropdown
    ResizeTDBCombo Me.cmbPgNumAlignment, 3
    ResizeTDBCombo Me.cmbPgNumLocation, 3
    ResizeTDBCombo Me.cmbPgNumFormat, 6
    
    mpBase2.MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    mpBase2.SetLastPosition Me
End Sub
