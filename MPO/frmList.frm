VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmList 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   240
   ClientWidth     =   3750
   Icon            =   "frmList.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   3750
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   1500
      TabIndex        =   3
      Top             =   3915
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   2565
      TabIndex        =   2
      Top             =   3915
      Width           =   1000
   End
   Begin TrueDBList60.TDBList lstItems 
      Height          =   3225
      Left            =   165
      OleObjectBlob   =   "frmList.frx":058A
      TabIndex        =   0
      Top             =   450
      Width           =   3390
   End
   Begin VB.Label lblItems 
      Caption         =   "###"
      Height          =   240
      Left            =   180
      TabIndex        =   1
      Top             =   210
      Width           =   2580
   End
End
Attribute VB_Name = "frmList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_vSelItem As Variant
Private m_xDlgCaption As String
Private m_xLblCaption As String
Private m_oArray As XArray
Private m_bCancelled As Boolean

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Set List(oNew As XArray)
    On Error GoTo ProcError
    Set m_oArray = oNew
    With Me.lstItems
        .Array = m_oArray
        .Rebind
        If (SelectedItem = Empty) Then
            .Row = 0
        Else
            .BoundText = Me.SelectedItem
        End If
    End With
    Exit Property
ProcError:
    g_oError.Show Err
    Exit Property
End Property

Public Property Get SelectedItem() As Variant
    SelectedItem = Me.lstItems.BoundText
End Property

Public Property Let SelectedItem(vNew As Variant)
    On Error GoTo ProcError
    Me.lstItems.BoundText = CStr(vNew)
    Exit Property
ProcError:
    g_oError.Show Err
    Exit Property
End Property

Public Property Let DialogCaption(xNew As String)
    m_xDlgCaption = xNew
End Property

Public Property Get DialogCaption() As String
    DialogCaption = m_xDlgCaption
End Property

Public Property Let LabelCaption(xNew As String)
    m_xLblCaption = xNew
End Property

Public Property Get LabelCaption() As String
    LabelCaption = m_xLblCaption
End Property

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    m_bCancelled = True
End Sub

Private Sub btnOK_Click()
    Me.Hide
    DoEvents
    m_bCancelled = False
End Sub

Private Sub Form_Load()
    Me.lblItems.Caption = Me.LabelCaption
    Me.Caption = Me.DialogCaption
    m_bCancelled = True
End Sub

Private Sub lstItems_DblClick()
    btnOK_Click
End Sub
