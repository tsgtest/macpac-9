VERSION 5.00
Begin VB.Form frmPassword 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Enter Password"
   ClientHeight    =   1800
   ClientLeft      =   48
   ClientTop       =   348
   ClientWidth     =   5028
   Icon            =   "frmPassword.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1800
   ScaleWidth      =   5028
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton txtCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3885
      TabIndex        =   3
      Top             =   1275
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   400
      Left            =   2790
      TabIndex        =   2
      Top             =   1275
      Width           =   1000
   End
   Begin VB.TextBox txtItemName 
      Height          =   360
      IMEMode         =   3  'DISABLE
      Left            =   135
      MaxLength       =   50
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   780
      Width           =   4755
   End
   Begin VB.Label Label1 
      Caption         =   "###"
      Height          =   600
      Left            =   180
      TabIndex        =   0
      Top             =   135
      Width           =   4635
   End
End
Attribute VB_Name = "frmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xName As String
Private m_bCancelled As Boolean
Private m_xMessage As String

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get ItemName() As String
    ItemName = m_xName
End Property

Public Property Let ItemName(xNew As String)
    m_xName = xNew
End Property

Public Property Get Message() As String
    Message = m_xMessage
End Property

Public Property Let Message(xNew As String)
    m_xMessage = xNew
End Property

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.ItemName = Me.txtItemName
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    m_bCancelled = True
    Me.Label1.Caption = Me.Message
End Sub

Private Sub txtCancel_Click()
    m_bCancelled = True
    Me.ItemName = Empty
    Me.Hide
    DoEvents
End Sub

Private Sub txtItemName_Change()
'   enable OK btn only when there
'   is text in the Name txt box
    If txtItemName.Text = "" Then
        btnOK.Enabled = False
    Else
        btnOK.Enabled = True
    End If
End Sub

Private Sub txtItemName_GotFocus()
    With Me.txtItemName
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
