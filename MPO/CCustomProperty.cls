VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomProperty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
  Option Explicit

'**********************************************************
'   CCustomProperty Class
'   created 10/22/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods of a
'   MacPac Component Custom Property

'   Member of App
'   Container for CCustomProperty
'**********************************************************
Public Enum CustomPropertyTypes
    CustomPropertyType_Document = 1
    CustomPropertyType_Author = 2
    CustomPropertyType_AuthorOption = 3
End Enum

Private Const mpDocSegment As String = "CustomProperty"
Private m_oDoc As MPO.CDocument
Private m_bWholePara As Boolean
Private m_xName As String
Private m_xBookmark As String
Private m_xDelim As String
Private m_xLinkedProp As String
Private m_iPropType As mpCustomPropertyTypes
Private m_xMacro As String
Private m_bIndexed As Boolean
Private m_lAction As Long   'defined as long to allow future switch to tblActions with IDs
Private m_iUnderlineLen As Integer

Public Event AfterValueSet(Property As MPO.CCustomProperty)

'**********************************************************
'   Properties
'**********************************************************
Friend Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Friend Property Let Macro(xNew As String)
    m_xMacro = xNew
End Property

Public Property Get Macro() As String
    Macro = m_xMacro
End Property

Friend Property Let LinkedProperty(xNew As String)
    m_xLinkedProp = xNew
End Property

Public Property Get LinkedProperty() As String
    LinkedProperty = m_xLinkedProp
End Property

Friend Property Get Indexed() As Boolean
    Indexed = m_bIndexed
End Property

Public Property Let Indexed(bNew As Boolean)
    m_bIndexed = bNew
End Property

Friend Property Let Bookmark(xNew As String)
    m_xBookmark = xNew
End Property

Public Property Get Bookmark() As String
    Bookmark = m_xBookmark
End Property

Public Property Get PropertyType() As mpCustomPropertyTypes
    PropertyType = m_iPropType
End Property

Public Property Let PropertyType(iNew As mpCustomPropertyTypes)
    m_iPropType = iNew
End Property

Public Property Let Value(vNew As Variant)
    On Error GoTo ProcError
    If Me.Value <> vNew Or g_bForceItemUpdate Then
'       store both new Value and current Text property -
'       value is used as the dialog control value on reuse
        m_oDoc.SaveItem Me.Name, mpDocSegment, , Me.Text & "??" & vNew
        Me.RunMacro
'        RaiseEvent AfterValueSet(Me)
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CCustomProperty.Value"
    Exit Property
End Property
Public Property Get Value() As Variant
Attribute Value.VB_Description = "sets/returns the value of the associated dialog control"
'the Value is used as the dialog control value on reuse
    Dim xTemp As String
    Dim iPos As Integer
    
'   get stored value
    On Error Resume Next
    xTemp = m_oDoc.RetrieveItem(Me.Name, mpDocSegment)
    On Error GoTo 0
    
    If xTemp <> Empty Then
'       get delimiter position
        iPos = InStr(xTemp, "??")
        
        If iPos Then
'           everything to the right of
'           the delimiter is the text
            Value = Mid(xTemp, iPos + Len("??"))
        Else
'           we should never get here, but just in case
            Value = xTemp
        End If
    End If
End Property

Public Property Let Text(xNew As String)
    Dim bChecked As Boolean
    
    On Error GoTo ProcError
    If Me.Text <> xNew Or g_bForceItemUpdate Then
        If Me.Action = mpCustomPropertyAction_InsertText Then
'           write to doc
            If xNew <> "-Field-" Then  ' ***** CHANGED
                m_oDoc.EditItem_ALT Me.Bookmark, _
                    xNew, , Me.WholeParagraph, , , , Me.UnderlineLength
            Else
                 m_oDoc.EditDateItem Me.Bookmark, , , mpDateFormat_LongText
            End If
        ElseIf Me.Action = mpCustomPropertyAction_InsertCheckbox Then
'           convert text value to T/F
            On Error Resume Next
            bChecked = CBool(xNew)
            On Error GoTo ProcError
            
'           insert MacPac dynamic checkbox
            m_oDoc.InsertDynamicCheckBox Me.Bookmark, bChecked
        End If
        
'       store both new Value and current Text property -
'       value is used as the dialog control value on reuse
        m_oDoc.SaveItem Me.Name, mpDocSegment, , xNew & "??" & Me.Value
        
'       set value = text
        Me.Value = xNew
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CCustomProperty.Text"
    Exit Property
End Property

Public Property Get Text() As String
    Dim xTemp As String
    Dim iPos As Integer
    
'   get stored value
    On Error Resume Next
    xTemp = m_oDoc.RetrieveItem(Me.Name, mpDocSegment)
    On Error GoTo 0
    
    If xTemp <> Empty Then
'       get delimiter position
        iPos = InStr(xTemp, "??")
        
        If iPos Then
'           everything to the left of
'           the delimiter is the text
            Text = Left(xTemp, iPos - 1)
        Else
'           we should never get here, but just in case
            Text = xTemp
        End If
    End If
End Property

Friend Property Let DelimiterReplacement(xNew As String)
    m_xDelim = xNew
End Property

Public Property Get DelimiterReplacement() As String
    DelimiterReplacement = m_xDelim
End Property

Friend Property Get WholeParagraph() As Boolean
    WholeParagraph = m_bWholePara
End Property

Public Property Let WholeParagraph(bNew As Boolean)
    m_bWholePara = bNew
End Property

Public Property Let Action(lNew As mpCustomPropertyActions)
    m_lAction = lNew
End Property

Public Property Get Action() As mpCustomPropertyActions
    Action = m_lAction
End Property

Public Property Let UnderlineLength(iNew As Integer)
    m_iUnderlineLen = iNew
End Property

Public Property Get UnderlineLength() As Integer
    UnderlineLength = m_iUnderlineLen
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Show(ByVal bWholeParagraph As Boolean)
    m_oDoc.HideItem Me.Bookmark, False, bWholeParagraph
End Function

Public Function Hide(ByVal bWholeParagraph As Boolean)
    m_oDoc.HideItem Me.Bookmark, True, bWholeParagraph
End Function

Public Function RunMacro()
'   run macro if it exists
    If Len(Me.Macro) Then
        On Error Resume Next
        Err.Clear
        mpVer.RunMacro Me.Macro, Me.Name, Me.Text, Me.Value, Me.Bookmark
        If Err.Number Then
            xMsg = "Unable to run the macro '" & Me.Macro & _
                   "'.  The macro may not exist or may not " & _
                   "be defined with the following arguments:" & vbCr & _
                   "ByVal xPropName As String, ByVal xText As String, " & _
                   "ByVal xValue As String, ByVal xBookmark As String."
            MsgBox xMsg, vbExclamation, App.Title
        End If
    End If
End Function

'**********************************************************
'   Class Events
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
End Sub



