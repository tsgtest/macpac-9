VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRecipient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Option Explicit
'**********************************************************
'   CRecipient Class
'   created 12/20/99 by Jeffrey Sweetland
'   This a collection of CMPItem objects
'   Designed to allow recipient to contain any number of
'   Fields
'
'   Container for CMPItem
'**********************************************************
Private m_colItems As Collection
Private m_xKey As String
'**********************************************************
'   Properties
'**********************************************************
Public Property Get Item(vntIndexKey As Variant) As MPO.CMPItem
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
  Set Item = m_colItems(vntIndexKey)
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "440"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = m_colItems.[_NewEnum]
End Property
Friend Property Let Key(xNew As String)
    m_xKey = xNew
End Property
Friend Property Get Key() As String
    Key = m_xKey
End Property
'**********************************************************
'   Public Methods
'**********************************************************
Public Function AddItem(xDesc As String, Optional vValue As Variant) As MPO.CMPItem
    
    'create a new object
    Dim objNewMember As MPO.CMPItem
    
    Set objNewMember = New MPO.CMPItem

    On Error GoTo ProcError
    
    ' xDesc must not be empty as it is used as the unique key
    If Len(xDesc) = 0 Then _
        Err.Raise mpError_NullValueNotAllowed
        
    'set the properties passed into the method
    If Not IsMissing(vValue) Then
        objNewMember.Value = vValue
    End If
        
    objNewMember.Description = xDesc
    m_colItems.Add objNewMember, xDesc

    'return the object created
    Set AddItem = objNewMember
    Set objNewMember = Nothing
    
    Exit Function

ProcError:
    RaiseError "MPO.CRecipient.AddItem"
    Exit Function
End Function
Public Function CountItems() As Long
    'used when retrieving the number of elements in the
    'collection
    CountItems = m_colItems.Count
End Function
Public Sub RemoveItem(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    
    m_colItems.Remove vntIndexKey
End Sub
Public Function Items() As MPO.CRecipient
'   Allows using more intuitive "For each vItem in oRecip.Items" syntax
'   In addition to "For Each vItem in oRecip"
    Set Items = Me
End Function
Public Sub SetValues(ParamArray vValues())
    Dim i As Integer
    
    On Error GoTo ProcError
    If UBound(vValues()) > Me.CountItems - 1 Then _
        Err.Raise mpError_ArrayDimensionsOutOfBounds

    For i = 1 To UBound(vValues()) + 1
        Item(i).Value = vValues(i)
    Next i
    
ProcError:
    Err.Number = mpError_ArrayDimensionsOutOfBounds
    RaiseError "MPO.CRecipient.SetValues"
    Exit Sub
End Sub
'**********************************************************
'   Internal Methods
'**********************************************************
Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set m_colItems = New Collection
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set m_colItems = Nothing
End Sub
