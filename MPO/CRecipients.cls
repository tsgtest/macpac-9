VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRecipients"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CRecipient"
Attribute VB_Ext_KEY = "Member0" ,"CRecipient"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CRecipients Class
'   created 12/20/99 by Jeffrey Sweetland
'   Contains properties and methods
'   to add and remove items
'   from a collection of CRecipient and
'   handles setting the description names used
'   by the underlying CMPItem objects
'
'   Container for CRecipient
'**********************************************************
Public Event AfterRefresh(iStart As Integer)
Public Event ItemRemoved(iIndex As Integer)
Public Event ItemAdded(iIndex As Integer)

Private bStopEvents As Boolean

Private m_Col As Collection
Private m_bFieldsSet As Boolean
Private m_xarFieldNames As XArray
'**********************************************************
'   Properties
'**********************************************************
Public Property Get Item(vntIndexKey As Variant) As CRecipient
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
    On Error GoTo ProcError
    Set Item = m_Col(vntIndexKey)
    Exit Property
ProcError:
    Err.Number = mpError_InvalidMember
    RaiseError "MPO.CRecipients.Item"
End Property
Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection
    Count = m_Col.Count
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "440"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = m_Col.[_NewEnum]
End Property
'**********************************************************
'   Public Methods
'**********************************************************
Public Property Get FieldNames(iIndex As Integer) As String
    On Error GoTo ProcError
    
    FieldNames = m_xarFieldNames(iIndex, 0)
    
    Exit Property
ProcError:
    RaiseError "MPO.CRecipients.FieldNames"
End Property
Public Property Get FieldCount() As Integer
    On Error Resume Next
    FieldCount = m_xarFieldNames.UpperBound(1) + 1
End Property
Public Function Add() As CRecipient
    Dim i As Integer
    Dim xKey As String
    'create a new object
    
    Dim objNewMember As CRecipient
    Set objNewMember = New CRecipient

    'Add an MPItem to CRecipient object for each field defined
    For i = 1 To Me.FieldCount
        objNewMember.AddItem xDesc:=Me.FieldNames(i - 1)
    Next i
    
    xKey = MakeNewKey
    objNewMember.Key = xKey
    
    m_Col.Add objNewMember, xKey

        
    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing
    If Not bStopEvents Then _
        RaiseEvent ItemAdded(m_Col.Count)
End Function
Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    Dim i As Integer
    Dim iKey As Integer
    
    On Error GoTo ProcError
    If Not IsNumeric(vntIndexKey) Then
        For i = 1 To Me.Count
            If Me.Item(i).Key = vntIndexKey Then
                iKey = i
                Exit For
            End If
        Next i
    Else
        iKey = vntIndexKey
    End If
    
    m_Col.Remove vntIndexKey
    
    If Not bStopEvents Then _
        RaiseEvent ItemRemoved(iKey)
    Exit Sub
ProcError:
    Err.Number = mpError_InvalidItemIndex
    RaiseError "MPO.CRecipients.Remove"
    Exit Sub
End Sub
Public Function ListArray() As XArray
    ' Returns collection as an XArray
    Dim i As Integer
    Dim j As Integer
    Dim xARTemp As XArray
    
    i = Me.Count - 1
    j = Me.FieldCount - 1
    
    Set xARTemp = New XArray
    
    xARTemp.ReDim 0, i, 0, j
    
    With xARTemp
        For i = 0 To .UpperBound(1)
            For j = 0 To .UpperBound(2)
                .Value(i, j) = Me.Item(i + 1).Item(j + 1)
            Next j
        Next i
    End With
    Set ListArray = xARTemp
    Set xARTemp = Nothing
End Function
Public Sub UpdateFromXArray(xarNew As XArray, Optional bAppend As Boolean = False, _
        Optional bForceRefresh As Boolean = True)
    ' Transfer XArray elements to recipient object properties
        
    Dim iRow As Integer
    Dim icol As Integer
    Dim oRecip As CRecipient
    Dim iNewIndex As Integer
    
    On Error GoTo ProcError
    
    bStopEvents = True
    
    If m_xarFieldNames.UpperBound(1) <> xarNew.UpperBound(2) Then _
        Err.Raise mpError_ArrayDimensionsOutOfBounds
        
    If xarNew.UpperBound(1) = -1 And Not bAppend Then
        Set m_Col = New Collection
        iNewIndex = 1
    Else
        If Not bAppend Then Me.RemoveAll
        iNewIndex = m_Col.Count + 1
        With xarNew
            For iRow = 0 To .UpperBound(1)
                Set oRecip = Add
                For icol = 0 To .UpperBound(2)
                    oRecip.Item(icol + 1) = xarNew(iRow, icol)
                Next icol
            Next iRow
        End With
    End If
    
    bStopEvents = False
    
    If bForceRefresh Then _
        RaiseEvent AfterRefresh(iNewIndex)
    ' Raise event to tell Parent class to refill controls
    Exit Sub
ProcError:
    bStopEvents = False
    RaiseError "MPO.CRecipients.UpdateFromXArray"
    Exit Sub
End Sub
Public Sub SetFieldNames(xNames As String)
    ' Sets number of fields and field names to be used by recipient item
    ' xNames should be "|" delimited string
    On Error GoTo ProcError
    
    If Me.Count > 0 Then _
        Err.Raise mpError_RecipientFieldsAlreadySet
    
    If xNames = "" Then _
        Err.Raise mpError_NullValueNotAllowed
        
    Set m_xarFieldNames = xarStringToxArray(xNames, 1)
    m_bFieldsSet = True
    Exit Sub
ProcError:
    RaiseError "MPO.CRecipients.SetFieldNames"
    Exit Sub
End Sub
Public Sub RemoveAll()
    Set m_Col = New Collection
    If Not bStopEvents Then _
        RaiseEvent AfterRefresh(1)
End Sub
'**********************************************************
'   Private Methods
'**********************************************************
Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set m_Col = New Collection
    Me.SetFieldNames "Name|Address"
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set m_Col = Nothing
End Sub
Private Function MakeNewKey() As String
    Static iKey As Integer
    iKey = iKey + 1
    MakeNewKey = "R" & Format(iKey, "00000000")
End Function
