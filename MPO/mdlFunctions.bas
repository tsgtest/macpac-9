Attribute VB_Name = "mdlFunctions"
Option Explicit

Public Function bIsDefaultCountry(xCountry As String) As Boolean
    Dim oList As mpDB.CList
    Dim xH As New XHelper
    Dim lRowFound As Long
    Dim i As Integer
    
    On Error GoTo ProcError
    If xCountry = "" Then Exit Function
    Set oList = g_oDBs.Lists("CountryAliases")
    
    lRowFound = -1
    
    With oList
        .KeepFilter = True
        .FilterValue = Chr(34) & g_oDBs.Offices.Default.Country & Chr(34)
        .Refresh
        If .ListItems.Source.Count(1) Then
            For i = .ListItems.Source.LowerBound(1) To .ListItems.Source.UpperBound(1)
                .ListItems.Source.Value(i, 1) = UCase(.ListItems.Source.Value(i, 1))
            Next i
            lRowFound = xH.FindFirst(.ListItems.Source, UCase(xCountry), 1)
        End If
    End With
    
    bIsDefaultCountry = (lRowFound <> -1) Or _
                        (UCase(xCountry) = UCase(g_oDBs.Offices.Default.Country))
    
    Exit Function
ProcError:
    RaiseError "MPO.mdlFunctions.bIsDefaultCountry"
    Exit Function
End Function

Public Function bIsValidPleadingPaper(xFilterValue As String, _
                                      xFilterField As String, _
                                      lPaperID As Long) As Boolean
    Dim oList As mpDB.CList
    Dim xH As New XHelper
    Dim lRowFound As Long
    Dim i As Integer
    
    On Error GoTo ProcError
    Set oList = g_oDBs.Lists("PleadingPapers")
    
    lRowFound = -1
    
    With oList
        .KeepFilter = True
        .FilterField = xFilterField
        .FilterValue = xFilterValue
        .Refresh
        If .ListItems.Source.Count(1) Then
            lRowFound = xH.FindFirst(.ListItems.Source, lPaperID, 1)
        End If
    End With
    
    bIsValidPleadingPaper = (lRowFound <> -1)
    
    Exit Function
ProcError:
    RaiseError "MPO.mdlFunctions.bIsValidPleadingPaper"
    Exit Function
End Function

Public Function bIsBadLabel(vLabelID As Variant) As Boolean
    Dim oList As mpDB.CList
    Dim xH As New XHelper
    Dim lRowFound As Long
    Dim i As Integer
    
    If vLabelID = Empty Then Exit Function
    On Error Resume Next
    Set oList = g_oDBs.Lists("MSLabelBug")
    On Error GoTo ProcError
    
    If (oList Is Nothing) Then
        MsgBox "The MSLabelBug list in tblLists is missing.  Please contact your administrator.", _
               vbExclamation, App.Title
    End If
    
    lRowFound = -1
    
    With oList
        If .ListItems.Source.Count(1) Then
            For i = .ListItems.Source.LowerBound(1) To .ListItems.Source.UpperBound(1)
                .ListItems.Source.Value(i, 1) = UCase(.ListItems.Source.Value(i, 1))
            Next i
            lRowFound = xH.FindFirst(.ListItems.Source, UCase(vLabelID), 1)
        End If
    End With
    
    bIsBadLabel = (lRowFound <> -1)
    
    Exit Function
ProcError:
    RaiseError "MPO.mdlFunctions.bIsBadLabel"
    Exit Function
End Function

'9.7.1 - #4029
Public Sub RunEditMacro(oTemplate As MPO.CTemplate)
    Dim xEditMacro As String
    
    xEditMacro = oTemplate.EditMacro
    
    If xEditMacro <> "" Then
        On Error Resume Next
        Err.Clear
        Word.Application.Run xEditMacro
        If Err.Number Then
            MsgBox "Unable to run the document editing macro '" & xEditMacro & _
                   "'." & vbCr & vbCr & "Please contact your administrator.", _
                   vbExclamation, _
                   App.Title
        End If
    End If
    
End Sub

'Organizer Save Prompt
Public Function bOrganizerSavePrompt() As Boolean

        If g_bOrganizerSavePrompt And _
        WordBasic.FileNameFromWindow$() = "" Then
            MsgBox g_xOrganizerSavePrompt
            bOrganizerSavePrompt = True
        End If

End Function

Public Function bOrganizerSavePromptStyles() As Boolean
    Dim ltHeading As Word.ListTemplate
    Dim oNumbering As New CNumbering
    
    'get list template attached to Heading 1
    Set ltHeading = oNumbering.GetHeadingLT()
    
    If Not (ltHeading Is Nothing) Then
        If g_bOrganizerSavePrompt And _
        WordBasic.FileNameFromWindow$() = "" Then
            MsgBox g_xOrganizerSavePromptStyles
            bOrganizerSavePromptStyles = True
        End If
    End If

End Function

Public Function bTemplateContainsTypesWithDefaultSchemeSet() As Boolean
    Dim oDoc As New CDocument
    Dim bResult As Boolean
    Dim l As Long
    
    Select Case UCase(oDoc.GetClassName(ActiveDocument.AttachedTemplate))
        Case "CBUSINESS"
            For l = 0 To g_oDBs.BusinessDefs.Count - 1
                If (g_oDBs.BusinessDefs(g_oDBs.BusinessDefs.ListSource(l)).DefaultNumberingScheme <> "") Then
                    bResult = True
                    Exit For
                End If
            Next l
        Case "CPLEADING"
            For l = 1 To g_oDBs.PleadingDefs.Count
                If (g_oDBs.PleadingDefs(l).DefaultNumberingScheme <> "") Then
                    bResult = True
                    Exit For
                End If
            Next l
        Case Else
    End Select
    
    bTemplateContainsTypesWithDefaultSchemeSet = bResult
    
End Function


