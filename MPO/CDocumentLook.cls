VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentLook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CDocumentLook Class
'
'   created 3/13/00 by Daniel Fisherman
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac document look - a named set of formats

'   Container for CDocument
'**********************************************************

Private m_lType As Long
Private m_oDef As mpDB.CDocumentLookDef
Private m_iSection As Integer
Private m_bApplyMargins As Boolean
Private m_bApplyDifferentFirstPage As Boolean
Private m_bApplyFont As Boolean
Private m_bApplyColumns As Boolean
Private m_bApplyParagraphFormat As Boolean
Private m_bApplyPageNumber As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Let TypeID(lNew As Long)
    On Error GoTo ProcError
    m_lType = lNew
    Set m_oDef = g_oDBs.DocumentLookDefs.Item(lNew)
    Exit Property
ProcError:
    RaiseError "MPO.CDocumentLook.TypeID"
    Exit Property
End Property

Public Property Get TypeID() As Long
    TypeID = m_lType
End Property

Public Property Let Section(iNew As Integer)
    m_iSection = iNew
End Property

Public Property Get Section() As Integer
    Section = m_iSection
End Property

Public Property Let ApplyMargins(bNew As Boolean)
    m_bApplyMargins = bNew
End Property

Public Property Get ApplyMargins() As Boolean
    ApplyMargins = m_bApplyMargins
End Property

Public Property Let ApplyFont(bNew As Boolean)
    m_bApplyFont = bNew
End Property

Public Property Get ApplyFont() As Boolean
    ApplyFont = m_bApplyFont
End Property

Public Property Let ApplyColumns(bNew As Boolean)
    m_bApplyColumns = bNew
End Property

Public Property Get ApplyColumns() As Boolean
    ApplyColumns = m_bApplyColumns
End Property

Public Property Let ApplyParagraphFormat(bNew As Boolean)
    m_bApplyParagraphFormat = bNew
End Property

Public Property Get ApplyParagraphFormat() As Boolean
    ApplyParagraphFormat = m_bApplyParagraphFormat
End Property

Public Property Let ApplyPageNumber(bNew As Boolean)
    m_bApplyPageNumber = bNew
End Property

Public Property Get ApplyPageNumber() As Boolean
    ApplyPageNumber = m_bApplyPageNumber
End Property

Public Property Let ApplyDifferentFirstPage(bNew As Boolean)
    m_bApplyDifferentFirstPage = bNew
End Property

Public Property Get ApplyDifferentFirstPage() As Boolean
    ApplyDifferentFirstPage = m_bApplyDifferentFirstPage
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Definition() As mpDB.CDocumentLookDef
    Set Definition = m_oDef
End Function

Public Sub Apply()
'applies the document look to the document-
'either the selected section or the whole document
    Dim oPgNum As CPageNumber
    Dim oDef As mpDB.CDocumentLookDef
    Dim oRg As Word.Range
    Dim oSec As Word.Section
    Dim oTS As Word.TabStops
    Dim oPgSet As Word.PageSetup
    Dim xStyle As String
    Dim sArea As Single
    
    On Error GoTo ProcError
    
    Set oDef = Me.Definition
    
'   get scope
    If Me.Section Then
        Set oRg = Word.ActiveDocument.Sections(Me.Section).Range
    Else
        Set oRg = Word.ActiveDocument.Content
    End If
    
    If Me.ApplyPageNumber Then
'       remove existing doc look page number if it exists
        RemoveExistingPageNumber oRg

        Set oPgNum = New CPageNumber
         
'       get page number from definition if specified
        With oDef
            oPgNum.Alignment = .PageNumAlignment
            oPgNum.Section = Me.Section
            oPgNum.FirstPage = .PageNumFirstPage
            oPgNum.Location = .PageNumLocation
            oPgNum.NumberFormat = .PageNumFormat
            oPgNum.TextAfter = .PageNumTextAfter
            oPgNum.TextBefore = .PageNumTextBefore
        End With
    End If
    
    For Each oSec In oRg.Sections
        With oSec
'           do page setup if specified
            With .PageSetup
                If Me.ApplyColumns Then
                    .TextColumns.SetCount oDef.Columns
                    If .TextColumns.Count > 1 Then
                        .TextColumns.Spacing = InchesToPoints( _
                            oDef.SpaceBetweenColumns)
                    End If
                End If
                    
'               do margins if specified
                If Me.ApplyMargins Then
                    .TopMargin = InchesToPoints(oDef.TopMargin)
                    .BottomMargin = InchesToPoints(oDef.BottomMargin)
                    .LeftMargin = InchesToPoints(oDef.LeftMargin)
                    .RightMargin = InchesToPoints(oDef.RightMargin)
                End If
                    
'               do DifferentFirstPage if specified
                If Me.ApplyDifferentFirstPage Then
                    .DifferentFirstPageHeaderFooter = _
                        oDef.DifferentPage1HeaderFooter
                End If
            End With
        End With
    
'       insert page number if specified
        If Me.ApplyPageNumber Then
            oPgNum.Insert
        
            If oPgNum.Location = mpPageNumberLocation_Footer Then
'               redefine Header - Footer has been
'               redefined by the number insertion
                xStyle = "Header"
            Else
'               redefine Footer - Header has been
'               redefined by the number insertion
                xStyle = "Footer"
            End If
        
'           set tab stops - center and right
            Set oTS = Word.ActiveDocument.Styles( _
                xStyle).ParagraphFormat.TabStops
            oTS.ClearAll
            
            Set oPgSet = oSec.PageSetup
            
'           get width of area between margins
            With oPgSet
                sArea = .PageWidth - (.RightMargin + .LeftMargin + .Gutter)
            End With
            
            oTS.Add sArea, wdAlignTabRight
            oTS.Add sArea / 2, wdAlignTabCenter
        End If
        
'       do font if specified
        If Me.ApplyFont Then
            With Word.ActiveDocument.Styles("Normal").Font
                .Name = oDef.FontName
                .Size = oDef.FontSize
            End With
        End If
        
'       do para format if specified
        If Me.ApplyParagraphFormat Then
            With Word.ActiveDocument.Styles("Body Text").ParagraphFormat
                .Alignment = oDef.Alignment
                .FirstLineIndent = InchesToPoints(oDef.FirstLineIndent)
                .LineSpacingRule = oDef.LineSpacing
                .SpaceAfter = oDef.SpaceAfter
                .SpaceBefore = oDef.SpaceBefore
            End With
        End If
    Next oSec
    
'   save the most recently applied look
    Save
    
    Exit Sub
ProcError:
    RaiseError "MPO.CDocumentLook.Apply"
End Sub

Private Sub Save()
'saves values as doc vars - called from Apply
    Dim oDef As mpDB.CDocumentLookDef
    Set oDef = Me.Definition
    
    On Error GoTo ProcError
    With Word.ActiveDocument.Variables
            .Item("DocLook_Alignment") = oDef.Alignment
            .Item("DocLook_BottomMargin") = oDef.BottomMargin
            .Item("DocLook_Columns") = oDef.Columns
            .Item("DocLook_DifferentPage1HeaderFooter") = _
                oDef.DifferentPage1HeaderFooter
            .Item("DocLook_FirstLineIndent") = oDef.FirstLineIndent
            .Item("DocLook_FontName") = oDef.FontName
            .Item("DocLook_FontSize") = oDef.FontSize
            .Item("DocLook_LeftMargin") = oDef.LeftMargin
            .Item("DocLook_LineSpacing") = oDef.LineSpacing
            .Item("DocLook_PageNumAlignment") = oDef.PageNumAlignment
            .Item("DocLook_PageNumFirstPage") = oDef.PageNumFirstPage
            .Item("DocLook_PageNumFormat") = oDef.PageNumFormat
            .Item("DocLook_PageNumLocation") = oDef.PageNumLocation
            .Item("DocLook_PageNumTextAfter") = oDef.PageNumTextAfter
            .Item("DocLook_PageNumTextBefore") = oDef.PageNumTextBefore
            .Item("DocLook_RightMargin") = oDef.RightMargin
            .Item("DocLook_SpaceAfter") = oDef.SpaceAfter
            .Item("DocLook_SpaceBefore") = oDef.SpaceBefore
            .Item("DocLook_SpaceBetweenColumns") = _
                oDef.SpaceBetweenColumns
            .Item("DocLook_TopMargin") = oDef.TopMargin
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CDocumentLook.Save"
End Sub

Private Sub RemoveExistingPageNumber(oRng As Word.Range)
'removes the existing document look
'page number if it exists in range
    Dim iFormat As mpPageNumberFormats
    Dim xBefore As String
    Dim xAfter As String
    Dim oVars As Word.Variables
    Dim oFld As Word.Field
    Dim oStory As Word.Range
    Dim oScope As Word.Range
    Dim iSecFirst As Integer
    Dim iSecLast As Integer
    Dim iSec As Integer
    Dim bInScope As Boolean
    Dim oPgNum As Word.Range
    
    Set oVars = Word.ActiveDocument.Variables

    On Error Resume Next
    iFormat = oVars.Item("DocLook_PageNumFormat")
    If iFormat Then
'       get text before and after
        xBefore = oVars.Item("DocLook_PageNumTextBefore")
        xAfter = oVars.Item("DocLook_PageNumTextAfter")

'       get starting and ending section of scope
        With oRng.Sections
            iSecFirst = .First.Index
            iSecLast = .Last.Index
        End With
        
'       cycle through stories and fields looking for page num
        For Each oStory In Word.ActiveDocument.StoryRanges
            For Each oFld In oStory.Fields
                If oFld.Type = wdFieldPage Then
                    Set oPgNum = oFld.Code
                    
'                   determine if field is in scope
                    iSec = oPgNum.Sections.First.Index
                    bInScope = (iSec >= iSecFirst) And (iSec <= iSecLast)
                    
'                   delete if in scope (ie oRng)
                    If bInScope Then
'                       check for the text before and after
                        oPgNum.MoveStart wdCharacter, -1 - Len(xBefore)
                        oPgNum.MoveEnd wdCharacter, 1 + Len(xAfter)
                    End If
                End If
            Next oFld
        Next oStory
    End If
End Sub

Private Sub Class_Initialize()
    Me.ApplyColumns = True
    Me.ApplyDifferentFirstPage = True
    Me.ApplyFont = True
    Me.ApplyMargins = True
    Me.ApplyPageNumber = True
    Me.ApplyParagraphFormat = True
End Sub

Private Sub Class_Terminate()
    Set m_oDef = Nothing
End Sub
