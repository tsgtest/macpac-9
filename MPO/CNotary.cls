VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNotary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Certificate/Proof of Notary Class
'   created 6/11/00 by Daniel Fisherman

'   Contains properties and methods that
'   define a MacPac Notary

'   Container for CDocument
'**********************************************************

Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oProps As MPO.CCustomProperties
Private m_oDef As mpDB.CNotaryDef
Private m_bInit As Boolean

Public Event BeforeBoilerplateInsert()
Public Event AfterBoilerplateInsert()
Public Event AfterDocumentTitlesSet()

Const mpDocSegment As String = "Notary"
Implements MPO.IDocObj
'**********************************************************
'   Properties
'**********************************************************
Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Property Let TypeID(lNew As Long)
    Dim xDesc As String

    On Error GoTo ProcError
    
    If Me.TypeID <> lNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "TypeID", mpDocSegment, , lNew
        Set m_oDef = g_oDBs.NotaryDefs.Item(lNew)

'       alert and exit if not a valid Type
        If (m_oDef Is Nothing) Then
            xDesc = lNew & " is an invalid Notary TypeID.  " & _
                "Please check tblNotaryTypes in mpPrivate.mdb."
            Err.Raise mpError_InvalidMember
        End If
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CNotary.TypeID", xDesc
End Property

Public Property Get TypeID() As Long
    On Error Resume Next
    TypeID = m_oDoc.RetrieveItem("TypeID", mpDocSegment, , mpItemType_Integer)
End Property

Public Function Definition() As mpDB.CNotaryDef
    If (m_oDef Is Nothing) Then
        Set m_oDef = g_oDBs.NotaryDefs.Item(Me.TypeID)
    End If
    Set Definition = m_oDef
End Function

Public Property Get Gender() As mpGenders
    On Error Resume Next
    Gender = m_oDoc.RetrieveItem("Gender", mpDocSegment, , mpItemType_Integer)
End Property

Public Property Let Gender(iNew As mpGenders)
    If Me.Gender <> iNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "Gender", mpDocSegment, , iNew
        m_oDoc.SetGenderReferences iNew
    End If
End Property

Public Property Get NotaryName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("NotaryName", mpDocSegment)
    NotaryName = xTemp
End Property

Public Property Let NotaryName(xNew As String)
    If Me.NotaryName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpNotaryName", xNew, , False, , , , 60
        m_oDoc.SaveItem "NotaryName", mpDocSegment, , xNew
    End If
End Property

Public Property Get PersonsAppearing() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PersonsAppearing", mpDocSegment)
    PersonsAppearing = xTemp
End Property

Public Property Let PersonsAppearing(xNew As String)
    If Me.PersonsAppearing <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpPersonsAppearing", xNew, , False, , , , 80
        m_oDoc.SaveItem "PersonsAppearing", mpDocSegment, , xNew
    End If
End Property

Public Property Get CompanyName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("CompanyName", mpDocSegment)
    CompanyName = xTemp
End Property

Public Property Let CompanyName(xNew As String)
    If Me.CompanyName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpCompanyName", xNew, , False, , , , 80
        m_oDoc.SaveItem "CompanyName", mpDocSegment, , xNew
    End If
End Property

Public Property Get CompanyState() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("CompanyState", mpDocSegment)
    CompanyState = xTemp
End Property

Public Property Let CompanyState(xNew As String)
    If Me.CompanyState <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpCompanyState", xNew, , False, , , , 80
        m_oDoc.SaveItem "CompanyState", mpDocSegment, , xNew
    End If
End Property

Public Property Get SignerTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerTitle", mpDocSegment)
    SignerTitle = xTemp
End Property

Public Property Let SignerTitle(xNew As String)
    If Me.SignerTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerTitle", xNew, , False, , , , 80
        m_oDoc.SaveItem "SignerTitle", mpDocSegment, , xNew
    End If
End Property

Public Property Get PersonalProved() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PersonalProved", mpDocSegment)
    PersonalProved = xTemp
End Property

Public Property Let PersonalProved(xNew As String)
    Dim oBmk As Word.Bookmark
    If Me.PersonalProved <> xNew Or g_bForceItemUpdate Then
'       atttempt to fill text bookmark
        m_oDoc.EditItem_ALT "zzmpPersonalProved", xNew, , False, , , , 40
        On Error Resume Next
        
'       if checkboxes are to be checked, as in the CA All-Purpose notary,
'       xNew will be an integer - bookmarks will have to be set up to be
'       an integer set of the form XXX1, XXX2, XXX3, etc. - if this is the
'       case, we can click the field whose bookmark is XXX & xNew
        Set oBmk = Word.ActiveDocument.Bookmarks("zzmpFixedCheckPersonalProved" & xNew)
        On Error GoTo ProcError
        If Not (oBmk Is Nothing) Then
            mdlGlobal.ToggleFormsCheckboxSet oBmk.Range
        End If
        
'       save to document
        m_oDoc.SaveItem "PersonalProved", mpDocSegment, , xNew
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CNotary.PersonalProved"
End Property

Public Property Get County() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("County", mpDocSegment)
    County = xTemp
End Property

Public Property Let County(xNew As String)
    If Me.County <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpCounty", xNew, , False, Not Me.IncludeCountyHeading, , , 50
        m_oDoc.SaveItem "County", mpDocSegment, , xNew
    End If
End Property

Public Property Get DocumentTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("DocumentTitle", mpDocSegment)
    DocumentTitle = xTemp
End Property

Public Property Let DocumentTitle(xNew As String)
    If Me.DocumentTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpDocumentTitle", xNew, , False
        m_oDoc.SaveItem "DocumentTitle", mpDocSegment, , xNew
    End If
End Property

Public Property Get DocumentDate() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("DocumentDate", mpDocSegment)
    DocumentDate = xTemp
End Property

Public Property Let DocumentDate(xNew As String)
    If Me.DocumentDate <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpDocumentDate", xNew, , False
        m_oDoc.SaveItem "DocumentDate", mpDocSegment, , xNew
    End If
End Property

Public Property Get NumberOfPages() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("NumberOfPages", mpDocSegment)
    NumberOfPages = xTemp
End Property

Public Property Let NumberOfPages(xNew As String)
    If Me.NumberOfPages <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpNumberOfPages", xNew, , False
        m_oDoc.SaveItem "NumberOfPages", mpDocSegment, , xNew
    End If
End Property

Public Property Get OtherSigners() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("OtherSigners", mpDocSegment)
    OtherSigners = xTemp
End Property

Public Property Let OtherSigners(xNew As String)
    If Me.OtherSigners <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpOtherSigners", xNew, , False
        m_oDoc.SaveItem "OtherSigners", mpDocSegment, , xNew
    End If
End Property

Public Property Let IncludeCountyHeading(bNew As mpTriState)
    If bNew = mpUndefined Then
        bNew = mpFalse
    End If
        
    If Me.IncludeCountyHeading <> bNew Or g_bForceItemUpdate Then
        m_oDoc.HideItem "zzmpCountyHeading", (bNew = 0), False
        m_oDoc.SaveItem "IncludeCountyHeading", mpDocSegment, , CStr(bNew)
    End If
End Property

Public Property Get IncludeCountyHeading() As mpTriState
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("IncludeCountyHeading", mpDocSegment)
    If Len(xTemp) = 0 Then
        IncludeCountyHeading = mpUndefined
    ElseIf CBool(xTemp) Then
        IncludeCountyHeading = mpTrue
    Else
        IncludeCountyHeading = mpFalse
    End If
End Property

Public Property Let IncludeCommission(bNew As mpTriState)
    If bNew = mpUndefined Then
        bNew = mpFalse
    End If
        
    If Me.IncludeCommission <> bNew Or g_bForceItemUpdate Then
        m_oDoc.HideItem "zzmpCommission", (bNew = 0), True
        m_oDoc.SaveItem "IncludeCommission", mpDocSegment, , CStr(bNew)
    End If
End Property

Public Property Get IncludeCommission() As mpTriState
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("IncludeCommission", mpDocSegment)
    If Len(xTemp) = 0 Then
        IncludeCommission = mpUndefined
    ElseIf CBool(xTemp) Then
        IncludeCommission = mpTrue
    Else
        IncludeCommission = mpFalse
    End If
End Property

Public Property Let ExecuteDate(xNew As String)
    If Me.ExecuteDate <> xNew Or g_bForceItemUpdate Then
        If InStr(xNew, "Field") = 0 Then
            m_oDoc.EditItem_ALT "zzmpExecuteDate", xNew, , False, , , , 40
        Else
             m_oDoc.EditDateItem "zzmpExecuteDate", , , mpDateFormat_LongText
        End If
        m_oDoc.SaveItem "ExecuteDate", mpDocSegment, , xNew
    End If
End Property

Public Property Get ExecuteDate() As String
    On Error Resume Next
    ExecuteDate = m_oDoc.RetrieveItem("ExecuteDate", mpDocSegment)
End Property

Public Property Let AppearanceDate(xNew As String)
    If Me.AppearanceDate <> xNew Or g_bForceItemUpdate Then
        If InStr(xNew, "Field") = 0 Then
            m_oDoc.EditItem_ALT "zzmpAppearanceDate", xNew, , False
        Else
             m_oDoc.EditDateItem "zzmpAppearanceDate", , , mpDateFormat_LongText
        End If
        m_oDoc.SaveItem "AppearanceDate", mpDocSegment, , xNew
    End If
End Property

Public Property Get AppearanceDate() As String
    On Error Resume Next
    AppearanceDate = m_oDoc.RetrieveItem("AppearanceDate", mpDocSegment)
End Property

Public Property Get State() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("State", mpDocSegment)
    State = xTemp
End Property

Public Property Let State(xNew As String)
    If Me.State <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpState", xNew, , False, Not Me.IncludeCountyHeading, , , 54
        m_oDoc.SaveItem "State", mpDocSegment, , xNew
    End If
End Property

Public Property Get Custom1() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom1", mpDocSegment)
    Custom1 = xTemp
End Property

Public Property Let Custom1(xNew As String)
    If Me.Custom1 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpNotaryCustom1", xNew, , False, , , , 20
        m_oDoc.SaveItem "Custom1", mpDocSegment, , xNew
    End If
End Property

Public Property Get Custom2() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom2", mpDocSegment)
    Custom2 = xTemp
End Property

Public Property Let Custom2(xNew As String)
    If Me.Custom2 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpNotaryCustom2", xNew, , False, , , , 20
        m_oDoc.SaveItem "Custom2", mpDocSegment, , xNew
    End If
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Definition.BoilerplateFile
End Property

Public Sub Refresh()
    On Error GoTo ProcError
    With Me
        .AppearanceDate = .AppearanceDate
        .PersonsAppearing = .PersonsAppearing
        .County = .County
        .Custom1 = .Custom1
        .Custom2 = .Custom2
        .DocumentTitle = .DocumentTitle
        .ExecuteDate = .ExecuteDate
        .Gender = .Gender
        .IncludeCountyHeading = .IncludeCountyHeading
        .IncludeCommission = .IncludeCommission
        .NotaryName = .NotaryName
        .PersonalProved = .PersonalProved
        .State = .State
        .DocumentTitle = .DocumentTitle
        .DocumentDate = .DocumentDate
        .NumberOfPages = .NumberOfPages
        .OtherSigners = .OtherSigners
        .CompanyName = .CompanyName
        .CompanyState = .CompanyState
        .SignerTitle = .SignerTitle
        .CustomProperties.RefreshValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CNotary.Refresh"
    Exit Sub
End Sub

Public Sub RefreshEmpty()
    Dim oDef As mpDB.CNotaryDef
    
    Set oDef = Me.Definition
    On Error GoTo ProcError
    With Me
        If .AppearanceDate = Empty And oDef.AllowAppearanceDate Then _
            .AppearanceDate = .AppearanceDate
        If .PersonsAppearing = Empty And oDef.AllowAppearingPerson Then _
            .PersonsAppearing = .PersonsAppearing
        If .State = Empty And oDef.AllowState Then _
            .State = .State
        If .County = Empty And oDef.AllowCounty Then _
            .County = .County
        If .ExecuteDate = Empty And oDef.AllowExecuteDate Then _
            .ExecuteDate = .ExecuteDate
        If .DocumentTitle = Empty And oDef.AllowDocumentTitle Then _
            .DocumentTitle = .DocumentTitle
        If .DocumentDate = Empty And oDef.AllowDocumentDate Then _
            .DocumentDate = .DocumentDate
        If .NumberOfPages = Empty And oDef.AllowNumberOfPages Then _
            .NumberOfPages = .NumberOfPages
        If .OtherSigners = Empty And oDef.AllowOtherSigners Then _
            .OtherSigners = .OtherSigners
        If .Gender = Empty And oDef.AllowGender Then _
            .Gender = .Gender
        If .Custom1 = Empty And oDef.Custom1Label <> Empty Then _
            .Custom1 = .Custom1
        If .Custom2 = Empty And oDef.Custom2Label <> Empty Then _
            .Custom2 = .Custom2
        If .IncludeCountyHeading = mpUndefined And oDef.AllowCountyHeading Then _
            .IncludeCountyHeading = False
        If .IncludeCommission = mpUndefined And oDef.AllowCommission Then _
            .IncludeCommission = False
        If .NotaryName = Empty And oDef.AllowNotaryName Then _
            .NotaryName = .NotaryName
        If .PersonalProved = Empty And oDef.AllowPersonalProved Then _
            .PersonalProved = .PersonalProved
        If .CompanyName = Empty And oDef.AllowCompanyName Then _
            .CompanyName = .CompanyName
        If .CompanyState = Empty And oDef.AllowCompanyState Then _
            .CompanyState = .CompanyState
        If .SignerTitle = Empty And oDef.AllowSignerTitle Then _
            .SignerTitle = .SignerTitle
            
        .CustomProperties.RefreshEmptyValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CNotary.RefreshEmpty"
    Exit Sub
End Sub
Public Sub Finish(Optional bForceRefresh As Boolean = False)
    Dim rngP As Range

    On Error GoTo ProcError
    g_bForceItemUpdate = True
    With m_oDoc
        If bForceRefresh Then
            Refresh
        Else
            RefreshEmpty
        End If
        
        .UpdateFields
       
'       create ReuseAuthor doc var - this is necessary
'       to mark document for reuse - no ReuseAuthor, document
'       has not been "created", as far as MacPac is concerned
       Me.Document.SetVar "ReuseAuthor", "0"        '9.7.1 - #4223
       
'CharlieCore 9.2.0 - BaseStyle
'       create BaseStyle doc var - this is necessary for
'       insert Document macro so we know what style should
'       match target Document's Normal font properties
       Word.ActiveDocument.Variables("BaseStyle") = Me.Definition.BaseStyle

'       delete any hidden text - ie text marked for deletion
       .DeleteHiddenText , , , True
       .RemoveFormFields
       
'       run editing macro
        RunEditMacro Me.Template
        
        .rngTrimEmptyParagraphs 'GLOG: 5222, 5223
        
       .SelectStartPosition False
       .ClearBookmarks , .bHideFixedBookmarks
    End With
    
    g_bForceItemUpdate = False
    Exit Sub
ProcError:
    RaiseError "MPO.CNotary.Finish"
    g_bForceItemUpdate = False
End Sub

Friend Sub Initialize()
    On Error GoTo ProcError
    
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidNotaryTemplate
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    m_oDoc.Selection.StartOf
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CNotary.Initialize"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Public Function InsertBoilerplate()
    Dim oPgSet As Word.PageSetup
    
    On Error GoTo ProcError
    RaiseEvent BeforeBoilerplateInsert
    Me.Template.InsertBoilerplate BoilerplateDirectory() & Me.BoilerplateFile
    
'   set up section
    Set oPgSet = Word.ActiveDocument.Sections(1).PageSetup
    
    With Me.Definition
        oPgSet.LeftMargin = .LeftMargin * 72
        oPgSet.RightMargin = .RightMargin * 72
        oPgSet.TopMargin = .TopMargin * 72
        oPgSet.BottomMargin = .BottomMargin * 72
    End With
    
    RaiseEvent AfterBoilerplateInsert
    Exit Function
ProcError:
    RaiseError "MPO.CNotary.InsertBoilerplate"
    Exit Function
End Function

'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    Set m_oTemplate = g_oTemplates.ItemFromClass("CNotary")
End Sub

Private Sub Class_Terminate()
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
    Set m_oDef = Nothing
End Sub

'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
End Property
Private Property Get IDocObj_Author() As mpDB.CPerson
    Set IDocObj_Author = Nothing
End Property
Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property
Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function
Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
End Property
Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
    Set IDocObj_DefaultAuthor = Nothing
End Property
Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function
Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub
Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property
Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub
Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub
Private Sub IDocObj_Refresh()
    Refresh
End Sub
Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function
Private Sub IDocObj_UpdateForAuthor()
End Sub
