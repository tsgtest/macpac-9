Attribute VB_Name = "mdlDialog"
Option Explicit


Public Sub ShowOptions(oPerson As mpDB.CPerson, xTemplate As String, frmP As Form)
'   fill controls with user options -
'   cycle through flds and assign controls
'   of same name the value of the field
    Dim i As Integer
    Dim ctlP As Control
    Dim xValue As String
    Dim optP As mpDB.CPersonOption
    Dim opsP As mpDB.CPersonOptions
    
    Set opsP = oPerson.Options(xTemplate, frmP)
    opsP.Refresh
    
    For i = 2 To opsP.Count
        Set optP = opsP(i)
'       field name is option set item name
'       with different prefix
        If Not IsNull(optP.Value) Then
            xValue = optP.Value
        Else
            xValue = xNullToString(optP.Value)
        End If
        Set ctlP = Nothing
        
        On Error Resume Next
        Set ctlP = frmP.Controls(optP.Name)
        On Error GoTo 0
        
        If Not (ctlP Is Nothing) Then
            If (TypeOf ctlP Is TrueDBList60.TDBCombo) Then
                ctlP.BoundText = xValue
            ElseIf (TypeOf ctlP Is VB.CheckBox) Then
                If xValue Then
                    ctlP = vbChecked
                Else
                    ctlP = vbUnchecked
                End If
            ElseIf (TypeOf ctlP Is mpControls.SpinnerTextBox) Or _
                    (TypeOf ctlP Is mpControls3.SpinTextInternational) Then '*c
                ctlP.Value = xValue
            Else
                ctlP = xValue
            End If
        End If
    Next i
End Sub

Function ControlHasValue(ctlP As Control) As Boolean
'---for some reason, type of isn't returning CheckBox
    If InStr(ctlP.Name, "chk") Or _
        InStr(ctlP.Name, "CheckBox") Then
        ControlHasValue = True
        Exit Function
    End If
  
    If TypeOf ctlP Is TextBox Or _
        TypeOf ctlP Is TrueDBList60.TDBCombo Or _
        TypeOf ctlP Is VB.ComboBox Or _
        TypeOf ctlP Is CheckBox Or _
        TypeOf ctlP Is TrueDBList60.TDBList Or _
        TypeOf ctlP Is VB.ListBox Or _
        TypeOf ctlP Is OptionButton Then
        ControlHasValue = True
    Else
        ControlHasValue = False
    End If
End Function

Function bEnsureSelectedContent(ctlP As VB.Control, _
                                Optional bLastCharOnly As Boolean = False, _
                                Optional bForceSelection As Boolean = False) As Boolean
    Dim ctlData As VB.Data
    On Error Resume Next
    If bForceSelection Then
        With ctlP
            If (TypeOf ctlP Is ListBox) Then
                If .ListIndex < 0 Then _
                    .ListIndex = 0
            Else
                .SelStart = 0
                .SelLength = Len(.Text)
            End If
        End With
    Else
        If IsPressed(9) Then
            With ctlP
                If (TypeOf ctlP Is ListBox) Then
                    If .ListIndex < 0 Then _
                        .ListIndex = 0
                Else
                    If bLastCharOnly = False Then
                        .SelStart = 0
                        .SelLength = Len(.Text)
                    Else
                        .SelStart = Len(.Text)
                        .SelLength = 1
                    End If
                End If
            End With
        End If
    End If
End Function

Function IsPressed(lKey As Long) As Boolean
    IsPressed = (GetKeyState(lKey) < 0)
End Function

Public Function DisableInactiveTabs(frmP As VB.Form)
    Dim ctlP As VB.Control
    On Error Resume Next
    With frmP
        For Each ctlP In .Controls
            If (ctlP.Container Is .MultiPage1) Then
               ctlP.Enabled = (ctlP.Left >= 0)
            End If
        Next
    End With
End Function

Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xarP As XArray
    Dim iRows As Integer
    Dim xDesc As String
    
    On Error GoTo ProcError
    With tdbCBX
        If .Array Is Nothing Then
            xDesc = "No XArray has been assigned to " & tdbCBX.Name & "."
            Err.Raise mpError_ObjectExpected
        End If
        Set xarP = .Array
        iRows = Min(xarP.Count(1), CDbl(iMaxRows))
        On Error Resume Next
        .DropdownHeight = ((iRows) * .RowHeight)
        If Err.Number = 6 Then
 '---use hard value if there's a processor problem -- err 6 is overflow
            .DropdownHeight = (iRows * 239.811)
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.mdlDialog.ResizeTDBCombo", xDesc
    Exit Sub
End Sub

Public Sub MatchInXArrayList(KeyCode As Integer, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'first character is KeyCode
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As XArray
    Dim lStartRow As Long
    Dim xChr As String
    
'   get starting row
    lStartRow = ctlP.Bookmark
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1) - 1
    
'   get the char that we're searching for
    xChr = UCase(Chr(KeyCode))
    
    If lCurRow > lRows Then Exit Sub
    
'   loop through xarray until match with first letter is found
    While (xChr <> UCase(Left(xarP.Value(lCurRow, 1), 1)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        If lCurRow > lRows Then
            Exit Sub
        End If
    Wend
    
    DoEvents
    
'   select the matched item
    With ctlP
        .Bookmark = lCurRow
        .SelBookmarks.Remove 0
        .SelBookmarks.Add lCurRow
        .SetFocus
    End With
End Sub

Public Sub AutoCompleteInXArrayList(KeyCode As Integer, ctlP As Control)
'sets up auto completion

    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As XArray
    Dim lStartRow As Long
    Dim xChr As String
    Static xText As String
    Dim bTried As Boolean
    
'   get starting row
    lStartRow = ctlP.Bookmark
    
'   start with current row
    lCurRow = lStartRow
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1) - 1
    
'   get the char that we're searching for
    xChr = UCase(Chr(KeyCode))
    
'   load it in static var
    xText = xText + xChr
    
    If lCurRow > lRows Then Exit Sub
    
    
TryAgain:
    
'   loop through xarray until match is found
    While (xText <> UCase(Left(xarP.Value(lCurRow, 1), Len(xText))))
        lCurRow = lCurRow + 1
        If lCurRow > lRows Then
            lCurRow = 0
        End If
        If lCurRow = lStartRow Then
            xText = xChr
            If Not bTried Then
                bTried = True
                GoTo TryAgain
            End If
            Exit Sub
        End If
    Wend
    
    DoEvents
    
''   select the matched item
'    With ctlP
'        .Bookmark = lCurRow
'        .SelBookmarks.Remove 0
'        .SelBookmarks.Add lCurRow
'        .SetFocus
'    End With
'   select the matched item
'9.7.3010 #4777
    With ctlP
        .Bookmark = lCurRow
        '#4777
        On Error Resume Next
        .SelBookmarks.Remove 0
        On Error GoTo 0
        .SelBookmarks.Add lCurRow
        .SetFocus
    End With
End Sub

Public Sub GridTabOut(grdGrid As TDBGrid, bEnter As Boolean, iColtoCheck As Integer)
    '---this sub allows a tabout from grid when navigation param is set to grid navigation
    '---normally tabbing halts at last item in grid
    On Error Resume Next
    Select Case bEnter
        Case True
            grdGrid.TabAction = 2
        Case False
'            Debug.Print "Row " & grdGrid.RowBookmark(grdGrid.Row)
'            Debug.Print "UpperBound " & grdGrid.Array.UpperBound(1)
            
            If grdGrid.RowBookmark(grdGrid.Row) = grdGrid.Array.UpperBound(1) Or _
                IsNull(grdGrid.RowBookmark(grdGrid.Row)) Then
                If grdGrid.Col = iColtoCheck Then
                    '---change grid navigation to enable tab out
                    grdGrid.TabAction = 1
                End If
            End If
    Case Else
    End Select
End Sub

Public Sub UnderConstruction()
    MsgBox "Thanks for asking, but this feature is under construction!", vbInformation, App.Title
End Sub
Public Sub RefreshPeople(Optional vID As Variant, Optional oAuthorCombo As TDBCombo)
    Screen.MousePointer = vbHourglass
    g_oDBs.People.Refresh
    g_oDBs.Attorneys.Refresh
    With oAuthorCombo
        .Rebind
        .Bookmark = 0
        On Error Resume Next
        If Not IsMissing(vID) Then
            SelectPerson CLng(vID), oAuthorCombo
            DoEvents
        End If
        .SetFocus
    End With
    Screen.MousePointer = vbDefault
End Sub

Public Sub SelectPerson(lID As Long, oAuthorCombo As TDBCombo)
'selects the row in tdb control whose fldID  = lID
    Dim lRow As Long
    Dim xarP As XArray
    
    Set xarP = oAuthorCombo.Array
    
'   loop until a match is found in the xarray or until
'   all rows in xarray have been passed
    While lID <> xarP.Value(lRow, 2) And lRow <= xarP.Count(1)
        lRow = lRow + 1
    Wend
    
'   select first row if match was not found
    If lRow > xarP.Count(1) Then
        lRow = 0
    End If
    
'   set current row
    oAuthorCombo.Bookmark = lRow
End Sub


Function bForceCaps(ctlText As VB.Control) As Boolean
'force uppercase in control ctlText
'useful in change event of any ctl
'having a text property - needed
'because UCase will force repositioning
'of insertion point

    Dim iStartPos As Integer
    
    With ctlText
        iStartPos = .SelStart
        .Text = UCase(.Text)
        .SelStart = iStartPos
    End With
        
End Function

Function bIsDlgOptControl(ctlC As Control) As Boolean
    If Left(ctlC.Name, 6) = "txtOpt" Or _
            Left(ctlC.Name, 5) = "ddOpt" Or _
            Left(ctlC.Name, 6) = "lstOpt" Or _
            Left(ctlC.Name, 6) = "optOpt" Or _
            Left(ctlC.Name, 6) = "chkOpt" Or _
            Left(ctlC.Name, 6) = "cmbOpt" Or _
            Left(ctlC.Name, 6) = "cbxOpt" Then
        bIsDlgOptControl = True
    Else
        bIsDlgOptControl = False
    End If
End Function

Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    Dim oNum As CNumbers
    
    Set oNum = New CNumbers
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = oNum.Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .LimitToList Then
            If .BoundText = "" Then
                MsgBox "Invalid entry.  Please select an item " & _
                       "from the list.", vbExclamation, App.Title
            Else
'               set text of control equal to the
'               display text of the selected row
                .Text = .Columns(.ListField)
                bValidateBoundTDBCombo = True
            End If
        Else
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Public Function SetControlBackColor(oControl As VB.Control)
    Dim lBackColor As Long
    
    DoEvents
    
'   set previous control's backcolor
    On Error Resume Next
    If TypeOf g_oPrevControl Is CheckBox Or _
            TypeOf g_oPrevControl Is OptionButton Or _
            TypeOf g_oPrevControl Is CommandButton Then
        lBackColor = &H8000000F
    Else
        lBackColor = g_lInactiveCtlColor
    End If
    g_oPrevControl.BackColor = lBackColor
    On Error GoTo ProcError
    
    If Not (TypeOf oControl Is CommandButton) Then
        oControl.BackColor = g_lActiveCtlColor
    End If

'   set active control's backcolor
    Set g_oPrevControl = oControl
    
    Exit Function
ProcError:
    Err.Clear
    Exit Function
End Function

Public Function lGetActiveControlColor() As Long
    Dim lColor As Long
    On Error Resume Next
    lColor = GetMacPacIni("General", "ActiveControlColor")
    If lColor = 0 Then
        lColor = vbCyan
    End If
    lGetActiveControlColor = lColor
End Function

Public Function lGetInactiveControlColor() As Long
    Dim lColor As Long
    On Error Resume Next
    lColor = GetMacPacIni("General", "InactiveControlColor")
    If lColor = 0 Then
        lColor = vbWhite
    End If
    lGetInactiveControlColor = lColor
End Function

Public Sub SelectControlText()
'selects the text in the active control if appropriate
    Static oPrevCtl As VB.Control
    Dim oCurCtl As VB.Control

    On Error Resume Next
    Set oCurCtl = Screen.ActiveControl
    On Error GoTo 0

    If oCurCtl Is Nothing Then
        Exit Sub
    End If

    With oCurCtl
        If Not oCurCtl Is oPrevCtl Then
            If TypeOf oCurCtl Is TextBox Or _
                TypeOf oCurCtl Is mpControls.MultiLineCombo Then
                .SelStart = 0
                .SelLength = Len(.Text)
            ElseIf TypeOf oCurCtl Is mpControls.SpinnerTextBox Then
                .SelStart = 0
                .SelLength = Len(.Value)
            End If
            Set oPrevCtl = oCurCtl
        End If
    End With
End Sub

Public Function OnControlGotFocus(oCtl As Control, _
                                  Optional ByVal xBookmarkToSelect As String, _
                                  Optional bForceSelection As Boolean = False, _
                                  Optional bLastCharSelectOnly As Boolean = False)
'called from GotFocus event procedure of controls -
'handles generic got focus requirements
    Static oDoc As MPO.CDocument
    Dim oBmk As Word.Bookmark
    
    On Error GoTo ProcError
    If oDoc Is Nothing Then
        Set oDoc = New MPO.CDocument
    End If
    DoEvents
    bEnsureSelectedContent oCtl, bLastCharSelectOnly, bForceSelection
    SetControlBackColor oCtl
    
    On Error Resume Next
    Set oBmk = Word.ActiveDocument.Bookmarks(xBookmarkToSelect)
    On Error GoTo ProcError
    
    If Not (oBmk Is Nothing) Then
        oDoc.SelectItem xBookmarkToSelect
    Else
        With Word.Application
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    End If
    Exit Function
ProcError:
    g_oError.Show Err, "Could not successfully select the dialog control."
    Exit Function
End Function

Public Function ShowAboutDialog(Optional MacPac90Version As String, _
                                Optional DialogCaption As String)
    Dim oDlg As frmAbout
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

    Set oDlg = New frmAbout
    If DialogCaption <> Empty Then
        oDlg.Caption = DialogCaption
    End If
    If MacPac90Version <> Empty Then
        oDlg.MacPac90Version = "MacPac90.dll  -  " & MacPac90Version
    End If
    oDlg.Show vbModal
    Unload oDlg
    Set oDlg = Nothing
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

End Function
