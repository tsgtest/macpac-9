VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CReUseAuthor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CReUseAuthor Class
'   created 12/5/99 by Daniel Fisherman
'   Contains properties and methods that
'   define the CReUseAuthor Class - the
'   class that defines the author that
'   is stored in the document when the
'   document is created
'**********************************************************

Implements mpDB.CPerson

Private m_vID As Variant
Private m_iSource As Integer
Private m_xDisplayName As String
Private m_xFullName As String
Private m_xInformalName As String
Private m_xLastName As String
Private m_xFirstName As String
Private m_xMiddleName As String
Private m_Office As mpDB.COffice
Private m_xBarID As String
Private m_xInitials As String
Private m_bIsAtty As Boolean
Private m_xTitle As String
Private m_xCustom As String
Private m_xEmail As String
Private m_xPhone As String
Private m_xFax As String
Private m_xAdmittedIn As String
Private m_oOptions As MPO.CReUseAuthorOptions
Private m_oCustProps As mpDB.CCustomProperties
Private m_oLicenses As mpDB.CLicenses
Private m_oTemplate As MPO.CTemplate

Friend Property Let Template(ByVal oNew As MPO.CTemplate)
    Set m_oTemplate = oNew
End Property

Public Property Get Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Property

Public Property Let BarID(ByVal RHS As String)
    CPerson_BarID = RHS
End Property

Public Property Get BarID() As String
    BarID = CPerson_BarID
End Property

Public Property Let DisplayName(ByVal RHS As String)
    CPerson_DisplayName = RHS
End Property

Public Property Get DisplayName() As String
    DisplayName = CPerson_DisplayName
End Property

Public Property Get Email() As String
    Email = CPerson_EMail
End Property

Public Property Let Email(RHS As String)
    CPerson_EMail = RHS
End Property

Public Property Let Favorite(RHS As Boolean)
    CPerson_Favorite = RHS
End Property

Public Property Get Favorite() As Boolean
    Favorite = CPerson_Favorite
End Property

Public Property Let Fax(ByVal RHS As String)
    CPerson_Fax = RHS
End Property

Public Property Get Fax() As String
    Fax = CPerson_Fax
End Property

Public Property Let FirstName(ByVal RHS As String)
    CPerson_FirstName = RHS
End Property

Public Property Get FirstName() As String
    FirstName = CPerson_FirstName
End Property

Public Property Let FullName(ByVal RHS As String)
    CPerson_FullName = RHS
End Property

Public Property Get FullName() As String
    FullName = CPerson_FullName
End Property

Public Property Get ID() As Variant
    ID = CPerson_ID
End Property

Public Property Let ID(ByVal RHS As Variant)
    CPerson_ID = RHS
End Property

Public Property Let InformalName(ByVal RHS As String)
    CPerson_InformalName = RHS
End Property

Public Property Get InformalName() As String
    InformalName = CPerson_InformalName
End Property

Public Property Let Initials(ByVal RHS As String)
    CPerson_Initials = RHS
End Property

Public Property Get Initials() As String
    Initials = CPerson_Initials
End Property

Public Property Let IsAttorney(RHS As Boolean)
    CPerson_IsAttorney = RHS
End Property

Public Property Get IsAttorney() As Boolean
    IsAttorney = CPerson_IsAttorney
End Property

Public Property Let LastName(ByVal RHS As String)
    CPerson_LastName = RHS
End Property

Public Property Get LastName() As String
    LastName = CPerson_LastName
End Property

Public Property Let MiddleName(ByVal RHS As String)
    CPerson_MiddleName = RHS
End Property

Public Property Get MiddleName() As String
    MiddleName = CPerson_MiddleName
End Property

Public Property Let Office(RHS As mpDB.COffice)
    CPerson_Office = RHS
End Property

Public Property Get Office() As mpDB.COffice
    Office = CPerson_Office
End Property

Public Property Let Phone(ByVal RHS As String)
    CPerson_Phone = RHS
End Property

Public Property Get Phone() As String
    Phone = CPerson_Phone
End Property

Public Property Let Source(RHS As Integer)
    CPerson_Source = RHS
End Property

Public Property Get Source() As Integer
    Source = CPerson_Source
End Property

Public Property Let Title(ByVal RHS As String)
    CPerson_Title = RHS
End Property

Public Property Get Title() As String
    Title = CPerson_Title
End Property

Public Property Get AdmittedIn() As String
    AdmittedIn = CPerson_AdmittedIn
End Property

Public Property Let AdmittedIn(RHS As String)
    CPerson_AdmittedIn = RHS
End Property

'******************************************************
' METHODS
'******************************************************
Public Function Licenses() As CLicenses
    Set Licenses = CPerson_Licenses
End Function

Public Function CustomProperties() As mpDB.CCustomProperties
    Set CustomProperties = CPerson_CustomProperties
End Function

Public Function GetFormattedName(Optional ByVal iFormat As mpDB.mpNameFormats) As String
    GetFormattedName = CPerson_GetFormattedName(iFormat)
End Function

Public Function Options(ByVal xTemplate As String, Optional frmP As Object) As mpDB.CPersonOptions
    Set Options = CPerson_Options(xTemplate, frmP)
End Function

Private Sub Class_Initialize()
    Dim i As Integer
    Set m_oCustProps = New mpDB.CCustomProperties
    Set m_oLicenses = New mpDB.CLicenses
    
    m_oLicenses.Refresh Me.ID
    
    With g_oDBs.CustomPersonProperties
        For i = 1 To .Count
'           add custom properties to person
            m_oCustProps.Add .Item(i).Field, .Item(i).Name
        Next i
    End With
End Sub

Private Sub Class_Terminate()
    Set m_oCustProps = Nothing
    Set m_oLicenses = Nothing
End Sub

'******************************************************
' CPERSON INTERFACE
'******************************************************
Private Property Let CPerson_BarID(ByVal RHS As String)
    m_xBarID = RHS
End Property

Private Property Get CPerson_BarID() As String
    CPerson_BarID = m_xBarID
End Property

Private Function CPerson_CustomProperties() As mpDB.CCustomProperties
    Set CPerson_CustomProperties = m_oCustProps
End Function

Private Property Let CPerson_DisplayName(ByVal RHS As String)
    m_xDisplayName = RHS
End Property

Private Property Get CPerson_DisplayName() As String
    CPerson_DisplayName = m_xDisplayName
End Property

Private Property Let CPerson_EMail(ByVal RHS As String)
    m_xEmail = RHS
End Property

Private Property Get CPerson_EMail() As String
    CPerson_EMail = m_xEmail
End Property

Private Property Let CPerson_AdmittedIn(ByVal RHS As String)
    m_xAdmittedIn = RHS
End Property

Private Property Get CPerson_AdmittedIn() As String
    CPerson_AdmittedIn = m_xAdmittedIn
End Property

Private Property Let CPerson_Favorite(RHS As Boolean)

End Property

Private Property Get CPerson_Favorite() As Boolean
    CPerson_Favorite = False
End Property

Private Property Let CPerson_Fax(ByVal RHS As String)
    m_xFax = RHS
End Property

Private Property Get CPerson_Fax() As String
    CPerson_Fax = m_xFax
End Property

Private Property Let CPerson_FirstName(ByVal RHS As String)
    m_xFirstName = RHS
End Property

Private Property Get CPerson_FirstName() As String
    CPerson_FirstName = m_xFirstName
End Property

Private Property Let CPerson_FullName(ByVal RHS As String)
    m_xFullName = RHS
End Property

Private Property Get CPerson_FullName() As String
    CPerson_FullName = m_xFullName
End Property

Private Function CPerson_GetFormattedName(Optional iFormat As mpDB.mpNameFormats) As String

End Function

Private Property Get CPerson_ID() As Variant
    CPerson_ID = m_vID
End Property

Private Property Let CPerson_ID(ByVal RHS As Variant)
    m_vID = RHS
End Property

Private Property Let CPerson_InformalName(ByVal RHS As String)
    m_xInformalName = RHS
End Property

Private Property Get CPerson_InformalName() As String
    CPerson_InformalName = m_xInformalName
End Property

Private Property Let CPerson_Initials(ByVal RHS As String)
    m_xInitials = RHS
End Property

Private Property Get CPerson_Initials() As String
    CPerson_Initials = m_xInitials
End Property

Private Property Let CPerson_IsAttorney(RHS As Boolean)
    m_bIsAtty = RHS
End Property

Private Property Get CPerson_IsAttorney() As Boolean
    CPerson_IsAttorney = m_bIsAtty
End Property

Private Property Let CPerson_LastName(ByVal RHS As String)
    m_xLastName = RHS
End Property

Private Property Get CPerson_LastName() As String
    CPerson_LastName = m_xLastName
End Property

Private Function CPerson_Licenses() As mpDB.CLicenses
    Set CPerson_Licenses = m_oLicenses
End Function

Private Property Let CPerson_MiddleName(ByVal RHS As String)
    m_xMiddleName = RHS
End Property

Private Property Get CPerson_MiddleName() As String
    CPerson_MiddleName = m_xMiddleName
End Property

Private Property Let CPerson_Office(RHS As mpDB.COffice)
    Set m_Office = RHS
End Property

Private Property Get CPerson_Office() As mpDB.COffice
    Set CPerson_Office = m_Office
End Property

Private Function CPerson_Options(ByVal xTemplate As String, Optional frmP As Object) As mpDB.CPersonOptions
'return object if it exists, else create object
    Dim oReuseOptions As mpDB.CPersonOptions
    
    If (m_oOptions Is Nothing) Then
'       create reuse options object -
'       use as CPersonOptions interface
        Set oReuseOptions = New MPO.CReUseAuthorOptions
        With oReuseOptions
            .Template = Me.Template.ID
            .Refresh
        End With
        Set m_oOptions = oReuseOptions
    End If
    Set CPerson_Options = m_oOptions
End Function

Private Property Let CPerson_Phone(ByVal xNew As String)
    m_xPhone = xNew
End Property

Private Property Get CPerson_Phone() As String
    CPerson_Phone = m_xPhone
End Property

Private Property Let CPerson_Source(RHS As mpPeopleSourceLists)
    m_iSource = RHS
End Property

Private Property Get CPerson_Source() As mpPeopleSourceLists
    CPerson_Source = m_iSource
End Property

Private Property Let CPerson_Title(ByVal RHS As String)
    m_xTitle = RHS
End Property

Private Property Get CPerson_Title() As String
    CPerson_Title = m_xTitle
End Property

Private Function GetReUseAuthorOptions() As mpDB.CPersonOptions
'returns an option set that was stored with the document
    Dim i As Integer
    Dim lLeftPos As Long
    Dim lRightPos As Long
    Dim lSep As Long
    Dim xTemp As String
    Dim xName As String
    Dim xField As String
    Dim xValue As String
    Dim oOptions As mpDB.CPersonOptions
'CharlieCore 9.2.0 - Encrypt
    Dim oDoc As MPO.CDocument
    
'   create options object
'    Set m_oOptions = New mpDB.CPersonOptions
    
'CharlieCore 9.2.0 - Encrypt
    Set oDoc = New MPO.CDocument
    
'   get doc var
'    xTemp = Word.ActiveDocument.Variables("ReuseAuthorOptions")
'CharlieCore 9.2.0 - Encrypt
    xTemp = oDoc.GetVar("ReuseAuthorOptions")
    
    If xTemp <> Empty Then
        Do
'           get next delimiter
            lLeftPos = lRightPos
            lRightPos = InStr(lLeftPos + 1, xTemp, "|")
            
'           get field and value
            If lRightPos Then
'               field ends at right pipe
                xField = Mid(xTemp, lLeftPos + 1, lRightPos - lLeftPos - 1)
            Else
'               no more pipes, field ends at end of string
                xField = Mid(xTemp, lLeftPos + 1)
            End If
            
'           get name and value of field if it exists -
'           field is of the form Name??Value
            If xField <> Empty Then
                lSep = InStr(xField, "??")
                xName = Left(xField, lSep - 1)
                xValue = Mid(xField, lSep + 2)
                
'               assign value to corresponding option
                oOptions.Item(xName) = xValue
            End If
        Loop While lRightPos
    
'       return the modified set to
'       reflect values in document
        Set GetReUseAuthorOptions = oOptions
    Else
        Set GetReUseAuthorOptions = Nothing
    End If
End Function

