VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFaxCovers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   CFaxCovers Collection Class
'   created 4/13/98 by Michael Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of CFaxCovers

'   Member of
'   Container for CFaxCover

'   note that this is a complex collection - it uses the vb
'   collection object as its collection type, but simply fills
'   the id property of the singular object is responsible for
'   saving itself - ie for writing docvars to the document
'**********************************************************
Option Explicit

'**********************************************************
Private Const mpMaxValidID As Integer = 40
Private Const mpDocSegment As String = "FaxCover"
Private m_faxC As MPO.CFaxCover
Private m_Document As MPO.CDocument
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************

'---note:  we are treating faxes as a collection; each fax has a recipient;
'---if user chooses to create one fax cover sheet, the recipients property
'---of the collection, built from the recipient properties of each fax in the collection
'---is used to fill the recipient table
'---if user specifies one sheet per recipient, then we create the collection with recip
'---properties belonging to each member

'---recipient array is modified during each add, delete, or when recipient change event is fired.

Public Property Let SeparatePages(bNew As Boolean)
    If Me.SeparatePages <> bNew Then
        'm_Document.HideItem "zzmpchkSeparatePages", (bNew = 0)
        m_Document.SaveItem "SeparatePages", , , CStr(bNew)
    End If
End Property

Public Property Get SeparatePages() As Boolean
    On Error Resume Next
    SeparatePages = m_Document.RetrieveItem("SeparatePages")
End Property

Public Property Let Recipients(xarNew As XArray)
'   get attorneys names out of xArray
    Dim i As Integer
    Dim xTemp As String
    
    xTemp = xarrayToString(xarNew)
    
'    For i = xarNew.LowerBound(1) To xarNew.UpperBound(1)
'        xTemp = xTemp & xarNew.Value(i, mpAttyXArrayCol_Name)
'        If xarNew.Value(i, mpAttyXArrayCol_BarID) <> "" Then
'            xTemp = xTemp & " #" & xarNew.Value(i, mpAttyXArrayCol_BarID)
'        End If
'        xTemp = xTemp & Chr(11)
'    Next i
'
''   trim trailing char
'    xTemp = Left(xTemp, Len(xTemp) - 1)
    
    'm_Document.EditItem Me.Boilerplate & "_Attorneys",  xTemp, , , , Me.ID
    'm_Document.SaveItem "Recipients", mpDocSegment, Me.ID, , xarNew
End Property

Public Property Get Recipients() As XArray
    'Set Recipients = m_Document.RetrieveItem("Recipients", mpDocSegment, _
                                                                                  Me.ID, mpItemType_XArray, 4)
End Property





'**********************************************************
'   Methods
'**********************************************************
Public Sub InsertBoilerplate(Optional xLocationItem As String, _
            Optional iLocation As Integer = mpDocumentLocation_CurrentSelection)
'insert different boilerplates depending on signature type
    m_Document.InsertBoilerplate "Fax.mbp", _
                                                              "zzmpFaxCoverDefault", _
                                                             xLocationItem, _
                                                             iLocation
End Sub

Public Sub InsertAll()
    
End Sub

Public Function Exists(iID As Integer) As Boolean
'   test for existence by getting value of ID
    On Error Resume Next
    Exists = (m_Document.RetrieveItem("ID", mpDocSegment, iID) <> "")
End Function

Public Function Add() As MPO.CFaxCover
    Dim faxcNew As MPO.CFaxCover
    Dim iID As Integer
    Dim i As Integer
    
    Set faxcNew = New MPO.CFaxCover
    
'   generate error if collection is full
    If Me.Count = mpMaxValidID Then
        Err.Raise mpError_PleadingCounselsCollectionFull
    End If
    
'   generate ID in range 1 to mpMaxCaptions
    iID = 1
    
'   test for id existence
    While Me.Exists(iID)
        iID = iID + 1
    Wend
    
    
    faxcNew.ID = iID
    Set Add = faxcNew
End Function

Public Sub Delete(iID As Integer)
    m_Document.DeleteSegmentValues mpDocSegment, iID
End Sub

Public Function Count() As Long
'     Count = m_colCaptions.Count
    Dim i As Integer
    Dim iCount As Integer
    Dim bExists As Boolean

    For i = 1 To mpMaxValidID
        If Me.Exists(i) Then
            iCount = iCount + 1
        End If
    Next i
    Count = iCount
End Function

Public Function Item(Optional iID As Integer, Optional iIndex As Integer) As CFaxCover
'returns a PleadingCounsel object whose id = iID
    Dim iCount As Integer
    
    If iID = 0 Then
'       we must be looking for index-
'       get id of specified index

'       raise error if index is invalid
        If (iIndex > mpMaxValidID) Or (iIndex < 1) Then
            Err.Raise mpError_SubscriptOutOfRange
        End If
        
'       cycle through all valid IDs in order
        For iID = 1 To mpMaxValidID
            If Me.Exists(iID) Then
'               if a caption with id exists, count it
                iCount = iCount + 1
                If iCount = iIndex Then
'                   if this count is the item
'                   index desired, break out
                    Exit For
                End If
            End If
        Next iID
    End If
    
'   create new pleading caption with ID = iID
    Dim faxcP As MPO.CFaxCover
    Set faxcP = New MPO.CFaxCover
    faxcP.ID = iID
    Set Item = faxcP
End Function

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_Document = New MPO.CDocument
    m_Document.Selection.StartOf
End Sub

Private Sub Class_Terminate()
    Set m_Document = Nothing
End Sub


