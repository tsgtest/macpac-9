VERSION 5.00
Begin VB.Form frmUnloadMsg 
   Caption         =   "Instructions"
   ClientHeight    =   3135
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5100
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   3135
   ScaleWidth      =   5100
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtInstructions 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2265
      Left            =   135
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   165
      Width           =   4875
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "&Print"
      Height          =   435
      Left            =   2535
      TabIndex        =   1
      Top             =   2580
      Width           =   1170
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "O&K"
      Height          =   435
      Left            =   3825
      TabIndex        =   0
      Top             =   2580
      Width           =   1170
   End
End
Attribute VB_Name = "frmUnloadMsg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdOK_Click()
    Unload Me
End Sub

Private Sub cmdPrint_Click()
    Printer.Print Me.txtInstructions
    Printer.EndDoc
End Sub

Private Sub Form_Load()
    Dim fso As New FileSystemObject
    Dim oStream As TextStream
    
    Set oStream = fso.OpenTextFile(App.Path & "\mpUnload.txt", ForReading, True)
    Me.txtInstructions = oStream.ReadAll

End Sub

