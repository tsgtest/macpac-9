VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCaptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   CPleadingCaptions Collection Class
'   created 2/29/00 by Jeffrey Sweetland
'
'   Contains properties and methods that manage the
'   collection of CPleadingCaption

'   Member of
'   Container for CPleadingCaption
'**********************************************************
Private m_Col As Collection
Private m_oDoc As MPO.CDocument
Private m_iIndex As Integer
Private m_MaxValidID As Integer
Private Const mpMaxValidID As Integer = 7
Private Const mpDocSegment As String = "PleadingCaption"
Private Const mpCopySegment As String = "PleadingCaptionCopy"

Public Event ItemRemoved(iIndex As Integer)
Public Event ItemAdded(iIndex As Integer)
Const mpCapTableBookmark As String = "zzmpFIXED_CaptionTable"


Public Function Add(Optional objNewMember As MPO.CPleadingCaption, _
        Optional iID As Integer, _
        Optional sKey As String) As CPleadingCaption
    
'   generate error if collection is full
    If Me.Count = m_MaxValidID Then
        Err.Raise mpError_PleadingCaptionsCollectionFull
    End If
    
    'create a new object
    If objNewMember Is Nothing Then _
        Set objNewMember = New CPleadingCaption
        
    If iID = 0 Then _
        iID = NewIndex
        
    sKey = "C" & Format(iID, "00")
    
    'set the properties passed into the method
    objNewMember.ID = iID
    objNewMember.Key = sKey
    
    If Len(sKey) = 0 Then
        m_Col.Add objNewMember
    Else
        m_Col.Add objNewMember, sKey
    End If
    '---set caption count prop here to avoid circular ref
    objNewMember.CaptionsCount = m_Col.Count
    '---set caption high pos prop here to avoid circular ref
    If m_Col.Count = 1 Then
        objNewMember.HighestPosition = Me.HighPosition
    Else
        objNewMember.HighestPosition = Me.HighPosition + 1
    End If
    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing
    RaiseEvent ItemAdded(m_Col.Count)

End Function
Public Property Get Item(vntIndexKey As Variant) As CPleadingCaption
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
    On Error GoTo ProcError
    Set Item = m_Col(vntIndexKey)
    Exit Property
ProcError:
    g_oError.Raise mpError_InvalidItemIndex, "CPleadingCaptions.Item"
End Property
Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = m_Col.Count
End Property
Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)
    Dim iID As Integer
    iID = m_Col(vntIndexKey).ID
    m_Col.Remove vntIndexKey
    With m_oDoc
        .CopySegmentDocVars mpDocSegment, iID, mpCopySegment
        .DeleteSegmentValues mpDocSegment, iID
    End With
    RaiseEvent ItemRemoved(iID)
End Sub
Public Sub ClearCopiedDocVars()
    Dim i As Integer
    With m_oDoc
'deletes all doc vars belonging to specified doc segement
'   cycle through doc vars
        For i = .File.Variables.Count To 1 Step -1
    '       delete docvar if it belongs to specified segment
            On Error Resume Next
            If InStr(.File.Variables(i).Name, mpCopySegment) > 0 Then
                .File.Variables(i).Delete
            End If
        Next i
    End With
End Sub
Public Sub CancelRemove()
'---used when cancel used for interface called from main pleading -- restores original collection

    With m_oDoc
        .RestoreSegmentDocVars mpDocSegment, mpCopySegment
    End With
    
    If Me.Count = 0 Then
        Me.LoadValues
    End If
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = m_Col.[_NewEnum]
End Property
'**********************************************************
'   Methods
'**********************************************************
Public Sub LoadValues()
    ' Loads previously created pleading captions by checking for
    ' existence of saved document variables
    Dim i As Integer
    Dim capC As CPleadingCaption
    Set m_Col = New Collection
    m_iIndex = 0
    
    For i = 1 To m_MaxValidID
        If Exists(i) Then
            Set capC = New CPleadingCaption
            capC.ID = i
            capC.LoadValues
            Me.Add capC
        End If
    Next i
End Sub

'Public Sub Refresh(Optional sKey As String, _
'                            Optional bRebuild As Boolean = True, _
'                            Optional bAppend As Boolean = False)
'
'    Dim capC As CPleadingCaption
'    Dim iCount As Integer
'    iCount = 1
'    If Len(sKey) > 0 Then
'        Item(sKey).Refresh bRebuild, bAppend
'    Else
'' We're recreating all captions, so first delete any existing document variables
'' These will be recreated by Refresh method of CPleadingCaption
'        m_oDoc.DeleteDocVars mpDocSegment
'        For Each capC In m_Col
'            ' Refresh captions, forcing rebuild for first item only
'            capC.CaptionsCount = m_Col.Count
'            capC.Refresh (iCount = 1), (iCount > 1)
'            iCount = iCount + 1
'        Next
'    End If
'''---remove copied docvars if any items deleted
'    ClearCopiedDocVars
'
'End Sub


Public Sub Refresh(Optional sKey As String, _
                                Optional bRebuildTable As Boolean = True, _
                                Optional bAppend As Boolean = False)
'---9.8.00 these arguments are kept for back-compatibility only
'---They are now generated internally
    
    Dim capC As CPleadingCaption
    Dim iCount As Integer
    'Dim xARTemp As XArrayDB
    Dim xARTemp As XArray
    Dim i As Integer
    Dim iPosOffset As Integer
    Dim iPosToSkip As Integer
    Dim iHighPos As Integer
    Dim xTBM As String
    
    m_oDoc.AdjustTableStyle mpCapTableBookmark, "Caption"

    iCount = 1
'---first create an array of keys/positions
    iHighPos = Me.HighPosition
        
    If Len(sKey) > 0 Then
        Item(sKey).Refresh bRebuildTable, bAppend
    Else
        Set xARTemp = New XArray
        xARTemp.ReDim 1, m_Col.Count, 0, 1
        
        For Each capC In m_Col
            xARTemp.Value(iCount, 0) = capC.Key
            xARTemp.Value(iCount, 1) = capC.Position
            iCount = iCount + 1
            capC.CaptionsCount = m_Col.Count
            capC.HighestPosition = iHighPos
        Next
        
'---sort the array by the .Position column
        Dim xH As New XHelper
        xH.Sort xARTemp, 1
        
'---Delete any existing document variables
'---(will be recreated by Refresh method of CPleadingCounsel)
        m_oDoc.DeleteDocVars mpDocSegment

'---Now cyle through the xArrayDB and create/refresh couns objects by Key
'---Force empty row or cell if .Position <> item * offset
        For i = 1 To xARTemp.Count(1)
            Set capC = Nothing
            Set capC = Item(xARTemp.Value(i, 0))
            If Not capC Is Nothing Then
'---calculate positions to skip
                'If capC.Definition.FullRowFormat Then
'                    If i > 1 Then
'                        iPosToSkip = (capC.Position - (xARTemp(i - 1, 1) + 2)) / 2
'                    Else
'                        'If capC.Position <> 2 Then
'                            iPosToSkip = (capC.Position - (i * 2)) / 2
'                        'End If
'                    End If
'                Else
                    If i > 1 Then
                        iPosToSkip = capC.Position - (xARTemp(i - 1, 1)) - 1
                    Else
                        'If capC.Position <> 2 Then
                            iPosToSkip = (capC.Position - i)
                        'End If
                    End If
                End If
                capC.Refresh (i = 1), (i > 1), iPosToSkip, Me.HighPosition
                
'---9.5.2
''''                '---9.5.0 -- we're going to update the caption bordering only from this call to caption.refresh
''''                capC.UpdateCaptionBorders iPosToSkip, Me.HighPosition
                
'            End If
        
        Next i
    
    End If
    
'---remove copied docvars if any items deleted
    ClearCopiedDocVars
    
End Sub



Public Sub RefreshEmpty()
    Dim capC As CPleadingCaption
    Dim xARTemp As XArray
    Dim i As Integer
    Dim iPosToSkip As Integer
    
    Set xARTemp = ListArray(True)
    
    For i = xARTemp.LowerBound(1) To xARTemp.Count(1)
            Set capC = Nothing
            Set capC = Item(xARTemp.Value(i, 1))
            If Not capC Is Nothing Then
'---calculate positions to skip
                If i > 1 Then
                    iPosToSkip = capC.Position - (xARTemp(i - 1, 2)) - 1
                Else
                    iPosToSkip = (capC.Position - i)
                End If
                capC.RefreshEmpty iPosToSkip, Me.HighPosition
            End If
    
    Next i
    
End Sub

Public Sub Finish(Optional bForceRefresh As Boolean = True)
    If bForceRefresh Then
        Refresh
    Else
        RefreshEmpty
    End If
    If Me.Count > 0 Then
        Me.Item(1).Finish False
    End If
End Sub
'Public Function ListArray() As XArray
'    ' Returns collection as an XArray
'    Dim i As Integer
'    Dim iCount As Integer
'    Dim xARTemp As XArray
'
'    Set xARTemp = New XArray
'
'    iCount = Me.Count
'    If iCount = 0 Then
'        iCount = 1
'    End If
'
'    xARTemp.ReDim 1, iCount, 0, 1
'
'    With xARTemp
'        For i = 1 To .UpperBound(1)
'            .Value(i, 0) = Me.Item(i).DisplayName
'            .Value(i, 1) = Me.Item(i).Key
'        Next i
'    End With
'
'    Set ListArray = xARTemp
'    Set xARTemp = Nothing
'
'End Function


Public Function ListArray(Optional bSortByPosition As Boolean = False) As XArray
    ' Returns collection as an XArray
    Dim i As Integer
    Dim iCount As Integer
    Dim xARTemp As XArray
   
    Set xARTemp = New XArray
    
    iCount = Me.Count
    If iCount = 0 Then
        iCount = 1
    End If
    
    xARTemp.ReDim 1, iCount, 0, 2
    
    With xARTemp
        For i = 1 To .UpperBound(1)
            .Value(i, 0) = Me.Item(i).DisplayName
            .Value(i, 1) = Me.Item(i).Key
            .Value(i, 2) = Me.Item(i).Position
        Next i
    End With
    
    If bSortByPosition Then
        Dim xH As New XHelper
        xH.Sort xARTemp, 2
    End If
    
    Set ListArray = xARTemp
    Set xARTemp = Nothing

End Function

Public Property Get HighPosition() As Long
    'used to determine highest position property in collection
    Dim xARTemp As XArray
    Set xARTemp = Me.ListArray(True)
    HighPosition = xARTemp.Value(xARTemp.UpperBound(1), 2)
End Property


'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set m_Col = New Collection
    Set m_oDoc = New MPO.CDocument
    m_MaxValidID = Max(Val(GetMacPacIni("Pleading", "MaxCaptions")), mpMaxValidID)
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set m_Col = Nothing
End Sub
Private Function Exists(iID As Integer) As Boolean
'   test for existence by getting value of ID
    On Error Resume Next
    Exists = (m_oDoc.RetrieveItem("ID", mpDocSegment, iID) <> "")
End Function
Private Function NewIndex() As Integer
    ' Keeps track of next ID to assign
    ' There may be gaps if items have been deleted
    If Me.Count > m_iIndex Then _
        m_iIndex = Me.Item(Me.Count).ID
    
    m_iIndex = m_iIndex + 1
    NewIndex = m_iIndex
End Function
