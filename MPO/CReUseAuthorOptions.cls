VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CReUseAuthorOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CReUseAuthorOptions Class
'   created 12/5/99 by Daniel Fisherman
'   Contains properties and methods that
'   define the CReUseAuthorOptions Class - the
'   class that defines the options for the
'   author that is stored in the document
'   when the document is created
'**********************************************************

Implements mpDB.CPersonOptions

Private m_oParent As MPO.CReUseAuthor
Private m_oOptions As mpDB.CPersonOptions
Private m_xTemplate As String

'**********************************************************
'   Properties
'**********************************************************
'**********************************************************
'   Class Events
'**********************************************************
'Private Sub Class_Initialize()
''   this is a dummy call - it's necessary because
''   we want to keep CPersonOptions public not creatable
'    Set m_oOptions = g_oDBs.Offices(1).Options("Letter.dot")
'End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Function GetOptions() As mpDB.CPersonOptions
'returns the option set that is stored as a
'string the active document
    Dim lLeftPos As Long
    Dim lRightPos As Long
    Dim xTemp As String
    Dim lSepPos As Long
    Dim xField As String
    Dim xName As String
    Dim xValue As String
    Dim oVar As Word.Variable
    Dim oOffice As mpDB.COffice
'CharlieCore 9.2.0 - Encrypt
    Dim oDoc As MPO.CDocument
    
    On Error GoTo ProcError
    
'   raise error if no template specified
    If m_xTemplate = Empty Then
        Err.Raise mpError_InvalidOptionsTemplateSpecified
        Exit Function
    End If
    
'   get any default option set for this template - we'll
'   modify the values below to reflect what's in the doc.

'   get options
    Set m_oOptions = g_oDBs.People.First.Options(m_xTemplate)
    
'CharlieCore 9.2.0 - Encrypt
    Set oDoc = New MPO.CDocument

'   get options set from document
    On Error Resume Next
    Set oVar = Word.ActiveDocument.Variables("ReuseAuthorOptions")
    On Error GoTo ProcError
    
    If (oVar Is Nothing) Then
'       something is wrong - raise error
        Err.Raise mpError_NoReUseAuthorOptions, _
                          "mpDB.OptionsSetItems.Refresh"
    Else
'       store in string
'        xTemp = oVar.Value
'CharlieCore 9.2.0 - Encrypt
        xTemp = oDoc.GetVar("ReuseAuthorOptions")
    End If
    
    If xTemp = "" Then GoTo SkipVar
    Do
'       get first right side delimiter
        lRightPos = InStr(lLeftPos + 1, xTemp, "|")
        If lRightPos Then
'           there are more values
            xField = Mid(xTemp, lLeftPos + 1, lRightPos - lLeftPos - 1)
        Else
'           this is the last value
            xField = Mid(xTemp, lLeftPos + 1)
        End If
            
'       parse name and value from field and attempt
'       to assign to the appropriate custom property -
'       the property may or may not exist, so skip
'       if it doesn't
        lSepPos = InStr(xField, "??")
        If lSepPos = 0 Then
            Err.Raise mpError_InvalidReuseAuthorCustomFieldStorage
        Else
            xName = Left(xField, lSepPos - 1)
            xValue = Mid(xField, lSepPos + 2)
        End If
        
'       attempt to assign to property
        On Error Resume Next
        m_oOptions.Item(xName).Value = xValue
        On Error GoTo 0
        lLeftPos = lRightPos
    Loop While lRightPos
SkipVar:
    
'   return modified option set
    Set GetOptions = m_oOptions
    Exit Function
    
ProcError:
    RaiseError "MPO.CReuseAuthorOptions.GetOptions"
    Exit Function
End Function

'**********************************************************
'   CPerson Interface Procedures
'**********************************************************
Private Sub CPersonOptions_Copy(oDest As mpDB.CPerson)
    m_oOptions.Copy oDest
End Sub

Private Property Get CPersonOptions_LastEditTime() As Date
    CPersonOptions_LastEditTime = m_oOptions.LastEditTime
End Property

Private Sub CPersonOptions_Refresh(Optional DoXArray As Boolean = True, _
                                   Optional oForm As Object)
'gets the option set stored in the document
'document and fills an xarray
'   get options
    On Error GoTo ProcError
    
    Set m_oOptions = GetOptions()
    
    Exit Sub
    
ProcError:
    RaiseError "MPO.CReuseAuthorOptions.CPersonOptions_Refresh"
    Exit Sub
End Sub

Private Function CPersonOptions_Active() As mpDB.CPersonOption
'   this can be the only one active because there is
'   only one set per document
    Set CPersonOptions_Active = m_oOptions
End Function

Private Function CPersonOptions_Count() As Long
    CPersonOptions_Count = m_oOptions.Count
End Function

Private Sub CPersonOptions_Delete(Optional ByVal lID As Long, Optional ByVal xTemplate As String)
'do nothing - these can't be deleted
End Sub

Private Property Get CPersonOptions_Exists() As Boolean
    CPersonOptions_Exists = True
End Property

Private Property Get CPersonOptions_IsDirty() As Boolean
'always false because they can't be edited like db option sets
    CPersonOptions_IsDirty = False
End Property

Private Property Let CPersonOptions_IsDirty(RHS As Boolean)
'always false because they can't be edited like db option sets
End Property

Private Function CPersonOptions_Item(vID As Variant) As mpDB.CPersonOption
    Set CPersonOptions_Item = m_oOptions.Item(vID)
End Function

Private Property Let CPersonOptions_Owner(oNew As mpDB.CPerson)
    Set m_oParent = oNew
End Property

Private Property Get CPersonOptions_Owner() As mpDB.CPerson
    Set CPersonOptions_Owner = m_oParent
End Property

Private Property Let CPersonOptions_Parent(oNew As Object)
    Set m_oParent = oNew
End Property

Private Property Get CPersonOptions_Parent() As Object
    Set CPersonOptions_Parent = m_oParent
End Property

Private Sub CPersonOptions_Save()
'do nothing - these can't be saved
End Sub

Private Property Let CPersonOptions_Source(iNew As mpPersonOptionSources)
    m_oOptions.Source = iNew
End Property

Private Property Get CPersonOptions_Source() As mpPersonOptionSources
    CPersonOptions_Source = m_oOptions.Source
End Property

Private Property Let CPersonOptions_Template(xNew As String)
    m_xTemplate = xNew
End Property

Private Property Get CPersonOptions_Template() As String
    CPersonOptions_Template = m_xTemplate
End Property
