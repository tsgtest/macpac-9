VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVerification"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Verification Class
'   created 8/24/00 by Ajax Green

'   Contains properties and methods that
'   define a MacPac Verification

'   Container for CDocument, CPleadingPaper
'**********************************************************

Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oProps As MPO.CCustomProperties
Private m_oPaper As MPO.CPleadingPaper
Private m_oDef As mpDB.CVerificationDef
Private m_oAuthor As mpDB.CPerson
Private m_oOffice As mpDB.COffice
Private m_bInit As Boolean

Public Event BeforeBoilerplateInsert()
Public Event AfterBoilerplateInsert()
Public Event AfterDocumentTitlesSet()

Const mpDocSegment As String = "Verification"
Implements MPO.IDocObj
'**********************************************************
'   Properties
'**********************************************************
Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Property Get DefaultAuthor() As mpDB.CPerson
    Set DefaultAuthor = Me.Template.DefaultAuthor
End Property

Public Property Let DefaultAuthor(psnNew As mpDB.CPerson)
   Me.Template.DefaultAuthor = psnNew
End Property

Public Property Let Author(oNew As mpDB.CPerson)
    Dim bDo As Boolean
    
    On Error GoTo ProcError
    If oNew Is Nothing Then Exit Property
'   set property if no author currently exists
'   or if the author has changed
    If m_oAuthor Is Nothing Then
        bDo = True
    ElseIf m_oAuthor.ID <> oNew.ID Then
        bDo = True
    End If
    
    If bDo Then
        Set m_oAuthor = oNew
'       save the id for future retrieval
        m_oDoc.SaveItem "Author", mpDocSegment, , oNew.ID
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CVerification.Author"
    Exit Property
End Property
Public Property Get Author() As mpDB.CPerson
    Dim lAuthorID As Long
    If m_oAuthor Is Nothing Then
'       attempt to get author from document
        lAuthorID = m_oDoc.RetrieveItem("Author", mpDocSegment, , mpItemType_Integer)

'       if an author id has been retrieved from doc
'       the doc has an author set already
        If lAuthorID <> 0 Then 'mpUndefined Then
            Me.Author = g_oDBs.People(lAuthorID)
        End If
    End If
    Set Author = m_oAuthor
End Property
Public Property Let AuthorName(xNew As String)
    If Me.AuthorName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpAuthorName", xNew, , False, , , , 30 '*c
        m_oDoc.SaveItem "AuthorName", mpDocSegment, , xNew
    End If
End Property

Public Property Get AuthorName() As String
    On Error Resume Next
    AuthorName = m_oDoc.RetrieveItem("AuthorName", mpDocSegment)
End Property

Public Property Let TypeID(lNew As Long)
    Dim xDesc As String

    On Error GoTo ProcError
    
    If Me.TypeID <> lNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "TypeID", mpDocSegment, , lNew
        Set m_oDef = g_oDBs.VerificationDefs.Item(lNew)

    '       alert and exit if not a valid Type
        If (m_oDef Is Nothing) Then
            Err.Raise mpError_InvalidMember
        End If
    End If
    
    Exit Property

ProcError:
    If Err.Number = mpError_InvalidMember Then
        xDesc = "Invalid Verification TypeID.  " & _
            "Please check the appropriate tblUserOptions " & _
            "table in mpPrivate.mdb."
    Else
        xDesc = g_oError.Desc(Err.Number)
    End If
    RaiseError "MPO.CVerification.TypeID", xDesc
End Property
Public Property Get TypeID() As Long
    On Error Resume Next
    TypeID = m_oDoc.RetrieveItem("TypeID", mpDocSegment, , mpItemType_Integer)
End Property
Public Property Get DocumentTitles() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("DocumentTitles", mpDocSegment)
    DocumentTitles = xTemp
End Property
Public Property Let DocumentTitles(xNew As String)
    On Error GoTo ProcError
    If Me.DocumentTitles <> xNew Or g_bForceItemUpdate Then
        xNew = xSubstitute(xNew, vbCrLf & vbCrLf, "*@*|*@*")
        xNew = xSubstitute(xNew, vbCrLf, ", ") 'Chr(11))
        xNew = xSubstitute(xNew, "*@*|*@*", ", ") 'vbCrLf)
        m_oDoc.EditItem_ALT "zzmpDocumentTitles", xNew, vbCr, False, , , , 80
        m_oDoc.SaveItem "DocumentTitles", mpDocSegment, , xNew
        RaiseEvent AfterDocumentTitlesSet
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CVerification.DocumentTitles"
    Exit Property
End Property
Public Property Let Office(oNew As mpDB.COffice)
    Dim bDo As Boolean
    
    On Error GoTo ProcError
    If oNew Is Nothing Then Exit Property
'   set property if no author currently exists
'   or if the author has changed
    If m_oOffice Is Nothing Then
        bDo = True
    ElseIf m_oOffice.ID <> oNew.ID Then
        bDo = True
    End If
    
    If bDo Then
        Set m_oOffice = oNew
        
'       save the id for future retrieval
        m_oDoc.SaveItem "Office", mpDocSegment, , oNew.ID
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CVerification.Office"
    Exit Property
End Property

Public Property Get Office() As mpDB.COffice
    Dim lID As Long
    If m_oOffice Is Nothing Then
'       attempt to get Office from document
        lID = m_oDoc.RetrieveItem("Office", mpDocSegment, , mpItemType_Integer)

'       if an Office id has been retrieved from doc
'       the doc has an Office set already
        If lID <> 0 Then 'mpUndefined Then
            Me.Office = g_oDBs.Offices(lID)
        End If
    End If
    Set Office = m_oOffice
End Property

Public Function Definition() As mpDB.CVerificationDef
    If (m_oDef Is Nothing) Then
        Set m_oDef = g_oDBs.VerificationDefs.Item(Me.TypeID)
    End If
    Set Definition = m_oDef
End Function
Public Property Get SignerGender() As mpGenders
    On Error Resume Next
    SignerGender = m_oDoc.RetrieveItem("SignerGender", mpDocSegment, , mpItemType_Integer)
End Property
Public Property Let SignerGender(iNew As mpGenders)
    If Me.SignerGender <> iNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "SignerGender", mpDocSegment, , iNew
        m_oDoc.SetGenderReferences iNew
    End If
End Property

Public Property Get VerificationState() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("VerificationState", mpDocSegment)
    VerificationState = xTemp
End Property
Public Property Let VerificationState(xNew As String)
    If Me.VerificationState <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpVerificationState", xNew, , False, , , , 20
        m_oDoc.SaveItem "VerificationState", mpDocSegment, , xNew
    End If
End Property
Public Property Get VerificationCounty() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("VerificationCounty", mpDocSegment)
    VerificationCounty = xTemp
End Property
Public Property Let VerificationCounty(xNew As String)
    If Me.VerificationCounty <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpVerificationCounty", xNew, , False, , , , 20
        m_oDoc.SaveItem "VerificationCounty", mpDocSegment, , xNew
    End If
End Property
Public Property Get VerificationCity() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("VerificationCity", mpDocSegment)
    VerificationCity = xTemp
End Property
Public Property Let VerificationCity(xNew As String)
    If Me.VerificationCity <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpVerificationCity", xNew, , False, , , , 20
        m_oDoc.SaveItem "VerificationCity", mpDocSegment, , xNew
    End If
End Property
Public Property Get FirmAddress() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("FirmAddress", mpDocSegment)
    FirmAddress = xTemp
End Property
Public Property Let FirmAddress(xNew As String)
    If Me.FirmAddress <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpFirmAddress", xNew, , False, , , , 20
        m_oDoc.SaveItem "FirmAddress", mpDocSegment, , xNew
    End If
End Property
Public Property Get FirmState() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("FirmState", mpDocSegment)
    FirmState = xTemp
End Property
Public Property Let FirmState(xNew As String)
    If Me.FirmState <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpFirmState", xNew, , False, , , , 20
        m_oDoc.SaveItem "FirmState", mpDocSegment, , xNew
    End If
End Property

Public Property Get CorporationName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("CorporationName", mpDocSegment)
    CorporationName = xTemp
End Property
Public Property Let CorporationName(xNew As String)
    If Me.CorporationName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpCorporationName", xNew, , False, , , , 20
        m_oDoc.SaveItem "CorporationName", mpDocSegment, , xNew
    End If
End Property
Public Property Get CorporatePosition() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("CorporatePosition", mpDocSegment)
    CorporatePosition = xTemp
End Property
Public Property Let CorporatePosition(xNew As String)
    If Me.CorporatePosition <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpCorporatePosition", xNew, , False, , , , 20
        m_oDoc.SaveItem "CorporatePosition", mpDocSegment, , xNew
    End If
End Property
Public Property Get AttorneysFor() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("AttorneysFor", mpDocSegment)
    AttorneysFor = xTemp
End Property
Public Property Let AttorneysFor(xNew As String)
    If Me.AttorneysFor <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpAttorneysFor", xNew, , False, , , , 20
        m_oDoc.SaveItem "AttorneysFor", mpDocSegment, , xNew
    End If
End Property
Public Property Get DeceasedName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("DeceasedName", mpDocSegment)
    DeceasedName = xTemp
End Property
Public Property Let DeceasedName(xNew As String)
    If Me.DeceasedName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpDeceasedName", xNew, , False, , , , 20
        m_oDoc.SaveItem "DeceasedName", mpDocSegment, , xNew
    End If
End Property
Public Property Get ManualEntry() As Boolean
    Dim vTemp As Variant
    
    On Error Resume Next
    vTemp = m_oDoc.RetrieveItem( _
        "ManualEntry", mpDocSegment, , mpItemType_Boolean)
    If vTemp <> mpUndefined Then _
        ManualEntry = vTemp
End Property

Public Property Let ManualEntry(bNew As Boolean)
    If Me.ManualEntry <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "ManualEntry", mpDocSegment, , bNew
    End If
End Property

Public Property Get NotaryName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("NotaryName", mpDocSegment)
    NotaryName = xTemp
End Property
Public Property Let NotaryName(xNew As String)
    If Me.NotaryName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpNotaryName", xNew, , False, , , , 20
        m_oDoc.SaveItem "NotaryName", mpDocSegment, , xNew
    End If
End Property
Public Property Let ExecuteDate(xNew As String)
    If Me.ExecuteDate <> xNew Or g_bForceItemUpdate Then
        If InStr(xNew, "Field") = 0 Then
            m_oDoc.EditItem_ALT "zzmpExecuteDate", xNew, , False
        Else
             m_oDoc.EditDateItem "zzmpExecuteDate", , , mpDateFormat_LongText
        End If
        m_oDoc.SaveItem "ExecuteDate", mpDocSegment, , xNew
    End If
End Property
Public Property Get ExecuteDate() As String
    On Error Resume Next
    ExecuteDate = m_oDoc.RetrieveItem("ExecuteDate", mpDocSegment)
End Property
Public Property Get PartyName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PartyName", mpDocSegment)
    PartyName = xTemp
End Property
Public Property Let PartyName(xNew As String)
    If Me.PartyName <> xNew Or g_bForceItemUpdate Then
        xNew = xSubstitute(xNew, vbCrLf & vbCrLf, "*@*|*@*")
        xNew = xSubstitute(xNew, vbCrLf, ", ") 'Chr(11))
        xNew = xSubstitute(xNew, "*@*|*@*", ", ") 'vbCrLf)
        m_oDoc.EditItem_ALT "zzmpPartyName", xNew, , False, , , , 30
        m_oDoc.SaveItem "PartyName", mpDocSegment, , xNew
    End If
End Property
Public Property Get PartyTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PartyTitle", mpDocSegment)
    PartyTitle = xTemp
End Property
Public Property Let PartyTitle(xNew As String)
    If Me.PartyTitle <> xNew Or g_bForceItemUpdate Then
        xNew = xSubstitute(xNew, vbCrLf & vbCrLf, "*@*|*@*")
        xNew = xSubstitute(xNew, vbCrLf, ", ") 'Chr(11))
        xNew = xSubstitute(xNew, "*@*|*@*", ", ") 'vbCrLf)
        m_oDoc.EditItem_ALT "zzmpPartyTitle", xNew, , False, , , , 30
        m_oDoc.SaveItem "PartyTitle", mpDocSegment, , xNew
    End If
End Property
Public Property Get Custom1() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom1", mpDocSegment)
    Custom1 = xTemp
End Property
Public Property Let Custom1(xNew As String)
    If Me.Custom1 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpVerificationCustom1", xNew, , False, , , , 20
        m_oDoc.SaveItem "Custom1", mpDocSegment, , xNew
    End If
End Property
Public Property Get Custom2() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom2", mpDocSegment)
    Custom2 = xTemp
End Property
Public Property Let Custom2(xNew As String)
    If Me.Custom2 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpVerificationCustom2", xNew, , False, , , , 20
        m_oDoc.SaveItem "Custom2", mpDocSegment, , xNew
    End If
End Property
'Public Property Get FooterText() As String
'    On Error Resume Next
'    Dim xTemp As String
'    xTemp = m_oDoc.RetrieveItem("FooterText", mpDocSegment)
'    FooterText = xTemp
'End Property
'Public Property Let FooterText(xNew As String)
'    If Me.FooterText <> xNew Or g_bForceItemUpdate Then
'        m_oDoc.EditItem_ALT "zzmpFooterText", xNew, , False
'        m_oDoc.SaveItem "FooterText", mpDocSegment, , xNew
'    End If
'End Property
Public Property Get FirstNumberedPage() As Integer
    On Error Resume Next
     FirstNumberedPage = m_oDoc.RetrieveItem(" FirstNumberedPage")
End Property
Public Property Let FirstNumberedPage(xNew As Integer)
    If Me.FirstNumberedPage <> xNew Or g_bForceItemUpdate Then
        '---call page number setting method
        Me.Definition.FirstNumberedPage = xNew
        m_oDoc.SaveItem " FirstNumberedPage", , , xNew
    End If
End Property

Public Property Get FirstPageNumber() As Integer
    On Error Resume Next
     FirstPageNumber = m_oDoc.RetrieveItem(" FirstPageNumber")
End Property
Public Property Let FirstPageNumber(xNew As Integer)
    If Me.FirstPageNumber <> xNew Or g_bForceItemUpdate Then
        '---call page number setting method
        Me.Definition.FirstPageNumber = xNew
        m_oDoc.SaveItem " FirstPageNumber", , , xNew
    End If
End Property

Public Property Get Options() As mpDB.CPersonOptions
    On Error GoTo ProcError
    If Me.Author.ID = Empty Then
'       person has no ID, return default office options
        Set Options = g_oDBs.Offices.Default.Options(Me.Template.FileName)
    Else
'       return the options for the author
        Set Options = Me.Author.Options(Me.Template.FileName)
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CVerification.Options"
    Exit Property
End Property
'**********************************************************
'   Methods
'**********************************************************
Public Function PleadingPaper() As MPO.CPleadingPaper
    Set PleadingPaper = m_oPaper
End Function
Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Definition.BoilerplateFile
End Property
Public Sub Refresh()
    Dim i As Integer
    On Error GoTo ProcError
    With Me
        .AuthorName = .AuthorName
        .AttorneysFor = .AttorneysFor
        .CorporatePosition = .CorporatePosition
        .CorporationName = .CorporationName
        .Custom1 = .Custom1
        .Custom2 = .Custom2
        .PartyName = .PartyName
        .PartyTitle = .PartyTitle
        .SignerGender = .SignerGender
        .VerificationCity = .VerificationCity
        .VerificationCounty = .VerificationCounty
        .VerificationState = .VerificationState
        .ExecuteDate = .ExecuteDate
        .DeceasedName = .DeceasedName
        .NotaryName = .NotaryName
        .FirmAddress = .FirmAddress
        .FirmState = .FirmState
        .DocumentTitles = .DocumentTitles
        .CustomProperties.RefreshValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CVerification.Refresh"
    Exit Sub
End Sub
Public Sub RefreshEmpty()
    Dim i As Integer
    Dim oDef As mpDB.CVerificationDef
    On Error GoTo ProcError
    Set oDef = Me.Definition
    With Me
        If .AuthorName = Empty Then .AuthorName = .AuthorName
        If .AttorneysFor = Empty And oDef.AllowAttorneyFor Then _
            .AttorneysFor = .AttorneysFor
        If .CorporatePosition = Empty And oDef.AllowCorporatePosition Then _
            .CorporatePosition = .CorporatePosition
        If .CorporationName = Empty And oDef.AllowCorporationName Then _
            .CorporationName = .CorporationName
        If .Custom1 = Empty And oDef.Custom1Label <> Empty Then _
            .Custom1 = .Custom1
        If .Custom2 = Empty And oDef.Custom2Label <> Empty Then _
            .Custom2 = .Custom2
        If .PartyName = Empty And oDef.AllowPartyName Then _
            .PartyName = .PartyName
        If .PartyTitle = Empty And oDef.AllowPartyTitle Then _
            .PartyTitle = .PartyTitle
        If .SignerGender = Empty And oDef.AllowGender Then _
            .SignerGender = .SignerGender
        If .VerificationCounty = Empty Then _
            .VerificationCounty = .VerificationCounty
        If .VerificationCity = Empty Then _
            .VerificationCity = .VerificationCity
        If .VerificationState = Empty Then _
            .VerificationState = .VerificationState
        If .DocumentTitles = Empty Then _
            .DocumentTitles = .DocumentTitles
        If .ExecuteDate = Empty And oDef.AllowExecuteDate Then _
            .ExecuteDate = .ExecuteDate
        If .NotaryName = Empty And oDef.AllowNotaryName Then _
            .NotaryName = .NotaryName
        If .DeceasedName = Empty And oDef.AllowDeceasedName Then _
            .DeceasedName = .DeceasedName
        
        .CustomProperties.RefreshEmptyValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CVerification.RefreshEmpty"
    Exit Sub
End Sub
Public Sub Finish(Optional bForceRefresh As Boolean = False)
    Dim rngP As Range
    Dim oDef As mpDB.CVerificationDef
    Dim oWordDoc As Object
    
    On Error GoTo ProcError
    
    g_bForceItemUpdate = True
    With m_oDoc
        If bForceRefresh Then
            Refresh
        Else
            RefreshEmpty
        End If

'       save author for reuse if template is Verification
        If Me.Template.FileName = Document.Template Then _
            .StorePersonInDoc Me.Author
            
        .UpdateFields
       
'CharlieCore 9.2.0 - BaseStyle
        Set oDef = Me.Definition
        
'       create BaseStyle doc var - this is necessary for
'       insert Document macro so we know what style should
'       match target Document's Normal font properties
       Word.ActiveDocument.Variables("BaseStyle") = oDef.BaseStyle

'       delete any hidden text - ie text marked for deletion
       .DeleteHiddenText , , , True
       .RemoveFormFields
       
'       run editing macro
        RunEditMacro Me.Template
        
       .SelectStartPosition False
       .ClearBookmarks , .bHideFixedBookmarks
       
''---   update pleading paper last, since
'       its update method clears bookmarks!
       With Me.PleadingPaper
       
            If Not bIsValidPleadingPaper(Me.Definition.ID, _
                                         "tblPleadingPaperAssignments.fldVerificationTypeID", _
                                         .PleadingPaperDef.ID) Then
                Set .PleadingPaperDef = g_oDBs.PleadingPaperDefs.Item(Me.Definition.DefaultPleadingPaperType)
            End If

            .StartPageNumberingAt = oDef.FirstNumberedPage
            .StartingPageNumber = oDef.FirstPageNumber
            .UseBoilerplateFooterSetup = oDef.UseBoilerplateFooterSetup
            If .UseBoilerplateFooterSetup = True Then
                .SideBarType = mpPleadingSideBarNone
            End If
            '#3684
            If g_bUseDefOfficeInfo Then
                Set .Office = g_oDBs.Offices.Default
            Else
                Set .Office = Me.Author.Office
            End If
'            If m_oDoc.Template = Me.Template.FileName Then
'                ' If Verification template, update whole document
'                .Update
'            Else
                ' Else just update last section
                .Update m_oDoc.File.Sections.Last.Index
'            End If
        End With
        
    End With
    
    g_bForceItemUpdate = False
    Exit Sub
ProcError:
    RaiseError "MPO.CVerification.Finish"
        g_bForceItemUpdate = False
End Sub

Friend Sub Initialize()
    On Error GoTo ProcError
    
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidVerificationTemplate
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    With m_oDoc
        .Selection.StartOf
        If .IsCreated And Me.Template.FileName = _
                Word.ActiveDocument.AttachedTemplate And _
                Not (.ReuseAuthor Is Nothing) Then
            Me.Author = .ReuseAuthor
        ElseIf Not (Me.DefaultAuthor Is Nothing) Then
            Me.Author = Me.DefaultAuthor
            If Word.ActiveDocument.Variables.Count = 0 Then
                Me.UpdateForAuthor
            End If
        Else
            Me.Author = g_oDBs.People.First
            Me.UpdateForAuthor
        End If
    End With
    With m_oPaper
        ' Set pleading paper to default type if not already created in document
        If (Not .Exists) Or (Not m_oDoc.IsCreated) Then
            Set .PleadingPaperDef = _
                g_oDBs.PleadingPaperDefs.Item(Me.Definition.DefaultPleadingPaperType)
        End If
    End With

    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CVerification.Initialize"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Public Function UpdateForAuthor()
' updates relevant Verification properties to
' values belonging to current Author
    On Error GoTo ProcError
    
    Me.AuthorName = Me.Author.FullName
'   assign author's office as Verification office
    Me.Office = Me.Author.Office

'   update service properties linked to service office
    UpdateForOffice
    
'   update custom properties
    Me.CustomProperties.UpdateForAuthor Me.Author, Nothing
    Exit Function
ProcError:
    RaiseError "MPO.CVerification.UpdateForAuthor"
    Exit Function
End Function
Public Function UpdateForOffice()
' updates relevant service properties to
' values belonging to office
    On Error GoTo ProcError
    With Me.Office
        Me.FirmAddress = .Address(Me.Definition.AddressFormat)
        Me.VerificationCity = .City
        Me.VerificationState = .State
        Me.FirmState = .State
    End With
    Exit Function
ProcError:
    RaiseError "MPO.CVerification.UpdateForOffice"
    Exit Function
End Function
Public Function InsertBoilerplate()
    On Error GoTo ProcError
    RaiseEvent BeforeBoilerplateInsert
    Me.Template.InsertBoilerplate BoilerplateDirectory() & Me.BoilerplateFile
    RaiseEvent AfterBoilerplateInsert
    Exit Function
ProcError:
    RaiseError "MPO.CVerification.InsertBoilerplate"
    Exit Function
End Function
'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    Set m_oPaper = New MPO.CPleadingPaper
    
    Set m_oTemplate = g_oTemplates.ItemFromClass("CVerification")
End Sub
Private Sub Class_Terminate()
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
    Set m_oAuthor = Nothing
    Set m_oPaper = Nothing
    Set m_oDef = Nothing
End Sub
'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
    Me.Author = RHS
End Property
Private Property Get IDocObj_Author() As mpDB.CPerson
    Set IDocObj_Author = Me.Author
End Property
Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property
Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function
Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
    Me.DefaultAuthor = RHS
End Property
Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
    Set IDocObj_DefaultAuthor = Me.DefaultAuthor
End Property
Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function
Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub
Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property
Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub
Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub
Private Sub IDocObj_Refresh()
    Refresh
End Sub
Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function
Private Sub IDocObj_UpdateForAuthor()
    Me.UpdateForAuthor
End Sub
