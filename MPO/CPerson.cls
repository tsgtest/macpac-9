VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPerson"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   Person Class
'   created 12/11/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac person

'   Member of CPersons
'   Container for  COffice, CPersonOptions
'**********************************************************

'**********************************************************
Option Explicit

Private m_vID As Variant
Private m_iSource As Integer
Private m_xDisplayName As String
Private m_xFullName As String
Private m_xInformalName As String
Private m_xLastName As String
Private m_xFirstName As String
Private m_xMiddleName As String
Private m_Office As mpDB.COffice
Private m_xBarID As String
Private m_xInitials As String
Private m_bIsAtty As Boolean
Private m_xTitle As String
Private m_xCustom As String
Private m_xEMail As String
Private m_xPhone As String
Private m_xFax As String
Private m_Options As mpDB.CPersonOptions
Private m_oCustProps As CCustomProperties

Public Enum mpPeopleSourceLists
    mpPeopleSourceList_None = 0
    mpPeopleSourceList_People = 1
    mpPeopleSourceList_PeopleInput = 3
End Enum

Enum mpNameFormats
    mpNameFormat_Informal = 1
    mpNameFormat_FirstLast = 2
    mpNameFormat_LastFirst = 3
    mpNameFormat_FirstMidLast = 4
    mpNameFormat_LastFirstMid = 5
End Enum

'**********************************************************

'************************************************************************
'Properties
'************************************************************************
Public Property Let ID(ByVal vData As Variant)
    m_vID = vData
End Property

Public Property Get ID() As Variant
    ID = m_vID
End Property

Public Property Let Source(iNew As Integer)
    m_iSource = iNew
End Property

Public Property Get Source() As Integer
    Source = m_iSource
End Property

Public Property Let Favorite(bNew As Boolean)
'adds/deletes person id to favorites table
    Dim xSQL As String
    
    If bNew Then
        xSQL = "INSERT INTO tblFavorites (fldID) VALUES (" & Me.ID & ")"
    Else
        xSQL = "DELETE * FROM tblFavorites WHERE fldID = " & Me.ID
    End If
    
    Application.PrivateDB.Execute xSQL
End Property

Public Property Get Favorite() As Boolean
'returns true iff ID is in favorites table
    Dim xSQL As String
    Dim rs As DAO.Recordset
    
    xSQL = "SELECT fldID FROM tblFavorites " & _
           "WHERE fldID = " & Me.ID
    
    Set rs = Application.PrivateDB.OpenRecordset(xSQL, _
                dbOpenForwardOnly, dbReadOnly)
    Favorite = rs.RecordCount
    rs.Close
    Set rs = Nothing
End Property

Public Property Let Office(offNew As mpDB.COffice)
    Set m_Office = offNew
End Property

Public Property Get Office() As mpDB.COffice
    Set Office = m_Office
End Property

Public Property Let DisplayName(ByVal xData As String)
    m_xDisplayName = xData
End Property

Public Property Get DisplayName() As String
        DisplayName = m_xDisplayName
End Property

Public Property Let InformalName(ByVal xData As String)
    m_xInformalName = xData
End Property

Public Property Get InformalName() As String
        InformalName = m_xInformalName
End Property

Public Property Let FullName(ByVal xData As String)
    m_xFullName = xData
End Property

Public Property Get FullName() As String
        FullName = m_xFullName
End Property

Public Property Let LastName(ByVal xData As String)
    m_xLastName = xData
End Property

Public Property Get LastName() As String
        LastName = m_xLastName
End Property

Public Property Let FirstName(ByVal xData As String)
    m_xFirstName = xData
End Property

Public Property Get FirstName() As String
        FirstName = m_xFirstName
End Property

Public Property Let MiddleName(ByVal xData As String)
    m_xMiddleName = xData
End Property

Public Property Get MiddleName() As String
        MiddleName = m_xMiddleName
End Property

Public Property Get BarID() As String
        BarID = m_xBarID
End Property

Public Property Let BarID(ByVal xData As String)
    m_xBarID = xData
End Property

Public Property Get Initials() As String
        Initials = m_xInitials
End Property

Public Property Let Initials(ByVal xData As String)
    m_xInitials = xData
End Property

Public Property Get Title() As String
        Title = m_xTitle
End Property

Public Property Let Title(ByVal xData As String)
    m_xTitle = xData
End Property

Public Property Get EMail() As String
        EMail = m_xEMail
End Property

Public Property Let EMail(ByVal xData As String)
    m_xEMail = xData
End Property

Public Property Get Phone() As String
        Phone = m_xPhone
End Property

Public Property Let Phone(ByVal xData As String)
    m_xPhone = xData
End Property

Public Property Get Fax() As String
        Fax = m_xFax
End Property

Public Property Let Fax(ByVal xData As String)
    m_xFax = xData
End Property

Public Property Let IsAttorney(bNew As Boolean)
    m_bIsAtty = bNew
End Property

Public Property Get IsAttorney() As Boolean
    IsAttorney = m_bIsAtty
End Property

'************************************************************************
'Methods
'************************************************************************
Public Function CustomProperties() As CCustomProperties
    Set CustomProperties = m_oCustProps
End Function

Public Function Options(xTemplate As String, Optional frmP As Object) As mpDB.CPersonOptions
'returns a PersonOptions object containing
'the options for template xTemplate
    Dim m_frmP As Form
    Set m_frmP = frmP
    
    If (m_Options Is Nothing) Then
'       no options currently retrieved for this person -
'       create object and get options
        Set m_Options = New mpDB.CPersonOptions
        With m_Options
            .Template = xTemplate
            .Parent = Me
            .Refresh , frmP
        End With
    ElseIf xTemplate <> m_Options.Template Then
'       request has been made to switch to options for
'       different template - create a new collection
'       and retrieve appropriate options
        Set m_Options = New mpDB.CPersonOptions
        With m_Options
            .Template = xTemplate
            .Parent = Me
            .Refresh , frmP
        End With
    End If
    
'   return current options collection
    Set Options = m_Options
End Function

Public Function GetFormattedName(Optional iFormat As mpNameFormats) As String
'returns the First Mid. LastName
'of person with ID lID

    Dim xFirst As String
    Dim xMid As String
    Dim xLast As String
    Dim xName As String
        
    If iFormat = mpNameFormat_Informal Then
'       use informal field name
        xName = Me.InformalName
    Else
'       get fields from record
        xFirst = Me.FirstName
        xLast = Me.LastName
        xMid = Me.MiddleName
        
'       build string based on format
        Select Case iFormat
            Case mpNameFormat_FirstLast
                If (xFirst <> Empty) And (xLast <> Empty) Then
                    xName = xFirst & " " & xLast
                ElseIf xFirst <> Empty Then
                    xName = xFirst
                Else
                    xName = xLast
                End If
                
            Case mpNameFormat_LastFirst
                If (xFirst <> Empty) And (xLast <> Empty) Then
                    xName = xLast & " " & xFirst
                ElseIf xFirst <> Empty Then
                    xName = xFirst
                Else
                    xName = xLast
                End If
                
            Case mpNameFormat_FirstMidLast
                xName = xFirst
                If xMid <> Empty Then
                    If xName <> Empty Then
                        xName = xName & " " & xMid
                    Else
                        xName = xMid
                    End If
                End If
                If xLast <> Empty Then
                    If xName <> Empty Then
                        xName = xName & " " & xLast
                    Else
                        xName = xLast
                    End If
                End If
                
            Case mpNameFormat_LastFirstMid
                xName = xLast
'               add comma if last name and either first or mid
                If (xLast <> Empty) And ((xMid & xFirst) <> Empty) Then
                    xName = xName & ","
                End If
                If xFirst <> Empty Then
                    If xName <> Empty Then
                        xName = xName & " " & xFirst
                    Else
                        xName = xFirst
                    End If
                End If
                If xMid <> Empty Then
                    If xName <> Empty Then
                        xName = xName & " " & xMid
                    Else
                        xName = xMid
                    End If
                End If
        End Select
    End If
    GetFormattedName = xName
End Function


'************************************************************************
'Internal Procedures
'************************************************************************

Private Function rGetDetail(lPersonID As Long, Optional Source As Integer = 1) As DAO.Recordset
'returns a recordset containing record of ID lPersonID

    Dim xSQL As String
    Dim rTemp As DAO.Recordset
    Dim xTable As String
    
    On Error GoTo rGetDetail_Error
    
    If Source = 3 Then
        xTable = "tblPeopleInput"
    Else
        xTable = "tblPeople"
    End If
    
    xSQL = "SELECT * FROM " & xTable
    xSQL = xSQL & " WHERE fldID = " & lPersonID
    
    Set rTemp = Application.PrivateDB.OpenRecordset(xSQL)
    If rTemp.RecordCount > 0 Then
        Set rGetDetail = rTemp
    Else
        Set rGetDetail = Nothing
    End If
    Exit Function

rGetDetail_Error:
    Select Case Err
        Case Else
            MsgBox Error(Err), vbExclamation, App.ProductName & "( rGetPersonDetail)"
            Exit Function
    End Select
End Function

'**********************************************************
'  Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Dim i As Integer
    
    Set m_Office = New mpDB.COffice
    Set m_oCustProps = New CCustomProperties
    
    With Application.CustomProperties
        For i = 1 To .Count
'           add custom properties to person
            m_oCustProps.Add .Item(i).Field, .Item(i).Name
        Next i
    End With
End Sub

Private Sub Class_Terminate()
    Set m_Office = Nothing
    Set m_Options = Nothing
    Set m_oCustProps = Nothing
End Sub
