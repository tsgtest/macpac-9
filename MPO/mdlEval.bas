Attribute VB_Name = "mdlEval"
Option Explicit

'******************************************************
'THIS FUNCTION IS RUN BY THE INSTALL EXTENSION
'******************************************************

Private Function EncryptDate(ByVal d As Date) As String
'returns date until which MacPac is valid
    Dim xDate As String
    Dim xScram As String
    Dim xDateMod As String
    Dim bRnd As Byte
    Dim bDigit As Byte
    
    On Error GoTo ProcError
    
    xDate = Format(d, "ddyyyyMM")
    Randomize
    While Len(xDate)
'       insert a random digit before each each digit in xDate
        bRnd = Int(10 * Rnd)
        xDateMod = xDateMod & bRnd & Left(xDate, 1)
        xDate = Mid(xDate, 2)
    Wend
    
'   get a random int between 2 and 4
    Randomize
    bRnd = Int(3 * Rnd + 2)
    
    While Len(xDateMod)
'       convert every char divisible by
'       the random number to a char
        bDigit = Left(xDateMod, 1)
        If (bDigit Mod bRnd = 0) Then
            xScram = xScram & Chr(100 + bDigit)
        Else
            xScram = xScram & bDigit
        End If
        
'       trim left char
        xDateMod = Mid(xDateMod, 2)
    Wend
    
'   1st digit is garbage, 2nd digit is random factor
'   used above, the rest is the encrypted date
    EncryptDate = Int(10 * Rnd) & bRnd & xScram
    Exit Function
    
ProcError:
    Dim xMsg As String
    xMsg = "Could not create installation signature. The following error occurred: " & _
        vbCr & "number: " & Err.Number & vbCr & "description: " & Err.Description
    MsgBox xMsg, vbCritical, "Legal MacPac Installation"
End Function

Public Function UnEncryptDate(ByVal xCode As String) As Date
'returns the date encrypted in xcode
    Dim bRnd As Byte
    Dim xCodeMod As String
    Dim i As Integer
    Dim xDate As String
    Dim x As String
    
    On Error GoTo ProcError
    
'   remove garbage 1st digit
    xCode = Mid(xCode, 2)

'   store and remove random factor
    bRnd = Left(xCode, 1)
    xCode = Mid(xCode, 2)
    
'   collect chars at even indexes
    For i = 2 To Len(xCode) Step 2
        xCodeMod = xCodeMod & Mid(xCode, i, 1)
    Next
    
    While Len(xCodeMod)
        x = Left(xCodeMod, 1)
        If Not IsNumeric(x) Then
            x = Asc(x) - 100
        End If
        
        xDate = xDate & x
        
'       trim left char
        xCodeMod = Mid(xCodeMod, 2)
    Wend
    
'   convert to date
    UnEncryptDate = DateSerial(Mid(xDate, 3, 4), Mid(xDate, 7, 2), Mid(xDate, 1, 2))
    Exit Function
    
ProcError:
    Exit Function
End Function
