VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCalendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Calendar
'   created 4/4/05 by Charlie Homo -
'   charliehomo@pacbell.net

'   Contains properties and methods that
'   define a MacPac Calendar

'   Member of App
'   Container for CDocument
'**********************************************************

Private Const mpSegment As String = "Calendar"

'module vars
Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oProps As MPO.CCustomProperties
Private m_bInit As Boolean
Private m_xTo28 As String

'xArrays
Private m_oMonthNames As XArray
Private m_oMonthNamesAbbv As XArray
Private m_oDayNames As XArray
Private m_oDayNamesAbbv As XArray
Private m_oDaysInMonth As XArray


Public Event BeforeBoilerplateInsert()
Public Event AfterBoilerplateInsert()

Implements MPO.IDocObj


'**********************************************************
'   Properties
'**********************************************************

Public Property Let Portrait(bNew As Boolean)
    If Me.Portrait <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "Portrait", mpSegment, , bNew
    End If
End Property

Public Property Get Portrait() As Boolean
    On Error Resume Next
    Portrait = m_oDoc.RetrieveItem("Portrait", mpSegment, , mpItemType_Boolean)
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Template.BoilerplateFile
End Property

Public Property Get DayNames() As XArray
    Set DayNames = m_oDayNames
End Property

Public Property Get DaysInMonth() As XArray
    Set DaysInMonth = m_oDaysInMonth
End Property

Public Property Get DayNamesAbbv() As XArray
    Set DayNamesAbbv = m_oDayNamesAbbv
End Property

Public Property Get MonthNames() As XArray
    Set MonthNames = m_oMonthNames
End Property

Public Property Get MonthNamesAbbv() As XArray
    Set MonthNamesAbbv = m_oMonthNamesAbbv
End Property

Public Property Let AbbreviatedDays(bNew As mpTriState)
    If Me.AbbreviatedDays <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "AbbreviatedDays", mpSegment, , bNew
    End If
End Property

Public Property Get AbbreviatedDays() As mpTriState
    On Error Resume Next
    AbbreviatedDays = m_oDoc.RetrieveItem("AbbreviatedDays", mpSegment)
End Property

Public Property Let AbbreviatedMonth(bNew As mpTriState)
    If Me.AbbreviatedMonth <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "AbbreviatedMonth", mpSegment, , bNew
    End If
End Property

Public Property Get AbbreviatedMonth() As mpTriState
    On Error Resume Next
    AbbreviatedMonth = m_oDoc.RetrieveItem("AbbreviatedMonth", mpSegment)
End Property

Public Property Let DateNumberAlignment(iNew As Integer)
    m_oDoc.SaveItem "DateNumberAlignment", mpSegment, , iNew
End Property

Public Property Get DateNumberAlignment() As Integer
    On Error Resume Next
    DateNumberAlignment = m_oDoc.RetrieveItem("DateNumberAlignment", mpSegment, , mpItemType_Integer)
End Property

Public Property Let TextFontName(xNew As String)
    If Me.TextFontName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "TextFontName", mpSegment, , xNew
    End If
End Property

Public Property Get TextFontName() As String
    On Error Resume Next
    TextFontName = m_oDoc.RetrieveItem("TextFontName", mpSegment)
End Property

Public Property Let TextFontSize(sNew As Single)
    On Error Resume Next
    If Me.TextFontSize <> sNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "TextFontSize", mpSegment, , sNew
    End If
End Property

Public Property Get TextFontSize() As Single
    On Error Resume Next
    TextFontSize = mpBase2.xLocalizeNumericString(m_oDoc _
        .RetrieveItem("TextFontSize", mpSegment))
End Property

Public Property Let MonthYearFontName(xNew As String)
    If Me.MonthYearFontName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "MonthYearFontName", mpSegment, , xNew
    End If
End Property

Public Property Get MonthYearFontName() As String
    On Error Resume Next
    MonthYearFontName = m_oDoc.RetrieveItem("MonthYearFontName", mpSegment)
End Property

Public Property Let MonthYearFontSize(sNew As Single)
    On Error Resume Next
    If Me.MonthYearFontSize <> sNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "MonthYearFontSize", mpSegment, , sNew
    End If
End Property

Public Property Get MonthYearFontSize() As Single
    On Error Resume Next
    MonthYearFontSize = mpBase2.xLocalizeNumericString(m_oDoc _
        .RetrieveItem("MonthYearFontSize", mpSegment))
End Property

Public Property Let DaysOfWeekFontName(xNew As String)
    If Me.DaysOfWeekFontName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "DaysOfWeekFontName", mpSegment, , xNew
    End If
End Property

Public Property Get DaysOfWeekFontName() As String
    On Error Resume Next
    DaysOfWeekFontName = m_oDoc.RetrieveItem("DaysOfWeekFontName", mpSegment)
End Property

Public Property Let DaysOfWeekFontSize(sNew As Single)
    On Error Resume Next
    If Me.DaysOfWeekFontSize <> sNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "DaysOfWeekFontSize", mpSegment, , sNew
    End If
End Property

Public Property Get DaysOfWeekFontSize() As Single
    On Error Resume Next
    DaysOfWeekFontSize = mpBase2.xLocalizeNumericString(m_oDoc _
        .RetrieveItem("DaysOfWeekFontSize", mpSegment))
End Property

Public Property Let DateFontName(xNew As String)
    If Me.DateFontName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "DateFontName", mpSegment, , xNew
    End If
End Property

Public Property Get DateFontName() As String
    On Error Resume Next
    DateFontName = m_oDoc.RetrieveItem("DateFontName", mpSegment)
End Property

Public Property Let DateFontSize(sNew As Single)
    On Error Resume Next
    If Me.DateFontSize <> sNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "DateFontSize", mpSegment, , sNew
    End If
End Property

Public Property Get DateFontSize() As Single
    On Error Resume Next
    DateFontSize = mpBase2.xLocalizeNumericString(m_oDoc _
        .RetrieveItem("DateFontSize", mpSegment))
End Property

Public Property Let StartDay(iNew As Integer)
    m_oDoc.SaveItem "StartDay", mpSegment, , iNew
End Property

Public Property Get StartDay() As Integer
    On Error Resume Next
    StartDay = m_oDoc.RetrieveItem("StartDay", mpSegment, , mpItemType_Integer)
End Property

Public Property Let MonthStart(iNew As Integer)
    m_oDoc.SaveItem "MonthStart", mpSegment, , iNew
End Property

Public Property Get MonthStart() As Integer
    On Error Resume Next
    MonthStart = m_oDoc.RetrieveItem("MonthStart", mpSegment, , mpItemType_Integer)
End Property

Public Property Let YearStart(iNew As Integer)
    m_oDoc.SaveItem "YearStart", mpSegment, , iNew
End Property

Public Property Get YearStart() As Integer
    On Error Resume Next
    YearStart = m_oDoc.RetrieveItem("YearStart", mpSegment, , mpItemType_Integer)
End Property

Public Property Let MonthEnd(iNew As Integer)
    m_oDoc.SaveItem "MonthEnd", mpSegment, , iNew
End Property

Public Property Get MonthEnd() As Integer
    On Error Resume Next
    MonthEnd = m_oDoc.RetrieveItem("MonthEnd", mpSegment, , mpItemType_Integer)
End Property

Public Property Let YearEnd(iNew As Integer)
    m_oDoc.SaveItem "YearEnd", mpSegment, , iNew
End Property

Public Property Get YearEnd() As Integer
    On Error Resume Next
    YearEnd = m_oDoc.RetrieveItem("YearEnd", mpSegment, , mpItemType_Integer)
End Property

Public Property Get Initialized() As Boolean
    Initialized = m_bInit
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub SelectItem(xBookmark As String)
    m_oDoc.SelectItem xBookmark
End Sub

Public Function InsertBoilerplate()
    RaiseEvent BeforeBoilerplateInsert
    Me.Template.InsertBoilerplate BoilerplateDirectory() & Me.BoilerplateFile
    RaiseEvent AfterBoilerplateInsert
End Function

Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

'Public Function Finish(Optional bForceRefresh As Boolean = True)
Public Function Finish()
'   cleans up doc after all user input is complete
    Dim rngP As Range
    Dim iMonthCurr As Integer
    Dim iYearCurr As Integer
    Dim lYearMonthCurr As Long
    Dim lYearMonthEnd As Long
    Dim icol As Integer
    Dim objActiveRange As Word.Range
    Dim strDays As String
    Dim iDaysInMonth As Integer
    Dim i As Integer
    Dim strDayNames As String
    Dim iCnt As Integer
    Dim xDayNames() As String
    Dim xDays() As String
    Dim oMessage As Object
    Dim xDaysCount As String
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    g_bForceItemUpdate = True
        
'   show all chars-  necessary to guarantee
'   that hidden text is deleted when appropriate
    Word.ActiveDocument.ActiveWindow.View.ShowHiddenText = True
    
    Set oMessage = CreateObject("mpAXE.CStatus")
    With oMessage
        .Title = "Creating Calendar"
        .Show , "Creating Calendar.  Please wait..."
    End With

    Application.ScreenUpdating = False

    With m_oDoc

'start of build
        
        'insert weekday names
        strDayNames = ""
        
        If Me.AbbreviatedDays Then
            For iCnt = Me.StartDay To 6
                strDayNames = strDayNames & Me.DayNamesAbbv(iCnt, 1) & vbTab
            Next
            For iCnt = 0 To (Me.StartDay - 1)
                strDayNames = strDayNames & Me.DayNamesAbbv(iCnt, 1) & vbTab
            Next
        Else
            For iCnt = Me.StartDay To 6
                strDayNames = strDayNames & Me.DayNames(iCnt, 1) & vbTab
            Next
            For iCnt = 0 To (Me.StartDay - 1)
                strDayNames = strDayNames & Me.DayNames(iCnt, 1) & vbTab
            Next
        End If
        
        'Strip trailing Tab
        strDayNames = Left$(strDayNames, Len(strDayNames) - 1)
    
        'insert weekday names in existing table
        mpBase2.xStringToArray strDayNames, xDayNames(), 1, vbTab
                
        'insert dates
        iMonthCurr = Me.MonthStart + 1
        iYearCurr = Me.YearStart
        lYearMonthCurr = (CLng(iYearCurr) * 100) + CLng(iMonthCurr)
        lYearMonthEnd = (CLng(Me.YearEnd) * 100) + (CLng(Me.MonthEnd) + 1)
      
        If Me.Portrait Then
            ActiveDocument.PageSetup.Orientation = wdOrientPortrait
        Else
            ActiveDocument.PageSetup.Orientation = wdOrientLandscape
        End If
      
        'Create each month of the calendar
        Do Until lYearMonthCurr > lYearMonthEnd
      
            Set objActiveRange = ActiveDocument.Content
            
            With objActiveRange
                .EndOf
                
                'insert boilerplate
                If Me.Portrait Then
                    .InsertFile Me.Template.BoilerplateDirectory & _
                                "CalendarPortrait.mbp", "zzmpCalendarTable"
                Else
                    .InsertFile Me.Template.BoilerplateDirectory & _
                                Me.Template.BoilerplateFile, "zzmpCalendarTable"
                End If
            End With
            
            'Set month & year
            If Me.AbbreviatedMonth Then
                Me.Document.EditItem "zzmpCalendarMonthYear", _
                                     Me.MonthNamesAbbv(iMonthCurr - 1, 1) & " " & CStr(iYearCurr)
            Else
                Me.Document.EditItem "zzmpCalendarMonthYear", _
                                     Me.MonthNames(iMonthCurr - 1, 1) & " " & CStr(iYearCurr)
            End If
            
            'set weekday names
            ActiveDocument.Bookmarks("zzmpCalendarWeekDays").Select
        
            With Selection
                For i = 0 To UBound(xDayNames)
                    .InsertAfter xDayNames(i, 0)
                    .Move wdCell
                Next i
            End With
    
            'Determine number of days in month
            If iMonthCurr = 2 Then
                iDaysInMonth = DateSerial(iYearCurr, 3, 1) - DateSerial(iYearCurr, 2, 1)
            Else
                iDaysInMonth = Me.DaysInMonth(iMonthCurr - 1, 1)
            End If
    
            'Set Column of First Day Of Month by using Weekday function.  The 8th
            'is used (to start with) instead of the 1st to allow for offsetting (via
            'iWeekStartDay) for localization since the 8th will always fall on the
            'same day (of the week) as the 1st.
            icol = Weekday(DateSerial(iYearCurr, iMonthCurr, 8 - Me.StartDay))
    
            ActiveDocument.Bookmarks("zzmpCalendarDates").Select
    
            strDays = ""
            For i = 1 To icol
                strDays = strDays & vbTab
            Next i
            strDays = strDays & m_xTo28
            For i = 29 To iDaysInMonth
                strDays = strDays & vbTab & CStr(i)
            Next i
            'to get a 6 * 7 table always
            For i = iDaysInMonth + icol - 1 To 41
                strDays = strDays & vbTab
            Next i
            
            'insert in existing table
            mpBase2.xStringToArray strDays, xDays(), 1, vbTab
            
            With Selection
                For i = 1 To UBound(xDays)
                    .InsertAfter xDays(i, 0)
                    .Move wdCell
                Next i
                
                'delete last row if empty
                For i = (UBound(xDays) - 6) To UBound(xDays)
                    xDaysCount = xDaysCount & xDays(i, 0)
                Next i
                If xDaysCount = Empty Then
                    Selection.Rows(1).Delete
                End If
                xDaysCount = ""
            End With
            
            'Advance the current month
            If iMonthCurr = 12 Then
                iMonthCurr = 1
                iYearCurr = iYearCurr + 1
            Else
                iMonthCurr = iMonthCurr + 1
            End If
        
            lYearMonthCurr = (CLng(iYearCurr) * 100) + CLng(iMonthCurr)
    
    
            .ClearBookmarks
            
            If lYearMonthCurr <= lYearMonthEnd Then
                Set objActiveRange = ActiveDocument.Content
                objActiveRange.EndOf
                
                'Insert Page Break
                objActiveRange.InsertBreak wdPageBreak
                
                              
            End If
        Loop
    
        ActiveDocument.UndoClear
    
        'Format table
        With ActiveDocument.Styles
            'Calendar Text
            .Item("Cal Text").Font.Name = Me.TextFontName
            .Item("Cal Text").Font.Size = Me.TextFontSize
            'Calendar Month/Year
            .Item("Cal Month").Font.Name = Me.MonthYearFontName
            .Item("Cal Month").Font.Size = Me.MonthYearFontSize
            'Calendar Days of the Week
            .Item("Cal Days").Font.Name = Me.DaysOfWeekFontName
            .Item("Cal Days").Font.Size = Me.DaysOfWeekFontSize
            'Calendar Date
            .Item("Cal Date").Font.Name = Me.DateFontName
            .Item("Cal Date").Font.Size = Me.DateFontSize
            .Item("Cal Date").ParagraphFormat.Alignment = Me.DateNumberAlignment
        End With
                  
        'save main bp file for ResetStyles function
        Me.Document.SetVar "bpfile", Me.Template.BoilerplateFile  '9.7.1 - #4223

'       close status
        oMessage.Hide

    'end of build

'       delete any hidden text - ie text marked for deletion
        .DeleteHiddenText True, True, , True
        .RemoveFormFields
        
'       run editing macro
        RunEditMacro Me.Template
        
'        .SelectStartPosition
'       Doc.IsCreated will be TRUE only if Reuse Author is not undefined -
'       since there is no author in Calendar, dummy reuse author.
        Me.Document.SetVar "ReuseAuthor", "NoAuthor"        '9.7.1 - #4223
        DoEvents
        .ClearBookmarks
        .rngTrimEmptyParagraphs
    End With
    
'   Position cursor at the top
    Selection.HomeKey wdStory
    
    Application.ScreenUpdating = True
    Application.ScreenRefresh
                      
    g_bForceItemUpdate = False
    Exit Function
ProcError:
    Err.Raise Err.Number
    g_bForceItemUpdate = False
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************


'Public Sub Refresh()
''--refresh all properties by setting them to themselves
''--this ensures creation of doc vars
'    Dim bForceUpdate As Boolean
'    bForceUpdate = g_bForceItemUpdate
'    g_bForceItemUpdate = True
'    With Me
'        .Portrait = .Portrait
'        .AbbreviatedDays = .AbbreviatedDays
'        .AbbreviatedMonth = .AbbreviatedMonth
'        .TextFontName = .TextFontName
'        .TextFontSize = .TextFontSize
'        .DateNumberAlignment = .DateNumberAlignment
'        .StartDay = .StartDay
'        .MonthStart = .MonthStart
'        .YearStart = .YearStart
'        .MonthEnd = .MonthEnd
'        .YearEnd = .YearEnd
'
''       refresh all custom properties
'        .CustomProperties.RefreshValues
'
'    End With
'    g_bForceItemUpdate = bForceUpdate
'End Sub
'
'Public Sub RefreshEmpty()
''   force properties that may not have been
''   set by user to get updated in document
'    Dim bForceUpdate As Boolean
'
'    bForceUpdate = g_bForceItemUpdate
'    g_bForceItemUpdate = True
'
'    With Me
''       refresh empty properties of calendar
'        If .Portrait = mpUndefined Or _
'        .Portrait = Empty Then _
'            .Portrait = Empty
'        If .AbbreviatedDays = mpUndefined Or _
'        .AbbreviatedDays = Empty Then _
'            .AbbreviatedDays = Empty
'        If .AbbreviatedMonth = mpUndefined Or _
'        .AbbreviatedMonth = Empty Then _
'            .AbbreviatedMonth = Empty
'        If .TextFontName = Empty Then _
'            .TextFontName = Empty
'        If .TextFontSize = Empty Then _
'            .TextFontSize = Empty
'        If .DateNumberAlignment = Empty Then _
'            .DateNumberAlignment = Empty
'        If .StartDay = Empty Then _
'            .StartDay = Empty
'        If .MonthStart = Empty Then _
'            .MonthStart = Empty
'        If .YearStart = Empty Then _
'            .YearStart = Empty
'        If .MonthEnd = Empty Then _
'            .MonthEnd = Empty
'        If .YearEnd = Empty Then _
'            .YearEnd = Empty
'
''       refresh custom properties
'        .CustomProperties.RefreshEmptyValues
'    End With
'    g_bForceItemUpdate = bForceUpdate
'End Sub

Friend Sub Initialize()
    Dim i As Integer
    
    On Error GoTo ProcError
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    
    On Error Resume Next
    Set m_oTemplate = g_oTemplates.ItemFromClass("CCalendar")
    
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidTemplateName
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'   load arrays

    'Load Month Names arrays
    Set m_oMonthNames = New XArray
    
    With m_oMonthNames
        .ReDim 0, 11, 0, 1
        .Value(0, 1) = Format(#1/1/2003#, "mmmm")
        .Value(0, 0) = 0
        .Value(1, 1) = Format(#2/1/2003#, "mmmm")
        .Value(1, 0) = 1
        .Value(2, 1) = Format(#3/1/2003#, "mmmm")
        .Value(2, 0) = 2
        .Value(3, 1) = Format(#4/1/2003#, "mmmm")
        .Value(3, 0) = 3
        .Value(4, 1) = Format(#5/1/2003#, "mmmm")
        .Value(4, 0) = 4
        .Value(5, 1) = Format(#6/1/2003#, "mmmm")
        .Value(5, 0) = 5
        .Value(6, 1) = Format(#7/1/2003#, "mmmm")
        .Value(6, 0) = 6
        .Value(7, 1) = Format(#8/1/2003#, "mmmm")
        .Value(7, 0) = 7
        .Value(8, 1) = Format(#9/1/2003#, "mmmm")
        .Value(8, 0) = 8
        .Value(9, 1) = Format(#10/1/2003#, "mmmm")
        .Value(9, 0) = 9
        .Value(10, 1) = Format(#11/1/2003#, "mmmm")
        .Value(10, 0) = 10
        .Value(11, 1) = Format(#12/1/2003#, "mmmm")
        .Value(11, 0) = 11
    End With

    Set m_oMonthNamesAbbv = New XArray
    
    With m_oMonthNamesAbbv
        .ReDim 0, 11, 0, 1
        .Value(0, 1) = Format(#1/1/2003#, "mmm")
        .Value(0, 0) = 0
        .Value(1, 1) = Format(#2/1/2003#, "mmm")
        .Value(1, 0) = 1
        .Value(2, 1) = Format(#3/1/2003#, "mmm")
        .Value(2, 0) = 2
        .Value(3, 1) = Format(#4/1/2003#, "mmm")
        .Value(3, 0) = 3
        .Value(4, 1) = Format(#5/1/2003#, "mmm")
        .Value(4, 0) = 4
        .Value(5, 1) = Format(#6/1/2003#, "mmm")
        .Value(5, 0) = 5
        .Value(6, 1) = Format(#7/1/2003#, "mmm")
        .Value(6, 0) = 6
        .Value(7, 1) = Format(#8/1/2003#, "mmm")
        .Value(7, 0) = 7
        .Value(8, 1) = Format(#9/1/2003#, "mmm")
        .Value(8, 0) = 8
        .Value(9, 1) = Format(#10/1/2003#, "mmm")
        .Value(9, 0) = 9
        .Value(10, 1) = Format(#11/1/2003#, "mmm")
        .Value(10, 0) = 10
        .Value(11, 1) = Format(#12/1/2003#, "mmm")
        .Value(11, 0) = 11
    End With

    'Load Day Names arrays
    Set m_oDayNames = New XArray
    
    With m_oDayNames
        .ReDim 0, 6, 0, 1
        .Value(0, 1) = Format(DateTime.Weekday(vbSunday), "dddd")
        .Value(0, 0) = 0
        .Value(1, 1) = Format(DateTime.Weekday(vbMonday), "dddd")
        .Value(1, 0) = 1
        .Value(2, 1) = Format(DateTime.Weekday(vbTuesday), "dddd")
        .Value(2, 0) = 2
        .Value(3, 1) = Format(DateTime.Weekday(vbWednesday), "dddd")
        .Value(3, 0) = 3
        .Value(4, 1) = Format(DateTime.Weekday(vbThursday), "dddd")
        .Value(4, 0) = 4
        .Value(5, 1) = Format(DateTime.Weekday(vbFriday), "dddd")
        .Value(5, 0) = 5
        .Value(6, 1) = Format(DateTime.Weekday(vbSaturday), "dddd")
        .Value(6, 0) = 6
    End With

    Set m_oDayNamesAbbv = New XArray
    
    With m_oDayNamesAbbv
        .ReDim 0, 6, 0, 1
        .Value(0, 1) = Format(DateTime.Weekday(vbSunday), "Ddd")
        .Value(0, 0) = 0
        .Value(1, 1) = Format(DateTime.Weekday(vbMonday), "Ddd")
        .Value(1, 0) = 1
        .Value(2, 1) = Format(DateTime.Weekday(vbTuesday), "Ddd")
        .Value(2, 0) = 2
        .Value(3, 1) = Format(DateTime.Weekday(vbWednesday), "Ddd")
        .Value(3, 0) = 3
        .Value(4, 1) = Format(DateTime.Weekday(vbThursday), "Ddd")
        .Value(4, 0) = 4
        .Value(5, 1) = Format(DateTime.Weekday(vbFriday), "Ddd")
        .Value(5, 0) = 5
        .Value(6, 1) = Format(DateTime.Weekday(vbSaturday), "Ddd")
        .Value(6, 0) = 6
    End With
    
    'Load Days In Month array
    Set m_oDaysInMonth = New XArray
    
    With m_oDaysInMonth
        .ReDim 0, 11, 0, 1
        .Value(0, 1) = 31
        .Value(0, 0) = 0
        .Value(1, 1) = 28   'Will not be used - must be calculated for leap year accuracy
        .Value(1, 0) = 1
        .Value(2, 1) = 31
        .Value(2, 0) = 2
        .Value(3, 1) = 30
        .Value(3, 0) = 3
        .Value(4, 1) = 31
        .Value(4, 0) = 4
        .Value(5, 1) = 30
        .Value(5, 0) = 5
        .Value(6, 1) = 31
        .Value(6, 0) = 6
        .Value(7, 1) = 31
        .Value(7, 0) = 7
        .Value(8, 1) = 30
        .Value(8, 0) = 8
        .Value(9, 1) = 31
        .Value(9, 0) = 9
        .Value(10, 1) = 30
        .Value(10, 0) = 10
        .Value(11, 1) = 31
        .Value(11, 0) = 11
    End With
        
    m_xTo28 = ""
    For i = 1 To 28
        m_xTo28 = m_xTo28 & CStr(i) & vbTab
    Next i
    
    'strip off last tab
    m_xTo28 = Left$(m_xTo28, Len(m_xTo28) - 1)

        
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    With m_oDoc
        .Selection.StartOf
    End With
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CCalendar.Initialize"
    Exit Sub
End Sub


'**********************************************************
'   Class Event Procedures
'**********************************************************

Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oTemplate = g_oTemplates.ItemFromClass("CCalendar")
End Sub

Private Sub Class_Terminate()
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
    Set m_oProps = Nothing
End Sub

'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
End Property

Private Property Get IDocObj_Author() As mpDB.CPerson
End Property

Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property

Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function

Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
End Property

Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
End Property

Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function

Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish
End Sub

Private Sub IDocObj_Initialize()
    Me.Initialize
End Sub

Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property

Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub

Private Sub IDocObj_RefreshEmpty()
'    RefreshEmpty
End Sub

Private Sub IDocObj_Refresh()
'    Refresh
End Sub

Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function

Private Sub IDocObj_UpdateForAuthor()
End Sub


