VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmPersonDetail 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add Private Author"
   ClientHeight    =   6210
   ClientLeft      =   3945
   ClientTop       =   1740
   ClientWidth     =   5475
   Icon            =   "frmPeopleDetail.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6210
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox mltCustom 
      Appearance      =   0  'Flat
      Height          =   300
      Index           =   0
      Left            =   1770
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   20
      Tag             =   "Fax"
      Top             =   4230
      Visible         =   0   'False
      Width           =   3555
   End
   Begin VB.TextBox mltCustom 
      Appearance      =   0  'Flat
      Height          =   300
      Index           =   1
      Left            =   1755
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   19
      Tag             =   "Fax"
      Top             =   3795
      Visible         =   0   'False
      Width           =   3555
   End
   Begin VB.TextBox mltCustom 
      Appearance      =   0  'Flat
      Height          =   300
      Index           =   2
      Left            =   1725
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   18
      Tag             =   "Fax"
      Top             =   4020
      Visible         =   0   'False
      Width           =   3555
   End
   Begin VB.TextBox txtDisplayName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   1815
      MaxLength       =   255
      TabIndex        =   9
      Tag             =   "m"
      Top             =   1770
      Width           =   3500
   End
   Begin VB.CommandButton btnLicenses 
      Appearance      =   0  'Flat
      Caption         =   "&Bar IDs..."
      Default         =   -1  'True
      Height          =   400
      Left            =   1770
      TabIndex        =   15
      Top             =   5700
      Width           =   1100
   End
   Begin VB.TextBox txtFullName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   1815
      MaxLength       =   120
      TabIndex        =   7
      Tag             =   "m"
      Top             =   1365
      Width           =   3500
   End
   Begin VB.TextBox txtInitials 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   1815
      MaxLength       =   10
      TabIndex        =   11
      Tag             =   "i"
      Top             =   2205
      Width           =   3500
   End
   Begin VB.TextBox txtMI 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   1815
      MaxLength       =   50
      TabIndex        =   3
      Tag             =   "i"
      Top             =   555
      Width           =   3500
   End
   Begin VB.TextBox txtLastName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   1815
      MaxLength       =   100
      TabIndex        =   5
      Tag             =   "l"
      Top             =   960
      Width           =   3500
   End
   Begin VB.TextBox txtFirstName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   1815
      MaxLength       =   100
      TabIndex        =   1
      Tag             =   "n"
      Top             =   150
      Width           =   3500
   End
   Begin VB.CheckBox chkIsAtty 
      Appearance      =   0  'Flat
      Caption         =   "&Attorney"
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   1845
      TabIndex        =   14
      Top             =   3045
      Width           =   3375
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4215
      TabIndex        =   17
      Top             =   5700
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3015
      TabIndex        =   16
      Top             =   5700
      Width           =   1100
   End
   Begin TrueDBList60.TDBCombo cmbOffice 
      Height          =   315
      Left            =   1815
      OleObjectBlob   =   "frmPeopleDetail.frx":058A
      TabIndex        =   13
      Top             =   2595
      Width           =   3495
   End
   Begin VB.Label lblDisplayName 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BackStyle       =   0  'Transparent
      Caption         =   "&Display Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   8
      Top             =   1800
      Width           =   1455
   End
   Begin VB.Label lblOffice 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BackStyle       =   0  'Transparent
      Caption         =   "&Office:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   12
      Top             =   2640
      Width           =   1455
   End
   Begin VB.Label lblFullName 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BackStyle       =   0  'Transparent
      Caption         =   "Full &Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   6
      Top             =   1395
      Width           =   1425
   End
   Begin VB.Label lblInitials 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BackStyle       =   0  'Transparent
      Caption         =   "&Initials:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   10
      Top             =   2220
      Width           =   1455
   End
   Begin VB.Label lblMI 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BackStyle       =   0  'Transparent
      Caption         =   "&Middle Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   2
      Top             =   600
      Width           =   1425
   End
   Begin VB.Label lblLastName 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BackStyle       =   0  'Transparent
      Caption         =   "&Last Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   4
      Top             =   1005
      Width           =   1485
   End
   Begin VB.Label lblFirst 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BackStyle       =   0  'Transparent
      Caption         =   "&First Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   0
      Top             =   210
      Width           =   1485
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00808080&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00808080&
      Height          =   6300
      Left            =   -120
      Top             =   -60
      Width           =   1800
   End
End
Attribute VB_Name = "frmPersonDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum mpDetailControls
    mpDetailControl_TextBox = 1
    mpDetailControl_ListBox = 2
    mpDetailControl_ComboBox = 3
    mpDetailControl_CheckBox = 4
    mpDetailControl_Label = 5
    mpDetailControl_SpinText = 6
End Enum

Private m_oCtls As mpDB.CPersonDetailControls
Private m_Person As mpDB.CPerson
Private m_bCancelled As Boolean
Private m_oPrevCtl As Control
Private m_App As MPO.CApplication
Private m_xAR As XArray
Private m_bChanged As Boolean
Private m_bInitChanged As Boolean
Private m_iPrevTabIndex As Integer

Private m_imltTextBoxes As Integer

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Person(psnNew As mpDB.CPerson)
    Set m_Person = psnNew
    UpdateForm
End Property

Public Property Get Person() As mpDB.CPerson
    Set Person = m_Person
End Property

Public Property Get LicenseArray() As XArray
    Set LicenseArray = m_xAR
End Property

Public Property Let LicenseArray(xarNew As XArray)
    Set m_xAR = xarNew
End Property

Private Sub btnCancel_Click()
    On Error Resume Next
    Me.Cancelled = True
    Me.Hide
    Unload frmAttorneyLicenses
    Set frmAttorneyLicenses = Nothing
    DoEvents
End Sub

Private Sub btnLicenses_Click()
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    On Error GoTo ProcError
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If
    
    With frmAttorneyLicenses
        .Parent = Me.Name
        .Attorney = Me.Person
        .Show vbModal
        
'---grab any newly input license ids
        Me.LicenseArray = .LicenseArray
    End With
    
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2


    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    EchoOn
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnOK_Click()
    On Error Resume Next
    If InputIsValid() Then
        Me.Cancelled = False
        Me.Hide
        DoEvents
        UpdateObject
        Unload frmAttorneyLicenses
        Set frmAttorneyLicenses = Nothing
    End If
End Sub

Private Sub chkIsAtty_Click()
    Me.btnLicenses.Enabled = Me.chkIsAtty
End Sub

Private Sub cmbOffice_GotFocus()
    bEnsureSelectedContent Me.cmbOffice
End Sub

Private Sub Form_Load()
    Set m_Person = New mpDB.CPerson
    If m_Person.Source = mpPeopleSourceList_None Then
        m_Person.Source = mpPeopleSourceList_PeopleInput
    End If
    Me.cmbOffice.Array = g_oDBs.Offices.ListSource
    ResizeTDBCombo Me.cmbOffice, 6
    SetUpControls
    SetupForm
    Set m_oPrevCtl = Nothing
    Me.btnLicenses.Enabled = Me.chkIsAtty
    Me.Cancelled = True
End Sub

Private Function UpdateForm()
    Dim oCtl As Control
    Dim vValue As Variant
    
    On Error GoTo ProcError
    
    With Me.Person
        Me.txtFirstName = .FirstName
        Me.txtInitials = .Initials
        Me.txtLastName = .LastName
        Me.txtMI = .MiddleName
        Me.txtFullName = .FullName
        Me.txtDisplayName = .DisplayName
        Me.cmbOffice.BoundText = .Office.ID
        Me.chkIsAtty = Abs(.IsAttorney)
        
        For Each oCtl In Me.Controls
'           get appropriate value for custom controls
            If Mid(oCtl.Name, 4, 4) = "Cust" Then
                Err.Clear
                On Error Resume Next
'               get value - an error will be generated if the linked
'               property is a custom author property
                vValue = Empty
                vValue = CallByName(Me.Person, oCtl.Tag, VbGet)
                If Err.Number Then
                    On Error GoTo ProcError
'                   try custom people properties
                    vValue = Me.Person.CustomProperties.Item(oCtl.Tag)
                End If
                On Error GoTo ProcError
                
'               assign value to control
                If (TypeOf oCtl Is TDBCombo) Then
                    If Len(vValue) Then
                        oCtl.BoundText = vValue
                    Else
                        oCtl.Text = vValue
                    End If
                ElseIf (TypeOf oCtl Is TDBList) Then
                    If Len(vValue) Then
                        oCtl.BoundText = vValue
                    Else
                        oCtl.Text = vValue
                    End If
                ElseIf (TypeOf oCtl Is VB.TextBox) Then
                    oCtl.Text = vValue
                ElseIf (TypeOf oCtl Is mpControls.SpinnerTextBox) Or _
                        (TypeOf oCtl Is mpControls3.SpinTextInternational) Then '*c
                    oCtl.Value = vValue
                ElseIf (TypeOf oCtl Is VB.CheckBox) Then
                    If vValue <> Empty Then
                        oCtl.Value = Abs(CBool(vValue)) '#3740
                    End If
                Else
                    If Not IsNull(vValue) Then
                        oCtl.Value = vValue
                    End If
                End If
            End If
        Next oCtl
    End With
    Exit Function
ProcError:
    RaiseError "MPO.frmPersonDetail.UpdateForm"
    Exit Function
End Function

Private Function UpdateObject()
    Dim oCtl As VB.Control
    Dim vValue As Variant
    Dim i As Integer
    Dim lUB As Long
    
    With Me.Person
        .FirstName = Me.txtFirstName
        .Initials = Me.txtInitials
        .LastName = Me.txtLastName
        .MiddleName = Me.txtMI
        .FullName = Me.txtFullName
        .DisplayName = Me.txtDisplayName
        .Office = g_oDBs.Offices.Item(Me.cmbOffice.BoundText)
        .IsAttorney = Me.chkIsAtty
        
        For Each oCtl In Me.Controls
'           get appropriate value for custom controls
            If Mid(oCtl.Name, 4, 4) = "Cust" Then
'               assign value to control
                If (TypeOf oCtl Is TDBCombo) Then
                    If Len(oCtl.BoundText) Then
                        vValue = oCtl.BoundText
                    Else
                        vValue = oCtl.Text
                    End If
                ElseIf (TypeOf oCtl Is TDBList) Then
                    If Len(oCtl.BoundText) Then
                        vValue = oCtl.BoundText
                    Else
                        vValue = oCtl.Text
                    End If
                ElseIf (TypeOf oCtl Is VB.TextBox) Then
                    vValue = oCtl.Text
                ElseIf (TypeOf oCtl Is VB.CheckBox) Then
                    vValue = 0 - oCtl.Value '#3740
                Else
                    vValue = oCtl.Value
                End If
                
                Err.Clear
                On Error Resume Next
'               get value - an error will be generated if the linked
'               property is a custom author property
                CallByName Me.Person, oCtl.Tag, VbLet, vValue
                If Err.Number = 438 Then
                    On Error GoTo ProcError
'                   try custom people properties
                    Me.Person.CustomProperties.Item(oCtl.Tag) = vValue
                End If
                On Error GoTo ProcError
            End If
        Next oCtl
    
'---process any existing License IDs passed from input form
'        If (Not (Me.LicenseArray Is Nothing)) And frmPersonDetail.Caption Like "*Add*" Then
        If (Not (Me.LicenseArray Is Nothing)) And (Not frmAttorneyLicenses.Cancelled) Then
            lUB = LicenseArray.UpperBound(1)
            '---reset the collection
            Me.Person.Licenses.DeleteAll
            With .Licenses
                For i = 0 To lUB
                '---add elements from passed Array
                   With LicenseArray
                        Me.Person.Licenses.Add .Value(i, 1), _
                        .Value(i, 2), CStr(CBool(.Value(i, 3)))
                    End With
                Next i
            End With
        End If
        
    End With
    Exit Function
ProcError:
    RaiseError "MPO.frmPersonDetail.UpdateObject"
    Exit Function
End Function

Private Sub Form_Unload(Cancel As Integer)
    Set m_Person = Nothing
    Set g_oPrevControl = Nothing
'   reset licenses array
    Set m_xAR = Nothing     '#3932
'   reset previous tab
    m_iPrevTabIndex = 0
    m_imltTextBoxes = 0
End Sub

Private Function InputIsValid() As Boolean
    If Me.txtFirstName = Empty Then
        xMsg = "First Name is required information."
        MsgBox xMsg, vbExclamation, App.Title
        Me.txtFirstName.SetFocus
        Exit Function
    ElseIf Me.txtLastName = Empty Then
        xMsg = "Last Name is required information."
        MsgBox xMsg, vbExclamation, App.Title
        Me.txtLastName.SetFocus
        Exit Function
    ElseIf Me.txtFullName = Empty Then
        xMsg = "Full Name is required information."
        MsgBox xMsg, vbExclamation, App.Title
        Me.txtFullName.SetFocus
        Exit Function
    ElseIf Me.txtInitials = Empty Then
        xMsg = "Initials are required information."
        MsgBox xMsg, vbExclamation, App.Title
        Me.txtInitials.SetFocus
        Exit Function
    End If
    InputIsValid = True
End Function

Private Sub txtDisplayName_GotFocus()
    bEnsureSelectedContent Me.txtDisplayName, , True
End Sub

Private Sub txtFirstName_Change()
    If Not m_bChanged Then
        SetFullName
        SetDisplayName
    End If
    If Not m_bInitChanged Then
        SetInitials
    End If
End Sub

Private Sub txtFirstName_GotFocus()
    bEnsureSelectedContent Me.txtFirstName, , True
End Sub

Private Sub txtFullName_Change()
    m_bChanged = True
End Sub

Private Sub SetFullName()
'set full name based on existing name input
    Dim xTemp As String
    
'   build string
    xTemp = Me.txtFirstName & " "
    
    If Me.txtMI <> Empty Then
        xTemp = xTemp & Me.txtMI & " "
    End If
    If Me.txtLastName <> Empty Then
        xTemp = xTemp & Me.txtLastName & " "
    End If
    
'   trim initial and trailing spaces
    xTemp = LTrim(RTrim(xTemp))
    
'   assign
    Me.txtFullName = xTemp
    m_bChanged = False
End Sub

Private Sub SetDisplayName()
'set full name based on existing name input
    Dim xTemp As String
    
'   build string
    xTemp = xTemp & Me.txtLastName
    
    If Me.txtFirstName <> Empty Then
        If xTemp <> "" Then
            xTemp = xTemp & ", "
        End If
        xTemp = xTemp & Me.txtFirstName & " "
    End If
    
    If Me.txtMI <> Empty Then
        If xTemp <> "" And InStr(xTemp, ",") = 0 Then
            xTemp = xTemp & ", "
        End If
        xTemp = xTemp & Me.txtMI
    End If
    
'   trim initial and trailing spaces
    xTemp = LTrim(RTrim(xTemp))
    
'   assign
    Me.txtDisplayName = xTemp
    m_bChanged = False
End Sub

Private Sub SetInitials()
'enter default full name if no name currently exists
    Dim xTemp As String
    
'   build string
    xTemp = Left(Me.txtFirstName, 1)
    
    If Me.txtMI <> Empty Then
        xTemp = xTemp & Left(Me.txtMI, 1)
    End If
    
    If Me.txtLastName <> Empty Then
        xTemp = xTemp & Left(Me.txtLastName, 1)
    End If
    
'   trim initial and trailing spaces
    xTemp = LTrim(RTrim(xTemp))
    
'   assign
    Me.txtInitials = xTemp
    m_bInitChanged = False
End Sub

Private Sub SetUpControls()
'set up controls on dialog based
'on definitions in db
    Dim i As Long
    Dim oCtl As Control

'   get controls for form
    Set m_oCtls = g_oDBs.PersonDetailControls

    For i = 1 To m_oCtls.Count
        With m_oCtls.Item(i)
'           get control to which this custom
'           control will be assigned
            Select Case .ControlType
                Case mpDetailControl_TextBox
                    Set oCtl = SetUpTextBox(m_oCtls.Item(i))
                Case mpDetailControl_ComboBox, _
                     mpDetailControl_ListBox
                    Set oCtl = SetUpComboList(m_oCtls.Item(i))
                Case mpDetailControl_CheckBox
                    Set oCtl = SetUpCheckBox(m_oCtls.Item(i))
            End Select
            
'           set this control as the previous control
            Set m_oPrevCtl = oCtl
        End With
    Next i
    
End Sub

Private Function SetUpTextBox(oCustCtl As mpDB.CPersonDetailControl) As VB.TextBox
'sets up the next available textbox
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.TextBox
    Dim oLblCtl As VB.Label
    
    With oCustCtl
        If .Lines > 1 And m_imltTextBoxes < 3 Then
'           set up the multiline box in the array with specified index
            Set oCtl = Me.mltCustom(m_imltTextBoxes)
            With oCtl
'                .Name = "txtCust" & oCustCtl.LinkedProperty
                .Tag = oCustCtl.LinkedProperty
                .Visible = True
                .Height = oCustCtl.Lines * 250
                .Appearance = 0
            End With
            m_imltTextBoxes = m_imltTextBoxes + 1
        Else
'           set up the text box in the array with specified index
            Set oCtl = Me.Controls.Add("VB.TextBox", _
                        "txtCust" & .LinkedProperty, Me)
            With oCtl
                .Tag = oCustCtl.LinkedProperty
                .Visible = True
                .Height = Me.txtFirstName.Height
                .Appearance = 0
            End With
        End If
        
        '#2467
        oCtl.MaxLength = .MaxValue
        
'       set up corresponding label
        Set oLblCtl = Me.Controls.Add("VB.Label", _
                    "lbl" & .LinkedProperty, Me)
        With oLblCtl
            .Caption = oCustCtl.Caption & ":"
            .Visible = True
            .BackStyle = 0
            .ForeColor = &HFFFFFF
            .ZOrder 0
            .Width = Me.lblFirst.Width      'GLOG : 5701 : ceh
        End With
                
        
'       position controls
        PositionControl oCustCtl, oCtl, oLblCtl
    End With

'   return control
    Set SetUpTextBox = oCtl
End Function

Private Function SetUpCheckBox(oCustCtl As mpDB.CPersonDetailControl) As VB.CheckBox
'sets up the next available checkbox
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.CheckBox
    
    With oCustCtl
        Set oCtl = Me.Controls.Add("VB.CheckBox", _
                    "chkCust" & .LinkedProperty, Me)
        With oCtl
'           set up the text box in the
'           array with specified index
            .Tag = oCustCtl.LinkedProperty
            .Visible = True
            .Caption = oCustCtl.Caption
            .Appearance = 0
            .BackColor = &H8000000F
        End With
        
'       position controls
        PositionControl oCustCtl, oCtl
    End With

'   return control
    Set SetUpCheckBox = oCtl
End Function

Private Function SetUpComboList(oCustCtl As mpDB.CPersonDetailControl) As TDBCombo
'sets up the next available textbox
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    Dim oCombo As TDBCombo
    Dim oLabel As VB.Label

    With oCustCtl
'       set up the combo box with specified index in the array
        Set oCombo = Me.Controls.Add("TrueDBList60.TDBCombo", _
                    "cmbCust" & .LinkedProperty, Me)
                    
'       set up corresponding label
        Set oLabel = Me.Controls.Add("VB.Label", _
                    "lbl" & .LinkedProperty, Me)
        With oLabel
            .Caption = oCustCtl.Caption & ":"
            .Visible = True
            .BackStyle = 0
            .ForeColor = &HFFFFFF
            .ZOrder 0
            .Width = Me.lblFirst.Width  'GLOG : 5701 : ceh
        End With

'       set to list box if limited to list entries
        If .ControlType = mpDetailControl_ListBox Then
            oCombo.ComboStyle = 2
        End If

        oCombo.Visible = True

        If Not (.List Is Nothing) Then
            With oCombo
                .LayoutFileName = App.Path & "\tdblLayouts.mlf"
                .LayoutName = "CustomCombo"
                .LoadLayout
                .Tag = oCustCtl.LinkedProperty
                .Visible = True
                .Height = Me.txtFirstName.Height
                .Appearance = 0
                .Tag = oCustCtl.LinkedProperty
                .Array = oCustCtl.List.ListItems.Source
                .Rebind
            End With
        
            ResizeTDBCombo oCombo, 5
        End If

'       position controls
        PositionControl oCustCtl, _
                        oCombo, _
                        oLabel
    End With

'   return control
    Set SetUpComboList = oCombo
End Function

Private Sub PositionControl(oDef As mpDB.CPersonDetailControl, _
                            oCtl As Control, _
                            Optional oLabel As VB.Label)
'   position control and corresponding
'   label based on position of previous
'   control and space before for current control
    Dim sPrevTop As Single
    Dim sPrevHeight As Single
    Dim sPrevLeft As Single
    Dim oPrevCtl As Control
    
    Set oPrevCtl = m_oPrevCtl
    
'   test for previous control
    If (oPrevCtl Is Nothing) Then
'       no previous control - base position on first text box
        With Me.chkIsAtty
            sPrevTop = .Top
            sPrevHeight = .Height
            sPrevLeft = .Left
        End With
    Else
        With oPrevCtl
'           position control based on previous control
            sPrevTop = .Top
            sPrevHeight = .Height
            sPrevLeft = .Left
        End With
    End If

'   execute
    With oCtl
        .Left = Me.txtFirstName.Left + oDef.HorizontalOffset
        .Top = sPrevTop + oDef.VerticalOffset
        If oDef.Width > 0 Then
            .Width = oDef.Width
        Else
            .Width = Me.txtFirstName.Width
        End If
        If Not (oLabel Is Nothing) Then
'           chkboxes don't have associated label controls-
'           position labels for all other controls
            oLabel.Left = 135   'GLOG : 5701 : ceh
            oLabel.Top = .Top + 50
        End If

'       set tab index - first 12 positions are
'       reserved for design time controls
        If m_iPrevTabIndex = 0 Then
            m_iPrevTabIndex = 14
        End If
        
        If Not (oLabel Is Nothing) Then
            oLabel.TabIndex = m_iPrevTabIndex + 1
            .TabIndex = m_iPrevTabIndex + 2
            m_iPrevTabIndex = .TabIndex
        Else
            .TabIndex = m_iPrevTabIndex + 1
            m_iPrevTabIndex = .TabIndex
        End If
    End With
End Sub


Private Sub SetControlValue(xControlTag As String, vValue As Variant)
    Dim i As Integer
    Dim oCtl As Control

'   cycle through all controls
    For Each oCtl In Me.Controls
'       check only tagged controls - these
'       are the ones associated with person detail
        If Len(oCtl.Tag) Then
            If UCase(oCtl.Tag) = UCase(xControlTag) Then
                Exit For
            End If
        End If
    Next oCtl
    If oCtl Is Nothing Then Exit Sub
'--- set function return = value of control
    If TypeOf oCtl Is TDBCombo Then
        If Len(vValue) Then
            oCtl.BoundText = vValue
        Else
            oCtl.Text = vValue
        End If
    ElseIf TypeOf oCtl Is TDBList Then
        If Len(vValue) Then
            oCtl.BoundText = vValue
        Else
            oCtl.Text = vValue
        End If
    ElseIf TypeOf oCtl Is VB.TextBox Then
        oCtl.Text = vValue
    ElseIf (TypeOf oCtl Is mpControls.SpinnerTextBox) Or _
            (TypeOf oCtl Is mpControls3.SpinTextInternational) Then '*c
        oCtl.Valuev = vValue
    Else
        If Not IsNull(vValue) Then
            oCtl.Value = vValue
        End If
    End If
    
End Sub

Private Sub txtFullName_GotFocus()
    bEnsureSelectedContent Me.txtFullName, , True
End Sub

Private Sub txtInitials_Change()
    m_bInitChanged = True
End Sub

Private Sub txtInitials_GotFocus()
    bEnsureSelectedContent Me.txtInitials, , True
End Sub

Private Sub txtLastName_Change()
    If Not m_bChanged Then
        SetFullName
        SetDisplayName
    End If
    If Not m_bInitChanged Then
        SetInitials
    End If
End Sub

Private Sub txtLastName_GotFocus()
    bEnsureSelectedContent Me.txtLastName, , True
End Sub

Private Sub txtMI_Change()
    If Not m_bChanged Then
        SetFullName
        SetDisplayName
    End If
    If Not m_bInitChanged Then
        SetInitials
    End If
End Sub

Private Sub txtMI_GotFocus()
    bEnsureSelectedContent Me.txtMI, , True
End Sub

Private Function SetupForm()
    '#3471
    With m_oPrevCtl
        Me.Height = .Top + .Height + 1250
        Me.Shape2.Height = .Top + .Height + 1250
        Me.btnCancel.Top = .Top + .Height + 250
        Me.btnLicenses.Top = .Top + .Height + 250
        Me.btnOK.Top = .Top + .Height + 250
    End With
End Function
