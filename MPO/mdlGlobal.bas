Attribute VB_Name = "mdlGlobal"
Option Explicit

Public Declare Function GetKeyState Lib "user32" ( _
    ByVal nVirtKey As Long) As Integer
Public Declare Function GetFocus Lib "user32" () As Long
Public Declare Function GetWindowTextLength Lib "user32" Alias "GetWindowTextLengthA" ( _
    ByVal hwnd As Long) As Long
Public Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" ( _
    ByVal hwnd As Long, ByVal lpString, ByVal cch As Long) As Long
Public Declare Function SetWindowText Lib "user32" Alias "SetWindowTextA" ( _
    ByVal hwnd As Long, ByVal lpString As String) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
    ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Public Declare Function LockWindowUpdate Lib "user32" ( _
    ByVal hwndLock As Long) As Long
Public Declare Function GetDesktopWindow Lib "user32.dll" () As Long

Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)


'HTML help
Public Declare Function HtmlHelp Lib "hhctrl.ocx" Alias "HtmlHelpA" _
                        (ByVal hwndCaller As Long, ByVal pszFile As String, _
                        ByVal uCommand As Long, ByVal dwData As Long) As Long
                        
Public Const HH_DISPLAY_TOPIC = &H0         ' select last opened tab,[display a specified topic]
Public Const HH_DISPLAY_TOC = &H1           ' select contents tab, [display a specified topic]
Public Const HH_DISPLAY_INDEX = &H2         ' select index tab and searches for a keyword
Public Const HH_DISPLAY_SEARCH = &H3        ' select search tab and perform a search

'GLOG : 5594|5381
Public Declare Function GetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByRef lpvParam As Long, ByVal lWinIni As Long) As Long
Public Declare Function SetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByVal lpvParam As Long, ByVal lWinIni As Long) As Long
Public Const SPI_GETKEYBOARDCUES = 4106
Public Const SPI_SETKEYBOARDCUES = 4107

Public Const mpDateFormat_Default = "MM/d/yy"
Public Const mpDateFormat_LongText = "MMMM d, yyyy"
Public Const mpDateFormat_LongTextEuro = "d MMMM yyyy"
Public Const mpDateFormat_ShortText = "MMM d, yyyy"

Public Const mpNoTrailer As String = "***No Trailer - DO NOT delete***"

'---labels & envelope
Public Const mpLabel_HeaderDistance As Single = 0.3
Public Const mpLabel_FooterDistance As Single = 0.3
Public Const mpLabel_FooterDistance_EOD As Single = 0#

'---files
Public Const mpFirmINI As String = "MacPac.ini"
Public Const mpUserINI As String = "User.ini"

'VBA global template
Public Const mpStartupDot As String = "@@MP90.dot"
Public Const mpMenuDot As String = "MPMenu.dot"
Public Const mpAdminDot As String = "mpAdmin.dot"

'   general
Public Const mpBeginTypingText = "Begin Typing Here"

Public g_appCI As mpci.Application
Public g_oLists As mpDB.CLists
Public g_oDBs As mpDB.CDatabase
Public g_oContacts As CContacts
Public g_oError As mpError.CError
Public g_oTemplates As CTemplates
Public g_bScreenUpdating As Boolean
Public g_bForceItemUpdate As Boolean
Public g_bLastCreateCancelled As Boolean
Public g_oDMS As mpDMS.CDMS
Public g_bInit As Boolean
Public iUserChoice As Integer
Public xMsg As String
Public lRet As Long
Public g_bShowAddressBooks As Boolean
Public g_oPrevControl As VB.Control
Public g_bStylesInLH As Boolean
Public g_bIsEval As Boolean
Public g_lDaysLeft As Long
Public g_iSeed As Integer
Public g_iOldSeed As Integer    '9.7.1 - #4223
Public g_bUseDefOfficeInfo As Boolean
Public g_bWarnForPUP As Boolean
Public g_xUSPSCharsToDelete As String
Public g_xBeginTypingText As String

'   Office >= 12x
Public g_bConvertToOffice12Format As Boolean
Public g_bIsWord12x As Boolean
Public g_bIsWord14x As Boolean
Public g_bIsWord15x As Boolean
Public g_bIsWord16x As Boolean
Public g_xKeyTip As String
Public Const mpStartupDot12 As String = "@@MP90.dotm"
Public Const mpMenuDot12 As String = "MacPac.dotm"
Public Const mpAdminDot12 As String = "MacPac Administration.dotm"


'Service list
Public g_iPreviousFieldCount As Integer
Public g_iPreviousPartyTitleIndex As Integer
Public g_iPreviousPartyNameIndex As Integer
Public g_iPreviousDPhraseIndex As Integer

'Splash
Public oSplash As Object

'Contact Detail
Public g_oArrSuffixSeparatorExclusions As XArray
Public g_oArrPrefix As XArray
Public g_oArrSuffix As XArray
Public g_oArrSuffixToExclude As XArray
Public g_oArrPrefixToExclude As XArray
Public g_xSuffixSeparator As String

'Custom Object
Public g_iMaxIndexProperties As Integer

'colors
Public g_lActiveCtlColor As Long
Public g_lInactiveCtlColor As Long

'CI2.0
Public g_oCI20Session As CIX.CSession

Private m_oFSO As New FileSystemObject

'Organizer Save Prompt
Public g_bOrganizerSavePrompt As Boolean
Public g_bSkipStyles As Boolean
Public Const g_xOrganizerSavePrompt As String = "This macro is not available in an unsaved document.  " & _
                                                "Please save your document and run this macro again.  " & vbCr & vbCr & _
                                                "We apologize for the inconvenience but this is a temporary workaround for a Microsoft bug.  " & _
                                                "The bug was introduced recently in Word 2007 SP2 and affects copying styles via code from Word's Organizer feature.  " & _
                                                "Microsoft's hotfix should be available within a couple months."

Public Const g_xOrganizerSavePromptStyles = "This macro is not available in an unsaved document with outline numbered Word Heading styles.  " & _
                                            "Please save your document and run this macro again.  " & vbCr & vbCr & _
                                            "We apologize for the inconvenience but this is a temporary workaround for a Microsoft bug.  " & _
                                            "The bug was introduced recently in Word 2007 SP2 and affects copying styles via code using Word's Organizer feature.  " & _
                                            "Microsoft's hotfix should be available within a couple months."

Public Const g_xOrganizerMPNumberingStylePrompt As String = "The selected text has a MacPac Numbering Scheme applied.  " & _
                                                "Due to an intermittent Microsoft Word bug, this text may be inserted into the document without its numbering applied.  " & vbCr & vbCr & _
                                                "We apologize for the inconvenience.  " & _
                                                "The bug was introduced recently in Word 2007 SP2 and affects copying styles via code from Word's Organizer feature.  " & _
                                                "Microsoft's hotfix should be available within a couple months.  " & _
                                                "Once released this will no longer be an issue."

Public Const g_xOrganizerWordHeadingStylePrompt As String = "The selected text has outline numbered Word Heading styles applied.  " & _
                                                "Due to a Microsoft Word bug, this text can no longer be inserted into the document with its numbering applied.  " & _
                                                "The text will be inserted with Heading styles without their numbering.  " & vbCr & vbCr & _
                                                "We apologize for the inconvenience.  " & _
                                                "The bug was introduced recently in Word 2007 SP2 and affects copying styles via code from Word's Organizer feature.  " & _
                                                "Microsoft's hotfix should be available within a couple months.  " & _
                                                "Once released this will no longer be an issue."

Public Const g_xOrganizerDefaultNumberingStylePrompt As String = "This template was defined to include a default numbering scheme upon creation of a new document.  " & _
                                                "Due to a Microsoft Word bug, the default numbering scheme can no longer be applied automatically.  " & _
                                                "Once the document has been created, please use MacPac Numbering to insert the desired scheme.  " & vbCr & vbCr & _
                                                "We apologize for the inconvenience.  " & _
                                                "The bug was introduced recently in Word 2007 SP2 and affects copying styles via code from Word's Organizer feature.  " & _
                                                "Microsoft's hotfix should be available within a couple months.  " & _
                                                "Once released this will no longer be an issue."

Public Const g_xOrganizerDefaultNumberingStyleNewDocumentPrompt As String = "The selected template was defined to include, for certain types, a default numbering scheme upon creation of a new document.  " & _
                                                "Due to a Microsoft Word bug, the default numbering scheme can no longer be applied automatically.  " & _
                                                "Once the document has been created, please use MacPac Numbering to insert the desired scheme.  " & vbCr & vbCr & _
                                                "We apologize for the inconvenience.  " & _
                                                "The bug was introduced recently in Word 2007 SP2 and affects copying styles via code from Word's Organizer feature.  " & _
                                                "Microsoft's hotfix should be available within a couple months.  " & _
                                                "Once released this will no longer be an issue."
                                                
Public Const g_xProtectedMsg As String = "This is a protected document.  You must first unprotect " & _
                                        "the document before performing this function." & vbCr & _
                                        "Select 'Unprotect Document' from the Tools menu to " & _
                                        "unprotect the document."
                                        
Public Const g_xProtectedMsg12 As String = "This is a protected document.  You must first unprotect " & _
                                        "the document before performing this function." & vbCr & _
                                        "Select 'Protect Document' from the 'Review' tab to " & _
                                        "unprotect the document."
                                        
Public Const g_xProtectedMsg14 As String = "This is a protected document.  You must first unprotect " & _
                                        "the document before performing this function." & vbCr & _
                                        "Select 'Restrict Editing' from the 'Review' tab, or 'Protect Document' from the 'File\Info' menu to " & _
                                        "unprotect the document."

Public g_iStampIndex As Integer
Public Const mpTag As String = "|ZZMPTAG|"
'move to mpError
Private m_iDebugMode As Byte

'Office 2016
Public g_sSetupZoom As Single

'GLOG : 5727 : ceh
Public g_oApp As MPO.CApplication

Sub EchoOff()
    Call LockWindowUpdate(FindWindow("OpusApp", 0&))
End Sub

Sub EchoOn()
    Call LockWindowUpdate(0&)
End Sub

Sub EchoOffDesktop()
    Dim lDhdl As Long
    Dim l As Long
    lDhdl = GetDesktopWindow
    Call LockWindowUpdate(lDhdl)
End Sub

Public Function CurErrDesc() As String
'returns the description of the current error
    CurErrDesc = g_oError.Desc(Err.Number)
End Function

Public Function CurErrSource(ByVal xNewSource As String) As String
'returns the description of the current error
    CurErrSource = g_oError.Source(xNewSource, Err.Source)
End Function

Public Function RaiseError(ByVal xSource As String, Optional ByVal xDescription As String)
    With Err
'       raise the error
        .Raise .Number, _
              g_oError.Source(xSource, .Source), _
              g_oError.Desc(.Number, xDescription)
    End With
End Function

Function GetDocumentManager() As mpDMSs
    Dim iDMS As Integer
    Dim oTsk As Word.Task
    
    iDMS = mpBase2.GetMacPacIni("DMS", "DMS")
    
    Select Case iDMS
        Case mpDMS_PCDOCS
            GetDocumentManager = mpDMS_PCDOCS
        Case mpDMS_IManage4x
            GetDocumentManager = mpDMS_IManage4x
        Case mpDMS_IManage5x
            GetDocumentManager = mpDMS_IManage5x
        Case mpDMS_WorldDox
            GetDocumentManager = mpDMS_WorldDox
        Case mpDMS_Fusion
            GetDocumentManager = mpDMS_Fusion
        Case mpDMS_NetDocs
            GetDocumentManager = mpDMS_NetDocs
        Case mpDMS_HummingbirdCOM
            GetDocumentManager = mpDMS_HummingbirdCOM   '9.6.2020 - #4118
        Case mpDMS_Prolaw
            GetDocumentManager = mpDMS_Prolaw
        Case mpDMS_ndOffice 'GLOG 5399 - ndOffice integration
            GetDocumentManager = mpDMS_ndOffice
        Case Else
            GetDocumentManager = mpDMS_Windows
    End Select
    Exit Function
ProcError:
    RaiseError "MPO.mdlGlobal.GetDocumentManager"
    Exit Function
End Function

Public Sub GetDynamicCheckboxDetail(xFont As String, CheckedASCII As String, UnCheckedASCII As String)
'returns the necessary info to create a dynamic checkbox
    Static xFontTemp As String
    Static xCheckedASCII As String
    Static xUnCheckedASCII As String
    Dim xDesc As String
    Dim oFont As mpDB.CListItem
    
    On Error Resume Next
    If xFontTemp = Empty Then
        xFontTemp = GetMacPacIni( _
            "CustomTemplates", "CheckboxFont")
    End If
    If xCheckedASCII = Empty Then
        xCheckedASCII = Chr(GetMacPacIni( _
            "CustomTemplates", "CheckboxCheckedAscii"))
    End If
    If xUnCheckedASCII = Empty Then
        xUnCheckedASCII = Chr(GetMacPacIni( _
            "CustomTemplates", "CheckboxUnCheckedAscii"))
    End If

    If xFontTemp = Empty Then
'       no font specified -alert and exit
        On Error GoTo ProcError
        xDesc = "No font name for the checkbox was supplied.  " & _
                "Please ensure that the CheckboxFont key in " & _
                "MacPac.ini contains a valid font name."
        Err.Raise mpError_InvalidMember
    Else
'       ensure that font is valid on this system
        On Error Resume Next
        Set oFont = g_oLists.Item("Fonts").ListItems.Item(, xFontTemp)
        On Error GoTo ProcError
        If oFont Is Nothing Then
            xDesc = xFontTemp & " is an invalid font.  " & _
                "It is possible that it is not installed on this machine.  " & _
                "Please have your administrator install this font or change " & _
                "the font that is currently supplying the checkbox character."
            Err.Raise mpError_InvalidMember
        End If
    End If
    
'   ensure that the ascii numbers represent legitmate characters
    If xCheckedASCII = Empty Then
        On Error GoTo ProcError
        xDesc = "Invalid checkbox character.  Please ensure that the " & _
            "value for the CheckboxCheckedAscii key in MacPac.ini " & _
            "represents a valid ascii character."
        Err.Raise mpError_InvalidMember
    End If
    If xUnCheckedASCII = Empty Then
        On Error GoTo ProcError
        xDesc = "Invalid checkbox character.  Please ensure that the " & _
            "value for the CheckboxUnCheckedAscii key in MacPac.ini " & _
            "represents a valid ascii character."
        Err.Raise mpError_InvalidMember
    End If
    
'   return values
    xFont = xFontTemp
    CheckedASCII = xCheckedASCII
    UnCheckedASCII = xUnCheckedASCII
    Exit Sub
ProcError:
    RaiseError "MPO.mdlGlobal.GetDynamicCheckboxDetail", xDesc
    Exit Sub
End Sub

Sub ToggleFormsCheckBox(rngField As Word.Range)
'toggles the clicked MacPac dynamic checkbox
    On Error GoTo ProcError
    Set rngField = Selection.Range
    Application.ScreenUpdating = False

    With rngField
        .Fields.ToggleShowCodes
        With .Find
            .ClearFormatting
            .Text = "CheckBox "
            .Execute
            If .Found Then
                rngField.Collapse wdCollapseEnd
            End If
        End With
        .MoveEnd Unit:=wdCharacter
        .Select
        If Asc(.Text) = 253 Then
            .Text = Chr(168)
        Else
            .Text = Chr(253)
        End If
        .Fields.ToggleShowCodes
        Selection.MoveEnd
        Selection.Collapse wdCollapseEnd
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.mdlGlobal.ToggleFormsCheckBox"
    Exit Sub
End Sub

Sub ToggleFormsCheckboxSet(rngField As Word.Range)
'checks clicked check box,
'unchecks all others in the group
    Dim oRng As Word.Range
    Dim xBMK As String
    Dim iCurIndex As Integer
    Dim i As Integer
    Dim xMsg As String
    Dim bUpdating As Boolean
    Dim bHiddenBmks As Boolean
    
    With Word.Application
        bUpdating = .ScreenUpdating
        .ScreenUpdating = False
    End With
    
    With Word.ActiveDocument.Bookmarks
        bHiddenBmks = .ShowHidden
        .ShowHidden = True
    End With
    
    EchoOff
    With rngField
        xBMK = .Bookmarks(1).Name
        If Not IsNumeric(Right(xBMK, 1)) Then
            xMsg = "The checkbox is improperly bookmarked.  " & _
                "Checkbox sets must be bookmarked such that " & _
                "the bookmark name ends in a digit."
            MsgBox xMsg, vbExclamation, App.Title
            EchoOn
            Exit Sub
        End If
        
        iCurIndex = Right(xBMK, 1)
        xBMK = Left(xBMK, Len(xBMK) - 1)
        .Fields.ToggleShowCodes
        
        With .Find
            .ClearFormatting
            .Text = Chr(168)
            .Execute
            
            If .Found Then
                rngField.Text = Chr(253)
            End If
            rngField.Fields.ToggleShowCodes
        End With
        
        For i = 1 To 9
            If iCurIndex <> i Then
                On Error Resume Next
                Set oRng = Nothing
                Set oRng = Word.ActiveDocument.Bookmarks(xBMK & i).Range
                If oRng Is Nothing Then
                    EchoOn
                    Exit Sub
                End If
                oRng.Fields.ToggleShowCodes
                With oRng.Find
                    .ClearFormatting
                    .Text = Chr(253)
                    .Execute
                    
                    If .Found Then
                        oRng.Text = Chr(168)
                    End If
                End With
                oRng.Fields.ToggleShowCodes
            End If
        Next i
        rngField.Select
    End With
    Word.ActiveDocument.Bookmarks.ShowHidden = bHiddenBmks
    EchoOn
    Word.Application.ScreenUpdating = bUpdating
    Exit Sub
ProcError:
    Word.ActiveDocument.Bookmarks.ShowHidden = bHiddenBmks
    EchoOn
    Word.Application.ScreenUpdating = bUpdating
    RaiseError "MPO.mdlGlobal.ToggleFormsCheckboxSet"
End Sub

Public Function xGetTemplateConfigVal(ByVal xTemplate As String, ByVal xParam As String, _
    Optional bCheckIni As Boolean = False) As String
'returns the value for the specified configuration parameter
    Dim xTemp As String
    Dim xBase As String
    Dim lErr As Long
    Dim xDesc As String
    
    On Error GoTo ProcError
    
'---check for existence of key
    If Not bCheckIni Then
        If g_oDBs.Lists.Exists("CorrespondenceSettings") Then
            With g_oDBs.Lists("CorrespondenceSettings")
                .FilterValue = xTemplate
                .Refresh
                If .ListItems.Count > 0 Then
                    On Error Resume Next
                    xTemp = .ListItems.Item(, xParam).DisplayText
                    Select Case Err.Number
                        Case 91
                            ' No matching key, check Base template next
                        Case 0
                            ' Found value
                            xGetTemplateConfigVal = xTemp
                            Exit Function
                        Case Else
                            lErr = Err.Number
                            xDesc = Err.Description
                            On Error GoTo ProcError
                            Err.Raise lErr
                    End Select
                    On Error GoTo ProcError
                Else
                    ' Check MacPac.ini only of no settings exist for either template or base
                    bCheckIni = True
                End If
                ' Item was not found, so check Base Template next
                xBase = g_oDBs.TemplateDefinitions(xTemplate).BaseTemplate
                If xBase <> "" Then
                    .FilterValue = xBase
                    .Refresh
                    If .ListItems.Count > 0 Then
                        On Error Resume Next
                        xTemp = .ListItems.Item(, xParam).DisplayText
                        Select Case Err.Number
                            Case 91
                                ' No matching item, return empty string
                                xGetTemplateConfigVal = ""
                                Exit Function
                            Case 0
                                ' Found value
                                xGetTemplateConfigVal = xTemp
                                Exit Function
                            Case Else
                                lErr = Err.Number
                                xDesc = Err.Description
                                On Error GoTo ProcError
                                Err.Raise lErr
                        End Select
                        On Error GoTo ProcError
                    Else
                        ' Check MacPac.ini only of no settings exist for either template or base
                        bCheckIni = bCheckIni And True
                    End If
                End If
            End With
        Else
            ' DB List doesn't exist, so check Ini instead
            bCheckIni = True
        End If
    End If
    
    If bCheckIni Then
        If MacPacIniKeyExists(xTemplate, xParam) Then
    '       section/key exists - return value
            xGetTemplateConfigVal = GetMacPacIni(xTemplate, xParam)
        Else
    '       section/key doesn't exist - attempt to
    '       retrieve from section for base template
            xBase = g_oDBs.TemplateDefinitions(xTemplate).BaseTemplate
            xTemp = GetMacPacIni(xBase, xParam)
            xGetTemplateConfigVal = xTemp
        End If
    End If
    Exit Function
ProcError:
    RaiseError "MPO.mdlGlobal.xGetTemplateConfigVal"
    Exit Function
End Function

'9.4.0/ceh/#2214
Public Function bReRegisterServer(sRegister As String, Optional sUnregister As String = "")
    
    On Error GoTo ProcError
    Static strRegCmd As String
    Static strUnRegCmd As String
    
    If strRegCmd = "" Then
        strRegCmd = m_oFSO.GetSpecialFolder(SystemFolder) & "\Regsvr32.exe"
        If Dir(strRegCmd) = "" Then
            strRegCmd = m_oFSO.GetSpecialFolder(WindowsFolder) & "\Regsvr32.exe"
            If Dir(strRegCmd) = "" Then
                MsgBox ("Unable to register MacPac components, Regsvr32.exe not found" & vbCr & _
                    "Please contact your system administrator")
            End If
        End If
        strRegCmd = strRegCmd & " /s "
        strUnRegCmd = strRegCmd & "/u "
    End If
    ' Register DLLs and unregister existing versions if they exist
    If sUnregister <> "" And Dir(sUnregister) <> "" Then
        If InStr(UCase(sUnregister), ".EXE") > 0 Then
            Shell sUnregister & " /unregserver"
        Else
            Shell strUnRegCmd & Chr(34) & sUnregister & Chr(34)
        End If
        DoEvents
    End If
    
    If Dir(sRegister) <> "" Then
        On Error Resume Next
        If InStr(UCase(sRegister), ".EXE") > 0 Then
            Shell sRegister & " /regserver"
        Else
            Shell strRegCmd & Chr(34) & sRegister & Chr(34)
        End If
        If Err.Number Then
            MsgBox ("Unable to register MacPac components, you might not have rights to the registry." & vbCr & _
                "Please contact your system administrator")
        End If
        DoEvents
    End If
    
    Exit Function
    
ProcError:
    RaiseError "MPO.mdlGlobal.bReRegisterServer"
    Exit Function
End Function


Public Sub DebugOutput(ByVal OutputString As String)
     
    Call OutputDebugString(OutputString)
     
End Sub

'move to mpError
Private Function DebugMode() As Boolean
    On Error GoTo ProcError
    Const DEBUG_ON As Byte = 1
    Const DEBUG_OFF As Byte = 2
    
    If m_iDebugMode = Empty Then
        'get whether we're in debug mode or not
        m_iDebugMode = IIf(Trim$(UCase$(GetMacPacIni("General", "Debug"))) = _
            "TRUE", DEBUG_ON, DEBUG_OFF)
    End If
    
    DebugMode = (m_iDebugMode = DEBUG_ON)
    Exit Function
ProcError:
    RaiseError "MPO.CApplication.DebugMode"
    Exit Function
End Function

Public Sub SendToDebug(ByVal xMessage As String, ByVal xSource As String)
'sends the supplied message to debug if in debug mode
    Dim iFile As Integer
    Dim xMsgType As String
    
    On Error GoTo ProcError
    
    If DebugMode = True Then
        'send message to debug log
        iFile = FreeFile()
        Open mpBase2.ApplicationDirectory & "\mpDebug.log" For Append As #iFile
        
        'print info
        Print #iFile, Format(Now(), "mm-dd-yy hh:mm:ss") & "." & Strings.Right(Strings.Format(Timer, "#0.00"), 2) & "  " & xSource
        
        Print #iFile, "    " & xMessage
        
        'print blank line
        Print #iFile, ""
        
        Close #iFile
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CApplication.SendToDebug"
    Exit Sub
End Sub


