VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmOfficeAddresses 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Office Address"
   ClientHeight    =   3780
   ClientLeft      =   3828
   ClientTop       =   996
   ClientWidth     =   5052
   Icon            =   "frmOfficeAddresses.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3780
   ScaleWidth      =   5052
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkIncludeFirmName 
      Appearance      =   0  'Flat
      Caption         =   "Include &Firm/Company Name"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1950
      TabIndex        =   4
      Top             =   2910
      Width           =   2664
   End
   Begin TrueDBList60.TDBCombo cmbFormats 
      Height          =   330
      Left            =   1785
      OleObjectBlob   =   "frmOfficeAddresses.frx":058A
      TabIndex        =   3
      Top             =   2445
      Width           =   3165
   End
   Begin VB.CommandButton btnSetDefault 
      Caption         =   "&Set Default"
      Height          =   400
      Left            =   1770
      TabIndex        =   7
      Top             =   3330
      Width           =   1050
   End
   Begin TrueDBList60.TDBList lstOffices 
      Height          =   2130
      Left            =   1800
      OleObjectBlob   =   "frmOfficeAddresses.frx":279C
      TabIndex        =   1
      Top             =   120
      Width           =   3135
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3960
      TabIndex        =   6
      Top             =   3330
      Width           =   1000
   End
   Begin VB.CommandButton btnInsert 
      Caption         =   "&Insert"
      Default         =   -1  'True
      Height          =   400
      Left            =   2895
      TabIndex        =   5
      ToolTipText     =   "Address inserted at cursor position"
      Top             =   3330
      Width           =   1000
   End
   Begin VB.Label lblStyle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "S&tyle:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   2
      Top             =   2460
      Width           =   1455
   End
   Begin VB.Label lblOffices 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Offices:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   0
      Top             =   165
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   3885
      Left            =   -15
      Top             =   -90
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmOfficeAddresses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oAuthor As mpDB.CPerson
Private m_bInit As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnInsert
End Sub

Private Sub btnInsert_GotFocus()
    OnControlGotFocus Me.btnInsert
End Sub

Private Sub btnSetDefault_GotFocus()
    OnControlGotFocus Me.btnSetDefault
End Sub

Private Sub chkIncludeFirmName_GotFocus()
    OnControlGotFocus Me.chkIncludeFirmName
End Sub
Private Sub cmbFormats_GotFocus()
    OnControlGotFocus Me.cmbFormats
End Sub
Private Sub cmbFormats_ItemChange()
    Dim oFormat As COfficeAddressFormat
    'get format
    Set oFormat = g_oDBs.OfficeAddressFormats(Me.cmbFormats.BoundText)
    'set value of Firm Name
    If oFormat.FirmName <> mpAddressFirmName_None Then
        Me.chkIncludeFirmName.Value = 1
    End If
End Sub

Private Sub cmbFormats_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbFormats, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbFormats_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbFormats)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Private Sub Form_Activate()
    With Me.lstOffices
        .BoundText = g_oDBs.Offices.Default.ID
        .SelectedItem = .Bookmark
    End With
    
    Me.Initializing = False
End Sub

Private Sub Form_Load()
    Me.Initializing = True
    
    With Me.lstOffices
        .Array = g_oDBs.Offices.ListSource
        DoEvents
    End With
    
    With Me.cmbFormats
        .Array = g_oDBs.Lists("Standard Address Formats").ListItems.Source
        .Rebind
        ResizeTDBCombo Me.cmbFormats, 10
        DoEvents
        .Bookmark = 0
    End With

End Sub

Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnInsert_Click()
    Dim rRange As Word.Range
    Dim xSep As String
    Dim xAddress As String
    Dim oDoc As CDocument
    Dim oFormat As COfficeAddressFormat
    Dim bFormatFirmName As Boolean
    
    Me.Hide
    
    Application.ScreenRefresh

    Set oDoc = New CDocument
    Set oFormat = g_oDBs.OfficeAddressFormats(Me.cmbFormats.BoundText)
    
'   get office address string
    With g_oDBs.Offices(Me.lstOffices.BoundText)
        If Me.chkIncludeFirmName Then
            If oFormat.FirmName = mpAddressFirmName_None Then
                xAddress = .FirmName & oFormat.Separator
            End If
            xAddress = xAddress & .Address(oFormat)
        Else
            oFormat.IncludeSlogan = False
            oFormat.FirmName = mpAddressFirmName_None
            xAddress = .Address(oFormat)
        End If
        bFormatFirmName = .FirmNameFormatted <> "" And Me.chkIncludeFirmName
   End With
    
    
    EchoOff
    
'   insert in doc, creating space if necessary
    oDoc.InsertTextAfter xAddress, , _
                         True, oFormat.TrailingSeparator = "", _
                         bFormatFirmName
    
'   format firm name if necessary
    If bFormatFirmName Then
        With ActiveDocument.Bookmarks
            .Add "zzmpFirmAddress", Selection.Range
            
            oDoc.FormatBookmarkString "zzmpFirmAddress", _
                                      g_oDBs.Offices(Me.lstOffices.BoundText).FirmNameFormats
                                      
            .Item("zzmpFirmAddress").Range.Select
            Selection.EndOf
            .Item("zzmpFirmAddress").Delete
        End With
    End If
        
    EchoOn
        
    Set oDoc = Nothing
    Set oFormat = Nothing
    
    Unload Me
End Sub
Private Sub btnSetDefault_Click()
    On Error GoTo ProcError
    With Me
    
        mpBase2.SetUserIni "General", _
                           "Default Office", _
                           .lstOffices.BoundText
                     
        .btnCancel.Caption = "Close"
        .btnInsert.SetFocus
        MsgBox "Your default office has been set to " & _
            .lstOffices.Text & ".", vbInformation, App.Title
    End With
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not set default office."
    Exit Sub
End Sub

Private Sub lstOffices_DblClick()
    btnInsert_Click
End Sub

Private Sub lstOffices_GotFocus()
    OnControlGotFocus lstOffices
End Sub
