VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLetterSig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Option Explicit

'**********************************************************
'   LSig Class
'   created 11/15/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac letter signature

'   Member of App, Letter
'   Container for CDocument
'**********************************************************
Private Const mpAuthorInitSep As String = "/"
Private Const mpMultiAuthorInitSep As String = ":"
Private Const mpSegment As String = "Letter"
Private m_oDoc As MPO.CDocument
Attribute m_oDoc.VB_VarHelpID = -1
Private m_oAuthor As mpDB.CPerson
'---9.7.1 - 4147
Private m_oAuthor2 As mpDB.CPerson
Private m_oProps As MPO.CCustomProperties
Private m_oTemplate As MPO.CTemplate
 
'9.7.1 #4166/#4146
Public Enum mpLetterSignatureFormats
    mpLetterSigStandard = 0
    mpLetterSigElectronic = 1
    mpLetterSigAttorney = 2
    mpLetterSigElectronicAttorney = 3
End Enum
'**********************************************************
'   Properties
'**********************************************************
Public Property Get DefaultAuthor() As mpDB.CPerson
    Set DefaultAuthor = Me.Template.DefaultAuthor
End Property

Public Property Let Author(oNew As mpDB.CPerson)
    If Me.Author Is Nothing Then
        Set m_oAuthor = oNew
    ElseIf (Me.Author.ID <> oNew.ID) Then
        Set m_oAuthor = oNew
    End If
    '9.7.1 #4153
    ' Store Author ID for non-letter use
'    If UCase(Me.Template.ClassName) <> "CLETTER" Then
        m_oDoc.SaveItem "Author", mpSegment, , oNew.ID
'    End If
    
End Property

Private Property Let LastSignatureFormat(iNew As Integer)
    If LastSignatureFormat <> iNew Then
        m_oDoc.SaveItem "LastSignatureFormat", mpSegment, , CStr(iNew)
    End If
End Property
Private Property Get LastSignatureFormat() As Integer
    On Error Resume Next
    LastSignatureFormat = Val(m_oDoc.RetrieveItem("LastSignatureFormat", mpSegment))
End Property
Public Function SignatureFormat() As mpLetterSignatureFormats '9.7.1 #4166/#4146
    Dim iTemp As Integer
    If Me.UseElectronicSignature And bIniToBoolean(xGetTemplateConfigVal(Me.Template.ID, "AllowElectronicSignature")) Then
        iTemp = iTemp + 1
    End If
    
    If UseSeparateAttorneyFormat Then
        If Me.Author.IsAttorney Then
            iTemp = iTemp + 2
        End If
    End If
    
    SignatureFormat = iTemp
End Function
Public Property Get Author() As mpDB.CPerson
    Set Author = m_oAuthor
End Property

Public Property Let Author2(oNew As mpDB.CPerson)
    '       save the id for future retrieval
    If Not oNew Is Nothing Then
        Set m_oAuthor2 = oNew
        m_oDoc.SaveItem "Author2", mpSegment, , oNew.ID
        ' Update ID for Letterhead as well
        m_oDoc.SaveItem "Author2", Me.Document.GetLHSegmentName(Me.Template.ID) & "LH", , oNew.ID
    Else
        Set m_oAuthor2 = Nothing
        m_oDoc.SaveItem "Author2", mpSegment, , ""
        ' Update ID for Letterhead as well
        m_oDoc.SaveItem "Author2", Me.Document.GetLHSegmentName(Me.Template.ID) & "LH", , ""
    End If
    UpdateForAuthor2

End Property
Private Function UseSeparateAttorneyFormat() As Boolean '9.7.1 #4146
    UseSeparateAttorneyFormat = bIniToBoolean(xGetTemplateConfigVal(Me.Template.ID, "AlternateAttorneySignature"))
End Function
Public Property Get Author2() As mpDB.CPerson
    
    '---9.7.1 4147
    Dim lAuthor2ID As Long
    If m_oAuthor2 Is Nothing Then
'       attempt to get Author2 from document
        lAuthor2ID = m_oDoc.RetrieveItem("Author2", mpSegment, , mpItemType_Integer)

'       if an Author2 id has been retrieved from doc
'       the doc has an Author2 set already
        If lAuthor2ID <> 0 Then
            Set m_oAuthor2 = g_oDBs.People(lAuthor2ID)
        End If
    End If
    Set Author2 = m_oAuthor2
End Property
Public Property Let AuthorName(xNew As String)
    If Me.AuthorName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpAuthorName", xNew, , True
        m_oDoc.SaveItem "AuthorName", mpSegment, , xNew
    End If
End Property

Public Property Get AuthorName() As String
    On Error Resume Next
    AuthorName = m_oDoc.RetrieveItem("AuthorName", mpSegment)
End Property
Public Property Let ElectronicSignerName(xNew As String) '9.7.1 #4166
    If Me.ElectronicSignerName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpElectronicSignerName", xNew, , , Not Me.UseElectronicSignature
        m_oDoc.SaveItem "ElectronicSignerName", mpSegment, , xNew
    End If
End Property
Public Property Get ElectronicSignerName() As String '9.7.1 #4166
    On Error Resume Next
    ElectronicSignerName = m_oDoc.RetrieveItem("ElectronicSignerName", mpSegment)
End Property
Public Property Let AuthorName2(xNew As String)
    If Me.AuthorName2 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpAuthorName2", xNew, , True
        m_oDoc.SaveItem "AuthorName2", mpSegment, , xNew
    End If
End Property
Public Property Get AuthorName2() As String
    On Error Resume Next
    AuthorName2 = m_oDoc.RetrieveItem("AuthorName2", mpSegment)
End Property


Public Property Let AuthorTitle(xNew As String)
    If Me.AuthorTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpAuthorTitle", xNew, , , True
        m_oDoc.SaveItem "AuthorTitle", mpSegment, , xNew
    End If
End Property

Public Property Let AuthorTitle2(xNew As String)
    If Me.AuthorTitle2 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpAuthorTitle2", xNew, , , True
        m_oDoc.SaveItem "AuthorTitle2", mpSegment, , xNew
    End If
End Property

Public Property Let AuthorFirmName(xNew As String)
    Dim rRange As Word.Range
    If Me.AuthorFirmName <> xNew Or g_bForceItemUpdate Then
            m_oDoc.EditItem "zzmpFirmName", xNew, , _
            True, True
        m_oDoc.SaveItem "AuthorFirmName", mpSegment, , xNew
        If UCase(xGetTemplateConfigVal(Me.Template.ID, "FormatFirmName")) = "TRUE" Then
            '---call function to format bookmark range
            If xNew <> "" And Me.Author.Office.FirmNameFormatted <> "" Then
                Set rRange = Selection.Range
                Me.Document.FormatBookmarkString "zzmpFirmName", Me.Author.Office.FirmNameFormats
                rRange.Select
            End If
        End If
    End If
End Property
Public Property Let AuthorFirmSlogan(xNew As String) '9.7.1 #4150
    If Me.AuthorFirmSlogan <> xNew Or g_bForceItemUpdate Then
            m_oDoc.EditItem "zzmpFirmSlogan", xNew, , _
            True, True
        m_oDoc.SaveItem "AuthorFirmSlogan", mpSegment, , xNew
    End If
End Property
Public Property Get AuthorFirmSlogan() As String '9.7.1 #4150
    On Error Resume Next
    AuthorFirmSlogan = m_oDoc.RetrieveItem("AuthorFirmSlogan", mpSegment)
End Property

Public Property Get Template() As MPO.CTemplate
    If m_oTemplate Is Nothing Then
'DanCore 9.2.0 - Multitype:
        Set m_oTemplate = g_oTemplates.Item(Word.ActiveDocument.AttachedTemplate)
        'Set m_oTemplate = g_oTemplates.ItemFromClass("CLetter")
    End If
    Set Template = m_oTemplate
End Property

Public Property Let CCFromCI(xNew As String)
    On Error GoTo ProcError
        m_oDoc.SaveItem "CCFromCI", mpSegment, , xNew
    Exit Property
ProcError:
    RaiseError "MPO.CLetterSig.CCFromCI"
End Property

Public Property Get CCFromCI() As String
    On Error Resume Next
    CCFromCI = m_oDoc.RetrieveItem("CCFromCI", mpSegment)
End Property

Public Property Let BCCFromCI(xNew As String)
    On Error GoTo ProcError
        m_oDoc.SaveItem "BCCFromCI", mpSegment, , xNew
    Exit Property
ProcError:
    RaiseError "MPO.CLetterSig.BCCFromCI"
End Property

Public Property Get BCCFromCI() As String
    On Error Resume Next
    BCCFromCI = m_oDoc.RetrieveItem("BCCFromCI", mpSegment)
End Property

Public Property Let CCFromCIFullDetail(xNew As String)
    On Error GoTo ProcError
        m_oDoc.SaveItem "CCFromCIFullDetail", mpSegment, , xNew
    Exit Property
ProcError:
    RaiseError "MPO.CLetterSig.CCFromCIFullDetail"
End Property

Public Property Get CCFromCIFullDetail() As String
    On Error Resume Next
    CCFromCIFullDetail = m_oDoc.RetrieveItem("CCFromCIFullDetail", mpSegment)
End Property

Public Property Let BCCFromCIFullDetail(xNew As String)
    On Error GoTo ProcError
        m_oDoc.SaveItem "BCCFromCIFullDetail", mpSegment, , xNew
    Exit Property
ProcError:
    RaiseError "MPO.CLetterSig.BCCFromCIFullDetail"
End Property

Public Property Get BCCFromCIFullDetail() As String
    On Error Resume Next
    BCCFromCIFullDetail = m_oDoc.RetrieveItem("BCCFromCIFullDetail", mpSegment)
End Property

Public Property Get AuthorTitle() As String
    On Error Resume Next
    AuthorTitle = m_oDoc.RetrieveItem("AuthorTitle", mpSegment)
End Property

Public Property Get AuthorTitle2() As String
    On Error Resume Next
    AuthorTitle2 = m_oDoc.RetrieveItem("AuthorTitle2", mpSegment)
End Property

Public Property Get AuthorFirmName() As String
    On Error Resume Next
    AuthorFirmName = m_oDoc.RetrieveItem("AuthorFirmName", mpSegment)
End Property

Public Property Get InitialsSeparator()
    Dim xTemp As String
    xTemp = xGetTemplateConfigVal(Me.Template.ID, "InitialsSeparator")
    If xTemp = "" Then
        xTemp = mpAuthorInitSep
    End If
    InitialsSeparator = xTemp
End Property

Public Property Get MultiInitialsSeparator()
    '---9.7.1 - 4147
    Dim xTemp As String
    xTemp = xGetTemplateConfigVal(Me.Template.ID, "MultiAuthorInitialsSeparator")
    If xTemp = "" Then
        xTemp = mpMultiAuthorInitSep
    End If
    MultiInitialsSeparator = xTemp
End Property

Public Property Get Options() As mpDB.CPersonOptions
    Set Options = Me.Author.Options(Me.Template.ID)
End Property

Public Property Let ClosingPhrase(xNew As String)
    If Me.ClosingPhrase <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpClosingPhrase", xNew, , True
        m_oDoc.SaveItem "ClosingPhrase", mpSegment, , xNew
    End If
End Property

Public Property Get ClosingPhrase() As String
    On Error Resume Next
    ClosingPhrase = m_oDoc.RetrieveItem("ClosingPhrase", mpSegment)
End Property

Public Property Let Enclosures(xNew As String)
    If Me.Enclosures <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpEnclosures", xNew, , True, (xNew = Empty)
        m_oDoc.SaveItem "Enclosures", mpSegment, , xNew
    End If
End Property

Public Property Get Enclosures() As String
    On Error Resume Next
    Enclosures = m_oDoc.RetrieveItem("Enclosures", mpSegment)
End Property

Public Property Let IncludeAuthorTitle(bNew As mpTriState)
    If Me.IncludeAuthorTitle <> bNew Or g_bForceItemUpdate Or Not (Me.Author2 Is Nothing) Then
        m_oDoc.HideItem "zzmpAuthorTitle", (bNew = 0) Or Me.AuthorTitle = ""    '9.7.1 - #1284
        '---9.7.1 - 4147
        m_oDoc.HideItem "zzmpAuthorTitle2", (bNew = 0) Or Me.AuthorTitle2 = ""    '9.7.1 - #1284
        m_oDoc.SaveItem "IncludeAuthorTitle", mpSegment, , CStr(bNew)
    End If
End Property

Public Property Get IncludeAuthorTitle() As mpTriState
    On Error Resume Next
    IncludeAuthorTitle = m_oDoc.RetrieveItem("IncludeAuthorTitle", mpSegment)
End Property

Public Property Let CC(xNew As String)
    If Me.CC <> xNew Or g_bForceItemUpdate Then
        Document.SaveItem "CC", mpSegment, , xNew
        UpdateCC xNew
    End If
End Property

Public Property Get CC() As String
    On Error Resume Next
    CC = m_oDoc.RetrieveItem("CC", mpSegment)
End Property

Public Property Let BCC(xNew As String)
    If Me.BCC <> xNew Or g_bForceItemUpdate Then
        Document.SaveItem "BCC", mpSegment, , xNew
        UpdateBCC xNew
    End If
End Property

Public Property Get BCC() As String
    On Error Resume Next
    BCC = m_oDoc.RetrieveItem("BCC", mpSegment)
End Property

Public Property Let IncludeFirmName(bNew As mpTriState)
    If Me.IncludeFirmName <> bNew Or g_bForceItemUpdate Then
        m_oDoc.HideItem "zzmpFirmName", (bNew = 0)
        m_oDoc.HideItem "zzmpFirmSlogan", (bNew = 0) '9.7.1 #4150
        m_oDoc.SaveItem "IncludeFirmName", mpSegment, , CStr(bNew)
    End If
End Property

Public Property Get IncludeFirmName() As mpTriState
    On Error Resume Next
    IncludeFirmName = Document.RetrieveItem("IncludeFirmName", mpSegment)
End Property
Public Property Let UseElectronicSignature(bNew As Boolean) '9.7.1 #4166
    If Me.UseElectronicSignature <> bNew Or g_bForceItemUpdate Then
        m_oDoc.HideItem "zzmpElectronicSignerName", Not bNew
        m_oDoc.SaveItem "UseElectronicSignature", mpSegment, , CStr(bNew)
        RebuildIfNecessary True
    End If
End Property
Public Property Get UseElectronicSignature() As Boolean '9.7.1 #4166
    On Error Resume Next
    UseElectronicSignature = Document.RetrieveItem("UseElectronicSignature", mpSegment)
End Property
Public Property Let TypistInitials(xNew As String)
    Dim xInitials As String
    
        xInitials = GetInitials(xNew)
        
'       write to doc
        Document.EditItem "zzmpTypistInitials", xInitials
        
'       save in doc and in ini
        Document.SaveItem "TypistInitials", mpSegment, , xNew
        SetUserIni Me.Template.ID, "TypistInitials", xNew
'    End If
End Property
Public Property Get TypistInitials() As String
    Dim xTypistInitials As String
    
    On Error Resume Next
    If Me.Document.IsCreated Then ' 9.7.1 #4211
        ' Only read variable if reusing
        xTypistInitials = m_oDoc.RetrieveItem("TypistInitials", mpSegment)
    End If
    If xTypistInitials = "" Then _
        xTypistInitials = GetUserIni(Me.Template.ID, "TypistInitials")
    TypistInitials = xTypistInitials
    
End Property

Public Property Let AuthorInitials(xNew As String)
    '---9.7.1 - 4149
    Dim xInitials As String
'       save in doc
    Document.SaveItem "AuthorInitials", mpSegment, , xNew
    Me.TypistInitials = Me.TypistInitials
End Property

Public Property Get AuthorInitials() As String
    '---9.7.1 - 4149
    Dim xAuthorInitials As String
    
    On Error Resume Next
    xAuthorInitials = m_oDoc.RetrieveItem("AuthorInitials", mpSegment)
    
    AuthorInitials = xAuthorInitials
    
End Property

Public Property Get AllowAuthorInitials() As Boolean
    '---9.7.1 - 4149
    Dim bTemp As Boolean
    On Error Resume Next
'**********************************************************
'DanCore 9.2.0 - Multitype:
    bTemp = xGetTemplateConfigVal(Me.Template.ID, "AllowAuthorInitials")
'    bTemp = GetMacPacIni(Me.Template.ID, "AllowOptionalSignature")
'**********************************************************
    AllowAuthorInitials = bTemp
End Property


'**********************************************************
'   Methods
'**********************************************************
Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Sub Refresh()
'--refresh all properties by setting them to themselves
'--this ensures creation of doc vars
    Dim i As Integer
    Dim oCustProp As MPO.CCustomProperty
    With Me
        .AuthorName = .AuthorName
        .AuthorTitle = .AuthorTitle
        
        '---9.7.1 - 4148
        .AuthorName2 = .AuthorName2
        .AuthorTitle2 = .AuthorTitle2
        
        .AuthorFirmName = .AuthorFirmName
        .AuthorFirmSlogan = .AuthorFirmSlogan '9.7.1 #4150
        
        .BCC = .BCC
        .CC = .CC
        .ClosingPhrase = .ClosingPhrase
        .Enclosures = .Enclosures
        .IncludeAuthorTitle = .IncludeAuthorTitle
        .IncludeFirmName = .IncludeFirmName
        .AuthorInitials = .AuthorInitials
        .TypistInitials = .TypistInitials
        .ElectronicSignerName = .ElectronicSignerName '9.7.1 #4166
        .UseElectronicSignature = .UseElectronicSignature '9.7.1 #4166
'       refresh all custom properties
        .CustomProperties.RefreshValues
    End With
End Sub

Public Sub RefreshEmpty()
'force properties that may not have been
'set by user to get updated in document
    Dim oCustProp As MPO.CCustomProperty
    Dim bForceUpdate As Boolean
    
    bForceUpdate = g_bForceItemUpdate
    g_bForceItemUpdate = True
        
    With Me
        If .BCC = Empty Then _
            .BCC = Empty
        If .CC = Empty Then _
            .CC = Empty
        If .Enclosures = Empty Then _
            .Enclosures = Empty
        '---9.7.1 - 4149
        If .AuthorInitials = Empty Then _
            .AuthorInitials = Empty
        
        If .TypistInitials = Empty Then _
            .TypistInitials = Empty
        
'       refresh empty custom properties
        .CustomProperties.RefreshEmptyValues
    End With
    g_bForceItemUpdate = bForceUpdate
End Sub

'Public Sub InsertBoilerplate()
'    Dim xFontName As String
'
'    With Me.Document
'        .InsertBoilerplate "Letter.mbp", "zzmpSignature", , , , , _
'             mpBase2.BoilerplateDirectory
'
'        With .File.Styles
'            xFontName = .Item(wdStyleNormal).Font.Name
'            .Item("Letter Signature").Font.Name = xFontName
'            .Item("Letter Signature Sub").Font.Name = xFontName
'        End With
'
'        On Error Resume Next
'        .File.Bookmarks("zzmpBCCPage").Range.Style = "Letter Signature Sub"
'
'    End With
'
'End Sub

'Public Sub InsertBoilerplate()
'    Dim xFontName As String
'
'    With Me.Document
'        .InsertBoilerplate Me.Template.BoilerplateFile, "zzmpSignature", , , , , _
'                 Me.Template.BoilerplateDirectory
'
'        With .File.Styles
'            xFontName = .Item(wdStyleNormal).Font.Name
'            .Item("Letter Signature").Font.Name = xFontName
'            .Item("Letter Signature Sub").Font.Name = xFontName
'        End With
'
'        On Error Resume Next
'        .File.Bookmarks("zzmpBCCPage").Range.Style = "Letter Signature Sub"
'
'    End With
'
'End Sub

'*******************************************************************
'DanCore 9.2.0 - Multitype:
Public Sub InsertBoilerplate()
    Dim xFontName As String
    Dim xBP As String
    Dim xBase As String
    Dim oTemplateDef As mpDB.CTemplateDef
    Dim xDefTemplate As String
          
    On Error GoTo ProcError
    
    '9.7.1 #4166/#4146 - First check for new Formats table
    If InsertBoilerplateFormatted Then
        Exit Sub
    End If
    With Me.Document
'       attempt to insert signature from attached template's boilerplate File
        On Error Resume Next
        With g_oDBs.TemplateDefinitions
            Set oTemplateDef = .Item(Me.Template.ID)
            On Error GoTo ProcError
            If oTemplateDef Is Nothing Then
'               insert signature from default or letter template's bp
                xDefTemplate = GetMacPacIni("Signature", "DefaultLetterTemplate")
                If Len(xDefTemplate) = 0 Then
                    If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Or g_bIsWord16x Then
                        xDefTemplate = "Letter.dotx"
                    Else
                        xDefTemplate = "Letter.dot"
                    End If
                End If
                Me.Document.InsertBoilerplate .Item(xDefTemplate).BoilerplateFile, "zzmpSignature"
            Else
'               attempt to insert sig bp from template's bp file
                xBP = oTemplateDef.BoilerplateFile
                Dim xBPPath As String

                If oTemplateDef.WriteToBP Then
                    xBPPath = UserBoilerplateDirectory()
                End If

                On Error Resume Next
                If xBP <> "" Then
                    Me.Document.InsertBoilerplate xBP, "zzmpSignature", , , , , xBPPath
                End If
                If xBP = "" Or Err.Number Then
                    On Error GoTo ProcError
'                   attempt to insert sig bp from base template's bp file
                    xBase = oTemplateDef.BaseTemplate
                    xBP = g_oDBs.TemplateDefinitions(xBase).BoilerplateFile
                    
                    If xBP <> "" Then
                        Me.Document.InsertBoilerplate xBP, "zzmpSignature"
                    End If
                    If xBP = "" Or Err.Number Then
                        On Error GoTo ProcError
'                       insert signature from default or letter template's bp
                        xDefTemplate = GetMacPacIni("Signature", "DefaultLetterTemplate")
                        If Len(xDefTemplate) = 0 Then
                            If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Or g_bIsWord16x Then
                                xDefTemplate = "Letter.dotx"
                            Else
                                xDefTemplate = "Letter.dot"
                            End If
                        End If
                        '9.7.1 #4153
                        If .Item(xDefTemplate).WriteToBP Then
                            xBPPath = mpBase2.UserBoilerplateDirectory
                        Else
                            xBPPath = mpBase2.BoilerplateDirectory
                        End If
                        Me.Document.InsertBoilerplate .Item(xDefTemplate).BoilerplateFile, "zzmpSignature", , , , , xBPPath
                    End If
                End If
            End If
        End With
        
        With .File.Styles
            xFontName = .Item(wdStyleNormal).Font.Name
            .Item("Letter Signature").Font.Name = xFontName
            .Item("Letter Signature Sub").Font.Name = xFontName
        End With
        
        On Error Resume Next
        .File.Bookmarks("zzmpBCCPage").Range.Style = "Letter Signature Sub"
        On Error GoTo ProcError
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CLetterSig.InsertBoilerplate"
    Exit Sub
End Sub
Private Function InsertBoilerplateFormatted() As Boolean '9.7.1 #4166/#4146
    Dim oTemplate As mpDB.CTemplateDef
    Dim xTemplate As String
    Dim xDefTemplate
    Dim xBase As String
    Static oList As mpDB.CList
    Dim xBP As String

    On Error GoTo ProcError
    If oList Is Nothing Then
        If Not g_oDBs.Lists.Exists("LetterSignatureFormats") Then
            ' Not set up to use new scheme
            Exit Function
        End If

        Set oList = g_oDBs.Lists("LetterSignatureFormats")
    End If

    xTemplate = Me.Template.ID
    With oList
        .FilterValue = xTemplate
        .Refresh
        If .ListItems.Count = 0 Then
            ' No assignments, so try Base Template
            If g_oDBs.TemplateDefinitions.Exists(xTemplate) Then
                xBase = g_oDBs.TemplateDefinitions(xTemplate).BaseTemplate
                If xBase <> xTemplate Then
                    xTemplate = xBase
                    .FilterValue = xTemplate
                    .Refresh
                    If .ListItems.Count = 0 Then
                        ' Still no match so try Default Letter template
                        xDefTemplate = GetMacPacIni("Signature", "DefaultLetterTemplate")
                        If Len(xDefTemplate) = 0 Then
                            If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Or g_bIsWord16x Then
                                xDefTemplate = "Letter.dotx"
                            Else
                                xDefTemplate = "Letter.dot"
                            End If
                        End If
                        If g_oDBs.TemplateDefinitions.Exists(xDefTemplate) Then
                            xTemplate = xDefTemplate
                            .FilterValue = xTemplate
                            .Refresh
                        End If
                    End If
                End If
            End If
        End If
        If .ListItems.Count > 0 Then
            On Error Resume Next
            xBP = oList.ListItems.Item(, Me.SignatureFormat).DisplayText
            On Error GoTo ProcError
            If xBP <> "" Then
                'Check for signature bookmark - it won't exist if LSig macro is running
                If Word.ActiveDocument.Bookmarks.Exists("zzmpSignature") Then
                    ActiveDocument.Bookmarks("zzmpSignature").Range.Select
                    Word.Selection.EndOf wdParagraph, wdExtend
                End If
                With Me.Document
                    .InsertBoilerplate xBP, "zzmpSignature"
                    With .File.Styles
                        Dim xFontName As String
                        xFontName = .Item(wdStyleNormal).Font.Name
                        .Item("Letter Signature").Font.Name = xFontName
                        .Item("Letter Signature Sub").Font.Name = xFontName
                    End With
                    On Error Resume Next
                    .File.Bookmarks("zzmpBCCPage").Range.Style = "Letter Signature Sub"
                    On Error GoTo ProcError
                End With
                LastSignatureFormat = Me.SignatureFormat
                InsertBoilerplateFormatted = True
                Exit Function
            End If
        End If
    End With
    ' If we get here, no match was found for the signature type
    ' Use standard format, but warn if another format is specified
    If Me.SignatureFormat <> mpLetterSigStandard Then
        Dim xFormat As String
        Select Case Me.SignatureFormat
            Case mpLetterSigElectronic
                xFormat = "Electronic"
            Case mpLetterSigAttorney
                xFormat = "Attorney"
            Case mpLetterSigElectronicAttorney
                xFormat = "Electronic Attorney"
        End Select
        MsgBox "No boilerplate is configured for the " & xFormat & " Signature type.  " & _
            "Standard Format will be used.  Formats should be configured in tblLetterSignatureFormats.", _
            vbInformation
        InsertBoilerplateFormatted = False
    End If
    Exit Function
ProcError:
    RaiseError "MPO.CLetterSig.InsertBoilerplateFormatted"
    Exit Function

End Function

Public Function Rebuild()
    Me.InsertBoilerplate
    DoEvents
End Function


Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Sub Finish(Optional bForceRefresh As Boolean = True)
'cleans up doc after all user input is complete
    Dim rngP As Range

    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    g_bForceItemUpdate = True
    
'   show all chars-  necessary to guarantee
'   that hidden text is deleted when appropriate
    Word.ActiveDocument.ActiveWindow.View.ShowHiddenText = True
    
    With m_oDoc
'       Refresh properties
        If bForceRefresh Then
            Refresh
        End If
        .StorePersonInDoc Me.Author
 '      delete any hidden text -
 '      ie text marked for deletion
        .DeleteHiddenText
        .RemoveFormFields
        .ClearBookmarks True
    End With
    
    g_bForceItemUpdate = False
    Exit Sub
ProcError:
    g_bForceItemUpdate = False
    RaiseError "MPO.CLetterSig.Finish"
    Exit Sub
End Sub

Public Property Get LetterAuthor() As mpDB.CPerson
    Dim lAuthorID As Long
    
'   attempt to get author from document
    lAuthorID = m_oDoc.RetrieveItem("Author", mpSegment, , mpItemType_Integer)

    If lAuthorID <> 0 Then 'mpUndefined Then
        Set LetterAuthor = g_oDBs.People(lAuthorID)
    Else
        Set LetterAuthor = Nothing
    End If
    
End Property

Public Sub GetStartingAuthor()
'gets the first author for this object-
'first tries document, then default author,
'then first person in db list
    With m_oDoc
        If (m_oAuthor Is Nothing) Then
'            If .IsCreated And Not (.ReuseAuthor Is Nothing) Then
            If Not (.ReuseAuthor Is Nothing) Then   '9.7.1 - #3827
                Me.Author = .ReuseAuthor
            ElseIf Not (Me.Template.DefaultAuthor Is Nothing) Then
                Me.Author = Me.Template.DefaultAuthor
                Me.UpdateForAuthor
            Else
                Me.Author = g_oDBs.People.First
                Me.UpdateForAuthor
            End If
        End If
    End With
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Public Function UpdateForAuthor()
' updates relevant letter properties to
' values belonging to current Author - called
' from m_oDoc_AuthorChange

    Dim xDesc As String
    Dim bForce As Boolean

'   set appropriate author detail fields
    With Me.Author
        bForce = g_bForceItemUpdate
        g_bForceItemUpdate = True
        Me.TypistInitials = Me.TypistInitials
        Me.AuthorInitials = Me.AuthorInitials
        g_bForceItemUpdate = bForce
        Me.AuthorName = .FullName
        Me.AuthorTitle = .Title
        Me.AuthorFirmName = .Office.FirmName
        Me.AuthorFirmSlogan = .Office.Slogan '9.7.1 #4150
        Me.ElectronicSignerName = .FullName '9.7.1 #4166
    End With
    
'   set appropriate options fields
    With Me.Options
        If .Count = 0 Then
'           alert that author Preferences were not found
            xDesc = "Letter Author Preferences were expected but not found.  " & _
                "Please ensure that there is a Letter Author Preferences table " & _
                "in the public database, and that it is specified as the " & _
                "Preferences table for the Letter template in tblTemplates."
            Err.Raise mpError_AuthorPreferencesExpected
        End If
        On Error Resume Next
'       set options properties
        Me.ClosingPhrase = .Item("cmbClosingPhrases")
        bForce = g_bForceItemUpdate
        g_bForceItemUpdate = True
        Me.IncludeAuthorTitle = .Item("chkIncludeAuthorTitle")
        Me.IncludeFirmName = .Item("chkIncludeFirmName")
        Me.UseElectronicSignature = .Item("chkElectronicSignature") '9.7.1 #4166
        g_bForceItemUpdate = bForce
        RebuildIfNecessary True '9.7.1 #4166/4146
        On Error GoTo ProcError
    End With
    Me.CustomProperties.UpdateForAuthor Me.Author, Me.Options
    Exit Function
    
ProcError:
    RaiseError "MPO.CLetterSig.UpdateForAuthor", xDesc
    Exit Function
End Function

Public Function UpdateForAuthor2()
' updates relevant letter properties to
' values belonging to current Author - called
' from me.author2

    Dim xDesc As String
    Dim bForce As Boolean
    bForce = g_bForceItemUpdate
    g_bForceItemUpdate = True

'   set appropriate author detail fields
    With Me.Author2
        If Not Me.Author2 Is Nothing Then
            Me.AuthorName2 = .FullName
            Me.AuthorTitle2 = .Title
        Else
            Me.AuthorName2 = ""
            Me.AuthorTitle2 = ""
        End If
    End With
    
    g_bForceItemUpdate = bForce
    
    Exit Function
    
ProcError:
    RaiseError "MPO.CLetterSig.UpdateForAuthor2", xDesc
    Exit Function
End Function
Private Sub UpdateCC(xNew As String)
    Dim xLabel As String
    '#4604
    xLabel = xGetTemplateConfigVal(Me.Template.ID, "CCLabelText")
    If xLabel = "" Then
        xLabel = "cc:"
    End If
    xNew = xSubstitute(xNew, vbCrLf, Chr(11))
    m_oDoc.EditItem "zzmpCC", xLabel & vbTab & xNew, , True, (xNew = "")

End Sub

Private Sub UpdateBCC(xNew As String)
    Dim xLabel As String
    '9.7.1 #4151
    xLabel = xGetTemplateConfigVal(Me.Template.ID, "BCCLabelText")
    If xLabel = "" Then
        xLabel = "bcc:"
    End If
    xNew = xSubstitute(xNew, vbCrLf, Chr(11))
    m_oDoc.EditItem "zzmpBCC", xLabel & vbTab & xNew, , True, (xNew = "")
End Sub

'********************************************
'per log item 1805
'Private Function GetInitials(ByVal xTypistInitials As String) As String
'    Dim xAthInitials As String
'
'    If xTypistInitials <> Empty Then
''       get author initials
'        xAthInitials = Me.Author.Initials
'        If xAthInitials <> Empty Then
''           author initials exist - append separator
'            xAthInitials = xAthInitials & _
'                GetMacPacIni(Me.Template.ID, "InitialsSeparator")
'        End If
'
''       append typist initals to author
''       initials and separator
'        GetInitials = xAthInitials & xTypistInitials
'    End If
'End Function
Private Function GetInitials(ByVal xTypistInitials As String) As String
    Dim xAthInitials As String
    Dim xSep As String
    
'       get author initials
    '---9.7.1 - 4149
    If Me.AllowAuthorInitials = True Then
        xAthInitials = Me.AuthorInitials
    Else
        If xTypistInitials <> Empty Then
            xAthInitials = Me.Author.Initials
        End If
    End If
    
    If xAthInitials <> Empty Then
'           author initials exist - append separator
'           chcore - 9.3.0
'        If Me.AllowAuthorInitials = True Then
            If xTypistInitials <> "" Then
                xSep = xGetTemplateConfigVal(Me.Template.ID, "InitialsSeparator")
            End If
'        End If
                    
        xAthInitials = xAthInitials & xSep
    End If
    
'       append typist initals to author
'       initials and separator
    GetInitials = xAthInitials & xTypistInitials
End Function
Friend Sub RebuildIfNecessary(Optional bRefresh As Boolean = False, Optional bForce As Boolean = False)
    Dim bUpdate As Boolean
    
    ' Only necessary if dynamic editing is active
    If Not Word.ActiveDocument.Bookmarks.Exists("zzmpSignature") Then Exit Sub
    
    ' Don't rebuild signature if format hasn't changed
    If Me.SignatureFormat <> LastSignatureFormat Or bForce Then
        If InsertBoilerplateFormatted And bRefresh Then
            bUpdate = g_bForceItemUpdate
            g_bForceItemUpdate = True
            Refresh
            g_bForceItemUpdate = bUpdate
        End If
    End If
End Sub
'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    m_oProps.Category = "LetterSig"
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
    Set m_oProps = Nothing
    Set m_oTemplate = Nothing
End Sub

