VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   Letter Class
'   created 11/15/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac letter

'   Member of App
'   Container for CDocument
'**********************************************************
Private Const mpTemplate As String = "Letter"
Private m_Document As MPO.CDocument
Private m_lsgP As MPO.CLetterSig
Private m_cProps As MPO.CCustomProperties
Private m_Letterhead As MPO.CLetterhead
Private m_oAuthor As mpDB.CPerson

'**********************************************************
'   Properties
'**********************************************************
Public Property Get DefaultAuthor() As mpDB.CPerson
    Set DefaultAuthor = Document.DefaultAuthor
End Property

Public Property Let DefaultAuthor(psnNew As mpDB.CPerson)
   Dim psnTemp As mpDB.CPerson
   Set psnTemp = g_oDBs.SetDefaultAuthor(psnNew.ID, mpTemplate)
End Property

Public Property Let Author(oNew As mpDB.CPerson)
    Dim bDo As Boolean
    
'   set property if no author currently exists
'   or if the author has changed
    If m_oAuthor Is Nothing Then
        bDo = True
    ElseIf m_oAuthor.ID <> oNew.ID Then
        bDo = True
    End If
    
    If bDo Then
        Set m_oAuthor = oNew
'       save the id for future retrieval
        m_Document.SaveItem "Author", , , oNew.ID

'       sync letterhead and signature
        Me.Signature.Author = oNew
        Me.Letterhead.Author = oNew
        UpdateForAuthor
    End If
End Property

Public Property Get Author() As mpDB.CPerson
    Dim lAuthorID As Long
    If m_oAuthor Is Nothing Then
'       attempt to get author from document
        lAuthorID = m_Document.RetrieveItem("Author", , , mpItemType_Integer)

'       if an author id has been retrieved from doc
'       the doc has an author set already
        If lAuthorID <> mpUndefined Then
            Me.Author = g_oDBs.People(lAuthorID)
        End If
    End If
    Set Author = m_oAuthor
End Property

Public Property Let Recipients(xNew As String)
    If Me.Recipients <> xNew Or Me.Document.ForceItemUpdate Then
        m_Document.SaveItem "Recipients", , , xNew
        UpdateRecipients xNew
    End If
End Property

Public Property Get Recipients() As String
    On Error Resume Next
    Recipients = m_Document.RetrieveItem("Recipients")
End Property

Public Property Let Reline(xNew As String)
    If Me.Reline <> xNew Or Me.Document.ForceItemUpdate Then
        UpdateReline xNew
        m_Document.SaveItem "Reline", , , xNew
    End If
End Property

Public Property Get Reline() As String
    On Error Resume Next
    Reline = m_Document.RetrieveItem("Reline")
End Property

Public Property Let Salutation(xNew As String)
    If Me.Salutation <> xNew Then
        m_Document.EditItem "zzmpSalutation", xNew, , True
        m_Document.SaveItem "Salutation", , , xNew
    End If
End Property

Public Property Get Salutation() As String
    Salutation = m_Document.RetrieveItem("Salutation")
End Property

Public Property Get Options() As mpDB.CPersonOptions
    If Me.Author.ID = Empty Then
'       person has no ID, return default office options
        Set Options = g_oDBs.Offices.Default.Options("Letter")
    Else
'       return the options for the author
        Set Options = Me.Author.Options("Letter")
    End If
End Property
'**********************************************************
'   Methods
'**********************************************************

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_cProps
End Function


Public Function Signature() As MPO.CLetterSig
    Set Signature = m_lsgP
End Function

Public Function Letterhead() As MPO.CLetterhead
    Set Letterhead = m_Letterhead
End Function

Public Function Rebuild()
'    Dim bDynamic As Boolean
'
'    Application.ScreenUpdating = false
'    bDynamic = g_bDynamicEditing
'    g_bDynamicEditing = False
'
'    UpdateLetterhead
'    UpdateRecipients
'    UpdateReline
''    m_Letter.UpdateForAuthor
'    UpdateCC
'    UpdateBCC
'
'    EditItem "zzmpSig_Closing",  Me.cmbClosingPhrases
'    HideItem "zzmpLH_Author", (Me.ckhIncludeLetterheadName = 0), , True
'    HideItem "zzmpLH_Phone", (Me.chkIncludeLetterheadPhone = 0), , True
'    HideItem "zzmpLH_EMail", (Me.chkIncludeLetterheadEMail = 0), , True
'    EditItem "zzmpDeliveryPhrases",  Me.txtDeliveryPhrases
'    EditItem "zzmpSalutation",  Me.txtSalutation(1)
'    EditItem "zzmpSig_Closing",  Me.cmbClosingPhrases
'    EditItem "zzmpPage2LH_Text", Me.txtHeaderAdditionalText
'    HideItem "zzmpSig_AuthorTitle", (Me.chkIncludeAuthorTitle = 0), , True
'    HideItem "zzmpSig_FirmName", (Me.chkIncludeFirmName = 0), , True
'    EditItem "zzmpSig_Initials",  Me.txtTypistInitials
'    EditItem "zzmpSig_Enclosures",  Me.cmbEnclosures
'
'    g_bDynamicEditing = bDynamic
End Function

Public Sub SelectItem(xBkMk As String, Optional StoryRange As Word.Range)
    m_Document.SelectItem xBkMk
End Sub

Public Function Document() As MPO.CDocument
    Set Document = m_Document
End Function


Public Sub SetTemplateDefaults(lID As Long)
    Dim xTemplate As String
    
    Application.ScreenUpdating = False
    
    xTemplate = Document.WorkGroupPath & "\Letter.dot"
    
    Screen.MousePointer = vbHourglass

'   open the letter template
    Me.Document.OpenDoc xTemplate
    
    With Me.Options
        Document.SetStyles .Item("cmbNormalFontName"), _
                           .Item("spnNormalFontSize"), _
                           .Item("cmbBodyTextAlignment"), _
                           .Item("spnBodyTextFirstLineIndent")
    
'---    update letterheadtype & date
        Letterhead.UpdateLetterhead .Item("cmbLetterheadType")
        If .Item("cmbDateType").Value = "Field" Then
            Document.EditDateItem "zzmpInsertDateAsField", , , mpDateFormat_LongText
        End If
        Letterhead.Email = Me.Author.Email
        Letterhead.Phone = Me.Author.Phone
        Letterhead.Name = Me.Author.FullName
        Letterhead.IncludeEMail = .Item("chkIncludeLetterheadEMail")
        Letterhead.IncludePhone = .Item("chkIncludeLetterheadPhone")
        Letterhead.IncludeName = .Item("chkIncludeLetterheadName")

'---    set signature defaults
        Signature.AuthorName = Me.Author.FullName
        Signature.IncludeAuthorTitle = .Item("chkIncludeAuthorTitle")
        Signature.IncludeFirmName = .Item("chkIncludeFirmName")
        Signature.ClosingPhrase = .Item("cmbClosingPhrases")
        Signature.AuthorTitle = Me.Author.Title
        Signature.TypistInitials = Me.Signature.TypistInitials
    End With
    
    With Me.Document
'       delete all vars except Fixed ones
        .DeleteDocVars "zzmpFixed"
        
'''''       set author in template
''''        Me.AuthorID = Me.DefaultAuthor.ID
        
'       mark last edit time
        .SetVar "LastEditTime", Now
        
'       save and close template
        .CloseDoc True
        
    End With
        
    
    Screen.MousePointer = vbDefault
    Application.ScreenUpdating = True
    DoEvents
End Sub

'Public Sub AutoNew()
'   check last edit time doc var with last modified doc property
'    Dim datLastEdit As Date
'    Dim datFileDate As Date
'    Dim xMsg As String
'    Dim lID As Long
'    Dim iSource As Integer
'    Dim xLetter As String
'
'    xLetter = Word.Options.DefaultFilePath(wdWorkgroupTemplatesPath) & "\Letter.dot"
'
'    With ActiveDocument.AttachedTemplate
'        datFileDate = .BuiltInDocumentProperties("Last Save Time")
'        .Saved = True
'    End With
'
'    datLastEdit = ActiveDocument.Variables("LastEditTime")
'    If datFileDate > (datLastEdit + 1 / (24 * 60)) Then
'        xMsg = "You have received an updated Letter template from your administrator." & vbCr & "MacPac will now update this " & _
                     "template with your personal defaults."
'        MsgBox xMsg, vbInformation, App.Title
'        mpPeople.iGetDefaultAuthor g_appMP.PrivateDB, mpTemplate, lID, iSource
'        SetTemplateDefaults lID, iSource
 '       Word.ActiveDocument.Close wdDoNotSaveChanges
'        Word.Documents.Add xLetter
'    Else
'        Create
'    End If
'End Sub

Public Sub Finish(Optional bForceRefresh As Boolean = True)
'cleans up doc after all user input is complete
    Dim rngP As Range
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    With m_Document
        If bForceRefresh Then
            RefreshProperties
            Me.Letterhead.RefreshProperties
        Else
            RefreshOptionalEmptyProperties
            Me.Letterhead.RefreshProperties
        End If
        
'       save author for reuse
        .StorePersonInDoc Me.Author
        
 '      delete any hidden text - ie text marked for deletion
        .DeleteHiddenText , , , True
        .RemoveFormFields
        .SelectStartPosition
        .ClearBookmarks
'        .IsCreated = True
    End With
    
    Exit Sub

ProcError:
    Err.Raise Err.Number
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Private Function UpdateForAuthor()
' updates relevant letter properties to
' values belonging to current Author
    
    With Me.Options
        Document.SetStyles .Item("cmbNormalFontName"), _
                            .Item("spnNormalFontSize"), _
                            .Item("cmbBodyTextAlignment"), _
                            .Item("spnBodyTextFirstLineIndent")
    End With
End Function

Public Sub RefreshProperties()
'--refresh all properties by setting them to themselves
'--this ensures creation of doc vars
    Dim i As Integer
    
    With Me
        .Recipients = .Recipients
        .Reline = .Reline
        .Salutation = .Salutation

'cycle thru custom properties
        For i = 1 To .CustomProperties.Count
            With .CustomProperties(i)
                .Text = .Text
                .Value = .Value
                .Bookmark = .Bookmark
            End With
        Next
    End With
End Sub

Public Sub RefreshOptionalEmptyProperties()
'--Use this to retain empty property placeholders in underlying doc
'--Skipping some properties will leave their placeholders intact

    Dim i As Integer
    With Me
        
        If Me.Reline = "" Then
            Me.Reline = ""
        End If

'cycle thru custom properties
        For i = 1 To .CustomProperties.Count
            .CustomProperties(i).Text = .CustomProperties(i).Text
            .CustomProperties(i).Value = .CustomProperties(i).Value
            .CustomProperties(i).Bookmark = .CustomProperties(i).Bookmark
        Next
    End With
End Sub

Private Sub UpdateReline(xReline As String)
    Dim rngP As Range
    On Error Resume Next
    m_Document.EditItem "zzmpReline", xReline, , True
    
    If xReline <> "" Then
        Set rngP = ActiveDocument.Bookmarks("zzmpReline").Range
        m_Document.bUnderlineToLengthOfLongest rngP

'       redefine reline bookmark to include all of underline
        With Word.ActiveDocument.Bookmarks
            .Item("zzmpReline").Delete
            rngP.MoveEndUntil vbCr
            .Add "zzmpReline", rngP
        End With
    End If
End Sub

Private Sub UpdateRecipients(xNew As String)
'updates recipients
    Dim xTo As String
    Dim xResult As String
    Dim iPos As String
    Dim iTo As Integer
    Dim xTemp As String
    
    #If compHandleError Then
        On Error GoTo ProcError
    #End If
    
'   count number of recipients
    If xNew <> "" Then
        iTo = lCountChrs(xNew, vbCrLf & vbCrLf) + 1
    Else
        iTo = 0
    End If
    
    On Error Resume Next
    Application.ScreenUpdating = False
'   if less than threshold, add, else add as table
    If iTo < 3 Then
        Document.DeleteTable "zzmpRecipients", , True
        m_Document.EditItem "zzmpRecipients", xNew, vbCr, True
    Else
        Document.DeleteTable "zzmpRecipients", , True
        Document.ConvertToTable xNew, 2, vbCrLf & vbCrLf, iTo
    End If
        
    Application.ScreenUpdating = True
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    Err.Raise Err.Number
End Sub

'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_Document = New MPO.CDocument
    m_Document.Selection.StartOf
    Set m_lsgP = New MPO.CLetterSig
    Set m_cProps = New MPO.CCustomProperties
    Set m_Letterhead = New MPO.CLetterhead
    With m_Document
        .Template = mpTemplate
        If .IsCreated And Not (.ReuseAuthor Is Nothing) Then
            Me.Author = .ReuseAuthor
        ElseIf Not (.DefaultAuthor Is Nothing) Then
            Me.Author = .DefaultAuthor
        Else
            MsgBox "We must add an optional index parameter to " & _
                "the item method of CPersons to get the first person " & _
                "in the public list in cases where there is no " & _
                "default author.", vbInformation, "Programmer's Note"
        End If
    End With
End Sub

Private Sub Class_Terminate()
    Set m_lsgP = Nothing
    Set m_cProps = Nothing
    Set m_Document = Nothing
    Set m_Letterhead = Nothing
    Set m_oAuthor = Nothing
End Sub


