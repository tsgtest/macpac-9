VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomDocObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CustomDocObj Class
'   created 12/26/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac Custom Document Object

'   Member of App
'   Container for CDocument, CTemplate, mpDB.CPerson
'**********************************************************
Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oAuthor As mpDB.CPerson
Private m_bInit As Boolean
Private m_xName As String
Implements MPO.IDocObj

'**********************************************************
'   Properties
'**********************************************************
Public Property Get DefaultAuthor() As mpDB.CPerson
    Set DefaultAuthor = Me.Template.DefaultAuthor
End Property

Public Property Let DefaultAuthor(psnNew As mpDB.CPerson)
   Me.Template.DefaultAuthor = psnNew
End Property

Public Property Let Author(oNew As mpDB.CPerson)
    Dim bDo As Boolean
    
'   set property if no author currently exists
'   or if the author has changed
    If m_oAuthor Is Nothing Then
        bDo = True
    ElseIf m_oAuthor.ID <> oNew.ID Then
        bDo = True
    End If
    
    If bDo Then
        Set m_oAuthor = oNew
        
'       save the id for future retrieval
        m_oDoc.SaveItem "Author", , , oNew.ID
    End If
End Property

Public Property Get Author() As mpDB.CPerson
    Dim lAuthorID As Long
    If m_oAuthor Is Nothing Then
'       attempt to get author from document
        lAuthorID = m_oDoc.RetrieveItem("Author", , , mpItemType_Integer)

'       if an author id has been retrieved from doc
'       the doc has an author set already
        If lAuthorID <> 0 Then 'mpUndefined Then
            Me.Author = g_oDBs.People(lAuthorID)
        End If
    End If
    Set Author = m_oAuthor
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Get Options() As mpDB.CPersonOptions
    If Me.Template.OptionsTable <> "" Then
        If Me.Author.ID = Empty Then
'           person has no ID, return default office options
            Set Options = g_oDBs.Offices.Default.Options(Me.Name)
         Else
'           return the options for the author
        Set Options = Me.Author.Options(Me.Name)
        End If
    End If
End Property

Public Property Get Initialized() As Boolean
    Initialized = m_bInit
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Template.BoilerplateFile
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = Me.Template.CustomProperties
End Function

Public Sub SelectItem(xBookmark As String)
    m_oDoc.SelectItem xBookmark
End Sub

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Function InsertBoilerplate()
    Me.Template.InsertBoilerplate
End Function

Private Sub Finish(Optional bForceRefresh As Boolean = True)
'cleans up doc after all user input is complete
    Dim rngP As Range
    Dim i As Integer

    Dim xProtectedSecs As String
    
    On Error GoTo ProcError
    
    g_bForceItemUpdate = True
    With m_oDoc
        If bForceRefresh Then
            Refresh
        Else
            RefreshEmpty
        End If
                
'       save author for reuse
        .StorePersonInDoc Me.Author
        
'        get protected sections if any
         xProtectedSecs = m_oTemplate.ProtectedSections
        
'       delete any hidden text
        .DeleteHiddenText True, True, True, True, True  '***9.7.1
        .RemoveFormFields
        
'       run editing macro
        RunEditMacro Me.Template
                    
        .SelectStartPosition , , , (xProtectedSecs <> "")
        .ClearBookmarks
        
'       reprotect if necessary
        If xProtectedSecs <> "" Then
            .Protect
            With ActiveDocument.Sections
                For i = 1 To .Count
                    .Item(i).ProtectedForForms = _
                        (InStr(xProtectedSecs, "|" & i & "|") <> 0) '*c
                Next i
            End With
        End If
    End With
    g_bForceItemUpdate = False
    Exit Sub

ProcError:
    g_bForceItemUpdate = False
    RaiseError "MPO.CCustomDocObj.Finish"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Public Function UpdateForAuthor()
' updates relevant letter properties to
' values belonging to current Author - called
' from m_oDoc_AuthorChange
    
'   update custom properties of custom object
'   that are author or author options related
    Me.CustomProperties.UpdateForAuthor Me.Author, Me.Options
End Function

Private Sub Refresh()
'--refresh all properties by setting them to themselves
'--this ensures creation of doc vars and edits doc
    With Me
'       refresh custom properties
        Me.CustomProperties.RefreshValues
    End With
End Sub

Private Sub RefreshEmpty()
'force properties that may not have been
'set by user to get updated in document
    Dim i As Integer
    Dim bForceUpdate As Boolean
    
    bForceUpdate = g_bForceItemUpdate
    g_bForceItemUpdate = True
    Me.CustomProperties.RefreshEmptyValues
    g_bForceItemUpdate = bForceUpdate
End Sub

Friend Sub Initialize(xName As String)
    On Error GoTo ProcError
    Set m_oDoc = New MPO.CDocument
    
    On Error Resume Next
    Set m_oTemplate = g_oTemplates(xName)
        
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidTemplateName
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
    With m_oDoc
        .Selection.StartOf
        If .IsCreated And Not (.ReuseAuthor Is Nothing) Then
            Me.Author = .ReuseAuthor
        ElseIf Not (Me.DefaultAuthor Is Nothing) Then
            Me.Author = Me.DefaultAuthor
            If Me.Template.UpdateRequired Then
'               write defaults to template
                Me.Template.UpdateDefaults Me.Author.ID, Me
            End If
        Else
            Me.Author = g_oDBs.People.First
            With Me.Template
                .DefaultAuthor = Me.Author
                 .UpdateDefaults Me.Author.ID, Me
             End With
        End If
    End With
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CCustomDocObj.Initialize"
    Exit Sub
End Sub

'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Terminate()
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
    Set m_oAuthor = Nothing
End Sub

'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
    Me.Author = RHS
End Property

Private Property Get IDocObj_Author() As mpDB.CPerson
    Set IDocObj_Author = Me.Author
End Property

Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property

Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function

Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
    Me.DefaultAuthor = RHS
End Property

Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
    Set IDocObj_DefaultAuthor = Me.DefaultAuthor
End Property

Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function

Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub

Private Sub IDocObj_Initialize()
    Me.Initialize Me.Name
End Sub

Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property

Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub

Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub

Private Sub IDocObj_Refresh()
    Refresh
End Sub

Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function

Private Sub IDocObj_UpdateForAuthor()
    Me.UpdateForAuthor
End Sub
