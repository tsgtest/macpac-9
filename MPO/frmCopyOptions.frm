VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmCopyAuthorDefaults 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Copy Author Preferences"
   ClientHeight    =   2340
   ClientLeft      =   45
   ClientTop       =   240
   ClientWidth     =   5520
   Icon            =   "frmCopyOptions.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2340
   ScaleWidth      =   5520
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnOK 
      Caption         =   "&OK"
      Default         =   -1  'True
      Height          =   400
      Left            =   4350
      TabIndex        =   1
      Top             =   1365
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4350
      TabIndex        =   2
      Top             =   1830
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbAuthor 
      Height          =   2145
      Left            =   135
      OleObjectBlob   =   "frmCopyOptions.frx":058A
      TabIndex        =   0
      Top             =   1365
      Width           =   3615
   End
   Begin VB.Label lblAuthor 
      Caption         =   "###"
      Height          =   1080
      Left            =   150
      TabIndex        =   3
      Top             =   195
      Width           =   5160
   End
End
Attribute VB_Name = "frmCopyAuthorDefaults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    m_bCancelled = True
End Sub

Private Sub btnOK_Click()
    Me.Hide
    DoEvents
    m_bCancelled = False
End Sub

Private Sub cmbAuthor_Close()
    Application.ScreenRefresh
End Sub

Private Sub Form_Load()
    Me.cmbAuthor.Array = g_oDBs.People.ListSource
    m_bCancelled = True
End Sub
