VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusiness"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'****************************************************
'   CBusiness Class
'   created 3/17/00 by Daniel Fisherman
'   Contains properties and methods that
'   define the CBusiness Class - those
'   procedures that build a MacPac business document
'****************************************************

Option Explicit
Private Const mpSegment As String = "Business"
Private m_xTitle As String
Private m_oDoc As CDocument
Private m_oTemplate As CTemplate
Private m_oProps As CCustomProperties
Private m_bInit As Boolean
Private m_oDef As CBusinessDef

Implements MPO.IDocObj

Public Property Let TypeID(lNew As Long)
    Dim oDef As CBusinessDef
    Dim xDesc As String
    
'   check for valid business type
    On Error Resume Next
    Set m_oDef = g_oDBs.BusinessDefs(lNew)
    On Error GoTo ProcError
    
'   alert and exit if not valid
    If m_oDef Is Nothing Then
        xDesc = " is an invalid business type id. " & _
            "Please check tblBusinessTypes in mpPublic.mdb."
        Err.Raise mpError_InvalidMember
    End If
    
'   assign template
    Me.Template.BoilerplateFile = m_oDef.Boilerplate
    
'   apply type
    ApplyType
    
'   save type in doc
    m_oDoc.SaveItem "BusinessTypeID", mpSegment, , lNew
    
    Exit Property
ProcError:
    RaiseError "MPO.CBusiness.TypeID"
    Exit Property
End Property

Public Property Get TypeID() As Long
    TypeID = m_oDoc.RetrieveItem("BusinessTypeID", mpSegment)
End Property

Public Property Let DocumentTitle(xNew As String)
    If Me.DocumentTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpDocumentTitle", xNew, , True
        m_oDoc.SaveItem "DocumentTitle", mpSegment, , xNew
    End If
End Property

Public Property Get DocumentTitle() As String
    DocumentTitle = m_oDoc.RetrieveItem("DocumentTitle", mpSegment)
End Property

Public Property Let DeliveryPhrases(xNew As String)
    If Me.DeliveryPhrases <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpDeliveryPhrases", xNew, , True
        m_oDoc.EditItem "zzmpDeliveryPhrasesPage1", xNew, , True
        m_oDoc.SaveItem "DeliveryPhrases", mpSegment, , xNew
    End If
End Property

Public Property Get DeliveryPhrases() As String
    DeliveryPhrases = m_oDoc.RetrieveItem("DeliveryPhrases", mpSegment)
End Property
Public Property Let DeliveryPhrases2(xNew As String) '9.7.1 #4024
    If Me.DeliveryPhrases2 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpDeliveryPhrases2", xNew, , True
        m_oDoc.EditItem "zzmpDeliveryPhrases2Page1", xNew, , True
        m_oDoc.SaveItem "DeliveryPhrases2", mpSegment, , xNew
    End If
End Property

Public Property Get DeliveryPhrases2() As String '9.7.1 #4024
    DeliveryPhrases2 = m_oDoc.RetrieveItem("DeliveryPhrases2", mpSegment)
End Property

Public Property Get Initialized() As Boolean
    Initialized = m_bInit
End Property

Public Sub InsertBoilerplate()
    Me.Template.InsertBoilerplate
End Sub

Public Function Definition() As CBusinessDef
    Set Definition = m_oDef
End Function

Public Sub Finish(Optional bForceRefresh As Boolean = True)
'cleans up doc after all user input is complete
    Dim rngP As Range
    Dim oApp As Object
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    g_bForceItemUpdate = True
    
'   show all chars-  necessary to guarantee
'   that hidden text is deleted when appropriate
    Word.ActiveDocument.ActiveWindow.View.ShowHiddenText = True
    
    With m_oDoc
        If bForceRefresh Then
            Refresh
        Else
            RefreshEmpty
        End If
        
 '      delete any hidden text
        .DeleteHiddenText , True, True, True, True
        .RemoveFormFields
'       set default numbering scheme - 9.7.1 - #2489
'       Organizer Copy Bug
        If m_oDef.DefaultNumberingScheme <> "" And (Not g_bSkipStyles) Then
            Set oApp = GetObject(, "Word.Application")
            On Error Resume Next
            oApp.Run "zzmpUseScheme", m_oDef.DefaultNumberingScheme
            On Error GoTo ProcError
        End If
        
'       run editing macro
        RunEditMacro Me.Template
        
        .SelectStartPosition
        .DeleteLastParagraph
        
'       manually assign "No Reuse Author" -
'       this is necessary because document.IsCreated
'       will be TRUE only if Reuse Author is not undefined -
'       since there is no author in business, just dummy it.
        Me.Document.SetVar "ReuseAuthor", "NoAuthor"        '9.7.1 - #4223
        DoEvents
        .ClearBookmarks
    End With
    g_bForceItemUpdate = False
    Exit Sub

ProcError:
    g_bForceItemUpdate = False
    RaiseError "MPO.CBusiness.Finish"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub ApplyType()
    Dim bForce As Boolean
    Dim oSec As Word.Section
    
    On Error GoTo ProcError
    
    For Each oSec In ActiveDocument.Sections
        m_oDoc.ClearSection oSec
    Next oSec

'   insert boilerplate
    Me.Template.InsertBoilerplate
    
    With Word.ActiveDocument.PageSetup
'       apply margins
        .LeftMargin = m_oDef.LeftMargin * 72
        .RightMargin = m_oDef.RightMargin * 72
        .TopMargin = m_oDef.TopMargin * 72
        .BottomMargin = m_oDef.BottomMargin * 72
        .DifferentFirstPageHeaderFooter = m_oDef.DifferentPage1HeaderFooter
        .PageHeight = m_oDef.PageHeight * 72
        .PageWidth = m_oDef.PageWidth * 72
        
'       apply orientation
        .Orientation = m_oDef.Orientation
         
'       apply columns / column spacing
        With .TextColumns
            .SetCount m_oDef.Columns
            If .Count > 1 Then
                .Spacing = m_oDef.SpaceBetweenColumns * 72
                'reset 1st section to 1 column only
                ActiveDocument.Sections.First.PageSetup.TextColumns.SetCount 1    '9.7.1 - #3013
            End If
        End With
        
'       refresh dphrases and delivery phrases
        bForce = g_bForceItemUpdate
        g_bForceItemUpdate = True
        If Me.DeliveryPhrases <> Empty Then
            If m_oDef.AllowDeliveryPhrases Then
                Me.DeliveryPhrases = Me.DeliveryPhrases
            Else
                Me.DeliveryPhrases = Empty
            End If
        End If
        
        '9.7.1 #4024
        If Me.DeliveryPhrases2 <> Empty Then
            If m_oDef.AllowDeliveryPhrases And m_oDef.SplitDPhrases Then
                Me.DeliveryPhrases2 = Me.DeliveryPhrases2
            Else
                Me.DeliveryPhrases2 = Empty
            End If
        End If
        
        If Me.DocumentTitle <> Empty Then
            If m_oDef.AllowDocumentTitle Then
                Me.DocumentTitle = Me.DocumentTitle
            Else
                Me.DocumentTitle = Empty
            End If
        End If
        g_bForceItemUpdate = bForce
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CBusiness.ApplyType"
    Exit Sub
End Sub

Private Sub Refresh()
'--refresh all properties by setting them to themselves
'--this ensures creation of doc vars and edits doc
    Dim oCustProp As MPO.CCustomProperty
    
    With Me
        .DeliveryPhrases = .DeliveryPhrases
        .DeliveryPhrases2 = .DeliveryPhrases2 '9.7.1 #4024
        .DocumentTitle = .DocumentTitle

'       refresh all custom properties
        .CustomProperties.RefreshValues
    End With
End Sub

Private Sub RefreshEmpty()
'force properties that may not have been
'set by user to get updated in document
    Dim oCustProp As MPO.CCustomProperty
    Dim bForceUpdate As Boolean
    
    bForceUpdate = g_bForceItemUpdate
    g_bForceItemUpdate = True
    
    With Me
        If .DeliveryPhrases = Empty Then _
            .DeliveryPhrases = Empty
        If .DeliveryPhrases2 = Empty Then _
            .DeliveryPhrases2 = Empty '9.7.1 #4024
        If .DocumentTitle = Empty Then _
            .DocumentTitle = Empty

'       refresh custom properties
        .CustomProperties.RefreshEmptyValues
    End With
    g_bForceItemUpdate = bForceUpdate
End Sub

Public Sub Initialize()
    Dim xDesc As String
    
    On Error GoTo ProcError
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    
    On Error Resume Next
    Set m_oTemplate = g_oTemplates.ItemFromClass("CBusiness")
        
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        xDesc = "Could not find the business template."
        Err.Raise mpError_InvalidTemplateName
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    With m_oDoc
        .Selection.StartOf
    End With
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CBusiness.Initialize", xDesc
    Exit Sub
End Sub

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
    Set m_oProps = New CCustomProperties
    Set m_oTemplate = New CTemplate
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
    Set m_oProps = Nothing
    Set m_oTemplate = Nothing
End Sub

'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
End Property

Private Property Get IDocObj_Author() As mpDB.CPerson
End Property

Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.Template.BoilerplateFile
End Property

Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function

Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
End Property

Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
End Property

Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function

Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub

Private Sub IDocObj_Initialize()
    Me.Initialize
End Sub

Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property

Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub

Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub

Private Sub IDocObj_Refresh()
    Refresh
End Sub

Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function

Private Sub IDocObj_UpdateForAuthor()
End Sub

