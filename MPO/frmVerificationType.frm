VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmVerificationType 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Verification"
   ClientHeight    =   3132
   ClientLeft      =   36
   ClientTop       =   360
   ClientWidth     =   5784
   Icon            =   "frmVerificationType.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3132
   ScaleWidth      =   5784
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TrueDBList60.TDBList lstFavorites 
      Height          =   2010
      Left            =   2280
      OleObjectBlob   =   "frmVerificationType.frx":058A
      TabIndex        =   6
      Top             =   3105
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.CheckBox chkFavorites 
      Caption         =   "Fa&vorites"
      Height          =   400
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   2655
      Visible         =   0   'False
      Width           =   795
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4680
      TabIndex        =   5
      Top             =   2655
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   3615
      TabIndex        =   4
      Top             =   2655
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbLevel0 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmVerificationType.frx":26F6
      TabIndex        =   1
      Top             =   180
      Width           =   3930
   End
   Begin TrueDBList60.TDBList lstVerificationTypes 
      Height          =   1815
      Left            =   1770
      OleObjectBlob   =   "frmVerificationType.frx":4EC3
      TabIndex        =   3
      Top             =   705
      Width           =   3930
   End
   Begin VB.CommandButton btnAddToFaves 
      Caption         =   "&Add..."
      Height          =   400
      Left            =   960
      TabIndex        =   8
      Top             =   2655
      Visible         =   0   'False
      Width           =   765
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "D&elete..."
      Height          =   400
      Left            =   960
      TabIndex        =   10
      Top             =   2580
      Visible         =   0   'False
      Width           =   765
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "T&ype:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   2
      Top             =   720
      Width           =   1275
   End
   Begin VB.Label lblLevel0 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&State:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   0
      Top             =   240
      Width           =   1275
   End
   Begin VB.Label lblFavorites 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "&Favorites:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1875
      TabIndex        =   7
      Top             =   2775
      Visible         =   0   'False
      Width           =   1410
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   3540
      Left            =   -30
      Top             =   -375
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmVerificationType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_bInitializing As Boolean
Private m_bDefRetrieved As Boolean
Private m_bFaveRetrieved As Boolean
Private m_bSettingAllLevels As Boolean
Private m_xIniSection As String

'**********************************************************
'   Properties
'**********************************************************
Public Property Get VerificationType() As Long
    VerificationType = Me.lstVerificationTypes.BoundText
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub btnOK_Click()
    SetDefaults
    Me.Cancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmbLevel0_ItemChange()
    ShowVerificationTypes
End Sub

Private Sub cmbLevel0_Mismatch(NewEntry As String, Reposition As Integer)
    CorrectTDBComboMismatch Me.cmbLevel0, Reposition
End Sub

Private Sub Form_Activate()
    Dim lID As Long
    Dim lTypeID As Long
    Dim xTemplate As String
    Dim oVerification As MPO.CVerification
    Dim i As Integer
    Dim xVerificationType As String
    Dim bFound As Boolean
    
    If m_bInitializing Then
        LockWindowUpdate Me.hwnd
'       get default level 0
        xTemplate = g_oTemplates.ItemFromClass("CVerification").FileName
        
        ' Create Verification Object to check for existing type
        Set oVerification = New MPO.CVerification
        lTypeID = oVerification.TypeID
        Set oVerification = Nothing
        
        On Error Resume Next
        If lTypeID > 0 Then
            lID = g_oDBs.VerificationDefs.Item(lTypeID).Level0
        Else
            xTemplate = g_oTemplates.ItemFromClass("CVerification").FileName
            On Error Resume Next
            lID = GetUserIni(xTemplate, "Level0")
        End If
        On Error GoTo 0
        
        If lID Then
            Me.cmbLevel0.BoundText = lID
            DoEvents
            If lTypeID Then
                With lstVerificationTypes
                    For i = 0 To .Array.Count(1) - 1
                        If .Array.Value(i, 1) = lTypeID Then
                            .SelectedItem = i
                            bFound = True
                            Exit For
                        End If
                    Next i
                End With
                
                If Not bFound Then
            '       get Verification type
                    xVerificationType = GetUserIni(xTemplate, "VerificationType")
                    If xVerificationType <> "" Then
                        Me.lstVerificationTypes.BoundText = xVerificationType
                    Else
                        Me.lstVerificationTypes.SelectedItem = 0
                    End If
                End If
                
            Else
        '       get Verification type
                xVerificationType = GetUserIni(xTemplate, "VerificationType")
                If xVerificationType <> "" Then
                    Me.lstVerificationTypes.BoundText = xVerificationType
                Else
                    Me.lstVerificationTypes.SelectedItem = 0
                End If
            End If
        Else
'           select first item in list
            Me.cmbLevel0.SelectedItem = 0
        End If
        
        
        DoEvents
        LockWindowUpdate &O0
        
'       UNREM THESE LINES FOR THE 7/5/00 RELEASE
        EchoOn
        Application.ScreenUpdating = True
        Screen.MousePointer = vbDefault
        m_bInitializing = False
    End If
End Sub

Private Sub Form_Load()
    Dim bFaves As Boolean
    Dim iFaves As Integer
    
    On Error GoTo ProcError
    m_bInitializing = True
    m_bCancelled = True
    
    On Error Resume Next
'   load lists
    With g_oDBs.Lists
        '---9.6.1 - only display level 0 for states actually configured in tblVerificationTypes
        Dim iCount
        iCount = .Item("VerificationStatesConfigured").ListItems.Count
        If iCount > 0 Then
            Me.cmbLevel0.Array = _
                .Item("VerificationStatesConfigured").ListItems.Source
        Else
            Me.cmbLevel0.Array = _
                .Item("PleadingCourtsLevel0").ListItems.Source
        End If
    End With
    On Error GoTo ProcError
    
    Me.lstFavorites.Array = g_oDBs.PleadingFavorites.List
        
    ResizeTDBCombo Me.cmbLevel0, 6

'   show panel last displayed
    m_xIniSection = g_oTemplates.ItemFromClass("CVerification").ID
    
    'Get Descriptive Title from CVerification
    Me.Caption = "Create a " & g_oTemplates.ItemFromClass("CVerification").ShortName '*c
    
    Exit Sub
    
ProcError:
    RaiseError "MPO.frmVerificationType.FormLoad"
    Exit Sub
End Sub

Private Sub SetDefaults()
'sets level 0-3 defaults to user.ini
'based on current values of controls

    SetUserIni m_xIniSection, "Level0", Me.cmbLevel0.BoundText
    SetUserIni m_xIniSection, "VerificationType", Me.lstVerificationTypes.BoundText
    
'    SetUserIni m_xIniSection, "ShowFavorites", Me.chkFavorites.Value
'    SetUserIni m_xIniSection, "Favorite", Me.lstFavorites.BoundText
End Sub
Private Sub GetPanel1Defaults()
'gets level 0-3 defaults from user.ini-
'sets controls to supplied values-
'selects first item in each list if no
'supplied value
    Dim lLevel0 As Long
    
'   get sticky fields from ini
    On Error Resume Next
    lLevel0 = GetUserIni(m_xIniSection, "Level0")
    
    m_bSettingAllLevels = True
    
'   select item if supplied, else select first item in list
    If lLevel0 Then
        Me.cmbLevel0.BoundText = lLevel0
    Else
        Me.cmbLevel0.SelectedItem = 0
    End If
    
    ShowVerificationTypes
    
    m_bSettingAllLevels = False
    
'   note that defaults have been retrieved -
'   this is used in chkFavorites_Click to determine
'   whether to retrieve defaults
    m_bDefRetrieved = True
End Sub

Private Sub ShowVerificationTypes()
'updates all lists
    Dim l0 As Long
    
    On Error GoTo ProcError
    l0 = Me.cmbLevel0.BoundText
    With g_oDBs.Lists("VerificationTypes")
        .FilterValue = l0
        .Refresh
        Me.lstVerificationTypes.Array = .ListItems.Source
        With Me.lstVerificationTypes
            .Rebind
            If .Array.Count(1) Then
                .Row = 0
            End If
            Me.btnOK.Enabled = .Row > -1
        End With
    End With
    DoEvents
    Exit Sub
ProcError:
    RaiseError "MPO.frmVerificationType.ShowVerificationType"
End Sub

Private Sub lstFavorites_DblClick()
    btnOK_Click
End Sub
'
'Private Sub lstFavorites_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyDelete Then
'        DeleteFavorite
'    End If
'End Sub

'Private Sub GetDefaults()
'    Dim bPanel1 As Boolean
'
''   get defaults if switching to non-favorites panel
''   and defaults haven't been gotten (m_bDefRetrieved = false)
'    If bPanel1 And _
'        Not m_bDefRetrieved Then
'        GetPanel1Defaults
'    ElseIf Not bPanel1 And _
'        Not m_bFaveRetrieved Then
'        GetPanel2Defaults
'    End If
'
''   guarantee a favorites selection
'    If Not bPanel1 And Me.lstFavorites.Row = -1 Then
'        Me.lstFavorites.Row = 0
'    End If
'End Sub
'
'Private Sub ShowPanel()
''toggle panels from favorites to non-favorites
'    Dim bPanel1 As Boolean
'    bPanel1 = (Me.chkFavorites.Value = vbUnchecked)
'    Me.lblLevel0.Visible = bPanel1
'    Me.lblLevel1.Visible = bPanel1
'    Me.lblLevel2.Visible = bPanel1
'    Me.lblLevel3.Visible = bPanel1
'    Me.cmbLevel0.Visible = bPanel1
'    Me.cmbLevel1.Visible = bPanel1
'    Me.cmbLevel2.Visible = bPanel1
'    Me.cmbLevel3.Visible = bPanel1
'    Me.lblFavorites.Visible = Not bPanel1
'    Me.lstFavorites.Visible = Not bPanel1
'    Me.btnAddToFaves.Visible = bPanel1
'    Me.btnDelete.Visible = Not bPanel1
'    If bPanel1 Then
'        Me.cmbLevel0.SetFocus
'    Else
'        Me.lstFavorites.SetFocus
'    End If
'    DoEvents
'End Sub
'
'Private Sub GetPanel2Defaults()
''gets starting favorite value
'    Dim lFave As Long
'
''   get sticky value from ini
'    On Error Resume Next
'    lFave = GetUserIni(m_xIniSection, "Favorite")
'    On Error GoTo 0
'
'    If lFave Then
''       value supplied - select favorite
'        With Me.lstFavorites
'            .BoundText = lFave
'            If IsNull(.SelectedItem) Then
'                .SelectedItem = 0
'            End If
'        End With
'    Else
''       no default fave- select first item in list
'        Me.lstFavorites.SelectedItem = 0
'    End If
'
'    m_bFaveRetrieved = True
'End Sub
''
''Private Sub chkFavorites_Click()
''    If Not m_bInitializing Then
''        SwitchPanels
''    End If
''End Sub
''
''Private Sub SwitchPanels()
''    If Me.chkFavorites.Value = vbUnchecked And _
''        Not m_bDefRetrieved Then
''        GetPanel1Defaults
''    Else
''        GetPanel2Defaults
''    End If
''    ShowPanel
''End Sub

Private Sub lstVerificationTypes_DblClick()
    btnOK_Click
End Sub
