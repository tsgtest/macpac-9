VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersonsForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'**********************************************************
'   CPersonsForm Class
'   created 1/15/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage the Persons interface

'   THIS CLASS IS PRIVATE
'   Base class for all forms that expose operations
'   related to MacPac people

'   Member of
'   Container for
'**********************************************************

Public Sub ChangeSource(lID As Long, iSource As Integer)
    With g_oDBs.People
        .Item(lID).Source = iSource
        .Save
    End With
End Sub

Public Function AddPerson() As Long
'adds person as manually entered person-
'returns id of person added
    Dim lID As Long
    Dim oNew As mpDB.CPerson
    Dim oDlg As frmPersonDetail
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
'   create new person
    Set oNew = New mpDB.CPerson
    oNew.Office = g_oDBs.Offices.Default
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If
    
    Set oDlg = New frmPersonDetail
    
    With oDlg
'       set new person as person to add
        .Person = oNew
        
'       show dlg
        .Show vbModal
        
'       save if not cancelled
        If Not .Cancelled Then
            Screen.MousePointer = vbHourglass
            With g_oDBs.People
'               add to collection of People
                .Add oDlg.Person
                .Save
                
'               get id to refresh in list
                AddPerson = .Active.ID
            End With
        End If
        Screen.MousePointer = vbDefault
        Unload oDlg
    End With
    
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

End Function

Public Function EditPerson(oPerson As mpDB.CPerson) As Boolean
'allows user to edit specified person - UI element displayed
'returns TRUE if changes to oPerson were saved
    Dim oTemp As mpDB.CPerson
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean

    If oPerson.Source = mpPeopleSourceList_People Then
'       alert and exit - public list people can't be edited
        xMsg = "Only Private Authors may be edited."
        MsgBox xMsg, vbExclamation + vbOKOnly, App.Title
        Exit Function
    End If
    
'   edit supplied person
    Set oTemp = oPerson
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

    With frmPersonDetail
        .Caption = "Edit Author"
        
'       assign person to form
        .Person = oTemp
        
'       show form
        .Show vbModal
        
'       update if not cancelled
        If Not .Cancelled Then
            Screen.MousePointer = vbHourglass
            With .Person
                oTemp.DisplayName = .DisplayName
                oTemp.Email = .Email
                oTemp.Fax = .Fax
                oTemp.FirstName = .FirstName
                oTemp.InformalName = .InformalName
                oTemp.Initials = .Initials
                oTemp.IsAttorney = .IsAttorney
                oTemp.LastName = .LastName
                oTemp.MiddleName = .MiddleName
                oTemp.Office = .Office
                oTemp.Phone = .Phone
                oTemp.Title = .Title
                oTemp.AdmittedIn = .AdmittedIn
            End With
            
'           save
            g_oDBs.People.Save
             
'           return that person was edited
            EditPerson = True
            
            Screen.MousePointer = vbDefault
        End If
        Unload frmPersonDetail
        
        'GLOG : 5594|5381
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    End With
End Function

Function CopyPerson(oPerson As mpDB.CPerson) As mpDB.CPerson
'makes a copy of the specified
'person and adds it to the db
    Dim oTemp As mpDB.CPerson
    Dim i As Integer
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean

    
'   create new person
    Set oTemp = New mpDB.CPerson

'   copy all properties of source person to new person
    With oPerson
'        oTemp.BarID = .BarID
        oTemp.DisplayName = .DisplayName
        oTemp.Email = .Email
        oTemp.Fax = .Fax
        oTemp.FirstName = .FirstName
        oTemp.FullName = .FullName
        oTemp.InformalName = .InformalName
        oTemp.Initials = .Initials
        oTemp.IsAttorney = .IsAttorney
        oTemp.LastName = .LastName
        oTemp.MiddleName = .MiddleName
        oTemp.Office = .Office
        oTemp.Phone = .Phone
        oTemp.Source = mpPeopleSourceList_PeopleInput
        oTemp.Title = .Title
        oTemp.AdmittedIn = .AdmittedIn
        
'       copy licenses
        For i = 1 To .Licenses.Count
            With .Licenses(, i)
                oTemp.Licenses.Add .Description, .Number, .Default
            End With
        Next i
        
'       copy custom fields
        For i = 1 To .CustomProperties.Count
            oTemp.CustomProperties(i).Value = _
                .CustomProperties(i).Value
        Next i
    End With
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If
      
'   allow user to edit the copy
    With frmPersonDetail
        .Caption = "Copy Author to Private Author List"
        .Person = oTemp
        .Show vbModal
    
        If Not .Cancelled Then
'           create new person
            With g_oDBs.People
                .Add oTemp
                .Save
            End With
            Set CopyPerson = oTemp
        End If
    End With
    Unload frmPersonDetail
    
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

End Function

Function DeletePerson(oPerson As mpDB.CPerson) As Boolean
'deletes specified person after prompting the user-
'returns TRUE if the person was deleted
    Dim iChoice As VbMsgBoxResult
    
    xMsg = "Delete " & oPerson.FullName & "?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, "Delete Author")
    
    If iChoice = vbNo Then
        Exit Function
    End If
    
    If oPerson.Source = mpPeopleSourceList_People Then
'       delete from people list
        g_oDBs.People.Delete oPerson.ID
        g_oDBs.People.Refresh
    ElseIf oPerson.Source = mpPeopleSourceList_PeopleInput Then
'       delete from input people list
        g_oDBs.People.Delete oPerson.ID
        g_oDBs.People.Refresh
    End If
        
'   return that person was deleted
    DeletePerson = True

End Function
                                                                            

