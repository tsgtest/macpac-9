VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNumbering"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CNumbering Class
'   created 8/3/00 by Doug Miller-
'   dougmiller@telocity.com

'   Contains properties and methods related
'   to MacPac Numbering 2000
'**********************************************************

Private m_ltHeading As Word.ListTemplate
Private m_bRestoreHeadingScheme As Boolean
Private m_iLinkedLevels As Integer
Private m_oSource As Word.Document
Private m_xSource As String

Public Sub CopyNumberingStyles(xSource As String)
    Dim xScheme As String
    Dim xStyle As String
    Dim i As Integer
    Dim iLevels As Integer
    Dim styScheme As Word.Style
    Dim oDoc As Word.Document
    Dim ltScheme As Word.ListTemplate
    
    On Error GoTo ProcError
    
    Set oDoc = Documents(xSource)
    
    For Each ltScheme In oDoc.ListTemplates
        If ltScheme.Name <> "" Then
            xScheme = xGetProprietaryStyRoot(ltScheme.Name)
            
'           get number of levels
            iLevels = 9
            For i = 1 To 9
                xStyle = xScheme & "_L" & i
                Set styScheme = Nothing
                On Error Resume Next
                Set styScheme = Documents(xSource).Styles(xStyle)
                On Error GoTo ProcError
                If styScheme Is Nothing Then
                    iLevels = i - 1
                    Exit For
                End If
            Next i
            
'           create dummy cont styles in destination
            For i = 1 To iLevels
                xStyle = xScheme & " Cont " & i
                On Error Resume Next
                ActiveDocument.Styles.Add xStyle
                On Error GoTo ProcError
            Next i
            
'           copy cont styles
            For i = 1 To iLevels
                xStyle = xScheme & " Cont " & i
                On Error Resume Next
                Application.OrganizerCopy xSource, _
                                          ActiveDocument, _
                                          xStyle, _
                                          wdOrganizerObjectStyles
                On Error GoTo ProcError
            Next i
            
'           create dummy numbered styles in destination
            For i = 1 To iLevels
                xStyle = xScheme & "_L" & i
                On Error Resume Next
                ActiveDocument.Styles.Add xStyle
                On Error GoTo ProcError
            Next i
            
'           copy numbered styles
            For i = 1 To iLevels
                xStyle = xScheme & "_L" & i
                On Error Resume Next
                Application.OrganizerCopy xSource, _
                                          ActiveDocument, _
                                          xStyle, _
                                          wdOrganizerObjectStyles
                On Error GoTo ProcError
            Next i
        End If
    Next ltScheme
    
    Exit Sub
ProcError:
    RaiseError "MPO.CNumbering.CopyNumberingStyles"
    Exit Sub
End Sub


Public Sub RelinkSchemes()
'resets level-style links
    Dim i As Integer
    Dim iLevels As Integer
    Dim ltP As ListTemplate
    Dim styScheme As Word.Style
    Dim xStyle As String
    Dim xScheme As String
    
    On Error GoTo ProcError
    
    For Each ltP In ActiveDocument.ListTemplates
        If ltP.Name = "" Then GoTo labNextScheme
        
'       skip heading scheme
        If Not m_ltHeading Is Nothing Then
            If (ltP.Name = m_ltHeading.Name) Or _
                    (ltP.ListLevels(1).LinkedStyle = _
                    xTranslateHeadingStyle(1)) Then
                GoTo labNextScheme
            End If
        End If
        
'       get style root
        xScheme = xGetProprietaryStyRoot(ltP.Name)
            
'       get levels
        iLevels = 9
        For i = 1 To 9
            xStyle = xScheme & "_L" & i
            Set styScheme = Nothing
            On Error Resume Next
            Set styScheme = ActiveDocument.Styles(xStyle)
            On Error GoTo ProcError
            If styScheme Is Nothing Then
                iLevels = i - 1
                Exit For
            End If
        Next i
            
'       for each possible level...
        If iLevels <> 0 Then
            For i = 9 To 1 Step -1
                If i > iLevels Then
'                   level does not exist in scheme - unlink
                    ltP.ListLevels(i).LinkedStyle = ""
                Else
'                   link to proprietary style
                    xStyle = xScheme & "_L" & i
                    ltP.ListLevels(i).LinkedStyle = xStyle
                End If
            Next i
        End If
labNextScheme:
    Next ltP
    
    Exit Sub
ProcError:
    RaiseError "MPO.CNumbering.RelinkSchemes"
    Exit Sub
End Sub

Public Sub StoreHeadingScheme()
    Dim i As Integer
    Dim styHeading As Word.Style
    Dim styTemp As Word.Style
    Dim bDo As Boolean
    Dim oEnv As CEnvironment

    On Error GoTo ProcError

'   do only if specified in ini
    On Error Resume Next
    bDo = GetMacPacIni("Numbering", "PreserveNumberedHeadingStyles")
    If Not bDo Then _
        Exit Sub
    On Error GoTo ProcError
    
    Set oEnv = New CEnvironment
    
    oEnv.UnProtectDocument
    
'   get list template attached to Heading 1
    Set m_ltHeading = GetHeadingLT()
    
'   exit if Heading 1 is not linked to MacPac numbering
    If m_ltHeading Is Nothing Then
        oEnv.RestoreProtection
        Exit Sub
    End If
    
    Application.ScreenUpdating = False
        
    m_bRestoreHeadingScheme = True
    Set m_oSource = ActiveDocument
    m_xSource = WordBasic.FileName$()
        
'   get number of linked levels
    m_iLinkedLevels = iLinkedHeadings(m_ltHeading)
    
'   unlink before copying styles
    For i = 9 To 1 Step -1
        m_ltHeading.ListLevels(i).LinkedStyle = ""
    Next i
    
'   copy Heading 1-9 to temp styles
    For i = 1 To 9
        With ActiveDocument.Styles
            On Error Resume Next
            Set styTemp = .Add("zzmpHeading " & i)
            On Error GoTo ProcError
            
            Set styHeading = .Item(xTranslateHeadingStyle(i))
            With styTemp
                .BaseStyle = styHeading.BaseStyle
                .NextParagraphStyle = styHeading.NextParagraphStyle
                .Font = styHeading.Font
                .ParagraphFormat = styHeading.ParagraphFormat
'                .Borders = styHeading.Borders      'remmed per log #2008
            End With
        End With
    Next i
    
    oEnv.RestoreProtection
    
    Set oEnv = Nothing
    Exit Sub
ProcError:
    'ignore invalid password error
    If Err.Number = 599 Then
        Exit Sub
    Else
        RaiseError "MPO.CNumbering.StoreHeadingScheme"
    End If
    Exit Sub
End Sub

Public Sub RestoreHeadingScheme()
    Dim xLT As String
    Dim i As Integer
    Dim styHeading As Word.Style
    Dim styTemp As Word.Style
    Dim ltHeading As Word.ListTemplate
    Dim ltNew As Word.ListTemplate
    Dim xStyleRoot As String
    Dim xStyle As String
    Dim oEnv As CEnvironment
    
    On Error GoTo ProcError
    
    Application.ScreenUpdating = False
    
'   heading 1-9 were not originally linked to MacPac numbering
    If Not m_bRestoreHeadingScheme Then _
        Exit Sub
        
    Set oEnv = New CEnvironment
    oEnv.UnProtectDocument

'   if a new doc has been created,
'   original list template may not be present
    If Not bListTemplateExists(m_ltHeading.Name) Then
        Set ltNew = ActiveDocument.ListTemplates.Add(True)
        ltNew.Name = m_ltHeading.Name
        
'       copy attributes of original list template
        For i = 1 To 9
            iCopyListLevel m_ltHeading.ListLevels(i), _
                            ltNew.ListLevels(i), True
        Next i
    Else
        Set ltNew = ActiveDocument _
            .ListTemplates(m_ltHeading.Name)
    End If
        
'   get new list template attached to Heading 1
    Set ltHeading = GetHeadingLT()
    
'   unlink before copying styles
    If Not ltHeading Is Nothing Then
        For i = 9 To 1 Step -1
            ltHeading.ListLevels(i).LinkedStyle = ""
        Next i
    End If
    
'   copy temp styles to Heading 1-9
    For i = 1 To 9
        On Error Resume Next
        Set styTemp = m_oSource.Styles("zzmpHeading " & i)
        On Error GoTo ProcError
        
        If styTemp Is Nothing Then _
            GoTo labNextLevel
            
        Set styHeading = ActiveDocument _
            .Styles(xTranslateHeadingStyle(i))
        With styHeading
            .BaseStyle = styTemp.BaseStyle
            
'           next para style may not be in destination doc
            On Error Resume Next
            .NextParagraphStyle = styTemp.NextParagraphStyle
            On Error GoTo ProcError

            .Font = styTemp.Font
            .ParagraphFormat = styTemp.ParagraphFormat
'            .Borders = styTemp.Borders         'remmed per log #2008
        End With
        
'       delete temp style
        Application.OrganizerDelete m_xSource, _
            styTemp.NameLocal, wdOrganizerObjectStyles
            
        Set styTemp = Nothing
labNextLevel:
    Next i
    
'   relink to original list template
    For i = m_iLinkedLevels To 1 Step -1
        xStyle = xTranslateHeadingStyle(i)
        With ltNew.ListLevels(i)
            .LinkedStyle = xStyle
'           force font of numbers to match font of reset style
            With .Font
                .Name = ActiveDocument.Styles(xStyle).Font.Name
                .Size = ActiveDocument.Styles(xStyle).Font.Size
            End With
        End With
    Next i
    
'   new doc has been created
    If m_oSource <> ActiveDocument Then
'       relink scheme in original document
        For i = m_iLinkedLevels To 1 Step -1
            m_ltHeading.ListLevels(i).LinkedStyle = _
                m_oSource.Styles(xTranslateHeadingStyle(i)) _
                .NameLocal
        Next i
        
'       copy proprietary styles to new doc
        On Error Resume Next
        xStyleRoot = xGetProprietaryStyRoot(m_ltHeading.Name)
        For i = 9 To 1 Step -1
'           copy cont style (if it exists)
            Application.OrganizerCopy m_xSource, _
                                        WordBasic.FileName$(), _
                                        xStyleRoot & " Cont " & i, _
                                        wdOrganizerObjectStyles
'           copy numbered para style (if it exists)
            Application.OrganizerCopy m_xSource, _
                                        WordBasic.FileName$(), _
                                        xStyleRoot & "_L" & i, _
                                        wdOrganizerObjectStyles
        Next i
    End If
    
    oEnv.RestoreProtection
    Set oEnv = Nothing
    
    Exit Sub
ProcError:
    RaiseError "MPO.CNumbering.RestoreHeadingScheme"
    Exit Sub
End Sub

Public Function GetHeadingLT() As Word.ListTemplate
'returns the list template that uses Word Heading styles-
'else returns EMPTY
    Dim ltP As Word.ListTemplate
    Dim styHeading As Word.Style
    
    On Error Resume Next
    Set styHeading = Word.ActiveDocument.Styles(wdStyleHeading1)
    If styHeading Is Nothing Then _
        Exit Function
        
'   get list template linked to Heading 1 style
    With styHeading
'       test for InUse so as not to make style InUse
        If .InUse Then
'           get attached list template
            Set GetHeadingLT = .ListTemplate
        End If
    End With
End Function

Private Function iLinkedHeadings(ltHeading As Word.ListTemplate) As Integer
'returns number of heading levels that are linked to ltHeading
    Dim i As Integer
    
    For i = 1 To 9
        If ltHeading.ListLevels(i).LinkedStyle <> _
                xTranslateHeadingStyle(i) Then
            Exit For
        End If
    Next i
    iLinkedHeadings = i - 1
End Function

Private Function xTranslateHeadingStyle(iLevel As Integer) As String
'gets local name of iLevel Word Heading style
    With ActiveDocument
        Select Case iLevel
            Case 1
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading1).NameLocal
            Case 2
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading2).NameLocal
            Case 3
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading3).NameLocal
            Case 4
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading4).NameLocal
            Case 5
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading5).NameLocal
            Case 6
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading6).NameLocal
            Case 7
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading7).NameLocal
            Case 8
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading8).NameLocal
            Case 9
                xTranslateHeadingStyle = _
                    .Styles(wdStyleHeading9).NameLocal
        End Select
    End With
End Function

Private Function bListTemplateExists(xScheme As String) As Boolean
'returns TRUE if the doc contains a list template named xScheme
    Dim ltScheme As Word.ListTemplate
    Dim xLT As String
    
    For Each ltScheme In ActiveDocument.ListTemplates
        xLT = ltScheme.Name
        If xLT <> "" Then
            If xLT = xScheme Then
                bListTemplateExists = True
                Exit Function
            End If
        End If
    Next ltScheme
End Function

Private Function iCopyListLevel(llSource As ListLevel, _
                                llDest As ListLevel, _
                                Optional bCopyAllProps As Boolean = False) _
                                As Integer
    Dim fontSource As Word.Font
    
    With llDest
        ' Need to clear out format so that existing bullet style
        ' number will not cause error if format contains a level specifier
        .NumberFormat = ""
        .NumberStyle = llSource.NumberStyle
        .NumberFormat = llSource.NumberFormat
        .TrailingCharacter = llSource.TrailingCharacter
        .TextPosition = llSource.TextPosition
        .NumberPosition = llSource.NumberPosition
        .TabPosition = llSource.TabPosition
        .ResetOnHigher = llSource.ResetOnHigher
        .StartAt = llSource.StartAt
        .Alignment = llSource.Alignment
        
        Set fontSource = llSource.Font
        With .Font
            .Bold = fontSource.Bold
            .Italic = fontSource.Italic
'           the following conditional is necessary to
'           prevent unintended toggle
            If .AllCaps <> fontSource.AllCaps Then _
                .AllCaps = fontSource.AllCaps
            .Underline = fontSource.Underline
            .ColorIndex = fontSource.ColorIndex
            .Name = fontSource.Name

'           these aren't offered in MacPac customization,
'           so can be skipped to increase speed; reset should
'           call, since user may have changed using Word
            If bCopyAllProps Then
                .Strikethrough = fontSource.Strikethrough
                .Subscript = fontSource.Subscript
                .Superscript = fontSource.Superscript
                .Shadow = fontSource.Shadow
                .Outline = fontSource.Outline
                .Emboss = fontSource.Emboss
                .Engrave = fontSource.Engrave
                .Hidden = fontSource.Hidden
                .DoubleStrikeThrough = fontSource.DoubleStrikeThrough
                .Size = fontSource.Size
                .Animation = fontSource.Animation
            End If
        End With
    End With
End Function

Private Function xGetProprietaryStyRoot(xLT As String) As String
    Dim iPos As Integer
    Dim xRoot As String
    
'   trim properties from end
    iPos = InStr(xLT, "|")
    If iPos = 0 Then
        xRoot = xLT
    Else
        xRoot = Left(xLT, iPos - 1)
    End If
    
'   trim prefix from start
    If Left(xRoot, 4) = "zzmp" Then
        xRoot = Mid(xRoot, 5)
    ElseIf xRoot = "HeadingStyles" Then
        xRoot = "Heading"
    End If

    xGetProprietaryStyRoot = xRoot
End Function

Public Sub RenameLTsFromVars()
'   cycles through unnamed list templates; if level one is attached
'   to "_L1" style, uses backup props to rename it
    Dim xVar As String
    Dim ltP As Word.ListTemplate
    Dim xStyL1 As String
    Dim xProps As String
    
    On Error GoTo ProcError
    
'   cycle through list templates
    For Each ltP In Word.ActiveDocument.ListTemplates
        If ltP.Name = "" Then
'           get name of level 1 linked style
            xStyL1 = ltP.ListLevels(1).LinkedStyle
'           check for correct MacPac form (ie XXX_LY)
            If Len(xStyL1) > 3 Then
                If Right(xStyL1, 3) = "_L1" Then
'                   style name has MacPac form;
'                   look for variable containing backup prop
                    xProps = ""
                    xVar = "zzmp" & Left(xStyL1, Len(xStyL1) - 3)
                    
                    On Error Resume Next
                    xProps = ActiveDocument.Variables(xVar).Value
                    On Error GoTo ProcError
                    
                    If xProps <> "" Then
'                       strip 9.7 flag'*c
                        xProps = xTrimTrailingChrs(xProps, "*") '*c
                        
'                       rename list template
                        ltP.Name = xVar & xProps
                    End If
                End If
            End If
        End If
    Next ltP
    
    Exit Sub
ProcError:
    RaiseError "MPO.CNumbering.RenameLTsFromVars"
    Exit Sub
End Sub

Public Sub BackupLTNames(oSource As Word.Document, oDest As Word.Document)
'backs up names of list template in oSource to variables in oDest;
'variables are used to rename list templates after they are
'deleted by native Word functions, e.g. autotext insertion
    Dim xVar As String
    Dim ltP As Word.ListTemplate
    Dim xValue As String
    Dim iPos As Integer
    
    On Error GoTo ProcError
    
    For Each ltP In oSource.ListTemplates
        If ltP.Name <> "" Then
            iPos = InStr(ltP.Name, "|")
            If iPos <> 0 Then
                xVar = Left(ltP.Name, iPos - 1)
                xValue = Mid(ltP.Name, iPos)
                oDest.Variables(xVar) = xValue
            End If
        End If
    Next ltP
    
    Exit Sub
ProcError:
    RaiseError "MPO.CNumbering.BackupLTNames"
    Exit Sub
End Sub

