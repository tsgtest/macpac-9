VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CCustomProperties Collection Class
'   created 10/22/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of CCustomProperties

'   Member of App
'   Container for CCustomProperty
'**********************************************************

'**********************************************************
Private m_oCol As Collection
Private m_xCategory As String
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Category(xNew As String)
    m_xCategory = xNew
    RefreshCollection
End Property

Public Property Get Category() As String
    Category = m_xCategory
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Add(xName As String, _
                    Optional ByVal xBookmark As String, _
                    Optional ByVal xLinkedProperty As String, _
                    Optional ByVal xDelimiterReplacement As String, _
                    Optional ByVal bWholePara As Boolean, _
                    Optional ByVal iType As mpCustomPropertyTypes = mpCustomPropertyType_Document, _
                    Optional ByVal xMacro As String, _
                    Optional ByVal bIndexed As Boolean, _
                    Optional ByVal lAction As Long, _
                    Optional ByVal iUnderlineLength) As MPO.CCustomProperty
'adds a prop to the collection
    Dim oNew As CCustomProperty
    
    On Error GoTo ProcError
    Set oNew = New CCustomProperty
    m_oCol.Add oNew, xName
    With oNew
        .Name = xName
        .Bookmark = xBookmark
        .DelimiterReplacement = xDelimiterReplacement
        .LinkedProperty = xLinkedProperty
        .WholeParagraph = bWholePara
        .PropertyType = iType
        .Macro = xMacro
        .Indexed = bIndexed
        .Action = lAction
        .UnderlineLength = iUnderlineLength
    End With
    Set Add = oNew
    Exit Function
ProcError:
    RaiseError "MPO.CCustomProperties.Add"
    Exit Function
End Function

Public Property Get Count() As Integer
    Count = m_oCol.Count
End Property

Public Function Item(vID As Variant) As MPO.CCustomProperty
Attribute Item.VB_UserMemId = 0
    Dim oProp As CCustomProperty
    Dim iPos As Integer
    Dim xID As String
    Dim xIndices() As String
    Dim xName As String
    Dim xBookmark As String
    
    On Error Resume Next
    Set Item = m_oCol.Item(vID)
    If Item Is Nothing Then
        iPos = InStr(vID, "_")
        '---check for index separator
        If iPos Then
            Set oProp = m_oCol.Item(Left(vID, iPos - 1))
            If Not oProp Is Nothing Then
                Err.Clear
                '---if indexed = true, return custom prop
                If oProp.Indexed = True Then
                    '---reset bookmark & name props w/ ID appended
                    xID = Right(vID, Len(vID) - iPos + 1)
                    xIndices = Split(xID, "_")
                    xID = "_" & xIndices(1)
                    '---9.5.0
                    ' For BSig compatibility - include Parent ID and item ID
                    If UBound(xIndices()) > 1 Then
                        xID = xID & "_" & xIndices(2)
                    End If
                    '---End 9.5.0
                    xName = Left(vID, iPos - 1)
                    If InStr(oProp.Bookmark, "_") > 0 Then
                        xBookmark = Left(oProp.Bookmark, InStr(oProp.Bookmark, "_") - 1)
                    Else
                        xBookmark = oProp.Bookmark
                    End If
                    If oProp.Name <> vID Then
                        oProp.Name = xName & xID
                        oProp.Bookmark = xBookmark & xID
                    End If
                    Set Item = oProp
                    '---indexed item is invalid
                    If Err.Number Then
                        Err.Raise mpError.mpError_ItemDoesNotExist, , _
                            "Item method failed.  Check Property Name and Bookmark Name in mpPublic.tblCustomProperties."
                    End If
                End If
            Else
                '---indexed item is invalid
                If Err.Number Then
                    Err.Raise mpError.mpError_ItemDoesNotExist, , _
                        "Custom Property " & vID & " Does Not Exist.  Check Property Name in mpPublic.tblCustomProperties."
                End If
            End If
        Else
            '---item was invalid all along
            If Err.Number Then
                Err.Raise mpError.mpError_ItemDoesNotExist, , _
                    "Custom Property " & vID & " Does Not Exist.  Check Property Name in mpPublic.tblCustomProperties."
            End If
        End If
    End If
    Exit Function
ProcError:
    RaiseError "MPO.CCustomProperties.Item"
    Exit Function
End Function

'Private Function ItemOLD(vID As Variant) As CCustomProperty
'    Dim oProp As CCustomProperty
'    Dim iPos As Integer
'    Dim xID As String
'    On Error Resume Next
'    Set Item = m_oCol.Item(vID)
'    If Item Is Nothing Then
'        iPos = InStr(vID, "_")
'        '---check for index separator
'        If iPos Then
'            On Error GoTo ProcError
'            Set oProp = m_oCol.Item(Left(vID, iPos - 1))
'            If Not oProp Is Nothing Then
'                '---if indexed = true, return custom prop
'                If oProp.Indexed = True Then
'                    '---reset bookmark & name props w/ ID appended
'                    xID = Right(vID, Len(vID) - iPos + 1)
'                    If oProp.Name <> vID Then
'                        oProp.Name = oProp.Name & xID
'                        oProp.Bookmark = oProp.Bookmark & xID
'                    End If
'                    Set Item = oProp
'                End If
'            End If
'        Else
'            '---item was invalid all along
'            If Err Then GoTo ProcError
'        End If
'    End If
'    Exit Function
'ProcError:
'    RaiseError "MPO.CCustomProperties.Item"
'    Exit Function
'End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_oCol.[_NewEnum]
End Property

Public Sub UpdateForAuthor(oAuthor As mpDB.CPerson, _
                           Optional oAuthorOptions As mpDB.CPersonOptions)
'updates all the custom properties
'that are author detail or author options
    Dim oProp As MPO.CCustomProperty
    Dim bAuthorProp As Boolean
    Dim lErr As Long
    Dim xDesc As String
    
    On Error GoTo ProcError
    
'   cycle through properties
    For Each oProp In Me
        With oProp
            On Error Resume Next
            Select Case .PropertyType
                Case mpCustomPropertyType_Author
'                   get value for author detail linked property
                    bAuthorProp = True
                    Err.Clear
                    On Error Resume Next
'                   an error will be generated if the linked
'                   property is a custom author property
                    .Text = CallByName(oAuthor, .LinkedProperty, VbGet)
                    If Err.Number And (Err.Number <> 94) Then
'                       try custom people properties
                        .Text = oAuthor.CustomProperties.Item(.LinkedProperty)
                        If Err.Number Then
                            On Error GoTo ProcError
                            xDesc = "There is no author property called '" & _
                                .LinkedProperty & "'.  Please check " & _
                                "tblCustomProperties in mpPublic.mdb."
                            Err.Raise mpError_InvalidLinkedProperty
                        Else
                            On Error GoTo ProcError
                        End If
                    End If
                Case mpCustomPropertyType_AuthorPreference
'                   get value for author option linked property
                    On Error Resume Next
                    .Text = xNullToString(CallByName( _
                        oAuthorOptions(.LinkedProperty), "Value", VbGet))
                    If Err.Number Then
                        On Error GoTo ProcError
                        xDesc = "There is no author preference property called '" & _
                            .LinkedProperty & "'.  Please check " & _
                            "the appropriate Author Preference table in mpPublic.mdb."
                        Err.Raise mpError_InvalidLinkedProperty
                    Else
                        On Error GoTo ProcError
                    End If
                Case mpCustomPropertyType_AuthorOffice
                    On Error Resume Next
'                   9.4.1 fix
                    If .LinkedProperty = "Address" Then
'                        .Text = CallByName(oAuthor.Office, .LinkedProperty, VbMethod, Chr(11))
                        .Text = oAuthor.Office.Address(g_oDBs.OfficeAddressFormats.Item(2))
                    Else
'                       get value for author office linked property
                        .Text = CallByName(oAuthor.Office, .LinkedProperty, VbGet)
                    End If
                    
                    If Err.Number Then
                        On Error GoTo ProcError
                        xDesc = "There is no author office property called '" & _
                            .LinkedProperty & "'.  Please check " & _
                            "tblCustomProperties in mpPublic.mdb."
                        Err.Raise mpError_InvalidLinkedProperty
                    Else
                        On Error GoTo ProcError
                    End If
            End Select
        End With
    Next oProp
    Exit Sub
    
ProcError:
    RaiseError "MPO.CCustomProperties.UpdateForAuthor", xDesc
    Exit Sub
End Sub

Public Sub RefreshValues()
'   refresh values of all custom properties
    Dim oCustProp As MPO.CCustomProperty
    Dim i As Integer
    Dim xName As String

    For Each oCustProp In Me
        With oCustProp
            If Not .Indexed Then
                .Text = .Text
            Else
                If Len(.Text) <> 0 And Len(.Value) <> 0 Then
                    .Text = .Text
                End If
            End If
        End With
    Next
End Sub

Public Sub RefreshEmptyValues()
'   refresh values of empty custom properties
    Dim oCustProp As MPO.CCustomProperty
    For Each oCustProp In Me
        With oCustProp
            If Not .Indexed Then
                If Len(.Text) = 0 And Len(.Value) = 0 Then
                    .Text = Empty
                End If
            End If
        End With
    Next
End Sub

Public Sub RefreshValue(oProp As CCustomProperty)
'   refresh values of all custom properties
    With oProp
        .Name = .Name
        .Text = .Text
    End With
End Sub

Public Sub RefreshIndexedValues(lID As Long, Optional lParentID As Long)
    Dim i As Integer
    Dim xName As String
    
    For i = 1 To Me.Count
        If Me(i).Indexed = True Then
            xName = Me(i).Name
            If lParentID > 0 Then
                If Right(xName, Len("_" & lParentID & "_" & lID)) = "_" & lParentID & "_" & lID Then
                    Me.RefreshValue Me(xName)
                End If
            Else
                If lID = Mid(xName, InStr(xName, "_" & lID) + 1) Then
                        Me.RefreshValue Me(xName)
                End If
            End If
        End If
    Next
End Sub

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_oCol = Nothing
End Sub

Private Sub RefreshCollection()
'fill collection with items in CCustomPropertyDefs collection
    Dim oCustPropDefs As mpDB.CCustomPropertyDefs
    Dim i As Integer
    Dim j As Integer

    On Error Resume Next
    Set oCustPropDefs = g_oDBs.CustomProperties(Me.Category)
    On Error GoTo 0
    If oCustPropDefs Is Nothing Then
        Exit Sub
    End If

    With oCustPropDefs
'       cycle through each, adding as we go
        For i = 1 To .Count
            With .Item(i)
'               add new
                Me.Add .Name, _
                    .Bookmark, _
                    .LinkedProperty, _
                    .DelimiterReplacement, _
                    .WholeParagraph, _
                    .PropertyType, _
                    .Macro, _
                    .Indexed, .Action, .UnderlineLength

            '---add indexed version of props to collection if .index = true
    '---basically we're creating ten custom prop clones for each indexed base prop as
    '---a buffer.  No doc vars will be written if items are empty when refresh called
                If .Indexed = True Then
                    For j = 1 To g_iMaxIndexProperties
                        Me.Add .Name & "_" & j, _
                            .Bookmark & "_" & j, _
                            .LinkedProperty, _
                            .DelimiterReplacement, _
                            .WholeParagraph, _
                            .PropertyType, _
                            .Macro, _
                            .Indexed, .Action, .UnderlineLength
                    Next
                End If

            End With


        Next i
    End With
End Sub


