VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplates Collection Class
'   created 11/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CTemplates, the set of Templates
'   that are supported by MacPac - collection is an XArray

'   Member of
'   Container
'**********************************************************

'**********************************************************
Private m_oCol As VBA.Collection
Private m_oTemplate As CTemplate
Private m_iGroup As Integer
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Group(iNew As Integer)
    m_iGroup = iNew
    Me.Refresh
End Property

Public Property Get Group() As Integer
    Group = m_iGroup
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Count() As Long
    Count = m_oCol.Count
End Function

Public Function Item(ByVal vIndex As Variant) As MPO.CTemplate
Attribute Item.VB_Description = "Returns theCTemplate item with ID = vID or Index = lIndex"
Attribute Item.VB_UserMemId = 0
    On Error Resume Next
    Set Item = m_oCol.Item(vIndex)
    Exit Function
End Function

Public Function Exists(ByVal xTemplate As String) As Boolean
'returns TRUE if xTemplate is the name of a member
'of the templates collection
    On Error Resume Next
    Exists = Not (Item(xTemplate) Is Nothing)
End Function

'Public Sub Refresh()
'    Dim i As Integer
'    Dim oTemplate As MPO.CTemplate
'    Dim oDef As mpDB.CTemplateDef
'    Dim oExisting As MPO.CTemplate
'
'    #If compHandleErrors Then
'        On Error GoTo ProcError
'    #End If
'
'    Set m_oCol = New VBA.Collection
'
'    With g_oDBs.TemplateDefinitions
'        If Me.Group Then
''           get all templates in group
'            .Group = Me.Group
'        Else
''           get all templates
'            .Group = Empty
'        End If
'
'        For i = 1 To .Count
''           test if the template has already been
''           added from another template group
'            On Error Resume Next
'            Set oExisting = Nothing
'            Set oExisting = m_oCol.Item(.Item(i).FileName)
'            On Error GoTo ProcError
'
''           skip if already added
'            If (oExisting Is Nothing) Then
'                Set oDef = .Item(i)
'
''               create new CTemplate
'                Set oTemplate = New CTemplate
'
''               update properties with properties of definition
'                With oTemplate
'                    .BoilerplateFile = oDef.BoilerplateFile
'                    .FileName = oDef.FileName
'                    .Group = .Group
'                    .ID = oDef.FileName
'                    .ShortName = oDef.ShortName
'                    .TemplateType = oDef.TemplateType
'                    .Description = oDef.Description
'                    .OptionsTable = oDef.OptionsTable
'                    .ClassName = oDef.ClassName
'                    .Macro = oDef.Macro
'                    .DefaultDocIDStamp = oDef.DefaultDocIDStamp
'                End With
'
''               add to collection
'                m_oCol.Add oTemplate, oTemplate.ID
'            End If
'        Next i
'    End With
'    Exit Sub
'ProcError:
'    RaiseError "MPO.CTemplates.Refresh"
'    Exit Sub
'End Sub

Public Sub Refresh()
    Dim i As Integer
    Dim oTemplate As MPO.CTemplate
    Dim oDef As mpDB.CTemplateDef
    Dim oExisting As MPO.CTemplate
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If

    Set m_oCol = New VBA.Collection
    
    With g_oDBs.TemplateDefinitions
        If Me.Group Then
'           get all templates in group
            .Group = Me.Group
        Else
'           get all templates
            .Group = Empty
        End If

        For i = 1 To .Count
'           test if the template has already been
'           added from another template group
            On Error Resume Next
            Set oExisting = Nothing
            Set oExisting = m_oCol.Item(.Item(i).FileName)
            On Error GoTo ProcError
            
'           skip if already added
            If (oExisting Is Nothing) Then
                Set oDef = .Item(i)
            
'               create new CTemplate
                Set oTemplate = New CTemplate
            
'               update properties with properties of definition
                With oTemplate
                    .BoilerplateFile = oDef.BoilerplateFile
                    .FileName = oDef.FileName
                    .Group = .Group
                    .ID = oDef.FileName
                    .ShortName = oDef.ShortName
                    .TemplateType = oDef.TemplateType
                    .Description = oDef.Description
                    .ProtectedSections = oDef.ProtectedSections
                    .OptionsTable = oDef.OptionsTable
                    .ClassName = oDef.ClassName
                    .Macro = oDef.Macro
                    .DefaultDocIDStamp = oDef.DefaultDocIDStamp
                    .WriteToBP = oDef.WriteToBP
                    .AuthorDefaultsCategory = oDef.BaseTemplate
                    .MappedVariables = oDef.MappedVariables
                    .EditMacro = oDef.EditMacro
                    .SubFolder = oDef.SubFolder
                End With
            
'               add to collection
                m_oCol.Add oTemplate, oTemplate.ID
            End If
        Next i
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CTemplates.Refresh"
    Exit Sub
End Sub
'9.7.2010 - #4728
Function ItemFromClass(xClass As String) As MPO.CTemplate
'returns template whose associated class is xClass
    Dim oTemplate As MPO.CTemplate
    Dim iPos As Integer
    Dim bIsPreOffice12 As Boolean
    
    On Error GoTo ProcError
    
    'if attached template is not an office 12 template,
    'then we need to return a non-office 12 template object
    bIsPreOffice12 = UCase(Word.ActiveDocument.AttachedTemplate) Like "*.DOT"
    
    For Each oTemplate In m_oCol
        If (UCase(oTemplate.ClassName) = UCase(xClass)) Then
            iPos = InStr(oTemplate.FileName, ".dot")
            If Not bIsPreOffice12 Then
                If Len(Mid(oTemplate.FileName, iPos, Len(oTemplate.FileName))) = 5 Then
                    Set ItemFromClass = oTemplate
                    Exit For
                End If
            Else
                If Len(Mid(oTemplate.FileName, iPos, Len(oTemplate.FileName))) = 4 Then
                    Set ItemFromClass = oTemplate
                    Exit For
                End If
            End If
        End If
    Next oTemplate
    Exit Function
ProcError:
    RaiseError "MPO.CTemplates.ItemFromClass"
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oTemplate = New CTemplate
    Refresh
End Sub

Private Sub Class_Terminate()
    Set m_oTemplate = Nothing
    Set m_oCol = Nothing
End Sub
