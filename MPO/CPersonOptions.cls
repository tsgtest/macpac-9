VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersonOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   PersonOptions Collection Class
'   created 12/23/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of PersonOptions

'   Member of CPerson, COffice
'   Container for CPersonOption
'**********************************************************

'**********************************************************
Private Enum mpPersonOptionSources
    mpPersonOptionSources_None = 0
    mpPersonOptionSources_Personal = 1
    mpPersonOptionSources_Office = 2
    mpPersonOptionSources_Firm = 3
End Enum

Private m_colPersonOptions As Collection
Private m_colPersonOptionsForm As Collection
Private m_PersonOption As mpDB.CPersonOption
Private m_Owner As mpDB.CPerson
Private m_iSource As Integer
Private m_objParent As Object
Private m_bIsDirty As Boolean
Private m_xTemplate As String
'**********************************************************

'************************************************************************
'Properties
'************************************************************************
Public Property Let IsDirty(bNew As Boolean)
    m_bIsDirty = bNew
End Property

Public Property Get IsDirty() As Boolean
    IsDirty = m_bIsDirty
End Property

Public Property Let Template(xNew As String)
    m_xTemplate = xNew
End Property

Public Property Get Template() As String
    Template = m_xTemplate
End Property

Public Property Let Parent(objNew As Object)
    Set m_objParent = objNew
End Property

Public Property Get Parent() As Object
    Set Parent = m_objParent
End Property

Public Property Let Source(iNew As Integer)
    m_iSource = iNew
End Property

Public Property Get Source() As Integer
    Source = m_iSource
End Property

Public Property Let Owner(psnNew As mpDB.CPerson)
    Set m_Owner = psnNew
End Property

Public Property Get Owner() As mpDB.CPerson
    Set Owner = m_Owner
End Property

Public Property Get Exists() As Boolean
    Exists = HasPrivateOptions
End Property
'************************************************************************
' Methods
'************************************************************************
Public Function Active() As mpDB.CPersonOption
    Set Active = m_PersonOption
End Function

'Friend Sub Add(offNew As mpDB.CPersonOption)
''   not exposed to client dlls
'    m_colPersonOptions.Add offNew, offNew.Name
'End Sub
'
'Friend Sub Delete(vID As Variant)
''   not exposed to client dlls
''    m_colPersonOptions.Remove vID
'End Sub
'

Public Sub Save(Optional lID As Long = -9999, Optional xTemplate As String, Optional frmP As Object)

' saves options to db
    Dim i As Integer
    Dim xSQL As String
    Dim rs As DAO.Recordset
    Dim fldP As DAO.Field
    Dim opiP As mpDB.CPersonOption
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If Me.IsDirty Then
'       if record exists, update record
'       else create record, and update new record
'       check for existence in private db
        If lID = -9999 Then
            lID = Me.Parent.ID
        End If
        If xTemplate = "" Then
            xTemplate = Me.Template
        End If
        
'       err if option set is missing necessary id info
        If lID = 0 Or xTemplate = "" Then
            Err.Raise mpError_FailedSavingOptionSet_IDsMissing
        End If
    
        xSQL = "SELECT  * FROM tblUserOptions" & xTemplate & _
                      " WHERE fldID = " & lID

'       open recordset
        Set rs = g_App.PrivateDB.OpenRecordset(xSQL, _
                                          dbOpenDynaset)
        
'       add/edit as necessary
        With rs
            If Not (.BOF And .EOF) Then
                .Edit
            Else
                .AddNew
                !fldID = lID
            End If
            
'           cycle through fields in collection, updating
'           to value of corresponding collection member
            For Each fldP In .Fields
                If fldP.Name <> "fldID" Then
'                    Set opiP = m_colPersonOptions.Item(fldP.Name)
                    Set opiP = m_colPersonOptionsForm.Item(fldP.Name)
                    If opiP.Value = "" Then
                        'fldP.Value = IsNull(opiP.Value)
                    Else
                        fldP.Value = opiP.Value
                    End If
                End If
            Next fldP
            
            .Update
        End With
        Me.IsDirty = False
    End If
    Exit Sub
    
ProcError:
    Err.Raise Err.Number, "mpDB.CPersonOptions.Save"
    Exit Sub
End Sub

Public Sub Delete(lID As Long, xTemplate As String)

' saves options to db
    Dim i As Integer
    Dim xSQL As String
    Dim rs As DAO.Recordset
    Dim fldP As DAO.Field
    Dim opiP As mpDB.CPersonOption
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'       if record exists, update record
'       else create record, and update new record
'       check for existence in private db
        If lID = -9999 Then
            lID = Me.Parent.ID
        End If
        If xTemplate = "" Then
            xTemplate = Me.Template
        End If
        
'       err if option set is missing necessary id info
        If lID = 0 Or xTemplate = "" Then
            Err.Raise mpError_FailedSavingOptionSet_IDsMissing
        End If
    
        xSQL = "SELECT  * FROM tblUserOptions" & xTemplate & _
                      " WHERE fldID = " & lID

'       open recordset
        Set rs = g_App.PrivateDB.OpenRecordset(xSQL, _
                                          dbOpenDynaset)
        
        rs.MoveFirst
        rs.Delete
        Exit Sub
ProcError:
    Err.Raise Err.Number, "mpDB.CPersonOptions.Save"
    Exit Sub
End Sub

Public Function Count() As Long
    Count = m_colPersonOptions.Count
End Function

Public Function Item(vID As Variant) As mpDB.CPersonOption
Attribute Item.VB_UserMemId = 0
    Set Item = m_colPersonOptions.Item(vID)
End Function

Public Sub Refresh(Optional DoXArray As Boolean = True, Optional frmP As Object)
'opens the appropriate recordset as the collection
'source and fills an xarray
    Dim xSQL As String
    Dim rs As DAO.Recordset
    Dim fldP As DAO.Field
    Dim opiTemp As mpDB.CPersonOption
    Dim lID As Long
    
    Set m_colPersonOptions = New Collection
    Set m_colPersonOptionsForm = New Collection
    
    If Me.Parent.ID = Empty Then
        lID = 0
    Else
        lID = Me.Parent.ID
    End If
        
    xSQL = "SELECT  * FROM tblUserOptions" & Me.Template & _
                  " WHERE fldID = " & lID
    
'   open recordset
    Set rs = g_App.PrivateDB.OpenRecordset(xSQL, _
                                      dbOpenSnapshot, dbReadOnly)
                                      
    Me.Source = mpPersonOptionSources_Personal
    
    If rs.RecordCount = 0 Then
        rs.Close
'       attempt to get default office options
        xSQL = "SELECT  * FROM tblUserOptions" & Me.Template & _
                      " WHERE fldID = -" & GetIni("Firm", "Default Office", App.Path & "\" & mpFirmINI)
        
'       open recordset
        Set rs = g_App.PublicDB.OpenRecordset(xSQL, _
                                          dbOpenSnapshot, dbReadOnly)
    
        Me.Source = mpPersonOptionSources_Office
    
        If rs.RecordCount = 0 Then
            rs.Close
            
'           get firm options
            xSQL = "SELECT  * FROM tblUserOptions" & Me.Template & _
                          " WHERE fldID = " & mpOptions_FirmDefaultID
            
'           open recordset
            Set rs = g_App.PublicDB.OpenRecordset(xSQL, _
                                              dbOpenSnapshot, dbReadOnly)
                                              
            Me.Source = mpPersonOptionSources_Personal
        End If
        
        If rs.RecordCount = 0 Then
            rs.Close
            
'           something is dreadfully wrong
            Err.Raise mpError_NoFirmOptionsRecord, _
                              "mpDB.OptionsSetItems.Refresh"
        End If
    End If

'   cycle through fields, adding to collection
    For Each fldP In rs.Fields
        Set opiTemp = New mpDB.CPersonOption
            With opiTemp
            .Name = fldP.Name
            .Value = fldP.Value
            End With
        m_colPersonOptions.Add opiTemp, opiTemp.Name
        
        Set opiTemp = New mpDB.CPersonOption
        
        Dim i As Integer
        Dim ctlCtl As Control
        If Not frmP Is Nothing Then
            On Error Resume Next
            For Each ctlCtl In frmP.Controls
                If ctlCtl.Name = fldP.Name Then
                    If TypeOf ctlCtl Is VB.TextBox Then
                        opiTemp.Name = fldP.Name
                        opiTemp.Value = ctlCtl.Text
                    ElseIf TypeOf ctlCtl Is VB.CheckBox Then
                        opiTemp.Name = fldP.Name
                        opiTemp.Value = ctlCtl.Value
                    ElseIf InStr(ctlCtl.Name, "spn") Then
                        opiTemp.Name = fldP.Name
                        opiTemp.Value = ctlCtl.Value
                    ElseIf ctlCtl.DataMode = 4 Then     'it's  a tdbcombo
                        If InStr(fldP.Name, "spn") = 0 Then
                            opiTemp.Name = fldP.Name
                            opiTemp.Value = ctlCtl.BoundText
                        End If
                    End If
                    
                    m_colPersonOptionsForm.Add opiTemp, opiTemp.Name
                    Exit For
                End If
            Next
        End If
    
    Next fldP
    rs.Close

End Sub
    
Private Function HasPrivateOptions() As Boolean
'returns TRUE if person has a private
'options record for the specified template
    Dim xSQL As String
    Dim rs As DAO.Recordset
    
    xSQL = "SELECT  * FROM tblUserOptions" & Me.Template & _
                  " WHERE fldID = " & Me.Parent.ID
    
'   open recordset
    Set rs = g_App.PrivateDB.OpenRecordset(xSQL, _
                        dbOpenSnapshot, dbReadOnly)
                                      
    If rs.RecordCount > 0 Then
        HasPrivateOptions = True
    End If
        
    rs.Close
End Function

'************************************************************************
'  Internal Procedures
'************************************************************************
Private Sub Class_Initialize()
    Set m_PersonOption = New mpDB.CPersonOption
    m_PersonOption.Parent = Me
End Sub

Private Sub Class_Terminate()
    Set m_PersonOption = Nothing
    Set m_colPersonOptions = Nothing
    Set m_colPersonOptionsForm = Nothing
End Sub

