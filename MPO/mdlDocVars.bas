Attribute VB_Name = "mdlDocVars"
Option Explicit
Public Function DeleteAllDocVars(Optional bDeleteAll = False)
    Dim oDocVar As Variable
    
    For Each oDocVar In ActiveDocument.Variables
        If bDeleteAll = True Then
            oDocVar.Delete
        Else
            If Left(oDocVar.Name, 9) <> "zzmpFixed" Then
                oDocVar.Delete
            End If
        End If
    Next oDocVar
End Function

Function bDeleteDocVar(xDocVar As String) As Boolean
    Dim oDocVar As Variable
    For Each oDocVar In ActiveDocument.Variables
        If oDocVar.Name = xDocVar Then
            oDocVar.Delete
            Exit Function
        End If
    Next oDocVar
    
End Function

'CharlieCore 9.2.0 - Encrypt 'rems

'Public Sub SetArrayDocVars(PackedArray)
'    On Error GoTo AddVar
'    ActiveDocument.Variables("packedarray") = PackedArray
'    Exit Sub
'AddVar:
'    ActiveDocument.Variables.Add "packedarray", PackedArray
'    Resume
'End Sub
'
'Public Sub GetArrayDocVars(xDocVarName As String, _
'                           PackedArray)
'   On Error Resume Next
'    PackedArray = ActiveDocument _
'        .Variables(xDocVarName)
'End Sub
'
'Public Sub SetDocVars(frmP As Form, _
'                      Optional bMarkAsRestarted As Boolean = True)
'
'    Dim ctlControl As VB.Control
'    Dim xName As String
'    Dim vValue As Variant
'    Dim varReuse As Variable
'
'    For Each ctlControl In frmP.Controls
'        If ControlHasValue(ctlControl) Then
'                On Error Resume Next
''---        assign true false values for chkboxes, tglbuttons, Optionbuttons
'                xName = ctlControl.Name
'                vValue = ctlControl
'                If InStr(xName, "chk") Or InStr(xName, "tgl") Or InStr(xName, "opt") Then
'                    If vValue = False Then
'                        vValue = "False"
'                    Else
'                        vValue = "True"
'                    End If
'                ElseIf TypeOf ctlControl Is TrueDBList60.TDBCombo Then
'                    If ctlControl.ComboStyle = 2 Then
'                        vValue = ctlControl.BoundText
'                    Else
'                        vValue = ctlControl
'                    End If
'                ElseIf TypeOf ctlControl Is TrueDBList60.TDBList Then
'                    vValue = ctlControl.BoundText
'                Else
'                    vValue = ctlControl
'                End If
'
''               if doc var exists, set value, else add doc var
'                With ActiveDocument
'                    On Error Resume Next
'                    Set varReuse = Nothing
'                    Set varReuse = .Variables(xName)
'                    On Error GoTo 0
'
'                    If varReuse Is Nothing Then
'                        .Variables.Add xName, xNullToString(vValue)
'                    Else
'                        varReuse.Value = xNullToString(vValue)
'                    End If
'                End With
'        End If
'    Next ctlControl
'
''   mark as restarted
'    If bMarkAsRestarted Then _
'        ActiveDocument.Variables("Restarted") = True
'End Sub
'
'Public Sub GetDocVars(frmP As Form)
''assigns stored doc variable values to
''controls of same name - forces author list
''controls to evaluate first to avoid change
''events of those controls from affecting
''the values of other controls
'
'    Dim ctlControl As Control
'    Dim vCurValue As Variant
'    Dim xPriorityCtls(0 To 2) As String
'    Dim bIsPriorityControl As Boolean
'    Dim i As Integer
'    Dim xName As String
'    Dim SkipListUpdate As Boolean
'
''   enumerate trouble controls - this is the only part,
''   if any, that should require modification
'    xPriorityCtls(0) = "lstAttyList"
'    xPriorityCtls(1) = "cmbAuthor"
'    xPriorityCtls(2) = "cbxAuthor"
'
''   run through priority controls, setting their values-
''   remember, change events will occur, but they should
''   be irrelevant after non-priority controls are set below
'    For i = 1 To UBound(xPriorityCtls)
'        On Error Resume Next
'        Set ctlControl = frmP.Controls(xPriorityCtls(i))
'        vCurValue = ActiveDocument.Variables(ctlControl.Name)
'        If Not IsEmpty(vCurValue) Then
'            SkipListUpdate = True
'            ctlControl = vCurValue
'        End If
'    Next i
'
'    On Error GoTo 0
'
''   run through non-priority controls,
''   setting their values
'    For Each ctlControl In frmP.Controls
'        If ControlHasValue(ctlControl) Then
'            xName = ctlControl.Name
'            If Not bIsPriorityControl Then
'                On Error Resume Next
'                    vCurValue = ActiveDocument.Variables(ctlControl.Name)
'                If Not IsEmpty(vCurValue) Then
'                    SkipListUpdate = True
'                    If InStr(xName, "chk") Then
'                        ctlControl = Abs(CBool(vCurValue))
'                    ElseIf TypeOf ctlControl Is TrueDBList60.TDBCombo Then
'                        If ctlControl.ComboStyle = 2 Then
'                            ctlControl.BoundText = vCurValue
'                        Else
''                           in this case, the user may have entered text that
''                           doesn't match a list item, so try to enter value into
''                           both .text and .boundtext
'                            ctlControl = vCurValue
'                            ctlControl.BoundText = vCurValue
'                        End If
'                    ElseIf TypeOf ctlControl Is TrueDBList60.TDBList Then
'                        ctlControl.BoundText = vCurValue
'                    Else
'                        ctlControl = vCurValue
'                    End If
'                End If
'            End If
'        End If
'    Next ctlControl
'End Sub
'
'
