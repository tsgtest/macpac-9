VERSION 5.00
Object = "{C9F4AA26-1105-469A-B1E0-B147E230C297}#1.0#0"; "mpControls2.ocx"
Begin VB.Form frmNewDocument 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a New Document"
   ClientHeight    =   5316
   ClientLeft      =   1836
   ClientTop       =   2292
   ClientWidth     =   7896
   Icon            =   "frmNewDocument.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5316
   ScaleWidth      =   7896
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin mpControls2.MPTemplates MPTemplates1 
      Height          =   4680
      Left            =   75
      TabIndex        =   0
      Top             =   0
      Width           =   7800
      _ExtentX        =   13758
      _ExtentY        =   8255
      ItemSource      =   1
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   5310
      TabIndex        =   3
      Top             =   4845
      Width           =   1000
   End
   Begin VB.CommandButton btnFavorites 
      Caption         =   "Delete Fa&vorite"
      Height          =   400
      Left            =   1455
      TabIndex        =   2
      Top             =   4845
      Width           =   1230
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   6390
      TabIndex        =   4
      Top             =   4845
      Width           =   1000
   End
   Begin VB.CommandButton btnWord 
      Caption         =   "Native &Word..."
      Height          =   400
      Left            =   90
      TabIndex        =   1
      Top             =   4845
      Width           =   1230
   End
   Begin VB.Menu mnuTemplates 
      Caption         =   "Templates"
      Visible         =   0   'False
      Begin VB.Menu mnuTemplates_AddToFavorites 
         Caption         =   "Add Fa&vorite"
      End
      Begin VB.Menu mnuTemplates_RemoveFromFavorites 
         Caption         =   "Delete Fa&vorite"
      End
   End
End
Attribute VB_Name = "frmNewDocument"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Enum mpNewDocumentActions
    mpNewDocumentAction_Cancelled = 0
    mpNewDocumentAction_OK = 1
    mpNewDocumentAction_Word = 2
End Enum

Private m_bAction As mpNewDocumentActions
Private m_oDoc As CDocument
Private m_xSelID As String
Private m_bIsAttachTo As Boolean

Public Property Get Action() As mpNewDocumentActions
    Action = m_bAction
End Property

Public Property Get SelectedID() As String
    SelectedID = m_xSelID
End Property

Public Property Get AttachTo() As Boolean
    AttachTo = m_bIsAttachTo
End Property

Public Property Let AttachTo(bNew As Boolean)
    m_bIsAttachTo = bNew
End Property

Private Sub btnCancel_Click()
    m_bAction = mpNewDocumentAction_Cancelled
    
    Me.Hide
    DoEvents
    
''   return to all templates
'    With g_oDBs.TemplateDefinitions
'        .Group = Empty
'        .Refresh
'    End With
End Sub

Private Sub btnFavorites_Click()
    On Error GoTo ProcError
    With Me.MPTemplates1
        If InStr(Me.btnFavorites.Caption, "Add") Then
            .AddFavorite .SelectedID
        Else
            .DeleteFavorite .SelectedID
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not edit favorites."
    Exit Sub
End Sub

Private Sub btnOK_Click()
    On Error GoTo ProcError
    m_bAction = mpNewDocumentAction_OK
    
    m_xSelID = Me.MPTemplates1.SelectedID
    Me.Hide
    DoEvents
    
''   return to all templates
'    With g_oDBs.TemplateDefinitions
'        .Group = Empty
'        .Refresh
'    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnWord_Click()
    On Error GoTo ProcError
    m_bAction = mpNewDocumentAction_Word
    Me.Hide
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Command1_Click()
    MsgBox g_oDBs.SegmentGroups.Count
    With g_oDBs.SegmentDefinitions
        If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Or g_bIsWord16x Then
            .Refresh "Letter.dotx", 2
        Else
            .Refresh "Letter.dot", 2
        End If
        
        MsgBox g_oDBs.SegmentDefinitions.Count
    End With
End Sub

Private Sub Form_Activate()
    Dim bResult As Boolean
    
    Me.MPTemplates1.SetFocus
    
    'remove shortcut key for Word 2007
    'this allows SendKeys to be used for
    'bringing up Native File New Dialog
    If g_bIsWord12x Then
        Me.btnWord.Caption = "Native Word"
    End If
    
    '   show/hide native file new
    On Error Resume Next
    If Me.AttachTo Then
        bResult = CBool(GetMacPacIni("General", "AllowNativeWordAttachTo"))
        Me.btnWord.Visible = bResult
    Else
        Me.btnWord.Visible = GetMacPacIni("General", "AllowNativeWordNewDlg")
    End If
End Sub

Private Sub Form_Initialize()
    Set Me.MPTemplates1.DBObj = g_oDBs
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If IsPressed(vbKeyControl) And IsPressed(vbKeyF6) Then
        ShowAboutDialog
    End If
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim oNode As Node
    Dim bExpand As Boolean
    Dim bSelFaves As Boolean
    
    On Error GoTo ProcError
    m_bAction = mpNewDocumentAction_Cancelled
        
'   close Header/Footer
    Set m_oDoc = New CDocument
    m_oDoc.CloseHeaderFooter
        
    Exit Sub
ProcError:
    RaiseError "MPO.frmNewDocument.Form_Load"
    Exit Sub
End Sub

Private Sub MPTemplates1_AfterGroupCategoryChange(ByVal iCategory As mpControls2.mpTemplateGroupCategories)
End Sub

Private Sub MPTemplates1_GroupCategoryChange(ByVal iCategory As mpControls2.mpTemplateGroupCategories)
    If iCategory = mpTemplateGroupCategory_Favorites Then
        Me.btnFavorites.Caption = "Delete Fa&vorite"
    Else
        Me.btnFavorites.Caption = "Add Fa&vorite"
    End If
End Sub

Private Sub MPTemplates1_SelDoubleClick(ByVal SelID As String)
    m_xSelID = SelID
    btnOK_Click
End Sub

Private Sub MPTemplates1_Selection(ByVal SelID As String)
    Me.btnOK.Enabled = Len(SelID)
End Sub
