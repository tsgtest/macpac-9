VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmDocumentFont 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Change Document Font"
   ClientHeight    =   3516
   ClientLeft      =   3828
   ClientTop       =   996
   ClientWidth     =   5472
   Icon            =   "frmDocumentFont.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3516
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TrueDBList60.TDBCombo cbxFont 
      Height          =   2220
      Left            =   1800
      OleObjectBlob   =   "frmDocumentFont.frx":058A
      TabIndex        =   1
      Top             =   150
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cbxSize 
      Height          =   600
      Left            =   1800
      OleObjectBlob   =   "frmDocumentFont.frx":2301
      TabIndex        =   3
      Top             =   2565
      Width           =   3615
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4395
      TabIndex        =   5
      Top             =   3030
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3330
      TabIndex        =   4
      Top             =   3030
      Width           =   1000
   End
   Begin VB.Label lblSize 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Size:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1185
      TabIndex        =   2
      Top             =   2625
      Width           =   360
   End
   Begin VB.Label lblFont 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Font:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1170
      TabIndex        =   0
      Top             =   210
      Width           =   360
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6075
      Left            =   -15
      Top             =   -405
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmDocumentFont"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_oDoc As CDocument
Private m_bRestrictFont As Boolean
Private m_bRestrictSize As Boolean


'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Public Property Get RestrictFont() As Boolean
    RestrictFont = m_bRestrictFont
End Property

Public Property Let RestrictFont(bNew As Boolean)
    m_bRestrictFont = bNew
End Property

Public Property Get RestrictSize() As Boolean
    RestrictSize = m_bRestrictSize
End Property

Public Property Let RestrictSize(bNew As Boolean)
    m_bRestrictSize = bNew
End Property

'**********************************************************
'   Events
'**********************************************************

Private Sub cbxFont_Mismatch(NewEntry As String, Reposition As Integer)
    CorrectTDBComboMismatch Me.cbxFont, Reposition
End Sub

Private Sub cbxFont_Validate(Cancel As Boolean)
    Cancel = Not bValidateBoundTDBCombo(Me.cbxFont)
End Sub

Private Sub cbxSize_Validate(Cancel As Boolean)
    Dim xVal As String
    xVal = Me.cbxSize.Text
    If Not IsNumeric(xVal) Then
        Cancel = True
    ElseIf xVal < 6 Or xVal > 108 Then
        Cancel = True
    End If
    If Cancel Then
        xMsg = "Invalid font size."
        MsgBox xMsg, vbExclamation, App.Title
    End If
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Form_Activate()
    Dim xFont As String
    Dim xFontSize As String
    xFont = m_oDoc.NormalFontName
    xFontSize = m_oDoc.NormalFontSize
    
    Me.Initializing = False
    Me.cbxFont.BoundText = xFont
    Me.cbxSize.BoundText = xFontSize
End Sub

Private Sub Form_Load()
    Dim xFontSizes() As String
    Dim xSizes As String
    
    Me.Initializing = True

    Set m_oDoc = New CDocument
    
'*  list fonts
    With Me.cbxFont
        If Me.RestrictFont Then
            .Array = g_oDBs.Lists("RestrictedFonts").ListItems.Source
        Else
            .Array = g_oDBs.Lists("Fonts").ListItems.Source
        End If
        .Rebind
    End With
    
'*  list sizes
    With Me.cbxSize
        If Me.RestrictSize Then
            .Array = g_oDBs.Lists("RestrictedFontSizes").ListItems.Source
        Else
            GetIntegerSequence xFontSizes(), 8, 24
            xSizes = arrPToString(xFontSizes, "|")
            .Array = xarStringToxArray(xSizes)
        End If
        .Rebind
    End With
    
    ResizeTDBCombo Me.cbxSize, 4
End Sub

Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnOK_Click()
    
    If cbxFont.Text = "" Then
        xMsg = "Invalid Font Name."
        MsgBox xMsg, vbExclamation, App.Title
        cbxFont.SetFocus
        Exit Sub
    ElseIf cbxSize.Text = "" Then
        xMsg = "Invalid Font Size."
        MsgBox xMsg, vbExclamation, App.Title
        cbxSize.SetFocus
        Exit Sub
    End If
    
    Me.Hide
    Application.ScreenRefresh
    
'   change normal style font
    m_oDoc.NormalFontName = Me.cbxFont.Text
    m_oDoc.NormalFontSize = Me.cbxSize.Text
    
    
End Sub

