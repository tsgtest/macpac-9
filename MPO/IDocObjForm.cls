VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IDocObjForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   IDocObjForm Interface Class
'   created 1/3/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define the IDocObjForm Interface - those
'   properties and methods that all MacPac
'   document object Forms (e.g. frmCustomDocObj) must have
'**********************************************************


Property Get Cancelled() As Boolean
End Property

Public Property Get DocObject() As MPO.IDocObj
End Property

Public Property Set DocObject(oNew As MPO.IDocObj)
End Property

