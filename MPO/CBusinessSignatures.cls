VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessSignatures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CBusinessSignatures Collection Class
'   created 3/14/00 by Jeffrey Sweetland
'
'   Contains properties and methods that manage the
'   collection of CBusinessSignature

'   Member of
'   Container for CBusinessSignature
'**********************************************************
Option Explicit
Const mpParagraphBookmark As String = "zzmpFIXED_BSigIntroParagraph"
Const mpBoilerplateFileDef As String = "BusinessSigCADefault.mbp" '---9.5.0
Private Const mpMaxValidID As Integer = 20

Private m_Col As Collection
Private m_oDoc As MPO.CDocument
Private m_iIndex As Integer
Private m_bIncludeIntroParagraph As Boolean
Private m_xDocType As String
Private Const mpDocSegment As String = "BusinessSig"
Private m_oParent As CBusinessSignature
Public Property Let IncludeIntroParagraph(bNew As Boolean)
    m_bIncludeIntroParagraph = bNew
End Property
Public Property Get IncludeIntroParagraph() As Boolean
    IncludeIntroParagraph = m_bIncludeIntroParagraph
End Property
Public Property Get DocumentType() As String
    DocumentType = m_xDocType
End Property
Public Property Let DocumentType(xNew As String)
    m_xDocType = xNew
End Property
Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = m_Col.Count
End Property
Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)
    On Error GoTo ProcError
    Dim iID As Integer
    iID = m_Col(vntIndexKey).ID
    m_Col.Remove vntIndexKey
    m_oDoc.DeleteSegmentValues DocSegment, iID
    If m_Col.Count = 0 Then m_iIndex = 0
    Exit Sub
ProcError:
    g_oError.Raise mpError_InvalidItemIndex, "CBusinessSignatures.Remove"
    
End Sub
Public Sub RemoveAll()
    Dim xTBM As String
    Dim xSBM As String
    
    On Error GoTo ProcError
    If Me.Count = 0 Then Exit Sub
    With m_oDoc
        xTBM = mpParagraphBookmark
        
        If .bHideFixedBookmarks Then
            xTBM = "_" & xTBM
        End If
        If .bBoilerPlateExists(xTBM) Then
            ' Delete closing paragraph
            .DeleteItem xTBM, True
        End If
        xSBM = Item(1).Definition.TableBookmark
        If .bHideFixedBookmarks Then
            ' Delete sig table
            xSBM = "_" & xSBM
        End If
        If .bBoilerPlateExists(xSBM) Then
            On Error Resume Next
            .File.Bookmarks(xSBM).Range.Tables(1).Delete
            .File.Bookmarks(xSBM).Delete
            On Error GoTo ProcError
        End If
        ' Delete document variables
        .DeleteDocVars mpDocSegment
        ' Reset collection
        Set m_Col = New Collection
    End With
    Exit Sub
ProcError:
    g_oError.Raise mpError_InvalidItemIndex, "CBusinessSignatures.Remove"

End Sub
Public Property Get Item(vntIndexKey As Variant) As CBusinessSignature
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
    On Error GoTo ProcError
    Dim vKey As Variant
    If IsNumeric(vntIndexKey) Then
        vKey = "B" & Format(vntIndexKey, "00")
    Else
        vKey = vntIndexKey
    End If
    Set Item = m_Col(vKey)
    Exit Property
ProcError:
    g_oError.Raise mpError_InvalidItemIndex, "CBusinessSignatures.Item"
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = m_Col.[_NewEnum]
End Property
Friend Property Set Parent(oNew As CBusinessSignature)
    Set m_oParent = oNew
End Property
Friend Property Get Parent() As CBusinessSignature
    Set Parent = m_oParent
End Property
'**********************************************************
'   Methods
'**********************************************************
Public Function Add(Optional objNewMember As MPO.CBusinessSignature, _
        Optional iID As Integer, _
        Optional sKey As String) As CBusinessSignature
    
'   generate error if collection is full
    If Me.Count = mpMaxValidID Then
        Err.Raise mpError_BusinessSignaturesCollectionFull
    End If
    
    'create a new object
    If objNewMember Is Nothing Then _
        Set objNewMember = New CBusinessSignature
        
    If iID = 0 Then _
        iID = NewIndex
        
    sKey = "B" & Format(iID, "00")
    
    'set the properties passed into the method
    objNewMember.ID = iID
    objNewMember.Key = sKey
    If Not m_oParent Is Nothing Then
        objNewMember.ParentID = m_oParent.ID
        objNewMember.TypeID = m_oParent.TypeID
        objNewMember.Position = m_oParent.Position
    End If
    If Len(sKey) = 0 Then
        m_Col.Add objNewMember
    Else
        m_Col.Add objNewMember, sKey
    End If
        
    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing
End Function
Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function
Public Sub LoadValues()
    ' Loads previously created pleading captions by checking for
    ' existence of saved document variables
    Dim i As Integer
    Dim iParent As Integer
    Dim capC As CBusinessSignature
    Set m_Col = New Collection
    
    If Not m_oParent Is Nothing Then
        iParent = m_oParent.ID
        m_xDocType = ""
        m_bIncludeIntroParagraph = False
    Else
        iParent = 0
        With m_oDoc
            m_xDocType = .RetrieveItem("DocumentType", mpDocSegment, 0)
            m_bIncludeIntroParagraph = .RetrieveItem("IncludeIntroParagraph", mpDocSegment, 0, 2)
        End With
    End If
        
    m_iIndex = 0
    
    For i = 1 To mpMaxValidID
        If Exists(i) Then
            Set capC = New CBusinessSignature
            capC.ID = i
            capC.LoadValues iParent
            Me.Add capC
        End If
    Next i
End Sub
Public Sub Refresh(Optional sKey As String)
    Dim capC As CBusinessSignature
    Dim bFirst As Boolean
    Dim i As Integer
    Dim j As Integer
    Dim oStyle As Word.Style
    
    ' First delete any existing document variables
    ' These will be recreated by Refresh method of CBusinessSignature
    If IsNumeric(sKey) Then
        sKey = "B" & Format(sKey, "00")
    End If
    
    'GLOG : 5434 : ceh
    'remove existing so that new style can come in from boilerplate
    On Error Resume Next
    Set oStyle = ActiveDocument.Styles("Business Signature")
    On Error GoTo 0
    
    If Not (oStyle Is Nothing) Then
        If oStyle.ParagraphFormat.LeftIndent = 0 Then
            oStyle.Delete
        End If
    End If

    If Len(sKey) > 0 Then
        Item(sKey).Refresh
    Else
        m_oDoc.DeleteDocVars DocSegment
        bFirst = True
        
        
        'refresh starting from top
        For i = 1 To mpMaxValidID       '(m_Col.Count * 2)
            For Each capC In m_Col
                If capC.Position = i Then
                    j = j + 1
                    capC.Refresh (j = 1)
                End If
            Next capC
        Next i
        
'        'refresh starting from top
'        For i = 1 To (m_Col.Count * 2)
'            For Each capC In m_Col
'                If capC.Position = i Then
'                    capC.Refresh (i = 1)
'                End If
'            Next capC
'        Next i
        
'        For Each capC In m_Col
'            ' Refresh captions, forcing rebuild for first item only
'            capC.Refresh bFirst
'            bFirst = False
'        Next
    End If
    
End Sub
Public Sub RefreshEmpty()
    Dim capC As CBusinessSignature
    ' First delete any existing document variables
    ' These will be recreated by Refresh method of CBusinessSignature
    For Each capC In m_Col
        capC.RefreshEmpty
    Next capC
End Sub
Public Sub Finish(Optional bForceRefresh As Boolean = True)
    
    Dim oSig As CBusinessSignature
    InsertBoilerplate
        
    If bForceRefresh Then
        Refresh
    Else
        RefreshEmpty
    End If
        
    With m_oDoc
        .SaveItem "IncludeIntroParagraph", mpDocSegment, 0, m_bIncludeIntroParagraph
        .SaveItem "DocumentType", mpDocSegment, 0, m_xDocType
    End With
    
    For Each oSig In m_Col
        oSig.Finish False
        Exit For
    Next oSig
End Sub
'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set m_Col = New Collection
    Set m_oDoc = New MPO.CDocument
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set m_Col = Nothing
End Sub
Private Function Exists(iID As Integer) As Boolean
'   test for existence by getting value of ID
    On Error Resume Next
    Exists = (m_oDoc.RetrieveItem("ID", DocSegment, iID) <> "")
End Function
Private Function NewIndex() As Integer
    ' Keeps track of next ID to assign
    ' There may be gaps if items have been deleted
    If Me.Count > m_iIndex Then _
        m_iIndex = Me.Item(Me.Count).ID
    
    m_iIndex = m_iIndex + 1
    NewIndex = m_iIndex
End Function
Private Function DocSegment() As String
    ' Segment for sub-entities is default string plus Index of parent object
    If m_oParent Is Nothing Then
        DocSegment = mpDocSegment
    Else
        DocSegment = mpDocSegment & "_" & m_oParent.ID
    End If
End Function
Private Sub InsertBoilerplate()
    ' Inserts IN WITNESS WHEREOF... paragraph with document type
    
    Dim xTBM As String
    Dim rngBMK As Word.Range
    Dim tblSig As Word.Table
    Dim bAddedBeforeTable As Boolean
    Dim xSBM As String
    Dim xBP As String '---9.5.0
    
    With m_oDoc
        .Selection.Collapse wdCollapseEnd
        xTBM = mpParagraphBookmark
        
        If .bHideFixedBookmarks Then
            .HideBookmark (xTBM)
            xTBM = "_" & xTBM
        End If
        
        ' Replace existing paragraph bookmark if it exists,
        ' or insert before Signature table if that's found
        
        If Not .bBoilerPlateExists(xTBM) Then
            If Not m_bIncludeIntroParagraph Then Exit Sub
            xTBM = mpParagraphBookmark
            xSBM = Item(1).Definition.TableBookmark
            If .bHideFixedBookmarks Then
                .HideBookmark (xSBM)
                xSBM = "_" & xSBM
            End If
            If .bBoilerPlateExists(xSBM) Then
                Set rngBMK = .File.Bookmarks(xSBM).Range
                rngBMK.Collapse wdCollapseStart
                On Error Resume Next
                Set tblSig = rngBMK.Tables(1)
                On Error GoTo 0
                ' If range is within a table, add an empty paragraph before
                If Not tblSig Is Nothing Then
                    Set rngBMK = .rngAddParaBeforeTable(tblSig)
                    bAddedBeforeTable = True
                End If
                rngBMK.Select
            End If
                        
            '---9.5.0
            xBP = mpBase2.GetMacPacIni("Business", "BSigClosingBoilerplateFile")
            If xBP = "" Then _
                xBP = mpBoilerplateFileDef
            .InsertBoilerplate xBP, xTBM
            '---End 9.5.0
            DoEvents
            If .bHideFixedBookmarks Then
                .HideBookmark (xTBM)
                xTBM = "_" & xTBM
            End If
            ' If we had to add text before signature table, clean up
            ' extra para mark.
            If bAddedBeforeTable Then
                rngBMK.Next(wdParagraph, 1).Delete
            End If
        Else
            ' Delete existing table and reinsert blank
            Set rngBMK = ActiveDocument.Bookmarks(xTBM).Range.Paragraphs.Last.Range
            rngBMK.Text = ""
            DoEvents
            If m_bIncludeIntroParagraph Then
                On Error Resume Next
                Set tblSig = rngBMK.Tables(1)
                On Error GoTo 0
                ' If range is within a table, add an empty paragraph before
                If Not tblSig Is Nothing Then
                    Set rngBMK = .rngAddParaBeforeTable(tblSig)
                    bAddedBeforeTable = True
                End If
                rngBMK.Select
                xTBM = mpParagraphBookmark
                '---9.5.0
                xBP = mpBase2.GetMacPacIni("Business", "BSigClosingBoilerplateFile")
                If xBP = "" Then _
                    xBP = mpBoilerplateFileDef
                .InsertBoilerplate xBP, xTBM
                '---End 9.5.0
                DoEvents
                If .bHideFixedBookmarks Then
                    .HideBookmark (xTBM)
                    xTBM = "_" & xTBM
                End If
                ' If we had to add text before signature table, clean up
                ' extra para mark.
                If bAddedBeforeTable Then
                    rngBMK.Next(wdParagraph, 1).Delete
                End If
            Else
                On Error Resume Next
                ActiveDocument.Bookmarks(xTBM).Delete
                rngBMK.Select
                Exit Sub
            End If
        End If
        .EditItem "zzmpBSigDocumentType", _
            IIf(Me.DocumentType <> "", Me.DocumentType, "____________")
        Set rngBMK = ActiveDocument.Bookmarks(xTBM).Range
        rngBMK.Collapse wdCollapseEnd
        rngBMK.Select
    End With
End Sub
