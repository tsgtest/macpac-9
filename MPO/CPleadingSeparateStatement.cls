VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingSeparateStatement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   PleadingSeparateStatement Class
'   created 7/06/04 by Mike Conner

'   Contains properties and methods that
'   define a MacPac PleadingSeparateStatement

'   Container for CDocument
'**********************************************************

Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oProps As MPO.CCustomProperties
Private m_oDef As mpDB.CPleadingSeparateStatementDef
Private m_bInit As Boolean

Public Event BeforeBoilerplateInsert()
Public Event AfterBoilerplateInsert()

Const mpDocSegment As String = "PleadingSeparateStatement"
Const mpSepTableBookmark As String = "zzmpSeparateStatementTable"


Implements MPO.IDocObj
'**********************************************************
'   Properties
'**********************************************************
Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function


Public Property Let TypeID(lNew As Long)
    Dim xDesc As String

    On Error GoTo ProcError
    
    If Me.TypeID <> lNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "TypeID", mpDocSegment, , lNew
        Set m_oDef = g_oDBs.PleadingSeparateStatementDefs.Item(lNew)
        
        Me.InsertBoilerplate
        
    '       alert and exit if not a valid Type
        If (m_oDef Is Nothing) Then
            Err.Raise mpError_InvalidMember
        End If
    End If
    
    Exit Property

ProcError:
    If Err.Number = mpError_InvalidMember Then
        xDesc = "Invalid Verification TypeID."
    Else
        xDesc = g_oError.Desc(Err.Number)
    End If
    RaiseError "MPO.CPleadingSeparateStatement.TypeID", xDesc
End Property

Public Property Get TypeID() As Long
    On Error Resume Next
    TypeID = m_oDoc.RetrieveItem("TypeID", mpDocSegment, , mpItemType_Integer)
End Property

Public Function Definition() As mpDB.CPleadingSeparateStatementDef
    If (m_oDef Is Nothing) Then
        Set m_oDef = g_oDBs.PleadingSeparateStatementDefs.Item(Me.TypeID)
    End If
    Set Definition = m_oDef
End Function

Public Property Get LeftColumnHeading() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("LeftColumnHeading", mpDocSegment)
    LeftColumnHeading = xTemp
End Property
Public Property Let LeftColumnHeading(xNew As String)
    m_oDoc.EditItem_ALT "zzmpLeftColumnHeading", xNew, , False
    m_oDoc.SaveItem "LeftColumnHeading", mpDocSegment, , xNew
End Property

Public Property Get RightColumnHeading() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("RightColumnHeading", mpDocSegment)
    RightColumnHeading = xTemp
End Property
Public Property Let RightColumnHeading(xNew As String)
    m_oDoc.EditItem_ALT "zzmpRightColumnHeading", xNew, , False
    m_oDoc.SaveItem "RightColumnHeading", mpDocSegment, , xNew
End Property

Public Property Get SeparateStatementTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SeparateStatementTitle", mpDocSegment)
    SeparateStatementTitle = xTemp
End Property

Public Property Let SeparateStatementTitle(xNew As String)
    If Me.SeparateStatementTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSeparateStatementTitle", xNew, , True
        m_oDoc.SaveItem "SeparateStatementTitle", mpDocSegment, , xNew
    End If
End Property


Public Property Get IncludeTitle() As Boolean
    Dim vTemp As Variant
    On Error Resume Next
    vTemp = m_oDoc.RetrieveItem( _
        "IncludeTitle", mpDocSegment, , mpItemType_Boolean)
    If vTemp <> mpUndefined Then _
        IncludeTitle = vTemp
End Property

Public Property Let IncludeTitle(bNew As Boolean)
    If Me.IncludeTitle <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "IncludeTitle", mpDocSegment, , bNew
        If bNew = True Then
            Me.SeparateStatementTitle = Me.Definition.Title
        Else
            Me.SeparateStatementTitle = ""
        End If
    End If
End Property

Public Property Get IncludeTableBorders() As Boolean
    Dim vTemp As Variant
    On Error Resume Next
    vTemp = m_oDoc.RetrieveItem( _
        "IncludeTableBorders", mpDocSegment, , mpItemType_Boolean)
    If vTemp <> mpUndefined Then _
        IncludeTableBorders = vTemp
End Property

Public Property Let IncludeTableBorders(bNew As Boolean)
    If Me.IncludeTableBorders <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "IncludeTableBorders", mpDocSegment, , bNew
        Me.ApplyTableBorders bNew
    End If
End Property


'**********************************************************
'   Methods
'**********************************************************

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Definition.BoilerplateFile
End Property

Public Sub Refresh()
    Dim i As Integer
    On Error GoTo ProcError
    With Me
        .LeftColumnHeading = .LeftColumnHeading
        .RightColumnHeading = .RightColumnHeading
        .SeparateStatementTitle = .SeparateStatementTitle
        .IncludeTableBorders = .IncludeTableBorders
        .CustomProperties.RefreshValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CPleadingSeparateStatement.Refresh"
    Exit Sub
End Sub
Public Sub RefreshEmpty()
    Dim i As Integer
    Dim oDef As mpDB.CPleadingSeparateStatementDef
    On Error GoTo ProcError
    Set oDef = Me.Definition
    With Me
        If .LeftColumnHeading = Empty Then .LeftColumnHeading = .LeftColumnHeading
        If .RightColumnHeading = Empty Then .RightColumnHeading = .RightColumnHeading
        If .SeparateStatementTitle = Empty Then .SeparateStatementTitle = .SeparateStatementTitle
        If .IncludeTableBorders = Empty Then .IncludeTableBorders = .IncludeTableBorders
        
        .CustomProperties.RefreshEmptyValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CPleadingSeparateStatement.RefreshEmpty"
    Exit Sub
End Sub
Public Sub Finish(Optional bForceRefresh As Boolean = False)
    Dim rngP As Range
    Dim oDef As mpDB.CPleadingSeparateStatementDef
    On Error GoTo ProcError
    
    g_bForceItemUpdate = True
    With m_oDoc
        If bForceRefresh Then
            Refresh
        Else
            RefreshEmpty
        End If

        .UpdateFields
       
        Set oDef = Me.Definition
        
'       delete any hidden text - ie text marked for deletion
       .DeleteHiddenText , , , True
       .RemoveFormFields
       .SelectStartPosition , "zzmpFIXED_SegmentBodyStart", , False
        
       .ClearBookmarks , .bHideFixedBookmarks
       
    End With
    
    g_bForceItemUpdate = False
    Exit Sub
ProcError:
    RaiseError "MPO.CPleadingSeparateStatement.Finish"
        g_bForceItemUpdate = False
End Sub


Friend Sub Initialize()
    On Error GoTo ProcError
    
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidVerificationTemplate
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CPleadingSeparateStatement.Initialize"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Public Function InsertBoilerplate()
    On Error GoTo ProcError
    RaiseEvent BeforeBoilerplateInsert
    If ActiveDocument.Bookmarks.Exists(mpSepTableBookmark) Then
        ActiveDocument.StoryRanges(wdMainTextStory).Delete
    End If
    Me.Template.InsertBoilerplate BoilerplateDirectory() & Me.BoilerplateFile
    RaiseEvent AfterBoilerplateInsert
    Exit Function
ProcError:
    RaiseError "MPO.CPleadingSeparateStatement.InsertBoilerplate"
    Exit Function
End Function

Public Function ApplyTableBorders(bShow As Boolean)
    On Error GoTo ProcError
    
    With Me.Document
        With .File.Bookmarks(mpSepTableBookmark).Range.Tables(1)
            .Borders(wdBorderLeft).Visible = bShow
            .Borders(wdBorderRight).Visible = bShow
            .Borders(wdBorderTop).Visible = bShow
            .Borders(wdBorderBottom).Visible = bShow
            .Borders(wdBorderHorizontal).Visible = bShow
            .Borders(wdBorderVertical).Visible = bShow
        End With
    End With
    
    Exit Function
ProcError:
    RaiseError "MPO.CPleadingSeparateStatement.ApplyTableBorders"
    Exit Function
End Function


'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    
    Set m_oTemplate = g_oTemplates.ItemFromClass("CPleadingSeparateStatement")
End Sub
Private Sub Class_Terminate()
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
''''    Set m_oPaper = Nothing
    Set m_oDef = Nothing
End Sub
'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
''''    Me.Author = RHS
End Property
Private Property Get IDocObj_Author() As mpDB.CPerson
''''    Set IDocObj_Author = Me.Author
End Property
Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property
Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function
Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
''''    Me.DefaultAuthor = RHS
End Property
Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
''''    Set IDocObj_DefaultAuthor = Me.DefaultAuthor
End Property
Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function
Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub
Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property
Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub
Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub
Private Sub IDocObj_Refresh()
    Refresh
End Sub
Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function
Private Sub IDocObj_UpdateForAuthor()

End Sub
