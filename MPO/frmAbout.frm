VERSION 5.00
Begin VB.Form frmAbout 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4935
   ClientLeft      =   225
   ClientTop       =   1185
   ClientWidth     =   8025
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmAbout.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   8025
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox pctMPLogo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3540
      Left            =   250
      Picture         =   "frmAbout.frx":030A
      ScaleHeight     =   3540
      ScaleWidth      =   3600
      TabIndex        =   10
      Top             =   260
      Width           =   3600
   End
   Begin VB.PictureBox pctClose 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   7680
      Picture         =   "frmAbout.frx":15F0
      ScaleHeight     =   270
      ScaleWidth      =   285
      TabIndex        =   9
      Top             =   90
      Width           =   285
   End
   Begin VB.PictureBox pctTSGLogo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   4005
      Picture         =   "frmAbout.frx":19BC
      ScaleHeight     =   465
      ScaleWidth      =   570
      TabIndex        =   8
      Top             =   3870
      Width           =   570
   End
   Begin VB.PictureBox pctTSGTagline 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   390
      Left            =   240
      Picture         =   "frmAbout.frx":1E9B
      ScaleHeight     =   390
      ScaleWidth      =   4305
      TabIndex        =   7
      Top             =   4470
      Width           =   4305
   End
   Begin VB.PictureBox pctTSGInc 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   450
      Left            =   195
      Picture         =   "frmAbout.frx":2869
      ScaleHeight     =   450
      ScaleWidth      =   3870
      TabIndex        =   6
      Top             =   4050
      Width           =   3870
   End
   Begin VB.Label lblVersion 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Version automatic"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   3555
      TabIndex        =   5
      Top             =   1605
      Width           =   4530
   End
   Begin VB.Label lblCompatibility1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Compatible with 32-bit versions of"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3555
      TabIndex        =   4
      Top             =   2775
      Width           =   4530
   End
   Begin VB.Label lblCompatibility2 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Office 2016, 2013, 365 ProPlus, and 2010"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3555
      TabIndex        =   3
      Top             =   3030
      Width           =   4530
   End
   Begin VB.Label lblCompatibilty3 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3555
      TabIndex        =   2
      Top             =   3285
      Width           =   4530
   End
   Begin VB.Label lblAppName 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "MacPac"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   3555
      TabIndex        =   1
      Top             =   630
      Width           =   4530
   End
   Begin VB.Label lblCopyright 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "�1990 - 2017"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3555
      TabIndex        =   0
      Top             =   2145
      Width           =   4530
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_xMP90Ver As String

Public Property Let MacPac90Version(xNew As String)
    m_xMP90Ver = xNew
End Property

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    On Error Resume Next
    Me.lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub pctMPLogo_DblClick()
    Dim xMessage As String
    
    xMessage = "Application Paths:"
    
    xMessage = "Application Paths:" & vbCr & vbCr & _
                        "Program Dir " & vbTab & UCase(mpBase2.ProgramDirectory) & vbCr & _
                        "Application Dir " & vbTab & UCase(mpBase2.ApplicationDirectory) & vbCr & _
                        "--------------" & vbCr & _
                        "Boilerplate " & vbTab & UCase(mpBase2.BoilerplateDirectory) & vbCr & _
                        "Templates " & vbTab & UCase(mpBase2.TemplatesDirectory) & vbCr & _
                        "Personal    " & vbTab & UCase(mpBase2.UserFilesDirectory) & vbCr & _
                        "User Boilerplate " & vbTab & UCase(mpBase2.UserBoilerplateDirectory) & vbCr & _
                        "--------------" & vbCr & _
                        "Public DB   " & vbTab & UCase(g_oDBs.PublicDB.Name) & vbCr & _
                        "Private DB " & vbTab & UCase(g_oDBs.PrivateDB.Name) & vbCr & _
                        "--------------" & vbCr & _
                        "Word Startup " & vbTab & UCase(Word.Application.StartupPath) & vbCr & _
                        "Word Personal " & vbTab & UCase(Word.Options.DefaultFilePath(wdUserTemplatesPath)) & vbCr & _
                        "Word Workgroup " & vbTab & UCase(Word.Options.DefaultFilePath(wdWorkgroupTemplatesPath))

    MsgBox xMessage, , App.Title & " - " & mpBase2.GetMacPacIni("General", "SplashCustomText")
    
End Sub

Private Sub pctClose_Click()
    Unload Me
End Sub
