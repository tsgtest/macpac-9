VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private m_frmF As Form
Private m_Document As New MPO.CDocument

Public Function FormIsLoaded(xForm As String) As Boolean
    Dim frmTemp As Form
    For Each frmTemp In Forms
        If xForm = frmTemp.Name Then
            FormIsLoaded = True
            Exit Function
        End If
    Next
End Function

Public Property Let CurrentForm(frmNew As Form)
    Set m_frmF = frmNew
    m_frmF.Left = Me.CurrentFormLeft
    m_frmF.Top = Me.CurrentFormTop
End Property

Public Property Get CurrentForm() As Form
    Set CurrentForm = m_frmF
End Property

Public Property Let CurrentFormLeft(lNew As Long)
    If Me.CurrentForm Is Nothing Then Exit Property
    If lNew <= 0 Then Exit Property
    SetIni Me.CurrentForm.Name, "FormLeft", Str(lNew), m_Document.UserPath & "\user.ini"
End Property

Public Property Get CurrentFormLeft() As Long
    Dim lLeft As Long
    If Me.CurrentForm Is Nothing Then Exit Property
    lLeft = Val(GetIni(Me.CurrentForm.Name, "FormLeft", m_Document.UserPath & "\user.ini"))
    If lLeft = 0 Then lLeft = Me.CurrentForm.Left
    CurrentFormLeft = lLeft
End Property

Public Property Let CurrentFormTop(lNew As Long)
    If Me.CurrentForm Is Nothing Then Exit Property
    If lNew <= 0 Then Exit Property
    SetIni Me.CurrentForm.Name, "FormTop", Str(lNew), m_Document.UserPath & "\user.ini"
End Property

Public Property Get CurrentFormTop() As Long
    Dim lTop As Long
    If Me.CurrentForm Is Nothing Then Exit Property
    lTop = Val(GetIni(Me.CurrentForm.Name, "FormTop", m_Document.UserPath & "\user.ini"))
    If lTop = 0 Then lTop = Me.CurrentForm.Top
    CurrentFormTop = lTop
End Property

