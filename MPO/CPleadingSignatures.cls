VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingSignatures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CPleadingSignature"
Attribute VB_Ext_KEY = "Member0" ,"CPleadingSignature"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   CPleadingSignatures Collection Class
'   created 1/13/00 by Mike Conner-
'   barbies@ix.netcom.com
'   Reworked 2/18/00 by Jeffrey Sweetland
'
'   Contains properties and methods that manage the
'   collection of CPleadingSignatures

'   Member of
'   Container for CPleadingSignature
'**********************************************************
Private m_Col As Collection
Private m_oDoc As MPO.CDocument
Private Const mpMaxValidID As Integer = 10
Private m_MaxValidID As Integer
Private Const mpDocSegment As String = "PleadingSig"
Private Const mpColSegment As String = "Signatures"
Private Const mpCopySegment As String = "PleadingSignatureCopy"
Private Const mpClosingParagraphBookmark As String = "zzmpFIXED_ClosingParagraph"
Private Const mpSigTableBookmark As String = "zzmpFIXED_SignatureTable"


Public Function Add(Optional objNewMember As MPO.CPleadingSignature, _
        Optional iID As Integer, _
        Optional sKey As String) As CPleadingSignature
    
'   generate error if collection is full
    If Me.Count = m_MaxValidID Then
        Err.Raise mpError_PleadingSignaturesCollectionFull
    End If
    
    'create a new object
    If objNewMember Is Nothing Then _
        Set objNewMember = New CPleadingSignature
        
    If iID = 0 Then _
        iID = NewIndex
        
    sKey = "P" & Format(iID, "00")
    
    'set the properties passed into the method
    objNewMember.ID = iID
    objNewMember.Key = sKey
    If Len(sKey) = 0 Then
        m_Col.Add objNewMember
    Else
        m_Col.Add objNewMember, sKey
    End If
        
    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing
End Function
Public Property Get Item(vntIndexKey As Variant) As CPleadingSignature
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
    On Error GoTo ProcError
    Set Item = m_Col(vntIndexKey)
    Exit Property
ProcError:
    g_oError.Raise mpError_InvalidItemIndex, "CPleadingSignatures.Item"
End Property
Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = m_Col.Count
End Property
Public Property Get HighPosition() As Long
    'used to determine highest position property in collection
    Dim xARTemp As XArray
    Set xARTemp = Me.ListArray(True)
    HighPosition = xARTemp.Value(xARTemp.UpperBound(1), 2)
End Property
Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)
    Dim iID As Integer
    iID = m_Col(vntIndexKey).ID
    m_Col.Remove vntIndexKey
    With m_oDoc
        .CopySegmentDocVars mpDocSegment, iID, mpCopySegment
        .DeleteSegmentValues mpDocSegment, iID
    End With
End Sub
'Public Sub ClearCopiedDocVars()
'    Dim i As Integer
'    With m_oDoc
'        For i = 1 To mpMaxValidID
'            .DeleteSegmentValues mpCopySegment, i
'        Next
'    End With
'End Sub

Public Sub ClearCopiedDocVars()
    Dim i As Integer
    With m_oDoc
'deletes all doc vars belonging to specified doc segement
'   cycle through doc vars
        For i = .File.Variables.Count To 1 Step -1
    '       delete docvar if it belongs to specified segment
            On Error Resume Next
            If InStr(.File.Variables(i).Name, mpCopySegment) > 0 Then
                .File.Variables(i).Delete
            End If
        Next i
    End With
End Sub

Public Sub CancelRemove()
'---used when cancel used for interface called from main pleading -- restores original collection

    With m_oDoc
        .RestoreSegmentDocVars mpDocSegment, mpCopySegment
    End With
    
    If Me.Count = 0 Then
        Me.LoadValues
    End If
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = m_Col.[_NewEnum]
End Property

Public Property Let ClosingParagraph(bNew As mpTriState)
    If bNew = mpUndefined Then
        bNew = mpFalse
    End If
    
    If Me.ClosingParagraph <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "ClosingParagraph", mpColSegment, , CStr(bNew)
    End If
End Property

Public Property Get ClosingParagraph() As mpTriState
    On Error Resume Next
    ClosingParagraph = m_oDoc.RetrieveItem("ClosingParagraph", mpColSegment)
End Property

Public Property Get ClosingParagraphCity() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ClosingParagraphCity", mpColSegment)
    ClosingParagraphCity = xTemp
End Property

Public Property Let ClosingParagraphCity(xNew As String)
    If Me.ClosingParagraphCity <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpClosingParagraphCity", xNew, , False, , , , 20
        m_oDoc.SaveItem "ClosingParagraphCity", mpColSegment, , xNew
    End If
End Property

Public Property Get ClosingParagraphState() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ClosingParagraphState", mpColSegment)
    ClosingParagraphState = xTemp
End Property

Public Property Let ClosingParagraphState(xNew As String)
    If Me.ClosingParagraphState <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpClosingParagraphState", xNew, , False, , , , 20
        m_oDoc.SaveItem "ClosingParagraphState", mpColSegment, , xNew
    End If
End Property

Public Property Get ClosingParagraphDate() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ClosingParagraphDate", mpColSegment)
    ClosingParagraphDate = xTemp
End Property

Public Property Get ClosingParagraphDateText() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ClosingParagraphDateText", mpColSegment)
    ClosingParagraphDateText = xTemp
End Property

Public Property Let ClosingParagraphDateText(xNew As String)
    If Me.ClosingParagraphDateText <> xNew Or g_bForceItemUpdate Then
        If InStr(xNew, "Field") = 0 And xNew <> "" Then
            m_oDoc.EditItem_ALT "zzmpClosingParagraphDateText", xNew, , False
        Else
             m_oDoc.EditDateItem "zzmpClosingParagraphDateText", , , mpDateFormat_LongText
        End If
        m_oDoc.SaveItem "ClosingParagraphDateText", mpColSegment, , xNew
    End If
End Property

Public Property Let ClosingParagraphDate(xNew As String)
    If Me.ClosingParagraphDate <> xNew Or g_bForceItemUpdate Then
        If InStr(UCase(xNew), "<F>") Then
'           insert date as a field in specified format
             m_oDoc.EditDateItem "zzmpClosingParagraphDate", _
                 , , xNew
        ElseIf InStr(xNew, "Field") Then
'           insert date as a field in default format -
'           for compatibility with mp versions before v9.1
             m_oDoc.EditDateItem "zzmpClosingParagraphDate", _
                                  , , mpDateFormat_LongText
        ElseIf xNew = "" Then
'           insert date as text in specified pleading signature format
            m_oDoc.EditItem "zzmpClosingParagraphDate", _
                xNew, , True, , , True
        Else
            m_oDoc.EditItem "zzmpClosingParagraphDate", _
                Format(Date, xNew), , True
        End If
        m_oDoc.SaveItem "ClosingParagraphDate", mpColSegment, , xNew
    End If
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub LoadValues()
    ' Loads previously created pleading signatures by checking for
    ' existence of saved document variables
    Dim i As Integer
    Dim psgP As CPleadingSignature
    
    For i = 1 To m_MaxValidID
        If Exists(i) Then
            Set psgP = New CPleadingSignature
            psgP.ID = i
            psgP.LoadValues
            Me.Add psgP
        End If
    Next i
End Sub

'Public Sub Refresh(Optional sKey As String, _
'                                Optional bRebuildTable As Boolean = True, _
'                                Optional bReloadBP As Boolean = True)
'    Dim psgP As CPleadingSignature
'    Dim bFirst As Boolean
'
'    ' First delete any existing document variables
'    ' These will be recreated by Refresh method of CPleadingSignature
'    If Len(sKey) > 0 Then
'        Item(sKey).Refresh bRebuildTable, bReloadBP
'    Else
'        m_oDoc.DeleteDocVars mpDocSegment
'        If m_Col.Count = 1 Then
'            bFirst = bRebuildTable
'        Else
'            If m_oDoc.IsCreated Then
'                bFirst = True
'            Else
'                bFirst = bRebuildTable
'            End If
'        End If
'        For Each psgP In m_Col
'             ' Refresh signatures, forcing rebuild for first item only
'             psgP.Refresh bFirst, bReloadBP
'             bFirst = False
'         Next
'    End If
'    '---remove copied docvars if any items deleted
'    ClearCopiedDocVars
'End Sub

Public Sub Refresh(Optional sKey As String, _
                                Optional bRebuildTable As Boolean = True, _
                                Optional bAppend As Boolean = False)
'---9.8.00 these arguments are kept for back-compatibility only
'---They are now generated internally
    
    Dim psgP As CPleadingSignature
    Dim iCount As Integer
    'Dim xARTemp As XArrayDB
    Dim xARTemp As XArray
    Dim i As Integer
    Dim iPosOffset As Integer
    Dim iPosToSkip As Integer
    Dim bIsNonTable As Boolean
    Dim oTableRange As Word.Range
    Dim xTBM As String
    Dim oBookmarkRange As Word.Range
    
    m_oDoc.AdjustTableStyle mpSigTableBookmark, "Pleading Signature"

    iCount = 1
'---first create an array of keys/positions
        
    If Len(sKey) > 0 Then
        Item(sKey).Refresh bRebuildTable, bAppend
        bIsNonTable = Item(sKey).Definition.NonTableFormat
    Else
        Set xARTemp = New XArray
        xARTemp.ReDim 1, m_Col.Count, 0, 1
        
        For Each psgP In m_Col
            xARTemp.Value(iCount, 0) = psgP.Key
            xARTemp.Value(iCount, 1) = psgP.Position
            iCount = iCount + 1
        Next
'---sort the array by the .Position column
        Dim xH As New XHelper
        xH.Sort xARTemp, 1
        
'---Delete any existing document variables
'---(will be recreated by Refresh method of CPleadingSignature)
        m_oDoc.DeleteDocVars mpDocSegment

'---Now cyle through the xArrayDB and create/refresh sig objects by Key
'---Force empty row or cell if .Position <> item * offset
        For i = 1 To xARTemp.Count(1)
            Set psgP = Nothing
            Set psgP = Item(xARTemp.Value(i, 0))
            If Not psgP Is Nothing Then
'---calculate positions to skip
                If psgP.Definition.FullRowFormat Then
                    If i > 1 Then
                        iPosToSkip = (psgP.Position - (xARTemp(i - 1, 1) + 2)) / 2
                    Else
                        If psgP.Position <> 2 Then
                            iPosToSkip = (psgP.Position - (i * 2)) / 2
                        End If
                    End If
                Else
                    If i > 1 Then
                        iPosToSkip = psgP.Position - (xARTemp(i - 1, 1)) - 1
                    Else
                        If psgP.Position <> 2 Then
                            iPosToSkip = (psgP.Position - i)
                        End If
                    End If
                End If
                psgP.Refresh (i = 1), (i > 1), iPosToSkip
                bIsNonTable = psgP.Definition.NonTableFormat
            End If
        Next i
    
    End If
    
    xTBM = mpSigTableBookmark
    If m_oDoc.bHideFixedBookmarks Then
        xTBM = "_" & xTBM
    End If
    Set oBookmarkRange = ActiveDocument.Bookmarks(xTBM).Range
    
'---convert to text if necessary
    If bIsNonTable Then
        oBookmarkRange.Tables(1).Rows.ConvertToText wdSeparateByParagraphs
    End If
    
'---refresh closing paragraph if any
    RefreshClosingParagraph
    
'---remove copied docvars if any items deleted
    ClearCopiedDocVars
        
End Sub

Public Sub RefreshEmpty()
    Dim psgP As CPleadingSignature
    
    ' First delete any existing document variables
    ' These will be recreated by Refresh method of CPleadingSignature
    For Each psgP In m_Col
        psgP.RefreshEmpty
    Next psgP
End Sub
Public Sub Finish(Optional bForceRefresh As Boolean = True)
    If bForceRefresh Then
        Refresh
    Else
        RefreshEmpty
    End If
    If Me.Count > 0 Then
        Me.Item(1).Finish False
    End If
End Sub
'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set m_Col = New Collection
    Set m_oDoc = New MPO.CDocument
    m_MaxValidID = Max(Val(GetMacPacIni("Pleading", "MaxSignatures")), mpMaxValidID)
End Sub
Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set m_Col = Nothing
End Sub
Private Function Exists(iID As Integer) As Boolean
'   test for existence by getting value of ID
    On Error Resume Next
    Exists = (m_oDoc.RetrieveItem("ID", mpDocSegment, iID) <> "")
End Function
Private Function NewIndex() As Integer
    ' Keeps track of next ID to assign
    ' There may be gaps if items have been deleted
    Static iIndex As Integer
    If Me.Count > iIndex Then _
        iIndex = Me.Item(Me.Count).ID
    
    iIndex = iIndex + 1
    NewIndex = iIndex
End Function

Public Function ListArray(Optional bSortByPosition As Boolean = False) As XArray
    ' Returns collection as an XArray
    Dim i As Integer
    Dim iCount As Integer
    Dim xARTemp As XArray
   
    Set xARTemp = New XArray
    
    iCount = Me.Count
    If iCount = 0 Then
        iCount = 1
    End If
    
    xARTemp.ReDim 1, iCount, 0, 2
    
    With xARTemp
        For i = 1 To .UpperBound(1)
            .Value(i, 0) = Me.Item(i).DisplayName
            .Value(i, 1) = Me.Item(i).Key
            .Value(i, 2) = Me.Item(i).Position
        Next i
    End With
    
    If bSortByPosition Then
        Dim xH As New XHelper
        xH.Sort xARTemp, 2
    End If
    
    Set ListArray = xARTemp
    Set xARTemp = Nothing

End Function

Public Sub RefreshClosingParagraph()
    '---this appends a closing paragraph of type specified in first sig in collection
    '---to beginning of sig table

    '---if zzmpFIXED_PleadingSignatureClosingParagraph exists, evaluate paragraph count.
    '---if > 1, delete bookmark, inform user, and add new closing paragraph
    '---if = 1 then replace closing paragraph with new bp
    '---if fixed bm does not exist, create new closing paragraph.
    
    Dim rngSig As Word.Range
    Dim xMsg As String
    Dim oDoc As MPO.CDocument
    Dim xPBM As String
    Dim xTBM As String
    Dim oDef As mpDB.CPleadingSignatureDef
    Dim bTemp As String
    
    On Error Resume Next
    
    Set oDef = m_Col.Item(1).Definition
    Set oDoc = New MPO.CDocument
    
    If oDoc.bHideFixedBookmarks Then
        xPBM = "_" & mpClosingParagraphBookmark
        xTBM = "_" & mpSigTableBookmark
    End If

    Set rngSig = ActiveDocument.Bookmarks(xTBM).Range
    If rngSig Is Nothing Then Exit Sub
    
'---replace existing CP; do not replace if more than one paragraph, since user may have typed text inside hidden bookmark
    If ActiveDocument.Bookmarks.Exists(xPBM) Then
        If ActiveDocument.Bookmarks(xPBM).Range.Paragraphs.Count > 1 Then
            If Me.ClosingParagraph = Abs(mpTrue) Then
                xMsg = "Closing paragraph bookmark contains more than one paragraph.  " & _
                            "Existing text inside bookmark will be retained, and a new closing paragraph will be added " & _
                            "above your signature block table."
                MsgBox xMsg, vbInformation, App.Title
            End If
            ActiveDocument.Bookmarks(xPBM).Delete
       Else
            '---delete existing closing paragraph
            ActiveDocument.Bookmarks(xPBM).Range.Delete
        End If
    End If

'---exit if no closing paragraph specified
    If Me.ClosingParagraph <> mpTrue Then Exit Sub
    
    rngSig.Characters.First.Select
    Selection.MoveLeft wdCharacter, 2
    
'---handle bp with no spacer paragraphs
    If Selection.Paragraphs.Last.Range.Characters.Count > 2 Then
        Selection.InsertParagraphAfter
        Selection.MoveDown wdParagraph, 2
        Selection.MoveLeft wdCharacter, 1
        oDoc.InsertBoilerplate oDef.ClosingParagraphBoilerplateFile, mpClosingParagraphBookmark
        ActiveDocument.Bookmarks(mpClosingParagraphBookmark).Select
        Selection.MoveEnd wdParagraph, 1
        Selection.Paragraphs.Last.Range.Delete
    Else
        oDoc.InsertBoilerplate oDef.ClosingParagraphBoilerplateFile, mpClosingParagraphBookmark
    End If
    
'---refresh date type, office city & state
    bTemp = g_bForceItemUpdate
    g_bForceItemUpdate = True
    Me.ClosingParagraphCity = Me.ClosingParagraphCity
    Me.ClosingParagraphState = Me.ClosingParagraphState
    Me.ClosingParagraphDate = Me.ClosingParagraphDate
    Me.ClosingParagraphDateText = Me.ClosingParagraphDateText
    g_bForceItemUpdate = bTemp

'---hide bookmark if configured
    If oDoc.bHideFixedBookmarks Then
        oDoc.HideBookmark (mpClosingParagraphBookmark)
    End If

End Sub
Public Function ContainMixedFormats() As Boolean
    Dim bNonTableFormat As Boolean
    Dim bTableFormat As Boolean
    Dim oPsig As CPleadingSignature

    For Each oPsig In Me
        If oPsig.Definition.NonTableFormat Then
            bNonTableFormat = True
        Else
            bTableFormat = True
        End If
    Next oPsig
    If bNonTableFormat And bTableFormat Then
        ContainMixedFormats = True
    End If
End Function
