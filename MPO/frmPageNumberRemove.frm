VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmPageNumberRemove 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Remove Page Number"
   ClientHeight    =   3840
   ClientLeft      =   48
   ClientTop       =   240
   ClientWidth     =   3420
   Icon            =   "frmPageNumberRemove.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3840
   ScaleWidth      =   3420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraApplyTo 
      Caption         =   "Remove From"
      Height          =   2424
      Left            =   216
      TabIndex        =   0
      Top             =   120
      Width           =   2988
      Begin VB.TextBox txtMultipleSections 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   317
         Left            =   480
         TabIndex        =   4
         Top             =   1212
         Width           =   2220
      End
      Begin VB.OptionButton optMultipleSection 
         Caption         =   "M&ultiple Sections (e.g., 1, 3-5, 7)"
         Height          =   288
         Left            =   240
         TabIndex        =   3
         Top             =   936
         Width           =   2580
      End
      Begin VB.OptionButton optCurrentSection 
         Caption         =   "Current S&ection"
         Height          =   276
         Left            =   240
         TabIndex        =   2
         Top             =   624
         Width           =   2604
      End
      Begin VB.OptionButton optEntireDocument 
         Caption         =   "Entire &Document"
         Height          =   372
         Left            =   240
         TabIndex        =   1
         Top             =   252
         Value           =   -1  'True
         Width           =   2316
      End
      Begin TrueDBList60.TDBCombo cmbRemoveOptions 
         Height          =   540
         Left            =   216
         OleObjectBlob   =   "frmPageNumberRemove.frx":058A
         TabIndex        =   8
         Top             =   1920
         Width           =   2508
      End
      Begin VB.Label lblRemoveOptions 
         BackColor       =   &H00800080&
         BackStyle       =   0  'Transparent
         Caption         =   "&Pages:"
         ForeColor       =   &H80000008&
         Height          =   288
         Left            =   252
         TabIndex        =   7
         Top             =   1704
         Width           =   804
      End
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "&Remove"
      Height          =   400
      Left            =   1128
      TabIndex        =   5
      Top             =   3276
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   2196
      TabIndex        =   6
      Top             =   3276
      Width           =   1000
   End
   Begin VB.Label lblComment 
      Alignment       =   2  'Center
      Caption         =   "Page Numbers will be removed from Headers && Footers only"
      Height          =   444
      Left            =   204
      TabIndex        =   9
      Top             =   2664
      Width           =   2988
   End
End
Attribute VB_Name = "frmPageNumberRemove"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_xTargetedSections As String
Private m_oNum As MPO.CPageNumber

Private Const mpIniSec As String = "PageNumber"

Private Enum mpPageNumberRemoveOption
    mpPageNumberRemoveOption_AllPages = 1
    mpPageNumberRemoveOption_FirstPage = 2
End Enum


Public Property Set PageNumber(oNew As CPageNumber)
    Set m_oNum = oNew
End Property

Public Property Get PageNumber() As CPageNumber
    Set PageNumber = m_oNum
End Property

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Property Get TargetedSections() As String
    TargetedSections = m_xTargetedSections
End Property

Property Let TargetedSections(xNew As String)
    m_xTargetedSections = xNew
End Property


Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    Me.Cancelled = True
End Sub

Private Sub btnOK_Click()
    'validate if necessary
    If Me.optMultipleSection Then
        If Not bValidateMultipleSections() Then
            With Me.txtMultipleSections
                .SetFocus
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
            Exit Sub
        End If
    End If
    Me.Hide
    DoEvents
    SetProperties
    Me.Cancelled = False
End Sub

Private Sub SetProperties()
'sets properties of supplied page number object
    On Error GoTo ProcError
    
    'reset multiple section textbox if necessary
    If Not Me.optMultipleSection Then
        Me.txtMultipleSections = ""
    End If
    
    SaveDefaults
    LoadTargetedSections
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not set page number properties."
    Exit Sub
End Sub

Private Sub LoadTargetedSections()
    Dim xSectionList() As String
    Dim xSections As String
    Dim iSectionIndex As Integer
    Dim i As Integer
    Dim j As Integer
    Dim iCount As Integer
    Dim xMultiple As String
    Dim iPos As Integer
    Dim xStart As String
    Dim xEnd As String
    
    On Error GoTo ProcError
    
    If (Me.optCurrentSection) Then
        xSections = CStr(Word.Selection.Sections(1).Index)
    Else
        If Me.optMultipleSection Then
            xMultiple = Replace(Me.txtMultipleSections, " ", "")
            
            'get array of multiples
            xSectionList() = Split(xMultiple, ",")
            
            'cycle and add to list as necessary
            For i = 0 To UBound(xSectionList())
                If InStr(xSectionList(i), "-") Then
                    iPos = InStr(xSectionList(i), "-")
                    xStart = Mid(xSectionList(i), 1, iPos - 1)
                    If iPos < Len(xSectionList(i)) Then
                        xEnd = Mid(xSectionList(i), iPos + 1)
                        If Val(xStart) And Val(xEnd) Then
                            For j = Val(xStart) To Val(xEnd)
                                'do not add duplicates
                                If Not InStr(xSections, j) Then
                                    xSections = xSections & j & ","
                                End If
                            Next j
                        End If
                    End If
                Else
                    'do not add duplicates
                    If Not InStr(xSections, xSectionList(i)) Then
                        xSections = xSections & xSectionList(i) & ","
                    End If
                End If
            Next i
            
            'cleanup
            xSections = mpBase2.xTrimTrailingChrs(xSections, ",")
        Else
            iCount = ActiveDocument.Sections.Count
            For i = 1 To iCount
                xSections = xSections & CStr(i)
                If i < iCount Then
                    xSections = xSections & ","
                End If
            Next i
        End If
    End If
    
    Me.TargetedSections = xSections
        
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not load targeted sections."
    Exit Sub
End Sub

Private Sub cmbRemoveOptions_GotFocus()
    OnControlGotFocus Me.cmbRemoveOptions
End Sub

Private Sub cmbRemoveOptions_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbRemoveOptions, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbRemoveOptions_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbRemoveOptions)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    GetDefaults
End Sub

Private Sub Form_Load()
    Dim oRemoveOptions As XArray
    
    Set oRemoveOptions = New XArray
    
'   set alignments list
    With oRemoveOptions
        .ReDim 0, 1, 0, 1
        .Value(0, 1) = "All Pages"
        .Value(0, 0) = mpPageNumberRemoveOption_AllPages
        .Value(1, 1) = "First Page"
        .Value(1, 0) = mpPageNumberRemoveOption_FirstPage
    End With
        
'   assign lists to controls
    Me.cmbRemoveOptions.Array = oRemoveOptions
    
'   resize control dropdown
    ResizeTDBCombo Me.cmbRemoveOptions, 2
    
'   reset current section label
    Me.optCurrentSection.Caption = Me.optCurrentSection.Caption & _
            " (Section " & Word.Selection.Sections(1).Index & ")"
    
    Me.Cancelled = True
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmPageNumberRemove.Form_Load"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oPrevControl = Nothing
End Sub

Private Sub optCurrentSection_Click()
    Me.txtMultipleSections.Enabled = Not Me.optCurrentSection
End Sub

Private Sub optEntireDocument_Click()
    Me.txtMultipleSections.Enabled = Not Me.optEntireDocument
End Sub

Private Sub optMultipleSection_Click()
    Me.txtMultipleSections.Enabled = Me.optMultipleSection
    If Me.optMultipleSection Then
        Me.txtMultipleSections.SetFocus
    End If
End Sub

Private Sub GetDefaults()
'   get defaults from ini if they exist
    
    On Error Resume Next
    
    Me.cmbRemoveOptions.BoundText = GetUserIni(mpIniSec, "RemoveOption")
    Me.optEntireDocument.Value = GetUserIni(mpIniSec, "EntireDocument")
    Me.optMultipleSection.Value = GetUserIni(mpIniSec, "MultipleSection")
    Me.optCurrentSection.Value = GetUserIni(mpIniSec, "CurrentSection")
    
    On Error GoTo ProcError
    
'   set default alignment if there are no defaults saved
    If Me.cmbRemoveOptions.BoundText = Empty Then
        Me.cmbRemoveOptions.BoundText = mpPageNumberRemoveOption_AllPages
    End If
       
'   reset scope if necessary
    If Me.optMultipleSection Then
        optEntireDocument.Value = True
    End If
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmPageNumberRemove.GetDefaults"
    Exit Sub
End Sub

Private Sub SaveDefaults()
'   write values to ini as future defaults
    SetUserIni mpIniSec, "RemoveOption", Me.cmbRemoveOptions.BoundText
    SetUserIni mpIniSec, "EntireDocument", Me.optEntireDocument
    SetUserIni mpIniSec, "CurrentSection", Me.optCurrentSection
    SetUserIni mpIniSec, "MultipleSection", Me.optMultipleSection
End Sub

Function bValidateMultipleSections() As Boolean
    'Validates txtMultiple control contents
    
    Dim iSections As Integer
    Dim xMultiple As String
    Dim bValidated As Boolean
    Dim xSectionList() As String
    Dim i As Integer
    Dim iPos As Integer
    Dim j As Integer
    Dim xStart As String
    Dim xEnd As String
    Dim xItem As String
    Dim iItem As Integer
    
    bValidated = True
    
    'get sections count
    iSections = ActiveDocument.Sections.Count
    
    'remove empty spaces
    xMultiple = Replace(Me.txtMultipleSections, " ", "")
            
    'prompt if no section specified
    If (xMultiple = "") Then
        MsgBox "Please specify a section or sections.", _
                vbExclamation, App.Title
        bValidated = False
    Else
        'get array of multiples
        xSectionList() = Split(xMultiple, ",")
        
        For i = 0 To UBound(xSectionList())
            If InStr(xSectionList(i), "-") Then
                iPos = InStr(xSectionList(i), "-")
                xStart = Mid(xSectionList(i), 1, iPos - 1)
                
                'check start section
                If Val(xStart) = 0 Then
                    MsgBox "The specified section does not exist: " & xStart, _
                            vbExclamation, App.Title
                    bValidated = False
                    Exit For
                End If
                
                If iPos < Len(xSectionList(i)) Then
                    xEnd = Mid(xSectionList(i), iPos + 1)
                    
                    'check last section
                    If Val(xEnd) = 0 Then
                        MsgBox "The specified section does not exist: " & xEnd, _
                                vbExclamation, App.Title
                        bValidated = False
                        Exit For
                    End If
                    
                    For j = Val(xStart) To Val(xEnd)
                        If (j > iSections) Or (j = 0) Then
                            MsgBox "The specified section does not exist: " & j, _
                                    vbExclamation, App.Title
                            bValidated = False
                            Exit For
                        End If
                    Next j
                
                End If
            Else
                iItem = Val(xSectionList(i))
                If (iItem > iSections) Or _
                        (iItem = 0) Then
                    MsgBox "The specified section does not exist: " & xSectionList(i), _
                            vbExclamation, App.Title
                    bValidated = False
                End If
            End If
        Next i
    End If
    
    bValidateMultipleSections = bValidated
    
    Exit Function
ProcError:
    RaiseError "MPO.frmPageNumberRemove.bValidateMultipleSections"
    Exit Function
End Function
