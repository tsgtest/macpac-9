VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDepoSummary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Depo Summary
'   created 5/17/00 by Mike Conner -
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define a MacPac depo summary

'   Member of App
'   Container for CDocument
'**********************************************************

Private Const mpSegment As String = "DepoSummary"

'***module vars
Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oProps As MPO.CCustomProperties
Private m_oDefs As mpDB.CDepoSummaryDefs
Private m_bInit As Boolean

Public Event BeforeBoilerplateInsert()
Public Event AfterBoilerplateInsert()

Implements MPO.IDocObj


'**********************************************************
'   Properties
'**********************************************************

Public Property Get Definition() As mpDB.CDepoSummaryDef
    Set Definition = m_oDefs.Item(Me.FormatID)
End Property

Public Property Let DeponentName(xNew As String)
    If Me.DeponentName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "DeponentName", mpSegment, , xNew
        m_oDoc.EditItem "zzmpDeponentName", xNew, , False
    End If
End Property

Public Property Get DeponentName() As String
    On Error Resume Next
    DeponentName = m_oDoc.RetrieveItem("DeponentName", mpSegment)
End Property

Friend Property Get DynamicEditing() As Boolean
    DynamicEditing = GetUserIni("General", "DynamicEditing")
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Definition.BoilerplateFile
End Property

Public Property Let DepositionDate(xNew As String)
    If Me.DepositionDate <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpDepositionDate", xNew, , False
        m_oDoc.SaveItem "DepositionDate", mpSegment, , xNew
    End If
End Property

Public Property Get DepositionDate() As String
    On Error Resume Next
    DepositionDate = m_oDoc.RetrieveItem("DepositionDate", mpSegment)
End Property

Public Property Let ClientMatterNumber(xNew As String)
    If Me.ClientMatterNumber <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpClientMatterNumber", xNew, , False
        m_oDoc.SaveItem "ClientMatterNumber", mpSegment, , xNew
    End If
End Property

Public Property Get ClientMatterNumber() As String
    On Error Resume Next
    ClientMatterNumber = m_oDoc.RetrieveItem("ClientMatterNumber", mpSegment)
End Property

Public Property Let VolumeNumber(xNew As String)
    If Me.VolumeNumber <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpVolumeNumber", xNew, , False
        m_oDoc.SaveItem "VolumeNumber", mpSegment, , xNew
    End If
End Property

Public Property Get VolumeNumber() As String
    On Error Resume Next
    VolumeNumber = m_oDoc.RetrieveItem("VolumeNumber", mpSegment)
End Property

Public Property Let ExaminerName(xNew As String)
    If Me.ExaminerName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "ExaminerName", mpSegment, , xNew
        m_oDoc.EditItem "zzmpExaminerName", xNew, , False
    End If
End Property

Public Property Get ExaminerName() As String
    On Error Resume Next
    ExaminerName = m_oDoc.RetrieveItem("ExaminerName", mpSegment)
End Property

Public Property Let FormatID(lNew As Long)
    If Me.FormatID <> lNew Or g_bForceItemUpdate Then
        SetUserIni "Depo Summary", "FormatID", lNew
    End If
End Property

Public Property Get FormatID() As Long
    On Error Resume Next
    FormatID = Val(GetUserIni("Depo Summary", "FormatID"))
End Property

Public Property Get Initialized() As Boolean
    Initialized = m_bInit
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub SelectItem(xBookmark As String)
    m_oDoc.SelectItem xBookmark
End Sub

Public Function InsertBoilerplate()
    '---Get Format ID
    If Me.FormatID = Empty Then
        Me.FormatID = 1
    End If
    RaiseEvent BeforeBoilerplateInsert
    Me.Template.InsertBoilerplate BoilerplateDirectory() & Me.BoilerplateFile
    RaiseEvent AfterBoilerplateInsert
End Function

Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Function Finish(Optional bForceRefresh As Boolean = True)
'cleans up doc after all user input is complete
    Dim rngP As Range

    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    g_bForceItemUpdate = True
    
'   show all chars-  necessary to guarantee
'   that hidden text is deleted when appropriate
    Word.ActiveDocument.ActiveWindow.View.ShowHiddenText = True
    
    With m_oDoc
        If bForceRefresh Then
            If Me.DynamicEditing = False Then
                '---you must insert bp here because templates db table can't list mbp file for pleading
                Me.Template.BoilerplateFile = Me.Definition.BoilerplateFile
                Me.Template.InsertBoilerplate
            End If
            Refresh
        Else
            RefreshEmpty
        End If
        
'       delete any hidden text - ie text marked for deletion
        .DeleteHiddenText True, True, , True
        .RemoveFormFields
        
'       run editing macro
        RunEditMacro Me.Template
        
        .SelectStartPosition
'       Doc.IsCreated will be TRUE only if Reuse Author is not undefined -
'       since there is no author in depo, dummy reuse author.
        Me.Document.SetVar "ReuseAuthor", "NoAuthor"        '9.7.1 - #4223
        DoEvents
        .ClearBookmarks
        .rngTrimEmptyParagraphs
    End With
    
    g_bForceItemUpdate = False
    Exit Function
ProcError:
    Err.Raise Err.Number
    g_bForceItemUpdate = False
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************


Public Sub Refresh()
'--refresh all properties by setting them to themselves
'--this ensures creation of doc vars
    Dim bForceUpdate As Boolean
    bForceUpdate = g_bForceItemUpdate
    g_bForceItemUpdate = True
    With Me
        .ClientMatterNumber = .ClientMatterNumber
        .DeponentName = .DeponentName
        .DepositionDate = .DepositionDate
        .ExaminerName = .ExaminerName
        .FormatID = .FormatID
        .VolumeNumber = .VolumeNumber
        
'       refresh all custom properties
        .CustomProperties.RefreshValues
    
    End With
    g_bForceItemUpdate = bForceUpdate
End Sub

Public Sub RefreshEmpty()
'   force properties that may not have been
'   set by user to get updated in document
    Dim bForceUpdate As Boolean
    
    bForceUpdate = g_bForceItemUpdate
    g_bForceItemUpdate = True
    
    With Me
'       refresh empty properties of depo
        If .ClientMatterNumber = Empty Then _
            .ClientMatterNumber = Empty
        If .DeponentName = Empty Then _
            .DeponentName = Empty
        If .DepositionDate = Empty Then _
            .DepositionDate = Empty
        If .ExaminerName = Empty Then _
            .ExaminerName = Empty
        If .FormatID = Empty Then _
            .FormatID = Empty
        If .VolumeNumber = Empty Then _
            .VolumeNumber = Empty

'       refresh custom properties
        .CustomProperties.RefreshEmptyValues
    End With
    g_bForceItemUpdate = bForceUpdate
End Sub

Friend Sub Initialize(lFormatID As Long)
    On Error GoTo ProcError
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    Set m_oDefs = g_oDBs.DepoSummaryDefs
    
    On Error Resume Next
    Set m_oTemplate = g_oTemplates.ItemFromClass("CDepoSummary")
    
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidMemoTemplate
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'---set format id
    Me.FormatID = lFormatID
    
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    With m_oDoc
        .Selection.StartOf
    End With
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CDepoSummary.Initialize"
    Exit Sub
End Sub


'**********************************************************
'   Class Event Procedures
'**********************************************************

Private Sub Class_Initialize()
End Sub

Private Sub Class_Terminate()
    Set m_oDefs = Nothing
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
    Set m_oProps = Nothing
End Sub

'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
End Property

Private Property Get IDocObj_Author() As mpDB.CPerson
End Property

Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property

Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function

Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
End Property

Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
End Property

Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function

Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub

Private Sub IDocObj_Initialize()
    Me.Initialize Me.FormatID
End Sub

Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property

Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub

Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub

Private Sub IDocObj_Refresh()
    Refresh
End Sub

Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function

Private Sub IDocObj_UpdateForAuthor()
End Sub


