VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IDocObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   IDocObj Interface Class
'   created 12/15/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define the IDocObj Interface - those
'   properties and methods that all MacPac
'   document objects (e.g. Letter) must have
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Author(oNew As mpDB.CPerson)
End Property

Public Property Get Author() As mpDB.CPerson
End Property

Public Property Get DefaultAuthor() As mpDB.CPerson
End Property

Public Property Let DefaultAuthor(psnNew As mpDB.CPerson)
End Property

Public Property Get Initialized() As Boolean
End Property

Public Property Get BoilerplateFile() As String
End Property

'**********************************************************
'   Methods
'**********************************************************
'Public Sub Initialize()
'End Sub

Public Sub InsertBoilerplate()
End Sub

Public Sub UpdateForAuthor()
End Sub

Public Sub RefreshEmpty()
End Sub

Public Sub Refresh()
End Sub

Public Sub Finish(Optional ByVal bForceRefresh As Boolean = True)
End Sub

Public Function Document() As MPO.CDocument
End Function

Public Function Template() As MPO.CTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
End Function
