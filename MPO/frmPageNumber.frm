VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Begin VB.Form frmPageNumber 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert Page Number"
   ClientHeight    =   4236
   ClientLeft      =   48
   ClientTop       =   240
   ClientWidth     =   5916
   Icon            =   "frmPageNumber.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4236
   ScaleWidth      =   5916
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraApplyTo 
      Caption         =   "Apply To"
      Height          =   1812
      Left            =   2748
      TabIndex        =   11
      Top             =   120
      Width           =   2988
      Begin VB.TextBox txtMultipleSections 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   317
         Left            =   480
         TabIndex        =   15
         Top             =   1212
         Width           =   2220
      End
      Begin VB.OptionButton optMultipleSection 
         Caption         =   "M&ultiple Sections (e.g., 1, 3-5, 7)"
         Height          =   288
         Left            =   240
         TabIndex        =   14
         Top             =   936
         Width           =   2580
      End
      Begin VB.OptionButton optCurrentSection 
         Caption         =   "Current S&ection"
         Height          =   276
         Left            =   240
         TabIndex        =   13
         Top             =   624
         Width           =   2184
      End
      Begin VB.OptionButton optEntireDocument 
         Caption         =   "Entire &Document"
         Height          =   372
         Left            =   240
         TabIndex        =   12
         Top             =   252
         Value           =   -1  'True
         Width           =   2316
      End
   End
   Begin VB.TextBox txtBefore 
      Appearance      =   0  'Flat
      Height          =   317
      Left            =   192
      TabIndex        =   4
      Top             =   1308
      Width           =   2280
   End
   Begin VB.TextBox txtAfter 
      Appearance      =   0  'Flat
      Height          =   317
      Left            =   192
      TabIndex        =   6
      Top             =   1944
      Width           =   2280
   End
   Begin VB.CheckBox chkXofY 
      Appearance      =   0  'Flat
      Caption         =   "&X of Y"
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   252
      TabIndex        =   2
      Top             =   696
      Width           =   756
   End
   Begin VB.Frame fraOptions 
      Caption         =   "Options"
      Height          =   1464
      Left            =   2748
      TabIndex        =   16
      Top             =   2016
      Width           =   2988
      Begin VB.OptionButton optContinueFromPrevious 
         Caption         =   "&Continue from Previous"
         Height          =   300
         Left            =   252
         TabIndex        =   20
         Top             =   996
         Width           =   2256
      End
      Begin VB.OptionButton optStartNumberAt 
         Caption         =   "&Start Number at:"
         Height          =   360
         Left            =   252
         TabIndex        =   18
         Top             =   672
         Value           =   -1  'True
         Width           =   1488
      End
      Begin VB.CheckBox chkFirstPage 
         Appearance      =   0  'Flat
         Caption         =   "Insert on First &Page of Section"
         ForeColor       =   &H80000008&
         Height          =   360
         Left            =   264
         TabIndex        =   17
         Top             =   240
         Width           =   2532
      End
      Begin mpControls3.SpinTextInternational spnStartNumberAt 
         Height          =   300
         Left            =   1752
         TabIndex        =   19
         Top             =   696
         Width           =   624
         _ExtentX        =   1101
         _ExtentY        =   529
         MaxValue        =   45
         Value           =   1
      End
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "&Insert"
      Height          =   400
      Left            =   3672
      TabIndex        =   21
      Top             =   3720
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4740
      TabIndex        =   22
      Top             =   3720
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbNumberFormat 
      Height          =   540
      Left            =   192
      OleObjectBlob   =   "frmPageNumber.frx":058A
      TabIndex        =   1
      Top             =   336
      Width           =   2280
   End
   Begin TrueDBList60.TDBCombo cmbLocation 
      Height          =   540
      Left            =   192
      OleObjectBlob   =   "frmPageNumber.frx":2C31
      TabIndex        =   8
      Top             =   2544
      Width           =   2280
   End
   Begin TrueDBList60.TDBCombo cmbAlignment 
      Height          =   540
      Left            =   192
      OleObjectBlob   =   "frmPageNumber.frx":52D4
      TabIndex        =   10
      Top             =   3168
      Width           =   2280
   End
   Begin VB.Label lblAlignment 
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Align&ment:"
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   228
      TabIndex        =   9
      Top             =   2952
      Width           =   840
   End
   Begin VB.Label lblLocation 
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Location:"
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   228
      TabIndex        =   7
      Top             =   2328
      Width           =   804
   End
   Begin VB.Label lblNumberFormat 
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Format:"
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   228
      TabIndex        =   0
      Top             =   120
      Width           =   756
   End
   Begin VB.Label lblAfter 
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Text &After:"
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   228
      TabIndex        =   5
      Top             =   1716
      Width           =   864
   End
   Begin VB.Label lblBefore 
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Text &Before:"
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   228
      TabIndex        =   3
      Top             =   1080
      Width           =   1008
   End
End
Attribute VB_Name = "frmPageNumber"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_xTargetedSections As String
Private m_oNum As MPO.CPageNumber

Private Const mpIniSec As String = "PageNumber"

Public Property Set PageNumber(oNew As CPageNumber)
    Set m_oNum = oNew
End Property

Public Property Get PageNumber() As CPageNumber
    Set PageNumber = m_oNum
End Property

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Property Get TargetedSections() As String
    TargetedSections = m_xTargetedSections
End Property

Property Let TargetedSections(xNew As String)
    m_xTargetedSections = xNew
End Property


Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    Me.Cancelled = True
End Sub

Private Sub btnOK_Click()
    'validate if necessary
    If Me.optMultipleSection Then
        If Not bValidateMultipleSections() Then
            With Me.txtMultipleSections
                .SetFocus
                .SelStart = 0
                .SelLength = Len(.Text)
            End With
            Exit Sub
        End If
    End If
    Me.Hide
    DoEvents
    SetProperties
    Me.Cancelled = False
End Sub

Private Sub SetProperties()
'sets properties of supplied page number object
    On Error GoTo ProcError
    
    'reset multiple section textbox if necessary
    If Not Me.optMultipleSection Then
        Me.txtMultipleSections = ""
    End If
    
    SaveDefaults
    LoadTargetedSections
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not set page number properties."
    Exit Sub
End Sub

Private Sub LoadTargetedSections()
    Dim xSectionList() As String
    Dim xSections As String
    Dim iSectionIndex As Integer
    Dim i As Integer
    Dim j As Integer
    Dim iCount As Integer
    Dim xMultiple As String
    Dim iPos As Integer
    Dim xStart As String
    Dim xEnd As String
    
    On Error GoTo ProcError
    
    If (Me.optCurrentSection) Then
        xSections = CStr(Word.Selection.Sections(1).Index)
    Else
        If Me.optMultipleSection Then
            xMultiple = Replace(Me.txtMultipleSections, " ", "")
            
            'get array of multiples
            xSectionList() = Split(xMultiple, ",")
            
            'cycle and add to list as necessary
            For i = 0 To UBound(xSectionList())
                If InStr(xSectionList(i), "-") Then
                    iPos = InStr(xSectionList(i), "-")
                    xStart = Mid(xSectionList(i), 1, iPos - 1)
                    If iPos < Len(xSectionList(i)) Then
                        xEnd = Mid(xSectionList(i), iPos + 1)
                        If Val(xStart) And Val(xEnd) Then
                            For j = Val(xStart) To Val(xEnd)
                                'do not add duplicates
                                If Not InStr(xSections, j) Then
                                    xSections = xSections & j & ","
                                End If
                            Next j
                        End If
                    End If
                Else
                    'do not add duplicates
                    If Not InStr(xSections, xSectionList(i)) Then
                        xSections = xSections & xSectionList(i) & ","
                    End If
                End If
            Next i
            
            'cleanup
            xSections = mpBase2.xTrimTrailingChrs(xSections, ",")
        Else
            iCount = ActiveDocument.Sections.Count
            For i = 1 To iCount
                xSections = xSections & CStr(i)
                If i < iCount Then
                    xSections = xSections & ","
                End If
            Next i
        End If
    End If
    
    Me.TargetedSections = xSections
        
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not load targeted sections."
    Exit Sub
End Sub

Private Sub chkFirstPage_GotFocus()
    OnControlGotFocus Me.chkFirstPage
End Sub

Private Sub cmbAlignment_GotFocus()
    OnControlGotFocus Me.cmbAlignment
End Sub

Private Sub cmbAlignment_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAlignment, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAlignment_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbAlignment)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLocation_GotFocus()
    OnControlGotFocus Me.cmbLocation
End Sub

Private Sub cmbLocation_ItemChange()
    Dim bNotAtCursor As Boolean
    Static xAlign As String
    Static iPage1 As Byte
    Static iScope As Byte
    
    bNotAtCursor = _
        (Me.cmbLocation.BoundText <> mpPageNumberLocation_Cursor)
    
    If Me.cmbAlignment.Enabled Then
'       controls are currently enabled - get values
        xAlign = Me.cmbAlignment.BoundText
        iPage1 = Me.chkFirstPage.Value
    Else
'       controls are currently disabled - set values
'       to previously stored values because these
'       controls are about to be enabled - use
'       preset values if no previous values have been stored
        If xAlign <> Empty Then
            Me.cmbAlignment.BoundText = xAlign
            Me.chkFirstPage.Value = iPage1
        Else
            Me.cmbAlignment.BoundText = mpPageNumberLocation_Center
            Me.chkFirstPage.Value = vbChecked
        End If
    End If
    
'   enable or disable controls
    Me.cmbAlignment.Enabled = bNotAtCursor
    Me.chkFirstPage.Enabled = bNotAtCursor
    
'   clear out disabled controls
    If Not Me.cmbAlignment.Enabled Then
        Me.cmbAlignment.Text = ""
        Me.chkFirstPage.Value = False
    End If
End Sub

Private Sub cmbLocation_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLocation, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLocation_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLocation)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbNumberFormat_GotFocus()
    OnControlGotFocus Me.cmbNumberFormat
End Sub

Private Sub cmbNumberFormat_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNumberFormat, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbNumberFormat_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbNumberFormat)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    GetDefaults
End Sub

Private Sub Form_Load()
    Dim oAlignments As XArray
    Dim oLocs As XArray
    Dim oScopes As XArray
    
    Set oAlignments = New XArray
    Set oLocs = New XArray
    Set oScopes = New XArray
    
'   set alignments list
    With oAlignments
        .ReDim 0, 2, 0, 1
        .Value(0, 1) = "Left"
        .Value(0, 0) = mpPageNumberLocation_Left
        .Value(1, 1) = "Center"
        .Value(1, 0) = mpPageNumberLocation_Center
        .Value(2, 1) = "Right"
        .Value(2, 0) = mpPageNumberLocation_Right
    End With
    
'   set location list
    With oLocs
        .ReDim 0, 2, 0, 1
        .Value(0, 1) = "Header"
        .Value(0, 0) = mpPageNumberLocation_Header
        .Value(1, 1) = "Footer"
        .Value(1, 0) = mpPageNumberLocation_Footer
        .Value(2, 1) = "Insertion Point"
        .Value(2, 0) = mpPageNumberLocation_Cursor
    End With
    
'   set scope list
    With oScopes
        .ReDim 0, 1, 0, 1
        .Value(0, 1) = "Current Section"
        .Value(0, 0) = 0
        .Value(1, 1) = "Entire Document"
        .Value(1, 0) = 1
    End With
    
'   set number format list
    Me.cmbNumberFormat.Array = g_oDBs.Lists("PageNumberFormats").ListItems.Source
    
'   assign lists to controls
    Me.cmbAlignment.Array = oAlignments
    Me.cmbLocation.Array = oLocs
'    Me.cmbScope.Array = oScopes
    
'   resize control dropdown
    ResizeTDBCombo Me.cmbAlignment, 3
    ResizeTDBCombo Me.cmbLocation, 3
'    ResizeTDBCombo Me.cmbScope, 2
    ResizeTDBCombo Me.cmbNumberFormat, 6
    
    Me.Cancelled = True
    
'   reposition form
    'mpBase2.MoveToLastPosition Me
    Exit Sub
ProcError:
    RaiseError "MPO.frmPageNumber.Form_Load"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oPrevControl = Nothing
    'mpBase2.SetLastPosition Me
End Sub

Private Sub optCurrentSection_Click()
    Me.txtMultipleSections.Enabled = Not Me.optCurrentSection
End Sub

Private Sub optEntireDocument_Click()
    Me.txtMultipleSections.Enabled = Not Me.optEntireDocument
End Sub

Private Sub optMultipleSection_Click()
    Me.txtMultipleSections.Enabled = Me.optMultipleSection
    If Me.optMultipleSection Then
        Me.txtMultipleSections.SetFocus
    End If
End Sub

Private Sub txtAfter_GotFocus()
    OnControlGotFocus Me.txtAfter
End Sub

Private Sub txtBefore_GotFocus()
    OnControlGotFocus Me.txtBefore
End Sub

Private Sub GetDefaults()
'   get defaults from ini if they exist
    
    On Error Resume Next
    
    Me.cmbAlignment.BoundText = GetUserIni(mpIniSec, "Alignment")
    Me.chkFirstPage.Value = GetUserIni(mpIniSec, "FirstPage")
    Me.chkXofY = GetUserIni(mpIniSec, "XofY")
    Me.cmbLocation.BoundText = GetUserIni(mpIniSec, "Location")
    Me.cmbNumberFormat.BoundText = GetUserIni(mpIniSec, "NumberFormat")
    Me.txtAfter = GetUserIni(mpIniSec, "TextAfter")
    Me.txtBefore = GetUserIni(mpIniSec, "TextBefore")
    Me.optContinueFromPrevious.Value = GetUserIni(mpIniSec, "ContinueFromPrevious")
    Me.optStartNumberAt.Value = GetUserIni(mpIniSec, "StartNumberAt")
    Me.spnStartNumberAt.Value = GetUserIni(mpIniSec, "StartNumber")
    Me.optEntireDocument.Value = GetUserIni(mpIniSec, "EntireDocument")
    Me.optMultipleSection.Value = GetUserIni(mpIniSec, "MultipleSection")
    Me.optCurrentSection.Value = GetUserIni(mpIniSec, "CurrentSection")
    
    On Error GoTo ProcError
    
'   set default alignment if there are no defaults saved
    If Me.cmbAlignment.BoundText = Empty Then
        Me.cmbAlignment.BoundText = mpPageNumberLocation_Center
    End If
    
'   set default Location if there are no defaults saved
    If Me.cmbLocation.BoundText = Empty Then
        Me.cmbLocation.BoundText = mpPageNumberLocation_Footer
    End If
    
'   set default Number Format if there are no defaults saved
    If Me.cmbNumberFormat.BoundText = Empty Then
        Me.cmbNumberFormat.BoundText = mpPageNumberFormat_Arabic
    End If
    
'   set default starting number if there are no defaults saved
    If Me.spnStartNumberAt.Value = Empty Then
        Me.spnStartNumberAt.Value = 1
    End If
    
'   reset scope if necessary
    If Me.optMultipleSection Then
        optEntireDocument.Value = True
    End If
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmPageNumber.GetDefaults"
    Exit Sub
End Sub

Private Sub SaveDefaults()
'   write values to ini as future defaults
    SetUserIni mpIniSec, "Alignment", Me.cmbAlignment.BoundText
    SetUserIni mpIniSec, "FirstPage", Me.chkFirstPage
    SetUserIni mpIniSec, "XofY", Me.chkXofY
    SetUserIni mpIniSec, "Location", Me.cmbLocation.BoundText
    SetUserIni mpIniSec, "NumberFormat", Me.cmbNumberFormat.BoundText
    SetUserIni mpIniSec, "TextAfter", "'" & Me.txtAfter & "'"
    SetUserIni mpIniSec, "TextBefore", "'" & Me.txtBefore & "'"
    SetUserIni mpIniSec, "ContinueFromPrevious", Me.optContinueFromPrevious
    SetUserIni mpIniSec, "StartNumberAt", Me.optStartNumberAt
    SetUserIni mpIniSec, "StartNumber", Me.spnStartNumberAt.Value
    SetUserIni mpIniSec, "EntireDocument", Me.optEntireDocument
    SetUserIni mpIniSec, "CurrentSection", Me.optCurrentSection
    SetUserIni mpIniSec, "MultipleSection", Me.optMultipleSection
End Sub

Function bValidateMultipleSections() As Boolean
    'Validates txtMultiple control contents
    
    Dim iSections As Integer
    Dim xMultiple As String
    Dim bValidated As Boolean
    Dim xSectionList() As String
    Dim i As Integer
    Dim iPos As Integer
    Dim j As Integer
    Dim xStart As String
    Dim xEnd As String
    Dim xItem As String
    Dim iItem As Integer
    
    bValidated = True
    
    'get sections count
    iSections = ActiveDocument.Sections.Count
    
    'remove empty spaces
    xMultiple = Replace(Me.txtMultipleSections, " ", "")
            
    'prompt if no section specified
    If (xMultiple = "") Then
        MsgBox "Please specify a section or sections.", _
                vbExclamation, App.Title
        bValidated = False
    Else
        'get array of multiples
        xSectionList() = Split(xMultiple, ",")
        
        For i = 0 To UBound(xSectionList())
            If InStr(xSectionList(i), "-") Then
                iPos = InStr(xSectionList(i), "-")
                xStart = Mid(xSectionList(i), 1, iPos - 1)
                
                'check start section
                If Val(xStart) = 0 Then
                    MsgBox "The specified section does not exist: " & xStart, _
                            vbExclamation, App.Title
                    bValidated = False
                    Exit For
                End If
                
                If iPos < Len(xSectionList(i)) Then
                    xEnd = Mid(xSectionList(i), iPos + 1)
                    
                    'check last section
                    If Val(xEnd) = 0 Then
                        MsgBox "The specified section does not exist: " & xEnd, _
                                vbExclamation, App.Title
                        bValidated = False
                        Exit For
                    End If
                    
                    For j = Val(xStart) To Val(xEnd)
                        If (j > iSections) Or (j = 0) Then
                            MsgBox "The specified section does not exist: " & j, _
                                    vbExclamation, App.Title
                            bValidated = False
                            Exit For
                        End If
                    Next j
                
                End If
            Else
                iItem = Val(xSectionList(i))
                If (iItem > iSections) Or _
                        (iItem = 0) Then
                    MsgBox "The specified section does not exist: " & xSectionList(i), _
                            vbExclamation, App.Title
                    bValidated = False
                End If
            End If
        Next i
    End If
    
    bValidateMultipleSections = bValidated
    
    Exit Function
ProcError:
    RaiseError "MPO.frmPageNumber.bValidateMultipleSections"
    Exit Function
End Function
