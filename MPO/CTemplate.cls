VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplate Class
'   created 11/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac Template

'   Member of CTemplates
'   Container for
'**********************************************************
Public Enum mpTemplateTypes
    mpTemplateType_MacPac = 1
    mpTemplateType_Workgroup = 2
    mpTemplateType_User = 3
End Enum

Private m_xShortName As String
Private m_xClassName As String
Private m_xFile As String
Private m_xAuthorDefaultsCategory As String
Private m_xDesc As String
Private m_oMappedVariables() As String
Private m_xOptionsTable As String
Private m_xBPFile As String
Private m_xMacro As String
Private m_xEditMacro As String
Private m_xSubFolder As String
Private m_iType As mpTemplateTypes
Private m_lGroup As Long
Private m_lDefDocIDStamp As Long
Private m_oCustProps As CCustomProperties
Private m_oDefAuthor As mpDB.CPerson
Private m_bWriteToBP As Boolean
Private m_oDlgDef As mpDB.CCustomDialogDef
Private m_xProtectedSections As String

'**********************************************************
'   Properties
'**********************************************************
Friend Property Let WriteToBP(bNew As Boolean)
    m_bWriteToBP = bNew
End Property

Friend Property Get WriteToBP() As Boolean
    WriteToBP = m_bWriteToBP
End Property

Public Property Let DefaultAuthor(psnNew As mpDB.CPerson)
    Dim psnTemp As mpDB.CPerson
    Set psnTemp = g_oDBs.SetDefaultAuthor(psnNew.ID, Me.ID)
    Set m_oDefAuthor = psnTemp
End Property

Public Property Get DefaultAuthor() As mpDB.CPerson
'       set the default author to first person if
'       there is no default author
    If m_oDefAuthor Is Nothing Then
        Set m_oDefAuthor = g_oDBs.GetDefaultAuthor(Me.ID)
        
'       set the default author to first person if
'       there is no default author
        If m_oDefAuthor Is Nothing Then
            Me.DefaultAuthor = g_oDBs.People.First
        End If
    Else
        '---9.6.2 - #3670 - we need to always refresh for Input People,
        '---since users may delete a private author set as a default
        '---this would otherwise trigger an error when loading sa grid or author combos
        If m_oDefAuthor.Source = mpPeopleSourceList_PeopleInput Then
            
            Set m_oDefAuthor = g_oDBs.GetDefaultAuthor(Me.ID)
            
    '       set the default author to first person if
    '       there is no default author
            If m_oDefAuthor Is Nothing Then
                Me.DefaultAuthor = g_oDBs.People.First
            End If
        
        End If
    
    End If
    Set DefaultAuthor = m_oDefAuthor
End Property
Friend Property Let ID(xNew As String)
'same as file name
    m_xFile = xNew
End Property

Public Property Get ID() As String
'same as file name
    ID = m_xFile
End Property

Public Property Get BoilerplateVersion() As String
    BoilerplateVersion = GetUserIniNumeric(Me.FileName, "BPVersion")
End Property

Public Property Let BoilerplateVersion(xNew As String)
    SetUserIni Me.FileName, "BPVersion", xNew
End Property

Public Property Get OptionsVersion() As String
    OptionsVersion = GetUserIniNumeric(Me.FileName, "OptionsVersion")
End Property

Public Property Let OptionsVersion(xNew As String)
    SetUserIni Me.FileName, "OptionsVersion", xNew
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Friend Property Let ProtectedSections(xNew As String)
    m_xProtectedSections = xNew
End Property

Public Property Get ProtectedSections() As String
    ProtectedSections = m_xProtectedSections
End Property

Friend Property Let Macro(xNew As String)
    m_xMacro = xNew
End Property

Public Property Get Macro() As String
    Macro = m_xMacro
End Property

Friend Property Let EditMacro(xNew As String)
    m_xEditMacro = xNew
End Property

Public Property Get EditMacro() As String
    EditMacro = m_xEditMacro
End Property

Friend Property Let SubFolder(xNew As String)
    m_xSubFolder = xNew
End Property

Public Property Get SubFolder() As String
    SubFolder = m_xSubFolder
End Property

Friend Property Let OptionsTable(xNew As String)
    m_xOptionsTable = xNew
End Property

Public Property Get OptionsTable() As String
    OptionsTable = m_xOptionsTable
End Property

Friend Property Let ShortName(xNew As String)
    m_xShortName = xNew
End Property

Public Property Get ShortName() As String
    ShortName = m_xShortName
End Property

Friend Property Let ClassName(xNew As String)
    m_xClassName = xNew
End Property

Public Property Get ClassName() As String
    ClassName = m_xClassName
End Property

'**********************************
'DanCore 9.2.0 - Multitype:
Friend Property Let AuthorDefaultsCategory(xNew As String)
    m_xAuthorDefaultsCategory = xNew
End Property

Public Property Get AuthorDefaultsCategory() As String
    AuthorDefaultsCategory = m_xAuthorDefaultsCategory
End Property
'**********************************

Friend Property Let FileName(xNew As String)
'same as id
    m_xFile = xNew
End Property

Public Property Get FileName() As String
'same as id
    FileName = m_xFile
End Property

Public Property Get MappedVariables() As String()
    MappedVariables = m_oMappedVariables()
End Property

Public Property Let MappedVariables(xNew() As String)
    m_oMappedVariables() = xNew()
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get Directory() As String
'returns the path to the template -
'based on template type
    Dim xPath As String
    
    On Error GoTo ProcError
    
    Select Case TemplateType
        Case mpTemplateType_MacPac
            Directory = mpBase2.TemplatesDirectory & IIf(Me.SubFolder <> "", Me.SubFolder, "")     '9.7.1 - #4055
        Case mpTemplateType_Workgroup
            xPath = Word.Application.Options _
                .DefaultFilePath(wdWorkgroupTemplatesPath)
'           raise error if path is not valid
            If Not mpBase2.DirExists(xPath) Then
                Err.Raise mpError_InvalidWorkgroupTemplatesPath
            End If
            Directory = xPath
        Case mpTemplateType_User
            xPath = Word.Application.Options _
                .DefaultFilePath(wdUserTemplatesPath)
'           raise error if path is not valid
            If Not mpBase2.DirExists(xPath) Then
                Err.Raise mpError_InvalidUserTemplatesPath
            End If
            Directory = xPath
    End Select
    Exit Property
ProcError:
    RaiseError "MPO.CTemplate.Directory"
    Exit Property
End Property

Friend Property Let TemplateType(iNew As mpTemplateTypes)
    m_iType = iNew
End Property

Public Property Get TemplateType() As mpTemplateTypes
    TemplateType = m_iType
End Property

Friend Property Let DefaultDocIDStamp(lNew As Long)
    m_lDefDocIDStamp = lNew
End Property

Public Property Get DefaultDocIDStamp() As Long
    DefaultDocIDStamp = m_lDefDocIDStamp
End Property

Friend Property Let Group(lNew As Long)
    m_lGroup = lNew
End Property

Public Property Get Group() As Long
    Group = m_lGroup
End Property

Public Property Get FullName() As String
'returns the file path and name of template
    Dim xTemp As String
    
    xTemp = Me.Directory
    
    '   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If

    FullName = xTemp & Me.FileName
End Property

Public Function UpdateRequired() As Boolean
'   returns TRUE if a new boilerplate has
'   arrived or the default options have been
'   changed and are currently being used by the template
    Dim datLastOptions As Date
    Dim oAuthor As mpDB.CPerson
    Dim bNewTemplate As Boolean
    Dim bNewFirmOptions As Boolean
    Dim oOptions As mpDB.CPersonOptions
    Dim xBP As String
    Dim xDesc As String
    Dim oFSO As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim dblLastEditTime As Double
    Dim dblLastOpenTime As Double
    Dim xOldBPVersion As String
    Dim xNewBPVersion As String
    
    On Error GoTo ProcError
    
    If Not Me.WriteToBP Then
        Exit Function
    End If
'   get default author and author options
    Set oAuthor = Me.DefaultAuthor
    Set oFSO = New Scripting.FileSystemObject
    
'   exit if there is no default author -
'   no update is required
    If oAuthor Is Nothing Then
        Exit Function
    End If
    
    Set oOptions = oAuthor.Options(Me.ID)
    
    xBP = Me.BoilerplateDirectory & Me.BoilerplateFile
    Set oFile = oFSO.GetFile(xBP)
    
'   get last edit and open time of this bp file-
'   the concatenated string is the "version id"
    xNewBPVersion = CDbl(oFile.DateLastModified)
    
'   the old version id was written to the ini when the
'   template defaults were set
    xOldBPVersion = GetUserIniNumeric(Me.ID, "BPVersion")
    
'   an updated template has arrived if two ids don't match
    bNewTemplate = (xNewBPVersion <> xOldBPVersion)
    
'   check for a new set of firm options if the
'   user is currently using firm options
    If oOptions.Count Then
        If Not oOptions.Exists Then
'           get the version (time) of firm options that
'           were last applied to the template
            On Error Resume Next
            datLastOptions = GetUserIniNumeric(Me.ID, "OptionsVersion")
            On Error GoTo ProcError
        
'           determine if firm options have been
'           changed since the last time the the
'           firm options were set to the template
            bNewFirmOptions = _
                CStr(oOptions.LastEditTime) <> CStr(datLastOptions)
        End If
    End If
    
    If bNewTemplate Or bNewFirmOptions Then
        If Not (oAuthor Is Nothing) Then
'           template needs updating - most probably
'           the administrator has distributed a new template
            UpdateRequired = True
        End If
    End If
    Exit Function
    
ProcError:
    If Err.Number = mpError_InvalidBPFileFormat Then
        xDesc = "Invalid Boilerplate File Format.  The file '" & _
            xBP & "' must be saved as a Word Document Template " & _
            "with the extension .mbp."
    ElseIf Err.Number = mpError_InvalidBoilerplateFile Then
        xDesc = "The file '" & xBP & "' could not be found."
    End If
    
    RaiseError "MPO.CTemplate.UpdateRequired", _
               xDesc
    Exit Function
End Function


'**********************************************************
'   Methods
'**********************************************************


Public Sub UpdateDefaults(ByVal lAuthorID As Long, _
                          oDocObject As IDocObj, _
                          Optional ByVal bUpdateCurrentDoc As Boolean = True)
'updates the defaults in the the appropriate bp file
'and template - also updates current doc if specified-
'writes part of the document - author detail and options
    Dim xTemplate As String
    Dim xBP As String
    Dim oFSO As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim bForce As Boolean
    Dim oCurDoc As Word.Document
    Dim oBP As Word.Document
    Dim iWinState As WdWindowState
    Dim oMessage As Object
    Dim bProtected As Boolean
    
    On Error GoTo ProcError
    
    ' Do Nothing if boilerplate is not writable
    If Not Me.WriteToBP Then
        Exit Sub
    End If
    Set oMessage = CreateObject("mpAXE.CStatus")
    With oMessage
        .Title = "Updating Author Preferences"
        .Show , "Updating Author Preferences for template.  Please wait..."
    End With

'   refresh and freeze screen
    Screen.MousePointer = vbHourglass
    Word.Application.ScreenUpdating = False

'   ensure that all changes will be executed
    bForce = g_bForceItemUpdate
    g_bForceItemUpdate = True
    With oDocObject
        If Len(Word.ActiveDocument.Content) = 1 Then
    '       update current document
            .UpdateForAuthor
'            .Document.ClearContent  '#3986
            Dim oSec As Word.Section
            Dim oDoc As New MPO.CDocument
            For Each oSec In ActiveDocument.Sections
                oDoc.ClearSection oSec
            Next oSec
        Else
            .UpdateForAuthor
        End If
    End With
    
    Set oCurDoc = Word.ActiveDocument
    
'   get window state of cur window - after we
'   minimize the bp window below, windows in Word 97
'   end up restored & cascaded
    iWinState = Word.ActiveDocument.ActiveWindow.WindowState
    
'   open the boilerplate
    xBP = Me.BoilerplateDirectory & _
                Me.BoilerplateFile
                
    '#3732 - bug in Word 2003, this line of code triggers
    'you cannot close microsoft office word because a dialog box is open' error
    'see Error trapping
    'GLOG : 5623 : ceh
    DoEvents
    Set oBP = Word.Documents.Open(xBP)
   
    On Error GoTo ProcError
    
    Word.Application.StatusBar = ""
    With oBP.ActiveWindow
        .Caption = "Updating Preferences"
        .WindowState = wdWindowStateMinimize
    End With
        
'   unprotect if necessary
    If oBP.ProtectionType = wdAllowOnlyFormFields Then
        bProtected = True
        oBP.UnProtect
    End If
    
'GLOG : 5623 : ceh
'   make sure status dlg is up
    'v9.8.1010
'    On Error Resume Next
'    Word.Application.Tasks("mpAXE").Activate
'    On Error GoTo ProcError
    
    With oCurDoc.ActiveWindow
        .Activate
'       return to original window state
        .WindowState = iWinState
    End With
        
    With Word.Application
        .ScreenUpdating = True
        .ScreenRefresh
        .ScreenUpdating = False
    End With
    
    oBP.ActiveWindow.Activate
    DoEvents
    
'   update boilerplate file for current
'   author preferences
    With oDocObject
'       delete all doc vars - this keeps
'       the var set clean - any extraneous
'       vars will be removed - .updateForAuthor
'       will write only those vars that are necessary
        .Document.DeleteDocVars ""
        .Author = g_oDBs.People(lAuthorID)
        .UpdateForAuthor
    End With
    DebugOutput "MPO.CTemplate.UpdateDefaults" & vbTab & vbTab & vbTab & "7"
    
'   save bp page setup
    With oBP.Sections(1).PageSetup
        SetUserIni Me.FileName, "LMargin", .LeftMargin
        SetUserIni Me.FileName, "RMargin", .RightMargin
        SetUserIni Me.FileName, "TMargin", .TopMargin
        SetUserIni Me.FileName, "BMargin", .BottomMargin
        SetUserIni Me.FileName, "HDistance", .HeaderDistance
        SetUserIni Me.FileName, "FDistance", .FooterDistance
        SetUserIni Me.FileName, "FirstPageHeader", _
            .DifferentFirstPageHeaderFooter
        SetUserIni Me.FileName, "PHeight", .PageHeight
        SetUserIni Me.FileName, "PWidth", .PageWidth
        SetUserIni Me.FileName, "Orientation", .Orientation
    End With
     DebugOutput "MPO.CTemplate.UpdateDefaults" & vbTab & vbTab & vbTab & "8"
   
'   save bp defaults to ini
    Me.SaveDefaults
    DebugOutput "MPO.CTemplate.UpdateDefaults" & vbTab & vbTab & vbTab & "9"
    
    EchoOn
    Application.ScreenUpdating = True
    DoEvents
    
'   protect if previously protected
    If bProtected Then
        oBP.Protect wdAllowOnlyFormFields
    End If
    
    'GLOG : 5623 : ceh
    oBP.ActiveWindow.Activate
'   save and close boilerplate
    With oDocObject.Document
        .Saved = False
        'GLOG : 5623 : ceh
        DoEvents
        .CloseDoc True
        .DeleteMostRecentFile
    End With
            
    Application.ScreenUpdating = False

'   starting doc is now active - apply updated page setup
    Me.SetPage
    
    Set oFSO = New Scripting.FileSystemObject
    Set oFile = oFSO.GetFile(xBP)
    
'   mark version of boilerplate
    Me.BoilerplateVersion = CDbl(oFile.DateLastModified)

'   if template uses Preferences tables, mark
'   version of firm options that were applied
    If Len(Me.OptionsTable) Then
        Me.OptionsVersion = CDbl(oDocObject.Author _
            .Options(Me.ID).LastEditTime)
    End If
    
'   return to initial force state
    g_bForceItemUpdate = bForce
        
'   close status
    oMessage.Hide
        
    Screen.MousePointer = vbDefault
    Exit Sub
ProcError:
    DebugOutput "MPO.CTemplate.UpdateDefaults" & vbTab & vbTab & vbTab & "Error"
    '#3732 - bug in Word 2003, triggers
    'you cannot close microsoft office word because a dialog box is open' error
    'GLOG : 5623 : ceh
'    If Err.Number = 5479 Then
'        On Error Resume Next
'        Resume
'    End If
    Screen.MousePointer = vbDefault
    EchoOn
    Application.ScreenUpdating = True
    RaiseError "MPO.CTemplate.UpdateDefaults"
    Exit Sub
End Sub


Public Function CustomDialog() As mpDB.CCustomDialogDef
    Set CustomDialog = g_oDBs.CustomDialogDefs.Item(Me.FileName)
End Function

Public Function InsertBoilerplate(Optional ByVal xFile As String, _
    Optional iLocation As mpDocPositions = mpDocPosition_EOF)
    Dim oRange As Word.Range
    Dim xDesc As String
    Dim iInsertStartSection As Integer
    Dim iTempStartSection As Integer
    Dim lTemp As Long
    Dim oDoc As MPO.CDocument
    
    On Error GoTo ProcError
    
    If xFile = Empty And Me.BoilerplateFile <> Empty Then
'       get location of file
        xFile = Me.BoilerplateDirectory & _
                Me.BoilerplateFile
    End If
    
    If xFile = Empty Then
        Exit Function
    End If
    
    If Dir(xFile, vbDirectory) = Empty Then
        xDesc = "Could not find the boilerplate file '" & xFile & "'."
        Err.Raise mpError_InvalidBoilerplateFile
    End If
    
    With Word.ActiveDocument
        .CopyStylesFromTemplate xFile

'       insert file at specified location
        Select Case iLocation
            Case mpDocPosition_BOF
'               insert at start of file
                Set oRange = .Content
                oRange.StartOf
            Case mpDocPosition_EOF
'               insert at end of file
                Set oRange = .Content
                oRange.EndOf
            Case mpDocPosition_Selection
'               insert at current selection
                Set oRange = Word.Selection
                Dim oRangeTmp As Word.Range
                Set oRangeTmp = oRange.Duplicate
                oRangeTmp.EndOf
        End Select
                
        
        iInsertStartSection = oRange.Sections.First.Index
        
        With oRange
'           insert section break to guarantee
'           that headers/footers from bp file come in
            .InsertBreak wdSectionBreakNextPage
            .Move wdCharacter, 1
            
'           insert file
             .InsertFile xFile, , , , False
            .MoveStart wdCharacter, -1
            .Delete
        End With
        
        
        Set oDoc = New CDocument
        
        If Not oDoc.IsCreated Then
'           add default doc vars to doc
            Me.LoadDefaultValues
        End If
        
'       store names of list templates;'*c
'       they may need to be restored after auto text insertion'*c
        Dim oNum As CNumbering '*c
        Set oNum = New CNumbering '*c
        oNum.BackupLTNames ActiveDocument, ActiveDocument '*c

'*      store source bp file name, not whole
'       path - used by reset styles
        oDoc.SetVar "bpfile", mpBase2.xGetFileName(xFile, True)  '9.7.1 - #4223
        .Fields.Update
    End With
    DoEvents
    
    Dim xProtectedSecs As String
    Dim i As Integer
    
    xProtectedSecs = Me.ProtectedSections
    
'    If xProtectedSecs <> "" Then
        With ActiveDocument.Sections
'           protect only those sections specified for
'           template in tblTemplates
            For i = iInsertStartSection To .Count
'               set protection for each section in doc
                .Item(i).ProtectedForForms = _
                    InStr(xProtectedSecs, "|" & i - iInsertStartSection + 1 & "|")
'                .Item(i - iInsertStartSection + 1).ProtectedForForms = _
'                    InStr(xProtectedSecs, "|" & i - iInsertStartSection + 1 & "|")
            Next i
        End With
'    End If
    
    Exit Function
    
ProcError:
    RaiseError "MPO.CTemplate.InsertBoilerplate", xDesc
    Exit Function
End Function

Public Function CustomProperties() As CCustomProperties
    On Error GoTo ProcError
    If m_oCustProps Is Nothing Then
        Set m_oCustProps = New CCustomProperties
        If Me.ID = Empty Then
            Err.Raise mpError_InvalidTemplateName
        End If
        m_oCustProps.Category = Me.ID
    End If
    Set CustomProperties = m_oCustProps
    Exit Function
ProcError:
    RaiseError "MPO.CTemplate.CustomProperties"
    Exit Function
End Function

Public Function Exists() As Boolean
    Exists = (Dir(Me.FullName) <> "")
End Function

Public Sub Retrieve()
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If Me.Exists Then
        Word.Documents.Open Me.FullName, , , False
    Else
        Err.Raise mpError_TemplateNotFound
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CTemplate.Retrieve"
    Exit Sub
End Sub

Friend Sub CreateNewDocument()
'creates a new document based on the Template
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If Me.Exists Then
        Word.Documents.Add Me.FullName, False
        Word.Application.Run Me.Macro
    Else
        Err.Raise mpError_TemplateNotFound
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CTemplate.CreateNewDocument"
    Exit Sub
End Sub

Public Sub CreateNewTemplate()
'creates a new template based on the Template
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If Me.Exists Then
        mpVer.AddDocument Me.FullName, True, True
    Else
        Err.Raise mpError_TemplateNotFound
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CTemplate.CreateNewTemplate"
    Exit Sub
End Sub

Public Sub SaveDefaults()
'saves doc var defaults of active document to User.Ini
    Dim oVar As Word.Variable
    Dim i As Integer
    Dim xDef As String
    Dim xFile As String
    Dim oDoc As MPO.CDocument
    Dim xVarValue As String
    
    On Error GoTo ProcError
    
    xFile = Me.FileName
    
'   clear out ini keys
    Do
        xDef = Empty
        
'       get ini value
        xDef = GetUserIni(xFile, "Def" & i)
        
'       if not empty...
        If Len(xDef) Then
'           clear doc var
            mpBase2.DeleteUserIniKey xFile, "Def" & i
            
'           increment counter
            i = i + 1
        End If
    Loop While Len(xDef)
    
    i = 0
    
    Set oDoc = New MPO.CDocument
    
    For Each oVar In Word.ActiveDocument.Variables
        xVarValue = oDoc.GetVar(oVar.Name)
        
'       replace new line chrs with tokens
        xVarValue = Replace(xVarValue, vbCrLf, "<1310>")
        xVarValue = Replace(xVarValue, vbCr, "<13>")
        xVarValue = Replace(xVarValue, Chr(11), "<11>")
        
        SetUserIni Me.FileName, "Def" & i, _
            oVar.Name & "||" & xVarValue
        i = i + 1
    Next
    
    Exit Sub
ProcError:
    RaiseError "MPO.CTemplate.SaveDefaults"
    Exit Sub
End Sub

Public Sub LoadDefaultValues()
'loads default values into active document -
'useful to get any vars and page setup values
'into an "attach to" document - eg attaching a
'letter to Normal. skips vars that already have values.
    Dim xDef As String
    Dim xName As String
    Dim xValue As String
    Dim lPos As Long
    Dim i As Integer
    Dim oVar As Word.Variable
    Dim xTest As String
'CharlieCore 9.2.0 - Encrypt
    Dim oDoc As MPO.CDocument
    Dim lTemp As Long
    
    
    On Error GoTo ProcError
    
    Set oDoc = New MPO.CDocument
    lTemp = mpBase2.CurrentTick
    Do
        xDef = Empty
        
'       get ini value
        xDef = GetUserIni(Me.FileName, "Def" & i)
        
'       if not empty...
        If Len(xDef) Then
'           parse into name and value
            lPos = InStr(xDef, "||")
            xName = Left(xDef, lPos - 1)
            On Error Resume Next
            
'           error will be generated if var doesn't exist
            xTest = Word.ActiveDocument.Variables.Item(xName)
            
            If Err Then
                On Error GoTo 0
'               create doc var
                xValue = Mid(xDef, lPos + 2)
                
'               replace new line chrs with tokens
                xValue = Replace(xValue, "<1310>", vbCrLf)
                xValue = Replace(xValue, "<13>", vbCr)
                xValue = Replace(xValue, "<11>", Chr(11))
                
                oDoc.SetVar xName, xValue
            Else
                On Error GoTo 0
            End If
            
'           increment counter
            i = i + 1
        End If
    Loop While Len(xDef)
    DebugOutput "MPO.CTemplate.LoadDefaultValues - Loop" & vbTab & vbTab & vbTab & mpBase2.ElapsedTime(lTemp)
    lTemp = mpBase2.CurrentTick
    SetPage
    DebugOutput "MPO.CTemplate.LoadDefaultValues - SetPage" & vbTab & vbTab & vbTab & mpBase2.ElapsedTime(lTemp)
    Exit Sub
ProcError:
    RaiseError "MPO.CTemplate.LoadDefaultValues"
    Exit Sub
End Sub


Public Sub SetPage()
'   set up active document based
'   on values in user.ini
    Dim sLM As Single
    Dim sRM As Single
    Dim sTM As Single
    Dim sBM As Single
    Dim sHD As Single
    Dim sFD As Single
    Dim sPH As Single
    Dim sPW As Single
    Dim lOrient As Long
    Dim xDiffPage1 As String
    Dim bEcho As Boolean
    Dim oSec As Word.Section
    
    With Word.Application
'        bEcho = .ScreenUpdating
'        .ScreenUpdating = False
    End With
    
    On Error Resume Next
    sLM = GetUserIniNumeric(Me.FileName, "LMargin")
    sRM = GetUserIniNumeric(Me.FileName, "RMargin")
    sTM = GetUserIniNumeric(Me.FileName, "TMargin")
    sBM = GetUserIniNumeric(Me.FileName, "BMargin")
    sHD = GetUserIniNumeric(Me.FileName, "HDistance")
    sFD = GetUserIniNumeric(Me.FileName, "FDistance")
    xDiffPage1 = GetUserIni(Me.FileName, "FirstPageHeader")
    sPH = GetUserIniNumeric(Me.FileName, "PHeight")
    sPW = GetUserIniNumeric(Me.FileName, "PWidth")
    lOrient = GetUserIni(Me.FileName, "Orientation")
    
    On Error GoTo ProcError
    For Each oSec In Word.ActiveDocument.Sections
        With oSec.PageSetup
            If sLM Then _
                .LeftMargin = sLM
            If sRM Then _
                .RightMargin = sRM
            If sTM Then _
                .TopMargin = sTM
            If sBM Then _
                .BottomMargin = sBM
            If sHD Then _
                .HeaderDistance = sHD
            If sFD Then _
                .FooterDistance = sFD
            If sPH Then _
                .PageHeight = sPH
            If sPW Then _
                .PageWidth = sPW
            If lOrient <> Empty Then _
                .Orientation = lOrient
            If xDiffPage1 <> Empty Then _
                .DifferentFirstPageHeaderFooter = CBool(xDiffPage1)
        End With
    Next oSec
    
    Exit Sub
ProcError:
    RaiseError "MPO.CTemplate.SetPage"
    Exit Sub
End Sub

Public Function CreateTemplateVars(xSourceTemplateName As String, xSourceClassName As String) As Boolean  '9.7.1 - #3913
'creates those variables specified in tblTemplateMappings, and
'fills them with the specified values
    Dim aVars() As String
    Dim xDesc As String
    Dim i As Integer
    Dim oSourceVar As Word.Variable
    Dim oDestVar As Word.Variable
    Dim oDoc As MPO.CDocument
    Dim bTemplateVarsCreated As Boolean
    Dim iPos As Integer
    Dim xValue As String
    Dim xTemp As String
    
    On Error GoTo ProcError
    
    Set oDoc = New MPO.CDocument
    
    aVars = Me.MappedVariables()
    
    If aVars(0, 0) = "" Then
'       there are no vars
        Exit Function
    End If
    
    DoEvents
    
    For i = 0 To UBound(aVars())
        On Error Resume Next
        If (UCase(xSourceTemplateName) = UCase(aVars(i, 0))) Or _
           (UCase(xSourceClassName) = UCase(aVars(i, 0))) Then
                Set oSourceVar = oDoc.File.Variables(aVars(i, 2))
                xTemp = oDoc.GetVar(aVars(i, 2))
                If Err = 0 Then
                    On Error GoTo ProcError
            '       get delimiter position
                    iPos = InStr(xTemp, "??")
                    If iPos Then
            '           everything to the right of
            '           the delimiter is the text
                        xValue = Mid(xTemp, iPos + Len("??"))
                    Else
                        xValue = xTemp
                    End If
                    oDoc.SetVar aVars(i, 1), xValue
                Else
                '   get default value
                    oDoc.SetVar aVars(i, 1), aVars(i, 3)
                    On Error GoTo ProcError
                End If
                bTemplateVarsCreated = True
        End If
    Next i
    
    If bTemplateVarsCreated Then
        Me.LoadDefaultValues
    End If
    
    CreateTemplateVars = bTemplateVarsCreated
    
    Exit Function
ProcError:
    RaiseError "MPO.CTemplate.CreateTemplateVars", xDesc
End Function

Friend Function BoilerplateDirectory()
    If Me.WriteToBP Then
        BoilerplateDirectory = mpBase2.UserBoilerplateDirectory
    Else
        BoilerplateDirectory = mpBase2.BoilerplateDirectory
    End If
End Function

Private Sub Class_Terminate()
    Set m_oCustProps = Nothing
    Set m_oDlgDef = Nothing
End Sub

