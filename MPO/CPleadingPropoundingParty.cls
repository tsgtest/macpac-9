VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingPropoundingParty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   PleadingPropoundingParty Class
'   created 5/30/04 by Charlie Homo

'   Contains properties and methods that
'   define MacPac PleadingPropoundingParty information

'   Container for CDocument
'**********************************************************

Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oProps As MPO.CCustomProperties
'Private m_oDef As mpDB.CPleadingSeparateStatementDef
Private m_bInit As Boolean

Public Event BeforeBoilerplateInsert()
Public Event AfterBoilerplateInsert()

Const mpDocSegment As String = "PleadingPropoundingParty"
Const mpSepTableBookmark As String = "zzmpPropoundingPartyTable"


Implements MPO.IDocObj
'**********************************************************
'   Properties
'**********************************************************
Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Property Get PropoundingPartyName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PropoundingPartyName", mpDocSegment)
    PropoundingPartyName = xTemp
End Property
Public Property Let PropoundingPartyName(xNew As String)
    If Me.PropoundingPartyName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpPropoundingPartyName", xNew, , False
        m_oDoc.SaveItem "PropoundingPartyName", mpDocSegment, , xNew
    End If
End Property

Public Property Get RespondingPartyName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("RespondingPartyName", mpDocSegment)
    RespondingPartyName = xTemp
End Property
Public Property Let RespondingPartyName(xNew As String)
    If Me.RespondingPartyName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpRespondingPartyName", xNew, , False
        m_oDoc.SaveItem "RespondingPartyName", mpDocSegment, , xNew
    End If
End Property

Public Property Get PropoundingPartyTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PropoundingPartyTitle", mpDocSegment)
    PropoundingPartyTitle = xTemp
End Property
Public Property Let PropoundingPartyTitle(xNew As String)
    If Me.PropoundingPartyTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpPropoundingPartyTitle", xNew, , False
        m_oDoc.SaveItem "PropoundingPartyTitle", mpDocSegment, , xNew
    End If
End Property

Public Property Get RespondingPartyTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("RespondingPartyTitle", mpDocSegment)
    RespondingPartyTitle = xTemp
End Property
Public Property Let RespondingPartyTitle(xNew As String)
    If Me.RespondingPartyTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpRespondingPartyTitle", xNew, , False
        m_oDoc.SaveItem "RespondingPartyTitle", mpDocSegment, , xNew
    End If
End Property

Public Property Get SetNo() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SetNo", mpDocSegment)
    SetNo = xTemp
End Property

Public Property Let SetNo(xNew As String)
    If Me.SetNo <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSetNo", xNew, , True
        m_oDoc.SaveItem "SetNo", mpDocSegment, , xNew
    End If
End Property

'**********************************************************
'   Methods
'**********************************************************

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Template.BoilerplateFile
End Property

Public Sub Refresh()
    On Error GoTo ProcError
    With Me
        .PropoundingPartyName = .PropoundingPartyName
        .RespondingPartyName = .RespondingPartyName
        .PropoundingPartyTitle = .PropoundingPartyTitle
        .RespondingPartyTitle = .RespondingPartyTitle
        .SetNo = .SetNo
        .CustomProperties.RefreshValues
    End With
    Exit Sub
ProcError:
    RaiseError "CPleadingPropoundingParty.Refresh"
    Exit Sub
End Sub
Public Sub RefreshEmpty()
    Dim i As Integer
    Dim oDef As mpDB.CPleadingSeparateStatementDef
    On Error GoTo ProcError
    With Me
        If .PropoundingPartyName = Empty Then
            .PropoundingPartyName = .PropoundingPartyName
        End If
        If .RespondingPartyName = Empty Then
            .RespondingPartyName = .RespondingPartyName
        End If
        If .PropoundingPartyTitle = Empty Then
            .PropoundingPartyTitle = .PropoundingPartyTitle
        End If
        If .RespondingPartyTitle = Empty Then
            .RespondingPartyTitle = .RespondingPartyTitle
        End If
        If .SetNo = Empty Then
            .SetNo = .SetNo
        End If
        
        .CustomProperties.RefreshEmptyValues
    End With
    Exit Sub
ProcError:
    RaiseError "CPleadingPropoundingParty.RefreshEmpty"
    Exit Sub
End Sub
Public Sub Finish(Optional bForceRefresh As Boolean = False)
    Dim rngP As Range
'    Dim oDef As mpDB.CPleadingSeparateStatementDef
    On Error GoTo ProcError
    
    g_bForceItemUpdate = True
    With m_oDoc
        If bForceRefresh Then
            Refresh
        Else
            RefreshEmpty
        End If

        ApplyTableBorders False

        .UpdateFields
               
'       delete any hidden text - ie text marked for deletion
       .DeleteHiddenText , , , True
       .RemoveFormFields
        
       .ClearBookmarks , .bHideFixedBookmarks
       
    End With
    
    g_bForceItemUpdate = False
    Exit Sub
ProcError:
    RaiseError "CPleadingPropoundingParty.Finish"
        g_bForceItemUpdate = False
End Sub


Friend Sub Initialize()
    On Error GoTo ProcError
    
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidVerificationTemplate
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "CPleadingPropoundingParty.Initialize"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Public Function InsertBoilerplate()
    On Error GoTo ProcError
    RaiseEvent BeforeBoilerplateInsert
    If ActiveDocument.Bookmarks.Exists(mpSepTableBookmark) Then
        ActiveDocument.StoryRanges(wdMainTextStory).Delete
    End If
    Me.Template.InsertBoilerplate BoilerplateDirectory() & Me.BoilerplateFile
    RaiseEvent AfterBoilerplateInsert
    Exit Function
ProcError:
    RaiseError "CPleadingPropoundingParty.InsertBoilerplate"
    Exit Function
End Function

Private Function ApplyTableBorders(bShow As Boolean)
    On Error GoTo ProcError
    '*c - this is for testing
    With Me.Document.File.Bookmarks(mpSepTableBookmark).Range.Tables(1).Rows.Borders
        .OutsideLineStyle = wdLineStyleNone
        .InsideLineStyle = wdLineStyleNone
    End With
    Exit Function
ProcError:
    RaiseError "MPO.CPleadingPropoundingParty.ApplyTableBorders"
    Exit Function
End Function

'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    
    Set m_oTemplate = g_oTemplates.ItemFromClass("CPleadingPropoundingParty")
End Sub
Private Sub Class_Terminate()
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
End Sub
'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
End Property
Private Property Get IDocObj_Author() As mpDB.CPerson
End Property
Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property
Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function
Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
End Property
Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
End Property
Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function
Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub
Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property
Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub
Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub
Private Sub IDocObj_Refresh()
    Refresh
End Sub
Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function
Private Sub IDocObj_UpdateForAuthor()
End Sub
