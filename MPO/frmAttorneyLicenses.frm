VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Begin VB.Form frmAttorneyLicenses 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Manage Attorney Bar IDs"
   ClientHeight    =   3000
   ClientLeft      =   48
   ClientTop       =   240
   ClientWidth     =   5448
   Icon            =   "frmAttorneyLicenses.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   5448
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnDelete 
      Appearance      =   0  'Flat
      Caption         =   "&Delete ID"
      Height          =   400
      Left            =   1590
      TabIndex        =   5
      Top             =   2445
      Width           =   1035
   End
   Begin VB.CommandButton btnCancel 
      Appearance      =   0  'Flat
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4350
      TabIndex        =   6
      Top             =   2445
      Width           =   1000
   End
   Begin VB.CommandButton btnSave 
      Appearance      =   0  'Flat
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   3240
      TabIndex        =   4
      Top             =   2445
      Width           =   1000
   End
   Begin TrueDBGrid60.TDBGrid grdInput 
      Height          =   1455
      Left            =   1590
      OleObjectBlob   =   "frmAttorneyLicenses.frx":058A
      TabIndex        =   3
      Top             =   720
      Width           =   3780
   End
   Begin TrueDBList60.TDBCombo cmbAuthor 
      Height          =   600
      Left            =   1590
      OleObjectBlob   =   "frmAttorneyLicenses.frx":3E40
      TabIndex        =   1
      Top             =   150
      Width           =   3780
   End
   Begin VB.Label lblInput 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Bar &IDs:"
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   165
      TabIndex        =   2
      Top             =   750
      Width           =   1170
   End
   Begin VB.Label lblAuthor 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Author:"
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   300
      TabIndex        =   0
      Top             =   195
      Width           =   1035
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      FillColor       =   &H00800080&
      Height          =   9000
      Left            =   -285
      Top             =   -225
      Width           =   1800
   End
End
Attribute VB_Name = "frmAttorneyLicenses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_xAR As XArrayObject.XArray
Private m_oPerson As mpDB.CPerson
Private m_xParent As String
Private m_bCancelled As Boolean

Public Property Get LicenseArray() As XArray
    Set LicenseArray = m_xAR
End Property

Public Property Let LicenseArray(xarNew As XArray)
    Set m_xAR = xarNew
End Property

Public Property Get Parent() As String
    Parent = m_xParent
End Property

Public Property Let Parent(xNew As String)
    m_xParent = xNew
End Property

Public Property Let Attorney(oNew As mpDB.CPerson)
    Set m_oPerson = oNew
End Property

Public Property Get Attorney() As mpDB.CPerson
    Set Attorney = m_oPerson
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
End Sub

Private Sub btnDelete_Click()
    DeleteRecord grdInput.Row
End Sub

Private Sub btnSave_Click()
    Dim i As Integer
    
    If cmbAuthor.BoundText = "" And Me.Parent = "" Then
        MsgBox "Please select an author from the list.", vbInformation, "Manage Attorney License IDs"
        cmbAuthor.SetFocus
        Exit Sub
    End If

    grdInput.Update
    
    If Not bValidateInput Then Exit Sub  '*** jts 9.7.1 - #4069
    
    '9.7.1 - #2182 - automatically set a single license as the default
    If grdInput.Array.UpperBound(1) = 0 Then
        grdInput.Array.Value(0, 3) = -1
    End If
    
'---update collection if there's a loaded person
    If Not m_oPerson.FullName = "" Then
        With m_oPerson.Licenses
            '---clear collection
            'm_oPerson.Licenses.ListSource.ReDim 0, -1, 0, 3
            m_oPerson.Licenses.DeleteAll
            For i = 0 To grdInput.Array.UpperBound(1)
            '---add elements from grid -- lucky it's a clone!
                With grdInput.Array
                    m_oPerson.Licenses.Add .Value(i, 1), _
                                           .Value(i, 2), _
                                           CStr(CBool(.Value(i, 3)))
                End With
            Next i
        End With
    End If
    Me.LicenseArray = grdInput.Array
    
'---update db only if run standalone
    If Me.Parent = "" Then
        With g_oDBs.InputPeople
            .Save
        End With
    End If
    Me.Hide
End Sub

Private Sub cmbAuthor_ItemChange()
    Attorney = g_oDBs.InputPeople.Item(Me.cmbAuthor.BoundText)
    LoadInputGrid m_oPerson
End Sub

Private Sub Form_Load()

'---load grid for manual input
    Set m_xAR = New XArray
    With m_xAR
        .ReDim 0, 6, 0, 3
    End With
    
    With grdInput
        .Array = m_xAR
        .Rebind
    End With
    
    grdInput.EditActive = True
    
'---load IDs into grid if they exists
    
    If Me.Parent = "" Then
        If m_oPerson.Source = mpPeopleSourceList_PeopleInput Then
            g_oDBs.InputPeople.Refresh
            If g_oDBs.InputPeople.Count = 0 Then
                SetInterface False
                LoadInputGrid m_oPerson
            Else
                SetInterface True
                Me.cmbAuthor.Array = g_oDBs.InputPeople.ListSource
                ResizeTDBCombo cmbAuthor, 6
            End If
        Else
            SetInterface False
            LoadInputGrid m_oPerson
        End If
    Else
        SetInterface Me.Parent = ""
        LoadInputGrid Attorney
    End If


End Sub

'**********************************************
'---methods
'**********************************************

Public Sub ChangeDefault(iVal As Integer)
    Dim i As Integer
    Dim iSel As Integer
    Dim iMarq As Integer
    On Error Resume Next
    iSel = grdInput.Bookmark
    With grdInput
        iMarq = .MarqueeStyle
        .MarqueeStyle = dbgNoMarquee
        If IsNull(.Bookmark) Then Exit Sub
        For i = 0 To .Array.UpperBound(1)
            .Array.Value(i, 3) = 0
        Next i
        If Not iVal Then .Array.Value(iSel, 3) = -1
        .Rebind
        .Col = 3
        .MarqueeStyle = iMarq
        .FirstRow = 0
    End With
End Sub


Public Sub SetInterface(bStandalone As Boolean)
    Dim lAdjust As Long
    
    cmbAuthor.Visible = bStandalone
    lblAuthor.Visible = bStandalone
    
    If Not bStandalone Then
        Me.Caption = Me.Caption & " -- " & Me.Attorney.FullName
        lAdjust = grdInput.Top - cmbAuthor.Top
        
        lblInput.Top = lblAuthor.Top
        grdInput.Top = cmbAuthor.Top
    
        btnCancel.Top = btnCancel.Top - lAdjust
        btnSave.Top = btnSave.Top - lAdjust
        btnDelete.Top = btnDelete.Top - lAdjust
        Me.Height = Me.Height - lAdjust
    End If
End Sub

Private Sub LoadInputGrid(oPerson As mpDB.CPerson)
    Dim xAR As XArray
    Dim i As Integer
    Dim j As Integer
    Set xAR = New XArray
    On Error Resume Next
    
    If oPerson.Licenses.ListSource Is Nothing Then Exit Sub
'---we must load grid with clone of collection listsource to enable user to cancel transaction without changing collection
    With oPerson.Licenses.ListSource
        xAR.ReDim .LowerBound(1), .UpperBound(1), .LowerBound(2), .UpperBound(2)
        For i = .LowerBound(1) To .UpperBound(1)
            For j = .LowerBound(2) To .UpperBound(2)
 '---this deals with a quirk when called from input form, click okay, then called again from same input form
                If .Value(i, j) = "True" Then
                    xAR.Value(i, j) = -1
                ElseIf .Value(i, j) = "False" Then
                    xAR.Value(i, j) = 0
                Else
                    xAR.Value(i, j) = .Value(i, j)
                End If
            Next j
        Next i
    End With
    
    grdInput.Array = xAR
    grdInput.Rebind
    
'---enable controls in context
    btnDelete.Enabled = (oPerson.Source = mpPeopleSourceList_PeopleInput)
    btnSave.Enabled = btnDelete.Enabled
    
End Sub

Private Sub DeleteRecord(lRow As Long)
    Dim i As Integer
    On Error Resume Next
    With grdInput.Array
        .Delete 1, lRow
        If .UpperBound(1) = -1 Then
            .ReDim 0, 1, 0, 3
            .Value(0, 3) = 0
            .Value(1, 3) = 99
        End If
    End With
    grdInput.Rebind
    grdInput.FirstRow = 0
End Sub


Private Sub grdInput_GotFocus()
    grdInput.TabAction = 2
    grdInput.WrapCellPointer = True
End Sub

Private Sub grdInput_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim iVal As Integer
    Dim iRow As Integer
    iRow = grdInput.Row
    iVal = Val(xNullToString(grdInput.Columns(3).Value))
    If KeyCode = 9 Then
        With grdInput
            If .Col = 3 And .Row = .Array.UpperBound(1) Then
                .MoveLast
            ElseIf .Col = 2 And .Row = 0 And Not iVal Then
                .Update
                If .Columns(3).Value = Empty Then
                    ChangeDefault iVal
                End If
                .Col = 2
            ElseIf .Col = 2 Then
                .Update
                .Row = iRow
                If .Columns(3).Value = Empty And iRow <= .Array.UpperBound(1) Then
                    .Array.Value(iRow, 3) = 0
                End If
                .Rebind
                .Col = 2
                .FirstRow = 0
            ElseIf .Col = 1 Then
                .Update
                .Row = iRow
            Else
                .Update
            End If
        End With
    ElseIf KeyCode = vbKeySpace Then
        If grdInput.Col = 3 Then
            DoEvents
            ChangeDefault iVal
        End If
    Else
        If grdInput.Col = 3 Then
            Select Case KeyCode
                Case vbKeyUp, vbKeyDown, vbKeyRight, vbKeyLeft
                    '---let it go
                Case vbKeyReturn
                    grdInput.MoveNext
                Case Else
                    DoEvents
                    With grdInput
                        If .Columns(3).Value <> Empty Then
                            .Array.Value(.Row, 3) = .Array.Value(.Row, 3)
                            .Rebind
                            .Col = 3
                            .FirstRow = 0
                        End If
                    End With
            End Select
        End If
    End If
End Sub

Private Sub grdInput_LostFocus()
    grdInput.Update
End Sub

Private Sub grdInput_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Dim overRow As Long
    Dim overCol As Long
    Dim i As Integer
    Dim iVal As Integer
    On Error Resume Next
'---updates checkbox col
    With grdInput
        .Update
        overRow = .RowContaining(Y)
        grdInput.Row = overRow
        If overRow >= 0 Then grdInput.Row = overRow
        If .ColContaining(x) = 3 Then
'            DoEvents
            iVal = Val(xNullToString(grdInput.Columns(3).Value))
            ChangeDefault iVal
'---be careful here -- setting focus to an invisible column effectively disables control for input!
        End If
    End With
End Sub

Private Function bValidateInput() As Boolean
    '*** jts 9.7.1 - #4069
    Dim i As Integer
        
    With grdInput
        For i = 0 To .Array.UpperBound(1)
            If .Array.Value(i, 1) = "" Then
                MsgBox "Description is required information.", vbInformation, App.Title
                grdInput.SetFocus
                grdInput.Bookmark = i
                grdInput.Col = 1
                bValidateInput = False
                Exit Function
            ElseIf .Array.Value(i, 2) = "" Then
                MsgBox "License/BarID is required information.", vbInformation, App.Title
                grdInput.SetFocus
                grdInput.Bookmark = i
                grdInput.Col = 2
                bValidateInput = False
                Exit Function
            End If
        Next i
    End With
    bValidateInput = True
End Function

