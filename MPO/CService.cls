VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Certificate/Proof of Service Class
'   created 5/16/00 by Jeffrey Sweetland

'   Contains properties and methods that
'   define a MacPac Certificate/Proof of Service

'   Container for CDocument, CServiceList, CPleadingPaper
'**********************************************************

Private m_oServiceList As MPO.CServiceList
Private m_oTemplate As MPO.CTemplate
Private m_oDoc As MPO.CDocument
Private m_oProps As MPO.CCustomProperties
Private m_oPaper As MPO.CPleadingPaper
Private m_oDef As mpDB.CServiceDef
Private m_oAuthor As mpDB.CPerson
Private m_oOffice As mpDB.COffice
Private m_bInit As Boolean
Private m_bNoServiceTitle As Boolean
Private m_bESignature As Boolean

Public Event BeforeBoilerplateInsert()
Public Event AfterBoilerplateInsert()
Public Event AfterDocumentTitlesSet()

Const mpDocSegment As String = "Service"
Implements MPO.IDocObj
'**********************************************************
'   Properties
'**********************************************************
Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function

Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oProps
End Function

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Property Get NoServiceTitle() As Boolean
    Dim vTemp As Variant
    
    On Error Resume Next
    vTemp = m_oDoc.RetrieveItem( _
        "NoServiceTitle", mpDocSegment, , mpItemType_Boolean)
    If vTemp <> mpUndefined Then _
        NoServiceTitle = vTemp
End Property

Public Property Let NoServiceTitle(bNew As Boolean)
    If Me.NoServiceTitle <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "NoServiceTitle", mpDocSegment, , bNew
    End If
End Property

Public Property Get ESignature() As Boolean
    Dim vTemp As Variant
    
    On Error Resume Next
    vTemp = m_oDoc.RetrieveItem( _
        "ESignature", mpDocSegment, , mpItemType_Boolean)
    If vTemp <> mpUndefined Then _
        ESignature = vTemp
End Property

Public Property Let ESignature(bNew As Boolean)
    If Me.ESignature <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "ESignature", mpDocSegment, , bNew
    End If
End Property

Public Property Get DefaultAuthor() As mpDB.CPerson
    Set DefaultAuthor = Me.Template.DefaultAuthor
End Property
Public Property Let DefaultAuthor(psnNew As mpDB.CPerson)
   Me.Template.DefaultAuthor = psnNew
End Property
Public Property Let Author(oNew As mpDB.CPerson)
    Dim bDo As Boolean
    
    On Error GoTo ProcError
    If oNew Is Nothing Then Exit Property
'   set property if no author currently exists
'   or if the author has changed
    If m_oAuthor Is Nothing Then
        bDo = True
    ElseIf m_oAuthor.ID <> oNew.ID Then
        bDo = True
    End If
    
    If bDo Then
        Set m_oAuthor = oNew
'       save the id for future retrieval
        m_oDoc.SaveItem "Author", mpDocSegment, , oNew.ID
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CService.Author"
    Exit Property
End Property
Public Property Get Author() As mpDB.CPerson
    Dim lAuthorID As Long
    If m_oAuthor Is Nothing Then
'       attempt to get author from document
        lAuthorID = m_oDoc.RetrieveItem("Author", mpDocSegment, , mpItemType_Integer)

'       if an author id has been retrieved from doc
'       the doc has an author set already
        If lAuthorID <> 0 Then 'mpUndefined Then
            Me.Author = g_oDBs.People(lAuthorID)
        End If
    End If
    Set Author = m_oAuthor
End Property
Public Property Let TypeID(lNew As Long)
    Dim xDesc As String

    On Error GoTo ProcError
    
    If Me.TypeID <> lNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "TypeID", mpDocSegment, , lNew
        Set m_oDef = g_oDBs.ServiceDefs.Item(lNew)

    '       alert and exit if not a valid Type
        If (m_oDef Is Nothing) Then
            Err.Raise mpError_InvalidMember
        End If
    End If
    
    Exit Property

ProcError:
    If Err.Number = mpError_InvalidMember Then
        xDesc = "Invalid Service TypeID.  " & _
            "Please check the appropriate tblUserOptions " & _
            "table in mpPrivate.mdb."
    Else
        xDesc = g_oError.Desc(Err.Number)
    End If
    RaiseError "MPO.CService.TypeID", xDesc
End Property
Public Property Get TypeID() As Long
    On Error Resume Next
    TypeID = m_oDoc.RetrieveItem("TypeID", mpDocSegment, , mpItemType_Integer)
End Property

Public Property Let Office(oNew As mpDB.COffice)
    Dim bDo As Boolean
    
    On Error GoTo ProcError
    If oNew Is Nothing Then Exit Property
'   set property if no author currently exists
'   or if the author has changed
    If m_oOffice Is Nothing Then
        bDo = True
    ElseIf m_oOffice.ID <> oNew.ID Then
        bDo = True
    End If
    
    If bDo Then
        Set m_oOffice = oNew
        
'       save the id for future retrieval
        m_oDoc.SaveItem "Office", mpDocSegment, , oNew.ID
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CService.Office"
    Exit Property
End Property

Public Property Get Office() As mpDB.COffice
    Dim lID As Long
    If m_oOffice Is Nothing Then
'       attempt to get Office from document
        lID = m_oDoc.RetrieveItem("Office", mpDocSegment, , mpItemType_Integer)

'       if an Office id has been retrieved from doc
'       the doc has an Office set already
        If lID <> 0 Then 'mpUndefined Then
            Me.Office = g_oDBs.Offices(lID)
        End If
    End If
    Set Office = m_oOffice
End Property

Public Function Definition() As mpDB.CServiceDef
    If (m_oDef Is Nothing) Then
        Set m_oDef = g_oDBs.ServiceDefs.Item(Me.TypeID)
    End If
    Set Definition = m_oDef
End Function
Public Property Get SignerGender() As mpGenders
    On Error Resume Next
    SignerGender = m_oDoc.RetrieveItem("SignerGender", mpDocSegment, , mpItemType_Integer)
End Property
Public Property Let SignerGender(iNew As mpGenders)
    If Me.SignerGender <> iNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "SignerGender", mpDocSegment, , iNew
        m_oDoc.SetGenderReferences iNew
    End If
End Property
Public Property Get ServiceTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ServiceTitle", mpDocSegment)
    ServiceTitle = xTemp
End Property
Public Property Let ServiceTitle(xNew As String)
    If UCase(xNew) = "NONE" Then
        Me.NoServiceTitle = True
        Exit Property
    End If
    
    If (Me.ServiceTitle <> xNew Or g_bForceItemUpdate) Then
        m_oDoc.EditItem_ALT "zzmpServiceTitle", xNew, , False
        m_oDoc.SaveItem "ServiceTitle", mpDocSegment, , xNew
    End If
End Property
Public Property Get SignerName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerName", mpDocSegment)
    SignerName = xTemp
End Property
Public Property Let SignerName(xNew As String)
    If Me.SignerName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerName", xNew, , False, , , , 30
        m_oDoc.SaveItem "SignerName", mpDocSegment, , xNew
    End If
End Property

Public Property Get ESignerName() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ESignerName", mpDocSegment)
    ESignerName = xTemp
End Property
Public Property Let ESignerName(xNew As String)
    If Me.ESignerName <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpESignerName", xNew, , False, , , , 30
        m_oDoc.SaveItem "ESignerName", mpDocSegment, , xNew
    End If
End Property

Public Property Get SignerEMail() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerEmail", mpDocSegment)
    SignerEMail = xTemp
End Property

Public Property Let SignerEMail(xNew As String)
    If Me.SignerEMail <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerEmail", xNew, , False, , , , 30
        m_oDoc.SaveItem "SignerEmail", mpDocSegment, , xNew
    End If
End Property

Public Property Get ServiceState() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ServiceState", mpDocSegment)
    ServiceState = xTemp
End Property
Public Property Let ServiceState(xNew As String)
    If Me.ServiceState <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpServiceState", xNew, , False, , , , 20
        m_oDoc.SaveItem "ServiceState", mpDocSegment, , xNew
    End If
End Property
Public Property Get ServiceCounty() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("ServiceCounty", mpDocSegment)
    ServiceCounty = xTemp
End Property
Public Property Let ServiceCounty(xNew As String)
    If Me.ServiceCounty <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpServiceCounty", xNew, , False, , , , 20
        m_oDoc.SaveItem "ServiceCounty", mpDocSegment, , xNew
    End If
End Property
Public Property Get SignerCity() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerCity", mpDocSegment)
    SignerCity = xTemp
End Property
Public Property Let SignerCity(xNew As String)
    If Me.SignerCity <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerCity", xNew, , False, , , , 20
        m_oDoc.SaveItem "SignerCity", mpDocSegment, , xNew
    End If
End Property

Public Property Let ExecuteDate(xNew As String)
    If Me.ExecuteDate <> xNew Or g_bForceItemUpdate Then
        If InStr(xNew, "Field") = 0 Then
            m_oDoc.EditItem_ALT "zzmpExecuteDate", xNew, , False
        Else
             m_oDoc.EditDateItem "zzmpExecuteDate", , , mpDateFormat_LongText
        End If
        m_oDoc.SaveItem "ExecuteDate", mpDocSegment, , xNew
    End If
End Property
Public Property Get ExecuteDate() As String
    On Error Resume Next
    ExecuteDate = m_oDoc.RetrieveItem("ExecuteDate", mpDocSegment)
End Property
Public Property Let ServiceDate(xNew As String)
    If Me.ServiceDate <> xNew Or g_bForceItemUpdate Then
        If InStr(xNew, "Field") = 0 Then
            m_oDoc.EditItem_ALT "zzmpServiceDate", xNew, , False
        Else
             m_oDoc.EditDateItem "zzmpServiceDate", , , mpDateFormat_LongText
        End If
        
        m_oDoc.SaveItem "ServiceDate", mpDocSegment, , xNew
    End If
End Property
Public Property Get ServiceDate() As String
    On Error Resume Next
    ServiceDate = m_oDoc.RetrieveItem("ServiceDate", mpDocSegment)
End Property
Public Property Get PartyDescription() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PartyDescription", mpDocSegment)
    PartyDescription = xTemp
End Property
Public Property Let PartyDescription(xNew As String)
    If Me.PartyDescription <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpPartyDescription", xNew, , False, , , , 30
        m_oDoc.SaveItem "PartyDescription", mpDocSegment, , xNew
    End If
End Property
Public Property Get PartyTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("PartyTitle", mpDocSegment)
    PartyTitle = xTemp
End Property
Public Property Let PartyTitle(xNew As String)
    If Me.PartyTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpPartyTitle", xNew, , False, , , , 30
        m_oDoc.SaveItem "PartyTitle", mpDocSegment, , xNew
    End If
End Property

Public Property Get CourtTitle() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("CourtTitle", mpDocSegment)
    CourtTitle = xTemp
End Property
Public Property Let CourtTitle(xNew As String)
    If Me.CourtTitle <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpCourtTitle", xNew, , False, , , , 30
        m_oDoc.SaveItem "CourtTitle", mpDocSegment, , xNew
    End If
End Property

Public Property Get CaseNumber() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("CaseNumber", mpDocSegment)
    CaseNumber = xTemp
End Property
Public Property Let CaseNumber(xNew As String)
    If Me.CaseNumber <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpCaseNumber", xNew, , False, , , , 30
        m_oDoc.SaveItem "CaseNumber", mpDocSegment, , xNew
    End If
End Property

Public Property Get ManualEntry() As Boolean
    Dim vTemp As Variant
    
    On Error Resume Next
    vTemp = m_oDoc.RetrieveItem( _
        "ManualEntry", mpDocSegment, , mpItemType_Boolean)
    If vTemp <> mpUndefined Then _
        ManualEntry = vTemp
End Property

Public Property Let ManualEntry(bNew As Boolean)
    If Me.ManualEntry <> bNew Or g_bForceItemUpdate Then
        m_oDoc.SaveItem "ManualEntry", mpDocSegment, , bNew
    End If
End Property

Public Property Get SignerAddress() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerAddress", mpDocSegment)
    SignerAddress = xTemp
End Property
Public Property Let SignerAddress(xNew As String)
    If Me.SignerAddress <> xNew Or g_bForceItemUpdate Then
        xNew = mpBase2.xSubstitute(xNew, vbCrLf, ", ")
        m_oDoc.EditItem_ALT "zzmpSignerAddress", xNew, , False, , , , 50
        m_oDoc.SaveItem "SignerAddress", mpDocSegment, , xNew
    End If
End Property
Public Property Get SignerState() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerState", mpDocSegment)
    SignerState = xTemp
End Property
Public Property Let SignerState(xNew As String)
    If Me.SignerState <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerState", xNew, , False, , , , 20
        m_oDoc.SaveItem "SignerState", mpDocSegment, , xNew
    End If
End Property
Public Property Get SignerCounty() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerCounty", mpDocSegment)
    SignerCounty = xTemp
End Property
Public Property Let SignerCounty(xNew As String)
    If Me.SignerCounty <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerCounty", xNew, , False, , , , 20
        m_oDoc.SaveItem "SignerCounty", mpDocSegment, , xNew
    End If
End Property

Public Property Get SignerOfficePhone() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerOfficePhone", mpDocSegment)
    SignerOfficePhone = xTemp
End Property

Public Property Let SignerOfficePhone(xNew As String)
    If Me.SignerOfficePhone <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerOfficePhone", xNew, , False, , , , 20
        m_oDoc.SaveItem "SignerOfficePhone", mpDocSegment, , xNew
    End If
End Property

Public Property Get SignerOfficeFax() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerOfficeFax", mpDocSegment)
    SignerOfficeFax = xTemp
End Property
Public Property Let SignerOfficeFax(xNew As String)
    If Me.SignerOfficeFax <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerOfficeFax", xNew, , False, , , , 20
        m_oDoc.SaveItem "SignerOfficeFax", mpDocSegment, , xNew
    End If
End Property

Public Property Get SignerCompany() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("SignerCompany", mpDocSegment)
    SignerCompany = xTemp
End Property
Public Property Let SignerCompany(xNew As String)
    If Me.SignerCompany <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpSignerCompany", xNew, , False, , , , 30
        m_oDoc.SaveItem "SignerCompany", mpDocSegment, , xNew
    End If
End Property
Public Property Get Custom1() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom1", mpDocSegment)
    Custom1 = xTemp
End Property
Public Property Let Custom1(xNew As String)
    If Me.Custom1 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpServiceCustom1", xNew, , False, , , , 20
        m_oDoc.SaveItem "Custom1", mpDocSegment, , xNew
'       take care of ref fields if any
        If xNew = "" Then
            m_oDoc.UnderlineBookmark "zzmpServiceCustom1_Ref1"
            m_oDoc.UnderlineBookmark "zzmpServiceCustom1_Ref2"
        End If
    End If
End Property
Public Property Get Custom2() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom2", mpDocSegment)
    Custom2 = xTemp
End Property
Public Property Let Custom2(xNew As String)
    If Me.Custom2 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpServiceCustom2", xNew, , False, , , , 20
        m_oDoc.SaveItem "Custom2", mpDocSegment, , xNew
'       take care of ref fields if any
        If xNew = "" Then
            m_oDoc.UnderlineBookmark "zzmpServiceCustom2_Ref1"
            m_oDoc.UnderlineBookmark "zzmpServiceCustom2_Ref2"
        End If
    End If
End Property
Public Property Get Custom3() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom3", mpDocSegment)
    Custom3 = xTemp
End Property
Public Property Let Custom3(xNew As String)
    If Me.Custom3 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpServiceCustom3", xNew, , False, , , , 20
        m_oDoc.SaveItem "Custom3", mpDocSegment, , xNew
'       take care of ref fields if any
        If xNew = "" Then
            m_oDoc.UnderlineBookmark "zzmpServiceCustom3_Ref1"
            m_oDoc.UnderlineBookmark "zzmpServiceCustom3_Ref2"
        End If
    End If
End Property
Public Property Get Custom4() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("Custom4", mpDocSegment)
    Custom4 = xTemp
End Property
Public Property Let Custom4(xNew As String)
    If Me.Custom4 <> xNew Or g_bForceItemUpdate Then
        m_oDoc.EditItem_ALT "zzmpServiceCustom4", xNew, , False, , , , 40
        m_oDoc.SaveItem "Custom4", mpDocSegment, , xNew
'       take care of ref fields if any
        If xNew = "" Then
            m_oDoc.UnderlineBookmark "zzmpServiceCustom4_Ref1"
            m_oDoc.UnderlineBookmark "zzmpServiceCustom4_Ref2"
        End If
    End If
End Property

Public Property Get DocumentTitles() As String
    On Error Resume Next
    Dim xTemp As String
    xTemp = m_oDoc.RetrieveItem("DocumentTitles", mpDocSegment)
    DocumentTitles = xTemp
End Property

Public Property Let DocumentTitles(xNew As String)
    On Error GoTo ProcError
    If Me.DocumentTitles <> xNew Or g_bForceItemUpdate Then
        xNew = xSubstitute(xNew, vbCrLf & vbCrLf, "*@*|*@*")
        xNew = xSubstitute(xNew, vbCrLf, Chr(11))
        xNew = xSubstitute(xNew, "*@*|*@*", vbCrLf)
        m_oDoc.EditItem_ALT "zzmpDocumentTitles", xNew, vbCr, False, , , , 80
        m_oDoc.SaveItem "DocumentTitles", mpDocSegment, , xNew
        RaiseEvent AfterDocumentTitlesSet
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CService.DocumentTitles"
    Exit Property
End Property
Public Property Get FirstNumberedPage() As Integer
    On Error Resume Next
     FirstNumberedPage = m_oDoc.RetrieveItem(" FirstNumberedPage")
End Property
Public Property Let FirstNumberedPage(xNew As Integer)
    If Me.FirstNumberedPage <> xNew Or g_bForceItemUpdate Then
        '---call page number setting method
        Me.Definition.FirstNumberedPage = xNew
        m_oDoc.SaveItem " FirstNumberedPage", , , xNew
    End If
End Property
Public Property Get FirstPageNumber() As Integer
    On Error Resume Next
     FirstPageNumber = m_oDoc.RetrieveItem(" FirstPageNumber")
End Property
Public Property Let FirstPageNumber(xNew As Integer)
    If Me.FirstPageNumber <> xNew Or g_bForceItemUpdate Then
        '---call page number setting method
        Me.Definition.FirstPageNumber = xNew
        m_oDoc.SaveItem " FirstPageNumber", , , xNew
    End If
End Property
Public Property Get Options() As mpDB.CPersonOptions
    On Error GoTo ProcError
    If Me.Author.ID = Empty Then
'       person has no ID, return default office options
        Set Options = g_oDBs.Offices.Default.Options(Me.Template.FileName)
    Else
'       return the options for the author
        Set Options = Me.Author.Options(Me.Template.FileName)
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CService.Options"
    Exit Property
End Property
'**********************************************************
'   Methods
'**********************************************************
Public Function ServiceList() As MPO.CServiceList
    Set ServiceList = m_oServiceList
End Function

Public Function PleadingPaper() As MPO.CPleadingPaper
    Set PleadingPaper = m_oPaper
End Function

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Me.Definition.BoilerplateFile
End Property

Public Property Get ESignatureBoilerplateFile() As String
    ESignatureBoilerplateFile = Me.Definition.ESignatureBoilerplate
End Property


Public Sub Refresh()
    Dim i As Integer
    On Error GoTo ProcError
    With Me
        .Author = .Author
        .SignerName = .SignerName
        .ESignerName = .ESignerName
        .SignerEMail = .SignerEMail
        .SignerCompany = .SignerCompany
        .SignerAddress = .SignerAddress
        If .Definition.AllowServiceTitle Then _
            .ServiceTitle = .ServiceTitle
        .SignerCity = .SignerCity
        .SignerState = .SignerState
        .SignerCounty = .SignerCounty
        .SignerOfficePhone = .SignerOfficePhone
        .SignerOfficeFax = .SignerOfficeFax
        .ServiceCounty = .ServiceCounty
        .ServiceState = .ServiceState
        .ServiceDate = .ServiceDate
        .ExecuteDate = .ExecuteDate
        .DocumentTitles = .DocumentTitles
        .PartyDescription = .PartyDescription
        .PartyTitle = .PartyTitle
        .CaseNumber = .CaseNumber
        .CourtTitle = .CourtTitle
        .SignerGender = .SignerGender
        .Custom1 = .Custom1
        .Custom2 = .Custom2
        .Custom3 = .Custom3
        .Custom4 = .Custom4
        
        With .ServiceList
            On Error Resume Next
            .GetExistingValues
            '---prevents insertion of empty table on reuse
            If .Recipients.Count Then
                .Insert
            End If
        End With
        
        .CustomProperties.RefreshValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CService.Refresh"
    Exit Sub
End Sub
Public Sub RefreshEmpty()
    Dim i As Integer
    Dim oDef As mpDB.CServiceDef
    On Error GoTo ProcError
    Set oDef = Me.Definition
    With Me
        If .SignerName = Empty Then _
            .SignerName = .SignerName
        If .ESignerName = Empty Then _
            .ESignerName = .ESignerName
        If .SignerEMail = Empty Then _
            .SignerEMail = .SignerEMail
        If .SignerCompany = Empty Then _
            .SignerCompany = .SignerCompany
        If .SignerAddress = Empty Then _
            .SignerAddress = .SignerAddress
        If .SignerCounty = Empty Then _
            .SignerCounty = .SignerCounty
        If .SignerState = Empty Then _
            .SignerState = .SignerState
        If .SignerCity = Empty Then _
            .SignerCity = .SignerCity
        If .ServiceCounty = Empty Then _
            .ServiceCounty = .ServiceCounty
        If .SignerOfficePhone = Empty Then _
            .SignerOfficePhone = .SignerOfficePhone
        If .SignerOfficeFax = Empty Then _
            .SignerOfficeFax = .SignerOfficeFax
        If .ServiceState = Empty Then _
            .ServiceState = .ServiceState
        If .ServiceDate = Empty And oDef.AllowServiceDate Then _
            .ServiceDate = .ServiceDate
        If .ExecuteDate = Empty And oDef.AllowExecuteDate Then _
            .ExecuteDate = .ExecuteDate
        If .DocumentTitles = Empty Then _
            .DocumentTitles = .DocumentTitles
        If .PartyDescription = Empty And oDef.AllowPartyDescription Then _
            .PartyDescription = .PartyDescription
        If .PartyTitle = Empty And oDef.AllowPartyTitle Then _
            .PartyTitle = .PartyTitle
        If .CourtTitle = Empty And oDef.AllowCourtTitle Then _
            .CourtTitle = .CourtTitle
        If .CaseNumber = Empty And oDef.AllowCaseNumber Then _
            .CaseNumber = .CaseNumber
        If .SignerGender = Empty And oDef.AllowGender Then _
            .SignerGender = .SignerGender
        If .Custom1 = Empty And oDef.Custom1Label <> Empty Then _
            .Custom1 = .Custom1
        If .Custom2 = Empty And oDef.Custom2Label <> Empty Then _
            .Custom2 = .Custom2
        If .Custom3 = Empty And oDef.Custom3Label <> Empty Then _
            .Custom3 = .Custom3
        If .Custom4 = Empty And oDef.Custom4Label <> Empty Then _
            .Custom4 = .Custom4
        If .ServiceTitle = Empty And oDef.AllowServiceTitle Then _
            .ServiceTitle = .ServiceTitle
            
        .CustomProperties.RefreshEmptyValues
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CService.RefreshEmpty"
    Exit Sub
End Sub
Public Sub Finish(Optional bForceRefresh As Boolean = False)
    Dim rngP As Range
    Dim oDef As mpDB.CServiceDef
    Dim oWordDoc As Object
    
    On Error GoTo ProcError
    
    g_bForceItemUpdate = True
    With m_oDoc
        If bForceRefresh Then
            Refresh
        Else
            RefreshEmpty
        End If
        
'       save author for reuse if template is COS/POS
        If Me.Template.FileName = Document.Template Then _
            .StorePersonInDoc Me.Author
            
        'GLOG : 5021 : ceh
        If Me.ESignature Then
            On Error Resume Next
            Set rngP = ActiveDocument.Bookmarks("zzmpServiceSignature").Range
            On Error GoTo ProcError
            
            If Not (rngP Is Nothing) Then
                On Error Resume Next
                ActiveDocument.Styles("Srv Signature").Delete
                On Error GoTo ProcError
                rngP.InsertFile BoilerplateDirectory() & Me.ESignatureBoilerplateFile, "zzmpServiceSignature"
                Me.UpdateForAuthor
            End If
            
        End If
            
        .UpdateFields
        
        'Me.ServiceList.Finish bForceRefresh
       
'CharlieCore 9.2.0 - BaseStyle
        Set oDef = Me.Definition
        
'       create BaseStyle doc var - this is necessary for
'       insert Document macro so we know what style should
'       match target Document's Normal font properties
       Word.ActiveDocument.Variables("BaseStyle") = oDef.BaseStyle

        If Me.NoServiceTitle Then
            .DeleteStyledText ActiveDocument.Range, "Srv Title"
        End If
        
'       delete any hidden text - ie text marked for deletion
       .DeleteHiddenText , , , True
       .RemoveFormFields
       
'       run editing macro
        RunEditMacro Me.Template
       
       .rngTrimEmptyParagraphs 'GLOG : 5021 : ceh

       .SelectStartPosition False
        
       .ClearBookmarks , .bHideFixedBookmarks
       
''---update pleading paper last, since its update method clears bookmarks!
       With Me.PleadingPaper
       
            '9.7.3 #
            If Not bIsValidPleadingPaper(Me.Definition.ID, _
                                         "tblPleadingPaperAssignments.fldPOSTypeID", _
                                         .PleadingPaperDef.ID) Then
                Set .PleadingPaperDef = g_oDBs.PleadingPaperDefs.Item(Me.Definition.DefaultPleadingPaperType)
            End If
       
            .StartPageNumberingAt = oDef.FirstNumberedPage
            .StartingPageNumber = oDef.FirstPageNumber
            .UseBoilerplateFooterSetup = oDef.UseBoilerplateFooterSetup
            If .UseBoilerplateFooterSetup = True Then
                .SideBarType = mpPleadingSideBarNone
            End If
            '#3684
            If g_bUseDefOfficeInfo Then
                Set .Office = g_oDBs.Offices.Default
            Else
                Set .Office = Me.Author.Office
            End If
'            If m_oDoc.Template = Me.Template.FileName Then
'                ' If Service template, update whole document
'                .Update
'            Else
                ' Else just update last section
                .Update m_oDoc.File.Sections.Last.Index
'            End If
        End With
        
    End With
    
    g_bForceItemUpdate = False
    Exit Sub
ProcError:
    RaiseError "MPO.CService.Finish"
    g_bForceItemUpdate = False
End Sub

Friend Sub Initialize()
    On Error GoTo ProcError
    
'   template not found - alert and exit
    If (m_oTemplate Is Nothing) Then
        On Error GoTo ProcError
        Err.Raise mpError_InvalidServiceTemplate
        Exit Sub
    Else
        On Error GoTo ProcError
    End If
    
'   get custom properties
    m_oProps.Category = Me.Template.ID
    
    With m_oDoc
        .Selection.StartOf
        If .IsCreated And Me.Template.FileName = _
                Word.ActiveDocument.AttachedTemplate And _
                Not (.ReuseAuthor Is Nothing) Then
            Me.Author = .ReuseAuthor
        ElseIf Not (Me.DefaultAuthor Is Nothing) Then
            Me.Author = Me.DefaultAuthor
            If Word.ActiveDocument.Variables.Count = 0 Then
                Me.UpdateForAuthor
            End If
        Else
            Me.Author = g_oDBs.People.First
            Me.UpdateForAuthor
        End If
    End With
    
    With m_oPaper

        ' Set pleading paper to default type if not already created in document
        If (Not .Exists) Or (Not m_oDoc.IsCreated) Then
            Set .PleadingPaperDef = _
                g_oDBs.PleadingPaperDefs.Item(Me.Definition.DefaultPleadingPaperType)
        End If
    End With
    With m_oServiceList
        .Initialize Me.Definition.DefaultServiceList
    End With
    m_bInit = True
    Exit Sub
ProcError:
    RaiseError "MPO.CService.Initialize"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Public Function UpdateForAuthor()
' updates relevant service properties to
' values belonging to current Author
    On Error GoTo ProcError
    'GLOG : 5473 : ceh
    If Me.ManualEntry Then
        Me.SignerName = Me.SignerName
        Me.ESignerName = Me.SignerName
    Else
        Me.SignerName = Me.Author.FullName
        Me.ESignerName = Me.Author.FullName
    End If
    
    Me.SignerEMail = Me.Author.Email
    
'   assign author's office as service office
    Me.Office = Me.Author.Office
    
'   update service properties linked to service office
    UpdateForOffice
    
'   update custom properties
    Me.CustomProperties.UpdateForAuthor Me.Author, Nothing
    Exit Function
ProcError:
    RaiseError "MPO.CService.UpdateForAuthor"
    Exit Function
End Function

Public Function UpdateForOffice()
' updates relevant service properties to
' values belonging to office
    On Error GoTo ProcError
    With Me.Office
        Me.SignerAddress = .Address(Me.Definition.AddressFormat)
'       use upper case firm name if specified
        If Me.Definition.UpperCaseFirmName Then
            Me.SignerCompany = .FirmNameUpperCase
        Else
            Me.SignerCompany = .FirmName
        End If
        Me.SignerState = .State
        Me.SignerCity = .City
        Me.SignerCounty = .CountyID
        Me.SignerOfficePhone = .Phone1
        Me.SignerOfficeFax = .Fax1

    End With
    Exit Function
ProcError:
    RaiseError "MPO.CService.UpdateForAuthor"
    Exit Function
End Function
Public Function InsertBoilerplate()
    On Error GoTo ProcError
    RaiseEvent BeforeBoilerplateInsert
    Me.Template.InsertBoilerplate BoilerplateDirectory() & Me.BoilerplateFile
    RaiseEvent AfterBoilerplateInsert
    Exit Function
ProcError:
    RaiseError "MPO.CService.InsertBoilerplate"
    Exit Function
End Function
'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oProps = New MPO.CCustomProperties
    Set m_oServiceList = New MPO.CServiceList
    Set m_oPaper = New MPO.CPleadingPaper
    
    Set m_oTemplate = g_oTemplates.ItemFromClass("CService")
End Sub
Private Sub Class_Terminate()
    Set m_oServiceList = Nothing
    Set m_oTemplate = Nothing
    Set m_oDoc = Nothing
    Set m_oAuthor = Nothing
    Set m_oPaper = Nothing
    Set m_oDef = Nothing
End Sub
'**********************************************************
'   IDocObj Interface
'**********************************************************
Private Property Let IDocObj_Author(RHS As mpDB.CPerson)
    Me.Author = RHS
End Property
Private Property Get IDocObj_Author() As mpDB.CPerson
    Set IDocObj_Author = Me.Author
End Property
Private Property Get IDocObj_BoilerplateFile() As String
    IDocObj_BoilerplateFile = Me.BoilerplateFile
End Property
Private Function IDocObj_CustomProperties() As CCustomProperties
    Set IDocObj_CustomProperties = Me.CustomProperties
End Function
Private Property Let IDocObj_DefaultAuthor(RHS As mpDB.CPerson)
    Me.DefaultAuthor = RHS
End Property
Private Property Get IDocObj_DefaultAuthor() As mpDB.CPerson
    Set IDocObj_DefaultAuthor = Me.DefaultAuthor
End Property
Private Function IDocObj_Document() As CDocument
    Set IDocObj_Document = Me.Document
End Function
Private Sub IDocObj_Finish(Optional ByVal bForceRefresh As Boolean = True)
    Finish bForceRefresh
End Sub
Private Property Get IDocObj_Initialized() As Boolean
    IDocObj_Initialized = m_bInit
End Property
Private Sub IDocObj_InsertBoilerplate()
    Me.InsertBoilerplate
End Sub
Private Sub IDocObj_RefreshEmpty()
    RefreshEmpty
End Sub
Private Sub IDocObj_Refresh()
    Refresh
End Sub
Private Function IDocObj_Template() As CTemplate
    Set IDocObj_Template = Me.Template
End Function
Private Sub IDocObj_UpdateForAuthor()
    Me.UpdateForAuthor
End Sub
