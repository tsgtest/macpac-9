VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentInsert"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CDocumentInsert Class
'   created 10/26/05 by Jeffrey Sweetland
'   Contains properties and methods that
'   define the CDocumentInsert Class - those
'**********************************************************
Option Explicit

Enum mpDocInsertLocations
    mpDocInsertLocations_StartOfDocument = 1
    mpDocInsertLocations_EndOfDocument = 2
    mpDocInsertLocations_ReplaceBookmark = 3
    mpDocInsertLocations_AfterBookmark = 4
    mpDocInsertLocations_BeforeBookmark = 5
End Enum
    
Private m_oDoc As MPO.CDocument
Private m_oDef As mpDB.CDocumentInsertDef
Public Property Let TypeID(lNew As Long)
    Dim xDesc As String
    Dim xTemplate
    On Error Resume Next
    xTemplate = Word.ActiveDocument.AttachedTemplate
    Set m_oDef = g_oDBs.DocumentInsertDefs(xTemplate).Item(lNew)
    On Error GoTo ProcError
'   raise error if there is no
'   definition with id = lNew
    If (m_oDef Is Nothing) Then
        xDesc = "Invalid Document Insert type. " & lNew & _
            " is not a valid Insert id.  Please " & _
            "check tblDocumentInsertTypes in mpPublic.mdb."
        Err.Raise mpError_InvalidSegmentType
    End If

'   check to see that definition has necessary parameters
    ValidateDef
    Exit Property
ProcError:
    RaiseError "MPO.cDocumentInsert.TypeID", xDesc
End Property
Public Property Get TypeID() As Long
    TypeID = m_oDef.ID
End Property
Public Function Subject() As String
    Subject = m_oDef.Subject
End Function
Private Function ValidateDef() As Boolean
'returns TRUE if Insert definition is
'correctly defined, else FALSE
    Dim oTemplate As mpDB.CTemplateDef
    Dim xDesc As String
    
    With m_oDef
'           validate bpfile
        Dim xBP As String
        ' Check for existence of boilerplate and TargetBookmark name if TargetBookmark-type location is configured
        If .Location1 > 0 Then
            If Len(.Boilerplate1) Then
                xBP = mpBase2.BoilerplateDirectory & "\" & .Boilerplate1
                If Dir(xBP) = "" Then
    '                   invalid segment definition
                    xDesc = "Invalid Document Insert definition. '" & xBP & _
                        "' could not be found.  Please check tblDocumentInsertTypes " & _
                        "in mpPublic.mdb."
                    Err.Raise mpError_InvalidSegmentType
                End If
                If .Location1 > 2 And Len(.TargetBookmark1) = 0 Then
                    xDesc = "Invalid Document Insert definition. '" & _
                        "' No TargetBookmark1 property has been defined.  Please check tblDocumentInsertTypes " & _
                        "in mpPublic.mdb."
                    Err.Raise mpError_InvalidSegmentType
                End If
            Else
                xDesc = "Invalid Document Insert definition. '" & _
                    "' No Boilerplate1 property has been defined.  Please check tblDocumentInsertTypes " & _
                    "in mpPublic.mdb."
                Err.Raise mpError_InvalidSegmentType
            End If
        End If
        If .Location2 > 0 Then
            If Len(.Boilerplate2) Then
                xBP = mpBase2.BoilerplateDirectory & "\" & .Boilerplate2
                If Dir(xBP) = "" Then
    '                   invalid segment definition
                    xDesc = "Invalid Document Insert definition. '" & xBP & _
                        "' could not be found.  Please check tblDocumentInsertTypes " & _
                        "in mpPublic.mdb."
                    Err.Raise mpError_InvalidSegmentType
                End If
            End If
            If .Location2 > 2 And Len(.TargetBookmark2) = 0 Then
                xDesc = "Invalid Document Insert definition. '" & _
                    "' No TargetBookmark2 property has been defined.  Please check tblDocumentInsertTypes " & _
                    "in mpPublic.mdb."
                Err.Raise mpError_InvalidSegmentType
            End If
        End If
        If .Location3 > 0 Then
            If Len(.Boilerplate3) Then
                xBP = mpBase2.BoilerplateDirectory & "\" & .Boilerplate3
                If Dir(xBP) = "" Then
    '                   invalid segment definition
                    xDesc = "Invalid Document Insert definition. '" & xBP & _
                        "' could not be found.  Please check tblDocumentInsertTypes " & _
                        "in mpPublic.mdb."
                    Err.Raise mpError_InvalidSegmentType
                End If
            End If
            If .Location3 > 2 And Len(.TargetBookmark3) = 0 Then
                xDesc = "Invalid Document Insert definition. '" & _
                    "' No TargetBookmark3 property has been defined.  Please check tblDocumentInsertTypes " & _
                    "in mpPublic.mdb."
                Err.Raise mpError_InvalidSegmentType
            End If
        End If
        If .Location4 > 0 Then
            If Len(.Boilerplate4) Then
                xBP = mpBase2.BoilerplateDirectory & "\" & .Boilerplate4
                If Dir(xBP) = "" Then
    '                   invalid segment definition
                    xDesc = "Invalid Document Insert definition. '" & xBP & _
                        "' could not be found.  Please check tblDocumentInsertTypes " & _
                        "in mpPublic.mdb."
                    Err.Raise mpError_InvalidSegmentType
                End If
            End If
            If .Location4 > 2 And Len(.TargetBookmark4) = 0 Then
                xDesc = "Invalid Document Insert definition. '" & _
                    "' No TargetBookmark4 property has been defined.  Please check tblDocumentInsertTypes " & _
                    "in mpPublic.mdb."
                Err.Raise mpError_InvalidSegmentType
            End If
        End If
    End With
    ValidateDef = True
    Exit Function
ProcError:
    RaiseError "MPO.cDocumentInsert.ValidateDef", xDesc
End Function
Public Function Insert() As Boolean

    On Error GoTo ProcError
    Dim oRng As Word.Range
    Dim iLoc As mpDocInsertLocations
    Dim i As Integer
    Dim xBP As String
    Dim xBookmark As String
    Dim xTargetBookmark As String
    Dim oTempBmk As Word.Bookmark
    Dim cnt As Integer
    With m_oDef
        If .ID = 0 Then Exit Function
        For i = 1 To 4
            Select Case i
                Case 1
                    iLoc = .Location1
                    xBP = .Boilerplate1
                    xBookmark = .Bookmark1
                    xTargetBookmark = .TargetBookmark1
                Case 2
                    iLoc = .Location2
                    If Len(.Boilerplate2) Then
                        xBP = .Boilerplate2
                    Else
                        xBP = .Boilerplate1
                    End If
                    xBookmark = .Bookmark2
                    xTargetBookmark = .TargetBookmark2
                Case 3
                    iLoc = .Location3
                    If Len(.Boilerplate3) Then
                        xBP = .Boilerplate3
                    Else
                        xBP = .Boilerplate1
                    End If
                    xBookmark = .Bookmark3
                    xTargetBookmark = .TargetBookmark3
                Case 4
                    iLoc = .Location4
                    If Len(.Boilerplate4) Then
                        xBP = .Boilerplate4
                    Else
                        xBP = .Boilerplate1
                    End If
                    xBookmark = .Bookmark4
                    xTargetBookmark = .TargetBookmark4
            End Select
            If iLoc > 0 Then
                Select Case iLoc
                    Case mpDocInsertLocations_StartOfDocument
                        Set oRng = Word.ActiveDocument.Content
                        oRng.StartOf
            '           this is to avoid using the information method of a range object
                        On Error Resume Next
                        cnt = oRng.Cells.Count
            '           On Error GoTo ProcError
                        If cnt > 0 Then
            '               ensure that segment will not get inserted
            '               in a table cell - add para before
                            oRng.Select
                            
            '               bookmark this location for deletion below
                            Set oTempBmk = Word.ActiveDocument.Bookmarks.Add( _
                                "zzmpBmkTemp", oRng)
                            Selection.SplitTable
                        End If
                        Selection.StartOf wdStory
                        Selection.InsertBreak wdSectionBreakNextPage
                        m_oDoc.UnlinkHeadersFooters ActiveDocument.Sections(2)
                        m_oDoc.ClearFooters ActiveDocument.Sections(1)
                        m_oDoc.ClearHeaders ActiveDocument.Sections(1)
                        Set oRng = ActiveDocument.Content
                        oRng.StartOf

                    Case mpDocInsertLocations_EndOfDocument
                        Set oRng = Word.ActiveDocument.Content
                        oRng.EndOf
                        oRng.InsertBreak wdSectionBreakNextPage
                        oRng.EndOf
                        m_oDoc.UnlinkHeadersFooters Word.ActiveDocument.Sections.Last
                        m_oDoc.ClearFooters Word.ActiveDocument.Sections.Last
                        m_oDoc.ClearHeaders Word.ActiveDocument.Sections.Last
                        
                    Case Else
                        On Error Resume Next
                        Set oRng = Word.ActiveDocument.Bookmarks(xTargetBookmark).Range
                        On Error GoTo ProcError
                        If Not oRng Is Nothing Then
                            If iLoc = mpDocInsertLocations_AfterBookmark Then
                                oRng.Collapse wdCollapseEnd
                            ElseIf iLoc = mpDocInsertLocations_BeforeBookmark Then
                                oRng.Collapse wdCollapseStart
                            End If
                        End If
                End Select
                If Not oRng Is Nothing Then
                    oRng.Select
                    m_oDoc.InsertBoilerplate xBP, xBookmark, , mpDocumentLocation_CurrentSelection, _
                        iLoc = mpDocInsertLocations_ReplaceBookmark
                End If
            End If
        Next i
    End With
    Insert = True
    Exit Function
ProcError:
    RaiseError "MPO.cDocumentInsert.Insert"
    Exit Function
End Function
'**********************************************************
'   Class Events
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
End Sub

