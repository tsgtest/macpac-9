VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContacts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CContact"
Attribute VB_Ext_KEY = "Member0" ,"CContact"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'**********************************************************
'   CContacts Class
'   created 12/02/99 by Jeffrey Sweetland

'   Contains properties and methods that
'   integrate with Contact Integration

'   Member of
'   Container for CContact
'**********************************************************
Option Explicit

Public Enum mpCIVersions
    mpCIVersion_1 = 1
    mpCIVersion_2 = 2
End Enum

Private mCol As Collection
Private m_iToCount As Integer
Private m_iCcCount As Integer
Private m_iBccCount As Integer
Private m_iFromCount As Integer
Private m_iVer As Integer
Private m_oContacts As CIO.CContacts

'*********************************
'per log item 1817 - df
Public Sub Reinitialize()
    Set g_appCI = Nothing
    Set g_oCI20Session = Nothing
End Sub
'*********************************

Public Function CIVersion() As mpCIVersions
    Dim xVer As String
    On Error GoTo ProcError
    If m_iVer = Empty Then
        'get version of ci to use
        xVer = GetMacPacIni("CI", "CIVersion")
        
        If xVer Like "2.*" Then
            m_iVer = mpCIVersion_2
        Else
            'the ini specifies v1 or is empty
            m_iVer = mpCIVersion_1
        End If
    End If
    
    'return version
    CIVersion = m_iVer
    Exit Function
ProcError:
    RaiseError "MPO.CContacts.CIVersion"
    Exit Function
End Function

'CharlieCore 9.2.0 - CI
Public Sub Retrieve(Optional ByVal bIncludeTo As Boolean = True, _
                    Optional ByVal bIncludeFrom As Boolean = False, _
                    Optional ByVal bIncludeCC As Boolean = False, _
                    Optional ByVal bIncludeBCC As Boolean = False, _
                    Optional ByVal bIncludePhone As Boolean = False, _
                    Optional ByVal bIncludeFax As Boolean = False, _
                    Optional ByVal bIncludeEmail As Boolean = False, _
                    Optional ByVal iOnEmpty As ciEmptyAddress = ciEmptyAddress_PromptForExisting, _
                    Optional ByVal bPromptForPhones As Boolean = True, _
                    Optional ByVal iDefaultSelectionList As ciSelectionLists = ciSelectionList_To, _
                    Optional ByVal bNamesOnlyForTo As Boolean = False, _
                    Optional ByVal bNamesOnlyForFrom As Boolean = True, _
                    Optional ByVal bNamesOnlyForCC As Boolean = True, _
                    Optional ByVal bNamesOnlyForBCC As Boolean = True, _
                    Optional ByVal xSeparator As String = vbCrLf, _
                    Optional ByVal bIncludeCustomFields As Boolean = False, _
                    Optional ByVal bIncludeUserDefinedSalutation As Boolean = False)
    On Error GoTo ProcError
    
    If CIVersion = mpCIVersion_1 Then
        Retrieve10 bIncludeTo, bIncludeFrom, bIncludeCC, bIncludeBCC, bIncludePhone, bIncludeFax, bIncludeEmail, _
            iOnEmpty, bPromptForPhones, iDefaultSelectionList, bNamesOnlyForTo, bNamesOnlyForFrom, _
            bNamesOnlyForCC, bNamesOnlyForBCC, xSeparator, bIncludeCustomFields
    Else
        Retrieve20 bIncludeTo, bIncludeFrom, bIncludeCC, bIncludeBCC, bIncludePhone, bIncludeFax, bIncludeEmail, _
            iOnEmpty, iDefaultSelectionList, bNamesOnlyForTo, bNamesOnlyForFrom, _
            bNamesOnlyForCC, bNamesOnlyForBCC, xSeparator, bIncludeCustomFields, bIncludeUserDefinedSalutation
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts.Retrieve"
    Exit Sub
End Sub


Public Sub Retrieve_ALT(Optional ByVal bIncludeTo As Boolean = True, _
                    Optional ByVal bIncludeFrom As Boolean = False, _
                    Optional ByVal bIncludeCC As Boolean = False, _
                    Optional ByVal bIncludeBCC As Boolean = False, _
                    Optional ByVal bIncludePhone As Boolean = False, _
                    Optional ByVal bIncludeFax As Boolean = False, _
                    Optional ByVal bIncludeEmail As Boolean = False, _
                    Optional ByVal iOnEmpty As ciEmptyAddress = ciEmptyAddress_PromptForExisting, _
                    Optional ByVal bPromptForPhones As Boolean = True, _
                    Optional ByVal iDefaultSelectionList As ciSelectionLists = ciSelectionList_To, _
                    Optional ByVal bNamesOnlyForTo As Boolean = False, _
                    Optional ByVal bNamesOnlyForFrom As Boolean = True, _
                    Optional ByVal bNamesOnlyForCC As Boolean = True, _
                    Optional ByVal bNamesOnlyForBCC As Boolean = True, _
                    Optional ByVal xSeparator As String = vbCrLf, _
                    Optional ByVal bIncludeCustomFields As Boolean = False, _
                    Optional ByVal bIncludeAll As Boolean = False)
    On Error GoTo ProcError
    
    If CIVersion = mpCIVersion_1 Then
        Retrieve10 bIncludeTo, bIncludeFrom, bIncludeCC, bIncludeBCC, bIncludePhone, bIncludeFax, bIncludeEmail, _
            iOnEmpty, bPromptForPhones, iDefaultSelectionList, bNamesOnlyForTo, bNamesOnlyForFrom, _
            bNamesOnlyForCC, bNamesOnlyForBCC, xSeparator, bIncludeCustomFields
    Else
        Retrieve20_ALT bIncludeTo, bIncludeFrom, bIncludeCC, bIncludeBCC, bIncludePhone, bIncludeFax, bIncludeEmail, _
            iOnEmpty, iDefaultSelectionList, bNamesOnlyForTo, bNamesOnlyForFrom, _
            bNamesOnlyForCC, bNamesOnlyForBCC, xSeparator, bIncludeCustomFields, bIncludeAll
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts.Retrieve_ALT"
    Exit Sub
End Sub
Private Sub Retrieve10(Optional ByVal bIncludeTo As Boolean = True, _
                    Optional ByVal bIncludeFrom As Boolean = False, _
                    Optional ByVal bIncludeCC As Boolean = False, _
                    Optional ByVal bIncludeBCC As Boolean = False, _
                    Optional ByVal bIncludePhone As Boolean = False, _
                    Optional ByVal bIncludeFax As Boolean = False, _
                    Optional ByVal bIncludeEmail As Boolean = False, _
                    Optional ByVal iOnEmpty As ciEmptyAddress = ciEmptyAddress_PromptForExisting, _
                    Optional ByVal bPromptForPhones As Boolean = True, _
                    Optional ByVal iDefaultSelectionList As ciSelectionLists = ciSelectionList_To, _
                    Optional ByVal bNamesOnlyForTo As Boolean = False, _
                    Optional ByVal bNamesOnlyForFrom As Boolean = True, _
                    Optional ByVal bNamesOnlyForCC As Boolean = True, _
                    Optional ByVal bNamesOnlyForBCC As Boolean = True, _
                    Optional ByVal xSeparator As String = vbCrLf, _
                    Optional ByVal bIncludeCustomFields As Boolean = False)
    
    Dim iSelList As Integer
    Dim i As Integer
    Dim xKey As String
    Dim oStatus As Object
    On Error GoTo ProcError
    If g_appCI Is Nothing Then
        Set oStatus = CreateObject("mpAXE.CStatus")
        oStatus.Title = "Contact Integration"
        oStatus.Show , "Loading Contact Integration.  Please wait..."

        Set g_appCI = New mpci.Application
        Set oStatus = Nothing
    End If
    If Not g_appCI.Initialized Then
        Set g_appCI = Nothing
        Exit Sub
    End If
    
    With g_appCI.Options
        .NamesOnlyForBCC = bNamesOnlyForBCC
        .NamesOnlyForCC = bNamesOnlyForCC
        .NamesOnlyForFrom = bNamesOnlyForFrom
        .NamesOnlyForTo = bNamesOnlyForTo
        .IncludeEAddress = bIncludeEmail
        .IncludeFax = bIncludeFax
        .IncludePhone = bIncludePhone
        .PromptForMissingPhones = bPromptForPhones
        .OnEmptyAddresses = iOnEmpty
        .DefaultSelectionList = iDefaultSelectionList
        .IncludeCustomFields = bIncludeCustomFields
    End With

    If bIncludeTo Then _
        iSelList = mpciSelectionList_To
    If bIncludeFrom Then _
        iSelList = iSelList + mpCISelectionList_From
    If bIncludeCC Then _
        iSelList = iSelList + mpciSelectionList_CC
    If bIncludeBCC Then _
        iSelList = iSelList + mpciSelectionList_BCC
    
    g_appCI.Contacts.Retrieve iSelList
    DoEvents
    
    Set mCol = New Collection
    m_iToCount = 0
    m_iFromCount = 0
    m_iCcCount = 0
    m_iBccCount = 0
    With g_appCI.Contacts
        If .Count Then
            For i = 1 To .Count
                xKey = ""
                With .Item(i)
                    Select Case .ContactType
                        Case ciContactType_To
                            m_iToCount = m_iToCount + 1
                            xKey = "T" & Format(m_iToCount, "0000")
                        Case ciContactType_From
                            m_iFromCount = m_iFromCount + 1
                            xKey = "F" & Format(m_iFromCount, "0000")
                        Case ciContactType_CC
                            m_iCcCount = m_iCcCount + 1
                            xKey = "C" & Format(m_iCcCount, "0000")
                        Case ciContactType_BCC
                            m_iBccCount = m_iBccCount + 1
                            xKey = "B" & Format(m_iBccCount, "0000")
                    End Select
                End With
                If xKey <> "" Then
                    Add xKey
                    Item(xKey).LoadProperties10 .Item(i), xSeparator
                End If
            Next i
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts.Retrieve10"
    Exit Sub
End Sub

Private Sub Retrieve20(Optional ByVal bIncludeTo As Boolean = True, _
                    Optional ByVal bIncludeFrom As Boolean = False, _
                    Optional ByVal bIncludeCC As Boolean = False, _
                    Optional ByVal bIncludeBCC As Boolean = False, _
                    Optional ByVal bIncludePhone As Boolean = False, _
                    Optional ByVal bIncludeFax As Boolean = False, _
                    Optional ByVal bIncludeEmail As Boolean = False, _
                    Optional ByVal iOnEmpty As ciEmptyAddress = ciEmptyAddress_PromptForExisting, _
                    Optional ByVal iDefaultSelectionList As CIX.ciSelectionLists = CIX.ciSelectionList_To, _
                    Optional ByVal bNamesOnlyForTo As Boolean = False, _
                    Optional ByVal bNamesOnlyForFrom As Boolean = True, _
                    Optional ByVal bNamesOnlyForCC As Boolean = True, _
                    Optional ByVal bNamesOnlyForBCC As Boolean = True, _
                    Optional ByVal xSeparator As String = vbCrLf, _
                    Optional ByVal bIncludeCustomFields As Boolean = False, _
                    Optional ByVal bIncludeUserDefinedSalutation As Boolean = False)
    
    Dim iSelList As CIX.ciSelectionLists
    Dim i As Integer
    Dim xKey As String
    Dim oStatus As Object
    Dim iData As CIO.ciRetrieveData
    Dim iDataForTo As CIO.ciRetrieveData
    Dim iDataForFrom As CIO.ciRetrieveData
    Dim iDataForCC As CIO.ciRetrieveData
    Dim iDataForBCC As CIO.ciRetrieveData
    
    On Error GoTo ProcError
    If g_oCI20Session Is Nothing Then
        Set oStatus = CreateObject("mpAXE.CStatus")
        oStatus.Title = "Contact Integration"
        oStatus.Show , "Loading Contact Integration.  Please wait..."

        Set g_oCI20Session = New CIX.CSession
        Set oStatus = Nothing
    End If
    If Not g_oCI20Session.Initialized Then
        Set g_oCI20Session = Nothing
        Exit Sub
    End If
    
    If iOnEmpty = ciEmptyAddress_ReturnEmpty Then
        iData = ciRetrieveData_Names
    Else
        iData = ciRetrieveData_Names + ciRetrieveData_Addresses
    End If
    
    If bIncludePhone Then
        iData = iData + ciRetrieveData_Phones
    End If
    
    If bIncludeFax Then
        iData = iData + ciRetrieveData_Faxes
    End If
    
    If bIncludeEmail Then
        iData = iData + ciRetrieveData_EAddresses
    End If
    
    If bIncludeCustomFields Then
        iData = iData + ciRetrieveData_CustomFields
    End If
    
    If bIncludeTo Then
        iSelList = ciSelectionList_To
        If bNamesOnlyForTo Then
            iDataForTo = ciRetrieveData_Names
        Else
            iDataForTo = iData
        End If
    End If
    If bIncludeFrom Then
        iSelList = iSelList + ciSelectionList_From
        If bNamesOnlyForFrom Then
            iDataForFrom = ciRetrieveData_Names
        Else
            iDataForFrom = iData
        End If
    End If
    If bIncludeCC Then
        iSelList = iSelList + ciSelectionList_CC
        If bNamesOnlyForCC Then
            iDataForCC = ciRetrieveData_Names
        Else
            iDataForCC = iData
        End If
    End If
    If bIncludeBCC Then
        iSelList = iSelList + ciSelectionList_BCC
         If bNamesOnlyForBCC Then
            iDataForBCC = ciRetrieveData_Names
        Else
            iDataForBCC = iData
        End If
    End If
            
    If bIncludeUserDefinedSalutation Then
        Set m_oContacts = g_oCI20Session.GetContacts(iDataForTo + ciRetrieveData_Salutation, _
            iDataForFrom, iDataForCC, iDataForBCC, iDefaultSelectionList, 0)
    Else
        Set m_oContacts = g_oCI20Session.GetContacts(iDataForTo, _
            iDataForFrom, iDataForCC, iDataForBCC, iDefaultSelectionList, 0)
    End If
    
    DoEvents
    
    If m_oContacts Is Nothing Then
        Set m_oContacts = New CIO.CContacts
    End If
    
    Set mCol = New Collection
    m_iToCount = 0
    m_iFromCount = 0
    m_iCcCount = 0
    m_iBccCount = 0
    With m_oContacts
        If .Count Then
            For i = 1 To .Count
                xKey = ""
                With .Item(i)
                    Select Case .ContactType
                        Case CIO.ciContactType_To
                            m_iToCount = m_iToCount + 1
                            xKey = "T" & Format(m_iToCount, "0000")
                        Case CIO.ciContactType_From
                            m_iFromCount = m_iFromCount + 1
                            xKey = "F" & Format(m_iFromCount, "0000")
                        Case CIO.ciContactType_CC
                            m_iCcCount = m_iCcCount + 1
                            xKey = "C" & Format(m_iCcCount, "0000")
                        Case CIO.ciContactType_BCC
                            m_iBccCount = m_iBccCount + 1
                            xKey = "B" & Format(m_iBccCount, "0000")
                    End Select
                End With
                If xKey <> "" Then
                    Add xKey
                    Item(xKey).LoadProperties20 .Item(i), xSeparator
                End If
            Next i
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts2.Retrieve20"
    Exit Sub
End Sub

Private Sub Retrieve20_ALT(Optional ByVal bIncludeTo As Boolean = True, _
                    Optional ByVal bIncludeFrom As Boolean = False, _
                    Optional ByVal bIncludeCC As Boolean = False, _
                    Optional ByVal bIncludeBCC As Boolean = False, _
                    Optional ByVal bIncludePhone As Boolean = False, _
                    Optional ByVal bIncludeFax As Boolean = False, _
                    Optional ByVal bIncludeEmail As Boolean = False, _
                    Optional ByVal iOnEmpty As ciEmptyAddress = ciEmptyAddress_PromptForExisting, _
                    Optional ByVal iDefaultSelectionList As CIX.ciSelectionLists = CIX.ciSelectionList_To, _
                    Optional ByVal bNamesOnlyForTo As Boolean = False, _
                    Optional ByVal bNamesOnlyForFrom As Boolean = True, _
                    Optional ByVal bNamesOnlyForCC As Boolean = True, _
                    Optional ByVal bNamesOnlyForBCC As Boolean = True, _
                    Optional ByVal xSeparator As String = vbCrLf, _
                    Optional ByVal bIncludeCustomFields As Boolean = False, _
                    Optional ByVal bIncludeAll As Boolean = False)
    
    Dim iSelList As CIX.ciSelectionLists
    Dim i As Integer
    Dim xKey As String
    Dim oStatus As Object
    Dim iData As CIO.ciRetrieveData
    Dim iDataForTo As CIO.ciRetrieveData
    Dim iDataForFrom As CIO.ciRetrieveData
    Dim iDataForCC As CIO.ciRetrieveData
    Dim iDataForBCC As CIO.ciRetrieveData
    
    On Error GoTo ProcError
    If g_oCI20Session Is Nothing Then
        Set oStatus = CreateObject("mpAXE.CStatus")
        oStatus.Title = "Contact Integration"
        oStatus.Show , "Loading Contact Integration.  Please wait..."

        Set g_oCI20Session = New CIX.CSession
        Set oStatus = Nothing
    End If
    If Not g_oCI20Session.Initialized Then
        Set g_oCI20Session = Nothing
        Exit Sub
    End If
    
    If iOnEmpty = ciEmptyAddress_ReturnEmpty Then
        iData = ciRetrieveData_Names
    Else
        iData = ciRetrieveData_Names + ciRetrieveData_Addresses
    End If
    
    If bIncludePhone Then
        iData = iData + ciRetrieveData_Phones
    End If
    
    If bIncludeFax Then
        iData = iData + ciRetrieveData_Faxes
    End If
    
    If bIncludeEmail Then
        iData = iData + ciRetrieveData_EAddresses
    End If
    
    If bIncludeCustomFields Then
        iData = iData + ciRetrieveData_CustomFields
    End If
    
    If bIncludeTo Then
        iSelList = ciSelectionList_To
        If bNamesOnlyForTo Then
            iDataForTo = ciRetrieveData_Names
        Else
            iDataForTo = iData
        End If
    End If
    If bIncludeFrom Then
        iSelList = iSelList + ciSelectionList_From
        If bNamesOnlyForFrom Then
            iDataForFrom = ciRetrieveData_Names
        Else
            iDataForFrom = iData
        End If
    End If
    If bIncludeCC Then
        iSelList = iSelList + ciSelectionList_CC
        If bNamesOnlyForCC Then
            iDataForCC = ciRetrieveData_Names
        Else
            iDataForCC = iData
        End If
    End If
    If bIncludeBCC Then
        iSelList = iSelList + ciSelectionList_BCC
         If bNamesOnlyForBCC Then
            iDataForBCC = ciRetrieveData_Names
        Else
            iDataForBCC = iData
        End If
    End If
    
    If bIncludeAll Then
        If bIncludeTo Then _
                iDataForTo = ciRetrieveData_All
        If bIncludeFrom Then _
                iDataForFrom = ciRetrieveData_All
        If bIncludeCC Then _
                iDataForCC = ciRetrieveData_All
        If bIncludeBCC Then _
                iDataForBCC = ciRetrieveData_All
    End If
            
    Set m_oContacts = g_oCI20Session.GetContacts(iDataForTo, _
        iDataForFrom, iDataForCC, iDataForBCC, iDefaultSelectionList, 0)
    DoEvents
    '---end 9.5.0
    
    If m_oContacts Is Nothing Then
        Set m_oContacts = New CIO.CContacts
    End If
    
    Set mCol = New Collection
    m_iToCount = 0
    m_iFromCount = 0
    m_iCcCount = 0
    m_iBccCount = 0
    With m_oContacts
        If .Count Then
            For i = 1 To .Count
                xKey = ""
                With .Item(i)
                    Select Case .ContactType
                        Case CIO.ciContactType_To
                            m_iToCount = m_iToCount + 1
                            xKey = "T" & Format(m_iToCount, "0000")
                        Case CIO.ciContactType_From
                            m_iFromCount = m_iFromCount + 1
                            xKey = "F" & Format(m_iFromCount, "0000")
                        Case CIO.ciContactType_CC
                            m_iCcCount = m_iCcCount + 1
                            xKey = "C" & Format(m_iCcCount, "0000")
                        Case CIO.ciContactType_BCC
                            m_iBccCount = m_iBccCount + 1
                            xKey = "B" & Format(m_iBccCount, "0000")
                    End Select
                End With
                If xKey <> "" Then
                    Add xKey
                    Item(xKey).LoadProperties20 .Item(i), xSeparator
                End If
            Next i
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts2.Retrieve20_ALT"
    Exit Sub
End Sub
'CharlieCore 9.2.0 - CI
Public Sub CreateDataFile(xFile As String)
    On Error GoTo ProcError
    If CIVersion = mpCIVersion_1 Then
        CreateDataFile10 xFile
    Else
        CreateDataFile20 xFile
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts.CreateDataFile"
    Exit Sub
End Sub

Private Sub CreateDataFile10(xFile As String)
    Dim oStatus As Object
    
    On Error GoTo ProcError
    If g_appCI Is Nothing Then
        Set oStatus = CreateObject("mpAXE.CStatus")
        oStatus.Title = "Contact Integration"
        oStatus.Show , "Loading Contact Integration.  Please wait..."

        Set g_appCI = New mpci.Application
        Set oStatus = Nothing
    End If
    
    If Not g_appCI.Initialized Then
        Set g_appCI = Nothing
        Exit Sub
    End If
    
    g_appCI.Contacts.CreateDataFile xFile, "|"
    DoEvents
    
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts.CreateDataFile10"
    Exit Sub
End Sub

Private Sub CreateDataFile20(xFile As String)
    Dim oStatus As Object
    Dim oContacts As CIO.CContacts
    
    On Error GoTo ProcError
    If g_oCI20Session Is Nothing Then
        Set oStatus = CreateObject("mpAXE.CStatus")
        oStatus.Title = "Contact Integration"
        oStatus.Show , "Loading Contact Integration.  Please wait..."

        Set g_oCI20Session = New CIX.CSession
        Set oStatus = Nothing
    End If
    
    If Not g_oCI20Session.Initialized Then
        Set g_oCI20Session = Nothing
        Exit Sub
    End If
    
    m_oContacts.CreateDataFile xFile
    Exit Sub
ProcError:
    RaiseError "MPO.CContacts.CreateDataFile20"
    Exit Sub
End Sub

Public Property Get ToCount() As Integer
    ToCount = m_iToCount
End Property
Public Property Get CcCount() As Integer
    CcCount = m_iCcCount
End Property
Public Property Get BccCount() As Integer
    BccCount = m_iBccCount
End Property
Public Property Get FromCount() As Integer
    FromCount = m_iFromCount
End Property
Public Property Get ToItem(iIndex As Integer) As CContact
    On Error Resume Next
    If ToCount = 0 Then
        Set ToItem = Nothing
    Else
        Set ToItem = Item("T" & Format(iIndex, "0000"))
    End If
End Property
Public Property Get CcItem(iIndex As Integer) As CContact
    On Error Resume Next
    If CcCount = 0 Then
        Set CcItem = Nothing
    Else
        Set CcItem = Item("C" & Format(iIndex, "0000"))
    End If
End Property
Public Property Get BccItem(iIndex As Integer) As CContact
    On Error Resume Next
    If BccCount = 0 Then
        Set BccItem = Nothing
    Else
        Set BccItem = Item("B" & Format(iIndex, "0000"))
    End If
End Property
Public Property Get FromItem(iIndex As Integer) As CContact
    On Error Resume Next
    If FromCount = 0 Then
        Set FromItem = Nothing
    Else
        Set FromItem = Item("F" & Format(iIndex, "0000"))
    End If
End Property

Public Property Get Contacts() As Object
    On Error GoTo ProcError
    If CIVersion = mpCIVersion_1 Then
        Set Contacts = g_appCI.Contacts
    Else
        Set Contacts = m_oContacts
    End If
    Exit Property
ProcError:
    RaiseError "MPO.CContacts.Contacts"
    Exit Property
End Property

Public Function Add(Optional sKey As String) As MPO.CContact
    'create a new object
    Dim objNewMember As CContact
    Set objNewMember = New CContact


    'set the properties passed into the method
    If Len(sKey) = 0 Then
        mCol.Add objNewMember
    Else
        mCol.Add objNewMember, sKey
    End If


    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing
End Function
Public Property Get Item(vntIndexKey As Variant) As CContact
Attribute Item.VB_UserMemId = 0
  Set Item = mCol(vntIndexKey)
End Property
Public Property Get Count() As Long
    Count = mCol.Count
End Property
Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = mCol.[_NewEnum]
End Property
Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub
Private Sub Class_Terminate()
    Set mCol = Nothing
End Sub
