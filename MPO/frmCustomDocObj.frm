VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Object = "{FC715925-5921-4AC0-B4BD-36B619407BAE}#1.1#0"; "MPCM.ocx"
Begin VB.Form frmCustomDocObj 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "XXX"
   ClientHeight    =   6192
   ClientLeft      =   1932
   ClientTop       =   540
   ClientWidth     =   5568
   Icon            =   "frmCustomDocObj.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6192
   ScaleWidth      =   5568
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   4
      Top             =   5760
      Width           =   650
   End
   Begin VB.CommandButton btnContacts 
      Height          =   400
      Left            =   3315
      Picture         =   "frmCustomDocObj.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   5760
      Width           =   705
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4065
      TabIndex        =   3
      Top             =   5760
      Width           =   650
   End
   Begin C1SizerLibCtl.C1Tab tabPages 
      Height          =   6300
      Left            =   -45
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   -120
      Width           =   5655
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "|||"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Caption         =   "5780"
         Height          =   5856
         Left            =   6636
         TabIndex        =   155
         Top             =   12
         Width           =   5640
         Begin VB.CheckBox chkIncludeLetterheadAdmittedIn 
            Appearance      =   0  'Flat
            Caption         =   "Ad&mitted In"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   3795
            TabIndex        =   178
            Top             =   1185
            Width           =   1605
         End
         Begin VB.CheckBox chkIncludeLetterheadName 
            Appearance      =   0  'Flat
            Caption         =   "&Name"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   1845
            TabIndex        =   159
            Top             =   795
            Width           =   750
         End
         Begin VB.CheckBox chkIncludeLetterheadPhone 
            Appearance      =   0  'Flat
            Caption         =   "P&hone"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   2790
            TabIndex        =   160
            Top             =   795
            Width           =   870
         End
         Begin VB.CheckBox chkIncludeLetterheadEMail 
            Appearance      =   0  'Flat
            Caption         =   "&E-Mail"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   3795
            TabIndex        =   161
            Top             =   795
            Width           =   750
         End
         Begin VB.CheckBox chkIncludeLetterheadTitle 
            Appearance      =   0  'Flat
            Caption         =   "T&itle"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   1845
            TabIndex        =   162
            Top             =   1185
            Width           =   600
         End
         Begin VB.CheckBox chkIncludeLetterheadFax 
            Appearance      =   0  'Flat
            Caption         =   "Fa&x"
            ForeColor       =   &H80000008&
            Height          =   345
            Left            =   2790
            TabIndex        =   163
            Top             =   1185
            Width           =   735
         End
         Begin TrueDBList60.TDBCombo cmbLetterheadType 
            Height          =   360
            Left            =   1770
            OleObjectBlob   =   "frmCustomDocObj.frx":0CAC
            TabIndex        =   157
            Top             =   315
            Width           =   3585
         End
         Begin VB.Line linTab4 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5550
            Y1              =   5844
            Y2              =   5844
         End
         Begin VB.Label lblType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Letterhead T&ype:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   285
            TabIndex        =   156
            Top             =   360
            Width           =   1275
         End
         Begin VB.Label lblInclude 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Include Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   285
            TabIndex        =   158
            Top             =   855
            Width           =   1275
         End
         Begin VB.Shape Shape4 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H80000003&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H80000003&
            Height          =   9495
            Left            =   -45
            Top             =   -15
            Width           =   1725
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "5780"
         Height          =   5856
         Left            =   6396
         TabIndex        =   9
         Top             =   12
         Width           =   5640
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   26
            Left            =   1740
            TabIndex        =   109
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   25
            Left            =   1740
            TabIndex        =   107
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   24
            Left            =   1800
            TabIndex        =   106
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   23
            Left            =   1860
            TabIndex        =   104
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   22
            Left            =   1860
            TabIndex        =   102
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   21
            Left            =   1860
            TabIndex        =   100
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   20
            Left            =   1800
            TabIndex        =   98
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   19
            Left            =   1800
            TabIndex        =   96
            Top             =   1140
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   18
            Left            =   1740
            TabIndex        =   95
            Top             =   1080
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   13
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   86
            Top             =   1620
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   12
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   85
            Top             =   1620
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   11
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   84
            Top             =   1620
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   10
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   83
            Top             =   1620
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   12
            Left            =   4500
            TabIndex        =   175
            Top             =   4635
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   13
            Left            =   4530
            TabIndex        =   174
            Top             =   4365
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   14
            Left            =   4515
            TabIndex        =   173
            Top             =   4170
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   15
            Left            =   2820
            TabIndex        =   172
            Top             =   4590
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   16
            Left            =   2805
            TabIndex        =   171
            Top             =   4440
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   17
            Left            =   2850
            TabIndex        =   170
            Top             =   4245
            Visible         =   0   'False
            Width           =   200
         End
         Begin mpControls.MultiLineCombo mlcCustom 
            Height          =   540
            Index           =   2
            Left            =   1755
            TabIndex        =   91
            Top             =   345
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   953
            BackColor       =   -2147483643
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   12
            Left            =   1890
            OleObjectBlob   =   "frmCustomDocObj.frx":3355
            TabIndex        =   149
            Top             =   2175
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   13
            Left            =   1830
            OleObjectBlob   =   "frmCustomDocObj.frx":556A
            TabIndex        =   150
            Top             =   2520
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   14
            Left            =   1935
            OleObjectBlob   =   "frmCustomDocObj.frx":777F
            TabIndex        =   151
            Top             =   2400
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   15
            Left            =   1815
            OleObjectBlob   =   "frmCustomDocObj.frx":9994
            TabIndex        =   152
            Top             =   2400
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   16
            Left            =   1680
            OleObjectBlob   =   "frmCustomDocObj.frx":BBA9
            TabIndex        =   153
            Top             =   2385
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   17
            Left            =   1935
            OleObjectBlob   =   "frmCustomDocObj.frx":DDBE
            TabIndex        =   154
            Top             =   2370
            Visible         =   0   'False
            Width           =   3600
         End
         Begin MSComCtl2.DTPicker calCustom 
            Height          =   315
            Index           =   4
            Left            =   1800
            TabIndex        =   185
            Top             =   4920
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   550
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   73990145
            CurrentDate     =   36558
         End
         Begin MSComCtl2.DTPicker calCustom 
            Height          =   315
            Index           =   5
            Left            =   1800
            TabIndex        =   186
            Top             =   5280
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   550
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   73990145
            CurrentDate     =   36558
         End
         Begin VB.Label lblCustomCal 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   5
            Left            =   300
            TabIndex        =   196
            Top             =   5280
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCal 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   4
            Left            =   180
            TabIndex        =   195
            Top             =   4980
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   26
            Left            =   180
            TabIndex        =   118
            Top             =   1020
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   25
            Left            =   240
            TabIndex        =   116
            Top             =   960
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   24
            Left            =   240
            TabIndex        =   114
            Top             =   960
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   23
            Left            =   240
            TabIndex        =   112
            Top             =   960
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   22
            Left            =   240
            TabIndex        =   110
            Top             =   1020
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   21
            Left            =   240
            TabIndex        =   105
            Top             =   960
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   20
            Left            =   240
            TabIndex        =   101
            Top             =   960
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   19
            Left            =   180
            TabIndex        =   97
            Top             =   960
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   18
            Left            =   180
            TabIndex        =   93
            Top             =   960
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   13
            Left            =   180
            TabIndex        =   192
            Top             =   1500
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   11
            Left            =   240
            TabIndex        =   190
            Top             =   1440
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   10
            Left            =   240
            TabIndex        =   189
            Top             =   1440
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCM 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   4
            Left            =   120
            TabIndex        =   188
            Top             =   5280
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCM 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   3
            Left            =   120
            TabIndex        =   187
            Top             =   5040
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLC 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   2
            Left            =   180
            TabIndex        =   92
            Top             =   1500
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   17
            Left            =   90
            TabIndex        =   120
            Top             =   4545
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Index           =   16
            Left            =   75
            TabIndex        =   119
            Top             =   4680
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   15
            Left            =   75
            TabIndex        =   117
            Top             =   4815
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   14
            Left            =   75
            TabIndex        =   115
            Top             =   4245
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   13
            Left            =   75
            TabIndex        =   113
            Top             =   4410
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   12
            Left            =   75
            TabIndex        =   111
            Top             =   4965
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   17
            Left            =   60
            TabIndex        =   59
            Top             =   3840
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   16
            Left            =   105
            TabIndex        =   58
            Top             =   3870
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   15
            Left            =   15
            TabIndex        =   57
            Top             =   3855
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   14
            Left            =   30
            TabIndex        =   5
            Top             =   3840
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   13
            Left            =   -15
            TabIndex        =   134
            Top             =   3855
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   12
            Left            =   0
            TabIndex        =   133
            Top             =   3825
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Line linTab3 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5550
            Y1              =   5844
            Y2              =   5844
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H80000003&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H80000003&
            Height          =   9495
            Left            =   0
            Top             =   60
            Width           =   1725
         End
      End
      Begin VB.Frame fraPage2 
         BorderStyle     =   0  'None
         Height          =   5856
         Left            =   6156
         TabIndex        =   8
         Top             =   12
         Width           =   5640
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   14
            Left            =   1800
            TabIndex        =   43
            Top             =   3180
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   13
            Left            =   1680
            TabIndex        =   42
            Top             =   3240
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   12
            Left            =   1680
            TabIndex        =   41
            Top             =   3240
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   17
            Left            =   1680
            TabIndex        =   46
            Top             =   3240
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   16
            Left            =   1680
            TabIndex        =   45
            Top             =   3240
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   15
            Left            =   1620
            TabIndex        =   44
            Top             =   3240
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   9
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   77
            Top             =   1380
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   8
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   76
            Top             =   1380
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   7
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   75
            Top             =   1380
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   6
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   74
            Top             =   1380
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   6
            Left            =   4245
            TabIndex        =   169
            Top             =   4200
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   7
            Left            =   4275
            TabIndex        =   168
            Top             =   3930
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   8
            Left            =   4260
            TabIndex        =   167
            Top             =   3765
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   9
            Left            =   2550
            TabIndex        =   166
            Top             =   4455
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   10
            Left            =   2550
            TabIndex        =   165
            Top             =   4125
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   11
            Left            =   2610
            TabIndex        =   164
            Top             =   3810
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   5
            Left            =   1710
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   73
            Top             =   1440
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   11
            Left            =   1635
            TabIndex        =   40
            Top             =   3225
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   10
            Left            =   1650
            TabIndex        =   39
            Top             =   3225
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   9
            Left            =   1635
            TabIndex        =   38
            Top             =   3240
            Visible         =   0   'False
            Width           =   3600
         End
         Begin mpControls.MultiLineCombo mlcCustom 
            Height          =   540
            Index           =   1
            Left            =   1740
            TabIndex        =   89
            Top             =   1995
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   953
            BackColor       =   -2147483643
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   6
            Left            =   2310
            OleObjectBlob   =   "frmCustomDocObj.frx":FFD3
            TabIndex        =   143
            Top             =   2715
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   7
            Left            =   2055
            OleObjectBlob   =   "frmCustomDocObj.frx":121E7
            TabIndex        =   144
            Top             =   2730
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   8
            Left            =   1905
            OleObjectBlob   =   "frmCustomDocObj.frx":143FB
            TabIndex        =   145
            Top             =   2685
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   9
            Left            =   2295
            OleObjectBlob   =   "frmCustomDocObj.frx":1660F
            TabIndex        =   146
            Top             =   2715
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   10
            Left            =   1950
            OleObjectBlob   =   "frmCustomDocObj.frx":18823
            TabIndex        =   147
            Top             =   2685
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   11
            Left            =   1755
            OleObjectBlob   =   "frmCustomDocObj.frx":1AA38
            TabIndex        =   148
            Top             =   2700
            Visible         =   0   'False
            Width           =   3600
         End
         Begin MSComCtl2.DTPicker calCustom 
            Height          =   315
            Index           =   2
            Left            =   1920
            TabIndex        =   181
            Top             =   5040
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   550
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   73990145
            CurrentDate     =   36558
         End
         Begin MSComCtl2.DTPicker calCustom 
            Height          =   315
            Index           =   3
            Left            =   1920
            TabIndex        =   182
            Top             =   5400
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   550
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   73990145
            CurrentDate     =   36558
         End
         Begin VB.Label lblCustomCal 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   3
            Left            =   240
            TabIndex        =   194
            Top             =   5160
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCal 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   2
            Left            =   300
            TabIndex        =   193
            Top             =   5100
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   13
            Left            =   180
            TabIndex        =   55
            Top             =   3240
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   14
            Left            =   240
            TabIndex        =   56
            Top             =   3180
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   12
            Left            =   240
            TabIndex        =   54
            Top             =   3180
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   17
            Left            =   240
            TabIndex        =   62
            Top             =   3180
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   16
            Left            =   180
            TabIndex        =   61
            Top             =   3180
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   15
            Left            =   300
            TabIndex        =   60
            Top             =   3240
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   9
            Left            =   180
            TabIndex        =   82
            Top             =   1320
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   8
            Left            =   120
            TabIndex        =   81
            Top             =   1320
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   7
            Left            =   240
            TabIndex        =   80
            Top             =   1380
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   6
            Left            =   180
            TabIndex        =   79
            Top             =   1380
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCM 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   2
            Left            =   120
            TabIndex        =   184
            Top             =   5400
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCM 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   1
            Left            =   120
            TabIndex        =   183
            Top             =   5040
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLC 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   1
            Left            =   90
            TabIndex        =   90
            Top             =   1995
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   5
            Left            =   45
            TabIndex        =   78
            Top             =   1455
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   11
            Left            =   60
            TabIndex        =   108
            Top             =   4560
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Index           =   10
            Left            =   45
            TabIndex        =   103
            Top             =   4695
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   9
            Left            =   45
            TabIndex        =   128
            Top             =   4830
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   8
            Left            =   45
            TabIndex        =   127
            Top             =   4260
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   7
            Left            =   45
            TabIndex        =   126
            Top             =   4425
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   6
            Left            =   45
            TabIndex        =   125
            Top             =   4980
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   11
            Left            =   105
            TabIndex        =   53
            Top             =   3270
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   10
            Left            =   135
            TabIndex        =   52
            Top             =   3285
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   9
            Left            =   135
            TabIndex        =   51
            Top             =   3285
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   11
            Left            =   60
            TabIndex        =   132
            Top             =   3855
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   10
            Left            =   60
            TabIndex        =   131
            Top             =   3825
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   9
            Left            =   0
            TabIndex        =   50
            Top             =   3855
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   8
            Left            =   60
            TabIndex        =   49
            Top             =   3840
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   7
            Left            =   30
            TabIndex        =   48
            Top             =   3795
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   6
            Left            =   0
            TabIndex        =   47
            Top             =   3825
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Line linTab2 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5550
            Y1              =   5844
            Y2              =   5844
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00C0C0C0&
            FillColor       =   &H00800080&
            Height          =   9495
            Left            =   -45
            Top             =   0
            Width           =   1725
         End
      End
      Begin VB.Frame fraPage1 
         BorderStyle     =   0  'None
         Height          =   5856
         Left            =   12
         TabIndex        =   7
         Top             =   12
         Width           =   5640
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   8
            Left            =   1752
            TabIndex        =   32
            Top             =   900
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   7
            Left            =   1800
            TabIndex        =   30
            Top             =   900
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   6
            Left            =   1800
            TabIndex        =   27
            Top             =   900
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   4
            Left            =   1752
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   69
            Top             =   3360
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   3
            Left            =   1800
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   68
            Top             =   3360
            Visible         =   0   'False
            Width           =   3555
         End
         Begin MPCM.ClientMatterComboBox cmbCM 
            Height          =   315
            Left            =   1770
            TabIndex        =   176
            Top             =   5000
            Visible         =   0   'False
            Width           =   3675
            _ExtentX        =   6477
            _ExtentY        =   550
            SourceClass     =   "mpCMIA.CClientMatter"
            Col1Caption     =   "No."
            ColWidth1       =   1020
            ColWidth2       =   1020
            ListRows        =   6
            ComboType       =   2
            SelectTextOnFocus=   -1  'True
            PopulateOnLoad  =   0   'False
            AdditionalClientFilter=   ""
            AdditionalMatterFilter=   ""
         End
         Begin MSComCtl2.DTPicker calCustom 
            Height          =   330
            Index           =   0
            Left            =   1770
            TabIndex        =   136
            Top             =   4080
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   572
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   73990145
            CurrentDate     =   36558
         End
         Begin mpControls.MultiLineCombo mlcCustom 
            Height          =   540
            Index           =   0
            Left            =   1770
            TabIndex        =   87
            Top             =   2460
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   953
            BackColor       =   -2147483643
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   2
            Left            =   1752
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   67
            Top             =   3390
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   1
            Left            =   1755
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   65
            Top             =   3165
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.TextBox mltCustom 
            Appearance      =   0  'Flat
            Height          =   300
            Index           =   0
            Left            =   1752
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   63
            Top             =   3615
            Visible         =   0   'False
            Width           =   3555
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   5
            Left            =   1785
            TabIndex        =   37
            Top             =   2040
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   4
            Left            =   2640
            TabIndex        =   36
            Top             =   1920
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   3
            Left            =   3720
            TabIndex        =   35
            Top             =   1800
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   2
            Left            =   3240
            TabIndex        =   34
            Top             =   2160
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   1
            Left            =   2175
            TabIndex        =   33
            Top             =   1680
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.CheckBox chkCustom 
            Appearance      =   0  'Flat
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   0
            Left            =   2280
            TabIndex        =   31
            Top             =   2160
            Visible         =   0   'False
            Width           =   200
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   5
            Left            =   1752
            TabIndex        =   25
            Top             =   930
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   4
            Left            =   1770
            TabIndex        =   22
            Top             =   930
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   3
            Left            =   1770
            TabIndex        =   19
            Top             =   930
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   2
            Left            =   1770
            TabIndex        =   17
            Top             =   930
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   1
            Left            =   1770
            TabIndex        =   13
            Top             =   930
            Visible         =   0   'False
            Width           =   3600
         End
         Begin VB.TextBox txtCustom 
            Appearance      =   0  'Flat
            Height          =   315
            Index           =   0
            Left            =   1770
            TabIndex        =   11
            Top             =   930
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbAuthor 
            Height          =   375
            Left            =   1755
            OleObjectBlob   =   "frmCustomDocObj.frx":1CC4D
            TabIndex        =   1
            Top             =   195
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   1
            Left            =   1740
            OleObjectBlob   =   "frmCustomDocObj.frx":20B92
            TabIndex        =   137
            Top             =   1335
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   0
            Left            =   1755
            OleObjectBlob   =   "frmCustomDocObj.frx":22DA6
            TabIndex        =   138
            Top             =   1350
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   2
            Left            =   1755
            OleObjectBlob   =   "frmCustomDocObj.frx":24FBA
            TabIndex        =   139
            Top             =   1335
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   3
            Left            =   1725
            OleObjectBlob   =   "frmCustomDocObj.frx":271CE
            TabIndex        =   140
            Top             =   1350
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   4
            Left            =   1740
            OleObjectBlob   =   "frmCustomDocObj.frx":293E2
            TabIndex        =   141
            Top             =   1335
            Visible         =   0   'False
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbCustom 
            Height          =   345
            Index           =   5
            Left            =   1770
            OleObjectBlob   =   "frmCustomDocObj.frx":2B5F6
            TabIndex        =   142
            Top             =   1350
            Visible         =   0   'False
            Width           =   3600
         End
         Begin MSComCtl2.DTPicker calCustom 
            Height          =   312
            Index           =   1
            Left            =   1776
            TabIndex        =   179
            Top             =   4560
            Visible         =   0   'False
            Width           =   3600
            _ExtentX        =   6350
            _ExtentY        =   550
            _Version        =   393216
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Format          =   73990145
            CurrentDate     =   36558
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   8
            Left            =   240
            TabIndex        =   29
            Top             =   780
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   7
            Left            =   180
            TabIndex        =   26
            Top             =   840
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   6
            Left            =   120
            TabIndex        =   23
            Top             =   840
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   4
            Left            =   180
            TabIndex        =   72
            Top             =   3420
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Index           =   3
            Left            =   240
            TabIndex        =   71
            Top             =   3540
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCal 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   1
            Left            =   120
            TabIndex        =   180
            Top             =   4560
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCM 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   0
            Left            =   120
            TabIndex        =   177
            Top             =   5060
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCal 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   0
            Left            =   120
            TabIndex        =   135
            Top             =   4140
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLC 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   0
            Left            =   120
            TabIndex        =   88
            Top             =   2445
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   2
            Left            =   165
            TabIndex        =   70
            Top             =   3660
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   1
            Left            =   180
            TabIndex        =   66
            Top             =   3690
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomMLT 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   0
            Left            =   105
            TabIndex        =   64
            Top             =   3630
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   5
            Left            =   120
            TabIndex        =   124
            Top             =   1770
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   4
            Left            =   150
            TabIndex        =   123
            Top             =   1770
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   3
            Left            =   180
            TabIndex        =   122
            Top             =   1770
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   2
            Left            =   120
            TabIndex        =   121
            Top             =   1770
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Index           =   1
            Left            =   120
            TabIndex        =   99
            Top             =   1770
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCheck 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX Check"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Index           =   0
            Left            =   135
            TabIndex        =   94
            Top             =   1770
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   5
            Left            =   120
            TabIndex        =   28
            Top             =   1365
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   4
            Left            =   135
            TabIndex        =   24
            Top             =   1335
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   3
            Left            =   120
            TabIndex        =   21
            Top             =   1350
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   2
            Left            =   120
            TabIndex        =   15
            Top             =   1335
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   1
            Left            =   150
            TabIndex        =   130
            Top             =   1335
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomCombo 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   0
            Left            =   165
            TabIndex        =   129
            Top             =   1350
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   0
            Left            =   120
            TabIndex        =   10
            Top             =   930
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   1
            Left            =   120
            TabIndex        =   12
            Top             =   930
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   2
            Left            =   120
            TabIndex        =   14
            Top             =   930
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   3
            Left            =   120
            TabIndex        =   16
            Top             =   930
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   4
            Left            =   120
            TabIndex        =   18
            Top             =   930
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustomText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "XXX"
            ForeColor       =   &H00FFFFFF&
            Height          =   400
            Index           =   5
            Left            =   120
            TabIndex        =   20
            Top             =   930
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Line linTab1 
            BorderColor     =   &H80000005&
            X1              =   -36
            X2              =   5994
            Y1              =   5844
            Y2              =   5844
         End
         Begin VB.Label lblAuthor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            TabIndex        =   0
            Top             =   240
            Width           =   1455
         End
         Begin VB.Shape shpSidebar 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00C0C0C0&
            FillColor       =   &H00800080&
            Height          =   9495
            Left            =   -135
            Top             =   -210
            Width           =   1800
         End
      End
   End
   Begin VB.Label lblCustomMLT 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "XXX"
      ForeColor       =   &H00FFFFFF&
      Height          =   525
      Index           =   12
      Left            =   0
      TabIndex        =   191
      Top             =   0
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_EditAuthorOptions 
         Caption         =   "&Edit Author Defaults..."
      End
      Begin VB.Menu mnuAuthor_CopyOptions 
         Caption         =   "Cop&y Author Defaults..."
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmCustomDocObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const mpTemplate As String = "XXX"
Const mpMaxTextBoxes As Byte = 9 '*c 4348
Const mpMaxMultComboBoxes As Byte = 1
Const mpMaxCalendars As Byte = 2 '*c 4046 jsw
Const mpMaxMultTextBoxes As Byte = 5 '*c 4348
Const mpMaxComboBoxes As Byte = 6
Const mpMaxChkBx As Byte = 6
Const mpMaxCMControls = 1 '*c

Private m_iNextPosition(2) As Integer
Private m_iNumTextBoxes(2) As Integer
Private m_iNumMultComboBoxes(2) As Integer
Private m_iNumMultTextBoxes(2) As Integer
Private m_iNumListBoxes(2) As Integer
Private m_iNumComboBoxes(2) As Integer
Private m_iNumCalendars(2) As Integer
Private m_iNumChkBx(2) As Integer
Private m_iNumCMControls(2) As Integer '*c
Private m_oFirstCtlOnTab(2) As Control

Private m_oMenu As MPO.CAuthorMenu
Private m_PersonsForm As MPO.CPersonsForm
Private m_bInitializing As Boolean
Private m_oOptions As MPO.COptionsForm
Private m_oDocObj As MPO.IDocObj
Private m_oPrevCtl(2) As Control
Private m_oCustCtls As mpDB.CCustomControls
Private m_bCancelled As Boolean
Private m_bAuthorDirty As Boolean
Private b_Opened As Boolean
Private m_oLH As MPO.CLetterhead

'ClientMatter control'*c
Private m_bCM_RunConnected As Boolean '*c
Private m_bCM_ShowLoadMsg As Boolean '*c
Private m_bCM_AllowFilter As Boolean '*c
Private m_bCM_AllowUserToClearFilters As Boolean '*c
Private m_bCM_AllowValidation As Boolean '*c
Private m_xCM_Column1Heading As String '*c
Private m_xCM_Column2Heading As String '*c
Private m_xCM_Separator As String '*c
Private m_xCM_Backend As String '*c
Private m_xCM_RunConnectedTooltip As String
Private m_xCM_RunDisconnectedTooltip As String
Private m_bCM_SearchByMatter As Boolean '***9.7.1

Implements IDocObjForm

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property

Private Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property

Public Property Let DocObject(oNew As IDocObj)
    Set m_oDocObj = oNew
End Property

Public Property Get DocObject() As IDocObj
    Set DocObject = m_oDocObj
End Property

Public Property Let AuthorIsDirty(bNew As Boolean)
    m_bAuthorDirty = bNew
End Property

Public Property Get AuthorIsDirty() As Boolean
    AuthorIsDirty = m_bAuthorDirty
End Property

Public Sub UpdateForm()
'cycle through visible controls, getting
'appropriate value from DocObject property
    Dim oCtl As Control
    Dim oProps As MPO.CCustomProperties
    
    On Error GoTo ProcError
    
    Set oProps = Me.DocObject.CustomProperties
    
    On Error Resume Next
    For Each oCtl In Me.Controls
'       check for tag - only controls with a tag
'       are hooked up to DocObject properties
        With oCtl
            If Len(.Tag) Then
                Select Case m_oCustCtls.Item(.Tag).ControlType
                    Case mpCustomControl_MultilineTextBox, _
                        mpCustomControl_TextBox, _
                        mpCustomControl_MultiLineCombo:
                        .Text = oProps(.Tag).Text
                    Case mpCustomControl_CheckBox, _
                        mpCustomControl_Calendar:
                        If oProps(.Tag).Value <> mpUndefined Then _
                            .Value = oProps(.Tag).Value
                    Case mpCustomControl_ComboBox, _
                        mpCustomControl_OrdinalDateCombo:
                        .Text = oProps(.Tag).Value
                        .BoundText = .Text
                    Case mpCustomControl_ListBox
                        .Text = oProps(.Tag).Value
                        .BoundText = .Text
                    Case mpCustomControl_ClientMatter '*c
'                       set up list'*c
                        .AdditionalClientFilter = DocObject.Document _
                            .GetVar("CustomProperty_1_CMFilterString")  '*c
                        .AdditionalMatterFilter = DocObject.Document _
                            .GetVar("CustomProperty_1_CMFilterString")  '*c
'                        .Refresh '*c
                        .Value = oProps(.Tag).Value '*c
                End Select
            End If
        End With
    Next oCtl
    
'CharlieCore 9.2.0 - CustomLetterhead
'---get Letterhead properties, load controls on reuse
    If Me.DocObject.Template.CustomDialog.ShowLetterheadTab Then
        Me.cmbLetterheadType.BoundText = m_oLH.LetterheadType
        Me.chkIncludeLetterheadEMail = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadEmail")
        Me.chkIncludeLetterheadName = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadName")
        Me.chkIncludeLetterheadPhone = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadPhone")
        Me.chkIncludeLetterheadTitle = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadTitle")
        Me.chkIncludeLetterheadFax = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadFax")
        Me.chkIncludeLetterheadAdmittedIn = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadAdmittedIn")
        cmbLetterheadType_LostFocus
        chkIncludeLetterheadEMail_Click
        chkIncludeLetterheadName_Click
        chkIncludeLetterheadPhone_Click
        chkIncludeLetterheadTitle_Click
        chkIncludeLetterheadFax_Click
        chkIncludeLetterheadAdmittedIn_Click
    End If
    
    Me.AuthorIsDirty = False
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.UpdateForm"
    Exit Sub
End Sub
    
Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel, "zzmpBody"
End Sub

Private Sub btnContacts_Click()
    On Error GoTo ProcError
    GetContacts
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnContacts_GotFocus()
    OnControlGotFocus Me.btnContacts
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Me.Cancelled = False
    Me.Hide
    DoEvents
    SaveStickyControls
    Exit Sub

ProcError:
    EchoOn
    RaiseError "MPO.frmCustomDocObj.btnFinish_Click"
    Word.Application.ScreenUpdating = True
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Cancelled = True
    Me.Hide
    DoEvents
    Word.Application.ScreenUpdating = True
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.btnCancel_Click"
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish, "zzmpBody"
End Sub

Private Sub calCustom_GotFocus(Index As Integer)
    OnControlGotFocus Me.calCustom(Index), _
        Me.DocObject.CustomProperties(Me.calCustom(Index).Tag).Bookmark
End Sub

Private Sub calCustom_LostFocus(Index As Integer)
    Dim oProp As MPO.CCustomProperty
    
    On Error GoTo ProcError
    With Me.calCustom(Index)
        Set oProp = Me.DocObject.CustomProperties(.Tag)
        oProp.Text = Format(.Value, .CustomFormat)
        oProp.Value = .Value
    End With

    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.calCustom_LostFocus"
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_Click()
    On Error GoTo ProcError
    m_oLH.IncludeAdmittedIn = -(Me.chkIncludeLetterheadAdmittedIn)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadAdmittedIn, "zzmpLetterheadAdmittedIn"
End Sub

Private Sub chkIncludeLetterheadEMail_Click()
    On Error GoTo ProcError
    m_oLH.IncludeEMail = -(Me.chkIncludeLetterheadEMail)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadEMail_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadEMail, "zzmpLetterheadEMail"
End Sub

Private Sub chkIncludeLetterheadFax_Click()
    On Error GoTo ProcError
    m_oLH.IncludeFax = -(Me.chkIncludeLetterheadFax)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadFax_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadFax, "zzmpLetterheadFax"
End Sub

Private Sub chkIncludeLetterheadName_Click()
    On Error GoTo ProcError
    m_oLH.IncludeName = -(Me.chkIncludeLetterheadName)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadName_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadName, "zzmpLetterheadName"
End Sub

Private Sub chkIncludeLetterheadPhone_Click()
    On Error GoTo ProcError
    m_oLH.IncludePhone = -(Me.chkIncludeLetterheadPhone)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadPhone, "zzmpLetterheadPhone"
End Sub

Private Sub chkIncludeLetterheadTitle_Click()
    On Error GoTo ProcError
    m_oLH.IncludeTitle = -(Me.chkIncludeLetterheadTitle)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadTitle, "zzmpLetterheadTitle"
End Sub

Private Sub cmbAuthor_Change()
    AuthorIsDirty = True
End Sub

Private Sub cmbAuthor_Close()
    b_Opened = False
End Sub

Private Sub cmbAuthor_Click()
    If b_Opened Then
        AuthorIsDirty = False
    End If
End Sub

Private Sub cmbAuthor_GotFocus()
    OnControlGotFocus Me.cmbAuthor, "zzmpAuthorName"
End Sub

Private Sub cmbAuthor_ItemChange()
    On Error GoTo ProcError
    
    If Me.Initializing Then
        Exit Sub
    End If
    
    Me.AuthorIsDirty = True
    
    With Me.DocObject
        .Author = g_oDBs.People.Item(Me.cmbAuthor.BoundText)
        EchoOff
        If .Template.OptionsTable = Empty Then
            .CustomProperties.UpdateForAuthor .Author
        Else
            .CustomProperties.UpdateForAuthor .Author, .Author.Options(.Template.ID)
        End If
        If .Template.CustomDialog.ShowLetterheadTab Then
            m_oLH.Author = .Author
            cmbLetterheadType_LostFocus     '9.5.0
        End If
        EchoOn
    End With
    
    Me.AuthorIsDirty = False
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbAuthor_ItemChange"
    Exit Sub
End Sub

Private Sub cmbAuthor_LostFocus()
    Dim oPerson As mpDB.CPerson
    
'   update object with values of selected person
    If Me.DocObject.Template.CustomDialog.ShowLetterheadTab Then
        If Not Me.Initializing Then
            Set oPerson = Me.DocObject.Author
            With m_oLH
                .Author = oPerson
                .Name = oPerson.FullName
                .Email = oPerson.Email
                .Phone = oPerson.Phone
                .Fax = oPerson.Fax
                .Title = oPerson.Title
            End With
        End If
    End If

End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbAuthor_Mismatch"
    Exit Sub
End Sub

Private Sub cmbAuthor_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Dim bEnabled As Boolean
    
'   enable specified menu items only if author
'   is currently in the db
    On Error GoTo ProcError
    
    If TypeOf Me.DocObject.Author Is CReUseAuthor Then
        bEnabled = False
    Else
        bEnabled = True
    End If
    
    If Button = 2 Then
        Me.mnuAuthor_EditAuthorOptions.Enabled = bEnabled
        Me.mnuAuthor_Favorite.Enabled = bEnabled
        Me.mnuAuthor_SetDefault.Enabled = bEnabled
        Me.mnuAuthor_Favorite.Checked = Me.DocObject.Author.Favorite
        Me.PopupMenu Me.mnuAuthor
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbAuthor_MouseDown"
    Exit Sub
End Sub

Private Sub cmbAuthor_Open()
    b_Opened = True
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If m_bAuthorDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbAuthor_Validate"
    Exit Sub
End Sub

Private Sub cmbCustom_GotFocus(Index As Integer)
    OnControlGotFocus Me.cmbCustom(Index), _
        Me.DocObject.CustomProperties(Me.cmbCustom(Index).Tag).Bookmark
End Sub

Private Sub cmbCustom_LostFocus(Index As Integer)
    Dim oProp As MPO.CCustomProperty
    
    On Error GoTo ProcError
    With Me.cmbCustom(Index)
        Set oProp = Me.DocObject.CustomProperties(.Tag)
'       set the text property equal to the text of the control
        oProp.Text = .Text
        
'       set the value equal to the bound text if possible
        If Me.cmbCustom(Index).LimitToList Then
            oProp.Text = .BoundText
        Else
            oProp.Text = .Text
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbCustom_LostFocus"
    Exit Sub
End Sub

Private Sub chkCustom_GotFocus(Index As Integer)
    OnControlGotFocus Me.chkCustom(Index), _
        Me.DocObject.CustomProperties(Me.chkCustom(Index).Tag).Bookmark
End Sub

Private Sub chkCustom_LostFocus(Index As Integer)
    Dim oProp As MPO.CCustomProperty
    
    On Error GoTo ProcError
    With Me.chkCustom(Index)
        Set oProp = Me.DocObject.CustomProperties(.Tag)
        oProp.Text = CStr(.Value)
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.chkCustom_LostFocus"
    Exit Sub
End Sub

Private Sub cmbCustom_Mismatch(Index As Integer, NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCustom(Index), Reposition
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbCustom_Mismatch"
    Exit Sub
End Sub

Private Sub cmbCustom_Validate(Index As Integer, Cancel As Boolean)
    On Error GoTo ProcError
    If m_bAuthorDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbCustom(Index))
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbMismatch_Validate"
    Exit Sub
End Sub

Private Sub cmbLetterheadType_GotFocus()
    OnControlGotFocus Me.cmbLetterheadType
End Sub

Private Sub cmbLetterheadType_ItemChange()
    On Error GoTo ProcError
'*  enable/disable author details per LH type def
    Application.ScreenUpdating = False
    With g_oDBs.LetterheadDefs.Item(Me.cmbLetterheadType.BoundText)
        Me.chkIncludeLetterheadEMail.Enabled = .AllowAuthorEMail
        Me.chkIncludeLetterheadName.Enabled = .AllowAuthorName
        Me.chkIncludeLetterheadPhone.Enabled = .AllowAuthorPhone
        Me.chkIncludeLetterheadTitle.Enabled = .AllowAuthorTitle
        Me.chkIncludeLetterheadFax.Enabled = .AllowAuthorFax
        Me.chkIncludeLetterheadAdmittedIn.Enabled = .AllowAuthorAdmittedIn
        Me.lblInclude.Enabled = .AllowAuthorEMail Or .AllowAuthorName Or _
                                .AllowAuthorPhone Or .AllowAuthorFax Or _
                                .AllowAuthorTitle Or .AllowAuthorAdmittedIn
    End With
    Application.ScreenRefresh
    EchoOn
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_LostFocus()
    On Error Resume Next   '9.5.0
    DoEvents
    Application.ScreenUpdating = False
    With m_oLH
        .Author = Me.DocObject.Author
        .LetterheadType = Me.cmbLetterheadType.BoundText
        .Section = Selection.Sections.First.Index
        .Refresh
    End With
    Application.ScreenUpdating = True
End Sub

Private Sub cmbLetterheadType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLetterheadType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mltCustom_DblClick(Index As Integer)
    On Error GoTo ProcError
    If m_oCustCtls.Item(Me.mltCustom(Index).Tag).CIList > 0 Then
        GetContacts
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCustom_DblClick(Index As Integer)
    On Error GoTo ProcError
    If m_oCustCtls.Item(Me.txtCustom(Index).Tag).CIList > 0 Then
        GetContacts
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF2 Then
        btnContacts_Click
    ElseIf Shift = 0 Then
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Function OnFormKeyDown(KeyCode As Integer)
    If KeyCode = vbKeyF11 Then
        Word.ActiveDocument.ActiveWindow.LargeScroll , 1
        KeyCode = 0
    ElseIf KeyCode = vbKeyF12 Then
        Word.ActiveDocument.ActiveWindow.LargeScroll 1
        KeyCode = 0
    End If
End Function

Private Sub Form_Paint()
    On Error Resume Next
    m_oFirstCtlOnTab(Me.tabPages.CurrTab).SetFocus
End Sub

Private Sub Form_Resize()
    Dim sH As Single
    Dim sW As Single
    Dim sBtnTop As Single
    
    On Error GoTo ProcError
    sH = Me.Height
    sW = Me.Width
    
    With Me.tabPages
        If Val(Word.Application.Version) < 15 Then
            .Height = sH - 360
        Else
            .Height = sH - 435
        End If
        .Width = sW - 5
    End With
    
    sBtnTop = Me.tabPages.Height - 545
    
    Me.btnCancel.Left = sW - 900
    Me.btnFinish.Left = sW - 1590
    Me.btnContacts.Left = sW - 2340
    
    Me.btnCancel.Top = sBtnTop
    Me.btnFinish.Top = sBtnTop
    Me.btnContacts.Top = sBtnTop
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.Form_Resize"
    Exit Sub
End Sub

Private Sub mlcCustom_GotFocus(Index As Integer)
    OnControlGotFocus Me.mlcCustom(Index), _
        Me.DocObject.CustomProperties(Me.mlcCustom(Index).Tag).Bookmark
End Sub

Private Sub mlcCustom_LostFocus(Index As Integer)
    Dim oProp As MPO.CCustomProperty
    
    On Error GoTo ProcError
    With Me.mlcCustom(Index)
        Set oProp = Me.DocObject.CustomProperties(.Tag)
        oProp.Text = .Text
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.mlcCustom_LostFocus"
    Exit Sub
End Sub

'******************************************************************************
'Menu events
'******************************************************************************
Private Sub mnuAuthor_AddNew_Click()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.cmbAuthor.Columns("fldID")
    m_oMenu.AddPerson Me.cmbAuthor, lID
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.mnuAuthor_AddNew_Click"
    Exit Sub
End Sub

Private Sub mnuAuthor_Favorite_Click()
    Dim lID As Long
    
    On Error Resume Next
    DoEvents
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ToggleFavorite Me.cmbAuthor, lID
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.mnuAuthor_Favorite_Click"
    Exit Sub
End Sub

Private Sub mnuAuthor_Copy_Click()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
'   if author isn't in list, the
'   author is the reuse author
    On Error GoTo ProcError
    
    ' If Me.cmbAuthor.Row = -1 Then
    
    If TypeOf Me.DocObject.Author Is CReUseAuthor Then '***4337
'       use reuse author as source
        Set oSource = Me.DocObject.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oDBs.People(Me.cmbAuthor.BoundText)
    End If
    
'   copy the person
    Set oCopy = m_PersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.mnuAuthor_Copy_Click"
    Exit Sub
End Sub

Private Sub mnuAuthor_ManageLists_Click()
    Dim lID As Long
    
'   get current selection
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ManageAuthors Me.cmbAuthor, lID
    Else
        m_oMenu.ManageAuthors Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.mnuAuthor_ManageLists_Click"
    Exit Sub
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    On Error GoTo ProcError
    With Me.DocObject.Template
        .DefaultAuthor = g_oDBs.SetDefaultAuthor( _
            Me.cmbAuthor.BoundText, .FileName)
        .UpdateDefaults .DefaultAuthor.ID, Me.DocObject
        Word.Application.ScreenUpdating = True
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.mnuAuthor_SetDefault_Click"
    Exit Sub
End Sub

Private Sub tabPages_Click()
'sets focus to the appropriate control
'in this array - the controls are stored
'in array by the procedure PositionControl
    On Error Resume Next
    m_oFirstCtlOnTab(Me.tabPages.CurrTab).SetFocus
End Sub

'**********************************************************
'   Form Event Procedures
'**********************************************************
Private Sub Form_Activate()
    Dim lID As Long
    Dim iSource As Integer
    Dim datStart As Date
    Dim oDefAuthor As mpDB.CPerson
    Dim oCtl As VB.Control
    Dim bCreated As Boolean

'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    On Error GoTo ProcError
    If Me.Initializing Then
        Me.tabPages.CurrTab = 0
        
        bCreated = Me.DocObject.Document.IsCreated

'       set up author control if author control is to be shown
        If Me.DocObject.Template.CustomDialog.ShowAuthorControl Then
            If bCreated Then
'               load reuse author name - remember, that this author
'               may not be in the users list of authors - it may
'               have been created by another user, one who has this author-
'               thus we can't set the bound text = to the author id - it
'               may not be there
                Me.cmbAuthor.Text = Me.DocObject.Author.DisplayName
            Else
                Me.cmbAuthor.BoundText = Me.DocObject.Author.ID
'**************9.3.1#2051/CEH**************
                m_bInitializing = False
                ' Fill in any author detail custom properties for default author
                cmbAuthor_ItemChange
                cmbAuthor_LostFocus
'**************************************
                Me.AuthorIsDirty = False
            End If
        End If
        
        If Not bCreated Then
'           load default values
            UpdateDefaults
        Else
'           load reuse values
            UpdateForm
        End If
        
    End If
    
    cmbAuthor_Change
    Me.AuthorIsDirty = False
    
    EchoOn
    Word.Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    'mpBase2.MoveToLastPosition Me
    
    m_bInitializing = False
    Exit Sub
ProcError:
    EchoOn
    RaiseError "MPO.frmCustomDocObj.Form_Activate"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim oCurCtl As Control
    Dim iCtlIndex As Integer
    
    On Error GoTo ProcError
    Me.Top = 20000
    Me.Cancelled = True
    m_bInitializing = True
    
    Set m_PersonsForm = New MPO.CPersonsForm
    Set m_oOptions = New MPO.COptionsForm
    Set m_oMenu = New MPO.CAuthorMenu
    
'   set up dialog
    SetUpDialog
    
'   get controls for form
    Set m_oCustCtls = g_oDBs.CustomControls(Me.DocObject.Template.ID)

    Dim oCustCtl As mpDB.CCustomControl
    
    For i = 1 To m_oCustCtls.Count
'HERE
        Set oCustCtl = m_oCustCtls.Item(i)
'       get control to which this custom
'       control will be assigned
        Select Case oCustCtl.ControlType
            Case mpCustomControl_TextBox
                Set oCurCtl = SetUpTextBox(oCustCtl)
            Case mpCustomControl_ComboBox, _
                 mpCustomControl_ListBox, _
                 mpCustomControl_OrdinalDateCombo
                Set oCurCtl = SetUpComboList(oCustCtl)
            Case mpCustomControl_CheckBox
                Set oCurCtl = SetUpCheckBox(oCustCtl)
            Case mpCustomControl_MultilineTextBox
                Set oCurCtl = SetUpMultilineTextBox(oCustCtl)
            Case mpCustomControl_MultiLineCombo
                Set oCurCtl = SetUpMultilineCombo(oCustCtl)
            Case mpCustomControl_Calendar
                Set oCurCtl = SetUpCalendar(oCustCtl)
            Case mpCustomControl_ClientMatter '*c
                Set oCurCtl = SetUpClientMatterControl(oCustCtl) '*c
        End Select
        
'           set this control as the previous control
        Set m_oPrevCtl(oCustCtl.TabID) = oCurCtl
'END HERE
    Next i
    
'   setup author combo
    If Me.DocObject.Template.CustomDialog.ShowAuthorControl Then
        With Me.cmbAuthor
            .Array = g_oDBs.People.ListSource
            .Rebind
            ResizeTDBCombo Me.cmbAuthor, 10
        End With
    End If

'chcore_cust
'   setup Letterhead Tab
    If Me.DocObject.Template.CustomDialog.ShowLetterheadTab Then
        Set m_oLH = New CLetterhead
        m_oLH.Template = g_oTemplates(Word.ActiveDocument.AttachedTemplate)
    
        With Me.cmbLetterheadType
    '---    load different LH types by using
    '       m_oLH.Template.ID -- see note above
            .Array = g_oDBs.LetterheadDefs(m_oLH.Template.ID).ListSource
            .Rebind
            ResizeTDBCombo Me.cmbLetterheadType, 6
        End With
        
    End If

'   set up author menu
    If Me.DocObject.Template.OptionsTable = Empty Then
        Me.mnuAuthor_CopyOptions.Visible = False
        Me.mnuAuthor_EditAuthorOptions.Visible = False
        Me.mnuAuthor_Sep1.Visible = False
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.Form_Load"
    Exit Sub
End Sub

Private Sub SetUpDialog()
'assigns dialog properties
    Dim oDef As mpDB.CCustomDialogDef
    
    On Error GoTo ProcError
    
    Set oDef = Me.DocObject.Template.CustomDialog

    Me.Height = oDef.Height
    Me.Width = oDef.Width
    
'   show CI integration if specified
    Me.btnContacts.Visible = (oDef.CIFormat > 0)
    Me.btnContacts.Height = 405
    
    With Me.tabPages
'       set dialog title bar
        Me.Caption = oDef.Caption
        
'       show/hide tabs
        .TabVisible(0) = (oDef.Tab1 <> Empty)
        .TabVisible(1) = (oDef.Tab2 <> Empty)
        .TabVisible(2) = (oDef.Tab3 <> Empty)
        .TabVisible(3) = oDef.ShowLetterheadTab
        
        If ((Not .TabVisible(1)) And (Not .TabVisible(2)) And (Not .TabVisible(3))) Then
            .TabHeight = 1
            'Me.Line8.Visible = False
        Else
'           set tab button captions
            If oDef.Tab1 <> "" Then
                .TabCaption(0) = oDef.Tab1
            End If
            If oDef.Tab2 <> "" Then
                .TabCaption(1) = oDef.Tab2
            End If
            If oDef.Tab3 <> "" Then
                .TabCaption(2) = oDef.Tab3
            End If
            If oDef.ShowLetterheadTab Then
                .TabCaption(3) = "&Letterhead"
            End If
        End If
    End With
        
'   show/hide author control
    Me.cmbAuthor.Visible = oDef.ShowAuthorControl
    Me.lblAuthor.Visible = oDef.ShowAuthorControl
    
'   set position of lines on each tab
    Dim Y As Integer
    Y = Me.tabPages.Height - 525
    Me.linTab1.Y1 = Y
    Me.linTab1.Y2 = Y
    Me.linTab2.Y1 = Y
    Me.linTab2.Y2 = Y
    Me.linTab3.Y1 = Y
    Me.linTab3.Y2 = Y
    Me.linTab4.Y1 = Y
    Me.linTab4.Y2 = Y
    Me.shpSidebar.Height = Me.linTab1.Y1 + 215
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetupDialog"
    Exit Sub
End Sub

Private Sub GetContacts()
    Dim xTo As String
    Dim xFrom As String
    Dim xCC As String
    Dim xBCC As String
    Dim i As Integer
    Dim colContacts As CContacts
    Dim oCIFormat As mpDB.CCIFormat
    Dim oCtl As Control
    Dim lCIFormat As Long
    Static bCIActive As Boolean
    
    On Error GoTo ProcError
    lCIFormat = Me.DocObject.Template.CustomDialog.CIFormat
    
    If Not g_bShowAddressBooks Or (lCIFormat = 0) Then
        Exit Sub
    End If
    
    If bCIActive Then Exit Sub
    bCIActive = True
    
    Set oCIFormat = g_oDBs.CIFormats.Item(lCIFormat)
    
    Set colContacts = New CContacts
            
    With oCIFormat
        ' Retrieve contacts based on CI Format options
        colContacts.Retrieve .IncludeTo, .IncludeFrom, _
                    .IncludeCC, .IncludeBCC, .IncludePhone, .IncludeFax, _
                    .IncludeEMail, .OnEmptyAction, .PromptForPhones, _
                    .DefaultList, .ToNamesOnly, .FromNamesOnly, .CCNamesOnly, _
                    .BCCNamesOnly
    End With
        
    With colContacts
        If Not oCIFormat.ToNamesOnly Then
            For i = 1 To .ToCount
                With .ToItem(i)
                    xTo = xTo & .Detail
                    If .Phone <> "" Then
                        xTo = xTo & vbCrLf
                        If oCIFormat.PhoneLabel <> "" Then _
                            xTo = xTo & oCIFormat.PhoneLabel & " "
                        xTo = xTo & .Phone
                    End If
                    If .Fax <> "" Then
                        xTo = xTo & vbCrLf
                        If oCIFormat.FaxLabel <> "" Then _
                            xTo = xTo & oCIFormat.FaxLabel & " "
                        xTo = xTo & .Fax
                    End If
                    If .Email <> "" Then
                        xTo = xTo & vbCrLf
                        If oCIFormat.EmailLabel <> "" Then _
                            xTo = xTo & oCIFormat.EmailLabel & " "
                        xTo = xTo & .Email
                    End If
                End With
                If i < .ToCount Then _
                    xTo = xTo & vbCrLf & vbCrLf
            Next i
        Else
'           return only the display name
            For i = 1 To .ToCount
                xTo = xTo & .ToItem(i).DisplayName(False)
                If i < .ToCount Then _
                    xTo = xTo & vbCrLf
            Next i
        End If
            
        If Not oCIFormat.FromNamesOnly Then
'           return string with all From address detail
            For i = 1 To .FromCount
                With .FromItem(i)
                    xFrom = xFrom & .Detail
                    If .Phone <> "" Then
                        xFrom = xFrom & vbCrLf
                        If oCIFormat.PhoneLabel <> "" Then _
                            xFrom = xFrom & oCIFormat.PhoneLabel & " "
                        xFrom = xFrom & .Phone
                    End If
                    If .Fax <> "" Then
                        xFrom = xFrom & vbCrLf
                        If oCIFormat.FaxLabel <> "" Then _
                            xFrom = xFrom & oCIFormat.FaxLabel & " "
                        xFrom = xFrom & .Fax
                    End If
                    If .Email <> "" Then
                        xFrom = xFrom & vbCrLf
                        If oCIFormat.EmailLabel <> "" Then _
                            xFrom = xFrom & oCIFormat.EmailLabel & " "
                        xFrom = xFrom & .Email
                    End If
                End With
                If i < .FromCount Then _
                    xFrom = xFrom & vbCrLf & vbCrLf
            Next i
        Else
'           return only the display name
            For i = 1 To .FromCount
                xFrom = xFrom & .FromItem(i).DisplayName(False)
                If i < .FromCount Then _
                    xFrom = xFrom & vbCrLf
            Next i
        End If
        
        If Not oCIFormat.CCNamesOnly Then
'           return string with all cc address detail
            For i = 1 To .CcCount
                With .CcItem(i)
                    xCC = xCC & .Detail
                    If .Phone <> "" Then
                        xCC = xCC & vbCrLf
                        If oCIFormat.PhoneLabel <> "" Then _
                            xCC = xCC & oCIFormat.PhoneLabel & " "
                        xCC = xCC & .Phone
                    End If
                    If .Fax <> "" Then
                        xCC = xCC & vbCrLf
                        If oCIFormat.FaxLabel <> "" Then _
                            xCC = xCC & oCIFormat.FaxLabel & " "
                        xCC = xCC & .Fax
                    End If
                    If .Email <> "" Then
                        xCC = xCC & vbCrLf
                        If oCIFormat.EmailLabel <> "" Then _
                            xCC = xCC & oCIFormat.EmailLabel & " "
                        xCC = xCC & .Email
                    End If
                End With
                If i < .CcCount Then _
                    xCC = xCC & vbCrLf & vbCrLf
            Next i
        Else
'           return only the display name
            For i = 1 To .CcCount
                xCC = xCC & .CcItem(i).DisplayName(False)
                If i < .CcCount Then _
                    xCC = xCC & vbCrLf
            Next i
        End If

        If Not oCIFormat.BCCNamesOnly Then
'           return string with all bcc address detail
            For i = 1 To .BccCount
                With .BccItem(i)
                    xBCC = xBCC & .Detail
                    If .Phone <> "" Then
                        xBCC = xBCC & vbCrLf
                        If oCIFormat.PhoneLabel <> "" Then _
                            xBCC = xBCC & oCIFormat.PhoneLabel & " "
                        xBCC = xBCC & .Phone
                    End If
                    If .Fax <> "" Then
                        xBCC = xBCC & vbCrLf
                        If oCIFormat.FaxLabel <> "" Then _
                            xBCC = xBCC & oCIFormat.FaxLabel & " "
                        xBCC = xBCC & .Fax
                    End If
                    If .Email <> "" Then
                        xBCC = xBCC & vbCrLf
                        If oCIFormat.EmailLabel <> "" Then _
                            xBCC = xBCC & oCIFormat.EmailLabel & " "
                        xBCC = xBCC & .Email
                    End If
                End With
                If i < .BccCount Then _
                    xBCC = xBCC & vbCrLf & vbCrLf
            Next i
        Else
'           return only the display name
            For i = 1 To .BccCount
                xBCC = xBCC & .BccItem(i).DisplayName(False)
                If i < .BccCount Then _
                    xBCC = xBCC & vbCrLf
            Next i
        End If
    End With

'   filter client/matter based on contacts retrieved'*c
    Dim xSQL As String '*c
    If m_bCM_RunConnected And m_bCM_AllowFilter Then '*c
        If colContacts.Count Then '*c
            '---9.6.1
            Dim l_CIVersion As Long
            l_CIVersion = colContacts.CIVersion
            
            If Me.cmbCM.AdditionalClientFilter <> "" Then
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter(colContacts.Contacts)
                Else
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter2X(colContacts.Contacts)
                End If
            Else
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = xGetClientMatterFilter(colContacts.Contacts)
                Else
                    xSQL = xGetClientMatterFilter2X(colContacts.Contacts)
                End If
            End If
            
            If xSQL <> "" Then
                Me.cmbCM.AdditionalClientFilter = xSQL
                Me.cmbCM.AdditionalMatterFilter = xSQL
                Me.cmbCM.Refresh
            End If
            '---end 9.6.1
        End If '*c
    End If '*c
    
    Set colContacts = Nothing
    
    xTo = xTrimTrailingChrs(xTo, vbCrLf)
    xFrom = xTrimTrailingChrs(xFrom, vbCrLf)
    xCC = xTrimTrailingChrs(xCC, vbCrLf)
    xBCC = xTrimTrailingChrs(xBCC, vbCrLf)
    
    If xTo & xFrom & xCC & xBCC = "" Then
        Set colContacts = Nothing
        bCIActive = False       '9.7.2 #4609
        Exit Sub
    End If
    
    Dim xTemp As String
    Dim oMPCtl As mpDB.CCustomControl
    Dim xSep As String
    For Each oCtl In Me.Controls
        With oCtl
            ' Only controls with tag are linked to properties
            If Len(.Tag) Then
                ' This should only be defined for Textbox type controls,
                ' but check anyway
                Set oMPCtl = m_oCustCtls.Item(.Tag)
                If oMPCtl.ControlType = mpCustomControl_TextBox Or _
                    oMPCtl.ControlType = mpCustomControl_MultilineTextBox Then
                    xTemp = ""
                    Select Case oMPCtl.CIList
                        Case ciSelectionList_To
                            xTemp = xTo
'                           get different separator based on
'                           whether ci returns names only
                            If oCIFormat.ToNamesOnly Then
                                xSep = vbCrLf
                            Else
                                xSep = vbCrLf & vbCrLf
                            End If
                        Case ciSelectionList_From
'                           get different separator based on
'                           whether ci returns names only
                            If oCIFormat.FromNamesOnly Then
                                xSep = vbCrLf
                            Else
                                xSep = vbCrLf & vbCrLf
                            End If
                            xTemp = xFrom
                        Case ciSelectionList_CC
                            xTemp = xCC
'                           get different separator based on
'                           whether ci returns names only
                            If oCIFormat.CCNamesOnly Then
                                xSep = vbCrLf
                            Else
                                xSep = vbCrLf & vbCrLf
                            End If
                        Case ciSelectionList_BCC
                            xTemp = xBCC
'                           get different separator based on
'                           whether ci returns names only
                            If oCIFormat.BCCNamesOnly Then
                                xSep = vbCrLf
                            Else
                                xSep = vbCrLf & vbCrLf
                            End If
                    End Select
                    If Len(xTemp) Then
                        If Len(.Text) Then
                            .Text = .Text & xSep & xTemp
                        Else
                            .Text = xTemp
                        End If
                        m_oDocObj.CustomProperties(.Tag).Text = .Text
                    End If
                End If
            End If
        End With
    Next oCtl
    
    Set colContacts = Nothing
    Application.ScreenRefresh
    Me.SetFocus
    bCIActive = False
    Exit Sub
    
ProcError:
    bCIActive = False
    Set colContacts = Nothing
    RaiseError "MPO.frmCustomDocObj.GetContacts"
    Exit Sub
End Sub

Private Function SetUpTextBox(oCustCtl As mpDB.CCustomControl) As VB.TextBox
'sets up the next available textbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.TextBox
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumTextBoxes(.TabID) = mpMaxTextBoxes Then
            xMsg = "Too many text boxes defined for tab " & .TabID + 1 & ".  " & _
                   "Each tab is limited to " & mpMaxTextBoxes & " textboxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = (.TabID * mpMaxTextBoxes) + m_iNumTextBoxes(.TabID)
        
'       set up the text box in the array with specified index
        Set oCtl = Me.txtCustom(iCtlIndex)
        oCtl.Tag = .Name
        oCtl.Visible = True
        
'       set up corresponding label
        Me.lblCustomText(iCtlIndex).Caption = .Caption & ":"
        Me.lblCustomText(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomText(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumTextBoxes(.TabID) = m_iNumTextBoxes(.TabID) + 1 '*c
    End With

'   return control
    Set SetUpTextBox = oCtl
    Exit Function
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetUpTextBox"
    Exit Function
End Function

Private Function SetUpMultilineCombo(oCustCtl As mpDB.CCustomControl) As mpControls.MultiLineCombo
'sets up the next available multiline combo on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As mpControls.MultiLineCombo
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumMultComboBoxes(.TabID) = mpMaxMultComboBoxes Then
            xMsg = "Too many multiline combo boxes defined for tab " & _
                   .TabID + 1 & ".  " & "Each tab is limited to " & _
                   mpMaxMultComboBoxes & " combo boxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If

'       get next available text box for the specified tab
        iCtlIndex = (.TabID * mpMaxMultComboBoxes) + _
                     m_iNumMultComboBoxes(.TabID)
        
'       set up the text box in the array with specified index
        Set oCtl = Me.mlcCustom(iCtlIndex)
        With oCtl
            .Tag = oCustCtl.Name
            .Visible = True
            .Height = oCustCtl.Lines * 180
        
            If Not (oCustCtl.List Is Nothing) Then
                .List = oCustCtl.List.ListItems.Source
            End If
        End With
        
'       set up corresponding label
        Me.lblCustomMLC(iCtlIndex).Caption = oCustCtl.Caption & ":"
        Me.lblCustomMLC(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomMLC(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumMultComboBoxes(.TabID) = m_iNumMultComboBoxes(.TabID) + 1
    End With

'   return control
    Set SetUpMultilineCombo = oCtl
    Exit Function
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetUpMultilineCombo"
    Exit Function
End Function

Private Function SetUpMultilineTextBox(oCustCtl As mpDB.CCustomControl) As VB.TextBox
'sets up the next available textbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.TextBox
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumMultTextBoxes(.TabID) = mpMaxMultTextBoxes Then
            xMsg = "Too many multiline text boxes defined for tab " & .TabID + 1 & ".  " & _
                   "Each tab is limited to " & mpMaxMultTextBoxes & " textboxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = (.TabID * mpMaxMultTextBoxes) + _
                    m_iNumMultTextBoxes(.TabID)
        
'       set up the text box in the array with specified index
        Set oCtl = Me.mltCustom(iCtlIndex)
        oCtl.Tag = .Name
        oCtl.Visible = True
        oCtl.Height = oCustCtl.Lines * 180
        
'       set up corresponding label
        Me.lblCustomMLT(iCtlIndex).Caption = .Caption & ":"
        Me.lblCustomMLT(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomMLT(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumMultTextBoxes(.TabID) = m_iNumMultTextBoxes(.TabID) + 1
    End With

'   return control
    Set SetUpMultilineTextBox = oCtl
    Exit Function
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetUpMultilineTextBox"
    Exit Function
End Function

Private Function SetUpCheckBox(oCustCtl As mpDB.CCustomControl) As VB.CheckBox
'sets up the next available checkbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumChkBx(.TabID) = mpMaxChkBx Then
            xMsg = "Too many check boxes defined for tab " & .TabID + 1 & ".  " & _
                   "Each tab is limited to " & mpMaxChkBx & " check boxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = (.TabID * mpMaxChkBx) + m_iNumChkBx(.TabID)
        
'       set up the text box in the array with specified index
        Me.chkCustom(iCtlIndex).Tag = .Name
        Me.chkCustom(iCtlIndex).Visible = True
        Me.chkCustom(iCtlIndex).Caption = .Caption
        Me.lblCustomCheck(iCtlIndex).Caption = .Caption & ":"
        Me.lblCustomCheck(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, _
                        Me.chkCustom(iCtlIndex), _
                        Me.lblCustomCheck(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumChkBx(.TabID) = m_iNumChkBx(.TabID) + 1
    End With

'   return control
    Set SetUpCheckBox = Me.chkCustom(iCtlIndex)
    Exit Function
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetUpCheckBox"
    Exit Function
End Function

Private Function SetUpComboList(oCustCtl As mpDB.CCustomControl) As TDBCombo
'sets up the next available textbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCombo As TrueDBList60.TDBCombo
    Dim oLabel As VB.Label
    Dim oSource As XArray
    Dim i As Integer
    Dim xOrdSuffix As String
    Dim xVal As String
    Dim bIsList As Boolean
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumComboBoxes(.TabID) = mpMaxComboBoxes Then
            xMsg = "Too many combo/list boxes defined for tab " & .TabID + 1 & ".  " & _
                   "Each tab is limited to " & mpMaxComboBoxes & " combo/list boxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = (.TabID * mpMaxComboBoxes) + m_iNumComboBoxes(.TabID)
        
'       set up the combo box with specified index in the array
        Set oCombo = Me.cmbCustom(iCtlIndex)
        oCombo.Tag = .Name
        
'       set to list box if limited to list entries
        bIsList = (.ControlType = mpCustomControl_ListBox)
        oCombo.LimitToList = bIsList
        
        oCombo.Visible = True
        
        If Not (.List Is Nothing) Then
'***************************************
'per log item 1485
            Set oSource = mpBase2.xARClone(.List.ListItems.Source)
'***************************************
'           do manipulation of ordinal dates if control
'           is specified to be an ordinal date dropdown
            If .ControlType = mpCustomControl_OrdinalDateCombo Then
'               get ordinal suffix based on current date - eg "rd", "th"
                xOrdSuffix = GetOrdinalSuffix(Day(Date))
        
'               replace all instances of ordinal variable
'               with ordinal suffix-this is done here because
'               there is no way in vb to format a date as an ordinal
                For i = oSource.LowerBound(1) To oSource.UpperBound(1)
                    xVal = xSubstitute(oSource(i, 0), "%o%", xOrdSuffix)
                    xVal = xSubstitute(xVal, "%or%", xOrdSuffix)        '9.7.1020 #4584
                    oSource.Value(i, 0) = xVal
                    oSource.Value(i, 1) = xVal
                Next i
            End If
            oCombo.Array = oSource
            ResizeTDBCombo oCombo, 5
            oCombo.Rebind
'            oCombo.Bookmark = 0
'            cmbCustom_LostFocus iCtlIndex
        End If
        
'       set up label
        Set oLabel = Me.lblCustomCombo(iCtlIndex)
        oLabel.Caption = .Caption & ":"
        oLabel.Visible = True
        
'       position controls
        PositionControl oCustCtl, _
                        oCombo, _
                        oLabel
        
'       increment the number of combos used on the specified tab
        m_iNumComboBoxes(.TabID) = m_iNumComboBoxes(.TabID) + 1
    End With

'   return control
    Set SetUpComboList = oCombo
    Exit Function
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetUpComboList"
    Exit Function
End Function

Private Function SetUpCalendar(oCustCtl As mpDB.CCustomControl) As MSComCtl2.DTPicker
'sets up the next available multiline combo on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As MSComCtl2.DTPicker
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumCalendars(.TabID) = mpMaxCalendars Then '*c
            xMsg = "Too many calendard have been defined for tab " & .TabID + 1 & ".  " & _
                   "Each tab is limited to " & mpMaxCalendars & " calendar controls."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
               
'       get next available calendar for the specified tab
        iCtlIndex = (.TabID * mpMaxCalendars) + m_iNumCalendars(.TabID)

'       set up the text box in the array with specified index
        Set oCtl = Me.calCustom(iCtlIndex)
        With oCtl
            .Tag = oCustCtl.Name
            .Visible = True
            .CustomFormat = oCustCtl.Format
            .Format = dtpCustom
            .Value = Date
        End With
        
        calCustom_LostFocus (iCtlIndex)
        
'       set up corresponding label
        With Me.lblCustomCal(iCtlIndex)
            .Caption = oCustCtl.Caption & ":"
            .Visible = True
        End With
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomCal(iCtlIndex)
    
'4046 jsw '*c
'       increment the number of calendar controls used on the specified tab
        m_iNumCalendars(.TabID) = m_iNumCalendars(.TabID) + 1 '*c
    
    End With

'   return control
    Set SetUpCalendar = oCtl
    Exit Function
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetUpCalendar"
    Exit Function
End Function

Private Function SetUpClientMatterControl(oCustCtl As mpDB.CCustomControl) As MPCM.ClientMatterComboBox '*c
'sets up the client matter control
'to have the properties of the MacPac custom control definition
    On Error GoTo ProcError
    
    With oCustCtl
'       alert & exit if more than one cm control been defined
        If .TabID <> mpCustomTab_1 Then
            xMsg = "The client matter control is only available on Tab 1."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        ElseIf m_iNumCMControls(.TabID) = mpMaxCMControls Then
            xMsg = "Too many client matter controls have been defined.  " & _
                   "This control is limited to one per dialog."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       set up the control in the array with specified index
        Me.cmbCM.Tag = .Name
        Me.cmbCM.Visible = True
        
'       set up corresponding label
        Me.lblCM(0).Caption = .Caption & ":"
        Me.lblCM(0).Visible = True
        
'       position controls
        PositionControl oCustCtl, Me.cmbCM, Me.lblCM(0)
        
'       increment the number of textboxes used on the specified tab
        m_iNumCMControls(.TabID) = m_iNumCMControls(.TabID) + 1
    End With
    
'   configure
    m_xCM_Backend = mpBase2.GetMacPacIni("ClientMatterControl", "Backend")
    m_bCM_RunConnected = mpBase2.GetMacPacIni("ClientMatterControl", "RunConnected")
    m_bCM_ShowLoadMsg = mpBase2.GetMacPacIni("ClientMatterControl", "ShowLoadMessage")
    m_bCM_AllowFilter = mpBase2.GetMacPacIni("ClientMatterControl", "AllowFilter")
    m_bCM_AllowUserToClearFilters = mpBase2.GetMacPacIni("ClientMatterControl", "AllowUserToClearFilters")
    m_bCM_AllowValidation = mpBase2.GetMacPacIni("ClientMatterControl", "AllowValidation")
    m_xCM_Column1Heading = mpBase2.GetMacPacIni("ClientMatterControl", "Column1Heading")
    m_xCM_Column2Heading = mpBase2.GetMacPacIni("ClientMatterControl", "Column2Heading")
    m_xCM_Separator = mpBase2.GetMacPacIni("ClientMatterControl", "Separator")
    m_xCM_RunConnectedTooltip = mpBase2.GetMacPacIni("ClientMatterControl", "RunConnectedTooltip")
    m_xCM_RunDisconnectedTooltip = mpBase2.GetMacPacIni("ClientMatterControl", "RunDisconnectedTooltip")
    m_bCM_SearchByMatter = bIniToBoolean(mpBase2.GetMacPacIni("ClientMatterControl", "SearchByMatter")) '***9.7.1 - #4186
    
    With Me.cmbCM
        .RunConnectedToolTip = m_xCM_RunConnectedTooltip
        .RunDisconnectedToolTip = m_xCM_RunDisconnectedTooltip
        .RunConnected = m_bCM_RunConnected
        If Not .RunConnected Then
            .ComboType = ComboType_TextOnly
        Else
            .SourceClass = m_xCM_Backend
            If m_bCM_SearchByMatter Then .SearchMode = 1 '***9.7.1 - #4186
            .AllowUserToClearFilters = m_bCM_AllowUserToClearFilters
            .ShowLoadMessage = m_bCM_ShowLoadMsg
            .Column1Heading = m_xCM_Column1Heading
            .Column2Heading = m_xCM_Column2Heading
            .Separator = m_xCM_Separator
        End If
    End With

'   return control
    Set SetUpClientMatterControl = Me.cmbCM
    Exit Function
ProcError:
    RaiseError "MPO.frmCustomDocObj.SetUpClientMatterControl"
    Exit Function
End Function

Private Sub UpdateDefaults()
    Dim oCtl As Control
    Dim vTest
    Dim oProp As MPO.CCustomProperty
    Dim xCat As String
    
    ' Load sticky values stored in User.ini for this template
    ' Update Custom Properties if control values change
    On Error Resume Next
    For Each oCtl In Me.Controls
        With oCtl
            If Len(.Tag) Then
                Set oProp = m_oDocObj.CustomProperties(.Tag)
                xCat = m_oCustCtls.Category
                
                If m_oCustCtls.Item(.Tag).IsSticky Then
                    Select Case m_oCustCtls.Item(.Tag).ControlType
                        Case mpCustomControl_ComboBox, _
                            mpCustomControl_OrdinalDateCombo
                            vTest = GetUserIni(xCat, .Tag)
                            .BoundText = vTest
                            .Text = vTest
                            oProp.Text = .Text
                        Case mpCustomControl_ListBox
                            vTest = GetUserIni(xCat, .Tag)
                            .BoundText = vTest
                            .Text = vTest
                            oProp.Text = .Text
                        Case mpCustomControl_CheckBox, _
                            mpCustomControl_Calendar
                            vTest = GetUserIni(xCat, .Tag)
                            .Value = vTest
                            oProp.Text = .Value
                        Case Else
'                           textbox, multiline textbox, etc.
                            vTest = xSubstitute(GetUserIni(xCat, .Tag), "||", vbCrLf)
                            .Text = vTest
                            oProp.Text = .Text
                    End Select
                Else
'                   control is not sticky - set props of
'                   combos & lists to bound text of control
                    Select Case m_oCustCtls.Item(.Tag).ControlType
                        Case mpCustomControl_ComboBox, _
                             mpCustomControl_OrdinalDateCombo, _
                             mpCustomControl_ListBox
                             
                            oCtl.Bookmark = 0
                            
                           'DoEvents is necessary for the oProp.text
                           'below to write to the document
                           DoEvents
                           
'                           set the value equal to the bound text if possible
                            If oCtl.LimitToList Then
                                oProp.Text = .BoundText
                            Else
                                oProp.Text = .Text
                            End If
                       Case mpCustomControl_Calendar
                           oProp.Text = Format(.Value, .CustomFormat)
'                           oProp.Text = .Value
                    End Select
                End If
            End If
        End With
    Next oCtl
    
'CharlieCore 9.2.0 - CustomLetterhead
'   deal with letterhead tab
    If Me.DocObject.Template.CustomDialog.ShowLetterheadTab Then
        Dim lLHType As Long
        lLHType = GetUserIni(m_oCustCtls.Category, "LetterheadType")
        With Me.cmbLetterheadType
            .BoundText = lLHType
            If .BoundText = Empty Then
'               invalid Letterhead type - select first item in list
                .SelectedItem = 0
            End If
        End With
        Me.chkIncludeLetterheadEMail = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadEmail")
        Me.chkIncludeLetterheadName = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadName")
        Me.chkIncludeLetterheadPhone = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadPhone")
        Me.chkIncludeLetterheadTitle = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadTitle")
        Me.chkIncludeLetterheadFax = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadFax")
        Me.chkIncludeLetterheadAdmittedIn = GetUserIni(m_oCustCtls.Category, "IncludeLetterheadAdmittedIn")
        cmbLetterheadType_LostFocus
        chkIncludeLetterheadEMail_Click
        chkIncludeLetterheadName_Click
        chkIncludeLetterheadPhone_Click
        chkIncludeLetterheadTitle_Click
        chkIncludeLetterheadFax_Click
        chkIncludeLetterheadAdmittedIn_Click
    End If
    
End Sub

Private Sub SaveStickyControls()
    Dim oCtl As Control
    
    On Error Resume Next
    For Each oCtl In Me.Controls
        With oCtl
       ' Only controls with .Tag are linked to Custom Properties
            If Len(.Tag) Then
                If m_oCustCtls.Item(.Tag).IsSticky Then
                    Select Case m_oCustCtls.Item(.Tag).ControlType
                        Case mpCustomControl_TextBox, _
                            mpCustomControl_MultiLineCombo, _
                            mpCustomControl_MultilineTextBox
                            SetUserIni m_oCustCtls.Category, .Tag, xSubstitute(.Text, vbCrLf, "||")
                        Case mpCustomControl_ListBox, _
                             mpCustomControl_OrdinalDateCombo
                            SetUserIni m_oCustCtls.Category, .Tag, .BoundText
                        Case mpCustomControl_ComboBox
                            SetUserIni m_oCustCtls.Category, .Tag, .Text
                        Case mpCustomControl_CheckBox, _
                            mpCustomControl_Calendar
                            SetUserIni m_oCustCtls.Category, .Tag, .Value
                    End Select
                End If
            End If
        End With
    Next oCtl
    
'CharlieCore 9.2.0 - CustomLetterhead
'---get Letterhead properties, load controls on reuse
    If Me.DocObject.Template.CustomDialog.ShowLetterheadTab Then
        SetUserIni m_oCustCtls.Category, "LetterheadType", Me.cmbLetterheadType.BoundText
        SetUserIni m_oCustCtls.Category, "IncludeLetterheadEmail", Me.chkIncludeLetterheadEMail.Value
        SetUserIni m_oCustCtls.Category, "IncludeLetterheadName", Me.chkIncludeLetterheadName.Value
        SetUserIni m_oCustCtls.Category, "IncludeLetterheadPhone", Me.chkIncludeLetterheadPhone.Value
        SetUserIni m_oCustCtls.Category, "IncludeLetterheadTitle", Me.chkIncludeLetterheadTitle.Value
        SetUserIni m_oCustCtls.Category, "IncludeLetterheadFax", Me.chkIncludeLetterheadFax.Value
        SetUserIni m_oCustCtls.Category, "IncludeLetterheadAdmittedIn", Me.chkIncludeLetterheadAdmittedIn.Value
    End If

End Sub

Private Sub PositionControl(oDef As mpDB.CCustomControl, _
                            oCtl As Control, _
                            oLabel As VB.Label)
'   position control and corresponding
'   label based on position of previous
'   control and space before for current control
    Dim sPrevTop As Single
    Dim sPrevHeight As Single
    Dim sPrevLeft As Single
    Dim iPrevTabIndex As Integer
    Dim oPrevCtl As Control
    Dim bUseAuthorCtl As Boolean
    
    On Error GoTo ProcError
    bUseAuthorCtl = Me.DocObject.Template.CustomDialog.ShowAuthorControl
    
    Set oPrevCtl = m_oPrevCtl(oDef.TabID)
    
'   test for previous control
    If (oPrevCtl Is Nothing) Then
'       no previous control - base position on author
'       control if the dialog is using the author control
        If bUseAuthorCtl And (oDef.TabID = mpCustomTab_1) Then
            With Me.cmbAuthor
                sPrevTop = .Top
                sPrevHeight = .Height
                sPrevLeft = .Left
                iPrevTabIndex = .TabIndex
            End With
            
            If bUseAuthorCtl Then
'               set control as first control on tab -
'               this is used to determine focus when
'               the current tab changes
                Set m_oFirstCtlOnTab(oDef.TabID) = Me.cmbAuthor
            End If
        Else
'           author control is not being used -
'           place as first control in dlg
            sPrevTop = oDef.SpaceBefore
            sPrevHeight = 0
            sPrevLeft = Me.cmbAuthor.Left
            iPrevTabIndex = -1
            
'           set control as first control on tab -
'           this is used to determine focus when
'           the current tab changes
            Set m_oFirstCtlOnTab(oDef.TabID) = oCtl
        End If
    Else
        With oPrevCtl
'           position control based on previous control
            sPrevTop = .Top
            If .Name = "cmbCM" Then '*c
'               there's a bug in this control's height property'*c
                sPrevHeight = 315 '*c
            ElseIf TypeOf oPrevCtl Is mpControls.MultiLineCombo Then
                    sPrevHeight = .Height - 415 '*c #4348
            Else '*c
                sPrevHeight = .Height
            End If '*c
            sPrevLeft = .Left
            iPrevTabIndex = .TabIndex
        End With
    End If

'   execute
    With oCtl
        On Error Resume Next '* 4348
        .Left = sPrevLeft
        .Top = sPrevTop + sPrevHeight + oDef.SpaceBefore
        oLabel.Left = 105
        oLabel.Top = .Top + 40

'       set tab index
        If TypeOf oLabel Is VB.CheckBox Then
'           move tab index of checkbox label to end of list
            oLabel.TabIndex = 1000
        Else
            oLabel.TabIndex = iPrevTabIndex + 1
        End If
        .TabIndex = iPrevTabIndex + 2
        On Error GoTo ProcError
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.PositionControl"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'mpBase2.SetLastPosition Me
    
    Set m_PersonsForm = Nothing
    Set m_oOptions = Nothing
    Set m_oCustCtls = Nothing
    Set m_oMenu = Nothing
    Set g_oPrevControl = Nothing
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.Form_Unload"
    Exit Sub
End Sub

Private Sub txtCustom_GotFocus(Index As Integer)
    OnControlGotFocus Me.txtCustom(Index), _
        Me.DocObject.CustomProperties(Me.txtCustom(Index).Tag).Bookmark
End Sub

Private Sub txtCustom_LostFocus(Index As Integer)
    Dim oProp As MPO.CCustomProperty
    
    On Error GoTo ProcError
    With Me.txtCustom(Index)
        Set oProp = Me.DocObject.CustomProperties(.Tag)
        oProp.Text = .Text
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.txtCustom_LostFocus"
    Exit Sub
End Sub

Private Sub mltCustom_GotFocus(Index As Integer)
    OnControlGotFocus Me.mltCustom(Index), _
        Me.DocObject.CustomProperties(Me.mltCustom(Index).Tag).Bookmark
End Sub

Private Sub mltCustom_LostFocus(Index As Integer)
    Dim oProp As MPO.CCustomProperty
    Dim bChanged As Boolean
    
    On Error GoTo ProcError
    With Me.mltCustom(Index)
        Set oProp = Me.DocObject.CustomProperties(.Tag)
        bChanged = (oProp.Text <> .Text)
        oProp.Text = .Text
        oProp.Value = .Text
        If bChanged Then
            oProp.RunMacro
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.mltCustom_LostFocus"
    Exit Sub
End Sub

Private Sub cmbCM_ConnectionFailed(ByVal Message As String)

    MsgBox IIf(Message = "", "Could not connect to the client matter data source.  ", Message & vbCr & vbCr) _
            & "Instead of selecting client matter numbers from a list," & _
            " you can manually type the numbers instead", _
            vbExclamation, App.Title
        
    With Me.cmbCM
        .RunConnected = False
        .ComboType = ComboType_TextOnly
    End With
    
End Sub

Private Sub cmbCM_GotFocus()
    OnControlGotFocus Me.cmbCM, Me.DocObject.CustomProperties(Me.cmbCM.Tag).Bookmark
End Sub

Private Sub cmbCM_LostFocus()
    On Error GoTo ProcError
    Me.DocObject.CustomProperties(Me.cmbCM.Tag).Text = Me.cmbCM.Value
    '9.7.1 #4161
    If Me.DocObject.Template.CustomDialog.ShowLetterheadTab Then
        m_oLH.FileNumber = Me.cmbCM.Value
        cmbLetterheadType_LostFocus
    End If
    'save SQL for reuse
    Me.DocObject.Document.SaveItem "CMFilterString", _
        "CustomProperty", , Me.cmbCM.AdditionalClientFilter
        
'   get related values
    If m_bCM_RunConnected And (cmbCM.Value <> "") Then
        GetCMRelatedValues
    End If
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.cmbCM_LostFocus"
    Exit Sub
End Sub

Private Sub cmbCM_Validate(Cancel As Boolean)
    Dim iIsValid As cmValidationStatus
    
    If m_bCM_AllowValidation Then
    
        If Me.cmbCM.RunConnected And Me.cmbCM.Value <> Empty Then
            
            iIsValid = Me.cmbCM.Validity
            
            If iIsValid = cmValidationStatus_Invalid Then
                MsgBox "Invalid client matter number."
                Cancel = True
            End If
        End If
        
    End If
End Sub

Private Function xGetClientMatterFilter(oContacts As mpci.Contacts) As String '*c
    Dim i As Integer
    Dim xSQL As String
    
    For i = 1 To oContacts.Count
        xSQL = xSQL & "(INT_AUX_LST_CUSTOM.LISTING_ID=" & oContacts(i).ListingID & _
               " AND INT_AUX_LST_CUSTOM.LISTING_SRC_ID=" & Right(oContacts(i).StoreID, 1) + 1 & ") OR "
    Next i

    xSQL = "(" & Left(xSQL, Len(xSQL) - 3) & ")"

    xGetClientMatterFilter = xSQL
End Function

Public Function xGetClientMatterFilter2X(oContacts As CIO.CContacts) As String
    Dim i As Integer
    Dim xSQL As String
    
    
    Dim oUNID As CIO.CUNID
    Set oUNID = New CIO.CUNID
    
    For i = 1 To oContacts.Count
        With oContacts(i)
            '---9.6.1 - only IA4 and IA5 Win are supported for CM retrieval
            '---CE = 103
            '---IA5web = 101
            '---MAPI = 100
            '---MP9 = 104
            '---PL = 105
            '---GW = 106
            '---Notes = 110
            '---IA4/5win = 102
            
            If oUNID.GetListing(.UNID).BackendID = 102 Then
                xSQL = xSQL & "(INT_AUX_LST_CUSTOM.LISTING_ID=" & Right(oUNID.GetListing(.UNID).ID, (Len(oUNID.GetListing(.UNID).ID) - InStr(oUNID.GetListing(.UNID).ID, "."))) & _
                       " AND INT_AUX_LST_CUSTOM.LISTING_SRC_ID=" & Left(oUNID.GetListing(.UNID).ID, InStr(oUNID.GetListing(.UNID).ID, ".") - 1) & ") OR "
            End If
        End With
    Next i

    If xSQL <> "" Then
        xSQL = "(" & Left(xSQL, Len(xSQL) - 3) & ")"
    End If
    
    xGetClientMatterFilter2X = xSQL
End Function

Private Sub GetCMRelatedValues()
    Dim oCtl As Control
    Dim oMPCtl As mpDB.CCustomControl
    Dim vRelatedVals As Variant
    Dim xTemp As String
    
    On Error GoTo ProcError
    
    For Each oCtl In Me.Controls
        With oCtl
            ' Only controls with tag are linked to properties
            If Len(.Tag) Then
                ' This should only be defined for Textbox type controls,
                ' but check anyway
                Set oMPCtl = m_oCustCtls.Item(.Tag)
                If oMPCtl.ControlType = mpCustomControl_TextBox Or _
                        oMPCtl.ControlType = mpCustomControl_MultilineTextBox Then
                    xTemp = ""
                    Select Case oMPCtl.CMLink
                        Case mpClientMatterData_ClientNumber
                            xTemp = cmbCM.ClientData()(cmClientField_Number)
                        Case mpClientMatterData_ClientName
                            xTemp = cmbCM.ClientData()(cmClientField_Name)
                        Case mpClientMatterData_MatterNumber
                            xTemp = cmbCM.MatterData()(cmMatterField_Number)
                        Case mpClientMatterData_MatterName
                            xTemp = cmbCM.MatterData()(cmMatterField_Name)
                        Case mpClientMatterData_Other
                            vRelatedVals = _
                                cmbCM.MatterData()(cmMatterField_RelatedValues)
                            xTemp = vRelatedVals(oMPCtl.CMRelatedIndex)
                        Case Else
                    End Select
                    If Len(xTemp) Then
                        .Text = xTemp
                        m_oDocObj.CustomProperties(.Tag).Text = .Text
                    End If
                End If
            End If
        End With
    Next oCtl
    Exit Sub
ProcError:
    RaiseError "MPO.frmCustomDocObj.GetCMRelatedValues"
    Exit Sub
End Sub

'**********************************************************
'   IDocObjForm Interface
'**********************************************************
Private Property Get IDocObjForm_Cancelled() As Boolean
    IDocObjForm_Cancelled = Me.Cancelled
End Property

Private Property Set IDocObjForm_DocObject(RHS As IDocObj)
    Me.DocObject = RHS
End Property

Private Property Get IDocObjForm_DocObject() As IDocObj
    Set IDocObjForm_DocObject = Me.DocObject
End Property

