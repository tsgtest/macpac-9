VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEnvelope"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   Envelope Class
'   created 10/13/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define a MacPac Envelope

'   Container for CDocument, CEnvelopeDef, XArray
'**********************************************************
Option Explicit

Private Const mpDocSegment As String = "Envelope"

Public Enum mpEnvelopeOutputs
    mpEnvelopeOutput_Printer = 1
    mpEnvelopeOutput_StartofDocument = 2
    mpEnvelopeOutput_EndOfDocument = 3
    mpEnvelopeOutput_NewDocument = 4
End Enum

Public Enum mpEnvelopeBarCodePositions
    mpEnvelopeBarCodePositions_None = 0
    mpEnvelopeBarCodePositions_Above = 1
    mpEnvelopeBarCodePositions_below = 2
End Enum

Private m_oDoc As MPO.CDocument
Private m_oType As mpDB.CEnvelopeDef

Private m_iStartCol As Integer
Private m_iStartRow As Integer
Private m_iOfficeID As Integer
Private m_lType As Long
Private m_xClientMatter As String
Private m_xDPhrase As String
Private m_xDPhrase2 As String '9.7.1 #4024
Private m_xInitials As String
Private m_xFont As String

Private m_bBarCode As Boolean

Private m_iCount As Integer
Private m_xAddresses As String
Private m_xAuthor As String
Private m_xReturnAddress As String
Private m_xClimat As String
Private m_xBP As String
Private m_iID As Integer
Private m_sFontSize As Single
Private m_xSize As String
Private m_sTop As Single
Private m_sLeft As Single
Private m_sRetTop As Single
Private m_sRetLeft As Single
Private m_bReturnAddress As Boolean
Private m_oAuthor As mpDB.CPerson
Private m_iOutputTo As mpEnvelopeOutputs
Private m_bReturnAddressFirmName As Boolean
Private m_bReturnAddressName As Boolean
Private m_bClientMatter As Boolean
Private m_bAuthorInitials As Boolean
Private m_xAuthorTrailChr As String
Private m_xAuthorIntroChr As String
Private m_bUSPSStandards As Boolean

Private m_iBarCodePosition As mpEnvelopeBarCodePositions

Public Event BeforeEnvelopesPrint()
Public Event BoilerplateInserted()
Public Event After1stEnvelopeCreated()
'---9.7.1 - 4232
Public Event BeforeOutput(bSuppressPageSetup As Boolean)


Public Property Let OutputTo(iNew As Integer)
    m_iOutputTo = iNew
End Property

Public Property Get OutputTo() As Integer
    OutputTo = m_iOutputTo
End Property

Public Property Let BarCodePosition(iNew As mpEnvelopeBarCodePositions)
    m_iBarCodePosition = iNew
End Property

Public Property Get BarCodePosition() As mpEnvelopeBarCodePositions
    BarCodePosition = m_iBarCodePosition
End Property

Public Property Let IncludeReturnAddress(bNew As Boolean)
    m_bReturnAddress = bNew
End Property

Public Property Get IncludeReturnAddress() As Boolean
'can't include return address if no bookmark has been specified
    If Me.Definition.AllowReturnAddress Then
        IncludeReturnAddress = m_bReturnAddress
    Else
        IncludeReturnAddress = False
    End If
End Property

Public Property Let USPSStandards(bNew As Boolean)
    m_bUSPSStandards = bNew
End Property

Public Property Get USPSStandards() As Boolean
    USPSStandards = m_bUSPSStandards
End Property

Public Property Let IncludeReturnAddressFirmName(bNew As Boolean)
    m_bReturnAddressFirmName = bNew
End Property

Public Property Get IncludeReturnAddressFirmName() As Boolean
'can't include return address firm name if no bookmark has been specified
    If Me.Definition.AllowReturnAddress Then
        IncludeReturnAddressFirmName = m_bReturnAddressFirmName
    Else
        IncludeReturnAddressFirmName = False
    End If
End Property

Public Property Let IncludeReturnAddressName(bNew As Boolean)
    m_bReturnAddressName = bNew
End Property

Public Property Get IncludeReturnAddressName() As Boolean
'can't include return address name if no bookmark has been specified
    If Me.Definition.AllowReturnAddress Then
        IncludeReturnAddressName = m_bReturnAddressName
    Else
        IncludeReturnAddressName = False
    End If
End Property

Public Property Let IncludeAuthorInitials(bNew As Boolean)
    m_bAuthorInitials = bNew
End Property

Public Property Get IncludeAuthorInitials() As Boolean
'can't include return address name if no bookmark has been specified
    If Me.Definition.AllowBillingNumber Then
        IncludeAuthorInitials = m_bAuthorInitials
    Else
        IncludeAuthorInitials = False
    End If
End Property

Public Property Let IncludeClientMatterNo(bNew As Boolean)
    m_bClientMatter = bNew
End Property

Public Property Get IncludeClientMatterNo() As Boolean
'can't include return address name if no bookmark has been specified
    If Me.Definition.AllowBillingNumber Then
        IncludeClientMatterNo = m_bClientMatter
    Else
        IncludeClientMatterNo = False
    End If
End Property
Public Property Let IncludeBarCode(bNew As Boolean)
    m_bBarCode = bNew
End Property

Public Property Get IncludeBarCode() As Boolean
    IncludeBarCode = m_bBarCode
End Property

Public Property Let Office(iNew As Integer)
    m_iOfficeID = iNew
End Property

Public Property Get Office() As Integer
    Office = m_iOfficeID
End Property

Public Property Let AddressLeft(sNew As Single)
    m_sLeft = sNew
End Property

Public Property Get AddressLeft() As Single
    AddressLeft = m_sLeft
End Property

Public Property Let AddressTop(sNew As Single)
    m_sTop = sNew
End Property

Public Property Get AddressTop() As Single
    AddressTop = m_sTop
End Property

Public Property Let ReturnAddressLeft(sNew As Single)
    m_sRetLeft = sNew
End Property

Public Property Get ReturnAddressLeft() As Single
    ReturnAddressLeft = m_sRetLeft
End Property

Public Property Let ReturnAddressTop(sNew As Single)
    m_sRetTop = sNew
End Property

Public Property Get ReturnAddressTop() As Single
    ReturnAddressTop = m_sRetTop
End Property

Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Property Let AuthorIntroChr(xNew As String)
    m_xAuthorIntroChr = xNew
End Property

Public Property Get AuthorIntroChr() As String
    AuthorIntroChr = m_xAuthorIntroChr
End Property

Public Property Let AuthorTrailChr(xNew As String)
    m_xAuthorTrailChr = xNew
End Property

Public Property Get AuthorTrailChr() As String
    AuthorTrailChr = m_xAuthorTrailChr
End Property

Public Property Let Author(xNew As String)
    m_xAuthor = xNew
End Property

Public Property Get Author() As String
    Author = m_xAuthor
End Property

Public Property Let Addresses(xNew As String)
    If USPSStandards Then
        m_xAddresses = UCase(mpBase2.xStripPunctuation(xNew, g_xUSPSCharsToDelete))
    Else
        m_xAddresses = xNew
    End If
End Property

Public Property Get Addresses() As String
    Addresses = m_xAddresses
End Property

Public Property Let ReturnAddress(xNew As String)
    m_xReturnAddress = xNew
End Property

Public Property Get ReturnAddress() As String
'*********************************
'per item 1763 - df
    If m_xReturnAddress = Empty Then
        If Not m_oType Is Nothing Then
            m_xReturnAddress = g_oDBs.Offices(Me.Office).Address _
                (g_oDBs.OfficeAddressFormats(m_oType.ReturnAddressFormat))
        End If
    End If
'*********************************
    ReturnAddress = m_xReturnAddress
End Property

Public Property Get Count() As Integer
    If Me.Addresses <> Empty Then
        Count = mpBase2.lCountChrs(Me.Addresses, vbCrLf & vbCrLf) + 1
    End If
End Property

Public Property Get Definitions() As mpDB.CEnvelopeDefs
    Set Definitions = g_oDBs.EnvelopeDefs
End Property

Public Property Let ClientMatterNumber(xNew As String)
    m_xClimat = xNew
End Property

Public Property Get ClientMatterNumber() As String
    ClientMatterNumber = m_xClimat
End Property

Public Property Let FontName(xNew As String)
    m_xFont = xNew
End Property

Public Property Get FontName() As String
    FontName = m_xFont
End Property

Public Property Let FontSize(sNew As Single)
    m_sFontSize = sNew
End Property

Public Property Get FontSize() As Single
    FontSize = m_sFontSize
End Property

Public Property Let DeliveryPhrase(xNew As String)
    m_xDPhrase = xNew
End Property

Public Property Get DeliveryPhrase() As String
    DeliveryPhrase = m_xDPhrase
End Property
Public Property Let DeliveryPhrase2(xNew As String) '9.7.1 #4024
    m_xDPhrase2 = xNew
End Property

Public Property Get DeliveryPhrase2() As String '9.7.1 #4024
    DeliveryPhrase2 = m_xDPhrase2
End Property

Public Property Let EnvelopeType(lNew As Long)
    On Error Resume Next
    Set m_oType = g_oDBs.EnvelopeDefs.Item(lNew)
    On Error GoTo ProcError
    If Err.Number Then
        Err.Raise mpError_InvalidEnvelopeType
    End If
    m_lType = lNew
    Exit Property
    
ProcError:
    RaiseError "MPO.CEnvelope.EnvelopeType"
    Exit Property
End Property

Public Property Get EnvelopeType() As Long
    EnvelopeType = m_lType
End Property

'***********************************************
'   methods
'***********************************************
Function GetAddress(ByVal iIndex As Integer)
'returns the address with index iIndex
'in the addresses string
    Dim lPos1 As Long
    Dim lPos2 As Long
    Dim i As Integer
    
'   get starting position of requested address
    For i = 1 To iIndex - 1
        lPos1 = InStr(lPos1 + 1, Me.Addresses, _
            vbCrLf & vbCrLf, vbTextCompare)
    Next
    
    If lPos1 Then
        lPos1 = lPos1 + 3
    End If
    
'   get end position of requested address
    lPos2 = InStr(lPos1 + 1, Me.Addresses, _
        vbCrLf & vbCrLf, vbTextCompare)
    
    If lPos2 Then
        GetAddress = Mid(Me.Addresses, _
            lPos1 + 1, lPos2 - lPos1 - 1)
    Else
        GetAddress = Mid(Me.Addresses, lPos1 + 1)
    End If
End Function

Public Function Definition() As mpDB.CEnvelopeDef
    Set Definition = g_oDBs.EnvelopeDefs.Item(Me.EnvelopeType)
End Function

Public Sub Insert()
'inserts the envelope using the properties set for this object
    Dim i As Integer
    Dim iCount As Integer
    Dim xDoc As String
    Dim xTemp As String
    Dim oDef As CEnvelopeDef
    Dim xDesc As String
    Dim sFromLeft As Single
    Dim sFromTop As Single
    Dim oBkmk As Word.Bookmark
    Dim oRng As Word.Range
    Dim oSec As Word.Section
    Dim bDirty As Boolean
    Dim oAT As Word.AutoTextEntry
    Dim xTempFile As String
    Dim xCurFile As String
    Dim xBP As String
    Dim xAddress As String
    Dim bBlankEnvelope As Boolean
    Dim xCurFileWB As String
    Dim bShowFieldCodes As Boolean
    Dim xStyleName As String
    
    On Error GoTo ProcError
    
    EchoOff
    
    '9.7.1 - #4033
    bShowFieldCodes = Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes
    Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = False

'   get dirt of Normat.dot
    bDirty = Word.NormalTemplate.Saved

'   get envelope definition
    On Error Resume Next
    Set oDef = Me.Definitions.Item(Me.EnvelopeType)
    On Error GoTo ProcError

'   alert and exit if there is no envelope type
    If (oDef Is Nothing) Then
        If Me.EnvelopeType = 0 Then
            xDesc = "No envelope type has been selected."
        End If
        Err.Raise mpError_InvalidEnvelopeType
    End If

'   get current file
    xCurFile = Word.ActiveDocument.ActiveWindow.Caption
    
'---get current file name for cuse by Organizer
    xCurFileWB = WordBasic.FileName$()

'   create temp document
    xBP = mpBase2.BoilerplateDirectory & Me.Definition.BoilerplateFile
    If Dir(xBP) = "" Then
        xDesc = "Could not find the file '" & xBP & "'.  " & _
               "Please contact your administrator."
        Err.Raise mpError_InvalidBoilerplateFile
    End If
    
    m_oDoc.Documents.Add xBP
    'save activedocument only if inserting in current document
    If Me.OutputTo = mpLabelOutput_EndOfDocument Or _
        Me.OutputTo = mpLabelOutput_StartofDocument Then '**** 9.7.1 - #3919
        ActiveDocument.SaveAs mpBase2.UserFilesDirectory & "mptempenv.tmp", , , , False
    End If
    
'   broadcast bp insertion
    RaiseEvent BoilerplateInserted
    
    xTempFile = Word.ActiveDocument.FullName

    If m_oDoc.File.Sections.Count = 1 Then
        Set oRng = m_oDoc.File.Content
        oRng.EndOf
        oRng.InsertBreak wdSectionBreakNextPage
    End If
    
    With oDef
'       deal with blank envelope
        If Me.Count = 0 Then
            DoEnvelope1
            
'           get the appropriate address and write to envelope
            m_oDoc.EditItem "zzmpAddress", "", , False

'           get section of current address block
            Set oSec = Word.ActiveDocument _
                .Bookmarks("zzmpAddress").Range.Sections(1)

'           update and unlink any fields in address block
            With oSec.Range.Fields
                .Update
                .Unlink
            End With

'           clear bookmarks, footers, headers
            m_oDoc.ClearBookmarks

        Else
            Dim oStatus As Object
            Set oStatus = CreateObject("mpAXE.CStatus")
            oStatus.Title = "Creating MacPac Envelopes"
            
'           cycle through addresses, creating an envelope for each
            For i = 1 To Me.Count
                oStatus.Show , "Creating envelope " & i & " of " & Me.Count & ".  Please wait..."
            
'           use word processor to generate first envelope
                If i = 1 Then
                    DoEnvelope1
                    Set oSec = m_oDoc.File.Sections(1)
                    Set oRng = oSec.Range
'                   create autotext of envelope for later use
                    Set oAT = Word.NormalTemplate.AutoTextEntries _
                        .Add("tempEnv", oRng)
                Else
'                   create envelope by inserting newly created autotext entry
                    Set oSec = m_oDoc.File.Sections(i)
                    Set oRng = oSec.Range
                    oRng.Collapse wdCollapseStart
                    oAT.Insert oRng, True
                End If
'               get the appropriate address and write to envelope
                xAddress = GetAddress(i)
                
                m_oDoc.EditItem "zzmpAddress", xAddress
                
'               get section of current address block
                Set oSec = Word.ActiveDocument _
                    .Bookmarks("zzmpAddress").Range.Sections(1)

'               switch from ref field to literal text in bardcode field
'               to make sure address is clean
                If Not Me.USPSStandards And Me.IncludeBarCode Then
                    Me.Document.EditBarCodeFieldLiteralText oSec.Range, xAddress
                End If
                
'               update and unlink any fields in address block
                With oSec.Range.Fields
                    .Update
                    .Unlink
                End With
                
'               clear bookmarks, footers, headers
                m_oDoc.ClearBookmarks
            Next i
            
            Set oStatus = Nothing
            
        End If      'Me.count = 0
    End With

    Word.Documents(xTempFile).Activate
 
 '   delete invalid zip codes
    m_oDoc.DeleteInvalidBarCodes

'   delete autotext
    If Me.Count <> 0 Then
        oAT.Delete
        Set oAT = Nothing
    End If
    
'   restore normal.dot dirt
    Word.NormalTemplate.Saved = bDirty

'   update style
    'GLOG : 5089 : CEH
    With m_oDoc.File.Styles(wdStyleEnvelopeAddress)
        xStyleName = .NameLocal
        .Font.Name = Me.FontName
        .Font.Size = Me.FontSize
        With .Frame
            .HorizontalPosition = Me.AddressLeft * 72
            .VerticalPosition = Me.AddressTop * 72
            .Width = m_oDoc.File.Sections(1).PageSetup.PageWidth - .HorizontalPosition - 36
            .Height = m_oDoc.File.Sections(1).PageSetup.PageHeight - .VerticalPosition - 36
        End With
    End With

'   replace sections with page breaks
    ReplaceSections

    Dim bytOrient As WdOrientation
    bytOrient = m_oDoc.File.Sections.Last.PageSetup.Orientation

'   replace styles in final document
    If Me.OutputTo = mpLabelOutput_EndOfDocument Or _
        Me.OutputTo = mpLabelOutput_StartofDocument Then '**** 9.7.1 - #3919
        Application.OrganizerCopy Source:=xTempFile, _
                              Destination:=xCurFileWB, _
                              Name:=xStyleName, _
                              Object:=wdOrganizerObjectStyles
    End If
    
'   transfer envelopes from temp
'   file to final destination
    Output xCurFile, xTempFile
    
    Selection.HomeKey wdStory   'GLOG #5054 !gm
    
    EchoOn
    Word.Application.ScreenUpdating = True
    Word.Application.ScreenRefresh
    Word.Application.ScreenUpdating = False
    EchoOff
    
    '---9.7.1 - 4232
    Dim bSuppressPageSetup As Boolean
    RaiseEvent BeforeOutput(bSuppressPageSetup)
    If bSuppressPageSetup = False Then
'   setup Page
        If Me.OutputTo <> mpLabelOutput_Printer Then
            m_oDoc.SetSectionPaperSource Word.Selection.Sections.First, _
                                         True, _
                                         "Envelopes"
        End If
    End If

'CharlieCore 9.2.0 - NoTrailer
'       mark for notrailer
    If Me.OutputTo <> mpLabelOutput_Printer Then
        m_oDoc.MarkForNoTrailer Word.Selection.Sections.First, 8    '#3838
    End If

    Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = bShowFieldCodes
    
    EchoOn
    Word.Application.ScreenUpdating = True
    Word.Application.ScreenRefresh
    Word.Application.ScreenUpdating = False
    EchoOff
    
    
    Exit Sub

ProcError:
    EchoOn
'   delete autotext
    If Not oAT Is Nothing Then
        oAT.Delete
    End If

'   restore normal.dot dirt
    Word.NormalTemplate.Saved = bDirty

    RaiseError "MPO.CEnvelope.Insert", xDesc
End Sub

Public Sub DoEnvelope1()
    Dim oBkmk As Word.Bookmark
    Dim oBkmk_Below As Word.Bookmark
    Dim oBkmk_Above As Word.Bookmark
    Dim xDesc As String
    Dim oFrame As Word.Frame
    
'   reposition address frame to spec
    On Error Resume Next
    Set oBkmk = Word.ActiveDocument.Bookmarks("zzmpAddress")
    Set oFrame = oBkmk.Range.Frames(1)
    On Error GoTo ProcError
    
    If (oBkmk Is Nothing) Then
'       bookmark missing
        xDesc = "The file '" & Me.Definition.BoilerplateFile & "' is " & _
            "missing the zzmpAddressBlock bookmark.  You must bookmark " & _
            "the envelope recipient frame with this bookmark."
        Err.Raise mpError_BookmarkNotFound
    ElseIf oFrame Is Nothing Then
'       incorrect application of zzmpAddressBlock
        xDesc = "The envelope recipient frame in file '" & _
                Me.Definition.BoilerplateFile & "' must be bookmarked " & _
                "with zzmpAddressBlock."
        Err.Raise mpError_BookmarkNotFound
    Else
        With oBkmk.Range.Frames(1)
            .RelativeHorizontalPosition = wdRelativeHorizontalPositionPage
            .HorizontalPosition = Me.AddressLeft * 72
            .RelativeVerticalPosition = wdRelativeVerticalPositionPage
            .VerticalPosition = Me.AddressTop * 72
        End With
    End If
    
'---fill placeholders
    With m_oDoc
        Set oBkmk = Nothing
        
'       fill in billing info if specified
        If Len(Me.Author) Then
            .EditItem_ALT "zzmpAuthor", Me.AuthorIntroChr & _
                Me.Author & Me.AuthorTrailChr, , False
        Else
            .DeleteItem "zzmpAuthor", False
        End If
        .EditItem_ALT "zzmpClientMatterNo", Me.ClientMatterNumber, , False
      
        If Not Me.IncludeReturnAddress Then
'           delete return address block
            On Error Resume Next
            Set oBkmk = Word.ActiveDocument.Bookmarks("zzmpReturnAddressBlock")
            .DeleteItem "zzmpReturnAddressBlock"
            oBkmk.Range.Tables(1).Delete
            On Error GoTo ProcError
        Else
            If Me.IncludeReturnAddressFirmName Then
                .EditItem "zzmpFirmName", g_oDBs.Offices.Default.FirmName
            Else
                .DeleteItem "zzmpFirmName"
            End If
            .EditItem "zzmpReturnAddress", Me.ReturnAddress
                
        End If

        If Me.IncludeBarCode Then
'           check for barcode bookmark
            On Error Resume Next
            Set oBkmk = Nothing
            Set oBkmk = Word.ActiveDocument.Bookmarks("zzmpBarCode")
            Set oBkmk_Below = Word.ActiveDocument.Bookmarks("zzmpBarCodeBelow")
            Set oBkmk_Above = Word.ActiveDocument.Bookmarks("zzmpBarCodeAbove")
            On Error GoTo ProcError
'           insert field in bookmark if it exists
            If Not (oBkmk Is Nothing) Then
                InsertBarCode oBkmk.Range
                .EditItem "zzmpBarCodeAbove", "", , True, , , True
                .EditItem "zzmpBarCodeBelow", "", , True, , , True
            ElseIf Me.BarCodePosition = mpEnvelopeBarCodePositions_Above And _
            Not (oBkmk_Above Is Nothing) Then
                InsertBarCode oBkmk_Above.Range
                .EditItem "zzmpBarCodeBelow", "", , True, , , True
            ElseIf Me.BarCodePosition = mpEnvelopeBarCodePositions_below And _
            Not (oBkmk_Below Is Nothing) Then
                InsertBarCode oBkmk_Below.Range
                .EditItem "zzmpBarCodeAbove", "", , True, , , True
            End If
        Else
'           delete barcode paragraph
            .EditItem "zzmpBarCode", "", , True, , , True
            .EditItem "zzmpBarCodeAbove", "", , True, , , True
            .EditItem "zzmpBarCodeBelow", "", , True, , , True
        End If

'       insert dphrases if allowed in envelope
        If Me.Definition.AllowDPhrases Then
            .EditItem "zzmpDeliveryPhrase", _
                Me.DeliveryPhrase, , True, , , True
            .EditItem "zzmpDeliveryPhrase2", _
                Me.DeliveryPhrase2, , True, , , True '9.7.1 #4024
        Else
            .DeleteItem "zzmpDeliveryPhrase", True
            .DeleteItem "zzmpDeliveryPhrase2", True '9.7.1 #4024
        End If
    
'       broadcast that first envelope was created
        RaiseEvent After1stEnvelopeCreated
    
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CEnvelope.DoEnvelope1", xDesc
End Sub
Public Sub InsertBarCode(oRg As Word.Range)
'inserts a barcode at oRg that refs bookmark zzmpAddress
    Dim oCode As Word.Range
    Dim xLiteralText As String
    
    If Len(oRg) Then
        oRg.Delete
    End If
    
    Set oCode = oRg.Fields.Add(oRg, _
        wdFieldBarCode, """", False).Code
        
    With oCode
        .Collapse wdCollapseEnd
        .MoveUntil """"
        .Fields.Add oCode, wdFieldRef, """zzmpAddress"""
    End With
    
End Sub

Private Sub Output(ByVal xDoc As String, ByVal xTemp As String)
    Dim lLeftMargin As Long
    Dim lRightMargin As Long
    Dim lTopMargin As Long
    Dim lBottomMargin As Long
    Dim lHeaderDist As Long
    Dim lFooterDist As Long
    Dim bytOrient As WdOrientation
    Dim oSec As Word.Section
    Dim oRng As Word.Range
    Dim lPageHeight As Long
    Dim lPageWidth As Long
    Dim iSecCreated As Integer
    Dim oBofRng As Word.Range
    Dim cnt As Integer
    Dim oDialog As Word.Dialog
    
    On Error GoTo ProcError
   '---copy & output completed set
   
    
    With m_oDoc
        Select Case Me.OutputTo
            Case mpEnvelopeOutput_EndOfDocument
            
                Word.Documents(xTemp).Activate
                Set oRng = .File.Range
                
'               add section break to start of envelopes
                With oRng
                    .StartOf
                    .InsertBreak wdSectionBreakNextPage
                End With
                
                Set oRng = .File.Range
                
'               take everything up to trailing section break
                With oRng
                    .SetRange .Start, .End - 1
                    .Copy
                End With
                
                With .File
'                   close temp envelopes doc
                    .Saved = True
                    .Close
                    On Error Resume Next
                    Kill mpBase2.UserFilesDirectory & "mptempenv.tmp"
                    On Error GoTo ProcError
                End With
                
                With m_oDoc.File
'                   activate starting doc
                    .Windows(xDoc).Activate
                                        
'                   insert new section in starting doc
                    .Sections.Last.Range.Characters.Last. _
                            InsertBreak wdSectionBreakNextPage
                        
'                   clear & unlink headers/footers
                    m_oDoc.ClearFooters .Sections.Last, True
                    m_oDoc.ClearHeaders .Sections.Last, True
                    
'                   paste into new section
                    .Sections.Last.Range.Characters.Last.Paste
                End With
                
'               delete last section - it's blank
                Set oRng = .File.Sections.Last.Range
                With oRng
                    .MoveStart wdCharacter, -1
                    .Delete
                End With
                
'               delete blank section that precedes envelopes
                Set oRng = .File.Sections.Last.Range
                With oRng
                    .StartOf
                    .MoveStart wdCharacter, -1
                    .Delete
                End With
                
                m_oDoc.SelectEndOfSection mpDocPosition_EOF
            
                'clear clipboard      9.7.1 - #3605
                Clipboard.Clear
            Case mpEnvelopeOutput_StartofDocument
'               check for frame anchored to first paragraph of
'               original document and move it to 2nd paragraph
'               before going any further
                Set oBofRng = .File.Range(0, 0)
                With oBofRng
                    If .Frames.Count Then
                        .Frames(1).Cut
                        .Move wdParagraph, 1
                        .Paste
                    End If
                End With

''      activate temp document
'                .File.Windows(xTemp).Activate
                Word.Documents(xTemp).Activate
  
                Set oRng = .File.Range
  
'               copy everything but the last para mark
                With oRng
                    .SetRange .Start, .End - 1
                    .Copy
                End With
                With .File
                    .Saved = True
                    .Close
                    On Error Resume Next
                    Kill mpBase2.UserFilesDirectory & "mptempenv.tmp"
                    On Error GoTo ProcError

               End With
                .File.Windows(xDoc).Activate
'               Word.Documents(xDoc).Activate
'               put in new doc
                Set oBofRng = .File.Range(0, 0)
                With oBofRng
                    On Error Resume Next
                    cnt = .Cells.Count
                    On Error GoTo ProcError
                    If cnt > 0 Then
'                       can't insert in table
                        .InsertBreak wdPageBreak
                        .StartOf
                        .Paste
                        With m_oDoc.File
                            .Characters(1).Delete
                            .Sections(2).Range.Paragraphs(1).Range.Delete
                        End With
                    Else
                        .Paste
                    End If
                End With
                DoEvents
                DoEvents
                m_oDoc.UnlinkHeadersFooters m_oDoc.File.Sections(2)
                m_oDoc.SelectEndOfSection mpDocPosition_BOF
            
                'clear clipboard      9.7.1 - #3605
                Clipboard.Clear
            Case mpEnvelopeOutput_Printer
                Word.Documents(xTemp).Activate
                m_oDoc.SelectEndOfSection mpDocPosition_BOF
                
'               delete last section - it's blank
                Set oRng = .File.Sections.Last.Range
                With oRng
                    .MoveStart wdCharacter, -1
                    .Delete
                End With
                
                RaiseEvent BeforeEnvelopesPrint
                
'---set paper source in temp doc
                '---9.7.1
                Dim bSuppressPageSetup As Boolean
                RaiseEvent BeforeOutput(bSuppressPageSetup)
                If bSuppressPageSetup = False Then
                    m_oDoc.SetSectionPaperSource , True, "Envelopes"
                End If

'               prompt to print
                .Dialogs(wdDialogFilePrint).Show
                DoEvents
                .File.Saved = True
                .File.Close
            
            Case mpEnvelopeOutput_NewDocument
                Word.Documents(xTemp).Activate
                Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = False
                m_oDoc.SelectEndOfSection mpDocPosition_BOF
'               delete last section - it's blank
                Set oRng = .File.Sections.Last.Range
                With oRng
                    .MoveStart wdCharacter, -1
                    .Delete
                End With
            Case Else
    
        End Select
        
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.CEnvelope.Output"
    Exit Sub
End Sub

Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Me.AuthorIntroChr = mpBase2.GetMacPacIni("Envelopes", "AuthorIntroChr")
    Me.AuthorTrailChr = mpBase2.GetMacPacIni("Envelopes", "AuthorTrailChr")
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
    Set m_oAuthor = Nothing
End Sub

Private Sub ReplaceSections()

    Dim r As Word.Range
    Dim i As Integer
    
    Set r = ActiveDocument.Range
    
    If r.Sections.Count > 2 Then
        For i = 1 To r.Sections.Count - 2
            If i = 1 Then
                With r.Find
                    .ClearFormatting
                    .Text = "^b"
                    .Execute
                    If .Found Then
                        With r
                            .InsertBefore vbCr & vbCr
                            .StartOf
                            .InsertBreak wdPageBreak
                            .Move wdParagraph, -1
                            .InsertBefore vbCr
                            .MoveEnd , 2
                            .EndOf
                            If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Or g_bIsWord16x Then
                                .MoveEnd wdCharacter, 3
                            Else
                                .MoveEnd wdCharacter, 2
                            End If
                            .Delete
                        End With
                    End If
                End With
            Else
                With r.Find
                    .ClearFormatting
                    .Text = "^b"
                    .Execute
                    If .Found Then
                        With r
                            .StartOf
                            .InsertBreak wdPageBreak
                            .EndOf
                            .InsertBefore vbCr
                            .EndOf
                            .Delete
                        End With
                    End If
                End With
            End If
            r.WholeStory
        Next i
    ElseIf r.Sections.Count = 2 Then
        With r.Find
            .ClearFormatting
            .Text = "^b"
            .Execute
            If .Found Then
                r.InsertBefore vbCr
            End If
        End With
    End If

End Sub

Private Sub CreateSection(iPosition As mpDocPositions)
    
    Dim r As Word.Range
    
    On Error GoTo ProcError
    
    With m_oDoc.File
        Select Case iPosition
            Case mpDocPosition_BOF
                    .Characters.First.InsertBreak wdSectionBreakNextPage
                    .Characters.First.InsertParagraphBefore
                    m_oDoc.UnlinkHeadersFooters .Sections(2)
                    m_oDoc.ClearHeaders .Sections.First
            Case mpDocPosition_EOF
                    .Characters.Last.InsertBreak wdSectionBreakNextPage
                    m_oDoc.UnlinkHeadersFooters .Sections.Last
                    m_oDoc.ClearHeaders .Sections.Last
            Case mpDocPosition_Selection
                    Selection.InsertBreak wdSectionBreakNextPage
            Case Else
        End Select
    End With
    Exit Sub
ProcError:
    Select Case Err.Number
        Case 4605
            With m_oDoc.Selection
                .HomeKey Unit:=wdStory
                On Error Resume Next
                .SplitTable
                .Style = m_oDoc.File.Styles(wdStyleNormal)
                .InsertBreak Type:=wdSectionBreakNextPage
                .MoveRight wdCharacter, 1, 1
                .Delete
                Err.Clear
            End With
        Case Else
            RaiseError "MPO.CEnvelope.CreateSection"
    End Select
    Exit Sub
End Sub

