VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContactsFunctions"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CContactsFunctions Class
'   created 12/07/99 by Jeffrey Sweetland

'   Contains methods that
'   integrate with Contact Integration

'   Member of
'   Container for CContacts
'**********************************************************
Option Explicit
Public Sub GetLetterRecipients(Optional vTo As Variant, _
                               Optional vCC As Variant, _
                               Optional vBCC As Variant)
    Dim xTo As String
    Dim xCC As String
    Dim xBCC As String
    Dim i As Integer
    Dim colContacts As CContacts
    
    On Error GoTo ProcError
    Set colContacts = New CContacts
    colContacts.Retrieve bIncludeTo:=Not IsMissing(vTo), _
                bIncludeCC:=Not IsMissing(vCC), _
                bIncludeBCC:=Not IsMissing(vBCC)
    
    With colContacts
        For i = 1 To .ToCount
            xTo = xTo & .ToItem(i).Detail
            If i < .ToCount Then _
                xTo = xTo & vbCrLf & vbCrLf
        Next i
        For i = 1 To .CcCount
            xCC = xCC & .CcItem(i).DisplayName(False)
            If i < .CcCount Then _
                xCC = xCC & vbCrLf
        Next i
        For i = 1 To .BccCount
            xBCC = xBCC & .BccItem(i).DisplayName(False)
            If i < .BccCount Then _
                xBCC = xBCC & vbCrLf
        Next i
    End With
    
    Set colContacts = Nothing
    
    vTo = xTo
    vCC = xCC
    vBCC = xBCC
    Exit Sub
    
ProcError:
    Set colContacts = Nothing
    Err.Raise Err.Number, _
              "CContactsFunctions.GetLetterRecipients", _
              g_oError.Desc(Err.Number)
End Sub
Public Sub GetMemoRecipients(Optional vTo As Variant, _
                             Optional vFrom As Variant, _
                             Optional vCC As Variant, _
                             Optional vBCC As Variant)
    
    Dim xTo As String
    Dim xFrom As String
    Dim xCC As String
    Dim xBCC As String
    Dim i As Integer
    Dim colContacts As CContacts
    
    On Error GoTo ProcError
    Set colContacts = New CContacts
    colContacts.Retrieve bIncludeTo:=Not IsMissing(vTo), _
                            bIncludeFrom:=Not IsMissing(vFrom), _
                            bIncludeCC:=Not IsMissing(vCC), _
                            bIncludeBCC:=Not IsMissing(vBCC), _
                            iOnEmpty:=ciEmptyAddress_ReturnEmpty
    
    With colContacts
        For i = 1 To .ToCount
            xTo = xTo & .ToItem(i).DisplayName(False)
            If i < .ToCount Then _
                xTo = xTo & vbCrLf
        Next i
        For i = 1 To .FromCount
            xFrom = xFrom & .FromItem(i).DisplayName(False)
            If i < .FromCount Then _
                xFrom = xFrom & vbCrLf
        Next i
        For i = 1 To .CcCount
            xCC = xCC & .CcItem(i).DisplayName(False)
            If i < .CcCount Then _
                xCC = xCC & vbCrLf
        Next i
        For i = 1 To .BccCount
            xBCC = xBCC & .BccItem(i).DisplayName(False)
            If i < .BccCount Then _
                xBCC = xBCC & vbCrLf
        Next i
    End With
    
    Set colContacts = Nothing
    
    vTo = xTo
    vFrom = xFrom
    vCC = xCC
    vBCC = xBCC
ProcError:
    Set colContacts = Nothing
    Err.Raise Err.Number, _
              "CContactsFunctions.GetMemoRecipients", _
              g_oError.Desc(Err.Number)
End Sub
Public Sub GetFaxRecipients(ByRef xarFax As XArray, _
                    Optional bIncludeCompany As Boolean = True, _
                    Optional bIncludePhone As Boolean = True, _
                    Optional bIncludeFax As Boolean = True)
    
    Dim i As Integer
    Dim iCompanyIndex As Integer
    Dim iPhoneIndex As Integer
    Dim iFaxIndex As Integer
    Dim iColCount As Integer
    Dim iRowCount As Integer
    Dim colContacts As CContacts
    
    On Error GoTo ProcError
    
    Set colContacts = New CContacts
    colContacts.Retrieve bIncludePhone:=True, bIncludeFax:=True, bPromptForPhones:=True
    
    If colContacts.Count = 0 Then Exit Sub
    
    iColCount = 0
    
    If bIncludeCompany Then
        iColCount = iColCount + 1
        iCompanyIndex = iColCount
    End If
    
    If bIncludeFax Then
        iColCount = iColCount + 1
        iFaxIndex = iColCount
    End If
    
    If bIncludePhone Then
        iColCount = iColCount + 1
        iPhoneIndex = iColCount
    End If
        
    With colContacts
        iRowCount = -1
        On Error Resume Next
        iRowCount = xarFax.UpperBound(1)
        On Error GoTo ProcError
        
        If iRowCount = -1 Then
            xarFax.ReDim 0, .Count, 0, iColCount
        Else
            xarFax.ReDim 0, iRowCount + .Count, 0, iColCount
        End If
    
        For i = 1 To .Count
            xarFax(iRowCount + i, 0) = .Item(i).FullName
            If bIncludeCompany Then _
                xarFax(iRowCount + i, iCompanyIndex) = .Item(i).Company
            If bIncludeFax Then _
                xarFax(iRowCount + i, iFaxIndex) = .Item(i).Fax
            If bIncludePhone Then _
                xarFax(iRowCount + i, iPhoneIndex) = .Item(i).Phone
        Next i
    End With
    
    Set colContacts = Nothing
ProcError:
    Set colContacts = Nothing
    Err.Raise Err.Number, _
              "CContactsFunctions.GetFaxRecipients", _
              g_oError.Desc(Err.Number)
End Sub
Public Sub GetServiceRecipients(ByRef xarS As XArray, _
                    Optional bIncludeFaxNumber As Boolean = True, _
                    Optional bPhoneField As Boolean = False, _
                    Optional bFaxField As Boolean = False, _
                    Optional xFaxLabel As String = "Fax: ")
    
    Dim i As Integer
    Dim iPhoneIndex As Integer
    Dim iFaxIndex As Integer
    Dim iColCount As Integer
    Dim iRowCount As Integer
    Dim colContacts As CContacts
    
    On Error GoTo ProcError
    Set colContacts = New CContacts
    
    colContacts.Retrieve bIncludePhone:=bPhoneField, _
            bIncludeFax:=bIncludeFaxNumber Or bFaxField, _
            bPromptForPhones:=False
    
    If colContacts.Count = 0 Then Exit Sub
    
    iColCount = 1
    
    If bPhoneField Then
        iColCount = iColCount + 1
        iPhoneIndex = iColCount
    End If
    
    If bFaxField Then
        iColCount = iColCount + 1
        iFaxIndex = iColCount
    End If
    
    With colContacts
        iRowCount = -1
        On Error Resume Next
        iRowCount = xarS.UpperBound(1)
        On Error GoTo ProcError
        
        If iRowCount = -1 Then
            xarS.ReDim 0, .Count - 1, 0, iColCount
        Else
            xarS.ReDim 0, iRowCount + .Count, 0, iColCount
        End If
    
        For i = 1 To .Count
            xarS(iRowCount + i, 0) = .Item(i).DisplayName
            xarS(iRowCount + i, 1) = .Item(i).Detail(bNoDisplayName:=True, _
                                bFax:=bIncludeFaxNumber And Not bFaxField, _
                                xFaxLabel:=xFaxLabel)
            If bPhoneField Then _
                xarS(iRowCount + i, iPhoneIndex) = .Item(i).Phone
            If bFaxField Then _
                xarS(iRowCount + i, iFaxIndex) = .Item(i).Fax
        Next i
    End With
    
    Set colContacts = Nothing
    
ProcError:
    Set colContacts = Nothing
    Err.Raise Err.Number, _
              "CContactsFunctions.GetServiceRecipients", _
              g_oError.Desc(Err.Number)
End Sub
Public Sub GetContactAddresses(ByRef vReturn As Variant, _
                                Optional bReturnXArray As Boolean = False, _
                                Optional bIncludePhone As Boolean = False, _
                                Optional xPhoneLabel As String = "Telephone: ", _
                                Optional bIncludeFax As Boolean = False, _
                                Optional xFaxLabel As String = "Facsimile: ", _
                                Optional bIncludeEmail As Boolean = False, _
                                Optional xEmailLabel As String = "E-mail: ")
    
    Dim i As Integer
    Dim iPhoneIndex As Integer
    Dim iFaxIndex As Integer
    Dim iEmailIndex As Integer
    
    Dim iColCount As Integer
    Dim colContacts As CContacts
    
    On Error GoTo ProcError
    
    Set colContacts = New CContacts
    colContacts.Retrieve bIncludePhone:=bIncludePhone, _
            bIncludeFax:=bIncludeFax, _
            bIncludeEmail:=bIncludeEmail, _
            bPromptForPhones:=False
    
    If colContacts.Count = 0 Then Exit Sub
    
    If bReturnXArray Then
        iColCount = 0
        
        If bIncludePhone Then
            iColCount = iColCount + 1
            iPhoneIndex = iColCount
        End If
    
        If bIncludeFax Then
            iColCount = iColCount + 1
            iFaxIndex = iColCount
        End If
        
        If bIncludeEmail Then
            iColCount = iColCount + 1
            iEmailIndex = iColCount
        End If
    
        With colContacts
            vReturn.ReDim 0, .Count - 1, 0, iColCount
    
            For i = 1 To .Count
                vReturn(i - 1, 0) = .Item(i).Detail
                If bIncludePhone And .Item(i).Phone <> "" Then _
                    vReturn(i - 1, iPhoneIndex) = .Item(i).Phone
                If bIncludeFax And .Item(i).Fax <> "" Then _
                    vReturn(i - 1, iFaxIndex) = .Item(i).Fax
                If bIncludeEmail And .Item(i).Email <> "" Then _
                    vReturn(i - 1, iEmailIndex) = .Item(i).Email
            Next i
        End With
    Else
        With colContacts
            For i = 1 To .Count
                vReturn = vReturn & .Item(i).Detail(bPhone:=bIncludePhone, _
                                            bFax:=bIncludeFax, _
                                            bEmail:=bIncludeEmail, _
                                            xPhoneLabel:=xPhoneLabel, _
                                            xFaxLabel:=xFaxLabel, _
                                            xEmailLabel:=xEmailLabel)
                If i < .Count Then _
                    vReturn = vReturn & vbCrLf & vbCrLf
            Next i
        End With
    End If
    
    Set colContacts = Nothing
ProcError:
    Set colContacts = Nothing
    Err.Raise Err.Number, _
              "CContactsFunctions.GetContactAddresses", _
              g_oError.Desc(Err.Number)
End Sub

