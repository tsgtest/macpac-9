VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessSignature"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CBusinessSignature Class
'   created 3/14/00  by Jeffrey Sweetland

'   Contains properties and methods that
'   define a MacPac Business Signature

'   Container for CDocument, CBusinessSignatures
'**********************************************************
Private Const mpDocSegment As String = "BusinessSig"
Private m_oDef As mpDB.CBusinessSignatureDef
Private m_bRetrieveVars As Boolean
Private m_bSaveVars As Boolean
Private m_bDynamicUpdating As Boolean
Private m_oDoc As MPO.CDocument
'---9.5.0
Private m_oCustProps As MPO.CCustomProperties
'---End 9.5.0
Private m_oTemplate As MPO.CTemplate

Private m_iID As Integer
' Type ID from BSigDefs db types
Private m_lSigType As Long
' Table cell holding signature
Private m_iPosition As Integer
Private m_xKey As String
Private m_xEntityTitle As String
Private m_xPartyName As String
Private m_xPartyDescription As String
Private m_xSignerName As String
Private m_xSignerTitle As String
Private m_bIncludeIts As Boolean
Private m_xDateText As String

' Object can have its own collection of sub-entities
Private m_oSubEntities As CBusinessSignatures
' If member of a sub-entity collection, this holds ID of Parent object in its collection
Private m_iParentID As Integer
'**********************************************************
'   Properties
'**********************************************************
Friend Property Let ID(iNew As Integer)
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "ID", DocSegment, iNew, iNew
    m_iID = iNew
End Property
Public Property Let ParentID(iNew As Integer)
    m_iParentID = iNew
End Property
Public Property Get ParentID() As Integer
    ParentID = m_iParentID
End Property
Public Property Get ID() As Integer
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_iID = m_oDoc.RetrieveItem("ID", DocSegment, m_iID)
    ID = m_iID
End Property
Private Function DocSegment() As String
    ' Segment will include index of parent object if Sub-entity
    If m_iParentID > 0 Then
        DocSegment = mpDocSegment & "_" & m_iParentID
    Else
        DocSegment = mpDocSegment
    End If
End Function
Friend Property Let Key(xNew As String)
    ' Unique string identifying member of CPleadingSignatures collection
    m_xKey = xNew
End Property
Public Property Get Key() As String
    On Error Resume Next
    Key = m_xKey
End Property
Public Property Let Position(iNew As Integer)
    Dim oSig As CBusinessSignature
    
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "Position", DocSegment, m_iID, iNew
    m_iPosition = iNew
    For Each oSig In SubEntities
        oSig.Position = iNew
    Next oSig
End Property
Public Property Get Position() As Integer
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_iPosition = m_oDoc.RetrieveItem("Position", DocSegment, m_iID)
    Position = m_iPosition
End Property
'Public Property Let DisplayName(xNew As String)
'    m_xDisplayName = xNew
'    If m_bSaveVars Or m_bDynamicUpdating Then _
'        m_oDoc.SaveItem "DisplayName", DocSegment, m_iID, xNew
'End Property

Public Property Get DisplayName() As String
    If Me.EntityTitle <> "" Then
        DisplayName = Me.EntityTitle
    ElseIf Me.PartyName <> "" Then
        DisplayName = Me.PartyName
    ElseIf Me.SignerName <> "" Then
        DisplayName = Me.SignerName
    ElseIf Me.SignerTitle <> "" Then
        DisplayName = Me.SignerTitle
    Else
        DisplayName = "Blank Signature Line"
    End If
End Property
Public Property Let DynamicUpdating(bNew As Boolean)
    m_bDynamicUpdating = bNew
End Property
Public Function Template() As MPO.CTemplate
    Set Template = m_oTemplate
End Function
Public Property Get Definition() As mpDB.CBusinessSignatureDef
    If m_oDef Is Nothing Then
        Set m_oDef = g_oDBs.BusinessSignatureDefs.Item(m_lSigType)
    End If
    Set Definition = m_oDef
End Property
'---9.5.0
Public Function CustomProperties() As MPO.CCustomProperties
    Set CustomProperties = m_oCustProps
End Function
'---End 9.5.0

Public Property Get BoilerplateFile() As String
    BoilerplateFile = Definition.BoilerplateFile
End Property
Public Property Let TypeID(lNew As Long)
    Dim xDesc As String
    Dim oSig As CBusinessSignature

    On Error GoTo ProcError
    
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "TypeID", DocSegment, m_iID, lNew
    
    m_lSigType = lNew
    
    Set m_oDef = g_oDBs.BusinessSignatureDefs.Item(lNew)

'       alert and exit if not a valid Type
    If (m_oDef Is Nothing) Then
        Err.Raise mpError_InvalidMember
    End If
   
    For Each oSig In SubEntities
        oSig.TypeID = lNew
    Next oSig
    
    Exit Property

ProcError:
    If Err.Number = mpError_InvalidMember Then
        xDesc = "Invalid TypeID.  " & _
            "Please check the appropriate tblUserOptions " & _
            "table in mpPrivate.mdb."
    Else
        xDesc = g_oError.Desc(Err.Number)
    End If
    Err.Raise Err.Number, "MPO.CBusinessSignature.TypeID", xDesc

End Property
Public Property Get TypeID() As Long
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_lSigType = m_oDoc.RetrieveItem("TypeID", DocSegment, m_iID, mpItemType_Integer)
    TypeID = m_lSigType
End Property
Public Property Let EntityTitle(xNew As String)
    If (Me.EntityTitle <> xNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpEntityTitle" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), xNew, , True, , m_iID
    End If
    m_xEntityTitle = xNew
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "EntityTitle", DocSegment, m_iID, xNew

End Property
Public Property Get EntityTitle() As String
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_xEntityTitle = m_oDoc.RetrieveItem("EntityTitle", DocSegment, m_iID)
    EntityTitle = m_xEntityTitle
End Property
Public Property Let PartyName(xNew As String)
    If (Me.PartyName <> xNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpPartyName" & _
            IIf(m_iParentID, "Sub_" & m_iParentID, ""), _
            xNew, , xNew & Me.PartyDescription = "", xNew & Me.PartyDescription = "", m_iID
    End If
    m_xPartyName = xNew
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "PartyName", DocSegment, m_iID, xNew

End Property
Public Property Get PartyName() As String
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_xPartyName = m_oDoc.RetrieveItem("PartyName", DocSegment, m_iID)
    PartyName = m_xPartyName
End Property
Public Property Let PartyDescription(xNew As String)
    If (Me.PartyDescription <> xNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpPartyDescription" & _
            IIf(m_iParentID, "Sub_" & m_iParentID, ""), _
            xNew, , xNew & Me.PartyName = "", xNew & Me.PartyName = "", m_iID
    End If
    m_xPartyDescription = xNew
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "PartyDescription", DocSegment, m_iID, xNew

End Property
Public Property Get PartyDescription() As String
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_xPartyDescription = m_oDoc.RetrieveItem("PartyDescription", DocSegment, m_iID)
    PartyDescription = m_xPartyDescription
End Property
Public Property Let SignerName(xNew As String)
    If (Me.SignerName <> xNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
        m_oDoc.EditItem "zzmpSignerName" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), xNew, , True, , m_iID
    End If
    m_xSignerName = xNew
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "SignerName", DocSegment, m_iID, xNew

End Property
Public Property Get SignerName() As String
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_xSignerName = m_oDoc.RetrieveItem("SignerName", DocSegment, m_iID)
    SignerName = m_xSignerName
End Property
Public Property Let SignerTitle(xNew As String)
    If (Me.SignerTitle <> xNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
        If Me.IncludeIts Then
            m_oDoc.EditItem "zzmpSignerTitle" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), "", , True, True, m_iID
            m_oDoc.EditItem "zzmpSignerIts" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), xNew, , False, False, m_iID, , True
        Else
            m_oDoc.EditItem "zzmpSignerTitle" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), xNew, , True, Me.SignerTitle = "", m_iID
            m_oDoc.EditItem "zzmpSignerIts" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), "", , True, True, m_iID, , True
        End If
    End If
    m_xSignerTitle = xNew
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "SignerTitle", DocSegment, m_iID, xNew

End Property
Public Property Get SignerTitle() As String
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_xSignerTitle = m_oDoc.RetrieveItem("SignerTitle", DocSegment, m_iID)
    SignerTitle = m_xSignerTitle
End Property
Public Property Let DateText(xNew As String)
'    If (Me.DateText <> xNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
'        m_oDoc.EditItem "zzmpDateText" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), xNew, , False, , m_iID, , True
'    End If
'    m_xDateText = xNew
'    If m_bSaveVars Or m_bDynamicUpdating Then _
'        m_oDoc.SaveItem "DateText", DocSegment, m_iID, xNew

'*c - WF
    If (Me.DateText <> xNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
        If InStr(xNew, "Field") = 0 Then
            m_oDoc.EditItem_ALT "zzmpDateText" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), xNew, , False, , m_iID, , True
        Else
             m_oDoc.EditDateItem "zzmpDateText" & IIf(m_iParentID, "Sub_" & m_iParentID, ""), , , mpDateFormat_LongText
        End If
    End If
    m_xDateText = xNew
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "DateText", DocSegment, m_iID, xNew
End Property

Public Property Get DateText() As String
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_xDateText = m_oDoc.RetrieveItem("DateText", DocSegment, m_iID)
    DateText = m_xDateText
End Property

Public Property Let IncludeIts(bNew As Boolean)
    If (Me.IncludeIts <> bNew And m_bDynamicUpdating) Or g_bForceItemUpdate Then
        If Me.SignerTitle = "" And Not bNew Then
            m_oDoc.HideItem "zzmpSignerIts" & _
                IIf(m_iParentID, "Sub_" & m_iParentID, "") & "_" & m_iID, True, True
        ElseIf bNew Then
            m_oDoc.HideItem "zzmpSignerIts" & _
                IIf(m_iParentID, "Sub_" & m_iParentID, "") & "_" & m_iID, False, True
            m_oDoc.HideItem "zzmpSignerTitle" & _
                IIf(m_iParentID, "Sub_" & m_iParentID, "") & "_" & m_iID, True, True
        Else
            m_oDoc.HideItem "zzmpSignerIts" & _
                IIf(m_iParentID, "Sub_" & m_iParentID, "") & "_" & m_iID, True, True
            m_oDoc.HideItem "zzmpSignerTitle" & IIf(m_iParentID, "Sub_" & m_iParentID, "") & "_" & m_iID, False, True
        End If
    End If
    m_bIncludeIts = bNew
    If m_bSaveVars Or m_bDynamicUpdating Then _
        m_oDoc.SaveItem "IncludeIts", DocSegment, m_iID, bNew

End Property
Public Property Get IncludeIts() As Boolean
    On Error Resume Next
    If m_bRetrieveVars Then _
        m_bIncludeIts = m_oDoc.RetrieveItem("IncludeIts", DocSegment, m_iID, 1)
    IncludeIts = m_bIncludeIts
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function SubEntities() As CBusinessSignatures
    Set SubEntities = m_oSubEntities
End Function
Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function
Public Sub InsertBoilerplate(Optional bRebuildTable As Boolean = False)
    Dim xTBM As String
    Dim rngBMK As Word.Range
    Dim iCell As Integer
    
    On Error Resume Next
  '---test for valid type def
    If m_oDef Is Nothing Then _
        Set m_oDef = g_oDBs.BusinessSignatureDefs.Item(m_lSigType)
    
    If m_oDef Is Nothing Then Exit Sub
    
    With Document
'---if hide fixed bookmarks = true , then hide table bookmark,  reset string for location
        xTBM = Definition.TableBookmark
        
        If .bHideFixedBookmarks Then
            .HideBookmark (xTBM)
            xTBM = "_" & xTBM
        End If
        
        If Not .bBoilerPlateExists(xTBM) Then
            xTBM = Definition.TableBookmark
            If .Selection.Paragraphs(1).Range.Text <> vbCr Then
                .Selection.Collapse wdCollapseEnd
                .Selection.TypeText vbCr
            End If
            .InsertBoilerplate Definition.BoilerplateFile, xTBM
            '---9.5.0
            If Not Definition.FullRowFormat Then
                .DeleteTableCellContents 1, xTBM
            End If
            '---End 9.5.0
            DoEvents
            If .bHideFixedBookmarks Then
                .HideBookmark (xTBM)
                xTBM = "_" & xTBM
            End If
        ElseIf bRebuildTable Then
            ' Delete existing table and reinsert blank
            Set rngBMK = ActiveDocument.Bookmarks(xTBM).Range
            rngBMK.Text = ""
            rngBMK.Tables(1).Delete
            DoEvents
            xTBM = Definition.TableBookmark
            rngBMK.Select
            

            .InsertBoilerplate Definition.BoilerplateFile, xTBM
            '---9.5.0
            If Not Definition.FullRowFormat Then
                .DeleteTableCellContents 1, xTBM
            End If
            '---End 9.5.0
            DoEvents
            If .bHideFixedBookmarks Then
                .HideBookmark (xTBM)
                xTBM = "_" & xTBM
            End If
        End If
        
        DoEvents
        
        iCell = Me.Position
        
        ' If not sub-entity, then delete cell contents and replace
        ' Otherwise, add to end of existing cell contents
'---9.5.0
        If Not Definition.FullRowFormat Then
            If m_iParentID = 0 Then
                .DeleteTableCellContents iCell, xTBM
                .SelectTablePosition iCell, xTBM
                .InsertBoilerplate Definition.BoilerplateFile, Definition.SignatureBlockBookmark
            Else
                .SelectTablePosition iCell, xTBM
                Selection.EndOf wdCell, wdMove
                .InsertBoilerplate Definition.BoilerplateFile, Definition.SubEntityBookmark
            End If
        ElseIf iCell <> 1 Or m_iParentID > 0 Then
            If m_iParentID = 0 Then
                ActiveDocument.Bookmarks(xTBM).Range.Select
                Selection.EndOf
		'GLOG : 5434 ceh
                .InsertBoilerplate Definition.BoilerplateFile, Definition.TableBookmark, , , , , , True
                .File.Bookmarks.Add xTBM, .File.Bookmarks(Definition.TableBookmark).Range
                .File.Bookmarks(Definition.TableBookmark).Delete
            Else
                ActiveDocument.Bookmarks(Definition.SubEntityBookmark).Range.Select
                Selection.EndOf wdCell, wdMove
		'GLOG : 5434 ceh
                .InsertBoilerplate Definition.BoilerplateFile, Definition.SubEntityBookmark, , , , , , True
            End If
        End If
'---End 9.5.0
        
        .IndexPlaceholders m_iID, xTBM, m_iParentID
        
    End With
End Sub
Friend Sub Finish(Optional bForceRefresh As Boolean = True)
'cleans up doc after all user input is complete
    Dim rngP As Range
    Dim bDynamic As Boolean
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    g_bForceItemUpdate = True
    
    With m_oDoc
'       Refresh properties
        If bForceRefresh Then
            Refresh
        End If
 
 '      delete any hidden text
        .DeleteHiddenText , , , True
        .RemoveFormFields
        .Selection.MoveDown
        .ClearBookmarks
    End With
    
    g_bForceItemUpdate = True
    Exit Sub
ProcError:
    g_bForceItemUpdate = False
    Err.Raise Err.Number
End Sub
Public Sub Refresh(Optional bRebuild As Boolean = False)
'--refresh all properties by setting them to themselves
'--this ensures creation of doc vars
    Dim bForce As Boolean
    Dim oSig As CBusinessSignature
    
    m_bSaveVars = True
    With Me
        InsertBoilerplate bRebuild
        bForce = g_bForceItemUpdate
        g_bForceItemUpdate = True
        .ID = .ID
        .TypeID = .TypeID
        .Position = .Position
        .DateText = .DateText
        .EntityTitle = .EntityTitle
        .PartyDescription = .PartyDescription
        .PartyName = .PartyName
        .SignerName = .SignerName
        .SignerTitle = .SignerTitle
        .IncludeIts = .IncludeIts

        ' Refresh sub-entities properties
        For Each oSig In m_oSubEntities
            oSig.Refresh
        Next oSig
'---9.5.0
'       refresh custom properties
        If .ParentID > 0 Then
            .CustomProperties.RefreshIndexedValues .ID, .ParentID
        Else
            .CustomProperties.RefreshIndexedValues .ID
        End If
'---End 9.5.0
        g_bForceItemUpdate = bForce
    End With
    m_bSaveVars = False
End Sub
Public Sub RefreshEmpty()
'force properties that may not have been
'set by user to get updated in document
    Dim i As Integer
    Dim oCustProp As MPO.CCustomProperty
    Dim bForce As Boolean
    Dim oSig As CBusinessSignature
       
    m_bSaveVars = True
    With Me
        bForce = g_bForceItemUpdate
        g_bForceItemUpdate = True
        
                
        If .DateText = Empty Then _
            .DateText = .DateText
        If .EntityTitle = Empty Then _
            .EntityTitle = .EntityTitle
        If .PartyDescription = Empty Then _
            .PartyDescription = .PartyDescription
        If .PartyName = Empty Then _
            .PartyName = .PartyName
        If .SignerName = Empty Then _
            .SignerName = .SignerName
        If .SignerTitle = Empty Then _
            .SignerTitle = .SignerTitle
        If .IncludeIts = Empty Then _
            .IncludeIts = .IncludeIts
        
        ' Refresh sub-entities properties
        For Each oSig In m_oSubEntities
            oSig.RefreshEmpty
        Next oSig
'       refresh empty custom properties
        .CustomProperties.RefreshEmptyValues
        
        g_bForceItemUpdate = bForce
    End With
    m_bSaveVars = False
End Sub
Friend Sub LoadValues(Optional iParent As Integer)
    ' Loads all properties from saved document variables
    
    m_bRetrieveVars = True
    With Me
        m_iParentID = iParent
        .ID = .ID
        .TypeID = .TypeID
        .Position = .Position
        .DateText = .DateText
        .EntityTitle = .EntityTitle
        .PartyDescription = .PartyDescription
        .PartyName = .PartyName
        .SignerName = .SignerName
        .SignerTitle = .SignerTitle
        .IncludeIts = .IncludeIts
        ' Check for existence of stored sub-entities as well
        If m_iParentID = 0 Then
            m_oSubEntities.LoadValues
        End If
        
'       refresh empty custom properties
        .CustomProperties.RefreshValues
    End With
    m_bRetrieveVars = False
End Sub
'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New MPO.CDocument
    Set m_oCustProps = New MPO.CCustomProperties
    Set m_oSubEntities = New MPO.CBusinessSignatures
    Set m_oSubEntities.Parent = Me

    m_oCustProps.Category = "BusinessSignature"
    Set m_oTemplate = g_oTemplates.ItemFromClass("CBusiness")
    Me.Position = 1
End Sub
Private Sub Class_Terminate()
    Set m_oDoc = Nothing
    Set m_oCustProps = Nothing
    Set m_oTemplate = Nothing
    Set m_oSubEntities = Nothing
End Sub
