VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cPageNumberNEW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CPageNumber Class
'   created 3/6/00 by Dan Fisherman-
'   momshead@earthlink.net

'   edited 5/21/09 by Charlie Homo
'   for use in mp10

'   edited 02/20/14 by Charlie Homo
'   for use in mp9

'   contains methods and functions that insert Page Numbers
'**********************************************************
Option Explicit

Private Enum mpPageNumberLocations
    mpPageNumberLocation_Header = 1
    mpPageNumberLocation_Footer = 2
    mpPageNumberLocation_Cursor = 3
End Enum

Private Enum mpPageNumberAlignments
    mpPageNumberAlignment_Left = 1
    mpPageNumberAlignment_Center = 2
    mpPageNumberAlignment_Right = 3
End Enum

Private Enum mpPageNumbers
    mpPageNumbers_Arabic = 1
    mpPageNumbers_UpperRoman = 2
    mpPageNumbers_LowerRoman = 3
    mpPageNumbers_UpperAlpha = 4
    mpPageNumbers_LowerAlpha = 5
    mpPageNumbers_CardText = 6
End Enum

Private Enum mpPageNumberRemoveOptions
    mpPageNumberRemoveOption_AllPages = 1
    mpPageNumberRemoveOption_FirstPage = 2
End Enum

Private m_bXofY As Boolean
Private m_iNum As mpPageNumbers
Private m_iLocation As mpPageNumberLocations
Private m_iAlign As mpPageNumberAlignments
Private m_iNumRemoveOption As mpPageNumberRemoveOptions
Private m_xTextBefore As String
Private m_xTextAfter As String
Private m_bFirstPage As Boolean
Private m_iSection As Integer
Private m_bResetPrompt As Boolean
Private m_bRestartNumbering As Boolean
Private m_iRestartAtNumber As Integer
Private m_bSetupDiffFirstPage As Boolean
'
'**********************************************************
'   Methods
'**********************************************************
Public Sub Insert(ByVal iLocation As Integer, _
                  ByVal vTargetedSections As Variant, _
                  ByVal bFirstPage As Boolean, _
                  ByVal bXofY As Boolean, _
                  ByVal iAlignment As Integer, _
                  ByVal xTextBefore As String, _
                  ByVal xTextAfter As String, _
                  ByVal iNumber As Integer, _
                  ByVal bRestartNumbering As Boolean, _
                  ByVal iRestartAtNumber As Integer)
'inserts a page number -prompt is reset to allow
'user to be prompted in PromptForDeletion
    Const mpThisFunction As String = "MPO.cPageNumberNEW.Insert"
    Dim oRange As Word.Range
    Dim oSec As Word.Section
    Dim i As Integer
    Dim iAnswer As Integer
    Dim xMsg As String
    Dim oWordDoc As CDocument
    Dim xSections() As String
    
    On Error GoTo ProcError
    
'   initialize module level variables
    m_xTextBefore = xTextBefore
    m_xTextAfter = xTextAfter
    m_bXofY = bXofY
    m_iNum = iNumber
    m_iLocation = iLocation
    m_bFirstPage = bFirstPage
    m_iAlign = iAlignment
    m_bRestartNumbering = bRestartNumbering
    m_iRestartAtNumber = iRestartAtNumber
    
    Set oWordDoc = New CDocument
    
    'get targeted section array
    xSections() = Split(vTargetedSections, ",")
    
    If m_iLocation = mpPageNumberLocation_Cursor Then
'       insert page number at cursor
        Set oRange = Word.Selection.Range

'       insert number at range
        InsertPageNumber oRange
    ElseIf UBound(xSections()) = 0 Then   'only one targeted section
'*      check page setup
        'GLOG 4748: Don't use PageSetup object to avoid bug when Framed table exists
        If Not (ActiveDocument.Sections(CLng(xSections(0))) _
           .Footers(wdHeaderFooterFirstPage).Exists) And _
           Not m_bFirstPage Then
            xMsg = "You have chosen not to include a page number on the first page of this section, but "
            xMsg = xMsg & "it is not currently setup to allow for this."
            xMsg = xMsg & vbCr & vbCr & "Click 'Yes' to allow for a different first page header\footer."
            xMsg = xMsg & vbCr & "If you choose 'No', the page number will appear on every page."
            iAnswer = MsgBox(xMsg, vbYesNoCancel + vbInformation, App.Title)
            Select Case iAnswer
                Case vbYes
                    m_bSetupDiffFirstPage = True
                Case vbNo
                    m_bSetupDiffFirstPage = False
                Case Else
                    GoTo ProcEnd
            End Select
        Else
            m_bSetupDiffFirstPage = False
        End If
        InsertPageNumberInHeadersFooters xSections(0)
    Else    'there are more than one targeted section
'*      check page setup
        If oWordDoc.HasSecWithNoDiffFirstPage(xSections()) And _
           Not m_bFirstPage Then
            xMsg = "You have chosen not to include a page number on the first page of sections of this document, but "
            xMsg = xMsg & "some sections are not setup to allow for this."
            xMsg = xMsg & vbCr & vbCr & "Click 'Yes' to allow for a different first page header\footer on each section of this document."
            xMsg = xMsg & vbCr & "If you choose 'No', the page number will appear on every page of the sections that are not properly setup."
            iAnswer = MsgBox(xMsg, vbYesNoCancel + vbInformation, App.Title)
            Select Case iAnswer
                Case vbYes
                    m_bSetupDiffFirstPage = True
                Case vbNo
                    m_bSetupDiffFirstPage = False
                Case Else
                    GoTo ProcEnd
            End Select
        Else
            m_bSetupDiffFirstPage = False
        End If
'       insert in all sections - cycle
        For i = 0 To UBound(xSections())
            InsertPageNumberInHeadersFooters xSections(i)
        Next i
    End If

ProcEnd:
    'JTS 5/7/10: Open and close header to clear out empty paragraph mark if present
    Word.WordBasic.ViewHeader
    Word.WordBasic.ViewHeader
    m_bResetPrompt = True
    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub

'GLOG : 6025 : CEH
Public Sub Remove(ByVal iRemoveOption As Integer, _
                  ByVal vTargetedSections As Variant)
'inserts a page number -prompt is reset to allow
'user to be prompted in PromptForDeletion
    Const mpThisFunction As String = "MPO.CPageNumberNEW.Remove"
    
    Dim oRange As Word.Range
    Dim oSec As Word.Section
    Dim i As Integer
    Dim iAnswer As Integer
    Dim xMsg As String
    Dim oWordDoc As CDocument

    On Error GoTo ProcError

'   initialize module level variable for first page
    m_bFirstPage = (iRemoveOption = mpPageNumberRemoveOption_FirstPage)
    
    Set oWordDoc = New CDocument

    If UBound(vTargetedSections) = 0 Then   'only one targeted section
'*      check page setup
        'GLOG 4748: Don't use PageSetup object to avoid bug when Framed table exists
        If Not (ActiveDocument.Sections(vTargetedSections(0)) _
           .Footers(wdHeaderFooterFirstPage).Exists) And _
           m_bFirstPage Then
            xMsg = "You have chosen to remove the page number from the first page of this section, but "
            xMsg = xMsg & "it is not currently setup to allow for this."
            xMsg = xMsg & vbCr & vbCr & "Click 'Ok' to allow for a different first page header\footer."
            iAnswer = MsgBox(xMsg, vbOKCancel + vbInformation, App.Title)
            Select Case iAnswer
                Case vbOK
                    m_bSetupDiffFirstPage = True
                Case Else
                    GoTo ProcEnd
            End Select
        Else
            m_bSetupDiffFirstPage = False
        End If
        RemovePageNumberInHeadersFooters vTargetedSections(0)
    Else
'*      check page setup
        If Not (oWordDoc.HasSecWithNoDiffFirstPage(vTargetedSections)) And _
           m_bFirstPage Then
            xMsg = "You have chosen to remove the page number from the first page of all sections of this document, but "
            xMsg = xMsg & "some sections are not setup to allow for this."
            xMsg = xMsg & vbCr & vbCr & "Click 'Ok' to allow for a different first page header\footer on each section of this document."
            iAnswer = MsgBox(xMsg, vbOKCancel + vbInformation, App.Title)
            Select Case iAnswer
                Case vbOK
                    m_bSetupDiffFirstPage = True
                Case Else
                    GoTo ProcEnd
            End Select
        Else
            m_bSetupDiffFirstPage = False
        End If
'       insert in all sections - cycle
        For i = 0 To UBound(vTargetedSections)
            RemovePageNumberInHeadersFooters vTargetedSections(i)
        Next i
    End If

ProcEnd:
    Word.WordBasic.ViewHeader
    Word.WordBasic.ViewHeader
    m_bResetPrompt = True
    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub

Private Sub InsertPageNumberInHeadersFooters(ByVal iSection As Integer)
'inserts page number in headers or footers of specified section
    Const mpThisFunction As String = "MPO.CPageNumberNEW.InsertPageNumberInHeadersFooters"
    Dim oHFs As Word.HeadersFooters
    Dim oHF As Word.HeaderFooter
    Dim bDo As Boolean
    Dim vStyle As Variant
    Dim oSec As Word.Section
    Dim xDesc As String
    Dim oNextSec As Word.Section
    Dim xDest As String
    Dim oWordDoc As CDocument

    With Word.ActiveDocument.Sections
        On Error Resume Next
        Set oSec = .Item(iSection)
        On Error GoTo ProcError

'       get next section if it exists
        If iSection < .Count Then
            Set oNextSec = .Item(iSection + 1)
        End If
    End With

    If oSec Is Nothing Then
        xDesc = "Section " & iSection & " does not exist in this document."
        Err.Raise mpError_InvalidMember
    End If

    Set oWordDoc = New CDocument

'   get headers/footers
    If m_iLocation = mpPageNumberLocation_Header Then
        Set oHFs = oSec.Headers
        If oSec.PageSetup.Orientation = wdOrientLandscape Then
            vStyle = "Header Landscape"
            CheckStyleExists vStyle
        Else
            vStyle = wdStyleHeader
        End If

'       unlink current section headers
        oWordDoc.UnlinkHeaders oSec

        If Not (oNextSec Is Nothing) Then
'           unlink next section headers
            oWordDoc.UnlinkHeaders oNextSec
        End If

    ElseIf m_iLocation = mpPageNumberLocation_Footer Then
        Set oHFs = oSec.Footers
        If oSec.PageSetup.Orientation = wdOrientLandscape Then
            vStyle = "Footer Landscape"
            CheckStyleExists vStyle
        Else
            vStyle = wdStyleFooter
        End If
'       unlink current section footers
        oWordDoc.UnlinkFooters oSec
        If Not (oNextSec Is Nothing) Then
'           unlink next section footers
            oWordDoc.UnlinkFooters oNextSec
        End If
    End If

'   set destination string
    xDest = LCase(ActiveDocument.Styles(vStyle).NameLocal)

'********* JTS 2/20/02
    Dim xType As String

'   do primary header/footer
    Set oHF = oHFs(wdHeaderFooterPrimary)
    xType = "primary"

'   prompt for deletion of existing
'   contents if there are any
    bDo = PromptForDeletion(oHF, xType, xDest)
    
    If bDo Then
        With oHF.PageNumbers
            ' Set Numberstyle of section so that TOC/TOA entries will
            ' use the same format displayed on page
            Select Case m_iNum
                Case mpPageNumbers_LowerRoman
                    .NumberStyle = wdPageNumberStyleLowercaseRoman
                Case mpPageNumbers_UpperRoman
                    .NumberStyle = wdPageNumberStyleUppercaseRoman
                Case mpPageNumbers_LowerAlpha
                    .NumberStyle = wdPageNumberStyleLowercaseLetter
                Case mpPageNumbers_UpperAlpha
                    .NumberStyle = wdPageNumberStyleUppercaseLetter
                Case Else
                    .NumberStyle = wdPageNumberStyleArabic
            End Select
            If m_bRestartNumbering Then
                .RestartNumberingAtSection = True
                .StartingNumber = m_iRestartAtNumber
            Else
                .RestartNumberingAtSection = False
                .StartingNumber = 1
            End If
        End With
'       insert number based on format
        InsertPageNumber oHF.Range, vStyle, True
    End If

    ' Also do Even page footers if appropriate
    If oSec.PageSetup.OddAndEvenPagesHeaderFooter Then
        Set oHF = oHFs(wdHeaderFooterEvenPages)
        xType = "even pages"
'       prompt for deletion of existing
'       contents if there are any
        bDo = PromptForDeletion(oHF, xType, xDest)
        If bDo Then
'               clear header/footer
            With oHF.Range
                If Len(.Text) > 1 Then
                    .Text = ""
'                        .MoveEnd wdCharacter, -1
'                        .Delete
                End If
            End With
            InsertPageNumber oHF.Range, vStyle, True
        End If

    End If
'****************

'   do first page if needed
    If m_bSetupDiffFirstPage Then
        oSec.PageSetup.DifferentFirstPageHeaderFooter = True
    End If

    If oSec.PageSetup.DifferentFirstPageHeaderFooter Then

        Set oHF = oHFs(wdHeaderFooterFirstPage)
'       prompt for deletion of existing
'       contents if there are any
        bDo = PromptForDeletion(oHF, "first page", xDest)
        
        If bDo Then
            If m_bFirstPage Then
                With oHF.PageNumbers
                    If m_bRestartNumbering Then
                        .RestartNumberingAtSection = True
                        .StartingNumber = m_iRestartAtNumber
                    Else
                        .RestartNumberingAtSection = False
                        .StartingNumber = 1
                    End If
'                    .RestartNumberingAtSection = True
'                    .StartingNumber = 1
                End With

'               insert page number
                InsertPageNumber oHF.Range, vStyle, True
            Else
'               clear header/footer
                With oHF.Range
                    If Len(.Text) > 1 Then
                        .Text = ""
'                        .MoveEnd wdCharacter, -1
'                        .Delete
                    End If
                End With
            End If
        End If

    End If

    Set oWordDoc = Nothing

    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub


'GLOG : 6025 : CEH
Private Sub RemovePageNumberInHeadersFooters(ByVal iSection As Integer)
'inserts page number in headers or footers of specified section
    Const mpThisFunction As String = "MPO.CPageNumberNEW.RemovePageNumberInHeadersFooters"
    Dim oHFs As Word.HeadersFooters
    Dim oHF As Word.HeaderFooter
    Dim oSec As Word.Section
    Dim oWordDoc As CDocument
    Dim xDesc As String
    
    With Word.ActiveDocument.Sections
        On Error Resume Next
        Set oSec = .Item(iSection)
        On Error GoTo ProcError

    End With

    If oSec Is Nothing Then
        xDesc = "Section " & iSection & " does not exist in this document."
        Err.Raise mpError_InvalidMember
    End If

    If m_bFirstPage Then
'   remove only from first page
        If m_bSetupDiffFirstPage Then
            oSec.PageSetup.DifferentFirstPageHeaderFooter = True
        End If
        
    '   do first page header
        Set oHFs = oSec.Headers
        Set oHF = oHFs(wdHeaderFooterFirstPage)
        RemovePageNumber oHF.Range
        
    '   do first page footer
        Set oHFs = oSec.Footers
        Set oHF = oHFs(wdHeaderFooterFirstPage)
        RemovePageNumber oHF.Range
    Else
    '   do headers
        Set oHFs = oSec.Headers
        Set oHF = oHFs(wdHeaderFooterPrimary)
        RemovePageNumber oHF.Range
        
    '   do first page header if necessary
        If oSec.PageSetup.DifferentFirstPageHeaderFooter Then
            Set oHF = oHFs(wdHeaderFooterFirstPage)
            RemovePageNumber oHF.Range
        End If
        
    '   do footers
        Set oHFs = oSec.Footers
        Set oHF = oHFs(wdHeaderFooterPrimary)
        RemovePageNumber oHF.Range
    
    '   do first page footer if necessary
        If oSec.PageSetup.DifferentFirstPageHeaderFooter Then
            Set oHF = oHFs(wdHeaderFooterFirstPage)
            RemovePageNumber oHF.Range
        End If
    
    '   Also do Even page header/footer if appropriate
        If oSec.PageSetup.OddAndEvenPagesHeaderFooter Then
        '   do first page header
            Set oHFs = oSec.Headers
            Set oHF = oHFs(wdHeaderFooterEvenPages)
            RemovePageNumber oHF.Range
            
        '   do first page footer
            Set oHFs = oSec.Footers
            Set oHF = oHFs(wdHeaderFooterEvenPages)
            RemovePageNumber oHF.Range
        End If
    
    End If
    
    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub

Private Function PromptForDeletion(oHF As Word.HeaderFooter, _
                                   ByVal xType As String, _
                                   ByVal xDestination As String) As Boolean

'prompts to delete existing content in headers/footers if necessary -
'sets static var to TRUE to skip subsequent prompts -
'bReset reset static var to FALSE
    Const mpThisFunction As String = "MPO.CPageNumberNEW.PromptForDeletion"
    Dim iChoice As VbMsgBoxResult
    Dim xMsg As String
    Static bSkip As Boolean
    Static bDo As Boolean

    On Error GoTo ProcError

    If m_bResetPrompt Then
        bSkip = False
    End If

    If bSkip Then
'       skip prompt - if there is text in HF,
'       return user choice from previous prompt (bDo),
'       else return TRUE
        If Len(oHF.Range.Text) > 1 Then
            PromptForDeletion = bDo
        Else
            PromptForDeletion = True
        End If
        Exit Function
    End If

    If Len(oHF.Range.Text) > 1 Then
'       prompt only if there's text in the range

        On Error Resume Next
        xMsg = g_oDBs.Lists("PageNumberDeletion").ListItems.Item().DisplayText
        On Error GoTo ProcError
        If xMsg = "" Then
            xMsg = "The section " & oHF.Range.Sections.First.Index & " " & _
                xDestination & " is not empty.  The contents " & _
                "(except for the Trailer/Doc ID) will be deleted if the page number is inserted into this " & _
                xDestination & "." & vbCr & vbCr & "Do you want to insert page " & _
                "numbers when a header or footer is not empty?" & _
                vbCr & vbCr & "Choose No to insert " & _
                "page numbers only in headers and footers that are empty."
        Else
            xMsg = Replace(xMsg, "<Sec>", oHF.Range.Sections.First.Index)
            xMsg = Replace(xMsg, "<HF>", xDestination)
            xMsg = mpBase2.xReplaceTokens(xMsg)
        End If
        iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
'       bDo is static so that future requests
'       can use the value stored here
        bDo = (iChoice = vbYes)
'       set flag to skip prompts in the future
        bSkip = True
        PromptForDeletion = bDo
    Else
        PromptForDeletion = True
    End If
    Exit Function
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Function
End Function

Private Sub InsertPageNumber(oRange As Word.Range, _
                             Optional ByVal vStyle As Variant = "", _
                             Optional bDoAlignment As Boolean)

'inserts page number at specified location-
'applies style if specified-
'applies alignment if specified
    Const mpThisFunction As String = "MPO.CPageNumberNEW.InsertPageNumber"
    Dim xFormat As String
    Dim oStyle As Word.Style
    Dim oTS As Word.TabStops
    Dim oPgSet As Word.PageSetup
    Dim sArea As Single
    Dim xMsg As String
    Dim oRangeField As Word.Range
    Dim oNewFld As Word.Field

    On Error GoTo ProcError

    EchoOff

    With oRange
'       delete range if it's not an insertion point
        If Len(.Text) Then
            If m_iLocation = mpPageNumberLocation_Cursor Then
                .Delete
            Else
                If Len(.Text) > 1 Then
                    .Font.Reset
                    .Text = ""
                End If
            End If
        End If

'       apply style if specified
        If Len(vStyle) Then
            On Error Resume Next
            Set oStyle = Word.ActiveDocument.Styles(vStyle)
            On Error GoTo ProcError

'           alert that style doesn't exist
            If (oStyle Is Nothing) Then
                xMsg = "Could not apply the " & vStyle & " style " & _
                    "to the page number.  The style doesn't exist " & _
                    "in this document."
                MsgBox xMsg, vbExclamation, App.Title
            End If

'           apply styles - both specified and character styles
            .Style = vStyle
        End If

'       apply alignment if specified
        If bDoAlignment Then
'           set tab stops - center and right
            If vStyle <> "" Then
                Set oTS = Word.ActiveDocument.Styles( _
                    vStyle).ParagraphFormat.TabStops
            Else
                Set oTS = Word.ActiveDocument.Styles( _
                    .Style).ParagraphFormat.TabStops
            End If
            
            oTS.ClearAll

            Set oPgSet = oRange.Sections.First.PageSetup

'           get width of area between margins
            With oPgSet
                sArea = .PageWidth - (.RightMargin + .LeftMargin + .Gutter)
            End With
            oTS.Add sArea, wdAlignTabRight
            oTS.Add sArea / 2, wdAlignTabCenter

'           insert tabs
            .InsertAfter vbTab & vbTab
            .Font.Reset
            .StartOf

            If m_iAlign = mpPageNumberAlignment_Center Then
                .Move wdCharacter, 1
            ElseIf m_iAlign = mpPageNumberAlignment_Right Then
                .Move wdCharacter, 2
            End If
        End If

'       reference start of range
        .StartOf

'       insert text before if it exists
        If m_xTextBefore <> Empty Then
            .InsertAfter m_xTextBefore
            .Collapse wdCollapseEnd
        End If

'       insert text after if it exists
        'GLOG 4820: Character style applied to field also gets applied to following whitespace
        'insert temporary Character to ensure style is limited to page number field only
        'If m_xTextAfter <> Empty Then
            .InsertAfter "|" & m_xTextAfter
            .Collapse wdCollapseStart
        'End If

'       insert number based on format
        Select Case m_iNum
'************************ MODIFIED CODE
            Case mpPageNumbers_CardText
                ' Need to set in field because this is not
                ' a native Word format option
                xFormat = "\* CardText \* FirstCap "
            Case Else
                If m_iLocation = mpPageNumberLocation_Cursor Then   '"#3915
'                    insert number based on format
                     Select Case m_iNum
                         Case mpPageNumbers_Arabic
                             xFormat = "\* Arabic "
                         Case mpPageNumbers_LowerRoman
                             xFormat = "\* roman "
                         Case mpPageNumbers_UpperRoman
                             xFormat = "\* ROMAN "
                         Case mpPageNumbers_CardText
                             xFormat = "\* CardText \* FirstCap "
                         Case mpPageNumbers_LowerAlpha
                             xFormat = "\* alphabetic "
                         Case mpPageNumbers_UpperAlpha
                             xFormat = "\* ALPHABETIC "
                     End Select
                Else
                    xFormat = "\* MERGEFORMAT"
                End If
        End Select

'       add page number field
        Dim oFld As Word.Field
        Set oFld = .Fields.Add(oRange, wdFieldPage, xFormat, True)
        oFld.Result.Style = wdStylePageNumber
        
        'get view and selection for later return
        Dim iView As WdViewType
        Dim oSelRng As Word.Range
        
        iView = Word.ActiveDocument.ActiveWindow.View.Type
        Set oSelRng = Word.Selection.Range
        
'       check number text format
        'GLOG : 6025 : CEH
        If m_bXofY Then
            Set oRangeField = oFld.Result
            With oRangeField
                .EndOf
                .InsertAfter " of "
                .EndOf
                Select Case m_iNum
                     Case mpPageNumbers_Arabic
                         xFormat = "\* Arabic "
                     Case mpPageNumbers_LowerRoman
                         xFormat = "\* roman "
                     Case mpPageNumbers_UpperRoman
                         xFormat = "\* ROMAN "
                     Case mpPageNumbers_CardText
                         xFormat = "\* CardText \* FirstCap "
                     Case mpPageNumbers_LowerAlpha
                         xFormat = "\* alphabetic "
                     Case mpPageNumbers_UpperAlpha
                         xFormat = "\* ALPHABETIC "
                 End Select
                Set oNewFld = .Fields.Add(oRangeField, wdFieldNumPages, xFormat, True)
            End With
        End If
        
        'GLOG : 6025 : CEH
        oFld.Select
        'GLOG item #5574 - dcf - 6/29/11
        Selection.Style = wdStylePageNumber  'oRange.Document.Styles(Word.WdBuiltinStyle.wdStylePageNumber).NameLocal
        If Not (oNewFld Is Nothing) Then
            oNewFld.Select
            Selection.Style = wdStylePageNumber
        End If
        
        'GLOG 4820: Delete temporary character following page number
        Selection.Next(wdCharacter, 1).Delete
        oSelRng.Select
        Selection.Document.ActiveWindow.View.Type = iView
        EchoOn
    End With
    Exit Sub
ProcError:
    EchoOn
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub

'GLOG : 6025 : CEH
Private Sub RemovePageNumber(oRange As Word.Range)
'removes page number at specified location-
'leaves preceeding & following tabs in place
    
    Const mpThisFunction As String = "MPO.CPageNumberNEW.RemovePageNumber"
    Dim oField As Word.Field
    Dim oPara As Word.Range
    Dim l As Long
    
    On Error GoTo ProcError

    For Each oField In oRange.Fields
        If oField.Type = wdFieldPage Or _
        oField.Type = wdFieldNumPages Then
            'get field range
            Set oPara = oField.Result
            
            With oPara
            
                'deal with right side
                l = .MoveEndUntil(vbTab)
                If l = 0 Then
                    .MoveEnd wdParagraph
                    .MoveEnd wdCharacter, -1
                End If
                
                'GLOG : 6409 : CEH
                'deal with left side
                l = .MoveStartUntil(vbTab, wdBackward)
                If l = 0 Then
                    .MoveStart wdParagraph, -1
                End If
                
                'delete range
                .Delete
                
            End With
            Exit For
        End If
    Next oField
    
    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub


'**********************************************************
'   Internal Procs
'**********************************************************
Private Sub CheckStyleExists(ByVal vStyle As Variant)
    Const mpThisFunction As String = "MPO.CPageNumberNEW.CheckStyleExists"
    Dim oSty As Word.Style
    On Error Resume Next
    Set oSty = ActiveDocument.Styles(vStyle)
    If oSty Is Nothing Then
        Set oSty = ActiveDocument.Styles.Add(vStyle)
        oSty.BaseStyle = wdStyleNormal 'Word.ActiveDocument.Styles(Word.WdBuiltinStyle.wdStyleNormal).NameLocal
    End If
    Exit Sub
ProcError:
    mdlGlobal.RaiseError mpThisFunction
    Exit Sub
End Sub


