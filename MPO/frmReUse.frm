VERSION 5.00
Begin VB.Form frmReUse 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   2505
   ClientLeft      =   45
   ClientTop       =   240
   ClientWidth     =   4965
   Icon            =   "frmReUse.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2505
   ScaleWidth      =   4965
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.CheckBox chkIncludeSelText 
      Appearance      =   0  'Flat
      Caption         =   "chkIncludeSelText"
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   270
      TabIndex        =   3
      Top             =   1545
      Width           =   4365
   End
   Begin VB.OptionButton optReuse 
      Appearance      =   0  'Flat
      Caption         =   "optCreate"
      ForeColor       =   &H80000008&
      Height          =   340
      Index           =   2
      Left            =   270
      TabIndex        =   2
      Top             =   1005
      Width           =   4365
   End
   Begin VB.OptionButton optReuse 
      Appearance      =   0  'Flat
      Caption         =   "optCreate"
      ForeColor       =   &H80000008&
      Height          =   340
      Index           =   1
      Left            =   270
      TabIndex        =   1
      Top             =   622
      Width           =   4365
   End
   Begin VB.OptionButton optReuse 
      Appearance      =   0  'Flat
      Caption         =   "optCreate"
      ForeColor       =   &H80000008&
      Height          =   340
      Index           =   0
      Left            =   270
      TabIndex        =   0
      Top             =   240
      Value           =   -1  'True
      Width           =   4365
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   380
      Left            =   2775
      TabIndex        =   4
      Top             =   2040
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   380
      Left            =   3855
      TabIndex        =   5
      Top             =   2040
      Width           =   1000
   End
End
Attribute VB_Name = "frmReUse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Enum ReuseOptions
    ReuseOptCtls_Create = 0
    ReuseOptCtls_Replace = 1
    ReuseOptCtls_Append = 2
End Enum
    
Public m_bCancelled As Boolean
Private m_Action As String
Private m_oReuse As MPO.CReUse

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmdFinish_Click()
    Me.Cancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    Dim xType As String
    Dim xTypeDoc As String
    Dim bSel As Boolean
    Dim xAppend As String
    Dim xAppendLHTemplatesInXP As String
    
    Me.Cancelled = True
    Set m_oReuse = New MPO.CReUse
    
'   get document type label
    If UCase(mpBase2.GetMacPacIni("General", "UseDescriptionForReuse")) = "TRUE" Then
        xTypeDoc = g_oTemplates(m_oReuse.Document.Template).Description
    Else
        xTypeDoc = g_oTemplates(m_oReuse.Document.Template).ShortName
    End If
    
'   set label text
    With Me
        .Caption = "Reuse the Current " & xTypeDoc
        .optReuse(ReuseOptCtls_Create).Caption = _
            "Create a &New " & xTypeDoc
        .optReuse(ReuseOptCtls_Replace).Caption = _
            "&Replace the Existing " & xTypeDoc
        .optReuse(ReuseOptCtls_Append).Caption = _
            "&Append a New " & xTypeDoc & " to the Current Document"
        .chkIncludeSelText.Caption = _
            "&Include Selected Text in New " & xTypeDoc
    End With
    
'   is text currently selected?
    bSel = (Word.Selection.Type <> wdSelectionIP) And _
           (g_oTemplates(m_oReuse.Document.Template).ProtectedSections = "")
    
'   enable control if text is selected
    With Me.chkIncludeSelText
        .Enabled = bSel
        .Value = Abs(bSel)
    End With
    
'   diable ReuseAppend in Business documents
    If UCase(m_oReuse.Document.GetClassName(m_oReuse.Document.Template)) = "CBUSINESS" Then     '#3807 - 9.6.2
        Me.optReuse(ReuseOptCtls_Append).Enabled = False
    End If

'   display append option if specified
    xAppend = GetMacPacIni("General", "AllowReuseAppend")
    xAppendLHTemplatesInXP = GetMacPacIni("General", "AllowReuseAppendXPLetterheadTemplates")
    If Len(xAppend) Then
        If UCase(xAppend) = "FALSE" Or _
           ActiveDocument.Bookmarks.Exists("zzmpFIXED_LHFirstPage") And _
           Val(Application.Version) = 10 And _
           UCase(xAppendLHTemplatesInXP) = "FALSE" Then
'           hide radio btn
                Me.optReuse(ReuseOptCtls_Append).Visible = _
                    False
    '           move controls up
                Me.chkIncludeSelText.Top = _
                    Me.optReuse(ReuseOptCtls_Append).Top + 100
                Me.btnCancel.Top = Me.btnCancel.Top - 300
                Me.cmdFinish.Top = Me.cmdFinish.Top - 300
    '           resize dlg
                Me.Height = Me.Height - 300
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmReUse = Nothing
End Sub

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Private Sub optReuse_DblClick(Index As Integer)
    cmdFinish_Click
End Sub
