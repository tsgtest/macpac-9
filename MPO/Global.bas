Attribute VB_Name = "mdlGlobal"
Option Explicit

Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
Public Declare Function GetFocus Lib "user32" () As Long
Public Declare Function GetWindowTextLength Lib "user32" Alias "GetWindowTextLengthA" (ByVal hwnd As Long) As Long
Public Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString, ByVal cch As Long) As Long
Public Declare Function SetWindowText Lib "user32" Alias "SetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Public Const mpDateFormat_Default = "MM/d/yy"
Public Const mpDateFormat_LongText = "MMMM d, yyyy"
Public Const mpDateFormat_ShortText = "MMM d, yyyy"

'---labels & envelope
Public Enum mpOutPut
    mpOutPut_Printer = 1
    mpOutPut_StartofDocument = 2
    mpOutput_EndOfDocument = 3
    mpOutPut_NewDocument = 4
End Enum

Public Const mpLabel_HeaderDistance As Single = 0.3
Public Const mpLabel_FooterDistance As Single = 0.3

'---files
Public Const mpFirmINI As String = "MacPac.ini"
Public Const mpUserINI As String = "User.ini"

Public g_appCI As mpCI.Application
Public g_oLists As CLists
Public g_oDBs As mpDB.CDatabase
Public g_oContacts As CContacts
Public g_oError As mpError.CError

Public g_bDynamicEditing As Boolean
Public g_bForceItemUpdate As Boolean
Public g_bScreenUpdating As Boolean

Public iUserChoice As Integer
Public xMsg As String
