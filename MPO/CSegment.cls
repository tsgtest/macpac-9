VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSegment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CSegment Class
'   created 6/18/00 by Daniel Fisherman
'   Contains properties and methods that
'   define the CSegment Class - those
'   procedures that implement MacPac document segment
'   functionality - a segment is a part of a document
'**********************************************************
Option Explicit

Enum mpSegmentLocations
    mpSegmentLocations_NewFirstSection = 1
    mpSegmentLocations_NewLastSection = 2
    mpSegmentLocations_NewSectionAtSelection = 3
    mpSegmentLocations_InsertionPoint = 4
    mpSegmentLocations_EndOfDocument = 5
    mpSegmentLocations_StartOfDocument = 6
End Enum
    
Private m_oDoc As MPO.CDocument
Private m_iPos As MPO.mpSegmentLocations
Private m_xName As String
Private m_bSepSec As Boolean
Private m_bUseExistingPageSet As Boolean
Private m_oDef As mpDB.CSegmentDef

Public Property Let TypeID(lNew As Long)
    Dim xDesc As String
    Dim xTemplate
    On Error Resume Next
    xTemplate = Word.ActiveDocument.AttachedTemplate
    Set m_oDef = g_oDBs.SegmentDefs(xTemplate).Item(lNew)
    On Error GoTo ProcError
'   raise error if there is no
'   definition with id = lNew
    If (m_oDef Is Nothing) Then
        xDesc = "Invalid segment type. " & lNew & _
            " is not a valid segment id.  Please " & _
            "check tblSegmentTypes in mpPublic.mdb."
        Err.Raise mpError_InvalidSegmentType
    End If

'   check to see that definition has necessary parameters
    ValidateDef
    Exit Property
ProcError:
    RaiseError "MPO.CSegment.TypeID", xDesc
End Property

Public Property Let UseExistingPageSetup(bNew As Boolean)
    m_bUseExistingPageSet = bNew
End Property

Public Property Get UseExistingPageSetup() As Boolean
    UseExistingPageSetup = m_bUseExistingPageSet
End Property

Public Property Get TypeID() As Long
    TypeID = m_oDef.ID
End Property

Public Property Let Location(iNew As mpSegmentLocations)
    m_iPos = iNew
End Property

Public Property Get Location() As mpSegmentLocations
    If m_iPos = Empty Then
        m_iPos = mpSegmentLocations_EndOfDocument
    End If
    Location = m_iPos
End Property

Public Function Definition() As mpDB.CSegmentDef
    Set Definition = m_oDef
End Function

Private Function ValidateDef() As Boolean
'returns TRUE if Segment definition is
'correctly defined, else FALSE
    Dim oTemplate As mpDB.CTemplateDef
    Dim xDesc As String
    
    With m_oDef
        If Len(.SourceTemplate) Then
'           validate that template is a MacPac template
            On Error Resume Next
            Set oTemplate = g_oDBs.TemplateDefinitions(.SourceTemplate)
            On Error GoTo ProcError
            If (oTemplate Is Nothing) Then
'               invalid segment definition
                xDesc = "Invalid Segment definition. " & .SourceTemplate & _
                    " is not a valid template.  Please check tblSegments " & _
                    "in mpPublic.mdb."
                Err.Raise mpError_InvalidSegmentType
            End If
        Else
'           validate bpfile
            Dim xBP As String
            If Len(.Boilerplate) Then
                xBP = mpBase2.BoilerplateDirectory & "\" & .Boilerplate
                If Dir(xBP) = "" Then
'                   invalid segment definition
                    xDesc = "Invalid Segment definition. '" & xBP & _
                        "' could not be found.  Please check tblSegmentTypes " & _
                        "in mpPublic.mdb."
                    Err.Raise mpError_InvalidSegmentType
                End If
            Else
                xDesc = "Invalid Segment definition. Either a boilerplate " & _
                    "or a template must be specified for each record in " & _
                    "tblSegmentTypes in mpPublic.mdb."
                Err.Raise mpError_InvalidSegmentType
            End If
        End If
    End With
    ValidateDef = True
    Exit Function
ProcError:
    RaiseError "MPO.CSegment.ValidateDef", xDesc
End Function

Public Function Insert() As Boolean
'add new section for reuse document and
'back up doc vars in case user cancels at some point

    On Error GoTo ProcError
    
    With m_oDoc

'       set base style if it exists
        If m_oDef.BaseStyle <> Empty Then
            Word.ActiveDocument.Variables("BaseStyle") = m_oDef.BaseStyle
        End If

        If Len(m_oDef.SourceTemplate) Then
'           segment is a template type of segment
            InsertFromTemplate
        Else
'           insert boilerplate at location
            InsertBoilerplate m_oDef.Boilerplate
        End If
    End With
    
    ' Check for bookmark marking start of Segment Body Text
    Dim xBT As String
    xBT = "zzmpFIXED_SegmentBodyStart"
    If bIniToBoolean(GetMacPacIni("General", "HideFixedMacPacBookmarks")) Then
        xBT = "_" & xBT
    End If
    If ActiveDocument.Bookmarks.Exists(xBT) Then
        m_oDoc.SelectStartPosition , xBT, , False
    End If
    

'   every other auto text insertion unnames list templates'*c
    Dim oNum As CNumbering '*c
    Set oNum = New CNumbering '*c
    oNum.RenameLTsFromVars '*c
    
    Insert = True
    Exit Function
ProcError:
    EchoOn
    Word.Application.ScreenUpdating = True
    RaiseError "MPO.CSegment.Insert"
    Exit Function
End Function

Public Sub InsertBoilerplate(ByVal xBoilerplate As String)
'inserts a document segment into a document by
'dumping in the appropriate boilerplate
    Dim oLoc As Word.Range
    Dim oDoc As Word.Document
    Dim bSaved As Boolean
    Dim oRng As Word.Range
    Dim oMainDoc As Word.Document
    Dim PS As PageSetup
    Dim cnt As Integer
    Dim oBmk As Word.Bookmark
    
    Set oMainDoc = ActiveDocument
    
    bSaved = Word.NormalTemplate.Saved
    EchoOff
    Set oDoc = Word.Documents.Open(BoilerplateDirectory() & _
        Me.Definition.Boilerplate, , True, False)
    EchoOff
    
'   get whether to protect or not
    Dim bProtect As Boolean
    bProtect = Word.ActiveDocument.ProtectionType <> wdNoProtection
    
    If bProtect Then
'       unprotect temporarily - we'll close without saving
        oDoc.UnProtect
    End If
    
    oDoc.ActiveWindow.Caption = "Insert Boilerplate"
    
    If Me.Location = mpSegmentLocations_NewFirstSection Or _
        Me.Location = mpSegmentLocations_NewLastSection Or _
        Me.Location = mpSegmentLocations_NewSectionAtSelection Then
        Set oRng = Word.ActiveDocument.Content
        With oRng
            .InsertParagraphAfter
            .Paragraphs.Last.Style = wdStyleNormal
            .EndOf
            .InsertBreak wdSectionBreakNextPage
        End With
    End If
    
'   store names of list template
'   they may need to be restored after auto text insertion
    Dim oNum As CNumbering
    Set oNum = New CNumbering
    oNum.BackupLTNames oDoc, oMainDoc
    
'   create autotext entry of doc
    Word.NormalTemplate.AutoTextEntries.Add _
        "zzmpTemp", Word.ActiveDocument.Content
    
    oDoc.Saved = True
    oDoc.Close wdDoNotSaveChanges
    EchoOn
    Word.Application.ScreenRefresh
    
    
    Select Case Me.Location
        Case MPO.mpSegmentLocations_InsertionPoint
            Set oLoc = Word.Selection.Range
        Case MPO.mpSegmentLocations_EndOfDocument
            Set oLoc = Word.ActiveDocument.Content
            oLoc.EndOf
        Case MPO.mpSegmentLocations_StartOfDocument
            Set oLoc = Word.ActiveDocument.Content
            oLoc.StartOf
        Case MPO.mpSegmentLocations_NewFirstSection
            Set oLoc = Word.ActiveDocument.Content
            oLoc.StartOf
'           this is to avoid using the information method of a range object
            On Error Resume Next
            cnt = oLoc.Cells.Count
'           On Error GoTo ProcError
            If cnt > 0 Then
'               ensure that segment will not get inserted
'               in a table cell - add para before
                oLoc.Select
                
'               bookmark this location for deletion below
                Set oBmk = Word.ActiveDocument.Bookmarks.Add( _
                    "zzmpBmkTemp", oLoc)
                    
                Selection.SplitTable
            End If

        Case MPO.mpSegmentLocations_NewLastSection
            
            Set oLoc = Word.ActiveDocument.Content
            oLoc.EndOf
            oLoc.InsertBreak wdSectionBreakNextPage
            
        Case mpSegmentLocations_NewSectionAtSelection
            Set oLoc = Word.Selection.Range
            With oLoc
                .InsertBreak wdSectionBreakNextPage
'                .InsertBreak wdSectionBreakContinuous '*c NEW
                '---9.6.2 - only unlink if UseExistingPageSetup is false
                If Not Me.UseExistingPageSetup Then
                    With .Sections(1)
                        .Headers(wdHeaderFooterPrimary).LinkToPrevious = False
                        .Headers(wdHeaderFooterFirstPage).LinkToPrevious = False
                        .Footers(wdHeaderFooterPrimary).LinkToPrevious = False
                        .Footers(wdHeaderFooterFirstPage).LinkToPrevious = False

                    End With
                End If
            End With
    End Select
    
    EchoOff
    Word.Application.ScreenUpdating = False
    
'   insert new document segment autotext entry
    With Word.NormalTemplate.AutoTextEntries
        .Item("zzmpTemp").Insert oLoc, True
        .Item("zzmpTemp").Delete
    End With
    
    If Not oBmk Is Nothing Then
    '   extraneous paras exist - delete them
        Set oRng = oBmk.Range
        With oRng
            .Move wdParagraph, -1
            .Paragraphs(1).Range.Delete
            .Move wdParagraph, -1
            .MoveStartUntil Chr(12), -1
            .Delete
        End With
        oBmk.Delete
    End If
'   remove trailing extraneous section
    Select Case Me.Location
        Case MPO.mpSegmentLocations_NewLastSection
            Set oLoc = Word.ActiveDocument.Content
            oLoc.EndOf
            oLoc.Move wdCharacter, -2
            oLoc.MoveEnd wdStory
            
            '---9.6.2
            If Me.UseExistingPageSetup Then
                With oLoc.Sections.First
        
                    Set PS = ActiveDocument.Sections(.Index - 1).PageSetup
                    With .PageSetup
                        .TopMargin = PS.TopMargin
                        .BottomMargin = PS.BottomMargin
                        .LeftMargin = PS.LeftMargin
                        .RightMargin = PS.RightMargin
                        .HeaderDistance = PS.HeaderDistance
                        .FooterDistance = PS.FooterDistance
                        .DifferentFirstPageHeaderFooter = PS.DifferentFirstPageHeaderFooter
                    End With
        
                    .Headers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Headers(wdHeaderFooterFirstPage).LinkToPrevious = False
                    .Footers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Footers(wdHeaderFooterFirstPage).LinkToPrevious = False
               End With
            
            Else
                With Word.ActiveDocument.Sections.Last
                    .Headers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Headers(wdHeaderFooterFirstPage).LinkToPrevious = False
                    .Footers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Footers(wdHeaderFooterFirstPage).LinkToPrevious = False
                End With
                m_oDoc.ClearFooters ActiveDocument.Sections.Last
                m_oDoc.ClearHeaders ActiveDocument.Sections.Last
            
            End If
           
            oLoc.Delete
        Case mpSegmentLocations_NewSectionAtSelection
            '---9.6.2
            If Me.UseExistingPageSetup Then
                With oLoc.Sections.First
                    Set PS = ActiveDocument.Sections(.Index - 1).PageSetup
                    With .PageSetup
                        .TopMargin = PS.TopMargin
                        .BottomMargin = PS.BottomMargin
                        .LeftMargin = PS.LeftMargin
                        .RightMargin = PS.RightMargin
                        .HeaderDistance = PS.HeaderDistance
                        .FooterDistance = PS.FooterDistance
                        .DifferentFirstPageHeaderFooter = PS.DifferentFirstPageHeaderFooter
                    End With
                    
                    .Headers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Headers(wdHeaderFooterFirstPage).LinkToPrevious = False
                    .Footers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Footers(wdHeaderFooterFirstPage).LinkToPrevious = False
        
                End With
            
            Else
                With oLoc.Sections.First
                    .Headers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Headers(wdHeaderFooterFirstPage).LinkToPrevious = False
                    .Footers(wdHeaderFooterPrimary).LinkToPrevious = False
                    .Footers(wdHeaderFooterFirstPage).LinkToPrevious = False
                End With
                
                m_oDoc.ClearFooters oLoc.Sections.First
                m_oDoc.ClearHeaders oLoc.Sections.Last
            
            End If
    End Select
    
    '9.7.1 - #3355
    m_oDoc.ResetDocumentBreaks
    
    Word.NormalTemplate.Saved = bSaved
    oLoc.Select
    Word.ActiveDocument.ActiveWindow.LargeScroll 1
    oLoc.Select
    If Word.ActiveDocument.ProtectionType = wdNoProtection And bProtect Then
        Word.ActiveDocument.Protect wdAllowOnlyFormFields
    End If
    
    EchoOn
    
End Sub

Public Sub InsertFromTemplate()
'inserts a document segment into a document by
'running the creation macro of a template
    Dim oLocation As Word.Range
    Dim oLoc As Word.Range
    Dim oDestDoc As Word.Document
    Dim oTempDoc As Word.Document
    Dim xDestDoc As String
    Dim xTempDoc As String
    Dim xMacro As String
    Dim bSaved As Boolean
    Dim oEOF As Word.Range
    Dim cnt As Integer
    Dim oTemplate As mpDB.CTemplateDef
    Dim oSourceTemplate As MPO.CTemplate
    Dim oBmk As Word.Bookmark
    Dim oRng As Word.Range
    Dim oSty As Word.Style
    Dim xClassName As String
    
    On Error GoTo ProcError
    
'   get template
    Set oTemplate = g_oDBs.TemplateDefinitions(m_oDef.SourceTemplate)
    'GLOG : 5052 : CEH
    On Error Resume Next
    Set oSourceTemplate = g_oTemplates.Item(ActiveDocument.AttachedTemplate)
    On Error GoTo ProcError
    
    If (oSourceTemplate Is Nothing) Then
        Set oSourceTemplate = g_oDBs.TemplateDefinitions(Word.NormalTemplate.Name)
    End If

'   get name of macro to run
    xMacro = oTemplate.Macro
        
    If Len(xMacro) Then
'       there is a macro
        Set oDestDoc = Word.ActiveDocument
        xDestDoc = WordBasic.FileName$()
        
'       create a new document based on template
        Set oTempDoc = Word.Documents.Add(g_oTemplates.Item(oTemplate.FileName).FullName) '9.7.1 #4055

'        Set oTempDoc = Word.Documents.Add( _
             TemplatesDirectory() & m_oDef.SourceTemplate)
        xTempDoc = WordBasic.FileName$()
        
'******************************************
'GET MAPPED VARS HERE
'******************************************
        m_oDoc.CopyDocVars "", xDestDoc, xTempDoc
        'GLOG : 5052 : CEH
        On Error Resume Next
        xClassName = oSourceTemplate.ClassName '*c
        On Error GoTo ProcError
        
        If (Not g_oTemplates(Word.ActiveDocument.AttachedTemplate).CreateTemplateVars _
        (ActiveDocument.AttachedTemplate, xClassName)) And _
        xClassName <> oTemplate.ClassName Then '9.7.1 - #3913
           On Error Resume Next
           m_oDoc.File.Variables("ReuseAuthor").Delete
           On Error GoTo ProcError
        End If
        
'       add var that indicates that this is a doc segment-
'       used by CDocument.IsCreated- if var is TRUE, is
'       created will be false
        'GLOG : 5052 : CEH
'       not used any longer
'        Word.ActiveDocument.Variables("IsSegment") = True
        'GLOG : 5052 : CEH
        'set for cover page only
        If UCase(oTemplate.ClassName) = "CPLEADINGCOVERPAGE" Then
            Word.ActiveDocument.Variables("IsLitBackSegment") = True
        End If

'       add var that indicates where the segment was inserted
        m_oDoc.SetVar "SegmentLocation", Me.Location
'       run creation macro

        On Error Resume Next
        Err.Clear
        Word.Application.Run xMacro
        
        If Err.Number Then
            Err.Raise Err, , _
                "Could not run the macro '" & xMacro & "'."
        End If
        
        Word.ActiveDocument.UnProtect

        If Not g_bLastCreateCancelled Then

'           get BaseStyle if any
            Dim xBase As String
            xBase = Word.ActiveDocument.Variables("BaseStyle")

'           copy over - create temp autotext, then insert
            Word.Application.ScreenUpdating = False
            EchoOff
            
'           if location <= 3, document will go into
'           separate section- setup section parameters
            If (Me.Location <= 3) And (Not Me.UseExistingPageSetup) Then
'               insert section breaks after document - this
'               forces Word to include section data in the
'               autotext entry created below
                Set oEOF = Word.ActiveDocument.Content
                With oEOF
                    .EndOf
                    .InsertBreak wdSectionBreakNextPage
                    m_oDoc.UnlinkHeadersFooters Word.ActiveDocument.Sections.Last
'                   reference break for later deletion
                    .MoveStart wdCharacter, -1
                    .Bookmarks.Add "zzmpTempChr12"
                End With
            End If
            
            With Word.NormalTemplate
                bSaved = .Saved
                With .AutoTextEntries
'                   create autotext entry of doc
                    .Add "zzmpTemp", Word.ActiveDocument.Content
                
                    m_oDoc.CopyDocVars , xTempDoc, xDestDoc, , True
                    With Word.ActiveDocument
                        .Saved = True
                        .Close wdDoNotSaveChanges
                    End With
                    
                    Select Case Me.Location
                        Case MPO.mpSegmentLocations_InsertionPoint
                            Set oLocation = Word.Selection.Range
                        Case MPO.mpSegmentLocations_EndOfDocument
                            Set oLocation = Word.ActiveDocument.Content
                            With oLocation
                                .EndOf
'***************************9.3.1/CEH/2129*******************************
                                If .Paragraphs.Last.Range.Characters.Count > 1 Then
                                    .InsertParagraphAfter
                                    .EndOf
                                End If
                            End With
'************************************************************************
                        Case MPO.mpSegmentLocations_StartOfDocument
                            Set oLocation = Word.ActiveDocument.Content
                            oLocation.StartOf
'                           this is to avoid using the information method of a range object
                            On Error Resume Next
                            cnt = oLocation.Cells.Count
'                            On Error GoTo ProcError
                            If cnt > 0 Then
'                               ensure that segment will not get inserted
'                               in a table cell - add para before
                                oLocation.Select
                                
'                               bookmark this location for deletion below
                                Set oBmk = Word.ActiveDocument.Bookmarks.Add( _
                                    "zzmpBmkTemp", oLocation)
                                    
                                Selection.SplitTable
                                

                            End If
                        Case MPO.mpSegmentLocations_NewFirstSection
                            Set oLocation = Word.ActiveDocument.Content
                            oLocation.StartOf
'                           this is to avoid using the information method of a range object
                            On Error Resume Next
                            cnt = oLocation.Cells.Count
'                            On Error GoTo ProcError
                            If cnt > 0 Then
'                               ensure that segment will not get inserted
'                               in a table cell - add para before
                                oLocation.Select
                                
'                               bookmark this location for deletion below
                                Set oBmk = Word.ActiveDocument.Bookmarks.Add( _
                                    "zzmpBmkTemp", oLocation)
                                    
                                Selection.SplitTable
                                

                            End If
'                           cycle through headers and footers - dirty them, otherwise
'                           headers of source document always replace headers of this section
                            Dim oHF As Word.HeaderFooter
                            For Each oHF In Word.ActiveDocument.Sections(1).Headers
                                If Len(oHF.Range) = 1 Then
                                    oHF.Range.InsertBefore "d"
                                    oHF.Range.Characters.First.Delete
                                End If
                            Next oHF
                            
                            For Each oHF In Word.ActiveDocument.Sections(1).Footers
                                If Len(oHF.Range) = 1 Then
                                    oHF.Range.InsertBefore "d"
                                    oHF.Range.Characters.First.Delete
                                End If
                            Next oHF
                            
                            m_oDoc.UnlinkHeadersFooters Word.ActiveDocument.Sections(1)
                           
                        Case MPO.mpSegmentLocations_NewLastSection
                            Set oLocation = Word.ActiveDocument.Content
                            oLocation.EndOf
                            oLocation.InsertBreak wdSectionBreakNextPage
                        Case mpSegmentLocations_NewSectionAtSelection
                            Set oLocation = Word.Selection.Range
                            With oLocation
                                oLocation.InsertBreak wdSectionBreakNextPage
'                                oLocation.InsertBreak wdSectionBreakContinuous '*c NEW
                                m_oDoc.UnlinkHeadersFooters .Sections(1)
                            End With
                    End Select
                    
'                   insert new document segment autotext entry
                    .Item("zzmpTemp").Insert oLocation, True
                
'CharlieCore 9.2.0 - BaseStyle
'                   Make sure BaseStyle has target document's Normal
'                   font name and size props
                    If xBase <> "" Then
                        With m_oDoc.File.Styles
                            .Item(xBase).Font.Name = .Item(wdStyleNormal).Font.Name
                            .Item(xBase).Font.Size = .Item(wdStyleNormal).Font.Size
                        End With
                    End If
                    
                    '9.7.1 - #4288
'                   sink LineSpacing if styles exist - separate statement segment
                    '---synch line spacing props to Normal.
                    On Error Resume Next
                    Set oSty = ActiveDocument.Styles("SepStmtCol1Text")
                    If Not (oSty Is Nothing) Then
                        With ActiveDocument.Styles
                            oSty.ParagraphFormat.LineSpacingRule = _
                                        .Item(wdStyleNormal).ParagraphFormat.LineSpacingRule
                            oSty.ParagraphFormat.LineSpacing = _
                                        .Item(wdStyleNormal).ParagraphFormat.LineSpacing
                        End With
                    End If
                    
                    'GLOG : 5344 : ceh
'                   add space before to title style definition if necessary
                    On Error Resume Next
                    Set oSty = ActiveDocument.Styles("SepStmtTitle")
                    If Not (oSty Is Nothing) Then
                        With ActiveDocument.Styles
                            If ActiveDocument.Styles(wdStyleNormal).ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
                                oSty.ParagraphFormat.SpaceBefore = 12
                            Else
                                oSty.ParagraphFormat.SpaceBefore = 0
                            End If
                        End With
                    End If
        
                    If Not oBmk Is Nothing Then
                    '   extraneous paras exist - delete them'
                        Set oRng = oBmk.Range
                        With oRng
                            .Move wdParagraph, -1
                            .Paragraphs(1).Range.Delete
                            .Move wdParagraph, -1
                            .MoveStartUntil Chr(12), -1
                            .Delete
                        End With
                        oBmk.Delete
                    End If

'                   delete autotext entries
                   .Item("zzmpTemp").Delete
                End With
            End With
            
            Word.NormalTemplate.Saved = bSaved
            
'           remove trailing extraneous section
            Select Case Me.Location
                Case MPO.mpSegmentLocations_NewLastSection
                    Set oLoc = Word.ActiveDocument.Content
                    oLoc.EndOf
                    oLoc.Move wdCharacter, -2
                    oLoc.MoveEnd wdStory
                    
                    m_oDoc.UnlinkHeadersFooters Word.ActiveDocument.Sections.Last
                    oLoc.Delete
                Case mpSegmentLocations_NewSectionAtSelection
            End Select
            
            '9.7.1 - #3355
            m_oDoc.ResetDocumentBreaks
            
'           protect doc if specified
            If oTemplate.ProtectedSections <> Empty Then
                If Word.ActiveDocument.ProtectionType = wdNoProtection Then
                    Word.ActiveDocument.Protect wdAllowOnlyFormFields
                End If
            End If
                
            EchoOn
            Word.Application.ScreenUpdating = True
        Else
            Word.ActiveDocument.Saved = True
            Word.ActiveDocument.Close wdDoNotSaveChanges
        End If
    End If
    
'   force selection to appear at top of screen
    If Not (oLocation Is Nothing) Then
        oLocation.Select
    End If
    Word.ActiveDocument.ActiveWindow.LargeScroll 1
    If Not (oLocation Is Nothing) Then
        oLocation.Select
    End If
    Exit Sub
ProcError:
    EchoOn
    Word.Application.ScreenUpdating = True
    RaiseError "MPO.CSegment.InsertFromTemplate"
End Sub

Private Function CreateSection() As Integer
'creates a section at the specified location
    With ActiveDocument
        Select Case Me.Location
            Case mpSegmentLocations_NewFirstSection
                    .Characters.First.InsertBreak wdSectionBreakNextPage
                    .Characters.First.InsertParagraphBefore
                    m_oDoc.UnlinkHeadersFooters .Sections(2)
                    m_oDoc.ClearHeaders .Sections.First
                    m_oDoc.ClearFooters .Sections.First
                    CreateSection = 1
            Case mpSegmentLocations_NewLastSection
                    .Characters.Last.InsertBreak wdSectionBreakNextPage
                    m_oDoc.UnlinkHeadersFooters .Sections.Last
                    m_oDoc.ClearHeaders .Sections.Last
                    m_oDoc.ClearFooters .Sections.Last
                    CreateSection = .Sections.Last.Index
            Case mpSegmentLocations_NewSectionAtSelection
                    Selection.InsertBreak wdSectionBreakNextPage
'                    Selection.InsertBreak wdSectionBreakContinuous '*c NEW
            Case Else
        End Select
    End With
    
End Function

Private Sub CreateSegmentVars(oSourceDoc As Word.Document, oDestDoc As Word.Document)
'creates variables for the source template in the temp doc -
'creates those variables specified in tblSegmentMappings, and
'fills them with the specified values from the destination doc
    Dim aVars() As String
    Dim xDesc As String
    Dim xDum As String
    Dim i As Integer
    Dim oSourceVar As Word.Variable
    Dim oDestVar As Word.Variable
    
    aVars = Me.Definition.MappedVariables
    
    If aVars(0, 0) = "" Then
'       there are no vars
        Exit Sub
    End If
    
    For i = 0 To UBound(aVars, 1)
        On Error Resume Next
        Set oDestVar = oDestDoc.Variables(aVars(i, 1))
        xDum = oDestVar.Value
        If Err = 0 Then
            On Error GoTo ProcError
            oSourceDoc.Variables(aVars(i, 0)).Value = _
                oDestVar.Value
        Else
            On Error GoTo ProcError
        End If
    Next i
    Exit Sub
ProcError:
    RaiseError "MPO.CSegment.CreateSegmentVars", xDesc
End Sub

'**********************************************************
'   Class Events
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
End Sub

Private Sub Class_Terminate()
    Set m_oDoc = Nothing
End Sub
