Attribute VB_Name = "mdlLaunchApps"
Option Explicit
Public Declare Function ShellExecute Lib "shell32.dll" _
    Alias "ShellExecuteA" (ByVal hwnd As Long, _
    ByVal lpOperation As String, ByVal lpFile As String, _
    ByVal lpParameters As String, ByVal lpDirectory As String, _
    ByVal nShowCmd As Long) As Long

Private Const SW_SHOWNORMAL = 1
Public Sub CreateExcelDocument(xFileName As String)
'Create new Workbook based on an Excel Template
    Dim oApp As Object
    On Error Resume Next
    Set oApp = GetObject(, "Excel.Application")
    On Error GoTo ProcError
    If oApp Is Nothing Then
        Set oApp = CreateObject("Excel.Application")
        oApp.Visible = True
    End If
    oApp.Workbooks.Add xFileName
    oApp.ActiveWindow.Activate
    
    Exit Sub
ProcError:
    Select Case Err.Number
        Case 429
            RaiseError "MPO.mdlLaunchApps.CreateExcelDocument", "Excel is not installed.  " & _
            "The document could not be created."
        Case Else
            RaiseError "MPO.mdlLaunchApps.CreateExcelDocument"
        End Select
    Exit Sub
End Sub
Public Sub CreatePowerpointDocument(xFileName As String)
' Create new Presentation based on a PowerPoint template
    Dim oApp As Object
    On Error Resume Next
    Set oApp = GetObject(, "Powerpoint.Application")
    On Error GoTo ProcError
    If oApp Is Nothing Then
        Set oApp = CreateObject("Powerpoint.Application")
        oApp.Visible = True
    End If
    oApp.Presentations.Open xFileName, , True
    oApp.Activate
    
    Exit Sub
ProcError:
    Select Case Err.Number
        Case 429
            RaiseError "MPO.mdlLaunchApps.CreateExcelDocument", "PowerPoint is not installed.  " & _
            "The document could not be opened."
        Case Else
            RaiseError "MPO.mdlLaunchApps.CreateExcelDocument"
        End Select
    Exit Sub
End Sub
Public Sub LaunchDocumentByExtension(xFileName As String)
    Dim iRet As Integer
    On Error GoTo ProcError
    
    iRet = ShellExecute(0, "open", xFileName, "", "", SW_SHOWNORMAL)
    If iRet <= 32 Then
        MsgBox "The document could not be opened.  " & _
            "There may not be an installed application associated with this extension.", vbExclamation
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.mdlLaunchApps.LaunchDocumentByExtension"
End Sub

