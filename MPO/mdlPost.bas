Attribute VB_Name = "mdlPost"
Option Explicit
Private lRet As Long

Public Function lCallPost() As Long
    Dim xMsg As String
    Dim bDoPost As Boolean
    Dim xNetDir As String
    Dim xHomeDir As String
    Dim xHomeVar As String
    Dim xLocalDir As String
    Dim xSourceDir As String
    Dim iUser As Integer
    Dim xDestDir As String
       
    On Error GoTo ProcError
    
'   determine if files should be posted
    bDoPost = CBool(GetMacPacIni("General", "Post"))
                                          
    If bDoPost Then
'       get source dir
        xSourceDir = xTrimTrailingChrs(mpBase2.UserFilesDirectory, "\")
    
'       get dest dir
        xNetDir = GetMacPacIni("General", "PostDir")
        
        xHomeVar = GetMacPacIni("General", "HomePathVariable")
        
        If xHomeVar <> "" Then
            xHomeDir = Environ(xHomeVar)
            If Right(xNetDir, 1) <> "\" And Left(xHomeDir, 1) <> "\" Then
                xHomeDir = "\" & xHomeDir
            ElseIf Right(xNetDir, 1) = "\" And Left(xHomeDir, 1) = "\" Then
                xHomeDir = Mid(xHomeDir, 2)
            End If
            
            xNetDir = xNetDir & xHomeDir & "\MacPac"
        End If
            
        xLocalDir = xSourceDir & "_zzmpTemp"

'       check for dest dir availability
        On Error Resume Next
        If Dir(xNetDir & "\*.*") = "" Then
           
            If Err.Number = 68 Then 'device not available
'               prompt to copy locally
                xMsg = "The network directory on which your " & _
                       "personal settings are stored is currently " & _
                       "unavailable.  Settings will be stored locally " & _
                       "until the network drive becomes functional."
                MsgBox xMsg, vbExclamation, "The Legal MacPac"

'               post to local personal dir as files -
'               will err if dir exists
                xDestDir = xLocalDir
                MkDir xLocalDir
                On Error GoTo ProcError
                lRet = lPostToDir(xSourceDir, xLocalDir)
            Else
                '---check if the directory is there
                xDestDir = xNetDir
                MkDir xNetDir
                If Err.Number = 75 Or Err.Number = 0 Then
                '---directory exists, you can post!
                    lRet = lPostToDir(xSourceDir, xNetDir)
                Else
                    xMsg = "MacPac cannot backup your normal.dot" & _
                            " and default option settings to the network" & _
                            " at this time.  You may lose any changes" & _
                            " to your normal and default options made" & _
                            " during this Word session only." & _
                            "  Please contact your System Administrator if this is a problem."
                            
    '               not available, warn and do...
                    MsgBox xMsg, vbInformation, "The Legal MacPac"
                End If
            End If
        Else
'           post
            xDestDir = xNetDir
            lRet = lPostToDir(xSourceDir, xNetDir)
        End If
'       check for success
        If lRet <> 0 Then
'           warn user
            xMsg = "MacPac cannot backup all of your default option settings to " & _
            xDestDir & " at this time.  You may lose any changes to your " & _
            "normal template and default options made during this Word session only.  " & _
            "Please contact your Systems Administrator if this is a problem."
            MsgBox xMsg, vbInformation, "The Legal MacPac"
        End If
    End If
    lCallPost = lRet
    Exit Function
ProcError:
    RaiseError "MPO.mdlPost.lCallPost"
    Exit Function
End Function

Function lPostToDir(xSourceDir As String, _
                    xDestDir As String, _
                    Optional bCreateTempPersonal As Boolean = False) As Long
    
    Dim xFile As String
    Dim docNormal As Word.Document
    Dim xMacPacFiles() As String
    Dim bCopyMacPacOnly As Boolean
    Dim lNumMacPacFiles As Long
    Dim CopyError As Long
    Dim i As Integer
    
    bCopyMacPacOnly = bIniToBoolean(GetMacPacIni("General", "PostMacPacOnly"))
    
    On Error Resume Next
    
    If bCopyMacPacOnly Then
        lNumMacPacFiles = GetMacPacIni("Backup Files", "FileCount")
        ReDim xMacPacFiles(lNumMacPacFiles - 1)
        For i = 1 To lNumMacPacFiles
            xMacPacFiles(i - 1) = GetMacPacIni("Backup Files", "File" & i)
        Next i
    Else
        ReDim xMacPacFiles(0)
    End If
    
    Application.StatusBar = "Saving Personal Settings. Please wait..."
    
    DoEvents
    
    g_oDBs.PrivateDB.Close
    g_oDBs.PublicDB.Close
    
    DoEvents

'   enumerate personal files
    If bCopyMacPacOnly Then
        For i = 0 To lNumMacPacFiles - 1
            xFile = Dir(xSourceDir & "\" & xMacPacFiles(i))
            While xFile <> ""
                If UCase(xFile) = "NORMAL.DOT" Then
                    On Error GoTo SkipNormal
                    EchoOff
                    Set docNormal = Application.NormalTemplate.OpenAsDocument()
                    docNormal.SaveAs xDestDir & "\" & xFile
                    docNormal.Close wdDoNotSaveChanges
SkipNormal:
                    On Error Resume Next
                    If Err.Number <> 0 Then _
                        CopyError = Err.Number
                    EchoOn
                Else
                    FileCopy xSourceDir & "\" & xFile, _
                             xDestDir & "\" & xFile
                    If Err.Number <> 0 Then _
                        CopyError = Err.Number
                End If
                xFile = Dir()
            Wend
        Next i
    Else
        xFile = Dir(xSourceDir & "\*.*")
    
        While xFile <> ""
            If UCase(xFile) = "NORMAL.DOT" Then
                On Error GoTo SkipNormal2
                EchoOff
                Set docNormal = Application.NormalTemplate.OpenAsDocument()
                docNormal.SaveAs xDestDir & "\" & xFile
                docNormal.Close wdDoNotSaveChanges
SkipNormal2:
                On Error Resume Next
                If Err.Number <> 0 Then _
                    CopyError = Err.Number
                EchoOn
            Else
                FileCopy xSourceDir & "\" & xFile, _
                         xDestDir & "\" & xFile
                If Err.Number <> 0 Then _
                    CopyError = Err.Number
            End If
            xFile = Dir()
        Wend
    End If
    lPostToDir = CopyError
    EchoOn
    Application.StatusBar = ""
    
End Function



