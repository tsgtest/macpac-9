VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmInsertSegment 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert Document"
   ClientHeight    =   4935
   ClientLeft      =   45
   ClientTop       =   240
   ClientWidth     =   5205
   Icon            =   "frmInsertSegment.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   5205
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkUseExistingPageSetup 
      Appearance      =   0  'Flat
      Caption         =   "&Use Existing Page Setup (Headers/Footers/Margins...)"
      ForeColor       =   &H80000008&
      Height          =   345
      Left            =   150
      TabIndex        =   4
      ToolTipText     =   "If checked, the inserted document will use the page setup values of the previous section in the document"
      Top             =   3930
      Width           =   4920
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4020
      TabIndex        =   6
      Top             =   4395
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   2925
      TabIndex        =   5
      Top             =   4395
      Width           =   1000
   End
   Begin MSComctlLib.ListView lstItems 
      Height          =   2925
      Left            =   105
      TabIndex        =   1
      Top             =   270
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   5159
      Arrange         =   2
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      PictureAlignment=   4
      _Version        =   393217
      Icons           =   "ilNormal"
      SmallIcons      =   "ilSmall"
      ForeColor       =   -2147483640
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin TrueDBList60.TDBCombo cmbLocation 
      Height          =   600
      Left            =   105
      OleObjectBlob   =   "frmInsertSegment.frx":058A
      TabIndex        =   3
      Top             =   3510
      Width           =   4965
   End
   Begin MSComctlLib.ImageList ilNormal 
      Left            =   135
      Top             =   4290
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmInsertSegment.frx":27A5
            Key             =   "Normal Document"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblLocation 
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Location:"
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   150
      TabIndex        =   2
      Top             =   3300
      Width           =   1455
   End
   Begin VB.Label lblTemplates 
      BackStyle       =   0  'Transparent
      Caption         =   "Documents:"
      Height          =   255
      Left            =   135
      TabIndex        =   0
      Top             =   45
      Width           =   915
   End
End
Attribute VB_Name = "frmInsertSegment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private m_bCancelled As Boolean
Private m_bItemClicked As Boolean
Private m_oDefs As mpDB.CSegmentDefs

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Get SelectedItem() As Long
    SelectedItem = Mid(Me.lstItems.SelectedItem.Key, 2)
End Property

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    m_bCancelled = True
End Sub

Private Sub btnOK_Click()
    Me.Hide
    DoEvents
    m_bCancelled = False
End Sub

Private Sub cmbLocation_ItemChange()
    Me.chkUseExistingPageSetup.Enabled = Me.cmbLocation.Array.Value(Me.cmbLocation.SelectedItem, 2)
End Sub

Private Sub cmbLocation_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLocation, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLocation_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If bAuthorIsDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbLocation)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    Me.lstItems.SetFocus
End Sub

Private Sub Form_Load()
    ShowItems
    
'   select first item if it's available
    With Me.lstItems.ListItems
        If .Count Then
            .Item(1).Selected = True
        Else
            Me.btnOK.Enabled = False
        End If
    End With
    
    GetAllowableOptions
    m_bCancelled = True
End Sub

Private Sub ShowItems()
    Dim oArray As XArray
    Dim xTemplate As String
    xTemplate = Word.ActiveDocument.AttachedTemplate
    Set m_oDefs = g_oDBs.SegmentDefs(xTemplate)
    Set oArray = m_oDefs.ListSource
    With oArray
        For i = 1 To .Count(1)
            On Error GoTo 0
            Me.lstItems.ListItems.Add Key:="x" & .Value(i - 1, 1), _
                                      Text:=.Value(i - 1, 0), _
                                      Icon:="Normal Document"
        Next i
    End With
End Sub
                                          
Private Sub lstItems_DblClick()
    If m_bItemClicked Then
        m_bItemClicked = False
        btnOK_Click
    End If
End Sub

Private Sub lstItems_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim xDesc As String

    On Error GoTo ProcError

    m_bItemClicked = True
    GetAllowableOptions
    Exit Sub
ProcError:
    g_oError.Show Err
End Sub

Private Sub GetAllowableOptions()
'fills cmbLocations with available insertion
'locations for the selected Segment type

'   get location options
    Dim oArray As XArray
    Dim oDef As mpDB.CSegmentDef
    Dim iCurItem As Integer
    Dim xCurItem As String
    
    xCurItem = Me.cmbLocation.BoundText
    
    On Error Resume Next
    Set oDef = m_oDefs.Item(Me.SelectedItem)
    On Error GoTo ProcError
    
    If oDef Is Nothing Then
        Me.btnOK.Enabled = False
        Exit Sub
    End If

'   create an empty xarray
    Set oArray = New XArray
    oArray.ReDim 0, 4, 0, 2
    Dim iUpper As Integer
    iUpper = -1
'   add items if definition allows for it - column 2
'   holds boolean to enable/disable "Use Page Setup of adjacent section"
    With oArray
        If oDef.AllowInStartSection Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "Start of Document in Separate Section"
            .Value(iUpper, 0) = mpSegmentLocations_NewFirstSection
            .Value(iUpper, 2) = False
        End If
        If oDef.AllowInEndSection Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "End of Document in Separate Section"
            .Value(iUpper, 0) = mpSegmentLocations_NewLastSection
            .Value(iUpper, 2) = oDef.AllowExistingHeaderFooters
        End If
        If oDef.AllowInCursorSection Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "At Insertion Point in Separate Section"
            .Value(iUpper, 0) = mpSegmentLocations_NewSectionAtSelection
            .Value(iUpper, 2) = oDef.AllowExistingHeaderFooters
        End If
        If oDef.AllowAtCursor Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "Insertion Point"
            .Value(iUpper, 0) = mpSegmentLocations_InsertionPoint
            .Value(iUpper, 2) = False
        End If
        If oDef.AllowAtEndOfDocument Then
            iUpper = iUpper + 1
            .Value(iUpper, 1) = "End Of Document"
            .Value(iUpper, 0) = mpSegmentLocations_EndOfDocument
            .Value(iUpper, 2) = False
        End If
        .ReDim 0, iUpper, 0, 2
    End With
    If oArray.Count(1) = 0 Then
        xDesc = "No allowable locations have been specified for this segment." & _
            "  Please check tblSegmentTypes in mpPublic.mdb."
        Err.Raise mpError_InvalidList
    End If
    
'   bind to control
    With Me.cmbLocation
        .Array = oArray
        .Rebind
        
'       attempt to select previous selection
        .BoundText = xCurItem
        
'       if attempt was not successful, select first item
        If .BoundText = "" Then
            .SelectedItem = 0
        End If
    End With
    
    ResizeTDBCombo Me.cmbLocation, 5

'   setup checkbox
    If Not oDef.AllowExistingHeaderFooters Then
        Me.chkUseExistingPageSetup.Value = False
    End If
    Me.chkUseExistingPageSetup.Enabled = _
        oDef.AllowExistingHeaderFooters
    Exit Sub
ProcError:
    RaiseError "MPO.frmInsertSegment.GetAllowableOptions", xDesc
End Sub
Private Sub lstItems_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Dim oItem As ListItem

    If Button = 2 Then
        With Me.lstItems
            On Error Resume Next
            
'           exit if an icon was not clicked
            Set oItem = .HitTest(x, Y)
            If (oItem Is Nothing) Then
                Exit Sub
            End If
            
'           ensure that icon that has been
'           right-clicked on is selected
            oItem.Selected = True
        End With
    End If
End Sub


