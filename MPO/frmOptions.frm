VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{7FC0C800-384B-11D4-A8F3-00A0CC581503}#1.0#0"; "MPCONT~1.OCX"
Begin VB.Form frmAuthorDefaults 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "XXX"
   ClientHeight    =   6210
   ClientLeft      =   1935
   ClientTop       =   630
   ClientWidth     =   5475
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOptions.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6210
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin TrueDBList60.TDBCombo cmbTemplate 
      Height          =   345
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":000C
      TabIndex        =   1
      Top             =   120
      Width           =   3615
   End
   Begin mpControls.SpinnerTextBox stbCustom 
      Height          =   315
      Index           =   0
      Left            =   1770
      TabIndex        =   47
      Top             =   3615
      Visible         =   0   'False
      Width           =   3600
      _ExtentX        =   6350
      _ExtentY        =   556
      Object.Visible         =   0   'False
      IncrementValue  =   0
      MaxValue        =   0
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   7
      Left            =   1920
      TabIndex        =   32
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   6
      Left            =   1920
      TabIndex        =   31
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "Sa&ve"
      Height          =   400
      Left            =   2265
      TabIndex        =   55
      Top             =   5730
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   400
      Left            =   4425
      TabIndex        =   57
      Top             =   5730
      Width           =   1000
   End
   Begin VB.CommandButton btnReset 
      Caption         =   "&Reset"
      Height          =   400
      Left            =   3315
      TabIndex        =   56
      Top             =   5730
      Width           =   1035
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   1785
      TabIndex        =   15
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   1785
      TabIndex        =   14
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   1785
      TabIndex        =   13
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   1785
      TabIndex        =   12
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   4
      Left            =   1785
      TabIndex        =   11
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   5
      Left            =   1785
      TabIndex        =   10
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   0
      Left            =   1920
      TabIndex        =   9
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   1
      Left            =   1920
      TabIndex        =   8
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   2
      Left            =   1920
      TabIndex        =   7
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   3
      Left            =   1920
      TabIndex        =   6
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   4
      Left            =   1920
      TabIndex        =   5
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   5
      Left            =   1920
      TabIndex        =   4
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin TrueDBList60.TDBCombo cmbAuthor 
      Height          =   375
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":2043
      TabIndex        =   3
      Top             =   630
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   0
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":5F78
      TabIndex        =   16
      Top             =   1680
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   1
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":8178
      TabIndex        =   40
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   2
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":A378
      TabIndex        =   41
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   3
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":C578
      TabIndex        =   42
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   4
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":E778
      TabIndex        =   43
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   5
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":10978
      TabIndex        =   44
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   6
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":12B78
      TabIndex        =   45
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   7
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":14D78
      TabIndex        =   46
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin mpControls.SpinnerTextBox stbCustom 
      Height          =   315
      Index           =   1
      Left            =   1770
      TabIndex        =   49
      Top             =   3615
      Visible         =   0   'False
      Width           =   3600
      _ExtentX        =   6350
      _ExtentY        =   556
      Object.Visible         =   0   'False
      IncrementValue  =   0
      MaxValue        =   0
   End
   Begin mpControls.SpinnerTextBox stbCustom 
      Height          =   315
      Index           =   2
      Left            =   1770
      TabIndex        =   51
      Top             =   3615
      Visible         =   0   'False
      Width           =   3600
      _ExtentX        =   6350
      _ExtentY        =   556
      Object.Visible         =   0   'False
      IncrementValue  =   0
      MaxValue        =   0
   End
   Begin mpControls.SpinnerTextBox stbCustom 
      Height          =   315
      Index           =   3
      Left            =   1770
      TabIndex        =   53
      Top             =   3615
      Visible         =   0   'False
      Width           =   3600
      _ExtentX        =   6350
      _ExtentY        =   556
      Object.Visible         =   0   'False
      IncrementValue  =   0
      MaxValue        =   0
   End
   Begin VB.Label lblTemplate 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Template:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   135
      TabIndex        =   0
      Top             =   165
      Width           =   1455
   End
   Begin VB.Label lblCustomSpinner 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomSpinner"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Index           =   3
      Left            =   120
      TabIndex        =   54
      Top             =   3645
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomSpinner 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomSpinner"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Index           =   2
      Left            =   120
      TabIndex        =   52
      Top             =   3645
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomSpinner 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomSpinner"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Index           =   1
      Left            =   120
      TabIndex        =   50
      Top             =   3645
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomSpinner 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomSpinner"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   0
      Left            =   120
      TabIndex        =   48
      Top             =   3645
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Index           =   6
      Left            =   2400
      TabIndex        =   39
      Top             =   2760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Index           =   5
      Left            =   2760
      TabIndex        =   38
      Top             =   2760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Index           =   4
      Left            =   2760
      TabIndex        =   37
      Top             =   2760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Index           =   3
      Left            =   2760
      TabIndex        =   36
      Top             =   2760
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   2
      Left            =   135
      TabIndex        =   35
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   1
      Left            =   135
      TabIndex        =   34
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   0
      Left            =   135
      TabIndex        =   33
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   7
      Left            =   135
      TabIndex        =   30
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   6
      Left            =   135
      TabIndex        =   29
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   0
      Left            =   135
      TabIndex        =   28
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   1
      Left            =   135
      TabIndex        =   27
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   2
      Left            =   135
      TabIndex        =   26
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   3
      Left            =   135
      TabIndex        =   25
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   4
      Left            =   135
      TabIndex        =   24
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   5
      Left            =   135
      TabIndex        =   23
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblAuthor 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Author:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   420
      Left            =   120
      TabIndex        =   2
      Top             =   675
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   5
      Left            =   105
      TabIndex        =   22
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   4
      Left            =   105
      TabIndex        =   21
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   3
      Left            =   105
      TabIndex        =   20
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   2
      Left            =   105
      TabIndex        =   19
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   1
      Left            =   105
      TabIndex        =   18
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   0
      Left            =   105
      TabIndex        =   17
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      FillColor       =   &H00800080&
      Height          =   9000
      Left            =   -120
      Top             =   -375
      Width           =   1800
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_EditAuthorOptions 
         Caption         =   "&Edit Preferences..."
      End
      Begin VB.Menu mnuAuthor_CopyOptions 
         Caption         =   "Cop&y Preferences..."
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmAuthorDefaults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const mpCaptionRoot As String = "Author Preferences"
Private Const mpTemplate As String = "XXX"
Private Const mpMaxTextBoxes As Byte = 6
Private Const mpMaxComboBoxes As Byte = 8
Private Const mpMaxCheckBoxes As Byte = 6
Private Const mpMaxLabels As Byte = 6
Private Const mpMaxSpinTexts As Byte = 4

Private m_iNextPosition As Integer
Private m_iNumTextBoxes As Integer
Private m_iNumComboBoxes As Integer
Private m_iNumCheckBoxes As Integer
Private m_iNumLabels As Integer
Private m_iNumSpinTexts As Integer

Private m_bInitializing As Boolean
Private m_bUpdateDoc As Boolean
Private m_oOptions As mpDB.CPersonOptions
Private m_oPrevCtl As Control
Private m_oOptCtls As mpDB.COptionsControls
Private m_bCancelled As Boolean
Private m_DefaultAuthor As mpDB.CPerson
Private m_oAuthor As mpDB.CPerson
Private m_oDocObj As MPO.IDocObj
Private m_bDirty As Boolean
Private m_bPrefChanged As Boolean
Private m_bChangingTemplate As Boolean

Private m_oCtl As Control

Public Event DefaultOptionsChange()
Public Event CurrentDocAuthorOptionsChange()

Public Property Let DocObject(oNew As MPO.IDocObj)
    Set m_oDocObj = oNew
End Property

Public Property Get DocObject() As MPO.IDocObj
    Set DocObject = m_oDocObj
End Property

Public Property Get Template() As String
    On Error GoTo ProcError
    If Me.DocObject Is Nothing Then
        Template = Me.cmbTemplate.BoundText
    Else
        Template = Me.DocObject.Template.ID
    End If
    Exit Property
ProcError:
    RaiseError "MPO.frmAuthorDefaults.Template"
    Exit Property
End Property

Public Property Let Author(oNew As mpDB.CPerson)
    On Error GoTo ProcError
    Set m_oAuthor = oNew
    
'   update form to reflect new Author
    Set m_oOptions = m_oAuthor.Options(Me.Template)
    Exit Property
ProcError:
    RaiseError "MPO.frmAuthorDefaults.Author"
    Exit Property
End Property

Public Property Get Author() As mpDB.CPerson
    Set Author = m_oAuthor
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property

Private Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property

Public Sub UpdateForm()
'cycle through visible controls, getting
'appropriate value from DocObject property
    Dim oCtl As Control
    
    On Error GoTo ProcError
    For Each oCtl In Me.Controls
'       check for tag - only controls with a tag
'       are hooked up to DocObject properties
        If Len(oCtl.Tag) Then
            If TypeOf oCtl Is VB.TextBox Then
                oCtl.Text = xNullToString(m_oOptions.Item(oCtl.Tag).Value)
            ElseIf TypeOf oCtl Is VB.CheckBox Then
                If m_oOptions.Item(oCtl.Tag).Value Then
                    oCtl.Value = vbChecked
                Else
                    oCtl.Value = vbUnchecked
                End If
            ElseIf TypeOf oCtl Is TDBCombo Then
                oCtl.BoundText = m_oOptions.Item(oCtl.Tag).Value
            ElseIf TypeOf oCtl Is mpControls.SpinnerTextBox Then
                oCtl.Value = m_oOptions.Item(oCtl.Tag).Value
            End If
        End If
    Next oCtl
    
    SetFormCaption
    DoEvents
    m_bDirty = False
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.UpdateForm"
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    On Error GoTo ProcError
    SetControlProps Me.btnCancel
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyReturn Then _
        btnCancel_Click
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    On Error GoTo ProcError
    btnCancel_Click
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnReset_GotFocus()
    On Error GoTo ProcError
    SetControlProps Me.btnReset
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnReset_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyReturn Then _
        ResetOptions
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnReset_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    On Error GoTo ProcError
    ResetOptions
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnSave_GotFocus()
    On Error GoTo ProcError
    SetControlProps btnSave
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnSave_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then _
        SaveOptions
End Sub

Private Sub btnSave_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
    On Error GoTo ProcError
    SaveOptions
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkCustom_Click(Index As Integer)
    m_bDirty = True
End Sub

Private Sub chkCustom_GotFocus(Index As Integer)
    On Error GoTo ProcError
    SetControlProps Me.chkCustom(Index)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_GotFocus()
    On Error GoTo ProcError
    SetControlProps Me.cmbAuthor
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCustom_GotFocus(Index As Integer)
    On Error GoTo ProcError
    SetControlProps Me.cmbCustom(Index)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCustom_ItemChange(Index As Integer)
    m_bDirty = True
End Sub

Private Sub cmbCustom_Mismatch(Index As Integer, NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCustom(Index), Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCustom_Validate(Index As Integer, Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCustom(Index))
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTemplate_GotFocus()
    On Error GoTo ProcError
    SetControlProps Me.cmbTemplate
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTemplate_ItemChange()
    
    On Error GoTo ProcError
    
'   ask to save if options have changed
    If Not m_bInitializing Then AskAndSave

    m_bChangingTemplate = True
    LockWindowUpdate Me.hwnd
    
'   test for valid Preferences table
    If Len(g_oTemplates.Item(Me.cmbTemplate.BoundText).OptionsTable) = 0 Then
        xMsg = "No Preferences table specified for template."
        MsgBox xMsg, vbExclamation, App.Title
        Exit Sub
    End If
    
'   set template
    With m_oOptions
        .Template = Me.cmbTemplate.BoundText
        .Refresh
    End With
    
    HideControls
    SetUpControls
    Me.cmbAuthor.BoundText = Me.cmbAuthor.BoundText
    UpdateForm
    cmbAuthor_ItemChange
    Me.cmbTemplate.SetFocus
    m_bChangingTemplate = False
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()

    On Error Resume Next
    
    If Me.Initializing Then
'       setup author
        If Not (Me.DocObject Is Nothing) Then
            Me.Author = Me.DocObject.Author
            Me.cmbAuthor.SetFocus
        Else
            Me.Author = g_oDBs.People.First
            Me.cmbTemplate.SetFocus
        End If
    
'       select person if supplied - else
'       select the first person in the list
        With Me.cmbAuthor
            If (Me.DocObject Is Nothing) Then
                .SelectedItem = 0
            Else
                If (Me.DocObject.Author Is Nothing) Then
                    .SelectedItem = 0
                Else
                    .BoundText = Me.DocObject.Author.ID
                End If
            End If
        End With
        UpdateForm
    End If
    EchoOn
    m_bInitializing = False
End Sub

'**********************************************************
'   Form Event Procedures
'**********************************************************
Private Sub Form_Load()
    Dim oList As CList
    
    On Error GoTo ProcError
    m_bInitializing = True
    
'   set up dialog
    SetUpDialog
    
'   set dlg title
    SetFormCaption
    
'   load templates
    With Me.cmbTemplate
        On Error Resume Next
        Set oList = g_oDBs.Lists("Options Templates")
        With g_oTemplates
            .Group = Empty
            .Refresh
        End With
        
        On Error GoTo ProcError
        If oList Is Nothing Then
            Err.Raise mpError_InvalidMember
        End If
        .Array = oList.ListItems.Source
        .Rebind
        ResizeTDBCombo Me.cmbTemplate, 10
        DoEvents
        Me.cmbTemplate.Bookmark = 0
    End With
    
    With Me.cmbAuthor
        .Array = g_oDBs.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
        DoEvents
    End With
    
    SetUpControls
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.Form_Load"
    Exit Sub
End Sub

Private Sub SetUpDialog()
'assigns dialog properties
    On Error GoTo ProcError
    
    If (Me.DocObject Is Nothing) Then
'       setup dialog to show both template and author
        Me.btnCancel.Top = Me.btnCancel.Top + 510
        Me.btnReset.Top = Me.btnReset.Top + 510
        Me.btnSave.Top = Me.btnSave.Top + 510
        Me.Height = Me.Height + 510
    Else
'       setup dialog to show only author
        Me.cmbAuthor.Top = Me.cmbTemplate.Top
        Me.lblAuthor.Top = Me.lblTemplate.Top
        Me.cmbTemplate.Visible = False
        Me.lblTemplate.Visible = False
    End If
    
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetupDialog"
    Exit Sub
End Sub

Private Sub HideControls()
'hide all controls that are tagged-
'prepares form for SetupControls
    Dim oCtl As Control
    On Error GoTo ProcError
    For Each oCtl In Me.Controls
        If Len(oCtl.Tag) Then
            oCtl.Visible = False
            oCtl.Tag = Empty
            On Error Resume Next
            oCtl.Text = Empty
            oCtl.Value = Empty
            oCtl.BoundText = Empty
            Err.Clear
            On Error GoTo 0
        ElseIf TypeOf oCtl Is VB.Label Then
            If oCtl.Name <> "lblTemplate" And _
                oCtl.Name <> "lblAuthor" Then
                oCtl.Visible = False
            End If
        End If
    Next oCtl
    
    m_iNextPosition = Empty
    m_iNumTextBoxes = Empty
    m_iNumComboBoxes = Empty
    m_iNumCheckBoxes = Empty
    m_iNumLabels = Empty
    m_iNumSpinTexts = Empty
    Set m_oPrevCtl = Nothing
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.HideControls"
    Exit Sub
End Sub

Private Sub SetUpControls()
'set up controls on dialog based
'on definitions in db
    Dim i As Integer
    Dim oCurCtl As Control
    Dim iCtlIndex As Integer
            
'   get controls for form
    On Error GoTo ProcError
    Set m_oOptCtls = g_oDBs.OptionsControls(Me.Template)

    For i = 1 To m_oOptCtls.Count
        With m_oOptCtls(i)
'           get control to which this custom
'           control will be assigned
            Select Case .ControlType
                Case mpOptionsControl_TextBox
                    Set oCurCtl = SetUpTextBox(m_oOptCtls(i))
                Case mpOptionsControl_ComboBox, _
                     mpOptionsControl_ListBox
                    Set oCurCtl = SetUpComboList(m_oOptCtls(i))
                Case mpOptionsControl_CheckBox
                    Set oCurCtl = SetUpCheckBox(m_oOptCtls(i))
                Case mpOptionsControl_Label
                    Set oCurCtl = SetUpLabel(m_oOptCtls(i))
                Case mpOptionsControl_SpinText
                    Set oCurCtl = SetUpSpinText(m_oOptCtls(i))
            End Select
            
'           set this control as the previous control
            Set m_oPrevCtl = oCurCtl
        End With
    Next i
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetUpControls"
    Exit Sub
End Sub

Private Function SetUpLabel(oCustCtl As mpDB.COptionsControl) As VB.Label
'sets up the next available label
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.Label
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumLabels = mpMaxLabels Then
            xMsg = "Too many labels defined.  " & _
                   "Each options dialog is limited to " & _
                    mpMaxLabels & " textboxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumLabels
        
'       set up the text box in the array with specified index
        Set oCtl = Me.lblCustomLabel(iCtlIndex)
        oCtl.Caption = oCustCtl.Caption
        oCtl.Visible = True
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomLabel(iCtlIndex)
        
'       increment the number of labels used
        m_iNumLabels = m_iNumLabels + 1
    End With

'   return control
    Set SetUpLabel = oCtl
    Exit Function
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetUpLabel"
    Exit Function
End Function

Private Function SetUpTextBox(oCustCtl As mpDB.COptionsControl) As VB.TextBox
'sets up the next available textbox
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.TextBox
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumTextBoxes = mpMaxTextBoxes Then
            xMsg = "Too many text boxes defined.  " & _
                   "Each options dialog is limited to " & mpMaxTextBoxes & " textboxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumTextBoxes
        
'       set up the text box in the array with specified index
        Set oCtl = Me.txtCustom(iCtlIndex)
        oCtl.Tag = .OptionsField
        oCtl.Visible = True
        
'       set up corresponding label
        With Me.lblCustomText(iCtlIndex)
            .Caption = oCustCtl.Caption & ":"
            .Visible = True
        End With
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomText(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumTextBoxes = m_iNumTextBoxes + 1
    End With

'   return control
    Set SetUpTextBox = oCtl
    Exit Function
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetUpTextBox"
    Exit Function
End Function

Private Function SetUpSpinText(oCustCtl As mpDB.COptionsControl) As mpControls.SpinnerTextBox
'sets up the next available spinner textbox
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As mpControls.SpinnerTextBox
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumTextBoxes = mpMaxSpinTexts Then
            xMsg = "Too many spinner textboxes defined.  " & _
                   "Each options dialog is limited to " _
                   & mpMaxTextBoxes & " spinner textboxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumSpinTexts
        
'       set up the text box in the array with specified index
        Set oCtl = Me.stbCustom(iCtlIndex)
        oCtl.Tag = .OptionsField
        oCtl.Visible = True
        oCtl.MaxValue = .MaxValue
        oCtl.MinValue = .MinValue
        oCtl.IncrementValue = .Increment
        oCtl.SupplyQuotes = .AppendQuotes
        
'       set up corresponding label
        Me.lblCustomSpinner(iCtlIndex).Caption = .Caption & ":"
        Me.lblCustomSpinner(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomSpinner(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumSpinTexts = m_iNumSpinTexts + 1
    End With

'   return control
    Set SetUpSpinText = oCtl
    Exit Function
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetUpSpinText"
    Exit Function
End Function

Private Function SetUpCheckBox(oCustCtl As mpDB.COptionsControl) As VB.CheckBox
'sets up the next available checkbox
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumCheckBoxes = mpMaxCheckBoxes Then
            xMsg = "Too many check boxes defined.  " & _
                   "Each options dialog is limited to " & _
                    mpMaxCheckBoxes & " check boxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumCheckBoxes
        
'       set up the text box in the array with specified index
        Me.chkCustom(iCtlIndex).Tag = .OptionsField
        Me.chkCustom(iCtlIndex).Caption = .Caption
        Me.chkCustom(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, Me.chkCustom(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumCheckBoxes = m_iNumCheckBoxes + 1
    End With

'   return control
    Set SetUpCheckBox = Me.chkCustom(iCtlIndex)
    Exit Function
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetUpCheckBox"
    Exit Function
End Function

Private Function SetUpComboList(oCustCtl As mpDB.COptionsControl) As TDBCombo
'sets up the next available textbox
'to have the properties of the MacPac
'custom control definition
    Dim iCtlIndex As Integer
    Dim oCombo As TDBCombo
    Dim oLabel As VB.Label
    
    On Error GoTo ProcError
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumComboBoxes = mpMaxComboBoxes Then
            xMsg = "Too many combo/list boxes defined .  " & _
                   "Each options dialog is limited to " & mpMaxComboBoxes & " combo/list boxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumComboBoxes
        
'       set up the combo box with specified index in the array
        Set oCombo = Me.cmbCustom(iCtlIndex)
        oCombo.Tag = .OptionsField
        
'       set to list box if limited to list entries
        If .ControlType = mpOptionsControl_ComboBox Then
            oCombo.LimitToList = False
        End If
        
        oCombo.Visible = True
        
        If Not (.List Is Nothing) Then
            oCombo.Array = .List.ListItems.Source
            oCombo.Rebind
            ResizeTDBCombo oCombo, 5
        End If
        
'       set up label
        Set oLabel = Me.lblCustomCombo(iCtlIndex)
        oLabel.Caption = .Caption & ":"
        oLabel.Visible = True
        
'       position controls
        PositionControl oCustCtl, _
                        oCombo, _
                        oLabel
        
'       increment the number of combos used on the specified tab
        m_iNumComboBoxes = m_iNumComboBoxes + 1
    End With

'   return control
    Set SetUpComboList = oCombo
    Exit Function
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetUpComboList"
    Exit Function
End Function

Private Sub PositionControl(oDef As mpDB.COptionsControl, _
                            oCtl As Control, _
                            Optional oLabel As VB.Label)
'   position control and corresponding
'   label based on position of previous
'   control and space before for current control
    Dim sPrevTop As Single
    Dim sPrevHeight As Single
    Dim sPrevLeft As Single
    Dim iPrevTabIndex As Integer
    Dim oPrevCtl As Control
    
    On Error GoTo ProcError
    Set oPrevCtl = m_oPrevCtl
    
'   test for previous control
    If (oPrevCtl Is Nothing) Then
'       no previous control - base position on author control
        With Me.cmbAuthor
            sPrevTop = .Top
            sPrevHeight = .Height
            sPrevLeft = .Left
            iPrevTabIndex = .TabIndex
        End With
    Else
        With oPrevCtl
'           position control based on previous control
            sPrevTop = .Top
            sPrevHeight = .Height
            sPrevLeft = .Left
            iPrevTabIndex = .TabIndex
        End With
    End If

'   execute
    With oCtl
        .Left = Me.cmbAuthor.Left + oDef.HorizontalOffset
        .Top = sPrevTop + oDef.VerticalOffset
        If oDef.Width > 0 Then
            .Width = oDef.Width
        End If
        If Not (oLabel Is Nothing) Then
'           chkboxes don't have associated label controls-
'           position labels for all other controls
            oLabel.Left = 105
            oLabel.Top = .Top + 50
        End If

'       set tab index
        If Not (oLabel Is Nothing) Then
            oLabel.TabIndex = iPrevTabIndex + 1
            .TabIndex = iPrevTabIndex + 2
        Else
            .TabIndex = iPrevTabIndex + 1
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.PositionControl"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    mpBase2.SetLastPosition Me

    m_iNextPosition = Empty
    m_iNumTextBoxes = Empty
    m_iNumComboBoxes = Empty
    m_iNumCheckBoxes = Empty
    m_iNumLabels = Empty
    m_iNumSpinTexts = Empty
    
    Set m_oOptCtls = Nothing
    Set g_oPrevControl = Nothing
End Sub

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub SetFormCaption()
    On Error GoTo ProcError
    If (Me.DocObject Is Nothing) Then
        Me.Caption = mpBase2.xGetFileName(Me.cmbTemplate.BoundText) _
            & " " & mpCaptionRoot
    Else
        Me.Caption = Me.DocObject.Template.ShortName & " " & mpCaptionRoot
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetFormCaption"
    Exit Sub
End Sub

Private Sub ResetOptions()
    Dim iResult As VbMsgBoxResult
    Dim xMsg As String
    Dim xTemplate As String
    
    On Error GoTo ProcError
    If (Me.DocObject Is Nothing) Then
        xTemplate = LCase(mpBase2.xGetFileName(Me.cmbTemplate.BoundText))
    Else
        xTemplate = LCase(Me.DocObject.Template.ShortName)
    End If
    
'   confirm deletion
    xMsg = "Reset the " & xTemplate & " preferences for " & _
    Me.Author.FullName & " to the firm defaults?"
    iResult = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
    
    If iResult = vbYes Then
'       delete letter options for selected person
        m_oOptions.Delete
        m_oOptions.Refresh
        
'       refresh dlg values
        UpdateForm
        
        BroadcastChanges
    End If
    
    Me.cmbAuthor.SetFocus
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.ResetOptions"
    Exit Sub
End Sub

Private Sub SaveOptions()
    Dim oTemplate As MPO.CTemplate
    
    On Error GoTo ProcError
    Me.MousePointer = vbHourglass
    
    If m_bDirty Then
'       update options with dlg values
        UpdateObject
        
'       save record
        m_oOptions.Save
        
        BroadcastChanges
        
        m_bDirty = False
    End If
    Me.MousePointer = vbDefault
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SaveOptions"
    Exit Sub
End Sub

Private Sub BroadcastChanges()
'raises the appropriate events to
'update current doc and boilerplate

    On Error GoTo ProcError
    
    If Not (Me.DocObject Is Nothing) Then
'       this form is being called from
'       a doc object dlg (eg frmLetter) -
'       broadcast that options have changed
'       for default template author
        If Me.Author.ID = Me.DocObject.DefaultAuthor.ID Then
            RaiseEvent DefaultOptionsChange
        End If

'       broadcast that options have changed
'       for current document author
        If Me.Author.ID = Me.DocObject.Author.ID Then
            RaiseEvent CurrentDocAuthorOptionsChange
        End If
    Else
'       mark user ini to force update of boilerplate
'       if saved options belong to the default author
        With g_oTemplates(Me.Template)
            If Me.Author.ID = .DefaultAuthor.ID Then
                .BoilerplateVersion = CDbl(Now)
            End If
        End With
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.BroadcastChanges"
    Exit Sub
End Sub

Private Sub UpdateObject()
'update each member of the option set
'with the value in the associated control
    Dim i As Integer
    Dim oCtl As Control
    Dim oOpt As mpDB.CPersonOption
    
'   cycle through all controls
    On Error GoTo ProcError
    For Each oCtl In Me.Controls
'       check only tagged controls - these
'       are the ones associated with an option
        If Len(oCtl.Tag) Then
'           get option to update from the tag
            Set oOpt = m_oOptions.Item(oCtl.Tag)
            
'           set option value = value of control
            If TypeOf oCtl Is TDBCombo Then
                If Len(oCtl.BoundText) Then
                    oOpt.Value = oCtl.BoundText
                Else
                    oOpt.Value = oCtl.Text
                End If
            ElseIf TypeOf oCtl Is TDBList Then
                If Len(oCtl.BoundText) Then
                    oOpt.Value = oCtl.BoundText
                Else
                    oOpt.Value = oCtl.Text
                End If
            ElseIf TypeOf oCtl Is VB.TextBox Then
                oOpt.Value = oCtl.Text
            ElseIf TypeOf oCtl Is mpControls.SpinnerTextBox Then
                oOpt.Value = oCtl.Value
            Else
                If Not IsNull(oCtl.Value) Then
                    oOpt.Value = oCtl.Value
                End If
            End If
        End If
    Next oCtl
    m_bDirty = True
    m_oOptions.IsDirty = True
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.UpdateObject"
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    
    On Error GoTo ProcError
    
'   ask to save if options have changed
    AskAndSave
    
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnReset_Click()
    On Error GoTo ProcError
    ResetOptions
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnSave_Click()
    On Error GoTo ProcError
    SaveOptions
    Me.cmbAuthor.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_ItemChange()
    
    On Error GoTo ProcError
    
'   ask to save if options have changed
    If Not m_bInitializing And Not m_bChangingTemplate Then AskAndSave
    
    Me.Author = g_oDBs.People.Item(Me.cmbAuthor.BoundText)
    UpdateForm
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTemplate_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbTemplate, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTemplate_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbTemplate)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub stbCustom_Change(Index As Integer)
    m_bDirty = True
End Sub

Private Sub stbCustom_GotFocus(Index As Integer)
    On Error GoTo ProcError
    SetControlProps Me.stbCustom(Index)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub SetControlProps(oCtl As Control)
    On Error GoTo ProcError
    Set m_oCtl = oCtl
    SetControlBackColor oCtl
    SelectControlText
    Exit Sub
ProcError:
    RaiseError "MPO.frmAuthorDefaults.SetControlProps"
    Exit Sub
End Sub

Private Sub txtCustom_Change(Index As Integer)
    m_bDirty = True
End Sub

Private Sub txtCustom_GotFocus(Index As Integer)
    On Error GoTo ProcError
    SetControlProps Me.txtCustom(Index)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub AskAndSave()
    Dim iAnswer As Integer
    
    If m_bDirty = True Then
        iAnswer = MsgBox("Would you like to save the changes you have made?", _
                         vbYesNo + vbInformation, _
                         App.Title)
        
        If iAnswer = vbYes Then
            SaveOptions
            m_bPrefChanged = False
        End If
    End If
    
End Sub
