VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmDocumentLook 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Apply Document Type"
   ClientHeight    =   1245
   ClientLeft      =   3825
   ClientTop       =   1080
   ClientWidth     =   5595
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDocumentLook.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1245
   ScaleWidth      =   5595
   ShowInTaskbar   =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnDetail 
      Caption         =   "Show &Detail"
      Height          =   400
      Left            =   1845
      TabIndex        =   14
      Top             =   730
      Width           =   1290
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4430
      TabIndex        =   16
      Top             =   730
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   3345
      TabIndex        =   15
      Top             =   730
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbDocType 
      Height          =   600
      Left            =   1845
      OleObjectBlob   =   "frmDocumentLook.frx":000C
      TabIndex        =   1
      Top             =   195
      Width           =   3585
   End
   Begin VB.PictureBox picGroup 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   5265
      Left            =   30
      ScaleHeight     =   5265
      ScaleWidth      =   5520
      TabIndex        =   17
      Top             =   675
      Visible         =   0   'False
      Width           =   5520
      Begin VB.CheckBox chkApplyMargins 
         Height          =   240
         Left            =   1905
         TabIndex        =   3
         Top             =   300
         Value           =   1  'Checked
         Width           =   240
      End
      Begin VB.CheckBox chkApplyNumCols 
         Height          =   240
         Left            =   1905
         TabIndex        =   5
         Top             =   1365
         Value           =   1  'Checked
         Width           =   240
      End
      Begin VB.CheckBox chkApplyDiffPage1HF 
         Height          =   240
         Left            =   1905
         TabIndex        =   7
         Top             =   1950
         Value           =   1  'Checked
         Width           =   240
      End
      Begin VB.CheckBox chkApplyFont 
         Height          =   240
         Left            =   1905
         TabIndex        =   9
         Top             =   2340
         Value           =   1  'Checked
         Width           =   240
      End
      Begin VB.CheckBox chkApplyParaFormat 
         Height          =   240
         Left            =   1905
         TabIndex        =   11
         Top             =   2910
         Value           =   1  'Checked
         Width           =   240
      End
      Begin VB.CheckBox chkApplyPageNumber 
         Height          =   240
         Left            =   1905
         TabIndex        =   13
         Top             =   4260
         Value           =   1  'Checked
         Width           =   240
      End
      Begin VB.Label lblMargins 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "&Margins:"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   345
         TabIndex        =   2
         Top             =   270
         Width           =   1290
      End
      Begin VB.Label lblCols 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "&Columns:"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   180
         TabIndex        =   4
         Top             =   1365
         Width           =   1455
      End
      Begin VB.Label lblDiffHF 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Header/Footer: "
         ForeColor       =   &H00FFFFFF&
         Height          =   450
         Left            =   0
         TabIndex        =   6
         Top             =   1935
         Width           =   1635
      End
      Begin VB.Label lblFont 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "&Font:"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   345
         TabIndex        =   8
         Top             =   2340
         Width           =   1290
      End
      Begin VB.Label lblParaFormat 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "&Paragraph Format:"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   150
         TabIndex        =   10
         Top             =   2895
         Width           =   1485
      End
      Begin VB.Label lblPageNumber 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Page &Number:"
         ForeColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   150
         TabIndex        =   12
         Top             =   4245
         Width           =   1485
      End
      Begin VB.Label lblApply 
         Caption         =   "Apply"
         Height          =   255
         Left            =   1830
         TabIndex        =   54
         Top             =   0
         Width           =   495
      End
      Begin VB.Label lblTopMargin 
         Caption         =   "Top:"
         Height          =   225
         Left            =   2325
         TabIndex        =   53
         Top             =   300
         Width           =   1260
      End
      Begin VB.Label lblRightMargin 
         Caption         =   "Right:"
         Height          =   225
         Left            =   2325
         TabIndex        =   52
         Top             =   1020
         Width           =   1260
      End
      Begin VB.Label lblLeftMargin 
         Caption         =   "Left:"
         Height          =   225
         Left            =   2325
         TabIndex        =   51
         Top             =   780
         Width           =   1260
      End
      Begin VB.Label lblBottomMargin 
         Caption         =   "Bottom:"
         Height          =   225
         Left            =   2325
         TabIndex        =   50
         Top             =   540
         Width           =   1260
      End
      Begin VB.Label lblSpaceBetweenCols 
         Caption         =   "Space Between:"
         Height          =   225
         Left            =   2325
         TabIndex        =   49
         Top             =   1620
         Width           =   1260
      End
      Begin VB.Label lblNumCols 
         Caption         =   "Number:"
         Height          =   225
         Left            =   2325
         TabIndex        =   48
         Top             =   1380
         Width           =   1260
      End
      Begin VB.Label lblFontSize 
         Caption         =   "Size:"
         Height          =   225
         Left            =   2325
         TabIndex        =   47
         Top             =   2565
         Width           =   1260
      End
      Begin VB.Label lblFontName 
         Caption         =   "Name:"
         Height          =   225
         Left            =   2325
         TabIndex        =   46
         Top             =   2325
         Width           =   1260
      End
      Begin VB.Label lblParaAlignment 
         Caption         =   "Alignment:"
         Height          =   225
         Left            =   2325
         TabIndex        =   45
         Top             =   2910
         Width           =   1260
      End
      Begin VB.Label lblFirstLine 
         Caption         =   "First Line Indent:"
         Height          =   225
         Left            =   2325
         TabIndex        =   44
         Top             =   3150
         Width           =   1260
      End
      Begin VB.Label lblLineSpace 
         Caption         =   "Line Spacing:"
         Height          =   225
         Left            =   2325
         TabIndex        =   43
         Top             =   3390
         Width           =   1260
      End
      Begin VB.Label lblSpaceBefore 
         Caption         =   "Space Before:"
         Height          =   225
         Left            =   2325
         TabIndex        =   42
         Top             =   3630
         Width           =   1260
      End
      Begin VB.Label lblSpaceAfter 
         Caption         =   "Space After:"
         Height          =   225
         Left            =   2325
         TabIndex        =   41
         Top             =   3870
         Width           =   1260
      End
      Begin VB.Label lblUniqueHF 
         Caption         =   "Different First Page:"
         Height          =   225
         Left            =   2325
         TabIndex        =   40
         Top             =   1965
         Width           =   1440
      End
      Begin VB.Label lblTopMarginDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   39
         Top             =   300
         Width           =   1605
      End
      Begin VB.Label lblRightMarginDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   38
         Top             =   1020
         Width           =   1605
      End
      Begin VB.Label lblLeftMarginDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   37
         Top             =   780
         Width           =   1605
      End
      Begin VB.Label lblBottomMarginDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   36
         Top             =   540
         Width           =   1605
      End
      Begin VB.Label lblSpaceBetweenColsDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   35
         Top             =   1620
         Width           =   1605
      End
      Begin VB.Label lblNumColsDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   34
         Top             =   1380
         Width           =   1605
      End
      Begin VB.Label lblFontSizeDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   33
         Top             =   2565
         Width           =   1605
      End
      Begin VB.Label lblFontNameDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   32
         Top             =   2325
         Width           =   1605
      End
      Begin VB.Label lblParaAlignmentDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   31
         Top             =   2910
         Width           =   1605
      End
      Begin VB.Label lblFirstLineDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   30
         Top             =   3150
         Width           =   1605
      End
      Begin VB.Label lblLineSpaceDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   29
         Top             =   3390
         Width           =   1605
      End
      Begin VB.Label lblSpaceBeforeDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   28
         Top             =   3630
         Width           =   1605
      End
      Begin VB.Label lblSpaceAfterDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   27
         Top             =   3885
         Width           =   1605
      End
      Begin VB.Label lblUniqueHFDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   26
         Top             =   1965
         Width           =   1605
      End
      Begin VB.Label lblNumSample 
         Caption         =   "Sample:"
         Height          =   225
         Left            =   2340
         TabIndex        =   25
         Top             =   4275
         Width           =   1260
      End
      Begin VB.Label lblNumLocation 
         Caption         =   "Location:"
         Height          =   225
         Left            =   2340
         TabIndex        =   24
         Top             =   4515
         Width           =   1260
      End
      Begin VB.Label lblNumAllignment 
         Caption         =   "Alignment:"
         Height          =   225
         Left            =   2340
         TabIndex        =   23
         Top             =   4755
         Width           =   1260
      End
      Begin VB.Label lblNumPage1 
         Caption         =   "Insert On Page 1:"
         Height          =   225
         Left            =   2340
         TabIndex        =   22
         Top             =   4995
         Width           =   1260
      End
      Begin VB.Label lblNumSampleDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   21
         Top             =   4275
         Width           =   1605
      End
      Begin VB.Label lblNumLocationDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   20
         Top             =   4515
         Width           =   1605
      End
      Begin VB.Label lblNumAlignmentDesc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   19
         Top             =   4755
         Width           =   1605
      End
      Begin VB.Label lblNumPage1Desc 
         Caption         =   "###"
         Height          =   225
         Left            =   3885
         TabIndex        =   18
         Top             =   4995
         Width           =   1605
      End
      Begin VB.Shape Shape2 
         BackColor       =   &H80000003&
         BackStyle       =   1  'Opaque
         BorderColor     =   &H80000003&
         FillColor       =   &H80000003&
         Height          =   6690
         Left            =   -60
         Top             =   0
         Width           =   1770
      End
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00FFFFFF&
      Visible         =   0   'False
      X1              =   690
      X2              =   6360
      Y1              =   6930
      Y2              =   6930
   End
   Begin VB.Label lblDocType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Type:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   405
      TabIndex        =   0
      Top             =   255
      Width           =   1275
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H80000003&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   7260
      Left            =   -30
      Top             =   -270
      Width           =   1770
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000005&
      X1              =   -4485
      X2              =   1670
      Y1              =   6300
      Y2              =   6300
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmDocumentLook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_oLook As CDocumentLook
Private m_bCancelled As Boolean

Private m_sDetail As Single
Private m_sLine As Single
Private m_sOK As Single
Private m_sCancel As Single
Private m_sFormHeight As Single
Private m_sShape As Single

'**********************************************************
'   Properties
'**********************************************************
Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Public Property Get DocumentLook() As CDocumentLook
    Set DocumentLook = m_oLook
End Property

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnDetail_Click()
    If Me.btnDetail.Caption = "Show &Detail" Then
        Me.btnDetail.Top = 6030
        Me.btnOK.Top = 6030
        Me.btnCancel.Top = 6030
        Me.Height = 6915
        Me.picGroup.Visible = True
        Me.btnDetail.Caption = "Hide &Detail"
    Else
        Me.btnDetail.Top = m_sDetail
        Me.btnOK.Top = m_sOK
        Me.btnCancel.Top = m_sCancel
        Me.Height = m_sFormHeight
        Me.picGroup.Visible = False
        Me.btnDetail.Caption = "Show &Detail"
    End If
    DoEvents
    Application.ScreenRefresh
End Sub

Private Sub btnOK_Click()
    Me.Cancelled = False
    Me.Hide
    DoEvents
    With m_oLook
        .ApplyColumns = Me.chkApplyNumCols
        .ApplyDifferentFirstPage = Me.chkApplyDiffPage1HF
        .ApplyFont = Me.chkApplyFont
        .ApplyMargins = Me.chkApplyMargins
        .ApplyPageNumber = Me.chkApplyPageNumber
        .ApplyParagraphFormat = Me.chkApplyParaFormat
    End With
End Sub

Private Sub cmbDocType_ItemChange()
    Dim lType As Long
    
    lType = Me.cmbDocType.BoundText
    m_oLook.TypeID = lType
    
    ShowLookValues
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub ShowLookValues()
    Dim xNum As String
    
    With m_oLook.Definition
        Me.lblBottomMarginDesc = .BottomMargin & """"
        Me.lblTopMarginDesc = .TopMargin & """"
        Me.lblLeftMarginDesc = .LeftMargin & """"
        Me.lblRightMarginDesc = .RightMargin & """"
        Me.lblNumColsDesc = .Columns
        Me.lblSpaceBetweenColsDesc = .SpaceBetweenColumns
        Me.lblFontNameDesc = .FontName
        Me.lblFontSizeDesc = .FontSize
        Select Case .Alignment
            Case wdAlignParagraphLeft
                Me.lblParaAlignmentDesc = "Left"
            Case wdAlignParagraphCenter
                Me.lblParaAlignmentDesc = "Center"
            Case wdAlignParagraphRight
                Me.lblParaAlignmentDesc = "Right"
            Case wdAlignParagraphJustify
                Me.lblParaAlignmentDesc = "Justify"
        End Select
        
        Me.lblSpaceAfterDesc = .SpaceAfter & " pt"
        Me.lblSpaceBeforeDesc = .SpaceBefore & " pt"
        
        Me.lblFirstLineDesc = .FirstLineIndent & """"
        
        Select Case .LineSpacing
            Case wdLineSpaceSingle
                Me.lblLineSpaceDesc = "Single"
            Case wdLineSpace1pt5
                Me.lblLineSpaceDesc = "1.5"
            Case wdLineSpaceDouble
                Me.lblLineSpaceDesc = "Double"
        End Select
        
        If .DifferentPage1HeaderFooter Then
            Me.lblUniqueHFDesc = "Yes"
        Else
            Me.lblUniqueHFDesc = "No"
        End If
        
        Select Case .PageNumFormat
            Case mpPageNumberFormat_Arabic
                xNum = "1"
            Case mpPageNumberFormat_UpperRoman
                xNum = "I"
            Case mpPageNumberFormat_LowerRoman
                xNum = "i"
            Case mpPageNumberFormat_UpperAlpha
                xNum = "A"
            Case mpPageNumberFormat_LowerAlpha
                xNum = "a"
            Case mpPageNumberFormat_Cardtext
                xNum = "one"
        End Select
        
        Me.lblNumSampleDesc = .PageNumTextBefore & _
            xNum & .PageNumTextAfter
        
        Select Case .PageNumAlignment
            Case mpPageNumberLocation_Left
                Me.lblNumAlignmentDesc = "Left"
            Case mpPageNumberLocation_Center
                Me.lblNumAlignmentDesc = "Center"
            Case mpPageNumberLocation_Right
                Me.lblNumAlignmentDesc = "Right"
        End Select
        
        Select Case .PageNumLocation
            Case mpPageNumberLocation_Header
                Me.lblNumLocationDesc = "Header"
            Case mpPageNumberLocation_Footer
                Me.lblNumLocationDesc = "Footer"
        End Select
        
        If .PageNumFirstPage Then
            Me.lblNumPage1Desc = "Yes"
        Else
            Me.lblNumPage1Desc = "No"
        End If
      End With
End Sub

Private Sub Form_Activate()
    Dim lType As Long
    Me.cmbDocType.SelectedItem = 0
    lType = Me.cmbDocType.BoundText
    m_oLook.TypeID = lType
    Me.Initializing = False
End Sub

Private Sub Form_Load()
    Me.Cancelled = True
    Set m_oLook = New CDocumentLook
    
    Me.Initializing = True
    Me.cmbDocType.Array = g_oDBs.DocumentLookDefs.ListSource
    ResizeTDBCombo Me.cmbDocType, 6
    
'   store original control positions/sizes
    m_sDetail = Me.btnDetail.Top
    m_sLine = Me.Line2.Y1
    m_sOK = Me.btnOK.Top
    m_sCancel = Me.btnCancel.Top
    m_sFormHeight = Me.Height
    m_sShape = Me.Shape1.Height
    
    mpBase2.MoveToLastPosition Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    mpBase2.SetLastPosition Me
End Sub
