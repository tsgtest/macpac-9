Attribute VB_Name = "mdlFileSystem"
Option Explicit

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function CreateFile Lib "Kernel32.dll" Alias "CreateFileA" ( _
    ByVal lpFileName As String, ByVal dwDesiredAccess As Long, _
    ByVal dwShareMode As Long, ByRef lpSecurityAttributes As Any, _
    ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, _
    ByVal hTemplateFile As Long) As Long
Private Declare Function GetFileTime Lib "Kernel32.dll" (ByVal hFile As Long, _
    ByRef lpCreationTime As Any, ByRef lpLastAccessTime As Any, _
    ByRef lpLastWriteTime As Any) As Long
Private Declare Function CloseHandle Lib "Kernel32.dll" (ByVal hObject As Long) As Long
Private Declare Function FileTimeToSystemTime Lib "Kernel32.dll" ( _
    ByRef lpFileTime As FileTime, ByRef lpSystemTime As SystemTime) As Long

Private Type FileTime
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Private Type SystemTime
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Private Const GENERIC_READ As Long = &H80000000
Private Const OPEN_EXISTING As Long = &H3
Private Const FILE_ATTRIBUTE_NORMAL As Long = &H80
Public Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise mpError.mpError_NullValueNotAllowed, , _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    Set oStr = New CStrings
    GetUserVarPath = oStr.xSubstitute(xPath, "<UserName>", xBuf)
        
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "MPO.mdlFileSystem.GetUserVarPath", Err.Source, _
        Err.Description
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    End If
    
    Set oStr = New CStrings
    
    If xValue <> "" Then
        GetEnvironVarPath = oStr.xSubstitute(xPath, "<" & xToken & ">", xValue)
        
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "MPO.mdlFileSystem.GetEnvironVarPath"
    Exit Function
End Function

Public Function bCreatePath(xFullPath As String) As Boolean
    Dim oFSO As Scripting.FileSystemObject
    Dim xParent As String
    On Error GoTo ProcError
    Set oFSO = New Scripting.FileSystemObject
    If oFSO.FolderExists(xFullPath) Then  ' Folder already created
        bCreatePath = True
        Exit Function
    End If
    On Error Resume Next
    oFSO.CreateFolder xFullPath
    
    If Not oFSO.FolderExists(xFullPath) Then
        ' Couldn't create bottom level folder, call recursively using Parent Folder
        ' Only only the last level may created new
        xParent = oFSO.GetParentFolderName(xFullPath)
        If xParent = Empty Or Right(xParent, 1) = "\" Then ' Parent folder is Drive Root or nothing
            bCreatePath = False
        Else
            If bCreatePath(xParent) Then
                oFSO.CreateFolder xFullPath
                If oFSO.FolderExists(xFullPath) Then
                    bCreatePath = True
                End If
            End If
        End If
    Else
        bCreatePath = True
    End If
    Exit Function
ProcError:
    RaiseError "MPO.mdlFileSystem.bCreatePath"
End Function
Public Sub CopyUserCacheFiles(xUserDir As String, xUserBPDir As String)
    Dim oFSO As Scripting.FileSystemObject
    Dim oFile As Scripting.File
    Dim oCacheFolder As Scripting.Folder
    Dim xCacheFolder As String
    Dim bIsProgramDir As Boolean
    
    On Error GoTo ProcError
    Set oFSO = New Scripting.FileSystemObject
    ' **** JTS 1/27/05: THIS COULD USE AN INI SETTING INSTEAD OF A HARD-CODED PATH
    '//GLOG : 5566 : ceh
    xCacheFolder = mpBase2.xSubstitute(mpBase2.ApplicationDirectory, "\App", "\UserCache")
    
    'check previous location
    If Not oFSO.FolderExists(xCacheFolder) Then
        xCacheFolder = mpBase2.xSubstitute(mpBase2.ProgramDirectory, "\MacPac90\App", "\MacPac90\Tools\UserCache")
        If Not oFSO.FolderExists(xCacheFolder) Then
            Exit Sub  ' No Cache exists
        Else
            xCacheFolder = xCacheFolder & "\Personal"
            If Not oFSO.FolderExists(xCacheFolder) Then Exit Sub
        End If
    End If
    
    If xUserDir = "" Then Exit Sub
    
    'Process files for UserDir
    Set oCacheFolder = oFSO.GetFolder(xCacheFolder)
    
    'Copy User.ini first so settings are written to correct file
    If oFSO.FileExists(oCacheFolder & "\User.ini") Then
        Set oFile = oCacheFolder.Files("User.ini")
        UpdateUserCacheFile oFile, xUserDir, False
    End If
    
    For Each oFile In oCacheFolder.Files
        If UCase(oFile.Name) <> "USER.INI" Then
            UpdateUserCacheFile oFile, xUserDir, False
        End If
    Next oFile
    
    If xUserBPDir = "" Then Exit Sub
    
    'Process files for UserBoilerplateDir
    xCacheFolder = xCacheFolder & "\Boilerplate"
    If Not oFSO.FolderExists(xCacheFolder) Then Exit Sub
    
    Set oCacheFolder = oFSO.GetFolder(xCacheFolder)
    For Each oFile In oCacheFolder.Files
        UpdateUserCacheFile oFile, xUserBPDir, True
    Next oFile
    Exit Sub
ProcError:
    RaiseError "MPO.mdlFileSystem.CopyUserCacheFiles"
End Sub

Private Sub UpdateUserCacheFile(oFile As Scripting.File, xTargetPath As String, bForceOverwrite As Boolean)
    Const xINISECTION As String = "CacheFileVersions"
    Dim xTimeStamp As String
    Dim bCopy As Boolean
    Dim dLastTime As Double
    Dim dFileDate As Double
    Dim bOverwrite As Boolean
    Dim oFSO As New Scripting.FileSystemObject
    
    On Error GoTo ProcError
    ' Get last saved timestamp for each file
    xTimeStamp = mpBase2.GetUserIniNumeric(xINISECTION, oFile.Name)
    dFileDate = CDbl(GetLastModifiedDate(oFile.Path))
    If xTimeStamp <> "" Then
        ' If Timestamp exists, compare to current file time
        On Error Resume Next
        dLastTime = CDbl(xTimeStamp)
        On Error GoTo ProcError
        If dLastTime > 0 Then
            If Replace(dFileDate, ".", "") > Replace(dLastTime, ".", "") Then
                bCopy = True
                'Timestamp is earlier than current - overwrite previous version
                bOverwrite = True
            Else
                bCopy = False
            End If
        Else
            bCopy = True
            bOverwrite = bForceOverwrite
        End If
    Else
        bCopy = True
        'Timestamp wasn't previously saved - copy only if doesn't exist
        bOverwrite = bForceOverwrite
    End If
    If bCopy Then
        On Error Resume Next
        oFSO.CopyFile oFile.Path, xTargetPath & "\" & oFile.Name, bOverwrite
        Err.Clear
        On Error GoTo ProcError
        mpBase2.SetUserIni xINISECTION, oFile.Name, dFileDate
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.mdlFileSystem.UpdateUserCacheFile"
End Sub
Private Function GetLastModifiedDate(ByRef xFile As String) As Date
    'Return file date as universal time
    Dim hFile As Long
    Dim ModifiedTime As FileTime
    Dim SysTime As SystemTime

    ' Open file
    hFile = CreateFile(xFile, GENERIC_READ, 0&, _
        ByVal 0&, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0&)
    If (hFile) Then ' Get create time and convert to VB Date
        If (GetFileTime(hFile, ByVal 0&, ByVal 0&, ModifiedTime)) Then
            Call FileTimeToSystemTime(ModifiedTime, SysTime)
            With SysTime
                GetLastModifiedDate = DateSerial(.wYear, .wMonth, .wDay) + _
                TimeSerial(.wHour, .wMinute, .wSecond)
            End With
        End If
        ' Close file handle
        Call CloseHandle(hFile)
    End If
End Function
