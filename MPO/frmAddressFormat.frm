VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmAddressFormat 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Choose Format of Inserted Names"
   ClientHeight    =   2565
   ClientLeft      =   45
   ClientTop       =   240
   ClientWidth     =   4590
   Icon            =   "frmAddressFormat.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2565
   ScaleWidth      =   4590
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frmInclude 
      Caption         =   "Include"
      Height          =   735
      Left            =   165
      TabIndex        =   8
      Top             =   1095
      Width           =   4320
      Begin VB.CheckBox chkEmail 
         Appearance      =   0  'Flat
         Caption         =   "&Email"
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2595
         TabIndex        =   11
         Top             =   315
         Width           =   885
      End
      Begin VB.CheckBox chkFax 
         Appearance      =   0  'Flat
         Caption         =   "&Fax"
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1470
         TabIndex        =   10
         Top             =   315
         Width           =   885
      End
      Begin VB.CheckBox chkPhone 
         Appearance      =   0  'Flat
         Caption         =   "&Phone"
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   330
         TabIndex        =   9
         Top             =   315
         Width           =   885
      End
   End
   Begin VB.OptionButton optTableFormat 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   210
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   615
      Width           =   210
   End
   Begin VB.OptionButton optListFormat 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   330
      Left            =   210
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   150
      Value           =   -1  'True
      Width           =   270
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   380
      Left            =   3465
      TabIndex        =   13
      Top             =   2070
      Width           =   1000
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   380
      Left            =   2385
      TabIndex        =   12
      Top             =   2070
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbSeparators 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmAddressFormat.frx":058A
      TabIndex        =   2
      Top             =   135
      Width           =   1740
   End
   Begin TrueDBList60.TDBCombo cmbTBLColumns 
      Height          =   600
      Left            =   1905
      OleObjectBlob   =   "frmAddressFormat.frx":4433
      TabIndex        =   6
      Top             =   615
      Width           =   630
   End
   Begin VB.Label lblColumns 
      Caption         =   "Columns"
      Height          =   315
      Left            =   2640
      TabIndex        =   7
      Top             =   690
      Width           =   810
   End
   Begin VB.Label lblTableFormat 
      Caption         =   "&Table Format with"
      Height          =   285
      Left            =   510
      TabIndex        =   5
      Top             =   690
      Width           =   1380
   End
   Begin VB.Label lblSeparators 
      Caption         =   "Separators"
      Height          =   315
      Left            =   3600
      TabIndex        =   3
      Top             =   210
      Width           =   810
   End
   Begin VB.Label lblListFormat 
      Caption         =   "&List Format with"
      Height          =   285
      Left            =   525
      TabIndex        =   1
      Top             =   195
      Width           =   1170
   End
End
Attribute VB_Name = "frmAddressFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Private vars
Private m_bCancelled As Boolean
Private m_bListFormat As Boolean
Private m_xSeparator As String
Private m_iColumnNumber As Integer
Private m_bInitializing As Boolean

'*********PROPERTIES***************************
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Get ListFormat() As Boolean
    ListFormat = m_bListFormat
End Property

Public Property Let Separator(xNew As String)
    m_xSeparator = xNew
End Property

Public Property Get Separator() As String
    If IsNumeric(m_xSeparator) Then
        Separator = Chr(m_xSeparator)
    Else
'       replace tokens
        Separator = xReplaceTokens(m_xSeparator)
    End If
End Property

Public Property Let ColCount(iNew As Integer)
    m_iColumnNumber = iNew
End Property

Public Property Get ColCount() As Integer
    ColCount = m_iColumnNumber
End Property

'*********FORM EVENT CODE***************************
Private Sub cmdCancel_Click()
    Unload Me
    DoEvents
    m_bCancelled = True
End Sub

Private Sub cmbSeparators_GotFocus()
    If Not m_bInitializing Then Me.optListFormat = True
    bEnsureSelectedContent Me.cmbSeparators
End Sub

Private Sub cmbSeparators_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbSeparators, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbSeparators_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbSeparators)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTBLColumns_GotFocus()
    If Not m_bInitializing Then Me.optTableFormat = True
    bEnsureSelectedContent Me.cmbTBLColumns
End Sub

Private Sub cmbTBLColumns_Validate(Cancel As Boolean)
    Dim xVal As String
    xVal = Me.cmbTBLColumns.Text
    If Not IsNumeric(xVal) Then
        Cancel = True
    ElseIf xVal < 1 Or xVal > 4 Then
        Cancel = True
    End If
    If Cancel Then
        xMsg = "Enter a number between 1-4"
        MsgBox xMsg, vbExclamation, App.Title
    End If
End Sub

Private Sub cmdFinish_Click()
    With Me
'       set form props
        m_bListFormat = .optListFormat
        .Separator = Me.cmbSeparators.Columns(2)
        .ColCount = Me.cmbTBLColumns.Text

'       save settings
        SaveSettings

'       finish
        m_bCancelled = False
        .Hide
        DoEvents
        Application.ScreenUpdating = True
    End With
End Sub

Private Sub Form_Activate()
    Dim xVer As String
    
    m_bCancelled = True
    m_bInitializing = True
    
'   list Separators
    With Me.cmbSeparators
        .Array = g_oDBs.Lists("SeparatorList").ListItems.Source
        .Rebind
        DoEvents
    End With
    ResizeTDBCombo cmbSeparators, 6

    With Me.cmbTBLColumns
        .Array = xarStringToxArray("1|2|3|4")
        .Rebind
        DoEvents
    End With
    ResizeTDBCombo cmbTBLColumns, 6

'   get previously saved settings
    GetSettings
    
    If Me.optTableFormat Then
        Me.cmbTBLColumns.SetFocus
    Else
        Me.cmbSeparators.SetFocus
    End If
    
    xVer = GetMacPacIni("CI", "CIVersion")
    
'   hide checkboxes if not CI2x
    If Not (xVer Like "2.*") Then
        Me.frmInclude.Visible = False
        Me.cmdCancel.Top = Me.cmdCancel.Top - 750
        Me.cmdFinish.Top = Me.cmdFinish.Top - 750
        Me.Height = Me.Height - 750
    End If
    
    
    m_bInitializing = False
    
End Sub

Private Sub SaveSettings()
    mpBase2.SetUserIni "AddressBookNames", "ListFormat", Me.optListFormat
    mpBase2.SetUserIni "AddressBookNames", "Separator", Me.cmbSeparators.BoundText
    mpBase2.SetUserIni "AddressBookNames", "ColCount", Me.cmbTBLColumns.Text
    mpBase2.SetUserIni "AddressBookNames", "IncludePhone", Me.chkPhone.Value
    mpBase2.SetUserIni "AddressBookNames", "IncludeFax", Me.chkFax.Value
    mpBase2.SetUserIni "AddressBookNames", "IncludeEmail", Me.chkEmail.Value
End Sub

Private Sub GetSettings()
    Dim xListFormat As String
    Dim vSep As String
    Dim vCol As String
    Dim oXHelp As XHelper
    Dim lRowFound As Long
    
    On Error Resume Next
    
'   initialize an XArray Helper object
    Set oXHelp = New XHelper

    xListFormat = mpBase2.GetUserIni("AddressBookNames", "ListFormat")

    If xListFormat = "False" Then
        Me.optTableFormat = True
    End If

    vSep = mpBase2.GetUserIni("AddressBookNames", "Separator")
    vCol = mpBase2.GetUserIni("AddressBookNames", "ColCount")

    If vSep <> "" Then
        Me.cmbSeparators.BoundText = vSep
    Else
        Me.cmbSeparators.Bookmark = 0
    End If

    If vCol <> "" Then
        Me.cmbTBLColumns.Text = vCol
    Else
        Me.cmbTBLColumns.Bookmark = 0
    End If
    
    Me.chkPhone.Value = mpBase2.GetUserIni("AddressBookNames", "IncludePhone")
    Me.chkFax.Value = mpBase2.GetUserIni("AddressBookNames", "IncludeFax")
    Me.chkEmail.Value = mpBase2.GetUserIni("AddressBookNames", "IncludeEmail")

End Sub
