VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmServiceType 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Service Document"
   ClientHeight    =   3120
   ClientLeft      =   36
   ClientTop       =   360
   ClientWidth     =   5784
   Icon            =   "frmServiceType.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3120
   ScaleWidth      =   5784
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TrueDBList60.TDBCombo cmbLevel0 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmServiceType.frx":058A
      TabIndex        =   1
      Top             =   180
      Width           =   3930
   End
   Begin TrueDBList60.TDBList lstFavorites 
      Height          =   2010
      Left            =   2280
      OleObjectBlob   =   "frmServiceType.frx":2D57
      TabIndex        =   6
      Top             =   3510
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.CheckBox chkFavorites 
      Caption         =   "Fa&vorites"
      Height          =   400
      Left            =   90
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   3135
      Visible         =   0   'False
      Width           =   795
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4680
      TabIndex        =   5
      Top             =   2625
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   3615
      TabIndex        =   4
      Top             =   2625
      Width           =   1000
   End
   Begin TrueDBList60.TDBList lstServiceTypes 
      Height          =   1815
      Left            =   1770
      OleObjectBlob   =   "frmServiceType.frx":4EC3
      TabIndex        =   3
      Top             =   660
      Width           =   3930
   End
   Begin VB.CommandButton btnAddToFaves 
      Caption         =   "&Add..."
      Height          =   400
      Left            =   960
      TabIndex        =   8
      Top             =   3135
      Visible         =   0   'False
      Width           =   765
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "D&elete..."
      Height          =   400
      Left            =   960
      TabIndex        =   10
      Top             =   3060
      Visible         =   0   'False
      Width           =   765
   End
   Begin VB.Label lblType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "T&ype:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   2
      Top             =   675
      Width           =   1275
   End
   Begin VB.Label lblLevel0 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&State:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   0
      Top             =   240
      Width           =   1275
   End
   Begin VB.Label lblFavorites 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "&Favorites:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   1875
      TabIndex        =   7
      Top             =   2775
      Visible         =   0   'False
      Width           =   1410
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   3930
      Left            =   -30
      Top             =   -375
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmServiceType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_bInitializing As Boolean
Private m_bDefRetrieved As Boolean
Private m_bFaveRetrieved As Boolean
Private m_bSettingAllLevels As Boolean
Private m_xIniSection As String

'**********************************************************
'   Properties
'**********************************************************
Public Property Get ServiceType() As Long
    ServiceType = Me.lstServiceTypes.BoundText
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub btnOK_Click()
    SetDefaults
    Me.Cancelled = False
    Me.Hide
    DoEvents
End Sub

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub cmbLevel0_ItemChange()
    Dim lL0 As Long
    
    On Error GoTo ProcError
    lL0 = CLng(Me.cmbLevel0.BoundText)
    
    ShowServiceTypes
    
    Exit Sub
    
ProcError:
    RaiseError "MPO.frmServiceType.cmbLevel0_ItemChange"
    Exit Sub

End Sub

Private Sub cmbLevel0_Mismatch(NewEntry As String, Reposition As Integer)
    CorrectTDBComboMismatch Me.cmbLevel0, Reposition
End Sub

Private Sub cmbLevel0_GotFocus()
    OnControlGotFocus Me.cmbLevel0
End Sub

Private Sub Form_Activate()
    Dim lID As Long
    Dim lTypeID As Long
    Dim xTemplate As String
    Dim oService As MPO.CService
    Dim i As Integer
    Dim xServiceType As String
    Dim xServiceFilter As String
    Dim bFound As Boolean
    
    If m_bInitializing Then
        LockWindowUpdate Me.hwnd
'       get default level 0
        xTemplate = g_oTemplates.ItemFromClass("CService").FileName
        
        ' Create Service Object to check for existing type
        Set oService = New MPO.CService
        lTypeID = oService.TypeID
        Set oService = Nothing
        
        On Error Resume Next
        If lTypeID > 0 Then
            lID = g_oDBs.ServiceDefs.Item(lTypeID).Level0
        Else
            xTemplate = g_oTemplates.ItemFromClass("CService").FileName
            On Error Resume Next
            lID = GetUserIni(xTemplate, "Level0")
        End If
        On Error GoTo 0
        
        If lID Then
            Me.cmbLevel0.BoundText = lID
            DoEvents
            If lTypeID Then
                
                With lstServiceTypes
                    For i = 0 To .Array.Count(1) - 1
                        If .Array.Value(i, 1) = lTypeID Then
                            .SelectedItem = i
                            bFound = True
                            Exit For
                        End If
                    Next i
                    
                    If Not bFound Then
                '       get Service type
                        xServiceType = GetUserIni(xTemplate, "ServiceType")
                        If xServiceType <> "" Then
                            Me.lstServiceTypes.BoundText = xServiceType
                        Else
                            Me.lstServiceTypes.SelectedItem = 0
                        End If
                    End If
                    
                End With
                
            Else
        '       get Service type
                xServiceType = GetUserIni(xTemplate, "ServiceType")
                If xServiceType <> "" Then
                    Me.lstServiceTypes.BoundText = xServiceType
                Else
                    Me.lstServiceTypes.SelectedItem = 0
                End If
            End If
        Else
'           select first item in list
            Me.cmbLevel0.SelectedItem = 0
        End If
        
        
        DoEvents
        LockWindowUpdate &O0
        
'       UNREM THESE LINES FOR THE 7/5/00 RELEASE
        EchoOn
        Application.ScreenUpdating = True
        Screen.MousePointer = vbDefault
        m_bInitializing = False
    End If
End Sub

Private Sub Form_Load()
    Dim bFaves As Boolean
    Dim iFaves As Integer
    
    On Error GoTo ProcError
    m_bInitializing = True
    m_bCancelled = True
    
'   load lists
    On Error Resume Next
    
    '---9.6.1 - only display level 0 for states actually configured in tblVerificationTypes
    With g_oDBs.Lists
        Dim iCount
        iCount = .Item("ServiceStatesConfigured").ListItems.Count
        If iCount > 0 Then
            Me.cmbLevel0.Array = _
                .Item("ServiceStatesConfigured").ListItems.Source
        Else
            Me.cmbLevel0.Array = _
                .Item("PleadingCourtsLevel0").ListItems.Source
       End If

        On Error GoTo ProcError
    End With
    
    Me.lstFavorites.Array = g_oDBs.PleadingFavorites.List
        
    ResizeTDBCombo Me.cmbLevel0, 6
    

'   show panel last displayed
    m_xIniSection = g_oTemplates.ItemFromClass("CService").ID
    Exit Sub
    
ProcError:
    RaiseError "MPO.frmServiceType.FormLoad"
    Exit Sub
End Sub

Private Sub SetDefaults()
'sets level 0-3 defaults to user.ini
'based on current values of controls

    SetUserIni m_xIniSection, "Level0", Me.cmbLevel0.BoundText
    SetUserIni m_xIniSection, "ServiceType", Me.lstServiceTypes.BoundText
End Sub

Private Sub GetPanel1Defaults()
'gets level 0-3 defaults from user.ini-
'sets controls to supplied values-
'selects first item in each list if no
'supplied value
    Dim lLevel0 As Long
    
'   get sticky fields from ini
    On Error Resume Next
    lLevel0 = GetUserIni(m_xIniSection, "Level0")
    
    m_bSettingAllLevels = True
    
'   select item if supplied, else select first item in list
    If lLevel0 Then
        Me.cmbLevel0.BoundText = lLevel0
    Else
        Me.cmbLevel0.SelectedItem = 0
    End If
    
    ShowServiceTypes
    
    m_bSettingAllLevels = False
    
'   note that defaults have been retrieved -
'   this is used in chkFavorites_Click to determine
'   whether to retrieve defaults
    m_bDefRetrieved = True
End Sub

Private Sub ShowServiceTypes()
    Dim oList As mpDB.CList
    Dim xSQL As String
    Dim xState As String
    Dim lL0 As Long
    
    On Error GoTo ProcError
    
    lL0 = CLng(Me.cmbLevel0.BoundText)
    With g_oDBs.Lists("ServiceTypes")
        .FilterValue = lL0
        .Refresh
        Me.lstServiceTypes.Array = .ListItems.Source
        With Me.lstServiceTypes
            .Rebind
            If .Array.Count(1) Then
                .Row = 0
            End If
            Me.btnOK.Enabled = .Row > -1
        End With
    End With
    
    DoEvents
    Exit Sub
ProcError:
    RaiseError "MPO.frmServiceType.ShowServiceType"
End Sub

Private Sub lstFavorites_DblClick()
    btnOK_Click
End Sub

Private Sub lstServiceTypes_DblClick()
    btnOK_Click
End Sub

Private Sub lstServiceTypes_GotFocus()
    OnControlGotFocus Me.lstServiceTypes
End Sub
