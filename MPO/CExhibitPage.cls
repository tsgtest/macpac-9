VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CExhibitPage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CExhibitPage Class
'   created 3/21/00 by Jeffrey Sweetland
'
'**********************************************************
Const mpExhibitsBPFile As String = "BusinessExhibits.mbp"

Private m_xExhibitNumber As String
Private m_xExhibitTitle As String
Private m_oDoc As CDocument
Private m_iLocation As Integer
Public Property Get ExhibitNumber() As String
    ExhibitNumber = m_xExhibitNumber
End Property
Public Property Let ExhibitNumber(xNew As String)
    m_xExhibitNumber = xNew
    If g_bForceItemUpdate Then _
        m_oDoc.EditItem "zzmpExhibitNumber", m_xExhibitNumber, , False
    
End Property
Public Property Get ExhibitTitle() As String
    ExhibitTitle = m_xExhibitTitle
End Property
Public Property Let ExhibitTitle(xNew As String)
    m_xExhibitTitle = xNew
    If g_bForceItemUpdate Then
        With m_oDoc
            .EditItem "zzmpExhibitTitle", m_xExhibitTitle, , False
        End With
    End If
End Property
Public Property Let Location(iNew As mpDocPositions)
    m_iLocation = iNew
End Property
Public Property Get Location() As mpDocPositions
    Location = m_iLocation
End Property

Public Sub Finish()
    Dim i As Integer
    
    For i = 0 To 2
        With Me
            .ExhibitNumber = "666"
            .ExhibitTitle = "Chalrie"
            If i = 0 Then
                .CreateExhibitSection
            Else
                .Document.Selection.InsertBreak wdPageBreak
            End If
            With .Document
                .InsertBoilerplate mpExhibitsBPFile, ""
                Refresh
                ' Workaround for now:  page number is not being
                ' pulled in if added to top of document
                If Me.Location = mpDocPosition_BOF Then
                    If .Selection.Sections(1).PageSetup.DifferentFirstPageHeaderFooter Then _
                        .rngInsertPageNumber .Selection.Sections(1).Footers(wdHeaderFooterFirstPage).Range, _
                            "", "", , True
                    .rngInsertPageNumber .Selection.Sections(1).Footers(wdHeaderFooterPrimary).Range, _
                        "", "", , True
                ElseIf Me.Location <> mpDocPosition_Selection Then
                    .Selection.EndOf wdSection
                End If
            End With
            .Location = mpDocPosition_Selection
        End With
    Next i
        
    With m_oDoc
        .Selection.StartOf wdSection
'       delete any hidden text
        .DeleteHiddenText , , , True
        .RemoveFormFields
        .ClearBookmarks
    End With
    
End Sub

Public Sub CreateExhibitSection()
    With m_oDoc
        .CreateSection Me.Location
        Select Case Me.Location
            Case mpDocPosition_BOF
                .Selection.StartOf wdStory
                .Selection.Sections(1).Range.Style = "Normal"
                With .Selection.Next(wdSection, 1).Sections(1).Footers(1).PageNumbers
                    .RestartNumberingAtSection = True
                    .StartingNumber = 1
                End With
            Case mpDocPosition_EOF
                .Selection.EndOf wdStory
                .Selection.Style = "Normal"
            Case Else
'               Reset style of section break
                If .Selection.Sections(1).Range.Characters.Last = Chr(12) Then
                    .Selection.Sections(1).Range.Paragraphs.Last.Style = "Normal"
                End If
                .UnlinkFooters .Selection.Sections(1)
                .UnlinkHeaders .Selection.Sections(1)
                With .Selection.Sections(1).Footers(1).PageNumbers
                    .RestartNumberingAtSection = True
                    .StartingNumber = 1
                End With
                .Selection.Move wdCharacter, -1
                .CreateSection mpDocPosition_Selection
                .ClearHeaders .Selection.Sections(1), True
                With .Selection.Sections(1).Footers(1).PageNumbers
                    .RestartNumberingAtSection = True
                    .StartingNumber = 1
                End With
        End Select
        
    End With
    
End Sub
Public Sub FinishNEW()
    Dim oAT As Word.AutoTextEntry
    Dim bSaved As Boolean
    
    bSaved = Word.NormalTemplate.Saved
    With m_oDoc
'       insert at eof, then move to final location later-
'       this is the only way that Word will bring
'       in headers and footers
        .CreateSection mpDocPosition_EOF
        .Selection.EndOf wdStory
        
'       Insert bp
        m_oDoc.InsertBoilerplate mpExhibitsBPFile, ""
        
'       fill-in
        Refresh
        
'       create temp autotext
        Dim oRng As Word.Range
        Set oRng = Word.ActiveDocument.Sections.Last.Range
        oRng.MoveStart wdCharacter, -1
        oRng.InsertAfter Chr(12)
        Set oAT = Word.NormalTemplate.AutoTextEntries.Add("zzmpTempExhibit", oRng)
'       create actual location if necessary
        Select Case Me.Location
            Case mpDocPosition_BOF
'               insert at eof, then move to bof later-
'               this is the only way that Word will bring
'               in headers and footers
                oRng.Delete
                .CreateSection mpDocPosition_BOF
                Set oRng = Word.ActiveDocument.Content
                oRng.StartOf
                Word.NormalTemplate.AutoTextEntries("zzmpTempExhibit").Insert oRng, True
            Case mpDocPosition_EOF
            Case mpDocPosition_Selection
                oRng.MoveStart wdCharacter, -1
                oRng.Delete
                .CreateSection mpDocPosition_Selection
                ' Reset style of section break
                If .Selection.Sections(1).Range.Characters.Last = Chr(12) Then
                    .Selection.Sections(1).Range.Characters.First.Delete
                End If
                .Selection.Sections(1).Range.Paragraphs.Last.Style = "Normal"
                Selection.StartOf wdSection
                Word.NormalTemplate.AutoTextEntries("zzmpTempExhibit").Insert Selection.Range, True
        End Select
        With Selection.Sections(1).Footers(wdHeaderFooterFirstPage) _
            .PageNumbers
                .RestartNumberingAtSection = True
                .StartingNumber = 1
        End With
        
        
'      delete any hidden text
        .DeleteHiddenText , True, , True
        .RemoveFormFields
        .ClearBookmarks
        
'       delete autotext
        Word.NormalTemplate.AutoTextEntries("zzmpTempExhibit").Delete
        
'       return normal template dirt
        Word.NormalTemplate.Saved = bSaved
    End With
    
End Sub
Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
End Sub
Private Sub Class_Terminate()
    Set m_oDoc = Nothing
End Sub
Private Sub Refresh()
    g_bForceItemUpdate = True
    With Me
        .ExhibitNumber = .ExhibitNumber
        .ExhibitTitle = .ExhibitTitle
    End With
    g_bForceItemUpdate = False
End Sub
Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function
