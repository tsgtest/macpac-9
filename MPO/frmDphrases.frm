VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmDPhrases 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert/Edit Delivery Phrases"
   ClientHeight    =   3000
   ClientLeft      =   3828
   ClientTop       =   996
   ClientWidth     =   5460
   Icon            =   "frmDphrases.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3000
   ScaleWidth      =   5460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin mpControls.MultiLineCombo mlcDeliveryPhrase 
      Height          =   945
      Left            =   1770
      TabIndex        =   1
      Top             =   240
      Width           =   3585
      _ExtentX        =   6329
      _ExtentY        =   1672
      ListRows        =   6
      FontName        =   "MS Sans Serif"
      FontBold        =   0   'False
      FontSize        =   7.8
      FontItalic      =   0   'False
      FontUnderline   =   0   'False
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4350
      TabIndex        =   7
      Top             =   2505
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3285
      TabIndex        =   6
      Top             =   2505
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbAction 
      Height          =   600
      Left            =   1755
      OleObjectBlob   =   "frmDphrases.frx":058A
      TabIndex        =   3
      Top             =   1380
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbScope 
      Height          =   600
      Left            =   1755
      OleObjectBlob   =   "frmDphrases.frx":2433
      TabIndex        =   5
      Top             =   1875
      Width           =   3600
   End
   Begin VB.Label lblScope 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Apply &To:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   825
      TabIndex        =   4
      Top             =   1935
      Width           =   690
   End
   Begin VB.Label lblAction 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Action:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1005
      TabIndex        =   2
      Top             =   1440
      Width           =   510
   End
   Begin VB.Label lblDPhrases 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Phrases:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   825
      TabIndex        =   0
      Top             =   255
      Width           =   690
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6975
      Left            =   -30
      Top             =   -390
      Width           =   1695
   End
End
Attribute VB_Name = "frmDPhrases"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_oDoc As CDocument
Private m_iPhraseType As mpDPhraseTypes '9.7.1 #4024
Private m_xStyle As String '9.7.1 #4024
Private m_xHeaderStyle As String '9.7.1 #4024

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property
Public Property Let PhraseType(iNew As mpDPhraseTypes)
    m_iPhraseType = iNew '9.7.1 #4024
End Property
Public Property Get PhraseType() As mpDPhraseTypes
    PhraseType = m_iPhraseType '9.7.1 #4024
End Property
Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnOK_GotFocus()
    OnControlGotFocus Me.btnOK
End Sub

Private Sub cmbAction_Change()
    bUpdateScope
End Sub

Private Sub cmbAction_GotFocus()
    OnControlGotFocus Me.cmbAction
End Sub

Private Sub cmbAction_LostFocus()
    bUpdateScope
End Sub

Private Sub cmbScope_GotFocus()
    OnControlGotFocus Me.cmbScope
End Sub

'**********************************************************
'   Form Procedures
'**********************************************************
Private Sub Form_Activate()
'   select Action per presence of Dphrases in document
    If Me.mlcDeliveryPhrase.Text = "" Then
        Me.cmbAction.SelectedItem = 1
    Else
        Me.cmbAction.SelectedItem = 0
    End If
    
    Me.cmbScope.SelectedItem = 0
    
    bUpdateScope

    Me.Initializing = False
End Sub

Private Sub Form_Load()
    Dim xDocDPhrase As String
    Dim oSty As Word.Style
    Dim bCaps As Boolean
    Dim bSaved As Boolean
    Dim xCaption As String '9.7.1 #4024
    
    On Error Resume Next
    Set m_oDoc = New CDocument
    Me.Initializing = True
    
'*  fill list
    '9.7.1 #4024
    Select Case m_iPhraseType
        Case mpPhrasesCombined
            mlcDeliveryPhrase.List = g_oDBs.Lists.Item("DPhrases").ListItems.Source
            On Error Resume Next
            If g_oDBs.Lists.Exists("DPhraseStyles") Then
                m_xStyle = g_oDBs.Lists("DPhraseStyles").ListItems.Item(, "Phrase1").DisplayText
                m_xHeaderStyle = g_oDBs.Lists("DphraseStyles").ListItems.Item(, "Phrase1").SuppValue
            End If
            If g_oDBs.Lists.Exists("DPhraseCaptions") Then
                xCaption = g_oDBs.Lists("DPhraseCaptions").ListItems.Item(, "Phrase1").DisplayText
            End If
            If m_xStyle = "" Then
                m_xStyle = "Delivery Phrase"
            End If
            If m_xHeaderStyle = "" Then
                m_xHeaderStyle = "Header Delivery Phrase"
            End If
            If xCaption = "" Then
                xCaption = "Insert/Edit Delivery Phrases"
            End If
        Case mpDPhrases
            If g_oDBs.Lists.Exists("DPhrases1") Then
                mlcDeliveryPhrase.List = g_oDBs.Lists.Item("DPhrases1").ListItems.Source
            Else
                mlcDeliveryPhrase.List = g_oDBs.Lists.Item("DPhrases").ListItems.Source
            End If
            If g_oDBs.Lists.Exists("DPhraseStyles") Then
                m_xStyle = g_oDBs.Lists("DPhraseStyles").ListItems.Item(, "Phrase1").DisplayText
                m_xHeaderStyle = g_oDBs.Lists("DphraseStyles").ListItems.Item(, "Phrase1").SuppValue
            End If
            If g_oDBs.Lists.Exists("DPhraseCaptions") Then
                xCaption = g_oDBs.Lists("DPhraseCaptions").ListItems.Item(, "Phrase1").DisplayText
            End If
            If m_xStyle = "" Then
                m_xStyle = "Delivery Phrase"
            End If
            If m_xHeaderStyle = "" Then
                m_xHeaderStyle = "Header Delivery Phrase"
            End If
            If xCaption = "" Then
                xCaption = "Insert/Edit Delivery Phrases"
            End If
        Case mpCPhrases
            If g_oDBs.Lists.Exists("DPhrases2") Then
                mlcDeliveryPhrase.List = g_oDBs.Lists.Item("DPhrases2").ListItems.Source
            Else
                mlcDeliveryPhrase.List = g_oDBs.Lists.Item("DPhrases").ListItems.Source
            End If
            If g_oDBs.Lists.Exists("DPhraseStyles") Then
                m_xStyle = g_oDBs.Lists("DPhraseStyles").ListItems.Item(, "Phrase2").DisplayText
                m_xHeaderStyle = g_oDBs.Lists("DphraseStyles").ListItems.Item(, "Phrase2").SuppValue
            End If
            If g_oDBs.Lists.Exists("DPhraseCaptions") Then
                xCaption = g_oDBs.Lists("DPhraseCaptions").ListItems.Item(, "Phrase2").DisplayText
            End If
            If m_xStyle = "" Then
                m_xStyle = "Confidential Phrase"
            End If
            If m_xHeaderStyle = "" Then
                m_xHeaderStyle = "Header Confidential Phrase"
            End If
            If xCaption = "" Then
                xCaption = "Insert/Edit Confidential Phrases"
            End If
    End Select
    Me.Caption = xCaption
    
'9.7.1 - 2684
    Select Case m_iPhraseType
        Case mpPhrasesCombined, mpDPhrases
            Me.mlcDeliveryPhrase.Text = m_oDoc.RetrieveItem("DeliveryPhrases", _
                                                            m_oDoc.GetDPhraseSegmentName(ActiveDocument.AttachedTemplate))
        Case ""
            Me.mlcDeliveryPhrase.Text = m_oDoc.RetrieveItem("DeliveryPhrases2", _
                                                            m_oDoc.GetDPhraseSegmentName(ActiveDocument.AttachedTemplate))
    End Select
''   remove all caps from dphrase style if necessary -
''   this allows us to present dphrase text not in all caps
'    On Error Resume Next
'    Set oSty = Word.ActiveDocument.Styles(m_xStyle)
'    On Error GoTo 0
'
'    If Not oSty Is Nothing Then
'        bSaved = Word.ActiveDocument.Saved
'        bCaps = oSty.Font.AllCaps
'        oSty.Font.AllCaps = False
'    End If
'
''*  get dphrase text
'    xDocDPhrase = m_oDoc.GetDocStyleText(m_xStyle, , , , True)
'
''   return style to original state
'    If Not oSty Is Nothing Then
'        oSty.Font.AllCaps = bCaps
'        Word.ActiveDocument.Saved = bSaved
'    End If
'    xDocDPhrase = xTrimTrailingChrs(xDocDPhrase, vbCr, True)

'*  set action controls
    With Me.cmbAction
        .Array = xarStringToxArray( _
            "Replace Existing Phrases|Insert New Phrases at Cursor")
    End With
    
'*  set scope controls
    With Me.cmbScope
        .Array = xarStringToxArray("Current Section|Entire Document")
    End With
    
'   resize lists
    ResizeTDBCombo Me.cmbAction, 3
    ResizeTDBCombo Me.cmbScope, 3
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    Set m_oDoc = Nothing
    Set g_oPrevControl = Nothing
End Sub

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
End Sub

Private Sub btnOK_Click()
    UpdateDPhrases
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateDPhrases()
    Dim bRet As Boolean
    Dim x As String
    Dim xSegment As String
    
    x = Me.mlcDeliveryPhrase.Text
    
    If InStr(UCase(Me.cmbAction.Text), "INSERT") Then
        m_oDoc.InsertText x, m_xStyle, , True
    Else
        If InStr(UCase(Me.cmbScope.Text), "SECTION") Then
            bRet = m_oDoc.bReplaceStyle(x, m_xStyle, , True)
            bRet = m_oDoc.bReplaceStyle(x, m_xHeaderStyle, , True)
        Else
            bRet = m_oDoc.bReplaceStyle(x, m_xStyle)
            bRet = m_oDoc.bReplaceStyle(x, m_xHeaderStyle)
        End If
    End If

    '9.7.1 - #2684
    Select Case m_iPhraseType
        Case mpDPhrases, mpPhrasesCombined
            m_oDoc.SaveItem "DeliveryPhrases", _
                            m_oDoc.GetDPhraseSegmentName(ActiveDocument.AttachedTemplate), , x
        Case mpCPhrases
            m_oDoc.SaveItem "DeliveryPhrases2", _
                            m_oDoc.GetDPhraseSegmentName(ActiveDocument.AttachedTemplate), , x
        Case Else
    End Select
End Sub

Private Sub bUpdateScope()
    Dim bEnable As Boolean
    
    bEnable = InStr(UCase(Me.cmbAction.Text), "REPLACE")
    
    With Me
        .cmbScope.Enabled = bEnable
        .lblScope.Enabled = bEnable
    End With
    
End Sub

Private Sub cmbAction_ItemChange()
    bUpdateScope
End Sub

Private Sub cmbAction_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAction, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAction_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbAction)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbScope_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbScope, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbScope_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbScope)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_BeforeItemAdded(xItem As String)
    On Error GoTo ProcError
    xItem = mpBase2.AppendToDPhrase(xItem)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_GotFocus()
    OnControlGotFocus Me.mlcDeliveryPhrase
End Sub
