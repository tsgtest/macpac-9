VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEnvironment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CEnvironment Class
'   created 12/31/99 by Daniel Fisherman -
'   momshead@earthlink.net
'   Contains properties and methods that
'   define the CEnvironment Class - those
'   procedures that concern the Word
'   Processing environment
'**********************************************************

'**********************************************************
Private m_bShowAll As Boolean
Private m_bShowBookmarks As Boolean
Private m_bShowHidden As Boolean
Private m_bShowFieldCodes As Boolean
Private m_bDisplayHorizontalScrollBar As Boolean
Private m_bDisplayVerticalScrollBar As Boolean
Private m_iView As WdViewType
Private m_sZoom As Single
Private m_iBrowser As WdBrowseTarget
Private m_oSelRange As Word.Range
Private m_bytProtectType As WdProtectionType
Private m_bIsProtected As Boolean
Private m_xPWD As String


Public Property Let ProtectionType(bytNew As WdProtectionType)
    m_bytProtectType = bytNew
End Property

Public Property Get ProtectionType() As WdProtectionType
    ProtectionType = m_bytProtectType
End Property

Private Property Get DocumentIsProtected() As Boolean
    DocumentIsProtected = Not (ProtectionType = wdNoProtection)
End Property

Private Property Let ShowAll(bNew As Boolean)
    m_bShowAll = bNew
End Property

Private Property Get ShowAll() As Boolean
    ShowAll = m_bShowAll
End Property

Private Property Let DisplayHorizontalScrollBar(bNew As Boolean)
    m_bDisplayHorizontalScrollBar = bNew
End Property

Private Property Get DisplayHorizontalScrollBar() As Boolean
    DisplayHorizontalScrollBar = m_bDisplayHorizontalScrollBar
End Property

Private Property Let DisplayVerticalScrollBar(bNew As Boolean)
    m_bDisplayVerticalScrollBar = bNew
End Property

Private Property Get DisplayVerticalScrollBar() As Boolean
    DisplayVerticalScrollBar = m_bDisplayVerticalScrollBar
End Property

Private Property Let ShowBookmarks(bNew As Boolean)
    m_bShowBookmarks = bNew
End Property

Private Property Get ShowBookmarks() As Boolean
    ShowBookmarks = m_bShowBookmarks
End Property

Private Property Let ShowHidden(bNew As Boolean)
    m_bShowHidden = bNew
End Property

Private Property Get ShowHidden() As Boolean
    ShowHidden = m_bShowHidden
End Property

Private Property Let ShowFieldCodes(bNew As Boolean)
    m_bShowFieldCodes = bNew
End Property

Private Property Get ShowFieldCodes() As Boolean
    ShowFieldCodes = m_bShowFieldCodes
End Property

Private Property Let View(iNew As WdViewType)
    m_iView = iNew
End Property

Private Property Get View() As WdViewType
    View = m_iView
End Property

Private Property Let BrowserTarget(iNew As WdBrowseTarget)
    m_iBrowser = iNew
End Property

Private Property Get BrowserTarget() As WdBrowseTarget
    BrowserTarget = m_iBrowser
End Property

Private Property Let Zoom(sngNew As Single)
    m_sZoom = sngNew
End Property

Private Property Get Zoom() As Single
    Zoom = m_sZoom
End Property

Public Property Set CurrentSelection(oNew As Word.Range)
    Set m_oSelRange = oNew.Duplicate
End Property

Public Property Get CurrentSelection() As Word.Range
    Set CurrentSelection = m_oSelRange
End Property

Public Sub SwitchToPrintView()
    Word.ActiveDocument.ActiveWindow.View.Type = wdPrintPreview
End Sub

Public Sub SetMacPacState(Optional ByVal bShowAll As Boolean = False, _
                          Optional ByVal bShowHiddenText As Boolean = False, _
                          Optional ByVal bShowBookmarks As Boolean = False, _
                          Optional ByVal bUnprotect As Boolean = True)
    Dim bRet As Boolean
    Dim lErr As Long
    
    On Error GoTo ProcError
    
    With Word.ActiveDocument.ActiveWindow
        .DisplayHorizontalScrollBar = True
        .DisplayVerticalScrollBar = True
        With .View
            .ShowAll = bShowAll
            .ShowHiddenText = bShowHiddenText
            .ShowBookmarks = bShowBookmarks
            .ShowFieldCodes = False
        End With
    End With
    
    If bUnprotect Then
        On Error Resume Next
        UnProtectDocument
        
'       get any err that may have arisen during unprotect
        lErr = Err.Number
        On Error GoTo ProcError
        
        If lErr Then
'           restore the state of Word
            RestoreState , False

            Err.Raise lErr
        End If
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CEnvironment.SetMacPacState"
    Exit Sub
End Sub

Private Sub GetState(Optional ShowFieldCodes As Boolean = False)
'gets values from Word environment
    With Word.ActiveDocument.ActiveWindow
        DisplayHorizontalScrollBar = .DisplayHorizontalScrollBar
        DisplayVerticalScrollBar = .DisplayVerticalScrollBar
        With .View
            View = .Type
            Zoom = .Zoom
            ShowAll = .ShowAll
            ShowBookmarks = .ShowBookmarks
            ShowHidden = .ShowHiddenText
            ShowFieldCodes = .ShowFieldCodes
        End With
    End With
    
    With Application
        On Error Resume Next
        BrowserTarget = .Browser.Target
        On Error GoTo 0
        Set CurrentSelection = .Selection.Range
    End With
    
'   get protection
    ProtectionType = Word.ActiveDocument.ProtectionType
End Sub

Public Sub RestoreState(Optional ByVal bSelect As Boolean = False, _
                        Optional ByVal bClearUndo As Boolean = True, _
                        Optional ByVal bSeekMain As Boolean = True, _
                        Optional ByVal bApplicationStateOnly As Boolean = False)
'applies all values to Word environment-
'ignores doc-specific properties if specified
    On Error GoTo ProcError
    
    If Not bApplicationStateOnly Then
        With Word.ActiveDocument.ActiveWindow
            'GLOG : 5395 : ceh - moved before View reset
            If bSeekMain Then
                On Error Resume Next
                .View.SeekView = wdSeekMainDocument
                On Error GoTo ProcError
            End If
            
            .DisplayHorizontalScrollBar = DisplayHorizontalScrollBar
            .DisplayVerticalScrollBar = DisplayVerticalScrollBar
            With .View
                If .Type <> View Then
                    .Type = View
                End If
                .Zoom = Zoom
                .ShowAll = ShowAll
                .ShowHiddenText = ShowHidden
                .ShowBookmarks = ShowBookmarks
                .ShowFieldCodes = ShowFieldCodes
            End With
        End With
        
        If bClearUndo Then
            ActiveDocument.UndoClear
        End If
        
'        If bSeekMain Then
'            On Error Resume Next
'            Word.ActiveDocument.ActiveWindow.View.SeekView = wdSeekMainDocument
'        End If
    End If
    
    With Application
        On Error Resume Next
        .Browser.Target = BrowserTarget
        On Error GoTo ProcError
        If bSelect Then
            CurrentSelection.Select
        End If
    End With
       
'   restore original protection status
    RestoreProtection
    Exit Sub
    
ProcError:
    RaiseError "MPO.CEnvironment.RestoreState"
    Exit Sub
End Sub

Public Sub RestoreProtection()
'restores original protection status
    On Error GoTo ProcError
    If ProtectionType <> wdNoProtection Then
        If Word.ActiveDocument.ProtectionType = wdNoProtection Then
            If m_xPWD = "zzmpDummy" Then
                m_xPWD = ""
            End If
            Word.ActiveDocument.Protect ProtectionType, True, m_xPWD
        End If
    End If
    Exit Sub
ProcError:
    RaiseError "MPO.CEnvironment.RestoreProtection"
    Exit Sub
End Sub

Public Sub UnProtectDocument(Optional bNoPrompt = False)
    Dim bytTries As Byte
    Dim oForm As frmPassword
    Dim xMsg As String
    Dim xArrPWD() As String
    Dim xPWD As String
    Dim i As Integer
    
'   Ensure that document is unprotected.
'   "zzmpDummy" is used to force error if
'   protected with a password.
'   This is the only way to get password to
'   allow it to be restored at end of macro.
    On Error GoTo ProcError
    
    If ProtectionType <> wdNoProtection Then
        'get firm's standard password
        'could be in 2 locations (for backward compatibility)
        'GLOG : 5098 : CEH
        xPWD = mpBase2.GetMacPacIni( _
            "General", "ProtectedDocumentsDefaultPassword")
        If xPWD = "" Then
            xPWD = mpBase2.GetMacPacIni( _
                "ProtectedDocuments", "Password")
        End If
        
        'build array of passwords from ini
        mpBase2.xStringToArray xPWD, xArrPWD()

        'set module var to dummy PWD
        m_xPWD = "zzmpDummy"
        
        'force error
        ActiveDocument.UnProtect m_xPWD
    End If
    Exit Sub
ProcError:
    Select Case Err
        Case 5485
            bytTries = bytTries + 1
            If bytTries - 1 <= UBound(xArrPWD) Then
                'grab next password
                m_xPWD = xArrPWD(bytTries - 1, 0)
                Resume
            ElseIf (bytTries <= UBound(xArrPWD) + 5) And (Not bNoPrompt) Then
                'prompt for user password
                Set oForm = New frmPassword
                If bytTries > UBound(xArrPWD) + 2 Then
                    xMsg = "Invalid password." & vbCr & "Please provide the password to unprotect the document."
                Else
                    xMsg = "In order to continue this operation, the " & _
                        "document will need to be temporarily unprotected.  Please " & _
                        "provide the password to unprotect the document."
                End If
                
                With oForm
                    .Message = xMsg
                    .Show vbModal
                    If Not .Cancelled Then
                        m_xPWD = .ItemName
                        If m_xPWD = "" Then
                            m_xPWD = "zzmpDummy"
                        End If
                        Resume
                    Else
                        Err.Raise mpError_InvalidUnprotectPassword
                    End If
                End With
                
            Else
                If (Not bNoPrompt) Then
                    MsgBox "The document could not be unprotected.  " & _
                           "Invalid password.", vbExclamation
                    Err.Raise mpError_InvalidUnprotectPassword
                End If
            End If
        Case Else
            RaiseError "MPO.CEnvironment.UnProtectDocument"
    End Select
End Sub



Private Sub Class_Initialize()
    GetState
End Sub
