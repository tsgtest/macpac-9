VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'**********************************************************
'   CContact Class
'   created 12/02/99 by Jeffrey Sweetland

'   Contains properties and methods that
'   integrate with Contact Integration

'   Member of
'   Container for
'**********************************************************
Option Explicit
Private m_xFirstName As String
Private m_xMiddleName As String
Private m_xLastName As String
Private m_xPrefix As String
Private m_xSuffix As String
Private m_xTitle As String
Private m_xCompany As String
Private m_xStreetAddress As String
Private m_xDepartment As String
Private m_xCity As String
Private m_xState As String
Private m_xZipCode As String
Private m_xCountry As String
Private m_xCoreAddress As String
Private m_xPhone As String
Private m_xPhoneExtension As String
Private m_xFax As String
Private m_xEmail As String
Private m_xSalutation As String
Private m_colCustomFields As Object '---temp fix 1/23 mpci.CustomFields
Private m_iContactType As Integer
Private m_vListingID As Variant
Private m_vStoreID As Variant



'CharlieCore 9.2.0 - CI
Public Function Detail(Optional bNoDisplayName As Boolean = False, _
                        Optional bPhone As Boolean = False, _
                        Optional bFax As Boolean = False, _
                        Optional bEmail As Boolean = False, _
                        Optional xPhoneLabel As String = "", _
                        Optional xFaxLabel As String = "Fax: ", _
                        Optional xEmailLabel As String = "E-mail: ", _
                        Optional xSeparator As String = vbCrLf) As String
    
    Dim xTemp As String
    Dim xSlogan As String
    Dim xIncludeSlogan As String
    Dim oUNID As CUNID
    Dim oL As CIO.CListing
    Dim xAddress As String
    
    If Not bNoDisplayName Then
        If FullName <> "" Then _
            xTemp = FullName & xSeparator
        If Title <> "" Then _
            xTemp = xTemp & Title & xSeparator
        If Company <> "" Then _
            xTemp = xTemp & Company & xSeparator
    Else
        If DisplayName = FullName And Title <> "" Then _
            xTemp = xTemp & Title & xSeparator
        
        If DisplayName <> Company And Company <> "" Then _
            xTemp = xTemp & Company & xSeparator
    End If
                
    xIncludeSlogan = UCase(mpBase2.GetMacPacIni("CI", "CIIncludeSlogan"))
    
    If xIncludeSlogan = "TRUE" Then
 '      create new UNID object
        Set oUNID = New CUNID
        Set oL = oUNID.GetListing(Me.ListingID)

        If oL.BackendID = 104 Then
            xSlogan = g_oDBs.People(oL.ID).Office.Slogan
            If xSlogan <> "" Then
                xTemp = xTemp & g_oDBs.People(oL.ID).Office.Slogan & xSeparator
            End If
        End If
    End If
                
    If CoreAddress <> Empty Then
        'Core address could be delimited with vbCr instead of vbCrLf
        xAddress = mpBase2.xSubstitute(CoreAddress, vbCrLf, "{mpTempVBCRLF}")
        xAddress = mpBase2.xSubstitute(xAddress, vbCr, vbCrLf)
        xAddress = mpBase2.xSubstitute(xAddress, "{mpTempVBCRLF}", vbCrLf)
        xTemp = xTemp & Replace(xAddress, vbCrLf, xSeparator)
    Else
        If StreetAddress <> "" Then
            '#3661
            xTemp = xTemp & Replace(StreetAddress, vbCrLf, xSeparator) & xSeparator
        End If
        
        If City <> "" Then _
            xTemp = xTemp & City & ", "
        
        If State <> "" Then _
            xTemp = xTemp & State
            
        If ZipCode <> "" Then _
            xTemp = xTemp & "  " & ZipCode
    End If
    
    DebugOutput "InStr(CoreAddress, Country)" & InStr(CoreAddress, Country)
    DebugOutput "Country = " & Country
    DebugOutput "bIsDefaultCountry(Country) = " & bIsDefaultCountry(Country)
    
    If InStr(CoreAddress, Country) = 0 Then
        If Country <> "" Then
            If bIsDefaultCountry(Country) Then
                xTemp = xTemp & xSeparator
            Else
                ' append separator if necessary
                If Right(xTemp, Len(xSeparator)) <> xSeparator Then
                    xTemp = xTemp & xSeparator
                End If
                xTemp = xTemp & Country & xSeparator
            End If
            
        End If
    ElseIf bIsDefaultCountry(Country) Then      '9.7.1 - #4203
        'country is in CoreAddress and is also default country, so remove
        xTemp = mpBase2.xTrimTrailingChrs(xTemp, Country)
        xTemp = mpBase2.xTrimTrailingChrs(xTemp, " ")
        xTemp = mpBase2.xTrimTrailingChrs(xTemp, xSeparator)
    End If
    
    
    If bPhone And Phone <> "" Then _
        xTemp = xTemp & xPhoneLabel & Phone & xSeparator
    If bFax And Fax <> "" Then _
        xTemp = xTemp & xFaxLabel & Fax & xSeparator
    If bEmail And Email <> "" Then _
        xTemp = xTemp & xEmailLabel & Email & xSeparator
        
    xTemp = xTrimTrailingChrs(xTemp, Chr(10))
    xTemp = xTrimTrailingChrs(xTemp, vbCr)
    xTemp = xTrimTrailingChrs(xTemp, xSeparator)
    
    If Word.Options.AutoFormatAsYouTypeReplaceQuotes Then
        xTemp = xReturnSmartQuotes(xTemp)
    End If
    
    Detail = xTemp

End Function

Public Function DisplayName(Optional bIncludePrefix As Boolean = True) As String
    If FullName <> "" Then
        DisplayName = FullName(bIncludePrefix)
    ElseIf Company <> "" Then
        DisplayName = Company
    Else
        DisplayName = "No Name Listed"
    End If
End Function

'CharlieCore 9.2.0 - CI
Friend Sub LoadProperties10(oCI As mpci.Contact, _
                          Optional ByVal xSep As String = vbCrLf)
    Dim xName As String
    Dim xAddress As String
            
    If oCI Is Nothing Then Exit Sub
    
    With oCI
        If InStr(UCase(.AddressType), "HOME") = 0 Then
            m_xCompany = .Company
            m_xTitle = .Title
        Else
'           get from ci if company and title should be returned
            Dim xINI As String
            xINI = mpBase2.GetDLLOCXPath("mpCI.Application") & "mpCI.ini"
            If UCase(mpBase2.GetIni("Application", "AllowHomeCompany", xINI)) = "TRUE" Then
                m_xCompany = .Company
            End If
            If UCase(mpBase2.GetIni("Application", "AllowHomeTitle", xINI)) = "TRUE" Then
                m_xTitle = .Title
            End If
        End If
                
        m_xFirstName = .FirstName
        m_xMiddleName = .MiddleName
        m_xLastName = .LastName
        m_xPrefix = .Prefix
        m_xSuffix = .Suffix
        
        If .Street1 <> "" Then _
            xAddress = xAddress & .Street1 & xSep
        
        If .Street2 <> "" Then _
            xAddress = xAddress & .Street2 & xSep
        
        If .Street3 <> "" Then _
            xAddress = xAddress & .Street3 & xSep
            
        If .AdditionalInformation <> "" Then
            If UCase(mpBase2.GetMacPacIni("Application", "AdditionalInfoFirst")) = "TRUE" Then
                xAddress = .AdditionalInformation & xSep & xAddress
            Else
                xAddress = xAddress & .AdditionalInformation
            End If
        End If
        
        xAddress = xTrimTrailingChrs(xAddress, Chr(10))
        xAddress = xTrimTrailingChrs(xAddress, vbCr)
        xAddress = xTrimTrailingChrs(xAddress, xSep)
        
        m_xStreetAddress = xAddress
        m_xCity = .City
        m_xState = .State
        m_xZipCode = .ZipCode
        m_xCountry = .Country
        m_xDepartment = .Department
        m_xPhone = .Phone
        m_xPhoneExtension = .PhoneExtension
        m_xFax = .Fax
        m_xEmail = .EMailAddress
        m_xSalutation = .Salutation
        m_vListingID = .ListingID
        m_vStoreID = .StoreID
        Set m_colCustomFields = .CustomFields
        m_iContactType = .ContactType
    End With
End Sub

Friend Sub LoadProperties20(oCI As CIO.CContact, Optional ByVal xSep As String = vbCrLf)
    Dim xName As String
    Dim xAddress As String
            
    If oCI Is Nothing Then Exit Sub
    
    With oCI
        If InStr(UCase(.AddressTypeName), "HOME") = 0 Then
            m_xCompany = .Company
            m_xTitle = .Title
        Else
'           get from ci if company and title should be returned
            Dim xINI As String
            xINI = mpBase2.ApplicationDirectoryCI & "\CI.ini"
            If UCase(mpBase2.GetIni("CIApplication", "AllowHomeCompany", xINI)) = "TRUE" Then
                m_xCompany = .Company
            End If
            If UCase(mpBase2.GetIni("CIApplication", "AllowHomeTitle", xINI)) = "TRUE" Then
                m_xTitle = .Title
            End If
        End If
                
        m_xFirstName = .FirstName
        m_xMiddleName = .MiddleName
        m_xLastName = .LastName
        m_xPrefix = .Prefix
        m_xSuffix = .Suffix
        
        If .Street1 <> "" Then _
            xAddress = xAddress & .Street1 & xSep
        
        If .Street2 <> "" Then _
            xAddress = xAddress & .Street2 & xSep
        
        If .Street3 <> "" Then _
            xAddress = xAddress & .Street3 & xSep
            
        If .AdditionalInformation <> "" Then
            If UCase(mpBase2.GetMacPacIni("Application", "AdditionalInfoFirst")) = "TRUE" Then
                xAddress = .AdditionalInformation & xSep & xAddress
            Else
                xAddress = xAddress & .AdditionalInformation
            End If
        End If
        
        xAddress = xTrimTrailingChrs(xAddress, Chr(10))
        xAddress = xTrimTrailingChrs(xAddress, vbCr)
        xAddress = xTrimTrailingChrs(xAddress, xSep)
        
        m_xStreetAddress = xAddress
        
        'trim empty linefeeds at start or end of Core address
        Dim xCoreAddress As String
        xCoreAddress = mpBase2.xTrimTrailingChrs(.CoreAddress, vbCrLf, True, True)
        xCoreAddress = mpBase2.xTrimTrailingChrs(xCoreAddress, Chr(11), True, True)
        
        m_xCoreAddress = xCoreAddress
        
        m_xCity = .City
        m_xState = .State
        m_xZipCode = .ZipCode
        m_xCountry = .Country
        m_xDepartment = .Department
        m_xPhone = .Phone
        m_xPhoneExtension = .PhoneExtension
        m_xFax = .Fax
        m_xEmail = .EMailAddress
        m_xSalutation = .Salutation
        
        Dim oUNID As CIO.CUNID
        Set oUNID = New CIO.CUNID
        
        m_vListingID = .UNID
        m_vStoreID = oUNID.GetStore(.UNID).ID
        Set m_colCustomFields = .CustomFields
        m_iContactType = .ContactType
    End With
End Sub

Public Property Get ListingID() As Variant
    ListingID = m_vListingID
End Property
Public Property Get StoreID() As Variant
    StoreID = m_vStoreID
End Property
Public Property Get Salutation() As String
    Salutation = m_xSalutation
End Property
Public Property Get Email() As String
    Email = m_xEmail
End Property
Public Property Get Fax() As String
    Fax = m_xFax
End Property
Public Property Get Phone() As String
    Phone = m_xPhone
End Property
Public Property Get Country() As String
    Country = m_xCountry
End Property
Public Property Get CoreAddress() As String
    CoreAddress = m_xCoreAddress
End Property
Public Property Get ZipCode() As String
    ZipCode = m_xZipCode
End Property
Public Property Get State() As String
    State = m_xState
End Property
Public Property Get City() As String
    City = m_xCity
End Property
Public Property Get StreetAddress() As String
    StreetAddress = m_xStreetAddress
End Property
Public Property Get Company() As String
'************************************************
'per log item 1777
    If UCase$(Right$(m_xCompany, 5)) = ", THE" Then
        m_xCompany = mpBase2.xFlipName(m_xCompany, ",")
    End If
'************************************************
    Company = m_xCompany
End Property
Public Property Get Title() As String
    Title = m_xTitle
End Property

Public Function FullName(Optional bIncludePrefix As Boolean = True) As String
    
    Dim xName As String
    Dim oXHelp As XHelper
    Dim xTemp As String
    
    Set oXHelp = New XHelper

'   handle Prefix
    If bIncludePrefix And Prefix <> "" Then
        If Suffix <> "" Then
            If oXHelp.FindFirst(g_oArrSuffix, Suffix, 0) <> -1 Then     'suffix found
                If (g_oArrPrefixToExclude.Value(0, 0) <> "") And _
                (oXHelp.FindFirst(g_oArrPrefixToExclude, Prefix, 0) = -1) Then      'prefix to exclude was not found
                    xName = Prefix & " "
                End If
            Else
                xName = Prefix & " "
            End If
        Else
            xName = Prefix & " "
        End If
    End If
    
    If FirstName <> "" Then _
        xName = xName & FirstName & " "
    If MiddleName <> "" Then _
        xName = xName & MiddleName & " "
    If LastName <> "" Then _
        xName = xName & LastName
    
    xName = Trim(xName)
    
'   handle Suffix
    If Suffix <> "" Then
    
        If Prefix <> "" Then
            If oXHelp.FindFirst(g_oArrPrefix, Prefix, 0) <> -1 Then     'prefix found
                If (g_oArrSuffixToExclude.Value(0, 0) <> "") And _
                (oXHelp.FindFirst(g_oArrSuffixToExclude, Suffix, 0)) = -1 Then      'suffix to exclude was not found
                    xTemp = Suffix
                End If
            Else
                xTemp = Suffix
            End If
        Else
            xTemp = Suffix
        End If

'       Suffix Separator
        If xTemp <> "" Then
            ' ****9.6.2 - 3848
            ' Check if Suffix matches an exclusion, or starts with
            ' an excluded item followed by a space or comma
            If oXHelp.FindFirst(g_oArrSuffixSeparatorExclusions, Suffix, 0) <> -1 Or _
                oXHelp.FindFirst(g_oArrSuffixSeparatorExclusions, Suffix & " ", 0, True) <> -1 Or _
                oXHelp.FindFirst(g_oArrSuffixSeparatorExclusions, Suffix & ",", 0, True) <> -1 Then
                xName = xName & " " & xTemp
            Else
                ' ****9.6.2 - 3847
                ' Don't duplicate separator if it's already included
                ' at the start of the suffix
                If InStr(xTemp, g_xSuffixSeparator) = 1 Then
                    xName = xName & xTemp
                Else
                    xName = xName & g_xSuffixSeparator & xTemp
                End If
            End If
        End If
        
    End If
    FullName = xName
End Function

Public Property Get CustomFields() As Object
    Set CustomFields = m_colCustomFields
End Property
Public Property Get ContactType() As Integer
    ContactType = m_iContactType
End Property
Public Property Get Prefix() As String
    Prefix = m_xPrefix
End Property
Public Property Get Suffix() As String
    Suffix = m_xSuffix
End Property
Public Property Get FirstName() As String
    FirstName = m_xFirstName
End Property
Public Property Get LastName() As String
    LastName = m_xLastName
End Property
Public Property Get MiddleName() As String
    MiddleName = m_xMiddleName
End Property
Public Property Get Department() As String
    Department = m_xDepartment
End Property
Public Property Get PhoneExtension() As String
    PhoneExtension = m_xPhoneExtension
End Property
