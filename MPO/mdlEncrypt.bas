Attribute VB_Name = "mdlEncrypt"
Option Explicit

'******************************************************
'THIS FUNCTION IS RUN BY THE INSTALL EXTENSION
'******************************************************

Private Function EncryptDate(ByVal d As Date) As String
'returns date until which MacPac is valid
    Dim xDate As String
    Dim xScram As String
    Dim xDateMod As String
    Dim bRnd As Byte
    Dim bDigit As Byte
    
    On Error GoTo ProcError
    
    xDate = Format(d, "ddyyyyMM")
    Randomize
    While Len(xDate)
'       insert a random digit before each each digit in xDate
        bRnd = Int(10 * Rnd)
        xDateMod = xDateMod & bRnd & Left(xDate, 1)
        xDate = Mid(xDate, 2)
    Wend
    
'   get a random int between 2 and 4
    Randomize
    bRnd = Int(3 * Rnd + 2)
    
    While Len(xDateMod)
'       convert every char divisible by
'       the random number to a char
        bDigit = Left(xDateMod, 1)
        If (bDigit Mod bRnd = 0) Then
            xScram = xScram & Chr(100 + bDigit)
        Else
            xScram = xScram & bDigit
        End If
        
'       trim left char
        xDateMod = Mid(xDateMod, 2)
    Wend
    
'   1st digit is garbage, 2nd digit is random factor
'   used above, the rest is the encrypted date
    EncryptDate = Int(10 * Rnd) & bRnd & xScram
    Exit Function
    
ProcError:
    Dim xMsg As String
    xMsg = "Could not create installation signature. The following error occurred: " & _
        vbCr & "number: " & Err.Number & vbCr & "description: " & Err.Description
    MsgBox xMsg, vbCritical, App.Title
End Function

Public Function UnEncryptDate(ByVal xCode As String) As Date
'returns the date encrypted in xcode
    Dim bRnd As Byte
    Dim xCodeMod As String
    Dim i As Integer
    Dim xDate As String
    Dim x As String
    
    On Error GoTo ProcError
    
'   remove garbage 1st digit
    xCode = Mid(xCode, 2)

'   store and remove random factor
    bRnd = Left(xCode, 1)
    xCode = Mid(xCode, 2)
    
'   collect chars at even indexes
    For i = 2 To Len(xCode) Step 2
        xCodeMod = xCodeMod & Mid(xCode, i, 1)
    Next
    
    While Len(xCodeMod)
        x = Left(xCodeMod, 1)
        If Not IsNumeric(x) Then
            x = Asc(x) - 100
        End If
        
        xDate = xDate & x
        
'       trim left char
        xCodeMod = Mid(xCodeMod, 2)
    Wend
    
'DanCore 9.2.0 - UnEncryptDateFix - remember to add fix to PostInstall utility
'   convert to date
    UnEncryptDate = DateSerial(Mid(xDate, 3, 4), Mid(xDate, 7, 2), Mid(xDate, 1, 2))
    Exit Function
    
ProcError:
    Exit Function
End Function

'********************************************************************
'CharlieCore 9.2.0 - Encrypt

Public Function EncryptText(xText As String)
    Dim i As Integer
    Dim iChar As Integer
    Dim vScram As Variant
    Dim vTemp As Variant
    Dim iSeed As Integer
    Dim iRnd As Integer
    
'this function will encrypt supplied xText
'unless it already is encrypted

'   Initialize random-number generator.
    Randomize

'   Generate random value between 145 and 164.
    iRnd = CInt((20 * Rnd) + 145)

'   convert to number between 1 and 20
    iSeed = iRnd - 144
    
    If Not Left(xText, 2) = "~}" Then
        For i = 1 To Len(xText)
            iChar = Asc(Mid(xText, i, 1))
            ' Encrypted character may be above ASCII 255 so use ChrW
            vScram = vScram + ChrW(((iChar + g_iSeed) + (i Mod 5)) - iSeed)
        Next i
'*************************************************
'       add encryption markers:
'       - 1st and 2nd characters are constants
'       - 3rd character is random seed
'       - 4th character is value returned by iGetSeed() + 144
'       - vScram is actual variable value
'*************************************************
        vTemp = "~}" & Chr(iRnd) & Chr(g_iSeed + 144) & vScram
    Else
        vTemp = vScram
    End If

    EncryptText = vTemp

End Function

Public Function DecryptText(ByVal vText As Variant)    '9.7.1 - #4223
    Dim i As Integer
    Dim iChar As Integer
    Dim vScram As Variant
    Dim iSeed As Integer
    Dim iRnd As Integer
    
'this function will decrypt string
'encrypted by EncryptText function

'   first 2 characters are encryption markers
    If Left(vText, 2) = "~}" Then
'       3rd character is variable seed
        iRnd = Asc(Mid(vText, 3, 1))
        iSeed = iRnd - 144
        
'       stri((iChar - g_iSeed) - (i Mod 5)) + iSeedng to decrypt starts at 5th character
        vText = Mid(vText, 5)
        For i = 1 To Len(vText)
            On Error Resume Next
            If Asc(Mid(vText, i, 1)) <> AscW(Mid(vText, i, 1)) Then
                ' Use AscW because encrypted character may be beyond ASCII 255 limit
                ' However, it shouldn't be higher than
                ' ((255 + g_iSeed) + (i Mod 5)) - iSeed
                ' So use regular Asc function for those characters (Unicode)
                If AscW(Mid(vText, i, 1)) <= ((255 + IIf(g_iSeed <> 0, g_iSeed, g_iOldSeed)) + (i Mod 5)) - iSeed Then
                    iChar = AscW(Mid(vText, i, 1))
                Else
                    iChar = Asc(Mid(vText, i, 1))
                End If
            Else
                iChar = Asc(Mid(vText, i, 1))
            End If
            On Error GoTo 0
            vScram = vScram & Chr(((iChar - IIf(g_iSeed <> 0, g_iSeed, g_iOldSeed)) - (i Mod 5)) + iSeed)
        Next i
    Else
        vScram = vText
    End If
    
    DecryptText = vScram
End Function

Public Function iGetSeed(Optional xSeedKey As String = "Seed") As Integer   'noseed
    Dim iSeedLen As Integer
    Dim xChar As String
    Dim iDigit As Integer
    Dim iTemp As Integer
    Dim xSeed As String
    
'   this function gets global seed value, taken
'   from the MacPac.ini - Seed key string
    
'   get seed from ini
    xSeed = UCase(mpBase2.GetMacPacIni("General", xSeedKey))
    
'   get length
    iSeedLen = Len(xSeed)
    
    If iSeedLen > 0 Then
'       cycle through seed string and turn into number 'iDigit'
        While Len(xSeed)
            xChar = Mid(xSeed, 1, 1)
            iDigit = iDigit + Asc(xChar)
            xSeed = Mid(xSeed, 2)
        Wend
        iTemp = iDigit / iSeedLen
    Else
        iTemp = iSeedLen
    End If
    
'   return seed number
    iGetSeed = iTemp
    
End Function

Public Function SeedMatch(Optional xVarName As String = "") As Boolean    '9.7.1 - #4223
    Dim bTemp As Boolean
    Dim oDoc As New MPO.CDocument
    Dim xVarValue As String
    Dim xDocSeed As String
    Dim xSeed As String
    
'   Written by charlie homo - 010614
'   xVarName is supplied in cases where xVarName could have been encrypted
'   with a different seed, ie, client A saves document, runs trailer,
'   client B opens document and runs trailer, if client A reopens document,
'   then running trailer should not error

    On Error Resume Next
    xVarValue = oDoc.File.Variables(xVarName).Value
'   get value of variable created for each new documents
    xDocSeed = oDoc.GetVar("zzmpFixed_Variable")
    If xDocSeed = "" Then
        xDocSeed = oDoc.GetVar("zzmpFixed_MacPacEncryption")
    End If
    On Error GoTo ProcError

    If xDocSeed = "" Then
        If xVarName = "" Then
            bTemp = True
        Else    'check seed embeded in supplied variable
            bTemp = (Mid(xVarValue, 4, 1)) = Chr(IIf(g_iSeed <> 0, g_iSeed, g_iOldSeed) + 144)
        End If
    Else
        If g_iSeed Or g_iOldSeed Then 'global seed has/had value
            If xVarName = "" Then
                If g_iSeed Then
                    xSeed = "Seed"
                Else
                    xSeed = "OldSeed"
                End If
                bTemp = (UCase(xDocSeed) = UCase(mpBase2.GetMacPacIni("General", xSeed)))
            Else    'check seed embeded in variable
                bTemp = (Mid(xVarValue, 4, 1)) = Chr(IIf(g_iSeed <> 0, g_iSeed, g_iOldSeed) + 144)
            End If
        Else
            bTemp = False
        End If
    End If
    
    SeedMatch = bTemp
    
    Exit Function
    
ProcError:
    RaiseError "MPO.mdlEncrypt.SeedMatch"
    Exit Function
End Function
