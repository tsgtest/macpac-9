VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTableOfAuthorities"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTableOfAuthorities Class
'   created 3/28/00 by Jeffrey Sweetland
'
'**********************************************************
Const mpTOABPFile As String = "PleadingTOA.mbp"
Const mpTOABookmark As String = "zzmpFIXED_TableOfAuthorities"
Const mpDocSegment As String = "PleadingTOA"

Public Enum mpPageNumberFormats 'GLOG #5708 !gm
    mpPageNumberFormats_Arabic = 0
    mpPageNumberFormats_UpperCaseRoman = 1
    mpPageNumberFormats_LowerCaseRoman = 2
    mpPageNumberFormats_UpperCaseLetter = 3
    mpPageNumberFormats_LowerCaseLetter = 4
End Enum

Private m_xBookmarkName As String
Private m_bSectionOnly As Boolean
Private m_bKeepFormatting As Boolean
Private m_lSectionToInsertAfter As Long
Private m_bUsePassim As Boolean
Private m_bRestartPageNumbering As Boolean
Private m_iPageNumberFormat As mpPageNumberFormats
Private m_oDoc As CDocument
Private m_iLocation As Integer
Private m_oHeadingStyle As Word.Style
Private m_oPPaper As MPO.CPleadingPaper
Private m_bSaveVars As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Location(iNew As mpDocPositions)
    m_iLocation = iNew
End Property

Public Property Get Location() As mpDocPositions
    Location = m_iLocation
End Property

Public Property Let SectionToInsertAfter(lNew As Long)
    m_lSectionToInsertAfter = lNew
End Property

Public Property Get SectionToInsertAfter() As Long
    SectionToInsertAfter = m_lSectionToInsertAfter
End Property

Public Property Get SectionOnly() As Boolean
    SectionOnly = m_bSectionOnly
End Property

Public Property Let SectionOnly(bNew As Boolean)
    m_bSectionOnly = bNew
    If m_bSaveVars Then
        m_oDoc.SaveItem "SectionOnly", mpDocSegment, , m_bSectionOnly
    End If
End Property

Public Property Get KeepFormatting() As Boolean
    KeepFormatting = m_bKeepFormatting
End Property

Public Property Let KeepFormatting(bNew As Boolean)
    m_bKeepFormatting = bNew
    If m_bSaveVars Then
        m_oDoc.SaveItem "KeepFormatting", mpDocSegment, , m_bKeepFormatting
    End If
End Property

Public Property Get UsePassim() As Boolean
    UsePassim = m_bUsePassim
End Property

Public Property Let UsePassim(bNew As Boolean)
    m_bUsePassim = bNew
    If m_bSaveVars Then
        m_oDoc.SaveItem "UsePassim", mpDocSegment, , m_bUsePassim
    End If
End Property

Public Property Get RestartPageNumbering() As Boolean
    RestartPageNumbering = m_bRestartPageNumbering
End Property

Public Property Let RestartPageNumbering(bNew As Boolean)
    m_bRestartPageNumbering = bNew
    If m_bSaveVars Then
        m_oDoc.SaveItem "RestartPageNumbering", mpDocSegment, , m_bRestartPageNumbering
    End If
End Property

Public Property Get PageNumberFormat() As mpPageNumberFormats
    PageNumberFormat = m_iPageNumberFormat
End Property

Public Property Let PageNumberFormat(iNew As mpPageNumberFormats)
    m_iPageNumberFormat = iNew
End Property

Public Function HeadingStyle() As Word.Style
    Set HeadingStyle = m_oHeadingStyle
End Function

'**********************************************************
'   Methods
'**********************************************************
Public Function Document() As MPO.CDocument
    Set Document = m_oDoc
End Function

Public Function BoilerplateExists() As Boolean
    Dim oRange As Word.Range
    If m_oDoc.File.Bookmarks.Exists(m_xBookmarkName) Then
        BoilerplateExists = True
'   deal with 8.x TOA bookmarks
    ElseIf m_oDoc.File.Bookmarks.Exists("mpTableofAuthorities") Then
        m_oDoc.File.Bookmarks.Add m_xBookmarkName, _
                m_oDoc.File.Bookmarks("mpTableofAuthorities").Range
        BoilerplateExists = True
    Else
        BoilerplateExists = False
    End If
End Function

Public Sub Finish()
    Dim oRng As Word.Range
    Dim xBMK As String
    Dim xTOAPath As String
    Dim i As Integer
    Dim oF As Word.Field
    Dim rngEndOfSection As Word.Range
    Dim vStory As Variant
    
    Refresh
    
    With m_oDoc
        
        xTOAPath = mpBase2.BoilerplateDirectory & mpTOABPFile
        
        If Not BoilerplateExists Then
            If Me.Location = mpDocPosition_AfterTOC Then
'               insert after TOC & reset location prop
                If Me.SectionToInsertAfter = .File.Sections.Count Then
                    .CreateSection mpDocPosition_EOF
                    Me.Location = mpDocPosition_EOF
                Else
                    Set rngEndOfSection = m_oDoc.File.Sections(Me.SectionToInsertAfter).Range
                    With rngEndOfSection
                        .EndOf
                        .Select
                        m_oDoc.CreateSection mpDocPosition_Selection
                    End With
                    Me.Location = mpDocPosition_Selection
                End If
            Else
                .CreateSection Me.Location
            End If
            
            Select Case Me.Location
                Case mpDocPosition_BOF
                    .Selection.StartOf wdStory
                    .Selection.Sections(1).Footers(wdHeaderFooterPrimary) _
                        .PageNumbers.RestartNumberingAtSection = True
                Case mpDocPosition_EOF
                    .Selection.EndOf wdStory
                Case Else
                    ' Preserve Header/footers of following section
                    .UnlinkHeadersFooters .Selection.Sections(1)
                    .Selection.Move wdCharacter, -1
                    '#3682
                    If Me.SectionOnly Then
                        .Selection.InsertParagraphAfter
                    End If
                    .CreateSection mpDocPosition_Selection
                    ' Clear and Unlink headers for Exhibit Section
                    .ClearHeaders .Selection.Sections(1), True
                    .ClearFooters .Selection.Sections(1), True
            
 '//////////////// code here for extra section break maintenance
'---look ahead and behind for doubled section breaks
                    On Error Resume Next
                    If ActiveDocument.Bookmarks.Exists("mpTableOfContents") Then
                        If ActiveDocument.Sections(.Selection.Information(wdActiveEndSectionNumber) - 1) _
                                .Range.Characters.Count = 1 Then
                            ActiveDocument.Sections(.Selection.Information(wdActiveEndSectionNumber) - 1) _
                                .Range.Delete
                        ElseIf ActiveDocument.Sections(.Selection.Information(wdActiveEndSectionNumber) + 1) _
                                .Range.Characters.Count = 2 Then
                            ActiveDocument.Sections(.Selection.Information(wdActiveEndSectionNumber) + 1) _
                                .Range.Delete
                        End If
                    End If
                    On Error GoTo 0
            
            End Select
'           this is necessary for headers & footers to come through
            With .SelectedSection
                
               .Headers(wdHeaderFooterFirstPage).Range.InsertFile _
                        xTOAPath, _
                        "zzmpTOAHeaderFirstPage", , , _
                        False
                        
                .Headers(wdHeaderFooterPrimary).Range.InsertFile _
                        xTOAPath, _
                        "zzmpTOAHeaderPrimary", , , _
                        False
                        
                .Footers(wdHeaderFooterFirstPage).Range.InsertFile _
                        xTOAPath, _
                        "zzmpTOAFooterFirstPage", , , _
                        False

                .Footers(wdHeaderFooterPrimary).Range.InsertFile _
                        xTOAPath, _
                        "zzmpTOAFooterPrimary", , , _
                        False
                        
                Selection.InsertFile xTOAPath, mpTOABookmark, , , False
            End With
            
            xBMK = mpTOABookmark
            If .bHideFixedBookmarks Then
                .HideBookmark xBMK
                xBMK = "_" & xBMK
            End If
            Set oRng = .File.Bookmarks(xBMK).Range
            oRng.Text = ""
            With oRng.Sections(1)
                With .Footers(wdHeaderFooterFirstPage).PageNumbers
                    .StartingNumber = 1
                    Select Case m_iPageNumberFormat 'GLOG #5708 !gm
                        Case mpPageNumberFormats_Arabic
                            .NumberStyle = wdPageNumberStyleArabic
                        Case mpPageNumberFormats_UpperCaseRoman
                            .NumberStyle = wdPageNumberStyleUppercaseRoman
                        Case mpPageNumberFormats_LowerCaseRoman
                            .NumberStyle = wdPageNumberStyleLowercaseRoman
                        Case mpPageNumberFormats_UpperCaseLetter
                            .NumberStyle = wdPageNumberStyleUppercaseLetter
                        Case mpPageNumberFormats_LowerCaseLetter
                            .NumberStyle = wdPageNumberStyleLowercaseLetter
                    End Select
                    .ShowFirstPageNumber = True
                End With
'       this call is needed because .ShowFirstPageNumber = True
'       above turns off different first page
                .PageSetup.DifferentFirstPageHeaderFooter = True
                
                i = oRng.Sections.Last.Index
                
'               reset page numbering to continue from preceding TOC section
                If i > 1 Then
                    If ActiveDocument.Sections(i - 1) _
                    .Footers(wdHeaderFooterPrimary) _
                    .PageNumbers.NumberStyle = _
                    wdPageNumberStyleLowercaseRoman And _
                    Not m_bRestartPageNumbering Then 'GLOG #5710 !gm  checkbox val can override
                        .Footers(wdHeaderFooterFirstPage).PageNumbers _
                            .RestartNumberingAtSection = False
                    End If
                End If
                
                With m_oPPaper
                    If .Exists Then
                        .StartPageNumberingAt = 1
                        If m_bRestartPageNumbering Then 'GLOG #5710 !gm
                            .StartingPageNumber = 1
                        Else
                            oRng.Sections(1).Footers(wdHeaderFooterFirstPage).PageNumbers _
                                .RestartNumberingAtSection = False
                        End If
                        .Update i, , , False
                    End If
                End With
                
            End With
        Else
            Set oRng = .File.Bookmarks(m_xBookmarkName).Range
            With oRng
                .Delete
            End With
        End If
                        
'       #2426/CEH/9.4.1
        If Not Me.SectionOnly Then
            Dim bTOACats(1 To 16) As Boolean
            
'           this is necessary due to a bug in Word XP where the use
'           of Category:=0 to include all categories does not work
            oRng.Select
'           Cycle through fields and set flag for used categories
            On Error Resume Next
            For Each vStory In m_oDoc.File.StoryRanges
                For Each oF In vStory.Fields
                    If oF.Type = wdFieldTOAEntry Then
                        ' Only check those including category
                        If InStr(UCase(oF.Code), "\C ") Then
                            bTOACats(Val(Right(oF.Code, 3))) = True
                        End If
                    End If
                Next oF
            Next vStory
            On Error GoTo 0
            For i = 1 To 16
'               Generate TOA only for used categories
                If bTOACats(i) Then
                    With m_oDoc.File.TablesOfAuthorities
                        .Add Range:=Selection.Range, _
                             Category:=i, _
                             Passim:=Me.UsePassim, _
                             IncludeCategoryHeader:=True, _
                             KeepEntryFormatting:=Me.KeepFormatting
                        '#3443
                        .Item(.Count).TabLeader = wdTabLeaderDots
                    End With
                End If
            Next i
        End If
        
        
        With oRng
            .SetRange .Sections(1).Range.Start, .Sections(1).Range.End
            While .Characters.Last = Chr(12)
                .MoveEnd wdCharacter, -1
            Wend
            .Fields.Unlink
            .Bookmarks.Add m_xBookmarkName, oRng
            .Collapse wdCollapseStart
            .Select
        End With
        
'       delete any hidden text
        .ClearBookmarks
    End With
End Sub

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDoc = New CDocument
    Set m_oPPaper = New MPO.CPleadingPaper
    With m_oDoc
        m_xBookmarkName = mpTOABookmark
        If .bHideFixedBookmarks Then
            .HideBookmark (m_xBookmarkName)
            m_xBookmarkName = "_" & m_xBookmarkName
        End If
        Set m_oHeadingStyle = .File.Styles(wdStyleTOAHeading)
    End With
    LoadValues
End Sub
Private Sub Class_Terminate()
    Set m_oDoc = Nothing
    Set m_oPPaper = Nothing
End Sub
Private Sub Refresh()
    m_bSaveVars = True
    With Me
        .SectionOnly = .SectionOnly
        .KeepFormatting = .KeepFormatting
        .UsePassim = .UsePassim
        .RestartPageNumbering = .RestartPageNumbering
    End With
    m_bSaveVars = False
End Sub
Private Sub LoadValues()
    On Error Resume Next
    With m_oDoc
        m_bSectionOnly = .RetrieveItem("SectionOnly", mpDocSegment)
        m_bKeepFormatting = .RetrieveItem("KeepFormatting", mpDocSegment)
        m_bUsePassim = .RetrieveItem("UsePassim", mpDocSegment)
        m_bRestartPageNumbering = .RetrieveItem("RestartPageNumbering", mpDocSegment)
    End With
End Sub
