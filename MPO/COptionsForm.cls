VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COptionsForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'**********************************************************
'   COptionsForm Class
'   created 12/31/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage user options

'   Member of
'   Container for frmOptions
'**********************************************************
Private WithEvents m_dlgOptions As frmAuthorDefaults
Attribute m_dlgOptions.VB_VarHelpID = -1

Public Event DefaultOptionsChange()
Public Event CurrentDocAuthorOptionsChange()

'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Sub Show(oDocObject As IDocObj)
'shows the options form configured for specified doc object
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean

    On Error GoTo ProcError
    If oDocObject.Template.OptionsTable = Empty Then
        Err.Raise mpErrors.mpError_InvalidMember, , "No author preferences exist for this template."
    End If
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

    Set m_dlgOptions = New frmAuthorDefaults
    With m_dlgOptions
        .DocObject = oDocObject
        On Error Resume Next
        .Show vbModal
    End With
    Unload m_dlgOptions
    Set m_dlgOptions = Nothing
    
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    RaiseError "MPO.COptionsForm.Show"
    Exit Sub
End Sub

Public Sub CopyOptions(oPerson As mpDB.CPerson, ByVal xTemplate As String)
'copies the option set of reuse author for template xTemplate-
'typically used to copy the reuse author options to the private db
    Dim iChoice As VbMsgBoxResult
    Dim oSourceOptions As mpDB.CPersonOptions
    Dim xMsg As String
    Dim i As Integer
    Dim lID As Long
    Dim oFoundPerson As mpDB.CPerson
    Dim oOptions As mpDB.CPersonOptions
    Dim oDoc As MPO.CDocument
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean

    On Error GoTo ProcError
    
    Set oDoc = New MPO.CDocument

'   get options from reuse author - these
'   are the options to copy - ie the source options
    If (oPerson Is Nothing) Then
'       no author supplied - alert and exit
        Err.Raise mpError_InvalidPerson
    End If

    With oPerson
'       get author options for specified template
        Set oOptions = .Options(xTemplate)

        If (oOptions Is Nothing) Then
'           no author options stored in document - alert and exit
            xMsg = "No options exist for this author."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Sub
        End If

'       assign default author to which
'       these options will belong
        If .Source = mpPeopleSourceList_People Then
            lID = .ID
        Else
'           get number of entries with the same name-
'           it's necessary that we look for name, as
'           opposed to ID, because the IDs for the same
'           "real" person may be different in two private dbs.
            Set oFoundPerson = g_oDBs.People.Find(.LastName, .FirstName)
            If Not (oFoundPerson Is Nothing) Then
                lID = oFoundPerson.ID
            End If
        End If
    End With

    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

'   if no default author has been found, the
'   first row of people will be selected
    With frmCopyAuthorDefaults
        If lID Then
'           get author to copy options to
            .cmbAuthor.BoundText = lID
        Else
'           select first row - no person
'           matches people in list
            .cmbAuthor.Row = 0
        End If

'       specify message in dlg based on person type
        If TypeOf oPerson Is MPO.CReUseAuthor Then
            .lblAuthor.Caption = "The " & LCase(xTemplate) & " Preferences " & _
                "for " & oPerson.FullName & " were stored in this document " & _
                "when it was created.  You can permanently have access to these " & _
                "preferences by assigning them to an author in your list of authors.  " & _
                "Select the author to which you would like to assign " & _
                "a copy of these preferences."
        Else
            .lblAuthor.Caption = "Select the author to which you would like to assign " & _
                "a copy of the " & LCase(xTemplate) & " preferences " & _
                "of " & oPerson.FullName & "."
        End If

        .Show vbModal
        If Not .Cancelled Then
'           get selected person
            lID = .cmbAuthor.BoundText
            Set oPerson = g_oDBs.People(lID)
            oOptions.Copy oPerson
        End If
    End With
    Set oDoc = Nothing
    Set frmCopyAuthorDefaults = Nothing
    
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    Exit Sub

ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    Set oDoc = Nothing
    RaiseError "MPO.COptionsForm.CopyOptions"
    Exit Sub
End Sub

Private Sub m_dlgOptions_CurrentDocAuthorOptionsChange()
    RaiseEvent CurrentDocAuthorOptionsChange
End Sub

Private Sub m_dlgOptions_DefaultOptionsChange()
    RaiseEvent DefaultOptionsChange
End Sub
