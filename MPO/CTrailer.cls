VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTrailer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   Trailer Class
'   created 4/26/98 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define a MacPac trailer

'   Container for CDocument
'**********************************************************

Private Const mpDocSegment As String = "Trailer"

Public Enum mpTrailerLocations
    mpTrailerLocation_Footer = 1
    mpTrailerLocation_EODParagraph = 2
    mpTrailerLocation_EODPageBottom = 3
    mpTrailerLocation_Cursor = 4
    mpTrailerLocation_Header = 5
End Enum

Private m_iIndex As Integer
Private m_Document As MPO.CDocument
Private m_iID As Integer
Public Event LocationChange()


'**********************************************************
'   Properties
'**********************************************************

Public Property Let Location(iNew As Integer)
    Dim iLocation As Integer
    iLocation = Me.Location
    If iNew <> iLocation Then
        LocationChange
    End If
    m_Document.SaveItem "Location", mpDocSegment, , iNew
End Property

Public Property Get Location() As Integer
    On Error Resume Next
    Location = m_Document.RetrieveItem("Location", mpDocSegment)
End Property

Public Property Let DocID(xNew As String)
    m_Document.SaveItem "DocID", mpDocSegment, , xNew
    m_Document.InsertDocProperty "Trailer_1_DocID", xNew
End Property

Public Property Get DocID() As String
    On Error Resume Next
    DocID = m_Document.RetrieveItem("DocID", mpDocSegment)
End Property

Public Property Let Version(xNew As String)
    m_Document.SaveItem "Version", mpDocSegment, , xNew
    m_Document.InsertDocProperty "Trailer_1_Version", xNew
End Property

Public Property Get Version() As String
    On Error Resume Next
    Version = m_Document.RetrieveItem("Version", mpDocSegment)
End Property

Public Property Let Draft(xNew As String)
    m_Document.SaveItem "Draft", mpDocSegment, , xNew
    m_Document.InsertDocProperty "Trailer_1_Draft", xNew
End Property

Public Property Get Draft() As String
    On Error Resume Next
    Draft = m_Document.RetrieveItem("Draft", mpDocSegment)
End Property


Public Property Let DateStamp(xNew As String)
    m_Document.SaveItem "DateStamp", mpDocSegment, , xNew
    m_Document.InsertDocProperty "Trailer_1_DateStamp", xNew
End Property

Public Property Get DateStamp() As String
    On Error Resume Next
    DateStamp = m_Document.RetrieveItem("DateStamp", mpDocSegment)
End Property

Public Property Let TimeStamp(xNew As String)
    m_Document.SaveItem "TimeStamp", mpDocSegment, , xNew
    m_Document.InsertDocProperty "Trailer_1_TimeStamp", xNew
End Property

Public Property Get TimeStamp() As String
    On Error Resume Next
    TimeStamp = m_Document.RetrieveItem("TimeStamp", mpDocSegment)
End Property

Public Property Get Boilerplate() As String
'readonly property returns the root of the
'caption bookmark - based on CaptionType
    
'---different method used here -- CList set up on .mdb, table contains boilerplate bookmark names in column 3
'---this ensures that fax boilerplate can be added by adding record to tblFaxCovers, and boilerplate item in .mbp file
    Dim m_L As CList
    Set m_L = New CList
    With m_L
        .IsDBList = True
        .Name = "Trailers"
        .FilterValue = ""
        .Refresh
        Boilerplate = .ListItems.Source(Me.Location - 1, 2)
   
    End With
    
End Property


'**********************************************************
'   Methods
'**********************************************************
Public Sub Insert()
'inserts the trailer boilerplate only
'location determined by previously set properties
    Dim iLocation As Integer
    Dim iStory As Integer
    
    Select Case Me.Location
        Case mpTrailerLocation_Cursor
            iStory = mpDocumentStory_Main
            iLocation = mpDocumentLocation_CurrentSelection
            InsertBoilerplate iLocation, iStory
        Case mpTrailerLocation_EODPageBottom
            iStory = mpDocumentStory_Main
            iLocation = mpDocumentLocation_EOF
            InsertBoilerplate iLocation, iStory
        Case mpTrailerLocation_EODParagraph
            iStory = mpDocumentStory_Main
            iLocation = mpDocumentLocation_EOF
            InsertBoilerplate iLocation, iStory
        Case mpTrailerLocation_Footer
            iStory = mpDocumentStory_PrimaryFooter
            iLocation = mpDocumentLocation_CurrentSelection
            InsertBoilerplate iLocation, iStory
            iStory = mpDocumentStory_FirstFooter
            iLocation = mpDocumentLocation_CurrentSelection
            InsertBoilerplate iLocation, iStory
        Case mpTrailerLocation_Header
            iStory = mpDocumentStory_PrimaryHeader
            iLocation = mpDocumentLocation_CurrentSelection
            InsertBoilerplate iLocation, iStory
            iStory = mpDocumentStory_FirstHeader
            iLocation = mpDocumentLocation_CurrentSelection
            InsertBoilerplate iLocation, iStory
    End Select
    Me.Finish
End Sub

Public Sub Update()
    Me.Finish
End Sub

Public Sub Delete()
    DeleteProperties
    DeleteBoilerplate
End Sub

Friend Sub DeleteProperties()
    Document.DeleteSegmentValues mpDocSegment
    Document.DeleteCustomDocumentProperties mpDocSegment
End Sub


Public Sub DeleteBoilerplate()
'---this deletes all the trailer bp frames in a document
    Dim fld As Object
    Dim frm As Object
    Dim sec As Object
    Dim ftr As Object
    
    For Each frm In m_Document.File.Frames
        If frm.Range.Fields.Count > 0 Then
            For Each fld In frm.Range.Fields
                If fld.Type = 85 Then
                    frm.Cut
                End If
            Next
        End If
    Next
    
    For Each sec In m_Document.File.Sections
        
        For Each frm In sec.Footers(1).Range.Frames
            If frm.Range.Fields.Count > 0 Then
                For Each fld In frm.Range.Fields
                    If fld.Type = 85 Then
                        frm.Cut
                    End If
                Next
            End If
        Next
        
        For Each frm In sec.Footers(2).Range.Frames
            If frm.Range.Fields.Count > 0 Then
                For Each fld In frm.Range.Fields
                    If fld.Type = 85 Then
                        frm.Cut
                    End If
                Next
            End If
        Next
    
        For Each frm In sec.Headers(1).Range.Frames
            If frm.Range.Fields.Count > 0 Then
                For Each fld In frm.Range.Fields
                    If fld.Type = 85 Then
                        frm.Cut
                    End If
                Next
            End If
        Next
        
        For Each frm In sec.Headers(2).Range.Frames
            If frm.Range.Fields.Count > 0 Then
                For Each fld In frm.Range.Fields
                    If fld.Type = 85 Then
                        frm.Cut
                    End If
                Next
            End If
        Next
    Next
    
    Clipboard.Clear
End Sub
Public Sub LocationChange()
    '---If the location changes we assume we've got to strip existing trailers
    '---with the exception of cursor location!
    If Me.Location <> mpTrailerLocation_Cursor Then
        Me.DeleteBoilerplate
    End If
End Sub

Public Sub InsertBoilerplate(Optional iLocation As Integer = 1, _
                             Optional iStory As Integer = 1)
        m_Document.InsertBoilerplate "General.mbp", _
                                      Me.Boilerplate, , _
                                      iLocation, , _
                                      iStory
End Sub

Public Sub SelectItem(xBookmark As String, Optional StoryRange As Word.Range)
    m_Document.SelectItem xBookmark
End Sub

Public Function Document() As MPO.CDocument
    Set Document = m_Document
End Function

Public Sub Finish()
'cleans up doc after all user input is complete
    Dim rngP As Range

    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
        
    With m_Document
 '      delete any hidden text - ie text marked for deletion
        .RemoveFormFields
        .UpdateFields
        Application.ScreenUpdating = True
''       clear all associated bookmarks
'        Dim b As Word.Bookmark
'        For Each b In Word.ActiveDocument.Bookmarks
'            If InStr(b.Name, Me.Boilerplate) > 0 Then
'                b.Delete
'            End If
'        Next b
    End With
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    Err.Raise Err.Number
End Sub
'Private Sub m_Document_TrailerChange()
'    MsgBox "Call boilerplate sub here!"
'
'End Sub

Private Sub Class_Initialize()
    Set m_Document = New MPO.CDocument
    m_Document.Selection.StartOf
End Sub

Private Sub Class_Terminate()
    Set m_Document = Nothing
End Sub
