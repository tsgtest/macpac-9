Attribute VB_Name = "mdlXML"
Option Explicit

'WARNING: ANY USE BY YOU OF THE CODE PROVIDED IN THIS EXAMPLE IS
'AT YOUR OWN RISK. Microsoft provides this macro code "as is" without warranty of
'any kind, either express or implied, including but not limited to the implied warranties of
'merchantability and/or fitness for a particular purpose.
'===============================================

'NOTE - a reference has been set to Microsoft XML, v4.0
'===============================================

Function GetLabelDocumentID(vLabelID As Variant, strLayout As String, strPartner As String) As String
'pass in the LabelID, LabelLayout and LabelPartner
'output NEW Office 12+ Label ID
'strLayout, ie, "mailingLabel"  'these are attributes found in the pg_index.xml file - element Layout
'strPartner, ie, "Avery US Letter" 'another atrribute from that file
    Dim DocumentID As String
    Dim longID As Long
    Dim partID As Integer  'partner id
    Dim strIndexfile As DOMDocument60
    Dim strPartnerfile As String 'partner file
    Dim partnerNodeList As IXMLDOMNodeList
    Dim partnerList As IXMLDOMNodeList
    Dim productID As Long
    Dim i As Integer 'counter 1
    Dim n As Integer ' counter 2
    Dim partnerFile As String
    Dim oFSO As New FileSystemObject
    Dim oFolder As Folder
    Dim vPath As Variant
    Dim lRet As Long
    
    On Error GoTo ProcError
    productID = 0
    
    'get registry key value
    vPath = String(255, Chr(32))
    lRet = mpBase2.GetValue(HKEY_CURRENT_USER, "Software\Microsoft\Office\" & Word.Application.Version & "\Word\Options", "PROGRAMDIR", vPath)
    vPath = RTrim(vPath)

    If vPath <> "" Then
        If Right(vPath, 1) <> "\" Then _
            vPath = vPath & "\"
        vPath = vPath & "PAGESIZE"
    End If
'    xPath = "C:\Program Files\Microsoft Office\OFFICE12\PAGESIZE"
'    On Error Resume Next
'    Set oFolder = oFSO.GetFolder(xPath)
'    On Error GoTo ProcError
'
'    If oFolder Is Nothing Then
'    'must be 64-bit
'        xPath = "C:\Program Files (x86)\Microsoft Office\OFFICE12\PAGESIZE"
'    End If
    
    Set strIndexfile = New DOMDocument60
    strIndexfile.Load (vPath & "\PG_INDEX.XML")
    Set partnerNodeList = strIndexfile.getElementsByTagName("layout")
    For i = 0 To partnerNodeList.Length - 1
    'find the node "Layout" and find strLayout
        If UCase(partnerNodeList.Item(i).Attributes.Item(0).Text) = UCase(strLayout) Then
            Set partnerList = partnerNodeList.Item(i).childNodes
            'set a nodelist for node partner in Layout
            For n = 0 To partnerList.Length - 1
               If partnerList.Item(n).Attributes.getNamedItem("id").Text <> "1" Then
                If UCase(partnerList.Item(n).Attributes.getNamedItem("name").Text) = UCase(strPartner) Then
                 'returns the file
                 partID = CInt(partnerList.Item(n).Attributes.getNamedItem("id").Text)
                  partnerFile = vPath & "\" & _
                    partnerList.Item(n).Attributes.getNamedItem("file").Text
                 Exit For
                End If
               End If
            Next
            Exit For
        End If
    Next
    
    'use this to get the correct productID
    getID CStr(vLabelID), partnerFile, productID
    
    'since VB doesn't support Left Shift we are using 2^24 to simulate the Left Shift.
    'then using the Bitwise Or to get the DocumentID
    longID = (2 ^ 24) * partID Or productID
    DocumentID = CStr(longID)
    
    'return new Label ID
    GetLabelDocumentID = DocumentID
    Exit Function
ProcError:
    RaiseError "MPO.mdlXML.GetLabelDocumentID"
    Exit Function
End Function

Sub getID(prodName As String, xmlFile As String, prodID As Long)
    'gets the productID given the xml file and the product name
    Dim doc As DOMDocument60
    Dim prodNode As IXMLDOMNodeList
    Dim prodIDAttrib As IXMLDOMAttribute
    Dim strT As String
    Dim i As Integer
    Dim xTemp As Variant
    
    Set doc = New DOMDocument60
    doc.Load (xmlFile)
    Set prodNode = doc.getElementsByTagName("locName")
    '9.7.3010 #4831
    For i = 1 To prodNode.Length - 1
    
        On Error Resume Next
        xTemp = Val(prodNode.Item(i).Text)
        On Error GoTo 0
        
        If xTemp = "0" Then
            xTemp = prodNode.Item(i).Text
        End If
        
        If xTemp = prodName Then
            prodID = CLng(prodNode.Item(i).parentNode.parentNode.Attributes.getNamedItem("productID").Text)
            Exit For
        End If
    Next

End Sub
