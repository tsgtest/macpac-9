VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmPleadingType 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Pleading"
   ClientHeight    =   3060
   ClientLeft      =   36
   ClientTop       =   360
   ClientWidth     =   5496
   Icon            =   "frmPleadingType.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3060
   ScaleWidth      =   5496
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnFavorites 
      Caption         =   "Fa&vorites"
      Height          =   400
      Left            =   60
      TabIndex        =   13
      Top             =   2580
      Width           =   975
   End
   Begin TrueDBList60.TDBList lstFavorites 
      Height          =   2010
      Left            =   1770
      OleObjectBlob   =   "frmPleadingType.frx":058A
      TabIndex        =   8
      Top             =   195
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.CommandButton btnAddToFaves 
      Caption         =   "&Add..."
      Height          =   400
      Left            =   1080
      TabIndex        =   12
      Top             =   2580
      Width           =   1065
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4410
      TabIndex        =   10
      Top             =   2580
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   3345
      TabIndex        =   9
      Top             =   2580
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbLevel0 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmPleadingType.frx":29FE
      TabIndex        =   1
      Top             =   180
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbLevel1 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmPleadingType.frx":51C3
      TabIndex        =   3
      Top             =   750
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbLevel2 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmPleadingType.frx":7988
      TabIndex        =   5
      Top             =   1320
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbLevel3 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmPleadingType.frx":A14D
      TabIndex        =   7
      Top             =   1875
      Width           =   3615
   End
   Begin VB.CommandButton btnDelete 
      Caption         =   "D&elete..."
      Height          =   400
      Left            =   1080
      TabIndex        =   14
      Top             =   2580
      Width           =   1065
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      X1              =   15
      X2              =   5490
      Y1              =   2445
      Y2              =   2445
   End
   Begin VB.Label lblLevel3 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Branch/Division:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   60
      TabIndex        =   6
      Top             =   1935
      Width           =   1485
   End
   Begin VB.Label lblLevel2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Jurisdiction:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   270
      TabIndex        =   4
      Top             =   1380
      Width           =   1275
   End
   Begin VB.Label lblLevel1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Court:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   270
      TabIndex        =   2
      Top             =   810
      Width           =   1275
   End
   Begin VB.Label lblLevel0 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&State:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   270
      TabIndex        =   0
      Top             =   240
      Width           =   1275
   End
   Begin VB.Label lblFavorites 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "&Favorites:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   11
      Top             =   240
      Visible         =   0   'False
      Width           =   1410
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   2835
      Left            =   -30
      Top             =   -375
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmPleadingType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_bInitializing As Boolean
Private m_bDefRetrieved As Boolean
Private m_bFaveRetrieved As Boolean
Private m_bSettingAllLevels As Boolean
Private m_xIniSection As String
Private m_lLevel0 As Long
Private m_lLevel1 As Long
Private m_lLevel2 As Long
Private m_lLevel3 As Long
Private m_sTop As Single
Private m_bPanel1 As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Level0() As Long
    Level0 = m_lLevel0
End Property

Public Property Get Level1() As Long
    Level1 = m_lLevel1
End Property

Public Property Get Level2() As Long
    Level2 = m_lLevel2
End Property

Public Property Get Level3() As Long
    Level3 = m_lLevel3
End Property


Private Sub btnAddToFaves_Click()
    AddPleadingFavorite
End Sub

Private Sub AddPleadingFavorite()
    Dim lL0 As Long
    Dim lL1 As Long
    Dim lL2 As Long
    Dim lL3 As Long
    Dim xDescription As String
    Dim oDlg As frmAddPleadingFavorite
    Dim oDef As mpDB.CPleadingDef
    Dim lFave As Long
    Dim oFaves As CPleadingFavorites
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean

    
'   get current selected fave
    On Error Resume Next
    lFave = Me.lstFavorites.BoundText
    On Error GoTo ProcError
    
'   get level values
    With Me
        If Len(.cmbLevel0.BoundText) Then
            lL0 = .cmbLevel0.BoundText
        End If
        
        If Len(.cmbLevel1.BoundText) Then
            lL1 = .cmbLevel1.BoundText
        End If
        
        If Len(.cmbLevel2.BoundText) Then
            lL2 = .cmbLevel2.BoundText
        End If
        
        If Len(.cmbLevel3.BoundText) Then
            lL3 = .cmbLevel3.BoundText
        End If
    End With
    
'   test for valid pleading type
    Set oDef = g_oDBs.PleadingDefs.ItemFromCourt(lL0, lL1, lL2, lL3)
    
'   alert and exit if no type was found
    If (oDef Is Nothing) Then
        xMsg = "Could not add this pleading as a favorite. A pleading " & _
               "template for this court does not exist.  " & vbCr & _
               "Please contact your administrator if you wish to " & _
               "have this pleading type created."
        MsgBox xMsg, vbExclamation, App.Title
        Exit Sub
    End If
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Or g_bIsWord16x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

'   prompt for description
    Set oDlg = New frmAddPleadingFavorite
    With oDlg
        .Show vbModal, Me
        
        'GLOG : 5594|5381
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

        If .Cancelled Then
            Exit Sub
        Else
            xDescription = .txtDescription
        End If
    End With
    
    With g_oDBs.PleadingFavorites
'       add favorite
        .Add xDescription, lL0, lL1, lL2, lL3
        
'       get most current pleading favorites
        .Refresh
    End With
    
'   refresh list to show changes
    Set oFaves = g_oDBs.PleadingFavorites
    oFaves.Refresh
    With Me.lstFavorites
        .Array = oFaves.List
        .Rebind
        .BoundText = lFave
    End With
    
'   setup dialog
    Me.btnFavorites.Enabled = True
    Me.btnDelete.Enabled = True

    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    RaiseError "MPO.frmPleadingType.AddPleadingFavorite"
    Exit Sub
End Sub

Private Sub DeleteFavorite()
    Dim iUserChoice As VbMsgBoxResult
    Dim xMsg As String
    Dim lBookmark As Long
    
    xMsg = "Remove selected item from list?"
    iUserChoice = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
    If iUserChoice = vbYes Then
        With g_oDBs.PleadingFavorites
            .Delete Me.lstFavorites.BoundText
            .Refresh
        End With
        With Me.lstFavorites
            lBookmark = .SelectedItem
            .Array = g_oDBs.PleadingFavorites.List
            .Rebind
            If .Array.Count(1) Then
                .Row = Min(CDbl(lBookmark), .Array.Count(1) - 1)
                Me.btnDelete.Enabled = True
            Else
                m_bPanel1 = True
                ShowPanel
                Me.btnFavorites.Enabled = False
                Me.btnDelete.Enabled = False
            End If
        End With
    End If
End Sub

Private Sub btnDelete_Click()
    DeleteFavorite
End Sub

Private Sub btnFavorites_Click()
    If Not m_bInitializing Then
        m_bPanel1 = Not (Me.btnFavorites.Caption = "Fa&vorites")
        SwitchPanels
    End If
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub btnOK_Click()
    Dim oDef As mpDB.CPleadingDef
    Dim lL0 As Long
    Dim lL1 As Long
    Dim lL2 As Long
    Dim lL3 As Long
    Dim xMsg As String
    
'   validate user choices
    If Not m_bPanel1 Then
        If Me.lstFavorites.BoundText = Empty Then
            xMsg = "Please select a pleading from your list of favorites."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Sub
        End If
        
        With g_oDBs.PleadingFavorites(Me.lstFavorites.BoundText)
'           assign levels from selected favorite
            m_lLevel0 = .Level0
            m_lLevel1 = .Level1
            m_lLevel2 = .Level2
            m_lLevel3 = .Level3
            UpdateLevelLists .Level0, .Level1, .Level2, .Level3
        End With
    Else
'       assign level from text box choices
        On Error Resume Next
        m_lLevel0 = Me.cmbLevel0.BoundText
        m_lLevel1 = Me.cmbLevel1.BoundText
        m_lLevel2 = Me.cmbLevel2.BoundText
        m_lLevel3 = Me.cmbLevel3.BoundText
        On Error GoTo 0
    End If
    
'   get pleading type for specified levels
    Set oDef = g_oDBs.PleadingDefs.ItemFromCourt( _
        m_lLevel0, m_lLevel1, m_lLevel2, m_lLevel3)
    
'   alert if no type was found, else dismiss dlg and move on
    If (oDef Is Nothing) Then
        xMsg = "A pleading template for this court does not exist.  " & _
               "Please contact " & vbCr & "your administrator if you wish to " & _
               "have this pleading type created."
        MsgBox xMsg, vbExclamation, App.Title
    Else
        SetDefaults
        Me.Cancelled = False
        Me.Hide
        DoEvents
    End If
End Sub

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub GetDefaults()
    Dim m_bPanel1 As Boolean
    
'   get defaults if switching to non-favorites panel
'   and defaults haven't been gotten (m_bDefRetrieved = false)
    If m_bPanel1 And _
        Not m_bDefRetrieved Then
        GetPanel1Defaults
    ElseIf Not m_bPanel1 And _
        Not m_bFaveRetrieved Then
        GetPanel2Defaults
    End If
    
'   guarantee a favorites selection
    If Not m_bPanel1 And Me.lstFavorites.Row = -1 Then
        Me.lstFavorites.Row = 0
    End If
End Sub

Private Sub ShowPanel()
'toggle panels from favorites to non-favorites
    
    If Not m_bPanel1 Then
        btnFavorites.Caption = "&All Courts"
    Else
        btnFavorites.Caption = "Fa&vorites"
    End If
    
    Me.lblLevel0.Visible = m_bPanel1
    Me.lblLevel1.Visible = m_bPanel1
    Me.lblLevel2.Visible = m_bPanel1
    Me.lblLevel3.Visible = m_bPanel1
    Me.cmbLevel0.Visible = m_bPanel1
    Me.cmbLevel1.Visible = m_bPanel1
    Me.cmbLevel2.Visible = m_bPanel1
    Me.cmbLevel3.Visible = m_bPanel1
    Me.lblFavorites.Visible = Not m_bPanel1
    Me.lstFavorites.Visible = Not m_bPanel1
    Me.btnAddToFaves.Visible = m_bPanel1
    Me.btnDelete.Visible = Not m_bPanel1
    If m_bPanel1 Then
        Me.cmbLevel0.SetFocus
    Else
        Me.lstFavorites.SetFocus
    End If
    DoEvents
End Sub

Private Sub GetPanel2Defaults()
'gets starting favorite value
    Dim lFave As Long
    
'   get sticky value from ini
    On Error Resume Next
    lFave = GetUserIni(m_xIniSection, "Favorite")
    On Error GoTo 0
    
    If lFave Then
'       value supplied - select favorite
        With Me.lstFavorites
            .BoundText = lFave
            If IsNull(.SelectedItem) Then
                .SelectedItem = 0
            End If
        End With
    Else
'       no default fave- select first item in list
        Me.lstFavorites.SelectedItem = 0
    End If
    m_bFaveRetrieved = True
End Sub

Private Sub UpdateLevel1List()
    Dim xL1 As String
    Dim oList As mpDB.CList
    
'   get current value - we'll attempt to
'   return to this value later
    xL1 = Me.cmbLevel1.Text
    Set oList = g_oDBs.Lists("PleadingCourtsLevel1")
    
    With oList
'       update rows in list based on parent level value
        .FilterValue = CLng(Me.cmbLevel0.BoundText)
        .Refresh
    End With
        
    With Me.cmbLevel1
'       reassign updated list
        .Array = oList.ListItems.Source
        .Rebind
        
        If oList.ListItems.Count Then
            .BoundText = .Columns(1).Text
            .Enabled = True
            ResizeTDBCombo Me.cmbLevel1, 10
        Else
'           no items in list = disable control
            .Text = ""
            .Enabled = False
        End If
    End With
    DoEvents
End Sub

Private Sub UpdateLevel2List()
    Dim xL2 As String
    Dim oList As mpDB.CList
    
'   get current value - we'll attempt to
'   return to this value later
    xL2 = Me.cmbLevel2.Text
    Set oList = g_oDBs.Lists("PleadingCourtsLevel2")
    
    If Me.cmbLevel1.BoundText = "" Then
'       disable control
        With Me.cmbLevel2
            .BoundText = ""
            .Text = ""
            .Enabled = False
        End With
    Else
        With oList
'           update rows in list based on parent level value
            .FilterValue = CLng(Me.cmbLevel1.BoundText)
            .Refresh
        End With
        
        With Me.cmbLevel2
'           reassign updated list
            .Array = oList.ListItems.Source
            .Rebind
            
            If oList.ListItems.Count Then
                .BoundText = .Columns(1).Text
                .Enabled = True
                ResizeTDBCombo Me.cmbLevel2, 6
            Else
'               no items in list = disable control
                .Text = ""
                .Enabled = False
            End If
        End With
    End If
    DoEvents
    ResizeTDBCombo Me.cmbLevel2, 6

End Sub

Private Sub UpdateLevel3List()
    Dim xL3 As String
    Dim oList As mpDB.CList
    
'   get current value - we'll attempt to
'   return to this value later
    xL3 = Me.cmbLevel3.Text
    Set oList = g_oDBs.Lists("PleadingCourtsLevel3")
    
    If Me.cmbLevel2.BoundText = "" Then
'       disable control
        With Me.cmbLevel3
            .BoundText = ""
            .Text = ""
            .Enabled = False
        End With
    Else
        '---9.7.1 - 4070
        With Me.cmbLevel3
            .BoundText = ""
            .Text = ""
        End With
        '--- end
        With oList
'           update rows in list based on parent level value
            .FilterValue = CLng(Me.cmbLevel2.BoundText)
            .Refresh
        End With
        
        With Me.cmbLevel3
'           reassign updated list
            .Array = oList.ListItems.Source
            .Rebind
            
            If oList.ListItems.Count Then
'               there are items - attempt to set previous
'               value, else select first item in list
'                .Text = xL3
                If .BoundText = "" Then
'                   previous value is not in current list-
'                   select first item in list
                    .BoundText = .Columns(1).Text
                End If
                .Enabled = True
                ResizeTDBCombo Me.cmbLevel3, 6
            Else
'               no items in list = disable control
                .Text = ""
                .Enabled = False
            End If
        End With
    End If
    DoEvents
    ResizeTDBCombo Me.cmbLevel3, 6
End Sub

Private Sub SwitchPanels()
    If m_bPanel1 And Not m_bDefRetrieved Then
        GetPanel1Defaults
    Else
        GetPanel2Defaults
    End If
    ShowPanel
End Sub

Private Sub cmbLevel0_GotFocus()
    OnControlGotFocus Me.cmbLevel0
End Sub

Private Sub cmbLevel0_ItemChange()
    If Not m_bInitializing Then
        UpdateLevel1List
        If Not m_bSettingAllLevels Then
            UpdateLevel2List
            UpdateLevel3List
        End If
    End If
End Sub

Private Sub cmbLevel0_Mismatch(NewEntry As String, Reposition As Integer)
    CorrectTDBComboMismatch Me.cmbLevel0, Reposition
End Sub

Private Sub cmbLevel1_GotFocus()
    OnControlGotFocus Me.cmbLevel1
End Sub
Private Sub cmbLevel1_ItemChange()
    If Not m_bInitializing Then
        UpdateLevel2List
        If Not m_bSettingAllLevels Then
            UpdateLevel3List
        End If
    End If
End Sub

Private Sub cmbLevel1_Mismatch(NewEntry As String, Reposition As Integer)
    CorrectTDBComboMismatch Me.cmbLevel1, Reposition
End Sub

Private Sub cmbLevel2_GotFocus()
    OnControlGotFocus Me.cmbLevel2
End Sub

Private Sub cmbLevel2_ItemChange()
    If Not m_bInitializing Then
        UpdateLevel3List
    End If
End Sub

Private Sub cmbLevel2_Mismatch(NewEntry As String, Reposition As Integer)
    CorrectTDBComboMismatch Me.cmbLevel2, Reposition
End Sub

Private Sub cmbLevel3_GotFocus()
    OnControlGotFocus Me.cmbLevel3
End Sub

Private Sub cmbLevel3_Mismatch(NewEntry As String, Reposition As Integer)
    CorrectTDBComboMismatch Me.cmbLevel3, Reposition
End Sub

Private Sub Form_Activate()
    If m_bInitializing Then
        LockWindowUpdate Me.hwnd
        SwitchPanels
        DoEvents
        LockWindowUpdate &O0
        If Not m_bPanel1 Then
            Me.lstFavorites.BackColor = g_lActiveCtlColor
        End If
'        Me.Top = (Screen.Height / 2 - Me.Height / 2)
'        Me.Left = (Screen.Width / 2 - Me.Width / 2)
    End If
    m_bInitializing = False
End Sub

Private Sub Form_Load()
    Dim iFaves As Integer
    
    On Error GoTo ProcError
    m_bInitializing = True
    m_bCancelled = True
    
'   load lists
    With g_oDBs.Lists
'        Me.cmbLevel0.Array = _
'            .Item("PleadingCourtsLevel0").ListItems.Source
        On Error Resume Next
        
        '---9.6.1 - only display level 0 for courts actually configured in tblPleadingTypes
        Dim iCount
        iCount = .Item("PleadingCourtsConfigured").ListItems.Count
        If iCount > 0 Then
            Me.cmbLevel0.Array = _
                .Item("PleadingCourtsConfigured").ListItems.Source
        Else
            Me.cmbLevel0.Array = _
                .Item("PleadingCourtsLevel0").ListItems.Source
        End If
        
        On Error GoTo ProcError
        '---End 9.6.1
        
        Me.cmbLevel1.Array = _
            .Item("PleadingCourtsLevel1").ListItems.Source
'        Me.cmbLevel2.Array = _
'            .Item("PleadingCourtsLevel2").ListItems.Source
'        Me.cmbLevel3.Array = _
'            .Item("PleadingCourtsLevel3").ListItems.Source
    End With
    
    Me.lstFavorites.Array = g_oDBs.PleadingFavorites.List
        
    ResizeTDBCombo Me.cmbLevel0, 6
    ResizeTDBCombo Me.cmbLevel1, 6
'    ResizeTDBCombo Me.cmbLevel2, 6
'    ResizeTDBCombo Me.cmbLevel3, 6
    
'   show panel last displayed
    m_xIniSection = g_oTemplates.ItemFromClass("CPleading").ID
    
'   setup dialog
    iFaves = g_oDBs.PleadingFavorites.Count
    
    If iFaves Then
        On Error Resume Next
        m_bPanel1 = Not CBool(mpBase2.GetUserIni(m_xIniSection, "ShowFavorites"))
        On Error GoTo ProcError
    Else
        m_bPanel1 = True
    End If
    
    Me.btnFavorites.Enabled = (iFaves > 0)
    Me.btnDelete.Enabled = (iFaves > 0)
    Exit Sub
    
ProcError:
    RaiseError "MPO.frmPleadingType.Form_Load"
    Exit Sub
End Sub

Private Sub SetDefaults()
'sets level 0-3 defaults to user.ini
'based on current values of controls

    SetUserIni m_xIniSection, "Level0", Me.cmbLevel0.BoundText
    SetUserIni m_xIniSection, "Level1", Me.cmbLevel1.BoundText
    SetUserIni m_xIniSection, "Level2", Me.cmbLevel2.BoundText
    SetUserIni m_xIniSection, "Level3", Me.cmbLevel3.BoundText
    SetUserIni m_xIniSection, "ShowFavorites", CInt(Not m_bPanel1)
    SetUserIni m_xIniSection, "Favorite", Me.lstFavorites.BoundText
End Sub

Private Sub GetPanel1Defaults()
'gets level 0-3 defaults from user.ini-
'sets controls to supplied values-
'selects first item in each list if no
'supplied value
    Dim lLevel0 As Long
    Dim lLevel1 As Long
    Dim lLevel2 As Long
    Dim lLevel3 As Long
    
    EchoOff
    
'   get sticky fields from ini
    On Error Resume Next
    lLevel0 = GetUserIni(m_xIniSection, "Level0")
    lLevel1 = GetUserIni(m_xIniSection, "Level1")
    lLevel2 = GetUserIni(m_xIniSection, "Level2")
    lLevel3 = GetUserIni(m_xIniSection, "Level3")
    
    m_bSettingAllLevels = True
    
'   select item if supplied, else select first item in list
    If lLevel0 Then
        Me.cmbLevel0.BoundText = lLevel0
    Else
        Me.cmbLevel0.SelectedItem = 0
    End If
    
    UpdateLevel1List
    
'   select item if supplied, else select first item in list
    If lLevel1 Then
        Me.cmbLevel1.BoundText = lLevel1
    Else
        Me.cmbLevel1.SelectedItem = 0
    End If
    
    UpdateLevel2List
    
'   select item if supplied, else select first item in list
    If lLevel2 Then
        Me.cmbLevel2.BoundText = lLevel2
    Else
        Me.cmbLevel2.SelectedItem = 0
    End If
    
    UpdateLevel3List
    
'   select item if supplied, else select first item in list
    If lLevel3 Then
        Me.cmbLevel3.BoundText = lLevel3
    Else
        Me.cmbLevel3.SelectedItem = 0
    End If
    m_bSettingAllLevels = False
    
'   note that defaults have been retrieved -
'   this is used in btnFavorites_Click to determine
'   whether to retrieve defaults
    
    m_bDefRetrieved = True
    
    EchoOn
    
End Sub

Private Sub UpdateLevelLists(ByVal lLevel0 As Long, _
                             ByVal lLevel1 As Long, _
                             ByVal lLevel2 As Long, _
                             ByVal lLevel3 As Long)
'updates all lists
    Me.cmbLevel0.BoundText = lLevel0
    UpdateLevel1List
    Me.cmbLevel1.BoundText = lLevel1
    UpdateLevel2List
    Me.cmbLevel2.BoundText = lLevel2
    UpdateLevel3List
    Me.cmbLevel3.BoundText = lLevel3
End Sub

Private Sub lstFavorites_DblClick()
    btnOK_Click
End Sub

Private Sub lstFavorites_GotFocus()
    OnControlGotFocus Me.lstFavorites
End Sub

Private Sub lstFavorites_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        DeleteFavorite
    End If
End Sub
