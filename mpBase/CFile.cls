VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFile"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit



Function xGetFileName(ByVal xPath As String, Optional ByVal bWithExtension As Boolean = False) As String
'takes a path and returns FileName only with or without extension
'Search is not case sensitive

    Dim iBackSlashPos As Integer
    Dim iExtPos As Integer
    Dim xTemp As String
    
    If xPath = "" Then
        xGetFileName = ""
        Exit Function
    End If
    
'   get first "\" position
    iBackSlashPos = InStr(UCase(xPath), "\")
    
'   remove all but FileName with extension
    While iBackSlashPos
        xPath = Right(xPath, Len(xPath) - iBackSlashPos)
        iBackSlashPos = InStr(UCase(xPath), "\")
    Wend
    
    If Not bWithExtension Then
    
        xTemp = xPath
        
        iExtPos = InStr(UCase(xTemp), ".")
    '   remove extension regardless of length _
        (ie, if word 12.x doc then extension will be .docx instead of .doc)
        While iExtPos
            xTemp = Right(xTemp, Len(xTemp) - iExtPos)
            iExtPos = InStr(UCase(xTemp), ".")
        Wend
        
        xPath = Left(xPath, Len(xPath) - (Len(xTemp) + 1))
    
    End If

    xGetFileName = xPath
    
End Function

Public Function GetSubDirs(ByVal xDir As String) As String()
'returns the subdirectories of directory xDir as a string array
    Dim xSubDir As String
    Dim xSubDirs() As String
    
    xSubDir = Dir(xDir, vbDirectory)
    ReDim xSubDirs(0)
    
    While xSubDir <> ""
        If xSubDir <> "." And xSubDir <> ".." Then
'          Use bitwise comparison to make sure its a directory.
           If (GetAttr(xDir & xSubDir) And vbDirectory) Then
                If xSubDirs(0) <> "" Then
                    ReDim Preserve xSubDirs(UBound(xSubDirs) + 1)
                End If
                xSubDirs(UBound(xSubDirs)) = xSubDir
           End If
        End If
        xSubDir = Dir()
    Wend
    
    GetSubDirs = xSubDirs()
End Function

Public Property Get PublicDB() As String
    Dim xTemp As String
    Dim oIni As CIni
    Static xDir As String
    
    If xDir <> Empty Then
'       value has been retrieved already - exit
        PublicDB = xDir
        Exit Property
    End If
    
    Set oIni = New CIni
    
    On Error Resume Next
'   get path from ini
    xTemp = oIni.GetIni("Databases", "Public", GetAppPath & "\MacPac.ini")
    On Error GoTo ProcError
    
    If Len(xTemp) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(xTemp), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(xTemp)
        ElseIf (InStr(UCase(xTemp), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(xTemp)
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(xTemp)
        End If
    End If
    
'   raise error if file is invalid
    If Dir(xTemp) = "" Then
        Err.Raise mpError_InvalidPublicDB
    End If
    
'   store for future retrieval
    xDir = xTemp
    PublicDB = xTemp
    
    Exit Property
ProcError:
    Err.Raise Err.Number, _
              "mpBase2.PublicDB", _
              g_oError.Desc(Err.Number)
    Exit Property
End Property

Public Property Get PeopleDB() As String
    Dim xTemp As String
    Dim oIni As CIni
    Static xDir As String
    
    If xDir <> Empty Then
'       value has been retrieved already - exit
        PeopleDB = xDir
        Exit Property
    End If
    
    Set oIni = New CIni
    
    On Error Resume Next
'   get path from ini
    xTemp = oIni.GetIni("Databases", "People", GetAppPath & "\MacPac.ini")
    On Error GoTo ProcError
    
    If Len(xTemp) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(xTemp), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(xTemp)
        ElseIf (InStr(UCase(xTemp), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(xTemp)
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(xTemp)
        End If
    End If
    
'   raise error if file is invalid
    If Dir(xTemp) = "" Then
        Err.Raise mpError_invalidPeopleDB
    End If
    
'   store for future retrieval
    xDir = xTemp
    PeopleDB = xTemp
    
    Exit Property
ProcError:
    Err.Raise Err.Number, _
              "mpBase2.PeopleDB", _
              g_oError.Desc(Err.Number)
    Exit Property
End Property

Public Property Get BoilerplateDirectory() As String
    Dim xTemp As String
    Dim oIni As CIni
    Static xDir As String
    
    If xDir <> Empty Then
'       value has been retrieved already - exit
        BoilerplateDirectory = xDir
        Exit Property
    End If
    
    Set oIni = New CIni
        
    On Error Resume Next
'   get path from ini
    xTemp = oIni.GetIni("File Paths", "BoilerplateDir", GetAppPath & "\MacPac.ini")
    On Error GoTo ProcError
    
    If Len(xTemp) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(xTemp), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(xTemp)
        ElseIf (InStr(UCase(xTemp), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(xTemp)
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(xTemp)
        End If
    End If

'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        Err.Raise mpError_InvalidBPFilesPath
    End If
    
'   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If
    
'   store for future retrieval
    xDir = xTemp
    BoilerplateDirectory = xTemp
    
    Exit Property
ProcError:
    Err.Raise Err.Number, _
              "mpBase2.BoilerplateDirectory", _
              g_oError.Desc(Err.Number)
    Exit Property
End Property

Public Property Get UserFilesDirectory() As String
    UserFilesDirectory = GetUserFilesDirectory()
End Property

Public Property Get TemplatesDirectory() As String
    Dim xTemp As String
    Dim oIni As CIni
    Static xDir As String
    
    If xDir <> Empty Then
'       value has been retrieved already - exit
        TemplatesDirectory = xDir
        Exit Property
    End If
    
    Set oIni = New CIni
    
    On Error Resume Next
'   get path from ini
    xTemp = oIni.GetIni("File Paths", "TemplatesDir", GetAppPath & "\MacPac.ini")
    On Error GoTo ProcError
    
    If Len(xTemp) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(xTemp), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(xTemp)
        ElseIf (InStr(UCase(xTemp), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(xTemp)
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(xTemp)
        End If
    End If

'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        Err.Raise mpError_InvalidMacPacTemplatesPath
    End If
    
'   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If
    
    TemplatesDirectory = xTemp
    
    Exit Property
ProcError:
    Err.Raise Err.Number, _
              "mpBase2.TemplatesDirectory", _
              g_oError.Desc(Err.Number)
    Exit Property
End Property
Public Function ApplicationDirectory() As String
    ApplicationDirectory = GetAppPath()
End Function

Public Function ProgramDirectory() As String
    ProgramDirectory = App.Path
End Function

Public Function ApplicationDirectoryCI() As String
    ApplicationDirectoryCI = GetAppPathCI()
End Function

Public Function ApplicationDirectoryNumbering() As String
    ApplicationDirectoryNumbering = GetAppPathNumbering()
End Function

Public Property Get DirExists(xPath As String) As Boolean
    DirExists = mdlGlobal.DirExists(xPath)
End Property

Public Sub EnsureTrailingSlash(xDir As String)
'    add trailing slash if necessary
    If Right(xDir, 1) <> "\" Then
        xDir = xDir & "\"
    End If
End Sub

Public Sub EnsureNoTrailingSlash(xDir As String)
'   delete trailing slash if necessary
    If Right(xDir, 1) = "\" Then
        xDir = Left(xDir, Len(xDir) - 1)
    End If
End Sub

Public Function CountFiles(ByVal xDir As String) As Long
''returns the total number of files in the root
''and sub directories of directory xdir
'    Dim xSubDir As String
'    Dim xFile As String
'    Dim xSubDirs() As String
'    Dim i As Integer
'    Dim l As Long
'
'    EnsureTrailingSlash xDir
'
'    If Not DirIsValid(xDir) Then
'        Exit Function
'    End If
'
''   recurse through sub dirs
'    xSubDirs() = GetSubDirs(xDir)
'    If xSubDirs(0) <> Empty Then
'        For i = LBound(xSubDirs) To UBound(xSubDirs)
'            xSubDir = xDir & xSubDirs(i)
'            l = l + CountFiles(xSubDir)
'        Next i
'    End If
'
''   cycle through files in source dir-
''   remove attributes of same file in dest dir
'    xFile = Dir(xDir & "*.*")
'    While Len(xFile)
'        l = l + 1
'        xFile = Dir()
'    Wend
'    CountFiles = l
End Function

Public Property Get UserBoilerplateDirectory() As String
    Dim xTemp As String
    Dim oIni As CIni
    Static xDir As String
    
    If xDir <> Empty Then
'       value has been retrieved already - exit
        UserBoilerplateDirectory = xDir
        Exit Property
    End If
    
    Set oIni = New CIni
        
    On Error Resume Next
'   get path from ini
    xTemp = oIni.GetIni("File Paths", "UserBoilerplateDir", GetAppPath & "\MacPac.ini")
    On Error GoTo ProcError
    
    If Len(xTemp) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(xTemp), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(xTemp)
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(xTemp)
        End If
    End If

'   raise error if path is invalid
    If xTemp = "" Then
        ' If no separate INI key, use same location as regular boilerplate
        xTemp = BoilerplateDirectory
    ElseIf Not DirExists(xTemp) Then
        Err.Raise mpError_InvalidBPFilesPath
    End If
    
'   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If
    
'   store for future retrieval
    xDir = xTemp
    UserBoilerplateDirectory = xTemp
    
    Exit Property
ProcError:
    Err.Raise Err.Number, _
              "mpBase2.UserBoilerplateDirectory", _
              g_oError.Desc(Err.Number)
    Exit Property
End Property


