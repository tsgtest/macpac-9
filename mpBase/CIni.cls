VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIni"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long
    
Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias _
    "GetPrivateProfileSectionA" (ByVal lpAppName As String, _
    ByVal lpReturnedString As String, ByVal nSize As Long, _
    ByVal lpFileName As String) As Long
    
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As _
    String) As Long


Public Function GetIni(xSection As String, _
                        xKey As String, _
                        xIni As String) As Variant
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function

Public Function GetIniSection(ByVal xSection As String, _
                              ByVal xIni As String) As Variant
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    Dim i As Integer
    
    xValue = String(1024, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileSection xSection, _
                             xValue, _
                             Len(xValue), _
                             xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIniSection = ""
        Exit Function
    ElseIf xValue <> Empty Then
        Do
            i = iNullPos
            iNullPos = InStr(iNullPos + 1, xValue, Chr(0))
        Loop While iNullPos
        If i Then
            xValue = Left(xValue, i - 1)
        End If
    End If
    GetIniSection = xValue
End Function

Public Function SetIni(xSection As String, _
                        xKey As String, _
                        xValue As String, _
                        xIni As String) As Boolean
'writes the specified value to the specified key
'in the specified ini.
    WritePrivateProfileString xSection, _
                              xKey, _
                              xValue, _
                              xIni

End Function

Public Sub DeleteIniKey(xSection As String, _
                        xKey As String, _
                        xIni As String)
'deletes the specified key
'in the specified ini.
    WritePrivateProfileString xSection, _
                              xKey, _
                              vbNullString, _
                              xIni
End Sub
                        
Public Sub DeleteUserIniKey(ByVal xSection As String, _
                            ByVal xKey As String)
'deletes key in the User.Ini
    DeleteIniKey xSection, xKey, _
        GetUserFilesDirectory() & "\User.ini"
End Sub

Public Sub SetUserIni(ByVal xSection As String, _
                      ByVal xKey As String, _
                      ByVal xValue As String)
'writes to the User.Ini
    SetIni xSection, xKey, xValue, _
        GetUserFilesDirectory() & "\User.ini"
End Sub

Public Function GetUserIni(ByVal xSection As String, _
                           ByVal xKey As String) As Variant
'returns the value of the specified User.ini key.
'returns an empty string if the key does not exist.
    GetUserIni = GetIni(xSection, xKey, _
        GetUserFilesDirectory() & "\User.ini")
End Function

Public Sub SetMacPacIni(ByVal xSection As String, _
                        ByVal xKey As String, _
                        ByVal xValue As String)
'writes to the MacPac.Ini
    SetIni xSection, xKey, xValue, GetAppPath & "\MacPac.ini"
End Sub

Public Function GetMacPacIni(ByVal xSection As String, _
                             ByVal xKey As String) As Variant
'returns the value of the specified MacPac.ini key
'returns an empty string if the key does not exist.
    GetMacPacIni = GetIni(xSection, xKey, GetAppPath & "\MacPac.ini")
End Function

Public Function IniKeyExists(ByVal xSection As String, _
                        ByVal xKey As String, _
                        ByVal xIni As String) As Boolean

'---checks for existence of specified key
    Dim xValue As String
    
    xValue = GetIniSection(xSection, xIni)
    
    If Len(xValue) Then
        '---find key in string returned for section
        IniKeyExists = InStr(xValue, xKey) > 0
    Else
        IniKeyExists = False
    End If
    
End Function

Public Function MacPacIniKeyExists(ByVal xSection As String, _
                             ByVal xKey As String) As Boolean
'checks for existance of specified MacPac.ini key in specified section
    MacPacIniKeyExists = IniKeyExists(xSection, xKey, GetAppPath & "\MacPac.ini")
End Function

Public Function UserIniKeyExists(ByVal xSection As String, _
                             ByVal xKey As String) As Boolean
'checks for existance of specified MacPac.ini key in specified section
    UserIniKeyExists = IniKeyExists(xSection, xKey, GetUserFilesDirectory() & "\User.ini")
End Function

Public Function GetUserIniNumeric(ByVal xSection As String, _
                                  ByVal xKey As String) As Variant
'returns the value of the specified numeric User.ini key.
'returns an empty string if the key does not exist.
    Dim xValue As String
    Dim oStrings As CStrings
    
    Set oStrings = New CStrings
    xValue = GetIni(xSection, xKey, _
        GetUserFilesDirectory() & "\User.ini")
    xValue = oStrings.xLocalizeNumericString(xValue)
    GetUserIniNumeric = xValue
End Function

Public Function GetMacPacIniNumeric(ByVal xSection As String, _
                                    ByVal xKey As String) As Variant
'returns the value of the specified numeric MacPac.ini key
'returns an empty string if the key does not exist.
    Dim xValue As String
    Dim oStrings As CStrings
    
    Set oStrings = New CStrings
    xValue = GetIni(xSection, xKey, GetAppPath & "\MacPac.ini")
    xValue = oStrings.xLocalizeNumericString(xValue)
    GetMacPacIniNumeric = xValue
End Function



