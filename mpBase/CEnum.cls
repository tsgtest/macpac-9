VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEnum"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum mpPageNumberFormats
    mpPageNumberFormat_Arabic = 1
    mpPageNumberFormat_UpperRoman = 2
    mpPageNumberFormat_LowerRoman = 3
    mpPageNumberFormat_UpperAlpha = 4
    mpPageNumberFormat_LowerAlpha = 5
    mpPageNumberFormat_Cardtext = 6
End Enum

Public Enum mpPageNumberLocations
    mpPageNumberLocation_Header = 1
    mpPageNumberLocation_Footer = 2
    mpPageNumberLocation_Cursor = 3
End Enum

Public Enum mpPageNumberAlignments
    mpPageNumberLocation_Left = 1
    mpPageNumberLocation_Center = 2
    mpPageNumberLocation_Right = 3
End Enum

