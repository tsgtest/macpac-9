VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFiles"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Property Get UserFilesDirectory() As String
    Dim xTemp As String
    
    On Error Resume Next
'   get path from ini
    xTemp = GetIni("General", "UserDir", App.Path & "\" & mpFirmINI)
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   raise error if path is invalid
    If Dir(xTemp, vbDirectory) = "" Then
        Err.Raise mpError_InvalidMacPacUserFilesPath
    End If
    
'   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If
    
    Exit Property
ProcError:
    Err.Raise mpError_InvalidMacPacUserFilesPath, _
              "MPO.CApplication.UserFilesDirectory", _
              Error.Desc(Err.Number)
    Exit Property
End Property

Public Property Get TemplatesDirectory() As String
    Dim xTemp As String
    
    On Error Resume Next
'   get path from ini
    xTemp = GetIni("General", "TemplatesDir", App.Path & "\" & mpFirmINI)
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   raise error if path is invalid
    If Dir(xTemp, vbDirectory) = "" Then
        Err.Raise mpError_InvalidMacPacTemplatesPath
    End If
    
'   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If
    
    Exit Property
ProcError:
    Err.Raise mpError_InvalidMacPacTemplatesPath, _
              "MPO.CApplication.TemplatesDirectory", _
              Error.Desc(Err.Number)
    Exit Property
End Property


