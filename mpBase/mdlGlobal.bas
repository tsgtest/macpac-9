Attribute VB_Name = "mdlGlobal"
Option Explicit

Private Const S_OK = &H0                ' Success
Private Const S_FALSE = &H1             ' The Folder is valid, but does not exist
Private Const E_INVALIDARG = &H80070057 ' Invalid CSIDL Value

Private Const CSIDL_LOCAL_APPDATA = &H1C&
Private Const CSIDL_FLAG_CREATE = &H8000&
Private Const CSIDL_COMMON_DOCUMENTS = &H2E

Private Const SHGFP_TYPE_CURRENT = 0
Private Const SHGFP_TYPE_DEFAULT = 1
Private Const MAX_PATH = 260

Private Declare Function SHGetFolderPath Lib "shfolder" _
    Alias "SHGetFolderPathA" _
    (ByVal hwndOwner As Long, ByVal nFolder As Long, _
    ByVal hToken As Long, ByVal dwFlags As Long, _
    ByVal pszPath As String) As Long

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

Public g_oError As mpError.CError

Private m_oFSO As New Scripting.FileSystemObject


Sub Main()
    Set g_oError = New mpError.CError
End Sub

Public Property Get GetUserFilesDirectory() As String
    Dim xTemp As String
    Dim oIni As CIni
    Static xDir As String
    
    If xDir <> Empty Then
'       value has been retrieved already - exit
        GetUserFilesDirectory = xDir
        Exit Property
    End If
    
    Set oIni = New CIni
    
    On Error Resume Next
'   get path from ini
    xTemp = oIni.GetIni("File Paths", "UserDir", GetAppPath & "\MacPac.ini")
    On Error GoTo ProcError
    
    If Len(xTemp) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(xTemp), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(xTemp)
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(xTemp)
        End If
    End If

'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        Err.Raise mpError_InvalidMacPacUserFilesPath
    End If
    
'   add trailing slash if necessary
    If Right(xTemp, 1) <> "\" Then
        xTemp = xTemp & "\"
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetUserFilesDirectory = xTemp
    
    Exit Property
ProcError:
    Err.Raise Err.Number, _
              "mpBase2.GetUserFilesDirectory", _
              g_oError.Desc(Err.Number)
    Exit Property
End Property

Public Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise mpError.mpError_NullValueNotAllowed, , _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    Set oStr = New CStrings
    GetUserVarPath = oStr.xSubstitute(xPath, "<UserName>", xBuf)
        
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "mpBase2.mdlGlobal.GetUserVarPath", Err.Source, _
        Err.Description
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    End If
    
    Set oStr = New CStrings
    
    If xValue <> "" Then
        GetEnvironVarPath = oStr.xSubstitute(xPath, "<" & xToken & ">", xValue)
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpBase2.mdlGlobal.GetEnvironVarPath"
    Exit Function
End Function

Public Function DirExists(xPath As String) As Boolean
    
    Dim oFolder As Scripting.Folder
    
    On Error GoTo ProcError
    
    Set oFolder = m_oFSO.GetFolder(xPath)
        
    DirExists = Not (oFolder Is Nothing)
    
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "mpBase2.mdlGlobal.DirExists", Err.Source, _
        Err.Description
    Exit Function
End Function


Public Function GetAppPath() As String
'Subsitute for App.Path call: first looks in the registry for Public documents location
'If not found, then uses regular App.Path command

    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim oStr As CStrings
    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    Static xDir As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    'get registry key value
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\MacPac 9.0", "DataDirectory", vPath)
    vPath = RTrim(vPath)
    
    If Len(vPath) <= 0 Then
        'try old registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", "MacPacData", vPath)
        vPath = RTrim(vPath)
    End If
    
    'exists, so set path to value
    If Len(vPath) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    Else
        'registry value empty, so set path to App.Path
        xTemp = App.Path
    End If
    
'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        Err.Raise mpError_InvalidMacPacApplicationPath
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetAppPath = xTemp
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpBase2.mdlGlobal.GetAppPath"
    Exit Function
End Function

Public Function GetAppPathNumbering() As String
'Subsitute for App.Path call: first looks in the registry for Public documents location
'If not found, then uses regular App.Path command

    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim oStr As CStrings
    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    Static xDir As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    'get registry key value
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\Numbering", "DataDirectory", vPath)
    vPath = RTrim(vPath)
    
    If Len(vPath) <= 0 Then
    'try old registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", "NumberingData", vPath)
        vPath = RTrim(vPath)
    End If
    
    'exists, so set path to value
    If Len(vPath) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    Else
        'registry value empty, so set path to Numbering's Application Path
        xTemp = oReg.GetDLLOCXPath("MPN90.UserFunctions_Numbering")
    End If
    
'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        Err.Raise mpError_InvalidMacPacApplicationPath
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetAppPathNumbering = xTemp
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpBase2.mdlGlobal.GetAppPathNumbering"
    Exit Function
End Function

Public Function GetAppPathCI() As String
'Subsitute for App.Path call: first looks in the registry for Public documents location
'If not found, then uses regular App.Path command

    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim oStr As CStrings
    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    Static xDir As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    'get registry key value
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\CI", "DataDirectory", vPath)
    vPath = RTrim(vPath)
    
    If Len(vPath) <= 0 Then
    'try old registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", "ContactIntegrationData", vPath)
        vPath = RTrim(vPath)
    End If
    
    'exists, so set path to value
    If Len(vPath) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    Else
        'registry value empty, so set path to CI's Application Path
        xTemp = oReg.GetDLLOCXPath("CIO.CCONTACTS")
    End If
    
'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        Err.Raise mpError_InvalidMacPacApplicationPath
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetAppPathCI = xTemp
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpBase2.mdlGlobal.GetAppPathCI"
    Exit Function
End Function

Public Function GetCommonDocumentsPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    Dim oStr As CStrings
    
    On Error GoTo ProcError
    
'   get common documents folder
    xBuf = String(255, " ")
    
    SHGetFolderPath 0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf

    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise mpError.mpError_NullValueNotAllowed, , _
            "Common_Documents is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute common documents at specified point in path
    Set oStr = New CStrings
    GetCommonDocumentsPath = oStr.xSubstitute(xPath, "<Common_Documents>", xBuf)
        
    Exit Function
ProcError:
    Err.Raise Err.Number, _
        "mpBase2.mdlGlobal.GetCommonDocumentsPath", Err.Source, _
        Err.Description
End Function


