VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNumbers"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function GetTickCount Lib "kernel32" () As Long

'********************************
'Benchmark functions - use these
'instead of Now - they're much
'more precise

Function CurrentTick() As Long
    CurrentTick = GetTickCount()
End Function

Function IsEven(dNum As Double) As Boolean
    If dNum / 2 = CLng(dNum / 2) Then
        IsEven = True
    End If
End Function

Function ElapsedTime(lStartTick As Long) As Single
'returns the time elapsed from lStartTick-
'precision in milliseconds
    ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
End Function
'********************************


Function GetIntegerSequence(aInt() As String, _
                             iFirst As Integer, _
                             iLast As Integer, _
                             Optional bIncludeBlankLine = False) As Boolean
'fills supplied array with a string integer sequence
'from iFirst to iLast
    Dim i As Integer
    
    If bIncludeBlankLine Then
        ReDim aInt(0 To (iLast - iFirst + 1))
        aInt(0) = String(4, "_")
        For i = iFirst To iLast
            aInt(i - iFirst + 1) = i
        Next i
    Else
        ReDim aInt(0 To (iLast - iFirst))
        For i = iFirst To iLast
            aInt(i - iFirst) = i
        Next i
    End If
    
End Function

Function GetOrdinalSequence(aInt() As String, _
                             iFirst As Integer, _
                             iLast As Integer, _
                             Optional bIncludeBlankLine = False) As Boolean
    Dim i As Integer
    
    If bIncludeBlankLine Then
        ReDim aInt(0 To (iLast - iFirst + 1))
        aInt(0) = String(4, "_")
        For i = iFirst To iLast
            aInt(i - iFirst + 1) = GetOrdinal(i)
        Next i
    Else
        ReDim aInt(0 To (iLast - iFirst))
        For i = iFirst To iLast
            aInt(i - iFirst) = GetOrdinal(i)
        Next i
    End If
    
End Function

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Function GetOrdinal(i As Integer) As String
'returns the ordinal position of an integer
'as a string - e.g. 15 --> 15th
    If i >= 4 And i <= 20 Then
        GetOrdinal = i & "th"
    ElseIf Right(i, 1) = 1 Then
        GetOrdinal = i & "st"
    ElseIf Right(i, 1) = 2 Then
        GetOrdinal = i & "nd"
    ElseIf Right(i, 1) = 3 Then
        GetOrdinal = i & "rd"
    Else
        GetOrdinal = i & "th"
    End If
End Function

Function GetOrdinalSuffix(i As Integer) As String
'returns the ordinal position of an integer
'as a string - e.g. 15 --> 15th
    If i >= 4 And i <= 20 Then
        GetOrdinalSuffix = "th"
    ElseIf Right(i, 1) = 1 Then
        GetOrdinalSuffix = "st"
    ElseIf Right(i, 1) = 2 Then
        GetOrdinalSuffix = "nd"
    ElseIf Right(i, 1) = 3 Then
        GetOrdinalSuffix = "rd"
    Else
        GetOrdinalSuffix = "th"
    End If
End Function

Function GetOrdinalFormat(dDate As Date, xFormat As String) As String
'substitutes the ordinal suffix for the ordinal
'variable %o% in format string xFormat
    Dim xSuffix As String
    Dim oStrings As CStrings
    Set oStrings = New CStrings
    xSuffix = GetOrdinalSuffix(Day(dDate))
    GetOrdinalFormat = oStrings.xSubstitute(xFormat, "%o%", """" & xSuffix & """")
    Set oStrings = Nothing
End Function

