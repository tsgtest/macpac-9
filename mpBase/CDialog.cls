VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDialog"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Sub SetLastPosition(oForm As Object)
    Dim oIni As CIni
    
    On Error GoTo ProcError
    
    Set oIni = New CIni
    
'   record last position
    oIni.SetUserIni "Dialog", oForm.Name & "Top", oForm.Top
    oIni.SetUserIni "Dialog", oForm.Name & "Left", oForm.Left
    
    Set oIni = Nothing
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              "mpBase2.SetLastPosition", _
              "Could not set last dialog position."
    Exit Sub
End Sub

Sub MoveToLastPosition(oForm As Object)
'positions dialog to last set position-
'ensures dlg is on the screen
    Dim oIni As CIni
    Dim lXPix As Long
    Dim lYPix As Long
    Dim sT As Single
    Dim sL As Single
    Dim sW As Single
    Dim sH As Single
    Dim oNum As CNumbers
    
    Set oIni = New CIni
    Set oNum = New CNumbers
    
'   move dialog to last position
    On Error Resume Next
    
    With oForm
'       get last recorded position
        sT = oIni.GetUserIni("Dialog", .Name & "Top")
        sL = oIni.GetUserIni("Dialog", .Name & "Left")
        
'       get screen size in pixels
        lYPix = Screen.Height
        lXPix = Screen.Width
    
'       get form size
        sW = .Width
        sH = .Height
        
        If sT + sL <> 0 Then
'           place form - ensure that entire form is on screen
            .Left = oNum.Min(CDbl(sL), lXPix - sW)
            .Top = oNum.Min(CDbl(sT), lYPix - sH)
        Else
'           place form - ensure that entire form is on screen
            .Left = (lXPix / 2) - (sW / 2)
            .Top = (lYPix / 2) - (sH / 2)
        End If
        
    End With
    
    Set oIni = Nothing
    Set oNum = Nothing
    
End Sub
