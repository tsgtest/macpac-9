Attribute VB_Name = "mdlGlobal"
Option Explicit

Public g_oApp As Object

Public Function GetAppObject() As Object
    If g_oApp Is Nothing Then
        Set g_oApp = GetObject(, "Word.Application")
    End If
    Set GetAppObject = g_oApp
End Function
