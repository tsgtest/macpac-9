VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVersionFunctions"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function AddDocument(ByVal xFullName As String, ByVal bNewTemplate As Boolean, Optional bVisible As Boolean = True) As Object
'creates a new document - creates a template if bNewTemplate = TRUE
    If GetAppObject.Version Like "8.*" Then
        Set AddDocument = GetAppObject.Documents.Add(xFullName, bNewTemplate)
    Else
        Set AddDocument = GetAppObject.Documents.Add(xFullName, bNewTemplate, , bVisible)
    End If
End Function

Public Function SetPageView()
'sets the active window to page view
    GetAppObject.ActiveDocument.ActiveWindow.View.Type = 3
'    If Not (word.ActiveDocument.ActiveWindow Is Nothing) Then
'        If GetAppObject.Version Like "8.*" Then
'            GetAppObject.ActiveDocument.ActiveWindow.View.Type = wdPageView
'        Else
'            GetAppObject.ActiveDocument.ActiveWindow.View.Type = wdPrintView
'        End If
'    End If
End Function

Public Sub RunMacro(ByVal xMacroName As String, _
                    ByVal xPropName As String, _
                    ByVal xPropText As String, _
                    ByVal xPropValue As String, _
                    ByVal xBookmark As String)
'runs the Macro xMacroName
    Dim xINI As String
    Dim xT As String
    
    On Error Resume Next
'not in use anymore
'    xINI = mpBase2.ApplicationDirectory & "\MacroVals.ini"
    If GetAppObject.Version Like "8.*" Then
        GetAppObject.application.Run xMacroName
''       write values to ini
'        With GetAppObject.System
'            .PrivateProfileString(xINI, "Properties", xPropName & "_Name") = xPropName
'            .PrivateProfileString(xINI, "Properties", xPropName & "_Text") = xPropText
'            .PrivateProfileString(xINI, "Properties", xPropName & "_Value") = xPropValue
'            .PrivateProfileString(xINI, "Properties", xPropName & "_Bookmark") = xBookmark
'        End With
    Else
        GetAppObject.application.Run xMacroName, _
                             xPropName, _
                             xPropText, _
                             xPropValue, _
                             xBookmark
    End If
End Sub
