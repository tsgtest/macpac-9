VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Const mpError As Integer = vbError + 512

Enum mpErrors
    mpError_SubscriptOutOfRange = 9
    mpError_PersonsNotUpdatable = mpError + 1
    mpError_NoFirmOptionsRecord = mpError + 2
    mpError_FailedSavingOptionSet_IDsMissing = mpError + 3
    mpError_InvalidPleadingCaptionType = mpError + 4
    mpError_DocumentItemDoesNotExist = mpError + 5
    mpError_PleadingCaptionsCollectionFull = mpError + 6
    mpError_PleadingSignaturesCollectionFull = mpError + 7
    mpError_PleadingCounselsCollectionFull = mpError + 8
    mpError_NoTableFound = mpError + 9
    mpError_BookmarkNotFound = mpError + 10
    mpError_TemplateNotFound = mpError + 11
    mpError_TemplateGroupDoesNotExist = mpError + 12
    mpError_InvalidTemplateName = mpError + 13
    mpError_InvalidTemplateFileName = mpError + 14
    mpError_InvalidMember = mpError + 15
    mpError_ItemDoesNotExist = mpError + 16
    mpError_InvalidManualEntryID = mpError + 17
    mpError_InvalidCustomPersonField = mpError + 17
    mpError_NoTemplateGroups = mpError + 18
    mpError_InvalidItemIndex = mpError + 19
    mpError_InvalidMacPacTemplatesPath = mpError + 20
    mpError_InvalidUserTemplatesPath = mpError + 21
    mpError_InvalidWorkgroupTemplatesPath = mpError + 22
    mpError_MissingMacPacINI = mpError + 23
    mpError_InvalidMacPacUserFilesPath = mpError + 24
    mpError_InvalidReuseAuthorCustomFieldStorage = mpError + 25
    mpError_NoReUseAuthorOptions = mpError + 26
    mpError_NoTemplatesSpecified = mpError + 27
    mpError_FormObjectExpected = mpError + 28
    mpError_InvalidPerson = mpError + 29
    mpError_DocumentNotOnDisk = mpError + 30
    mpError_InvalidBPFilesPath = mpError + 31
    mpError_InvalidPublicDB = mpError + 32
    mpError_InvalidBoilerplateFile = mpError + 33
    mpError_InvalidPleadingState = mpError + 34
    mpError_InvalidPleadingType = mpError + 35
    mpError_InvalidPrivateDB = mpError + 36
    mpError_PrivateDBIsOpen = mpError + 37
    mpError_PublicDBIsOpen = mpError + 38
    mpError_NullValueNotAllowed = mpError + 39
    mpError_ArrayDimensionsOutOfBounds = mpError + 40
    mpError_RecipientFieldsAlreadySet = mpError + 41
    mpError_InvalidLetterTemplate = mpError + 42
    mpError_InvalidFaxTemplate = mpError + 43
    mpError_InvalidMemoTemplate = mpError + 44
    mpError_InvalidOptionsTemplateSpecified = mpError + 45
    mpError_NoOffices = mpError + 46
    mpError_NoPeople = mpError + 47
    mpError_InvalidLinkedAuthorProperty = mpError + 48
    mpError_InvalidLinkedOptionsProperty = mpError + 49
    mpError_DocObjectNotInitialized = mpError + 50
    mpError_NoCustomDialogTabs = mpError + 51
    mpError_InvalidCustomForm = mpError + 52
    mpError_TDBLExpected = mpError + 53
    mpError_InvalidBPFileFormat = mpError + 54
    mpError_InvalidPleadingTemplate = mpError + 55
    mpError_AuthorPreferencesExpected = mpError + 56
    mpError_InvalidParameter = mpError + 57
    mpError_InvalidLabelType = mpError + 58
    mpError_InvalidLetterheadType = mpError + 59
    mpError_InvalidMacPacDocObject = mpError + 60
    mpError_CouldNotInsertBP = mpError + 61
    mpError_InvalidFile = mpError + 62
    mpError_CouldNotInitialize = mpError + 63
    mpError_UndefinedDocStampType = mpError + 64
    mpError_InvalidDocStampCategory = mpError + 65
    mpError_InvalidEnvelopeType = mpError + 66
    mpError_InvalidLinkedProperty = mpError + 67
    mpError_ObjectExpected = mpError + 68
    mpError_CollectionNotUpdatable = mpError + 69
    mpError_invalidPeopleDB = mpError + 70
    mpError_BusinessSignaturesCollectionFull = mpError + 71
    mpError_RefreshLinkFailed = mpError + 72
    mpError_CreateLinkFailed = mpError + 73
    mpError_MismatchedPeopleFields = mpError + 74
    mpError_InvalidDocIDDocStamp = mpError + 75
    mpError_CoreComponentsFailed = mpError + 76
    mpError_InvalidUnprotectPassword = mpError + 77
    mpError_CouldNotUnprotectDocument = mpError + 78
    mpError_InvalidSection = mpError + 79
    mpError_InvalidServiceTemplate = mpError + 80
    mpError_InvalidDepoFormatType = mpError + 81
    mpError_InvalidBoilerplateEntry = mpError + 82
    mpError_InvalidNotaryTemplate = mpError + 83
    mpError_InvalidList = mpError + 84
    mpError_InvalidSegmentType = mpError + 85
    mpError_InvalidVerificationTemplate = mpError + 86
    mpError_UserOptsForNoPublicDB = mpError + 87
'**********************************************************
'multitype:
    mpError_InvalidTemplateConfiguration = mpError + 88
'**********************************************************
    mpError_SecurityViolation = mpError + 89
    mpError_InvalidMacPacApplicationPath = mpError + 90

End Enum

Public Sub Show(oErr As ErrObject, Optional ByVal xIntroMessage As String, Optional iSeverity As VbMsgBoxStyle = vbExclamation)
'shows a message box that describes the error condition
'contained in oErr - IntroMessage is the text that appears
'first in the message box
    Const TABS As String = vbTab & vbTab
    Dim lNum As Long
    Dim xDesc As String
    Dim xSource As String
    Dim xMsg As String
 
    With oErr
        xDesc = .Description
        xSource = .Source
        lNum = .Number
    End With
    
    xSource = Replace(xSource, ":", vbCr & TABS)
    
'   show message
    xMsg = xIntroMessage & IIf(xIntroMessage <> Empty, "  ", "") & _
           "The following unexpected error occurred:" & vbCr & vbCr & _
           "Error:  " & TABS & lNum & vbCr & _
           "Description:  " & vbTab & xDesc & vbCr & _
           "Source:  " & TABS & xSource
    MsgBox xMsg, iSeverity, App.Title
End Sub

Public Function Source(ByVal xNewSource As String, ByVal xCurSource As String, Optional ByVal bAppend As Boolean = False) As String
'returns the existing source if it is a 'MacPac'
'source - ie if the source has already been set by us.
'returns xCurSource if the source hasn't been set by us -
'in this case the source will be library name of the function
'in which the error was generated.
    If InStr(Err.Source, ":") = 0 Then
        Err.Source = ""
    Else
        Source = Err.Source
    End If
    Source = Source & xNewSource & ":"
End Function

Public Function Raise(lErr As Long, Optional xSource As String, Optional xSupp As String) As Boolean
'raises error lerr - xSource gets displayed in
'message only when the error is not a Error

    Dim xDescription As String
    Dim iLevel As Integer
    Dim fEndApp As Boolean
    Dim xMsg As String
    
    iLevel = vbExclamation
    Screen.MousePointer = vbDefault
    
    Select Case lErr
        Case Else
            iLevel = vbCritical
            xMsg = "The following error occurred"
            
            If xSource <> "" Then
                xMsg = xMsg & " in " & xSource
            End If
            
            xMsg = xMsg & ":" & vbCr & _
                   lErr & "::" & Error(lErr)
    End Select
    
    Screen.MousePointer = vbDefault
    
    If xMsg <> "" Then
        If xSupp <> Empty Then
            MsgBox xMsg & vbCrLf & xSupp, iLevel
        Else
            MsgBox xMsg, iLevel
        End If
    ElseIf xSupp <> Empty Then
        MsgBox xSupp, iLevel
    End If
End Function

Public Function Desc(Optional ByVal lErr As Long, Optional ByVal xProposedDescription As String) As String
'returns the description for the current (or supplied) error-
'either the current MacPac description, the
'supplied proposed description, or the default
'description for the supplied error (lErr)
    Dim xTag As String
    
'   if a description has a trailing space,
'   we've already set the description for the error
    If Len(Err.Description) Then
        xTag = Right(Err.Description, 1)
    End If
    
    If xTag = " " Then
'       return the description already set
        Desc = Err.Description
    ElseIf xProposedDescription = Empty Or xProposedDescription = " " Then
'       return default description of error since none was supplied
        If lErr Then
'           get default description of supplied error- tag with " " to
'           ensure that future calls to this function do not
'           substitute an error message
            Desc = GetDefaultDescription(lErr) & " "
        Else
'           get default description of current error- tag with " " to
'           ensure that future calls to this function do not
'           substitute an error message
            Desc = GetDefaultDescription(Err.Number) & " "
        End If
    Else
'       return the proposed description - tag with " " to
'       ensure that future calls to this function do not
'       substitute an error message
        Desc = xProposedDescription & " "
    End If
End Function

Public Function GetDefaultDescription(ByVal lErr As Long) As String
    Const mpContactAdmin As String = "  Please contact your administrator."
    Dim Desc As String
    Select Case lErr
        Case mpError_SubscriptOutOfRange
        Case mpError_PersonsNotUpdatable
        Case mpError_NoFirmOptionsRecord
        Case mpError_FailedSavingOptionSet_IDsMissing
        Case mpError_InvalidPleadingCaptionType
        Case mpError_DocumentItemDoesNotExist
        Case mpError_PleadingCaptionsCollectionFull
        Case mpError_PleadingSignaturesCollectionFull
        Case mpError_PleadingCounselsCollectionFull
        Case mpError_NoTableFound
        Case mpError_InvalidSection
            Desc = "Invalid section."
        Case mpError_InvalidUnprotectPassword
            Desc = "The document could not be unprotected. Invalid password."
        Case mpError_CouldNotUnprotectDocument
            Desc = "The document could not be unprotected." & mpContactAdmin
        Case mpError_InvalidFile
            Desc = "Invalid File."
        Case mpError_InvalidSegmentType
            Desc = "Invalid segment type."
        Case mpError_CoreComponentsFailed
            Desc = "The MacPac core components could not be successfully loaded.  " & vbCr & _
                   "You may not have a matched set of application files " & _
                   "registered correctly on this machine." & mpContactAdmin
        Case mpError_InvalidDocIDDocStamp
            Desc = "Invalid Doc ID document stamp.  The ID supplied for the document stamp " & _
                "is not the ID of a Doc ID document stamp."
        Case mpError_BookmarkNotFound
            Desc = "Bookmark is missing from file." & mpContactAdmin
        Case mpError_ObjectExpected
            Desc = "An object was expected but did not exist."
        Case mpError_InvalidLinkedProperty
            Desc = "Invalid linked author property.  Please check tblCustomProperties in mpPublic.mdb."
        Case mpError_InvalidParameter
            Desc = "No item matches the supplied parameters"
        Case mpError_AuthorPreferencesExpected
            Desc = "Author defaults were expected but not found."
        Case mpError_InvalidBPFileFormat
            Desc = "Invalid Boilerplate file format.  Boilerplate files must be saved " & _
                "as Word Document Templates with the extension .mbp."
        Case mpError_TDBLExpected
            Desc = "Type mismatch.  A TDBL object was expected by this function."
        Case mpError_InvalidCustomForm
            Desc = "Invalid Custom Form.  Please check tblCustomDialogs in mpPublic.mdb."
        Case mpError_NoCustomDialogTabs
            Desc = "You must specify at least one tab for a custom dialog.  " & _
                   "Please check tblTemplates in mpPublic.mdb."
        Case mpError_DocObjectNotInitialized
            Desc = "The document object could not " & _
                   "be correctly initialized. " & mpContactAdmin
        Case mpError_InvalidLinkedAuthorProperty
            Desc = "Invalid linked property for one or more custom author " & _
                   "properties.  Please check the linked property field of " & _
                   "tblCustomProperties in mpPublic.mdb."
        Case mpError_InvalidLinkedOptionsProperty
            Desc = "Invalid linked property for one or more custom author " & _
                   "options properties.  Please check the linked property field of " & _
                   "tblCustomProperties in mpPublic.mdb."
        Case mpError_NoPeople
            Desc = "No people have been specified.  You must setup " & _
                   "the application  to have at least one person."
        Case mpError_NoOffices
            Desc = "No offices have been specified.  You must setup " & _
                   "the application  to have at least one office."
        Case mpError_InvalidOptionsTemplateSpecified
            Desc = "Invalid template specified for option set.  " & _
                   "Please ensure that you specify a valid template " & _
                   "before attempting to retrieve options."
        Case mpError_InvalidFaxTemplate
            Desc = "No template is associated with MPO.CFax in mpPublic.mdb tblTemplates." & mpContactAdmin
        Case mpError_InvalidServiceTemplate
            Desc = "No template is associated with MPO.CService in mpPublic.mdb tblTemplates." & mpContactAdmin
        Case mpError_InvalidLetterTemplate
            Desc = "No template is associated with MPO.CLetter in mpPublic.mdb tblTemplates." & mpContactAdmin
        Case mpError_InvalidMemoTemplate
            Desc = "No template is associated with MPO.CMemo in mpPublic.mdb tblTemplates." & mpContactAdmin
        Case mpError_NullValueNotAllowed
            Desc = "String parameter must not be empty or null."
        Case mpError_ArrayDimensionsOutOfBounds
            Desc = "Array dimensions are incorrect."
        Case mpError_RecipientFieldsAlreadySet
            Desc = "Field Names cannot be changed for a collection with existing items."
        Case mpError_PublicDBIsOpen
            Desc = "The file mpPublic.mdb is currently in use.  Please exit Word, " & _
                   "close the file, and restart Word."
        Case mpError_PrivateDBIsOpen
            Desc = "The file mpPrivate.mdb is currently in use.  Please exit Word, " & _
                   "close the file, and restart Word."
        Case mpError_InvalidBoilerplateFile
            Desc = "Could not find the required boilerplate file in the MacPac\Boilerplate folder."
        Case mpError_InvalidPleadingState
            Desc = "There are no Pleading Paper types defined for the selected state."
        Case mpError_InvalidPleadingType
            Desc = "Pleading paper type must be assigned before updating pleading paper."
        Case mpError_InvalidPublicDB
            Desc = "Invalid public database file name file specified in MacPac.ini." & mpContactAdmin
        Case mpError_InvalidPrivateDB
            Desc = "The file mpPrivate.mdb is missing from the personal files directory." & mpContactAdmin
        Case mpError_InvalidBPFilesPath
            Desc = "Invalid boilerplate files directory." & mpContactAdmin
        Case mpError_FormObjectExpected
            Desc = "This function requires a form as an argument."
        Case mpError_NoTemplatesSpecified
            Desc = "No templates were specified for use." & mpContactAdmin
        Case mpError_NoReUseAuthorOptions
            Desc = "No option set found for this author during reuse."
        Case mpError_InvalidReuseAuthorCustomFieldStorage
            Desc = "Could not successfully retrieve the custom author field " & _
                   "that is currently stored in the active Word document.  Please " & _
                   "select another author."
        Case mpError_InvalidMacPacUserFilesPath
            Desc = "Invalid MacPac user files directory." & mpContactAdmin
        Case mpError_TemplateNotFound
            Desc = "Could not find the specified MacPac template." & mpContactAdmin
        Case mpError_TemplateGroupDoesNotExist
        Case mpError_InvalidTemplateName, mpError_InvalidTemplateFileName
            Desc = "Invalid MacPac Template." & _
                   "Please have your administrator check the File Name field in mpPublic.mdb."
        Case mpError_InvalidMacPacDocObject
            Desc = "An invalid MacPac DocObject was supplied to this function. " & _
                   "Please have your administrator check the Class Name field in mpPublic.mdb."
        Case mpError_InvalidPerson
            Desc = "Invalid person was supplied."
        Case mpError_InvalidUserTemplatesPath
            Desc = "Invalid User Templates directory." & mpContactAdmin
        Case mpError_InvalidWorkgroupTemplatesPath
            Desc = "Invalid Workgroup Templates directory." & mpContactAdmin
        Case mpError_InvalidMacPacTemplatesPath
            Desc = "Invalid MacPac Templates Directory." & mpContactAdmin
        Case mpError_InvalidItemIndex, mpError_InvalidMember
            Desc = "Requested member of collection does not exist."
        Case mpError_NoTemplateGroups
            Desc = "No Template Groups have been specified."
        Case mpError_ItemDoesNotExist
            Desc = "The requested document item does not exist."
        Case mpError_InvalidManualEntryID
            Desc = "The ID for the current manually entered person is invalid."
        Case mpError_InvalidCustomPersonField
            Desc = "A specified custom field does not exist in tblPeople."
        Case mpError_InvalidLabelType
            Desc = "Invalid Label Type.  Please check tblLabels in mpPublic.mdb."
        Case mpError_InvalidEnvelopeType
            Desc = "Invalid Envelope Type.  Please check tblEnvelopes in mpPublic.mdb."
        Case mpError_InvalidLetterheadType
            Desc = "Invalid Letterhead Type.  Please check tblLetterheads in mpPublic.mdb."
        Case mpError_CouldNotInsertBP
            Desc = "Could not insert the specified boilerplate entry."
        Case mpError_CouldNotInitialize
            Desc = "Could not correctly initialize the MacPac core components.  Please " & _
                "ensure that MacPac90.dll references the correct version of the " & _
                "core components.  You may need to recompile MacPac90.dll"
        Case mpError_UndefinedDocStampType
            Desc = "Stamp type must be assigned before inserting document stamp."
        Case mpError_InvalidDocStampCategory
            Desc = "There are no document stamp types defined for the specified category."
        Case mpError_CollectionNotUpdatable
            Desc = "The collection was not updatable.  Please review your documentation."
        Case mpError_invalidPeopleDB
            Desc = "Invalid People Database file specified in MacPac.ini." & mpContactAdmin
        Case mpError_BusinessSignaturesCollectionFull
            Desc = ""
        Case mpError_InvalidNotaryTemplate
            Desc = "No template is associated with MPO.CNotary in mpPublic.mdb tblTemplates." & mpContactAdmin
        Case mpError_InvalidList
            Desc = "Invalid List.  Please check tblLists/tblCustomLists in mpPublic.mdb."
        Case mpError_RefreshLinkFailed
            Desc = "Could not refresh the links in mpPublic.mdb." & mpContactAdmin
        Case mpError_CreateLinkFailed
            Desc = "Could not create links to the people tables in mpPublic.mdb." & mpContactAdmin
        Case mpError_MismatchedPeopleFields
            Desc = "The fields in mpPrivate.tblPeopleInput do not match the fields in mpPublic.qryPeoplePublic.  " & _
                   "Please ensure that the fields in the public people table(s) are identical to those in the " & _
                   "input people table in the private database."
        Case mpError_InvalidBoilerplateEntry
            Desc = "The inserted boilerplate entry cannot be used by this function. " & _
                "It is either missing required content or contains content " & _
                "and/or formatting that prevents this function from executing.  " & _
                "Please review your administrative documentation for more " & _
                "information concerning the requirements and restrictions of " & _
                "using this boilerplate."
        Case mpError_UserOptsForNoPublicDB
            Desc = "Can't connect to a MacPac public database. You won't be able to run MacPac functions in Word."
        Case mpError_SecurityViolation
            Desc = "Cannot perform this operation on a secure document."
        Case mpError_InvalidMacPacApplicationPath
            Desc = "Cannot find Application Path"
        Case Else
            If InStr(Err.Description, "connections are invalid") Then
                Desc = "An error occurred that has forced MacPac to shut down.  Please restart Word."
            Else
                Desc = Error(lErr) & "."
            End If
    End Select
    
    GetDefaultDescription = Desc
End Function

Public Function Version() As String
    On Error Resume Next
    Version = App.Major & "." & App.Minor & "." & App.Revision
End Function

