VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CiManageProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**********************************************************
'   CiManageProfile Class
'   created 12/13/99 by Jeffrey Sweetland

'   Contains properties and methods to
'   access iManage Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Const mpDocProfile_DOCNUMBER As String = "9"
Const mpDocProfile_DOCNAME As String = "7"
Const mpDocProfile_AUTHORUSERID As String = "1"
Const mpDocProfile_TYPISTUSERID As String = "8"
Const mpDocProfile_DOCTYPEID As String = "3"
Const mpDocProfile_MATTERID As String = "16"
Const mpDocProfile_CLIENTID As String = "15"
Const mpDocProfile_LIBRARY As String = "13"

Const mpDMSFileSaveMacro As String = "IMan80S.FileSave.FileSave"
Const mpDMSFileSaveAsMacro As String = "IMan80S.FileSaveAs.FileSaveAs"
Const mpDMSFileCloseMacro As String = "IMan80S.FileClose.FileClose"

Const xIniSection As String = "IManage4x"

Implements IDMS
Implements IProfileCreator

Private WithEvents m_oEvent As CEventGenerator
Attribute m_oEvent.VB_VarHelpID = -1

Private Declare Function MHIsDocInDatabase Lib "MHDLL32" (ByVal DocType$, _
                                                        ByVal FileSpec$) As Integer

Private Declare Function MHGetDocumentInfo Lib "MHDLL32" (ByVal FileSpec$, _
                                                            ByVal Item As Integer, _
                                                            ByVal outData$, _
                                                            ByVal outDataLen As Integer) As Integer

Private Declare Function MHIsManageThere Lib "MHDLL32" () As Integer
Private m_oCustFields As CCustomFields

'***Class Events
Private Sub Class_Initialize()
    Set m_oEvent = New CEventGenerator
End Sub

Private Sub Class_Terminate()
    Set m_oEvent = Nothing
End Sub

'***Properties***
Private Property Get IDMS_AuthorID() As String
    IDMS_AuthorID = GetProfileInfo(mpDocProfile_AUTHORUSERID)
End Property
Private Property Get IDMS_ClientID() As String
    IDMS_ClientID = GetProfileInfo(mpDocProfile_CLIENTID)
End Property
'MHDLL32 does not return date fields so we are using Word's built-in doc prop here
Private Property Get IDMS_CreationDate() As String
    Dim xStrDate As String
    Dim xStrTime As String
    
    mpBase2.CStrings.xSplitString ActiveDocument.BuiltInDocumentProperties(wdPropertyTimeCreated), _
                                                            xStrDate, xStrTime, " "
    IDMS_CreationDate = xStrDate
End Property
'MHDLL32 does not return date fields so we are using Word's built-in doc prop here
Private Property Get IDMS_RevisionDate() As String
    Dim xStrDate As String
    Dim xStrTime As String
        
    mpBase2.CStrings.xSplitString ActiveDocument.BuiltInDocumentProperties(wdPropertyTimeLastSaved), _
                                                            xStrDate, xStrTime, " "
    
    IDMS_RevisionDate = xStrDate
End Property

Private Property Get IDMS_Custom1() As String
    If IDMS_CustomFields.Count > 0 Then
        IDMS_Custom1 = GetProfileInfo(IDMS_CustomFields.Item(1).Value)
    End If
End Property
Private Property Get IDMS_Custom2() As String
    If IDMS_CustomFields.Count > 1 Then
        IDMS_Custom2 = GetProfileInfo(IDMS_CustomFields.Item(2).Value)
    End If
End Property
Private Property Get IDMS_Custom3() As String
    If IDMS_CustomFields.Count > 2 Then
        IDMS_Custom3 = GetProfileInfo(IDMS_CustomFields.Item(3).Value)
    End If
End Property
Private Function IDMS_CustomFields() As CCustomFields
    
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function

Private Property Get IDMS_DocName() As String
    IDMS_DocName = GetProfileInfo(mpDocProfile_DOCNAME)
End Property
Private Property Get IDMS_DocNumber() As String
    IDMS_DocNumber = GetProfileInfo(mpDocProfile_DOCNUMBER)
End Property
Private Property Get IDMS_DocTypeID() As String
    IDMS_DocTypeID = GetProfileInfo(mpDocProfile_DOCTYPEID)
End Property

Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property

Private Property Get IDMS_Library() As String
    IDMS_Library = GetProfileInfo(mpDocProfile_LIBRARY)
End Property
Private Property Get IDMS_MatterID() As String
    IDMS_MatterID = GetProfileInfo(mpDocProfile_MATTERID)
End Property
Private Property Get IDMS_TypistID() As String
    IDMS_TypistID = GetProfileInfo(mpDocProfile_TYPISTUSERID)
End Property
Private Property Get IDMS_Version() As String
    Dim xVersion As String
    Dim ManageStatus As Integer
    Dim xFileSpec As String
    Dim xDocType As String
    Dim ExistStatus As Integer
    Dim ReturnStatus As Integer

    ManageStatus = MHIsManageThere

    If ActiveDocument Is Nothing Then Exit Property
    
    If ManageStatus <> 1 Then
        Exit Property
    End If
    
    
    xFileSpec = ActiveDocument.FullName
    xDocType = "WORD97"

    ExistStatus = MHIsDocInDatabase(xDocType, xFileSpec)

    If ExistStatus = 0 Then     '***if document does not exist in manage do nothing
        Exit Property
    End If
    
    xVersion = String(8, " ")
    ReturnStatus = MHGetDocumentInfo(xFileSpec, 10, xVersion, 8)

    If (ReturnStatus <> 1) Then
        Exit Property
    End If
    
    IDMS_Version = xTranslateString(xVersion)

End Property
Private Property Get IDMS_AuthorName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CiManageProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_TypistName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CiManageProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_ClientName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CiManageProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_MatterName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CiManageProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_DocTypeDescription() As String
    Err.Raise E_NOTIMPL, "mpDMS.CiManageProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Abstract() As String
    Err.Raise E_NOTIMPL, "mpDMS.CiManageProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_GroupName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CiManageProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Path() As String
    If ActiveDocument Is Nothing Then Exit Property
    IDMS_Path = ActiveDocument.Path
End Property
Private Property Get IDMS_FileName() As String
    If ActiveDocument Is Nothing Then Exit Property
    IDMS_FileName = ActiveDocument.Name
End Property
'***Methods***
Public Function GetProfileInfo(vItem As Variant) As String
                            
    Dim xItemBuffer As String
    Dim xOpenFileName As String
    
    xItemBuffer = String(512, " ")

    Dim xVersion As String
    Dim ManageStatus
    Dim FileSpec$
    Dim DocType$
    Dim ExistStatus
    Dim xValue As String
    Dim ReturnStatus As Integer

    ManageStatus = MHIsManageThere

    If ActiveDocument Is Nothing Then Exit Function
    
    If ManageStatus <> 1 Then
        Exit Function
    End If
    
    FileSpec$ = ActiveDocument.FullName
    DocType$ = "WORD97"

    ExistStatus = MHIsDocInDatabase(DocType$, FileSpec$)

    If ExistStatus = 0 Then     '***if document does not exist in manage do nothing
        Exit Function
    End If
    
    xValue = String(256, " ")
    ReturnStatus = MHGetDocumentInfo(FileSpec$, Val(vItem), xValue, 256)

    If (ReturnStatus <> 1) Then
        Exit Function
    End If
    
    GetProfileInfo = xTranslateString(xValue)

End Function
Private Function xTranslateString(xVer As String)
    Dim sVar As String
    Dim l As Integer

    For l = 1 To Len(xVer)
        sVar = Mid(xVer, l, 1)
        On Error GoTo DocVerError
        
        If Asc(sVar) = False Then
DocVerError:
           Err = 0
           xVer = Left(xVer, l - 1)
           xTranslateString = xVer
           Exit For
        End If
    Next l
    
End Function
Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    IDMS_GetProfileInfo = GetProfileInfo(vItem)
End Function
Private Function IDMS_MainInterface() As Object
    Dim oRet As CiManageProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function
Private Sub IDMS_FileSave()
    FileSave
End Sub
Private Sub IDMS_FileSaveAs()
    FileSaveAs
End Sub
Private Sub IDMS_DocClose()
    DocClose
End Sub
Private Sub IDMS_FileClose()
    FileClose
End Sub
Public Sub FileSaveAs()

    Dim bIsUnprotected As Boolean
    Dim lProtection As Long
    Dim xPassword As String
    Dim tmpCurrent As Word.Template
    Dim xCurFile As String

    On Error GoTo FileSaveAs_Error
    If Documents.Count = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If

'   UnProtect document if necessary
    bIsUnprotected = bUnprotect(lProtection, _
                                xPassword, _
                                False, _
                                "File Save As")

    If Not bIsUnprotected Then Exit Sub

    xCurFile = Word.ActiveDocument.FullName
    Application.Run MacroName:=mpDMSFileSaveAsMacro

    Set tmpCurrent = ActiveDocument.AttachedTemplate
    If Not tmpCurrent.Saved And tmpCurrent <> NormalTemplate Then
        tmpCurrent.Saved = True
    End If

    If UCase(xCurFile) <> UCase(Word.ActiveDocument.FullName) Then
        m_oEvent.RaiseFileSavedEvent
    End If

'***Protect document if necessary
    bProtect lProtection, xPassword

    AppActivate "Microsoft Word"
    Exit Sub

FileSaveAs_Error:
    Err.Raise Err.Number, "mpDMS.ciManageProfile.FileSaveAs", Err.Description
End Sub

Public Sub FileSave()
    
    Dim xCurFile As String
    Dim bWasSaved As Boolean
    Dim bIsUnprotected As Boolean
    Dim lProtection As Long
    Dim xPassword As String
    
    On Error GoTo FileSave_Error
    
    '9.7.1 #4233 - avoid error 5155 for read-only docs
    If ActiveDocument.ReadOnly Then
        FileSaveAs
        Exit Sub
    End If
'   UnProtect document if necessary
    bIsUnprotected = bUnprotect(lProtection, _
                                xPassword, _
                                False, _
                                "File Save")
    If Not bIsUnprotected Then Exit Sub
    
    With ActiveDocument
        If .Saved Then
            bWasSaved = True
            .Saved = False
        End If
        Application.Run MacroName:=mpDMSFileSaveMacro
    End With
    
    With ActiveDocument
        If Not .Saved And bWasSaved Then
            .Saved = True
        ElseIf .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
'***Protect document if necessary
    bProtect lProtection, xPassword
    
    AppActivate "Microsoft Word"
    
    Exit Sub
    
FileSave_Error:
    Err.Raise Err.Number, "mpDMS.ciManageProfile.FileSave", Err.Description
End Sub
Public Sub FileClose()
    Dim xCurFile As String
    Dim bIsUnprotected As Boolean
    Dim lProtection As Long
    Dim xPassword As String
    Dim iUserChoice As Integer
    
    xCurFile = ActiveDocument.FullName
    If Not ActiveDocument.Saved Then
'           prompt for doc save
        xMsg = "Do you want to save the changes you made to " & ActiveDocument.Name & "?"
        iUserChoice = MsgBox(xMsg, _
                             vbExclamation + vbYesNoCancel, _
                             "Microsoft Word")
        Select Case iUserChoice
            Case vbYes
                bIsUnprotected = bUnprotect(lProtection, _
                                        xPassword, _
                                        True, _
                                        "File Close")
                If Not bIsUnprotected Then Exit Sub
                
                Application.Run mpDMSFileSaveMacro
                
                If ActiveDocument.Saved Then
                    m_oEvent.RaiseFileSavedEvent

                    Application.Run mpDMSFileCloseMacro
                Else
'***                    if iManage dbox was cancelled
'***                    we need to reprotect document and delete doc var
                    bProtect lProtection, xPassword
                    If lProtection <> wdNoProtection Then
                        ActiveDocument.Variables("ProtectionType").Delete
                        If xPassword <> "" Then _
                            ActiveDocument.Variables("ProtectionPassword").Delete
                    End If

                End If
            Case vbNo
                ActiveDocument.Saved = True
                Application.Run mpDMSFileCloseMacro
            Case Else
        End Select
    Else
        bIsUnprotected = bUnprotect(lProtection, _
                    xPassword, _
                    True, _
                    "File Close")
        If Not bIsUnprotected Then Exit Sub
        Application.Run mpDMSFileCloseMacro
    End If

'       code below resets pointer,
'       which seems to remain as an hourglass
'       if the user cancels out of save

    On Error Resume Next
    Application.ScreenRefresh
    AppActivate "Microsoft Word"
    Exit Sub
FileClose_Error:
    Err.Raise Err.Number, "mpDMS.ciManageProfile.FileClose", Err.Description
End Sub

Public Sub FileOpen()
    m_oEvent.RaiseFileOpenedEvent
End Sub

Public Sub DocClose()
'   close active window if doc has
'   more than one window, else close
'   file - (this mimics Native DOC CLOSE)

    On Error Resume Next
    If Application.Windows.Count = 0 Then _
        Exit Sub
    
    If ActiveDocument.Windows.Count > 1 Then
        If Word.ActiveDocument.ActiveWindow.View = wdPrintPreview Then
            ActiveDocument.ClosePrintPreview
        Else
            ActiveDocument.ActiveWindow.Close
        End If
    Else
        FileClose
    End If
End Sub

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AuthorID(RHS As Variant)
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
End Property

Private Property Get IProfileCreator_ClientID() As Variant
End Property

Private Function IProfileCreator_CreateProfile() As Variant
End Function

Private Function IProfileCreator_CustomProperties() As Collection
End Function

Private Property Get IProfileCreator_DocName() As String
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
End Property

Private Property Get IProfileCreator_Library() As Variant
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
End Property

Private Property Get IProfileCreator_MatterID() As Variant
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
End Property

Private Property Get IProfileCreator_TypistID() As Variant
End Property

Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
End Property

Private Property Get IProfileCreator_DepartmentID() As String
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
End Property

Private Property Get IProfileCreator_SecurityCode() As String
End Property

