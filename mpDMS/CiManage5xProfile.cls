VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CiManage5xProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CiManageProfile5x Class
'   created 8/9/00 by Daniel Fisherman

'   Contains properties and methods to
'   access iManage Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Const mpDocProfile_DOCNUMBER = 1  ' nrDocNum
Const mpDocProfile_DOCNAME = 3 ' nrDescription
Const mpDocProfile_DOCTYPEID = 8  ' nrClass   '***9.7.1
Const mpDocProfile_DOCTYPEDESCRIPTION = 77 ' nrClassDescription  '***9.7.1
Const mpDocProfile_LIBRARY = 0 ' nrDatabase
Const mpDocProfile_AUTHORUSERID = 5 ' nrAuthor
Const mpDocProfile_AUTHORFULLNAME = 74 ' nrAuthorDescription  '***9.7.1
Const mpDocProfile_TYPISTUSERID = 18 ' nrLastUser
Const mpDocProfile_TYPISTFULLNAME = 79  ' nrLastUserDescription
Const mpDocProfile_CREATIONDATE = 11 ' nrCreateDate
Const mpDocProfile_LASTEDITDATE = 10 ' nrEditDate
Const mpDocProfile_COMMENT = 24 ' nrComment
Const mpDocProfile_CLIENTID = 25 ' nrCustom1
Const mpDocProfile_CLIENTNAME = 60 ' nrCustom1Description
Const mpDocProfile_MATTERID = 26 ' nrCustom2
Const mpDocProfile_MATTERNAME = 61 ' nrCustom2Description
Const mpDocProfile_VERSION = 2  ' nrVersion

Const xIniSection = "Imanage5x"
Implements IDMS
Implements IProfileCreator

' Private WithEvents m_oExt As iManO2K.iManageExtensibility
Private m_oExt As Object
Private m_oEvent As CEventGenerator
Private m_oNRTDoc As Object
Private m_oCustFields As CCustomFields


'***Class Events
Private Sub Class_Initialize()
    If g_iTargetApp = mpApps.mpApp_Excel Then
        InitializeExcel
    ElseIf g_iTargetApp = mpApps.mpApp_PPT Then
        InitializePowerPoint
    Else
        InitializeWord
    End If
End Sub
Private Sub InitializeWord()
'   dim word as object so that we don't need
'   a reference to the Word 9 object library -
'   compatibility with Word 97 is necessary
    Dim oWord As Object 'word.application
    Dim oAddIn As Object
    
    On Error GoTo ProcError
    Set oWord = Word.Application
    
    On Error Resume Next
    'GLOG 5608
    If Val(Word.Application.Version) <= 11 Then
        'JTS 10/22/12: Reverse the order here so that if both Addins
        'are present, only the latest will be recognized
        Set oAddIn = oWord.COMAddIns("WorkSiteOffice2003Addins.Connect")
        'Different Add-in used with Worksite 8.5 SP3
        If oAddIn Is Nothing Then
            Set oAddIn = oWord.COMAddIns("iManO2K.AddinForWord2000")
        End If
    Else
        'Different Add-in used with Worksite 8.5 SP3
        Set oAddIn = oWord.COMAddIns("WorkSiteOffice2007Addins.Connect")
        If oAddIn Is Nothing Then
            Set oAddIn = oWord.COMAddIns("oUTR02K.Connect")
        End If
    End If
    On Error GoTo ProcError
    
    If oAddIn Is Nothing Then
        Err.Raise mpErrors.mpError_InvalidMember, , _
        "iManO2K could not be found in the list of COM add-in.  " & _
        "You may need to re-install iManage.  Please contact your administrator."
    End If
    
    Set m_oExt = oAddIn.object
    Set m_oEvent = New CEventGenerator
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile.InitializeWord", Err.Description

End Sub
Private Sub InitializeExcel()
'   dim excel as object so that we don't need
'   a reference to the excel 9 object library -
'   compatibility with excel 97 is necessary
    Dim oExcel As Object 'Excel.application
    Dim oAddIn As Object
    
    On Error GoTo ProcError
    Set oExcel = GetExcelObject()
    
    On Error Resume Next
    'GLOG 5608
    If Val(oExcel.Version) < 12 Then
        Set oAddIn = oExcel.COMAddIns("iManO2K.AddinForExcel2000")
        'Different Add-in used with Worksite 8.5 SP3
        If oAddIn Is Nothing Then
            Set oAddIn = oExcel.COMAddIns("WorkSiteOffice2003Addins.Connect")
        End If
    Else
        'Different Add-in used with Worksite 8.5 SP3
        Set oAddIn = oExcel.COMAddIns("WorkSiteOffice2007Addins.Connect")
        If oAddIn Is Nothing Then
            Set oAddIn = oExcel.COMAddIns("oUTR02K.Connect")
        End If
    End If
    On Error GoTo ProcError
    
    If oAddIn Is Nothing Then
        Err.Raise mpErrors.mpError_InvalidMember, , _
        "iManO2K could not be found in the list of COM add-ins.  " & _
        "You may need to re-install iManage.  Please contact your administrator."
    End If
    
    Set m_oExt = oAddIn.object
    Set m_oEvent = New CEventGenerator
    Exit Sub

ProcError:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile.InitializeExcel", Err.Description

End Sub
Private Sub InitializePowerPoint()
    Dim oPPT As Object 'Excel.application
    Dim oAddIn As Object
    
    On Error GoTo ProcError
    Set oPPT = GetPPTObject()
    
    On Error Resume Next
    'GLOG 5608
    If Val(oPPT.Version) < 12 Then
        Set oAddIn = oPPT.COMAddIns("iManO2K.AddinForPowerpoint2000")
        'Different Add-in used with Worksite 8.5 SP3
        If oAddIn Is Nothing Then
            Set oAddIn = oPPT.COMAddIns("WorkSiteOffice2003Addins.Connect")
        End If
    Else
        'Different Add-in used with Worksite 8.5 SP3
        Set oAddIn = oPPT.COMAddIns("WorkSiteOffice2007Addins.Connect")
        If oAddIn Is Nothing Then
            Set oAddIn = oPPT.COMAddIns("oUTR02K.Connect")
        End If
    End If
    On Error GoTo ProcError
    
    If oAddIn Is Nothing Then
        Err.Raise mpErrors.mpError_InvalidMember, , _
        "iManO2K could not be found in the list of COM add-ins.  " & _
        "You may need to re-install iManage.  Please contact your administrator."
    End If
    
    Set m_oExt = oAddIn.object
    Set m_oEvent = New CEventGenerator
    Exit Sub

ProcError:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile.InitializePowerpoint", Err.Description
End Sub
Private Sub Class_Terminate()
    Set m_oEvent = Nothing
End Sub

'***Properties***
Private Property Get IDMS_AuthorID() As String
    IDMS_AuthorID = GetProfileInfo(mpDocProfile_AUTHORUSERID)
End Property
Private Property Get IDMS_ClientID() As String
    IDMS_ClientID = GetProfileInfo(mpDocProfile_CLIENTID)
End Property
Private Property Get IDMS_CreationDate() As String
    Dim xStrDate As String
    Dim xStrTime As String
    Dim xTemp As String
    
    xTemp = GetIni(xIniSection, "CreationDateFromDocProps", mpBase2.ApplicationDirectory & g_xIni)

    If UCase(xTemp) = "TRUE" Then
        mpBase2.CStrings.xSplitString ActiveDocument.BuiltInDocumentProperties(wdPropertyTimeCreated), _
                                                                xStrDate, xStrTime, " "
        
        IDMS_CreationDate = xStrDate
    Else
        IDMS_CreationDate = GetProfileInfo(mpDocProfile_CREATIONDATE)
    End If
End Property
Private Property Get IDMS_DocName() As String
    IDMS_DocName = GetProfileInfo(mpDocProfile_DOCNAME)
End Property
Private Property Get IDMS_DocNumber() As String
    IDMS_DocNumber = GetProfileInfo(mpDocProfile_DOCNUMBER)
End Property
Private Property Get IDMS_DocTypeID() As String
    IDMS_DocTypeID = GetProfileInfo(mpDocProfile_DOCTYPEID)
End Property

Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property

Private Property Get IDMS_IsOn() As Boolean
    IDMS_IsOn = GetProfileInfo(mpDocProfile_DOCNUMBER) <> ""
End Property

Private Property Get IDMS_Library() As String
    IDMS_Library = GetProfileInfo(mpDocProfile_LIBRARY)
End Property

Private Property Get IDMS_MatterID() As String
    IDMS_MatterID = GetProfileInfo(mpDocProfile_MATTERID)
End Property

Private Property Let IDMS_MiscObject(RHS As Object)
    Set m_oNRTDoc = RHS
End Property

Private Property Get IDMS_MiscObject() As Object
    Set IDMS_MiscObject = m_oNRTDoc
End Property

Private Property Get IDMS_RevisionDate() As String
    Dim xStrDate As String
    Dim xStrTime As String
    Dim xDocPropsDate As String
    Dim xSystemDate As String
    Dim xFormat As String
    
    xDocPropsDate = GetIni(xIniSection, "EditDateFromDocProps", mpBase2.ApplicationDirectory & g_xIni)
    xSystemDate = GetIni(xIniSection, "EditDateFromSystem", mpBase2.ApplicationDirectory & g_xIni)
    xFormat = GetIni(xIniSection, "EditDateFromSystemFormat", mpBase2.ApplicationDirectory & g_xIni)
    
    If UCase(xDocPropsDate) = "TRUE" Then
        On Error Resume Next
        mpBase2.CStrings.xSplitString ActiveDocument.BuiltInDocumentProperties(wdPropertyTimeLastSaved), _
                                                                xStrDate, xStrTime, " "
        If xStrDate = "" Then
            mpBase2.CStrings.xSplitString ActiveDocument.BuiltInDocumentProperties(wdPropertyTimeCreated), _
                                                                    xStrDate, xStrTime, " "
        End If
        IDMS_RevisionDate = xStrDate
    ElseIf UCase(xSystemDate) = "TRUE" Then
        If xFormat = "" Then xFormat = "MM/d/yy"
        xStrDate = Format(Now, xFormat)
        IDMS_RevisionDate = xStrDate
    Else
        IDMS_RevisionDate = GetProfileInfo(mpDocProfile_LASTEDITDATE)
    End If
End Property

Private Property Get IDMS_TypistID() As String
    IDMS_TypistID = GetProfileInfo(mpDocProfile_TYPISTUSERID)
End Property
Private Property Get IDMS_Version() As String
    IDMS_Version = GetProfileInfo(mpDocProfile_VERSION)
End Property
Private Property Get IDMS_AuthorName() As String
    IDMS_AuthorName = GetProfileInfo(mpDocProfile_AUTHORFULLNAME)
End Property
Private Property Get IDMS_TypistName() As String
    IDMS_TypistName = GetProfileInfo(mpDocProfile_TYPISTFULLNAME)
End Property
Private Property Get IDMS_ClientName() As String
    IDMS_ClientName = GetProfileInfo(mpDocProfile_CLIENTNAME)
End Property
Private Property Get IDMS_MatterName() As String
    IDMS_MatterName = GetProfileInfo(mpDocProfile_MATTERNAME)
End Property
Private Property Get IDMS_DocTypeDescription() As String
    IDMS_DocTypeDescription = GetProfileInfo(mpDocProfile_DOCTYPEDESCRIPTION)
End Property
Private Property Get IDMS_Abstract() As String
    IDMS_Abstract = GetProfileInfo(mpDocProfile_COMMENT)
End Property
Private Property Get IDMS_GroupName() As String
    Err.Raise E_NOTIMPL, "mpDMS.cIManage5xProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Path() As String
    IDMS_Path = GetTargetAppFilePath
End Property
Private Property Get IDMS_FileName() As String
    IDMS_FileName = GetTargetAppFileName
End Property
Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    IDMS_GetProfileInfo = GetProfileInfo(vItem)
End Function
Private Function IDMS_MainInterface() As Object
    Dim oRet As CiManage5xProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function
Private Property Get IDMS_Custom1() As String
    If IDMS_CustomFields.Count > 0 Then
        IDMS_Custom1 = GetProfileInfo(IDMS_CustomFields.Item(1).Value)
    End If
End Property
Private Property Get IDMS_Custom2() As String
    If IDMS_CustomFields.Count > 1 Then
        IDMS_Custom2 = GetProfileInfo(IDMS_CustomFields.Item(2).Value)
    End If
End Property
Private Property Get IDMS_Custom3() As String
    If IDMS_CustomFields.Count > 2 Then
        IDMS_Custom3 = GetProfileInfo(IDMS_CustomFields.Item(3).Value)
    End If
End Property
Private Function IDMS_CustomFields() As CCustomFields
    
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function

Private Sub IDMS_FileSave()
End Sub
Private Sub IDMS_FileSaveAs()
End Sub
Private Sub IDMS_DocClose()
End Sub
Private Sub IDMS_FileClose()
End Sub

'***Methods***
Public Function GetProfileInfo(vItem As Variant) As String
    Dim o As Object
    Dim oVar As Word.Variable
    Dim xVar As String
    
    On Error Resume Next
    Select Case g_iTargetApp '*c
        Case mpApps.mpApp_Excel '*c
            xVar = GetExcelObject.ActiveWorkbook.CustomDocumentProperties("DMS_Temp").Value '*c
        Case mpApps.mpApp_PPT
            xVar = GetPPTObject.ActivePresentation.CustomDocumentProperties("DMS_Temp").Value
        Case Else '*c
            xVar = Word.ActiveDocument.Variables("DMS_Temp").Value
    End Select '*c
    On Error GoTo ProcError
    
    If xVar <> Empty Then
'       we've written profile info to a doc var in the document-
'       use that, as the most current profile info is not yet
'       available through iManage
        GetProfileInfo = GetVarAttributeByID(vItem)
    Else
'       no var is present - use profile info from iManage
        On Error Resume Next
        Set o = m_oExt.GetDocumentFromPath(GetTargetAppFileFullName())
        If Not o Is Nothing Then
            GetProfileInfo = o.GetAttributeByID(vItem)
        End If
    End If
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
        "mpDMS.CiManage5xProfile.GetProfileInfo", _
        Err.Description
End Function

Private Function GetVarAttributeByID(vItem As Variant) As String
'returns the value of the specified data
'from the active document's doc var
    Dim xVar As String
    Dim iPos As Integer
    Dim iStart As Integer
    Dim iEnd As Integer
    
    On Error Resume Next
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            xVar = GetExcelObject.ActiveWorkbook.CustomDocumentProperties("DMS_Temp").Value
        Case mpApps.mpApp_PPT
            xVar = GetPPTObject.ActivePresentation.CustomDocumentProperties("DMS_Temp").Value
        Case Else
            xVar = Word.ActiveDocument.Variables("DMS_TEMP")
    End Select
    On Error GoTo ProcError
    
    If xVar <> Empty Then
'       this data is available - test for field
        iPos = InStr(xVar, vItem & "~")
        
        If iPos > 0 Then
'           field exists - get start and end points of field data
            iStart = iPos + Len(vItem & "~")
            iEnd = InStr(iStart, xVar, "||")
            If iEnd > 0 Then
'               field delimiter exists
                GetVarAttributeByID = Mid(xVar, iStart, iEnd - iStart)
            Else
                GetVarAttributeByID = Mid(xVar, iStart)
            End If
        End If
    End If
        
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpDMS.CiManage5xProfile", Err.Description
End Function
Private Function xTranslateString(xVer As String)
End Function

Public Sub FileSaveAs()

    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            FileSaveAsExcel
        Case mpApps.mpApp_PPT
            FileSaveAsPPT
        Case mpApps.mpApp_Word
            FileSaveAsWord
    End Select
End Sub
Public Sub FileSave()
'saves active document - runs save as if necessary
    On Error GoTo ProcError
    
    If GetTargetAppFileCount() = 0 Then
        Exit Sub
    End If
    
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            FileSaveExcel
        Case mpApps.mpApp_PPT
            FileSavePPT
        Case mpApps.mpApp_Word
            FileSaveWord
    End Select
    Exit Sub
ProcError:
    If Err.Number > 0 Then
        g_oError.Raise Err.Number, "mpDMS.cImanage5xProfile.FileSave"
    End If
    Exit Sub
End Sub
Private Sub FileSaveAsWord()
    Dim lRet As Long
    
    If IDMS_IsOn Then
        Exit Sub
    End If
    
    On Error GoTo FileSaveAs_Error
    
    If Documents.Count = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
    lRet = Dialogs(wdDialogFileSaveAs).Show
    If lRet Then
       m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile", Err.Description
End Sub

Private Sub FileSaveWord()
    Dim o As IDMS
    Dim iFormat As Integer
    Dim xFormat As String
    Dim xMsg As String
    Dim iRet As Integer
    Dim xCurFile As String
    Dim xTempFile As String
    Dim conv As FileConverter
    
    If IDMS_IsOn Then
        Exit Sub
    End If
    
    On Error GoTo FileSave_Error
    
    With ActiveDocument
        On Error Resume Next
        
        '9.7.1 #4233 - avoid error 5155 for read-only docs
        If .ReadOnly Then
            FileSaveAs
            Exit Sub
        End If
        
        iFormat = .SaveFormat
        
        'Handle native Word formats
        If iFormat = 0 Or iFormat = 1 Or (iFormat >= 12 And iFormat <= 16) Then
            .Save
        Else
'---        non word format, prompt as in Native Word
            If Not .Saved Then

'---            since open/save converters are shuffled together
'               in collection we need to look only at save converters
'---            to display format name correctly in dbox
                Select Case iFormat
                    Case wdFormatText
                        xFormat = "Text Only"
                    Case wdFormatTextLineBreaks
                        xFormat = "Text Only with Line Breaks"
                    Case wdFormatRTF
                        xFormat = "Rich Text Format"
                    Case wdFormatUnicodeText
                        xFormat = "Unicode Text"
                    Case wdFormatDOSText
                        xFormat = "MS-DOS Text"
                    Case wdFormatDOSTextLineBreaks
                        xFormat = "MS-DOS Text with Line Breaks"
                    Case Else
                        For Each conv In FileConverters
                            If conv.CanSave Then
                                If conv.SaveFormat = iFormat Then
                                    xFormat = conv.FormatName
                                End If
                            End If
                        Next conv
                End Select
                If xFormat = Empty Then _
                    xFormat = "unknown"
                    
                If iFormat > 1 And (iFormat < 12 Or iFormat > 16) Then
                    xMsg = "This document may contain formatting which " & _
                           "will be lost upon conversion to " & xFormat & _
                           " format.  " & _
                           "Click No to save the document in Word format " & _
                           "and preserve the current formatting." & _
                           String(2, vbLf) & "Continue with save in " & _
                           xFormat & " format?"
                    
                    iRet = MsgBox(xMsg, vbQuestion + vbYesNoCancel, App.Title)
                    Select Case iRet
                        Case vbYes
'                           save in non word format
                            .Save
                            
                        Case vbNo
    '                       convert to Word Format when saving
                            If Val(Word.Application.Version) > 11 Then
                                xTempFile = "c:\tempsave.docx"
                            Else
                                xTempFile = "c:\tempsave.doc"
                            End If
                            
                            xCurFile = ActiveDocument.FullName

'---                        save to temp file in Word format
                            ActiveDocument.SaveAs FileName:=xTempFile, _
                                                  FileFormat:=0
                    
'---                        save back to original filename
                            ActiveDocument.SaveAs FileName:=xCurFile, _
                                                  FileFormat:=0
                                
                            On Error Resume Next
                            Kill xTempFile
                            
                        Case Else
                            Exit Sub
                    End Select
                Else
                    .Save
                End If
            End If
        End If
         
        If .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
FileSave_Error:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile", Err.Description
End Sub
Private Sub FileSaveAsExcel()
    Dim lRet As Long
    
    If IDMS_IsOn Then
        Exit Sub
    End If
    
    On Error GoTo FileSaveAs_Error
    
    If GetExcelObject.Workbooks.Count = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
    ' Display Excel Save As dialog
    lRet = GetExcelObject.Application.Dialogs(xlDialogSaveAs).Show
    If lRet Then
       m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile", Err.Description
End Sub
Private Sub FileSaveExcel()
    Dim o As IDMS
    Dim iFormat As Integer
    Dim xFormat As String
    Dim xMsg As String
    Dim iRet As Integer
    Dim xCurFile As String
    Dim xTempFile As String
    Dim conv As FileConverter
    
    If IDMS_IsOn Then
        Exit Sub
    End If
    
    On Error GoTo FileSave_Error
    
    With ActiveWorkbook
        On Error Resume Next
        
        If .Path <> Empty Then
            ' Workbook has been saved previously
            .Save
        Else
            ' New workbook, so display Save As dialog
            GetExcelObject.Application.Dialogs(xlDialogSaveAs).Show
        End If
         
        If .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
FileSave_Error:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile", Err.Description
End Sub
Private Sub FileSaveAsPPT()
    Dim lRet As Long
    Dim dlg As Object
    
    On Error GoTo FileSaveAs_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
    
'    Set dlg = GetPPTObject.FileDialog(msoFileDialogSaveAs)
    Set dlg = GetPPTObject.FileDialog(2)
    lRet = dlg.Show
    If lRet Then
       m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    g_oError.Raise Err.Number, "mpDMS.WindowsProfile", Err.Description
End Sub
Private Sub FileSavePPT()
    Dim o As IDMS
    Dim iFormat As Integer
    Dim xFormat As String
    Dim xMsg As String
    Dim iRet As Integer
    Dim xCurFile As String
    Dim xTempFile As String
    Dim conv As FileConverter
    Dim dlg As Object
    
    If IDMS_IsOn Then
        Exit Sub
    End If
    
    On Error GoTo FileSave_Error
    
    With ActivePresentation
        On Error Resume Next
        
        If .Path <> Empty Then
            ' Workbook has been saved previously
            .Save
        Else
'            Set dlg = GetPPTObject.FileDialog(msoFileDialogSaveAs)
            Set dlg = GetPPTObject.FileDialog(2)
            lRet = dlg.Show
        End If
         
        If .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
FileSave_Error:
    Err.Raise Err.Number, "mpDMS.cIManage5xProfile", Err.Description
End Sub

Public Sub FileClose()
End Sub

Public Sub FileOpen()
    m_oEvent.RaiseFileOpenedEvent
End Sub

Public Sub DocClose()
'   close active window if doc has
'   more than one window, else close
'   file - (this mimics Native DOC CLOSE)

End Sub

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AuthorID(RHS As Variant)
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
End Property

Private Property Get IProfileCreator_ClientID() As Variant
End Property

Private Function IProfileCreator_CreateProfile() As Variant
End Function

Private Function IProfileCreator_CustomProperties() As Collection
End Function

Private Property Get IProfileCreator_DocName() As String
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
End Property

Private Property Get IProfileCreator_Library() As Variant
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
End Property

Private Property Get IProfileCreator_MatterID() As Variant
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
End Property

Private Property Get IProfileCreator_TypistID() As Variant
End Property

Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
End Property

Private Property Get IProfileCreator_DepartmentID() As String
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
End Property

Private Property Get IProfileCreator_SecurityCode() As String
End Property



