VERSION 5.00
Begin VB.Form frmNetDocsSaveAs 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Save Options"
   ClientHeight    =   2370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5550
   Icon            =   "frmNetDocsSaveAs.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   5550
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.OptionButton optSaveChanges 
      Caption         =   "&Replace existing document"
      Height          =   330
      Left            =   585
      TabIndex        =   5
      Top             =   1275
      Width           =   3870
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2775
      TabIndex        =   3
      Top             =   1830
      Width           =   1095
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   375
      Left            =   1560
      TabIndex        =   2
      Top             =   1830
      Width           =   1095
   End
   Begin VB.OptionButton optSaveAs 
      Caption         =   "Save this document as a &new document"
      Height          =   330
      Left            =   585
      TabIndex        =   1
      Top             =   945
      Width           =   3870
   End
   Begin VB.OptionButton optNewVersion 
      Caption         =   "Save this document as a new &version."
      Height          =   360
      Left            =   585
      TabIndex        =   0
      Top             =   600
      Value           =   -1  'True
      Width           =   5010
   End
   Begin VB.Label lblHelp 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   210
      TabIndex        =   6
      ToolTipText     =   "Versioning is not enabled for this document because it is not saved to a net documents folder that supports versioning."
      Top             =   630
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.Label lblMsg 
      Caption         =   "How do you want to save this document?"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   645
      TabIndex        =   4
      Top             =   240
      Width           =   3585
   End
End
Attribute VB_Name = "frmNetDocsSaveAs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCanceled As Boolean

Public Property Get Canceled() As Boolean
    Canceled = m_bCanceled
End Property

Private Sub btnCancel_Click()
    m_bCanceled = True
    Me.Hide
End Sub

Private Sub btnOK_Click()
    m_bCanceled = False
    Me.Hide
End Sub

Private Sub Form_Load()
    Dim xFile As String
    Dim oND As NEWEBCLLib.IndActiveDoc
    Dim bVerAllowed As Boolean
    Dim xVerAttr As String
    Dim bFiledInND As Boolean
    Dim oXL As Excel.Application
    On Error GoTo ProcError
    m_bCanceled = True
    
    'enable/disable new version option as appropriate
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            Set oXL = GetObject(, "Excel.Application")
            xFile = oXL.ActiveWorkbook.FullName
        Case mpApps.mpApp_PPT
            Dim oPPT As PowerPoint.Application
            Set oPPT = GetObject(, "PowerPoint.Application")
            xFile = oPPT.ActivePresentation.FullName
        Case mpApps.mpApp_Word
            xFile = Word.ActiveDocument.FullName
    End Select
    
    Set oND = CreateObject("ND.ActiveDoc")
    
    '2/22/09: Added support for importing local document as new version of profiled doc
    'The VERALLOWED attribute will be Y if the document is filed in a NetDocuments
    'cabinet that allows versioning, N if the document is filed in a NetDocuments
    'cabinet that does not allow versioning, or empty if the document is not
    'filed in NetDocuments.
    If Not oND Is Nothing Then
        On Error Resume Next
        xVerAttr = UCase$(oND.DocAttribute(xFile, "VERALLOWED"))
        On Error GoTo ProcError
        bVerAllowed = (xVerAttr = "Y")
        bFiledInND = CBool(Len(xVerAttr) > 0)
    End If
    If bFiledInND Then
        Me.optNewVersion.Caption = "Save this document as a new &version"
    Else
        Me.optNewVersion.Caption = "Save this document as a new &version of an existing document"
        bVerAllowed = True
    End If
    If Not bVerAllowed Then
        Me.optNewVersion.Enabled = False
        Me.lblHelp.Visible = True
        Me.optSaveAs.Value = True
    Else
        'Default to Save as New Version for Profiled docs, otherwise new Document
        Me.optSaveAs.Value = Not bFiledInND
        Me.optNewVersion.Value = bFiledInND
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lblHelp_Click()
    MsgBox "Versioning is not enabled for this document because it is not " & _
        "saved to a net documents folder that supports versioning.", vbInformation
End Sub
