VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEventGenerator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Event FileSaved()
Public Event ProfileChanged()
Public Event FileOpened()

Friend Sub RaiseFileOpenedEvent()
    RaiseEvent FileOpened
End Sub
Friend Sub RaiseFileSavedEvent()
    RaiseEvent FileSaved
End Sub
Friend Sub RaiseProfileChangedEvent()
    RaiseEvent ProfileChanged
End Sub


