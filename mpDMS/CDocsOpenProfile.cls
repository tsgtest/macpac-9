VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocsOpenProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Attribute VB_Ext_KEY = "Member0" ,"clsDMSDocsOpen"
'**********************************************************
'   CDocsOpenProfile Class
'   created 12/13/99 by Jeffrey Sweetland

'   Contains properties and methods to
'   access DocsOpen Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Option Explicit

Const mpDocProfile_DOCNUMBER As String = "DOCNUMBER"
Const mpDocProfile_DOCNAME As String = "DOCNAME"
Const mpDocProfile_LIBRARY As String = "LIBRARYNAME"
Const mpDocProfile_AUTHORUSERID As String = "AUTHOR.USER_ID"
Const mpDocProfile_AUTHORFULLNAME As String = "AUTHOR.FULL_NAME"
Const mpDocProfile_TYPISTUSERID As String = "TYPIST.USER_ID"
Const mpDocProfile_TYPISTFULLNAME As String = "TYPIST.FULL_NAME"
Const mpDocProfile_DOCTYPEID As String = "DOCUMENTTYPE.TYPE_ID"
Const mpDocProfile_DOCTYPEDESC As String = "DOCUMENTTYPE.DESCRIPTION"
Const mpDocProfile_CREATIONDATE As String = "CREATION_DATE"
Const mpDocProfile_LASTEDITDATE As String = "LAST_EDIT_DATE"
Const mpDocProfile_ABSTRACT As String = "ABSTRACT"
Const mpDocProfile_MATTERID As String = "MATTER.MATTER_ID"
Const mpDocProfile_MATTERNAME As String = "MATTER.MATTER_NAME"
Const mpDocProfile_CLIENTID As String = "MATTER.CLIENT_ID.CLIENT_ID"
Const mpDocProfile_CLIENTNAME As String = "MATTER.CLIENT_ID.CLIENT_NAME"

Private Declare Function DOCSGetCurrentLibrary Lib "DOCSAP32.DLL" _
                    (ByVal xlibName As String) As Integer

Private Declare Function DOCSGetProfileInfo Lib "DOCSAP32.DLL" _
                    (ByVal openFileID As String, _
                    ByVal Item As String, _
                    ByVal itemBuffer As String, _
                    ByVal bufferLen As Integer) As Integer
    
Private WithEvents m_oEvent As CEventGenerator
Attribute m_oEvent.VB_VarHelpID = -1

Implements IDMS
Implements IProfileCreator

Private m_oCustFields As mpDMS.CCustomFields
Const xIniSection = "DocsOpen"
'***Class events***
Private Sub Class_Initialize()
    On Error Resume Next
    Set m_oEvent = New CEventGenerator
End Sub
Private Sub Class_Terminate()
    Set m_oEvent = Nothing
End Sub
'***Properties***
Private Property Get IDMS_AuthorID() As String
    IDMS_AuthorID = GetProfileInfo(mpDocProfile_AUTHORUSERID)
End Property
Private Property Get IDMS_ClientID() As String
    IDMS_ClientID = GetProfileInfo(mpDocProfile_CLIENTID)
End Property
Private Property Get IDMS_CreationDate() As String
    IDMS_CreationDate = GetProfileInfo(mpDocProfile_CREATIONDATE)
End Property
Private Property Get IDMS_Custom1() As String
    If IDMS_CustomFields.Count > 0 Then
        IDMS_Custom1 = GetProfileInfo(IDMS_CustomFields.Item(1).Value)
    End If
End Property
Private Property Get IDMS_Custom2() As String
    If IDMS_CustomFields.Count > 1 Then
        IDMS_Custom2 = GetProfileInfo(IDMS_CustomFields.Item(2).Value)
    End If
End Property
Private Property Get IDMS_Custom3() As String
    If IDMS_CustomFields.Count > 2 Then
        IDMS_Custom3 = GetProfileInfo(IDMS_CustomFields.Item(3).Value)
    End If
End Property
Private Function IDMS_CustomFields() As CCustomFields
    
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function
Private Property Get IDMS_DocName() As String
    IDMS_DocName = GetProfileInfo(mpDocProfile_DOCNAME)
End Property
Private Property Get IDMS_DocNumber() As String
    IDMS_DocNumber = GetProfileInfo(mpDocProfile_DOCNUMBER)
End Property
Private Property Get IDMS_DocTypeID() As String
    IDMS_DocTypeID = GetProfileInfo(mpDocProfile_DOCTYPEID)
End Property
Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property
Private Property Get IDMS_Library() As String
    IDMS_Library = GetProfileInfo(mpDocProfile_LIBRARY)
End Property
Private Function IDMS_MainInterface() As Object
    Dim oRet As CDocsOpenProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function
Private Property Get IDMS_MatterID() As String
    IDMS_MatterID = GetProfileInfo(mpDocProfile_MATTERID)
End Property
Private Property Get IDMS_RevisionDate() As String
    IDMS_RevisionDate = GetProfileInfo(mpDocProfile_LASTEDITDATE)
End Property
Private Property Get IDMS_TypistID() As String
    IDMS_TypistID = GetProfileInfo(mpDocProfile_TYPISTUSERID)
End Property
Private Property Get IDMS_AuthorName() As String
    IDMS_AuthorName = GetProfileInfo(mpDocProfile_AUTHORFULLNAME)
End Property
Private Property Get IDMS_TypistName() As String
    IDMS_TypistName = GetProfileInfo(mpDocProfile_TYPISTFULLNAME)
End Property
Private Property Get IDMS_ClientName() As String
    IDMS_ClientName = GetProfileInfo(mpDocProfile_CLIENTNAME)
End Property
Private Property Get IDMS_MatterName() As String
    IDMS_MatterName = GetProfileInfo(mpDocProfile_MATTERNAME)
End Property
Private Property Get IDMS_DocTypeDescription() As String
    IDMS_DocTypeDescription = GetProfileInfo(mpDocProfile_DOCTYPEDESC)
End Property
Private Property Get IDMS_Abstract() As String
    IDMS_Abstract = GetProfileInfo(mpDocProfile_ABSTRACT)
End Property
Private Property Get IDMS_GroupName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CDocsOpenProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Path() As String
    If Word.ActiveDocument Is Nothing Then Exit Property
    IDMS_Path = Word.WordBasic.FileNameInfo$(Word.WordBasic.FileName$(), 5)
End Property
Private Property Get IDMS_FileName() As String
    If Word.ActiveDocument Is Nothing Then Exit Property
    IDMS_FileName = Word.WordBasic.FileNameInfo$(Word.WordBasic.FileName$(), 3)
End Property

Private Property Get IDMS_Version() As String
    Dim iDot As Integer
    Dim xVersion As String
    Dim I As Integer
    
    If Word.ActiveDocument Is Nothing Then Exit Property
    
    xVersion = Word.ActiveDocument.FullName
    If InStr(xVersion, "ODMA") Then
        Do
            I = I + 1
        Loop Until Left(Right(xVersion, I), 1) = "\"
        
        IDMS_Version = Right(Right(xVersion, I), I - 1)
        Exit Property
    End If
    
    If InStr(xVersion, "!") Then '---docs configured w/ app extensions
        iDot = InStr(xVersion, ".")

        If iDot Then
            xVersion = Left(xVersion, iDot - 1)
            xVersion = Right(xVersion, 3)
        End If

        If Right(xVersion, 1) = "!" Then _
            xVersion = Left(xVersion, Len(xVersion) - 1)
            
        If Left(xVersion, 1) = "0" Then _
            xVersion = Right(xVersion, Len(xVersion) - 1)
    Else
        iDot = InStr(xVersion, ".")
        If iDot Then
            xVersion = Right(xVersion, Len(xVersion) - iDot + 1)
        Else
            xVersion = ""
        End If
    End If
    IDMS_Version = xVersion
    
End Property
'***Methods***
Public Function GetProfileInfo(vItem As Variant) As String
    
    Dim xItemBuffer As String
    Dim xOpenFileName As String
    Dim rc
    
    If Word.ActiveDocument Is Nothing Then Exit Function
    
    On Error Resume Next
    xOpenFileName = Word.ActiveDocument.FullName
    xItemBuffer = String(512, " ")
    rc = DOCSGetProfileInfo(xOpenFileName, vItem, xItemBuffer, 512)
    If rc = 0 Then
        GetProfileInfo = ""
    Else
        GetProfileInfo = Left(xItemBuffer, InStr(xItemBuffer, Chr(0)) - 1)
    End If
End Function
Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    IDMS_GetProfileInfo = GetProfileInfo(vItem)
End Function
Private Sub IDMS_FileSave()
    FileSave
End Sub
Private Sub IDMS_FileSaveAs()
    FileSaveAs
End Sub
Private Sub IDMS_DocClose()
    DocClose
End Sub
Private Sub IDMS_FileClose()
    FileClose
End Sub
Public Sub FileSaveAs()
    
    Dim xCurFile As String
    Dim lRet As Long
    Dim lErr As Long
    
    On Error GoTo FileSaveAs_Error
    
    If Documents.Count = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
    Dim xOrig As String
    xOrig = Word.ActiveDocument.FullName
    On Error Resume Next
    lRet = Word.Dialogs(wdDialogFileSaveAs).Show
    '  Ignore error Word generates when attempting to save over read-only file
    lErr = Err.Number
    On Error GoTo FileSaveAs_Error
    If lErr = 5155 Then
        ' This error will be generated even if filename is corrected and document saved
        If Word.ActiveDocument.Saved And (xOrig <> Word.ActiveDocument.FullName) Then
            lRet = -1
        End If
    ElseIf lErr <> 0 Then
        Err.Raise lErr
    End If
    If lRet Then
        m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    Err.Raise Err.Number, "mpDMS.CDocsOpenProfile.FileSaveAs", Err.Description
End Sub
Public Sub FileSave()
    
    Dim iFormat As Integer
    Dim xFormat As String
    Dim xMsg As String
    Dim iRet As Integer
    Dim xCurFile As String
    Dim xTempFile As String
    Dim conv As FileConverter
    Dim xDocPath As String
    
    On Error GoTo FileSave_Error
    
    With Word.ActiveDocument
        '9.7.1 #4233 - avoid error 5155 for read-only docs
        If .ReadOnly Then
            FileSaveAs
            Exit Sub
        End If
        '*c - MoFo fix
'       circumvents Docs issue with saving copies of documents
'       already open by other users.  Quick fix until Docs fix:
'       if document resides in temp folder below and
'       word XP is running then call SaveAs
        If Val(Word.Application.Version) = "10" Then
            xDocPath = "c:\Documents and Settings\<UserName>\Local Settings\Temp\"
            xDocPath = GetUserVarPath(xDocPath)
    
            If Left(WordBasic.FileName$(), 2) <> "\\" Then
                If InStr(UCase(GetLongFilename(WordBasic.FileName$())), UCase(xDocPath)) > 0 Then
                    FileSaveAs
                    Exit Sub
                End If
            End If
        End If
        On Error Resume Next
        
        iFormat = .SaveFormat
        
        'Handle native Word formats
        If iFormat = 0 Or iFormat = 1 Or (iFormat >= 12 And iFormat <= 16) Then
            .Save
        Else
'---        non word format, prompt as in Native Word
            If Not .Saved Then

'---            since open/save converters are shuffled together
'               in collection we need to look only at save converters
'---            to display format name correctly in dbox
                Select Case iFormat
                    Case wdFormatText
                        xFormat = "Text Only"
                    Case wdFormatTextLineBreaks
                        xFormat = "Text Only with Line Breaks"
                    Case wdFormatRTF
                        xFormat = "Rich Text Format"
                    Case wdFormatUnicodeText
                        xFormat = "Unicode Text"
                    Case wdFormatDOSText
                        xFormat = "MS-DOS Text"
                    Case wdFormatDOSTextLineBreaks
                        xFormat = "MS-DOS Text with Line Breaks"
                    Case Else
                        For Each conv In FileConverters
                            If conv.CanSave Then
                                If conv.SaveFormat = iFormat Then
                                    xFormat = conv.FormatName
                                End If
                            End If
                        Next conv
                End Select
                
                If xFormat = Empty Then _
                    xFormat = "unknown"
                    
                If iFormat > 1 And (iFormat < 12 Or iFormat > 16) Then
                    Dim xNewFormat As String
                    xNewFormat = GetWordVersionString()
                    xMsg = "This document may contain formatting which " & _
                           "will be lost upon conversion to " & xFormat & _
                           " format.  " & "Click No to save the document in " & _
                            xNewFormat & " format " & _
                           "and preserve the current formatting." & _
                           vbLf & vbLf & "Continue with save in " & _
                           xFormat & " format?"

                    iRet = MsgBox(xMsg, vbQuestion + vbYesNoCancel)
                    Select Case iRet
                        Case vbYes
'                           save in non word format
                            .Save
                            
                        Case vbNo
                            xCurFile = Word.ActiveDocument.FullName
                            
    '                       convert to Word Format when saving
                            If Val(Word.Application.Version) > 11 Then
                                xTempFile = "c:\tempsave.docx"
                            Else
                                xTempFile = "c:\tempsave.doc"
                            End If
                            
'---                        save to temp file in Word format
                            Word.ActiveDocument.SaveAs xTempFile, 0
                    
'---                        save back to original filename
                            Word.ActiveDocument.SaveAs xCurFile, 0
                                
                            On Error Resume Next
                            Kill xTempFile
                            
                        Case Else
                            Exit Sub
                    End Select
                Else
                    .Save
                End If
            End If
        End If
        
        If .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
    
FileSave_Error:
    Err.Raise Err.Number, "mpDMS.CDocsOpenProfile.FileSave", Err.Description
End Sub
Public Sub FileClose()
'close doc, run autotrailer-
'autotrailer runs trailer if
'filename has changed as a result
'of the close
    Dim iUserChoice As Integer
    
    On Error GoTo FileClose_Error
    
    If Documents.Count = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
'   the following is necessary so that
'   trailer will run when appropriate -
'   if we just run the native command,
'   we can't run trailer at all
    With Word.ActiveDocument
        If Not .Saved Then

'           prompt for doc save
'*********** Changed from .Name to .ActiveWindow.Caption to work around
'*********** difference in ODMA behavior under Word XP
            xMsg = "Do you want to save the changes you made to " & .ActiveWindow.Caption & "?"
            iUserChoice = MsgBox(xMsg, _
                                 vbExclamation + vbYesNoCancel, _
                                 "Microsoft Word")
            Select Case iUserChoice
                Case vbYes
                    FileSave
                    If .Saved Then
                        .Close
                    End If
                Case vbNo
                    .Saved = True
                    .Close
                Case Else
            End Select
        Else
            .Close
        End If
    End With
    
'   code below resets pointer,
'   which seems to remain as an hourglass
'   if the user cancels out of save
    Application.ScreenRefresh
    Exit Sub
    
FileClose_Error:
    Err.Raise Err.Number, "mpDMS.CDocsOpenProfile.FileClose", Err.Description
End Sub

Public Sub FileOpen()
    m_oEvent.RaiseFileOpenedEvent
End Sub

Public Sub DocClose()
'   close active window if doc has
'   more than one window, else close
'   file - (this mimics Native DOC CLOSE)

    On Error Resume Next
    If Application.Windows.Count = 0 Then _
        Exit Sub
    
    If Word.ActiveDocument.Windows.Count > 1 Then
        If Word.ActiveDocument.ActiveWindow.View = wdPrintPreview Then
            Word.ActiveDocument.ClosePrintPreview
        Else
            Word.ActiveDocument.ActiveWindow.Close
        End If
    Else
        FileClose
    End If
End Sub

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AuthorID(RHS As Variant)
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
End Property

Private Property Get IProfileCreator_ClientID() As Variant
End Property

Private Function IProfileCreator_CreateProfile() As Variant
End Function

Private Function IProfileCreator_CustomProperties() As Collection
End Function

Private Property Get IProfileCreator_DocName() As String
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
End Property

Private Property Get IProfileCreator_Library() As Variant
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
End Property

Private Property Get IProfileCreator_MatterID() As Variant
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
End Property

Private Property Get IProfileCreator_TypistID() As Variant
End Property

Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
End Property

Private Property Get IProfileCreator_DepartmentID() As String
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
End Property

Private Property Get IProfileCreator_SecurityCode() As String
End Property

