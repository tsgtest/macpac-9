VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IProfileCreator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   IProfileCreator Interface
'   created 2/27/01 by Daniel Fisherman

'   Interface used by document management classes
'   to create profiles

'   Member of
'   Container for
'**********************************************************

'***************************************************
'properties
'***************************************************\
Public Property Let AddSecurity(bNew As Boolean)

End Property
Public Property Get AddSecurity() As Boolean

End Property
Public Property Let DocName(xNew As String)

End Property

Public Property Get DocName() As String

End Property

Public Property Let DocTypeID(vNew As Variant)

End Property

Public Property Get DocTypeID() As Variant

End Property

Public Property Let Library(vNew As Variant)

End Property

Public Property Get Library() As Variant

End Property

Public Property Let AuthorID(vNew As Variant)

End Property

Public Property Get AuthorID() As Variant

End Property

Public Property Let TypistID(vNew As Variant)

End Property

Public Property Get TypistID() As Variant

End Property

Public Property Let ClientID(vNew As Variant)

End Property

Public Property Get ClientID() As Variant

End Property

Public Property Let MatterID(vNew As Variant)

End Property

Public Property Get MatterID() As Variant

End Property

Public Property Get SecurityCode() As String

End Property

Public Property Let SecurityCode(xNew As String)

End Property
'*********************
'dept id:
Public Property Get DepartmentID() As String

End Property

Public Property Let DepartmentID(xNew As String)

End Property
'*********************
'***************************************************
'methods
'***************************************************
Public Function CreateProfile() As Variant

End Function

Public Function CustomProperties() As Collection

End Function

