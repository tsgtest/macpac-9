VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cProlawProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************************
'   CProlawProfile Class
'   created 7/18/2008 by Jeffrey Sweetland

'   Contains properties and methods to
'   access Prolaw Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Option Explicit

Const mpDocProfile_DOCNUMBER As String = "1"
Const mpDocProfile_VERSION As String = "2"
Const mpDocProfile_AUTHORID As String = "3"
Const mpDocProfile_AUTHORFULLNAME As String = "4"
Const mpDocProfile_DOCTYPEDESC As String = "5"
Const mpDocProfile_CREATIONDATE As String = "6"
Const mpDocProfile_ABSTRACT As String = "7"
Const mpDocProfile_MATTERID As String = "8"
Const mpDocProfile_MATTERNAME As String = "9"
Const mpDocProfile_CLIENTNAME As String = "10"

Private WithEvents m_oEvent As CEventGenerator
Attribute m_oEvent.VB_VarHelpID = -1

Implements IDMS
Implements IProfileCreator

Private m_oCustFields As mpDMS.CCustomFields
Private m_xActiveDocument As String
Private m_oCol As Collection
Const m_xIniSection = "ProLaw"
'***Class events***
Private Sub Class_Initialize()
    On Error GoTo ProcError
    If Not Tasks.Exists("ProLaw") Then
        Err.Raise mpErrors.mpError_InvalidMember, , _
        "Prolaw is not running."
    End If
    Set m_oEvent = New CEventGenerator
    Exit Sub
    
ProcError:
    Err.Raise Err.Number, "mpDMS.CDMSiManage_COMProfile.Class_Initialize", Err.Description
End Sub
Private Sub Class_Terminate()
    Set m_oEvent = Nothing
End Sub
'***Properties***
Private Property Get IDMS_AuthorID() As String
    IDMS_AuthorID = GetProfileInfo(mpDocProfile_AUTHORID)
End Property
Private Property Get IDMS_ClientID() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_CreationDate() As String
    IDMS_CreationDate = GetProfileInfo(mpDocProfile_CREATIONDATE)
End Property
Private Property Get IDMS_Custom1() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Custom2() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Custom3() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Function IDMS_CustomFields() As CCustomFields
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, m_xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function
Private Property Get IDMS_DocName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_DocNumber() As String
    IDMS_DocNumber = GetProfileInfo(mpDocProfile_DOCNUMBER)
End Property
Private Property Get IDMS_DocTypeID() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property
Private Property Get IDMS_Library() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Function IDMS_MainInterface() As Object
    Dim oRet As cProlawProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function
Private Property Get IDMS_MatterID() As String
    IDMS_MatterID = GetProfileInfo(mpDocProfile_MATTERID)
End Property
Private Property Get IDMS_RevisionDate() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_TypistID() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_AuthorName() As String
    IDMS_AuthorName = GetProfileInfo(mpDocProfile_AUTHORFULLNAME)
End Property
Private Property Get IDMS_TypistName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_ClientName() As String
    IDMS_ClientName = GetProfileInfo(mpDocProfile_CLIENTNAME)
End Property
Private Property Get IDMS_MatterName() As String
    IDMS_MatterName = GetProfileInfo(mpDocProfile_MATTERNAME)
End Property
Private Property Get IDMS_DocTypeDescription() As String
    IDMS_DocTypeDescription = GetProfileInfo(mpDocProfile_DOCTYPEDESC)
End Property
Private Property Get IDMS_Abstract() As String
    IDMS_Abstract = GetProfileInfo(mpDocProfile_ABSTRACT)
End Property
Private Property Get IDMS_GroupName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CProLawProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Path() As String
    If Word.ActiveDocument Is Nothing Then Exit Property
    IDMS_Path = Word.ActiveDocument.Path
End Property
Private Property Get IDMS_FileName() As String
    If Word.ActiveDocument Is Nothing Then Exit Property
    IDMS_FileName = Word.ActiveDocument.Name
End Property

Private Property Get IDMS_Version() As String
    IDMS_Version = GetProfileInfo(mpDocProfile_VERSION)
End Property
'***Methods***
Public Function GetProfileInfo(vItem As Variant) As String
    Dim oCnn As ADODB.Connection
    Dim oRS As ADODB.Recordset
    Dim xSQL As String
    Dim xNumVer As String
    Dim xNum As String
    Dim xVer As String
    Dim xFile As String
    
    If vItem = vbEmpty Then
        Exit Function
    End If
    On Error GoTo ExitProc
    If UCase(m_xActiveDocument) <> UCase(Word.ActiveDocument.FullName) Or m_oCol Is Nothing Then
        'Populate collection with profile information
        xFile = ActiveDocument.Name
        If InStr(xFile, ".") Then
            xFile = Left(xFile, InStr(xFile, ".") - 1)
        End If
        Set oCnn = GetConnection
        xSQL = "SELECT Events.RTF AS NumVer, Events.DocDir AS Path, Professionals.Initials AS AuthorID, " & _
                "Professionals.ProfName AS AuthorName, Matters.MatterID, Matters.ShortDesc AS MatterName, " & _
                "COMPANY.COMPNAME AS ClientName, EventTypes.EventDesc AS DocTypeName, Events.ShortNote AS Abstract, " & _
                "Events.AddingDateTime AS CreationDate FROM Matters INNER JOIN EventTypes INNER JOIN Events ON " & _
                "EventTypes.EventTypes = Events.EventTypes INNER JOIN Professionals ON " & _
                "Events.AddingProfessionals = Professionals.Professionals INNER JOIN EventMatters ON " & _
                "Events.Events = EventMatters.Events ON Matters.Matters = EventMatters.Matters LEFT OUTER JOIN " & _
                "COMPANY ON Matters.ClientSort = COMPANY.CompSort WHERE (Events.EventKind = 'O') AND " & _
                "(Events.RTF = '" & xFile & "')"
        Set oRS = oCnn.Execute(xSQL)
        If oRS.RecordCount = 0 Then
            Set m_oCol = Nothing
            GetProfileInfo = ""
            GoTo ExitProc
        End If
        Set m_oCol = New Collection
        With m_oCol
            xNumVer = oRS!NumVer
            If InStr(xNumVer, "_") Then
                xNum = Left(xNumVer, InStr(xNumVer, "_") - 1)
                xVer = Mid(xNumVer, InStr(xNumVer, "_") + 1)
            Else
                xNum = xNumVer
                xVer = 1
            End If
            .Add xNum, mpDocProfile_DOCNUMBER
            .Add xVer, mpDocProfile_VERSION
            On Error Resume Next
            .Add oRS!AuthorID & "", mpDocProfile_AUTHORID
            .Add oRS!AuthorName & "", mpDocProfile_AUTHORFULLNAME
            .Add oRS!MatterID & "", mpDocProfile_MATTERID
            .Add oRS!MatterName & "", mpDocProfile_MATTERNAME
            .Add oRS!ClientName & "", mpDocProfile_CLIENTNAME
            .Add oRS!DocTypeName & "", mpDocProfile_DOCTYPEDESC
            .Add oRS!Abstract & "", mpDocProfile_ABSTRACT
            .Add oRS!CreationDate & "", mpDocProfile_CREATIONDATE
            On Error GoTo ExitProc
            oRS.Close
            Set oRS = Nothing
        End With
        m_xActiveDocument = Word.ActiveDocument.FullName
    End If
    GetProfileInfo = m_oCol(vItem)
ExitProc:
    If Not oCnn Is Nothing Then
        oCnn.Close
        Set oCnn = Nothing
    End If
End Function
Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    IDMS_GetProfileInfo = GetProfileInfo(vItem)
End Function
Private Sub IDMS_FileSave()
    FileSave
End Sub
Private Sub IDMS_FileSaveAs()
    FileSaveAs
End Sub
Private Sub IDMS_DocClose()
    DocClose
End Sub
Private Sub IDMS_FileClose()
    FileClose
End Sub
Public Sub FileSaveAs()
    Dim lRet As Long
    Dim lErr As Long
    On Error GoTo FileSaveAs_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    Dim xOrig As String
    xOrig = Word.ActiveDocument.FullName
    On Error Resume Next
    lRet = Word.Dialogs(wdDialogFileSaveAs).Show
    '  Ignore error Word generates when attempting to save over read-only file
    lErr = Err.Number
    On Error GoTo FileSaveAs_Error
    If lErr = 5155 Then
        ' This error will be generated even if filename is corrected and document saved
        If Word.ActiveDocument.Saved And (xOrig <> Word.ActiveDocument.FullName) Then
            lRet = -1
        End If
    ElseIf lErr <> 0 Then
        Err.Raise lErr
    End If
    If lRet Then
       m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    g_oError.Raise Err.Number, "mpDMS.CProlawProfile", Err.Description
End Sub
Public Sub FileSave()
    
    Dim iFormat As Integer
    Dim xFormat As String
    Dim xMsg As String
    Dim iRet As Integer
    Dim xCurFile As String
    Dim xTempFile As String
    Dim conv As FileConverter
    
    On Error GoTo FileSave_Error
    
    With ActiveDocument
        
        '9.7.1 #4233 - avoid error 5155 for read-only docs
        If .ReadOnly Then
            FileSaveAs
            Exit Sub
        End If
        
        iFormat = .SaveFormat
        
        'Handle native Word formats
        If iFormat = 0 Or iFormat = 1 Or (iFormat >= 12 And iFormat <= 15) Then
            .Save
        Else
'---        non word format, prompt as in Native Word
            If Not .Saved Then

'---            since open/save converters are shuffled together
'               in collection we need to look only at save converters
'---            to display format name correctly in dbox
                Select Case iFormat
                    Case wdFormatText
                        xFormat = "Text Only"
                    Case wdFormatTextLineBreaks
                        xFormat = "Text Only with Line Breaks"
                    Case wdFormatRTF
                        xFormat = "Rich Text Format"
                    Case wdFormatUnicodeText
                        xFormat = "Unicode Text"
                    Case wdFormatDOSText
                        xFormat = "MS-DOS Text"
                    Case wdFormatDOSTextLineBreaks
                        xFormat = "MS-DOS Text with Line Breaks"
                    Case Else
                        For Each conv In FileConverters
                            If conv.CanSave Then
                                If conv.SaveFormat = iFormat Then
                                    xFormat = conv.FormatName
                                End If
                            End If
                        Next conv
                End Select
                If xFormat = Empty Then _
                    xFormat = "unknown"
                    
                If iFormat > 1 And (iFormat < 12 Or iFormat > 15) Then
                    xMsg = "This document may contain formatting which " & _
                           "will be lost upon conversion to " & xFormat & _
                           " format.  " & _
                           "Click No to save the document in Word format " & _
                           "and preserve the current formatting." & _
                           String(2, vbLf) & "Continue with save in " & _
                           xFormat & " format?"
                    
                    iRet = MsgBox(xMsg, vbQuestion + vbYesNoCancel, App.Title)
                    Select Case iRet
                        Case vbYes
'                           save in non word format
                            .Save
                            
                        Case vbNo
'                           convert to Word Format when saving
                            If Val(Word.Application.Version) > 11 Then
                                xTempFile = "c:\tempsave.docx"
                            Else
                                xTempFile = "c:\tempsave.doc"
                            End If
                            
                            xCurFile = ActiveDocument.FullName

'---                        save to temp file in Word format
                            ActiveDocument.SaveAs FileName:=xTempFile, _
                                                  FileFormat:=0
                    
'---                        save back to original filename
                            ActiveDocument.SaveAs FileName:=xCurFile, _
                                                  FileFormat:=0
                                
                            On Error Resume Next
                            Kill xTempFile
                            
                        Case Else
                            Exit Sub
                    End Select
                Else
                    .Save
                End If
            End If
        End If
         
        If .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
    
FileSave_Error:
'raise all errors except cancel
    If Err.Number <> 4198 Then
        g_oError.Raise Err.Number, "mpDMS.CProlawProfile", Err.Description
    End If
End Sub
Public Sub FileClose()
'close doc, run autotrailer-
'autotrailer runs trailer if
'filename has changed as a result
'of the close
    Dim iUserChoice As Integer
    
    On Error GoTo FileClose_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
'   the following is necessary so that
'   trailer will run when appropriate -
'   if we just run the native command,
'   we can't run trailer at all
    With ActiveDocument
        If Not .Saved Then

'           prompt for doc save
            xMsg = "Do you want to save the changes you made to " & .Name & "?"
            iUserChoice = MsgBox(xMsg, _
                                 vbExclamation + vbYesNoCancel, _
                                 "Microsoft Word")
            Select Case iUserChoice
                Case vbYes
                    FileSave
                    If .Saved Then
                        .Close
                    End If
                Case vbNo
                    .Saved = True
                    .Close
                Case Else
            End Select
        Else
            .Close
        End If
    End With
    
'   code below resets pointer,
'   which seems to remain as an hourglass
'   if the user cancels out of save
    Application.ScreenRefresh
    Exit Sub
    
FileClose_Error:
    g_oError.Raise Err.Number, "mpDMS.CProlawProfile", Err.Description
End Sub

Public Sub FileOpen()
    m_oEvent.RaiseFileOpenedEvent
End Sub

Public Sub DocClose()
'   close active window if doc has
'   more than one window, else close
'   file - (this mimics Native DOC CLOSE)

    On Error Resume Next
    If Application.Windows.Count = 0 Then _
        Exit Sub

    If Word.ActiveDocument.Windows.Count > 1 Then
        If Word.ActiveDocument.ActiveWindow.View = wdPrintPreview Then
            Word.ActiveDocument.ClosePrintPreview
        Else
            Word.ActiveDocument.ActiveWindow.Close
        End If
    Else
        FileClose
    End If
End Sub

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AuthorID(RHS As Variant)
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
End Property

Private Property Get IProfileCreator_ClientID() As Variant
End Property

Private Function IProfileCreator_CreateProfile() As Variant
End Function

Private Function IProfileCreator_CustomProperties() As Collection
End Function

Private Property Get IProfileCreator_DocName() As String
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
End Property

Private Property Get IProfileCreator_Library() As Variant
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
End Property

Private Property Get IProfileCreator_MatterID() As Variant
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
End Property

Private Property Get IProfileCreator_TypistID() As Variant
End Property

Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
End Property

Private Property Get IProfileCreator_DepartmentID() As String
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
End Property

Private Property Get IProfileCreator_SecurityCode() As String
End Property

Private Function GetConnection() As ADODB.Connection
'connect to prolaw database
    Dim bRet As Boolean
    Dim hKey As Long
    Dim xSQL As String
    Dim xAdminDB As String
    Dim xMsg As String
    Dim lErr As Long
    Dim xDesc As String
    Dim xUserName As String
    Dim xCnn As String
    Dim xPath As String
    Dim xPrivateDB As String
    Dim xUserDir As String
    
    On Error Resume Next
    
    Dim oCnn As ADODB.Connection
    Set oCnn = New ADODB.Connection
    xCnn = mpBase2.GetIni(m_xIniSection, "Connection", mpBase2.ApplicationDirectory & "\mpDMS.ini")
    oCnn.Open xCnn
    lErr = Err.Number
    If lErr Then
        xDesc = Err.Description
        On Error GoTo ProcError
        Err.Raise lErr, , "Could not connect to the ProLaw SQL database.  " & xDesc
    End If
    
    On Error GoTo ProcError
    Set GetConnection = oCnn
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpDMS.cProlawProfile.GetConnection"
    Exit Function
End Function

