VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CndOfficeProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Attribute VB_Ext_KEY = "Member0" ,"clsDMSDocsOpen"
'**********************************************************
'   CndOfficeProfile Class
'   created 5/27/2014 by Jeffrey Sweetland 'GLOG 5399

'   Contains properties and methods to
'   access Netdocuments ndOffice Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Option Explicit

Const xIniSection As String = "ndOffice"

Implements IDMS
Implements IProfileCreator

Private m_xBrowserShellString As String
Private m_xOpenURL As String
Private m_xSearchURL As String
Private m_oCustFields As mpDMS.CCustomFields
Private WithEvents m_oEvent As CEventGenerator
Attribute m_oEvent.VB_VarHelpID = -1
Private m_xDST As String
Private m_xDeptID As String
'Private m_bLoggedOn As Boolean
'Private m_bLogonAttempted As Boolean
'***Class events***
Private Sub Class_Initialize()
'   dim word as object so that we don't need
'   a reference to the Word 9 object library -
'   compatibility with Word 97 is necessary
    Dim oWord As Object 'word.application
    Dim oAddIn As Object
    
    If g_iTargetApp = mpApp_Word Then
        On Error GoTo ProcError
        Set oWord = Word.Application
        
        On Error Resume Next
        Set oAddIn = oWord.COMAddIns("NetDocuments.Client.WordAddIn")
        On Error GoTo ProcError
        If oAddIn Is Nothing Then
            Err.Raise mpErrors.mpError_InvalidMember, , _
            "NetDocument ndOffice COM Add-in is not loaded.  " & _
            "You may need to re-install ndOffice.  Please contact your administrator."
        End If
    End If
    On Error GoTo ProcError
    Set m_oEvent = New CEventGenerator
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpDMS.CndOfficeProfile.Class_Initialize", Err.Description
End Sub
Private Sub Class_Terminate()
    Set m_oEvent = Nothing
End Sub
'***Properties***
Private Property Get IDMS_AuthorID() As String
    Dim xField As String
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to author id
        xField = mpBase2.GetIni(xIniSection, "AuthorIDAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_AuthorID = GetProfileInfo(xField)
        End If
    End If
End Property
Private Property Get IDMS_ClientID() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to client id
        xField = mpBase2.GetIni(xIniSection, "ClientIDAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_ClientID = GetProfileInfo(xField)
        End If
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_ClientID"
    Exit Property
End Property
Private Property Get IDMS_CreationDate() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to creation date
        xField = mpBase2.GetIni(xIniSection, "CreationDateAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_CreationDate = GetProfileInfo(xField)
        End If
    Else
        Select Case g_iTargetApp
            Case mpApps.mpApp_Excel
                IDMS_CreationDate = Excel.ActiveWorkbook _
                    .BuiltInDocumentProperties("Creation Date")
            Case mpApps.mpApp_PPT
                IDMS_CreationDate = PowerPoint.ActivePresentation _
                    .BuiltInDocumentProperties("Creation Date")
            Case mpApps.mpApp_Word
                IDMS_CreationDate = Word.ActiveDocument _
                    .BuiltInDocumentProperties(wdPropertyTimeCreated)
        End Select
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_CreationDate"
    Exit Property
End Property

Private Property Get IDMS_Custom1() As String
    If IDMS_CustomFields.Count > 0 Then
        IDMS_Custom1 = GetProfileInfo(IDMS_CustomFields.Item(1).Value)
    End If
End Property
Private Property Get IDMS_Custom2() As String
    If IDMS_CustomFields.Count > 1 Then
        IDMS_Custom2 = GetProfileInfo(IDMS_CustomFields.Item(2).Value)
    End If
End Property
Private Property Get IDMS_Custom3() As String
    If IDMS_CustomFields.Count > 2 Then
        IDMS_Custom3 = GetProfileInfo(IDMS_CustomFields.Item(3).Value)
    End If
End Property
Private Function IDMS_CustomFields() As CCustomFields
    
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function

Private Property Get IDMS_DocName() As String
    If IDMS_DocNumber Like "????-????-????" Then
        IDMS_DocName = GetProfileInfo("DocName")
    Else
        Select Case g_iTargetApp
            Case mpApps.mpApp_Excel
                IDMS_DocName = Excel.ActiveWorkbook _
                    .BuiltInDocumentProperties("Title")
            Case mpApps.mpApp_PPT
                IDMS_DocName = PowerPoint.ActivePresentation _
                    .BuiltInDocumentProperties("Title")
            Case mpApps.mpApp_Word
                IDMS_DocName = Word.ActiveDocument _
                    .BuiltInDocumentProperties(wdPropertyTitle)
        End Select
    End If
End Property
Private Function ShortDocInfo() As Variant
    Dim oCOM As Object
    Dim oProfile As Variant
    On Error GoTo ProcError
    Set oCOM = CreateObject("ndOffice.EchoingDataService")
    If Not oCOM Is Nothing Then
        Set ShortDocInfo = oCOM.GetDocumentInfo(GetTargetAppFileFullName())
    End If
ProcError:
    Set oCOM = Nothing
End Function
Private Property Get IDMS_DocNumber() As String
    IDMS_DocNumber = GetProfileInfo("ID")
End Property
Private Property Get IDMS_DocTypeID() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to doc type id
        xField = mpBase2.GetIni(xIniSection, "DocTypeIDAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_DocTypeID = GetProfileInfo(xField)
        End If
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_DocTypeID"
    Exit Property
End Property
Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property
Private Property Get IDMS_Library() As String
    Err.Raise E_NOTIMPL, "mpDMS.CndOfficeProfile", _
        "Library is not an implemented DMS field."
End Property
Private Function IDMS_MainInterface() As Object
    Dim oRet As CndOfficeProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function
Private Property Get IDMS_MatterID() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to matter id
        xField = mpBase2.GetIni(xIniSection, "MatterIDAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_MatterID = GetProfileInfo(xField)
        End If
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_MatterID"
    Exit Property
End Property
Private Property Get IDMS_RevisionDate() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        IDMS_RevisionDate = GetProfileInfo("LastModifiedTime")
    Else
        Select Case g_iTargetApp
            Case mpApps.mpApp_Excel
                IDMS_RevisionDate = Excel.ActiveWorkbook _
                    .BuiltInDocumentProperties("Last Save Time")
            Case mpApps.mpApp_PPT
                IDMS_RevisionDate = PowerPoint.ActivePresentation _
                    .BuiltInDocumentProperties("Last Save Time")
            Case mpApps.mpApp_Word
                IDMS_RevisionDate = Word.ActiveDocument _
                    .BuiltInDocumentProperties(wdPropertyTitle)
        End Select
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_RevisionDate"
    Exit Property
End Property
Private Property Get IDMS_TypistID() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to typist id
        xField = mpBase2.GetIni(xIniSection, "TypistIDAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_TypistID = GetProfileInfo(xField)
        End If
    Else
        Select Case g_iTargetApp
            Case mpApps.mpApp_Excel
                IDMS_TypistID = Excel.Application.UserName
            Case mpApps.mpApp_PPT
                IDMS_TypistID = ""
            Case mpApps.mpApp_Word
                IDMS_TypistID = Word.Application.UserName
        End Select
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_TypistID"
    Exit Property
End Property
Private Property Get IDMS_AuthorName() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to author name
        xField = mpBase2.GetIni(xIniSection, "AuthorNameAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_AuthorName = GetProfileInfo(xField)
        End If
    Else
        Select Case g_iTargetApp
            Case mpApps.mpApp_Excel
                IDMS_AuthorName = Excel.ActiveWorkbook _
                    .BuiltInDocumentProperties("Author")
            Case mpApps.mpApp_PPT
                IDMS_AuthorName = Empty
            Case mpApps.mpApp_Word
                IDMS_AuthorName = Word.ActiveDocument _
                    .BuiltInDocumentProperties(wdPropertyAuthor)
        End Select
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_AuthorName"
    Exit Property
End Property
Private Property Get IDMS_TypistName() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to typist name
        xField = mpBase2.GetIni(xIniSection, "TypistNameAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_TypistName = GetProfileInfo(xField)
        End If
    Else
        Select Case g_iTargetApp
            Case mpApps.mpApp_Excel
                IDMS_TypistName = Excel.Application.UserName
            Case mpApps.mpApp_PPT
                IDMS_TypistName = Empty
            Case mpApps.mpApp_Word
                IDMS_TypistName = Word.Application.UserName
        End Select
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_TypistName"
    Exit Property
End Property
Private Property Get IDMS_ClientName() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to client name
        xField = mpBase2.GetIni(xIniSection, "ClientNameAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_ClientName = GetProfileInfo(xField)
        End If
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_ClientName"
    Exit Property
End Property
Private Property Get IDMS_MatterName() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to matter name
        xField = mpBase2.GetIni(xIniSection, "MatterNameAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_MatterName = GetProfileInfo(xField)
        End If
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_MatterName"
    Exit Property
End Property
Private Property Get IDMS_DocTypeDescription() As String
    Dim xField As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to doc type desc
        xField = mpBase2.GetIni(xIniSection, "DocTypeDescriptionAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_DocTypeDescription = GetProfileInfo(xField)
        End If
    Else
        Select Case g_iTargetApp
            Case mpApps.mpApp_Excel
                IDMS_DocTypeDescription = _
                    Excel.ActiveWorkbook.BuiltInDocumentProperties("Comments")
            Case mpApps.mpApp_PPT
                IDMS_DocTypeDescription = _
                    Word.ActiveDocument.BuiltInDocumentProperties(wdPropertyComments)
            Case mpApps.mpApp_Word
                IDMS_DocTypeDescription = _
                    Word.ActiveDocument.BuiltInDocumentProperties(wdPropertyComments)
        End Select
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_DocTypeDescription"
    Exit Property
End Property
Private Property Get IDMS_Abstract() As String
    Dim xField As String
    If IDMS_DocNumber Like "????-????-????" Then
        'get name of net docs attribute corresponding to abstract
        xField = mpBase2.GetIni(xIniSection, "AbstractAttribute", mpBase2.ApplicationDirectory & g_xIni)
        If xField <> Empty Then
            'attribute has been defined
            IDMS_Abstract = GetProfileInfo(xField)
        End If
    End If
End Property
Private Property Get IDMS_GroupName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CndOfficeProfile", _
        "GroupName is not an implemented DMS field."
End Property
Private Property Get IDMS_Path() As String
    'If Word.ActiveDocument Is Nothing Then Exit Property
    IDMS_Path = GetTargetAppFilePath()
End Property
Private Property Get IDMS_FileName() As String
    'If Word.ActiveDocument Is Nothing Then Exit Property
    If IDMS_DocNumber Like "????-????-????" Then
        IDMS_FileName = GetProfileInfo("FileName")
    Else
        IDMS_FileName = GetTargetAppFileName()
    End If
End Property

Private Property Get IDMS_Version() As String
    On Error GoTo ProcError
    If IDMS_DocNumber Like "????-????-????" Then
        IDMS_Version = GetProfileInfo("Version")
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CndOfficeProfile.IDMS_Version"
    Exit Property
End Property
'***Methods***
Public Function GetProfileInfo(vItem As Variant) As String
    Dim o As Variant
    Dim oAttr As Variant
    Dim oVar As Word.Variable
    Dim xVar As String
    
    On Error Resume Next
    xVar = Word.ActiveDocument.Variables("DMS_Temp").Value
    On Error GoTo ProcError
    
    If xVar <> Empty Then
        GetProfileInfo = GetVarAttributeByID(vItem)
    Else
'       no var is present - use profile info from ndOffice
        On Error Resume Next
        Set o = ShortDocInfo
        If Not o Is Nothing Then
            If UCase(vItem) = "ID" Then
                GetProfileInfo = o.Identifier
            ElseIf UCase(vItem) = "VERSION" Then
                GetProfileInfo = o.Version
            ElseIf UCase(vItem) = "FILENAME" Then
                GetProfileInfo = o.FileName
            ElseIf UCase(vItem) = "CABINET" Then
                GetProfileInfo = o.Cabinet
            ElseIf UCase(vItem) = "LASTMODIFIED" Then
                GetProfileInfo = o.LastModified
            Else
                For Each oAttr In o.ProfileAttributes
                    If UCase(oAttr.Name) = UCase(vItem) Then
                        GetProfileInfo = oAttr.Value
                        Exit Function
                    End If
                Next oAttr
            End If
        End If
    End If
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpDMS.CndOfficeProfile.GetProfileInfo", Err.Description
    Exit Function
End Function
Private Function GetVarAttributeByID(vItem As Variant) As String
'returns the value of the specified data
'from the active document's doc var
    Dim xVar As String
    Dim iPos As Integer
    Dim iStart As Integer
    Dim iEnd As Integer
    
    On Error Resume Next
    xVar = Word.ActiveDocument.Variables("DMS_Temp")
    On Error GoTo ProcError
    If xVar <> Empty And InStr(1, xVar, "ndOffice", vbTextCompare) = 1 Then
'       this data is available - test for field
        iPos = InStr(1, xVar, "||" & vItem & "~", vbTextCompare)
        
        If iPos > 0 Then
'           field exists - get start and end points of field data
            iStart = iPos + Len("||" & vItem & "~")
            iEnd = InStr(iStart, xVar, "||")
            If iEnd > 0 Then
'               field delimiter exists
                GetVarAttributeByID = Mid(xVar, iStart, iEnd - iStart)
            Else
                GetVarAttributeByID = Mid(xVar, iStart)
            End If
        End If
    End If
        
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpDMS.CndOfficeProfile.GetVarAttributeByID", Err.Description
End Function
Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    IDMS_GetProfileInfo = GetProfileInfo(vItem)
End Function
Private Sub IDMS_FileSave()
End Sub
Private Sub IDMS_FileSaveAs()
End Sub
Private Sub IDMS_DocClose()
End Sub
Private Sub IDMS_FileClose()
End Sub
Public Sub FileSaveAs()
End Sub
Public Sub FileSave()
End Sub

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AuthorID(RHS As Variant)
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
End Property

Private Property Get IProfileCreator_ClientID() As Variant
End Property

Private Function IProfileCreator_CreateProfile() As Variant
End Function

Private Function IProfileCreator_CustomProperties() As Collection
End Function

Private Property Get IProfileCreator_DocName() As String
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
End Property

Private Property Get IProfileCreator_Library() As Variant
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
End Property

Private Property Get IProfileCreator_MatterID() As Variant
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
End Property

Private Property Get IProfileCreator_TypistID() As Variant
End Property

Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
End Property

Private Property Get IProfileCreator_DepartmentID() As String
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
End Property

Private Property Get IProfileCreator_SecurityCode() As String
End Property

