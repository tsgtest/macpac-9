VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IDMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"clsDMSDocsOpen"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   IDMS Interface
'   created 12/13/99 by Daniel Fisherman

'   Interface used by document management classes

'   Member of
'   Container for
'**********************************************************
Option Explicit
Public Event Saved()

Public Property Get EventGenerator() As CEventGenerator

End Property
Public Function MainInterface() As Object

End Function
Public Function GetProfileInfo(vItem As Variant) As String

End Function
Public Property Get Version() As String

End Property
Public Property Get DocNumber() As String

End Property
Public Property Get DocName() As String

End Property
Public Property Get DocTypeID() As String

End Property

Public Property Get Library() As String

End Property

Public Property Get AuthorID() As String

End Property

Public Property Get TypistID() As String

End Property

Public Property Get ClientID() As String

End Property

Public Property Get MatterID() As String

End Property

Public Property Get CreationDate() As String

End Property

Public Property Get RevisionDate() As String

End Property

Public Property Get AuthorName() As String

End Property
Public Property Get TypistName() As String

End Property
Public Property Get ClientName() As String

End Property
Public Property Get MatterName() As String

End Property
Public Property Get DocTypeDescription() As String

End Property
Public Property Get Abstract() As String

End Property
Public Property Get GroupName() As String

End Property
Public Property Get Path() As String

End Property
Public Property Get FileName() As String

End Property

Public Function CustomFields() As mpDMS.CCustomFields

End Function

Friend Sub FileSave()

End Sub
Friend Sub FileSaveAs()

End Sub
Friend Sub FileClose()

End Sub
Friend Sub DocClose()

End Sub
Friend Sub FileOpen()

End Sub
Public Property Get Custom1() As String

End Property
Public Property Get Custom2() As String

End Property
Public Property Get Custom3() As String

End Property
