Attribute VB_Name = "mdlGlobal"
Option Explicit
Public Const E_NOTIMPL As Long = &H80004001
Public Const g_xIni As String = "\mpDMS.ini"

Public g_oError As New mpError.CError
Public g_iTargetApp As mpApps

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

Public Const mpNoOpenDocument = "This command is not available because no document is open."

Public g_bCloseFlag As Boolean
Public g_oXLApp As Excel.Application

Public xMsg As String
Public lRet As Long

Function bUnprotect(lProtection As Long, _
                        xPassword As String, _
                        Optional bAddProtectedVariable As Boolean = False, _
                        Optional xTitle As String = "MacPac 9") _
                        As Boolean
'Ensures that document is unprotected before calling macros.
'"zzmpDummy" is used to force error if protected with a password.
'This is the only way to get password for restoration upon reprotection.
'Returns false if document cannot be unprotected.

    Dim bWasSaved As Boolean
    Dim iNumTry As Integer
    Dim xFirmIni As String
    
    On Error GoTo bUnprotect_Error

    xFirmIni = mpBase2.ApplicationDirectory & "\MacPac.INI"
    xPassword = "zzmpDummy"
    
    With ActiveDocument
    
        bWasSaved = .Saved
    
'***    Get protection type
        lProtection = .ProtectionType
        
'***    Unprotect document if needed
        If lProtection <> wdNoProtection Then
            .Unprotect (xPassword)
            If xPassword = "zzmpDummy" Then _
                xPassword = ""
            If bAddProtectedVariable Then
                .Variables("ProtectionType").Value = CStr(lProtection)
                .Variables("ProtectionPassword").Value = xPassword
                If bWasSaved Then .Save
            End If
        End If
        
    End With
    
    bUnprotect = True
    Exit Function

bUnprotect_Error:
    Select Case Err
        Case 5485   'incorrect password
            iNumTry = iNumTry + 1
            If iNumTry = 1 Then
                'try firm's standard password
                'could be in 2 locations (for backward compatibility)
                'GLOG : 5098 : CEH
                xPassword = System.PrivateProfileString(xFirmIni, _
                    "General", "ProtectedDocumentsDefaultPassword")
                If xPassword = "" Then
                    xPassword = System.PrivateProfileString(xFirmIni, _
                        "ProtectedDocuments", "Password")
                End If
                
                If xPassword = "" Then _
                    xPassword = "zzmpDummy"
                Resume
            ElseIf iNumTry = 2 Then
                'prompt for user password
                xPassword = InputBox("In order to complete this operation, document " & _
                    "protection will need to be temporarily removed.  Please " & _
                    "provide the password.", xTitle)
                If xPassword = "" Then
                    MsgBox "This operation cannot be performed on a protected document.", _
                        vbInformation, xTitle
                    Exit Function
                End If
                Resume
            Else
                'user could not supply correct password
                MsgBox "Password is incorrect.  " & _
                    "This operation cannot be performed on a protected document.", _
                    vbInformation, xTitle
                Exit Function
            End If
        Case Else
            g_oError.Raise Err.Number, "mpDMS.mdlGlobal", Err.Description
    End Select
    
End Function

Function bProtect(lProtection As Long, xPassword As String) As Boolean

    Dim bWasSaved As Boolean

    With ActiveDocument
    
        bWasSaved = .Saved
        
'***    Protect document if needed
'       (without resetting fields)
        If lProtection <> wdNoProtection Then
            .Protect lProtection, True, xPassword
            If bWasSaved Then .Save
        End If
        
    End With
End Function

Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
        GetUserVarPath = "Not here"
''       alert to no logon name
'        Err.Raise mpError.mpError_NullValueNotAllowed, , _
'            "UserName is empty.  You might not be logged on to the system. " & _
'                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    GetUserVarPath = mpBase2.xSubstitute(xPath, "<UserName>", xBuf)
        
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "mpDMS.mdlGlobal.GetUserVarPath", Err.Description
End Function

Public Function GetLongFilename _
  (ByVal sShortName As String) _
  As String

  Dim sLongName As String
  Dim sTemp As String
  Dim iSlashPos As Integer

  'Add \ to short name to prevent Instr from failing
  sShortName = sShortName & "\"

  'Start from 4 to ignore the "[Drive Letter]:\" characters
  iSlashPos = InStr(4, sShortName, "\")

  'Pull out each string between \ character for conversion
  While iSlashPos
    sTemp = Dir(Left$(sShortName, iSlashPos - 1), _
      vbNormal + vbHidden + vbSystem + vbDirectory)
    If sTemp = "" Then
      'Error 52 - Bad File Name or Number
      GetLongFilename = ""
      Exit Function
    End If
    sLongName = sLongName & "\" & sTemp
    iSlashPos = InStr(iSlashPos + 1, sShortName, "\")
  Wend

  'Prefix with the drive letter
  GetLongFilename = Left$(sShortName, 2) & sLongName

End Function
Public Sub LoadCustomFields(ByRef oCustFields As CCustomFields, xIniSection As String)
    Dim oFld As CCustomField
    Dim I As Integer
    
    Set oCustFields = New CCustomFields
    With oCustFields
        For I = 1 To 3
            Set oFld = New CCustomField
            oFld.Name = "Custom" & I
            oFld.Value = GetIni(xIniSection, "Custom" & I, mpBase2.ApplicationDirectory & g_xIni)
            .Add oFld
            Set oFld = Nothing
        Next I
    End With
End Sub
Public Function GetWordVersionString() As String
    Dim xTemp As String
    xTemp = "Word"
    If Left(Word.Application.Version, 2) = "14" Then
        xTemp = xTemp & " 2010"
    ElseIf Left(Word.Application.Version, 2) = "12" Then
        xTemp = xTemp & " 2007"
    ElseIf Left(Word.Application.Version, 2) = "11" Then
        xTemp = xTemp & " 2003"
    ElseIf Left(Word.Application.Version, 2) = "10" Then
        xTemp = xTemp & " XP"
    ElseIf Left(Word.Application.Version, 1) = "9" Then
        xTemp = xTemp & " 2000"
    ElseIf Left(Word.Application.Version, 1) = "8" Then
        xTemp = xTemp & " 97"
    End If
    GetWordVersionString = xTemp
End Function
Public Function GetTargetAppFileCount() As Integer
    On Error GoTo ProcError
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            If GetExcelObject().ActiveWorkbook Is Nothing Then
                GetTargetAppFileCount = 0
            Else
                GetTargetAppFileCount = GetExcelObject().Workbooks.Count
            End If
        Case mpApps.mpApp_PPT
            If GetPPTObject().ActivePresentation Is Nothing Then
                GetTargetAppFileCount = 0
            Else
                GetTargetAppFileCount = GetPPTObject().Presentations.Count
            End If
        Case mpApps.mpApp_Word
            GetTargetAppFileCount = Word.Documents.Count
    End Select
    Exit Function
ProcError:
'    g_oError.Raise "mpDMS.mdlGlobal.GetTargetAppFileCount"
    Exit Function
End Function

Public Function GetTargetAppFileFullName() As String
    On Error GoTo ProcError
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            GetTargetAppFileFullName = GetExcelObject().ActiveWorkbook.FullName
        Case mpApps.mpApp_PPT
            GetTargetAppFileFullName = GetPPTObject().ActivePresentation.FullName
        Case mpApps.mpApp_Word
            If Val(Word.Application.Version) > 11 Then
                GetTargetAppFileFullName = ActiveDocument.FullName
            Else
                GetTargetAppFileFullName = Word.WordBasic.FileName$
            End If
    End Select
    Exit Function
ProcError:
    g_oError.Raise "mpDMS.mdlGlobal.GetTargetAppFileFullName"
    Exit Function
End Function

Public Function GetTargetAppFileName(Optional xIniSection As String = "") As String
    On Error GoTo ProcError
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            GetTargetAppFileName = GetExcelObject.ActiveWorkbook.Name
        Case mpApps.mpApp_PPT
            GetTargetAppFileName = GetPPTObject().ActivePresentation.Name
        Case mpApps.mpApp_Word
            Dim xFullName As String
            Dim xFName As String
            Dim xTemp As String
            Dim bWordBasicFileName As Boolean
            
            If xIniSection <> "" Then
                xTemp = GetIni(xIniSection, _
                               "UseWordBasicFileName", _
                               mpBase2.ApplicationDirectory & g_xIni)
                bWordBasicFileName = (UCase(xTemp) = "TRUE")
            End If
            
            If Val(Word.Application.Version) > 11 And (Not bWordBasicFileName) Then
                xFullName = ActiveDocument.FullName
            Else
                xFullName = Word.WordBasic.FileName$
            End If
            'GLOG : 5658 : jsw - DEAL WITH SHAREPOINT PATH (originally for Interior Architects)
            If InStr(UCase(xFullName), "HTTP") = 1 Then
                GetTargetAppFileName = ActiveDocument.Name
            Else
                GetTargetAppFileName = Word.WordBasic.FileNameInfo$(xFullName, 3)
            End If
    End Select
    Exit Function
ProcError:
    g_oError.Raise "mpDMS.mdlGlobal.GetTargetAppFileName"
    Exit Function
End Function

Public Function GetTargetAppFilePath() As String
    On Error GoTo ProcError
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            GetTargetAppFilePath = GetExcelObject.ActiveWorkbook.Path
        Case mpApps.mpApp_PPT
            GetTargetAppFilePath = GetPPTObject.ActivePresentation.Path
        Case mpApps.mpApp_Word
            GetTargetAppFilePath = Word.ActiveDocument.Path
    End Select
    
    '2/22/09 (JTS)
    If GetTargetAppFilePath <> "" Then
'        If Right$(GetTargetAppFilePath, 1) <> "\" Then
        GetTargetAppFilePath = GetTargetAppFilePath & "\"
'        End If
    End If
    
    Exit Function
ProcError:
    g_oError.Raise "mpDMS.mdlGlobal.GetTargetAppFilePath"
    Exit Function
End Function
Public Function GetTargetAppWindowCaption() As String
    On Error GoTo ProcError
    
    If GetTargetAppFileCount = 0 Then
        Exit Function
    End If
    
    Select Case g_iTargetApp
        Case mpApps.mpApp_Excel
            GetTargetAppWindowCaption = GetExcelObject.ActiveWorkbook.Windows(1).Caption
        Case mpApps.mpApp_PPT
            GetTargetAppWindowCaption = GetPPTObject().ActivePresentation.Windows(1).Caption
        Case mpApps.mpApp_Word
            GetTargetAppWindowCaption = Word.ActiveDocument.ActiveWindow.Caption
    End Select
    
    Exit Function
ProcError:
    g_oError.Raise "mpDMS.mdlGlobal.GetTargetAppWindowCaption"
    Exit Function
End Function
Public Function GetExcelObject() As Excel.Application
    If g_oXLApp Is Nothing Then
        Set g_oXLApp = GetObject(, "Excel.Application")
    End If
    Set GetExcelObject = g_oXLApp
End Function

Public Function GetPPTObject() As PowerPoint.Application
    Set GetPPTObject = GetObject(, "Powerpoint.Application")
End Function
