VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CWindowsProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'**********************************************************
'   CWindowsProfile Class
'   created 3/30/00 by Daniel Fisherman

'   Contains properties and methods to
'   access Windows Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Option Explicit
Const xIniSection As String = "Native Word"

Implements IDMS
Implements IProfileCreator

Private m_oDoc As Word.Document
Private m_oCustFields As CCustomFields
Private WithEvents m_oEvent As CEventGenerator
Attribute m_oEvent.VB_VarHelpID = -1


'***Class events***
Private Sub Class_Initialize()
    Set m_oEvent = New CEventGenerator
End Sub

Private Sub Class_Terminate()
    Set m_oEvent = Nothing
End Sub

'***Properties***
Private Property Get IDMS_Abstract() As String
End Property

Private Property Get IDMS_AuthorID() As String
    Dim xTemp As String
    Dim xInits As String
    Dim lPos As Long
    
    On Error Resume Next
'   try author initials doc prop
    Select Case g_iTargetApp
        Case mpApp_Word
            xTemp = Word.ActiveDocument.CustomDocumentProperties("Author Initials")
        Case mpApp_Excel
            xTemp = GetExcelObject().ActiveWorkbook.CustomDocumentProperties("Author Initials")
        Case mpApp_PPT
            xTemp = GetPPTObject().ActivePresentation.CustomDocumentProperties("Author Initials")
    End Select
    
'   try building a default one
    If xTemp = Empty Then
'       build string
        xTemp = Left(IDMS_AuthorName, 1)
        
        xInits = Left(xTemp, 1)
        
        lPos = InStr(xTemp, " ")
        If lPos Then
            xInits = xInits & Mid(xTemp, lPos + 1, 1)
        End If
        
        lPos = InStr(lPos + 1, xTemp, " ")
        If lPos Then
            xInits = xInits & Mid(xTemp, lPos + 1, 1)
        End If
        xTemp = xInits
    End If
    IDMS_AuthorID = xTemp
End Property

Private Property Get IDMS_AuthorName() As String
    Select Case g_iTargetApp
        Case mpApp_Word
            IDMS_AuthorName = Word.ActiveDocument _
                .BuiltInDocumentProperties(wdPropertyAuthor)
        Case mpApp_Excel
            IDMS_AuthorName = GetExcelObject().ActiveWorkbook _
                .BuiltInDocumentProperties("Author")
        Case mpApp_PPT
            IDMS_AuthorName = GetPPTObject().ActivePresentation _
                .BuiltInDocumentProperties("Author")
    End Select
End Property

Private Property Get IDMS_ClientID() As String
End Property

Private Property Get IDMS_ClientName() As String
End Property

Private Property Get IDMS_CreationDate() As String
    Select Case g_iTargetApp
        Case mpApp_Word
            IDMS_CreationDate = Word.ActiveDocument _
                .BuiltInDocumentProperties(wdPropertyTimeCreated)
        Case mpApp_Excel
            IDMS_CreationDate = GetExcelObject().ActiveWorkbook _
                .BuiltInDocumentProperties("Creation Date")
        Case mpApp_PPT
            IDMS_CreationDate = GetPPTObject().ActivePresentation _
                .BuiltInDocumentProperties("Creation Date")
    End Select
End Property
Private Property Get IDMS_Custom1() As String
    If g_iTargetApp <> mpApp_Word Then Exit Property
    If IDMS_CustomFields.Count > 0 Then
        IDMS_Custom1 = WordBasic.[GetDocumentProperty$](IDMS_CustomFields.Item(1).Value)
    End If
End Property
Private Property Get IDMS_Custom2() As String
    If g_iTargetApp <> mpApp_Word Then Exit Property
    If IDMS_CustomFields.Count > 1 Then
        IDMS_Custom2 = WordBasic.[GetDocumentProperty$](IDMS_CustomFields.Item(2).Value)
    End If
End Property
Private Property Get IDMS_Custom3() As String
    If g_iTargetApp <> mpApp_Word Then Exit Property
    If IDMS_CustomFields.Count > 2 Then
        IDMS_Custom3 = WordBasic.[GetDocumentProperty$](IDMS_CustomFields.Item(3).Value)
    End If
End Property
Private Function IDMS_CustomFields() As CCustomFields
   
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function

Private Sub IDMS_DocClose()
    DocClose
End Sub

Private Property Get IDMS_DocName() As String
    Select Case g_iTargetApp
        Case mpApp_Word
            IDMS_DocName = Word.ActiveDocument _
                .BuiltInDocumentProperties(wdPropertyTitle)
        Case mpApp_Excel
            IDMS_DocName = GetExcelObject().ActiveWorkbook _
                .BuiltInDocumentProperties("Title")
        Case mpApp_PPT
            IDMS_DocName = GetPPTObject().ActivePresentation _
                .BuiltInDocumentProperties("Title")
    End Select
End Property

Private Property Get IDMS_DocNumber() As String
    IDMS_DocNumber = IDMS_FileName
End Property

Private Property Get IDMS_DocTypeDescription() As String
    Select Case g_iTargetApp
        Case mpApp_Word
            IDMS_DocTypeDescription = Word.ActiveDocument _
                .BuiltInDocumentProperties(wdPropertyComments)
        Case mpApp_Excel
            IDMS_DocTypeDescription = GetExcelObject().ActiveWorkbook _
                .BuiltInDocumentProperties("Comments")
        Case mpApp_PPT
            IDMS_DocTypeDescription = GetPPTObject().ActivePresentation _
                .BuiltInDocumentProperties("Comments")
    End Select
End Property

Private Property Get IDMS_DocTypeID() As String
End Property

Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property

Private Sub IDMS_FileClose()
    FileClose
End Sub

Private Property Get IDMS_FileName() As String
    IDMS_FileName = GetTargetAppFileName(xIniSection)
End Property

Private Sub IDMS_FileSave()
    FileSave
End Sub

Private Sub IDMS_FileSaveAs()
    FileSaveAs
End Sub

Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    g_oError.Raise E_NOTIMPL, "mpDMS.CWindowsProfile", g_oError.Desc(E_NOTIMPL)
End Function

Private Property Get IDMS_GroupName() As String
End Property

Private Property Get IDMS_Library() As String
End Property

Private Function IDMS_MainInterface() As Object
    Dim oRet As CWindowsProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function

Private Property Get IDMS_MatterID() As String
End Property

Private Property Get IDMS_MatterName() As String
End Property

Private Property Get IDMS_Path() As String
    Dim xFullName As String

    IDMS_Path = GetTargetAppFilePath()
End Property

Private Property Get IDMS_RevisionDate() As String
    IDMS_RevisionDate = _
        Word.ActiveDocument.BuiltInDocumentProperties(wdPropertyTimeLastSaved)
End Property

Private Property Get IDMS_TypistID() As String
End Property

Private Property Get IDMS_TypistName() As String
    IDMS_TypistName = Word.Application.UserName
End Property

Private Property Get IDMS_Version() As String
End Property
Public Sub FileSaveAs()
    
    Select Case g_iTargetApp
        Case mpApp_Word
            FileSaveAsWord
        Case mpApp_Excel
            FileSaveAsExcel
        Case mpApp_PPT
            FileSaveAsPPT
    End Select
End Sub
Private Sub FileSaveAsWord()
    Dim lRet As Long
    Dim lErr As Long
    On Error GoTo FileSaveAs_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    Dim xOrig As String
    xOrig = Word.ActiveDocument.FullName
    On Error Resume Next
    lRet = Word.Dialogs(wdDialogFileSaveAs).Show
    '  Ignore error Word generates when attempting to save over read-only file
    lErr = Err.Number
    On Error GoTo FileSaveAs_Error
    If lErr = 5155 Then
        ' This error will be generated even if filename is corrected and document saved
        If Word.ActiveDocument.Saved And (xOrig <> Word.ActiveDocument.FullName) Then
            lRet = -1
        End If
    ElseIf lErr <> 0 Then
        Err.Raise lErr
    End If
    If lRet Then
       m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    g_oError.Raise Err.Number, "mpDMS.CWindowsProfile", Err.Description
End Sub
Private Sub FileSaveAsExcel()
    Dim lRet As Long
    Dim dlg As Excel.Dialog
    
    On Error GoTo FileSaveAs_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
    
    Set dlg = GetExcelObject.Dialogs(xlDialogSaveAs)
    lRet = dlg.Show
    If lRet Then
       m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    g_oError.Raise Err.Number, "mpDMS.WindowsProfile", Err.Description
End Sub
Private Sub FileSaveAsPPT()
'    Dim lRet As Long
'    Dim dlg As Office.FileDialog
'
'    On Error GoTo FileSaveAs_Error
'
'    If GetTargetAppFileCount = 0 Then
'        MsgBox mpNoOpenDocument, vbInformation, App.Title
'        Exit Sub
'    End If

    SendKeys "{F12}", True

'    Set dlg = GetPPTObject.FileDialog(msoFileDialogSaveAs)
'    lRet = dlg.Show
'    If lRet Then
'       m_oEvent.RaiseFileSavedEvent
'    End If
    Exit Sub

FileSaveAs_Error:
    g_oError.Raise Err.Number, "mpDMS.WindowsProfile", Err.Description
End Sub
Public Sub FileSave()
    Select Case g_iTargetApp
        Case mpApp_Word
            FileSaveWord
        Case mpApp_Excel
            FileSaveExcel
        Case mpApp_PPT
            FileSavePPT
    End Select
End Sub
Public Sub FileSaveWord()
    
    Dim iFormat As Integer
    Dim xFormat As String
    Dim xMsg As String
    Dim iRet As Integer
    Dim xCurFile As String
    Dim xTempFile As String
    Dim conv As FileConverter
    
    On Error GoTo FileSave_Error
    
    With ActiveDocument
        
        '9.7.1 #4233 - avoid error 5155 for read-only docs
        If .ReadOnly Then
            FileSaveAs
            Exit Sub
        End If
        
        iFormat = .SaveFormat
        
        'Handle native Word formats
        If iFormat = 0 Or iFormat = 1 Or (iFormat >= 12 And iFormat <= 16) Then
            .Save
        Else
'---        non word format, prompt as in Native Word
            If Not .Saved Then

'---            since open/save converters are shuffled together
'               in collection we need to look only at save converters
'---            to display format name correctly in dbox
                Select Case iFormat
                    Case wdFormatText
                        xFormat = "Text Only"
                    Case wdFormatTextLineBreaks
                        xFormat = "Text Only with Line Breaks"
                    Case wdFormatRTF
                        xFormat = "Rich Text Format"
                    Case wdFormatUnicodeText
                        xFormat = "Unicode Text"
                    Case wdFormatDOSText
                        xFormat = "MS-DOS Text"
                    Case wdFormatDOSTextLineBreaks
                        xFormat = "MS-DOS Text with Line Breaks"
                    Case Else
                        For Each conv In FileConverters
                            If conv.CanSave Then
                                If conv.SaveFormat = iFormat Then
                                    xFormat = conv.FormatName
                                End If
                            End If
                        Next conv
                End Select
                If xFormat = Empty Then _
                    xFormat = "unknown"
                    
                If iFormat > 1 And (iFormat < 12 Or iFormat > 16) Then
                    xMsg = "This document may contain formatting which " & _
                           "will be lost upon conversion to " & xFormat & _
                           " format.  " & _
                           "Click No to save the document in Word format " & _
                           "and preserve the current formatting." & _
                           String(2, vbLf) & "Continue with save in " & _
                           xFormat & " format?"
                    
                    iRet = MsgBox(xMsg, vbQuestion + vbYesNoCancel, App.Title)
                    Select Case iRet
                        Case vbYes
'                           save in non word format
                            .Save
                            
                        Case vbNo
'                           convert to Word Format when saving
                            If Val(Word.Application.Version) > 11 Then
                                xTempFile = "c:\tempsave.docx"
                            Else
                                xTempFile = "c:\tempsave.doc"
                            End If
                            
                            xCurFile = ActiveDocument.FullName

'---                        save to temp file in Word format
                            ActiveDocument.SaveAs FileName:=xTempFile, _
                                                  FileFormat:=0
                    
'---                        save back to original filename
                            ActiveDocument.SaveAs FileName:=xCurFile, _
                                                  FileFormat:=0
                                
                            On Error Resume Next
                            Kill xTempFile
                            
                        Case Else
                            Exit Sub
                    End Select
                Else
                    .Save
                End If
            End If
        End If
         
        If .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
    
FileSave_Error:
'raise all errors except cancel
    If Err.Number <> 4198 Then
        g_oError.Raise Err.Number, "mpDMS.CWindowsProfile", Err.Description
    End If
End Sub
Public Sub FileSaveExcel()
    
    On Error GoTo FileSave_Error
    
    With GetExcelObject
        .ActiveWorkbook.Save
         
        If .ActiveWorkbook.Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
    
FileSave_Error:
'raise all errors except cancel
    If Err.Number <> 4198 Then
        g_oError.Raise Err.Number, "mpDMS.CWindowsProfile", Err.Description
    End If
End Sub
Public Sub FileSavePPT()
    
    On Error GoTo FileSave_Error
    
    With GetPPTObject
        .ActivePresentation.Save
         
        If .ActivePresentation.Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
    
FileSave_Error:
'raise all errors except cancel
    If Err.Number <> 4198 Then
        g_oError.Raise Err.Number, "mpDMS.CWindowsProfile", Err.Description
    End If
End Sub
Public Sub FileClose()
    Select Case g_iTargetApp
        Case mpApp_Word
            FileCloseWord
        Case mpApp_Excel
            FileCloseExcel
        Case mpApp_PPT
            FileClosePPT
    End Select
End Sub
Public Sub FileCloseWord()
'close doc, run autotrailer-
'autotrailer runs trailer if
'filename has changed as a result
'of the close
    Dim iUserChoice As Integer
    
    On Error GoTo FileClose_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
'   the following is necessary so that
'   trailer will run when appropriate -
'   if we just run the native command,
'   we can't run trailer at all
    With ActiveDocument
        If Not .Saved Then

'           prompt for doc save
            xMsg = "Do you want to save the changes you made to " & .Name & "?"
            iUserChoice = MsgBox(xMsg, _
                                 vbExclamation + vbYesNoCancel, _
                                 "Microsoft Word")
            Select Case iUserChoice
                Case vbYes
                    FileSave
                    If .Saved Then
                        .Close
                    End If
                Case vbNo
                    .Saved = True
                    .Close
                Case Else
            End Select
        Else
            .Close
        End If
    End With
    
'   code below resets pointer,
'   which seems to remain as an hourglass
'   if the user cancels out of save
    Application.ScreenRefresh
    Exit Sub
    
FileClose_Error:
    g_oError.Raise Err.Number, "mpDMS.CWindowsProfile", Err.Description
End Sub
Public Sub FileCloseExcel()
'close doc, run autotrailer-
'autotrailer runs trailer if
'filename has changed as a result
'of the close
    Dim iUserChoice As Integer
    
    On Error GoTo FileClose_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
'   the following is necessary so that
'   trailer will run when appropriate -
'   if we just run the native command,
'   we can't run trailer at all
    With GetExcelObject.ActiveWorkbook
        If Not .Saved Then

'           prompt for doc save
            xMsg = "Do you want to save the changes you made to " & .Name & "?"
            iUserChoice = MsgBox(xMsg, _
                                 vbExclamation + vbYesNoCancel, _
                                 "Microsoft Excel")
            Select Case iUserChoice
                Case vbYes
                    FileSave
                    If .Saved Then
                        .Close
                    End If
                Case vbNo
                    .Saved = True
                    .Close
                Case Else
            End Select
        Else
            .Close
        End If
    End With
    
    Exit Sub
    
FileClose_Error:
    g_oError.Raise Err.Number, "mpDMS.WindowsProfile", Err.Description
End Sub
Public Sub FileClosePPT()
'close doc, run autotrailer-
'autotrailer runs trailer if
'filename has changed as a result
'of the close
    Dim iUserChoice As Integer
    
    On Error GoTo FileClose_Error
    
    If GetTargetAppFileCount = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
'   the following is necessary so that
'   trailer will run when appropriate -
'   if we just run the native command,
'   we can't run trailer at all
    With GetPPTObject.ActivePresentation
        If Not .Saved Then

'           prompt for doc save
            xMsg = "Do you want to save the changes you made to " & .Name & "?"
            iUserChoice = MsgBox(xMsg, _
                                 vbExclamation + vbYesNoCancel, _
                                 "Microsoft Word")
            Select Case iUserChoice
                Case vbYes
                    FileSave
                    If .Saved Then
                        .Close
                    End If
                Case vbNo
                    .Saved = True
                    .Close
                Case Else
            End Select
        Else
            .Close
        End If
    End With
    
    Exit Sub
    
FileClose_Error:
    g_oError.Raise Err.Number, "mpDMS.WindowsProfile", Err.Description
End Sub
Public Sub DocClose()
'   close active window if doc has
'   more than one window, else close
'   file - (this mimics Native DOC CLOSE)

    If g_iTargetApp <> mpApp_Word Then Exit Sub
    On Error Resume Next
    If Application.Windows.Count = 0 Then _
        Exit Sub
    
    If ActiveDocument.Windows.Count > 1 Then
        If Word.ActiveDocument.ActiveWindow.View = wdPrintPreview Then
            ActiveDocument.ClosePrintPreview
        Else
            ActiveDocument.ActiveWindow.Close
        End If
    Else
        FileClose
    End If
End Sub

Public Sub FileOpen()
    m_oEvent.RaiseFileOpenedEvent
End Sub

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AuthorID(RHS As Variant)
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
End Property

Private Property Get IProfileCreator_ClientID() As Variant
End Property

Private Function IProfileCreator_CreateProfile() As Variant
End Function

Private Function IProfileCreator_CustomProperties() As Collection
End Function

Private Property Get IProfileCreator_DocName() As String
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
End Property

Private Property Get IProfileCreator_Library() As Variant
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
End Property

Private Property Get IProfileCreator_MatterID() As Variant
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
End Property

Private Property Get IProfileCreator_TypistID() As Variant
End Property

Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
End Property

Private Property Get IProfileCreator_DepartmentID() As String
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
End Property

Private Property Get IProfileCreator_SecurityCode() As String
End Property



