VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHummingbirdCOMProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   cHummingbirdCOMProfile Class
'   created 9/26/05 by Jeffrey Sweetland

'   Contains properties and methods to
'   access Hummingbird DM Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Option Explicit

Private Declare Function GetForegroundWindow Lib "user32" () As Long

Dim m_oAppInt As DOCSObjects.DM
Const xIniSection As String = "Hummingbird COM"

Const APP_SELECT = 8
Const UNMANAGED = 9
Const NOT_LOGGED_IN = 10
'Return Codes = True, False, APP_SELECT, NOT_LOGGED_IN

Const mpDMProfile_DOCNUMBER As String = "DOCNUM"
Const mpDMProfile_DOCNAME As String = "DOCNAME"
Const mpDMProfile_AUTHORUSERID As String = "AUTHOR_ID"
Const mpDMProfile_AUTHORFULLNAME As String = "AUTHOR_FULL_NAME"
Const mpDMProfile_TYPISTUSERID As String = "TYPIST_ID"
Const mpDMProfile_DOCTYPEID As String = "TYPE_ID"
Const mpDMProfile_DOCTYPEDESC As String = "DOCUMENTTYPE"
Const mpDMProfile_CREATIONDATE As String = "CREATION_DATE"
Const mpDMProfile_LASTEDITDATE As String = "LASTEDITDATE"
Const mpDMProfile_ABSTRACT As String = "ABSTRACT"
Const mpDMProfile_MATTERID As String = "MATTER_ID"
Const mpDMProfile_CLIENTID As String = "CLIENT_ID"
Const mpDMProfile_LIBRARY As String = "LIBNAME" 'JTS 7/19/11

Private WithEvents m_oEvent As CEventGenerator
Attribute m_oEvent.VB_VarHelpID = -1

Implements IDMS
Implements IProfileCreator

Private m_oCustFields As mpDMS.CCustomFields
Private m_bFirstSaved As Boolean

'***Class events***
Private Sub Class_Initialize()
'   dim word as object so that we don't need
'   a reference to the Word 9 object library -
'   compatibility with Word 97 is necessary
    Dim oWord As Object 'word.application
    Dim oAddIn As Object
    
    If g_iTargetApp = mpApp_Word Then
        On Error GoTo ProcError
        Set oWord = Word.Application
        
        On Error Resume Next
        If Val(Word.Application.Version) < 12 Then
            Set oAddIn = oWord.COMAddIns("DM_COM_Addin.WordAddin")
        ElseIf Val(Word.Application.Version) = 12 Then
            Set oAddIn = oWord.COMAddIns("DMIntegration_NET.Connect")
        ElseIf Val(Word.Application.Version) = 14 Then
            Set oAddIn = oWord.COMAddIns("DMWord2010")
        'GLOG 7919: Word 2013
        ElseIf Val(Word.Application.Version) = 15 Then
            'JTS 11/19/10: Different COM Addin name for 2010
            Set oAddIn = oWord.COMAddIns("DMWord2013")
        ElseIf Val(Word.Application.Version) = 16 Then
            'GLOG : 15914 : ceh
            Set oAddIn = oWord.COMAddIns("DMWord2016")
        End If
        On Error GoTo ProcError
        
        If oAddIn Is Nothing Then
            Err.Raise mpErrors.mpError_InvalidMember, , _
            "Hummingbird DM COM Add-in is not loaded.  " & _
            "You may need to re-install Hummingbird DM.  Please contact your administrator."
        End If
        'Workaround for bug with initial 5.2 release - first save in session may not display profile dialog
        If mpBase2.bIniToBoolean(mpBase2.GetIni(xIniSection, "Enable52FirstSaveFix", mpBase2.ApplicationDirectory & g_xIni)) Then
            m_bFirstSaved = False
        Else
            m_bFirstSaved = True
        End If
    End If
    Set m_oEvent = New CEventGenerator
    Set m_oAppInt = CreateObject("DOCSObjects.DM")
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpDMS.cHummingbirdCOMProfile.Class_Initialize", Err.Description
End Sub
Private Sub Class_Terminate()
    Set m_oEvent = Nothing
    'Set m_oAppInt = Nothing
End Sub
'***************************************************
'IDMS Interface
'***************************************************
Private Property Get IDMS_AuthorID() As String
    IDMS_AuthorID = GetProfileInfo(mpDMProfile_AUTHORUSERID)
End Property
Private Property Get IDMS_ClientID() As String
    IDMS_ClientID = GetProfileInfo(mpDMProfile_CLIENTID)
End Property
Private Property Get IDMS_CreationDate() As String
    IDMS_CreationDate = GetProfileInfo(mpDMProfile_CREATIONDATE)
End Property

Private Property Get IDMS_Custom1() As String
    If IDMS_CustomFields.Count > 0 Then
        IDMS_Custom1 = GetProfileInfo(IDMS_CustomFields.Item(1).Value)
    End If
End Property
Private Property Get IDMS_Custom2() As String
    If IDMS_CustomFields.Count > 1 Then
        IDMS_Custom2 = GetProfileInfo(IDMS_CustomFields.Item(2).Value)
    End If
End Property
Private Property Get IDMS_Custom3() As String
    If IDMS_CustomFields.Count > 2 Then
        IDMS_Custom3 = GetProfileInfo(IDMS_CustomFields.Item(3).Value)
    End If
End Property
Private Function IDMS_CustomFields() As CCustomFields
    
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function
Private Property Get IDMS_DocName() As String
    IDMS_DocName = GetProfileInfo(mpDMProfile_DOCNAME)
End Property
Private Property Get IDMS_DocNumber() As String
    Dim xElements() As String
    Dim xNum As String
    On Error GoTo ProcError
    If m_oAppInt Is Nothing Then
        Set m_oAppInt = CreateObject("DOCSObjects.DM")
    End If
    
    If bIsDMDoc(GetTargetAppFileFullName()) Then
        On Error GoTo ProcError
        ' Version is third element in filename
        xElements = SplitDMName(GetTargetAppFileName())
        If UBound(xElements) > 0 Then
            xNum = xElements(1)
            If InStr(xNum, "#") = 1 Then
                IDMS_DocNumber = Mid(xNum, 2)
            End If
        End If
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CHummingbirdCOMProfile.IDMS_DocNumber"
End Property
Private Property Get IDMS_DocTypeID() As String
    IDMS_DocTypeID = GetProfileInfo(mpDMProfile_DOCTYPEID)
End Property
Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property
Private Property Get IDMS_Library() As String
    Dim xLibrary As String
    Dim xRegKey As String
    Dim xTestLib As String
    
    On Error GoTo ProcError
    Dim xElements() As String

    If m_oAppInt Is Nothing Then
        Set m_oAppInt = CreateObject("DOCSObjects.DM")
    End If

    If bIsDMDoc(GetTargetAppFileFullName()) Then
        On Error GoTo ProcError
        ' Library is first element in filename
        xElements = SplitDMName(GetTargetAppFileName())
        xLibrary = xElements(0)
        If InStr(xLibrary, "_") > 0 Then
            'Underscores in file name might be replacing spaces in Library Name, so test which string exists in the registry
            xRegKey = "Software\Hummingbird\PowerDOCS\Core\Plugins\Fusion\OM\CheckedOutDocs\" + xLibrary
            If Not RegKeyExists(HKEY_CURRENT_USER, xRegKey) Then
                xTestLib = Replace(xLibrary, "_", " ")
                xRegKey = "Software\Hummingbird\PowerDOCS\Core\Plugins\Fusion\OM\CheckedOutDocs\" + xTestLib
                If RegKeyExists(HKEY_CURRENT_USER, xRegKey) Then
                    xLibrary = xTestLib
                End If
            End If
        End If
        IDMS_Library = xLibrary
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CHummingbirdCOMProfile.IDMS_Library"
End Property
Private Function IDMS_MainInterface() As Object
    Dim oRet As CHummingbirdCOMProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function
Private Property Get IDMS_MatterID() As String
    IDMS_MatterID = GetProfileInfo(mpDMProfile_MATTERID)
End Property
Private Property Get IDMS_RevisionDate() As String
    IDMS_RevisionDate = GetProfileInfo(mpDMProfile_LASTEDITDATE)
End Property
Private Property Get IDMS_TypistID() As String
    IDMS_TypistID = GetProfileInfo(mpDMProfile_TYPISTUSERID)
End Property
Private Property Get IDMS_AuthorName() As String
    IDMS_AuthorName = GetProfileInfo(mpDMProfile_AUTHORFULLNAME)
End Property
Private Property Get IDMS_TypistName() As String
    Err.Raise E_NOTIMPL, "mpDMS.cHummingbirdCOMProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_ClientName() As String
    Err.Raise E_NOTIMPL, "mpDMS.cHummingbirdCOMProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_MatterName() As String
    Err.Raise E_NOTIMPL, "mpDMS.cHummingbirdCOMProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_DocTypeDescription() As String
    IDMS_DocTypeDescription = GetProfileInfo(mpDMProfile_DOCTYPEDESC)
End Property
Private Property Get IDMS_Abstract() As String
    IDMS_Abstract = GetProfileInfo(mpDMProfile_ABSTRACT)
End Property
Private Property Get IDMS_GroupName() As String
    Err.Raise E_NOTIMPL, "mpDMS.cHummingbirdCOMProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Path() As String
    'JTS 6/20/12 - Excel and Powerpoint compatibility
    If GetTargetAppFileCount = 0 Then Exit Property
    IDMS_Path = GetTargetAppFilePath()
End Property
Private Property Get IDMS_FileName() As String
    'JTS 6/20/12 - Excel and Powerpoint compatibility
    If GetTargetAppFileCount = 0 Then Exit Property
    IDMS_FileName = GetTargetAppFileName()
End Property

Private Property Get IDMS_Version() As String
    Dim xElements() As String
    Dim xVer As String
   
    If m_oAppInt Is Nothing Then
        Set m_oAppInt = CreateObject("DOCSObjects.DM")
   End If
   
    If bIsDMDoc(GetTargetAppFileFullName()) Then 'JTS 6/20/12 - Needs to be compatible with other Apps besides Word
        On Error GoTo ProcError
        ' Version is third element in filename
        xElements = SplitDMName(GetTargetAppFileName)
        If UBound(xElements) > 1 Then
            xVer = xElements(2)
            If InStr(UCase(xVer), "V") = 1 Then
                IDMS_Version = Mid(xVer, 2)
            End If
        End If
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CHummingbirdCOMProfile.IDMS_Version"
End Property '***Methods***
Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    IDMS_GetProfileInfo = GetProfileInfo(vItem)
End Function
Private Sub IDMS_FileSave()
    FileSave
End Sub
Private Sub IDMS_FileSaveAs()
    FileSaveAs
End Sub
Private Sub IDMS_DocClose()
    DocClose
End Sub
Private Sub IDMS_FileClose()
    FileClose
End Sub

'***************************************************
'Main Interface
'***************************************************

Public Function GetProfileInfo(ByVal FieldName As String) As String
    GetProfileInfo = GetProfileInfoPCD(GetTargetAppFileName, FieldName)
End Function
Public Sub FileSaveAs()
    'Only DM Native code should be used for this
End Sub
Public Sub FileSave()
    On Error Resume Next
    'Word only
    If g_iTargetApp = mpApp_Word Then
        Dim oSave As Object
        If Val(Word.Application.Version) > 11 Then
            Set oSave = Word.CommandBars
            oSave.ExecuteMso "FileSave"
        Else
            Set oSave = Word.CommandBars("File").FindControl(ID:=3)
            oSave.Execute
        End If
    End If
End Sub

Public Sub FileClose()
    'Only DM Native code should be used for this
End Sub

Public Sub FileOpen()
    MsgBox "mpDMS.cHummingbirdCOMProfile.FileOpen is not built."
    'm_oEvent.RaiseFileOpenedEvent
End Sub

Public Sub DocClose()
    'Only DM Native code should be used for this
End Sub

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
End Property

Private Property Let IProfileCreator_AuthorID(RHS As Variant)
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
End Property

Private Property Get IProfileCreator_ClientID() As Variant
End Property

Private Function IProfileCreator_CreateProfile() As Variant
End Function

Private Function IProfileCreator_CustomProperties() As Collection
End Function

Private Property Get IProfileCreator_DocName() As String
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
End Property

Private Property Get IProfileCreator_Library() As Variant
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
End Property

Private Property Get IProfileCreator_MatterID() As Variant
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
End Property

Private Property Get IProfileCreator_TypistID() As Variant
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
End Property

Private Property Get IProfileCreator_DepartmentID() As String
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
End Property

Private Property Get IProfileCreator_SecurityCode() As String
End Property

Private Function bIsAutoRecover(oDoc As Word.Document) As Boolean
    Dim vKeyValue As Variant
    On Error GoTo ProcError
    vKeyValue = GetKeyValue(HKEY_CURRENT_USER, "Software\Hummingbird\PowerDOCS\Core\Plugins\Fusion\OM\Excludes", _
        "MS WORD")
    If Val(vKeyValue) <> 1 Then
        ' DM AutoRecover functionality not enabled
        Exit Function
    End If
    bIsAutoRecover = Right(UCase(oDoc.ActiveWindow.Caption), 11) = "(RECOVERED)"
ProcError:
End Function
Private Function bDocIsCheckedOut(xFile As String)
    ' Returns true if current document is checked out by current user
    On Error GoTo ProcError
    Dim xLib As String
    Dim xNum As String
    Dim xName As String
    Dim xElements() As String
    Dim xRegKey As String
    If Not bIsDMDoc(xFile) Then
        bDocIsCheckedOut = False
        GoTo ExitProc
    End If
    
    '*c
    xName = GetTargetAppFileName ' Word.WordBasic.FileNameInfo$(xFile, 0)
    xElements = SplitDMName(xName)
    If UBound(xElements) > 0 Then
        'JTS 7/19/11: Use GetDocInfo instead of parsing file name,
        'since spaces in Library name will be replaced by underscores in file name
        xLib = IDMS_Library ' xElements(0)
        xNum = IDMS_DocNumber   ' GetProfileInfoFromPath(xFile, mpDMProfile_DOCNUMBER)
    End If
    If (xLib = "" Or xNum = "") Then
        bDocIsCheckedOut = False
        GoTo ExitProc
    End If
    xRegKey = "Software\Hummingbird\PowerDOCS\Core\Plugins\Fusion\OM\CheckedOutDocs\" + xLib + "\" + xNum
    If mpRegistry.RegKeyExists(HKEY_CURRENT_USER, xRegKey) Then
        bDocIsCheckedOut = True
    Else
        bDocIsCheckedOut = False
    End If
ExitProc:
ProcError:
End Function

Private Function bIsDMDoc(xFile As String)
    Dim xElements() As String
    Dim lRet As Long
    Dim xNum As String
    
    On Error GoTo ProcError
    'Always return false for UNC path
    If InStr(xFile, "\\") = 1 Then
        bIsDMDoc = False
        Exit Function
    End If
    xElements = SplitDMName(xFile)
    If UBound(xElements) > 2 Then
        If m_oAppInt Is Nothing Then
            Set m_oAppInt = CreateObject("DOCSObjects.DM")
        End If
        
        lRet = m_oAppInt.IsDMDoc(xFile)
        bIsDMDoc = (lRet < 0)
    End If
ProcError:
End Function
Private Function SplitDMName(xName As String) As String()
    'Don't check for underscore anymore
    If mpBase2.lCountChrs(xName, "-") > 0 Then
        SplitDMName = Split(xName, "-")
    End If
End Function
Private Function GetProfileInfoPCD(ByVal xFileName As String, ByVal xField As String) As String
    Dim oPCDDoc As Object
    Dim oDMApp As Object
    Dim xDocNum As String
    Dim xLibrary As String
    Dim xDST As String
    Dim xForm As String 'GLOG 5613
    Dim xRegKey As String
    Dim xTestLib As String
    
    Dim xElements() As String
    'GLOG 5452 (JTS 10/17/14): Simplified API does not return correct profile information
    'for newly-saved documents using DM 10.  Therefore, we are using the the DM Extensions API instead
    On Error GoTo ProcError
    'MsgBox "xFilename=" & xFileName & vbCrLf & "xField=" & xField
    xElements = SplitDMName(xFileName)
    If UBound(xElements) > 0 Then
        xDocNum = xElements(1)
        If InStr(xDocNum, "#") = 1 Then
            xDocNum = Mid(xDocNum, 2)
        End If
        xLibrary = xElements(0)
        If InStr(xLibrary, "_") > 0 Then
            'Underscores in file name might be replacing spaces in Library Name, so test which string exists in the registry
            xRegKey = "Software\Hummingbird\PowerDOCS\Core\Plugins\Fusion\OM\CheckedOutDocs\" + xLibrary
            If Not RegKeyExists(HKEY_CURRENT_USER, xRegKey) Then
                xTestLib = Replace(xLibrary, "_", " ")
                xRegKey = "Software\Hummingbird\PowerDOCS\Core\Plugins\Fusion\OM\CheckedOutDocs\" + xTestLib
                If RegKeyExists(HKEY_CURRENT_USER, xRegKey) Then
                    xLibrary = xTestLib
                End If
            End If
        End If
    Else
        Exit Function
    End If
    'MsgBox "Library=" & xLibrary & vbCrLf & "DocNum=" & xDocNum
    Set oDMApp = CreateObject("DOCSObjects.Application")
    'Get Document Security Token (DST) from active DM session using DM Extensions API's "Application" Object in order to use DM API calls
    xDST = oDMApp.DST
    'GLOG 5613
    Select Case UCase(xField)
        Case "CLIENT_ID", "MATTER_ID":
            'These don't exist in v_defprof, use default profile form instead
            On Error Resume Next
            xForm = oDMApp.CurrentLibrary.SelectProfileForm(0).FormName
            On Error GoTo ProcError
        Case Else
            'Use internal form for other fields
            xForm = "v_defprof"
    End Select
    If xForm = "" Then
        xForm = "LAWPROF"
    End If
    Set oDMApp = Nothing
    'MsgBox "DST=" & xDST
    
    'Gather profile information using DM API
    Set oPCDDoc = CreateObject("PCDClient.PCDDocObject")
    oPCDDoc.SetDST xDST
    oPCDDoc.SetObjectType xForm 'GLOG 5613
    ' Set library incase the document is from a remote library
    oPCDDoc.SetProperty "%TARGET_LIBRARY", xLibrary
    oPCDDoc.SetProperty "%OBJECT_IDENTIFIER", xDocNum
    oPCDDoc.Fetch
    
    GetProfileInfoPCD = oPCDDoc.GetReturnProperty(xField)
    Set oPCDDoc = Nothing
    'MsgBox "GetProfileInfoPCD='" & GetProfileInfoPCD & "'"
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CHummingbirdCOMProfile.GetProfileInfoPCD"
End Function
