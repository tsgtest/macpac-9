VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CCustomFields Class
'   created 9/27/01 by Daniel Fisherman
'   Contains properties and methods that
'   manage a CustomFields collection
'**********************************************************

Private m_oCol As Collection

Public Property Get Count() As Integer
    On Error GoTo ProcError
    Count = m_oCol.Count
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CCustomFields.Count"
    Exit Property
End Property

Public Function Item(ByVal vID As Variant) As mpDMS.CCustomField
    On Error GoTo ProcError
    Set Item = m_oCol.Item(vID)
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CCustomFields.Item"
    Exit Function
End Function

Friend Sub Add(oNew As mpDMS.CCustomField)
    On Error GoTo ProcError
    m_oCol.Add oNew, oNew.Name
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CCustomFields.Add"
    Exit Sub
End Sub

Private Sub Class_Initialize()
    Set m_oCol = New Collection
End Sub
