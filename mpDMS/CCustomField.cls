VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CCustomField Class
'   created 2/28/01 by Daniel Fisherman
'   Contains properties and methods that
'   define a CustomField
'**********************************************************
Private m_xName As String
Private m_xVal As String
'Private m_xVal As String

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Name(xNew As String)
    If Name <> xNew Then
        m_xName = xNew
    End If
End Property

Public Property Get Value() As Variant
Attribute Value.VB_UserMemId = 0
    Value = m_xVal
End Property

Public Property Let Value(xNew)
    m_xVal = xNew
End Property
