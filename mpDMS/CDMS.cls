VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum mpDMSs
    mpDMS_Windows = 0
    mpDMS_PCDOCS = 1
    mpDMS_IManage4x = 2
    mpDMS_IManage5x = 3
    mpDMS_WorldDox = 4
    mpDMS_Fusion = 5
    mpDMS_NetDocs = 6
    mpDMS_HummingbirdCOM = 7
    mpDMS_StarLaw = 8
    mpDMS_Prolaw = 9
    mpDMS_ndOffice = 10 'GLOG 5399: ndOffice integration
End Enum

Public Enum mpApps
    mpApp_Word = 0
    mpApp_Excel = 1
    mpApp_PPT = 2
End Enum

Public Event FileSaved()
Public Event FileOpened()
Public Event ProfileChanged()

Private m_iTypeID As mpDMSs
Private m_oProfile As IDMS
Private m_oCreator As IProfileCreator
Private WithEvents m_oEvents As CEventGenerator
Attribute m_oEvents.VB_VarHelpID = -1

Public Property Let TargetApplication(ByVal iNew As mpApps)
    g_iTargetApp = iNew
End Property

Public Property Get TargetApplication() As mpApps
    TargetApplication = g_iTargetApp
End Property

Public Property Let TypeID(iNew As mpDMSs)
    Dim bDMSExists As Boolean
    Dim oAddIn As Word.AddIn
'9.4.0/dm/#1742
    m_iTypeID = iNew
    Select Case m_iTypeID
        Case mpDMS_Windows
            Set m_oProfile = New CWindowsProfile
        Case mpDMS_PCDOCS
            Set m_oProfile = New CDocsOpenProfile
        Case mpDMS_IManage4x
        
            With Word.Application
                On Error Resume Next
                bDMSExists = .AddIns("iMan80.dot").Installed
                If Not bDMSExists Then
                    bDMSExists = .AddIns("iMan80s.dot").Installed
                End If
                On Error GoTo 0
            End With
            If bDMSExists Then
                Set m_oProfile = New mpDMS.CiManageProfile
            Else
                Set m_oProfile = New CWindowsProfile
            End If
            
        Case mpDMS_IManage5x
            On Error Resume Next
            Set m_oProfile = New mpDMS.CiManage5xProfile
            If Err.Number = mpError_InvalidMember Then
                On Error GoTo 0
                ' iManage COM Add-in is not loaded
                Set m_oProfile = New CWindowsProfile
            End If
        
        Case mpDMS_WorldDox
            Select Case g_iTargetApp
                Case mpApp_Word
                    'v9.8.1010
                    With Word.Application
                        For Each oAddIn In .AddIns
                            If (UCase(oAddIn.Name) Like "@WD*") Then 'GLOG 5532
                                On Error Resume Next
                                bDMSExists = .AddIns(oAddIn.Name).Installed
                                On Error GoTo 0
                                If bDMSExists Then
                                    Exit For
                                End If
                            End If
                        Next oAddIn
                        
        
                    End With
                Case mpApp_Excel
                    Dim oExcel As Object
                    Dim o As Object
                    Set oExcel = GetExcelObject()
                    If Val(oExcel.Version) > 12 Then	'GLOG : 5638 : ceh
                        With oExcel
                            For Each o In .AddIns2
                                If UCase(o.Name) Like "@WD*" Then 'GLOG 5532
                                    bDMSExists = True
                                    Exit For
                                End If
                            Next o
                        End With
                    Else
                        On Error Resume Next
                        Dim oWB As Object
                        Set oWB = oExcel.Workbooks("@wdexclc.xlam")
			'GLOG 5532
                        If (oWB Is Nothing) Then
                            oWB = oExcel.Workbooks("@wdbasicc.xla")
                        End If
                        If (oWB Is Nothing) Then
                            oWB = oExcel.Workbooks("@wdexclb.xla")
                        End If
                        If (oWB Is Nothing) Then
                            bDMSExists = False
                        Else
                            bDMSExists = True
                        End If
                    End If
                Case mpApp_PPT
                    bDMSExists = True
            End Select
            If bDMSExists Then
                Set m_oProfile = New mpDMS.CWorldoxProfile
            Else
                Set m_oProfile = New CWindowsProfile
            End If
        Case mpDMS_Fusion
            Set m_oProfile = New mpDMS.CFusionProfile
        Case mpDMS_NetDocs
            Set m_oProfile = New mpDMS.CNetDocsProfile
        Case mpDMS_HummingbirdCOM
            On Error Resume Next
            Set m_oProfile = New mpDMS.CHummingbirdCOMProfile
            If Err.Number = mpError_InvalidMember Then
                On Error GoTo 0
                ' Hummingbird COM Add-in is not loaded
                Set m_oProfile = New CWindowsProfile
            End If
        Case mpDMS_Prolaw
            On Error Resume Next
            Set m_oProfile = New mpDMS.cProlawProfile
            If Err.Number = mpError_InvalidMember Then
                On Error GoTo 0
                ' Prolaw is not running
                Set m_oProfile = New CWindowsProfile
            End If
        Case mpDMS_ndOffice 'GLOG 5399
            On Error Resume Next
            Set m_oProfile = New mpDMS.CndOfficeProfile
            If Err.Number = mpError_InvalidMember Then
                On Error GoTo 0
                ' ndOffice COM Add-in is not loaded
                Set m_oProfile = New CWindowsProfile
            End If
        Case Else
    End Select

'   set creator var = to new profile object
    Set m_oCreator = m_oProfile
    
'   set event generator to event backend generator
    Set m_oEvents = m_oProfile.EventGenerator
End Property

Public Property Get TypeID() As mpDMSs
    TypeID = m_iTypeID
End Property

Public Function Profile() As IDMS
    Set Profile = m_oProfile
End Function

Public Function Creator() As IProfileCreator
    Set Creator = m_oCreator
End Function

Public Sub FileOpen()
    m_oProfile.MainInterface.FileOpen
End Sub

Public Sub FileSave()
    m_oProfile.MainInterface.FileSave
End Sub

Public Sub FileSaveAs()
    m_oProfile.MainInterface.FileSaveAs
End Sub

Public Sub FileClose()
    m_oProfile.MainInterface.FileClose
End Sub

Public Sub DocClose()
    m_oProfile.MainInterface.DocClose
End Sub

Private Sub m_oEvents_FileOpened()
    RaiseEvent FileOpened
End Sub

Private Sub m_oEvents_FileSaved()
    RaiseEvent FileSaved
End Sub

Private Sub m_oEvents_ProfileChanged()
    RaiseEvent ProfileChanged
End Sub


