VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFusionProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"No"
Attribute VB_Ext_KEY = "Member0" ,"clsDMSDocsOpen"
'**********************************************************
'   CFusionProfile Class
'   created 2/27/01 by Daniel Fisherman

'   Contains properties and methods to
'   access Fusion Profile information

'   Member of
'   Container for
'   Implements IDMS
'**********************************************************
Option Explicit

Const mpFusionProfile_DOCNUMBER As String = "DOCNUM"
Const mpFusionProfile_DOCNAME As String = "DOCNAME"
Const mpFusionProfile_AUTHORUSERID As String = "AUTHOR_ID"
Const mpFusionProfile_AUTHORFULLNAME As String = "AUTHOR_FULL_NAME"
Const mpFusionProfile_TYPISTUSERID As String = "TYPIST_ID"
Const mpFusionProfile_DOCTYPEID As String = "TYPE_ID"
Const mpFusionProfile_DOCTYPEDESC As String = "DOCUMENTTYPE"
Const mpFusionProfile_CREATIONDATE As String = "CREATION_DATE"
Const mpFusionProfile_LASTEDITDATE As String = "LASTEDITDATE"
Const mpFusionProfile_ABSTRACT As String = "ABSTRACT"
Const mpFusionProfile_MATTERID As String = "MATTER_ID"
Const mpFusionProfile_CLIENTID As String = "CLIENT_ID"

Const xIniSection As String = "PowerDocs"

Private WithEvents m_oEvent As CEventGenerator
Attribute m_oEvent.VB_VarHelpID = -1

Implements IDMS
Implements IProfileCreator

Private m_vAuthorID As Variant
Private m_vLibraryID As Variant
Private m_vClientID As Variant
Private m_vMatterID As Variant
Private m_xDocName As String
Private m_vDocTypeID As Variant
Private m_vTypistID As Variant
Private m_oCustProps As Collection
Private m_xFileNameSep As String
Private m_bAddSecurity As Boolean
Private m_xDST As String
Private m_xDeptID As String
Private m_oCustFields As CCustomFields

'***Class events***
Private Sub Class_Initialize()
    On Error Resume Next
    Set m_oEvent = New CEventGenerator
End Sub
Private Sub Class_Terminate()
    Set m_oEvent = Nothing
End Sub

'**************************************************
'PRIVATE PROPERTIES
'**************************************************
Private Property Get FileNameSeparator() As String
    If m_xFileNameSep = Empty Then
        m_xFileNameSep = mpBase2.GetIni(xIniSection, _
                                        "FileNameSeparator", _
                                        mpBase2.ApplicationDirectory & g_xIni)
        If m_xFileNameSep = Empty Then
            Err.Raise mpError.mpError_NullValueNotAllowed, , _
                "Missing or empty PowerDocs\FileNameSeparator key in '" & mpBase2.ApplicationDirectory & "\mpDMS.ini'."
        End If
    End If
    FileNameSeparator = m_xFileNameSep
End Property

Private Property Get GetLibrary() As String
    Dim xElements() As String
    
    On Error GoTo ProcError
    xElements = Split(Word.ActiveDocument.FullName, "\")
    GetLibrary = xElements(2)
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CFusionProfile.GetLibrary"
End Property

'***************************************************
'IDMS Interface
'***************************************************
Private Property Get IDMS_AuthorID() As String
    IDMS_AuthorID = GetProfileInfo(mpFusionProfile_AUTHORUSERID)
End Property
Private Property Get IDMS_ClientID() As String
    IDMS_ClientID = GetProfileInfo(mpFusionProfile_CLIENTID)
End Property
Private Property Get IDMS_CreationDate() As String
    IDMS_CreationDate = GetProfileInfo(mpFusionProfile_CREATIONDATE)
End Property

Private Property Get IDMS_Custom1() As String
    If IDMS_CustomFields.Count > 0 Then
        IDMS_Custom1 = GetProfileInfo(IDMS_CustomFields.Item(1).Value)
    End If
End Property
Private Property Get IDMS_Custom2() As String
    If IDMS_CustomFields.Count > 1 Then
        IDMS_Custom2 = GetProfileInfo(IDMS_CustomFields.Item(2).Value)
    End If
End Property
Private Property Get IDMS_Custom3() As String
    If IDMS_CustomFields.Count > 2 Then
        IDMS_Custom3 = GetProfileInfo(IDMS_CustomFields.Item(3).Value)
    End If
End Property
Private Function IDMS_CustomFields() As CCustomFields
    
    If m_oCustFields Is Nothing Then
        LoadCustomFields m_oCustFields, xIniSection
    End If
    Set IDMS_CustomFields = m_oCustFields
End Function

Private Property Get IDMS_DocName() As String
    IDMS_DocName = GetProfileInfo(mpFusionProfile_DOCNAME)
End Property
Private Property Get IDMS_DocNumber() As String
    Dim xElements() As String
    
    On Error GoTo ProcError
    If InStr(ActiveDocument.FullName, "ODMA") Then
        xElements = Split(Word.ActiveDocument.FullName, "\")
        IDMS_DocNumber = xElements(3)
    End If
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CFusionProfile.IDMS_DocNumber"
End Property
Private Property Get IDMS_DocTypeID() As String
    IDMS_DocTypeID = GetProfileInfo(mpFusionProfile_DOCTYPEID)
End Property
Private Property Get IDMS_EventGenerator() As CEventGenerator
    Set IDMS_EventGenerator = m_oEvent
End Property
Private Property Get IDMS_Library() As String
    Dim xElements() As String
    
    On Error GoTo ProcError
    xElements = Split(Word.ActiveDocument.FullName, "\")
    IDMS_Library = xElements(2)
    Exit Property
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CFusionProfile.IDMS_Library"
End Property
Private Function IDMS_MainInterface() As Object
    Dim oRet As CFusionProfile
    Set oRet = Me
    Set IDMS_MainInterface = oRet
End Function
Private Property Get IDMS_MatterID() As String
    IDMS_MatterID = GetProfileInfo(mpFusionProfile_MATTERID)
End Property
Private Property Get IDMS_RevisionDate() As String
    IDMS_RevisionDate = GetProfileInfo(mpFusionProfile_LASTEDITDATE)
End Property
Private Property Get IDMS_TypistID() As String
    IDMS_TypistID = GetProfileInfo(mpFusionProfile_TYPISTUSERID)
End Property
Private Property Get IDMS_AuthorName() As String
    IDMS_AuthorName = GetProfileInfo(mpFusionProfile_AUTHORFULLNAME)
End Property
Private Property Get IDMS_TypistName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CFusionProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_ClientName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CFusionProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_MatterName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CFusionProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_DocTypeDescription() As String
    IDMS_DocTypeDescription = GetProfileInfo(mpFusionProfile_DOCTYPEDESC)
End Property
Private Property Get IDMS_Abstract() As String
    IDMS_Abstract = GetProfileInfo(mpFusionProfile_ABSTRACT)
End Property
Private Property Get IDMS_GroupName() As String
    Err.Raise E_NOTIMPL, "mpDMS.CFusionProfile", g_oError.Desc(E_NOTIMPL)
End Property
Private Property Get IDMS_Path() As String
    If ActiveDocument Is Nothing Then Exit Property
    IDMS_Path = Word.WordBasic.FileNameInfo$(Word.WordBasic.FileName$(), 5)
End Property
Private Property Get IDMS_FileName() As String
    If ActiveDocument Is Nothing Then Exit Property
    IDMS_FileName = Word.WordBasic.FileNameInfo$(Word.WordBasic.FileName$(), 3)
End Property

Private Property Get IDMS_Version() As String
    Dim iDot As Integer
    Dim xVersion As String
    Dim I As Integer
    
    On Error GoTo ProcError
    If ActiveDocument Is Nothing Then Exit Property
    
    xVersion = ActiveDocument.FullName
    If InStr(xVersion, "ODMA") Then
        Dim xElements() As String
    
        xElements = Split(Word.ActiveDocument.FullName, "\")
        IDMS_Version = xElements(4)
        Exit Property
    End If
    
    If InStr(xVersion, "!") Then '---docs configured w/ app extensions
        iDot = InStr(xVersion, ".")

        If iDot Then
            xVersion = Left(xVersion, iDot - 1)
            xVersion = Right(xVersion, 3)
        End If

        If Right(xVersion, 1) = "!" Then _
            xVersion = Left(xVersion, Len(xVersion) - 1)
            
        If Left(xVersion, 1) = "0" Then _
            xVersion = Right(xVersion, Len(xVersion) - 1)
    Else
        iDot = InStr(xVersion, ".")
        If iDot Then
            xVersion = Right(xVersion, Len(xVersion) - iDot + 1)
        Else
            xVersion = ""
        End If
    End If
    IDMS_Version = xVersion
    Exit Property
    
ProcError:
    Err.Raise Err.Number, "mpDMS.CFusionProfile.Version", Err.Description
End Property
'***Methods***
Private Function IDMS_GetProfileInfo(vItem As Variant) As String
    IDMS_GetProfileInfo = GetProfileInfo(vItem)
End Function
Private Sub IDMS_FileSave()
    FileSave
End Sub
Private Sub IDMS_FileSaveAs()
    FileSaveAs
End Sub
Private Sub IDMS_DocClose()
    DocClose
End Sub
Private Sub IDMS_FileClose()
    FileClose
End Sub

'***************************************************
'Main Interface
'***************************************************

Public Function GetProfileInfo(ByVal FieldName As String) As String
    Dim pcdSrch As Object
    Dim oPDApp As Object
    Dim xDocNum As String
    Dim xElements() As String
    Dim oSearch As PCDCLIENTLib.PCDSearch
    Dim oLogin As PCDLogin

'   get dst number of current power docs user
    Set oPDApp = GetObject("", "DOCSObjects.Application")
    Dim xDST As String
    xDST = oPDApp.DST

    If xDST = Empty Or UCase(xDST) = "UNPLUGGED" Then
'       not connected to fusion server - return empty
        Exit Function
    Else
'       Retrieve data from connected fusion server
        On Error GoTo ProcError
        If FileNameSeparator = Empty Then
            Err.Raise mpError_InvalidParameter, , _
                "PowerDocs\FileNameSeparator key is missing from " & mpBase2.ApplicationDirectory & g_xIni
        End If
        
        Dim iPos1 As Integer
        Dim iPos2 As Integer
        Dim xFile As String
        
        xFile = Word.ActiveDocument.Name
        
        'search for beginning of doc number in file name
        iPos1 = InStr(xFile, FileNameSeparator & "#")
        If iPos1 > 0 Then
            'found start of doc ID string - typically found in DM5 - get end
            iPos2 = InStr(iPos1 + 1, xFile, FileNameSeparator)
            If iPos2 > 0 Then
                xDocNum = Mid$(xFile, iPos1 + 2, iPos2 - (iPos1 + 2))
            Else
                xDocNum = Mid$(xFile, iPos1 + 2)
            End If
        Else
            'file name doesn't have that form - typically
            'this will happen when using fusion
            xElements = Split(Word.ActiveDocument.FullName, FileNameSeparator)
            xDocNum = xElements(3)
        End If
        
        Set oLogin = New PCDCLIENTLib.PCDLogin
        Set oSearch = New PCDCLIENTLib.PCDSearch
        With oSearch
            .SetDST xDST
            .AddSearchLib GetLibrary
            .SetSearchObject "CYD_LAWPROF"
            .AddSearchCriteria "DOCNUM", xDocNum
            .AddReturnProperty FieldName
            .Execute
    
            If .ErrNumber = 0 Then
                .NextRow
                GetProfileInfo = .GetPropertyValue(FieldName)
            Else
                Err.Raise .ErrNumber, , .ErrDescription
            End If
      
            .ReleaseResults
      End With
    End If
    
    Set pcdSrch = Nothing
    Exit Function
    
ProcError:
    Err.Raise Err.Number, "mpDMS.CFusionProfile.GetProfileInfo"
End Function

'Public Function GetProfileInfo(ByVal FieldName As String) As String
'    Dim pcdSrch As Object
'    Dim oPDApp As Object
'    Dim xDocNum As String
'    Dim xElements() As String
'    Dim oSearch As PCDCLIENTLib.PCDSearch
'
''   get dst number of current power docs user
'    Set oPDApp = GetObject("", "DOCSObjects.Application")
'    Dim xDST As String
'    xDST = oPDApp.DST
'
'    If xDST = Empty Or UCase(xDST) = "UNPLUGGED" Then
''       not connected to fusion server - return empty
'        Exit Function
'    Else
''       Retrieve data from connected fusion server
'        On Error GoTo ProcError
'        If FileNameSeparator = Empty Then
'            Err.Raise mpError_InvalidParameter, , _
'                "PowerDocs\FileNameSeparator key is missing from " & mpbase2.applicationDirectory & g_xIni
'        End If
'        xElements = Split(Word.ActiveDocument.FullName, FileNameSeparator)
'        xDocNum = xElements(3)
'
'        Set oSearch = New PCDCLIENTLib.PCDSearch
'        With oSearch
'            .SetDST xDST
'            .AddSearchLib xElements(2)
'            .SetSearchObject "CYD_LAWPROF"
'            .AddSearchCriteria "DOCNUM", xDocNum
'            .AddReturnProperty FieldName
'            .Execute
'
'            If .ErrNumber = 0 Then
'                .NextRow
'                GetProfileInfo = .GetPropertyValue(FieldName)
'            Else
'                Err.Raise .ErrNumber, , .ErrDescription
'            End If
'
'            .ReleaseResults
'      End With
'    End If
'
'    Set pcdSrch = Nothing
'    Exit Function
'
'ProcError:
'    Err.Raise Err.Number, "mpDMS.CFusionProfile.GetProfileInfo"
'End Function

Public Sub FileSaveAs()
    
    Dim xCurFile As String
    Dim lRet As Long
    Dim lErr As Long
    
    On Error GoTo FileSaveAs_Error
    
    If Documents.Count = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
    Dim xOrig As String
    xOrig = Word.ActiveDocument.FullName
    On Error Resume Next
    lRet = Word.Dialogs(wdDialogFileSaveAs).Show
    '  Ignore error Word generates when attempting to save over read-only file
    lErr = Err.Number
    On Error GoTo FileSaveAs_Error
    If lErr = 5155 Then
        ' This error will be generated even if filename is corrected and document saved
        If Word.ActiveDocument.Saved And (xOrig <> Word.ActiveDocument.FullName) Then
            lRet = -1
        End If
    ElseIf lErr <> 0 Then
        Err.Raise lErr
    End If
    If lRet Then
        m_oEvent.RaiseFileSavedEvent
    End If
    Exit Sub
    
FileSaveAs_Error:
    Err.Raise Err.Number, "mpDMS.CFusionProfile.FileSaveAs", Err.Description
End Sub

Public Sub FileSave()
    Dim iFormat As Integer
    Dim xFormat As String
    Dim xMsg As String
    Dim iRet As Integer
    Dim xCurFile As String
    Dim xTempFile As String
    Dim conv As FileConverter
    
    On Error GoTo FileSave_Error
    
    With ActiveDocument
        On Error Resume Next
        
        '9.7.1 #4233 - avoid error 5155 for read-only docs
        If .ReadOnly Then
            FileSaveAs
            Exit Sub
        End If
        
        iFormat = .SaveFormat
        
        'Handle native Word formats
        If iFormat = 0 Or iFormat = 1 Or (iFormat >= 12 And iFormat <= 16) Then
            .Save
        Else
'---        non word format, prompt as in Native Word
            If Not .Saved Then

'---            since open/save converters are shuffled together
'               in collection we need to look only at save converters
'---            to display format name correctly in dbox
                Select Case iFormat
                    Case wdFormatText
                        xFormat = "Text Only"
                    Case wdFormatTextLineBreaks
                        xFormat = "Text Only with Line Breaks"
                    Case wdFormatRTF
                        xFormat = "Rich Text Format"
                    Case wdFormatUnicodeText
                        xFormat = "Unicode Text"
                    Case wdFormatDOSText
                        xFormat = "MS-DOS Text"
                    Case wdFormatDOSTextLineBreaks
                        xFormat = "MS-DOS Text with Line Breaks"
                    Case Else
                        For Each conv In FileConverters
                            If conv.CanSave Then
                                If conv.SaveFormat = iFormat Then
                                    xFormat = conv.FormatName
                                End If
                            End If
                        Next conv
                End Select
                
                If xFormat = Empty Then _
                    xFormat = "unknown"
                    
                If iFormat > 1 And (iFormat < 12 Or iFormat > 16) Then
                    Dim xNewFormat As String
                    xNewFormat = GetWordVersionString()
                    xMsg = "This document may contain formatting which " & _
                           "will be lost upon conversion to " & xFormat & _
                           " format.  " & "Click No to save the document in " & _
                            xNewFormat & " format " & _
                           "and preserve the current formatting." & _
                           vbLf & vbLf & "Continue with save in " & _
                           xFormat & " format?"

                    iRet = MsgBox(xMsg, vbQuestion + vbYesNoCancel)
                    Select Case iRet
                        Case vbYes
'                           save in non word format
                            .Save
                            
                        Case vbNo
                            xCurFile = ActiveDocument.FullName
                            
    '                       convert to Word Format when saving
                            If Val(Word.Application.Version) > 11 Then
                                xTempFile = "c:\tempsave.docx"
                            Else
                                xTempFile = "c:\tempsave.doc"
                            End If
                            
'---                        save to temp file in Word format
                            ActiveDocument.SaveAs xTempFile, 0
                    
'---                        save back to original filename
                            ActiveDocument.SaveAs xCurFile, 0
                                
                            On Error Resume Next
                            Kill xTempFile
                            
                        Case Else
                            Exit Sub
                    End Select
                Else
                    .Save
                End If
            End If
        End If
        
        If .Saved Then
            m_oEvent.RaiseFileSavedEvent
        End If
    End With
    
    Exit Sub
    
FileSave_Error:
    Err.Raise Err.Number, "mpDMS.CFusionProfile.FileSave", Err.Description
End Sub

Public Sub FileClose()
'close doc, run autotrailer-
'autotrailer runs trailer if
'filename has changed as a result
'of the close
    Dim iUserChoice As Integer
    
    On Error GoTo ProcError
    
    If Documents.Count = 0 Then
        MsgBox mpNoOpenDocument, vbInformation, App.Title
        Exit Sub
    End If
    
'   the following is necessary so that
'   trailer will run when appropriate -
'   if we just run the native command,
'   we can't run trailer at all
    With ActiveDocument
        If Not .Saved Then

'           prompt for doc save
            xMsg = "Do you want to save the changes you made to " & .Name & "?"
            iUserChoice = MsgBox(xMsg, _
                                 vbExclamation + vbYesNoCancel, _
                                 "Microsoft Word")
            Select Case iUserChoice
                Case vbYes
                    FileSave
                    If .Saved Then
                        .Close
                    End If
                Case vbNo
                    .Saved = True
                    .Close
                Case Else
            End Select
        Else
            .Close
        End If
    End With
    
'   code below resets pointer,
'   which seems to remain as an hourglass
'   if the user cancels out of save
    Application.ScreenRefresh
    Exit Sub
    
ProcError:
    Err.Raise Err.Number, "mpDMS.CFusionProfile.FileClose", Err.Description
End Sub

Public Sub FileOpen()
    MsgBox "mpDMS.CFusionProfile.FileOpen is not built."
    m_oEvent.RaiseFileOpenedEvent
End Sub

Public Sub DocClose()
'   close active window if doc has
'   more than one window, else close
'   file - (this mimics Native DOC CLOSE)

    On Error Resume Next
    If Application.Windows.Count = 0 Then _
        Exit Sub
    
    If ActiveDocument.Windows.Count > 1 Then
        If Word.ActiveDocument.ActiveWindow.View = wdPrintPreview Then
            ActiveDocument.ClosePrintPreview
        Else
            ActiveDocument.ActiveWindow.Close
        End If
    Else
        FileClose
    End If
    Exit Sub
    
ProcError:
    Err.Raise Err.Number, "mpDMS.CFusionProfile.DocClose", Err.Description
End Sub

Private Property Let IProfileCreator_AddSecurity(RHS As Boolean)
    m_bAddSecurity = RHS
End Property

Private Property Get IProfileCreator_AddSecurity() As Boolean
    IProfileCreator_AddSecurity = m_bAddSecurity
End Property

'***************************************************
'IProfileCreator Interface
'***************************************************
Private Property Let IProfileCreator_AuthorID(RHS As Variant)
    m_vAuthorID = RHS
End Property

Private Property Get IProfileCreator_AuthorID() As Variant
    IProfileCreator_AuthorID = m_vAuthorID
End Property

Private Property Let IProfileCreator_ClientID(RHS As Variant)
    m_vClientID = RHS
End Property

Private Property Get IProfileCreator_ClientID() As Variant
    IProfileCreator_ClientID = m_vClientID
End Property

Private Function IProfileCreator_CreateProfile() As Variant
    Dim o As UPFusion.WordDoc
    Dim oCustFld As mpDMS.CCustomField
    Dim oPDApp As Object
    Dim xDST As String
    
    On Error GoTo ProcError
    Set o = New UPFusion.WordDoc

'   get dst number of current power docs user
    Set oPDApp = GetObject("", "DOCSObjects.Application")
    xDST = oPDApp.DST

    If xDST = Empty Or UCase(xDST) = "UNPLUGGED" Then
'       not connected to fusion server - return empty
        Exit Function
    Else
        o.DST = xDST
        o.LibraryName = IProfileCreator_Library
        o.Typist = IProfileCreator_TypistID
        IProfileCreator_CreateProfile = o.CreateProfile(IProfileCreator_ClientID, _
                        IProfileCreator_MatterID, _
                        IProfileCreator_DocTypeID, _
                        IProfileCreator_DocName, _
                        Word.ActiveDocument, _
                        IProfileCreator_AddSecurity, _
                        IProfileCreator_AuthorID)
    End If
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CFusionProfile.IProfileCreator_CreateProfile"
End Function

Private Function IProfileCreator_CustomProperties() As Collection
    Dim xFld As String
    Dim oCustFld As mpDMS.CCustomField
    Dim I As Integer
    
    On Error GoTo ProcError
    
    If m_oCustProps Is Nothing Then
'       get custom prop fields from dms.ini
        Set m_oCustProps = New Collection
        
'       get first key in ini
        I = 1
        xFld = GetIni("Custom Fields", CStr(I), mpBase2.ApplicationDirectory & g_xIni)
        
'       add custom properties for all ini keys
        While Len(xFld)
            Set oCustFld = New mpDMS.CCustomField
            oCustFld.Name = xFld
            m_oCustProps.Add oCustFld, oCustFld.Name

'           get next key in ini
            I = I + 1
            xFld = GetIni("Custom Fields", CStr(I), mpBase2.ApplicationDirectory & g_xIni)
        Wend
    End If
    Set IProfileCreator_CustomProperties = m_oCustProps
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "mpDMS.CFusionProfile.IProfileCreator_CustomProperties"
End Function

Private Property Get IProfileCreator_DocName() As String
    IProfileCreator_DocName = m_xDocName
End Property

Private Property Let IProfileCreator_DocName(RHS As String)
    m_xDocName = RHS
End Property

Private Property Let IProfileCreator_DocTypeID(RHS As Variant)
    m_vDocTypeID = RHS
End Property

Private Property Get IProfileCreator_DocTypeID() As Variant
    IProfileCreator_DocTypeID = m_vDocTypeID
End Property

Private Property Let IProfileCreator_Library(RHS As Variant)
    m_vLibraryID = RHS
End Property

Private Property Get IProfileCreator_Library() As Variant
    IProfileCreator_Library = m_vLibraryID
End Property

Private Property Let IProfileCreator_MatterID(RHS As Variant)
    m_vMatterID = RHS
End Property

Private Property Get IProfileCreator_MatterID() As Variant
    IProfileCreator_MatterID = m_vMatterID
End Property

Private Property Let IProfileCreator_TypistID(RHS As Variant)
    m_vTypistID = RHS
End Property

Private Property Get IProfileCreator_TypistID() As Variant
    IProfileCreator_TypistID = m_vTypistID
End Property

Private Property Let IProfileCreator_DepartmentID(RHS As String)
    m_xDeptID = RHS
End Property

Private Property Get IProfileCreator_DepartmentID() As String
    IProfileCreator_DepartmentID = m_xDeptID
End Property

Private Property Let IProfileCreator_SecurityCode(RHS As String)
    m_xDST = RHS
End Property

Private Property Get IProfileCreator_SecurityCode() As String
    IProfileCreator_SecurityCode = m_xDST
End Property

