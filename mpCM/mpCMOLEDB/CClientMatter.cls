VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CClientMatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   mpCMOLEDB.ClientMatter Class
'   created 12/21/01 by Daniel Fisherman
'   Client/Matter lookup integration for OLE DB backends
'**********************************************************

Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long
    
Enum mpCMOLEDB_Errors
    Error_InvalidParameter = vbError + 512 + 1
End Enum
    
Private Const mpCMAppName As String = "Tab Delimited Text"

Private m_bInit As Boolean
Private m_oCnn As ADODB.Connection
Private m_xConnectionString As String
Private m_xIni As String
    
Implements CMO.IClientMatter

Public Property Let ConnectionString(xNew As String)
    m_xConnectionString = xNew
End Property

Public Property Get ConnectionString() As String
    ConnectionString = m_xConnectionString
End Property

Private Sub Class_Initialize()
    m_xIni = mpBase2.ApplicationDirectory & "\mpCMOLEDB.ini"
End Sub

Private Sub OpenConnection()
'Creates a ADO connection to a data source
'specified in the ini
    Dim xCnnString As String
    Dim xUID As String
    Dim xPWD As String
    Dim xDesc As String
            
    On Error GoTo ProcError
    
    If Me.ConnectionString = Empty Then
'       get connection string from mpCMOLEDB.ini
        xCnnString = GetIni("Connection", "String", m_xIni)
        xUID = GetIni("Connection", "UID", m_xIni)
        xPWD = GetIni("Connection", "PWD", m_xIni)
        
'       decrypts account info if encrypted
'        xCnnString = Replace(xCnnString, "<UID>", DecryptText(xUID))
'        xCnnString = Replace(xCnnString, "<PWD>", DecryptText(xPWD))

'       set property
        Me.ConnectionString = xCnnString
    End If
        
'   create connection object
    Set m_oCnn = New ADODB.Connection
    With m_oCnn
        .CursorLocation = adUseClient
        .ConnectionString = Me.ConnectionString
        
        Dim iTimeout As Integer
        
'       get timeout from ini
        On Error Resume Next
        iTimeout = GetIni("Connection", "Timeout", mpBase2.ApplicationDirectory & "\mpCMOLEDB.ini")
        On Error GoTo ProcError
        
        If iTimeout = 0 Then
'           use default timeout
            iTimeout = 5
        End If
        
        .ConnectionTimeout = iTimeout
'       open connection
        On Error Resume Next
        .Open
        If Err.Number Then
            xDesc = Err.Description
            
            On Error GoTo ProcError
'           could not create connection - alert
            Err.Raise Error_InvalidParameter, , _
                "Could not connect to the client matter data source.  " & _
                "The following message was returned by the OLE Provider: " & xDesc
        Else
            On Error GoTo ProcError
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.OpenConnection"
    Exit Sub
End Sub

Private Sub CloseConnection()
'closes the ADO connection to the data source
    On Error GoTo ProcError
    If Not m_oCnn Is Nothing Then _
        m_oCnn.Close
    Set m_oCnn = Nothing
    Exit Sub
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.CloseConnection"
    Exit Sub
End Sub

Public Function IClientMatter_Clients(Optional ByVal ClientFilter As Variant, _
                                      Optional ByVal AdditionalFilter As String, _
                                      Optional ByVal SortBy As cmSortBy) As CMO.CClients
'returns a populated CClients collection object
    Dim oRS As ADODB.Recordset
    Dim oClients As CMO.CClients
    Static xClientsSQL As String
    Static xClientIDField As String
    Static xClientNameField As String
    Dim xSQL As String
    Dim bForceIDSearch As Boolean
    
'   connect if necessary
    On Error GoTo ProcError
    If m_oCnn Is Nothing Then
        OpenConnection
    End If
    
    Set oClients = New CMO.CClients
    

    If xClientsSQL = Empty Then
'       get Clients sql statement
        xClientsSQL = GetIni("SQL", "Clients1", m_xIni) & " " & _
            GetIni("SQL", "Clients2", m_xIni)
        
'       raise error if missing
        If Trim(xClientsSQL) = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/Clients' is missing or invalid.'"
        End If
    End If
    
    If xClientIDField = Empty Then
'       get Client ID field
        xClientIDField = GetIni("SQL", "ClientIDField", m_xIni)

'       raise error if missing
        If xClientIDField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/ClientIDField' is missing or invalid.'"
        End If
    End If
    
    If xClientNameField = Empty Then
'       get Client Name field
        xClientNameField = GetIni("SQL", "ClientNameField", m_xIni)

'       raise error if missing
        If xClientNameField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/ClientNameField' is missing or invalid.'"
        End If
    End If
    
ForceIDSearch:
    xSQL = xClientsSQL
    
    If IsMissing(ClientFilter) Then
        ClientFilter = Empty
    End If
    
    If IsMissing(AdditionalFilter) Then
        AdditionalFilter = Empty
    End If
    
    If Not (ClientFilter = Empty And AdditionalFilter = Empty) Then
'       add WHERE or AND, if necessary
        If InStr(UCase(xSQL), "WHERE") = 0 Then
            xSQL = xSQL & " WHERE "
        Else
            xSQL = xSQL & " AND "
        End If
        
        If AdditionalFilter <> Empty Then
'           add additional filter to WHERE clause
            xSQL = xSQL & AdditionalFilter
        End If
        
        If ClientFilter <> Empty Then
            If AdditionalFilter <> Empty Then
'               add operator
                xSQL = xSQL & " AND "
            End If
            
'           ClientFilter based on ID or Name
            If IsNumeric(ClientFilter) Or bForceIDSearch Then
'               ID has been supplied - get client with specified ID
                '9.7.1 - match start of string
                xSQL = xSQL & xClientIDField & " LIKE '" & ClientFilter & "%'"
                bForceIDSearch = False
            Else
'               Name has been supplied - get all clients with similar name
                xSQL = xSQL & xClientNameField & " LIKE '%" & ClientFilter & "%'"
                bForceIDSearch = True
            End If
        End If
    End If
    
'   sort appropriately
    If SortBy = cmSortBy_Name Then
        xSQL = xSQL & " ORDER BY " & xClientNameField
    Else
        xSQL = xSQL & " ORDER BY " & xClientIDField
    End If
    
    Set oRS = New ADODB.Recordset
    
'   open recordset
    On Error Resume Next
    oRS.Open xSQL, m_oCnn, adOpenForwardOnly
    
    If Err Then
'       something wrong - alert - probably SQL statement
        On Error GoTo ProcError
        Err.Raise Error_InvalidParameter, , _
        "Could not get clients. The SQL statement '" & xClientsSQL & _
        "' may be invalid."
    Else
        On Error GoTo ProcError
    End If
    
    With oRS
        If Not .EOF And Not .BOF Then
'           Load recordset into XArray
            oClients.ListArray.LoadRows .GetRows(80000)
            .Close
        ElseIf bForceIDSearch Then
'           try again but search id instead
            GoTo ForceIDSearch
        Else
            oClients.ListArray.ReDim 0, -1, 0, 1
        End If
    End With
    
    Set oRS = Nothing
    Set IClientMatter_Clients = oClients
    Exit Function
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.IClientMatter_Clients"
    Exit Function
End Function

Private Function IClientMatter_IsConnected() As Boolean
'returns TRUE if connection exists to data source
    On Error GoTo ProcError
    If m_oCnn Is Nothing Then
        On Error Resume Next
        OpenConnection
        'On Error GoTo ProcError
    End If
    IClientMatter_IsConnected = m_oCnn.State = adStateOpen
    Exit Function
ProcError:
    RaiseError "mpCMOLEDB.CClientMatter.IClientMatter_IsConnected"
    Exit Function
End Function

Private Function IClientMatter_IsValid(ByVal ClientID As Variant, Optional ByVal MatterID As Variant) As Boolean
'returns a populated CMatters collection object
    Dim oClients As CMO.CClients
    Dim oMatters As CMO.CMatters
    Dim oMatter As CMO.CMatter

    On Error GoTo ProcError
    On Error GoTo ProcError
    If IsMissing(MatterID) Then
'       check for client only
        Set oClients = IClientMatter_Clients(ClientID)
        On Error Resume Next
        IClientMatter_IsValid = Not (oClients.Item(ClientID) Is Nothing)
        On Error GoTo ProcError
    Else
'       check for matters
        Set oMatters = IClientMatter_Matters(ClientID)

        On Error Resume Next
        Set oMatter = oMatters.ItemFromID(MatterID)
        On Error GoTo ProcError

        IClientMatter_IsValid = Not oMatter Is Nothing
    End If
    Exit Function
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.IClientMatter_IsValid"
    Exit Function
End Function

Private Function IClientMatter_Matters(Optional ByVal ClientID As Variant, _
                                       Optional ByVal AdditionalFilter As String, _
                                       Optional ByVal SortBy As CMO.cmSortBy) As CMO.CMatters
    Dim oRS As ADODB.Recordset
    Dim oMatters As CMO.CMatters
    Dim xSQL As String
    Dim lCount As Long
    Dim xMattersClientIDField As String
    Dim xOrderField As String
    
    On Error GoTo ProcError
    
'   connect if necessary
    If m_oCnn Is Nothing Then
        OpenConnection
    End If

    Set oMatters = New CMO.CMatters

    xSQL = GetIni("SQL", "Matters1", m_xIni) & " " & _
        GetIni("SQL", "Matters2", m_xIni)
    
'   raise error if missing
    If Trim(xSQL) = Empty Then
        Err.Raise Error_InvalidParameter, , _
            "The ini key 'SQL/Matters' is missing or invalid.'"
    End If
        
'   get Matters foreign key field for Client
    xMattersClientIDField = GetIni("SQL", "MattersClientIDField", m_xIni)
    
'   raise error if missing
    If xMattersClientIDField = Empty Then
        Err.Raise Error_InvalidParameter, , _
            "The ini key 'SQL/MattersClientIDField' is missing or invalid.'"
    End If
        
'
    If Not (IsMissing(ClientID) And AdditionalFilter = Empty) Then
'       add filtering parameters

'       add WHERE or AND, if necessary
        If InStr(UCase(xSQL), "WHERE") = 0 Then
            xSQL = xSQL & " WHERE "
        Else
            xSQL = xSQL & " AND "
        End If
        
        If ClientID <> Empty Then
'           filter based on Client ID
            xSQL = xSQL & " " & xMattersClientIDField & "= '" & ClientID & "'"
        End If
        
        
        If AdditionalFilter <> Empty Then
'           add operator for additional WHERE conjunct
            If ClientID <> Empty Then
                xSQL = xSQL & " AND "
            End If
            
            xSQL = xSQL & AdditionalFilter
        End If
    End If

'   add sort condition
    If SortBy = cmSortBy_Name Then
'       get field name for name field from ini
        xOrderField = GetIni("SQL", "MattersNameField", m_xIni)
        If xOrderField = Empty Then
'           no value for key
            Err.Raise Error_InvalidParameter, , _
                "Missing or empty key '[SQL]\MattersNameField'"
        End If
    Else
'       get field name for name field from ini
        xOrderField = GetIni("SQL", "MattersIDField", m_xIni)
        If xOrderField = Empty Then
'           no value for key
            Err.Raise Error_InvalidParameter, , _
                "Missing or empty key '[SQL]\MattersIDField'"
        End If
    End If
    
    xSQL = xSQL & " ORDER BY " & xOrderField

    Set oRS = New ADODB.Recordset
    
'   open recordset
    With oRS
        .Open xSQL, m_oCnn, adOpenForwardOnly
        If Not .EOF And Not .BOF Then
'           Load recordset into XArray
            oMatters.ListArray.LoadRows .GetRows(80000)
            .Close
        Else
            oMatters.ListArray.ReDim 0, -1, 0, 1
        End If
    End With
    Set oRS = Nothing
    Set IClientMatter_Matters = oMatters
    Exit Function
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.IClientMatter_Matters"
    Exit Function
End Function

Private Function DecryptText(ByVal xText As String) As String
    Dim i As Integer
    Dim iChar As Integer
    Dim xScram As String
    Dim iSeed As Integer
    Dim iRnd As Integer
    Dim iLen As Integer
    Dim xLen As String
    
'this function will decrypt string
'encrypted by EncryptText function
    On Error GoTo ProcError
    If xText = Empty Then
        Exit Function
    End If
    
'   1st character is random seed
    iRnd = Asc(Left(xText, 1)) - 48

'   last char is len of string
'   that starts at char 2.
    xLen = Right(xText, 1)
    iLen = Asc(xLen) - 65
    
'   string to decrypt starts at 2nd character
'   and goes for the number of characters
'   specified by the last character
    For i = 1 To iLen
        iChar = Asc(Mid(xText, i + 1, 1))
        xScram = xScram + Chr((iChar - (i Mod 3)) + iRnd)
    Next i
    
    DecryptText = xScram
    Exit Function
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.DecryptText"
    Exit Function
End Function

Private Sub RaiseError(Optional ByVal xNewSource As String)
'raises the current error - sets source if source
'has been supplied and is not already set

    If xNewSource <> Empty Then
'       source supplied
        If InStr(Err.Source, ".") = 0 Then
'           source not set
            Err.Source = xNewSource
        End If
    End If
    
'   raises modified error
    Err.Raise Err.Number, Err.Source, Err.Description
End Sub


Public Function GetIni(xSection As String, _
                        xKey As String, _
                        xIni As String) As Variant
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function
Public Function IClientMatter_ClientsByMatter(Optional ByVal MatterFilter As Variant, _
                                      Optional ByVal AdditionalFilter As String, _
                                      Optional ByVal SortBy As cmSortBy) As CMO.CClients
'returns a populated CClients collection object
    Dim oRS As ADODB.Recordset
    Dim oClients As CMO.CClients
    Static xClientsSQL As String
    Static xClientIDField As String
    Static xMatterIDField As String
    Static xClientNameField As String
    Static xMatterNameField As String
    Dim xSQL As String
    Dim bForceIDSearch As Boolean
    
'   connect if necessary
    On Error GoTo ProcError
    If m_oCnn Is Nothing Then
        OpenConnection
    End If
    
    Set oClients = New CMO.CClients
    

    If xClientsSQL = Empty Then
'       get Clients sql statement
        xClientsSQL = GetIni("SQL", "ClientsByMatter1", m_xIni) & " " & _
            GetIni("SQL", "ClientsByMatter2", m_xIni) & " " & GetIni("SQL", "ClientsByMatter3", m_xIni)
        
'       raise error if missing
        If Trim(xClientsSQL) = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/ClientsByMatter1' is missing or invalid.'"
        End If
    End If
    
    If xClientIDField = Empty Then
'       get Client ID field
        xClientIDField = GetIni("SQL", "ClientIDField", m_xIni)

'       raise error if missing
        If xClientIDField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/ClientIDField' is missing or invalid.'"
        End If
    End If
    
    If xClientNameField = Empty Then
'       get Client Name field
        xClientNameField = GetIni("SQL", "ClientNameField", m_xIni)

'       raise error if missing
        If xClientNameField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/ClientNameField' is missing or invalid.'"
        End If
    End If
    
    If xMatterIDField = Empty Then
'       get Client ID field
        xMatterIDField = GetIni("SQL", "MattersIDField", m_xIni)

'       raise error if missing
        If xMatterIDField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/MatterIDsField' is missing or invalid.'"
        End If
    End If
    
    If xMatterNameField = Empty Then
'       get Client Name field
        xMatterNameField = GetIni("SQL", "MattersNameField", m_xIni)

'       raise error if missing
        If xMatterNameField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/MattersNameField' is missing or invalid.'"
        End If
    End If
    
ForceIDSearch:
    xSQL = xClientsSQL
    
    If IsMissing(MatterFilter) Then
        MatterFilter = Empty
    End If
    
    If IsMissing(AdditionalFilter) Then
        AdditionalFilter = Empty
    End If
    
    If Not (MatterFilter = Empty And AdditionalFilter = Empty) Then
'       add WHERE or AND, if necessary
        If InStr(UCase(xSQL), "WHERE") = 0 Then
            xSQL = xSQL & " WHERE "
        Else
            xSQL = xSQL & " AND "
        End If
        
        If AdditionalFilter <> Empty Then
'           add additional filter to WHERE clause
            xSQL = xSQL & AdditionalFilter
        End If
        
        If MatterFilter <> Empty Then
            If AdditionalFilter <> Empty Then
'               add operator
                xSQL = xSQL & " AND "
            End If
            
'           MatterFilter based on ID or Name
            If IsNumeric(MatterFilter) Or bForceIDSearch Then
'               ID has been supplied - get client with specified ID
                xSQL = xSQL & xMatterIDField & " LIKE '" & MatterFilter & "%'"
                bForceIDSearch = False
            Else
'               Name has been supplied - get all clients with similar name
                xSQL = xSQL & xMatterNameField & " LIKE '%" & MatterFilter & "%'"
                bForceIDSearch = True
            End If
        End If
    End If
    
'   sort appropriately
    If SortBy = cmSortBy_Name Then
        xSQL = xSQL & " ORDER BY " & xClientNameField
    Else
        xSQL = xSQL & " ORDER BY " & xClientIDField
    End If
    
    Set oRS = New ADODB.Recordset
    
'   open recordset
    On Error Resume Next
    oRS.Open xSQL, m_oCnn, adOpenForwardOnly
    
    If Err Then
'       something wrong - alert - probably SQL statement
        On Error GoTo ProcError
        Err.Raise Error_InvalidParameter, , _
        "Could not get clients. The SQL statement '" & xClientsSQL & _
        "' may be invalid."
    Else
        On Error GoTo ProcError
    End If
    
    With oRS
        If Not .EOF And Not .BOF Then
'           Load recordset into XArray
            oClients.ListArray.LoadRows .GetRows(80000)
            .Close
        ElseIf bForceIDSearch Then
'           try again but search id instead
            GoTo ForceIDSearch
        Else
            oClients.ListArray.ReDim 0, -1, 0, 1
        End If
    End With
    
    Set oRS = Nothing
    Set IClientMatter_ClientsByMatter = oClients
    Exit Function
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.IClientMatter_ClientsByMatter"
    Exit Function
End Function
Private Function IClientMatter_MattersFiltered(Optional ByVal ClientID As Variant, _
                                       Optional ByVal MatterFilter As Variant, _
                                       Optional ByVal AdditionalFilter As String, _
                                       Optional ByVal SortBy As CMO.cmSortBy) As CMO.CMatters
    Dim oRS As ADODB.Recordset
    Dim oMatters As CMO.CMatters
    Dim xSQL As String
    Dim xMattersSQL As String
    Dim lCount As Long
    Dim xMattersClientIDField As String
    Dim xMatterIDField As String
    Dim xMatterNameField As String
    Dim xOrderField As String
    Dim bForceIDSearch
    
    On Error GoTo ProcError
    
'   connect if necessary
    If m_oCnn Is Nothing Then
        OpenConnection
    End If

    Set oMatters = New CMO.CMatters

    xMattersSQL = GetIni("SQL", "Matters1", m_xIni) & " " & _
        GetIni("SQL", "Matters2", m_xIni)
    
'   raise error if missing
    If Trim(xMattersSQL) = Empty Then
        Err.Raise Error_InvalidParameter, , _
            "The ini key 'SQL/Matters' is missing or invalid.'"
    End If
        
'   get Matters foreign key field for Client
    xMattersClientIDField = GetIni("SQL", "MattersClientIDField", m_xIni)
    
'   raise error if missing
    If xMattersClientIDField = Empty Then
        Err.Raise Error_InvalidParameter, , _
            "The ini key 'SQL/MattersClientIDField' is missing or invalid.'"
    End If
        
    If xMatterIDField = Empty Then
'       get Client ID field
        xMatterIDField = GetIni("SQL", "MattersIDField", m_xIni)

'       raise error if missing
        If xMatterIDField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/MattersIDField' is missing or invalid.'"
        End If
    End If
    
    If xMatterNameField = Empty Then
'       get Client Name field
        xMatterNameField = GetIni("SQL", "MattersNameField", m_xIni)

'       raise error if missing
        If xMatterNameField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key 'SQL/MattersNameField' is missing or invalid.'"
        End If
    End If

ForceIDSearch:
    xSQL = xMattersSQL
    If Not (IsMissing(ClientID) And IsMissing(MatterFilter) And AdditionalFilter = Empty) Then
'       add filtering parameters

'       add WHERE or AND, if necessary
        If InStr(UCase(xSQL), "WHERE") = 0 Then
            xSQL = xSQL & " WHERE "
        Else
            xSQL = xSQL & " AND "
        End If
        
        If ClientID <> Empty Then
'           filter based on Client ID
            xSQL = xSQL & " " & xMattersClientIDField & "= '" & ClientID & "'"
        End If
        
        If MatterFilter <> Empty Then
            If ClientID <> Empty Then
'               add operator
                xSQL = xSQL & " AND "
            End If
            
'           MatterFilter based on ID or Name
            If IsNumeric(MatterFilter) Or bForceIDSearch Then
'               ID has been supplied - get client with specified ID
                xSQL = xSQL & xMatterIDField & " LIKE '" & MatterFilter & "%'"
                bForceIDSearch = False
            Else
'               Name has been supplied - get all clients with similar name
                xSQL = xSQL & xMatterNameField & " LIKE '%" & MatterFilter & "%'"
                bForceIDSearch = True
            End If
        End If
        
        If AdditionalFilter <> Empty Then
'           add operator for additional WHERE conjunct
            If ClientID <> Empty Or MatterFilter <> Empty Then
                xSQL = xSQL & " AND "
            End If
            
            xSQL = xSQL & AdditionalFilter
        End If
    End If
'   add sort condition
    If SortBy = cmSortBy_Name Then
'       get field name for name field from ini
        xOrderField = GetIni("SQL", "MattersNameField", m_xIni)
        If xOrderField = Empty Then
'           no value for key
            Err.Raise Error_InvalidParameter, , _
                "Missing or empty key '[SQL]\MattersNameField'"
        End If
    Else
'       get field name for name field from ini
        xOrderField = GetIni("SQL", "MattersIDField", m_xIni)
        If xOrderField = Empty Then
'           no value for key
            Err.Raise Error_InvalidParameter, , _
                "Missing or empty key '[SQL]\MattersIDField'"
        End If
    End If
    
    xSQL = xSQL & " ORDER BY " & xOrderField

    Set oRS = New ADODB.Recordset
    
'   open recordset
    With oRS
        .Open xSQL, m_oCnn, adOpenForwardOnly
        If Not .EOF And Not .BOF Then
'           Load recordset into XArray
            oMatters.ListArray.LoadRows .GetRows(80000)
            .Close
        ElseIf bForceIDSearch Then
'           try again but search id instead
            GoTo ForceIDSearch
        Else
            oMatters.ListArray.ReDim 0, -1, 0, 1
        End If
    End With
    Set oRS = Nothing
    Set IClientMatter_MattersFiltered = oMatters
    Exit Function
ProcError:
    RaiseError "mpCMOLEDB.ClientMatter.IClientMatter_MattersFiltered"
    Exit Function
End Function


