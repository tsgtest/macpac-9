Attribute VB_Name = "mdlMain"
Option Explicit

Public Enum mpCMErrors
    mpCMError_NullValueNotAllowed = vbError + 512 + 1
End Enum

Public g_oClimat As CMO.IClientMatter
Public g_bForceSearch As Boolean

Sub RaiseError(ByVal xSource As String)
    xSource = xSource & ":" & Err.Source
    Err.Raise Err.Number, xSource, Err.Description
End Sub

Sub ShowError(Optional ByVal xIntroMessage As String, Optional iSeverity As VbMsgBoxStyle = vbExclamation)
'shows a message box that describes the
'error condition contained in Err
    Const TABS As String = vbTab & vbTab
    Dim lNum As Long
    Dim xDesc As String
    Dim xSource As String
    Dim xMsg As String

    With Err
        xDesc = .Description
        xSource = .Source
        lNum = .Number
    End With

    xSource = Replace(xSource, ":", vbCr & vbTab & vbTab)

'   trim trailing tabs and return
    xSource = Left(xSource, Len(xSource) - 2)

'   show message
    xMsg = xIntroMessage & IIf(xIntroMessage <> Empty, "  ", "") & _
           "The following unexpected error occurred:" & vbCr & vbCr & _
           "Error:  " & TABS & lNum & vbCr & _
           "Description:  " & vbTab & xDesc & vbCr & _
           "Source:  " & TABS & xSource
    MsgBox xMsg, iSeverity, App.Title
End Sub

Public Sub Initialize(ByVal xSourceClass As String)
    Dim lErr As Long

    On Error GoTo ProcError

    If xSourceClass = Empty Then
'       missing class
        Err.Raise mpCMError_NullValueNotAllowed, , _
            "The SourceClass property of the control is empty.  " & _
            "Please contact your administrator."
    End If

'   create client matter object
    On Error Resume Next
    Set g_oClimat = CreateObject(xSourceClass)
    lErr = Err.Number
    On Error GoTo ProcError

    If g_oClimat Is Nothing Then
'       couldn't create object
        Err.Raise lErr, , _
            "Could not create object '" & xSourceClass & "'."
    End If
    Exit Sub
ProcError:
    RaiseError "MPCM.mdlMain.Initialize"
    Exit Sub
End Sub
