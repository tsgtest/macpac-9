VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsFlex8l.OCX"
Begin VB.UserControl ClientMatterComboBox 
   BackStyle       =   0  'Transparent
   ClientHeight    =   2328
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3900
   LockControls    =   -1  'True
   ScaleHeight     =   2328
   ScaleWidth      =   3900
   ToolboxBitmap   =   "ClientMatterComboBox.ctx":0000
   Begin VB.CheckBox chkShowClimat 
      Appearance      =   0  'Flat
      DownPicture     =   "ClientMatterComboBox.ctx":0312
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   3615
      MaskColor       =   &H8000000F&
      Picture         =   "ClientMatterComboBox.ctx":055C
      Style           =   1  'Graphical
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   0
      Width           =   255
   End
   Begin VSFlex8LCtl.VSFlexGrid fgClimat 
      Height          =   1999
      Left            =   0
      TabIndex        =   2
      Top             =   300
      Visible         =   0   'False
      Width           =   3870
      _cx             =   6826
      _cy             =   3526
      Appearance      =   0
      BorderStyle     =   1
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   0
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      BackColorFixed  =   -2147483633
      ForeColorFixed  =   -2147483630
      BackColorSel    =   -2147483635
      ForeColorSel    =   -2147483634
      BackColorBkg    =   -2147483643
      BackColorAlternate=   -2147483643
      GridColor       =   -2147483633
      GridColorFixed  =   -2147483632
      TreeColor       =   -2147483632
      FloodColor      =   192
      SheetBorder     =   -2147483642
      FocusRect       =   1
      HighLight       =   1
      AllowSelection  =   -1  'True
      AllowBigSelection=   0   'False
      AllowUserResizing=   1
      SelectionMode   =   1
      GridLines       =   0
      GridLinesFixed  =   2
      GridLineWidth   =   1
      Rows            =   1
      Cols            =   3
      FixedRows       =   1
      FixedCols       =   0
      RowHeightMin    =   0
      RowHeightMax    =   0
      ColWidthMin     =   0
      ColWidthMax     =   0
      ExtendLastCol   =   -1  'True
      FormatString    =   $"ClientMatterComboBox.ctx":07A6
      ScrollTrack     =   0   'False
      ScrollBars      =   3
      ScrollTips      =   0   'False
      MergeCells      =   0
      MergeCompare    =   0
      AutoResize      =   -1  'True
      AutoSizeMode    =   0
      AutoSearch      =   1
      AutoSearchDelay =   1.5
      MultiTotals     =   -1  'True
      SubtotalPosition=   1
      OutlineBar      =   2
      OutlineCol      =   0
      Ellipsis        =   1
      ExplorerBar     =   0
      PicturesOver    =   0   'False
      FillStyle       =   0
      RightToLeft     =   0   'False
      PictureType     =   0
      TabBehavior     =   0
      OwnerDraw       =   0
      Editable        =   0
      ShowComboButton =   1
      WordWrap        =   -1  'True
      TextStyle       =   0
      TextStyleFixed  =   0
      OleDragMode     =   0
      OleDropMode     =   0
      ComboSearch     =   1
      AutoSizeMouse   =   -1  'True
      FrozenRows      =   0
      FrozenCols      =   0
      AllowUserFreezing=   0
      BackColorFrozen =   0
      ForeColorFrozen =   0
      WallPaperAlignment=   9
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   24
   End
   Begin VB.TextBox txtMsg 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   75
      TabIndex        =   3
      Text            =   "Loading data.  Please wait..."
      Top             =   45
      Visible         =   0   'False
      Width           =   2490
   End
   Begin VB.TextBox txtClimat 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "Enter a client matter number, or search for one by typing some search text and clicking ENTER"
      Top             =   0
      Width           =   3615
   End
   Begin VB.Menu mnuContext 
      Caption         =   "Context"
      Begin VB.Menu mnuShowAllClients 
         Caption         =   "Show &All Clients"
      End
      Begin VB.Menu mnuSearch 
         Caption         =   "&Search"
      End
      Begin VB.Menu mnuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSearchClients 
         Caption         =   "Search by &Client"
      End
      Begin VB.Menu mnuSearchMatters 
         Caption         =   "Search by &Matter"
      End
   End
End
Attribute VB_Name = "ClientMatterComboBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   ClientMatterCombo Control Class
'   created 10/16/01 by Daniel Fisherman
'**********************************************************
Option Explicit

Private Declare Function GetKeyState Lib "User32" ( _
    ByVal nVirtKey As Long) As Integer
    
Private Declare Function LockWindowUpdate Lib "User32" ( _
    ByVal hwndlock As Long) As Long

Public Enum cmClientFields
    cmClientField_Number = 0
    cmClientField_Name = 1
    cmClientField_ValidationStatus = 2
End Enum

Public Enum cmMatterFields
    cmMatterField_Number = 0
    cmMatterField_Name = 1
    cmMatterField_ClientNumber = 2
    cmMatterField_RelatedValues = 3
    cmMatterField_ValidationStatus = 4
End Enum

Public Enum cmValidationStatus
    cmValidationStatus_Unknown = 0
    cmValidationStatus_Valid = 1
    cmValidationStatus_Invalid = 2
End Enum

Enum ComboTypes
    ComboType_Standard = 1
    ComboType_Dropdown = 2
    ComboType_TextOnly = 3
End Enum

'****9.7.1
Enum SearchModes
    cmSearchMode_Client = 0
    cmSearchMode_Matter = 1
End Enum

Private m_bBlockAfterSelChange As Boolean
Private m_xSep As String
Private m_xClass As String
Private m_iRows As Integer
Private m_iComboType As ComboTypes
Private m_bShowLoadMsg As Boolean
Private m_bSelectOnFocus As Boolean
Private m_bShowClientsOnly As Boolean
Private m_bShowListIfNoContactMatterMatch As Boolean
Private m_bPopulateOnLoad As Boolean
Private m_bRunConnected As Boolean
Private m_bBlockBeforeSelChange As Boolean
Private m_xAdditionalClientFilter As String
Private m_xAdditionalMatterFilter As String
Private m_oClients As CMO.CClients
Private m_lBackColor As OLE_COLOR
Private m_oClient As CMO.CClient
Private m_bAllowUserToClearFilters As Boolean
Private m_lClientThreshold As Long
Private m_xRunConnectedTooltip As String
Private m_xRunDisconnectedTooltip As String
Private m_bEnabled As Boolean
Private m_iSearchMode As SearchModes
Private m_xMatterFilter As String

Public Event ClientMatterSelected()
Attribute ClientMatterSelected.VB_Description = "Raised when a client matter number in the list is selected."
Public Event ConnectionFailed(ByVal Message As String)
Attribute ConnectionFailed.VB_Description = "Raised if the connection to an external data source fails"
Public Event BeforeListPopulate(Cancel As Boolean)
Attribute BeforeListPopulate.VB_Description = "Raised before the list is populated with Clients."

''***************************************************
''Properties
''***************************************************
Public Property Let RunConnected(bNew As Boolean)
Attribute RunConnected.VB_Description = "Sets/Returns whether the control should attempt to connect to a data source to display a list of client matter numbers."
    If bNew Then
        UserControl.txtClimat.ToolTipText = RunConnectedToolTip
    Else
        UserControl.txtClimat.ToolTipText = RunDisconnectedToolTip
    End If
    m_bRunConnected = bNew
End Property

Public Property Get RunConnected() As Boolean
    RunConnected = m_bRunConnected
End Property

Public Property Let Enabled(bNew As Boolean)
    With UserControl
        .txtClimat.Enabled = bNew
        .chkShowClimat.Enabled = bNew
        .txtMsg.Enabled = bNew
        .fgClimat.Enabled = bNew
    End With
    m_bEnabled = bNew
End Property

Public Property Get Enabled() As Boolean
    Enabled = m_bEnabled
End Property

Public Property Let RunConnectedToolTip(xNew As String)
    m_xRunConnectedTooltip = Trim(xNew)
    PropertyChanged "RunConnectedToolTip"
End Property

Public Property Get RunConnectedToolTip() As String
    RunConnectedToolTip = m_xRunConnectedTooltip
End Property

Public Property Let RunDisconnectedToolTip(xNew As String)
    m_xRunDisconnectedTooltip = Trim(xNew)
    PropertyChanged "RunDisconnectedToolTip"
End Property

Public Property Get RunDisconnectedToolTip() As String
    RunDisconnectedToolTip = m_xRunDisconnectedTooltip
End Property

Public Property Let ClientThreshold(lNew As Long)
Attribute ClientThreshold.VB_Description = "Sets/Returns the number of clients which will trigger a user alert."
    m_lClientThreshold = lNew
End Property

Public Property Get ClientThreshold() As Long
    ClientThreshold = m_lClientThreshold
End Property
Public Property Get SearchMode() As SearchModes '***9.7.1
    SearchMode = m_iSearchMode
End Property
Public Property Let SearchMode(iNew As SearchModes) '***9.7.1
    ' Groupwise backend doesn't support search by matter
    If InStr(UCase(Me.SourceClass), "MPCMGW") Then iNew = cmSearchMode_Client
    m_iSearchMode = iNew
    If m_iSearchMode = cmSearchMode_Matter Then
        mnuSearchMatters.Checked = True
        mnuSearchClients.Checked = False
    Else
        mnuSearchClients.Checked = True
        mnuSearchMatters.Checked = False
    End If
    PropertyChanged "SearchMode"
End Property
Public Property Let AllowUserToClearFilters(bNew As Boolean)
Attribute AllowUserToClearFilters.VB_Description = "Sets/Returns whether the user can right click on the text portion of the control to clear any filters specified by the programmer."
    m_bAllowUserToClearFilters = bNew
    UserControl.mnuShowAllClients.Visible = bNew
End Property

Public Property Get AllowUserToClearFilters() As Boolean
    AllowUserToClearFilters = m_bAllowUserToClearFilters
End Property

Public Property Let BackColor(lNew As OLE_COLOR)
Attribute BackColor.VB_Description = "Sets/returns the background color of the text portion of the control."
    UserControl.txtClimat.BackColor = lNew
    UserControl.txtMsg.BackColor = lNew
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.txtClimat.BackColor
End Property

Public Property Let ShowClientsOnly(bNew As Boolean)
Attribute ShowClientsOnly.VB_Description = "Sets/Returns if only client numbers are displayed in the dropdown."
    m_bShowClientsOnly = bNew
    PropertyChanged "ShowClientsOnly"
End Property

Public Property Get ShowClientsOnly() As Boolean
    ShowClientsOnly = m_bShowClientsOnly
End Property

Friend Property Let ShowListIfNoContactMatterMatch(bNew As Boolean)
    m_bShowListIfNoContactMatterMatch = bNew
    PropertyChanged "ShowListIfNoContactMatterMatch"
End Property

Friend Property Get ShowListIfNoContactMatterMatch() As Boolean
    ShowListIfNoContactMatterMatch = m_bShowListIfNoContactMatterMatch
End Property

Friend Property Let PopulateOnLoad(bNew As Boolean)
    m_bPopulateOnLoad = bNew
    PropertyChanged "PopulateOnLoad"
End Property

Friend Property Get PopulateOnLoad() As Boolean
    PopulateOnLoad = m_bPopulateOnLoad
End Property

Public Property Let AdditionalClientFilter(xNew As String)
Attribute AdditionalClientFilter.VB_Description = "Sets an additional WHERE clause that can be used for further client filtering at run time."
    m_xAdditionalClientFilter = xNew
    PropertyChanged "AdditionalClientFilter"
End Property

Public Property Get AdditionalClientFilter() As String
    AdditionalClientFilter = m_xAdditionalClientFilter
End Property

Public Property Let AdditionalMatterFilter(xNew As String)
Attribute AdditionalMatterFilter.VB_Description = "Sets an additional WHERE clause that can be used for further matter filtering at run time."
    m_xAdditionalMatterFilter = xNew
    PropertyChanged "AdditionalMatterFilter"
End Property

Public Property Get AdditionalMatterFilter() As String
    AdditionalMatterFilter = m_xAdditionalMatterFilter
End Property

Public Property Let SelectTextOnFocus(bNew As Boolean)
Attribute SelectTextOnFocus.VB_Description = "Sets/Returns whether or not client matter text is selected when the control gets focus."
    m_bSelectOnFocus = bNew
    PropertyChanged "SelectTextOnFocus"
End Property

Public Property Get SelectTextOnFocus() As Boolean
    SelectTextOnFocus = m_bSelectOnFocus
End Property

Public Property Let ComboType(iNew As ComboTypes)
Attribute ComboType.VB_Description = "Sets/Returns the type of control that is displayed."
'set up as standard combo or dropdown combo
    m_iComboType = iNew
    PropertyChanged "ComboType"
    PositionControls
End Property

Public Property Get ComboType() As ComboTypes
    ComboType = m_iComboType
End Property

Public Property Let ListRows(iNew As Integer)
Attribute ListRows.VB_Description = "Sets/Returns the number of rows displayed by the dropdown."
    On Error GoTo ProcError

    m_iRows = iNew
    PropertyChanged "ListRows"
    Exit Property
ProcError:
    RaiseError "MPCM.ClientMatterFlexGrid.ListRows"
    Exit Property
End Property

Public Property Get ListRows() As Integer
    ListRows = m_iRows
End Property

Public Property Let ListVisible(bNew As Boolean)
Attribute ListVisible.VB_Description = "Sets/Returns the visibility of the dropdown list."
    If bNew Or Me.ComboType = ComboType_Standard Then
        UserControl.chkShowClimat.Value = vbChecked
        UserControl.fgClimat.Visible = True
    Else
        UserControl.chkShowClimat.Value = vbUnchecked
        UserControl.fgClimat.Visible = False
    End If
    PropertyChanged "ListVisible"
End Property

Public Property Get ListVisible() As Boolean
    ListVisible = UserControl.chkShowClimat.Value
End Property

Public Property Let ShowLoadMessage(bNew As Boolean)
Attribute ShowLoadMessage.VB_Description = "Sets/Returns whether or not a ""load"" message is displayed in the text portion of the control when the control is loading data."
    m_bShowLoadMsg = bNew
    PropertyChanged "ShowLoadMessage"
End Property

Public Property Get ShowLoadMessage() As Boolean
    ShowLoadMessage = m_bShowLoadMsg
End Property

Public Property Let SourceClass(xNew As String)
Attribute SourceClass.VB_Description = "Sets/Returns the ProgID of the COM class providing data access support to the control."
    m_xClass = xNew
    PropertyChanged "SourceClass"
End Property

Public Property Get SourceClass() As String
    SourceClass = m_xClass
End Property

Public Property Let Separator(xNew As String)
Attribute Separator.VB_Description = "Sets/Returns the client matter separator character(s)."
    m_xSep = xNew
    PropertyChanged "Separator"
End Property

Public Property Get Separator() As String
    Separator = m_xSep
End Property

Public Property Let Value(xNew As String)
Attribute Value.VB_Description = "Sets/Returns the client matter value of the control."
    UserControl.txtClimat.Text = Trim(xNew)
    PropertyChanged "Value"
End Property

Public Property Get Value() As String
    Value = UserControl.txtClimat.Text
End Property

Public Property Let Column1Heading(xNew As String)
Attribute Column1Heading.VB_Description = "Sets/Returns the heading of the 1st column in the dropdown."
    UserControl.fgClimat.Cell(flexcpText, 0, 0, 0, 0) = xNew
    PropertyChanged "Column1Heading"
End Property

Public Property Get Column1Heading() As String
    Column1Heading = UserControl.fgClimat.Cell(flexcpText, 0, 0, 0, 0)
End Property

Public Property Let Column2Heading(xNew As String)
Attribute Column2Heading.VB_Description = "Sets/Returns heading of the 2nd column in the dropdown."
    UserControl.fgClimat.Cell(flexcpText, 0, 1, 0, 1) = xNew
    PropertyChanged "Column2Heading"
End Property

Public Property Get Column2Heading() As String
    Column2Heading = UserControl.fgClimat.Cell(flexcpText, 0, 1, 0, 1)
End Property

Public Property Let Column1Width(lNew As Long)
Attribute Column1Width.VB_Description = "Sets/Returns the width of the 1st column in the dropdown."
    UserControl.fgClimat.ColWidth(0) = lNew
    PropertyChanged "Column1Width"
End Property

Public Property Get Column1Width() As Long
    Column1Width = UserControl.fgClimat.ColWidth(0)
End Property

Public Property Let Column2Width(lNew As Long)
Attribute Column2Width.VB_Description = "Sets/Returns the width of the 2nd column in the dropdown."
    UserControl.fgClimat.ColWidth(1) = lNew
    PropertyChanged "Column2Width"
End Property

Public Property Get Column2Width() As Long
    Column2Width = UserControl.fgClimat.ColWidth(1)
End Property

'***************************************************
'Event Procs
'***************************************************
Private Sub ShowMessage(ByVal xMessage As String)
'   show message
    With UserControl.txtMsg
        If xMessage <> Empty Then
            .Text = xMessage
        End If
        .Visible = True
    End With
    DoEvents
End Sub

Private Sub HideMessage()
    UserControl.txtMsg.Visible = False
    DoEvents
End Sub

Private Sub chkShowClimat_Click()
    
    On Error GoTo ProcError

    If g_bForceSearch And UserControl.txtClimat.Text = Empty Then
        GoTo NoSearch
    End If
    
'   load contacts if necessary and if button was clicked down
    If (m_oClients Is Nothing) And (UserControl.chkShowClimat.Value = vbChecked) Then
        'alert if no filter text exists and required specified
        If Me.ShowLoadMessage Then
            ShowMessage ("Loading data.  Please wait...")
        End If
        SearchClient UserControl.txtClimat.Text
        HideMessage
    End If

NoSearch:
    With UserControl.fgClimat
'       set visible if clicked down and clients exist
        If .Rows > 1 Then
            .Visible = UserControl.chkShowClimat.Value = vbChecked

            If .Visible Then
    '           give focus if visiblef
                .SetFocus
                If UserControl.txtClimat = "" Then
                    ShowClientNode 1
                    CollapseClientNode 1
                End If
            End If
        Else
            UserControl.chkShowClimat.Value = vbUnchecked
        End If
    End With
    Exit Sub
ProcError:
    HideMessage
    ShowError
    Exit Sub
End Sub

Private Sub fgClimat_AfterRowColChange(ByVal OldRow As Long, ByVal OldCol As Long, ByVal NewRow As Long, ByVal NewCol As Long)
'NOTE: this code was originally run in AfterSelChange
'Event Proc, but it proved to be buggy
    On Error GoTo ProcError

'   m_bBlockAfterSelChange prevents
'   indefinite iteration of this code
    If Not m_bBlockAfterSelChange Then
        DoNewSelRow NewRow
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub


Private Sub fgClimat_BeforeSelChange(ByVal OldRowSel As Long, ByVal OldColSel As Long, ByVal NewRowSel As Long, ByVal NewColSel As Long, Cancel As Boolean)
    On Error GoTo ProcError
    If Not m_bBlockBeforeSelChange Then
        If OldRowSel = 0 Then
            Exit Sub
        End If

    '   if the previously selected row was
    '   a client row ensure that it is collapsed
        If UserControl.fgClimat.IsSubtotal(NewRowSel) Then
            CollapseClientNode OldRowSel
        End If
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub fgClimat_DblClick()
    Me.ListVisible = False
End Sub

Private Sub fgClimat_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If (KeyCode = vbKeyReturn Or KeyCode = vbKeyEscape) And Shift = 0 Then
'       enter or esc has been pressed
        Me.ListVisible = False
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuSearch_Click()
    On Error GoTo ProcError
    If Me.SearchMode = cmSearchMode_Matter Then
        Me.SearchMatter UserControl.txtClimat.Text
    Else
        Me.SearchClient UserControl.txtClimat.Text
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuSearchClients_Click()
    mnuSearchClients.Checked = False
    mnuSearchClients.Checked = True
    Me.SearchMode = cmSearchMode_Client
    m_xMatterFilter = Empty
End Sub

Private Sub mnuSearchMatters_Click()
    mnuSearchClients.Checked = False
    mnuSearchMatters.Checked = True
    Me.SearchMode = cmSearchMode_Matter
End Sub

Private Sub mnuShowAllClients_Click()
    Me.AdditionalClientFilter = Empty
    Me.AdditionalMatterFilter = Empty
    m_xMatterFilter = Empty
    Me.Refresh
End Sub

Private Sub txtClimat_GotFocus()
    On Error GoTo ProcError
    If m_bSelectOnFocus Then
'       select text
        With UserControl.txtClimat
            .SelStart = 0
            .SelLength = Len(.Text)
        End With
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub txtClimat_KeyDown(KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ProcError

    If KeyCode = 13 And Shift = 0 And (Me.ComboType <> ComboType_TextOnly) And (Me.RunConnected) Then
        If g_bForceSearch And UserControl.txtClimat.Text = Empty Then
            Exit Sub
        End If
'       ENTER was pressed - search for client based on input
        Select Case Me.SearchMode
            Case cmSearchMode_Matter
                If SearchMatter(Me.Value) = True Then
                    KeyCode = 0
                End If
            Case Else
                If SearchClient(Me.Value) = True Then
                    KeyCode = 0
                End If
        End Select
    ElseIf IsPressed(vbKeyMenu) And KeyCode = vbKeyDown Then
'       alt-downArrow was pressed - show list
        Me.ListVisible = True
        UserControl.fgClimat.SetFocus
    ElseIf KeyCode = vbKeyEscape And Shift = 0 Then
'       esccape key pressed - hide list
        Me.ListVisible = False
    ElseIf IsPressed(vbKeyControl) And KeyCode = vbKeyF6 Then
        ShowVersion
    End If

    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub UserControl_ExitFocus()
    Me.ListVisible = False
End Sub

Private Sub UserControl_Initialize()
'   force search if set in ini
    g_bForceSearch = (UCase(mpBase2.GetMacPacIni("ClientMatterControl", "ForceSearch")) = "TRUE")
End Sub

Private Sub UserControl_InitProperties()
'set properties when control is placed on container
    On Error GoTo ProcError
    Me.RunConnected = True
    Me.Separator = "."
    Me.RunConnectedToolTip = "Enter a client matter number, or search for one by typing some search text and clicking ENTER"
    Me.RunDisconnectedToolTip = ""
    Me.Enabled = True
    Me.Column1Heading = "No."
    Me.Column2Heading = "Name"
    Me.Column1Width = 1020
    Me.Column2Width = 1020
    Me.ListRows = 6
    Me.ComboType = ComboType_Dropdown
    Me.SelectTextOnFocus = True
    Me.ShowClientsOnly = False
    Me.ShowListIfNoContactMatterMatch = False
    Me.PopulateOnLoad = False
    Me.AllowUserToClearFilters = True
    Me.ClientThreshold = 1000
    Me.SearchMode = cmSearchMode_Client '***9.7.1
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub txtClimat_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Button = 2 And RunConnected Then
        'avoid disabled grey text by locking updates
        LockWindowUpdate txtClimat.hWnd
        
        'a disabled textbox will not display a context menu
        txtClimat.Enabled = False
        
        DoEvents
        
        ' No separate Client and Matter search modes for Groupwise
        If InStr(UCase(Me.SourceClass), "MPCMGW") Then
            mnuSep1.Visible = False
            mnuSearchClients.Visible = False
            mnuSearchMatters.Visible = False
        End If

        'display customized context menu
        UserControl.PopupMenu UserControl.mnuContext
        
        'enable control again
        txtClimat.Enabled = True
        
        'unlock updates
        LockWindowUpdate 0&
    End If
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    ReadProperties PropBag
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub UserControl_Resize()
    On Error GoTo ProcError
    PositionControls
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    WriteProperties PropBag
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

'*********************************************
'Private Procs
'*********************************************
Private Sub ReadProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    Me.Separator = PropBag.ReadProperty("Separator", ".")
    Me.RunConnectedToolTip = PropBag.ReadProperty("RunConnectedtoolitp", "Enter a client matter number, or search for one by typing some search text and clicking ENTER")
    Me.RunDisconnectedToolTip = PropBag.ReadProperty("RunDisconnectedtoolitp", "")
    Me.SourceClass = PropBag.ReadProperty("SourceClass", "")
    Me.ShowLoadMessage = PropBag.ReadProperty("ShowLoadMessage", False)
    Me.ShowClientsOnly = PropBag.ReadProperty("ShowClientsOnly", False)
    Me.ShowListIfNoContactMatterMatch = PropBag.ReadProperty("ShowListIfNoContactMatterMatch", False)
    Me.Column1Heading = PropBag.ReadProperty("Col1Caption", "ID")
    Me.Column2Heading = PropBag.ReadProperty("Col2Caption", "Name")
    Me.Column1Width = PropBag.ReadProperty("ColWidth1", 1020)
    Me.Column2Width = PropBag.ReadProperty("ColWidth2", 1020)
    Me.ListRows = PropBag.ReadProperty("ListRows", 6)
    Me.ComboType = PropBag.ReadProperty("ComboType", ComboType_Dropdown)
    Me.SelectTextOnFocus = PropBag.ReadProperty("SelectTextOnFocus", True)
    Me.PopulateOnLoad = PropBag.ReadProperty("PopulateOnLoad", True)
    Me.AdditionalClientFilter = PropBag.ReadProperty("AdditionalClientFilter", "")
    Me.AdditionalMatterFilter = PropBag.ReadProperty("AdditionalMatterFilter", "")
    Me.BackColor = PropBag.ReadProperty("Backcolor", &H80000005)
    Me.RunConnected = PropBag.ReadProperty("RunConnected", True)
    Me.Enabled = PropBag.ReadProperty("Enabled", True)
    Me.AllowUserToClearFilters = PropBag.ReadProperty("AllowUserToClearFilters", True)
    Me.ClientThreshold = PropBag.ReadProperty("ClientThreshold", 1000)
    Me.SearchMode = PropBag.ReadProperty("SearchMode", cmSearchMode_Client) '***9.7.1
    Exit Sub
ProcError:
    RaiseError "MPCM.ClientMatterFlexGrid.ReadProperties"
    Exit Sub
End Sub

Private Sub PositionControls()
    Dim sHeight As Single
    Dim bIsDropdown As Boolean

    On Error GoTo ProcError

'   get combo type
    bIsDropdown = m_iComboType = ComboType_Dropdown

    With UserControl
        Select Case m_iComboType
            Case ComboType_Dropdown
'               set up for dropdown
                Me.ListVisible = False
                .chkShowClimat.Visible = True
                .fgClimat.Width = Max(.Width - 60, 0)
                .txtClimat.Width = Max(.Width - .chkShowClimat.Width - 60, 0)
                .chkShowClimat.Left = Max(.txtClimat.Left + .txtClimat.Width + 1, 0)
                .chkShowClimat.Height = Max(.txtClimat.Height, 0)
            Case ComboTypes.ComboType_Standard
'               set up for standard combo
                 Me.ListVisible = True
                .chkShowClimat.Visible = False
                .fgClimat.Width = .Width
                .txtClimat.Width = .Width
            Case ComboTypes.ComboType_TextOnly
                Me.ListVisible = False
                .chkShowClimat.Visible = False
                .txtClimat.Width = .Width
        End Select

'       get height for list
        sHeight = (Me.ListRows + 1) * .fgClimat.RowHeight(0)

'       set list height
        .fgClimat.Height = sHeight

'       set load message to same size as text box
        .txtMsg.Width = .txtClimat.Width
'       set height of control
        If .Ambient.UserMode = True Or Me.ComboType = ComboType_Standard Then
'           in container run-time - run time height includes list height
            .Extender.Height = sHeight + .txtClimat.Height
        Else
'           is dropdown and in container design-time - height excludes height of list
            .Extender.Height = .txtClimat.Height
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPCM.ClientMatterFlexGrid.PositionControls"
    Exit Sub
End Sub

Private Function LoadClients(Optional ByVal ForceRefresh As Boolean = False, Optional ByVal ClientFilter As String) As Long
    Dim oClient As CMO.CClient
    Dim l As Long

    If Not Me.RunConnected Then
        Exit Function
    End If

    On Error GoTo ProcError
    If g_oClimat Is Nothing Then
'       client matter objects are not
'       yet initialized - do it
        Initialize Me.SourceClass
    End If

    Dim Cancel As Boolean
    RaiseEvent BeforeListPopulate(Cancel)

'   cancel load if specified
    If Cancel Then
        Exit Function
    End If

'   get clients
    If m_oClients Is Nothing Or ForceRefresh Then
        If Not g_oClimat.IsConnected() Then
            RaiseEvent ConnectionFailed(Err.Description)
            Exit Function
        End If

        If Me.SearchMode = cmSearchMode_Matter Then
            Set m_oClients = g_oClimat.ClientsByMatter(ClientFilter, Me.AdditionalClientFilter, cmSortBy_Name)
        Else
            Set m_oClients = g_oClimat.Clients(ClientFilter, Me.AdditionalClientFilter, cmSortBy_Name)
        End If
    End If
'   prevent the afterSelChange event proc from running -
'   it will generate errors until clients are added
    m_bBlockAfterSelChange = True

'   ensure that list is clear
    UserControl.fgClimat.Rows = 1

'   cycle through clients collection
    UserControl.fgClimat.Redraw = flexRDNone
    For l = 0 To m_oClients.Count - 1
        Set oClient = m_oClients.ItemFromIndex(l)
        With UserControl.fgClimat
'           add the client - all clients are Subtotals
            .AddItem oClient.Number & vbTab & oClient.Name
            .IsSubtotal(l + 1) = True
            .RowOutlineLevel(l + 1) = 1
        End With
    Next l
    UserControl.fgClimat.Redraw = flexRDDirect

    Dim lNumClients As Long

    lNumClients = m_oClients.Count

    If lNumClients > 0 Then
'       select the client
        UserControl.fgClimat.Select 1, 0, 1, 1

        If lNumClients = 1 Or Me.SearchMode = cmSearchMode_Matter Then
'           get matters
            Dim lNumMatters As Long
            If Me.SearchMode = cmSearchMode_Matter Then
                lNumMatters = LoadMatters(1, ClientFilter)
            Else
                lNumMatters = LoadMatters(1)
            End If
            If lNumMatters = 1 Then
'               select the matter
                UserControl.fgClimat.Select 2, 0, 2, 1
            End If
        End If
    End If

    If (lNumClients > 1) Or (lNumClients = 1 And lNumMatters <> 1) Then
'       no matter was selected  - show list if specified
        Me.ListVisible = True
    End If

'   enable/disable dropdown based on clients
    UserControl.chkShowClimat.Enabled = (lNumClients > 0)

'   allow this event proc to run
    m_bBlockAfterSelChange = False

'   return number of clients
    LoadClients = lNumClients
    Exit Function
ProcError:
    RaiseError "MPCM.ClientMatterFlexGrid.GetClients"
    Exit Function
End Function

Private Function WriteProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    With PropBag
        .WriteProperty "Separator", Me.Separator, "."
        .WriteProperty "RunConnectedTooltip", Me.RunConnectedToolTip, "Enter a client matter number, or search for one by typing some search text and clicking ENTER"
        .WriteProperty "RunDisconnectedTooltip", Me.RunDisconnectedToolTip, ""
        .WriteProperty "SourceClass", Me.SourceClass, ""
        .WriteProperty "ShowLoadMessage", Me.ShowLoadMessage, False
        .WriteProperty "ShowClientsOnly", Me.ShowClientsOnly, False
        .WriteProperty "ShowListIfNoContactMatterMatch", Me.ShowListIfNoContactMatterMatch, False
        .WriteProperty "Col1Caption", Me.Column1Heading, "ID"
        .WriteProperty "Col2Caption", Me.Column2Heading, "Name"

        .WriteProperty "ColWidth1", Me.Column1Width
        .WriteProperty "ColWidth2", Me.Column2Width
        .WriteProperty "ListRows", Me.ListRows
        .WriteProperty "ComboType", Me.ComboType
        .WriteProperty "SelectTextOnFocus", Me.SelectTextOnFocus
        .WriteProperty "PopulateOnLoad", Me.PopulateOnLoad
        .WriteProperty "AdditionalClientFilter", Me.AdditionalClientFilter
        .WriteProperty "AdditionalMatterFilter", Me.AdditionalMatterFilter
        .WriteProperty "BackColor", Me.BackColor, &H80000005
        .WriteProperty "RunConnected", Me.RunConnected, True
        .WriteProperty "AllowUserToClearFilters", Me.AllowUserToClearFilters, True
        .WriteProperty "ClientThreshold", Me.ClientThreshold, 1000
        .WriteProperty "Enabled", Me.Enabled, True
        .WriteProperty "SearchMode", Me.SearchMode, cmSearchMode_Client '***9.7.1
    End With
    Exit Function
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.UserControl_WriteProperties"
    Exit Function
End Function

Private Function IsPressed(lKey As Long) As Boolean
    IsPressed = (GetKeyState(lKey) < 0)
End Function

Private Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Public Sub Refresh(Optional ByVal ClientFilter As String)
Attribute Refresh.VB_Description = "Clears and reloads client/matter numbers into the control after requerying data source"
'refresh the items in the list
    If Me.ComboType <> ComboType_TextOnly Then
        LoadClients True, ClientFilter
    End If
End Sub

Public Property Get ClientData() As Variant()
Attribute ClientData.VB_Description = "Returns a variant array of data describing the client in the text portion of the control."
    Static vArray(2) As Variant
    Dim oClient As CMO.CClient
    Dim xClimat As String
    Dim xMatter As String
    Dim xClient As String

    On Error GoTo ProcError
    
    If g_oClimat Is Nothing Then
'       client matter objects are not
'       yet initialized - do it
        Initialize Me.SourceClass
    End If
    
    xClimat = UserControl.txtClimat.Text

'   if previous client number is different,
'   or the client object doesn't exist
'   we need to refresh the returned client info
    Parse xClimat, xClient, xMatter

    If xClient = Empty Then
        vArray(cmClientField_ValidationStatus) = cmValidationStatus_Invalid
    End If

    If UCase(vArray(cmClientField_Number)) <> UCase(xClient) Then
        If Me.RunConnected Then
            On Error Resume Next
            Set oClient = GetClient(xClimat)
            On Error GoTo ProcError
            
            vArray(cmClientField_Number) = Empty
            vArray(cmClientField_Name) = Empty

'           set sub vars of Client Type
            If Not oClient Is Nothing Then
                With oClient
                    vArray(cmClientField_Number) = .Number
                    vArray(cmClientField_Name) = .Name
                    vArray(cmClientField_ValidationStatus) = cmValidationStatus_Valid
                End With
            Else
                vArray(cmClientField_Number) = xClient
                vArray(cmClientField_ValidationStatus) = cmValidationStatus_Invalid
            End If
        Else
'           running disconnected from data source, can't validate
            vArray(cmClientField_Number) = xClient
            vArray(cmClientField_ValidationStatus) = cmValidationStatus_Unknown
        End If
    End If
    ClientData = vArray
    Exit Property
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.ClientData"
    Exit Property
End Property


Public Property Get MatterDataVar() As Variant
Attribute MatterDataVar.VB_Description = "Returns a variant array of data describing the client matter in the text portion of the control as a Variant.  Differs from MatterData property in that MatterData returns a Variant Array as a Variant Array."
    On Error GoTo ProcError
    MatterDataVar = MatterData()
    Exit Property
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.MatterDataVar"
    Exit Property
End Property


Public Property Get MatterData() As Variant()
Attribute MatterData.VB_Description = "Returns a variant array of data describing the client matter in the text portion of the control."
'returns a MatterData type filled with
'data from the matter specified in the text
'portion of the control
    Static vArray(4) As Variant
    Dim oMatter As CMO.CMatter
    Dim xClimat As String
    Dim xMatter As String
    Dim xClient As String

    On Error GoTo ProcError
    
    If g_oClimat Is Nothing Then
'       client matter objects are not
'       yet initialized - do it
        Initialize Me.SourceClass
    End If
    
    xClimat = UserControl.txtClimat.Text

    Parse xClimat, xClient, xMatter

    If xMatter = Empty Then
        vArray(cmMatterField_ValidationStatus) = cmValidationStatus_Invalid
    End If
    
'   if the previous client or matter number is
'   different,we need to update matter data
    If (vArray(cmMatterField_Number) <> xMatter) Or _
        (vArray(cmMatterField_ClientNumber) <> xClient) Then

        If Me.RunConnected Then
            On Error Resume Next
            Set oMatter = GetMatter(xClimat)
            On Error GoTo ProcError
            
    '       clear properties of MatterData
            vArray(cmMatterField_Number) = Empty
            vArray(cmMatterField_Name) = Empty
            vArray(cmMatterField_ClientNumber) = Empty
            vArray(cmMatterField_ValidationStatus) = cmValidationStatus_Unknown
            vArray(cmMatterField_RelatedValues) = Empty

            If Not oMatter Is Nothing Then
                With oMatter
                    vArray(cmMatterField_Number) = .Number
                    vArray(cmMatterField_Name) = .Name
                    vArray(cmMatterField_ClientNumber) = xClient
                    vArray(cmMatterField_ValidationStatus) = cmValidationStatus_Valid
                    vArray(cmMatterField_RelatedValues) = .RelatedValues
                End With
            Else
                vArray(cmMatterField_Number) = xMatter
                vArray(cmMatterField_ValidationStatus) = cmValidationStatus_Invalid
            End If
        Else
'           running disconnected from data source, can't validate
            vArray(cmMatterField_Number) = xMatter
            vArray(cmMatterField_ValidationStatus) = cmValidationStatus_Unknown
        End If
    End If
    MatterData = vArray
    Exit Property
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.MatterData"
    Exit Property
End Property

Public Property Get Validity() As cmValidationStatus
Attribute Validity.VB_Description = "Returns TRUE if the client matter number in the text portion of the control is valid."
'returns TRUE if the control contains
'a valid client matter number

    On Error GoTo ProcError

    If g_oClimat Is Nothing Then
'       client matter objects are not
'       yet initialized - do it
        Initialize Me.SourceClass
    End If
    
    If Not Me.RunConnected Then
        Validity = cmValidationStatus_Unknown
    ElseIf Not g_oClimat.IsConnected() Then
        RaiseEvent ConnectionFailed(Err.Description)
        Validity = cmValidationStatus_Unknown
        Exit Property
    End If

    If Me.ClientData()(cmClientField_ValidationStatus) = cmValidationStatus_Invalid Or _
        Me.MatterData()(cmMatterField_ValidationStatus) = cmValidationStatus_Invalid Then
'       either client or matter number is invalid, hence whole is invalid
        Validity = cmValidationStatus_Invalid
    ElseIf Me.ClientData()(cmClientField_ValidationStatus) = cmValidationStatus_Valid And _
        Me.MatterData()(cmMatterField_ValidationStatus) = cmValidationStatus_Valid Then
'       both client and matter are valid, hence whole is valid
        Validity = cmValidationStatus_Valid
    Else
'       neither are invalid, and at least one
'       is unknown, hence whole is unknown
        Validity = cmValidationStatus_Unknown
    End If
    Exit Property
ProcError:
    Err.Raise Err.Number, "MPCM.ClientMatterFlexGrid.Validity"
    Exit Property
End Property

Private Function LoadMatters(lRow As Long, Optional xFilter As String) As Long
    Dim l As Long
    Dim oMatter As CMO.CMatter
    Dim oMatters As CMatters
    Dim oNode As VSFlexNode
    Dim bMattersRetrieved As Boolean
    Dim vID As Variant
    Dim xtest As String
    Dim lErr As Long

    On Error GoTo ProcError

    If Not Me.RunConnected Then
        Exit Function
    ElseIf Not g_oClimat.IsConnected() Then
        RaiseEvent ConnectionFailed(Err.Description)
        Exit Function
    End If

'   prevent recursive iterations -
'   adding matter rows (below) will
'   cause the selRowchange event to
'   fire, which will fire this procedure
    m_bBlockAfterSelChange = True

'   initialize CMO objects if necessary
    If g_oClimat Is Nothing Then
        Initialize Me.SourceClass
    End If

    If lRow < 0 Then
'       invalid row, exit
        m_bBlockAfterSelChange = False
        Exit Function
    End If

    With UserControl.fgClimat
'       get matters for current row
        vID = m_oClients.Item(.Cell(flexcpText, , 0)).ParentID

        If Me.SearchMode = cmSearchMode_Matter Then
            Set oMatters = g_oClimat.MattersFiltered(vID, _
                xFilter, Me.AdditionalMatterFilter, cmSortBy_ID)
        Else
            Set oMatters = g_oClimat.Matters(vID, _
                Me.AdditionalMatterFilter, cmSortBy_ID)
        End If

'       get associated row
        On Error Resume Next
        Set oNode = .GetNode(lRow)
        On Error GoTo 0
        If oNode Is Nothing Then
            m_bBlockAfterSelChange = False
            Exit Function
        End If

'       matters have already been retrieved if
'       the next row is a level 2 row.  this will
'       err if the client is the last row - in this
'       case no matters will have been retrieved
        On Error Resume Next
        bMattersRetrieved = .RowOutlineLevel(lRow + 1) = 0
        On Error GoTo ProcError

        If .RowOutlineLevel(lRow) = 1 And Not bMattersRetrieved Then
'           current row has not had matters added

            .Redraw = flexRDNone

'           cycle through matters for the client
            For l = 0 To oMatters.Count - 1
'               get matter
                Set oMatter = oMatters.ItemFromIndex(l)

'               add matter to subsequent row
                lRow = .Row + l + 1
                Dim i As Integer
                Dim xValues As String
                Dim xArray() As String

                xArray = oMatter.RelatedValues

'               test for existence of related values
                lErr = 0
                Err.Clear

                On Error Resume Next
                xtest = xArray(0)
'               if xarray has no elements an error will exist here
                lErr = Err.Number
                On Error GoTo ProcError

                xValues = Empty

                If lErr = 0 Then
'                   elements exist - convert values array to string
                    For i = 0 To UBound(xArray)
                        xValues = xValues & xArray(i) & "|"
                    Next i

'                   trim trailing tab
                    xValues = Left(xValues, Len(xValues) - 1)
                End If

                .AddItem oMatter.Number & vbTab & oMatter.Name & vbTab & _
                    xValues, lRow

'               make this row pale yellow
                .Cell(flexcpBackColor, lRow, 0, , 1) = &HC0FFFF
            Next l

            .Redraw = flexRDDirect

        End If
    End With

'   allow code to run from selRowChange event
    m_bBlockAfterSelChange = False

'   return number of matters
    LoadMatters = oMatters.Count
    Exit Function
ProcError:
    RaiseError "MPCM.ClientMatterFlexGrid.GetMatters"
    Exit Function
End Function

Private Sub DoNewSelRow(ByVal lRow As Long)
'acts appropriately for selected row - shows the matters if
'row is a client, else writes matter to text portion
    On Error GoTo ProcError

    With UserControl.fgClimat
        If lRow < 1 Then
'           no valid row was selected - exit
            Exit Sub
        End If

        If Me.ShowClientsOnly Then
            Me.Value = Trim(.Cell(flexcpText, lRow, 0, lRow, 0))
        ElseIf .IsSubtotal(lRow) Then
'           row is client - expand to show matters
            ShowClientNode lRow
        Else
            SetSelMatterProps lRow
        End If
    End With

    Exit Sub
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.DoNewSelRow"
    Exit Sub
End Sub

Private Sub SetSelMatterProps(lRow As Long)
'fills the properties of the matter based on the selected row
    Dim l As Long
    Dim oMatter As CMO.CMatter

    On Error GoTo ProcError

    With UserControl.fgClimat
'       row is matter - find parent client node,
'       and set text to client matter
        l = lRow
        Do
'           get previous row
            l = l - 1
        Loop While .IsSubtotal(l) = False

'       set text to client matter
        Me.Value = Trim$(.Cell(flexcpText, l, 0, l, 0)) & Me.Separator & _
            Trim$(.Cell(flexcpText, lRow, 0, lRow, 0))
        RaiseEvent ClientMatterSelected
    End With
    Exit Sub
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.GetSelectedMatter"
    Exit Sub
End Sub

Private Sub ShowClientNode(ByVal lRow As Long)
    Dim sCellTop As Single
    On Error GoTo ProcError
    With UserControl.fgClimat
'       selected row was a client - expand node and add matters
        .IsCollapsed(lRow) = flexOutlineExpanded

'       make row bold
        .Cell(flexcpFontBold, lRow, 0, lRow, 1) = True

'       scroll to end of list then to selected row - this
'       forces the row to appear as the first displayed row
        If Not .RowIsVisible(lRow) Then
            .ShowCell .Rows - 1, 0
            .ShowCell lRow, 0
        End If

'       set value of text box = to client ID followed by the separator
        UserControl.txtClimat.Text = _
            Trim(.Cell(flexcpText, lRow, 0, lRow, 0)) & Me.Separator

        If (.Rows = lRow + 1) Then
'           client row is the last row -
'           add matters underneath client row
            LoadMatters lRow, m_xMatterFilter
        ElseIf .IsSubtotal(lRow + 1) Then
'           next row is also a subtotal -
'           add matters underneath client row
            LoadMatters lRow, m_xMatterFilter
        End If

'       set focus to the tree
        On Error Resume Next
        .SetFocus
    End With
    Exit Sub
ProcError:
    RaiseError "MPCM.ClientMatterFlexGrid.ShowClientNode"
    Exit Sub
End Sub


Private Sub Parse(ByVal xClientMatter As String, xClient As String, xMatter As String)
    Dim iPos As Integer

    On Error GoTo ProcError

    If xClientMatter = Empty Then
        Exit Sub
    End If

'   get separator position
    iPos = InStr(xClientMatter, Me.Separator)

    If iPos Then
'       validate client and matter
        xClient = Left(xClientMatter, iPos - 1)
        xMatter = Mid(xClientMatter, iPos + 1)
    Else
'       invalid entry
'        return empty client and matter
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, "MPCM.ClientMatterFlexGrid.Parse"
    Exit Sub
End Sub

Private Sub ShowVersion()
'show version info and support class for the control
    Dim xMsg As String

    xMsg = "MacPac Client Matter Combo Control" & vbCr & _
        "Version " & App.Major & "." & App.Minor & "." & App.Revision & vbCr & _
        "Supported by class '" & Me.SourceClass & "'"

    MsgBox xMsg, vbInformation + vbOKOnly, "Legal MacPac"
End Sub

Public Function SearchClient(ByVal xSearchText As String, Optional ByVal ShowDropdown As Boolean = True) As Boolean
'searches for the client matching the
'text specified in txtClimat - returns
'true if a match occurred
    Dim lFound As Long
    Dim iSearchCol As Integer
    Dim oFG As VSFlexGrid
    Dim xClient As String
    Dim xMatter As String

    On Error GoTo ProcError
    If g_oClimat Is Nothing Then
'       client matter objects are not
'       yet initialized - do it
        Initialize Me.SourceClass
    End If
    
    If Not Me.RunConnected Then
        Exit Function
    ElseIf Not g_oClimat.IsConnected() Then
        RaiseEvent ConnectionFailed(Err.Description)
        Exit Function
    End If
    
    Parse xSearchText, xClient, xMatter

    If xClient <> Empty Then
        xSearchText = xClient
    End If

    Screen.MousePointer = vbHourglass
'    ShowMessage "Searching for client..."
    Refresh xSearchText
    HideMessage
    Screen.MousePointer = vbDefault

'   get flexGrid
    Set oFG = UserControl.fgClimat

    If oFG.Rows = 1 Then
'       only row is header row -
'       alert that no match was found
        Dim xMsg As String
        xMsg = "No client name or number matched the text '" & xSearchText & "'."
        MsgBox xMsg, vbExclamation, "Client Search"
        Exit Function
    Else
        UserControl.Refresh
        Me.ListVisible = True
        oFG.SetFocus
        SearchClient = True
    End If
    Exit Function
ProcError:
    m_bBlockBeforeSelChange = False
    HideMessage
    Screen.MousePointer = vbDefault
    Err.Raise Err.Number, "MPCM.ClientMatterFlexGrid.SearchClient"
    Exit Function
End Function
Public Function SearchMatter(ByVal xSearchText As String, Optional ByVal ShowDropdown As Boolean = True) As Boolean
'searches for the client matching the
'text specified in txtClimat - returns
'true if a match occurred
    Dim lFound As Long
    Dim iSearchCol As Integer
    Dim oFG As VSFlexGrid
    Dim xClient As String
    Dim xMatter As String

    On Error GoTo ProcError
    If g_oClimat Is Nothing Then
'       client matter objects are not
'       yet initialized - do it
        Initialize Me.SourceClass
    End If
    
    If Not Me.RunConnected Then
        Exit Function
    ElseIf Not g_oClimat.IsConnected() Then
        RaiseEvent ConnectionFailed(Err.Description)
        Exit Function
    End If
    Parse xSearchText, xClient, xMatter

    If xMatter <> Empty Then
        xSearchText = xMatter
    ElseIf xClient <> Empty Then
        xSearchText = xClient
    End If

    m_xMatterFilter = xSearchText '***9.7.1
    
    Screen.MousePointer = vbHourglass
'    ShowMessage "Searching for client..."
    Refresh xSearchText
    HideMessage
    Screen.MousePointer = vbDefault

'   get flexGrid
    Set oFG = UserControl.fgClimat

    If oFG.Rows = 1 Then
'       only row is header row -
'       alert that no match was found
        Dim xMsg As String
        xMsg = "No matter name or number matched the text '" & xSearchText & "'."
        MsgBox xMsg, vbExclamation, "Client Search"
        Exit Function
    Else
        UserControl.Refresh
        Me.ListVisible = True
        oFG.SetFocus
        SearchMatter = True
    End If
    Exit Function
ProcError:
    m_bBlockBeforeSelChange = False
    HideMessage
    Screen.MousePointer = vbDefault
    Err.Raise Err.Number, "MPCM.ClientMatterFlexGrid.SearchClient"
    Exit Function
End Function

Private Function GetClient(ByVal xClientMatterNumber As String) As CMO.CClient
    Dim xClient As String
    Dim xMatter As String
    Dim oClients As CMO.CClients

'   get client and number from text
    On Error GoTo ProcError
    Parse xClientMatterNumber, xClient, xMatter

'   return nothing if no client number
    If xClient = Empty Then
        Exit Function
    End If

    On Error GoTo ProcError
    If g_oClimat Is Nothing Then
'       client matter objects are not
'       yet initialized - do it
        Initialize Me.SourceClass
    End If

    On Error Resume Next
    Set oClients = g_oClimat.Clients(xClient, Me.AdditionalClientFilter, cmSortBy_Name)
    Set m_oClient = oClients.Item(xClient)
    Set GetClient = m_oClient
    Exit Function
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.GetClient"
    Exit Function
End Function

Private Function GetMatterOLD(ByVal xClientMatterNumber As String) As CMO.CMatter
    Dim xClient As String
    Dim xMatter As String
    Dim oClient As CMO.CClient

'   get client and matter number from text
    On Error GoTo ProcError
    Parse xClientMatterNumber, xClient, xMatter

    On Error Resume Next
    Set oClient = m_oClients.Item(xClient)
    Set GetMatterOLD = g_oClimat.Matters(oClient.ParentID).ItemFromID(xMatter)
    Exit Function
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.GetMatter"
    Exit Function
End Function

Private Function GetMatter(ByVal xClientMatterNumber As String) As CMO.CMatter
'returns the CMatter object specified by xClientMatterNumber
    Dim xClient As String
    Dim xMatter As String
    Dim oClients As CMO.CClients
    Dim oClient As CMO.CClient

'   get client and matter number from text
    On Error GoTo ProcError
    Parse xClientMatterNumber, xClient, xMatter

    If m_oClient.Number = xClient Then
'       we already have the client object,
'       so we don't have to get it (fyi, it's
'       set in GetClient - this is an optimization
'       even though it's funky.)
        Set oClient = m_oClient
    End If

    If oClient Is Nothing Then
        Set oClients = g_oClimat.Clients(xClient)

        If Not oClients Is Nothing Then
            On Error Resume Next
            Set oClient = oClients.Item(xClient)
            On Error GoTo ProcError
        End If
    End If

    If Not oClient Is Nothing Then
        On Error Resume Next
        Set GetMatter = g_oClimat.Matters(oClient.ParentID).ItemFromID(xMatter)
    End If
    Exit Function
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.GetMatter"
    Exit Function
End Function

Private Function GetMatterFromClient(oClient As CMO.CClient, ByVal xMatterNumber As String) As CMO.CMatter
'returns the CMatter object specified by
'the client oClient, and xMatterNumber
    Dim xMatter As String

    If oClient Is Nothing Or xMatterNumber = Empty Then
        Exit Function
    End If
    On Error Resume Next
    Set GetMatterFromClient = g_oClimat.Matters(oClient.ParentID).ItemFromID(xMatter)
    Exit Function
ProcError:
    RaiseError "MPCM.ClientMatterComboBox.GetMatter"
    Exit Function
End Function

Private Sub CollapseClientNode(ByVal lRow As Long)
'collapses and unbolds the specified row
    On Error GoTo ProcError

    If lRow = 0 Then
'       no row is selected
        Exit Sub
    End If

    With UserControl.fgClimat
        If .IsSubtotal(lRow) = False Then
'           row is a matter, find parent Client row
            Do
                lRow = lRow - 1
            Loop While .IsSubtotal(lRow) = False
        End If

'       collapse and unbold
        .IsCollapsed(lRow) = flexOutlineCollapsed
        .Cell(flexcpFontBold, lRow, 0, lRow, 1) = False

    End With

    Exit Sub
ProcError:
    RaiseError "MPCM.ClientMatterFlexGrid.CollapseClientNode"
    Exit Sub
End Sub
