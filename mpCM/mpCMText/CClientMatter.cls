VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CClientMatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   mpCMText.CClientMatter Class
'   created 10/01 by Daniel Fishermna
'   Client/Matter lookup integration for Tab-Delimited Text files
'**********************************************************

Option Explicit

Private Const mpCMAppName As String = "Tab Delimited Text"

Private m_bInit As Boolean
Private m_oConnect As DAO.Connection
Private m_oWS As DAO.Workspace
Private m_oError As mperror.CError

Implements CMO.IClientMatter

Private Sub Class_Initialize()
    Set m_oError = New mperror.CError
End Sub

Private Function IClientMatter_Clients(Optional ByVal Filter As Variant, Optional ByVal SortBy As CMO.cmSortBy) As CMO.CClients
    Dim oRS As DAO.Recordset
    Dim oClients As CMO.CClients
    Dim xSQL As String
    Dim lCount As Long
    
'   connect if necessary
    On Error GoTo ProcError
    If m_oConnect Is Nothing Then
        OpenConnection
    End If
    
    Set oClients = New CMO.CClients
            
    ' Read fields from tab delimited Clients.txt
    ' Col1= [Client_ID]
    ' Col2= [Client_Name]
    xSQL = "SELECT DISTINCT [Client_ID],[Client_Name] FROM [Clients#txt]"
    If Not IsMissing(Filter) Then
'       filter based on ID or Name
        If IsNumeric(Filter) Then
'           ID has been supplied - get client with specified ID
            xSQL = xSQL & " WHERE [Client_ID] = " & Filter
        Else
'           Name has been supplied - get all clients with similar name
            xSQL = xSQL & " WHERE [Client_Name] LIKE '%" & Filter & "%'"
        End If
    End If
    
    If SortBy = cmSortBy_Name Then
        xSQL = xSQL & " ORDER BY [Client_Name]"
    Else
        xSQL = xSQL & " ORDER BY [Client_ID]"
    End If
    
    Set oRS = m_oConnect.OpenRecordset(xSQL, dbOpenSnapshot)
    
    With oRS
        If Not .EOF And Not .BOF Then
            ' Load recordset into XArray
            oClients.ListArray.LoadRows .GetRows(80000)
            .Close
        Else
            oClients.ListArray.ReDim 0, -1, 0, 1
        End If
    End With
    Set oRS = Nothing
    Set IClientMatter_Clients = oClients
    Exit Function
ProcError:
    RaiseError "mpCMIManage.CClientMatter.IClientMatter_Clients"
    Exit Function
End Function

Private Function IClientMatter_IsValid(ByVal ClientID As Variant, Optional ByVal MatterID As Variant) As Boolean
    Dim oClients As CMO.CClients
    Dim oMatters As CMO.CMatters
    Dim oMatter As CMO.CMatter
    
    On Error GoTo ProcError
    On Error GoTo ProcError
    If IsMissing(MatterID) Then
'       check for client only
        Set oClients = IClientMatter_Clients(ClientID)
        On Error Resume Next
        IClientMatter_IsValid = Not (oClients.Item(ClientID) Is Nothing)
        On Error GoTo ProcError
    Else
'       check for matters
        Set oMatters = IClientMatter_Matters(ClientID)
            
        On Error Resume Next
        Set oMatter = oMatters.ItemFromID(MatterID)
        On Error GoTo ProcError
            
        IClientMatter_IsValid = Not oMatter Is Nothing
    End If
    Exit Function
ProcError:
    RaiseError "mpCMText.CClientMatter.IClientMatter_IsValid"
    Exit Function
End Function

Private Function IClientMatter_Matters(Optional ByVal ClientID As Variant, _
                                       Optional ByVal AdditionalFilter As String, _
                                       Optional ByVal SortBy As CMO.cmSortBy) As CMO.CMatters

    Dim oRS As DAO.Recordset
    Dim oMatters As CMO.CMatters
    Dim xSQL As String
    Dim lCount As Long
    
    On Error GoTo ProcError
    
'   connect if necessary
    If m_oConnect Is Nothing Then
        OpenConnection
    End If
    
    Set oMatters = New CMO.CMatters
            
    ' Read fields from tab delimited Clients.txt
    ' Col1= [Matter_ID]
    ' Col2= [Matter_Name]
    xSQL = "SELECT DISTINCT [Matter_ID],[Matter_Name] FROM [Matters#txt]"
    If Not IsMissing(ClientID) Then
        If ClientID <> Empty Then
'           filter based on Client ID
            xSQL = xSQL & " WHERE [Client_ID] = '" & ClientID & "'"
        End If
    End If
    
    If SortBy = cmSortBy_Name Then
        xSQL = xSQL & " ORDER BY [Matter_Name]"
    Else
        xSQL = xSQL & " ORDER BY [Matter_ID]"
    End If
    
    Set oRS = m_oConnect.OpenRecordset(xSQL, dbOpenSnapshot)
    
    With oRS
        If Not .EOF And Not .BOF Then
            ' Load recordset into XArray
            oMatters.ListArray.LoadRows .GetRows(80000)
            .Close
        Else
            oMatters.ListArray.ReDim 0, -1, 0, 1
        End If
    End With
    Set oRS = Nothing
    Set IClientMatter_Matters = oMatters
    Exit Function
ProcError:
    RaiseError "mpCMText.CClientMatter.IClientMatter_Matters"
    Exit Function
End Function

Private Sub OpenConnection()
    On Error GoTo ProcError
    
'   check for text file and schema ini
    If Dir(App.Path & "\Schema.ini") = Empty Then
        Err.Raise mperror.mpError_InvalidFile, , _
            App.Path & "\Schema.ini" & " could not be found."
    End If
    
    ' Create connection to Text file using ODBC
    Set m_oWS = DAO.CreateWorkspace("", "", "", dbUseODBC)
    Set m_oConnect = m_oWS.OpenConnection("", , , _
        "ODBC;DRIVER={Microsoft Text Driver (*.txt; *.csv)};DBQ=" & App.Path)
    Exit Sub
ProcError:
    RaiseError "mpCMText.CClientMatter.OpenConnection"
End Sub

Private Sub CloseConnection()
    On Error GoTo ProcError
    If Not m_oConnect Is Nothing Then _
        m_oConnect.Close
    If Not m_oWS Is Nothing Then _
        m_oWS.Close
    Set m_oConnect = Nothing
    Set m_oWS = Nothing
    Exit Sub
ProcError:
    RaiseError "mpCMIManage.CClientMatter.CloseConnection"
    Exit Sub
End Sub

Private Sub RaiseError(Optional ByVal xNewSource As String)
'raises the current error - sets source if source
'has been supplied and is not already set

    If xNewSource <> Empty Then
'       source supplied
        If InStr(Err.Source, ".") = 0 Then
'           source not set
            Err.Source = xNewSource
        End If
    End If
    
'   raises modified error
    Err.Raise Err.Number, Err.Source
End Sub

