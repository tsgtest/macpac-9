VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClientMatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   mpCMDOCS.ClientMatter Class
'   created 04/11/01 by Jeffrey Sweetland
'   Client/Matter lookup integration for DOCSOpen
'**********************************************************
Option Explicit

Private Const mpCMAppName As String = "DOCSOpen"

Private m_oMatter As mpCliMat.CMatter
Private m_oClient As mpCliMat.CClient
Private m_oMatters As mpCliMat.CMatters
Private m_oClients As mpCliMat.CClients
Private m_xSep As String
Private m_bInit As Boolean
Private m_oConnect As DAO.Connection
Private m_oWS As DAO.Workspace
Private m_xDataSource As String
Private m_xFilter As String
Private m_iSortColumn As Integer

Implements CMO.IClientMatter



'Private Property Get IClientMatter_AppName() As String
'    IClientMatter_AppName = mpCMAppName
'End Property
'
'Private Property Let iClientMatter_ClientFilter(xNew As String)
'    If xNew <> m_xFilter Then
'        m_xFilter = xNew
'        LoadClients
'    End If
'End Property
'
'Private Property Get iClientMatter_ClientFilter() As String
'    iClientMatter_ClientFilter = m_xFilter
'End Property
'
'Public Property Set iClientMatter_CurrentMatter(oNew As mpCliMat.CMatter)
'    Set m_oMatter = oNew
'End Property
'Public Property Get iClientMatter_CurrentMatter() As mpCliMat.CMatter
'    Set iClientMatter_CurrentMatter = m_oMatter
'End Property
'Public Property Set iClientMatter_CurrentClient(oNew As mpCliMat.CClient)
'    Set CurrentClient = oNew
'End Property
'Public Property Get iClientMatter_CurrentClient() As mpCliMat.CClient
'    Set iClientMatter_CurrentClient = m_oClient
'End Property
'Public Function iClientMatter_ClientMatterNumber() As String
'    iClientMatter_ClientMatterNumber = ClientMatterNumber()
'End Function
'Public Property Set iClientMatter_Clients(oNew As mpCliMat.CClients)
'    Set m_oClients = oNew
'End Property
'Public Property Get iClientMatter_Clients() As mpCliMat.CClients
'    Set iClientMatter_Clients = m_oClients
'End Property
'Public Function iClientMatter_ClientArray() As XArrayDBObject.XArrayDB
'    Set iClientMatter_ClientArray = ClientArray()
'End Function
'Private Property Let iClientMatter_DataSource(xNew As String)
'    m_xDataSource = xNew
'End Property
'Private Property Get iClientMatter_DataSource() As String
'    iClientMatter_DataSource = m_xDataSource
'End Property
'Private Function IClientMatter_Initialized() As Boolean
'    IClientMatter_Initialized = m_bInit
'End Function
'Private Sub IClientMatter_LoadList()
'    LoadList
'End Sub
'Public Function iClientMatter_MatterArray() As XArrayDBObject.XArrayDB
'    Set iClientMatter_MatterArray = MatterArray()
'End Function
'
'Private Property Get iClientMatter_Matters() As mpCliMat.CMatters
'    Set iClientMatter_Matters = m_oMatters
'End Property
'
'Private Property Set iClientMatter_Matters(RHS As mpCliMat.CMatters)
'    Set m_oMatters = RHS
'End Property
'
'Public Property Get iClientMatter_Separator() As String
'    iClientMatter_Separator = m_xSep
'End Property
'Public Property Let iClientMatter_Separator(xNew As String)
'    m_xSep = xNew
'End Property
'Private Sub Class_Terminate()
'    Set m_oClient = Nothing
'    Set m_oMatter = Nothing
'    Set m_oClients = Nothing
'    Set m_oMatters = Nothing
'    CloseConnection
'End Sub
'Private Sub LoadList()
'    Dim i As Integer
'    If m_oConnect Is Nothing Then
'        OpenConnection
'        If Err.Number Then
'            Err.Raise Err.Number, "ClientMatter.LoadList", Err.Description
'            Exit Sub
'        End If
'    End If
'    LoadClients
'    m_bInit = True
'End Sub
'Private Sub LoadMatters(vClientID)
'    Dim i As Long
'    Dim rsMatters As DAO.Recordset
'    Dim xSQL As String
'
'    If Not m_oMatters Is Nothing Then _
'        Set m_oMatters = Nothing
'
'    Set m_oMatters = New mpCliMat.CMatters
'
'    xSQL = "SELECT MATTER.MATTER_ID, MATTER.MATTER_NAME " & _
'            "FROM DOCSADM.CLIENT INNER JOIN DOCSADM.MATTER " & _
'            "ON CLIENT.SYSTEM_ID = MATTER.CLIENT_ID " & _
'            "WHERE CLIENT.CLIENT_ID= '" & vClientID & "'"
'
'    Set rsMatters = m_oConnect.OpenRecordset(xSQL, dbOpenDynaset)
'
'    With rsMatters
'
'        If Not .EOF And Not .BOF Then
'            .MoveLast
'            .MoveFirst
'            ' Load Recordset into XArray
'            m_oMatters.ListArray.LoadRows .GetRows(.RecordCount)
'            m_oMatters.ListArray.QuickSort 0, m_oMatters.ListArray.UpperBound(1), m_iSortColumn, XORDER_ASCEND, XTYPE_STRINGCASESENSITIVE
'            Set CurrentMatter = m_oMatters.Item(, 1)
'            .Close
'        Else
'            m_oMatters.ListArray.ReDim 0, -1, 0, 1
'            Set CurrentMatter = Nothing
'        End If
'    End With
'    Set rsMatters = Nothing
'End Sub
'Private Property Set CurrentClient(oNew As mpCliMat.CClient)
'    If Not m_oClient Is Nothing Then _
'        Set m_oClient = Nothing
'    Set m_oClient = oNew
'    If Not m_oClient Is Nothing Then
'        LoadMatters m_oClient.ID
'    End If
'End Property
'Private Function ClientMatterNumber() As String
'    ' Return Client and Matter ID with separator
'    If Not m_oClient Is Nothing Then
'        ClientMatterNumber = m_oClient.ID
'        If Not m_oMatter Is Nothing Then
'            If m_oMatter.ID <> "" Then _
'                ClientMatterNumber = ClientMatterNumber & m_xSep & m_oMatter.ID
'        End If
'    End If
'
'End Function
'Private Property Set CurrentMatter(oNew As mpCliMat.CMatter)
'    If Not m_oMatter Is Nothing Then _
'        Set m_oMatter = Nothing
'    Set m_oMatter = oNew
'End Property
'Private Sub LoadClients()
'    Dim rsClients As DAO.Recordset
'    Dim xSQL As String
'
'    If Not m_oClients Is Nothing Then _
'        Set m_oClients = Nothing
'
'    Set m_oClients = New mpCliMat.CClients
'
'    xSQL = "SELECT CLIENT.CLIENT_ID, " & _
'        "CLIENT.CLIENT_NAME FROM DOCSADM.CLIENT"
'    If m_xFilter <> "" Then _
'        xSQL = xSQL & " WHERE CLIENT.CLIENT_NAME LIKE '%" & m_xFilter & "%'"
'
'    xSQL = xSQL & " ORDER BY CLIENT.CLIENT_NAME;"
'
'    Set rsClients = m_oConnect.OpenRecordset(xSQL, dbOpenDynaset)
'
'    With rsClients
'
'        If Not .EOF And Not .BOF Then
'
'            .MoveLast
'            .MoveFirst
'            ' Load Recordset into XArray
'            m_oClients.ListArray.LoadRows .GetRows(.RecordCount)
'            m_oClients.ListArray.QuickSort 0, m_oClients.ListArray.UpperBound(1), m_iSortColumn, XORDER_ASCEND, XTYPE_STRINGCASESENSITIVE
'            Set CurrentClient = m_oClients.Item(, 1)
'            .Close
'        Else
'            m_oClients.ListArray.ReDim 0, -1, 0, 1
'            Set CurrentClient = Nothing
'        End If
'    End With
'    Set rsClients = Nothing
'End Sub
'Private Function MatterArray() As XArrayDBObject.XArrayDB
'    Set MatterArray = m_oMatters.ListArray
'End Function
'Private Function ClientArray() As XArrayDBObject.XArrayDB
'    Set ClientArray = m_oClients.ListArray
'End Function
'Private Sub OpenConnection()
'    Set m_oWS = DAO.CreateWorkspace("", "DOCSADM", "DOCSADM", dbUseODBC)
'    Set m_oConnect = m_oWS.OpenConnection(m_xDataSource, dbDriverNoPrompt, True, _
'        "ODBC;DSN=" & m_xDataSource)
'End Sub
'Private Property Get iClientMatter_SortColumn() As Integer
'    iClientMatter_SortColumn = m_iSortColumn
'End Property
'Private Property Let iClientMatter_SortColumn(iNew As Integer)
'    If m_iSortColumn <> iNew Then
'        m_iSortColumn = iNew
'    End If
'End Property
'Private Sub CloseConnection()
'    If Not m_oConnect Is Nothing Then _
'        m_oConnect.Close
'    If Not m_oWS Is Nothing Then _
'        m_oWS.Close
'    Set m_oConnect = Nothing
'    Set m_oWS = Nothing
'End Sub
