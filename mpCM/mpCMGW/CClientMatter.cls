VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CClientMatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   mpCMGW.ClientMatter Class
'   created 3/31/03 by Charlie Homo
'   Client/Matter lookup integration for GroupWise backend
'**********************************************************

Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long
    
Enum mpCMGW_Errors
    Error_InvalidParameter = vbError + 512 + 1
End Enum

Private m_xIni As String
Private m_oApp As GroupwareTypeLibrary.Application
Private m_oAcct As GroupwareTypeLibrary.Account
Private m_oAB As GroupwareTypeLibrary.AddressBook
Private m_xUserName As String
Private m_xClientIDField As String
Private m_xClientNameField As String
Private m_xMattersIDField As String
Private m_xMattersNameField As String
Private m_xRelatedFields() As String
    
Implements CMO.IClientMatter

Private Sub Class_Initialize()
    m_xIni = mpBase2.ApplicationDirectory & "\mpCMGW.ini"
    LoadRelatedFields
End Sub

Private Sub OpenConnection()
'establishes a gw session
    Dim xABName As String
    
    On Error GoTo ProcError
    
    Set m_oApp = New GroupwareTypeLibrary.Application

    With m_oApp
        On Error Resume Next
        Set m_oAcct = .Login(, , , egwPromptIfNeeded)
        On Error GoTo ProcError
        
        If Not (m_oAcct Is Nothing) Then
            xABName = GetIni("Application", "AddressBook", m_xIni)
            If xABName = Empty Then
    '           no value for key
                Err.Raise Error_InvalidParameter, , _
                    "Missing or empty key '[APPLICATION]\AddressBook' in mpCMGW.ini"
            End If

'           set addressbook
            On Error Resume Next
            If UCase(xABName) = "DEFAULT" Then
                Set m_oAB = m_oAcct.DefaultAddressBook
            Else
                Set m_oAB = m_oAcct.AddressBooks(xABName)
            End If
            On Error GoTo ProcError
            
            If m_oAB Is Nothing Then
                MsgBox "Could not open the " & xABName & " Groupwise Address Book.", vbExclamation, App.Title
            End If
            
'           get user name
            m_xUserName = m_oAcct.Owner.DisplayName
        Else
            MsgBox "Could not connect to Groupwise.", vbExclamation, App.Title
        End If
    End With
    
    Exit Sub
ProcError:
    RaiseError "mpCMGW.ClientMatter.OpenConnection"
    Exit Sub
Exit Sub

End Sub

Private Sub CloseConnection()
'connection to the data source
    On Error GoTo ProcError
    Set m_oAB = Nothing
    Set m_oAcct = Nothing
    Set m_oApp = Nothing
    Exit Sub
ProcError:
    RaiseError "mpCMGW.ClientMatter.CloseConnection"
    Exit Sub
End Sub

Public Function IClientMatter_Clients(Optional ByVal ClientFilter As Variant, _
                                      Optional ByVal AdditionalFilter As String, _
                                      Optional ByVal SortBy As cmSortBy) As CMO.CClients
'returns a populated CClients collection object
    
    Dim oClients As CMO.CClients
    Dim iCount As Integer
    Dim i As Integer
    Dim l As Long
    Dim m As Long
    Dim oarr() As Variant
    Dim xVal As String
    Dim bInAdditionalFilter As Boolean
    Dim xName As String
    Dim xID As String
    
    '   connect if necessary
    On Error GoTo ProcError
    If m_oAcct Is Nothing Then
        OpenConnection
    End If
    
    Set oClients = New CMO.CClients

    If m_xClientIDField = Empty Then
'       get Client ID field
        m_xClientIDField = GetIni("CMFIELDS", "ClientIDField", m_xIni)

'       raise error if missing
        If m_xClientIDField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key '[CMFIELDS]\ClientIDField' is missing or invalid.'"
        End If
    End If
    
    If m_xClientNameField = Empty Then
'       get Client Name field
        m_xClientNameField = GetIni("CMFIELDS", "ClientNameField", m_xIni)

'       raise error if missing
        If m_xClientNameField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key '[CMFIELDS]\ClientNameField' is missing or invalid.'"
        End If
    End If
    
    If IsMissing(ClientFilter) Then
        ClientFilter = Empty
    End If
    
    If IsMissing(AdditionalFilter) Then
        AdditionalFilter = Empty
    Else
        Dim xArr() As String
        ReDim xArr(0)
        iCount = iStringToArray(AdditionalFilter, xArr())
    End If
    
    For l = 1 To m_oAB.AddressBookEntries.Count
        With m_oAB.AddressBookEntries(l)
            On Error Resume Next
            xVal = .Fields(m_xClientIDField, egwString).Value
            xName = .Fields(m_xClientNameField, egwString).Value
            On Error GoTo ProcError
            
            If AdditionalFilter <> Empty Then
'               additional filter exists
                xID = .DisplayName & "^" & .EmailAddress
                For i = 0 To iCount - 1
                    If xID <> xArr(i) Then
                        bInAdditionalFilter = False
                    Else
                        bInAdditionalFilter = True
                        Exit For
                    End If
                Next i
            Else
                bInAdditionalFilter = True
            End If
            
            If xVal <> "" And bInAdditionalFilter Then
                If (ClientFilter = "") And (xVal <> "") Then
                    ReDim Preserve oarr(1, m)
                    oarr(0, m) = xVal
                    oarr(1, m) = xName
                    m = m + 1
                ElseIf IsNumeric(ClientFilter) Then
                    If ClientFilter = xVal Then
                        ReDim Preserve oarr(1, m)
                        oarr(0, m) = xVal
                        oarr(1, m) = xName
                        m = m + 1
                    End If
                ElseIf UCase(xName) Like "*" & UCase(ClientFilter) & "*" Then
                    ReDim Preserve oarr(1, m)
                    oarr(0, m) = xVal
                    oarr(1, m) = xName
                    m = m + 1
                End If
            End If
            xVal = ""
        End With
    Next l
    
    If m > 0 Then
        oClients.ListArray.LoadRows oarr
    Else
        oClients.ListArray.ReDim 0, -1, 0, 1
    End If
    
    Set IClientMatter_Clients = oClients
    
    Exit Function
ProcError:
    RaiseError "mpCMGW.ClientMatter.IClientMatter_Clients"
    Exit Function
End Function

Private Function IClientMatter_IsConnected() As Boolean
'returns TRUE if connection exists to data source
    On Error GoTo ProcError
    If m_oAcct Is Nothing Then
        On Error Resume Next
        OpenConnection
    End If
    IClientMatter_IsConnected = m_xUserName <> ""
    Exit Function
ProcError:
    RaiseError "mpCMGW.CClientMatter.IClientMatter_IsConnected"
    Exit Function
End Function

Private Function IClientMatter_IsValid(ByVal ClientID As Variant, Optional ByVal MatterID As Variant) As Boolean
'returns a populated CMatters collection object
    Dim oClients As CMO.CClients
    Dim oMatters As CMO.CMatters
    Dim oMatter As CMO.CMatter

    On Error GoTo ProcError
    On Error GoTo ProcError
    If IsMissing(MatterID) Then
'       check for client only
        Set oClients = IClientMatter_Clients(ClientID)
        On Error Resume Next
        IClientMatter_IsValid = Not (oClients.Item(ClientID) Is Nothing)
        On Error GoTo ProcError
    Else
'       check for matters
        Set oMatters = IClientMatter_Matters(ClientID)

        On Error Resume Next
        Set oMatter = oMatters.ItemFromID(MatterID)
        On Error GoTo ProcError

        IClientMatter_IsValid = Not oMatter Is Nothing
    End If
    Exit Function
ProcError:
    RaiseError "mpCMGW.ClientMatter.IClientMatter_IsValid"
    Exit Function
End Function

Private Function IClientMatter_Matters(Optional ByVal ClientID As Variant, _
                                       Optional ByVal AdditionalFilter As String, _
                                       Optional ByVal SortBy As CMO.cmSortBy) As CMO.CMatters
    Dim oMatters As CMO.CMatters
    Dim xSQL As String
    Dim lCount As Long
    Dim xOrderField As String
    Dim l As Long
    Dim m As Long
    Dim i As Integer
    Dim oarr() As Variant
    Dim xClientID As String
    Dim xMatterID As String
    
    On Error GoTo ProcError
    
'   connect if necessary
    If m_oAcct Is Nothing Then
        OpenConnection
    End If

    Set oMatters = New CMO.CMatters

    
    If m_xMattersIDField = Empty Then
        
'       get Matters foreign key field for Client
        m_xMattersIDField = GetIni("CMFIELDS", "MattersIDField", m_xIni)
        
'       raise error if missing
        If m_xMattersIDField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key '[CMFIELDS]\MattersIDField' is missing or invalid.'"
        End If
    End If
        
    
    If m_xMattersNameField = Empty Then
        
'       get Matters foreign key field for Client
        m_xMattersNameField = GetIni("CMFIELDS", "MattersNameField", m_xIni)
        
'       raise error if missing
        If m_xMattersNameField = Empty Then
            Err.Raise Error_InvalidParameter, , _
                "The ini key '[CMFIELDS]\MattersNameField' is missing or invalid.'"
        End If
    End If
        
    If IsMissing(AdditionalFilter) Then
        AdditionalFilter = Empty
    End If

'   add sort condition
    If SortBy = cmSortBy_Name Then
'       get field name for name field from ini
        xOrderField = GetIni("CMFIELDS", "MattersNameField", m_xIni)
        If xOrderField = Empty Then
'           no value for key
            Err.Raise Error_InvalidParameter, , _
                "Missing or empty key '[CMFIELDS]\MattersNameField' in mpCMGW.ini"
        End If
    Else
'       get field name for name field from ini
        xOrderField = GetIni("CMFIELDS", "MattersIDField", m_xIni)
        If xOrderField = Empty Then
'           no value for key
            Err.Raise Error_InvalidParameter, , _
                "Missing or empty key '[CMFIELDS]\MattersIDField' in mpCMGW.ini"
        End If
    End If
    
'   Fill matters
    For l = 1 To m_oAB.AddressBookEntries.Count
        With m_oAB.AddressBookEntries(l).Fields
        
            On Error Resume Next
            xClientID = .Item(m_xClientIDField, egwString).Value
            xMatterID = .Item(m_xMattersIDField, egwString).Value
            On Error GoTo ProcError
            
'           fill matters that match clientID and
            If xClientID = ClientID And xMatterID <> Empty Then
                ReDim Preserve oarr(2 + UBound(m_xRelatedFields), m)
                On Error Resume Next
                oarr(0, m) = .Item(m_xMattersIDField, egwString).Value
                oarr(1, m) = .Item(m_xMattersNameField, egwString).Value
                
                For i = 0 To UBound(m_xRelatedFields)
                    oarr(2 + i, m) = .Item(m_xRelatedFields(i), egwString).Value
                Next i
                On Error GoTo ProcError
                m = m + 1
            End If
            
            xClientID = ""
            
        End With
    Next l
    
    If m > 0 Then
        oMatters.ListArray.LoadRows oarr
    Else
        oMatters.ListArray.ReDim 0, -1, 0, 1
    End If
            
    Set IClientMatter_Matters = oMatters
    Exit Function
ProcError:
    RaiseError "mpCMGW.ClientMatter.IClientMatter_Matters"
    Exit Function
End Function

Private Sub RaiseError(Optional ByVal xNewSource As String)
'raises the current error - sets source if source
'has been supplied and is not already set

    If xNewSource <> Empty Then
'       source supplied
        If InStr(Err.Source, ".") = 0 Then
'           source not set
            Err.Source = xNewSource
        End If
    End If
    
'   raises modified error
    Err.Raise Err.Number, Err.Source, Err.Description
End Sub


Public Function GetIni(xSection As String, _
                        xKey As String, _
                        xIni As String) As Variant
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function



Private Function iStringToArray(ByVal xString As String, _
                    arrP() As String, _
                    Optional ByVal xSep As String = ",") As Integer
'fills arrP with items
'separated by xSep in xString
    Dim iNewUBound As Integer
    Dim xNewItem As String
    Dim iCurSepPos As Integer
    
   
'   get current delimiter position
    iCurSepPos = InStr(xString, xSep)
    
    While iCurSepPos
    
'       dim to one greater if last item
'       in array is not empty, else use
'       last item

        If arrP(UBound(arrP)) <> "" Then
            iNewUBound = UBound(arrP) + 1
            ReDim Preserve arrP(iNewUBound)
        Else
            iNewUBound = UBound(arrP)
        End If
        
'       isolate item from string
        xNewItem = Left(xString, iCurSepPos - 1)

'       fill array with new item
        arrP(iNewUBound) = xNewItem
        
'       trim item from original string
        xString = Mid(xString, iCurSepPos + _
                      Len(xSep))
                      
'       search remainder of string for delimiter
        iCurSepPos = InStr(xString, xSep)
    Wend
    
'   dim to one greater if last item
'   in array is not empty, else use
'   last item
    If arrP(UBound(arrP)) <> "" Then
        iNewUBound = UBound(arrP) + 1
        ReDim Preserve arrP(iNewUBound)
    Else
        iNewUBound = UBound(arrP)
    End If
        
'   fill array with new item
    If xString <> "" Then
        arrP(iNewUBound) = xString
        iNewUBound = UBound(arrP) + 1
    End If
    
    iStringToArray = iNewUBound
    
End Function

Private Function LoadRelatedFields()
    Dim i As Integer
    Dim xKey As String
    Dim xRelatedFieldName As String
    
    i = 1
    
'   additional fields
    xKey = "RelatedFields" & i
    
'   name required for Backend to be read
    xRelatedFieldName = GetIni("RELATEDFIELDS", xKey, m_xIni)
    
'   fill related fields array
    While xRelatedFieldName <> ""
        ReDim Preserve m_xRelatedFields(i - 1)
        m_xRelatedFields(i - 1) = xRelatedFieldName

        i = i + 1
        xKey = "RelatedFields" & i
        xRelatedFieldName = GetIni("RELATEDFIELDS", xKey, m_xIni)
    Wend

End Function
Private Function IClientMatter_MattersFiltered(Optional ByVal ParentID As Variant, Optional ByVal MatterFilter As Variant, Optional ByVal AdditionalFilter As String, Optional ByVal SortBy As CMO.cmSortBy) As CMO.CMatters

End Function

Private Function IClientMatter_ClientsByMatter(Optional ByVal MatterFilter As Variant, Optional ByVal AdditionalFilter As String, Optional ByVal SortBy As CMO.cmSortBy) As CMO.CClients

End Function


