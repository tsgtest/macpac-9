VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMatters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CMatter"
Attribute VB_Ext_KEY = "Member0" ,"CMatter"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Option Explicit
Private m_oXArr As XArrayDBObject.XArrayDB
Public Function ItemFromIndex(lIndex As Long) As CMatter
'returns the CMatter object in the specified ordinal position
    Dim oMatter As CMO.CMatter
    
    On Error GoTo ProcError
    With m_oXArr
        If lIndex < .LowerBound(1) Or lIndex > .UpperBound(1) Then
'           the index is out of bounds
            Err.Raise CMO_Error_InvalidItemIndex, , "Invalid Item Index."
            Exit Function
        End If
        
'       create a new cMatter object
        Set oMatter = New CMO.CMatter
        
'       get properties of object
        oMatter.Number = .Value(lIndex, 0)
On Error Resume Next
        oMatter.Name = .Value(lIndex, 1)

'       get related values
        oMatter.RelatedValues = GetRelatedValuesArray(lIndex)
    End With
    Set ItemFromIndex = oMatter
    Exit Function
ProcError:
    RaiseError "CMO.CMatters.ItemFromIndex"
    Exit Function
End Function

Private Function GetRelatedValuesArray(lIndex) As String()
    Dim xValues() As String
    With m_oXArr
'       get number of related values
        Dim iNumValues As Integer
        iNumValues = .Count(2) - 2
        
        If iNumValues > 0 Then
'           there are more than 2 columns in xarray -
'           create array holding related values
            
'           size array to number of related values
            ReDim xValues(iNumValues - 1)
            
'           fill array with values
            Dim i As Integer
            For i = 1 To iNumValues
                xValues(i - 1) = .Value(lIndex, i + 1)
            Next i
            
'           return array
            GetRelatedValuesArray = xValues
        End If
    End With
End Function

Public Function ItemFromID(vKey As Variant) As CMatter
'returns the CMatter object associated with the specified name or id
    Dim oMatter As CMO.CMatter
    Dim l As Long
    
    On Error GoTo ProcError
    
'   id has been supplied
    l = m_oXArr.Find(0, 0, CStr(vKey), , XCOMP_EQ, XTYPE_STRING)

    If l > -1 Then
'       item has been found - create/return cMatter object
        Set oMatter = New CMO.CMatter
        oMatter.Number = m_oXArr.Value(l, 0)
On Error Resume Next
        oMatter.Name = m_oXArr.Value(l, 1)
        
'       get related values
        oMatter.RelatedValues = GetRelatedValuesArray(l)
        
        Set ItemFromID = oMatter
    Else
'       item not found by id
        Err.Raise CMO_Error_InvalidItemIndex, , "Invalid item ID."
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "CMO.CMatters.Item", Err.Description
    Exit Function
End Function

Public Function ItemFromDesc(vKey As Variant) As CMatter
'returns the CMatter object associated with the specified name or id
    Dim oMatter As CMO.CMatter
    Dim l As Long
    
    On Error GoTo ProcError
    
     If Not IsNumeric(vKey) Then
'       check if name has been supplied
        l = m_oXArr.Find(0, 1, CStr(vKey), , XCOMP_EQ, XTYPE_STRING)
'       if not found then try searching id
        If l = -1 Then
            l = m_oXArr.Find(0, 0, CStr(vKey), , XCOMP_EQ, XTYPE_STRING)
        End If
    Else
'       id has been supplied
        l = m_oXArr.Find(0, 0, CLng(vKey), , XCOMP_EQ, XTYPE_LONG)
    End If

    If l > -1 Then
'       item has been found - create/return cMatter object
        Set oMatter = New CMO.CMatter
        oMatter.Number = m_oXArr.Value(l, 0)
        On Error Resume Next
        oMatter.Name = m_oXArr.Value(l, 1)
        
'       get related values
        oMatter.RelatedValues = GetRelatedValuesArray(l)
        
        Set ItemFromDesc = oMatter
    Else
'       item not found
        Err.Raise CMO_Error_InvalidItemIndex, , "Invalid item."
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "CMO.CMatters.Item", Err.Description
    Exit Function
End Function

Public Property Get Count() As Long
    Count = m_oXArr.Count(1)
End Property
Private Sub Class_Initialize()
    Set m_oXArr = New XArrayDBObject.XArrayDB
    m_oXArr.ReDim 0, -1, 0, 1
End Sub
Private Sub Class_Terminate()
    Set m_oXArr = Nothing
End Sub
Public Property Get ListArray() As XArrayDBObject.XArrayDB
    Set ListArray = m_oXArr
End Property
Public Property Set ListArray(oNew As XArrayDBObject.XArrayDB)
    Set m_oXArr = oNew
End Property

