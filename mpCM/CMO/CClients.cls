VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CClients"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CClient"
Attribute VB_Ext_KEY = "Member0" ,"CClient"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Option Explicit
Private m_oXArr As XArrayDBObject.XArrayDB

Public Function ItemFromIndex(lIndex As Long) As CClient
'returns the CClient object in the specified ordinal position
    Dim oClient As CMO.CClient
    
    On Error GoTo ProcError
    With m_oXArr
        If lIndex < .LowerBound(1) Or lIndex > .UpperBound(1) Then
'           the index is out of bounds
            Err.Raise CMO_Error_InvalidItemIndex, , "Invalid Item Index."
            Exit Function
        End If
        
'       create a new cclient object
        Set oClient = New CMO.CClient
        
'       get properties of object
        oClient.Number = .Value(lIndex, 0)
        
        On Error Resume Next
        oClient.Name = .Value(lIndex, 1)
        On Error GoTo ProcError
        
        If m_oXArr.Count(2) = 3 Then
            oClient.ParentID = .Value(lIndex, 2)
        End If
        
    End With
    Set ItemFromIndex = oClient
    Exit Function
ProcError:
    RaiseError "CMO.CClients.ItemFromIndex"
    Exit Function
End Function

Public Function Item(vKey As Variant) As CClient
'returns the CClient object associated with the specified name or id
    Dim oClient As CMO.CClient
    Dim l As Long
    
    l = -1
    
    On Error Resume Next
    
    If Not IsNumeric(vKey) Then
'       check if name has been supplied
        l = m_oXArr.Find(0, 1, CStr(vKey), , XCOMP_EQ, XTYPE_STRING)
'       if not found then try searching id
        If l = -1 Then
            l = m_oXArr.Find(0, 0, CStr(vKey), , XCOMP_EQ, XTYPE_STRING)
        End If
    Else
'       id has been supplied
        l = m_oXArr.Find(0, 0, CLng(vKey), , XCOMP_EQ, XTYPE_LONG)
    End If
    
    On Error GoTo ProcError
    
    If l > -1 Then
'       item has been found - create/return cClient object
        Set oClient = New CMO.CClient
        oClient.Number = m_oXArr(l, 0)
On Error Resume Next
        oClient.Name = m_oXArr(l, 1)
        If m_oXArr.Count(2) = 3 Then
            oClient.ParentID = m_oXArr(l, 2)
        End If
        
        Set Item = oClient
    ElseIf IsNumeric(vKey) Then
'       item not found by id
        Err.Raise CMO_Error_InvalidItemIndex, , "Invalid item id."
        Exit Function
    Else
'       item not found by name or id
        Err.Raise CMO_Error_InvalidItemIndex, , "Invalid item."
        Exit Function
    End If
    
    Exit Function
ProcError:
    RaiseError "CMO.CClients.Item"
    Exit Function
End Function
Public Property Get Count() As Long
    Count = m_oXArr.Count(1)
End Property
Private Sub Class_Initialize()
    Set m_oXArr = New XArrayDBObject.XArrayDB
    m_oXArr.ReDim 0, -1, 0, 1
End Sub
Private Sub Class_Terminate()
    Set m_oXArr = Nothing
End Sub
Public Property Get ListArray() As XArrayDBObject.XArrayDB
    Set ListArray = m_oXArr
End Property
Public Property Set ListArray(oNew As XArrayDBObject.XArrayDB)
    Set m_oXArr = oNew
End Property
