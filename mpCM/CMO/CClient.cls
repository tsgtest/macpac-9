VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CClient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Option Explicit
Private m_xNumber As String
Private m_xParentID As String
Private m_xName As String

Public Property Let Name(ByVal xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Number(ByVal xNew As String)
    m_xNumber = xNew
End Property
Public Property Get Number() As String
    Number = m_xNumber
End Property

Public Property Let ParentID(ByVal xNew As String)
    m_xParentID = xNew
End Property

Public Property Get ParentID() As String
    If m_xParentID = Empty Then
        ParentID = m_xNumber
    Else
        ParentID = m_xParentID
    End If
End Property
