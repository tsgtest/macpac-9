VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IClientMatter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Option Explicit
'**********************************************************
'   IDMS Interface
'   created 01/30/01 by Jeffrey Sweetland

'   Interface used by client/matter classes

'   Member of
'   Container for
'**********************************************************
Enum cmSortBy
    cmSortBy_ID = 1
    cmSortBy_Name = 2
End Enum
Public Function IsConnected() As Boolean

End Function
Public Function Clients(Optional ByVal ClientFilter As Variant, Optional ByVal AdditionalFilter As String, Optional ByVal SortBy As cmSortBy) As CMO.CClients
End Function

Public Function Matters(Optional ByVal ParentID As Variant, Optional ByVal AdditionalFilter As String, Optional ByVal SortBy As cmSortBy) As CMO.CMatters
End Function

Public Function IsValid(ByVal ClientID As Variant, Optional ByVal MatterID As Variant) As Boolean
End Function
Public Function ClientsByMatter(Optional ByVal MatterFilter As Variant, Optional ByVal AdditionalFilter As String, Optional ByVal SortBy As cmSortBy) As CMO.CClients
End Function
Public Function MattersFiltered(Optional ByVal ParentID As Variant, Optional ByVal MatterFilter As Variant, Optional ByVal AdditionalFilter As String, Optional ByVal SortBy As cmSortBy) As CMO.CMatters
End Function
