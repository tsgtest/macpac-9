Attribute VB_Name = "mdlGlobal"
Option Explicit

Public Enum CMO_Errors
    CMO_Error_InvalidItemIndex = vbError + 512 + 1
End Enum

Sub RaiseError(ByVal xSource As String)
    If InStr(Err.Source, ".") Then
        xSource = Err.Source
    End If
    Err.Raise Err.Number, xSource, Err.Description
End Sub
