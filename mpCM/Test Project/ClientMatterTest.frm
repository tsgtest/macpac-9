VERSION 5.00
Object = "{976B1996-3753-11D6-8144-00A0CC58115F}#1.2#0"; "MPCM.ocx"
Begin VB.Form ClientMatterTest 
   Caption         =   "Client Matter Test"
   ClientHeight    =   3300
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7200
   LinkTopic       =   "Form1"
   ScaleHeight     =   3300
   ScaleWidth      =   7200
   StartUpPosition =   2  'CenterScreen
   Begin MPCM.ClientMatterComboBox ClientMatterComboBox1 
      Height          =   315
      Left            =   195
      TabIndex        =   3
      Top             =   135
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   556
      SourceClass     =   "mpCMOLEDB.CClientMatter"
      Col1Caption     =   "No."
      ColWidth1       =   1020
      ColWidth2       =   1020
      ListRows        =   6
      ComboType       =   2
      SelectTextOnFocus=   -1  'True
      PopulateOnLoad  =   0   'False
      AdditionalClientFilter=   ""
      AdditionalMatterFilter=   ""
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Refresh List"
      Height          =   360
      Left            =   5640
      TabIndex        =   2
      Top             =   570
      Width           =   1455
   End
   Begin VB.CommandButton SelectContacts 
      Caption         =   "Select Contacts"
      Height          =   360
      Left            =   5640
      TabIndex        =   1
      Top             =   135
      Width           =   1455
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   630
      Left            =   225
      MultiLine       =   -1  'True
      TabIndex        =   0
      Top             =   2535
      Width           =   4635
   End
End
Attribute VB_Name = "ClientMatterTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub ClientMatterComboBox1_ClientMatterSelected()
'        Dim xRelatedVals() As String
'
'        xRelatedVals = Me.ClientMatterComboBox1.Matter.RelatedValues
'        Me.Text1.Text = Me.ClientMatterComboBox1.Client.Name & vbCrLf & Me.ClientMatterComboBox1.Matter.Description
End Sub

Private Sub ClientMatterComboBox1_ConnectionFailed(ByVal Message As String)
    MsgBox Message & ".  You will not be able to select client matter numbers from the list.", _
        vbExclamation, "Legal MacPac 2000"

    With Me.ClientMatterComboBox1
        .RunConnected = False
        .ComboType = ComboType_TextOnly
    End With
End Sub

Private Sub ClientMatterComboBox1_Validate(Cancel As Boolean)
    If Me.ClientMatterComboBox1.Value <> Empty Then
        Dim iIsValid As cmValidationStatus

        iIsValid = Me.ClientMatterComboBox1.Validity

        If iIsValid = cmValidationStatus_Unknown Then
            'do nothing
        ElseIf iIsValid = cmValidationStatus_Invalid Then
            MsgBox "Invalid client matter number."
            Cancel = True
        Else
'            Dim xRelatedVals() As String
'            On Error Resume Next
'            xRelatedVals = Me.ClientMatterComboBox1.MatterDataVar.RelatedValues
'            Me.Text1.Text = Me.ClientMatterComboBox1Client.Name & vbCrLf &
'                Me.ClientMatterComboBox1.Matter.Name & vbCrLf '& _
'                'Me.ClientMatterComboBox1.Matter.RelatedValues(0)
        End If
    End If
End Sub

Private Sub Command1_Click()
    Me.ClientMatterComboBox1.Refresh
End Sub

'    Dim iIsValid As cmValidationStatus
'
'    If Me.ClientMatterComboBox1.RunConnected And Me.ClientMatterComboBox1.Value <> Empty Then
'
'        iIsValid = Me.ClientMatterComboBox1.Validity
'
'        If iIsValid = cmValidationStatus_Invalid Then
'            MsgBox "Invalid client matter number."
'            Cancel = True
'        Else
''            Dim vRelatedVals As Variant
''            vRelatedVals = Me.ClientMatterComboBox2.MatterDataVar(cmMatterField_RelatedValues)
''            Me.Text1.Text = vRelatedVals(0)
''            Me.Text1.Text = Me.ClientMatterComboBox2.ClientData()(cmClientField_Name)
''            Me.txtReLine.Text = "Matter Name" 'Me.cmbCM.MatterData(cmMatterField_Name)
'        End If
'    End If

'    If Me.ClientMatterComboBox2.Value <> Empty Then
'        Dim iIsValid As cmValidationStatus
'        On Error Resume Next
'        iIsValid = Me.ClientMatterComboBox2.Validity
'
'        If iIsValid = cmValidationStatus_Unknown Then
'            'do nothing
'        ElseIf iIsValid = cmValidationStatus_Invalid Then
'            MsgBox "Invalid client matter number."
'            Cancel = True
'        Else
'            Dim vRelatedVals As Variant
'            vRelatedVals = Me.ClientMatterComboBox2.MatterDataVar(cmMatterField_RelatedValues)
'            Me.Text1.Text = vRelatedVals(0)
'
'        End If
'    End If
'End Sub

Private Sub SelectContacts_Click()
'    Static o As mpCI.Application
'    Static oC As mpCI.Contacts
'
'    If o Is Nothing Then
'        Set o = New mpCI.Application
'        Set oC = o.Contacts
'    End If
'
'    oC.Retrieve mpCI.ciSelectionList_To, False, False, False
'    Dim i As Integer
'    Dim x As String
'
'    If oC.Count Then
'        For i = 1 To oC.Count
'            x = x & oC(i).ListingID & ","
'        Next i
'
'        x = Left(x, Len(x) - 1)
'
'        Me.ClientMatterComboBox2.AdditionalClientFilter = x
'        Me.ClientMatterComboBox2.AdditionalMatterFilter = x
'        Me.ClientMatterComboBox2.Refresh
'
'        If oC.Count = 1 Then
'            ClientMatterComboBox2_Validate , False
'        End If
'    End If
End Sub


Private Sub Form_Load()
'    Me.ClientMatterComboBox1.BackColor = &H80FFFF
End Sub

