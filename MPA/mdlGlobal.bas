Attribute VB_Name = "mdlGlobal"
Option Explicit

'****************
'Constants
'****************

Public Const mpUnderConstruction As String = _
             "Thanks for asking but this feature is still under construction."

Public Const mpAppName As String = _
             "MacPac 9.0 Admin Utility"

Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
    ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Public Declare Function LockWindowUpdate Lib "user32" ( _
    ByVal hwndLock As Long) As Long

Public Const mpDoubleQuotes As String = """"

Public Const g_xProtectedMsg As String = "This is a protected document.  You must first unprotect " & _
                                        "the document before performing this function." & vbCr & _
                                        "Select 'Unprotect Document' from the Tools menu to " & _
                                        "unprotect the document."
                                        
Public Const g_xProtectedMsg12 As String = "This is a protected document.  You must first unprotect " & _
                                        "the document before performing this function." & vbCr & _
                                        "Select 'Protect Document' from the 'Review' tab to " & _
                                        "unprotect the document."
                                        
Public Const g_xProtectedMsg14 As String = "This is a protected document.  You must first unprotect " & _
                                        "the document before performing this function." & vbCr & _
                                        "Select 'Restrict Editing' from the 'Review' tab, or 'Protect Document' from the 'File\Info' menu to " & _
                                        "unprotect the document."

'****************
'Global Vars
'****************
Public g_oFSO As FileSystemObject
Public g_xDocStyles As String
Public g_xBPFiles As String
Public g_xTemplateFiles As String
Public g_frmBP As frmBP


Sub Main()

End Sub
