VERSION 5.00
Begin VB.Form frmDMS 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Document Manager"
   ClientHeight    =   3396
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   2952
   Icon            =   "frmDMS.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3396
   ScaleWidth      =   2952
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   1620
      TabIndex        =   2
      Top             =   2820
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   420
      TabIndex        =   1
      Top             =   2820
      Width           =   1000
   End
   Begin VB.ListBox lstDMS 
      Height          =   2160
      ItemData        =   "frmDMS.frx":000C
      Left            =   225
      List            =   "frmDMS.frx":002E
      TabIndex        =   0
      Top             =   195
      Width           =   2505
   End
End
Attribute VB_Name = "frmDMS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnOK_Click()
    Dim i As Integer
    
    If Me.lstDMS.ListIndex < 8 Then
        i = Me.lstDMS.ListIndex
    Else
        i = Me.lstDMS.ListIndex + 1
    End If
    
    SetMacPacIni "DMS", "DMS", i
    
    Unload Me
    
    MsgBox "You have just changed the document manager that " & _
        "MacPac recognizes on this machine.  To implement this " & _
        "change on all workstations, please distribute the MacPac.ini " & _
        "located in the MacPac90\App directory on this machine.", vbInformation
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    i = GetMacPacIni("DMS", "DMS")
    
    If i >= 9 Then
        i = i - 1
    End If
    
    On Error Resume Next
    Me.lstDMS.ListIndex = i
End Sub
