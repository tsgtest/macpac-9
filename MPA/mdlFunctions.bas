Attribute VB_Name = "mdlFunctions"
Option Explicit

Function bCopyStyle(ByVal xStyleName As String, _
                    ByVal xDestinationPath As String) As Boolean
                    
    Word.Application.OrganizerCopy ActiveDocument.FullName, _
                                   xDestinationPath, _
                                   xStyleName, _
                                   wdOrganizerObjectStyles

End Function

Function LoadStyles() As String
    Dim sty As Word.Style
    Dim x As String
    
    For Each sty In ActiveDocument.Styles
        If sty.InUse Then
            x = x & sty.NameLocal & "|"
        End If
    Next sty
    
'*  set variable
    g_xDocStyles = xTrimTrailingChrs(x, "|")
End Function

Function LoadBPFiles() As String
    Dim xBPPath As String
    Dim x As String
    Dim Folder As Scripting.Folder
    Dim File As Scripting.File
    Dim xUserPath As String
    
'*  get bp path
    xBPPath = BoilerplateDirectory
    xUserPath = UserBoilerplateDirectory
    
    With g_oFSO
'       main boilerplate
        If .FolderExists(xBPPath) Then
            Set Folder = g_oFSO.GetFolder(xBPPath)
        End If
    
        If Not (Folder Is Nothing) Then
            For Each File In Folder.Files
                x = x & File.Name & "|"
            Next File
        End If
    
'       user boilerplate
        If xUserPath <> xBPPath Then
            If .FolderExists(xUserPath) Then
                Set Folder = g_oFSO.GetFolder(xUserPath)
            End If
        
            If Not (Folder Is Nothing) Then
                For Each File In Folder.Files
                    x = x & File.Name & "|"
                Next File
            End If
        End If
    End With
    
'*  set variable
    g_xBPFiles = xTrimTrailingChrs(x, "|")
    
End Function

Function LoadTemplateFiles() As String
    Dim xTemplatesPath As String
    Dim x As String
    Dim Folder As Scripting.Folder
    Dim File As Scripting.File
    Dim xUserPath As String
    
'*  get bp path
    xTemplatesPath = mpBase2.TemplatesDirectory
    
    With g_oFSO
'       main boilerplate
        If .FolderExists(xTemplatesPath) Then
            Set Folder = g_oFSO.GetFolder(xTemplatesPath)
        End If
    
        If Not (Folder Is Nothing) Then
            For Each File In Folder.Files
                x = x & File.Name & "|"
            Next File
        End If
    
    End With
    
'*  set variable
    g_xTemplateFiles = xTrimTrailingChrs(x, "|")
    
End Function


Sub EchoOff()
    Call LockWindowUpdate(FindWindow("OpusApp", 0&))
End Sub

Sub EchoOn()
    Call LockWindowUpdate(0&)
End Sub

Public Function DeleteAutoExec()
'***This function is needed to rewrite code to the
'***existing Normal.dot "AutoClose" event.
    Dim numlines As Integer
    Dim startnum As Integer
    
    On Error GoTo bUpdate_ERROR
    
    numlines = 0
    startnum = 0

    With ActiveDocument.VBProject.VBComponents("mdlAuto").CodeModule
    
'       Get the Total number of lines used by the existing procedure.
        numlines = .ProcCountLines("AutoExec", vbext_pk_Proc)

'       Get the Starting position (Line number)
        startnum = .ProcStartLine("AutoExec", vbext_pk_Proc)

'       Now that we have the Starting position of the procedure
'       as well as the total number of lines, we can delete
        .DeleteLines startline:=startnum, Count:=numlines
    
    End With
    
    Exit Function

bUpdate_ERROR:
    Exit Function
End Function

Public Function WriteAutoExec()
    Dim numlines As Integer
    
    On Error GoTo bUpdate_ERROR
    
    numlines = 0

'   Set the object = to the "mdlAuto" code module
    With ActiveDocument.VBProject.VBComponents("mdlAuto").CodeModule

'       Get the Total number of lines used by the existing procedure
        numlines = .CountOfLines

        .InsertLines numlines + 2, "Sub AutoExec()"
        .InsertLines numlines + 3, "    Set o_DMSOpenInt = New clsDMSOpenInt"
        .InsertLines numlines + 4, "End Sub"

    End With
    
    Exit Function

bUpdate_ERROR:
    Exit Function
End Function


Function ClearOutProps(oDoc As Word.Document)
    On Error GoTo ProcError
    With oDoc.BuiltInDocumentProperties
        .Item(wdPropertyAuthor).Value = ""
        .Item(wdPropertyTitle).Value = ""
        .Item(wdPropertySubject).Value = ""
        .Item(wdPropertyCompany).Value = ""
        .Item(wdPropertyManager).Value = ""
    End With
    Exit Function
ProcError:
    MsgBox Err.Number & ":" & Err.Description
End Function

Public Function IsValidEnvironment(Optional ByVal bRequireActiveDoc As Boolean = True, _
                                   Optional ByVal bAllowProtectedDoc As Boolean = False, _
                                   Optional ByVal bAllowPrintPreview As Boolean = False) As Boolean
'returns TRUE if the current environment meets the
'supplied conditions - displays alerts if appropriate
    Dim xMsg As String
    Dim iChoice As VbMsgBoxResult
    Dim xVersion As String
    
    On Error GoTo ProcError
    IsValidEnvironment = True
    
    If bRequireActiveDoc And Application.Windows.Count = 0 Then
        MsgBox "Please open a document before running this function.", vbExclamation
        IsValidEnvironment = False
    ElseIf (Not bAllowProtectedDoc) And (Not (ActiveDocument.ProtectionType = wdNoProtection)) Then
        xVersion = xWordVersion()
        Select Case xVersion
            Case "12"
                xMsg = g_xProtectedMsg12
            Case "14", "15"
                xMsg = g_xProtectedMsg14
            Case Else
                xMsg = g_xProtectedMsg
        End Select
        
        MsgBox xMsg, vbExclamation
        
        IsValidEnvironment = False
        Exit Function
    ElseIf (Not bAllowPrintPreview) And (Word.ActiveDocument.ActiveWindow.View = wdPrintPreview) Then
'       prompt user to switch to normal view
        xMsg = "This function cannot execute when the active document is in print preview.  " & _
            vbCr & "Would you like to switch to Normal view?"
            
        iChoice = MsgBox(xMsg, vbExclamation + vbYesNo)
        If iChoice = vbYes Then
'           switch to normal view
            Word.ActiveDocument.ActiveWindow.View = wdNormalView
        Else
            IsValidEnvironment = False
        End If
        Exit Function
    End If
    Exit Function
ProcError:
    MsgBox Err.Number & ":" & Err.Description
    Exit Function
End Function

Public Function xWordVersion() As String
    If (Val(Word.Application.Version) >= 12) And (Val(Word.Application.Version) < 13) Then
        xWordVersion = "12"
    ElseIf (Val(Word.Application.Version) >= 14) And (Val(Word.Application.Version) < 15) Then
        xWordVersion = "13"
    ElseIf (Val(Word.Application.Version) >= 15) Then
        xWordVersion = "15"
    Else
        xWordVersion = "11"
    End If
    
End Function
