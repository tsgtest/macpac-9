VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmBP 
   Caption         =   "View Boilerplate"
   ClientHeight    =   5940
   ClientLeft      =   60
   ClientTop       =   348
   ClientWidth     =   3840
   Icon            =   "frmBP.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5940
   ScaleWidth      =   3840
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Interval        =   200
      Left            =   30
      Top             =   720
   End
   Begin TrueDBList60.TDBList lstFiles 
      Height          =   5940
      Left            =   0
      OleObjectBlob   =   "frmBP.frx":000C
      TabIndex        =   0
      Top             =   0
      Width           =   3840
   End
End
Attribute VB_Name = "frmBP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
Private m_xCurFile As String
Private Sub Form_Load()
    Dim oFiles As xArray
    Dim oX As XHelper
    Dim xLeft As String
    Dim xIni As String
    
    xIni = mpBase2.ApplicationDirectory & "\MPA.ini"
    
'*  load variables
    LoadBPFiles
    
'*  set controls
    Set oX = New XHelper
    Set oFiles = xarStringToxArray(g_xBPFiles)
    oX.Sort oFiles, 0
    Me.lstFiles.Array = oFiles
    
'    xLeft = GetIni("frmBP", "Left", xIni)
'    If xLeft <> "" Then
'        Me.Left = xLeft
'        Me.Top = GetIni("frmBP", "Top", xIni)
'        Me.Width = GetIni("frmBP", "Width", xIni)
'        Me.Height = GetIni("frmBP", "Height", xIni)
'    Else
''       position at left of screen
'        Me.Left = Screen.Width - Me.Width
'        Me.Top = (Screen.Height / 2) - (Me.Width / 2)
'    End If
End Sub

Private Sub Form_Resize()
    Me.lstFiles.Height = Me.ScaleHeight
    Me.lstFiles.Width = Me.ScaleWidth
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim xIni As String
    
'    xIni = mpBase2.ApplicationDirectory & "\MPA.ini"
'    SetIni "frmBP", "Left", Me.Left, xIni
'    SetIni "frmBP", "Top", Me.Top, xIni
'    SetIni "frmBP", "Height", Me.Height, xIni
'    SetIni "frmBP", "Width", Me.Width, xIni
    
    Set frmBP = Nothing
    Exit Sub
ProcError:
    MsgBox Error(Err)
End Sub

Private Sub lstFiles_DblClick()
    Dim i As Integer
    
    On Error Resume Next
    Word.Documents(m_xCurFile).Close
    m_xCurFile = BoilerplateDirectory & Me.lstFiles.BoundText
    On Error GoTo ProcError
    Word.Documents.Open m_xCurFile
    Exit Sub
ProcError:
    Select Case Err.Number
        Case 5174   'file not found
'           try user boilerplate
            If i > 1 Then Exit Sub
            m_xCurFile = UserBoilerplateDirectory & Me.lstFiles.BoundText
            i = i + 1
            Resume
        Case Else
            MsgBox Error(Err) & " - (#" & Err.Number & ")"
    End Select
End Sub

Private Sub lstFiles_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        lstFiles_DblClick
    End If
End Sub
