VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CApplication"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long


'*****************************
'Admin Functions
'*****************************

Public Sub SelectDMS()
    Dim dlg As frmDMS
    Set dlg = New frmDMS
    dlg.Show vbModal
    Unload dlg
    Set dlg = Nothing
End Sub

Public Sub EditBoilerplate()
    If g_frmBP Is Nothing Then _
        Set g_frmBP = New frmBP
    g_frmBP.Show vbModeless
End Sub

Public Sub RemoveTestDocs()
    Dim doc As Word.Document
    
    Application.ScreenUpdating = False
    For Each doc In Documents
        If UCase(doc.Name) Like "DOCUMENT*" Or _
        UCase(doc.Name) Like "LABEL*" Then
            With doc
        '---9.5.0 - mc - new MacPac menu issue where users are asked to save .dot
                If InStr(.Name, ".dot") = 0 Then
                    .AttachedTemplate.Saved = True
                End If
        '---End 9.5.0
                .Saved = True
                .Close
            End With
        End If
    Next doc
    Application.ScreenUpdating = True
    Application.ScreenRefresh
End Sub

Public Sub IntegrateDocID()
    Dim dlg As frmDocIDIntegration
    
    Select Case mpBase2.GetMacPacIni("DMS", "DMS")
        Case 3, 6, 7
            MsgBox "This function is unavailable for the current Document Manager.  " & _
            "Please contact your administrator.", vbInformation, App.Title
            Exit Sub
        Case Else
    End Select
    
    Set dlg = New frmDocIDIntegration
    dlg.Show vbModal
    Unload dlg
    Set dlg = Nothing
End Sub

Public Sub CopyStyles()
    Dim dlg As frmCopyStyles
    
    If Not IsValidEnvironment() Then
        Exit Sub
    End If
    
    Set dlg = New frmCopyStyles
    dlg.Show vbModal
    Unload dlg
    Set dlg = Nothing
End Sub

Public Sub ShowDocVars()
    Dim dlgDocVars As frmDocVars
    Set dlgDocVars = New frmDocVars
    dlgDocVars.Show vbModal
    Unload dlgDocVars
    Set dlgDocVars = Nothing
End Sub

Public Sub EditMacPacIni()
    Dim xPath As String
    
    xPath = mpBase2.ApplicationDirectory & "\MacPac.ini"
    
    If g_oFSO.FileExists(xPath) Then
        Shell "notepad.exe " & xPath, _
              vbNormalFocus
    Else
        MsgBox xPath & " could not be found.", _
               vbCritical, _
               App.Title
    End If
    
End Sub

Public Sub EditUserIni()
    Dim xPath As String
    
    xPath = mpBase2.UserFilesDirectory & "user.ini"
        
    If g_oFSO.FileExists(xPath) Then
        Shell "notepad.exe " & xPath, _
              vbNormalFocus
    Else
        MsgBox xPath & " could not be found.", _
               vbCritical, _
               App.Title
    End If
    
End Sub

Public Sub EditPublicDB()
    Dim xPath As String
    
    xPath = mpBase2.ApplicationDirectory & "\mpPublic.mdb"
        
    If g_oFSO.FileExists(xPath) Then
        ShellExecute 0&, "Open", mpDoubleQuotes & xPath & mpDoubleQuotes, vbNullString, vbNullString, 1
    Else
        MsgBox xPath & " could not be found.", _
               vbCritical, _
               App.Title
    End If
    
End Sub

Public Sub EditPeopleDB()
    Dim xPath As String
    
    xPath = mpBase2.ApplicationDirectory & "\mpPeople.mdb"
        
    If g_oFSO.FileExists(xPath) Then
        ShellExecute 0&, "Open", mpDoubleQuotes & xPath & mpDoubleQuotes, vbNullString, vbNullString, 1
    Else
        MsgBox xPath & " could not be found.", _
               vbCritical, _
               App.Title
    End If
    
End Sub

Public Sub EditPrivateDB()
    Dim xPath As String
    
    xPath = mpBase2.UserFilesDirectory & "mpPrivate.mdb"
        
    If g_oFSO.FileExists(xPath) Then
        ShellExecute 0&, "Open", mpDoubleQuotes & xPath & mpDoubleQuotes, vbNullString, vbNullString, 1
    Else
        MsgBox xPath & " could not be found.", _
               vbCritical, _
               App.Title
    End If
    
End Sub

Public Sub EditmpN90Ini()
    Dim xPath As String
    
    xPath = mpBase2.ApplicationDirectoryNumbering & "\mpN90.ini"
    
    If g_oFSO.FileExists(xPath) Then
        Shell "notepad.exe " & xPath, _
              vbNormalFocus
    Else
        MsgBox xPath & " could not be found.", _
               vbCritical, _
               App.Title
    End If
End Sub

Public Sub EditmpCIini()
    Dim xPath As String
    
    xPath = mpBase2.xSubstitute(App.Path, "MacPac90", "CI") & "\mpCI.ini"
    
    If g_oFSO.FileExists(xPath) Then
        Shell "notepad.exe " & xPath, _
              vbNormalFocus
    Else
        xPath = mpBase2.xSubstitute(App.Path, "MacPac90\App", "CI") & "\mpCI.ini"
        If g_oFSO.FileExists(xPath) Then
            Shell "notepad.exe " & xPath, _
                  vbNormalFocus
        Else
            MsgBox xPath & " could not be found.", _
                   vbCritical, _
                   App.Title
        End If
    End If
End Sub

Public Sub EditCIini()
    Dim xPath As String
    
    xPath = mpBase2.ApplicationDirectoryCI & "\CI.ini"
    
    If g_oFSO.FileExists(xPath) Then
        Shell "notepad.exe " & xPath, _
              vbNormalFocus
    Else
        MsgBox xPath & " could not be found.", _
               vbCritical, _
               App.Title
    End If
    
End Sub

Public Function Version() As String
    On Error Resume Next
    Version = App.Major & "." & App.Minor & "." & App.Revision
End Function

Public Sub CleanTemplateDocProps()
    Dim dlg As frmBPClean
    Dim i As Integer
    Dim xarr As xArray
    Dim SelBks As TrueDBList60.SelBookmarks
    Dim oDoc As Word.Document
    Dim xFile As String
    
    LoadTemplateFiles
    
    Set dlg = New frmBPClean
    
    On Error GoTo ProcError
    
    With dlg
        .FileList = g_xTemplateFiles
        .Caption = "Cleanup Templates"
        .Show vbModal
        
        Set SelBks = .lstFiles.SelBookmarks
        
        Set xarr = .lstFiles.Array
        
        EchoOff
        For i = 0 To SelBks.Count - 1
            xFile = mpBase2.TemplatesDirectory & xarr(SelBks(i))
            Set oDoc = Word.Documents.Open(Chr(34) & xFile & Chr(34))
            ClearOutProps oDoc
            
            'dirty document (issue seen at DOJ with Word 2007)
            Selection.InsertAfter " "
            Selection.Delete
            
            ActiveDocument.Save
            ActiveDocument.Close
        Next i
        EchoOn
    End With
    
    Unload dlg
    Set dlg = Nothing
    Exit Sub
ProcError:
    EchoOn
    Select Case Err.Number
        Case Else
            MsgBox Error(Err) & " - (#" & Err.Number & ")"
    End Select
End Sub

Public Sub CleanBPDocProps()
    Dim dlg As frmBPClean
    Dim i As Integer
    Dim xarr As xArray
    Dim SelBks As TrueDBList60.SelBookmarks
    Dim oDoc As Word.Document
    Dim xFile As String
    
    LoadBPFiles
    
    Set dlg = New frmBPClean
    
    On Error GoTo ProcError
    
    With dlg
        .FileList = g_xBPFiles
        .Caption = "Cleanup Boilerplates"
        .Show vbModal
        
        Set SelBks = .lstFiles.SelBookmarks
        
        Set xarr = .lstFiles.Array
        
        EchoOff
        
        For i = 0 To SelBks.Count - 1
            xFile = BoilerplateDirectory & xarr(SelBks(i))
            Set oDoc = Word.Documents.Open(xFile)
            ClearOutProps oDoc
            'dirty document (issue seen at DOJ with Word 2007)
            Selection.InsertAfter " "
            Selection.Delete
            oDoc.Close wdSaveChanges
        Next i
        
        EchoOn
    End With
    
    Unload dlg
    Set dlg = Nothing
    Exit Sub
ProcError:
    Select Case Err.Number
        Case 5174   'file not found
'           try user boilerplate
            If i > 1 Then Exit Sub
            xFile = UserBoilerplateDirectory & xarr(SelBks(i))
            i = i + 1
            Resume
        Case Else
            EchoOn
            MsgBox Error(Err) & " - (#" & Err.Number & ")"
    End Select
 
End Sub


Public Sub SetTemplatesCompatibility()
    Dim dlg As frmBPClean
    Dim i As Integer
    Dim xarr As xArray
    Dim SelBks As TrueDBList60.SelBookmarks
    Dim oDoc As Word.Document
    Dim xFile As String
    
    LoadTemplateFiles
    
    Set dlg = New frmBPClean
    
    On Error GoTo ProcError
    
    With dlg
        .FileList = g_xTemplateFiles
        .Caption = "Set Word Compatibility"
        .cmdCleanup.Caption = "Set"
        .Show vbModal
        
        Set SelBks = .lstFiles.SelBookmarks
        
        Set xarr = .lstFiles.Array
        
        EchoOff
        For i = 0 To SelBks.Count - 1
            xFile = mpBase2.TemplatesDirectory & xarr(SelBks(i))
            Set oDoc = Word.Documents.Open(xFile)
            
            Application.Dialogs(wdDialogToolsOptionsCompatibility).Show
            
            oDoc.Close wdSaveChanges
        Next i
        EchoOn
    End With
    
    Unload dlg
    Set dlg = Nothing
    Exit Sub
ProcError:
    EchoOn
    Select Case Err.Number
        Case Else
            MsgBox Error(Err) & " - (#" & Err.Number & ")"
    End Select
End Sub

Public Sub SetBoilerplateCompatibility()
    Dim dlg As frmBPClean
    Dim i As Integer
    Dim xarr As xArray
    Dim SelBks As TrueDBList60.SelBookmarks
    Dim oDoc As Word.Document
    Dim xFile As String
    
    LoadBPFiles
    
    Set dlg = New frmBPClean
    
    On Error GoTo ProcError
    
    With dlg
        .FileList = g_xBPFiles
        .Caption = "Set Word Compatibility"
        .cmdCleanup.Caption = "Set"
        .Show vbModal
        
        Set SelBks = .lstFiles.SelBookmarks
        
        Set xarr = .lstFiles.Array
        
        EchoOff
        For i = 0 To SelBks.Count - 1
            xFile = mpBase2.BoilerplateDirectory & xarr(SelBks(i))
            Set oDoc = Word.Documents.Open(xFile)
            
            Application.Dialogs(wdDialogToolsOptionsCompatibility).Show
            
            oDoc.Close wdSaveChanges
        Next i
        EchoOn
    End With
    
    Unload dlg
    Set dlg = Nothing
    Exit Sub
ProcError:
    EchoOn
    Select Case Err.Number
        Case Else
            MsgBox Error(Err) & " - (#" & Err.Number & ")"
    End Select
End Sub


Public Sub CleanBPFiles()
    Dim lt As Word.ListTemplate
    Dim i As Integer
    Dim j As Integer
    Dim s As Word.Style
    Dim oDoc As Word.Document
    Dim fFile As File
    Dim xPath As String
    Dim fFolder As Folder
    Dim iAnswer As Integer
    Dim iCount As Integer
    
    On Error Resume Next
    
    iAnswer = MsgBox("This macro will cycle through all the files in your boilerplate directory and do the following:" & vbCr & vbCr & _
                     "1. Set the name property of an existing 'HeadingStyles' ListTemplate to an empty string" & vbCr & _
                     "2. Remove Outline Numbering from Heading 1 - 9 styles" & vbCr & _
                     "3. Delete the 'zzmpFixedCurScheme' variable" & vbCr & vbCr & "Continue?", _
                     vbYesNo, App.Title)
                     
    If iAnswer = vbYes Then
    
'       get boilerplate folder path
        xPath = mpBase2.BoilerplateDirectory
        
        If g_oFSO.FolderExists(xPath) Then
            Set fFolder = g_oFSO.GetFolder(xPath)
        Else
            MsgBox xPath & " folder could not be found", _
                   vbCritical, _
                   App.Title
            Exit Sub
        End If
        
        EchoOff
        
        For Each fFile In fFolder.Files
'           get document object
            Set oDoc = Documents.Open(fFile.Path)

            With oDoc
'               delete zzmpFixedCurScheme variable
                .Variables("zzmpFixedCurScheme").Delete
                
                For Each lt In .ListTemplates
                    If InStr(UCase(lt.Name), "HEADINGSTYLES") Then
                        lt.Name = ""
                    End If
                Next lt
                                
'               make sure Heading 1-9 are not outlinenumbered
                For i = 1 To 9
                    Set s = .Styles("Heading " & i)
                    With s
                        If .InUse Then
                            If Not (.ListTemplate Is Nothing) Then
                                Set lt = .ListTemplate
                                For j = 1 To 9
                                    lt.ListLevels(j).LinkedStyle = ""
                                Next j
                            End If
                        End If
                    End With
                Next i
                .Save
                .Close
            End With
            Next fFile
        
        EchoOn
        
        With Application
            .ScreenUpdating = True
            .ScreenRefresh
            .StatusBar = "Finished!"
        End With
        
    End If
    
End Sub

Public Sub HeadingStylesInBP()
    Dim lt As Word.ListTemplate
    Dim i As Integer
    Dim s As Word.Style
    Dim oDoc As Word.Document
    Dim fFile As File
    Dim xPath As String
    Dim fFolder As Folder
    Dim xMessage As String
    
'   get boilerplate folder path
    xPath = mpBase2.BoilerplateDirectory
    
    If g_oFSO.FolderExists(xPath) Then
        Set fFolder = g_oFSO.GetFolder(xPath)
    Else
        MsgBox xPath & " folder could not be found", _
               vbCritical, _
               App.Title
        Exit Sub
    End If
    
    EchoOff
    
    For Each fFile In fFolder.Files
'       get document object
        Set oDoc = Documents.Open(fFile.Path)
        
        With oDoc
'           check if Heading styles are outline numbered
            For i = 1 To 9
                Set s = .Styles("Heading " & i)
                With s
                    If .InUse Then
                        xMessage = xMessage & oDoc.Name & vbCr
                        Exit For
                    End If
                End With
            Next i
            .Saved = True
            .Close
        End With
    Next fFile

    EchoOn
    
    If xMessage = "" Then
        MsgBox "None of the boilerplate files have Heading styles in use!", , App.Title
    Else
        Clipboard.SetText xMessage
        MsgBox "The following files have Heading styles in use:" & _
               vbCr & vbCr & xMessage, , _
               App.Title
    End If
        
End Sub

Function SetTrailerIntegration(iDMS)
    Dim o As Word.Template
    Dim iVer As Integer
    Dim xFile As String
    
    On Error GoTo ProcError
    
    iVer = Int(Left(Word.Application.Version, InStr(Word.Application.Version, ".") - 1))

    If iVer < 12 Then
        xFile = Word.Application.Addins("@@MP90.dot").Path & "\@@MP90.dot"
    Else
        xFile = Word.Application.Addins("@@MP90.dotm").Path & "\@@MP90.dotm"
    End If

    Set o = Templates(xFile)
'    Set o = Templates(Word.Application.StartupPath & "\@@MP90.dot")
    With o.VBProject.VBComponents("mdlDMS").CodeModule
        .DeleteLines 1, .CountOfLines
    
        If iDMS And &H1 Then
            .AddFromString GetIntegrationCode("FileSave")
        End If
        If iDMS And &H2 Then
            .AddFromString GetIntegrationCode("FileSaveAs")
        End If
        If iDMS And &H4 Then
            .AddFromString GetIntegrationCode("FileClose")
        End If
        If iDMS And &H8 Then
            .AddFromString GetIntegrationCode("DocClose")
        End If
        If iDMS And &H10 Then
            .AddFromString GetIntegrationCode("FileOpen")
        End If
        If iDMS And &H20 Then
            .AddFromString GetIntegrationCodeBasic("FilePrint")
        End If
        If iDMS And &H40 Then
            .AddFromString GetIntegrationCodeBasic("FilePrintDefault")
        End If
    End With
    o.Saved = True
    Exit Function
ProcError:
    MsgBox Err.Description, vbExclamation, App.Title
End Function

Function GetIntegrationCode(xFunctionName As String) As String
    Dim x As String
    x = "Sub " & xFunctionName & "()" & vbCr & _
        "    On Error GoTo ProcError" & vbCr & _
        "    Dim oDoc As Word.Document" & vbCr & _
        "    Set oDoc = Word.ActiveDocument" & vbCr & _
        "    If Not (MacPac Is Nothing) Then" & vbCr & _
        "        MacPac." & xFunctionName & vbCr & _
        "    else" & vbCr & _
        "       ' Wait for Diagnosis Report to complete" & vbCr & _
        "       While Tasks.Exists(" & """" & "mpDiagnose" & """" & ")" & vbCr & _
        "           DoEvents" & vbCr & _
        "       Wend" & vbCr & _
        "       oDoc.Activate" & vbCr & _
        "       DMS" & xFunctionName & vbCr & _
        "    End If" & vbCr & _
        "    Exit Sub" & vbCr & _
        "ProcError:" & vbCr & vbCr & _
        "    Exit Sub" & vbCr & _
        "End Sub" & vbCr & vbCr
    GetIntegrationCode = x
End Function

Function GetIntegrationCodeBasic(xFunctionName As String) As String
    Dim x As String
    x = "Sub " & xFunctionName & "()" & vbCr & _
        "    On Error GoTo ProcError" & vbCr & _
        "    If Not (MacPac Is Nothing) Then" & vbCr & _
        "        MacPac." & xFunctionName & vbCr & _
        "    End If" & vbCr & _
        "    Exit Sub" & vbCr & _
        "ProcError:" & vbCr & vbCr & _
        "    Exit Sub" & vbCr & _
        "End Sub" & vbCr & vbCr
    GetIntegrationCodeBasic = x
End Function

'*****************************
'Class Events
'*****************************
Private Sub Class_Initialize()
    Set g_oFSO = New FileSystemObject
End Sub

Private Sub Class_Terminate()
    On Error Resume Next
    If Not g_frmBP Is Nothing Then
        g_frmBP.Hide
        Unload g_frmBP
        Set g_frmBP = Nothing
    End If
    Set g_oFSO = Nothing
End Sub


