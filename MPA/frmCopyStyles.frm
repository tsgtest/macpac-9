VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmCopyStyles 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Copy Styles To Selected Boilerplate Files"
   ClientHeight    =   5028
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   7464
   Icon            =   "frmCopyStyles.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5028
   ScaleWidth      =   7464
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnCopyAll 
      Caption         =   ">>"
      CausesValidation=   0   'False
      Height          =   400
      Left            =   2985
      TabIndex        =   3
      ToolTipText     =   "Copy All Styles"
      Top             =   1275
      Width           =   555
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   400
      Left            =   6225
      TabIndex        =   6
      Top             =   4500
      Width           =   1000
   End
   Begin VB.CommandButton btnCopy 
      Caption         =   ">"
      Height          =   400
      Left            =   2985
      TabIndex        =   2
      ToolTipText     =   "Copy Selected Styles"
      Top             =   765
      Width           =   555
   End
   Begin TrueDBList60.TDBList lstFiles 
      Height          =   4050
      Left            =   3675
      OleObjectBlob   =   "frmCopyStyles.frx":000C
      TabIndex        =   5
      Top             =   330
      Width           =   3600
   End
   Begin TrueDBList60.TDBList lstStyles 
      Height          =   4050
      Left            =   165
      OleObjectBlob   =   "frmCopyStyles.frx":1E0C
      TabIndex        =   1
      Top             =   330
      Width           =   2670
   End
   Begin VB.Label lblBoilerplates 
      Caption         =   "&Boilerplate Files:"
      Height          =   225
      Left            =   3705
      TabIndex        =   4
      Top             =   105
      Width           =   3105
   End
   Begin VB.Label lblStyles 
      Caption         =   "&Styles in Active Document:"
      Height          =   225
      Left            =   195
      TabIndex        =   0
      Top             =   105
      Width           =   2820
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmCopyStyles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_bCancelled As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Private Sub chkCopyAll_Click()

End Sub

Private Sub SelectAll()
    Dim i As Integer
    For i = 0 To Me.lstStyles.Array.Count(1) - 1
        Me.lstStyles.SelBookmarks.Add i
    Next i
End Sub

Private Sub btnCopyAll_Click()
    CopySelStyles
End Sub

Private Sub btnCopy_Click()
    CopySelStyles
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub CopyAllStyles()
    Dim dlg As New frmCopyStyles
    Dim i As Integer
    Dim j As Integer
    Dim xStyle As String
    Dim xBP As String
    Dim sty As Word.Style
    Dim oSelFiles As TrueDBList60.SelBookmarks
    Dim oDoc As Word.Document
    
    On Error GoTo ProcError
    With Word.Application
        .StatusBar = "Copying styles..."
        .ScreenUpdating = False
    End With
    
    Set oSelFiles = Me.lstFiles.SelBookmarks
    Set oDoc = Word.ActiveDocument
    
    For j = 0 To oSelFiles.Count - 1
        xBP = BoilerplateDirectory & _
                Me.lstFiles.Columns(0).CellText(oSelFiles(j))
        Word.Documents.Open xBP, , , False
        Word.ActiveDocument.ActiveWindow.WindowState = wdWindowStateMinimize
        oDoc.Activate
        Word.Application.ScreenUpdating = True
        Word.Application.ScreenRefresh
        For i = 0 To Me.lstStyles.Array.Count(1) - 1
'*          get selected styles
            xStyle = Me.lstStyles.Columns(0).CellText(i)
    
'*          copy style
            bCopyStyle xStyle, xBP
        Next i
        'retro0705
        With Word.Documents(xBP)
            .Save
            .Close
        End With
    Next j
    
    With Word.Application
        .StatusBar = "Finished"
        .ScreenUpdating = True
        .ScreenRefresh
    End With
    Exit Sub
ProcError:
    MsgBox Error(Err)
    Exit Sub
End Sub

Private Sub CopySelStyles()
    Dim dlg As New frmCopyStyles
    Dim i As Integer
    Dim j As Integer
    Dim xStyle As String
    Dim xBP As String
    Dim sty As Word.Style
    Dim oSelFiles As TrueDBList60.SelBookmarks
    Dim oSelStyles As TrueDBList60.SelBookmarks
    Dim oDoc As Word.Document
    
    On Error GoTo ProcError
    With Word.Application
        .StatusBar = "Copying styles..."
        .ScreenUpdating = False
    End With
    
    Set oSelFiles = Me.lstFiles.SelBookmarks
    Set oSelStyles = Me.lstStyles.SelBookmarks
    Set oDoc = Word.ActiveDocument
    
    For j = 0 To oSelFiles.Count - 1
        xBP = BoilerplateDirectory & _
                Me.lstFiles.Columns(0).CellText(oSelFiles(j))
        If Dir(xBP) = Empty Then
            xBP = mpBase2.UserBoilerplateDirectory & _
                    Me.lstFiles.Columns(0).CellText(oSelFiles(j))
        End If
        Word.Documents.Open xBP, , , False
        Word.ActiveDocument.ActiveWindow.WindowState = wdWindowStateMinimize
        oDoc.Activate
        Word.Application.ScreenUpdating = True
        Word.Application.ScreenRefresh
        Word.Application.ScreenUpdating = False
'        Word.AddIns.Add xBP, True
        For i = 0 To oSelStyles.Count - 1
'*          get selected styles
            xStyle = Me.lstStyles.Columns(0).CellText(oSelStyles(i))
    
'*          copy style
            
            bCopyStyle xStyle, xBP
        Next i
        'retro0705
        With Word.Documents(xBP)
            .Save
            .Close
        End With
    Next j
    
    With Word.Application
        .StatusBar = "Finished"
        .ScreenUpdating = True
        .ScreenRefresh
    End With
    Exit Sub
ProcError:
    MsgBox Error(Err)
    Exit Sub
End Sub

Private Sub Form_Activate()
    Me.Initializing = False
End Sub

Private Sub Form_Load()
    Dim oFiles As xArray
    Dim oStyles As xArray
    Dim oX As XHelper
    Me.Initializing = True
    
'*  load variables
    LoadStyles
    LoadBPFiles
    
'*  set controls
    With Me.lstStyles
        Set oFiles = xarStringToxArray(g_xDocStyles)
        Set oX = New XHelper
        oX.Sort oFiles, 0
        .Array = oFiles
        .SelectedItem = .FirstRow
    End With
    
    Set oStyles = xarStringToxArray(g_xBPFiles)
    oX.Sort oStyles, 0
    Me.lstFiles.Array = oStyles
    
'*  set caption
    Me.lblStyles.Caption = "Styles in : " & ActiveDocument.Name
End Sub

Private Sub btnOK_Click()
    Me.Hide
    Me.Cancelled = False
End Sub

Private Sub btnCancel_Click()
    Me.Hide
    Me.Cancelled = True
End Sub
