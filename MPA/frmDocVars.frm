VERSION 5.00
Begin VB.Form frmDocVars 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Manage Document Variables"
   ClientHeight    =   5220
   ClientLeft      =   1728
   ClientTop       =   2280
   ClientWidth     =   6900
   Icon            =   "frmDocVars.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   6900
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnReport 
      Caption         =   "&Create Report"
      Height          =   360
      Left            =   5580
      TabIndex        =   8
      ToolTipText     =   "Create Word Document Listing Document Variables and Values for this Document"
      Top             =   3975
      Width           =   1160
   End
   Begin VB.ListBox lstVarNames 
      Height          =   2460
      IntegralHeight  =   0   'False
      Left            =   180
      TabIndex        =   7
      Top             =   300
      Width           =   5055
   End
   Begin VB.CommandButton btnDeleteOne 
      Caption         =   "Delete &One"
      Height          =   360
      Left            =   5580
      TabIndex        =   6
      Top             =   1215
      Width           =   1160
   End
   Begin VB.CommandButton btnReset 
      Caption         =   "&Reset"
      Height          =   360
      Left            =   5580
      TabIndex        =   3
      Top             =   345
      Width           =   1160
   End
   Begin VB.TextBox txtValue 
      Height          =   1785
      Left            =   150
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   3240
      Width           =   5115
   End
   Begin VB.CommandButton btnDeleteAll 
      Caption         =   "Delete &All"
      Height          =   360
      Left            =   5580
      TabIndex        =   4
      Top             =   780
      Width           =   1160
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   360
      Left            =   5580
      TabIndex        =   5
      Top             =   4635
      Width           =   1160
   End
   Begin VB.Label lblOffices 
      AutoSize        =   -1  'True
      Caption         =   "&Variables:"
      Height          =   210
      Left            =   195
      TabIndex        =   0
      Top             =   90
      Width           =   735
   End
   Begin VB.Label lblValue 
      AutoSize        =   -1  'True
      Caption         =   "V&alue:"
      Height          =   210
      Left            =   210
      TabIndex        =   1
      Top             =   3015
      Width           =   465
   End
End
Attribute VB_Name = "frmDocVars"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private DocVars() As String
Private vbControls() As String
Public m_bFinished As Boolean

Private Function bCreateDocVarArray() As Boolean
    Dim oDocVar As Variable
    Dim iCount As Integer
    Dim iVCount As Integer
    
    On Error Resume Next
'---create array
    With ActiveDocument
        iCount = .Variables.Count
        If iCount = 0 Then iCount = 1
        ReDim DocVars(iCount - 1, 1)
        
        For Each oDocVar In .Variables
            With oDocVar
                DocVars(iVCount, 0) = .Name
                DocVars(iVCount, 1) = .Value
                iVCount = iVCount + 1
            End With
        Next oDocVar

    End With

End Function
Private Function DeleteOneDocVar() As Boolean
    ActiveDocument.Variables(Me.lstVarNames.Text).Delete

End Function


Private Function DeleteAllDocVars() As Boolean
    Dim oDocVar As Variable
    Dim iCount As Integer
    Dim iVCount As Integer
    
    On Error Resume Next
'---create array
    With ActiveDocument
        For Each oDocVar In .Variables
            With oDocVar
                .Delete
            End With
        Next oDocVar

    End With

End Function
Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnDeleteAll_Click()
    Dim bRet As Boolean
    DeleteAllDocVars
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()
End Sub

Private Sub btnDeleteOne_Click()
   Dim bRet As Boolean
    DeleteOneDocVar
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()

End Sub

Private Sub btnReport_Click()
    Dim oDoc As Word.Document
    Dim oSource As Word.Document
    Dim oTbl As Word.Table
    Dim lCount As Long
    Dim i, j As Integer
    
    Set oSource = ActiveDocument
    
    '---add word doc
    Documents.Add
    Set oDoc = ActiveDocument
    lCount = -9999
    lCount = UBound(DocVars(), 1)
    If lCount = 0 Then
        MsgBox "There are no Document Variables in " & ActiveDocument.Name & ".", vbCritical, "MacPac 9"
        Exit Sub
    End If
    
    
    Me.Hide
    Application.ScreenUpdating = False
    
    With oDoc
        '---format page setup
        With .PageSetup
            .Orientation = wdOrientLandscape
            .LeftMargin = InchesToPoints(0.5)
            .RightMargin = InchesToPoints(0.5)
            .DifferentFirstPageHeaderFooter = False
        End With
        
        '---create header
        With .Sections.First.Headers(wdHeaderFooterPrimary).Range
            .InsertAfter "MacPac Document Variables Report" & vbCrLf
            
            .ParagraphFormat.Alignment = wdAlignParagraphCenter
            .InsertAfter "Document Name:  " & oSource.Name & vbCrLf
            .ParagraphFormat.Alignment = wdAlignParagraphRight
            
            .InsertAfter "Attached Template:  " & oSource.AttachedTemplate & vbCrLf
            .InsertAfter Format(Date, "MMMM dd, yyyy") & vbCrLf
            .Fields.Add .Paragraphs.Last.Range, wdFieldPage
            .Paragraphs.Last.Range.InsertBefore "Page "
            .Paragraphs.Last.Range.ParagraphFormat.SpaceAfter = 12
            
        End With
        
        '---add table
        .Tables.Add .Range, lCount, 3   ', wdWord9TableBehavior, wdAutoFitWindow
        
        Set oTbl = .Tables(.Tables.Count)
    
        If Not oTbl Is Nothing Then
            '---format table
            oTbl.Borders.InsideLineStyle = wdLineStyleNone
            oTbl.Borders.OutsideLineStyle = wdLineStyleNone
            oTbl.Range.ParagraphFormat.SpaceAfter = 3
            oTbl.Rows.AllowBreakAcrossPages = False
            oTbl.Rows.Borders(wdBorderHorizontal) = wdLineStyleSingle
            
            With oTbl.Rows(1)
                .HeadingFormat = True
                .Range.ParagraphFormat.Alignment = wdAlignParagraphCenter
                .Range.ParagraphFormat.SpaceAfter = 6
                .Cells(1).Range.Text = "Variable Name"
                .Cells(1).Range.ParagraphFormat.Alignment = wdAlignParagraphLeft
                .Cells(2).Range.Text = "Variable Value"
                .Cells(2).Range.ParagraphFormat.Alignment = wdAlignParagraphLeft
                .Cells(3).Range.Text = "Comment"
                .Range.Font.Bold = True
                .Range.Borders(wdBorderBottom).LineStyle = wdLineStyleDouble
                
            End With
            
            '---fill table
            For i = 1 To lCount - 1
                For j = 0 To 1
                    oTbl.Cell(i + 1, j + 1).Range.Text = DocVars(i, j)
                Next j
            
            Next i
        End If
    
        oDoc.Activate
        Application.ScreenUpdating = True
    
    End With


End Sub

Private Sub btnReset_Click()
    Dim iPlace As Integer
    Dim bRet As Boolean
    iPlace = lstVarNames.ListIndex
    ActiveDocument.Variables(lstVarNames.Text) = txtValue
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()
    'lstVarNames.ListIndex = iPlace

End Sub

Private Sub lstVarNames_Click()
    Me.txtValue = DocVars(Me.lstVarNames.ListIndex, 1)
End Sub

Private Sub Form_Load()

    Dim bRet As Boolean
    Me.m_bFinished = False
      
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()

End Sub

Function bLSTList(lstPVBA As VB.ListBox, _
                  xArray() As String, _
                  Optional lstPVB As VB.ListBox) As Boolean
'assigns array xArray() to listbox lstP

    Dim i As Integer
    Dim j As Integer
    Dim bIsOneDimensional As Boolean
    Dim iArrayUBound As Integer

'////////////////////////
'note -- this is for one column list boxes only! (bug fix
'for pleadings & business dboxes

    Dim lstP As Control
    
    If Not lstPVB Is Nothing Then
        Set lstP = lstPVB
    Else
        Set lstP = lstPVBA
    End If
    
'////////////////////////
'   test for number of dimensions in array
    On Error Resume Next
    iArrayUBound = UBound(xArray, 2)
    bIsOneDimensional = (Err.Number > 0)
    Err.Number = 0
    
'   clear existing if necessary
    If lstP.ListCount Then _
        lstP.Clear
        
    If bIsOneDimensional Then
'       add items from one dimensional array
        If Not (UBound(xArray) = 0 And xArray(0) = "") Then
            For i = LBound(xArray) To UBound(xArray)
                lstP.AddItem xArray(i)
                'DoEvents
            Next i
        End If
    Else
'       add items from two dimensional array
        If Not (UBound(xArray) = 0 And xArray(0, 0) = "") Then
            For i = LBound(xArray, 1) To UBound(xArray, 1)
                With lstP
                    .AddItem xArray(i, 0)
                    For j = 1 To iArrayUBound
                        .List(i, j) = xArray(i, j)
                    Next j
                End With
            Next i
        End If
    End If
End Function


