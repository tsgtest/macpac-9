VERSION 5.00
Begin VB.Form frmDocIDIntegration 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Update Document ID"
   ClientHeight    =   5040
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2490
   Icon            =   "frmDocIDIntegration.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5040
   ScaleWidth      =   2490
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkFileExit 
      Caption         =   "On File E&xit"
      Height          =   330
      Left            =   225
      TabIndex        =   8
      Top             =   4005
      Width           =   1770
   End
   Begin VB.CheckBox chkFileCloseAll 
      Caption         =   "On File Close A&ll"
      Height          =   330
      Left            =   225
      TabIndex        =   5
      Top             =   2610
      Width           =   1770
   End
   Begin VB.CheckBox chkFileSaveAll 
      Caption         =   "On File Sa&ve All"
      Height          =   330
      Left            =   225
      TabIndex        =   3
      Top             =   1680
      Width           =   1890
   End
   Begin VB.CheckBox chkFilePrintDefault 
      Caption         =   "On File Print &Default"
      Height          =   330
      Left            =   225
      TabIndex        =   7
      Top             =   3540
      Width           =   1770
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   225
      TabIndex        =   10
      Top             =   4530
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   1290
      TabIndex        =   9
      Top             =   4530
      Width           =   1000
   End
   Begin VB.CheckBox chkFileOpen 
      Caption         =   "On File &Open"
      Height          =   330
      Left            =   225
      TabIndex        =   0
      Top             =   285
      Width           =   1770
   End
   Begin VB.CheckBox chkFilePrint 
      Caption         =   "On File &Print"
      Height          =   330
      Left            =   225
      TabIndex        =   6
      Top             =   3075
      Width           =   1770
   End
   Begin VB.CheckBox chkFileClose 
      Caption         =   "On File &Close"
      Height          =   330
      Left            =   225
      TabIndex        =   4
      Top             =   2145
      Width           =   1770
   End
   Begin VB.CheckBox chkFileSaveAs 
      Caption         =   "On File Save &As"
      Height          =   330
      Left            =   225
      TabIndex        =   2
      Top             =   1215
      Width           =   1770
   End
   Begin VB.CheckBox chkFileSave 
      Caption         =   "On First &Save"
      Height          =   330
      Left            =   225
      TabIndex        =   1
      Top             =   750
      Width           =   1770
   End
End
Attribute VB_Name = "frmDocIDIntegration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btnCancel_Click()
    Me.Hide
    'retro0705
    With ActiveDocument
        .Save
        .Close
    End With
End Sub

Private Sub btnOK_Click()
    Dim i As Integer
    
    On Error GoTo ProcError
    
    If Me.chkFileOpen Then
        i = 1
    End If
    If Me.chkFileSave Then
        i = i + 2
    End If
    If Me.chkFileSaveAs Then
        i = i + 4
    End If
    If Me.chkFileSaveAll Then
        i = i + 8
    End If
    If Me.chkFileClose Then
        i = i + 16  '10
    End If
    If Me.chkFileCloseAll Then
        i = i + 32  '20
    End If
    If Me.chkFilePrint Then
        i = i + 64  '40
    End If
    If Me.chkFilePrintDefault Then
        i = i + 128 '80
    End If
    If Me.chkFileExit Then
        i = i + 256 '100
    End If
    
    SetDocIDIntegration i
    Me.Hide
    DoEvents
    
    With ActiveDocument
        .Save
        .Close
    End With
    
    MsgBox "You have just changed MacPac Document ID integration with " & _
        "Word's file management functions.  To implement this change " & _
        "on user's machines, please distribute @@MP90.dot, which is located " & _
        "in the Microsoft Word Startup directory.", vbInformation, App.Title
    Exit Sub
ProcError:
    MsgBox Err.Description
End Sub

Private Sub SetDocIDIntegration(iEvents As Integer)
    
    On Error GoTo ProcError
    With ActiveDocument.VBProject
        
'       do file open - mdlAuto already exists
        With .VBComponents("mdlAuto").CodeModule
            DeleteAutoExec
            If iEvents And &H1 Then
                WriteAutoExec
            End If
        End With

        EnsureModuleExists "FileSave"
        With .VBComponents("FileSave").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H2 Then
                .AddFromString GetProc("FileSave")
            End If
        End With
        
        EnsureModuleExists "FileSaveAs"
        With .VBComponents("FileSaveAs").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H4 Then
                .AddFromString GetProc("FileSaveAs")
            End If
        End With
        
        EnsureModuleExists "FileSaveAll"
        With .VBComponents("FileSaveAll").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H8 Then
                .AddFromString GetProcBasic("FileSaveAll")
            End If
        End With
    
        EnsureModuleExists "FileClose"
        With .VBComponents("FileClose").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H10 Then
                .AddFromString GetProc("FileClose", False)
            End If
        End With
        
        EnsureModuleExists "FileCloseAll"
        With .VBComponents("FileCloseAll").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H20 Then
                .AddFromString GetProc("FileCloseAll")
            End If
        End With
                
        EnsureModuleExists "FilePrint"
        With .VBComponents("FilePrint").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H40 Then
                .AddFromString GetProcBasic("FilePrint")
            End If
        End With
        
        EnsureModuleExists "FilePrintDefault"
        With .VBComponents("FilePrintDefault").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H80 Then
                .AddFromString GetProcBasic("FilePrintDefault")
            End If
        End With
        
        EnsureModuleExists "FileExit"
        With .VBComponents("FileExit").CodeModule
            .DeleteLines 1, .CountOfLines
            If iEvents And &H100 Then
                .AddFromString GetProc("FileExit")
            End If
        End With
    
    End With
    Exit Sub
ProcError:
    MsgBox Error(Err), vbExclamation, App.Title
End Sub

Function EnsureModuleExists(xName As String)
    Dim oVBC As Object
    On Error Resume Next
    With Word.ActiveDocument.VBProject
        Set oVBC = .VBComponents(xName)
        On Error GoTo ProcError
        
        If oVBC Is Nothing Then
            Set oVBC = .VBComponents.Add(1)
            oVBC.Name = xName
        End If
    End With
    Exit Function
ProcError:
    MsgBox Error(Err), vbExclamation, App.Title
End Function

Function GetProcBasic(ByVal xProcName As String, Optional ByVal bSplash As Boolean = True) As String
    Dim x As String
    x = vbCr & "Sub " & xProcName & "()" & vbCr & _
        "    On Error GoTo ProcError" & vbCr & _
        "    If Not (MacPac(" & bSplash & ") Is Nothing) Then" & vbCr & _
        "        MacPac." & xProcName & vbCr & _
        "    End If" & vbCr & _
        "   Exit Sub" & vbCr & _
        "ProcError:" & vbCr & _
        "End Sub" & vbCr
    GetProcBasic = x
ProcError:
    
    Exit Function
End Function
    
Function GetProc(ByVal xProcName As String, Optional ByVal bSplash As Boolean = True) As String
    Dim x As String
    x = vbCr & "Sub " & xProcName & "()" & vbCr & _
        "    On Error GoTo ProcError" & vbCr & _
        "    Dim oDoc As Word.Document" & vbCr & _
        "    Set oDoc = Word.ActiveDocument" & vbCr & _
        "    If Not (MacPac(" & bSplash & ") Is Nothing) Then" & vbCr & _
        "        MacPac." & xProcName & vbCr & _
        "    else" & vbCr & _
        "       ' Wait for Diagnosis Report to complete" & vbCr & _
        "       While Tasks.Exists(" & """" & "mpDiagnose" & """" & ")" & vbCr & _
        "           DoEvents" & vbCr & _
        "       Wend" & vbCr & _
        "       oDoc.Activate" & vbCr & _
        "       DMS" & xProcName & vbCr & _
        "    End If" & vbCr & _
        "   Exit Sub" & vbCr & _
        "ProcError:" & vbCr & _
        "End Sub" & vbCr
    GetProc = x
ProcError:
    
    Exit Function
End Function
    
Private Sub Form_Load()
    Dim oTemplate As Word.Template
    Dim oDoc As Word.Document
    Dim xFile As String
    Dim xMsg As String
    Dim iVer As Integer
    
    iVer = Int(Left(Word.Application.Version, InStr(Word.Application.Version, ".") - 1))

    If iVer < 12 Then
        xFile = Word.Application.AddIns("@@MP90.dot").Path & "\@@MP90.dot"
    Else
        xFile = Word.Application.AddIns("@@MP90.dotm").Path & "\@@MP90.dotm"
    End If
    
    On Error Resume Next
    Set oTemplate = Word.Templates(xFile)
    On Error GoTo ProcError
    
    If oTemplate Is Nothing Then
        xMsg = "@@MP90.dot is not loaded as an add-in.  " & _
            "Please ensure that MacPac is loaded on this machine " & _
            "before attempting to run MacPac administrative functions."
        MsgBox xMsg, vbCritical, App.Title
        Exit Sub
    End If
    
    Set oDoc = oTemplate.OpenAsDocument
    With oDoc.ActiveWindow
        .Caption = "Updating Document ID Integration"
    End With
    
    On Error Resume Next
    Dim o As Object
    With oTemplate.VBProject.VBComponents
        Set o = Nothing
        Set o = .Item("mdlAuto")
        If Not o Is Nothing Then
            If .Item("mdlAuto").CodeModule.Find("Sub AutoExec()", 1, 1, -1, -1) Then _
                Me.chkFileOpen.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FileSave")
        If Not o Is Nothing Then
            If .Item("FileSave").CodeModule.Find("Sub FileSave()", 1, 1, -1, -1) Then _
                Me.chkFileSave.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FileSaveAs")
        If Not o Is Nothing Then
            If .Item("FileSaveAs").CodeModule.Find("Sub FileSaveAs()", 1, 1, -1, -1) Then _
                Me.chkFileSaveAs.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FileClose")
        If Not o Is Nothing Then
            If .Item("FileClose").CodeModule.Find("Sub FileClose()", 1, 1, -1, -1) Then _
                Me.chkFileClose.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FilePrint")
        If Not o Is Nothing Then
            If .Item("FilePrint").CodeModule.Find("Sub FilePrint()", 1, 1, -1, -1) Then _
                Me.chkFilePrint.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FilePrintDefault")
        If Not o Is Nothing Then
            If .Item("FilePrintDefault").CodeModule.Find("Sub FilePrintDefault()", 1, 1, -1, -1) Then _
                Me.chkFilePrintDefault.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FileSaveAll")
        If Not o Is Nothing Then
            If .Item("FileSaveAll").CodeModule.Find("Sub FileSaveAll()", 1, 1, -1, -1) Then _
                Me.chkFileSaveAll.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FileCloseAll")
        If Not o Is Nothing Then
            If .Item("FileCloseAll").CodeModule.Find("Sub FileCloseAll()", 1, 1, -1, -1) Then _
                Me.chkFileCloseAll.Value = vbChecked
        End If
        
        Set o = Nothing
        Set o = .Item("FileExit")
        If Not o Is Nothing Then
            If .Item("FileExit").CodeModule.Find("Sub FileExit()", 1, 1, -1, -1) Then _
                Me.chkFileExit.Value = vbChecked
        End If
        
        
    End With
    Exit Sub
ProcError:
Stop
Resume
    MsgBox Error(Err), vbExclamation, App.Title
End Sub
