VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmBPClean 
   Caption         =   "Cleanup Boilerplates"
   ClientHeight    =   6492
   ClientLeft      =   60
   ClientTop       =   348
   ClientWidth     =   3840
   Icon            =   "frmBPClean.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6492
   ScaleWidth      =   3840
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCleanup 
      Caption         =   "Cleanup"
      Default         =   -1  'True
      Height          =   405
      Left            =   2580
      TabIndex        =   1
      Top             =   6030
      Width           =   1140
   End
   Begin VB.Timer Timer1 
      Interval        =   200
      Left            =   30
      Top             =   720
   End
   Begin TrueDBList60.TDBList lstFiles 
      Height          =   5940
      Left            =   0
      OleObjectBlob   =   "frmBPClean.frx":000C
      TabIndex        =   0
      Top             =   0
      Width           =   3840
   End
End
Attribute VB_Name = "frmBPClean"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
Private m_xCurFile As String
Private m_xFileList As String

Public Property Let FileList(xNew As String)
    m_xFileList = xNew
End Property

Public Property Get FileList() As String
    FileList = m_xFileList
End Property

Private Sub cmdCleanup_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Dim oFiles As xArray
    Dim oX As XHelper
    Dim xLeft As String
    Dim xIni As String
    
    xIni = mpBase2.ApplicationDirectory & "\MPA.ini"
        
'*  set controls
    Set oX = New XHelper
    Set oFiles = xarStringToxArray(Me.FileList)
    oX.Sort oFiles, 0
    Me.lstFiles.Array = oFiles
    
'    xLeft = GetIni("frmBPClean", "Left", xIni)
'    If xLeft <> "" Then
'        Me.Left = xLeft
'        Me.Top = GetIni("frmBPClean", "Top", xIni)
'        Me.Width = GetIni("frmBPClean", "Width", xIni)
'        Me.Height = GetIni("frmBPClean", "Height", xIni)
'    Else
''       position at left of screen
'        Me.Left = Screen.Width - Me.Width
'        Me.Top = (Screen.Height / 2) - (Me.Width / 2)
'    End If
End Sub

Private Sub Form_Resize()
    Me.lstFiles.Height = Me.Height - 1000
    Me.lstFiles.Width = Me.ScaleWidth
    Me.cmdCleanup.Top = Me.Height - 900
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim xIni As String
    
'    xIni = mpBase2.ApplicationDirectory & "\MPA.ini"
'    SetIni "frmBPClean", "Left", Me.Left, xIni
'    SetIni "frmBPClean", "Top", Me.Top, xIni
'    SetIni "frmBPClean", "Height", Me.Height, xIni
'    SetIni "frmBPClean", "Width", Me.Width, xIni
    
    Exit Sub
ProcError:
    MsgBox Error(Err)
End Sub
