VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmServiceList 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit Service List"
   ClientHeight    =   6684
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   5472
   Icon            =   "frmServiceList.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6684
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   3105
      Picture         =   "frmServiceList.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   33
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   6240
      Width           =   750
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4725
      TabIndex        =   35
      Top             =   6240
      Width           =   750
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3915
      TabIndex        =   34
      Top             =   6240
      Width           =   750
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   4650
      Left            =   -15
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   1980
      Width           =   6540
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|E&xtras|F&ormat"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   400
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin C1SizerLibCtl.C1Elastic vsElastic2 
         Height          =   4236
         Left            =   7032
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   12
         Width           =   6516
         _cx             =   11494
         _cy             =   7472
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   0
         MousePointer    =   0
         Version         =   801
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         FloodColor      =   192
         ForeColorDisabled=   -2147483631
         Caption         =   ""
         Align           =   0
         AutoSizeChildren=   0
         BorderWidth     =   6
         ChildSpacing    =   4
         Splitter        =   0   'False
         FloodDirection  =   0
         FloodPercent    =   0
         CaptionPos      =   1
         WordWrap        =   -1  'True
         MaxChildSize    =   0
         MinChildSize    =   0
         TagWidth        =   0
         TagPosition     =   0
         Style           =   0
         TagSplit        =   0
         PicturePos      =   4
         CaptionStyle    =   0
         ResizeFonts     =   0   'False
         GridRows        =   0
         GridCols        =   0
         Frame           =   3
         FrameStyle      =   0
         FrameWidth      =   1
         FrameColor      =   -2147483628
         FrameShadow     =   -2147483632
         FloodStyle      =   1
         _GridInfo       =   ""
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase 
            Height          =   690
            Left            =   1770
            TabIndex        =   18
            Top             =   180
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   1207
            ListRows        =   10
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin TrueDBList60.TDBCombo cmbPartyTitle 
            Height          =   450
            Left            =   1770
            OleObjectBlob   =   "frmServiceList.frx":0CAC
            TabIndex        =   20
            Top             =   1035
            Width           =   3570
         End
         Begin VB.CheckBox chkIsNonEF 
            Appearance      =   0  'Flat
            Caption         =   "Recipient Not &Served Electronically"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   1845
            TabIndex        =   23
            Top             =   2220
            Width           =   3555
         End
         Begin VB.TextBox txtPartyNames 
            Appearance      =   0  'Flat
            Height          =   495
            Left            =   1785
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   22
            Top             =   1515
            Width           =   3570
         End
         Begin VB.Label lblPartyNames 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Part&y Name(s):"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Left            =   135
            TabIndex        =   21
            Top             =   1530
            Width           =   1455
         End
         Begin VB.Label lblPartyTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   120
            TabIndex        =   19
            Top             =   1065
            Width           =   1455
         End
         Begin VB.Label lblDeliveryPhrase 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " &Delivery Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   90
            TabIndex        =   17
            Top             =   195
            Width           =   1485
            WordWrap        =   -1  'True
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00FFFFFF&
            X1              =   -12
            X2              =   5588
            Y1              =   4176
            Y2              =   4176
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00404040&
            Height          =   4235
            Left            =   -30
            Top             =   -45
            Width           =   1725
         End
      End
      Begin C1SizerLibCtl.C1Elastic vsElastic1 
         Height          =   4236
         Left            =   12
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   12
         Width           =   6516
         _cx             =   11494
         _cy             =   7472
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   0
         MousePointer    =   0
         Version         =   801
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         FloodColor      =   192
         ForeColorDisabled=   -2147483631
         Caption         =   ""
         Align           =   0
         AutoSizeChildren=   0
         BorderWidth     =   6
         ChildSpacing    =   4
         Splitter        =   0   'False
         FloodDirection  =   0
         FloodPercent    =   0
         CaptionPos      =   1
         WordWrap        =   -1  'True
         MaxChildSize    =   0
         MinChildSize    =   0
         TagWidth        =   0
         TagPosition     =   0
         Style           =   0
         TagSplit        =   0
         PicturePos      =   4
         CaptionStyle    =   0
         ResizeFonts     =   0   'False
         GridRows        =   0
         GridCols        =   0
         Frame           =   3
         FrameStyle      =   0
         FrameWidth      =   1
         FrameColor      =   -2147483628
         FrameShadow     =   -2147483632
         FloodStyle      =   1
         _GridInfo       =   ""
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
         Begin VB.TextBox txtPhone 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   12
            Top             =   1335
            Width           =   3570
         End
         Begin VB.TextBox txtFax 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   14
            Top             =   1770
            Width           =   3570
         End
         Begin VB.TextBox txtEMail 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   16
            Top             =   2190
            Width           =   3570
         End
         Begin VB.TextBox txtRecipient 
            Appearance      =   0  'Flat
            Height          =   1020
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   10
            Top             =   165
            Width           =   3570
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00FFFFFF&
            X1              =   0
            X2              =   5600
            Y1              =   4176
            Y2              =   4176
         End
         Begin VB.Label lblRecipient 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Name/Address:"
            ForeColor       =   &H00FFFFFF&
            Height          =   585
            Left            =   135
            TabIndex        =   9
            Top             =   180
            Width           =   1455
         End
         Begin VB.Label lblPhone 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "P&hone:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   135
            TabIndex        =   11
            Top             =   1380
            Width           =   1455
         End
         Begin VB.Label lblFax 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Fac&simile:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   135
            TabIndex        =   13
            Top             =   1800
            Width           =   1455
         End
         Begin VB.Label lblEMail 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "E-Mai&l:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   135
            TabIndex        =   15
            Top             =   2220
            Width           =   1455
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00404040&
            Height          =   4285
            Left            =   -30
            Top             =   -90
            Width           =   1725
         End
      End
      Begin C1SizerLibCtl.C1Elastic vsElastic3 
         Height          =   4236
         Left            =   7272
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   12
         Width           =   6516
         _cx             =   11494
         _cy             =   7472
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   -1  'True
         Appearance      =   0
         MousePointer    =   0
         Version         =   801
         BackColor       =   -2147483633
         ForeColor       =   -2147483630
         FloodColor      =   192
         ForeColorDisabled=   -2147483631
         Caption         =   ""
         Align           =   0
         AutoSizeChildren=   0
         BorderWidth     =   6
         ChildSpacing    =   4
         Splitter        =   0   'False
         FloodDirection  =   0
         FloodPercent    =   0
         CaptionPos      =   1
         WordWrap        =   -1  'True
         MaxChildSize    =   0
         MinChildSize    =   0
         TagWidth        =   0
         TagPosition     =   0
         Style           =   0
         TagSplit        =   0
         PicturePos      =   4
         CaptionStyle    =   0
         ResizeFonts     =   0   'False
         GridRows        =   0
         GridCols        =   0
         Frame           =   3
         FrameStyle      =   0
         FrameWidth      =   1
         FrameColor      =   -2147483628
         FrameShadow     =   -2147483632
         FloodStyle      =   1
         _GridInfo       =   ""
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
         Begin VB.CheckBox chkSeparatePage 
            Appearance      =   0  'Flat
            Caption         =   "&Insert On Separate Page"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   1815
            TabIndex        =   32
            Top             =   2265
            Width           =   2130
         End
         Begin TrueDBList60.TDBCombo cmbType 
            Height          =   375
            Left            =   1755
            OleObjectBlob   =   "frmServiceList.frx":2ECD
            TabIndex        =   25
            Top             =   240
            Width           =   3570
         End
         Begin TrueDBList60.TDBCombo cmbFit 
            Height          =   375
            Left            =   1755
            OleObjectBlob   =   "frmServiceList.frx":50E0
            TabIndex        =   27
            Top             =   720
            Width           =   3570
         End
         Begin mpControls3.SpinTextInternational stbLeftIndent 
            Height          =   315
            Left            =   1770
            TabIndex        =   29
            Top             =   1260
            Width           =   3570
            _ExtentX        =   6287
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   2
            AppendSymbol    =   -1  'True
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin mpControls3.SpinTextInternational stbRightIndent 
            Height          =   315
            Left            =   1770
            TabIndex        =   31
            Top             =   1755
            Width           =   3570
            _ExtentX        =   6287
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   2
            AppendSymbol    =   -1  'True
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Left Indent:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   120
            TabIndex        =   28
            Top             =   1275
            Width           =   1455
         End
         Begin VB.Label lblFit 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Fit Ser&vice Table:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   120
            TabIndex        =   26
            Top             =   735
            Width           =   1455
         End
         Begin VB.Label lblRightIndent 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Right Indent:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   120
            TabIndex        =   30
            Top             =   1785
            Width           =   1455
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   120
            TabIndex        =   24
            Top             =   300
            Width           =   1455
         End
         Begin VB.Line Line4 
            BorderColor     =   &H00FFFFFF&
            X1              =   -12
            X2              =   5588
            Y1              =   4176
            Y2              =   4176
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00404040&
            Height          =   4565
            Left            =   -15
            Top             =   -375
            Width           =   1705
         End
      End
   End
   Begin TrueDBList60.TDBList lstRecipients 
      Height          =   960
      Left            =   1800
      OleObjectBlob   =   "frmServiceList.frx":72F2
      TabIndex        =   4
      Top             =   525
      Width           =   3570
   End
   Begin VB.CommandButton btnNew 
      Height          =   400
      Left            =   3990
      Picture         =   "frmServiceList.frx":9237
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Add a new recipient (F5)"
      Top             =   45
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4455
      Picture         =   "frmServiceList.frx":9779
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Save new recipient (F6)"
      Top             =   45
      Width           =   435
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4935
      Picture         =   "frmServiceList.frx":98F7
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Delete recipient (F7)"
      Top             =   45
      Width           =   435
   End
   Begin VB.CheckBox chkIncludePhone 
      Appearance      =   0  'Flat
      Caption         =   "&Phone"
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   1920
      TabIndex        =   6
      Top             =   1560
      Width           =   840
   End
   Begin VB.CheckBox chkIncludeFax 
      Appearance      =   0  'Flat
      Caption         =   "F&ax"
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   3060
      TabIndex        =   7
      Top             =   1560
      Width           =   840
   End
   Begin VB.CheckBox chkIncludeEMail 
      Appearance      =   0  'Flat
      Caption         =   "&E-Mail"
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   4155
      TabIndex        =   8
      Top             =   1560
      Width           =   840
   End
   Begin VB.Label lblInclude 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Include For All:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   150
      TabIndex        =   5
      Top             =   1590
      Width           =   1455
   End
   Begin VB.Label lblRecipients 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Recipients:"
      ForeColor       =   &H00FFFFFF&
      Height          =   810
      Left            =   150
      TabIndex        =   3
      Top             =   555
      Width           =   1455
   End
   Begin VB.Shape Shape6 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00800080&
      Height          =   5940
      Left            =   0
      Top             =   0
      Width           =   1695
   End
End
Attribute VB_Name = "frmServiceList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const mpCancelAdd As String = "Cancel unsaved recipient (F7)"
Const mpDeleteSig As String = "Delete this recipient (F7)"
Const mpCancelChange As String = "Cancel unsaved changes (F7)"
Const mpSaveChanges As String = "Save changes to this recipient (F6)"
Const mpSaveNewSignature As String = "Save new recipient (F6)"

Private m_bCancelled As Boolean
Private m_oRecip As MPO.CRecipient
Private m_lServiceType As Long
Private m_lDefServiceListType As Long
Private m_bInit As Boolean
Private m_bAdded As Boolean
Private m_bChanged As Boolean
Private m_oParent As Form

Private WithEvents m_oServiceList As MPO.CServiceList
Attribute m_oServiceList.VB_VarHelpID = -1
Private WithEvents m_oRecips As MPO.CRecipients
Attribute m_oRecips.VB_VarHelpID = -1

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Property Get Initialized() As Boolean
    Initialized = m_bInit
End Property
Property Let Initialized(bNew As Boolean)
    m_bInit = bNew
End Property
Public Property Get ServiceList() As MPO.CServiceList
    Set ServiceList = m_oServiceList
End Property

Public Property Set ServiceList(oNew As MPO.CServiceList)
    Set m_oServiceList = oNew
End Property
Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property
Public Property Get Added() As Boolean
    Added = m_bAdded
End Property
Public Property Let Added(bNew As Boolean)
    m_bAdded = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelAdd
        Me.btnSave.ToolTipText = mpSaveNewSignature
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Changed() As Boolean
    Changed = m_bChanged
End Property
Public Property Let Changed(bNew As Boolean)
    m_bChanged = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelChange
        Me.btnSave.ToolTipText = mpSaveChanges
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Private Sub ClearInput()
'clears all recipient detail fields
    On Error GoTo ProcError
    Me.txtRecipient.Text = ""
    Me.txtFax.Text = ""
    Me.txtPhone.Text = ""
    Me.txtEMail.Text = ""
    Me.cmbPartyTitle.BoundText = ""
    Me.mlcDeliveryPhrase.Text = ""
    Me.txtPartyNames.Text = ""
    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.ClearInput"
    Exit Sub
End Sub

Private Sub NewRecipient()
'creates a new, empty recipient
    
'   save recipient if necessary
    On Error GoTo ProcError
    If Me.Changed Then
        PromptToSave
    End If
    
'   add recipient
    Set m_oRecip = Me.ServiceList.Recipients.Add()
    
'   select last entry in list
    Me.lstRecipients.SelectedItem = _
        Me.lstRecipients.ApproxCount - 1
    ClearInput
    
    On Error Resume Next
    Me.txtRecipient.SetFocus
    Me.btnDelete.Enabled = True
    Me.btnSave.Enabled = True
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.NewRecipient"
    Exit Sub
End Sub

Private Sub PromptToSave()
    Dim xMsg As String
    Dim iChoice As Integer
    If m_bInit Then Exit Sub
    On Error GoTo ProcError
    xMsg = "Save current entry?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        UpdateRecipient
    ElseIf iChoice = vbNo And m_oRecip.Item("Address").value = "" Then
        DeleteRecipient
    End If
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.PromptToSave"
    Exit Sub
End Sub

Private Sub EnableInput(bEnabled As Boolean)
    On Error GoTo ProcError
    Me.lblRecipient.Enabled = bEnabled
    Me.txtRecipient.Enabled = bEnabled
    Me.btnDelete.Enabled = bEnabled
    Me.btnSave.Enabled = bEnabled
    If Me.lblFax.Visible Then
        Me.lblFax.Enabled = bEnabled
        Me.txtFax.Enabled = bEnabled
    End If
    If Me.chkIncludePhone.Visible Then
        Me.lblPhone.Enabled = bEnabled
        Me.txtPhone.Enabled = bEnabled
    End If
    If Me.chkIncludeEMail.Visible Then
        Me.lblEMail.Enabled = bEnabled
        Me.txtEMail.Enabled = bEnabled
    End If
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.EnableInput"
    Exit Sub
End Sub
    
Private Sub SaveRecipient()
'saves the current recipient
    On Error GoTo ProcError
'   ensure that there is a new recipient object
    If Me.ServiceList.Recipients.Count = 0 Then
'       add recipient
        Set m_oRecip = Me.ServiceList.Recipients.Add()
    End If
    UpdateRecipient
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.SaveRecipient"
    Exit Sub
End Sub

Private Sub RefreshList()
    Dim iNum As Integer
    With Me.lstRecipients
        .Array = m_oRecips.ListArray
        .Rebind
        
        iNum = Me.lstRecipients.Array.Count(1)
        
        On Error GoTo ProcError
        If iNum > 1 Then
            Me.lblRecipients = _
                " &Recipients (" & iNum & "):"
        ElseIf iNum = 1 And Me.txtRecipient <> Empty Then
            Me.lblRecipients = " &Recipients (1):"
        Else
            Me.lblRecipients = " &Recipients:"
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub UpdateRecipient()
'updates the current recipient object
'with dialog field values
    On Error GoTo ProcError
    With m_oRecip
        .Item("Fax").value = Me.txtFax
        .Item("Address").value = Me.txtRecipient
        .Item("Phone").value = Me.txtPhone
        .Item("EMail").value = Me.txtEMail
        .Item("PartyTitle").value = Me.cmbPartyTitle.Text
        .Item("PartyNames").value = Me.txtPartyNames
        .Item("DPhrase").value = Me.mlcDeliveryPhrase.Text
        '---9.7.1
        .Item("IsNonEF").value = chkIsNonEF
''''        If Me.chkIsNonEF.value = vbChecked Then
''''            Me.ServiceList.HasNonEF = True
''''        End If
    End With
    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.UpdateRecipient"
    Exit Sub
End Sub

Private Sub ShowRecipient()
    On Error GoTo ProcError
    With m_oRecip
        Me.txtRecipient = .Item("Address").value
        Me.txtPhone = .Item("Phone").value
        Me.txtFax = .Item("Fax").value
        Me.txtEMail = .Item("EMail").value
        Me.cmbPartyTitle.Text = .Item("PartyTitle").value
        Me.txtPartyNames = .Item("PartyNames").value
        Me.mlcDeliveryPhrase = .Item("DPhrase").value
        '---9.7.1
        Me.chkIsNonEF = .Item("IsNonEF").value
    End With
    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.ShowRecipient"
    Exit Sub
End Sub

Private Sub ChangeActiveRecipient()
'   prompt to save current recipient if necessary
    On Error GoTo ProcError
    If Me.Changed Then
        PromptToSave
    End If
    
'   switch recipient and show in dialog
    Set m_oRecip = Me.ServiceList.Recipients( _
        Me.lstRecipients.Bookmark + 1)
    ShowRecipient
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmServiceList.ChangeActiveRecipient"
    Exit Sub
End Sub

Private Sub DeleteRecipient()
    Dim i As Integer
    Dim iNum As Integer
    Dim vbResult As VbMsgBoxResult
    
    On Error GoTo ProcError
    
    vbResult = MsgBox("Are you sure you want to delete this recipient?", _
                      vbYesNo, _
                      "Service List")
                      
    If vbResult <> vbYes Then Exit Sub

    
    i = Me.lstRecipients.Row
    If Me.lstRecipients.Row > -1 Then
        m_oRecips.Remove (Me.lstRecipients.Bookmark + 1)
        ClearInput
        If m_oRecips.Count Then
            Me.lstRecipients.Row = Min(CDbl(i), m_oRecips.Count - 1)
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.DeleteRecipient"
End Sub

Private Sub btnAddressBooks_Click()
    On Error GoTo ProcError
    GetRecipients
    Exit Sub
ProcError:
    g_oError.Show Err, "MacPac90.frmServiceList.btnAddressBooks_Click"
    Exit Sub
End Sub

Private Sub GetRecipients()
'retrieves service list recipients using CI
    Dim oContacts As MPO.CContacts
    Dim oContact As MPO.CContact
    Dim bPhone As Boolean
    Dim bFax As Boolean
    Dim bEMail As Boolean
    Static bCIActive As Boolean
    
    On Error GoTo ProcError

    If Not g_bShowAddressBooks Then Exit Sub
    
    If bCIActive Then Exit Sub
    bCIActive = True
    
    m_bInit = True
    
'   get options for inclusion
    With Me.ServiceList
        bPhone = 0 - Me.chkIncludePhone
        bFax = 0 - Me.chkIncludeFax
        bEMail = 0 - Me.chkIncludeEMail
    End With
    
    Set oContacts = g_oMPO.Contacts
    
    With oContacts
'       call CI
        .Retrieve , , , , True, True, _
            True, , bPhone Or _
            bFax Or bEMail, ciSelectionList_To
            
'       do if contacts are selected
        If .Count Then
'           delete empty lone recipient
            '9.7.1 - #3862
            If m_oRecips.Count = 1 Or m_bAdded Then
                If m_oRecips.Item(m_oRecips.Count).Item("Address") = "" Then
                    m_oRecips.Remove m_oRecips.Count ' Remove last empty item
                End If
            End If
            '**** End 9.7.1
            
            For Each oContact In oContacts
'               add each contact as a recipient
                Set m_oRecip = m_oRecips.Add
                
'               fill in recipient detail
                With m_oRecip
                    .Item("Address").value = oContact.Detail
                    .Item("Phone").value = oContact.Phone
                    .Item("Fax").value = oContact.Fax
                    .Item("EMail").value = oContact.EMail
                End With
            Next oContact
            
'           refresh the list
            RefreshList
            
'           select the last inserted recipient
            Me.lstRecipients.SelectedItem = m_oRecips.Count - 1
            ShowRecipient
        End If
    End With
    Me.SetFocus
    bCIActive = False
    m_bInit = False
    Exit Sub
ProcError:
    bCIActive = False
    m_bInit = False
    g_oError.Raise Err, "MacPac90.frmServiceList.GetRecipients"
    Exit Sub
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    m_bCancelled = True
End Sub

Private Sub btnDelete_Click()
    '---9.7.1 - 3880
    Dim bAdded As Boolean
    
    On Error GoTo ProcError
    bAdded = Me.Added
    If Me.Changed Then
        Me.Changed = False
        ChangeActiveRecipient
        DoEvents
        Me.Added = bAdded
    Else
        DeleteRecipient
        Me.Changed = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnDelete_GotFocus()
    OnControlGotFocus Me.btnDelete
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    If Me.Changed Then
'       current record is dirty -
'       save automatically if there is
'       only 1 recipient, else prompt
        If m_oRecips.Count = 1 Then
            UpdateRecipient
        Else
            PromptToSave
        End If
    End If
    UpdateObject
    Me.Hide
    DoEvents
    
    With Word.Application
        .ScreenUpdating = False
        EchoOff
        If Me.ParentForm Is Nothing Then
            ServiceList.Finish
        Else
            ServiceList.Insert
        End If
        EchoOn
    End With
    m_bCancelled = False
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not successfully insert service list."
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub
Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnNew_GotFocus()
    OnControlGotFocus Me.btnNew
End Sub

Private Sub btnSave_Click()
    Dim iPos As Integer
    On Error GoTo ProcError
    SaveRecipient
    RefreshList
    Me.lstRecipients.BoundText = Me.txtRecipient
    If Me.Added Then
        Me.Added = False
    Else
        Me.Changed = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnSave_GotFocus()
    OnControlGotFocus Me.btnSave
End Sub

Private Sub chkIncludePhone_GotFocus()
    OnControlGotFocus Me.chkIncludePhone
End Sub
Private Sub chkIncludeFax_GotFocus()
    OnControlGotFocus Me.chkIncludeFax
End Sub
Private Sub chkIncludeEmail_GotFocus()
    OnControlGotFocus Me.chkIncludeEMail
End Sub

Private Sub chkIsNonEF_Click()
    Me.Changed = True
End Sub

Private Sub chkIsNonEF_GotFocus()
    OnControlGotFocus Me.chkIsNonEF
End Sub

Private Sub chkSeparatePage_Click()
'''    Me.Changed = True
End Sub

Private Sub chkSeparatePage_GotFocus()
    OnControlGotFocus Me.chkSeparatePage
End Sub

Private Sub cmbFit_GotFocus()
    OnControlGotFocus Me.cmbFit
End Sub

Private Sub cmbType_GotFocus()
    OnControlGotFocus Me.cmbType
End Sub

Private Sub cmbType_ItemChange()
    On Error GoTo ProcError
    GetDefinitionDetail
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub GetDefinitionDetail()
    Dim oDef As mpDB.CServiceListDef
    Dim xDesc As String
    Dim sOffset As Single
    
    On Error Resume Next
    Set oDef = g_oMPO.db.ServiceListDefs.Item(Me.cmbType.BoundText)
    On Error GoTo ProcError
    
    If oDef Is Nothing Then
        xDesc = "Invalid service list type.  There is no service list type " & _
            "with ID = " & Me.cmbType.BoundText & "."
        Err.Raise mpError_InvalidMember
    End If
    
    If m_bInit Then Exit Sub
    With oDef
        Me.cmbFit.BoundText = .WidthRule
        Me.stbLeftIndent.value = .LeftIndent / 72
        Me.stbRightIndent.value = .RightIndent / 72
        
        Me.lblPartyTitle.Visible = .AllowPartyTitle
        Me.cmbPartyTitle.Visible = .AllowPartyTitle
        
        Me.lblDeliveryPhrase.Visible = .AllowDPhrase
        Me.mlcDeliveryPhrase.Visible = .AllowDPhrase
        
        Me.lblPartyNames.Visible = .AllowPartyNames
        Me.txtPartyNames.Visible = .AllowPartyNames
        
        If Not .AllowPartyTitle Then
            Me.cmbPartyTitle.BoundText = ""
        Else
        End If
        
        If Not .AllowPartyNames Then
            Me.txtPartyNames = ""
        End If
        
        Me.chkIncludePhone.Visible = .AllowPhone And g_bShowAddressBooks
        Me.vsIndexTab1.TabEnabled(1) = .AllowDPhrase Or .AllowPartyNames Or .AllowPartyTitle
        Me.txtPhone.Visible = .AllowPhone
        Me.lblPhone.Visible = .AllowPhone
        
        If m_bInit Then _
            Me.chkIncludePhone = Abs(.IncludePhone)
        
        Me.chkIncludeFax.Visible = .AllowFax And g_bShowAddressBooks
        Me.lblFax.Visible = .AllowFax
        Me.txtFax.Visible = .AllowFax
        
        '---9.7.1
        Me.chkIsNonEF.Visible = .AllowNonEF
        
        If m_bInit Then _
            Me.chkIncludeFax = Abs(.IncludeFax)
       
        Me.chkIncludeEMail.Visible = .AllowEMail And g_bShowAddressBooks
        Me.lblEMail.Visible = .AllowEMail
        Me.txtEMail.Visible = .AllowEMail
        
        If m_bInit Then _
            Me.chkIncludeEMail = Abs(.IncludeEMail)
        
        
        Me.lblInclude.Visible = (.AllowEMail Or .AllowFax Or .AllowPhone) And _
                                g_bShowAddressBooks
                                
        If Not g_bShowAddressBooks Then
            Me.chkIncludeEMail = 1
            Me.chkIncludeFax = 1
            Me.chkIncludePhone = 1
        End If

        PositionControls

    End With
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmServiceList.GetDefinitionDetail", xDesc
    Exit Sub
End Sub


Public Sub PositionControls()
'adjust dialog for manual entry / no manual entry
'    Dim iSpaceBefore As Integer
'    Dim oCtl As Control
'    Dim oLastVisible As Control
'    Dim arrCtls() As String
'    Dim i As Integer
'
'    On Error GoTo ProcError
'
'    ReDim arrCtls(Me.Controls.Count)
'
''   place each control in tab order sort
'    For Each oCtl In Me.Controls
'        On Error Resume Next
'        arrCtls(oCtl.TabIndex) = oCtl.Name
'    Next
'
'    iSpaceBefore = 110
'
''   position controls based on visibility and tab index -
''   skip author and signer name controls - they'll always
''   appear at the top of the dlg at the same y position
'
'    For i = 3 To 22     'first 3 are command buttons
''       get control
'        Set oCtl = Me.Controls(arrCtls(i))
''       do only if visible
'        If oCtl.Visible Then
'            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
'                TypeOf oCtl Is VB.TextBox Or _
'                TypeOf oCtl Is VB.CheckBox Or _
'                TypeOf oCtl Is TDBList Or _
'                TypeOf oCtl Is MultiLineCombo) Then
'                If oLastVisible Is Nothing Then
''                   position as 1st control
'                    oCtl.Top = 520
'                ElseIf (TypeOf oLastVisible Is MultiLineCombo) Then
'                    oCtl.Top = oLastVisible.Top + 800
'                Else
''                   position control relative to previous control
'                    If i <> 21 And i <> 22 Then     'last checkboxes should be on same line
'                        oCtl.Top = oLastVisible.Top + _
'                                   oLastVisible.Height + _
'                                   iSpaceBefore
'                    Else
'                        oCtl.Top = oLastVisible.Top
'                    End If
'                End If
'
''               set accompanying label vertical position
'                If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
'                    Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
'                End If
'
''               set control as last visible control
'                Set oLastVisible = oCtl
'            End If
'        Else
'            iSpaceBefore = iSpaceBefore + 25       'add more space if controls are not visible
'        End If
'    Next i
'
''    Me.btnFinish.Top = oLastVisible.Top + _
''        oLastVisible.Height + 210
''
''    Me.btnCancel.Top = Me.btnFinish.Top
''    Me.Height = Me.btnFinish.Top + Me.btnFinish.Height + 550
'
'    Word.Application.ScreenRefresh
'    Exit Sub
'ProcError:
'    g_oError.Raise Err, "MacPac90.frmServiceList.PositionControls"
'    Exit Sub
End Sub

Private Sub cmbType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    
    UpdateForm

    ' If defined type is not valid, then select first item in list
    If Len(Me.cmbType.BoundText) = 0 Then
        Me.cmbType.Bookmark = 0
        DoEvents
        m_bInit = False
        GetDefinitionDetail
    Else
        '   enable/disable phone/fax/email chkboxes
        With Me.ServiceList.Definition
            Me.chkIncludeEMail.Visible = .AllowEMail And g_bShowAddressBooks
            Me.lblEMail.Visible = .AllowEMail
            Me.txtEMail.Visible = .AllowEMail

            Me.chkIncludeFax.Visible = .AllowFax And g_bShowAddressBooks
            Me.lblFax.Visible = .AllowFax
            Me.txtFax.Visible = .AllowFax

            Me.chkIncludePhone.Visible = .AllowPhone And g_bShowAddressBooks
            Me.lblPhone.Visible = .AllowPhone
            Me.txtPhone.Visible = .AllowPhone

            Me.lblInclude.Visible = (.AllowEMail Or .AllowFax Or .AllowPhone) And _
                                    g_bShowAddressBooks

            If Not g_bShowAddressBooks Then
                Me.chkIncludeEMail = 1
                Me.chkIncludeFax = 1
                Me.chkIncludePhone = 1
            End If
            
            Me.vsIndexTab1.TabEnabled(1) = .AllowDPhrase Or .AllowPartyNames Or .AllowPartyTitle Or .AllowNonEF
            '---9.7.1
            Me.chkIsNonEF.Visible = .AllowNonEF

        End With
    End If

    PositionControls

    Me.vsIndexTab1.CurrTab = 0
'   force add mode
    btnNew_Click
    DoEvents
    Me.Added = True
    Me.txtRecipient.SetFocus
    txtRecipient_GotFocus
    
    m_bInit = False
    
    Application.ScreenRefresh
    Application.ScreenUpdating = True
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF2 Then
        GetRecipients
    ElseIf KeyCode = vbKeyF5 And btnNew.Enabled And vsIndexTab1.CurrTab = 0 Then
        btnNew_Click
    ElseIf KeyCode = vbKeyF6 And btnSave.Enabled And vsIndexTab1.CurrTab = 0 Then
        btnSave_Click
    ElseIf KeyCode = vbKeyF7 And btnDelete.Enabled And vsIndexTab1.CurrTab = 0 Then
        btnDelete_Click
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oFitArray As XArray
    Dim oDoc As MPO.CDocument
    
    Application.ScreenUpdating = False
    m_bCancelled = True
    m_bInit = True
    Set m_oRecips = Me.ServiceList.Recipients
    
'   load table fit options
    Set oFitArray = mpbase2.xarStringToxArray( _
        "Within Margins|1|Within Page Boundaries|2", 2)
    Me.cmbFit.Array = oFitArray
    ResizeTDBCombo Me.cmbFit, 6
   
'   bind recipient array
    Me.lstRecipients.Array = Me.ServiceList.Recipients.ListArray
    
    Set oDoc = New MPO.CDocument

'   get service list types available for service document
    On Error Resume Next
    m_lServiceType = oDoc.GetVar("Service_1_TypeID")
    On Error GoTo 0
    Me.cmbType.Array = g_oMPO.db.ServiceListDefs(m_lServiceType).ListSource
    ResizeTDBCombo Me.cmbType, 6
    
    '   assign row source
    With g_oMPO.Lists
        Me.cmbPartyTitle.Array = .Item("PleadingParties").ListItems.Source
        Me.mlcDeliveryPhrase.List = .Item("DPhrases").ListItems.Source
    End With

    ResizeTDBCombo Me.cmbPartyTitle, 10
    
'   show AddressBooks button
    Me.btnAddressBooks.Visible = g_bShowAddressBooks
    
'   set spinners to display Word measurement unit
    With Word.Options
        Me.stbLeftIndent.DisplayUnit = .MeasurementUnit
        Me.stbRightIndent.DisplayUnit = .MeasurementUnit
    End With
    
    'If Me.ParentForm Is Nothing Then _
        MoveToLastPosition Me
            
End Sub

Private Sub btnNew_Click()
    On Error GoTo ProcError
    Me.Added = True
    NewRecipient
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oRecip = Nothing
    Set m_oRecips = Nothing
    Set g_oPrevControl = Nothing
    'If Me.ParentForm Is Nothing Then _
        SetLastPosition Me
End Sub

Private Sub lstRecipients_DblClick()
    GetRecipients
End Sub

Private Sub lstRecipients_GotFocus()
    OnControlGotFocus Me.lstRecipients
End Sub

Private Sub lstRecipients_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyDelete Then
        DeleteRecipient
    End If
End Sub
Private Sub lstRecipients_RowChange()
    On Error GoTo ProcError
    If m_oRecips.Count Then
        ChangeActiveRecipient
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub m_oRecips_AfterRefresh(iStart As Integer)
    RefreshList
End Sub
Private Sub m_oRecips_ItemAdded(iIndex As Integer)
    RefreshList
    EnableInput True
End Sub
Private Sub m_oRecips_ItemRemoved(iIndex As Integer)
    RefreshList
    EnableInput m_oRecips.Count
End Sub

Private Sub mlcDeliveryPhrase_BeforeItemAdded(xItem As String)
    On Error GoTo ProcError
    xItem = mpbase2.AppendToDPhrase(xItem)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_Change()
    Me.Changed = True
End Sub

Private Sub mlcDeliveryPhrase_GotFocus()
    OnControlGotFocus Me.mlcDeliveryPhrase
End Sub

Private Sub mlcDeliveryPhrase_LostFocus()
    Me.mlcDeliveryPhrase.ListVisible = False
End Sub

Private Sub stbLeftIndent_GotFocus()
    OnControlGotFocus Me.stbLeftIndent
End Sub

Private Sub stbRightIndent_GotFocus()
    OnControlGotFocus Me.stbRightIndent
End Sub

Private Sub stbLeftIndent_Validate(Cancel As Boolean)
    If Not stbLeftIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub stbRightIndent_Validate(Cancel As Boolean)
    If Not stbRightIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub txtEMail_Change()
    Me.Changed = True
End Sub

Private Sub txtEMail_DblClick()
    GetRecipients
End Sub

Private Sub txtEMail_GotFocus()
    OnControlGotFocus Me.txtEMail
End Sub

Private Sub txtFax_Change()
    Me.Changed = True
End Sub

Private Sub txtFax_DblClick()
    GetRecipients
End Sub

Private Sub txtFax_GotFocus()
    OnControlGotFocus Me.txtFax
End Sub

Private Sub txtPhone_Change()
    Me.Changed = True
End Sub

Private Sub txtPhone_DblClick()
    GetRecipients
End Sub

Private Sub txtPhone_GotFocus()
    OnControlGotFocus Me.txtPhone
End Sub
Private Sub txtPartyNames_Change()
    Me.Changed = True
End Sub
Private Sub txtPartyNames_GotFocus()
    OnControlGotFocus Me.txtPartyNames
End Sub
Private Sub txtRecipient_Change()
    Me.Changed = True
    If m_oRecips.Count = 1 Then
        If Me.txtRecipient.Text = Empty Then
            Me.lblRecipients = "&Recipients:"
        Else
            Me.lblRecipients = "&Recipients (1):"
        End If
    End If
End Sub

Private Sub txtRecipient_DblClick()
    On Error GoTo ProcError
    GetRecipients
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtRecipient_GotFocus()
    OnControlGotFocus Me.txtRecipient
End Sub

Private Sub vsIndexTab1_Click()
    Dim oDef As mpDB.CServiceListDef
    
    If m_bInit Then Exit Sub
    
    Set oDef = g_oMPO.db.ServiceListDefs.Item(Me.cmbType.BoundText)
    Select Case vsIndexTab1.CurrTab
        Case 0:
            Me.vsIndexTab1.Height = 4650
            Me.vsIndexTab1.Top = 1980
            Me.lblRecipients.Visible = True
            
            Me.chkIncludePhone.Visible = oDef.AllowPhone And g_bShowAddressBooks
            Me.chkIncludeFax.Visible = oDef.AllowFax And g_bShowAddressBooks
            Me.chkIncludeEMail.Visible = oDef.AllowEMail And g_bShowAddressBooks

            Me.btnNew.Visible = True
            Me.btnSave.Visible = True
            Me.btnDelete.Visible = True
            Me.lstRecipients.Visible = True
            
            If Me.btnNew.Enabled Then
                Me.btnNew.SetFocus
            ElseIf Me.btnSave.Enabled Then
                Me.btnSave.SetFocus
            Else
                Me.lstRecipients.SetFocus
            End If
        Case 1:
            Me.vsIndexTab1.Height = 4650
            Me.vsIndexTab1.Top = 1980
            Me.lblRecipients.Visible = True
            
            Me.chkIncludePhone.Visible = oDef.AllowPhone And g_bShowAddressBooks
            Me.chkIncludeFax.Visible = oDef.AllowFax And g_bShowAddressBooks
            Me.chkIncludeEMail.Visible = oDef.AllowEMail And g_bShowAddressBooks

            Me.btnNew.Visible = True
            Me.btnSave.Visible = True
            Me.btnDelete.Visible = True
            Me.lstRecipients.Visible = True
            
            If Me.mlcDeliveryPhrase.Visible Then
                Me.mlcDeliveryPhrase.SetFocus
            ElseIf Me.cmbPartyTitle.Visible Then
                Me.cmbPartyTitle.SetFocus
            ElseIf Me.txtPartyNames.Visible Then
                Me.txtPartyNames.SetFocus
            End If
        Case 2:
            Me.vsIndexTab1.Height = 6655
            Me.vsIndexTab1.Top = -13
            Me.Shape1.Height = 6557
            Me.Line4.Y1 = 6150
            Me.Line4.Y2 = 6150
            Me.Line4.X1 = 0
            Me.Line4.X2 = 5600
            Me.lblRecipients.Visible = False
            Me.chkIncludeEMail.Visible = False
            Me.chkIncludeFax.Visible = False
            Me.chkIncludePhone.Visible = False
            Me.btnNew.Visible = False
            Me.btnSave.Visible = False
            Me.btnDelete.Visible = False
            Me.lstRecipients.Visible = False

            Me.cmbType.SetFocus
    End Select
    
End Sub
Private Sub UpdateForm()
    On Error GoTo ProcError
    With Me.ServiceList
        Me.cmbType.BoundText = .TypeID
        Me.cmbFit.BoundText = .WidthRule
        Me.stbLeftIndent.value = .LeftIndent / 72
        Me.stbRightIndent.value = .RightIndent / 72
        Me.chkIncludeEMail = Abs(.IncludeEMail)
        Me.chkIncludeFax = Abs(.IncludeFax)
        Me.chkIncludePhone = Abs(.IncludePhone)
        Me.chkSeparatePage = Abs(.InsertSeparatePage)
        RefreshList
        If .Recipients.Count Then _
            Me.lstRecipients.Bookmark = 0
        DoEvents
    End With
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmServiceList.UpdateForm"
    Exit Sub
End Sub
Private Sub UpdateObject()
    On Error GoTo ProcError
    With Me.ServiceList
        .TypeID = Me.cmbType.BoundText
        .WidthRule = Me.cmbFit.BoundText
        .LeftIndent = Me.stbLeftIndent.value * 72
        .RightIndent = Me.stbRightIndent.value * 72
        .IncludeEMail = 0 - Me.chkIncludeEMail
        .IncludeFax = 0 - Me.chkIncludeFax
        .IncludePhone = 0 - Me.chkIncludePhone
        .InsertSeparatePage = 0 - Me.chkSeparatePage
    End With
        
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmServiceList.UpdateObject"
    Exit Sub
End Sub
Private Sub cmbPartyTitle_Change()
    If Me.cmbPartyTitle.Text = "" Then
        Me.Changed = True
    End If
End Sub

Private Sub cmbPartyTitle_GotFocus()
    OnControlGotFocus cmbPartyTitle
End Sub

Private Sub cmbPartyTitle_ItemChange()
    Me.Changed = True
End Sub
