VERSION 5.00
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{E7785200-7134-46D5-BD04-D3F5C8C0BE97}#1.0#0"; "mpSal.ocx"
Begin VB.Form frmLetterRecipients 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add/Update Recipients"
   ClientHeight    =   6324
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   5472
   Icon            =   "frmLetterRecipients.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6324
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin mpSal.SalutationCombo cmbSalutation 
      Height          =   336
      Left            =   1680
      TabIndex        =   4
      Top             =   4140
      Width           =   3588
      _ExtentX        =   6329
      _ExtentY        =   593
      SelectionType   =   1
      FontName        =   "MS Sans Serif"
      FontSize        =   7.8
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "&Delete All"
      Height          =   435
      Left            =   1680
      TabIndex        =   7
      Top             =   5850
      Width           =   1395
   End
   Begin VB.TextBox txtHeaderAdditionalText 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   975
      Left            =   1695
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   6
      Top             =   4630
      Width           =   3645
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   435
      Left            =   3960
      TabIndex        =   9
      Top             =   5850
      Width           =   705
   End
   Begin VB.CommandButton btnAddressBooks 
      Height          =   435
      Left            =   3228
      Picture         =   "frmLetterRecipients.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   5850
      Width           =   705
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   435
      Left            =   4680
      TabIndex        =   10
      Top             =   5850
      Width           =   705
   End
   Begin VB.TextBox txtRecipients 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   3576
      Left            =   1680
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   120
      Width           =   3645
   End
   Begin TrueDBGrid60.TDBGrid grdRecipients 
      Height          =   3588
      Left            =   1680
      OleObjectBlob   =   "frmLetterRecipients.frx":0CAC
      TabIndex        =   11
      Top             =   120
      Visible         =   0   'False
      Width           =   3648
   End
   Begin VB.CheckBox chkRecipientsTableFormat 
      Appearance      =   0  'Flat
      Caption         =   "Use Ta&ble Formatting for Recipients"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   1800
      TabIndex        =   2
      Top             =   3768
      Width           =   3245
   End
   Begin VB.Label lblSalutation 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   " &Salutation:"
      ForeColor       =   &H00FFFFFF&
      Height          =   216
      Left            =   696
      TabIndex        =   3
      Top             =   4200
      Width           =   792
   End
   Begin VB.Label lblAdditionalText 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Page &2 Header:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   210
      TabIndex        =   5
      Top             =   4630
      Width           =   1275
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblTo 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&To:"
      ForeColor       =   &H00FFFFFF&
      Height          =   1200
      Left            =   180
      TabIndex        =   0
      Top             =   135
      Width           =   1260
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6384
      Left            =   -108
      Top             =   -24
      Width           =   1692
   End
   Begin VB.Menu mnuRecipients 
      Caption         =   "Recipients"
      Visible         =   0   'False
      Begin VB.Menu mnuRecipients_ClearDPhrase 
         Caption         =   "&Clear Delivery Phrase"
      End
      Begin VB.Menu mnuRecipients_Delete 
         Caption         =   "&Delete Recipient"
      End
      Begin VB.Menu mnuRecipients_DeleteAll 
         Caption         =   "Delete &All Recipients"
      End
   End
End
Attribute VB_Name = "frmLetterRecipients"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bCancelled As Boolean
Private m_xarRecips As XArray
Private m_oLetter As MPO.CLetter
Private m_bChangingRecipsWithoutTyping As Boolean
Private m_bShowSalutationDropdown As Boolean

Private m_bInitializing As Boolean
Public Property Let Letter(oNew As MPO.CLetter)
    Set m_oLetter = oNew
End Property
Public Property Get Letter() As MPO.CLetter
    Set Letter = m_oLetter
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Private Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Private Sub btnCancel_Click()
    Cancelled = True
    Me.Hide
End Sub

Private Sub btnFinish_Click()
    Cancelled = False
    FinishDocument
    Me.Hide
End Sub

Private Sub chkRecipientsTableFormat_Click() '---9.9.1 #5122
    Application.ScreenUpdating = False
    If chkRecipientsTableFormat = 1 Then
        Letter.UserRecipientTableThreshold = lCountChrs(Me.txtRecipients, vbCrLf & vbCrLf) + 1
    Else
        Letter.UserRecipientTableThreshold = 0
    End If
    Letter.Recipients = ""  '!gm  Refresh recipients
    Letter.Recipients = Me.txtRecipients
    Application.ScreenUpdating = True
End Sub

Private Sub chkRecipientsTableFormat_GotFocus()
    OnControlGotFocus Me.chkRecipientsTableFormat
End Sub

Private Sub cmdClear_Click()
    Me.txtHeaderAdditionalText = ""
    Me.txtRecipients = ""
    Me.txtRecipients.SetFocus
    Me.cmbSalutation.Text = Me.Letter.DefaultSalutation
End Sub

Private Sub Form_Activate()
'    If Me.Letter.Document.IsCreated Then
    Dim iThreshold As Integer
    
    If Me.Letter.Document.HasReuseAuthor Then     '9.7.1 - #3827
        UpdateForm
    Else
        Me.cmbSalutation.Text = Me.Letter.DefaultSalutation
    End If
    If Me.txtRecipients.Visible Then
        Me.txtRecipients.SetFocus
    Else
        Me.grdRecipients.SetFocus
    End If
    m_bInitializing = False
    
    '9.9.1 #5122
    Me.chkRecipientsTableFormat.Visible = Me.Letter.AllowUserRecipientThreshold
    
    If Me.chkRecipientsTableFormat.Visible Then
        iThreshold = IIf(Me.Letter.UserRecipientTableThreshold = "", 0, Me.Letter.UserRecipientTableThreshold)
        If iThreshold > 0 Then
            Me.chkRecipientsTableFormat = vbChecked
        End If
    End If
    
End Sub

Private Sub Form_Load()
    Dim xVal As String
    Cancelled = True
    m_bInitializing = True
    If m_oLetter Is Nothing Then _
        Set m_oLetter = g_oMPO.NewLetter
    
    Set m_xarRecips = g_oMPO.NewXArray
    m_xarRecips.ReDim 0, -1, 0, 1
    Me.grdRecipients.Array = m_xarRecips
'   set Salutation control properties
    With Me.cmbSalutation
        .Schema = xGetTemplateConfigVal(Me.Letter.Template.ID, _
                    "SalutationSchema")
        m_bShowSalutationDropdown = xGetTemplateConfigVal(Me.Letter.Template.ID, _
                                    "ShowSalutationDropdown")
        
        '#4404
        xVal = xGetTemplateConfigVal(Me.Letter.Template.ID, _
            "MultiRecipientSeparator")
                    
        If xVal <> "" Then
            .MultiRecipientSeparator = xVal
        End If
    End With
    
    'MoveToLastPosition Me
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF2 Then
        btnAddressBooks_Click
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
    Set frmLetterRecipients = Nothing
    Set m_xarRecips = Nothing
End Sub

Private Sub grdRecipients_DblClick()
    If grdRecipients.Col = 0 Then
        GetContacts
    End If
End Sub

Private Sub txtHeaderAdditionalText_GotFocus()
    OnControlGotFocus Me.txtHeaderAdditionalText
End Sub

Private Sub txtRecipients_Change()
    Dim iNumEntries As Integer
    On Error GoTo ProcError
    If Me.txtRecipients <> "" Then
        iNumEntries = lCountChrs(Me.txtRecipients, vbCrLf & vbCrLf) + 1
        If iNumEntries = 1 Then
            Me.lblTo = " &To:" & vbCrLf & " (1 recipient)"
        Else
            Me.lblTo = " &To:" & vbCrLf & " (" & iNumEntries & " recipients)"
        End If
    Else
        Me.lblTo = " &To:"
'       clear Salutation list
        Me.cmbSalutation.ClearList
    End If
    With Me.cmbSalutation
        If Not m_bChangingRecipsWithoutTyping And .AllowDropdown Then
            'the recips textbox is being changed by keystrokes-
            'since cmbSalutation doesn't know the changes, we
            'disable the dropdown salutation list
            .AllowDropdown = False
            .ShowDropdown False
        
            'save salutation list for reuse
            Letter.Document.SaveItem "SalParsedData", "Letter", , ""
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub btnAddressBooks_Click()
    GetContacts
End Sub
Private Sub GetContacts()
    Dim vTo As Variant
    Static bCIActive As Boolean
    Dim i As Integer
    Dim oTemp As XArray
    Dim oContacts As Object
    
    On Error GoTo ProcError
  
    If Not g_bShowAddressBooks Then Exit Sub
    If bCIActive Then Exit Sub
    bCIActive = True
    
    g_oMPO.GetLetterRecipients vTo, , , , , , oContacts, True
                                
    m_bChangingRecipsWithoutTyping = True
                                
    If vTo <> "" Then
        If Me.txtRecipients.Visible Then
            If Me.txtRecipients <> "" Then
                Me.txtRecipients = xTrimTrailingChrs(Me.txtRecipients, vbCrLf) & _
                                vbCrLf & vbCrLf & vTo
            Else
                Me.txtRecipients = vTo
            End If
        Else
            Set oTemp = xarStringToxArray(vTo, 1, vbCrLf & vbCrLf)
            For i = 0 To oTemp.UpperBound(1)
                m_xarRecips.Insert 1, m_xarRecips.UpperBound(1) + 1
                m_xarRecips.value(m_xarRecips.UpperBound(1), 0) = oTemp.value(i, 0)
            Next i
            grdRecipients.Rebind
            SetRecipientsText
        End If
    End If
    'fill salutation dropdown if there are contacts
    'and the dropdown is to be used
    If oContacts.Count And m_bShowSalutationDropdown Then
        With Me.cmbSalutation
            .AllowDropdown = True
            .ShowDropdown True

            'add the contacts to salutations
            If g_oMPO.Contacts.CIVersion = mpCIVersion_1 Then
                .AddContacts oContacts
            Else
                .AddContacts20 oContacts
            End If
            .RefreshList
            
        End With
    End If
    SetRecipientFields '9.7.1 #4162
    Me.SetFocus
    bCIActive = False ' **** NEW LINE
    m_bChangingRecipsWithoutTyping = False
    Exit Sub
ProcError:
    bCIActive = False ' **** NEW LINE
    m_bChangingRecipsWithoutTyping = False
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtRecipients_DblClick()
    If Not g_bShowAddressBooks Then Exit Sub
    On Error GoTo ProcError
    GetContacts
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtRecipients_GotFocus()
    OnControlGotFocus Me.txtRecipients
End Sub
Private Sub txtRecipients_LostFocus()
    SetRecipientFields '9.7.1 #4162
    If Me.chkRecipientsTableFormat = 1 Then _
        Letter.UserRecipientTableThreshold = lCountChrs(Me.txtRecipients, vbCrLf & vbCrLf) + 1  '9.9.1 #5122
End Sub

Private Sub mnuRecipients_Delete_Click()
    Dim i As Integer
    
    On Error GoTo ProcError
    With grdRecipients
        If .Array.UpperBound(1) < 0 Or _
            IsNull(.Bookmark) Then Exit Sub
        i = .Bookmark
        m_xarRecips.Delete 1, i
        ' Reset array to Recipients array
        .Rebind
        .Col = 0
        .Bookmark = 0
        If i - 1 < .Array.UpperBound(1) Then
            .Bookmark = i - 1
        Else
            .Bookmark = .Array.UpperBound(1)
        End If
        SetRecipientsText
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub mnuRecipients_DeleteAll_Click()
    Dim i As Integer
    Dim iCount As Integer
    
    If m_xarRecips.UpperBound(1) = -1 Then
        Exit Sub
    End If

    m_xarRecips.ReDim 0, -1, 0, 0
    With grdRecipients
        .Array = m_xarRecips
        .Rebind
    End With
    SetRecipientsText
    g_oMPO.ScreenUpdating = False
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub SetRecipientsText()
    Dim i As Integer
    Dim xRecips As String
    On Error GoTo ProcError
    With grdRecipients.Array
        On Error Resume Next
        If grdRecipients.DataChanged Then
            .Update
        End If
        For i = 0 To .UpperBound(1)
            If .value(i, 0) <> "" Then _
                xRecips = xRecips & xTrimTrailingChrs(.value(i, 0), vbCrLf) & vbCrLf
            xRecips = xRecips & vbCrLf
        Next i
        xRecips = xTrimTrailingChrs(xRecips, vbCrLf)
        If xRecips = "" And Me.grdRecipients.Array.Count(1) > 0 Then
            MsgBox "Recipient Address cannot be blank.  To Delete an existing addressee, " & _
                "right-click on grid and select 'Delete Recipient'.", vbInformation
            For i = 0 To grdRecipients.Array.UpperBound(1)
                If grdRecipients.Array.value(i, 0) = "" Then
                    grdRecipients.Bookmark = i
                    grdRecipients.Col = 0
                    Exit For
                End If
            Next i
            grdRecipients.SetFocus
            Exit Sub
        End If
        Me.txtRecipients = xRecips
        SetRecipientFields
    End With
    Exit Sub
ProcError:
    g_oError.Show Err, "SetRecipientsText"
End Sub
Private Sub SetRecipientFields()
    Me.txtHeaderAdditionalText = ExtractFirstLineWithSeparators(txtRecipients, , _
                                        Me.Letter.HeaderNamesSeparator, _
                                        Me.Letter.HeaderNamesFinalSeparator) '9.7.1 #4162
End Sub
Private Sub UpdateForm()
    With Me.Letter
        m_bChangingRecipsWithoutTyping = True
        Me.txtRecipients = .Recipients
        m_bChangingRecipsWithoutTyping = False
        Me.txtHeaderAdditionalText = .HeaderAdditionalText
        With Me.cmbSalutation
            'set up list
            .ParsedContactData = Letter.Document.GetVar("Letter_1_SalParsedData")
            If .ParsedContactData = Empty And Letter.Salutation <> Empty Then
                .AllowDropdown = False
            End If
            
            .Text = Letter.Salutation
        End With
    End With
End Sub
Private Sub FinishDocument()
    Dim rngHeader As Word.Range
    Dim bForce As Boolean
    Dim rngPara As Word.Range
    
    On Error GoTo ProcError
    EchoOff
    Selection.HomeKey wdStory
    With Selection.Find
        .Style = "Addressee"
        .Text = ""
        .Format = True
        .Execute
        If Not .Found Then
            MsgBox "Could not determine Addressee position.  Please make sure 'Addressee' style is applied to existing Addresses.", vbInformation, App.Title
            Exit Sub
        End If
    End With
    If Selection.Information(wdWithInTable) Then
        Selection.MoveEnd wdTable
    Else
        While Selection.Next(wdParagraph, 1).Style = "Addressee"
            Selection.MoveEnd wdParagraph, 1
        Wend
        Selection.MoveEndWhile 13, -1
    End If
    ActiveDocument.Bookmarks.Add "zzmpRecipients", Selection.Range
    Set rngHeader = Selection.Sections(1).Headers(wdHeaderFooterPrimary).Range
    With rngHeader
        .Collapse wdCollapseEnd
        While .Previous(wdParagraph, 1).Fields.Count = 1 Or .Previous(wdParagraph).Frames.Count = 1 '#3744
            .Move wdParagraph, -1
            '9.7.1 - #3874
            On Error Resume Next
            Set rngPara = rngHeader.Previous(wdParagraph, 1)
            On Error GoTo ProcError
            If rngPara Is Nothing Then
                GoTo ExitWhile
            End If
        Wend
ExitWhile:
        If Letter.HeaderAdditionalText = "" And Me.txtHeaderAdditionalText <> "" Then
            .InsertParagraphBefore
            .Collapse wdCollapseStart
            .Style = wdStyleHeader
            .Bookmarks.Add "zzmpHeaderAdditionalText"
        Else
            .Move wdParagraph, -1
            .MoveEnd wdParagraph, 1
            .MoveEnd wdCharacter, -1
            If Me.txtHeaderAdditionalText <> "" Then
                .Bookmarks.Add "zzmpHeaderAdditionalText"
            Else
                .Paragraphs(1).Range.Delete
            End If
        End If
            
    End With
    Selection.HomeKey wdStory
    With Selection.Find
        'GLOG : 5079 : CEH
        .ClearFormatting
        'GLOG : 5089 : CEH
        .Style = wdStyleSalutation
        .Text = ""
        .Format = True
        .Execute
        If Not .Found Then
            MsgBox "Could not determine Salutation position.  Please make sure 'Salutation' style is applied to existing Salutation.", vbInformation, App.Title
        Else
            Selection.MoveEndWhile 13, -1
            ActiveDocument.Bookmarks.Add "zzmpSalutation", Selection.Range
        End If
        'GLOG : 5079 : CEH
        .ClearFormatting
    End With
    bForce = g_oMPO.ForceItemUpdate
    g_oMPO.ForceItemUpdate = True
    Letter.Recipients = xTrimTrailingChrs(Me.txtRecipients, vbCrLf)
    Letter.HeaderAdditionalText = xTrimTrailingChrs(Me.txtHeaderAdditionalText, vbCrLf)
    Letter.Salutation = Me.cmbSalutation.Text
    'save salutation list for reuse
    Letter.Document.SaveItem "SalParsedData", _
        "Letter", , Me.cmbSalutation.ParsedContactData
    g_oMPO.ForceItemUpdate = bForce
    Letter.Document.ClearBookmarks
    EchoOn
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub grdRecipients_AfterUpdate()
    SetRecipientsText
End Sub

Private Sub grdRecipients_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    grdRecipients.EditActive = True
End Sub


Private Sub grdRecipients_Click()
    OnControlGotFocus Me.grdRecipients
End Sub

Private Sub grdRecipients_GotFocus()
    OnControlGotFocus grdRecipients
    grdRecipients.MarqueeStyle = dbgSolidCellBorder
    grdRecipients.TabAction = dbgGridNavigation
    grdRecipients.BackColor = g_lActiveCtlColor
    grdRecipients.EditActive = True
    If m_xarRecips.UpperBound(1) = -1 Then
        SendKeys "{Right}", True
    End If
End Sub

Private Sub grdRecipients_KeyDown(KeyCode As Integer, Shift As Integer)
    With grdRecipients
        If KeyCode = vbKeyTab Then
            If Shift = 0 Then
                If .Col = 0 And .Text = Empty Then
                    grdRecipients.TabAction = dbgControlNavigation
                End If

            Else
                                    
                If .Bookmark = 0 And .Row = 0 And .Col = 0 Then
                    .TabAction = dbgControlNavigation
                ElseIf IsNull(.Bookmark) And .Array.UpperBound(1) = -1 Then
                    .TabAction = dbgControlNavigation
                End If
            End If
        ElseIf KeyCode = vbKeyReturn Then
            KeyCode = 0
        ElseIf KeyCode = vbKeyF2 Then
            btnAddressBooks_Click
        End If
    End With
End Sub

Private Sub grdRecipients_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        KeyAscii = 0
        With grdRecipients
            .SelStart = .SelStart + .SelLength
            .SelText = vbCrLf
            .SelStart = .SelStart + 2
            .SelLength = 0
        End With
    End If
End Sub

Private Sub grdRecipients_LostFocus()
    grdRecipients.MarqueeStyle = dbgNoMarquee
    grdRecipients.BackColor = g_lInactiveCtlColor
    grdRecipients.TabAction = dbgGridNavigation
    If grdRecipients.DataChanged Then _
        grdRecipients.Update
End Sub

Private Sub cmbSalutation_GotFocus()
    
    OnControlGotFocus Me.cmbSalutation

End Sub

