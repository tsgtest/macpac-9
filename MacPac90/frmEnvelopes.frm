VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Object = "{FC715925-5921-4AC0-B4BD-36B619407BAE}#1.1#0"; "MPCM.ocx"
Begin VB.Form frmEnvelopes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create Envelopes"
   ClientHeight    =   5652
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   5484
   Icon            =   "frmEnvelopes.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5652
   ScaleWidth      =   5484
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3330
      TabIndex        =   29
      Top             =   5190
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4395
      TabIndex        =   30
      Top             =   5190
      Width           =   1000
   End
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   2565
      Picture         =   "frmEnvelopes.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   28
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   5190
      Width           =   690
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   5625
      Left            =   -15
      TabIndex        =   31
      TabStop         =   0   'False
      Top             =   -15
      Width           =   5535
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|E&xtras"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   " "
         Height          =   5184
         Left            =   12
         TabIndex        =   33
         Top             =   15
         Width           =   5505
         Begin VB.CheckBox chkUSPS 
            Appearance      =   0  'Flat
            Caption         =   "USPS Stan&dards"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   3615
            TabIndex        =   5
            Top             =   2565
            Width           =   1590
         End
         Begin mpControls.MultiLineCombo mlcReturnAddress 
            Height          =   975
            Left            =   1770
            TabIndex        =   7
            Top             =   3000
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   1715
            BackColor       =   -2147483643
            Separator       =   ""
            SeparatorSpecial=   0
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.CheckBox chkReturnAddress 
            Appearance      =   0  'Flat
            Caption         =   "Return &Address"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   1860
            TabIndex        =   4
            Top             =   2565
            Width           =   1485
         End
         Begin VB.TextBox txtAddresses 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   1785
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   1
            Top             =   120
            Width           =   3570
         End
         Begin TrueDBList60.TDBCombo cmbEnvelopeTypes 
            Height          =   345
            Left            =   1770
            OleObjectBlob   =   "frmEnvelopes.frx":0CAC
            TabIndex        =   9
            Top             =   4140
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbOutputTo 
            Height          =   585
            Left            =   1770
            OleObjectBlob   =   "frmEnvelopes.frx":2CFC
            TabIndex        =   11
            Top             =   4635
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbBarCode 
            Height          =   345
            Left            =   1770
            OleObjectBlob   =   "frmEnvelopes.frx":4F0F
            TabIndex        =   3
            Top             =   2100
            Width           =   3615
         End
         Begin VB.Label lblBarCode 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " &Bar Code:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   825
            TabIndex        =   2
            Top             =   2175
            Width           =   765
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Include:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   1020
            TabIndex        =   34
            Top             =   2595
            Width           =   555
         End
         Begin VB.Label lblReturnAddress 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Ret&urn Address:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   375
            TabIndex        =   6
            Top             =   3015
            Width           =   1200
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00FFFFFF&
            X1              =   -60
            X2              =   5495
            Y1              =   5124
            Y2              =   5124
         End
         Begin VB.Line Line1 
            BorderColor     =   &H00FFFFFF&
            X1              =   0
            X2              =   5530
            Y1              =   5115
            Y2              =   5115
         End
         Begin VB.Label lblEnvelopeTypes 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Envelope Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   270
            TabIndex        =   8
            Top             =   4185
            Width           =   1335
         End
         Begin VB.Label lblAddresses 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Recipients:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   795
            TabIndex        =   0
            Top             =   135
            Width           =   795
         End
         Begin VB.Label lblOutputTo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Output Envelopes To:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   15
            TabIndex        =   10
            Top             =   4710
            Width           =   1590
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6600
            Left            =   -15
            Top             =   -1455
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   5184
         Left            =   6024
         TabIndex        =   32
         Top             =   15
         Width           =   5505
         Begin MPCM.ClientMatterComboBox cmbCM 
            Height          =   315
            Left            =   1755
            TabIndex        =   13
            Top             =   210
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            SourceClass     =   "mpCMIA.CClientMatter"
            Col1Caption     =   "No."
            ColWidth1       =   1020
            ColWidth2       =   1020
            ListRows        =   6
            ComboType       =   2
            SelectTextOnFocus=   -1  'True
            PopulateOnLoad  =   0   'False
            AdditionalClientFilter=   ""
            AdditionalMatterFilter=   ""
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase 
            Height          =   690
            Left            =   1740
            TabIndex        =   17
            Top             =   1230
            Width           =   3630
            _ExtentX        =   6414
            _ExtentY        =   1207
            ListRows        =   10
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase2 
            Height          =   690
            Left            =   1740
            TabIndex        =   19
            Top             =   2100
            Width           =   3630
            _ExtentX        =   6414
            _ExtentY        =   1207
            ListRows        =   10
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin TrueDBList60.TDBCombo cmbFontName 
            Height          =   600
            Left            =   1740
            OleObjectBlob   =   "frmEnvelopes.frx":6F59
            TabIndex        =   21
            Top             =   2985
            Width           =   3615
         End
         Begin VB.TextBox txtAuthorInitials 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1755
            TabIndex        =   15
            Top             =   705
            Width           =   3615
         End
         Begin mpControls3.SpinTextInternational spnFontSize 
            Height          =   315
            Left            =   1755
            TabIndex        =   23
            Top             =   3540
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            MaxValue        =   37
            Value           =   12
         End
         Begin mpControls3.SpinTextInternational spnLeftMargin 
            Height          =   315
            Left            =   1755
            TabIndex        =   25
            Top             =   4065
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   5
            AppendSymbol    =   -1  'True
            Value           =   0.25
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin mpControls3.SpinTextInternational spnTopMargin 
            Height          =   315
            Left            =   1755
            TabIndex        =   27
            Top             =   4590
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   5
            AppendSymbol    =   -1  'True
            Value           =   0.5
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin VB.Label lblDeliveryPhrase2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Confidential Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   420
            Left            =   45
            TabIndex        =   18
            Top             =   2130
            Width           =   1545
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblAuthorInitials 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " &Author Name/Initials:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   60
            TabIndex        =   14
            Top             =   750
            Width           =   1530
         End
         Begin VB.Label lblDeliveryPhrase 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " &Delivery Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   150
            TabIndex        =   16
            Top             =   1260
            Width           =   1440
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblClientMatter 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Client-Matter No.:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   150
            TabIndex        =   12
            Top             =   255
            Width           =   1440
         End
         Begin VB.Label lblTopMargin 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Top Margin:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   150
            TabIndex        =   26
            Top             =   4620
            Width           =   1440
         End
         Begin VB.Label lblNormalFontName 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Font &Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   150
            TabIndex        =   20
            Top             =   3045
            Width           =   1440
         End
         Begin VB.Label lblNormalFontSize 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Font &Size:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   0
            Left            =   150
            TabIndex        =   22
            Top             =   3555
            Width           =   1440
         End
         Begin VB.Label lblLeftMargin 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "L&eft Indent:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   1
            Left            =   150
            TabIndex        =   24
            Top             =   4080
            Width           =   1440
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6195
            Left            =   0
            Top             =   -1080
            Width           =   1695
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00FFFFFF&
            X1              =   -30
            X2              =   5525
            Y1              =   5100
            Y2              =   5100
         End
      End
   End
End
Attribute VB_Name = "frmEnvelopes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const mpIniSec As String = "Envelopes"

Private WithEvents m_oEnv As MPO.CEnvelope
Attribute m_oEnv.VB_VarHelpID = -1
Private m_bInit As Boolean
Private m_bCanceled As Boolean

'******************************************
'---properties
'******************************************

Property Get Cancelled() As Boolean
    Cancelled = m_bCanceled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCanceled = bNew
End Property

Public Property Get Envelope() As MPO.CEnvelope
    Set Envelope = m_oEnv
End Property

Public Property Let Envelope(oNew As MPO.CEnvelope)
    Set m_oEnv = oNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Private Sub btnAddressBooks_Click()
    GetContacts
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    Me.Cancelled = True
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    Me.Hide
    DoEvents
    Me.Cancelled = False
    
    SaveDefaults
    UpdateObject
    Application.ScreenUpdating = False
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub chkReturnAddress_Click()
    Me.mlcReturnAddress.Enabled = Me.chkReturnAddress
    If Me.chkReturnAddress Then
        Me.mlcReturnAddress.Text = GetReturnAddress()
    Else
        Me.mlcReturnAddress.Text = Empty
    End If
    Me.lblReturnAddress.Enabled = Me.chkReturnAddress
End Sub

Private Sub chkReturnAddress_GotFocus()
    OnControlGotFocus Me.chkReturnAddress
End Sub

Private Sub chkUSPS_GotFocus()
    OnControlGotFocus Me.chkUSPS
End Sub

Private Sub cmbBarCode_GotFocus()
    OnControlGotFocus Me.cmbBarCode
End Sub

Private Sub cmbBarCode_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbBarCode, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCM_ConnectionFailed(ByVal Message As String)

    MsgBox IIf(Message = "", "Could not connect to the client matter data source.  ", Message & vbCr & vbCr) _
            & "Instead of selecting client matter numbers from a list," & _
            " you can manually type the numbers instead.", _
            vbExclamation, App.Title
        
    With Me.cmbCM
        .RunConnected = False
        .ComboType = ComboType_TextOnly
    End With
    
End Sub

Private Sub cmbCM_GotFocus()
    OnControlGotFocus Me.cmbCM, "zzmpReline"
End Sub

Private Sub cmbCM_LostFocus()
    On Error GoTo ProcError
    Me.Envelope.ClientMatterNumber = Me.cmbCM.value
    
    'save SQL for reuse
    Me.Envelope.Document.SaveItem "CMFilterString", _
        "Envelope", , Me.cmbCM.AdditionalClientFilter
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCM_Validate(Cancel As Boolean)
    Dim iIsValid As cmValidationStatus
    
    If g_bCM_AllowValidation Then
        If Me.cmbCM.RunConnected And Me.cmbCM.value <> Empty Then
            
            iIsValid = Me.cmbCM.Validity
            
            If iIsValid = cmValidationStatus_Invalid Then
                MsgBox "Invalid client matter number."
                Cancel = True
            End If
        End If
    End If
End Sub

Private Sub cmbEnvelopeTypes_GotFocus()
    OnControlGotFocus Me.cmbEnvelopeTypes
End Sub

Private Sub cmbEnvelopeTypes_ItemChange()
   Dim lID As Long
   Dim oSpinner As mpControls3.SpinTextInternational
   
   lID = cmbEnvelopeTypes.BoundText
    
    With Envelope.Definitions.Item(lID)
        If Not Me.Initializing Then
'           don't set defaults during intit because
'           they are gotten from the ini
            Set oSpinner = Me.spnLeftMargin
            oSpinner.value = .AddressLeftDefault
            oSpinner.MaxValue = .AddressLeftMaximum
            oSpinner.MinValue = .AddressLeftMinimum
        
            Set oSpinner = Me.spnTopMargin
            oSpinner.value = .AddressTopDefault
            oSpinner.MaxValue = .AddressTopMaximum
            oSpinner.MinValue = .AddressTopMinimum
        Else
'           set max/min values upon ini
            Set oSpinner = Me.spnLeftMargin
            oSpinner.MaxValue = .AddressLeftMaximum
            oSpinner.MinValue = .AddressLeftMinimum

            Set oSpinner = Me.spnTopMargin
            oSpinner.MaxValue = .AddressTopMaximum
            oSpinner.MinValue = .AddressTopMinimum
        End If
        
'       set these all the time because they're
'       determined by the envelope type
        If .AllowReturnAddress Then
            Me.chkReturnAddress.Enabled = True
        Else
            Me.chkReturnAddress.Enabled = False
            Me.chkReturnAddress.value = vbUnchecked
        End If
        
        If .AllowBarCode Then
            Me.lblBarCode.Enabled = True
            Me.cmbBarCode.Enabled = True
            'GLOG 4969 - 9.8.1030
            Me.lblBarCode.Visible = True
            Me.cmbBarCode.Visible = True
        Else
            Me.lblBarCode.Enabled = False
            Me.cmbBarCode.Enabled = False
            'GLOG 4969 - 9.8.1030
            Me.lblBarCode.Visible = False
            Me.cmbBarCode.Visible = False
        End If
        
        Me.mlcDeliveryPhrase.Enabled = .AllowDPhrases
        '9.7.1 #4024
        If .DPhrase1Caption <> "" Then _
            Me.lblDeliveryPhrase.Caption = .DPhrase1Caption
        Me.mlcDeliveryPhrase2.Visible = .AllowDPhrases And .SplitDPhrases
        Me.lblDeliveryPhrase2.Visible = .AllowDPhrases And .SplitDPhrases
        If .SplitDPhrases Then
            mlcDeliveryPhrase.List = g_oMPO.db.Lists("DPhrases1").ListItems.Source
            mlcDeliveryPhrase2.List = g_oMPO.db.Lists("DPhrases2").ListItems.Source
            If .DPhrase2Caption <> "" Then _
                Me.lblDeliveryPhrase2.Caption = .DPhrase2Caption
        Else
            mlcDeliveryPhrase2.Text = ""
            mlcDeliveryPhrase.List = g_oMPO.db.Lists("DPhrases").ListItems.Source
        End If
        '---
        Me.cmbCM.Enabled = .AllowBillingNumber
        Me.cmbCM.TabStop = .AllowBillingNumber
        Me.txtAuthorInitials.Enabled = .AllowAuthorInitials
        Me.lblClientMatter.Enabled = .AllowBillingNumber
        Me.lblAuthorInitials.Enabled = .AllowAuthorInitials
        
    End With
    
End Sub

Private Sub cmbEnvelopeTypes_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbEnvelopeTypes, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbEnvelopeTypes_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbEnvelopeTypes)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbFontName_GotFocus()
    OnControlGotFocus Me.cmbFontName
End Sub

Private Sub cmbOutputTo_GotFocus()
    OnControlGotFocus Me.cmbOutputTo
End Sub

Private Sub cmbOutputTo_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbOutputTo, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub cmbOutputTo_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbOutputTo)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    
'   set defaults from ini
    On Error GoTo ProcError
    GetDefaults
    
    Me.Refresh
    

    DoEvents
    Me.Initializing = False
    Me.vsIndexTab1.CurrTab = 0
    Exit Sub
ProcError:
    Me.Initializing = False
    g_oError.Show Err, "Error Activating Envelopes Form"
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If IsPressed(vbKeyF2) Then
        GetContacts
    End If
End Sub

Private Sub Form_Load()
    Dim xAddresses As String
    Dim xDPhrases As String
    Dim oDoc As MPO.CDocument
    
    On Error GoTo ProcError
    
    Me.Initializing = True
    Me.Cancelled = True
    
    Set m_oEnv = g_oMPO.NewEnvelope
    
'   assign row source to Envelope types
    Me.cmbEnvelopeTypes.Array = Envelope.Definitions.ListSource
    
'   assign row source
    With g_oMPO.Lists
        ' Me.mlcDeliveryPhrase.List = .Item("DPhrases").ListItems.Source '9.7.1 #4024
        Me.cmbFontName.Array = .Item("Fonts").ListItems.Source
        Me.cmbOutputTo.Array = .Item("LabelsOutput").ListItems.Source
        Me.cmbBarCode.Array = xarStringToxArray("0|(none)|1|Above Address|2|Below Address", 2, "|")
    End With
    
'   resize combos
    ResizeTDBCombo Me.cmbOutputTo, 4
    ResizeTDBCombo Me.cmbFontName, 6
    ResizeTDBCombo Me.cmbEnvelopeTypes, 5
    ResizeTDBCombo Me.cmbBarCode, 3
    
    g_oMPO.GetSelectedAddresses xAddresses, xDPhrases
    
    '9.7.1 - #4143
    Set oDoc = New MPO.CDocument
    Me.cmbCM.value = oDoc.ClientMatter
    
    If xAddresses = Empty Then  'load recipients
        xAddresses = oDoc.GetRecipients
    End If
    
    xAddresses = xCleanAddress(xAddresses) '9.9.1010 #5141  !gm
    
    'load cc & bccs - from CLetter only
    Me.txtAddresses = xAddMoreRecipients(xAddresses, False)
    Me.mlcDeliveryPhrase.Text = xDPhrases

'   update client\matter label
    Me.lblClientMatter.Caption = GetMacPacIni _
                                 ("Envelopes", "ClientMatterDBoxCaption")
                                 
'   set Client Matter Control properties
    With Me.cmbCM
        .RunConnectedToolTip = g_xCM_RunConnectedTooltip
        .RunDisconnectedToolTip = g_xCM_RunDisconnectedTooltip
        .RunConnected = g_bCM_RunConnected
        If Not .RunConnected Then
            .ComboType = ComboType_TextOnly
        Else
            .SourceClass = g_xCM_Backend
            If g_bCM_SearchByMatter Then .SearchMode = 1 '***9.7.1 - #4186
            .AllowUserToClearFilters = g_bCM_AllowUserToClearFilters
            .ShowLoadMessage = g_bCM_ShowLoadMsg
            .Column1Heading = g_xCM_Column1Heading
            .Column2Heading = g_xCM_Column2Heading
            .Separator = g_xCM_Separator
        End If
    End With

    
'   show AddressBooks button
    Me.btnAddressBooks.Visible = g_bShowAddressBooks
    
'   show USPS Standards checkbox
    Me.chkUSPS.Visible = g_bUSPSStandards
    
'   set spinners to display Word measurement unit
    With Word.Options
        Me.spnLeftMargin.DisplayUnit = .MeasurementUnit
        Me.spnTopMargin.DisplayUnit = .MeasurementUnit
    End With
    
    'mpbase2.MoveToLastPosition Me
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Loading Envelopes Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'mpbase2.'SetLastPosition Me
    Set frmEnvelopes = Nothing
    Set m_oEnv = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Unloading Envelopes Form"
    Exit Sub
End Sub

Private Sub m_oEnv_BeforeOutput(bSuppressPageSetup As Boolean)
    '---9.7.1 - 4232
    bSuppressPageSetup = UCase(GetMacPacIni("Envelopes", "ShowPageSetupDialog")) = "FALSE"
End Sub
Private Sub mlcDeliveryPhrase_BeforeItemAdded(xAddr As String)
    On Error GoTo ProcError
    xAddr = mpbase2.AppendToDPhrase(xAddr)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub GetDefaults()
    Dim lAuthor As Long
    Dim xAuthor As String
    Dim lEnvType As Long
    Dim xFont As String
    Dim sLMargin As Single
    Dim sTMargin As Single
    Dim sFontSize As Single
    Dim bIncludeBarCode As Boolean
    Dim bIncludeReturnAddress As Boolean
    Dim iOutput As Integer
    Dim lID As Long
    Dim xBarCode As String
    
    On Error Resume Next
    xAuthor = GetUserIni(mpIniSec, "Author")
    lEnvType = GetUserIni(mpIniSec, "Type")
    iOutput = GetUserIni(mpIniSec, "Output")
    xFont = GetUserIni(mpIniSec, "FontName")
    sFontSize = GetUserIniNumeric(mpIniSec, "FontSize")
    sLMargin = GetUserIniNumeric(mpIniSec, "LeftMargin")
    sTMargin = GetUserIniNumeric(mpIniSec, "TopMargin")
    xBarCode = GetUserIni(mpIniSec, "BarCodeFormat")
    bIncludeReturnAddress = GetUserIni(mpIniSec, "ReturnAddress")
    
    On Error GoTo ProcError
    Me.Envelope.Office = g_oMPO.Offices.Default.ID
    
'   Assign author
    Me.txtAuthorInitials = xAuthor
    
'   assign output location
    With Me.cmbOutputTo
        .BoundText = iOutput
        If .BoundText = Empty Then
'           invalid output value - select first item in list
            .SelectedItem = 0
        End If
    End With
    
'   Assign envelopes type
    With Me.cmbEnvelopeTypes
        .BoundText = lEnvType
        If .BoundText = Empty Then
'           invalid label type - select first item in list
            .SelectedItem = 0
        End If
        lID = .BoundText
    End With
    
    On Error Resume Next
'   Assign left margin
    If sLMargin Then
        Me.spnLeftMargin.value = sLMargin
    Else
        Me.spnLeftMargin.value = m_oEnv.Definitions.Item(lID).AddressLeftDefault
    End If
    
'   Assign top margin
    If sTMargin Then
        Me.spnTopMargin.value = sTMargin
    Else
        Me.spnTopMargin.value = m_oEnv.Definitions.Item(lID).AddressTopDefault
    End If
    
'   Assign font
    With Me.cmbFontName
        If xFont <> "" Then
            .BoundText = xFont
        Else
            .BoundText = .Columns(1)
        End If
    End With
    
'   Assign font size
    If sFontSize Then
        Me.spnFontSize.value = sFontSize
    Else
        Me.spnFontSize.value = 12
    End If
    
    If xBarCode <> "" Then
        Me.cmbBarCode.BoundText = xBarCode
    Else
        Me.cmbBarCode.Bookmark = 0
    End If

'   fill mlc with offices
    Me.mlcReturnAddress.List = g_oMPO.db.Offices.ListSource

'   assign bar code & return Address
    Me.chkReturnAddress.value = Abs(bIncludeReturnAddress)
    Me.mlcReturnAddress.Enabled = Me.chkReturnAddress
    Me.lblReturnAddress.Enabled = Me.chkReturnAddress
    
    cmbEnvelopeTypes_ItemChange
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmEnvelopes.GetDefaults"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub SaveDefaults()
    On Error GoTo ProcError
    SetUserIni mpIniSec, "BarCodeFormat", Me.cmbBarCode.BoundText
    SetUserIni mpIniSec, "ReturnAddress", Me.chkReturnAddress
    SetUserIni mpIniSec, "Output", Me.cmbOutputTo.BoundText
    SetUserIni mpIniSec, "Author", Me.txtAuthorInitials
    SetUserIni mpIniSec, "Type", Me.cmbEnvelopeTypes.BoundText
    SetUserIni mpIniSec, "FontName", Me.cmbFontName.BoundText
    SetUserIni mpIniSec, "FontSize", Me.spnFontSize.value
    SetUserIni mpIniSec, "LeftMargin", Me.spnLeftMargin.value
    SetUserIni mpIniSec, "TopMargin", Me.spnTopMargin.value
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmEnvelopes.SaveDefaults"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub UpdateObject()
    With Me.Envelope
        .EnvelopeType = Me.cmbEnvelopeTypes.BoundText
        .USPSStandards = 0 - Me.chkUSPS
        .Addresses = Me.txtAddresses
        .AddressLeft = Me.spnLeftMargin.value
        .AddressTop = Me.spnTopMargin.value
        .FontSize = Me.spnFontSize.value
        If .Definition.AllowAuthorInitials Then _
            .Author = Me.txtAuthorInitials
        If .Definition.AllowBillingNumber Then _
            .ClientMatterNumber = Me.cmbCM.value
        .DeliveryPhrase = Me.mlcDeliveryPhrase.Text
        If .Definition.SplitDPhrases Then '9.7.1 #4024
            .DeliveryPhrase2 = Me.mlcDeliveryPhrase2.Text
        End If
        .IncludeBarCode = Me.cmbBarCode.Bookmark > 0
        .IncludeReturnAddress = Me.chkReturnAddress
        .IncludeClientMatterNo = True
        .FontName = Me.cmbFontName
        .OutputTo = Me.cmbOutputTo.BoundText
        .ReturnAddress = Me.mlcReturnAddress.Text
        .BarCodePosition = Me.cmbBarCode.BoundText
    End With
End Sub
    
Private Sub mlcDeliveryPhrase_GotFocus()
    OnControlGotFocus Me.mlcDeliveryPhrase
End Sub
Private Sub mlcDeliveryPhrase2_GotFocus() '9.7.1 #4024
    OnControlGotFocus Me.mlcDeliveryPhrase2
End Sub

Private Sub mlcReturnAddress_BeforeItemAdded(xItem As String)
'set mlc = address of selected office
    On Error GoTo ProcError
    xItem = GetReturnAddress
    Me.mlcReturnAddress.Text = Empty
    Exit Sub
ProcError:
    g_oError.Show Err
End Sub

'9.7.1 - #4197
Private Function GetReturnAddress() As String
'returns either the default office address or the
'address of the office selected in mlcReturnAddress
    Dim xAddr As String
    Dim xID As String
    Dim oFormat As mpDB.COfficeAddressFormat
    
'   get id of selected office
    On Error Resume Next
    xID = Me.mlcReturnAddress.ColumnValue(1)
    On Error GoTo ProcError
    
'   create an office format to send to the .Address method below

    Set oFormat = g_oMPO.db.OfficeAddressFormats("Envelope Dialog")
            
    If xID = Empty Then
'       no office has been selected - get address of default office
        xAddr = g_oMPO.Offices.Default.Address(oFormat)
    Else
'       get address of selected office
        xAddr = g_oMPO.Offices(Me.mlcReturnAddress.ColumnValue(1)) _
            .Address(oFormat)
    End If
    
    GetReturnAddress = Replace(xAddr, Chr(11), vbCrLf)

    Exit Function
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("MacPac90.frmEnvelopes.GetReturnAddress"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Sub spnFontSize_Validate(Cancel As Boolean)
    If Not Me.spnFontSize.IsValid Then _
        Cancel = True
End Sub

Private Sub spnLeftMargin_Validate(Cancel As Boolean)
    If Not Me.spnLeftMargin.IsValid Then _
        Cancel = True
End Sub

Private Sub spnTopMargin_Validate(Cancel As Boolean)
    If Not Me.spnTopMargin.IsValid Then _
        Cancel = True
End Sub

Private Sub spnFontSize_GotFocus()
    OnControlGotFocus Me.spnFontSize
End Sub

Private Sub spnLeftMargin_GotFocus()
    OnControlGotFocus Me.spnLeftMargin
End Sub

Private Sub spnTopMargin_GotFocus()
    OnControlGotFocus Me.spnTopMargin
End Sub

Private Sub txtAddresses_Change()
    On Error GoTo ProcError
    UpdateLabelCaption Me.txtAddresses, Me.lblAddresses, "Envelope &Text"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtAddresses_LostFocus()
    Me.txtAddresses = xCleanAddress(Me.txtAddresses) '9.9.1010 #5141  !gm
End Sub
Private Sub UpdateLabelCaption(txtB As TextBox, lblL As Label, xCaptionRoot As String)
    Dim lNumEntries As Long
    
    On Error GoTo ProcError
    If txtB.Text <> "" Then
        lNumEntries = lCountChrs(txtB.Text, vbCrLf & vbCrLf) + 1
        If lNumEntries = 1 Then
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (1 address)"
        Else
            lblL = " " & xCaptionRoot & " " & vbCrLf & _
                " (" & lNumEntries & " addresses)"
        End If
    Else
        lblL = " " & xCaptionRoot
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmEnvelopes.UpdateLabelCaption"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub txtAddresses_DblClick()
    btnAddressBooks_Click
End Sub

Private Sub txtAddresses_GotFocus()
    OnControlGotFocus Me.txtAddresses
End Sub

Private Sub txtAuthorInitials_GotFocus()
    OnControlGotFocus Me.txtAuthorInitials
End Sub

'return address
Private Sub mlcReturnAddress_GotFocus()
    OnControlGotFocus Me.mlcReturnAddress, , , True
End Sub

Private Sub vsIndexTab1_Click()
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            Me.txtAddresses.SetFocus
        Case 1
            If Me.cmbCM.Enabled Then
                Me.cmbCM.SetFocus
            ElseIf Me.txtAuthorInitials.Enabled Then
                Me.txtAuthorInitials.SetFocus
            ElseIf Me.mlcDeliveryPhrase.Enabled Then
                Me.mlcDeliveryPhrase.SetFocus
            Else
                Me.cmbFontName.SetFocus
            End If
    End Select

End Sub

Private Sub GetContacts()
'   get contacts - place in text box
    Dim vTo As Variant
    Dim oContacts As Object
    Dim xSQL As String
    Static bCIActive As Boolean
    
    On Error GoTo ProcError
   
    If Not g_bShowAddressBooks Then Exit Sub
    If bCIActive Then Exit Sub
    bCIActive = True
    
    g_oMPO.GetLetterRecipients vTo, , , , , , oContacts
                                
    If vTo <> "" Then
        If Me.txtAddresses <> "" Then
            Me.txtAddresses = xTrimTrailingChrs(Me.txtAddresses, vbCrLf) & _
                                    vbCrLf & vbCrLf & vTo
        Else
            Me.txtAddresses = vTo
        End If
    End If
    
'   filter client/matter based on contacts retrieved
    If g_bCM_RunConnected And g_bCM_AllowFilter Then
        If oContacts.Count Then
            '---9.6.1
            Dim l_CIVersion As Long
            l_CIVersion = g_oMPO.Contacts.CIVersion
            
            If Me.cmbCM.AdditionalClientFilter <> "" Then
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter(oContacts)
                Else
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter2X(oContacts)
                End If
            Else
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = xGetClientMatterFilter(oContacts)
                Else
                    xSQL = xGetClientMatterFilter2X(oContacts)
                End If
            End If
            
            If xSQL <> "" Then
                Me.cmbCM.AdditionalClientFilter = xSQL
                Me.cmbCM.AdditionalMatterFilter = xSQL
                Me.cmbCM.Refresh
            End If
            '---end 9.6.1
        End If
    End If
    
    Me.SetFocus
    bCIActive = False
    Exit Sub
ProcError:
    bCIActive = False
    g_oError.Show Err
    Exit Sub
End Sub
