Attribute VB_Name = "mpTrailer"
Option Explicit

Const mpTrailerDraft As Integer = 100
Const mpTrailerFinal As Integer = 101
Const mpTrailerStrip As Integer = 102
Const mpTrailerNone As Integer = 102
'Const mpFooterThreshold As Integer = 6
Const mpNoTrailer As String = "***No Trailer - DO NOT delete***"
Const mpItemNameNotFound As Integer = 5834
Const mpCollectionMemberDoesNotExist As Integer = 5941
Const mpFileNotFound As Integer = 4149

Const mpDateFormatDef As String = "M/d/yy"
Public Const mpTimeFormatDef As String = "h:mm am/pm"
Public Const mpDateTimeFormatDef As String = "M/d/yy h:mm am/pm"

Private bRet As Boolean
Private m_oDocID As MPO.CDocumentStamp
Private m_oDoc As MPO.CDocument
Private m_xNameString As String
Public Function Insert85Trailer(oDocID As MPO.CDocumentStamp) As Boolean
                    
    Dim iTrailerType As Integer
    Dim bDraft As Boolean
    Dim bDate As Boolean
    Dim bTime As Boolean
    Dim oEnv As MPO.CEnvironment
    Dim lView As Long
    
    On Error GoTo bTrailerCreate_Error
    
    Set oEnv = New CEnvironment
    
'   set the default macpac state
    On Error Resume Next
    
    lView = Application.ActiveWindow.View   '9.7.1 #4306
    Application.ActiveWindow.View = wdNormalView
    oEnv.SetMacPacState
    
    On Error GoTo bTrailerCreate_Error
    
    Set m_oDocID = oDocID
    Set m_oDoc = oDocID.Document
    m_xNameString = m_oDocID.Definition.PrimaryTextTemplate
    
    With m_oDocID.Definition
        Select Case .Location
            Case mpDocStampLocation_None
                iTrailerType = mpTrailerNone
            Case mpDocStampLocation_Footer
                    iTrailerType = mpTrailerDraft
            Case mpDocStampLocation_EndOfDocument
                iTrailerType = mpTrailerFinal
            Case Else
                iTrailerType = mpTrailerDraft
        End Select
    End With
    
    With m_oDocID
        bRet = bTrailerWrite(iTrailerType, _
                .IncludeDate, _
                .IncludeTime, _
                .IncludeDraftText, _
                .InsertDateTimeAsField)
            

'       store current trailer type for reuse of trailer
        .Document.SetVar "85TrailerType", iTrailerType
        .Document.SetVar "NewDocStampType", .Definition.ID
        .Document.SetVar "85TrailerDate", Abs(.IncludeDate)
        .Document.SetVar "85TrailerTime", Abs(.IncludeTime)
        .Document.SetVar "85TrailerDraft", Abs(.IncludeDraftText)
        .Document.SetVar "85TrailerDateField", Abs(.InsertDateTimeAsField)
        '---9.7.1 4033 - capture on the fly checkbox items
        Dim i As Integer
        
        If g_bAllowTrailerOptions = True Then
            For i = 0 To g_xAROptionalControls.UpperBound(1)
                With g_xAROptionalControls
                    m_oDocID.Document.SetVar "85Trailer" & .value(i, 0), .value(i, 2)
                End With
            Next i
        End If
    
    End With
    oEnv.RestoreState True, False '9.7.1 - #4071
    ActiveWindow.View = lView
    Set m_oDocID = Nothing
    Set m_oDoc = Nothing
    Insert85Trailer = bRet
    Exit Function
    
bTrailerCreate_Error:
    Set m_oDocID = Nothing
    Set m_oDoc = Nothing
    Insert85Trailer = Err
End Function
Private Function bInsertOneColOnce(ByVal rngLoc As Word.Range, _
                            Optional bDate As Boolean = True, _
                            Optional bTime As Boolean = True, _
                            Optional bDraft As Boolean = False, _
                            Optional bDateAsField As Boolean = True) _
                                As Boolean

'   insert file name at
'   beginning of range
    Dim rStart As Word.Range
    Dim xFileIDClosingChar As String
    Dim xFormat As String
    Dim rngEndItem As Word.Range
    
    If bDate = False And bTime = False And bDraft = False Then
        xFileIDClosingChar = ""
    Else
        xFileIDClosingChar = vbCr
    End If
    
    rngLoc.StartOf
    
    Set rStart = rngInsertTrailerItem(rngLocation:=rngLoc, _
                                    bFileNameField:=True, _
                                    xClosingChar:=xFileIDClosingChar)
    
'    insert unformatted space after fileID
     Set rngEndItem = rStart.Characters.Last
     With rngEndItem
         If rngEndItem = vbCr Then
             .InsertBefore " "
             .Font.Reset
         Else
             .MoveEnd wdCharacter
             .MoveStart wdCharacter
             If rngEndItem <> vbTab Then
                 .StartOf
                 .InsertAfter " "
                 .Characters.Last.Font.Reset
             End If
         End If
     End With

    If bDraft Then
        bEORange rStart
        Set rStart = rngInsertTrailerItem(rngLocation:=rStart, _
                                    xText:="DRAFT", _
                                    xClosingChar:=" ")
    End If
    
    If bDate Or bTime Then
        If bDate And bTime Then
            xFormat = m_oDocID.Definition.DateFormat & " " & m_oDocID.Definition.TimeFormat
        ElseIf bDate Then
            xFormat = m_oDocID.Definition.DateFormat
        ElseIf bTime Then
            xFormat = m_oDocID.Definition.TimeFormat
        End If
        
        bEORange rStart
        Set rStart = rngInsertTrailerItem(rngLocation:=rStart, _
                                        bDate:=True, _
                                        xDateFormat:=xFormat, _
                                        bDateAsField:=bDateAsField, _
                                        xClosingChar:="")
    End If
    
'   insert unformatted space after last item
    If xFileIDClosingChar <> "" Then
        Set rngEndItem = rStart.Characters.Last
    
        With rngEndItem
            If rngEndItem = vbCr Then
                .InsertBefore " "
                .Font.Reset
            Else
                .MoveEnd wdCharacter
                .MoveStart wdCharacter
                .StartOf
                .InsertAfter " "
                .Characters.Last.Font.Reset
            End If
        End With
        
    End If
    
'   ensure that second line has no space before
'   this is an issue in pleading footers
    If xFileIDClosingChar = vbCr Then _
        rngEndItem.ParagraphFormat.SpaceBefore = 0
        
End Function

'''NOT FINISHED / NOT USED
'''Private Function iDeleteTrailerItemsNEW(ByVal rngPointer As Word.Range) As Integer
'''    Dim iRet As Integer
'''    Dim bSmartCut As Boolean
'''    Dim styTrailerItem As Style
'''    Dim bParaDeleted As Boolean
'''    Dim iTries As Integer
'''
'''    On Error GoTo iDeleteTrailerItems_Error
'''
'''    bSmartCut = Options.SmartCutPaste
'''    Options.SmartCutPaste = False
'''
'''    On Error Resume Next
'''    Set styTrailerItem = ActiveDocument.Styles("zzmpTrailerItem")
'''    On Error GoTo iDeleteTrailerItems_Error
'''
'''    If Not styTrailerItem Is Nothing Then
'''        With rngPointer.Find
'''            .ClearFormatting
'''            .Format = True
'''            .Style = "zzmpTrailerItem"
'''            .Execute
'''            While .Found
'''                iTries = iTries + 1
'''                rngPointer.Text = ""
'''
'''                'delete extra space if necessary
'''                rngPointer.MoveEnd wdCharacter, 1
'''                If rngPointer.Text = " " Then
'''                    rngPointer.Text = ""
'''                End If
'''
'''                Dim iShapeCount As Integer
'''
'''                On Error Resume Next
'''                ' Don't delete the paragraph if it's the anchor for a textbox
'''                iShapeCount = rngPointer.Paragraphs(1).Range.ShapeRange.Count
'''                On Error GoTo iDeleteTrailerItems_Error
'''                If rngPointer.Text = Chr(13) And iShapeCount = 0 Then
'''                    rngPointer.Paragraphs(1).Range.Delete
'''                End If
'''
'''                .Execute
''''               under certain circumstances, trailer deletion will
''''               get into an infinite loop (ie, when last paragraph
''''               in footer is styled with 'zzmpTrailerItem')
'''                If iTries > 50 Then GoTo ExitLoop
'''            Wend
'''ExitLoop:
'''        End With
'''
'''    End If
'''    Options.SmartCutPaste = bSmartCut
'''    Exit Function
'''
'''iDeleteTrailerItems_Error:
'''    g_oError.Raise Err.Number, "mpTrailer.iDeleteTrailerItemsNEW", Err.Description
'''End Function

Private Function iDeleteTrailerItems(ByVal rngPointer As Word.Range) As Integer
    Dim iRet As Integer
    Dim bSmartCut As Boolean
    Dim styTrailerItem As Style
    Dim rngEOTrailer As Word.Range
    Dim iTries As Integer
    
    On Error GoTo iDeleteTrailerItems_Error
    
    Set rngEOTrailer = ActiveDocument.Content
    
    bSmartCut = Options.SmartCutPaste
    Options.SmartCutPaste = False
    
    Application.ScreenUpdating = False
    
    With rngPointer
        .StartOf
        .Select
    End With
    
    On Error Resume Next
    Set styTrailerItem = ActiveDocument.Styles("zzmpTrailerItem")
    On Error GoTo iDeleteTrailerItems_Error
        
'   we use WordBasic for speed
    If Not styTrailerItem Is Nothing Then
        With WordBasic
            .ViewFieldCodes 1
            m_oDoc.bEditFindReset
            .EditFindClearFormatting
            .EditFindStyle Style:="zzmpTrailerItem"
            .EditFind Find:="", Direction:=0, Format:=1
            While .EditFindFound()
                iTries = iTries + 1
                .WW6_EditClear
                If .[Selection$]() = Chr(32) Then
                    .CharRight 1, 1
                    .WW6_EditClear
                End If
                
                Dim iShapeCount As Integer
                
                On Error Resume Next
                ' Don't delete the paragraph if it's the anchor for a textbox
                iShapeCount = Application.Selection.Paragraphs(1).Range.ShapeRange.Count
                On Error GoTo iDeleteTrailerItems_Error
                If .[Selection$]() = vbCr And iShapeCount = 0 Then
                    .WW6_EditClear
                    Set rngEOTrailer = Selection.Range
                End If
                
                .EditFind Find:="", Direction:=0, Format:=1
'               under certain circumstances, trailer deletion will
'               get into an infinite loop (ie, when last paragraph
'               in footer is styled with 'zzmpTrailerItem')
                If iTries > 50 Then GoTo ExitLoop
            Wend
            
ExitLoop:
            On Error Resume Next
            .Closepane
            On Error GoTo iDeleteTrailerItems_Error
            m_oDoc.bEditFindReset
            
            .ViewFieldCodes 0
        End With
                
    End If
    Options.SmartCutPaste = bSmartCut
    Exit Function
    
iDeleteTrailerItems_Error:
    g_oError.Raise Err.Number, "mpTrailer.iDeleteTrailerItems", Err.Description
End Function
Private Function rngInsertTrailerItem _
        (rngLocation As Word.Range, _
        Optional xText As String = "", _
        Optional bDate As Boolean, _
        Optional xDateFormat As String = mpDateFormatDef, _
        Optional bDateAsField As Boolean = True, _
        Optional bFileNameField As Boolean = True, _
        Optional xClosingChar As String = " ") _
            As Word.Range

'inserts either text, filename, or
'date at rngLocation - closing character
'is inserted if specified

    Dim lItemEnd As Long
    Dim rngStart As Word.Range
    Dim rngClosingChar As Word.Range
    Dim xFileName As String

    On Error GoTo ITI_Error
    
    With rngLocation
        If xText <> "" Then
            .Text = xText & xClosingChar
            .Style = "zzmpTrailerItem"
            
            'GLOG : 5227 : ceh
            ResetParaFont rngLocation

        ElseIf bDate Then
            .Collapse
'            .InsertDateTime xDateFormat, _
'                    bDateAsField
            .InsertDateTime xDateFormat, _
                    True
            Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = True
'---expand range either to end of field code or end of table cell
            .MoveEndUntil Cset:=vbCr & Chr(21), Count:=wdForward
            Word.ActiveDocument.ActiveWindow.View.ShowFieldCodes = False
            .Style = "zzmpTrailerItem"
            
            'GLOG : 5227 : ceh
            ResetParaFont rngLocation
            
            .InsertAfter xClosingChar
            If Not bDateAsField Then _
                .Fields(1).Unlink
        
        ElseIf bFileNameField Then
'---function for generating docs ID called here
'---note:  set switches either per options panel or
'---client preference
            xFileName = sGetDocumentID
            
'---store filename as doc var (for use with DOCs integration)
            xFileName = xFileName & xClosingChar
            
            .Text = xFileName
            .Style = "zzmpTrailerItem"
            
            'GLOG : 5227 : ceh
            ResetParaFont rngLocation

        End If
    End With
    Set rngInsertTrailerItem = rngLocation
    Exit Function

ITI_Error:
    Select Case Err
        Case mpItemNameNotFound 'can't apply trailer item style
            CopyTrailerStyle
            Resume
        Case Else
            g_oError.Raise Err.Number, "mpTrailer.rngInsertTrailerItem", Err.Description
    End Select

End Function
Private Function bTrailerWrite(iTrailerType As Integer, _
                        Optional bDate As Boolean = False, _
                        Optional bTime As Boolean = False, _
                        Optional bDraft As Boolean = False, _
                        Optional bDateAsField As Boolean = False) As Boolean

    Dim i As Integer
    Dim j As Integer
    Dim rngPrimaryHeader As Word.Range
    Dim rngFirstHeader As Word.Range
    Dim rngPrimaryFooter As Word.Range
    Dim rngFirstFooter As Word.Range
    Dim rngMain As Word.Range
    Dim rngLocation As Word.Range
    Dim rngTrailer As Word.Range
    Dim secScope As Word.Section
    Dim secCurrent As Word.Section
    Dim iNumSections As Integer
    Dim iNumCols As Integer
    Dim iRet As Integer
    Dim xMessage As String
    Dim iOldTrailerType As Integer
    Dim vCurValue As Variant
    Dim varOldTrailerType As Variable
    Dim oDoc As MPO.CDocument
    Dim lStartTime As Long
    
    DoEvents
    
    On Error GoTo bTrailerWrite_Error
    
    iNumSections = ActiveDocument.Sections.Count
        
    Set oDoc = New MPO.CDocument
    
    'mp10trailer
    If g_iMP10TrailerType = 0 Then
        On Error Resume Next
        iOldTrailerType = -1
        iOldTrailerType = Val(oDoc.GetVar("85TrailerType"))
        If iOldTrailerType < 1 Then
    '       look for 8.0 trailer
            Dim xOldTrailerType As String
            xOldTrailerType = oDoc.GetVar("iTrailerType")
            If xOldTrailerType <> "" Then
                iOldTrailerType = Val(xOldTrailerType) + 100
            Else
                iOldTrailerType = mpTrailerNone
            End If
        End If
        On Error GoTo bTrailerWrite_Error
        
    '---first delete old trailers if trailers exist
        lStartTime = mpbase2.CurrentTick
        If m_oDocID.Exists Then
            m_oDocID.Remove
        ElseIf iOldTrailerType = mpTrailerNone Then '****#4508
        '#3843 - remove trailers from entire document
            iRet = iDeleteTrailerItems(ActiveDocument.Content)
            For i = 1 To iNumSections
                Set secCurrent = ActiveDocument.Sections(i)

    '---        get document ranges
                m_oDoc.GetDocRanges rngMain, _
                                     rngPrimaryHeader, _
                                     rngPrimaryFooter, _
                                     rngFirstHeader, _
                                     rngFirstFooter, , _
                                     secCurrent
        
                iRet = iDeleteTrailerItems(rngPrimaryFooter)

                If secCurrent.PageSetup.DifferentFirstPageHeaderFooter Then _
                        iRet = iDeleteTrailerItems(rngFirstFooter)

                If secCurrent.PageSetup.OddAndEvenPagesHeaderFooter Then _
                        iRet = iDeleteTrailerItems(secCurrent.Footers(wdHeaderFooterEvenPages).Range)
            Next i
        ElseIf iOldTrailerType = mpTrailerFinal Then
            iRet = iDeleteTrailerItems(ActiveDocument.Content)
        ElseIf iOldTrailerType = mpTrailerDraft Then
            For i = 1 To iNumSections
                Set secCurrent = ActiveDocument.Sections(i)

    '---        get document ranges
                m_oDoc.GetDocRanges rngMain, _
                                     rngPrimaryHeader, _
                                     rngPrimaryFooter, _
                                     rngFirstHeader, _
                                     rngFirstFooter, , _
                                     secCurrent
                                     
                iRet = iDeleteTrailerItems(rngPrimaryFooter)

                If secCurrent.PageSetup.DifferentFirstPageHeaderFooter Then _
                        iRet = iDeleteTrailerItems(rngFirstFooter)
                        
                '#3704
                If secCurrent.PageSetup.OddAndEvenPagesHeaderFooter Then _
                        iRet = iDeleteTrailerItems(secCurrent.Footers(wdHeaderFooterEvenPages).Range)
            Next i
        End If

        On Error Resume Next
        ActiveDocument.Styles("zzmpTrailerItem").Delete
        On Error GoTo bTrailerWrite_Error

        DebugOutput "MacPac90.mpTrailer.bTrailerWrite ###  deletetrailers " & vbTab & vbTab & vbTab & mpbase2.ElapsedTime(lStartTime)
        lStartTime = mpbase2.CurrentTick
        CopyTrailerStyle
        DebugOutput "MacPac90.mpTrailer.bTrailerWrite ###  copytrailerstyle " & vbTab & vbTab & vbTab & mpbase2.ElapsedTime(lStartTime)
        
    Else
        MP10TrailerExists True
        g_iMP10TrailerType = 0
    End If

'---write new trailers
    If iTrailerType = mpTrailerFinal Then
        Set secCurrent = ActiveDocument.Sections.Last
        
'---    get document ranges
        m_oDoc.GetDocRanges rngMain, _
                             rngPrimaryHeader, _
                             rngPrimaryFooter, _
                             rngFirstHeader, _
                             rngFirstFooter, , _
                             secCurrent
    
'---    set target range for trailer item (eof, tablecol, etc.)
        Set rngTrailer = rngSetTarget(rngPrimaryFooter, _
                                      secCurrent, _
                                      wdHeaderFooterPrimary, _
                                      iTrailerType)
        If Not (rngTrailer Is Nothing) Then
            rngTrailer.Style = wdStyleNormal
        
'---        add new trailer if specified at target point
            bRet = bInsertOneColOnce(rngTrailer, _
                                     bDate, _
                                     bTime, _
                                     bDraft, _
                                     bDateAsField)
        End If
    ElseIf iTrailerType = mpTrailerDraft Then
        For i = 1 To iNumSections
            Set secCurrent = ActiveDocument.Sections(i)
    
'---        get document ranges
            bRet = m_oDoc.GetDocRanges(rngMain, _
                                 rngPrimaryHeader, _
                                 rngPrimaryFooter, _
                                 rngFirstHeader, _
                                 rngFirstFooter, , _
                                 secCurrent)
            
'---        determine if footers are currently linked and trailer markable
            If (Not secCurrent.Footers(wdHeaderFooterPrimary).LinkToPrevious) And _
                m_oDoc.IsMarkedForTrailer(secCurrent, wdHeaderFooterPrimary) Then

'---            set target range for trailer item (eof, tablecol, etc.)
                Set rngTrailer = rngSetTarget(rngPrimaryFooter, _
                                              secCurrent, _
                                              wdHeaderFooterPrimary, _
                                              iTrailerType)
                                              
    
'---            add new trailer if specified at target point
                bRet = bInsertOneColOnce(rngTrailer, bDate, bTime, bDraft, bDateAsField)
            End If
    
'---        check for first page footers
            If secCurrent.PageSetup.DifferentFirstPageHeaderFooter Then
                If (Not secCurrent.Footers(wdHeaderFooterFirstPage).LinkToPrevious) And _
                    m_oDoc.IsMarkedForTrailer(secCurrent, wdHeaderFooterFirstPage) Then
                    
'---                set target range for trailer item (eof, tablecol, etc.)
                    Set rngTrailer = rngSetTarget(rngFirstFooter, _
                                                  secCurrent, _
                                                  wdHeaderFooterFirstPage, _
                                                  iTrailerType)
                                                  
                    
                    bRet = bInsertOneColOnce(rngTrailer, _
                                             bDate, bTime, bDraft, bDateAsField)
                End If
            End If  '---second footers
            
'---        check for Odd/Even footers            '#3704
            If secCurrent.PageSetup.OddAndEvenPagesHeaderFooter Then
                If (Not secCurrent.Footers(wdHeaderFooterEvenPages).LinkToPrevious) And _
                    m_oDoc.IsMarkedForTrailer(secCurrent, wdHeaderFooterEvenPages) Then
                    
'---                set target range for trailer item (eof, tablecol, etc.)
                    Set rngTrailer = rngSetTarget(secCurrent.Footers(wdHeaderFooterEvenPages).Range, _
                                                  secCurrent, _
                                                  wdHeaderFooterEvenPages, _
                                                  iTrailerType)
                    
                    bRet = bInsertOneColOnce(rngTrailer, _
                                             bDate, bTime, bDraft, bDateAsField)
                End If
            End If  '---second footers

        Next i  'section counter
        
    End If  '---test for draft/final
    
    If (rngTrailer Is Nothing) And (iTrailerType <> mpTrailerNone) Then
        Dim xMsg As String
'       no trailer was inserted
        xMsg = "A trailer could not be inserted.  Each section in "
        xMsg = xMsg & "this document is marked for " & Chr(34)
        xMsg = xMsg & "No Trailer." & Chr(34)
        MsgBox xMsg, vbExclamation, App.Title
    End If
        
'---finish & cleanup
bTrailerWrite_Finish:
    
    On Error Resume Next
    m_oDoc.ClearBookmarks
    
    Exit Function

bTrailerWrite_Error:
    Select Case Err
        Case mpItemNameNotFound, _
             mpCollectionMemberDoesNotExist
            CopyTrailerStyle
            Resume
        Case 4605   'framed error GLOG 2290
            Resume Next
        Case Else
           g_oError.Raise Err.Number, "mpTrailer.bTrailerWrite", Err.Description
    End Select

End Function

Private Function CopyTrailerStyle()

    Dim styNormal As Word.Style
    
    On Error GoTo ProcError
    
    Application.OrganizerCopy g_oMPO.BoilerplateDirectory & "TrailerStyle.mbp", _
        WordBasic.FileName$(), "zzmpTrailerItem", wdOrganizerObjectStyles

    If Not g_bForceForceTrailerFont Then
        With ActiveDocument.Styles
            .Item("zzmpTrailerItem").Font.Name = _
                    .Item(wdStyleNormal).Font.Name
            .Item("zzmpTrailerItem").Font.ColorIndex = _
                    .Item(wdStyleNormal).Font.ColorIndex
        End With
    End If
    
    Exit Function
ProcError:
'GLOG : 5094 : CEH
    Select Case Err
        Case mpItemNameNotFound, _
             mpCollectionMemberDoesNotExist, _
             mpFileNotFound
            'Organizer save prompt
            'check for presence of TrailerStyle.mbp and prompt if it doesn't exist
            If Dir(g_oMPO.BoilerplateDirectory & "TrailerStyle.mbp") = "" Then
                MsgBox g_oMPO.BoilerplateDirectory & "TrailerStyle.mbp could not be found." & _
                       vbCr & vbCr & "Please contact your administrator.", vbExclamation, App.Title
            End If
            CreateTrailerStyle
            Resume Next
        Case Else
           g_oError.Raise Err.Number, "mpTrailer.CopyTrailerStyle", Err.Description
    End Select
    Exit Function
End Function

Private Function CreateTrailerStyle()
    Dim styNormal As Style
    
    On Error Resume Next
    
    With ActiveDocument
        Set styNormal = .Styles(wdStyleNormal)
        
        .Styles.Add "zzmpTrailerItem", wdStyleTypeCharacter
        With .Styles("zzmpTrailerItem")
            .BaseStyle = "Default Paragraph Font"
            .LanguageID = wdNoProofing
            
'           style is added with attributes of current selection-
'           so ensure no character formats are originally applied
            With .Font
                .Bold = False
                .Italic = False
                .AllCaps = False
                .Underline = False
                .SmallCaps = False
                .DoubleStrikeThrough = False
                .Emboss = False
                .Engrave = False
                .Hidden = False
                .Position = 0
                .Shadow = False
                .Subscript = False
                .Superscript = False
                .Spacing = 0
                .Scaling = 0

'               apply intended trailer font formats
                .Name = styNormal.Font.Name
                .Size = 8
                If UCase(mpbase2.GetMacPacIni("General", "Animate8xTrailer")) = "TRUE" Then
                    .Animation = wdAnimationMarchingRedAnts
                Else
                    .Animation = wdAnimationNone
                End If
            End With
        End With
    End With
End Function

Private Function rngSetTarget(rngTarget As Word.Range, _
                      secSection As Word.Section, _
                      vHFIndex As Variant, _
                      iTrailerType As Integer) As Word.Range
'   sets location for start of trailer
                            
    Dim iNumCols As Integer
    Dim iNumChars As Integer
    Dim iNumSections As Integer
    Dim bTargetSet As Boolean
    Dim xCC As String
    Dim xEnclosures As String
    Dim rngPrevChr As Word.Range
    Dim secExisting As Word.Section
    Dim i As Integer
    
    On Error GoTo rngSetTarget_Error
    
    Select Case iTrailerType
        Case mpTrailerDraft
'---        determine footer type, whether table present, etc.
            iNumCols = iFooterType(secSection.Footers(vHFIndex)) - 2
                                    
            If iNumCols < 0 Then    'no table
                
                iNumChars = Len(rngTarget.Text)
                
'---            determine if existing stuff is centered or in a frame
'---            if it is, set iNumChars to generate trailer below
                If rngTarget.ParagraphFormat. _
                    Alignment = wdAlignParagraphCenter Or _
                    rngTarget.Frames.Count > 0 Then _
                        iNumChars = g_iFooterThreshold + 1
                If iNumChars > g_iFooterThreshold Then
'---                there's existing stuff and trailer goes below it
                    Set rngSetTarget = rngTarget
                    With rngSetTarget
                        .SetRange rngTarget.End, rngTarget.End
                        .MoveStart wdCharacter, -1
                        
                        If .Characters(1) = vbCr Then
                            .MoveStart wdCharacter, 1
                        Else
                            .MoveStart wdCharacter, 1
                            .InsertParagraphAfter
                            .SetRange rngTarget.End, rngTarget.End
                        End If
                    End With
                Else
'                   if tab exists, set target to left of tab
                    With rngTarget.Find
                        .Text = "^t"
                        .Execute
                        If .Found Then
                            rngTarget.StartOf
                        End If
                    End With
                    Set rngSetTarget = rngTarget
                End If
                Exit Function
            
            Else
                Set rngSetTarget = rngTarget.Tables(1) _
                                    .Cell(1, iNumCols).Range
                If Not (rngSetTarget Is Nothing) Then
                    'insert trailer after text
                    With rngSetTarget
                        .EndOf
                        .Move wdCharacter, -1
                        If .Cells(1).Range.Characters.Count > 1 Then
                            .MoveStart wdCharacter, -1
                            If .Characters(1) = vbCr Then
                                .MoveStart wdCharacter, 1
                            Else
                                .MoveStart wdCharacter, 1
                                .InsertParagraphAfter
                                .EndOf
                            End If
                        End If
                    End With
                End If
                
            End If

        Case mpTrailerFinal
'           attempt to put before bccs
'           else insert at eoSection
            Set rngSetTarget = ActiveDocument.Content
            With rngSetTarget
                On Error GoTo rngSetTarget_Error
                
'               find bcc - first with a para
'               before, then with no para
                With .Find
                    .ClearFormatting
                    .Text = vbCr & Chr(12) & "bcc:" + vbTab
                    .Execute
                    If .Found Then
'                       set range before break
                        With rngSetTarget
                            .StartOf
                            .Style = "zzmpTrailerItem"
                        End With
                    Else
                        .ClearFormatting
                        .Text = Chr(12) & "bcc:" + vbTab
                        .Execute
                        If .Found Then
'                           set range before break
                            With rngSetTarget
                                .StartOf
                                .InsertParagraphAfter
                                .Style = "zzmpTrailerItem"
                                .StartOf
                            End With
                        End If
                    End If
                End With
                    
                If .Find.Found Then
'                   ensure that there's a para
'                   mark before trailer
                    .MoveStart wdCharacter, -1
                    
                    If .Characters(1) = vbCr Then
                        .MoveStart wdCharacter, 1
                    Else
                        .MoveStart wdCharacter, 1
                        .InsertParagraphAfter
                        .SetRange .End, .End
                    End If
                    bTargetSet = True
                End If
                
                If Not bTargetSet Then
'                  insert at eoSection

'                   point to last section that is
'                   capable of being marked for a trailer
                    iNumSections = ActiveDocument.Sections.Count
                    Set rngSetTarget = Nothing
                    
'                   cycle through sections
                    For i = iNumSections To 1 Step -1
                        Set secExisting = ActiveDocument.Sections(i)
                        If m_oDoc.IsMarkedForTrailer(secExisting, _
                                        wdHeaderFooterPrimary) Then
                            Set rngSetTarget = secExisting.Range
                            With rngSetTarget
                                If .Characters.Last = Chr(12) Then _
                                    .MoveEnd wdCharacter, -1
                            End With
                            Exit For
                        End If
                    Next i
                    
'                   if a trailerable section was found, point to eoSection
                    If Not (rngSetTarget Is Nothing) Then
                        With rngSetTarget
                            .EndOf
                            .InsertParagraphAfter
                            .EndOf
                        End With
                    End If
                
                End If
            End With
        Case Else
    
    End Select
    Exit Function
    
rngSetTarget_Error:
    Select Case Err
        Case mpItemNameNotFound 'can't apply trailer item style
            CopyTrailerStyle
            Resume
        Case Else
            g_oError.Raise Err.Number, "mpTrailer.rngSetTarget", Err.Description
    End Select

End Function

Public Function sGetDocumentID(Optional xTextTemplate As String) As String
'returns a dms profile text string - fills tokens
'provided in xTextScheme or xAltTextScheme
    Dim xTemp As String
    Dim oDMS As CDMS
    Dim iPos As Integer
    Dim xToken As String
    Dim xVal As String
    Dim bHasPhrases As Boolean
    Dim xFormat As String
    Dim l As Long
    Dim oTemplate As MPO.CTemplate
    Dim lDefaultDocID As Long
    
    On Error GoTo ProcError
'   get desired dms if the the active
'   document is a profiled document - this
'   indicates that the desired dms is running
'    xClientID = g_oMPO.DocumentManager.Profile.DocNumber
    
    'used by call from Application.IDPropChanged
    If m_oDocID Is Nothing Then
        Set oTemplate = g_oMPO.Templates(NormalTemplate.Name)
        If oTemplate.DefaultDocIDStamp = 0 Then
            lDefaultDocID = g_lDefaultDocID
        Else
            lDefaultDocID = oTemplate.DefaultDocIDStamp
        End If
        Set m_oDocID = g_oMPO.NewDocumentIDStamp(lDefaultDocID)
    End If
    
    If g_oMPO.DocumentManager.Profile.DocNumber <> Empty Then
        Set oDMS = g_oMPO.DocumentManager
        xTemp = IIf(xTextTemplate <> "", xTextTemplate, m_oDocID.Definition.PrimaryTextTemplate)
    Else
        Set oDMS = New CDMS
        oDMS.TypeID = mpDMS_Windows
        
'       use alt text scheme - alt text schemes
'       are reserved for non-managed documents
        xTemp = m_oDocID.Definition.PrimaryAltTextTemplate
    End If
    
'---9.7.1 4033 r927- cycle through the token array for custom fields.  remove token
'---if stored checkbox value is false
    
    Dim i As Integer
    If g_bAllowTrailerOptions = True And Not g_xAROptionalControls Is Nothing Then
        For i = 0 To g_xAROptionalControls.UpperBound(1)
            With g_xAROptionalControls
                If .value(i, 2) = vbUnchecked Then
                    '---remove the token phrase from the xTemp string
                    If InStr(xTemp, "|") Then
                        If .value(i, 3) = "%cm" Then    'client/matter as one option
                            xTemp = DeleteTokenPhrase(xTemp, "%c")
                            xTemp = DeleteTokenPhrase(xTemp, "%m")
                        Else
                            xTemp = DeleteTokenPhrase(xTemp, .value(i, 3))
                        End If
                    Else
                        If .value(i, 3) = "%cm" Then    'client/matter as one option
                            xTemp = xSubstitute(xTemp, "%c", "")
                            xTemp = xSubstitute(xTemp, "%m", "")
                        Else
                            xTemp = xSubstitute(xTemp, .value(i, 3), "")
                        End If
                    End If
                    
                End If
            End With
        Next
        
        'reset text template properties
        With m_oDocID.Definition
            .PrimaryTextTemplate = xTemp
            .Page1TextTemplate = xTemp
        End With
        
    End If
    
'   this boolean is here for compatibility with
'   beta versions of MP90, which didn't support
'   phrased profile strings - below, we won't
'   attempt to delete any phrases if the profile
'   tokens are empty
    bHasPhrases = InStr(xTemp, "|")
    
    If xTemp = Empty Then
        Exit Function
    End If
        
    On Error Resume Next
'   replace non-printing character codes
    xTemp = xSubstitute(xTemp, "<9>", vbTab)
    xTemp = xSubstitute(xTemp, "<11>", Chr(11))
    xTemp = xSubstitute(xTemp, "<13>", vbCr)
    
'   these character substitutions remain for
'   compatibility with early MP90 text scheme strings
    xTemp = xSubstitute(xTemp, "%$", vbTab)
    xTemp = xSubstitute(xTemp, "%#", Chr(11))
    xTemp = xSubstitute(xTemp, "%^", vbCr)
    
'   loop through tokens of the profile template,
'   substituting the appropriate values for each
'   token found
    iPos = InStr(1, xTemp, "%")
    
    With oDMS.Profile
        While iPos
'           get token
            On Error Resume Next
            xToken = Mid(xTemp, iPos, 2)
            On Error GoTo ProcError
            
            
'           get value to substitute
            Select Case xToken
                Case "%n"   'Document Number
                    xVal = .DocNumber
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%l"   'Document Library
                    xVal = .Library
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%a"   'Document Name
                    xVal = .DocName
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%v"   'Document Version
                    xVal = .Version
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%c"   'Client ID
                    xVal = .ClientID
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%m"   'Matter ID
                    xVal = .MatterID
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%f"   'File name
                    xVal = .FileName
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%p"   'File path
                    xVal = .Path
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%y"   'typist initials
                    xVal = .TypistID
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%u"   'User initials
                    xVal = .AuthorID
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%e"   'Creation Date
                    xFormat = m_oDocID.Definition.DateFormat
                    xVal = Format(.CreationDate, xFormat)
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%r"   'Revision Date
                    xFormat = m_oDocID.Definition.DateFormat
                    xVal = Format(.RevisionDate, xFormat)
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
               Case "%t"   ' Document Type ID
                    xVal = .DocTypeID
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%j"   ' Doc Type Description
                    xVal = .DocTypeDescription
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%h"   ' Author Full Name
                    xVal = .AuthorName
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%b"   ' Abstract
                    xVal = .Abstract
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%x"   ' Typist Name
                    xVal = .TypistName
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%i"     ' Client Description
                    xVal = .ClientName
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%z"    ' Matter Description
                    xVal = .MatterName
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%d"
'                   replace variable with another var - this
'                   will prevent the code from deleting the
'                   variable, while allowing us to replace
'                   the var in FillBookmarks
                    xVal = "^d"
                Case "%1"
                    xVal = .Custom1
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%2"
                    xVal = .Custom2
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case "%3"
                    xVal = .Custom3
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
                Case Else
                    If InStr(xVal, "%") Then
                        xVal = xSubstitute(xVal, "%", "~~")
                    End If
            End Select
            
'           if value is not empty, substitute
'           else remove token phrase - also
'           run substitute if there are no pipes
'           to define phrases
            If Len(xVal) Or (Not bHasPhrases) Then
                xTemp = xSubstitute(xTemp, xToken, xVal)
            Else
                xTemp = DeleteTokenPhrase(xTemp, xToken)
            End If
            
'           look for next token
            iPos = InStr(xTemp, "%")
        Wend
        '---replace placeholders with percents
        If InStr(xTemp, "~~") Then
            xTemp = xSubstitute(xTemp, "~~", "%")
        End If
    End With
    Err.Clear
    xTemp = mpbase2.xTrimTrailingChrs(xTemp, " ", True)
    sGetDocumentID = mpbase2.xSubstitute(xTemp, "|", "")
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "mpTrailer.sGetDocumentID"
    Exit Function
End Function

Public Function DeleteTokenPhrase(ByVal xText As String, ByVal xToken As String) As String
'deletes from xText the phrase containing the supplied token
    Dim iBOPhrase As Integer
    Dim iEOPhrase As Integer
    Dim xPostPhrase As String
    Dim xPrePhrase As String
    Dim i As Integer
    Dim iPos As Integer
    
'   get token start position
    iPos = InStr(xText, xToken)
    
    If iPos = 0 Then
        '---4033 r927
        DeleteTokenPhrase = xText
        Exit Function
    End If
    
'   get end of phrase
    On Error GoTo ProcError
    iEOPhrase = InStr(iPos, xText, "|")
    If iEOPhrase Then
'       get text after phrase
        xPostPhrase = Mid(xText, iEOPhrase + 1)
    End If
        
'   get start of phrase
    xPrePhrase = Left(xText, iPos - 1)
    If xPrePhrase <> Empty Then
'       start of phrase is character after the
'       last pipe in the prephase string
        i = InStr(1, xPrePhrase, "|")
        If i = 0 Then   'grab all chars to the left
            iBOPhrase = Len(xPrePhrase) + 1
        Else
            While i
                iBOPhrase = i
                i = InStr(i + 1, xPrePhrase, "|")
            Wend
        End If
    Else
        iBOPhrase = 1
    End If
    
'   set prephase as all text to the left of the phrase
    xPrePhrase = Left(xText, iBOPhrase - 1)
    
    If (xPrePhrase <> Empty) And (xPostPhrase <> Empty) Then
'       add separator between pre and post phrases
        xText = xPrePhrase & "|" & xPostPhrase
    Else
'       at least one of the strings will be empty
        xText = xPrePhrase & xPostPhrase
    End If
    
'   return phrase
    DeleteTokenPhrase = xText
    
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "mpTrailer.DeleteTokenPhrase"
    Exit Function
End Function
Public Function Trailer85Exists() As Boolean
    Dim oVar As Word.Variable
    Dim xTest As String
    Err.Clear
    On Error Resume Next
    Set oVar = Word.ActiveDocument.Variables("85TrailerType")
    xTest = oVar.Name
    Trailer85Exists = (Err = 0)
End Function
Private Function bEORange(rngRange As Word.Range) As Boolean
'   redefines rngRange to be end of rngRange
    On Error Resume Next
    With rngRange
        .SetRange .End, .End
    End With
End Function

Function iFooterType(ftrFooter As HeaderFooter) As Integer
'returns 0 if not a MacPac
'footer (zzmpFooter style) -
'else, returns an integer
'representing # of columns

    Dim tFtrTable As Word.Table
    Dim xStylePrefix As String
    On Error Resume Next
'   cycle through all tables in footer
    For Each tFtrTable In ftrFooter.Range.Tables
        With tFtrTable.Range
            xStylePrefix = Left(.Style, 10)
            If InStr(xStylePrefix, "Footer") Then
                iFooterType = .Columns.Count
                Exit Function
            Else
                iFooterType = .Columns.Count
            End If
        End With
    Next tFtrTable
End Function

Public Sub SaveDocIDVars(oDocID As MPO.CDocumentStamp)
    Dim oDoc As MPO.CDocument
    
    Set oDoc = New MPO.CDocument
    
    oDoc.SetVar "MPDocID", sGetDocumentID(oDocID.Definition.PrimaryTextTemplate)
    oDoc.SetVar "MPDocIDTemplate", oDocID.Definition.PrimaryTextTemplate
    oDoc.SetVar "MPDocIDTemplateDefault", xGetDefaultTextTemplate()
    
End Sub

Public Sub CreateOptionsArray(oStamp As CDocumentStampDef)
    '---9.7.1 4033r927 - Creates an array of options/values
    'based on configuration of Document stamp OptionalControls strings
    Dim xOptionalControls As String
    Dim j As Integer
    Dim k As Integer
    Dim i As Integer
    Dim m As Integer
    Dim oDMS As CDMS
    
    On Error GoTo 0
    If oStamp Is Nothing Then
        Set oStamp = m_oDocID.Definition
    End If
    
    On Error Resume Next
    
    If g_oMPO.DocumentManager.Profile.DocNumber <> Empty Then
        Set oDMS = g_oMPO.DocumentManager
        xOptionalControls = oStamp.OptionalControls
    Else
        Set oDMS = New CDMS
        oDMS.TypeID = mpDMS_Windows
        
        xOptionalControls = ""
    End If
    
    If g_xAROptionalControls Is Nothing Then
        Set g_xAROptionalControls = g_oMPO.NewXArray
    End If
    
    '---this array consists of property, caption, value set by interface and token
    
    g_xAROptionalControls.ReDim 0, (mpbase2.lCountChrs(xOptionalControls, "%")) - 1, 0, 3
    
    '---set up g_xAROptionalControlsray - cycle through all the supported tokens
    
    If InStr(xOptionalControls, "%l") Then
        g_xAROptionalControls.value(j, 0) = "Library"
        g_xAROptionalControls.value(j, 1) = "&Library"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%l"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%n") Then
        g_xAROptionalControls.value(j, 0) = "Number"
        g_xAROptionalControls.value(j, 1) = "&Number"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%n"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%v") Then
        g_xAROptionalControls.value(j, 0) = "Version"
        g_xAROptionalControls.value(j, 1) = "&Version"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%v"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%cm") Then     'client/matter as one control
        g_xAROptionalControls.value(j, 0) = "ClientMatter"
        g_xAROptionalControls.value(j, 1) = "&Client/Matter"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%cm"
        j = j + 1
    Else
        If InStr(xOptionalControls, "%c") Then
            g_xAROptionalControls.value(j, 0) = "Client"
            g_xAROptionalControls.value(j, 1) = "&Client"
            g_xAROptionalControls.value(j, 2) = vbChecked
            g_xAROptionalControls.value(j, 3) = "%c"
            j = j + 1
        End If
    
        If InStr(xOptionalControls, "%m") Then
            g_xAROptionalControls.value(j, 0) = "Matter"
            g_xAROptionalControls.value(j, 1) = "&Matter"
            g_xAROptionalControls.value(j, 2) = vbChecked
            g_xAROptionalControls.value(j, 3) = "%m"
            j = j + 1
        End If
    End If
    
    If InStr(xOptionalControls, "%a") Then
        g_xAROptionalControls.value(j, 0) = "Title"
        g_xAROptionalControls.value(j, 1) = "Doc &Title"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%a"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%f") Then
        g_xAROptionalControls.value(j, 0) = "FileNo"
        g_xAROptionalControls.value(j, 1) = "F&ile No."
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%f"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%p") Then
        g_xAROptionalControls.value(j, 0) = "Path"
        g_xAROptionalControls.value(j, 1) = "&Path"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%p"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%y") Then
        g_xAROptionalControls.value(j, 0) = "TypistID"
        g_xAROptionalControls.value(j, 1) = "T&ypist ID"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%y"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%u") Then
        g_xAROptionalControls.value(j, 0) = "Author"
        g_xAROptionalControls.value(j, 1) = "&Author"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%u"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%e") Then
        g_xAROptionalControls.value(j, 0) = "CreateDate"
        g_xAROptionalControls.value(j, 1) = "C&reate Date"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%e"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%r") Then
        g_xAROptionalControls.value(j, 0) = "RevisionDate"
        g_xAROptionalControls.value(j, 1) = "&Revision Date"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%r"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%t") Then
        g_xAROptionalControls.value(j, 0) = "DocType"
        g_xAROptionalControls.value(j, 1) = "D&oc Type"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%t"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%h") Then
        g_xAROptionalControls.value(j, 0) = "AuthorName"
        g_xAROptionalControls.value(j, 1) = "A&uthor Name"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%h"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%x") Then
        g_xAROptionalControls.value(j, 0) = "TypistName"
        g_xAROptionalControls.value(j, 1) = "Ty&pist Name"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%x"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%i") Then
        g_xAROptionalControls.value(j, 0) = "ClientName"
        g_xAROptionalControls.value(j, 1) = "C&lient Name"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%i"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%z") Then
        g_xAROptionalControls.value(j, 0) = "MatterName"
        g_xAROptionalControls.value(j, 1) = "Ma&tter Name"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%z"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%j") Then
        g_xAROptionalControls.value(j, 0) = "Doc_TypeDesc"
        g_xAROptionalControls.value(j, 1) = "Doc T&ype Desc."
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%j"
        j = j + 1
    End If
    
    If InStr(xOptionalControls, "%b") Then
        g_xAROptionalControls.value(j, 0) = "Abstract"
        g_xAROptionalControls.value(j, 1) = "A&bstract"
        g_xAROptionalControls.value(j, 2) = vbChecked
        g_xAROptionalControls.value(j, 3) = "%b"
        j = j + 1
    End If
    
    
End Sub

Public Function xGetDefaultTextTemplate() As String
    Dim oDocID As MPO.CDocumentStamp
    Dim oTemplate As MPO.CTemplate
    
    On Error GoTo ProcError
    
'   get active template
    Set oTemplate = g_oMPO.Templates( _
        Word.ActiveDocument.AttachedTemplate)
        
    If (oTemplate Is Nothing) Then
'       active template is not a MacPac template, so use normal.dot-
'       this assumes that normal.dot is a MacPac template
        Set oTemplate = g_oMPO.Templates(NormalTemplate.Name)
'       activetemplate doesn't have a default stamp
    ElseIf (oTemplate.DefaultDocIDStamp = 0) Then
        Set oTemplate = g_oMPO.Templates(NormalTemplate.Name)
    End If

    On Error Resume Next
'       get default document stamp for template
    Set oDocID = g_oMPO.NewDocumentIDStamp(oTemplate.DefaultDocIDStamp)
    On Error GoTo ProcError
        
    If oDocID Is Nothing Then
'           get default document stamp
        Set oDocID = g_oMPO.NewDocumentIDStamp(g_lDefaultDocID)
    End If
    
    xGetDefaultTextTemplate = oDocID.Definition.PrimaryTextTemplate
    
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.Application.sGetDefaultTextTemplate"
    Exit Function
End Function

'mp10trailer
Public Function MP10TrailerExists(Optional bDelete As Boolean = False) As Boolean

    Dim wrdSection As Word.Section
    Dim vType As Variant
    Dim gcwfFields As Word.Field
    Dim x As Integer
    Dim bExists As Boolean
    Dim oStyle As Word.Style
    
    On Error GoTo ProcError
    'reset MP10 Trailer flag
    g_iMP10TrailerType = 0

    On Error Resume Next
    Set oStyle = ActiveDocument.Styles("MacPac Trailer")
    On Error GoTo ProcError

    If oStyle Is Nothing Then
        GoTo skip
    End If
    
    DoEvents

'   Search for and delete mp10 trailers in Headers/Footers
    For Each wrdSection In Word.ActiveDocument.Sections
        For Each vType In Array(Word.wdHeaderFooterPrimary, _
            Word.wdHeaderFooterEvenPages, Word.wdHeaderFooterFirstPage)
            If wrdSection.Footers(vType).Exists Then
                MP10TrailerInRange wrdSection.Footers(vType).Range, True, bDelete
            End If
         Next ' associated to each footer type
    Next ' associated to total number of sections
    If Selection.Information(wdInHeaderFooter) = True Then
        ActiveWindow.ActivePane.Close
    End If

    DoEvents

    If bDelete Or (g_iMP10TrailerType = 0) Then
        '  Search for and delete mp10 trailers in main document
        For Each wrdSection In Word.ActiveDocument.Sections
            MP10TrailerInRange ActiveDocument.Range, False, bDelete
        Next ' associated to total number of sections
    End If
    If (Not (oStyle Is Nothing)) And bDelete Then
        ActiveDocument.Styles("MacPac Trailer").Delete
    End If
    
skip:
    MP10TrailerExists = (g_iMP10TrailerType <> 0)
    DoEvents
    Exit Function
ProcError:
    MP10TrailerExists = bExists
    RaiseError "MacPac90.mpTrailer.MP10TrailerExists"
    Exit Function
End Function

'mp10trailer
Private Sub MP10TrailerInRange(oRange As Word.Range, _
                                bIsHeaderFooter As Boolean, _
                                bDelete As Boolean)
'deletes all shapes in the category that
'are members of the supplied collection
    Dim oRng As Word.Range
    Dim oShape As Word.Shape
    Dim i As Integer
    Dim iCount As Integer
    Dim xStyleName As String
    
    On Error GoTo ProcError
    
    DoEvents
    
    For Each oShape In oRange.ShapeRange
        If oShape.Type = 17 Then    'textbox
           On Error Resume Next
           xStyleName = oShape.TextFrame.TextRange.Style
           On Error GoTo ProcError
           If xStyleName = "MacPac Trailer" Then
               If bIsHeaderFooter Then
                   g_iMP10TrailerType = mpTrailerDraft
               Else
                   g_iMP10TrailerType = mpTrailerFinal
               End If
               'If Anchor paragraph is empty delete it as well
               If bDelete Then
                  With oShape.Anchor.Paragraphs(1).Range
		   	'GLOG - 5685 - ceh
                       If (.Text = vbCr) And (.Bookmarks.Count = 0) Then
                           oShape.Delete
                           .Delete
                       Else
                           oShape.Delete
                       End If
                   End With
               End If
           End If
       End If
    Next oShape
    DoEvents
    Exit Sub
ProcError:
    RaiseError "MacPac90.mpTrailer.MP10TrailerInRange"
    Exit Sub
End Sub

Private Sub ResetParaFont(rngRange As Word.Range)
    Dim oTable As Object
    
    On Error GoTo ProcError
    
    With rngRange
        With .ParagraphFormat
            If Not rngRange.Information(wdWithInTable) Then
                .LeftIndent = 0
            Else
                Set oTable = rngRange.Tables(1)
                If oTable.LeftPadding <> 0 Then
                    .LeftIndent = 0
                End If
            End If
            .FirstLineIndent = 0
            .Alignment = wdAlignParagraphLeft
            'GLOG : 5027 : ceh - Set line spacing format 'before' setting line spacing rule
            .LineSpacing = 10
            .LineSpacingRule = wdLineSpaceExactly
        End With
        
        'GLOG : 5227 : ceh
        With .Font
            .Bold = False
            .Italic = False
            .AllCaps = False
            .Underline = False
            .SmallCaps = False
            .DoubleStrikeThrough = False
            .Emboss = False
            .Engrave = False
            .Hidden = False
            .Position = 0
            .Shadow = False
            .Subscript = False
            .Superscript = False
            .Spacing = 0
        End With
    End With
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, "mpTrailer.ResetParaFont", Err.Description
    Exit Sub
End Sub

