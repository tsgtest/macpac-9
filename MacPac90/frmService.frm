VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmService 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   6672
   ClientLeft      =   1932
   ClientTop       =   636
   ClientWidth     =   5484
   Icon            =   "frmService.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6672
   ScaleWidth      =   5484
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnServiceList 
      Caption         =   "Service &List"
      Height          =   400
      Left            =   1770
      TabIndex        =   47
      Top             =   6216
      Width           =   1185
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   49
      Top             =   6216
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4065
      TabIndex        =   48
      Top             =   6216
      Width           =   650
   End
   Begin C1SizerLibCtl.C1Tab tabPages 
      Height          =   6735
      Left            =   -30
      TabIndex        =   50
      TabStop         =   0   'False
      Top             =   -105
      Width           =   5520
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|Si&gner"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame fraPage2 
         BorderStyle     =   0  'None
         Height          =   6300
         Left            =   6024
         TabIndex        =   52
         Top             =   12
         Width           =   5508
         Begin VB.TextBox txtSignerCounty 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1755
            ScrollBars      =   2  'Vertical
            TabIndex        =   46
            Top             =   2415
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtSignerState 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1755
            ScrollBars      =   2  'Vertical
            TabIndex        =   44
            Top             =   1995
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtSignerCity 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1755
            ScrollBars      =   2  'Vertical
            TabIndex        =   42
            Top             =   1575
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtSignerCompany 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1755
            ScrollBars      =   2  'Vertical
            TabIndex        =   38
            Top             =   255
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtSignerAddress 
            Appearance      =   0  'Flat
            Height          =   765
            Left            =   1755
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   40
            Top             =   675
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.Label lblSignerCounty 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signer Co&unty:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   120
            TabIndex        =   45
            Top             =   2460
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblSignerState 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signer &State:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   120
            TabIndex        =   43
            Top             =   2040
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblSignerCity 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signer Cit&y:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   120
            TabIndex        =   41
            Top             =   1605
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblSignerAddress 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signer &Address:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   105
            TabIndex        =   39
            Top             =   705
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblSignerCompany 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signer &Company:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   30
            TabIndex        =   37
            Top             =   285
            Visible         =   0   'False
            Width           =   1530
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000005&
            X1              =   -132
            X2              =   5418
            Y1              =   6252
            Y2              =   6252
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00C0C0C0&
            FillColor       =   &H00800080&
            Height          =   6255
            Left            =   -30
            Top             =   15
            Width           =   1725
         End
      End
      Begin VB.Frame fraPage1 
         BorderStyle     =   0  'None
         Height          =   6300
         Left            =   12
         TabIndex        =   51
         Top             =   12
         Width           =   5508
         Begin VB.CheckBox chkElectronicSignature 
            Appearance      =   0  'Flat
            Caption         =   "&Use Electronic Signature Format"
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   1800
            TabIndex        =   36
            Top             =   6000
            Width           =   2745
         End
         Begin VB.TextBox txtCustom4 
            Appearance      =   0  'Flat
            Height          =   330
            Left            =   1770
            ScrollBars      =   2  'Vertical
            TabIndex        =   33
            Top             =   4965
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtCustom3 
            Appearance      =   0  'Flat
            Height          =   330
            Left            =   1770
            ScrollBars      =   2  'Vertical
            TabIndex        =   31
            Top             =   4875
            Visible         =   0   'False
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbAuthor 
            Height          =   555
            Left            =   1770
            OleObjectBlob   =   "frmService.frx":058A
            TabIndex        =   1
            Top             =   240
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbOffice 
            Height          =   600
            Left            =   1755
            OleObjectBlob   =   "frmService.frx":44CF
            TabIndex        =   3
            Top             =   690
            Visible         =   0   'False
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbServiceTitle 
            Height          =   900
            Left            =   1755
            OleObjectBlob   =   "frmService.frx":66E0
            TabIndex        =   9
            Top             =   1005
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbServiceDate 
            Height          =   375
            Left            =   1755
            OleObjectBlob   =   "frmService.frx":8BFF
            TabIndex        =   13
            Top             =   2355
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbExecuteDate 
            Height          =   375
            Left            =   1755
            OleObjectBlob   =   "frmService.frx":AE21
            TabIndex        =   15
            Top             =   2685
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbCounty 
            Height          =   345
            Left            =   1755
            OleObjectBlob   =   "frmService.frx":D043
            TabIndex        =   17
            Top             =   3015
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbPartyTitle 
            Height          =   450
            Left            =   1755
            OleObjectBlob   =   "frmService.frx":F01C
            TabIndex        =   19
            Top             =   4140
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbGender 
            Height          =   375
            Left            =   1755
            OleObjectBlob   =   "frmService.frx":1123D
            TabIndex        =   25
            Top             =   4425
            Width           =   3615
         End
         Begin VB.TextBox txtCaseNumber 
            Appearance      =   0  'Flat
            Height          =   330
            Left            =   1770
            ScrollBars      =   2  'Vertical
            TabIndex        =   7
            Top             =   5520
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtCourtTitle 
            Appearance      =   0  'Flat
            Height          =   720
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            Top             =   5250
            Width           =   3615
         End
         Begin VB.CheckBox chkNoServiceTitle 
            Appearance      =   0  'Flat
            Caption         =   "Do No&t Include Service Title In Document"
            ForeColor       =   &H80000008&
            Height          =   405
            Left            =   1800
            TabIndex        =   35
            Top             =   5835
            Width           =   3510
         End
         Begin VB.TextBox txtCustom2 
            Appearance      =   0  'Flat
            Height          =   330
            Left            =   1770
            ScrollBars      =   2  'Vertical
            TabIndex        =   29
            Top             =   5160
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtCustom1 
            Appearance      =   0  'Flat
            Height          =   330
            Left            =   1770
            ScrollBars      =   2  'Vertical
            TabIndex        =   27
            Top             =   4755
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.CheckBox chkManualEntry 
            Appearance      =   0  'Flat
            Caption         =   "&Enter Signer Info Manually"
            ForeColor       =   &H80000008&
            Height          =   405
            Left            =   1800
            TabIndex        =   34
            Top             =   5505
            Width           =   2235
         End
         Begin VB.TextBox txtDocumentTitle 
            Appearance      =   0  'Flat
            Height          =   930
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   11
            Top             =   1380
            Width           =   3615
         End
         Begin VB.TextBox txtFooterText 
            Appearance      =   0  'Flat
            Height          =   480
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   23
            Top             =   3900
            Width           =   3615
         End
         Begin VB.TextBox txtPartyDescription 
            Appearance      =   0  'Flat
            Height          =   480
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   21
            Top             =   3375
            Width           =   3615
         End
         Begin VB.TextBox txtSignerName 
            Appearance      =   0  'Flat
            Height          =   330
            Left            =   1770
            ScrollBars      =   2  'Vertical
            TabIndex        =   53
            Top             =   240
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.Label lblCustom4 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom #4:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            TabIndex        =   32
            Top             =   5370
            Width           =   1455
         End
         Begin VB.Label lblCustom3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom #3:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   120
            TabIndex        =   30
            Top             =   4980
            Width           =   1455
         End
         Begin VB.Label lblPartyTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Part&y Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   75
            TabIndex        =   18
            Top             =   4155
            Width           =   1455
         End
         Begin VB.Label lblCaseNumber 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Case Nu&mber:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   75
            TabIndex        =   6
            Top             =   5565
            Width           =   1455
         End
         Begin VB.Label lblCourtTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Co&urt Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   465
            TabIndex        =   4
            Top             =   5250
            Width           =   1080
         End
         Begin VB.Label lblServiceTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Se&rvice Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   120
            TabIndex        =   8
            Top             =   1035
            Width           =   1455
         End
         Begin VB.Label lblOffice 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Office:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   105
            TabIndex        =   2
            Top             =   720
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCustom2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom #2:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            TabIndex        =   28
            Top             =   5205
            Width           =   1455
         End
         Begin VB.Label lblCustom1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom #1:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            TabIndex        =   26
            Top             =   4785
            Width           =   1455
         End
         Begin VB.Label lblExecuteDate 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Date &Executed:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   120
            TabIndex        =   14
            Top             =   2745
            Width           =   1455
         End
         Begin VB.Label lblDocumentTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Document Title(s):"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   135
            TabIndex        =   10
            Top             =   1380
            Width           =   1455
         End
         Begin VB.Label lblFooterText 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Footer Te&xt:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   105
            TabIndex        =   22
            Top             =   3900
            Width           =   1455
         End
         Begin VB.Label lblGender 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Gende&r:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   105
            TabIndex        =   24
            Top             =   4485
            Width           =   1455
         End
         Begin VB.Label lblCounty 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Service &County:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   120
            TabIndex        =   16
            Top             =   3075
            Width           =   1455
         End
         Begin VB.Label lblServiceDate 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Date &Served:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   120
            TabIndex        =   12
            Top             =   2430
            Width           =   1455
         End
         Begin VB.Label lblPartyDescription 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party Descri&ption:"
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   135
            TabIndex        =   20
            Top             =   3375
            Width           =   1455
         End
         Begin VB.Label lblAuthor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   105
            TabIndex        =   0
            Top             =   285
            Width           =   1455
         End
         Begin VB.Line Line8 
            BorderColor     =   &H80000005&
            X1              =   -48
            X2              =   5502
            Y1              =   6252
            Y2              =   6252
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00C0C0C0&
            FillColor       =   &H00800080&
            Height          =   6445
            Left            =   -120
            Top             =   -180
            Width           =   1800
         End
      End
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmService"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oService As MPO.CService
Private m_bCancelled As Boolean
Private m_bInit As Boolean
Private m_oPersonsForm As MPO.CPersonsForm
Private m_oContacts As MPO.CContacts
Private m_oMenu As MacPac90.CAuthorMenu
Private m_bAuthorIsDirty As Boolean
Private m_bItemChanged As Boolean
Private m_lFooterTextMaxLength  As Long

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Set Service(oNew As MPO.CService)
    Set m_oService = oNew
End Property
 
Public Property Get Service() As MPO.CService
    Set Service = m_oService
End Property
Public Property Get FooterTextMaxLength() As Long
    FooterTextMaxLength = m_lFooterTextMaxLength
End Property
Public Property Let FooterTextMaxLength(lNew As Long)
    m_lFooterTextMaxLength = lNew
End Property

Public Sub SetManualEntry(ByVal bOn As Boolean)
    On Error GoTo ProcError
    Me.cmbAuthor.Visible = Not bOn
    Me.txtSignerName.Visible = bOn
    With Me.Service.Definition
        Me.tabPages.TabEnabled(1) = bOn And (.AllowManualAddress Or _
                                             .AllowManualCompany Or _
                                             .AllowManualCity Or _
                                             .AllowManualCounty Or _
                                             .AllowManualState)
    End With
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmService.SetManualEntry"
    Exit Sub
End Sub

Public Sub SetSignerTabControls()
'sets visibility - if signer tab is not visible,
'controls won't be visible regardless of these settings
    On Error GoTo ProcError
    With Me.Service.Definition
        Me.txtSignerAddress.Visible = .AllowManualAddress
        Me.lblSignerAddress.Visible = .AllowManualAddress
        Me.txtSignerCompany.Visible = .AllowManualCompany
        Me.lblSignerCompany.Visible = .AllowManualCompany
        Me.txtSignerCity.Visible = .AllowManualCity
        Me.lblSignerCity.Visible = .AllowManualCity
        Me.txtSignerCounty.Visible = .AllowManualCounty
        Me.lblSignerCounty.Visible = .AllowManualCounty
        Me.txtSignerState.Visible = .AllowManualState
        Me.lblSignerState.Visible = .AllowManualState
    End With
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmService.SetSignerTabControls"
    Exit Sub
End Sub

Public Sub PositionControls()
'adjust dialog for manual entry / no manual entry
    Const iSpaceBefore As Integer = 165
    Dim oCtl As Control
    Dim oLastVisible As Control
    Dim arrCtls() As String
    Dim i As Integer
    
    On Error GoTo ProcError

    ReDim arrCtls(Me.Controls.Count)
    
'   place each control in tab order sort
    For Each oCtl In Me.Controls
        On Error Resume Next
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next

'   position controls based on visibility and tab index -
'   skip author and sifgner name controls - they'll always
'   appear at the top of the dlg at the same y position
    Set oLastVisible = Me.cmbAuthor
'    Set oLastVisible = Me.cmbOffice
    For i = 4 To 36
'       get control
        Set oCtl = Me.Controls(arrCtls(i))
'       do only if visible
        If oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is VB.CheckBox) Then
'               position control relative to previous control
                oCtl.Top = oLastVisible.Top + _
                           oLastVisible.Height + _
                           iSpaceBefore
                
'               set accompanying label vertical position
                If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                    Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                End If
                
'               set control as last visible control
                Set oLastVisible = oCtl
            End If
        End If
    Next i
    
    Me.btnFinish.Top = oLastVisible.Top + _
        oLastVisible.Height + 210
        
    Me.btnCancel.Top = Me.btnFinish.Top
    Me.btnServiceList.Top = Me.btnFinish.Top
    
    If Me.Service.Definition.AllowManualInput Then
        Me.Line8.Y1 = Me.btnFinish.Top
        Me.Line8.Y2 = Me.btnFinish.Top
        Me.Line1.Y1 = Me.btnFinish.Top
        Me.Line1.Y2 = Me.btnFinish.Top
        Me.Shape1.Top = -20
        Me.Shape1.Height = Me.btnFinish.Top + 10
        Me.Shape3.Top = -20
        Me.Shape3.Height = Me.btnFinish.Top + 10
    Else
        Me.Line8.Visible = False
        Me.Line1.Visible = False
        Me.Shape1.Top = -20
        Me.Shape1.Height = 10000
        Me.Shape3.Top = -20
        Me.Shape3.Height = 10000
    End If
    
    If Not Me.Service.Definition.AllowManualInput Then
'       size dialog
        '*Word15
        Me.Height = Me.btnFinish.Top + Me.btnFinish.Height + IIf(g_bIsWord15x, 500 + g_sWord15HeightAdjustment, 500)
        Me.tabPages.Height = Me.Height - 40
        
'       exit - the code below positions the manual entry controls
        Exit Sub
    End If
    
'   position controls on signer tab
    Set oLastVisible = Nothing
    For i = 37 To 46
'       get control
        Set oCtl = Me.Controls(arrCtls(i))
'       do only if visible
        If oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is VB.CheckBox) Then
                If oLastVisible Is Nothing Then
'                   control is the first control on the tab -
'                   position as the first control
                    oCtl.Top = 255
                Else
'                   position control relative to previous control
                    oCtl.Top = oLastVisible.Top + _
                               oLastVisible.Height + _
                               iSpaceBefore
                End If
                
'               set accompanying label vertical position
                If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                    Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                End If
                
'               set control as last visible control
                Set oLastVisible = oCtl
            End If
        End If
    Next i
    
    '*Word15
    Me.tabPages.Height = Me.btnFinish.Top + Me.btnFinish.Height + 115
    Me.Height = Me.tabPages.Height + IIf(g_bIsWord15x, 375 + g_sWord15HeightAdjustment, 375)
    
    Word.Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmService.Resize"
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    m_bCancelled = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    m_bCancelled = False
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Sub ChangeAuthor(ByVal lID As Long)
    Dim xMsg As String
    Dim bForce As Boolean
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If
    
'   write to object
    On Error GoTo 0

    With Me.Service
'       set new author
        .Author = g_oMPO.People(lID)
    End With
    
    If Me.ActiveControl.Name = "cmbAuthor" Then _
        cmbAuthor_GotFocus
        
    Word.ActiveDocument.Fields.Update
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub btnServiceList_Click()
    CallServiceList
End Sub

Private Sub btnServiceList_GotFocus()
    OnControlGotFocus Me.btnServiceList
End Sub

Private Sub chkManualEntry_Click()
    Dim bForce As Boolean
    Dim bMan As Boolean
    
    On Error GoTo ProcError
    
    DoEvents
    
    bMan = Me.chkManualEntry.value = vbChecked
    SetManualEntry bMan
    PositionControls
    bForce = g_oMPO.ForceItemUpdate
    If bMan Then
'       don't force update if the dialog is intializing -
'       if we're in this branch and the dialog is initializing,
'       we must be defaulting to Manual input - if so, we
'       don't want the document placeholders wiped out when
'       there's going to be no input
        If Not m_bInit Then
            g_oMPO.ForceItemUpdate = True
        End If
        UpdateForSigner
        Me.txtSignerName.SetFocus
    Else
        g_oMPO.ForceItemUpdate = True
        Me.Service.UpdateForAuthor
    End If
    
    Me.Service.ManualEntry = Me.chkManualEntry
    
    g_oMPO.ForceItemUpdate = bForce
    Exit Sub
ProcError:
    g_oError.Show Err, "An error occurred while setting entry options."
End Sub

Private Sub UpdateForSigner()
    On Error GoTo ProcError
    With Me.Service
        .SignerName = Me.txtSignerName
        .SignerAddress = Me.txtSignerAddress
        .SignerCity = Me.txtSignerCity
        .SignerCompany = Me.txtSignerCompany
        .SignerCounty = Me.txtSignerCounty
        .SignerState = Me.txtSignerState
        'GLOG : 5473 : ceh
        .ESignerName = Me.txtSignerName
    End With
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmService.UpdateForSigner"
    Exit Sub
End Sub

Private Sub chkManualEntry_GotFocus()
    OnControlGotFocus Me.chkManualEntry
End Sub

Private Sub chkNoServiceTitle_Click()
    On Error GoTo ProcError
    Me.Service.NoServiceTitle = -(Me.chkNoServiceTitle)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkNoServiceTitle_GotFocus()
    OnControlGotFocus Me.chkNoServiceTitle
End Sub

Private Sub chkElectronicSignature_Click()
    On Error GoTo ProcError
    Me.Service.ESignature = -(Me.chkElectronicSignature)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkElectronicSignature_GotFocus()
    OnControlGotFocus Me.chkElectronicSignature
End Sub

Private Sub cmbAuthor_GotFocus()
    OnControlGotFocus Me.cmbAuthor, "zzmpSignerName"
End Sub

Private Sub cmbAuthor_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = wdKeyF10 And Shift = 1 Then
        ShowAuthorMenu
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthorIsDirty = False
        If Len(Me.cmbAuthor.BoundText) Then
            cmbAuthor_ItemChange
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_LostFocus()
'   update for author only if item change event ran
    On Error GoTo ProcError
    If m_bItemChanged Then
        With Me
'           update object with values of selected person
            If Not .Initializing Then
                .Service.UpdateForAuthor
            End If
        End With
        
        Word.ActiveDocument.Fields.Update

        m_bItemChanged = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
    If Button = 2 Then ShowAuthorMenu
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub ShowAuthorMenu()
    Dim bEnabled As Boolean
    On Error GoTo ProcError
    
'   enable specified menu items only if author
'   is currently in the db
    If TypeOf Me.Service.Author Is CReUseAuthor Then
        bEnabled = False
    Else
        bEnabled = True
    End If
    
    Me.mnuAuthor_Favorite.Enabled = bEnabled
    Me.mnuAuthor_SetDefault.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Checked = Me.Service.Author.Favorite
    Me.PopupMenu mnuAuthor
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If m_bAuthorIsDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_ItemChange()
    On Error GoTo ProcError
    With Me.cmbAuthor
        If .BoundText <> Me.Service.Author.ID Then
            ChangeAuthor .BoundText
            txtSignerName = .Text
            m_bAuthorIsDirty = True
            m_bItemChanged = True
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_ItemChange()
    On Error GoTo ProcError
    Me.Service.SignerGender = cmbGender.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOffice_GotFocus()
    OnControlGotFocus Me.cmbOffice, "zzmpSignerAddress"
End Sub

Private Sub cmbOffice_LostFocus()
    On Error GoTo ProcError
    With Me.Service
        .Office = g_oMPO.db.Offices(Me.cmbOffice.BoundText)
        .UpdateForOffice
    End With
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not successfully change the office."
    Exit Sub
End Sub

Private Sub cmbOffice_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbOffice, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOffice_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbOffice)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub txtCourtTitle_GotFocus()
    OnControlGotFocus Me.txtCourtTitle, "zzmpCourtTitle"
End Sub

Private Sub txtCourtTitle_LostFocus()
    On Error GoTo ProcError
    Me.Service.CourtTitle = Me.txtCourtTitle
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCaseNumber_GotFocus()
    OnControlGotFocus Me.txtCaseNumber, "zzmpCaseNumber"
End Sub

Private Sub txtCaseNumber_LostFocus()
    On Error GoTo ProcError
    Me.Service.CaseNumber = Me.txtCaseNumber
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbServiceTitle_GotFocus()
    OnControlGotFocus Me.cmbServiceTitle, "zzmpServiceTitle"
End Sub

Private Sub cmbServiceTitle_LostFocus()
    On Error GoTo ProcError
    Me.Service.ServiceTitle = Me.cmbServiceTitle.BoundText
    Word.Application.ScreenUpdating = True
    Word.Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbServiceTitle_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbServiceTitle)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbServiceTitle_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbServiceTitle, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    Dim xDocTitle As String
    Dim xOrdSuffix As String
    Dim xDefDate As String
    Dim oDoc As MPO.CDocument
    
    On Error GoTo ProcError
    If Me.Initializing Then
        LockWindowUpdate (Me.hwnd)
        Me.tabPages.CurrTab = 0
        With Me.Service
            If .Document.IsCreated And _
                .Document.Template = .Template.FileName Then
                UpdateForm
            Else

'               update for author if not manual entry
                If Not (.Definition.DefaultToManualInput) Then
                    Me.Service.UpdateForAuthor
                End If
                
'               set author to default author
                With .Author
                    Me.cmbAuthor.BoundText = .ID
                End With
                
                .Office = g_oMPO.Offices(.Author.Office.ID)
                .UpdateForOffice

                If .Definition.AllowServiceTitle Then
                    Me.cmbServiceTitle.SelectedItem = 0
                End If

                If Me.cmbServiceTitle.BoundText <> Empty Then
                    .ServiceTitle = Me.cmbServiceTitle.BoundText
                End If

'               set starting selections
                GetExistingValues
                
                m_bAuthorIsDirty = False
    
                Set oDoc = New MPO.CDocument
                
                On Error Resume Next
                Me.cmbGender.SelectedItem = 0
                .Custom1 = .Definition.Custom1DefaultText
                Me.txtCustom1 = .Custom1
                .Custom2 = .Definition.Custom2DefaultText
                Me.txtCustom2 = .Custom2
                If .ServiceState = .SignerState Then
                    Me.cmbCounty.BoundText = .SignerCounty
                Else
                    Me.cmbCounty.SelectedItem = 0
                End If
                
                With .Definition
                    If .AllowExecuteDate Then
                        Me.cmbExecuteDate.BoundText = .DefaultExecuteDate
                    End If
                    If .AllowServiceDate Then
                        Me.cmbServiceDate.BoundText = .DefaultServiceDate
                    End If
                End With
                
                On Error GoTo ProcError
'               set props based on selected items
                If Me.cmbGender.BoundText <> Empty Then
                    .SignerGender = Me.cmbGender.BoundText
                End If
                If Me.cmbCounty.BoundText <> Empty Then
                    .ServiceCounty = Me.cmbCounty.BoundText
                End If
                
'               could not get .text property below to return
'               the correct values without a preceding DoEvents
                DoEvents
                .ExecuteDate = Me.cmbExecuteDate.Text
                .ServiceDate = Me.cmbServiceDate.Text
            
                If .Definition.AllowManualInput Then
                    Me.txtSignerAddress = g_oMPO.Offices.Default.AddressOLD(vbCrLf)
                    Me.txtSignerCity = g_oMPO.Offices.Default.City
                    Me.txtSignerCounty = g_oMPO.Offices.Default.CountyID
                    Me.txtSignerState = g_oMPO.Offices.Default.State
                End If
            
'               setup dialog based on Manual Entry default
                If .Definition.DefaultToManualInput Then
'                   push the button - controls will
'                   be repositioned
                    Me.chkManualEntry.value = vbChecked
                End If
            End If
            
'           force reposition of controls
            PositionControls
            
            If Me.txtSignerName.Visible Then Me.chkElectronicSignature.Enabled = False 'GLOG : 5021 : !gm
            
            Me.tabPages.TabEnabled(1) = _
                Me.chkManualEntry.value = vbChecked
            
            With .Definition
            
                Me.tabPages.TabEnabled(1) = (chkManualEntry.value = vbChecked) And (.AllowManualAddress Or _
                                             .AllowManualCompany Or _
                                             .AllowManualCity Or _
                                             .AllowManualCounty Or _
                                             .AllowManualState)
                
                If .DefaultFooterText <> Empty Then _
                    Me.Service.PleadingPaper.DocTitle = .DefaultFooterText
                
                If .FooterTextMaxChars Then
                    Me.txtFooterText = Me.Service.PleadingPaper.DocTitle
                End If
                
                Me.btnServiceList.Visible = Me.Service.Definition.AllowServiceList
                
            End With
            
        End With
        
'       update any fields
        Me.Service.Document.UpdateFields
        
        If Not Me.Service.Document.IsSegment Then
            g_oMPO.ForceItemUpdate = False
        End If
        
'       move dialog to last position
        'MoveToLastPosition Me
        LockWindowUpdate (0&)
        
        DoEvents
        Screen.MousePointer = vbDefault
        
        EchoOn
        Word.Application.ScreenUpdating = True
            
        If Me.chkManualEntry = vbChecked Then
            Me.txtSignerName.SetFocus
            txtSignerName_GotFocus
        Else
            Me.cmbAuthor.SetFocus
            cmbAuthor_GotFocus
        End If
        
    End If
    SendKeys "+"
    m_bInit = False
    
    Exit Sub
ProcError:
    Word.Application.ScreenUpdating = True
    EchoOn
    Screen.MousePointer = vbDefault
    m_bInit = False
    g_oError.Show Err, "Error Activating Service Form"
    Exit Sub
End Sub

Private Sub cmbCounty_GotFocus()
    OnControlGotFocus Me.cmbCounty, "zzmpServiceCounty"
End Sub

Private Sub cmbCounty_LostFocus()
    On Error GoTo ProcError
    Me.Service.ServiceCounty = Me.cmbCounty.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCounty_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCounty)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCounty_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCounty, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbExecuteDate_GotFocus()
    OnControlGotFocus Me.cmbExecuteDate, "zzmpExecuteDate"
End Sub

Private Sub cmbExecuteDate_LostFocus()
    On Error GoTo ProcError
    Me.Service.ExecuteDate = Me.cmbExecuteDate.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbServiceDate_GotFocus()
    OnControlGotFocus Me.cmbServiceDate, "zzmpServiceDate"
End Sub

Private Sub cmbServiceDate_LostFocus()
    On Error GoTo ProcError
    Me.Service.ServiceDate = Me.cmbServiceDate.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_GotFocus()
    OnControlGotFocus Me.cmbGender
End Sub

Private Sub cmbGender_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbGender)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbGender, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    OnFormKeyDown KeyCode
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oGenders As XArray
    Dim oList As CList
    Dim oDateFormats As CList
    Dim oDef As CServiceDef
    Dim i As Integer
    Dim oArray As XArray
    Dim xOrdSuffix As String
    
    On Error GoTo ProcError
    Word.Application.StatusBar = "Initializing.  Please wait..."
    Application.ScreenUpdating = False

    m_bInit = True

'   move dialog way down - form activate
'   resizes the dialog - to prevent the user
'   from seeing weirdness, resize off the screen
'   then move into position
    Me.Top = 20000

'   set signer controls based on type def
    SetSignerTabControls
    
    Me.Cancelled = True
    
'   ensure date update
    g_oMPO.ForceItemUpdate = True
    
    Set m_oPersonsForm = New MPO.CPersonsForm
    Set m_oContacts = New MPO.CContacts
    Set m_oMenu = New MacPac90.CAuthorMenu
    Set oDef = Me.Service.Definition
    
'   set up date controls
    Set oDateFormats = g_oMPO.Lists("DateFormatsVariable")
    With oDateFormats
'       the date formats value of the definition
'       is the id of the group of date formats
        .FilterValue = oDef.DateFormats
        .Refresh
    End With
    
    With oDef
'       enable/disable footer text
        Me.txtFooterText.Visible = .FooterTextMaxChars
        Me.lblFooterText.Visible = .FooterTextMaxChars
'       enable/disable date as appropriate
        Me.cmbExecuteDate.Visible = .AllowExecuteDate
        Me.lblExecuteDate.Visible = .AllowExecuteDate
        Me.cmbServiceDate.Visible = .AllowServiceDate
        Me.lblServiceDate.Visible = .AllowServiceDate
'       enable/disable gender control
        Me.cmbGender.Visible = .AllowGender
        Me.lblGender.Visible = .AllowGender
'       enable/disable county control
        Me.cmbCounty.Visible = .AllowCounty
        Me.lblCounty.Visible = .AllowCounty
'       enable/disable party description control
        Me.txtPartyDescription.Visible = .AllowPartyDescription
        Me.lblPartyDescription.Visible = .AllowPartyDescription
        Me.cmbPartyTitle.Visible = .AllowPartyTitle
        Me.lblPartyTitle.Visible = .AllowPartyTitle
        Me.txtCourtTitle.Visible = .AllowCourtTitle
        Me.lblCourtTitle.Visible = .AllowCourtTitle
        Me.txtCaseNumber.Visible = .AllowCaseNumber
        Me.lblCaseNumber.Visible = .AllowCaseNumber
        Me.chkNoServiceTitle.Visible = .AllowDoNoIncludeServiceTitle
        Me.lblServiceTitle.Visible = .AllowServiceTitle
        Me.cmbServiceTitle.Visible = .AllowServiceTitle
            
        '---4092 - footer text max length

        On Error GoTo ProcError
        Dim lRet As Long
        lRet = .FooterTextMaxChars
        If lRet > 0 Then
            Me.txtFooterText.MaxLength = lRet
            Me.FooterTextMaxLength = lRet
        End If
'       set up Service Title control
        If .AllowServiceTitle Then
            Me.cmbServiceTitle.Array = xarStringToxArray(.ServiceTitles, 1)
            Me.cmbServiceTitle.Rebind
            ResizeTDBCombo Me.cmbServiceTitle, 3
        End If
            
'       get date list
        Set oArray = oDateFormats.ListItems.Source
        
'       get ordinal suffix based on current date - eg "rd", "th"
        xOrdSuffix = GetOrdinalSuffix(Day(Date))
        
'       replace all instances of ordinal variable
'       with ordinal suffix-this is done here because
'       there is no way in vb to format a dateas an ordinal
        For i = oArray.LowerBound(1) To oArray.UpperBound(1)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%o%", xOrdSuffix)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%or%", xOrdSuffix)  '9.7.1 #4402
        Next i
        
'       set up date controls
        If .AllowExecuteDate Then
            Me.cmbExecuteDate.Array = oArray
            Me.cmbExecuteDate.Rebind
            ResizeTDBCombo Me.cmbExecuteDate, 4
        End If
        
        If .AllowServiceDate Then
            Me.cmbServiceDate.Array = oArray
            Me.cmbServiceDate.Rebind
            ResizeTDBCombo Me.cmbServiceDate, 4
        End If
        
        If .Custom1Label <> "" Then
            Me.lblCustom1 = .Custom1Label
            Me.txtCustom1.Visible = True
            Me.lblCustom1.Visible = True
        Else
            Me.lblCustom1.Visible = False
            Me.txtCustom1.Visible = False
        End If
        
        If .Custom2Label <> "" Then
            Me.lblCustom2 = .Custom2Label
            Me.lblCustom2.Visible = True
            Me.txtCustom2.Visible = True
        Else
            Me.lblCustom2.Visible = False
            Me.txtCustom2.Visible = False
        End If
        If .Custom3Label <> "" Then
            Me.lblCustom3 = .Custom3Label
            Me.txtCustom3.Visible = True
            Me.lblCustom3.Visible = True
        Else
            Me.lblCustom3.Visible = False
            Me.txtCustom3.Visible = False
        End If
        
        If .Custom4Label <> "" Then
            Me.lblCustom4 = .Custom4Label
            Me.lblCustom4.Visible = True
            Me.txtCustom4.Visible = True
        Else
            Me.lblCustom4.Visible = False
            Me.txtCustom4.Visible = False
        End If
    End With
    
'   set up gender control if allowed
    If oDef.AllowGender Then
        Set oGenders = xarStringToxArray( _
            "Indefinite (he/she/they)|0|Female " & _
            "(she)|1|Male (he)|2|Plural (they)|3|Unknown (____)|4", 2)
        Me.cmbGender.Array = oGenders
        Me.cmbGender.Rebind
        ResizeTDBCombo Me.cmbGender, 5
    End If
    
'   set up county control if allowed
    If oDef.AllowCounty Then
        Set oList = g_oMPO.Lists("Counties")
        With oList
            .FilterValue = oDef.Level0
            .Refresh
            Me.cmbCounty.Array = .ListItems.Source
        End With
        Me.cmbCounty.Rebind
        ResizeTDBCombo Me.cmbCounty, 6
    End If
    
    If oDef.AllowPartyTitle Then
        Set oList = g_oMPO.Lists("PleadingParties")
        Set Me.cmbPartyTitle.Array = oList.ListItems.Source
        ResizeTDBCombo Me.cmbPartyTitle, 10
    End If
    
    With oDef
        Me.Caption = .DialogCaption
        If .AllowManualInput Then
            Me.chkManualEntry.Visible = True
            Me.tabPages.TabHeight = 400
        Else
            Me.chkManualEntry.Visible = False
            Me.tabPages.TabHeight = 1
        End If
    End With

    g_oMPO.People.Refresh
    
    With Me.cmbAuthor
        .Array = g_oMPO.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
    End With

    With Me.cmbOffice
        .Array = g_oMPO.Offices.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbOffice, 10
    End With

    If Not Me.Service.Document.IsSegment Then
        g_oMPO.ForceItemUpdate = False
    End If
    
    'GLOG : 5021 : !gm
    Me.chkElectronicSignature.Visible = (Me.Service.ESignatureBoilerplateFile <> "")
    
'   set service state
    Me.Service.ServiceState = g_oMPO.Lists("PleadingCourtsLevel0") _
        .ListItems.Item(, oDef.Level0).DisplayText
    
    LockWindowUpdate Me.hwnd
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oPersonsForm = Nothing
    Set m_oContacts = Nothing
    Set m_oMenu = Nothing
    Set g_oPrevControl = Nothing
    'SetLastPosition Me
End Sub

Private Sub tabPages_Click()
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    Select Case Me.tabPages.CurrTab
        Case 0
            If Me.txtSignerName.Visible Then
                Me.txtSignerName.SetFocus
            Else
                Me.cmbAuthor.SetFocus
            End If
        Case 1
            If Me.txtSignerCompany.Visible Then
                Me.txtSignerCompany.SetFocus
            ElseIf Me.txtSignerAddress.Visible Then
                Me.txtSignerAddress.SetFocus
            ElseIf Me.txtSignerCity.Visible Then
                Me.txtSignerCity.SetFocus
            ElseIf Me.txtSignerState.Visible Then
                Me.txtSignerState.SetFocus
            ElseIf Me.txtSignerCounty.Visible Then
                Me.txtSignerCounty.SetFocus
            End If
    End Select

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
    
End Sub

Private Sub txtCustom1_GotFocus()
    OnControlGotFocus Me.txtCustom1, "zzmpServiceCustom1"
End Sub

Private Sub txtCustom1_LostFocus()
    On Error GoTo ProcError
    Me.Service.Custom1 = Me.txtCustom1
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCustom2_GotFocus()
    OnControlGotFocus Me.txtCustom2, "zzmpServiceCustom2"
End Sub
Private Sub txtCustom2_LostFocus()
    On Error GoTo ProcError
    Me.Service.Custom2 = Me.txtCustom2
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCustom3_GotFocus()
    OnControlGotFocus Me.txtCustom3, "zzmpServiceCustom3"
End Sub

Private Sub txtCustom3_LostFocus()
    On Error GoTo ProcError
    Me.Service.Custom3 = Me.txtCustom3
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCustom4_GotFocus()
    OnControlGotFocus Me.txtCustom4, "zzmpServiceCustom4"
End Sub
Private Sub txtCustom4_LostFocus()
    On Error GoTo ProcError
    Me.Service.Custom4 = Me.txtCustom4
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtDocumentTitle_KeyPress(KeyAscii As Integer)
'   Restrict Enter key in this control
    If KeyAscii = vbKeyReturn Or KeyAscii = 10 Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtFooterText_Change()
    FooterLengthWarningIfNecessary
End Sub

Private Sub txtFooterText_KeyDown(KeyCode As Integer, Shift As Integer)
    '---9.7.1 - 4092
    Dim ShiftKey As Integer
       ShiftKey = Shift And 7
    Select Case ShiftKey
        Case 4 ' or vbAltMask
           'ALT key
           Exit Sub
        Case Else
    End Select
    Select Case KeyCode
        Case vbKeyTab, vbKeyBack, vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
        
        Case Else
            FooterLengthWarningIfNecessary
    End Select
End Sub
Private Sub txtFooterText_LostFocus()
    On Error GoTo ProcError
    Me.Service.PleadingPaper.DocTitle = Me.txtFooterText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPartyDescription_GotFocus()
    OnControlGotFocus Me.txtPartyDescription, "zzmpPartyDescription"
End Sub

Private Sub txtPartyDescription_LostFocus()
    On Error GoTo ProcError
    Me.Service.PartyDescription = Me.txtPartyDescription
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbPartyTitle_GotFocus()
    OnControlGotFocus cmbPartyTitle, "zzmpPartyTitle"
End Sub
Private Sub cmbPartyTitle_LostFocus()
    On Error GoTo ProcError
    Me.Service.PartyTitle = cmbPartyTitle.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerAddress_GotFocus()
    OnControlGotFocus Me.txtSignerAddress, "zzmpSignerAddress"
End Sub

Private Sub txtSignerAddress_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0
End Sub

Private Sub txtSignerAddress_LostFocus()
    On Error GoTo ProcError
    Me.Service.SignerAddress = Me.txtSignerAddress
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDocumentTitle_GotFocus()
    OnControlGotFocus Me.txtDocumentTitle, "zzmpDocumentTitles"
End Sub

Private Sub txtDocumentTitle_LostFocus()
    On Error GoTo ProcError
    Me.Service.DocumentTitles = Me.txtDocumentTitle
    If Me.Service.Definition.DefaultFooterText = "" And _
    Me.txtFooterText.Visible Then
        Me.txtFooterText = Me.txtDocumentTitle
        Me.Service.PleadingPaper.DocTitle = Me.txtFooterText    'more accurate than txtDocumentTitle, based on MaxFooterLength
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerCity_GotFocus()
    OnControlGotFocus Me.txtSignerCity, "zzmpSignerCity"
End Sub

Private Sub txtSignerCity_LostFocus()
    On Error GoTo ProcError
    Me.Service.SignerCity = Me.txtSignerCity
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerCompany_GotFocus()
    OnControlGotFocus Me.txtSignerCompany, "zzmpSignerCompany"
End Sub

Private Sub txtSignerCompany_LostFocus()
    On Error GoTo ProcError
    Me.Service.SignerCompany = Me.txtSignerCompany
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFooterText_GotFocus()
    OnControlGotFocus Me.txtFooterText, "zzmpDocTitle"
End Sub

Private Sub txtSignerCounty_GotFocus()
    OnControlGotFocus Me.txtSignerCounty, "zzmpSignerCounty"
End Sub

Private Sub txtSignerCounty_LostFocus()
    On Error GoTo ProcError
    Me.Service.SignerCounty = Me.txtSignerCounty
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerName_GotFocus()
    OnControlGotFocus Me.txtSignerName, "zzmpSignerName"
End Sub

Private Sub UpdateForm()
    On Error GoTo ProcError
    With Me.Service
        Me.cmbAuthor.Text = .Document.ReuseAuthor.DisplayName
        Me.cmbOffice.BoundText = .Office.ID
        Me.txtDocumentTitle = .DocumentTitles
        If .Definition.FooterTextMaxChars Then _
            Me.txtFooterText = .PleadingPaper.DocTitle
            
        Me.cmbServiceTitle.BoundText = .ServiceTitle
        If Me.cmbServiceTitle.BoundText = Empty Then
            Me.cmbServiceTitle.SelectedItem = 0
            cmbServiceTitle_LostFocus
        End If
        
        Me.txtSignerAddress = .SignerAddress
        Me.txtSignerCity = .SignerCity
        Me.cmbPartyTitle.BoundText = .PartyTitle
        Me.txtSignerCompany = .SignerCompany
        Me.txtSignerCounty = .SignerCounty
        Me.txtSignerName = .SignerName
        Me.txtSignerState = .SignerState
        Me.cmbCounty.BoundText = .ServiceCounty
        Me.cmbExecuteDate.Text = .ExecuteDate
        Me.cmbServiceDate.Text = .ServiceDate
        Me.cmbGender.BoundText = .SignerGender
        Me.txtPartyDescription = .PartyDescription
        Me.txtCaseNumber = .CaseNumber
        Me.txtCourtTitle = .CourtTitle
        If .Definition.Custom1Label <> Empty Then _
            Me.txtCustom1 = .Custom1
        If .Definition.Custom2Label <> Empty Then _
            Me.txtCustom2 = .Custom2
        If .Definition.Custom3Label <> Empty Then _
            Me.txtCustom3 = .Custom3
        If .Definition.Custom4Label <> Empty Then _
            Me.txtCustom4 = .Custom4
        
        '9.8.1030
        'force default author if new type is not setup for manual entry
        If (Not Me.Service.Definition.AllowManualInput) And (.ManualEntry) Then
            Me.chkManualEntry = 0
            Me.Service.UpdateForAuthor
        Else
            If .ManualEntry <> mpUndefined Then _
                    Me.chkManualEntry = Abs(.ManualEntry)
        End If
                
        If .NoServiceTitle <> mpUndefined Then _
                Me.chkNoServiceTitle = Abs(.NoServiceTitle)
                
        If .ESignature <> mpUndefined Then _
                Me.chkElectronicSignature = Abs(.ESignature)
        
    
    End With
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmService.UpdateForm"
    Exit Sub
End Sub
'******************************************************************************
'Menu events
'******************************************************************************
Private Sub mnuAuthor_ManageLists_Click()
'   get current selection
    
    Dim lID As Long
    
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ManageAuthors Me.cmbAuthor, lID
    Else
        m_oMenu.ManageAuthors Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_AddNew_Click()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.cmbAuthor.Columns("fldID")
    m_oMenu.AddPerson Me.cmbAuthor, lID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Favorite_Click()
    Dim lID As Long
    
    On Error Resume Next
    DoEvents
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ToggleFavorite Me.cmbAuthor, lID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    On Error GoTo ProcError
    With Me.Service
        .Template.DefaultAuthor = .Author
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Copy_Click()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
    On Error GoTo ProcError
'   if author isn't in list, the
'   author is the reuse author
    ' If Me.cmbAuthor.Row = -1 Then
    If TypeOf Me.Service.Author Is CReUseAuthor Then '***4337
'       use reuse author as source
        Set oSource = Me.Service.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oMPO.People(Me.cmbAuthor.BoundText)
    End If
    
'   copy the person
    Set oCopy = m_oPersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerName_LostFocus()
    On Error GoTo ProcError
    Me.Service.SignerName = Me.txtSignerName
    If Me.txtSignerName.Text <> "" Then 'GLOG : 5021 : !gm
        Me.chkElectronicSignature.Enabled = True
    Else
        Me.chkElectronicSignature = vbUnchecked
        Me.chkElectronicSignature.Enabled = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerState_GotFocus()
    OnControlGotFocus Me.txtSignerState, "zzmpSignerState"
End Sub

Private Sub txtSignerState_LostFocus()
    On Error GoTo ProcError
    Me.Service.SignerState = Me.txtSignerState
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub CallServiceList()
    Dim oExistingList As MPO.CServiceList
    Dim oForm As frmServiceList
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    DoEvents
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

    Set oForm = New frmServiceList
    
    With oForm
        .ParentForm = Me
        Set oExistingList = g_oMPO.ExistingServiceList
        
        If oExistingList Is Nothing Then
            Set .ServiceList = Me.Service.ServiceList
        Else
            Set .ServiceList = oExistingList
        End If
        .Left = Me.Left
        .Top = Me.Top
        .Show vbModal
        If Not .Cancelled Then
            On Error Resume Next
            Set Me.Service.ServiceList = .ServiceList
        Else
            .ServiceList.Recipients.RemoveAll
        End If
    End With
    Unload oForm
    Set oForm = Nothing
    
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
    
    DoEvents
    Me.PositionControls
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    g_oError.Show Err
    Exit Sub

End Sub

Private Sub GetExistingValues()
    Dim xDocTitle As String
    Dim oDoc As MPO.CDocument
    Dim xTemp As String
    Dim lID As Long
    Dim oPleading As MPO.CPleading
    Dim xPartyTitle As String
    Dim xPartyName As String

    On Error GoTo ProcError
    Set oDoc = New MPO.CDocument
    
'   Get pleading document title
    xDocTitle = oDoc.GetVar("PleadingCaption_1_CaseTitle")
    
    If xDocTitle = "" Then
        xDocTitle = oDoc.GetVar("Pleading_1_DocumentTitle")
    End If
    
    If xDocTitle <> "" Then
        Me.txtDocumentTitle = xDocTitle
        Me.Service.DocumentTitles = xDocTitle
    End If

'   Get pleading case number
    Me.txtCaseNumber.Text = oDoc.GetVar("PleadingCaption_1_CaseNumber1")
    Me.Service.CaseNumber = Me.txtCaseNumber.Text
    
'   Get pleading Court title
    xTemp = oDoc.GetVar("Pleading_1_CourtTitle")
    xTemp = xSubstitute(xTemp, vbCrLf, vbCrLf & " ")
    xTemp = mpbase2.xInitialCap(xTemp)
    xTemp = xSubstitute(xTemp, vbCrLf & " ", vbCrLf)
    
    Me.txtCourtTitle.Text = xTemp
    Me.Service.CourtTitle = Me.txtCourtTitle.Text
    
'   Get Pleading Party Title & Description/Name
    xPartyTitle = oDoc.RetrieveItem("PartyTitle", "PleadingCounsel")
    If xPartyTitle = Empty Then
        xPartyTitle = oDoc.RetrieveItem("PartyTitle", "PleadingSig")
    End If
    xPartyName = oDoc.RetrieveItem("PartyName", "PleadingCounsel")
    If xPartyName = Empty Then
        xPartyName = oDoc.RetrieveItem("PartyName", "PleadingSig")
    End If
    Me.cmbPartyTitle.Text = xPartyTitle
    Me.txtPartyDescription = xPartyName
    Me.Service.PartyTitle = xPartyTitle
    Me.Service.PartyDescription = xPartyName
    
'   Get Pleading Signer
    lID = g_oMPO.GetPleadingSignerID()
    
    If lID <> 0 Then
        m_bInit = False
        Me.cmbAuthor.BoundText = lID
        If Me.cmbAuthor.BoundText = Empty Then
            Me.cmbAuthor.SelectedItem = 0
        End If
        cmbAuthor_ItemChange
        cmbAuthor_LostFocus
        m_bInit = True
    End If

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub FooterLengthWarningIfNecessary()
    Dim xMsg As String
    
    If Me.FooterTextMaxLength = 0 Then Exit Sub
    
    If Len(txtFooterText) >= Me.FooterTextMaxLength Then
        xMsg = "The footer text is" & Str(Me.FooterTextMaxLength) & " characters and might not fit in the footer table cell." & _
            vbLf & vbLf & "Please edit the text in the footer text box."
        MsgBox xMsg, vbInformation, App.Title
    End If

End Sub
