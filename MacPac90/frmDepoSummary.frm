VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmDepoSummary 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Deposition Summary"
   ClientHeight    =   4044
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   5448
   Icon            =   "frmDepoSummary.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4044
   ScaleWidth      =   5448
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtExaminerName 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1770
      TabIndex        =   9
      Top             =   2325
      Width           =   3570
   End
   Begin VB.TextBox txtDepositionDate 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1770
      TabIndex        =   5
      Top             =   1290
      Width           =   3570
   End
   Begin VB.TextBox txtVolumeNumber 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1770
      TabIndex        =   3
      Top             =   765
      Width           =   3570
   End
   Begin VB.TextBox txtClientMatterNumber 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1770
      TabIndex        =   7
      Top             =   1800
      Width           =   3570
   End
   Begin VB.TextBox txtDeponentName 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1770
      TabIndex        =   1
      Top             =   240
      Width           =   3570
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3285
      TabIndex        =   12
      Top             =   3510
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4350
      TabIndex        =   13
      Top             =   3510
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbFormatType 
      Height          =   300
      Left            =   1755
      OleObjectBlob   =   "frmDepoSummary.frx":058A
      TabIndex        =   11
      Top             =   2880
      Width           =   3585
   End
   Begin VB.Label lblFormatType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Summary Format:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   10
      Top             =   2940
      Width           =   1455
   End
   Begin VB.Label lblExaminerName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "E&xaminer Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   105
      TabIndex        =   8
      Top             =   2385
      Width           =   1455
   End
   Begin VB.Label lblDepositionDate 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Date of Deposition:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   105
      TabIndex        =   4
      Top             =   1320
      Width           =   1455
   End
   Begin VB.Label lblVolumeNumber 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Volume Number:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   105
      TabIndex        =   2
      Top             =   780
      Width           =   1455
   End
   Begin VB.Label lblClientMatterNumber 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "File Num&ber:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   105
      TabIndex        =   6
      Top             =   1845
      Width           =   1455
   End
   Begin VB.Label lblDeponentName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Deponent &Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   90
      TabIndex        =   0
      Top             =   255
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6075
      Left            =   -15
      Top             =   -15
      Width           =   1710
   End
End
Attribute VB_Name = "frmDepoSummary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_oDepo As CDepoSummary
Private m_oDepos As mpDB.CDepoSummaryDefs
Private m_oDoc As CDocument
Private m_bCancelled As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Let DepoSummary(oNew As CDepoSummary)
    Set m_oDepo = oNew
End Property

Public Property Get DepoSummary() As CDepoSummary
    Set DepoSummary = m_oDepo
End Property

Public Property Let Document(oNew As CDocument)
    Set m_oDoc = oNew
End Property

Public Property Get Document() As CDocument
    Set Document = m_oDoc
End Property

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Cancelled = True
    Me.Hide
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnOK_Click()
    On Error GoTo ProcError
    Me.Cancelled = False
    Me.Hide
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnOK_GotFocus()
    OnControlGotFocus Me.btnOK
End Sub

Private Sub cmbFormatType_GotFocus()
    Application.ScreenRefresh
    OnControlGotFocus Me.cmbFormatType
End Sub

Private Sub cmbFormatType_ItemChange()
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    DepoSummary.FormatID = cmbFormatType.BoundText
    If g_oMPO.DynamicEditing = True Then
        EchoOff
        Document.ClearSection Selection.Sections(1)
        DepoSummary.InsertBoilerplate
        DepoSummary.Refresh
        EchoOn
    End If
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbFormatType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbFormatType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    On Error GoTo ProcError
    If Me.Initializing Then
        If Me.Document.IsCreated Then
            Me.Initializing = False
            UpdateForm
            Me.Initializing = True
        Else
        '---preload date field
            Me.txtDepositionDate = Format(Now, "mmmm d, yyyy")
            DepoSummary.DepositionDate = Me.txtDepositionDate
        End If

'---    this is a "sticky property" in back end - stored in user ini
        Me.cmbFormatType.BoundText = DepoSummary.FormatID
        EchoOn
        Application.ScreenUpdating = True
        DoEvents
        Me.Initializing = False
        Screen.MousePointer = vbDefault

        'chadd - b5
'       move dialog to last position
        'MoveToLastPosition Me
    End If
    Exit Sub
ProcError:
    EchoOn
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Error Activating Depo Summary Form"
    Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim lFormatID As Long
    On Error GoTo ProcError
    Application.ScreenUpdating = False
    Me.Initializing = True
    
    Me.Top = 20000
    
    Me.Cancelled = True

'   ensure that we have a Depo object
    If Me.DepoSummary Is Nothing Then
'---obtain sticky type id
        lFormatID = Max(Val(GetUserIni("Depo Summary", "FormatID")), 1)
        Me.DepoSummary = g_oMPO.NewDepoSummary(lFormatID)
    End If

    Set m_oDoc = Me.DepoSummary.Document
    Set m_oDepos = g_oMPO.db.DepoSummaryDefs

'---load dropdown with format types
    Me.cmbFormatType.Array = m_oDepos.ListSource

    ResizeTDBCombo Me.cmbFormatType, 4

    'chrem - b5
''   move dialog to last position
'    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Error Loading Depo Summary Form"
    Application.ScreenUpdating = True
    Exit Sub
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    OnFormKeyDown KeyCode
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'SetLastPosition Me
    Set m_oDepo = Nothing
    Set m_oDepos = Nothing
    Set m_oDoc = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Unloading Form"
    Exit Sub
End Sub

Private Sub txtDeponentName_GotFocus()
    OnControlGotFocus Me.txtDeponentName, "zzmpDeponentName"
End Sub

Private Sub txtDeponentName_LostFocus()
    On Error GoTo ProcError
    DepoSummary.DeponentName = Me.txtDeponentName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtClientMatterNumber_GotFocus()
    OnControlGotFocus Me.txtClientMatterNumber, "zzmpClientMatterNumber"
End Sub

Private Sub txtClientMatterNumber_LostFocus()
    On Error GoTo ProcError
    DepoSummary.ClientMatterNumber = Me.txtClientMatterNumber
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtVolumeNumber_GotFocus()
    OnControlGotFocus Me.txtVolumeNumber, "zzmpVolumeNumber"
End Sub

Private Sub txtVolumeNumber_LostFocus()
    On Error GoTo ProcError
    DepoSummary.VolumeNumber = Me.txtVolumeNumber
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtExaminerName_GotFocus()
    OnControlGotFocus Me.txtExaminerName, "zzmpExaminerName"
End Sub

Private Sub txtExaminerName_LostFocus()
    On Error GoTo ProcError
    DepoSummary.ExaminerName = Me.txtExaminerName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDepositionDate_GotFocus()
    OnControlGotFocus Me.txtDepositionDate, "zzmpDepositionDate"
End Sub

Private Sub txtDepositionDate_LostFocus()
    On Error GoTo ProcError
    DepoSummary.DepositionDate = Me.txtDepositionDate
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


'************************************
'---methods
'************************************

Private Sub UpdateForm()
    With Me.DepoSummary
        Me.txtClientMatterNumber = .ClientMatterNumber
        Me.txtDeponentName = .DeponentName
        Me.txtDepositionDate = .DepositionDate
        Me.txtExaminerName = .ExaminerName
        Me.txtVolumeNumber = .VolumeNumber
    End With
End Sub

