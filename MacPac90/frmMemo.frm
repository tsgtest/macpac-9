VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Object = "{FC715925-5921-4AC0-B4BD-36B619407BAE}#1.1#0"; "MPCM.ocx"
Begin VB.Form frmMemo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Memo"
   ClientHeight    =   6216
   ClientLeft      =   2856
   ClientTop       =   540
   ClientWidth     =   5460
   Icon            =   "frmMemo.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6216
   ScaleWidth      =   5460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4065
      TabIndex        =   47
      Top             =   5760
      Width           =   650
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   48
      Top             =   5760
      Width           =   650
   End
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   3315
      Picture         =   "frmMemo.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   46
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   5760
      Width           =   705
   End
   Begin C1SizerLibCtl.C1Tab TabPages 
      Height          =   6300
      Left            =   -45
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   -120
      Width           =   5595
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|P&hrases|&Letterhead"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Caption         =   "            '9.7.1 #4028"
         Height          =   5850
         Left            =   6336
         TabIndex        =   52
         Top             =   15
         Width           =   5580
         Begin VB.TextBox txtCustom3 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1830
            TabIndex        =   45
            Top             =   4410
            Visible         =   0   'False
            Width           =   3570
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom3 
            Appearance      =   0  'Flat
            Caption         =   "Custom 3"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   4230
            TabIndex        =   37
            Top             =   1440
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom2 
            Appearance      =   0  'Flat
            Caption         =   "Custom 2"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3045
            TabIndex        =   36
            Top             =   1440
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom1 
            Appearance      =   0  'Flat
            Caption         =   "Custom 1"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1860
            TabIndex        =   35
            Top             =   1440
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeDPhrases2InHeader 
            Appearance      =   0  'Flat
            Caption         =   "Include &Confidential Phrases in Header"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1905
            TabIndex        =   43
            Top             =   3990
            Width           =   3135
         End
         Begin VB.CheckBox chkIncludeLetterheadAdmittedIn 
            Appearance      =   0  'Flat
            Caption         =   "Ad&mitted In"
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   4230
            TabIndex        =   34
            Top             =   1110
            Width           =   1215
         End
         Begin VB.CheckBox chkIncludeLetterheadFax 
            Appearance      =   0  'Flat
            Caption         =   "Fa&x"
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3045
            TabIndex        =   33
            Top             =   1110
            Width           =   735
         End
         Begin VB.CheckBox chkIncludeLetterheadTitle 
            Appearance      =   0  'Flat
            Caption         =   "T&itle"
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   1860
            TabIndex        =   32
            Top             =   1110
            Width           =   600
         End
         Begin VB.CheckBox chkIncludeLetterheadName 
            Appearance      =   0  'Flat
            Caption         =   "&Name"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   1860
            TabIndex        =   29
            Top             =   765
            Width           =   800
         End
         Begin VB.CheckBox chkIncludeLetterheadPhone 
            Appearance      =   0  'Flat
            Caption         =   "P&hone"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   3045
            TabIndex        =   30
            Top             =   765
            Width           =   800
         End
         Begin VB.CheckBox chkIncludeLetterheadEMail 
            Appearance      =   0  'Flat
            Caption         =   "&E-Mail"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   4230
            TabIndex        =   31
            Top             =   765
            Width           =   800
         End
         Begin VB.CheckBox chkIncludeDPhrasesInHeader 
            Appearance      =   0  'Flat
            Caption         =   "Include Delivery &Phrases in Header"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1905
            TabIndex        =   42
            Top             =   3570
            Width           =   3435
         End
         Begin VB.TextBox txtHeaderAdditionalText 
            Appearance      =   0  'Flat
            Height          =   975
            Left            =   1830
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   41
            Top             =   2400
            Width           =   3570
         End
         Begin TrueDBList60.TDBCombo cmbLetterheadType 
            DataField       =   "fldName"
            Height          =   555
            Left            =   1815
            OleObjectBlob   =   "frmMemo.frx":0CAC
            TabIndex        =   27
            Top             =   255
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbDateType 
            Height          =   555
            Left            =   1830
            OleObjectBlob   =   "frmMemo.frx":3355
            TabIndex        =   39
            Top             =   1890
            Width           =   3555
         End
         Begin VB.Label lblCustom3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 3:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   120
            TabIndex        =   44
            Top             =   4470
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblDateType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Date Format:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   180
            TabIndex        =   38
            Top             =   1965
            Width           =   1455
         End
         Begin VB.Label lblInclude 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Include Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   165
            TabIndex        =   28
            Top             =   810
            Width           =   1455
         End
         Begin VB.Label lblLetterheadType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Letterhead T&ype:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   165
            TabIndex        =   26
            Top             =   285
            Width           =   1455
         End
         Begin VB.Label lblPage2HeaderText 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Page &2 Header:"
            ForeColor       =   &H8000000E&
            Height          =   285
            Left            =   180
            TabIndex        =   40
            Top             =   2370
            Width           =   1455
         End
         Begin VB.Line Line10 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5515
            Y1              =   0
            Y2              =   0
         End
         Begin VB.Line Line9 
            BorderColor     =   &H00FFFFFF&
            X1              =   0
            X2              =   5530
            Y1              =   5808
            Y2              =   5808
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   9495
            Left            =   30
            Top             =   -3675
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   5850
         Left            =   6096
         TabIndex        =   51
         Top             =   15
         Width           =   5580
         Begin VB.TextBox txtCustom2 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1800
            TabIndex        =   25
            Top             =   3300
            Visible         =   0   'False
            Width           =   3585
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase 
            Height          =   735
            Left            =   1800
            TabIndex        =   17
            Top             =   240
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   1291
            ListRows        =   8
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase2 
            Height          =   735
            Left            =   1815
            TabIndex        =   19
            Top             =   1230
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   1291
            ListRows        =   8
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.TextBox txtTypistInitials 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1800
            TabIndex        =   21
            Top             =   2205
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbEnclosures 
            Height          =   600
            Left            =   1800
            OleObjectBlob   =   "frmMemo.frx":5568
            TabIndex        =   23
            Top             =   2745
            Width           =   3600
         End
         Begin VB.Label lblCustom2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 2:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   135
            TabIndex        =   24
            Top             =   3360
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblDeliveryPhrase2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Confidential Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   420
            Left            =   105
            TabIndex        =   18
            Top             =   1245
            Width           =   1545
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblEnclosures 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " E&nclosures:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   330
            TabIndex        =   22
            Top             =   2790
            Width           =   1320
         End
         Begin VB.Label lblTypistInitials 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " T&ypist Initials:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   330
            TabIndex        =   20
            Top             =   2220
            Width           =   1320
         End
         Begin VB.Label lblDeliveryPhrase 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Delivery &Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   105
            TabIndex        =   16
            Top             =   255
            Width           =   1545
            WordWrap        =   -1  'True
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00FFFFFF&
            X1              =   0
            X2              =   5530
            Y1              =   5808
            Y2              =   5808
         End
         Begin VB.Line Line4 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5515
            Y1              =   0
            Y2              =   0
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   9515
            Left            =   30
            Top             =   -3690
            Width           =   1695
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   5850
         Left            =   12
         TabIndex        =   50
         Top             =   15
         Width           =   5580
         Begin VB.TextBox txtCustom1 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1830
            TabIndex        =   15
            Top             =   5370
            Visible         =   0   'False
            Width           =   3585
         End
         Begin MPCM.ClientMatterComboBox cmbCM 
            Height          =   315
            Left            =   1830
            TabIndex        =   11
            Top             =   4080
            Width           =   3525
            _ExtentX        =   6223
            _ExtentY        =   550
            SourceClass     =   "mpCMIA.CClientMatter"
            Col1Caption     =   "No."
            ColWidth1       =   1020
            ColWidth2       =   1020
            ListRows        =   4
            ComboType       =   2
            SelectTextOnFocus=   -1  'True
            PopulateOnLoad  =   0   'False
            AdditionalClientFilter=   ""
            AdditionalMatterFilter=   ""
            RunConnected    =   0   'False
         End
         Begin VB.TextBox txtFrom 
            Appearance      =   0  'Flat
            Height          =   690
            Left            =   1830
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   5
            Top             =   1545
            Width           =   3585
         End
         Begin VB.TextBox txtRecipients 
            Appearance      =   0  'Flat
            Height          =   690
            Left            =   1830
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   705
            Width           =   3585
         End
         Begin VB.TextBox txtCC 
            Appearance      =   0  'Flat
            Height          =   690
            Left            =   1830
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   7
            Top             =   2385
            Width           =   3585
         End
         Begin VB.TextBox txtBCC 
            Appearance      =   0  'Flat
            Height          =   690
            Left            =   1830
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   9
            Top             =   3240
            Width           =   3585
         End
         Begin VB.TextBox txtReline 
            Appearance      =   0  'Flat
            Height          =   690
            Left            =   1830
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   13
            Top             =   4545
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbAuthor 
            Height          =   600
            Left            =   1815
            OleObjectBlob   =   "frmMemo.frx":7785
            TabIndex        =   1
            Top             =   225
            Width           =   3615
         End
         Begin VB.Label lblCustom1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 1:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   120
            TabIndex        =   14
            Top             =   5430
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblFrom 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Fr&om:"
            ForeColor       =   &H00FFFFFF&
            Height          =   675
            Left            =   180
            TabIndex        =   4
            Top             =   1545
            Width           =   1455
         End
         Begin VB.Label lblClientMatter 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Fil&e No.:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   180
            TabIndex        =   10
            Top             =   4110
            Width           =   1455
         End
         Begin VB.Line Line2 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5550
            Y1              =   0
            Y2              =   0
         End
         Begin VB.Line Line8 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5550
            Y1              =   0
            Y2              =   0
         End
         Begin VB.Label lblAuthor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   180
            TabIndex        =   0
            Top             =   270
            Width           =   1455
         End
         Begin VB.Label lblReline 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Re Line:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   1050
            TabIndex        =   12
            Top             =   4560
            Width           =   585
         End
         Begin VB.Line Line7 
            BorderColor     =   &H80000005&
            X1              =   -12
            X2              =   5503
            Y1              =   5808
            Y2              =   5808
         End
         Begin VB.Label lblBCC 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&bcc:"
            ForeColor       =   &H00FFFFFF&
            Height          =   675
            Left            =   180
            TabIndex        =   8
            Top             =   3225
            Width           =   1455
         End
         Begin VB.Label lblCC 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&cc:"
            ForeColor       =   &H00FFFFFF&
            Height          =   675
            Left            =   180
            TabIndex        =   6
            Top             =   2415
            Width           =   1455
         End
         Begin VB.Label lblRecipients 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&To:"
            ForeColor       =   &H00FFFFFF&
            Height          =   675
            Left            =   180
            TabIndex        =   2
            Top             =   735
            Width           =   1455
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6055
            Left            =   30
            Top             =   -240
            Width           =   1695
         End
      End
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_EditAuthorOptions 
         Caption         =   "&Edit Author Preferences..."
      End
      Begin VB.Menu mnuAuthor_CopyOptions 
         Caption         =   "Cop&y Author Preferences..."
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmMemo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_Memo As MPO.CMemo
Private m_PersonsForm As MPO.CPersonsForm
Private m_oContacts As MPO.CContacts
Private m_oMenu As MacPac90.CAuthorMenu
Private WithEvents m_oOptForm As MPO.COptionsForm
Attribute m_oOptForm.VB_VarHelpID = -1

Private m_bAuthorDirty As Boolean
Private b_Opened As Boolean
Private m_bCancelled As Boolean
Private m_bAuthorManual As Boolean
Private m_ShowHiddenText As Boolean
Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private xMsg As String
Private m_xPrevAuthor As String
Private m_bDyn As Boolean
Private m_bItemChanged As Boolean
Private m_xDefaultCCLabel As String '9.7.1 #4151
Private m_xDefaultBCCLabel As String '9.7.1 #4151
Private m_bUpdateDefaults As Boolean    'GLOG : 5598 : ceh


Private Const mpExtrasCaptions = "&Main|E&xtras|&Letterhead"
Private Const mpPhrasesCaptions = "&Main|P&hrases|&Letterhead"

'****************************************************************
'Properties
'****************************************************************
Public Property Get Memo() As MPO.CMemo
    Set Memo = m_Memo
End Property

Public Property Let Memo(oNew As MPO.CMemo)
    Set m_Memo = oNew
End Property

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property

Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
'    Me.CascadeUpdates = False
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = Not bNew '*c
End Property

Public Property Let AuthorIsDirty(bNew As Boolean)
    m_bAuthorDirty = bNew
End Property

Public Property Get AuthorIsDirty() As Boolean
    AuthorIsDirty = m_bAuthorDirty
End Property

Public Property Let AuthorIsManual(bNew As Boolean)
    m_bAuthorManual = bNew
End Property

Public Property Get AuthorIsManual() As Boolean
    AuthorIsManual = m_bAuthorManual
End Property

Private Sub UpdateHeaderText() '9.7.1 #4162
    Dim xTemp As String
    Dim vTemp As Variant
    Dim i As Integer
    xTemp = Me.txtRecipients
    If xTemp <> "" And (Me.Memo.HeaderNamesSeparator <> vbCrLf Or Me.Memo.HeaderNamesFinalSeparator <> vbCrLf) Then
        vTemp = Split(xTemp, vbCrLf)
        xTemp = ""
        For i = 0 To UBound(vTemp)
            If xTemp <> "" Then
                If i < UBound(vTemp) Then
                    xTemp = xTemp & Me.Memo.HeaderNamesSeparator
                ElseIf i > 0 Then
                    xTemp = xTemp & Me.Memo.HeaderNamesFinalSeparator
                End If
            End If
            xTemp = xTemp & vTemp(i)
        Next i
    End If
    Me.Memo.HeaderAdditionalText = xTemp
    Me.txtHeaderAdditionalText = xTemp
End Sub
Private Sub GetContacts(iDefSelection As ciSelectionLists)
    Dim xTo As String
    Dim xCC As String
    Dim xBCC As String
    Dim xFrom As String
    Static bCIActive As Boolean
    Dim oContacts As Object
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    If Not g_bShowAddressBooks Then Exit Sub
    If bCIActive Then Exit Sub
    bCIActive = True
    
    g_oMPO.GetMemoRecipients xTo, _
                             xFrom, _
                             xCC, _
                             xBCC, _
                             iDefSelection, _
                             oContacts
    
'   append new recips to existing recips
    If Len(xTo) Then
        If Len(Me.txtRecipients) Then
            Me.txtRecipients = _
                xTrimTrailingChrs(Me.txtRecipients, vbCrLf) & vbCrLf & xTo
        Else
            Me.txtRecipients = xTo
        End If
    End If
    
'   append new cc to existing cc
    If Len(xFrom) Then
        If Len(Me.txtFrom) Then
            Me.txtFrom = xTrimTrailingChrs(Me.txtFrom, vbCrLf) & vbCrLf & xFrom
        Else
            Me.txtFrom = xFrom
        End If
    End If
    
'   append new cc to existing cc
    If Len(xCC) Then
        If Len(Me.txtCC) Then
            Me.txtCC = xTrimTrailingChrs(Me.txtCC, vbCrLf) & vbCrLf & xCC
        Else
            Me.txtCC = xCC
        End If
    End If
    
'   append new bcc to existing bcc
    If Len(xBCC) Then
        If Len(Me.txtBCC) Then
            Me.txtBCC = _
                xTrimTrailingChrs(Me.txtBCC, vbCrLf) & vbCrLf & xBCC
        Else
            Me.txtBCC = xBCC
        End If
    End If
    
'   filter client/matter based on contacts retrieved
    If g_bCM_RunConnected And g_bCM_AllowFilter Then
        If Len(xTo) Then
            '---9.6.1
            Dim l_CIVersion As Long
            l_CIVersion = g_oMPO.Contacts.CIVersion
            
            If Me.cmbCM.AdditionalClientFilter <> "" Then
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter(oContacts)
                Else
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter2X(oContacts)
                End If
            Else
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = xGetClientMatterFilter(oContacts)
                Else
                    xSQL = xGetClientMatterFilter2X(oContacts)
                End If
            End If
            
            If xSQL <> "" Then
                Me.cmbCM.AdditionalClientFilter = xSQL
                Me.cmbCM.AdditionalMatterFilter = xSQL
                Me.cmbCM.Refresh
            End If
            '---end 9.6.1

        End If
    End If
    
    DoEvents
    
'   update document - as if focus had been lost
    Word.Application.ScreenUpdating = False
    txtRecipients_LostFocus
    DoEvents
    txtFrom_LostFocus
    DoEvents
    txtCC_LostFocus
    DoEvents
    txtBCC_LostFocus
    Word.Application.ScreenUpdating = True
    Me.SetFocus
    bCIActive = False
    Exit Sub
ProcError:
    bCIActive = False
    g_oError.Show Err
    Word.Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub btnAddressBooks_Click()
    GetContacts ciSelectionList_To
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    Me.Cancelled = True
    DoEvents
    g_oMPO.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err
    g_oMPO.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    Me.Cancelled = False
    'GLOG : 5598 : ceh
    If m_bUpdateDefaults Then
        With Me.Memo.Template
            .UpdateDefaults .DefaultAuthor.ID, Me.Memo
            m_bUpdateDefaults = False
        End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub chkIncludeDPhrasesInHeader_Click()
    On Error GoTo ProcError
    If Me.CascadeUpdates Then
        Application.ScreenUpdating = False
        Memo.IncludeDPhrasesInHeader = _
            0 - Me.chkIncludeDPhrasesInHeader
        Application.ScreenUpdating = True
        Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeDPhrasesInHeader_GotFocus()
    OnControlGotFocus Me.chkIncludeDPhrasesInHeader, "zzmpHeaderDeliveryPhrases"
End Sub
Private Sub chkIncludeDPhrases2InHeader_Click()
    On Error GoTo ProcError
    If Me.CascadeUpdates Then
        Application.ScreenUpdating = False
        Memo.IncludeDPhrases2InHeader = _
            0 - Me.chkIncludeDPhrases2InHeader
        Application.ScreenUpdating = True
        Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeDPhrases2InHeader_GotFocus()
    OnControlGotFocus Me.chkIncludeDPhrases2InHeader, "zzmpHeaderDeliveryPhrases2"
End Sub

Private Sub chkIncludeLetterheadTitle_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeTitle = _
                -(Me.chkIncludeLetterheadTitle)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadTitle, "zzmpLetterheadTitle"
End Sub

Private Sub chkIncludeLetterheadFax_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadFax, "zzmpLetterheadFax"
End Sub

Private Sub chkIncludeLetterheadFax_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeFax = _
                -(Me.chkIncludeLetterheadFax)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludePhone = _
                -(Me.chkIncludeLetterheadPhone)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadPhone, "zzmpLetterHeadPhone"
End Sub

Private Sub chkIncludeLetterheadEMail_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeEMail = _
                -(Me.chkIncludeLetterheadEMail)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadEMail_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadEMail, "zzmpLetterheadEMail"
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeAdmittedIn = _
                -(Me.chkIncludeLetterheadAdmittedIn)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadAdmittedIn, "zzmpLetterheadAdmittedIn"
End Sub

Private Sub chkIncludeLetterheadName_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeName = _
                -(Me.chkIncludeLetterheadName)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadName_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadName, "zzmpLetterheadName"
End Sub

Private Sub cmbAuthor_Change()
    AuthorIsDirty = True
End Sub

Private Sub cmbAuthor_Click()
    If b_Opened Then
        AuthorIsDirty = False
    End If
End Sub

Private Sub cmbAuthor_Close()
    b_Opened = False
End Sub

Private Sub cmbAuthor_LostFocus()
    Dim bCascade As Boolean

'   update for author only if itemchange event ran
    If m_bItemChanged Then

        With Me
'           update values in form
            bCascade = .CascadeUpdates
            .CascadeUpdates = False
        
'           update object with values of selected person
            If Not .Initializing Then
                .Memo.UpdateForAuthor
            End If
            
'           update "From" field
            Me.txtFrom = Me.Memo.From
        
'           show options in form
            .ShowOptions .Memo.Author.ID
            
            bResetLetterheadCombo .cmbLetterheadType, _
                                  .Memo.Letterhead
            
            .CascadeUpdates = bCascade
        End With
    
        m_bItemChanged = False

    End If
    
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Open()
    b_Opened = True
End Sub

Private Sub cmbAuthor_GotFocus()
    OnControlGotFocus Me.cmbAuthor, "zzmpFrom"
    DoEvents
End Sub

Private Sub cmbAuthor_ItemChange()
    Dim lID As Long
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If

    g_oMPO.ScreenUpdating = False
    
    With Memo
'       set new author
        lID = Me.cmbAuthor.BoundText
        .Author = g_oMPO.People(lID)
    End With

    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    
    m_bItemChanged = True
    Me.AuthorIsDirty = False
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Me.AuthorIsDirty = False
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 38 Or KeyCode = 40 Then
        AuthorIsDirty = True
    ElseIf KeyCode = wdKeyF10 And Shift = 1 Then
        ShowAuthorMenu
    End If
End Sub

Private Sub cmbAuthor_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
         AuthorIsDirty = False
        If Len(Me.cmbAuthor.BoundText) Then
            cmbAuthor_ItemChange
        End If
     End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        If Not Me.cmbAuthor.IsOpen Then
            ShowAuthorMenu
        End If
    End If
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If m_bAuthorDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCM_ConnectionFailed(ByVal Message As String)

    MsgBox IIf(Message = "", "Could not connect to the client matter data source.  ", Message & vbCr & vbCr) _
            & "Instead of selecting client matter numbers from a list," & _
            " you can manually type the numbers instead.", _
            vbExclamation, App.Title
        
    With Me.cmbCM
        .RunConnected = False
        .ComboType = ComboType_TextOnly
    End With
    
End Sub

Private Sub cmbCM_GotFocus()
    OnControlGotFocus Me.cmbCM, "zzmpClientMatterNumber"
End Sub

Private Sub cmbCM_LostFocus()
    On Error GoTo ProcError
    Application.ScreenUpdating = False '#4158
    With Me.Memo
        .ClientMatterNumber = Me.cmbCM.value
        If .ClientMatterNumber = Empty Then
            .ClientNumber = ""
            .ClientName = ""
            .MatterNumber = ""
            .MatterName = ""
        ElseIf cmbCM.RunConnected Then  '#4457
            If cmbCM.Validity = cmValidationStatus_Valid Then  '#4158
                .ClientNumber = Me.cmbCM.ClientData()(cmClientField_Number)
                .ClientName = Me.cmbCM.ClientData()(cmClientField_Name)
                .MatterNumber = Me.cmbCM.MatterData()(cmMatterField_Number)
                .MatterName = Me.cmbCM.MatterData()(cmMatterField_Name)
            End If
        ElseIf g_xCM_Separator <> "" Then
            ' Parse out Client and Matter Numbers using separator
            Dim i As Integer
            i = InStr(cmbCM.value, g_xCM_Separator)
            If i > 0 Then
                .ClientNumber = Left(cmbCM.value, i - 1)
                .MatterNumber = Mid(cmbCM.value, i + Len(g_xCM_Separator))
                .ClientName = ""
                .MatterName = ""
            Else
                .ClientNumber = ""
                .ClientName = ""
                .MatterNumber = ""
                .MatterName = ""
            End If
        Else
            .ClientNumber = ""
            .ClientName = ""
            .MatterNumber = ""
            .MatterName = ""
        End If
    End With ' end #4158
    'save SQL for reuse
    Memo.Document.SaveItem "CMFilterString", _
        "Memo", , Me.cmbCM.AdditionalClientFilter
    Application.ScreenUpdating = True '#4158
    Application.ScreenRefresh '#4158
    Exit Sub
ProcError:
    g_oError.Show Err
    Application.ScreenUpdating = True '#4158
    Exit Sub
End Sub

Private Sub cmbCM_Validate(Cancel As Boolean)
    Dim iIsValid As cmValidationStatus
    
    If g_bCM_AllowValidation Then
    
        If Me.cmbCM.RunConnected And Me.cmbCM.value <> Empty Then
            
            iIsValid = Me.cmbCM.Validity
            
            If iIsValid = cmValidationStatus_Invalid Then
                MsgBox "Invalid client matter number."
                Cancel = True
            End If
        End If
        
    End If
End Sub

Private Sub cmbDateType_GotFocus()
    OnControlGotFocus Me.cmbDateType, "zzmpDateType"
End Sub

Private Sub cmbDateType_LostFocus()
    On Error GoTo ProcError
    Memo.DateType = cmbDateType.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDateType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbDateType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbEnclosures_GotFocus()
    OnControlGotFocus Me.cmbEnclosures, "zzmpEnclosures"
End Sub

Private Sub cmbEnclosures_LostFocus()
    On Error GoTo ProcError
    Memo.Enclosures = cmbEnclosures.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_ItemChange()
    On Error GoTo ProcError
'*  enable/disable author details per LH type def
    With g_oMPO.db.LetterheadDefs().Item(Me.cmbLetterheadType.BoundText)
        Me.chkIncludeLetterheadEMail.Enabled = .AllowAuthorEMail
        Me.chkIncludeLetterheadName.Enabled = .AllowAuthorName
        Me.chkIncludeLetterheadPhone.Enabled = .AllowAuthorPhone
        Me.chkIncludeLetterheadTitle.Enabled = .AllowAuthorTitle
        Me.chkIncludeLetterheadFax.Enabled = .AllowAuthorFax
        Me.chkIncludeLetterheadAdmittedIn.Enabled = .AllowAuthorAdmittedIn
        '9.7.1 #4028
        Me.chkIncludeLetterheadCustom1.Enabled = .AllowCustomDetail1
        Me.chkIncludeLetterheadCustom2.Enabled = .AllowCustomDetail2
        Me.chkIncludeLetterheadCustom3.Enabled = .AllowCustomDetail3
        Me.lblInclude.Enabled = .AllowAuthorEMail Or .AllowAuthorName Or _
                                .AllowAuthorPhone Or .AllowAuthorFax Or _
                                .AllowAuthorTitle Or .AllowAuthorAdmittedIn Or _
                                .AllowCustomDetail1 Or .AllowCustomDetail2 Or _
                                .AllowCustomDetail3
        '---
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLetterheadType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLetterheadType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_BeforeItemAdded(xItem As String)
    On Error GoTo ProcError
    xItem = mpbase2.AppendToDPhrase(xItem)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_AddNew_Click()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.cmbAuthor.Columns("fldID")
    m_oMenu.AddPerson Me.cmbAuthor, lID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Favorite_Click()
    Dim lID As Long
    
    On Error Resume Next
    DoEvents
    lID = Me.cmbAuthor.BoundText
    On Error GoTo ProcError
    
    If lID Then
        m_oMenu.ToggleFavorite Me.cmbAuthor, lID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_ManageLists_Click()
'   get current selection
    
    Dim lID As Long
    
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    On Error GoTo ProcError
    
    If lID Then
        m_oMenu.ManageAuthors Me.cmbAuthor, lID
    Else
        m_oMenu.ManageAuthors Me.cmbAuthor
    End If

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    On Error GoTo ProcError
    With Memo.Template
        .DefaultAuthor = Me.Memo.Author
        'GLOG : 5598 : ceh
        If g_bUpdatePreferencesInDialog Then
            .UpdateDefaults .DefaultAuthor.ID, Me.Memo
             m_bUpdateDefaults = False
        Else
             m_bUpdateDefaults = True
        End If
    End With
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Copy_Click()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
    On Error GoTo ProcError
'   if author isn't in list, the
'   author is the reuse author
    'If Me.cmbAuthor.Row = -1 Then
    If TypeOf Me.Memo.Author Is CReUseAuthor Then '***4337
'       use reuse author as source
        Set oSource = Me.Memo.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oMPO.People(Me.cmbAuthor.BoundText)
    End If
    
'   copy the person
    Set oCopy = m_PersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_CopyOptions_Click()
    On Error GoTo ProcError
    m_oOptForm.CopyOptions Me.Memo.Author, _
                           Me.Memo.Template.ID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_EditAuthorOptions_Click()
    On Error GoTo ProcError
    DoEvents
    m_oOptForm.Show Me.Memo
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCopies_DeleteAll_Click()
    Beep
    Beep
End Sub

Private Sub txtTypistInitials_GotFocus()
    OnControlGotFocus Me.txtTypistInitials, "zzmpTypistInitials"
End Sub

Private Sub txtTypistInitials_LostFocus()
    On Error GoTo ProcError
    Memo.TypistInitials = txtTypistInitials.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_GotFocus()
    OnControlGotFocus Me.cmbLetterheadType
End Sub

Private Sub cmbLetterheadType_LostFocus()
'set letterhead as appropriate, if necessary
    Dim lType As Long
    Dim bRefresh As Boolean
    
    On Error GoTo ProcError
    Word.Application.ScreenUpdating = False
    
    With Memo.Letterhead
'       get selected letterhead type
        lType = Me.cmbLetterheadType.BoundText
        
'       compare with previous letterhead type
        If .LetterheadType <> lType Then
'           type is different - set
'           type to selected type
            .LetterheadType = lType

'           if dynamic editing is on, refresh the letterhead
            If g_oMPO.DynamicEditing Then
                .Refresh
            End If
        End If
    End With
    
    Word.Application.ScreenUpdating = True
    Exit Sub
ProcError:
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oOptForm_CurrentDocAuthorOptionsChange()
'the options of the author of the current Memo
'have changed - update dlg and doc to reflect changes
    On Error GoTo ProcError
    With Me.Memo
        .Author.Options(.Template.ID).Refresh
    End With
    cmbAuthor_ItemChange
    g_oMPO.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oOptForm_DefaultOptionsChange()
'options of the default author have changed -
'write these changes to the template
    On Error GoTo ProcError
    With Me.Memo
        .Author.Options(.Template.ID).Refresh
        'GLOG : 5598 : ceh
        If g_bUpdatePreferencesInDialog Then
            .Template.UpdateDefaults .DefaultAuthor.ID, Me.Memo
             m_bUpdateDefaults = False
        Else
             m_bUpdateDefaults = True
        End If

    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Form Event Procedures
'**********************************************************
Private Sub Form_Activate()
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    On Error GoTo ProcError
    If Me.Initializing Then
        SetInterfaceOptions Me, Me.Memo.Template.ID '9.7.1 - #4160
        m_xDefaultCCLabel = Me.lblCC.Caption '9.7.1 #4151
        m_xDefaultBCCLabel = Me.lblBCC.Caption '9.7.1 #4151
        PositionControls '9.7.1 - #4160
        Me.tabPages = 0
        With Memo
            If .Document.IsCreated Then
                UpdateForm
            Else
'               set author to default author
                Me.cmbAuthor.BoundText = .Author.ID
                Me.txtFrom = .Author.FullName
                DoEvents

'               get user.ini items & update document
                Me.txtTypistInitials = .TypistInitials
                txtTypistInitials_LostFocus
                
'               show options for author in dlg
                Me.ShowOptions .Author.ID
                                
                bResetLetterheadCombo Me.cmbLetterheadType, _
                                      .Letterhead
                
                Me.AuthorIsDirty = False
            End If
            Set .PreviousAuthor = .Author
        End With

        SendKeys "+"
        Me.Initializing = False
    End If
    
    g_oMPO.ForceItemUpdate = False
    DoEvents
    ActiveDocument.ActiveWindow.View.ShowHiddenText = False
    On Error Resume Next
    ActiveWindow.PageScroll , 1
    EchoOn
    Word.Application.ScreenUpdating = True
    
    cmbAuthor.SetFocus
    Screen.MousePointer = vbDefault
    Application.ScreenRefresh
'   move dialog to last position
    'MoveToLastPosition Me
    Exit Sub
    
ProcError:
    EchoOn
    Word.Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    g_oError.Show Err, "Error Activating Memo Form"
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF2 Then
        GetContacts ciSelectionList_To
    Else
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim bShow As Boolean
    
    On Error GoTo ProcError
    g_oMPO.ScreenUpdating = False

    Me.Initializing = True
    
    Me.Cancelled = True
    
    Me.Top = 20000
    
    Set m_PersonsForm = New MPO.CPersonsForm
    Set m_oContacts = New MPO.CContacts
    Set m_oOptForm = New MPO.COptionsForm
    Set m_oMenu = New MacPac90.CAuthorMenu
    
    If Me.Memo Is Nothing Then
        Me.Memo = g_oMPO.NewMemo
    End If
    
    With Me.Memo
        With .Document
            .StatusBar = "Initializing.  Please wait..."
            
            If .File.ProtectionType <> wdNoProtection Then
                   .File.Unprotect
            End If
        End With
        
'***    get lists
        '9.7.1 #4024
        With g_oMPO.Lists
            If Me.Memo.SplitDeliveryPhrases Then
                Me.mlcDeliveryPhrase.List = .Item("DPhrases1").ListItems.Source
                Me.mlcDeliveryPhrase2.List = .Item("DPhrases2").ListItems.Source
                Me.lblDeliveryPhrase2.Visible = True
                Me.mlcDeliveryPhrase2.Visible = True
                Me.chkIncludeDPhrases2InHeader.Visible = True
            Else
                Me.mlcDeliveryPhrase.List = .Item("DPhrases").ListItems.Source
                Me.lblDeliveryPhrase2.Visible = False
                Me.mlcDeliveryPhrase2.Visible = False
                Me.chkIncludeDPhrases2InHeader.Visible = False
            End If
            Me.cmbEnclosures.Array = .Item("Enclosures").ListItems.Source
            Me.cmbDateType.Array = .Item("DateFormats").ListItems.Source
        End With
    
'---    load different LH types -- see note above
        Me.cmbLetterheadType.Array = _
            g_oMPO.db.LetterheadDefs(.Template.ID).ListSource
    
'       set dialog caption to short name
        Me.Caption = "Create a " & .Template.ShortName
    End With

'***resize contorls
    ResizeTDBCombo Me.cmbLetterheadType, 6
    ResizeTDBCombo Me.cmbEnclosures, 4
    ResizeTDBCombo Me.cmbDateType, 4

    g_oMPO.People.Refresh

'***get authors list
    With Me.cmbAuthor
        .Array = g_oMPO.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
        DoEvents
    End With

'***show memo sig?
    bShow = xGetTemplateConfigVal(Me.Memo.Template.ID, "ShowDlgMemoSig")
    
    Me.lblEnclosures.Visible = bShow
    Me.cmbEnclosures.Visible = bShow
    Me.lblTypistInitials.Visible = bShow
    Me.txtTypistInitials.Visible = bShow
    
    If bShow Then
        Me.tabPages.Caption = mpExtrasCaptions
    Else
        Me.tabPages.Caption = mpPhrasesCaptions
    End If
    
'   show page 2 header controls?
    bShow = Empty
    bShow = xGetTemplateConfigVal(Me.Memo.Template.ID, "ShowDlgPage2Header")
    
    Me.lblPage2HeaderText.Visible = bShow
    Me.txtHeaderAdditionalText.Visible = bShow
    If Not bShow Then
        Me.chkIncludeDPhrasesInHeader.Top = Me.chkIncludeDPhrasesInHeader.Top - 1000
    End If
    
'   update client\matter label
    Me.lblClientMatter.Caption = _
        xGetTemplateConfigVal(Me.Memo.Template.ID, "ClientMatterDBoxCaption")

'   set Client Matter Control properties
    With Me.cmbCM
        .RunConnectedToolTip = g_xCM_RunConnectedTooltip
        .RunDisconnectedToolTip = g_xCM_RunDisconnectedTooltip
        .RunConnected = g_bCM_RunConnected
        If Not .RunConnected Then
            .ComboType = ComboType_TextOnly
        Else
            .SourceClass = g_xCM_Backend
            If g_bCM_SearchByMatter Then .SearchMode = 1 '***9.7.1 - #4186
            .AllowUserToClearFilters = g_bCM_AllowUserToClearFilters
            .ShowLoadMessage = g_bCM_ShowLoadMsg
            .Column1Heading = g_xCM_Column1Heading
            .Column2Heading = g_xCM_Column2Heading
            .Separator = g_xCM_Separator
        End If
    End With

    '9.7.1 #4028
    If g_xLHCustom1Caption <> "" Then
        Me.chkIncludeLetterheadCustom1.Caption = g_xLHCustom1Caption
        Me.chkIncludeLetterheadCustom1.Visible = True
    Else
        Me.chkIncludeLetterheadCustom1.Visible = False
    End If
    If g_xLHCustom2Caption <> "" Then
        Me.chkIncludeLetterheadCustom2.Caption = g_xLHCustom2Caption
        Me.chkIncludeLetterheadCustom2.Visible = True
    Else
        Me.chkIncludeLetterheadCustom2.Visible = False
    End If
    If g_xLHCustom3Caption <> "" Then
        Me.chkIncludeLetterheadCustom3.Caption = g_xLHCustom3Caption
        Me.chkIncludeLetterheadCustom3.Visible = True
    Else
        Me.chkIncludeLetterheadCustom3.Visible = False
    End If
    '---

'   ensure date update
    g_oMPO.ForceItemUpdate = True
    Me.Memo.DateType = Me.Memo.DateType
    g_oMPO.ForceItemUpdate = False

'   show AddressBooks button
    Me.btnAddressBooks.Visible = g_bShowAddressBooks

    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = False
    Me.Initializing = True
    g_oError.Show Err, "Error Loading Memo Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'SetLastPosition Me
    
    Set m_PersonsForm = Nothing
    Set m_oContacts = Nothing
    Set m_Memo = Nothing
    Set m_oMenu = Nothing
    Set m_oOptForm = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Unloading Memo Form"
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_GotFocus()
    OnControlGotFocus Me.mlcDeliveryPhrase, "zzmpDeliveryPhrases"
End Sub

Private Sub mlcDeliveryPhrase_LostFocus()
    On Error GoTo ProcError
    Memo.DeliveryPhrases = Me.mlcDeliveryPhrase.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub mlcDeliveryPhrase2_GotFocus() '9.7.1 #4024
    OnControlGotFocus Me.mlcDeliveryPhrase2, "zzmpDeliveryPhrases2"
End Sub

Private Sub mlcDeliveryPhrase2_LostFocus() '9.7.1 #4024
    On Error GoTo ProcError
    Memo.DeliveryPhrases2 = Me.mlcDeliveryPhrase2.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub tabPages_Click()
    On Error GoTo ProcError
     Select Case Me.tabPages.CurrTab
        Case 0
            Me.cmbAuthor.SetFocus
        Case 1
            SelectFirstAvailableControl Me, Me.mlcDeliveryPhrase.TabIndex, Me.cmbEnclosures.TabIndex '9.7.1 - #4160
        Case 2
            SelectFirstAvailableControl Me, Me.cmbLetterheadType.TabIndex, Me.chkIncludeDPhrases2InHeader.TabIndex '9.7.1 - #4160
    End Select
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_GotFocus()
    OnControlGotFocus Me.txtBCC, "zzmpBCC"
End Sub

Private Sub txtBCC_LostFocus()
    On Error GoTo ProcError
    Memo.BCC = txtBCC
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_Change()
    On Error GoTo ProcError
    UpdateLabelCaption txtBCC, lblBCC, m_xDefaultBCCLabel, "copy", "copies" '9.7.1 #4151
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub UpdateLabelCaption(txtB As TextBox, _
                               lblL As Label, _
                               xCaptionRoot As String, _
                               Optional xRecip As String = "recipient", _
                               Optional xRecipPlural As String = "recipients")
    Dim iNumEntries As Integer
    Dim iNumEntriesComma As Integer
    On Error GoTo ProcError
    If txtB.Text <> "" Then
        iNumEntries = lCountChrs(txtB.Text, vbCrLf) + 1
        iNumEntriesComma = lCountChrs(txtB.Text, ",") + 1
        iNumEntries = CInt(Max(CDbl(iNumEntries), CDbl(iNumEntriesComma)))
        If iNumEntries = 1 Then
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (1 " & xRecip & ")"
        Else
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (" & iNumEntries & " " & xRecipPlural & ")"
        End If
    Else
        lblL = " " & xCaptionRoot
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmMemo.UpdateLabelCaption"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub txtCC_Change()
    On Error GoTo ProcError
    UpdateLabelCaption txtCC, lblCC, m_xDefaultCCLabel, "copy", "copies"  '9.7.1 #4151
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCC_GotFocus()
    OnControlGotFocus Me.txtCC, "zzmpCC"
End Sub

Private Sub txtCC_LostFocus()
    On Error GoTo ProcError
    Memo.CC = txtCC.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFrom_Change()
    On Error GoTo ProcError
    UpdateLabelCaption Me.txtFrom, Me.lblFrom, "Fr&om:", "author", "authors"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFrom_GotFocus()
    OnControlGotFocus Me.txtFrom, "zzmpFrom"
End Sub

Private Sub txtFrom_LostFocus()
    On Error GoTo ProcError
    Memo.From = Me.txtFrom
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'SAVE!!!Private Sub txtFrom_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'    If Button = 2 Then
''---fool windows into disabling default popup (necessary for textboxes)
'        LockWindowUpdate txtFrom.hwnd
'        Me.btnAddressBooks.SetFocus
'        Set m_Ctl = txtFrom
'        txtFrom.Enabled = False
'        DoEvents
'        Me.PopupMenu mnuRecipients
'        txtFrom.Enabled = True
'        LockWindowUpdate 0&
'        txtFrom.SetFocus
'        Set m_Ctl = Nothing
'    End If
'End Sub

Private Sub txtReline_GotFocus()
    OnControlGotFocus Me.txtReline, "zzmpReline"
End Sub

Private Sub txtReline_LostFocus()
    On Error GoTo ProcError
    Memo.Reline = Me.txtReline
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtHeaderAdditionalText_GotFocus()
    OnControlGotFocus Me.txtHeaderAdditionalText, "zzmpHeaderAdditionalText"
End Sub

Private Sub txtHeaderAdditionalText_LostFocus()
    On Error GoTo ProcError
    Memo.HeaderAdditionalText = Me.txtHeaderAdditionalText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtRecipients_Change()
    On Error GoTo ProcError
    UpdateLabelCaption txtRecipients, Me.lblRecipients, "&To:"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtRecipients_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_To
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCC_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_CC
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFrom_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_From
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_BCC
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtRecipients_GotFocus()
    OnControlGotFocus Me.txtRecipients, "zzmpRecipients"
    DoEvents
End Sub

Private Sub txtRecipients_LostFocus()
    On Error GoTo ProcError
    Memo.Recipients = Me.txtRecipients
    UpdateHeaderText '9.7.1 #4162
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
'**********************************************************
'   Methods
'**********************************************************
Public Sub UpdateForm()
    On Error GoTo ProcError
    '---get properties, load controls on reuse
    With Memo
        Me.cmbAuthor.Text = .Document.ReuseAuthor.DisplayName
        Me.txtRecipients = .Recipients
        Me.txtFrom = .From
        Me.txtCC = .CC
        Me.txtBCC = .BCC
        With Me.cmbCM
            If g_bCM_AllowFilter Then  '#3818 - 9.6.2
                'set up list
                .AdditionalClientFilter = Me.Memo.Document.GetVar("Memo_1_CMFilterString")
                .AdditionalMatterFilter = Me.Memo.Document.GetVar("Memo_1_CMFilterString")
            End If
            .value = Me.Memo.ClientMatterNumber
        End With
        Me.txtReline = .Reline
        Me.mlcDeliveryPhrase.Text = .DeliveryPhrases
        If .SplitDeliveryPhrases Then '9.7.1 #4024
            Me.mlcDeliveryPhrase2.Text = .DeliveryPhrases2
            Me.chkIncludeDPhrases2InHeader = Abs(.IncludeDPhrases2InHeader)
        End If
        Me.txtTypistInitials.Text = .TypistInitials
        Me.cmbEnclosures.BoundText = .Enclosures
        Me.txtHeaderAdditionalText = .HeaderAdditionalText
        Me.cmbDateType.BoundText = .DateType
        If .IncludeDPhrasesInHeader <> mpUndefined Then _
            Me.chkIncludeDPhrasesInHeader = Abs(.IncludeDPhrasesInHeader)
    
        Me.txtCustom1 = .Custom1 '9.7.1 - #4268
        Me.txtCustom2 = .Custom2 '9.7.1 - #4268
        Me.txtCustom3 = .Custom3 '9.7.1 - #4268
        
        With .Letterhead
            Me.cmbLetterheadType.BoundText = .LetterheadType
            If .IncludeEMail <> mpUndefined Then _
                Me.chkIncludeLetterheadEMail = Abs(.IncludeEMail)
            If .IncludeName <> mpUndefined Then _
                Me.chkIncludeLetterheadName = Abs(.IncludeName)
            If .IncludePhone <> mpUndefined Then _
                Me.chkIncludeLetterheadPhone = Abs(.IncludePhone)
            If .IncludeFax <> mpUndefined Then _
                Me.chkIncludeLetterheadFax = Abs(.IncludeFax)
            If .IncludeTitle <> mpUndefined Then _
                Me.chkIncludeLetterheadTitle = Abs(.IncludeTitle)
            If .IncludeAdmittedIn <> mpUndefined Then _
                Me.chkIncludeLetterheadAdmittedIn = Abs(.IncludeAdmittedIn)
            '9.7.1 #4028
            If .IncludeCustom1Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom1 = Abs(.IncludeCustom1Detail)
            If .IncludeCustom2Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom2 = Abs(.IncludeCustom2Detail)
            If .IncludeCustom3Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom3 = Abs(.IncludeCustom3Detail)
            '---
        End With
    
    End With
    Me.AuthorIsDirty = False
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmMemo.UpdateForm"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub ShowAuthorMenu()
    Dim bEnabled As Boolean
    On Error GoTo ProcError
    
'   enable specified menu items only if author
'   is currently in the db
    If TypeOf Memo.Author Is CReUseAuthor Then
        bEnabled = False
    Else
        bEnabled = True
    End If
    
    Me.mnuAuthor_EditAuthorOptions.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Enabled = bEnabled
    Me.mnuAuthor_SetDefault.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Checked = Me.Memo.Author.Favorite
    Me.PopupMenu mnuAuthor
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Sub ShowOptions(lID As Long)
    Dim bCascade As Boolean
    
    On Error GoTo ProcError
'   get original state of property
    bCascade = Me.CascadeUpdates
    
'   ensure no cascading
    Me.CascadeUpdates = False
    
'   show options
    mdlDialog.ShowOptions Me.Memo.Author, Me.Memo.Template.ID, Me
    
'   return to original state
    Me.CascadeUpdates = bCascade
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmMemo.ShowOptions"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub
Private Sub txtCustom1_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom1, "zzmpCustom1"
End Sub

Private Sub txtCustom1_LostFocus() '9.7.1 - #4268
    Me.Memo.Custom1 = Me.txtCustom1
End Sub
Private Sub txtCustom2_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom2, "zzmpCustom2"
End Sub

Private Sub txtCustom2_LostFocus() '9.7.1 - #4268
    Me.Memo.Custom2 = Me.txtCustom2
End Sub

Private Sub PositionControls() '9.7.1 - #4160
    Dim sIncrement As Single
    Dim sOffset As Single
    Dim sAdjustIncrement As Single
    Dim i As Integer
    Dim oCtl As VB.Control
    Dim arrCtls() As String
    Dim bVisible As Boolean
    Dim iCurrTab As Integer

    On Error GoTo ProcError
    HideUnusedLetterheadControls Me, Me.Memo.Template.ID '9.7.1 #4158
    
    ReDim arrCtls(Me.Controls.Count)
'   place each control in tab order sort
    On Error Resume Next
    For Each oCtl In Me.Controls
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next
    On Error GoTo ProcError
    sIncrement = 88
    For i = txtRecipients.TabIndex To txtCustom1.TabIndex ' adjust if controls added on first tab after Custom2
        ' See which controls are visible to set correct spacing
        Set oCtl = Me.Controls(arrCtls(i))
        
'       do only if visible
        If Not oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.CheckBox Or _
                TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                TypeOf oCtl Is mpSal.SalutationCombo) Then
'               position control relative to previous control
                sIncrement = sIncrement + 60
            ElseIf (TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is mpControls4.RelineControl) Then
                sIncrement = sIncrement + ((oCtl.Height / 315) * 60)
            ElseIf (TypeOf oCtl Is mpControls.MultiLineCombo) Then
                sIncrement = sIncrement + 120
            End If
            
        End If
    Next i
    If sIncrement > 250 Then sIncrement = 250
    iCurrTab = 0
    bVisible = True
    sOffset = Me.cmbAuthor.Top + Me.cmbAuthor.Height + sIncrement
    For i = txtRecipients.TabIndex To txtCustom3.TabIndex ' Adjust if controls added to last tab after Include DPhrases
        Set oCtl = Me.Controls(arrCtls(i))
        If Not TypeOf oCtl Is VB.Frame Then
            If i > Me.txtCustom1.TabIndex And iCurrTab = 0 Then
                iCurrTab = 1
                sIncrement = 250
                sOffset = 200
                bVisible = False
            ElseIf i > Me.txtCustom2.TabIndex And iCurrTab = 1 Then
                If Not bVisible Then
                    ' All controls on tab are hidden
                    Me.tabPages.TabVisible(iCurrTab) = False
                End If
                iCurrTab = 2
                sIncrement = 250
                sOffset = 200
                bVisible = False
            End If
            
    '       do only if visible
            If oCtl.Visible Then
                bVisible = True
                If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                    TypeOf oCtl Is VB.TextBox Or _
                    TypeOf oCtl Is VB.CheckBox Or _
                    TypeOf oCtl Is mpControls.MultiLineCombo Or _
                    TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                    TypeOf oCtl Is mpControls4.RelineControl Or _
                    TypeOf oCtl Is mpSal.SalutationCombo) Then
                    
                    If TypeOf oCtl Is mpControls.MultiLineCombo Then
                        oCtl.Top = sOffset
                        sOffset = sOffset + (Me.cmbAuthor.Height * 2.5) + sIncrement
                    ElseIf TypeOf oCtl Is MPCM.ClientMatterComboBox Then
                        oCtl.Top = sOffset
                        sOffset = sOffset + Me.cmbAuthor.Height + sIncrement
                    ElseIf TypeOf oCtl Is mpControls4.RelineControl Then
                        oCtl.Top = sOffset
                        sOffset = sOffset + (oCtl.Height - 300) + sIncrement
                    ElseIf TypeOf oCtl Is VB.CheckBox Then
    '               position control relative to previous control
                        If InStr(UCase(oCtl.Name), "CHKINCLUDELETTERHEAD") = 0 Then
                            oCtl.Top = sOffset
                            sOffset = oCtl.Top + oCtl.Height + sIncrement
                        End If
                    Else
                        oCtl.Top = sOffset
                        sOffset = sOffset + oCtl.Height + sIncrement
                        If UCase(oCtl.Name) = "CMBLETTERHEADTYPE" Then
                            sOffset = sOffset - 50
                            sOffset = PositionLetterheadControls(Me, i + 1, sOffset, sIncrement)
                        End If
                    End If
    '               set accompanying label vertical position
                    If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                        Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                    End If
                    
                End If
            End If
        End If
    Next i
    If Not bVisible Then
        ' All controls on tab are hidden
        Me.tabPages.TabVisible(iCurrTab) = False
    End If
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmMemo.PositionControls"
End Sub
Private Sub chkIncludeLetterheadCustom1_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeCustom1Detail = _
                -(Me.chkIncludeLetterheadCustom1)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom1_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom1, "zzmpLetterheadCustom1Detail"
End Sub
Private Sub chkIncludeLetterheadCustom2_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeCustom2Detail = _
                -(Me.chkIncludeLetterheadCustom2)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom2_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom2, "zzmpLetterheadCustom2Detail"
End Sub
Private Sub chkIncludeLetterheadCustom3_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Memo.Letterhead.IncludeCustom3Detail = _
                -(Me.chkIncludeLetterheadCustom3)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom3_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom3, "zzmpLetterheadCustom3Detail"
End Sub


