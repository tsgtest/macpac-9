VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmLetterhead 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create Letterhead"
   ClientHeight    =   3312
   ClientLeft      =   1908
   ClientTop       =   1308
   ClientWidth     =   5520
   Icon            =   "frmLetterhead.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3312
   ScaleWidth      =   5520
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkIncludeLetterheadCustom1 
      Appearance      =   0  'Flat
      Caption         =   "Custom 1"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   1800
      TabIndex        =   13
      Top             =   2340
      Width           =   1155
   End
   Begin VB.CheckBox chkIncludeLetterheadCustom2 
      Appearance      =   0  'Flat
      Caption         =   "Custom 2"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   2985
      TabIndex        =   14
      Top             =   2340
      Width           =   1155
   End
   Begin VB.CheckBox chkIncludeLetterheadCustom3 
      Appearance      =   0  'Flat
      Caption         =   "Custom 3"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   4170
      TabIndex        =   15
      Top             =   2340
      Width           =   1155
   End
   Begin VB.CheckBox chkIncludeLetterheadAdmittedIn 
      Appearance      =   0  'Flat
      Caption         =   "Admi&tted In"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   4170
      TabIndex        =   12
      Top             =   2010
      Width           =   1155
   End
   Begin VB.CheckBox chkIncludeLetterheadFax 
      Appearance      =   0  'Flat
      Caption         =   "Fa&x"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   2985
      TabIndex        =   11
      Top             =   2010
      Width           =   1155
   End
   Begin VB.CheckBox chkIncludeLetterheadTitle 
      Appearance      =   0  'Flat
      Caption         =   "T&itle"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   1800
      TabIndex        =   10
      Top             =   2010
      Width           =   1155
   End
   Begin TrueDBList60.TDBCombo cmbLetterheadType 
      Height          =   555
      Left            =   1830
      OleObjectBlob   =   "frmLetterhead.frx":058A
      TabIndex        =   5
      Top             =   1155
      Width           =   3585
   End
   Begin VB.CheckBox chkIncludeLetterheadName 
      Appearance      =   0  'Flat
      Caption         =   "&Name"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   1800
      TabIndex        =   7
      Top             =   1680
      Width           =   1155
   End
   Begin VB.CheckBox chkIncludeLetterheadPhone 
      Appearance      =   0  'Flat
      Caption         =   "P&hone"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   2985
      TabIndex        =   8
      Top             =   1680
      Width           =   1155
   End
   Begin VB.CheckBox chkIncludeLetterheadEMail 
      Appearance      =   0  'Flat
      Caption         =   "&E-Mail"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   4170
      TabIndex        =   9
      Top             =   1680
      Width           =   1155
   End
   Begin VB.TextBox txtTitle 
      Appearance      =   0  'Flat
      Height          =   300
      Left            =   1815
      MultiLine       =   -1  'True
      TabIndex        =   20
      Top             =   7800
      Visible         =   0   'False
      Width           =   3585
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4350
      TabIndex        =   17
      Top             =   2715
      Width           =   1000
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3225
      TabIndex        =   16
      Top             =   2715
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbAuthor 
      Height          =   375
      Left            =   1830
      OleObjectBlob   =   "frmLetterhead.frx":2C33
      TabIndex        =   1
      Top             =   240
      Width           =   3585
   End
   Begin TrueDBList60.TDBCombo cmbAuthor2 
      Height          =   585
      Left            =   1830
      OleObjectBlob   =   "frmLetterhead.frx":6B60
      TabIndex        =   3
      Top             =   690
      Width           =   3585
   End
   Begin VB.Label lblAuthor2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&2nd Author:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   2
      Top             =   765
      Width           =   1275
   End
   Begin VB.Label lblInclude 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Include Author:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   6
      Top             =   1695
      Width           =   1275
   End
   Begin VB.Label lblAuthor 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Author:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   0
      Top             =   315
      Width           =   1275
   End
   Begin VB.Label lblClientMatter 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Client-Matter No.:"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   7455
      TabIndex        =   19
      Top             =   2940
      Width           =   1455
   End
   Begin VB.Label lblDPhrases 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   " &Delivery Phrases:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   6945
      TabIndex        =   18
      Top             =   1815
      Width           =   1320
   End
   Begin VB.Line Line6 
      BorderColor     =   &H00E0E0E0&
      Index           =   2
      X1              =   7635
      X2              =   13155
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Label lblType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Letterhead T&ype:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   300
      TabIndex        =   4
      Top             =   1200
      Width           =   1275
   End
   Begin VB.Label lblTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Title"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   90
      TabIndex        =   21
      Top             =   7830
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      FillColor       =   &H00800080&
      Height          =   9495
      Left            =   -120
      Top             =   0
      Width           =   1800
   End
   Begin VB.Menu mnuAuthor2 
      Caption         =   "Author 2"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor2_Clear 
         Caption         =   "&Clear"
      End
   End
End
Attribute VB_Name = "frmLetterhead"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private WithEvents m_oLH As MPO.CLetterhead
Attribute m_oLH.VB_VarHelpID = -1
Private m_oPersonsForm As MPO.CPersonsForm
Private m_oOptions As MPO.COptionsForm
Private m_oMenu As MacPac90.CAuthorMenu

Private m_bAuthorDirty As Boolean
Private b_Opened As Boolean
Private m_bAuthorManual As Boolean
Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private xMsg As String
Private m_lSectionIndex As Long
'---9.7.1 - 4147
Private m_bAuthor2Dirty As Boolean
Private m_bOpened2 As Boolean

Public Property Get SectionIndex() As Long
    SectionIndex = m_lSectionIndex
End Property

Public Property Let SectionIndex(lNew As Long)
    m_lSectionIndex = lNew
End Property

Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property

Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
    Me.CascadeUpdates = False
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = True
End Property

Public Property Let bAuthorIsDirty(bNew As Boolean)
    m_bAuthorDirty = bNew
End Property

Public Property Get bAuthorIsDirty() As Boolean
    bAuthorIsDirty = m_bAuthorDirty
End Property

Public Property Let AuthorIsManual(bNew As Boolean)
    m_bAuthorManual = bNew
End Property

Public Property Get AuthorIsManual() As Boolean
    AuthorIsManual = m_bAuthorManual
End Property

Public Sub ShowOptions(lID As Long)
    On Error GoTo ProcError
    Dim bCascade As Boolean
    Dim xTemplate As String '9.7.1 #4153
    
'   get original state of property
    bCascade = Me.CascadeUpdates
    
'   ensure no cascading
    Me.CascadeUpdates = False
    
    '9.7.l #4153
    ' Use letter options if current template doesn't have options
    If m_oLH.Author.Options(m_oLH.Template.ID).Count = 0 Then
        xTemplate = g_oMPO.Templates.ItemFromClass("CLetter").ID
    Else
        xTemplate = m_oLH.Template.ID
    End If
'   show options
    mdlDialog.ShowOptions m_oLH.Author, _
                          xTemplate, _
                          Me
    If Me.cmbLetterheadType.BoundText = "" Then
        Me.cmbLetterheadType.Bookmark = 0
    End If
    '---
    DoEvents
    
'   return to original state
    Me.CascadeUpdates = bCascade
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLetterhead.ShowOptions"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub cmbAuthor_Change()
    bAuthorIsDirty = True
End Sub

Private Sub cmbAuthor_Click()
    If b_Opened Then
        bAuthorIsDirty = False
    End If
End Sub

Private Sub cmbAuthor_Close()
    b_Opened = False
End Sub

Private Sub cmbAuthor_GotFocus()
    DoEvents
    OnControlGotFocus Me.cmbAuthor, , True
End Sub

Private Sub cmbAuthor_ItemChange()
    Dim lID As Long
    
    On Error GoTo ProcError
    
    If Me.Initializing Then
        Exit Sub
    End If
    
'   write to object
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    m_oLH.Author = g_oMPO.People.Item(lID)
    m_oLH.UpdateForAuthor
    Me.ShowOptions lID
    Me.Refresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 38 Or KeyCode = 40 Then
        bAuthorIsDirty = True
    End If
End Sub

Private Sub cmbAuthor_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
        bAuthorIsDirty = False
        If Len(Me.cmbAuthor.BoundText) Then
            cmbAuthor_ItemChange
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Open()
    b_Opened = True
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If bAuthorIsDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        Me.PopupMenu mnuAuthor2
    End If

End Sub
Private Sub mnuAuthor2_Clear_Click()
    Me.cmbAuthor2.Bookmark = 0
End Sub
Private Sub cmbAuthor2_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor2, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor2_Open()
    m_bOpened2 = True
End Sub

Private Sub cmbAuthor2_Change()
    m_bAuthor2Dirty = True
End Sub

Private Sub cmbAuthor2_Click()
    If m_bOpened2 Then
        m_bAuthor2Dirty = False
    End If
End Sub

Private Sub cmbAuthor2_Close()
    m_bOpened2 = False
End Sub

Private Sub cmbAuthor2_GotFocus()
    DoEvents
    OnControlGotFocus Me.cmbAuthor2
End Sub
Private Sub cmbAuthor2_ItemChange()
    Dim lID As Long
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If
    If cmbAuthor2.BoundText > 0 Then
        m_oLH.Author2 = g_oMPO.People(cmbAuthor2.BoundText)
    Else
        m_oLH.Author2 = Nothing
    End If
    m_bAuthor2Dirty = False
    Me.Refresh
    DoEvents
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    m_bAuthor2Dirty = False
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbAuthor2_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If Me.cmbAuthor2.Text = Me.cmbAuthor.Text Then
        MsgBox "Second author must be different.", vbExclamation, App.Title
        Cancel = True
    ElseIf m_bAuthor2Dirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor2)
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor2_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
         m_bAuthor2Dirty = False
        If Len(Me.cmbAuthor2.BoundText) Then
            cmbAuthor2_ItemChange
        End If
     End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbAuthor2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthor2Dirty = True
    End If
End Sub

Private Sub cmbLetterheadType_GotFocus()
    OnControlGotFocus Me.cmbLetterheadType
End Sub

Private Sub cmbLetterheadType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLetterheadType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLetterheadType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_Click()
    On Error GoTo ProcError
    m_oLH.IncludePhone = -(Me.chkIncludeLetterheadPhone)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadPhone, "zzmpLetterheadPhone"
End Sub


Private Sub chkIncludeLetterheadEMail_Click()
    On Error GoTo ProcError
    m_oLH.IncludeEMail = -(Me.chkIncludeLetterheadEMail)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadEMail_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadEMail, "zzmpLetterheadEMail"
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_Click()
    On Error GoTo ProcError
    m_oLH.IncludeAdmittedIn = -(Me.chkIncludeLetterheadAdmittedIn)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadAdmittedIn, "zzmpLetterheadAdmittedIn"
End Sub
Private Sub chkIncludeLetterheadCustom1_Click() '9.7.1 #4028
    On Error GoTo ProcError
    m_oLH.IncludeCustom1Detail = -(Me.chkIncludeLetterheadCustom1)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom1_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom1, "zzmpLetterheadCustom1Detail"
End Sub
Private Sub chkIncludeLetterheadCustom2_Click() '9.7.1 #4028
    On Error GoTo ProcError
    m_oLH.IncludeCustom2Detail = -(Me.chkIncludeLetterheadCustom2)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom2_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom2, "zzmpLetterheadCustom2Detail"
End Sub
Private Sub chkIncludeLetterheadCustom3_Click() '9.7.1 #4028
    On Error GoTo ProcError
    m_oLH.IncludeCustom3Detail = -(Me.chkIncludeLetterheadCustom3)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom3_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom3, "zzmpLetterheadCustom3Detail"
End Sub

Private Sub chkIncludeLetterheadName_Click()
    On Error GoTo ProcError
    m_oLH.IncludeName = -(Me.chkIncludeLetterheadName)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadName_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadName, "zzmpLetterheadName"
End Sub

Private Sub chkIncludeLetterheadTitle_Click()
    On Error GoTo ProcError
    m_oLH.IncludeTitle = -(Me.chkIncludeLetterheadTitle)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadTitle, "zzmpLetterheadTitle"
End Sub

Private Sub chkIncludeLetterheadFax_Click()
    On Error GoTo ProcError
    m_oLH.IncludeFax = -(Me.chkIncludeLetterheadFax)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadFax_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadFax, "zzmpLetterheadFax"
End Sub

Private Sub cmbLetterheadType_ItemChange()
    On Error GoTo ProcError
    DisplayOptionalControls Me.cmbLetterheadType.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    Application.ScreenUpdating = False
    With m_oLH
        If Len(Me.cmbAuthor.BoundText) <> 0 And .Document.HasReuseAuthor Then   '9.7.1 - #3827
            bPromptForSignatureChange .Template
        End If
        EchoOffDesktop
        .Section = Me.SectionIndex
        .LetterheadType = Me.cmbLetterheadType.BoundText
        .Finish True
        EchoOn
    End With
    Application.ScreenUpdating = True
    Unload Me
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
    Unload Me
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Form Event Procedures
'**********************************************************
Private Sub Form_Activate()
    Dim lID As Long
    Dim iSource As Integer
    Dim datStart As Date
    
    On Error GoTo ProcError
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    If Me.Initializing Then
        PositionControls '9.7.1 #4028
        On Error Resume Next
        With m_oLH
'            If .Document.IsCreated Then
            If .Document.HasReuseAuthor Then    '9.7.1 - #3827
                UpdateForm
            Else
                Me.cmbAuthor.BoundText = .Author.ID
                Me.ShowOptions .Author.ID
                DoEvents
                Me.bAuthorIsDirty = False
                Me.cmbAuthor2.Bookmark = 0
            End If
        
            With .Document.File
                .ActiveWindow.View.ShowHiddenText = False
                DoEvents
            
                If .ProtectionType <> wdNoProtection Then
                    .Unprotect
                End If
            End With
        End With

        SendKeys "+"
        Me.Initializing = False
        'MoveToLastPosition Me '9.7.1 #4028
        g_oMPO.ScreenUpdating = True
    '    Me.Timer1.Enabled = True
        Me.cmbAuthor.SetFocus
    End If
    Exit Sub
ProcError:
        Me.Initializing = False
        g_oMPO.ScreenUpdating = True
    g_oError.Show Err, "Error Activating Letterhead Form"
    Exit Sub
End Sub

Private Sub Form_Load()
    
    On Error GoTo ProcError
    Me.Top = 20000 '9.7.1 #4028
    Me.Initializing = True
    
    Set m_oLH = g_oMPO.NewLetterhead
    Set m_oPersonsForm = New MPO.CPersonsForm
    Set m_oOptions = New MPO.COptionsForm
    
    With m_oLH
        '9.7.1 #4153
        Dim oTemplate As MPO.CTemplate
        On Error Resume Next
        Set oTemplate = g_oMPO.Templates(Word.ActiveDocument.AttachedTemplate)
        On Error GoTo ProcError
        If oTemplate Is Nothing Then
            Set oTemplate = g_oMPO.Templates(NormalTemplate.Name)
        End If
        
        If g_oMPO.db.LetterheadDefs(oTemplate.ID).Count = 0 Then
            Set oTemplate = g_oMPO.Templates.ItemFromClass("CLetter")
        End If
        .Template = oTemplate
        '---
        .Document.StatusBar = "Initializing.  Please wait..."
    
'       get starting author of the letterhead object
        .GetStartingAuthor
    
        With .Document
            If .File.ProtectionType <> wdNoProtection Then
                    .File.Unprotect
            End If
        End With
        
'---    load different LH types by using
'       m_oLH.Template.ID -- see note above
        Me.cmbLetterheadType.Array = _
            g_oMPO.db.LetterheadDefs(.Template.ID).ListSource
    End With
    
    ResizeTDBCombo Me.cmbLetterheadType, 6
    
    g_oMPO.People.Refresh

'   setup author list
    With Me.cmbAuthor
        .Array = g_oMPO.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
        DoEvents
    End With
    
    
    '---9.7.1 - 4147
    If bIniToBoolean(xGetTemplateConfigVal(m_oLH.Template.ID, "ShowAuthor2")) Then
        With Me.cmbAuthor2
            .Array = xARClone(g_oMPO.People.ListSource)
            .Rebind
            .Array.Insert 1, 0
            .Array.value(0, 0) = -1
            .Array.value(0, 1) = "-none-"
            .Array.value(0, 2) = -1
            ResizeTDBCombo Me.cmbAuthor2, 10
            DoEvents
        End With
    Else
        Me.lblAuthor2.Visible = False
        Me.cmbAuthor2.Visible = False
    End If
    
    '9.7.1 #4028
    If g_xLHCustom1Caption <> "" Then
        Me.chkIncludeLetterheadCustom1.Caption = g_xLHCustom1Caption
        Me.chkIncludeLetterheadCustom1.Visible = True
    Else
        Me.chkIncludeLetterheadCustom1.Visible = False
    End If
    If g_xLHCustom2Caption <> "" Then
        Me.chkIncludeLetterheadCustom2.Caption = g_xLHCustom2Caption
        Me.chkIncludeLetterheadCustom2.Visible = True
    Else
        Me.chkIncludeLetterheadCustom2.Visible = False
    End If
    If g_xLHCustom3Caption <> "" Then
        Me.chkIncludeLetterheadCustom3.Caption = g_xLHCustom3Caption
        Me.chkIncludeLetterheadCustom3.Visible = True
    Else
        Me.chkIncludeLetterheadCustom3.Visible = False
    End If
    '---
    
    m_oLH.Document.StatusBar = ""
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Loading Letterhead Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'SetLastPosition Me
    Set m_oPersonsForm = Nothing
    Set m_oLH = Nothing
    Set m_oOptions = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Unloading Letterhead Form"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Public Sub UpdateForm()
    On Error GoTo ProcError
'---get properties, load controls on reuse
    With m_oLH
        Me.cmbLetterheadType.BoundText = .LetterheadType
        
        If .IncludeEMail <> mpUndefined Then _
            Me.chkIncludeLetterheadEMail = Abs(.IncludeEMail)
        If .IncludeName <> mpUndefined Then _
            Me.chkIncludeLetterheadName = Abs(.IncludeName)
        If .IncludePhone <> mpUndefined Then _
            Me.chkIncludeLetterheadPhone = Abs(.IncludePhone)
        If .IncludeTitle <> mpUndefined Then _
            Me.chkIncludeLetterheadTitle = Abs(.IncludeTitle)
        If .IncludeFax <> mpUndefined Then _
            Me.chkIncludeLetterheadFax = Abs(.IncludeFax)
        If .IncludeAdmittedIn <> mpUndefined Then _
            Me.chkIncludeLetterheadAdmittedIn = Abs(.IncludeAdmittedIn)
        '9.7.1 #4028
        If .IncludeCustom1Detail <> mpUndefined Then _
            Me.chkIncludeLetterheadCustom1 = Abs(.IncludeCustom1Detail)
        If .IncludeCustom2Detail <> mpUndefined Then _
            Me.chkIncludeLetterheadCustom2 = Abs(.IncludeCustom2Detail)
        If .IncludeCustom3Detail <> mpUndefined Then _
            Me.chkIncludeLetterheadCustom3 = Abs(.IncludeCustom3Detail)
        '---
        Me.bAuthorIsDirty = False
        
        '9.7.1020 - #4532
        If g_oMPO.People.IsInPeopleList(.Document.ReuseAuthor.ID) Then
            Me.cmbAuthor.Text = .Document.ReuseAuthor.DisplayName
        Else
            Me.cmbAuthor.Text = .Author.DisplayName
        End If
        Me.bAuthorIsDirty = False
    
        '---9.7.1 - 4147
        
        If Not m_oLH.Author2 Is Nothing Then
            Me.cmbAuthor2.BoundText = m_oLH.Author2.ID
            
        Else
            Me.cmbAuthor2.Bookmark = 0
        End If
    
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLetterhead.UpdateForm"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Public Sub DisplayOptionalControls(lType As Long)
    On Error GoTo ProcError
'---enable/disable controls based on db interface rule settings
    Dim oDef As mpDB.CLetterheadDef
    
    Set oDef = g_oMPO.db.LetterheadDefs(m_oLH.Template.ID).Item(lType)
    If oDef Is Nothing Then
        Exit Sub
    End If
    
    With oDef
        Me.chkIncludeLetterheadEMail.Enabled = CBool(.AllowAuthorEMail)
        Me.chkIncludeLetterheadName.Enabled = CBool(.AllowAuthorName)
        Me.chkIncludeLetterheadPhone.Enabled = CBool(.AllowAuthorPhone)
        Me.chkIncludeLetterheadTitle.Enabled = CBool(.AllowAuthorTitle)
        Me.chkIncludeLetterheadFax.Enabled = CBool(.AllowAuthorFax)
        Me.chkIncludeLetterheadAdmittedIn.Enabled = CBool(.AllowAuthorAdmittedIn)
        '9.7.1 #4028
        Me.chkIncludeLetterheadCustom1.Enabled = .AllowCustomDetail1
        Me.chkIncludeLetterheadCustom2.Enabled = .AllowCustomDetail2
        Me.chkIncludeLetterheadCustom3.Enabled = .AllowCustomDetail3
        Me.lblInclude.Enabled = .AllowAuthorEMail Or .AllowAuthorName Or _
                                .AllowAuthorPhone Or .AllowAuthorFax Or _
                                .AllowAuthorTitle Or .AllowAuthorAdmittedIn Or _
                                .AllowCustomDetail1 Or .AllowCustomDetail2 Or _
                                .AllowCustomDetail3
        '---
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLetterhead.DisplayOptionalControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub m_oLH_BeforeBoilerplateInsert()
    Remove8xLetterhead
End Sub

Private Sub Remove8xLetterhead()
'   clear Headers/Footers per clients 8.x letterhead setup

    Dim xVal As String
    
    On Error GoTo ProcError
    
'   get MacPac document version
    On Error Resume Next
    xVal = m_oLH.Document.File.Variables("zzmpFixed_MacPacVersion").value
    On Error GoTo ProcError
    
    With m_oLH.Document
    
        If xVal <> "9.0" Then
    '       if 8.x document then clear Headers/Footers based on Template
'****************************************************************
'multitype: this assumes that all letter types will have a base template
'           whose template name has e.g. 'letter' in the template name
            Dim xBase As String
            g_oMPO.db.TemplateDefinitions.Refresh
            xBase = g_oMPO.db.TemplateDefinitions(m_oLH.Template.ID).BaseTemplate
            If InStr(UCase(xBase), "LETTER") Then
                .ClearHeaders .SelectedSection, , True
                .Clear8xPrimaryHeader .SelectedSection
            ElseIf InStr(UCase(xBase), "MEMO") Then
                .ClearHeaders .SelectedSection, , True
                .Clear8xPrimaryHeader .SelectedSection
            Else
                .ClearHeaders .SelectedSection, , True
                .ClearFooters .SelectedSection, , True
            End If
'            Select Case m_oLH.Template.ID
'                Case "Letter.dot"
'                    .ClearHeaders .SelectedSection, , True
'                    .Clear8xPrimaryHeader .SelectedSection
'                Case "Memo.dot"
'                    .ClearHeaders .SelectedSection, , True
'                    .Clear8xPrimaryHeader .SelectedSection
'                Case Else
'                    .ClearHeaders .SelectedSection, , True
'                    .clearfooters .SelectedSection, , True
'            End Select
'****************************************************************
        End If
    
    End With
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub PositionControls() '9.7.1 #4028
    Dim sOffset As Single
    If Not Me.cmbAuthor2.Visible Then
        Me.cmbLetterheadType.Top = Me.cmbAuthor2.Top + 100
        Me.lblType.Top = Me.cmbLetterheadType.Top + 30
    End If
    HideUnusedLetterheadControls Me, m_oLH.Template.ID
    sOffset = Me.cmbLetterheadType.Top + Me.cmbLetterheadType.Height + 200
    sOffset = PositionLetterheadControls(Me, Me.lblInclude.TabIndex, sOffset, 250)
    Me.btnCancel.Top = sOffset
    Me.btnFinish.Top = sOffset
    Me.Height = Me.btnFinish.Top + Me.btnFinish.Height + 600
End Sub
