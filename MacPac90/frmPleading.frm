VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmPleading 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Pleading"
   ClientHeight    =   6468
   ClientLeft      =   3072
   ClientTop       =   828
   ClientWidth     =   5484
   Icon            =   "frmPleading.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6468
   ScaleWidth      =   5484
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnMore 
      Caption         =   "M&ore..."
      Height          =   380
      Left            =   2055
      TabIndex        =   46
      Top             =   6015
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   48
      Top             =   6000
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4005
      TabIndex        =   47
      Top             =   6000
      Width           =   690
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   6480
      Left            =   -45
      TabIndex        =   45
      TabStop         =   0   'False
      Top             =   -75
      Width           =   5925
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Parties|Detai&ls"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   400
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   1
         Left            =   30
         Top             =   45
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   6060
         Left            =   6420
         TabIndex        =   50
         Top             =   12
         Width           =   5904
         Begin VB.TextBox txtSignerName 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            TabIndex        =   42
            Top             =   5490
            Visible         =   0   'False
            Width           =   3570
         End
         Begin mpControls.MultiLineCombo txtOriginalCourtTitle 
            Height          =   660
            Left            =   1740
            TabIndex        =   28
            Top             =   3060
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   1164
            BackColor       =   -2147483643
            ListRows        =   11
            Separator       =   " "
            SeparatorSpecial=   3
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.CheckBox chkIncludeCoverPage 
            Appearance      =   0  'Flat
            Caption         =   "&Include Cover Page"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1950
            TabIndex        =   32
            Top             =   4275
            Width           =   2490
         End
         Begin TrueDBList60.TDBCombo cmbOneOf 
            Height          =   300
            Left            =   1770
            OleObjectBlob   =   "frmPleading.frx":058A
            TabIndex        =   44
            Top             =   5625
            Width           =   3615
         End
         Begin VB.CheckBox chkIncludeClosingParagraph 
            Appearance      =   0  'Flat
            Caption         =   "E&xecuted At..."
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1950
            TabIndex        =   38
            Top             =   5250
            Visible         =   0   'False
            Width           =   2490
         End
         Begin VB.TextBox txtPresidingJudge 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            TabIndex        =   33
            Top             =   4035
            Visible         =   0   'False
            Width           =   3570
         End
         Begin VB.TextBox txtDocumentTitle 
            Appearance      =   0  'Flat
            Height          =   660
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   24
            Top             =   1260
            Width           =   3615
         End
         Begin VB.TextBox txtCaseTitle 
            Appearance      =   0  'Flat
            Height          =   660
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   22
            Top             =   1125
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtFooterText 
            Appearance      =   0  'Flat
            Height          =   660
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   26
            Top             =   2085
            Width           =   3615
         End
         Begin VB.TextBox txtJudgeName 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            TabIndex        =   40
            Top             =   5400
            Visible         =   0   'False
            Width           =   3570
         End
         Begin TrueDBList60.TDBCombo cmbDateType 
            Height          =   300
            Left            =   1740
            OleObjectBlob   =   "frmPleading.frx":27A6
            TabIndex        =   37
            Top             =   4905
            Width           =   3615
         End
         Begin TrueDBGrid60.TDBGrid grdDateTime 
            Height          =   975
            Left            =   1740
            OleObjectBlob   =   "frmPleading.frx":49C5
            TabIndex        =   30
            Top             =   3060
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbSignatureType 
            Height          =   300
            Left            =   1740
            OleObjectBlob   =   "frmPleading.frx":73D6
            TabIndex        =   35
            Top             =   4380
            Width           =   3615
         End
         Begin TrueDBGrid60.TDBGrid grdCaseNumbers 
            Height          =   780
            Left            =   1740
            OleObjectBlob   =   "frmPleading.frx":9612
            TabIndex        =   20
            ToolTipText     =   "Use CTL/Enter to create multiline case numbers, Up/Down arrows to scroll individual case numbers"
            Top             =   150
            Width           =   3615
         End
         Begin VB.Label lblSignerName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Si&gner Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   75
            TabIndex        =   41
            Top             =   5535
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblOneOf 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "B&y...:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   105
            TabIndex        =   43
            Top             =   5745
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Line Line3 
            X1              =   3045
            X2              =   3045
            Y1              =   135
            Y2              =   915
         End
         Begin VB.Label lblPresidingJudge 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Presiding &Judge:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   90
            TabIndex        =   31
            Top             =   4080
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblOriginalCourtTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "A&ppeal From:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   75
            TabIndex        =   27
            Top             =   3045
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblCaseTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Document Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   -480
            TabIndex        =   21
            Top             =   1320
            Visible         =   0   'False
            Width           =   2010
         End
         Begin VB.Label lblDocumentTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Document Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -480
            TabIndex        =   23
            Top             =   1125
            Width           =   2010
         End
         Begin VB.Label lblDateTime 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "D&ate && Time Info:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   75
            TabIndex        =   29
            Top             =   3045
            Width           =   1455
         End
         Begin VB.Label lblCaseNumber 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Case &Noxxxx:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -480
            TabIndex        =   19
            Top             =   150
            Width           =   2010
         End
         Begin VB.Label lblSignatureDate 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signat&ure Date:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   75
            TabIndex        =   36
            Top             =   4950
            Width           =   1455
         End
         Begin VB.Label lblFooterText 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Footer Te&xt:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -180
            TabIndex        =   25
            Top             =   2085
            Width           =   1695
         End
         Begin VB.Label lblSignatureType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Signature Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   75
            TabIndex        =   34
            Top             =   4425
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   1
            X1              =   -48
            X2              =   5952
            Y1              =   6012
            Y2              =   6012
         End
         Begin VB.Label lblJudgeName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Jud&ge Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   75
            TabIndex        =   39
            Top             =   5445
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H80000003&
            Height          =   6380
            Left            =   -120
            Top             =   -345
            Width           =   1800
         End
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   6060
         Left            =   12
         TabIndex        =   49
         Top             =   12
         Width           =   5904
         Begin mpControls.SigningAttorneysGrid saGrid 
            Height          =   1056
            Left            =   1728
            TabIndex        =   1
            Top             =   72
            Width           =   3612
            _ExtentX        =   6371
            _ExtentY        =   3387
            BackColor       =   -2147483643
            RecordSelectors =   0   'False
            Separator       =   "; "
            BarIDSuffix     =   ""
            BarIDPrefix     =   ", Bar No. "
            SignerEMailPrefix=   ", "
            SeparatorSpecial=   2
            IncludeSignerEmail=   -1  'True
            FontName        =   "Arial"
            FontBold        =   0   'False
            FontSize        =   8.4
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.TextBox txtParty3Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1710
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   18
            Top             =   5415
            Width           =   3615
         End
         Begin VB.TextBox txtParty1Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   12
            Top             =   3555
            Width           =   3615
         End
         Begin VB.TextBox txtClientName 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1725
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   9
            Top             =   2640
            Width           =   3585
         End
         Begin VB.TextBox txtCourtTitle 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   975
            Left            =   1725
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   1200
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbAttorneysFor 
            Height          =   300
            Left            =   1716
            OleObjectBlob   =   "frmPleading.frx":BD1A
            TabIndex        =   7
            Top             =   2316
            Width           =   3612
         End
         Begin TrueDBList60.TDBCombo cmbParty2Title 
            Height          =   300
            Left            =   1725
            OleObjectBlob   =   "frmPleading.frx":DF3D
            TabIndex        =   14
            Top             =   4170
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbParty1Title 
            Height          =   450
            Left            =   1725
            OleObjectBlob   =   "frmPleading.frx":1015F
            TabIndex        =   11
            Top             =   3240
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbParty3Title 
            Height          =   300
            Left            =   1710
            OleObjectBlob   =   "frmPleading.frx":12381
            TabIndex        =   17
            Top             =   5100
            Width           =   3630
         End
         Begin VB.TextBox txtParty2Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   15
            Top             =   4485
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbNoticeAttorney 
            Height          =   588
            Left            =   1716
            OleObjectBlob   =   "frmPleading.frx":145A3
            TabIndex        =   5
            Top             =   2268
            Width           =   3588
         End
         Begin VB.Label lblNoticeAttorney 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Notice Attorney:"
            ForeColor       =   &H00FFFFFF&
            Height          =   312
            Left            =   60
            TabIndex        =   4
            Top             =   2292
            Width           =   1452
         End
         Begin VB.Label lblVersus 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "v."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Left            =   3435
            TabIndex        =   53
            Top             =   4635
            Width           =   135
         End
         Begin VB.Label lblVersus2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "and"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   1170
            TabIndex        =   52
            Top             =   4755
            Visible         =   0   'False
            Width           =   300
         End
         Begin VB.Label lblParty3Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &3:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   16
            Tag             =   "Party &3:"
            Top             =   5145
            Width           =   1455
         End
         Begin VB.Label lblVersus1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "v."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   1260
            TabIndex        =   51
            Top             =   3810
            Visible         =   0   'False
            Width           =   195
         End
         Begin VB.Label lblParty1Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &1:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   10
            Tag             =   "Party &1:"
            Top             =   3270
            Width           =   1455
         End
         Begin VB.Label lblAttorneysFor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   " Attorne&ys For:"
            ForeColor       =   &H00FFFFFF&
            Height          =   228
            Left            =   96
            TabIndex        =   6
            Top             =   2388
            Width           =   1416
         End
         Begin VB.Label lblCourtTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Court &Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Left            =   45
            TabIndex        =   2
            Top             =   1260
            Width           =   1455
         End
         Begin VB.Label lblAttorneys 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Attorneys:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   0
            Top             =   120
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   5
            X1              =   -36
            X2              =   5409
            Y1              =   6012
            Y2              =   6012
         End
         Begin VB.Label lblParty2Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &2:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   13
            Tag             =   "Party &2:"
            Top             =   4200
            Width           =   1455
         End
         Begin VB.Label lblClientName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Client Name(s):"
            ForeColor       =   &H00FFFFFF&
            Height          =   288
            Left            =   48
            TabIndex        =   8
            Top             =   2640
            Width           =   1452
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H80000003&
            Height          =   6335
            Left            =   -120
            Top             =   -300
            Width           =   1800
         End
      End
   End
   Begin VB.Menu mnuOther 
      Caption         =   "&Other Functions"
      Visible         =   0   'False
      Begin VB.Menu mnuOther_Counsels 
         Caption         =   "Co&unsel/Co-Counsel..."
      End
      Begin VB.Menu mnuOther_CrossActions 
         Caption         =   "&Captions/Cross-Actions..."
      End
      Begin VB.Menu mnuOther_Signatures 
         Caption         =   "Si&gnatures..."
      End
      Begin VB.Menu mnuOther_ChangeCourt 
         Caption         =   "C&hange Court..."
      End
      Begin VB.Menu mnuOther_ChangeFont 
         Caption         =   "Change &Font..."
      End
      Begin VB.Menu mnuOther_TitlePage 
         Caption         =   "&Title Page"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuOther_BlueBack 
         Caption         =   "&Blue Back"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuOther_CoverPage 
         Caption         =   "&Litigation Back..."
      End
   End
   Begin VB.Menu mnuDateTime 
      Caption         =   "Date/Time Info"
      Visible         =   0   'False
      Begin VB.Menu mnuDateTime_Delete 
         Caption         =   "&Delete Row"
      End
      Begin VB.Menu mnuDateTime_Insert 
         Caption         =   "&Insert Row"
      End
      Begin VB.Menu mnuDateTime_Sep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDateTime_ClearAll 
         Caption         =   "&Clear All Rows"
      End
      Begin VB.Menu mnuDateTime_Reset 
         Caption         =   "&Reset to Defaults"
      End
   End
   Begin VB.Menu mnuCaseNumbers 
      Caption         =   "Case Numbers"
      Visible         =   0   'False
      Begin VB.Menu mnuCaseNumbers_DeleteCaseNumber 
         Caption         =   "&Delete Case Number"
      End
      Begin VB.Menu mnuCaseNumbers_DeleteAllNumbers 
         Caption         =   "Delete &All Case Numbers"
      End
      Begin VB.Menu mnuCaseNumbers_Sep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCaseNumbers_Reset 
         Caption         =   "&Reset to Defaults"
      End
   End
End
Attribute VB_Name = "frmPleading"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oPersonsForm As MPO.CPersonsForm
Private m_oMenu As MacPac90.CAuthorMenu
Private WithEvents m_oPleading As MPO.CPleading
Attribute m_oPleading.VB_VarHelpID = -1
Private WithEvents m_oSig As MPO.CPleadingSignature
Attribute m_oSig.VB_VarHelpID = -1
Private WithEvents m_oCnsl As MPO.CPleadingCounsel
Attribute m_oCnsl.VB_VarHelpID = -1
Private WithEvents m_oCap As MPO.CPleadingCaption
Attribute m_oCap.VB_VarHelpID = -1
Private WithEvents m_oCap2 As MPO.CPleadingCaption
Attribute m_oCap2.VB_VarHelpID = -1
Private WithEvents m_oOptForm As MPO.COptionsForm
Attribute m_oOptForm.VB_VarHelpID = -1

Private m_bCancelled As Boolean
Private m_bAuthorDirty As Boolean
Private m_ShowHiddenText As Boolean
Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private m_bOtherFormDisplayed As Boolean
Private m_bFooterTextWarning As Boolean
Private m_bCourtChanged As Boolean
Private m_bSigChanged As Boolean

Private xMsg As String
Private m_lSubForm As Long
Private m_lFooterTextMaxLength  As Long

'---9.4.1
Private m_lCaption1CaseNumberCount As Long
Private m_lCaption2CaseNumberCount As Long
'---End 9.4.1

Private Enum pldSubForm
    pldSubForm_Signature = 1
    pldSubForm_Counsels = 2
    pldSubForm_Captions = 3
    pldSubForm_ChangeCourt = 4
    pldSubForm_TitlePage = 5
    pldSubForm_BlueBack = 6
    pldSubForm_ChangeFont = 7
    pldSubForm_CoverPage = 8
End Enum

Private Const mpCapDateTimeSeparator As String = "|"
Private Const mpCaseNoSep As String = "  "
'---9.4.1
Private Const mpBlankLine10 As String = "__________"

Public Property Get Pleading() As MPO.CPleading
    Set Pleading = m_oPleading
End Property

Public Property Let Pleading(oNew As MPO.CPleading)
    Set m_oPleading = oNew
End Property

Public Property Get Signature() As MPO.CPleadingSignature
    Set Signature = m_oSig
End Property

Public Property Let Signature(oNew As MPO.CPleadingSignature)
    Set m_oSig = oNew
End Property

Public Property Let Counsel(oNew As MPO.CPleadingCounsel)
    Set m_oCnsl = oNew
End Property

Public Property Get Counsel() As MPO.CPleadingCounsel
    Set Counsel = m_oCnsl
End Property
Public Property Let PleadingCaption(oNew As MPO.CPleadingCaption)
    Set m_oCap = oNew
End Property
Public Property Get PleadingCaption() As MPO.CPleadingCaption
    Set PleadingCaption = m_oCap
End Property

'---9.4.1
Public Property Let PleadingCaption2(oNew As MPO.CPleadingCaption)
    Set m_oCap2 = oNew
End Property
Public Property Get PleadingCaption2() As MPO.CPleadingCaption
    Set PleadingCaption2 = m_oCap2
End Property
'---End 9.4.1

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property
Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property
Public Property Get FooterTextWarning() As Boolean
    FooterTextWarning = m_bFooterTextWarning
End Property
Public Property Let FooterTextWarning(bNew As Boolean)
    m_bFooterTextWarning = bNew
End Property
Public Property Get FooterTextMaxLength() As Long
    FooterTextMaxLength = m_lFooterTextMaxLength
End Property
Public Property Let FooterTextMaxLength(lNew As Long)
    m_lFooterTextMaxLength = lNew
End Property

'---9.4.1
Public Property Get Caption1CaseNumberCount() As Long
    Caption1CaseNumberCount = m_lCaption1CaseNumberCount
End Property
Public Property Let Caption1CaseNumberCount(lNew As Long)
    m_lCaption1CaseNumberCount = lNew
End Property
Public Property Get Caption2CaseNumberCount() As Long
    Caption2CaseNumberCount = m_lCaption2CaseNumberCount
End Property
Public Property Let Caption2CaseNumberCount(lNew As Long)
    m_lCaption2CaseNumberCount = lNew
End Property

'---End 9.4.1

Public Property Let AuthorIsDirty(bNew As Boolean)
    m_bAuthorDirty = bNew
End Property
Public Property Get AuthorIsDirty() As Boolean
    AuthorIsDirty = m_bAuthorDirty
End Property
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
    Me.CascadeUpdates = False
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = True
End Property
Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnCancel_GotFocus()
    SetControlBackColor btnCancel
End Sub

Private Sub btnFinish_Click()
    g_oMPO.SendToDebugLog "Start", "MacPac90.frmPleading.btnFinish_Click"

'   This is called explicitly, because if Finish is clicked while
'   grdDateTime has focus, LostFocus event never gets called
    On Error Resume Next
    grdDateTime_LostFocus
    
    On Error GoTo ProcError
    If Me.saGrid.Author = -1 Then
        xMsg = "You must select a signer"
        MsgBox xMsg, vbExclamation
        Exit Sub
    End If
    
'   check to see if signature formats match
    If Me.Pleading.Document.IsCreated Then
        If Me.Pleading.Signatures.ContainMixedFormats Then
            MsgBox g_xMixedPleadingSignatureWarning, vbExclamation
            Exit Sub
        End If
    End If
    '---9.7.1 - 4025 - refresh cover page
    If Me.chkIncludeCoverPage.Visible = True And Me.chkIncludeCoverPage = vbChecked Then
        Me.Pleading.GenerateCoverPage = chkIncludeCoverPage = vbChecked
    End If
    
    '---9.9.3 - 5717 - copy word count certificate properties from signature
    Me.Pleading.CertificateSigner = Me.Pleading.Author.FullName
    Me.Pleading.CertificateDate = Me.Pleading.Signatures(1).Dated
    Me.Pleading.CertificatePartyName = Me.Pleading.Signatures(1).PartyName
    Me.Pleading.CertificatePartyTitle = Me.Pleading.Signatures(1).PartyTitle
    
    m_bCancelled = False
    Me.Hide
    DoEvents
    Exit Sub

ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    SetControlBackColor btnFinish
End Sub

Private Sub btnMore_Click()
    On Error GoTo ProcError
    DoEvents
    Me.PopupMenu mnuOther, y:=btnMore.Top + btnMore.Height, x:=btnMore.Left
    Application.ScreenRefresh
    Application.ScreenUpdating = True
    EchoOn
    g_oMPO.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnMore_GotFocus()
    SetControlBackColor btnMore
End Sub

Private Sub chkIncludeClosingParagraph_Click()
    On Error GoTo ProcError
        If m_bSigChanged Then Exit Sub
        If Not Me.Initializing Then EchoOff
        UpdateClosingParagraph 0 - chkIncludeClosingParagraph.value
        If Not Me.Initializing Then EchoOn
   Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Exit Sub
End Sub

Private Sub chkIncludeClosingParagraph_GotFocus()
    OnControlGotFocus chkIncludeClosingParagraph
End Sub
Private Sub cmbAttorneysFor_GotFocus()
    OnControlGotFocus cmbAttorneysFor
    If Not Me.Counsel Is Nothing Then _
        m_oPleading.Document.SelectItem "zzmpCounselClientPartyTitle" & "_" & Me.Counsel.ID
End Sub
Private Sub cmbAttorneysFor_LostFocus()
    On Error GoTo ProcError
    If Not Me.Counsel Is Nothing Then _
        Me.Counsel.ClientPartyTitle = cmbAttorneysFor.Text
        Me.Signature.PartyTitle = cmbAttorneysFor.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub cmbDateType_GotFocus()
    OnControlGotFocus cmbDateType, "zzmpSignatureDated" & "_" & Me.Signature.ID
End Sub

Private Sub cmbDateType_LostFocus()
    On Error GoTo ProcError
    If Me.cmbDateType.Text = "None" Then
        Me.Signature.Dated = ""
    Else
        Me.Signature.Dated = Me.cmbDateType.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDateType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOneOf_GotFocus()
    OnControlGotFocus cmbOneOf, "zzmpSignatureOneOfText_" & Me.Signature.ID
End Sub

Private Sub cmbOneOf_ItemChange()
    Me.Signature.OneOfSignerText = cmbOneOf.Text
End Sub

Private Sub cmbOneOf_Change()
    Me.Signature.OneOfSignerText = cmbOneOf.Text
End Sub

Private Sub cmbParty3Title_GotFocus()
    OnControlGotFocus cmbParty3Title, "zzmpParty3Title" & "_1"
End Sub
Private Sub cmbParty3Title_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.Party3Title = cmbParty3Title.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbSignatureType_GotFocus()
    OnControlGotFocus cmbSignatureType, "zzmpSignatureBlock" & "_" & Me.Pleading.Signatures.Item(1).ID
End Sub

Private Sub cmbSignatureType_ItemChange()
'set signature type as appropriate, if necessary
    Dim lType As Long
    Dim xTemp As String
    
    On Error GoTo ProcError
    If Not Me.Initializing Then
        Application.ScreenUpdating = False
    End If
    
'---9.4.0
    Dim oOff As COffice
    If g_oMPO.UseDefOfficeInfo Then
        Set oOff = g_oMPO.Offices.Default
    Else
        Set oOff = Pleading.Author.Office
    End If
    
    With Pleading.Signatures
'       get selected signature type
        lType = Me.cmbSignatureType.BoundText
            .Item(1).TypeID = lType

        
'---refresh bar ID handling
        With Me.Signature.Definition
            
            '---9.7.1 4188
            saGrid.IncludeSignerEmail = .IncludeSignerEmail
            saGrid.SignerEmailPrefix = .SignerEmailPrefix
            saGrid.SignerEmailSuffix = .SignerEmailSuffix
            
            '---9.7.1 4097 - set this via Sig Def property
            saGrid.SignerNameCase = .SignerNameCase
            
            saGrid.IncludeBarID = .IncludeBarID
            saGrid.BarIDPrefix = .BarIDPrefix
            saGrid.BarIDSuffix = .BarIDSuffix
        End With

        xTemp = xSubstitute(saGrid.OtherSignersText, saGrid.Separator, vbLf)
        Me.Signature.OtherSigners = xTemp
        '---9.7.1 - 4081
        Me.Signature.Signer = saGrid.SignerText
        If Signature.Definition.DisplaySignerName = True And Not Me.Initializing Then
            Signature.InputSigner = saGrid.SignerText
            txtSignerName = Signature.InputSigner
        End If
        
        '---9.7.1 - 2437
        Me.Signature.Signer2 = saGrid.SignerText
        '---9.7.1 - 4167
        saGrid.IncludeSignerEmail = False
        saGrid.IncludeBarID = False
        Me.Signature.ESigner = saGrid.SignerText
        saGrid.IncludeSignerEmail = Me.Signature.Definition.IncludeSignerEmail
        saGrid.IncludeBarID = Me.Signature.Definition.IncludeBarID
        
        Me.Signature.AllSigners = xSubstitute(saGrid.SignersText, saGrid.Separator, vbLf)

        
        '---9.4.0
        If Me.Signature.Definition.ShowFirmID = True Then
            Me.Pleading.FirmID = oOff.FirmID
            Me.Signature.FirmID = oOff.FirmID
        Else
            Me.Pleading.FirmID = ""
            Me.Signature.FirmID(mpTrue) = ""
        End If
        
'           if dynamic editing is on, refresh signature
        If g_oMPO.DynamicEditing Then
            If Not Me.Initializing Then .Refresh
        End If

'       End If
    End With

    With Signature.Definition
        Me.lblJudgeName.Visible = .DisplayJudgeName
        Me.txtJudgeName.Visible = .DisplayJudgeName
        Me.chkIncludeClosingParagraph.Enabled = .AllowClosingParagraph
'---9.4.1
        If Me.txtJudgeName.Visible = True And Me.txtJudgeName.Text = "" Then
            '---9.6.1
            If Me.txtPresidingJudge <> "" Then
                Me.txtJudgeName = Me.txtPresidingJudge
                Signature.JudgeName = Me.txtJudgeName
            End If
            '---end 9.6.1
        End If
        
        '---9.7.1 - 4081
        Me.txtSignerName.Visible = .DisplaySignerName
        Me.lblSignerName.Visible = .DisplaySignerName
        
        DisplayControls True
        PositionControls
'---end 9.4.1
        If .AllowClosingParagraph = False Then
            chkIncludeClosingParagraph = vbUnchecked
        End If
        If Not Me.Initializing Then
            Me.cmbDateType.BoundText = .DateFormat
        End If
    End With
    If Not Me.Initializing Then Application.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err, "Cannot set signature type."
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub cmbSignatureType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbSignatureType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    OnFormKeyDown KeyCode
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'GLOG : 5718 : ceh
Private Sub cmbNoticeAttorney_GotFocus()
    OnControlGotFocus Me.cmbNoticeAttorney
End Sub

Private Sub cmbNoticeAttorney_LostFocus()
    On Error GoTo ProcError
    
    If Me.cmbNoticeAttorney.BoundText > 0 And _
    Me.cmbNoticeAttorney.Enabled Then
        Me.Pleading.NoticeAttorney = Me.cmbNoticeAttorney.BoundText
    End If
    
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbNoticeAttorney_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNoticeAttorney, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbNoticeAttorney_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbNoticeAttorney)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_AfterColUpdate(ByVal ColIndex As Integer)
    '---9.4.1
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    
    If Pleading.Definition.IsMultiParty = True And Not PleadingCaption2 Is Nothing Then
        Select Case grdCaseNumbers.Bookmark
            Case 0
                If Me.Caption1CaseNumberCount > 0 Then
                    PleadingCaption.CaseNumber1 = grdCaseNumbers.Columns(1).Text
                Else
                    If Me.Caption2CaseNumberCount > 0 Then
                        PleadingCaption2.CaseNumber1 = grdCaseNumbers.Columns(1).Text
                    End If
                End If
                '---update pleading paper case number prop as well
                If Me.Pleading.Definition.FooterTextIncludesCaseNo = True Then
                    Me.Pleading.PleadingPaper.CaseNumber = grdCaseNumbers.Columns(1).Text
                Else
                    Me.Pleading.PleadingPaper.CaseNumber = ""
                End If
            Case 1
                If Me.Caption1CaseNumberCount > 1 Then
                    PleadingCaption.CaseNumber2 = grdCaseNumbers.Columns(1).Text
                Else
                    Select Case Me.Caption1CaseNumberCount
                        Case 0
                            PleadingCaption2.CaseNumber2 = grdCaseNumbers.Columns(1).Text
                        Case 1
                            PleadingCaption2.CaseNumber1 = grdCaseNumbers.Columns(1).Text
                    End Select
                End If
            Case 2
                If Me.Caption1CaseNumberCount > 2 Then
                    PleadingCaption.CaseNumber3 = grdCaseNumbers.Columns(1).Text
                Else
                    Select Case Me.Caption1CaseNumberCount
                        Case 0
                            PleadingCaption2.CaseNumber3 = grdCaseNumbers.Columns(1).Text
                        Case 1
                            PleadingCaption2.CaseNumber2 = grdCaseNumbers.Columns(1).Text
                        Case 2
                            PleadingCaption2.CaseNumber1 = grdCaseNumbers.Columns(1).Text
                    End Select
                End If
            Case 3
                If Me.Caption1CaseNumberCount > 3 Then
                    PleadingCaption.CaseNumber4 = grdCaseNumbers.Columns(1).Text
                Else
                    Select Case Me.Caption1CaseNumberCount
                        Case 0
                            PleadingCaption2.CaseNumber4 = grdCaseNumbers.Columns(1).Text
                        Case 1
                            PleadingCaption2.CaseNumber3 = grdCaseNumbers.Columns(1).Text
                        Case 2
                            PleadingCaption2.CaseNumber2 = grdCaseNumbers.Columns(1).Text
                        Case 3
                            PleadingCaption2.CaseNumber1 = grdCaseNumbers.Columns(1).Text
                    End Select
                End If
            Case 4
                If Me.Caption1CaseNumberCount > 4 Then
                    PleadingCaption.CaseNumber5 = grdCaseNumbers.Columns(1).Text
                Else
                    Select Case Me.Caption1CaseNumberCount
                        Case 0
                            PleadingCaption2.CaseNumber5 = grdCaseNumbers.Columns(1).Text
                        Case 1
                            PleadingCaption2.CaseNumber4 = grdCaseNumbers.Columns(1).Text
                        Case 2
                            PleadingCaption2.CaseNumber3 = grdCaseNumbers.Columns(1).Text
                        Case 3
                            PleadingCaption2.CaseNumber2 = grdCaseNumbers.Columns(1).Text
                        Case 4
                            PleadingCaption2.CaseNumber1 = grdCaseNumbers.Columns(1).Text
                    End Select
                End If
            Case 5
                If Me.Caption1CaseNumberCount > 5 Then
                    PleadingCaption.CaseNumber6 = grdCaseNumbers.Columns(1).Text
                Else
                    Select Case Me.Caption1CaseNumberCount
                        Case 0
                            PleadingCaption2.CaseNumber6 = grdCaseNumbers.Columns(1).Text
                        Case 1
                            PleadingCaption2.CaseNumber5 = grdCaseNumbers.Columns(1).Text
                        Case 2
                            PleadingCaption2.CaseNumber4 = grdCaseNumbers.Columns(1).Text
                        Case 3
                            PleadingCaption2.CaseNumber3 = grdCaseNumbers.Columns(1).Text
                        Case 4
                            PleadingCaption2.CaseNumber2 = grdCaseNumbers.Columns(1).Text
                        Case 5
                            PleadingCaption2.CaseNumber1 = grdCaseNumbers.Columns(1).Text
                    End Select
                End If
    
        End Select
    
    Else
        With Me.PleadingCaption
            Select Case grdCaseNumbers.Bookmark
                Case 0
                    .CaseNumber1 = grdCaseNumbers.Columns(1).Text
                    '---update pleading paper case number prop as well
                    If Me.Pleading.Definition.FooterTextIncludesCaseNo = True Then
                        Me.Pleading.PleadingPaper.CaseNumber = .CaseNumber1
                    Else
                        Me.Pleading.PleadingPaper.CaseNumber = ""
                    End If
                Case 1
                    .CaseNumber2 = grdCaseNumbers.Columns(1).Text
                Case 2
                    .CaseNumber3 = grdCaseNumbers.Columns(1).Text
                Case 3
                    .CaseNumber4 = grdCaseNumbers.Columns(1).Text
                Case 4
                    .CaseNumber5 = grdCaseNumbers.Columns(1).Text
                Case 5
                    .CaseNumber6 = grdCaseNumbers.Columns(1).Text
            End Select
        End With
    End If
    '---End 9.4.1
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_FirstRowChange(ByVal SplitIndex As Integer)
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    If Not IsNull(grdCaseNumbers.Bookmark) Then
        Me.Pleading.Document.SelectItem "zzmpCaseNumber" & grdCaseNumbers.Bookmark + 1
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_GotFocus()
    On Error Resume Next
    grdCaseNumbers.EditActive = True
    GridTabOut Me.grdCaseNumbers, True, 1
    OnControlGotFocus grdCaseNumbers, "zzmpCaseNumber" & grdCaseNumbers.Bookmark + 1 & "_1"
End Sub

Private Sub grdCaseNumbers_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
    If Button = 2 Then
        Me.PopupMenu mnuCaseNumbers
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub grdCaseNumbers_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    With grdCaseNumbers
        '.EditActive = True
        If Not IsNull(.Bookmark) Then
            Me.Pleading.Document.SelectItem "zzmpCaseNumber" & .Bookmark + 1
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim xText As String
    On Error GoTo ProcError
    
    '---deal with grid selecting next row and transmitting returns that overwrite existing next row text
    If Shift = 2 Then
        If KeyCode = 13 Then
            With grdCaseNumbers
                If .SelLength = 0 Then
                    KeyCode = 0
                    xText = xTrimTrailingChrs(.Columns(.Col).Text, vbCrLf)
                    .Columns(.Col).Text = xText
                    .SelStart = Len(.Columns(.Col).Text)
                End If
            End With
        End If
    End If
    
    Select Case KeyCode
        Case 9
            GridTabOut Me.grdCaseNumbers, False, 1
        Case 39
            With grdCaseNumbers
                If .SelLength > 1 Then
                    .SelStart = .SelLength - 1
                    .SelLength = 1
                End If
            End With
        Case Else
    End Select
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdDateTime_GotFocus()
    GridTabOut Me.grdDateTime, True, 1
    OnControlGotFocus grdDateTime, "zzmpDateTimeInfo_1"
End Sub

Private Sub grdDateTime_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If Shift = 1 Then
        If KeyCode = 9 Then
            grdDateTime.TabAction = 0
        End If
    Else
        If KeyCode = 13 Then    '---change signer if in col 1
            If grdDateTime.Col = 1 Then
                KeyCode = 0
                GridMoveNext grdDateTime
            End If
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdDateTime_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
    If Button = 2 Then
        Me.PopupMenu mnuDateTime
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdDateTime_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    
    On Error Resume Next
    With grdDateTime
        If .RowBookmark(.Row) = .Array.UpperBound(1) Then
            If .Col = 1 Then GridTabOut Me.grdDateTime, False, .Col
        ElseIf .RowBookmark(.Row) >= .Array.LowerBound(1) Then
            If .Col = 0 Then GridTabOut Me.grdDateTime, False, .Col
        End If
    GridTabOut Me.grdDateTime, False, LastCol
    End With
End Sub

Private Sub mnuCaseNumbers_ClearAll_Click()
    Dim iRows As Integer
    
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iRows = .Array.Count(1)
        .Array.ReDim 0, -1, 0, 1
        .Array.ReDim 0, iRows, 0, 1
        .Rebind
        .Columns(0).AllowFocus = True
        .Columns(0).Locked = False
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub m_oPleading_CoverPageGenerated(rCP As Word.Range)
    '---9.7.1 - 4025 your custom code here.
End Sub

Private Sub mnuCaseNumbers_DeleteAllNumbers_Click()
    Dim iRow As Integer
    Dim iCount As Integer
    Dim i As Integer
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iCount = .Array.UpperBound(1)
        For i = 0 To iCount
            .Array.value(i, 1) = Empty
        Next i
        .Rebind
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCaseNumbers_DeleteCaseNumber_Click()
    Dim iRow As Integer
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iRow = .RowBookmark(.Row)
        .Array.value(iRow, 1) = Empty
        .Rebind
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCaseNumbers_DeleteRow_Click()
    Dim iRow As Integer
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iRow = .RowBookmark(.Row)
        .Array.value(iRow, 0) = Empty
        .Array.value(iRow, 1) = Empty
        .Rebind
        If iRow = 0 Then
            .Columns(0).AllowFocus = True
            .Columns(0).Locked = False
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCaseNumbers_Reset_Click()
    Dim xTemp As String
    Dim xarNew As XArray
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        .Array = CaseNumberArray(True)
        .Rebind
        .Col = 1
        .Bookmark = 0
        .EditActive = True
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub mnuOther_BlueBack_Click()
    m_lSubForm = pldSubForm_BlueBack
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_ChangeCourt_Click()
    m_lSubForm = pldSubForm_ChangeCourt
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_ChangeFont_Click()
    m_lSubForm = pldSubForm_ChangeFont
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_CoverPage_Click()
    m_lSubForm = pldSubForm_CoverPage
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_TitlePage_Click()
    With mnuOther_TitlePage
        .Checked = Not .Checked
        Me.Pleading.CoverPage = .Checked
        UnderConstruction
    End With
End Sub

Private Sub saGrid_ColumnEdit(iCol As Integer)
   '---9.7.1 4096
   Me.AuthorIsDirty = True
End Sub

Private Sub saGrid_MenuAddNewClick()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.Pleading.Author.ID
    m_oMenu.AddPerson Me.saGrid, lID
    With g_oMPO.Attorneys
        .Refresh
        saGrid.Attorneys = .ListSource
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_MenuCopyClick()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
    On Error GoTo ProcError
'   if author isn't in list, the
'   author is the reuse author
    If saGrid.Author = Empty Then
'       use reuse author as source
        Set oSource = Me.Pleading.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oMPO.People(Me.saGrid.Author)
    End If
    
'   copy the person
    Set oCopy = m_oPersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.saGrid
        With g_oMPO.Attorneys
            .Refresh
            saGrid.Attorneys = .ListSource
        End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub mnuDateTime_ClearAll_Click()
    On Error GoTo ProcError
    With Me.grdDateTime
        .Array.ReDim 0, -1, 0, 1
        .Rebind
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuDateTime_Delete_Click()
    On Error GoTo ProcError
    Me.grdDateTime.Delete
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuDateTime_Insert_Click()
    On Error Resume Next
    With Me.grdDateTime
        .Update
        .Array.Insert 1, .Bookmark
        .Rebind
    End With
End Sub

Private Sub mnuDateTime_Reset_Click()
    Dim xTemp As String
    Dim xarNew As XArray
    On Error GoTo ProcError
    With Me.PleadingCaption.Definition
        xTemp = .DateTimeInfoString
        Set xarNew = xarStringToxArray(xTemp, 2, mpCapDateTimeSeparator)
    End With
    With Me.grdDateTime
        .Array = xarNew
        .Rebind
        .MoveFirst
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuOther_Counsels_Click()
    m_lSubForm = pldSubForm_Counsels
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_CrossActions_Click()
    m_lSubForm = pldSubForm_Captions
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_Signatures_Click()
    m_lSubForm = pldSubForm_Signature
    Timer2.Enabled = True
End Sub

Private Sub saGrid_AuthorNotInList(lAuthorID As Long)
    MsgBox "Author " & lAuthorID & " not in loaded list.", vbInformation, "Signing Attorney Grid"
End Sub

Private Sub saGrid_GotFocus()
    OnControlGotFocus saGrid
End Sub

Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String, xSignerEmail As String)
    Dim oPerson As CPerson
    On Error Resume Next
    Set oPerson = g_oMPO.People.Item(lAuthorID)
    xLicenseID = oPerson.Licenses.Default.Number
    xAttorneyName = oPerson.FullName '(or use Short Name or whatever client prefers)
    '---9.6.2
    xSignerEmail = oPerson.EMail
    
    If saGrid.Signers.Count(1) > 1 Then
        Me.AuthorIsDirty = True
    End If

End Sub

''''Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String)
''''    Dim oPerson As CPerson
''''    On Error Resume Next
''''    Set oPerson = g_oMPO.People.Item(lAuthorID)
''''    xLicenseID = oPerson.Licenses.Default.Number
''''    xAttorneyName = oPerson.FullName '(or use Short Name or whatever client prefers)
''''    '---9.6.2
''''    If saGrid.Signers.Count(1) > 1 Then
''''        Me.AuthorIsDirty = True
''''    End If
''''End Sub
''''
Private Sub saGrid_OnLicenseColDropDown(lSignerID As Long)
    On Error Resume Next
    saGrid.SignerBarIDs = g_oMPO.People.Item(lSignerID).Licenses.ListSource
End Sub

Private Sub saGrid_SelectedSigner(lAuthorID As Long, bCancelChange As Boolean)
    MsgBox "Input signer cannot be the author of this document.  Please select an author from the dropdown list", vbInformation, "Signing Attorney Grid"
    '---don't allow pen for input author
    bCancelChange = True
End Sub

Private Sub saGrid_SignerChange()
    Dim bCascade As Boolean
    Dim lID As Long
    Dim xMsg As String
    Dim bForce As Boolean
    
    On Error GoTo ProcError
    DoEvents
    If Me.Initializing Then
        Exit Sub
    End If
    
    If Me.AuthorIsDirty Then
        AuthorIsDirty = False
        Exit Sub
    End If
    
    Me.AuthorIsDirty = True
    
    With Me.saGrid
        If .SignerText = "" And .Signers.Count(1) > 1 Then
'           validate
            xMsg = "Author is required information"
            .SetFocus
            MsgBox xMsg, vbExclamation, App.Title
            Exit Sub
        End If
            
    End With
        
'   write to object
    On Error GoTo ProcError
    If Not Me.Initializing Then EchoOff
    
    With Pleading
'       set new author & display name
        lID = Me.saGrid.Author
'        If saGrid.Signers.Count(1) = 1 Then
'            If lID = 0 Then
'                lID = saGrid.Signers.Value(0, 2)
'            End If
'        End If
        If lID = 0 Then Exit Sub
        .Author = g_oMPO.People(lID)
        Signature.DisplayName = g_oMPO.People(saGrid.Author).FullName
        
'       update values in form
        bCascade = Me.CascadeUpdates
        Me.CascadeUpdates = False
        
'       update object with values of selected person
        If Not Me.Initializing Then
            .UpdateForAuthor
        End If
        
        Me.CascadeUpdates = bCascade
    End With
    
    'Me.AuthorIsDirty = False
    If Not Me.Initializing Then EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    'Me.AuthorIsDirty = False
    Exit Sub
End Sub

Private Sub Timer1_Timer()
'    SetMyControlBackColor
'    SelectMyTextOnEntry
End Sub

Private Sub saGrid_SignersChange()
    
    '---9.7.2 - 4241
    '---author hasn't necessarily change but order of signer has - need to refresh Counsel props
    Me.AuthorIsDirty = True
    
    'GLOG : 5718 : ceh
    UpdateNoticeAttorneyControl
End Sub
Private Sub Timer2_Timer()
   '---note:  this routine is called from the more button popup menu
   '---it's a workaround that enables popup menus to be displayed by
   '---any form called by a popup menu
   '---if these routines were called directly from the more button popup
   '---no popup menus would display in sig or caption forms.  This is a VB bug
   '---around since version 1!
    Timer2.Enabled = False
    Select Case m_lSubForm
        Case pldSubForm_Signature
            CreatePleadingSignatures
        Case pldSubForm_Captions
            CreatePleadingCaptions
        Case pldSubForm_Counsels
            '---9.5.0
            '---9.7.1 - 4025
            If Pleading.Definition.AllowCoCounsel = False Then
                If Pleading.Definition.DefaultCounselType = Empty Then
                    CreatePleadingSignatures True
                Else
                    If Me.chkIncludeCoverPage.Visible = True And Me.chkIncludeCoverPage = vbChecked Then
                        CreatePleadingCounsels
                    Else
                        CreatePleadingSignatures True
                    End If
                End If
            Else
                CreatePleadingCounsels
            End If
            
        Case pldSubForm_ChangeCourt
            Me.ChangePleadingType
        Case pldSubForm_ChangeFont
            Me.ChangeFont
        Case pldSubForm_BlueBack
            UnderConstruction
        Case pldSubForm_CoverPage
            Me.InsertCoverPage
        Case Else
    End Select
End Sub

Private Sub txtClientName_GotFocus()
    OnControlGotFocus txtClientName
    If Not Me.Counsel Is Nothing Then _
        m_oPleading.Document.SelectItem "zzmpCounselClientPartyName" & "_" & Me.Counsel.ID
End Sub
Private Sub txtClientName_LostFocus()
    On Error GoTo ProcError
    If Not Me.Counsel Is Nothing Then _
        Me.Counsel.ClientPartyName = txtClientName
    Me.Signature.PartyName = txtClientName
''---synch party1 or party2 name to textbox if party titles match match
''---remm'd out for generic, put it back in if client requests

'    If Len(txtClientName) > 0 Then
'        With Me.Signature
'            If .PartyTitle = Me.PleadingCaption.Party1Title Then
'                '---synch to party1name
'                Me.PleadingCaption.Party1Name = .PartyName
'                Me.txtParty1Name.Text = txtClientName
'            End If
''---in case you have both
'            If .PartyTitle = Me.PleadingCaption.Party2Title Then
'                '---synch to party2name
'                Me.PleadingCaption.Party2Name = .PartyName
'                Me.txtParty2Name.Text = txtClientName
'            End If
'        End With
'    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbDateType_ItemChange()
    On Error GoTo ProcError
    If Me.cmbDateType.Text = "None" Then
        Me.Signature.Dated = ""
    Else
        Me.Signature.Dated = Me.cmbDateType.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub Form_Activate()
'    On Error GoTo ProcError
    Dim bUpdate As Boolean
    Dim lTemp As Long
    
    
    DoEvents
    '---form activate fires after subforms are released
    '---R&D needed -- some subforms don't cause this behavior
    
    If m_bOtherFormDisplayed Then
        m_bOtherFormDisplayed = False
        Exit Sub
    End If
    
    Me.vsIndexTab1.CurrTab = 0
'---display controls appropriate to pleading type def
    SetInterfaceControls
 
    If Me.Initializing Then
        DoEvents
        With Me.Pleading
            If .Document.IsCreated Then
                UpdateForm
                ChangeFontOnReuse
                '---refresh closing paragraph if appropriate
                If Me.Pleading.Signatures.ClosingParagraph = mpTrue Then
                    Me.Pleading.Signatures.RefreshClosingParagraph
                End If
                '---refresh split court title
                bUpdate = g_oMPO.ForceItemUpdate
                g_oMPO.ForceItemUpdate = True
                .CourtTitle = .CourtTitle
                .CourtTitle1 = .CourtTitle1
                .CourtTitle2 = .CourtTitle2
                .CourtTitle3 = .CourtTitle3
                g_oMPO.ForceItemUpdate = bUpdate
                
            Else
''---Assign signer's pen to default author only if default author is attorney
''---accounts for .defaultauthor = .authors.listsource.first condition
                If .Author.IsAttorney Then Me.saGrid.Author = .Author.ID
                Me.AuthorIsDirty = False
'---set properties based on preloads
                UpdateObjectDefaults
                'GLOG : 5718 : ceh
                Me.cmbNoticeAttorney.SelectedItem = 0
            End If
            'GLOG : 5718 : ceh
            UpdateNoticeAttorneyControl
        End With
                
        Screen.MousePointer = vbDefault
        DoEvents
'---wake up the form for SDI
        SendKeys "+", True
        DoEvents
        Me.Initializing = False
    End If
    btnMore.Enabled = True
'   move dialog to last position
    'MoveToLastPosition Me

    Application.ScreenUpdating = True
    Application.ScreenRefresh
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Screen.MousePointer = vbDefault
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
    
    Set m_oMenu = Nothing
    Set m_oPersonsForm = Nothing
    Set m_oPleading = Nothing
    Set m_oOptForm = Nothing
    Set m_oCnsl = Nothing
    Set m_oSig = Nothing
    Set g_oPrevControl = Nothing
    '---9.4.1
    Set m_oCap = Nothing
    Set m_oCap2 = Nothing
    
End Sub
'******************************************************************************
'Menu events
'******************************************************************************

Private Sub Form_Load()
    Dim oParties As XArray
    Dim datStart As Date
    Dim xFilter As String
    Dim i As Integer
    Dim oList As mpDB.CList
    Dim bDyn As Boolean
    Dim oArray As XArray
    Dim xOrdSuffix As String
    Dim lTemp As Long
    
    On Error GoTo ProcError
    
    lTemp = mpbase2.CurrentTick
    
    Me.Initializing = True
    m_bCancelled = True
    Me.Top = 20000
    EchoOff
    '---9.6.1
    Application.ScreenUpdating = False
    DoEvents
    Set m_oPersonsForm = New MPO.CPersonsForm
    Set m_oOptForm = New MPO.COptionsForm
    Set m_oMenu = New MacPac90.CAuthorMenu
    
    bDyn = g_oMPO.DynamicEditing
    
    With Me.Pleading
        If .Definition.DefaultSignatureType Then
            Signature = .Signatures(1)
            Me.Signature.DynamicUpdating = bDyn
        End If
        If .Definition.DefaultCounselType Then
            Counsel = .Counsels(1)
            Me.Counsel.DynamicUpdating = bDyn
        End If
        If .Definition.DefaultCaptionType Then
            PleadingCaption = .Captions(1)
            Me.PleadingCaption.DynamicUpdating = bDyn
        
        End If

'---9.4.1 class action support
        
        If .Definition.DefaultCaptionType2 And .Definition.IsMultiParty Then
            If Not Pleading.Document.IsCreated Then
                .Captions.Add
                .Captions(2).TypeID = .Definition.DefaultCaptionType2
                .Captions(2).Position = 2
            End If
            PleadingCaption2 = .Captions(2)
            Me.PleadingCaption2.DynamicUpdating = bDyn
        End If
        
'---End 9.4.1
    
    
    End With
    
    
'---fill combos with lists
'---refresh attorneys first
    g_oMPO.Attorneys.Refresh
    Me.saGrid.Attorneys = g_oMPO.Attorneys.ListSource
    DoEvents
    Set oParties = g_oMPO.Lists("PleadingParties").ListItems.Source
    Me.cmbAttorneysFor.Array = oParties
    Me.cmbParty1Title.Array = oParties
    Me.cmbParty2Title.Array = oParties
    Me.cmbParty3Title.Array = oParties
    
    '---9.4.1
    Set oParties = Nothing
    Set oParties = g_oMPO.Lists("PleadingPartiesOneOf").ListItems.Source
    Me.cmbOneOf.Array = oParties
    '---end 9.4.1
    
    With g_oMPO.Lists
'---load date array with non-jurat choices
'        Me.cmbDateType.Array = _
'            .Item("DateFormats").ListItems.Source
        
'---if you want to load/support Jurat type dates, unrem code below and load date combo with result
        Set oArray = g_oMPO.NewXArray
        Set oArray = .Item("DateFormatsPleading").ListItems.Source
        
'       get ordinal suffix based on current date - eg "rd", "th"
        xOrdSuffix = GetOrdinalSuffix(Day(Date))
        
'       replace all instances of ordinal variable
'       with ordinal suffix-this is done here because
'       there is no way in vb to format a dateas an ordinal
        For i = oArray.LowerBound(1) To oArray.UpperBound(1)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%o%", xOrdSuffix)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%or%", xOrdSuffix)  '9.7.1 #4402
        Next i
        
        Me.cmbDateType.Array = oArray
        
        Me.cmbSignatureType.Array = _
            .Item("PleadingSignatures").ListItems.Source

'---load sig types list valid for pleading type - filter by ID
        Set oList = .Item("PleadingSignatures")
        With oList
            .FilterField = "tblPleadingSignatureAssignments.fldPleadingTypeID"
            .FilterValue = Me.Pleading.Definition.ID
            .Refresh
            Me.cmbSignatureType.Array = .ListItems.Source
        End With
        
        'GLOG : 5718 : ceh
        With Me.cmbNoticeAttorney
            .Array = xARClone(g_oMPO.Attorneys.ListSource)
            .Rebind
            .Array.Insert 1, 0
            .Array.value(0, 0) = -1
            .Array.value(0, 1) = "-none-"
            .Array.value(0, 2) = -1
            ResizeTDBCombo Me.cmbNoticeAttorney, 10
            DoEvents
        End With

    End With
    
'   set combo dropdown height values
    ResizeTDBCombo Me.cmbDateType, 6
    ResizeTDBCombo Me.cmbSignatureType, 6
    ResizeTDBCombo Me.cmbAttorneysFor, 6
    ResizeTDBCombo Me.cmbParty1Title, 10
    ResizeTDBCombo Me.cmbParty2Title, 10
    ResizeTDBCombo Me.cmbParty3Title, 10
    ResizeTDBCombo Me.cmbOneOf, 6
    saGrid.ZOrder 0
    txtCourtTitle.ZOrder 1

    DebugOutput "MacPac90.frmPleading ### " & vbTab & " Load was " & mpbase2.ElapsedTime(lTemp)

''   move dialog to last position
'    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub grdDateTime_LostFocus()
    On Error Resume Next
    DoEvents
    With grdDateTime
        .Update
        .Col = 0
        .Row = 0
    End With
    On Error GoTo ProcError
    '---9.4.1
    '---for now we are only allowing either cap1 or cap2 to have date time info when filled from pleading main
    If Me.PleadingCaption.Definition.DateTimeInfoString <> "" Then
        Me.PleadingCaption.DateTimeInfo = xDateTimeString
    End If
    
    If Not Me.PleadingCaption2 Is Nothing Then
        If Me.PleadingCaption2.Definition.DateTimeInfoString <> "" Then
            Me.PleadingCaption2.DateTimeInfo = xDateTimeString
        End If
    End If
    
    '---End 9.4.1
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_LostFocus()
    On Error GoTo ProcError
    With grdCaseNumbers
        .Update
        .Col = 1
        .Row = 0
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Function bLoadTDGDropDown(grdGrid As TDBGrid, iTargCol As Integer, x_XAR As XArray, iColIndex As Integer)
    Dim ctl As Control
    Dim g_Grid As TDBGrid
    Dim i As Integer
    Dim VItem As New TrueDBGrid60.ValueItem
        
    On Error GoTo ProcError
        
    Set g_Grid = grdGrid

'---load dropdown
    Dim xAR As XArray
    With g_Grid
        .Columns(iTargCol).ValueItems.Clear
        For i = 0 To x_XAR.UpperBound(1)
            VItem.value = x_XAR(i, 0)
            VItem.DisplayValue = x_XAR(i, 1)
            .Columns(iTargCol).ValueItems.Add VItem
        Next i

    End With
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.bLoadTDGDropDown"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Sub saGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 28 Then KeyCode = 0
    '---9.6.2
    If KeyCode = 13 Then Me.AuthorIsDirty = True
End Sub

Private Sub saGrid_LostFocus()

    Dim xTemp As String
    Dim xSep As String
    Dim oOff As COffice
    Dim xFN As String

    On Error Resume Next

    '9.7.1 #4423
    If Me.Initializing Then Exit Sub
 
 '---9.4.1
    If Pleading.Author.ID <> saGrid.Author Then
        If saGrid.Author > 0 Then
            Pleading.Author = g_oMPO.db.People(saGrid.Author)
            Me.AuthorIsDirty = True
        End If
    End If
    '---end 9.4.1

    '---9.5.0
    If g_oMPO.UseDefOfficeInfo Then
        Set oOff = g_oMPO.Offices.Default
    Else
        Set oOff = Pleading.Author.Office
    End If
    '---9.7.1 - set new office properties for proper refresh of firm name formats if author changed
    With Me.Pleading
        Me.Signature.Office = oOff
        Me.Counsel.Office = oOff
        Me.Pleading.PleadingPaper.Office = oOff
    End With
    '---end 9.5.0

    If (Me.AuthorIsDirty = True) And saGrid.Author > 0 Then
        '---do nothing


    ElseIf (Me.AuthorIsDirty = False) And saGrid.Author > 0 Then
        Exit Sub

    Else

        '---9.4.0
        With saGrid
            xSep = .Separator
            '---refresh signers in counsel
            If Not Me.Counsel Is Nothing Then
                '---9.4.1
                With Me.Counsel.Definition
                    saGrid.IncludeBarID = .IncludeBarID
                    saGrid.BarIDPrefix = .BarIDPrefix
                    saGrid.BarIDSuffix = .BarIDSuffix
                
                    '---9.6.2
                    saGrid.IncludeSignerEmail = .IncludeSignerEmail
                    saGrid.SignerEmailPrefix = .SignerEmailPrefix
                    '---9.7.1 4105
                    saGrid.SignerEmailSuffix = .SignerEmailSuffix
                    '---9.7.1 4096
                    saGrid.SignerNameCase = .SignerNameCase
                    
                End With
                '---end 9.4.1
                If .SignersText = "" Then
                    xTemp = .SignerText
                Else
                    xTemp = .SignersText
                End If
                xTemp = xSubstitute(xTemp, xSep, vbLf)
                 Me.Counsel.AttorneyNames = xTemp
                xTemp = ""
            End If

'---9.5
            With Me.Counsel
                
                '---9.7.1 - 4030
                If .Definition.OfficeAddressFormat = 0 Then
                    .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
                Else
                    .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
                End If
                
                .FirmFax = oOff.Fax1
                .FirmPhone = oOff.Phone1
                '---9.4.1
                .FirmName = oOff.FirmName
                '---end 9.4.1
                If .FirmName <> "" Then
                    xFN = " - " & .FirmName
                Else
                    xFN = ""
                End If
                
                '---9.7.1 4096
                If InStr(saGrid.SignerText, ",") Then
                    .DisplayName = StrConv(Left(saGrid.SignerText, InStr(saGrid.SignerText, ",") - 1), vbProperCase) & xFN
                Else
                    .DisplayName = StrConv(saGrid.SignerText, vbProperCase) & xFN
                End If

                If saGrid.SignerText = "" Then
                    .SignerEMail = ""
                Else
                    .SignerEMail = Pleading.Author.EMail
                End If
                
                Set .Signers = saGrid.Signers
                
                '---9.7.1 4104
                .SignersEmail = saGrid.SignersEmailText
                
            End With
'---end 9.5
            '---refresh signers in signature
            With Me.Signature.Definition
                
                '---9.7.1 4188
                saGrid.IncludeSignerEmail = .IncludeSignerEmail
                saGrid.SignerEmailPrefix = .SignerEmailPrefix
                saGrid.SignerEmailSuffix = .SignerEmailSuffix
                
                saGrid.IncludeBarID = .IncludeBarID
                saGrid.BarIDPrefix = .BarIDPrefix
                saGrid.BarIDSuffix = .BarIDSuffix
                '---9.7.1 4097 - set this via Sig Def property
                saGrid.SignerNameCase = .SignerNameCase
            End With

            xTemp = xSubstitute(.OtherSignersText, xSep, vbLf)
            With Me.Signature
                .OtherSigners = xTemp
                '---9.7.1 - 4081
                .Signer = saGrid.SignerText
                
                If Signature.Definition.DisplaySignerName = True Then
                    If txtSignerName = "" Then
                        txtSignerName = saGrid.SignerText
                        Signature.InputSigner = txtSignerName
                    End If
                End If
                
                '---9.4.1
                .Signer2 = saGrid.SignerText
                '---9.7.1 - 4167
                saGrid.IncludeSignerEmail = False
                saGrid.IncludeBarID = False
                .ESigner = saGrid.SignerText
                saGrid.IncludeSignerEmail = .Definition.IncludeSignerEmail
                saGrid.IncludeBarID = .Definition.IncludeBarID
                
                .AllSigners = xSubstitute(saGrid.SignersText, xSep, vbLf)
                '---end 9.4.1
                Set .Signers = saGrid.Signers
            End With

        End With    '---saGrid

        Exit Sub
    End If

    '---9.4.0
    If Me.Signature.Definition.ShowFirmID = True Then
        Me.Pleading.FirmID = oOff.FirmID
    Else
        Me.Pleading.FirmID(mpTrue) = ""
    End If

    On Error GoTo ProcError
    With Me.saGrid
        If .SignerText = "" Then
            Exit Sub
        End If
        xSep = .Separator

    '---set up counsel
       If Not Me.Counsel Is Nothing Then
            With Me.Counsel.Definition
            
                '---9.6.2
                saGrid.IncludeSignerEmail = .IncludeSignerEmail
                saGrid.SignerEmailPrefix = .SignerEmailPrefix
                '---9.7.1 4105
                saGrid.SignerEmailSuffix = .SignerEmailSuffix
                 '---9.7.1 4096
                saGrid.SignerNameCase = .SignerNameCase
           
                saGrid.IncludeBarID = .IncludeBarID
                saGrid.BarIDPrefix = .BarIDPrefix
                saGrid.BarIDSuffix = .BarIDSuffix
            End With
            If .SignersText = "" Then
                xTemp = .SignerText
            Else
                xTemp = .SignersText
            End If
            xTemp = xSubstitute(xTemp, xSep, vbLf)
             Me.Counsel.AttorneyNames = xTemp
            xTemp = ""
            With Me.Counsel
                
                '---9.7.1 - 4030
                If .Definition.OfficeAddressFormat = 0 Then
                    .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
                Else
                    .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
                End If
                
                .FirmFax = oOff.Fax1
                .FirmPhone = oOff.Phone1
                '---9.4.1
                .FirmName = oOff.FirmName
                '---end 9.4.1
                If .FirmName <> "" Then
                    xFN = " - " & .FirmName
                Else
                    xFN = ""
                End If

                '---9.7.1 - 4096
                If InStr(saGrid.SignerText, ",") Then
                    .DisplayName = StrConv(Left(saGrid.SignerText, InStr(saGrid.SignerText, ",") - 1), vbProperCase) & xFN
                Else
                    .DisplayName = StrConv(saGrid.SignerText, vbProperCase) & xFN
                End If

                If saGrid.SignerText = "" Then
                    .SignerEMail = ""
                Else
                    .SignerEMail = Pleading.Author.EMail
                End If
                
                Set .Signers = saGrid.Signers

                '---9.7.1 4104
                .SignersEmail = saGrid.SignersEmailText
            End With

       End If

 '---set up sigs

        With Me.Signature.Definition
            
            '---9.7.1 4188
            saGrid.IncludeSignerEmail = .IncludeSignerEmail
            saGrid.SignerEmailPrefix = .SignerEmailPrefix
            saGrid.SignerEmailSuffix = .SignerEmailSuffix
            
            saGrid.IncludeBarID = .IncludeBarID
            saGrid.BarIDPrefix = .BarIDPrefix
            saGrid.BarIDSuffix = .BarIDSuffix
        
            '---9.7.1 4097 - set this via Sig Def property
            saGrid.SignerNameCase = .SignerNameCase
        
        End With

        xTemp = xSubstitute(.OtherSignersText, xSep, vbLf)

 '---9.4.0
        With Me.PleadingCaption
            .OtherSigners = xTemp
            .Signer = saGrid.SignerText
            .FirmName = oOff.FirmName
            
            '---9.7.1 - 4030
            If .Definition.OfficeAddressFormat = 0 Then
                .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
            Else
                .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
            End If
            
            .FirmPhone = oOff.Phone1
            .FirmFax = oOff.Fax1
            .SignerEMail = Me.Pleading.Author.EMail
        End With

        With Me.Signature
            .OtherSigners = xTemp
            .AllSigners = xSubstitute(saGrid.SignersText, xSep, vbLf)
            '---9.7.1 - 4081
            .Signer = saGrid.SignerText
            
            If Signature.Definition.DisplaySignerName = True Then
                If txtSignerName = "" Or Me.AuthorIsDirty Then
                    txtSignerName = saGrid.SignerText
                    .InputSigner = txtSignerName
                End If
            End If
            
            .Signer2 = saGrid.SignerText
            '---9.7.1 - 4167
            saGrid.IncludeSignerEmail = False
            saGrid.IncludeBarID = False
            .ESigner = saGrid.SignerText
            saGrid.IncludeSignerEmail = .Definition.IncludeSignerEmail
            saGrid.IncludeBarID = .Definition.IncludeBarID
            
            Set .Signers = saGrid.Signers
            '---9.7.1 - 4030
            If .Definition.OfficeAddressFormat = 0 Then
                .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
            Else
                .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
            End If
            .FirmPhone = oOff.Phone1
            .FirmFax = oOff.Fax1
            .FirmCity = oOff.City
            .FirmState = oOff.State
            '---9.7.1 4096
            .DisplayName = StrConv(saGrid.SignerText, vbProperCase)

            '---9.4.0
            If .Definition.ShowFirmID = True Then
                .FirmID = oOff.FirmID
            Else
                .FirmID(mpTrue) = ""
            End If

            '---9.4.1
            .FirmName = oOff.FirmName
            
            '---9.9.1 4954
            .FirmNameESig = oOff.FirmName
            
            '---end 9.4.1

            If saGrid.SignerText = "" Then
                .SignerEMail = ""
            Else
                .SignerEMail = Me.Pleading.Author.EMail
            End If
        End With

'        '---update closing paragraph if any
        EchoOff
        UpdateClosingParagraph 0 - chkIncludeClosingParagraph.value
        EchoOn

    End With
    Pleading.Document.StatusBar = ""
    Me.AuthorIsDirty = False
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOff
    Me.AuthorIsDirty = False
    Exit Sub
End Sub

Private Sub saGrid_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim bEnabled As Boolean
    On Error GoTo ProcError
    If Button = 2 Then
'        With Me.cmbAuthor
'            Me.mnuAuthor_SetDefault.Enabled = _
'                (.Text = .Columns(1)) And (.Columns(0).Value > 1)
'        End With
        With saGrid
        '   enable specified menu items only if author
        '   is currently in the db
            If TypeOf Pleading.Author Is CReUseAuthor Then
                bEnabled = False
            ElseIf saGrid.Author <> saGrid.Signers(saGrid.SelectedRow, AttyArrayCol_ID) Then
                bEnabled = False
            Else
                bEnabled = True
            End If

            With Me.saGrid
                .MenuCopyEnabled = bEnabled
                .MenuFavoriteEnabled = bEnabled
                .MenuSetDefaultEnabled = bEnabled
                .MenuFavoriteChecked = Me.Pleading.Author.Favorite
            End With
           
           'If .Author = .SelectedRow And .Author > -1 Then
                'Me.PopupMenu mnuAuthor
           ' End If
        End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCourtTitle_GotFocus()
    Dim xBm As String
    If ActiveDocument.Bookmarks.Exists("zzmpCourtTitle_1") Then
        xBm = "zzmpCourtTitle_1"
    Else
        xBm = "zzmpCourtTitle"
    End If
    OnControlGotFocus txtCourtTitle, xBm
'---wake up the form for SDI
    SendKeys "+", True
End Sub
Private Sub txtCourtTitle_LostFocus()
    On Error GoTo ProcError
    
    If Me.Pleading.Definition.CourtTitle1 = "" Then
        Me.Pleading.CourtTitle = Me.txtCourtTitle
    Else
        '---split court title
        SplitCourtTitle
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDocumentTitle_GotFocus()
    OnControlGotFocus txtDocumentTitle, "zzmpDocumentTitle"
End Sub
Private Sub txtDocumentTitle_LostFocus()
    Dim xTemp As String
    On Error GoTo ProcError
    Me.Pleading.DocumentTitle = txtDocumentTitle
    If Me.txtFooterText.Visible Then
'**************
        If Me.Pleading.Definition.FooterTextIncludesCaseNo = True Then
            Me.Pleading.PleadingPaper.CaseNumber = Me.PleadingCaption.CaseNumber1
        Else
            Me.Pleading.PleadingPaper.CaseNumber = ""
        End If
        With Me.Pleading.PleadingPaper
            Me.txtFooterText = Me.txtDocumentTitle
            .DocTitle = Me.txtFooterText
        End With
    End If
'**************
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtCaseTitle_GotFocus()
    '---9.4.1
    If Not Me.PleadingCaption2 Is Nothing Then
        If Me.PleadingCaption2.Definition.IncludesCaseTitle = True Then
            OnControlGotFocus txtCaseTitle, "zzmpCaseTitle" & "_2"
        Else
            OnControlGotFocus txtCaseTitle, "zzmpCaseTitle" & "_1"
        End If
    Else
        OnControlGotFocus txtCaseTitle, "zzmpCaseTitle" & "_1"
    End If
    '---End 9.4.1
End Sub
Private Sub txtCaseTitle_LostFocus()
    Dim xTemp As String
    On Error GoTo ProcError
    Me.PleadingCaption.CaseTitle = Me.txtCaseTitle
    '---9.4.1
    If Not Me.PleadingCaption2 Is Nothing Then
        Me.PleadingCaption2.CaseTitle = Me.txtCaseTitle
    End If
    '---End 9.4.1
    If Me.txtFooterText.Visible Then
        '---9.4.1
        If Me.Pleading.Definition.FooterTextIncludesCaseNo = True Then
            If Me.Caption1CaseNumberCount > 0 Then
                Me.Pleading.PleadingPaper.CaseNumber = Me.PleadingCaption.CaseNumber1
            Else
                If Me.Caption2CaseNumberCount > 0 Then
                    Me.Pleading.PleadingPaper.CaseNumber = Me.PleadingCaption2.CaseNumber1
                End If
            End If
        '---End 9.4.1
        Else
            Me.Pleading.PleadingPaper.CaseNumber = ""
        End If
        With Me.Pleading.PleadingPaper
            Me.txtFooterText = Me.txtCaseTitle
            .DocTitle = Me.txtFooterText
        End With
    End If
    DoEvents
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtFooterText_GotFocus()
    OnControlGotFocus txtFooterText
End Sub

Private Sub txtFooterText_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim xMsg As String
    Select Case KeyCode
        Case vbKeyTab, vbKeyBack, vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
        
        Case Else
            If Me.FooterTextMaxLength = 0 Then Exit Sub
            If Len(txtFooterText) = Me.FooterTextMaxLength Then
                xMsg = "The footer text is" & Str(Me.FooterTextMaxLength) & " characters and might not fit in the footer table cell." & _
                    vbLf & vbLf & "Please edit the text in the footer text box."
                MsgBox xMsg, vbInformation, App.Title
            End If
    End Select
End Sub

Private Sub txtFooterText_LostFocus()
    On Error GoTo ProcError
    Me.Pleading.PleadingPaper.DocTitle = Me.txtFooterText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtJudgeName_LostFocus()
    On Error GoTo ProcError
    Me.Signature.JudgeName = txtJudgeName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtJudgeName_GotFocus()
    OnControlGotFocus txtJudgeName, "zzmpSignatureJudgeName_1"
End Sub

Private Sub txtSignerName_LostFocus()
    '---9.7.1 - 4081
    On Error GoTo ProcError
    Me.Signature.Signer = Me.txtSignerName
    Me.Signature.InputSigner = Me.txtSignerName
    Me.Signature.DisplayName = Me.txtSignerName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerName_GotFocus()
    OnControlGotFocus txtSignerName, "zzmpSignatureSigner_1"
End Sub

Private Sub txtOriginalCourtTitle_BeforeItemAdded(xItem As String)

''---this is california-centric at this point
''---try the second one if you're dealing with Appeals Courts/ district lists from other states
'    If Me.Pleading.Level0String = "California" Then
'        If Me.Pleading.Level1String = "U.S. Court of Appeals" Then
'            If InStr(xItem, "District") = 0 Then
'                xItem = xItem & " District of California"
'                'xItem = xItem & "District of " & Me.Pleading.Level0String
'            End If
'        End If
'    Else
'        If Me.Pleading.Level1String = "Appellate Court" Then
'            xItem = xItem & " County, Illinois," & vbCrLf & "County Department, Law Division"
'        End If
'    End If

    '---9.4.1
    Dim xPrefillText As String
    
    Me.txtOriginalCourtTitle.Text = ""
    
    With Me.Pleading.Captions(1)
        If InStr(xItem, "None") Then
            xItem = mpBlankLine10
        End If
        xPrefillText = .Definition.OnAppealFromPrefillText
        If InStr(.Definition.OnAppealFromPrefillText, "%*") <> 0 Then
            xPrefillText = xSubstitute(.Definition.OnAppealFromPrefillText, "%*", xItem)
        End If
        xItem = xPrefillText
    End With
    '---end 9.4.1


End Sub

Private Sub txtOriginalCourtTitle_GotFocus()
    OnControlGotFocus txtOriginalCourtTitle, "zzmpAppealFromCourtTitle_1"
End Sub

Private Sub txtOriginalCourtTitle_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.AppealFromCourtTitle = txtOriginalCourtTitle.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPresidingJudge_GotFocus()
    OnControlGotFocus txtPresidingJudge, "zzmpPresidingJudge_1"
End Sub

Private Sub txtPresidingJudge_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.PresidingJudge = txtPresidingJudge
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtParty1Name_GotFocus()
    OnControlGotFocus txtParty1Name, "zzmpParty1Name_1"
End Sub
Private Sub txtParty1Name_LostFocus()
    On Error GoTo ProcError
    With Me.PleadingCaption
       '---9.4.1
        Me.PleadingCaption.Party1Name = txtParty1Name
        .DisplayName = CaptionDisplayName
        '---End 9.4.1
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtParty2Name_GotFocus()
    OnControlGotFocus txtParty2Name, "zzmpParty2Name_1"
End Sub
Private Sub txtParty2Name_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.Party2Name = txtParty2Name
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtParty3Name_GotFocus()
    OnControlGotFocus txtParty3Name, "zzmpParty3Name_1"
End Sub

Private Sub txtParty3Name_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.Party3Name = txtParty3Name
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbParty1Title_GotFocus()
    OnControlGotFocus cmbParty1Title, "zzmpParty1Title" & "_1"
End Sub
Private Sub cmbParty1Title_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.Party1Title = cmbParty1Title.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbParty2Title_GotFocus()
    OnControlGotFocus cmbParty2Title, "zzmpParty2Title" & "_1"
End Sub

Private Sub cmbParty2Title_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.Party2Title = cmbParty2Title.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Sub UpdateForm()
    Dim xTemp As String
    Dim xARDateTime As XArray
    Dim xTitle As String
    
    On Error GoTo ProcError
     '---get properties, load controls on reuse
    With Pleading
        
        Me.txtDocumentTitle = .DocumentTitle
        '---handle split court title
        If Me.Pleading.CourtTitle1 = "" Then
            Me.txtCourtTitle = .CourtTitle
        Else
            xTitle = Pleading.CourtTitle
            If Pleading.CourtTitle1 <> "" Then
                xTitle = xTitle & vbCrLf & Pleading.CourtTitle1
            End If
            If Pleading.CourtTitle2 <> "" Then
                xTitle = xTitle & vbCrLf & Pleading.CourtTitle2
            End If
            If Pleading.CourtTitle3 <> "" Then
                xTitle = xTitle & vbCrLf & Pleading.CourtTitle3
            End If
            Me.txtCourtTitle = xTitle
        End If
        DoEvents
        
        '---9.7.1 - 4025
        Me.chkIncludeCoverPage.value = Abs(CInt(Me.Pleading.GenerateCoverPage))
        
        If Not Me.Counsel Is Nothing Then
            With Me.Counsel
                Me.txtClientName = .ClientPartyName
                '---9.5.0
                Me.cmbAttorneysFor.BoundText = .ClientPartyTitle
            End With
        Else
            With Me.Signature
                Me.txtClientName = .PartyName
                '---9.5.0
                Me.cmbAttorneysFor.BoundText = .PartyTitle
            End With
        End If
        
        With Me.PleadingCaption
'---If the .datetimeinfo is empty we're gonna reload the grid with defaults
'---you may not want to do this
'            xTemp = xSubstitute(.DateTimeInfo, vbLf, "")
'            xTemp = xSubstitute(.DateTimeInfo, vbTab, "")
'            xTemp = xSubstitute(.DateTimeInfo, .Definition.DateTimeInfoSeparator & "|", "|")
'---9.4.0
            xTemp = xSubstitute(.DateTimeInfo, vbLf, "")
            xTemp = xSubstitute(xTemp, vbTab, "")
            xTemp = xSubstitute(xTemp, .Definition.DateTimeInfoSeparator & "|", "|")
            xTemp = xSubstitute(xTemp, Chr(11), "|")
'---end 9.4.0
            If xTemp <> "" Then
                Me.grdDateTime.Array = xarStringToxArray(xTemp, 2, mpCapDateTimeSeparator)
            Else
'---initialize date/time grid
                With .Definition
                    Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                    With grdDateTime
                        .Array = xARDateTime
                    End With
                End With
            End If
        End With
            
        If Not Me.PleadingCaption2 Is Nothing Then
'---If the .datetimeinfo is empty we're gonna reload the grid with defaults
'---you may not want to do this
            If PleadingCaption2.DateTimeInfo & PleadingCaption2.Definition.DateTimeInfoString <> "" Then
            
                With PleadingCaption2
                    xTemp = xSubstitute(.DateTimeInfo, vbLf, "")
                    xTemp = xSubstitute(xTemp, vbTab, "")
                    xTemp = xSubstitute(xTemp, .Definition.DateTimeInfoSeparator & "|", "|")
                    xTemp = xSubstitute(xTemp, Chr(11), "|")
                    If xTemp <> "" Then
                        Me.grdDateTime.Array = xarStringToxArray(xTemp, 2, mpCapDateTimeSeparator)
                    Else
        '---initialize date/time grid
                        With .Definition
                            Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                            With grdDateTime
                                .Array = xARDateTime
                            End With
                        End With
                    End If
                End With
            
            End If
        
        End If
        
        Me.grdDateTime.Rebind
        
        With Me.PleadingCaption
            Me.grdCaseNumbers.Array = CaseNumberArray(True)
            Me.grdCaseNumbers.Rebind
            
            '---9.5.0
            Me.cmbParty1Title.BoundText = .Party1Title
            Me.txtParty1Name = .Party1Name
            Me.cmbParty2Title.BoundText = .Party2Title
            Me.txtParty2Name = .Party2Name
            Me.cmbParty3Title.BoundText = .Party3Title
            '---end 9.5.0
            
            Me.txtParty3Name = .Party3Name
            Me.txtCaseTitle = .CaseTitle
            Me.txtPresidingJudge = .PresidingJudge
            Me.txtOriginalCourtTitle.Text = .AppealFromCourtTitle
            If .Definition.IncludesOnAppealFromCourt Then
                LoadAppealFromCourtsList
            End If
        End With
        
        With .PleadingPaper
            If Me.txtFooterText.Visible Then _
                Me.txtFooterText = .DocTitle
        End With
        
        With Me.Signature
            Me.saGrid.Signers = .Signers
            If Me.saGrid.Author <> Me.Pleading.Author.ID Then _
                Me.saGrid.Author = Me.Pleading.Author.ID
            If .Dated = "" Then
                Me.cmbDateType.Text = "None"
            Else
                Me.cmbDateType.Text = .Dated
            End If
            Me.txtJudgeName = .JudgeName
            Me.cmbSignatureType.BoundText = .TypeID
            '---9.7.1 - 4081
            If .Definition.DisplaySignerName = True Then
                Me.txtSignerName = .InputSigner
            End If
        End With
        
        'GLOG : 5718 : ceh
        Me.cmbNoticeAttorney.BoundText = Me.Pleading.NoticeAttorney
        DoEvents
        If Me.cmbNoticeAttorney.Text = "" Then
            Me.cmbNoticeAttorney.SelectedItem = 0
        End If
        
'---set menu items
'        Me.mnuOther_BlueBack.Checked = .BlueBack
'        Me.mnuOther_TitlePage.Checked = .CoverPage
        
        DoEvents
        Me.chkIncludeClosingParagraph.Visible = Me.Signature.Definition.AllowClosingParagraph
        If Me.Pleading.Signatures.ClosingParagraph = mpTrue Then
            m_bSigChanged = True
        End If
        Me.chkIncludeClosingParagraph.value = Abs(Me.Pleading.Signatures.ClosingParagraph)
        m_bSigChanged = False
    End With
    Me.AuthorIsDirty = False
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.UpdateForm"), _
              g_oError.Desc(Err.Number)
    m_bSigChanged = False
    Exit Sub
    
End Sub

Private Sub saGrid_MenuManageListsClick()
'   get current selection

    Dim lID As Long

    On Error Resume Next
    lID = Me.saGrid.Author
    On Error GoTo ProcError

    If lID Then
        m_oMenu.ManageAuthors Me.saGrid, lID
    Else
        m_oMenu.ManageAuthors Me.saGrid
    End If
    
    saGrid.Attorneys = g_oMPO.Attorneys.ListSource
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_MenuAuthorFavoriteClick()
    Dim lID As Long

    On Error Resume Next
    lID = Me.saGrid.Author
    On Error GoTo ProcError

    If lID Then
        m_oMenu.ToggleFavorite Me.saGrid, lID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub saGrid_MenuSetDefaultClick()
    On Error GoTo ProcError
    With Pleading
        .Template.DefaultAuthor = g_oMPO.People(saGrid.Author)
        .Document.StatusBar = "Default Author set to " & .Template.DefaultAuthor.FullName
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub vsIndexTab1_Click()
    On Error GoTo ProcError
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            Me.saGrid.SetFocus
        Case 1
            Me.grdCaseNumbers.SetFocus
    End Select

    Exit Sub
ProcError:
    Exit Sub
End Sub
Private Function CaseNumberArray(Optional bLoadValues As Boolean = False) As XArray
    Dim xarTemp As XArray
    Dim i As Integer
    
    On Error GoTo ProcError
    Set xarTemp = g_oMPO.NewXArray
    xarTemp.ReDim 0, -1, 0, 1
    If bLoadValues = False Then
        bLoadValues = Pleading.Document.IsCreated
    End If
    
    With Me.PleadingCaption.Definition
            If .CaseNumber1Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber1Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption.CaseNumber1
                End If
            End If
            If .CaseNumber2Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber2Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption.CaseNumber2
                End If
            End If
            If .CaseNumber3Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber3Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption.CaseNumber3
                End If
            End If
            If .CaseNumber4Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber4Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption.CaseNumber4
                End If
            End If
            If .CaseNumber5Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber5Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption.CaseNumber5
                End If
            End If
            If .CaseNumber6Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber6Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption.CaseNumber6
                End If
            End If
            Me.Caption1CaseNumberCount = xarTemp.UpperBound(1) + 1
    
    End With
    
    
'---9.4.1
    '---we're gonna piggyback 2nd cap case number prefills/values into case number array
    If Pleading.Definition.IsMultiParty And Not Me.PleadingCaption2 Is Nothing Then
    
        With Me.PleadingCaption2.Definition
                If .CaseNumber1Label <> "" Then
                    xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                    xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber1Label
                    If bLoadValues Then
                        xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption2.CaseNumber1
                    End If
                End If
                If .CaseNumber2Label <> "" Then
                    xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                    xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber2Label
                    If bLoadValues Then
                        xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption2.CaseNumber2
                    End If
                End If
                If .CaseNumber3Label <> "" Then
                    xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                    xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber3Label
                    If bLoadValues Then
                        xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption2.CaseNumber3
                    End If
                End If
                If .CaseNumber4Label <> "" Then
                    xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                    xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber4Label
                    If bLoadValues Then
                        xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption2.CaseNumber4
                    End If
                End If
                If .CaseNumber5Label <> "" Then
                    xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                    xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber5Label
                    If bLoadValues Then
                        xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption2.CaseNumber5
                    End If
                End If
                If .CaseNumber6Label <> "" Then
                    xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                    xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber6Label
                    If bLoadValues Then
                        xarTemp(xarTemp.UpperBound(1), 1) = Me.PleadingCaption2.CaseNumber6
                    End If
                End If
                
                Me.Caption2CaseNumberCount = xarTemp.UpperBound(1) + 1 - Me.Caption1CaseNumberCount
        
        End With
    
    End If
'---end 9.4.1
    
    
    
    
    Set CaseNumberArray = xarTemp
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.CaseNumberArray"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Function bIncludeEmptyDateTime() As Boolean
    On Error GoTo ProcError
    bIncludeEmptyDateTime = mpbase2.bIniToBoolean(GetMacPacIni("Pleading", "IncludeEmptyDateTime"))
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.bIncludeEmptyDateTime"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function
Private Function xDateTimeString() As String
    Dim xTemp As String
    Dim i As Integer
    Dim xSep As String
    Dim xLabelSep As String
    '---9.4.1
    Dim xLabelSep2 As String
    Dim bDateTimeInfo2 As Boolean
    
    If Not Me.PleadingCaption2 Is Nothing Then
        If Me.PleadingCaption2.Definition.DateTimeInfoString <> "" Then
            bDateTimeInfo2 = True
        End If
    End If
    
    '---9.4.0
    
    On Error GoTo ProcError
    xSep = mpCapDateTimeSeparator
    xLabelSep = Me.PleadingCaption.Definition.DateTimeInfoSeparator
    '---end 9.4.0
    
    '---9.4.1
    If bDateTimeInfo2 Then
        xLabelSep = Me.PleadingCaption2.Definition.DateTimeInfoSeparator
    End If
    '---End 9.4.1
    
   
    If Not bIncludeEmptyDateTime Then
        With grdDateTime.Array
            For i = 0 To .UpperBound(1)
                '  Only include if value is entered or if entire row is blank
                '  (meaning an extra line should be included before next entry
                ' in caption)
                If .value(i, 0) & .value(i, 1) = "" Then
                    '---handle spacer rows inside array of labels with no values
                    '---9.4
                    If xTemp <> "" Then
                        xTemp = xTemp & xSep _
                            & vbLf & xSep
                    End If
                ElseIf .value(i, 1) <> "" Then
                    xTemp = xTemp & .value(i, 0) & xLabelSep & xSep & _
                        xTrimTrailingChrs(.value(i, 1), Chr(10)) & vbLf & xSep
                End If
            Next i
            xDateTimeString = xTrimTrailingChrs(xTemp, vbLf & xSep)
        End With
    Else
        '---9.4.0
'        xDateTimeString = xTrimTrailingChrs(XArrayToString(Me.grdDateTime.Array, xLabelSep & xSep, , True, Chr(11)), vbLf) & xSep
        xDateTimeString = xTrimTrailingChrs(XArrayToString(Me.grdDateTime.Array, xLabelSep & xSep, , True, vbLf & xSep), vbLf) & xSep
        '---end 9.4.0
    End If
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.xDateTimeString"), _
              g_oError.Desc(Err.Number)
    Exit Function

End Function
Private Function PositionControls()
    On Error GoTo ProcError
    Dim sOffset As Single
'---9.4.1
    Dim sOff2 As Single

'---adjust this if you want more or less space between controls on second tab
    Const sIncrement As Single = 200
    
    'GLOG : 5718 : ceh
    '---first tab -- this is hard coded for now
    If Me.PleadingCaption.Definition.PartiesInCaption < 3 Then
        With Me
            .lblAttorneys.Top = 180
            .saGrid.Top = 135
            If Me.lblNoticeAttorney.Visible Then
                .lblCourtTitle.Top = 1260
                .txtCourtTitle.Top = 1260
                
                .lblNoticeAttorney.Top = 2350
                .cmbNoticeAttorney.Top = 2350
                
                .lblAttorneysFor.Top = 2770
                .cmbAttorneysFor.Top = 2770
                
                .lblClientName.Top = 3090
                .txtClientName.Top = 3090
                
                .lblParty1Name.Top = 3800
                .cmbParty1Title.Top = 3800
                .txtParty1Name.Top = 4115
                
                .lblParty2Name.Top = 4980
                .cmbParty2Title.Top = 4980
                .txtParty2Name.Top = 5300
            Else
                .lblCourtTitle.Top = 1320
                .txtCourtTitle.Top = 1320
                .lblAttorneysFor.Top = 2565
                .cmbAttorneysFor.Top = 2520
                .lblClientName.Top = 2835
                .txtClientName.Top = 2835
                .lblParty1Name.Top = 3720
                .cmbParty1Title.Top = 3690
                .txtParty1Name.Top = 4005
                .lblParty2Name.Top = 4980
                .cmbParty2Title.Top = 4950
                .txtParty2Name.Top = 5265
            End If
            .lblParty3Name.Top = .txtParty2Name.Top
            .cmbParty3Title.Top = .cmbParty2Title.Top
            .txtParty3Name.Top = .txtParty2Name.Top
        End With
    Else
        With Me
            .lblAttorneys.Top = 120
            .saGrid.Top = 75
            If Me.lblNoticeAttorney.Visible Then
                .lblCourtTitle.Top = 1150
                .txtCourtTitle.Top = 1150
                .txtCourtTitle.Height = 760
                            
                .lblNoticeAttorney.Top = 1970
                .cmbNoticeAttorney.Top = 1970

                .lblAttorneysFor.Top = 2335
                .cmbAttorneysFor.Top = 2335
                
                .lblClientName.Top = 2650
                .txtClientName.Top = 2650
                
                .lblParty1Name.Top = 3270
                .cmbParty1Title.Top = 3240
                .txtParty1Name.Top = 3555
                
                .lblParty2Name.Top = 4200
                .cmbParty2Title.Top = 4170
                .txtParty2Name.Top = 4485
                
                .lblParty3Name.Top = 5100
                .cmbParty3Title.Top = 5100
                .txtParty3Name.Top = 5415
            Else
                .lblCourtTitle.Top = 1260
                .txtCourtTitle.Top = 1260
                .lblAttorneysFor.Top = 2325
                .cmbAttorneysFor.Top = 2280
                .lblClientName.Top = 2595
                .txtClientName.Top = 2595
                .lblParty1Name.Top = 3270
                .cmbParty1Title.Top = 3240
                .txtParty1Name.Top = 3555
                .lblParty2Name.Top = 4200
                .cmbParty2Title.Top = 4170
                .txtParty2Name.Top = 4485
                .lblParty3Name.Top = 5100
                .cmbParty3Title.Top = 5100
                .txtParty3Name.Top = 5415
            End If
        End With
    End If


'---second tab
    With Me
        sOffset = .grdCaseNumbers.Top + grdCaseNumbers.Height + sIncrement

'---If both case title and doc title display, you might have to resize controls or adjust sIncrement constant
        If .txtCaseTitle.Visible = True Then
            .lblCaseTitle.Top = sOffset
            .txtCaseTitle.Top = sOffset
            sOffset = sOffset + .txtCaseTitle.Height + sIncrement
        End If
        
        If .txtDocumentTitle.Visible = True Then
            .lblDocumentTitle.Top = sOffset
            .txtDocumentTitle.Top = sOffset
            sOffset = sOffset + .txtDocumentTitle.Height + sIncrement
        End If

'---if all three display, you may have to resize these controls!!!
        If .txtFooterText.Visible = True Then
            .lblFooterText.Top = sOffset
            .txtFooterText.Top = sOffset
            sOffset = sOffset + .txtFooterText.Height + sIncrement
        End If
        
        If .grdDateTime.Visible = True Then
            .lblDateTime.Top = sOffset
            .grdDateTime.Top = sOffset
            sOffset = sOffset + .grdDateTime.Height + sIncrement
        End If
        
        If .txtOriginalCourtTitle.Visible = True Then
            .lblOriginalCourtTitle.Top = sOffset
            .txtOriginalCourtTitle.Top = sOffset
            '---this is a mlc set to the same height as .txtDocumentTitle.  mcl height = text box + dropdown
            sOffset = sOffset + .txtDocumentTitle.Height + sIncrement
        End If
        
        If .txtPresidingJudge.Visible = True Then
            .lblPresidingJudge.Top = sOffset
            .txtPresidingJudge.Top = sOffset
            sOffset = sOffset + .txtPresidingJudge.Height + sIncrement
        End If
        
        '---9.7.1 - 4025
        If .chkIncludeCoverPage.Visible = True Then
            .chkIncludeCoverPage.Top = sOffset - 100
            sOffset = sOffset - 100 + .chkIncludeCoverPage.Height + sIncrement
        End If
        
        
        .lblSignatureType.Top = sOffset
        .cmbSignatureType.Top = sOffset
        sOffset = sOffset + cmbSignatureType.Height + sIncrement
        
        .lblSignatureDate.Top = sOffset
        .cmbDateType.Top = sOffset
        sOffset = sOffset + cmbDateType.Height + sIncrement
        
'---9.4.1
        If .cmbOneOf.Visible = True Then
            .lblOneOf.Top = sOffset
            .cmbOneOf.Top = sOffset
            sOffset = sOffset + cmbOneOf.Height + sIncrement
        End If

        If .chkIncludeClosingParagraph.Visible = True Then
            .chkIncludeClosingParagraph.Top = sOffset - 100
            sOffset = sOffset - 100 + .chkIncludeClosingParagraph.Height + sIncrement
        End If
        
'---End 9.4.1
        
        '---9.7.1 - 4081
        If .txtJudgeName.Visible = True Then
            .lblJudgeName.Top = sOffset
            .txtJudgeName.Top = sOffset
            sOffset = sOffset + .txtJudgeName.Height + sIncrement
        End If
        
        .lblSignerName.Top = sOffset
        .txtSignerName.Top = sOffset
        '---end 4081
    
    
    End With
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.PositionControls"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function


Private Function DisplayControls(Optional bReload As Boolean = False)
    '---9.4.1
    
    On Error GoTo ProcError
    
    Dim bRet As Boolean
    Dim lRet As Long
    Dim xCap As String
    Dim xLC As String
    Static sHeight As Single
    Static sgH As Single
    '---9.4.1
    Dim bCaption2Exists As Boolean
    Dim iParties As Integer
    '---End 9.4.1
    
    With Me.Pleading
        
        bRet = .Definition.RequiresFooter
        Me.txtFooterText.Visible = bRet
        Me.lblFooterText.Visible = bRet
        
        '---9.7.1 4067
        If .Counsels.Count > 0 Then
            saGrid.DisplayEMail = .Counsels(1).Definition.AllowSignerEmailInput
        Else
            '---9.7.1 4188
            If .Signatures.Count > 0 Then _
                saGrid.DisplayEMail = .Signatures(1).Definition.AllowSignerEmailInput
        End If
        
        On Error GoTo ProcError
        lRet = .Definition.FooterTextMaxChars
        If lRet > 0 Then
            Me.txtFooterText.MaxLength = lRet
            Me.FooterTextMaxLength = lRet
        End If
        
        bRet = Me.PleadingCaption.Definition.IncludesCaseTitle
        '---9.4.1
        If Me.Pleading.Definition.IsMultiParty = True And Not Me.PleadingCaption2 Is Nothing Then
            bCaption2Exists = True
            bRet = Me.PleadingCaption2.Definition.IncludesCaseTitle
        End If
        '---End 9.4.1
        Me.txtCaseTitle.Visible = bRet
        Me.lblCaseTitle.Visible = bRet
        
        bRet = .Definition.RequiresDocTitle
        Me.txtDocumentTitle.Visible = bRet
        Me.lblDocumentTitle.Visible = bRet
        If Me.PleadingCaption.Definition.IncludesCaseTitle Then
            Me.lblDocumentTitle.Caption = "Document Title &2:"
        Else
            Me.lblDocumentTitle.Caption = "&Document Title:"
        End If
        
        '---9.4.1
        
        If bCaption2Exists Then
            iParties = Max(CDbl(Me.PleadingCaption.Definition.PartiesInCaption), _
                    CDbl(Me.PleadingCaption2.Definition.PartiesInCaption))
        Else
            iParties = Me.PleadingCaption.Definition.PartiesInCaption
        End If
        
        Select Case iParties
        'Select Case Me.PleadingCaption.Definition.PartiesInCaption
        '---End 9.4.1
            Case 0
               Me.lblParty1Name.Visible = False
                Me.cmbParty1Title.Visible = False
                Me.txtParty1Name.Visible = False
                Me.lblParty2Name.Visible = False
                Me.txtParty2Name.Visible = False
                Me.cmbParty2Title.Visible = False
                Me.lblVersus.Visible = False
                Me.lblVersus1.Visible = False
                Me.lblVersus2.Visible = False
                Me.lblParty3Name.Visible = False
                Me.txtParty3Name.Visible = False
                Me.cmbParty3Title.Visible = False
            Case 1
                Me.lblParty2Name.Visible = False
                Me.txtParty2Name.Visible = False
                Me.cmbParty2Title.Visible = False
                Me.lblVersus.Visible = False
                Me.lblVersus1.Visible = False
                Me.lblVersus2.Visible = False
                Me.lblParty3Name.Visible = False
                Me.txtParty3Name.Visible = False
                Me.cmbParty3Title.Visible = False
                '---9.4.1
                If bCaption2Exists Then
                    If PleadingCaption2.Definition.Party1TitleDefault = "None" And _
                            PleadingCaption.Definition.Party1TitleDefault = "None" Then
                        Me.cmbParty1Title.Enabled = False
                    End If
                Else
                    Me.cmbParty1Title.Enabled = Not PleadingCaption.Definition.Party1TitleDefault = "None"
                End If
                '---End 9.4.1
            Case 2
                Me.lblParty2Name.Visible = True
                Me.txtParty2Name.Visible = True
                Me.cmbParty2Title.Visible = True
                Me.lblVersus.Visible = True
                Me.lblVersus1.Visible = False
                Me.lblVersus2.Visible = False
           
                Me.lblParty3Name.Visible = False
                Me.txtParty3Name.Visible = False
                Me.cmbParty3Title.Visible = False
           
                '---9.4.1
                If bCaption2Exists Then
                    If PleadingCaption2.Definition.Party1TitleDefault = "None" And _
                            PleadingCaption.Definition.Party1TitleDefault = "None" Then
                        Me.cmbParty1Title.Enabled = False
                    End If
                    If PleadingCaption2.Definition.Party2TitleDefault = "None" And _
                            PleadingCaption.Definition.Party2TitleDefault = "None" Then
                        Me.cmbParty2Title.Enabled = False
                    End If
                Else
                    Me.cmbParty1Title.Enabled = Not (PleadingCaption.Definition.Party1TitleDefault = "None")
                    Me.cmbParty2Title.Enabled = Not (PleadingCaption.Definition.Party2TitleDefault = "None")
                End If
                '---End 9.4.1
           
           Case 3
                Me.lblParty2Name.Visible = True
                Me.txtParty2Name.Visible = True
                Me.cmbParty2Title.Visible = True
                Me.lblVersus.Visible = False
                Me.lblVersus1.Visible = True
                Me.lblVersus2.Visible = True
                
                Me.lblParty3Name.Visible = True
                Me.txtParty3Name.Visible = True
                Me.cmbParty3Title.Visible = True
        
                '---9.4.1
                If bCaption2Exists Then
                    If PleadingCaption2.Definition.Party1TitleDefault = "None" And _
                            PleadingCaption.Definition.Party1TitleDefault = "None" Then
                        Me.cmbParty1Title.Enabled = False
                    End If
                    If PleadingCaption2.Definition.Party2TitleDefault = "None" And _
                            PleadingCaption.Definition.Party2TitleDefault = "None" Then
                        Me.cmbParty2Title.Enabled = False
                    End If
                    If PleadingCaption2.Definition.Party3TitleDefault = "None" And _
                            PleadingCaption.Definition.Party3TitleDefault = "None" Then
                        Me.cmbParty3Title.Enabled = False
                    End If
                Else
                    Me.cmbParty1Title.Enabled = Not (PleadingCaption.Definition.Party1TitleDefault = "None")
                    Me.cmbParty2Title.Enabled = Not (PleadingCaption.Definition.Party2TitleDefault = "None")
                    Me.cmbParty3Title.Enabled = Not (PleadingCaption.Definition.Party3TitleDefault = "None")
                End If
                '---End 9.4.1
        End Select
        

        bRet = False
        If bCaption2Exists Then
            bRet = Me.PleadingCaption2.Definition.IncludesDateTimeInfo
        End If
        
        If bRet = True Then
            Me.grdDateTime.Visible = bRet
        Else
             Me.grdDateTime.Visible = Me.PleadingCaption.Definition.IncludesDateTimeInfo
        End If
        Me.lblDateTime.Visible = grdDateTime.Visible

'        Me.txtOriginalCourtTitle.Visible = Me.PleadingCaption.Definition.IncludesOnAppealFromCourt
        bRet = False
        If bCaption2Exists Then
            bRet = Me.PleadingCaption2.Definition.IncludesOnAppealFromCourt
        End If
        
        If bRet = True Then
            Me.txtOriginalCourtTitle.Visible = bRet
        Else
             Me.txtOriginalCourtTitle.Visible = Me.PleadingCaption.Definition.IncludesOnAppealFromCourt
        End If
        Me.lblOriginalCourtTitle.Visible = Me.txtOriginalCourtTitle.Visible
        
'        Me.txtPresidingJudge.Visible = Me.PleadingCaption.Definition.IncludesPresidingJudge
        bRet = False
        If bCaption2Exists Then
            bRet = Me.PleadingCaption2.Definition.IncludesPresidingJudge
        End If
        
        If bRet = True Then
            Me.txtPresidingJudge.Visible = bRet
        Else
             Me.txtPresidingJudge.Visible = Me.PleadingCaption.Definition.IncludesPresidingJudge
        End If
        Me.lblPresidingJudge.Visible = Me.txtPresidingJudge.Visible

'---9.4.1
        Me.lblOneOf.Visible = Me.Signature.Definition.AllowOneOfSignerText
        Me.cmbOneOf.Visible = Me.Signature.Definition.AllowOneOfSignerText
'---End 9.4.1
        
        Me.chkIncludeClosingParagraph.Visible = Me.Signature.Definition.AllowClosingParagraph

'---End 9.4.1

'---set case numbers grid & label
'---control height is adjusted here so 2 case nos display 2 lines each, 3 displays one line each
'---adjust case 2 if you want 2 line display for 3 case nos, etc.
        With grdCaseNumbers
            If sHeight = 0 Then
                sHeight = .RowHeight
            End If
            If sgH = 0 Then
                sgH = .Height
            End If
            .Array = CaseNumberArray(bReload)
            .Rebind
            Select Case .Array.UpperBound(1)
                Case 0, -1
                    .Height = sgH
                    xLC = "Case &Number:"
                    .RowHeight = .Height - 15
                Case 1
                    .Height = 890
                    xLC = "Case &Numbers:"
                    .RowHeight = (.Height / 2) - 15
                Case 2
                    .Height = 890
                    xLC = "Case &Numbers:"
                    .RowHeight = (.Height / 3) - 15
                Case Else
                    .Height = sgH
                    xLC = "Case &Numbers:"
                    .RowHeight = sHeight
            End Select
            lblCaseNumber.Caption = xLC
        End With

'---set menu items
        Me.mnuOther_CoverPage.Visible = .Definition.AllowsBlueBack
        
        '---9.7.1 - 4025 unrem if you need this for back compatibility
        'Me.mnuOther_TitlePage.Visible = .Definition.AllowsCoverPage
        Me.chkIncludeCoverPage.Visible = .Definition.AllowsCoverPage
        '---end 9.7.1
        
        Me.mnuOther_CrossActions.Visible = .Definition.AllowCrossAction
        
        '---9.5.0
        'Me.mnuOther_Counsels.Visible = .Definition.AllowCoCounsel
        '---End 9.5.0
        Me.mnuOther_ChangeFont.Visible = .Definition.AllowsFontChange
        
        'GLOG : 5718 : ceh
        Me.cmbNoticeAttorney.Visible = .Definition.NoticeAttorney
        Me.lblNoticeAttorney.Visible = .Definition.NoticeAttorney

'---set the dbox caption
        xCap = Me.Caption
        If InStr(xCap, " - ") Then
            xCap = Left(xCap, InStr(xCap, " - ") - 1)
        End If
        Me.Caption = xCap & " - " & .Level0String
    End With
    
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.DisplayControls"), _
              g_oError.Desc(Err.Number)
    Exit Function

'---End 9.4.1
End Function

Public Sub SetInterfaceControls()
    Dim xARDateTime As XArray
    Dim oCaption As MPO.CPleadingCaption
    Dim oOff As COffice
    Dim lFilter As Long
    Dim oList As CList
    Dim xTitle As String
    
    On Error Resume Next
'---set display
    DisplayControls
'adjust control position based on above
    PositionControls
    
'   set office
    If g_oMPO.UseDefOfficeInfo Then
        Set oOff = g_oMPO.Offices.Default
    Else
        Set oOff = Pleading.Author.Office
    End If
    
    If Not Me.Pleading.Document.IsCreated Then
'---set defaults for lists/combos
        With Me.Pleading
            Set oCaption = Me.PleadingCaption
            
            '---handle split court title
            If .CourtTitle1 <> "" Then
                xTitle = .CourtTitle & vbCrLf & .CourtTitle1
                If .CourtTitle2 <> "" Then
                    xTitle = xTitle & vbCrLf & .CourtTitle2
                End If
                If .CourtTitle3 <> "" Then
                    xTitle = xTitle & vbCrLf & .CourtTitle3
                End If
                Me.txtCourtTitle = xTitle
            Else
                Me.txtCourtTitle = .CourtTitle
            End If
            
            
            If Not Me.Counsel Is Nothing Then
                Me.cmbAttorneysFor.BoundText = Me.Counsel.Definition.ClientTitleDefault
                '---set default position for counsel type
                Me.Counsel.Position = Counsel.Definition.DefaultPosition
            Else
                If Not Me.Signature Is Nothing Then
                    Me.cmbAttorneysFor.BoundText = oCaption.Definition.Party1TitleDefault
                End If
            End If
            
            With oCaption.Definition
                '---9.4.1
                If .Party1Label <> "" Then _
                    Me.lblParty1Name.Caption = .Party1Label
                If .Party2Label <> "" Then _
                    Me.lblParty2Name.Caption = .Party2Label
                If .Party3Label <> "" Then _
                    Me.lblParty3Name.Caption = .Party3Label
            
                '---End 9.4.1
                Me.cmbParty1Title.BoundText = .Party1TitleDefault
                Me.cmbParty2Title.BoundText = .Party2TitleDefault
                Me.cmbParty3Title.BoundText = .Party3TitleDefault
                '---handle non-0 tab condition (this should not be the case on init, since .currtab set to 0 immediately
                '---before call to SetInterfaceControls
                If Me.vsIndexTab1.CurrTab = 0 Then
                    Me.saGrid.SetFocus
                    OnControlGotFocus Me.saGrid
                End If
'---load mlc dropdown for appellate pleading original court title
                If .IncludesOnAppealFromCourt Then
                    LoadAppealFromCourtsList
                End If
            End With
            
'---initialize date/time grid
            With Me.PleadingCaption.Definition
                Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                With grdDateTime
                    .Array = xARDateTime
                    .Rebind
                End With
            End With
            
            '---9.4.1 -- note that for now we are only gonna support one date time array per pleading main
            
            Dim bDateTimeInfo2 As Boolean
            If Not Me.PleadingCaption2 Is Nothing Then
                If PleadingCaption2.Definition.IncludesDateTimeInfo Then
                    bDateTimeInfo2 = True
                End If
            End If
            
            If bDateTimeInfo2 = True Then
                With .Definition
                    If bDateTimeInfo2 = True Then
                        Set xARDateTime = Nothing
                        Set xARDateTime = xarStringToxArray(PleadingCaption2.Definition.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                    End If
                    With grdDateTime
                        .Array = xARDateTime
                        .Rebind
                    End With
                End With
            End If


'            With Me.Signature
'                .Position = .Definition.DefaultPosition
'                .FirmName = oOff.FirmNameUpperCase
'                .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
'                .FirmPhone = oOff.Phone1
'                .FirmFax = oOff.Fax1
'                .FirmCity = oOff.City
'                .FirmState = oOff.State
'                .SignerEMail = Me.Pleading.Author.Email
'            End With
            
            Me.cmbDateType.BoundText = Me.Signature.Definition.DateFormat
            DoEvents
            Me.cmbSignatureType.BoundText = .Definition.DefaultSignatureType
            '---9.7.1 - 4081
            If Signature.Definition.DisplaySignerName = True Then
                txtSignerName = Signature.InputSigner
            End If
            
            '---9.4.1
            
            '---9.7.1 - 4112
            Me.cmbOneOf.SelectedItem = 0
        End With
    Else
   
    End If
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.SetInterfaceControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


Private Sub UpdateObjectDefaults()
    Dim xTemp As String
    Dim i As Integer
    Dim oOff As mpDB.COffice
    Dim xSep As String
    Dim xRep As String
    Dim xARDateTime As XArray
    
'---set pleading properties based on preloaded values
'---necessary because we don't prewrite default values for
'---the template
'---runs on new doc create or court change from main db only

    On Error GoTo ProcError
        
    If g_oMPO.DynamicEditing Then
        g_oMPO.ForceItemUpdate = True
            With Me.Pleading
                If Not m_bCourtChanged Then
'---9.6.2
                    .CourtTitle = .CourtTitle
'---split court title
                    .CourtTitle1 = .CourtTitle1
                    .CourtTitle2 = .CourtTitle2
                    .CourtTitle3 = .CourtTitle3
                End If
                '---delete rebuild arguments for sub objects if for any reason you want to rebuild bp table on creation
                '---i.e, .refresh xxx.key reloads bp table into doc.  This ensures loading default types into doc if mbp
                '---does not match db type settings
                
                '---9.4.1
                With Me.PleadingCaption
                    .CourtTitle = Pleading.CourtTitle
                    .CourtTitle1 = Pleading.CourtTitle1
                    .CourtTitle2 = Pleading.CourtTitle2
                    .CourtTitle3 = Pleading.CourtTitle3
                End With
                
                If Pleading.Definition.IsMultiParty And Not Me.PleadingCaption2 Is Nothing Then
                    With Me.PleadingCaption2
                        .CourtTitle = Pleading.CourtTitle
                        .CourtTitle1 = Pleading.CourtTitle1
                        .CourtTitle2 = Pleading.CourtTitle2
                        .CourtTitle3 = Pleading.CourtTitle3
                    End With
                End If
                '---end 9.4.1
                
                Me.PleadingCaption.RefreshOnInitialize False, False
                '---9.4.1
                If Pleading.Definition.IsMultiParty And Not Me.PleadingCaption2 Is Nothing Then
                    Me.PleadingCaption2.RefreshOnInitialize False, True
                End If
                '---End 9.4.1
                                
                If Not Me.Counsel Is Nothing Then
                    If .Definition.DefaultCounselType <> 0 Then _
                            Me.Counsel.RefreshOnInitialize
                End If
                'Me.Signature.RefreshOnInitialize False, False
                'Me.Signature.Refresh False, False
           End With
    End If
    
    Application.ScreenUpdating = False
    
'---set oOff variable according to Macpac.ini pref
    If g_oMPO.UseDefOfficeInfo Then
        Set oOff = g_oMPO.Offices.Default
    Else
        Set oOff = Me.Pleading.Author.Office
    End If
    
    With Me.PleadingCaption
        .Party1Title = .Definition.Party1TitleDefault
        .Party2Title = .Definition.Party2TitleDefault
        .Party3Title = .Definition.Party3TitleDefault
        .DisplayName = .Definition.Description & " - " & .Party1Title

'---xsub level 3 string with placeholder for appellate
        If Len(.Definition.OnAppealFromPrefillText) Then
            If Me.Pleading.Level3 <> 0 Then
                xRep = Me.Pleading.Level3String
            Else
                xRep = ""
            End If
            
            '---9.4.1
            Dim xPrefillText As String
            xPrefillText = .Definition.OnAppealFromPrefillText
            If InStr(.Definition.OnAppealFromPrefillText, "%*") <> 0 Then
                xPrefillText = xSubstitute(.Definition.OnAppealFromPrefillText, "%*", mpBlankLine10)
            End If
            Me.txtOriginalCourtTitle.Text = xSubstitute(xPrefillText, "%1", xRep)
            '---end 9.4.1
            
            'Me.txtOriginalCourtTitle.Text = xSubstitute(.Definition.OnAppealFromPrefillText, "%1", xRep)
            
            .AppealFromCourtTitle = txtOriginalCourtTitle.Text
        End If
    
'---load date time grid with defaults if empty & visible
        If m_bCourtChanged Then
            '---9.4.1 -- note that for now we are only gonna support one date time array per pleading main
            
            Dim bDateTimeInfo2 As Boolean
            If Not Me.PleadingCaption2 Is Nothing Then
                If PleadingCaption2.Definition.IncludesDateTimeInfo Then
                    bDateTimeInfo2 = True
                End If
            End If
            
            If .Definition.IncludesDateTimeInfo Or bDateTimeInfo2 = True Then
                With .Definition
                    If bDateTimeInfo2 = True Then
                        Set xARDateTime = xarStringToxArray(PleadingCaption2.Definition.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                    Else
                        Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                    End If
                    With grdDateTime
                        .Array = xARDateTime
                        .Rebind
                    End With
                End With
            End If
        End If
        '---End 9.4.1
        
        For i = 0 To Me.grdCaseNumbers.Array.UpperBound(1)
            xTemp = grdCaseNumbers.Array.value(i, 1)
            Select Case i
                Case 0
                    .CaseNumber1 = xTemp
                Case 1
                    .CaseNumber2 = xTemp
                Case 2
                    .CaseNumber3 = xTemp
                Case 3
                    .CaseNumber4 = xTemp
                Case 4
                    .CaseNumber5 = xTemp
                Case 5
                    .CaseNumber6 = xTemp
            End Select
        Next i
        .DateTimeInfo = xDateTimeString
        .RefreshEmpty
        
        '---9.4.0
        .FirmName = oOff.FirmName
        '---9.7.1 - 4030
        If .Definition.OfficeAddressFormat = 0 Then
            .FirmAddress = xTrimTrailingChrs(oOff.AddressOLD(vbLf, _
                                                 mpOfficeAddressFormat_CountryIfForeign), vbLf)
        Else
            .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
        End If
        
        
        .FirmFax = oOff.Fax1
        .FirmPhone = oOff.Phone1
        .SignerEMail = Pleading.Author.EMail
    
        With saGrid
            '---for now we'll assume caption bar ID config = sigs
            .IncludeBarID = Signature.Definition.IncludeBarID
            .BarIDPrefix = Signature.Definition.BarIDPrefix
            .BarIDSuffix = Signature.Definition.BarIDSuffix
            xSep = .Separator
            xSep = .Separator
            PleadingCaption.OtherSigners = xSubstitute(.OtherSignersText, xSep, vbLf)
            PleadingCaption.Signer = .SignerText
        End With
    
    End With
       

'---9.4.1

    If Not Me.PleadingCaption2 Is Nothing Then
        With Me.PleadingCaption2
            .Party1Title = .Definition.Party1TitleDefault
            .Party2Title = .Definition.Party2TitleDefault
            .Party3Title = .Definition.Party3TitleDefault
            .DisplayName = .Definition.Description & " - " & .Party1Title
    
    '---xsub level 3 string with placeholder for appellate
            If Len(.Definition.OnAppealFromPrefillText) Then
                If Me.Pleading.Level3 <> 0 Then
                    xRep = Me.Pleading.Level3String
                Else
                    xRep = ""
                End If
                Me.txtOriginalCourtTitle.Text = xSubstitute(.Definition.OnAppealFromPrefillText, "%1", xRep)
                .AppealFromCourtTitle = txtOriginalCourtTitle.Text
            End If
        
    '---load date time grid with defaults if empty & visible
            If m_bCourtChanged Then
                If .Definition.IncludesDateTimeInfo Then
                    With .Definition
                        Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                        With grdDateTime
                            .Array = xARDateTime
                            .Rebind
                        End With
                    End With
                End If
            End If
            
'---recode this to compare prefill/labels in cap(1) to those in cap(2) -- we'll use setinterfacecontrols to load grid w/
'---cap1 prefills plus cap2 prefills

'            For i = 0 To Me.grdCaseNumbers.Array.UpperBound(1)
'                xTemp = grdCaseNumbers.Array.Value(i, 1)
'                Select Case i
'                    Case 0
'                        .CaseNumber1 = xTemp
'                    Case 1
'                        .CaseNumber2 = xTemp
'                    Case 2
'                        .CaseNumber3 = xTemp
'                    Case 3
'                        .CaseNumber4 = xTemp
'                    Case 4
'                        .CaseNumber5 = xTemp
'                    Case 5
'                        .CaseNumber6 = xTemp
'                End Select
'            Next i
            
            .DateTimeInfo = xDateTimeString
            .RefreshEmpty
            
            '---9.4.0
            .FirmName = oOff.FirmName
            
            '---9.7.1 - 4030
            If .Definition.OfficeAddressFormat = 0 Then
                .FirmAddress = xTrimTrailingChrs(oOff.AddressOLD(vbLf, _
                                                     mpOfficeAddressFormat_CountryIfForeign), vbLf)
            Else
                .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
            End If
            
            .FirmFax = oOff.Fax1
            .FirmPhone = oOff.Phone1
            .SignerEMail = Pleading.Author.EMail
        
            With saGrid
                '---for now we'll assume caption bar ID config = sigs
                .IncludeBarID = Signature.Definition.IncludeBarID
                .BarIDPrefix = Signature.Definition.BarIDPrefix
                .BarIDSuffix = Signature.Definition.BarIDSuffix
                xSep = .Separator
                PleadingCaption.OtherSigners = xSubstitute(.OtherSignersText, xSep, vbLf)
                PleadingCaption.Signer = .SignerText
            End With
        
        End With

    End If

'---End 9.4.1

'---9.4.0
    If Me.Signature.Definition.ShowFirmID = True Then
        Me.Pleading.FirmID = oOff.FirmID
    Else
        Me.Pleading.FirmID(mpTrue) = ""
    End If
    
    If Not Me.Counsel Is Nothing Then
        With Me.Counsel
 '---set signers grid with bar id props from def
            '---9.7.1 - 4035
            .Office = oOff
            
            '---9.6.2
            saGrid.IncludeSignerEmail = .Definition.IncludeSignerEmail
            saGrid.SignerEmailPrefix = .Definition.SignerEmailPrefix
            '---9.7.1 4105
            saGrid.SignerEmailSuffix = .Definition.SignerEmailSuffix
            '---9.7.1 4096
            saGrid.SignerNameCase = .Definition.SignerNameCase
            
            saGrid.IncludeBarID = .Definition.IncludeBarID
            saGrid.BarIDPrefix = .Definition.BarIDPrefix
            saGrid.BarIDSuffix = .Definition.BarIDSuffix
            .ClientPartyTitle = .Definition.ClientTitleDefault
            
             '---9.7.1 - 4030
            If .Definition.OfficeAddressFormat = 0 Then
                .FirmAddress = xTrimTrailingChrs(oOff.AddressOLD(vbLf, _
                                                     mpOfficeAddressFormat_CountryIfForeign), vbLf)
            Else
                .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
            End If
           
            .FirmName = oOff.FirmName
            .FirmSlogan = oOff.Slogan
            .FirmSlogan2 = oOff.Slogan
            .FirmFax = oOff.Fax1
            .FirmPhone = oOff.Phone1
            .DisplayName = Me.Pleading.Author.FullName & "-" & oOff.FirmName
            .SignerEMail = Pleading.Author.EMail
            .HasMatchingSignature = True
            
            Set .Signers = Me.saGrid.Signers
            '---9.7.1 4104
            .SignersEmail = saGrid.SignersEmailText
            With saGrid
                xSep = .Separator
                If .SignersText = "" Then
                    xTemp = .SignerText
                Else
                    xTemp = .SignersText
                End If
                xTemp = xSubstitute(xTemp, xSep, vbLf)
            End With
            .AttorneyNames = xTemp
            .RefreshEmpty
        End With
    End If
    
        
    With Me.Signature
        '---9.6.2
        Dim bForce As Boolean
        bForce = g_oMPO.ForceItemUpdate
        g_oMPO.ForceItemUpdate = False
        .DynamicUpdating = False
        
        '---9.7.1 - 4035
        .Office = oOff
        With saGrid
 '---set signers grid with bar id props from def
            
            '---9.7.1 4188
            .IncludeSignerEmail = Signature.Definition.IncludeSignerEmail
            .SignerEmailPrefix = Signature.Definition.SignerEmailPrefix
            .SignerEmailSuffix = Signature.Definition.SignerEmailSuffix
            
            
            '---9.7.1 4097 - set this via Sig Def property
            .SignerNameCase = Signature.Definition.SignerNameCase
            
            .IncludeBarID = Signature.Definition.IncludeBarID
            .BarIDPrefix = Signature.Definition.BarIDPrefix
            .BarIDSuffix = Signature.Definition.BarIDSuffix
            xSep = .Separator
            Signature.OtherSigners = xSubstitute(.OtherSignersText, xSep, vbLf)
            '---9.7.1 - 4081
            Signature.Signer = .SignerText
            If Signature.Definition.DisplaySignerName = True Then
                Signature.InputSigner = .SignerText
            End If
            
            '---9.4.1
            Signature.Signer2 = .SignerText
            '---9.7.1 - es
            saGrid.IncludeSignerEmail = False
            saGrid.IncludeBarID = False
            Signature.ESigner = saGrid.SignerText
            saGrid.IncludeSignerEmail = Signature.Definition.IncludeSignerEmail
            saGrid.IncludeBarID = Signature.Definition.IncludeBarID
            
            Signature.AllSigners = xSubstitute(.SignersText, xSep, vbLf)
        End With
        
        Me.chkIncludeClosingParagraph.Visible = .Definition.AllowClosingParagraph
        Set .Signers = Me.saGrid.Signers
                
        .Position = .Definition.DefaultPosition
        
        '---9.7.1 - 4082
        .FirmName = oOff.FirmName
        
        '---9.9.1 - 4954
        .FirmNameESig = oOff.FirmName
        
        .FirmSlogan = oOff.Slogan   '*c
        '---9.7.1 - 4030
        If .Definition.OfficeAddressFormat = 0 Then
            .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
        Else
            .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
        End If
            
        
        .FirmPhone = oOff.Phone1
        .FirmFax = oOff.Fax1
        .FirmCity = oOff.City
        .FirmState = oOff.State
        '---9.4.0
        If .Definition.ShowFirmID = True Then
            .FirmID = oOff.FirmID
        Else
            .FirmID(mpTrue) = ""
        End If
        
        .DisplayName = g_oMPO.People(saGrid.Author).FullName
        
        .SignerEMail = Me.Pleading.Author.EMail
       
        If Me.Counsel Is Nothing Then
            .PartyTitle = Me.PleadingCaption.Definition.Party1TitleDefault
        Else
            .PartyTitle = Me.Counsel.ClientPartyTitle
        End If
        
        .DynamicUpdating = g_oMPO.DynamicEditing
        g_oMPO.ForceItemUpdate = bForce
       .Refresh False, False
    End With
    
    If g_oMPO.ForceItemUpdate = True Then g_oMPO.ForceItemUpdate = False
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.UpdateObjectDefaults"), _
              g_oError.Desc(Err.Number)
    If g_oMPO.ForceItemUpdate = True Then g_oMPO.ForceItemUpdate = False
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub CreatePleadingSignatures(Optional bCalledFromCounsel As Boolean = False)
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    On Error GoTo ProcError

'---disable timer to avoid backcolor conflicts
    m_bOtherFormDisplayed = True
    DoEvents
    
    With Me.Signature
        If Me.saGrid.Author <> -1 Then
            '---9.7.1 4096
            If InStr(.Signer, ",") Then
                .DisplayName = StrConv(Left(.Signer, InStr(.Signer, ",") - 1), vbProperCase)
                
            Else
                .DisplayName = StrConv(.Signer, vbProperCase)
            End If
        Else
            .DisplayName = "Default Signature - No Signer Selected"
        End If
        
    End With
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If
    
    With frmPleadingSignature
        .ParentForm = Me
        .PleadingType = Me.Pleading.Definition
        .Signatures = Me.Pleading.Signatures
        Me.Signature.DynamicUpdating = False
        '---9.5
        If Me.Counsel Is Nothing Or bCalledFromCounsel = True Then
            .Caption = "Create A Pleading Counsel/Co-Counsel/Signature"
        End If        '---end 9.5
        .Show vbModal
        DoEvents
        If Not .Cancelled Then
            On Error Resume Next
            '---9.5.1
            '---make sure to refresh pleading form w/ first item in sig col after working w/ sigs
            Me.Signature = .Signatures.Item(1)
           
            '---end 9.5.1
            Unload frmPleadingSignature
            Set frmPleadingSignature = Nothing
            
            'GLOG : 5594|5381
            'turn off accelerator cues if necessary
            If bRestoreNoCues Then _
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
            
            With Me.Pleading
                If Not Me.Signature Is Nothing Then
                    '---refresh form
                    With Me.Signature
                        Me.cmbDateType.Text = .Dated
                        '---disable date type control if fullrowformat = false and .datetype = ""
                        If .Dated = "" Then
                            Me.cmbDateType.Text = "None"
                            Me.cmbDateType.Enabled = False
                        Else
                            Me.cmbDateType.Enabled = True
                        End If
                        Me.Initializing = True
                        Me.cmbSignatureType.BoundText = .TypeID
                        DoEvents
                        Me.Initializing = False
                        Me.txtJudgeName = .JudgeName
                        Me.cmbAttorneysFor.Text = .PartyTitle
                        Me.txtClientName = .PartyName
                        Me.saGrid.Signers = .Signers
''''                        '---9.4.1
''''                        saGrid_LostFocus
''''                        '---end 9.4.1
                        Set Me.Counsel.Signers = .Signers
                        '---reset display name for counsel
                        Dim xFN As String
                        If .FirmName <> "" Then
                            xFN = " - " & .FirmName
                        Else
                            xFN = ""
                        End If
                        '---9.7.1 4096
                        Me.Counsel.DisplayName = StrConv(saGrid.SignerText, vbProperCase) & xFN
                        '---9.5
                        saGrid_LostFocus
                        '---end 9.5
                        
                        m_bSigChanged = True
                        Me.chkIncludeClosingParagraph = Abs(Me.Pleading.Signatures.ClosingParagraph)
                        Me.chkIncludeClosingParagraph.Enabled = .Definition.AllowClosingParagraph
                        If Me.chkIncludeClosingParagraph.value = vbChecked Then
                            EchoOff
                            Me.Pleading.Signatures.RefreshClosingParagraph
                            EchoOn
                        End If
                        m_bSigChanged = False
                    End With
                End If
            End With
        Else
            Me.Pleading.Signatures.CancelRemove
            Unload frmPleadingSignature
            Set frmPleadingSignature = Nothing
            'GLOG : 5594|5381
            'turn off accelerator cues if necessary
            If bRestoreNoCues Then _
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

        End If
    End With
    Me.Signature.DynamicUpdating = g_oMPO.DynamicEditing
'---reenable backcolor timer
'    Me.Timer1.Enabled = True
    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

'    Me.Timer1.Enabled = True
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub CreatePleadingCaptions()
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
   
    On Error GoTo ProcError

    m_bOtherFormDisplayed = True
    DoEvents
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If
    
    With frmPleadingCaption
        .ParentForm = Me
        .PleadingType = Me.Pleading.Definition
        .PleadingCaptions = Me.Pleading.Captions
        .PleadingCaption = Me.PleadingCaption
        Me.PleadingCaption.DynamicUpdating = False
        .Show vbModal
        DoEvents
        If Not .Cancelled Then
            On Error Resume Next
            '---refresh pleading controls with updated items
            DisplayControls True
            PositionControls
            Me.cmbParty1Title.Text = .PleadingCaptions(1).Party1Title
            Me.cmbParty2Title.Text = .PleadingCaptions(1).Party2Title
            Me.cmbParty3Title.Text = .PleadingCaptions(1).Party3Title
            Me.txtParty1Name.Text = .PleadingCaptions(1).Party1Name
            Me.txtParty2Name.Text = .PleadingCaptions(1).Party2Name
            Me.txtParty3Name.Text = .PleadingCaptions(1).Party3Name
            '---9.4.1
            With .PleadingCaptions(1).Definition
                If .Party1Label <> "" Then
                    Me.lblParty1Name.Caption = .Party1Label
                Else
                    Me.lblParty1Name.Caption = Me.lblParty1Name.Tag
                End If
                If .Party2Label <> "" Then
                    Me.lblParty2Name.Caption = .Party2Label
                Else
                    Me.lblParty2Name.Caption = Me.lblParty2Name.Tag
                End If
                If .Party3Label <> "" Then
                    Me.lblParty3Name.Caption = .Party3Label
                Else
                    Me.lblParty3Name.Caption = Me.lblParty3Name.Tag
                End If
            
            End With
            
            If .PleadingCaptions(1).DateTimeInfo = "" Then
                If .PleadingCaptions.Count > 1 Then
                    
                    If .PleadingCaptions(2).DateTimeInfo <> "" Then
                    
                        With .PleadingCaptions(2)
                            Dim xTemp As String
                            Dim xARDateTime As XArray
                            
                            '---9.4.0
    '                        xTemp = xSubstitute(.DateTimeInfo, vbLf, "")
    '                        xTemp = xSubstitute(.DateTimeInfo, vbTab, "")
    '                        xTemp = xSubstitute(.DateTimeInfo, .Definition.DateTimeInfoSeparator & "|", "|")
                            xTemp = xSubstitute(.DateTimeInfo, vbLf, "")
                            xTemp = xSubstitute(xTemp, vbTab, "")
                            xTemp = xSubstitute(xTemp, .Definition.DateTimeInfoSeparator & "|", "|")
                            xTemp = xSubstitute(xTemp, Chr(11), "|")
                            '---end 9.4.0
                            
                            If xTemp <> "" Then
                                Me.grdDateTime.Array = xarStringToxArray(xTemp, 2, mpCapDateTimeSeparator)
                            Else
                '---initialize date/time grid
                                With .Definition
                                    Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                                    With grdDateTime
                                        .Array = xARDateTime
                                    End With
                                End With
                            End If
                        End With
                        grdDateTime.Rebind
                
                    End If
                
                End If
            
            End If
            '---End 9.4.1
            Unload frmPleadingCaption
            Set frmPleadingCaption = Nothing
        Else
            Me.Pleading.Captions.CancelRemove
            Unload frmPleadingCaption
            Set frmPleadingCaption = Nothing
        End If
    
        'GLOG : 5594|5381
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    End With
    Me.PleadingCaption.DynamicUpdating = g_oMPO.DynamicEditing
    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    g_oError.Show Err
    Exit Sub

End Sub

Private Sub CreatePleadingCounsels()
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    On Error GoTo ProcError

    m_bOtherFormDisplayed = True
    DoEvents
    
    '---9.4.1
    If Me.saGrid.Author = -1 Then
        If Me.Counsel.DisplayName = "" Then
            Me.Counsel.DisplayName = "Default Counsel - No Signer Selected"
        End If
    End If
    '---End 9.4.1
    
    'GLOG : 5594|5381
    'force accelerator cues in Word 2013 & newer
    If g_bIsWord15x Then
        lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
        If lKeyboardCues = 0 Then
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
            bRestoreNoCues = True
        End If
    End If

    With frmPleadingCounsel
        .ParentForm = Me
        .PleadingType = Me.Pleading.Definition
        .Counsels = Me.Pleading.Counsels
        .Counsel = Me.Counsel
        Me.Counsel.DynamicUpdating = False
        .Show vbModal
        DoEvents
        If Not .Cancelled Then
            On Error Resume Next
            '---refresh pleading controls with updated items
            '---9.4.1
            If .Counsel.Definition.AttorneyRequired Then
                Me.cmbAttorneysFor.Text = .Counsels(1).ClientPartyTitle
                Me.txtClientName = .Counsels(1).ClientPartyName
                Me.saGrid.Signers = .Counsels(1).Signers
            End If
            '---call lost focus sub to refresh counsels and sigs if .Counsels(1) was changed in interface
'            EchoOff
'            Me.Pleading.Counsels.Refresh
'            EchoOn
            saGrid_LostFocus
            '---End 9.4.1
            Unload frmPleadingCounsel
            Set frmPleadingCounsel = Nothing
        Else
            Me.Pleading.Counsels.CancelRemove
            Unload frmPleadingCounsel
            Set frmPleadingCounsel = Nothing
        End If
        
        'GLOG : 5594|5381
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
        
    End With
    Me.Counsel.DynamicUpdating = g_oMPO.DynamicEditing
    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    g_oError.Show Err
    Exit Sub

End Sub

Public Sub InsertCoverPage()
    '---runs a variation of MPO.InsertSegmentE
    Dim lID As Long
    Dim i As Integer
    Dim oSegDefs As mpDB.CSegmentDefs
    Dim oSegDef As mpDB.CSegmentDef
    Dim oTDefs As MPO.CTemplates
    Dim oTemplate As MPO.CTemplate
    Dim oCPTemplate As MPO.CTemplate
    Dim oDB As mpDB.CDatabase
    Dim oSeg As MPO.CSegment
    Dim oDoc As MPO.CDocument
    Dim xTest As String
    Dim xCPBM As String
    
'---2.28  temporary bailout if cp already exists in pleading pending development of
'---reuseactivedocumentsegment method that will allow modification of already existing
'---cp attached to pleading
    On Error GoTo ProcError
    Set oDoc = New CDocument
    xCPBM = "zzmpFIXED_PleadingCoverPage"
    If oDoc.bHideFixedBookmarks Then
        xCPBM = "_" & xCPBM
    End If
        
    If ActiveDocument.Bookmarks.Exists(xCPBM) Then
        MsgBox "You cannot create more than 1 cover page/litigation back from the More button menu.  Please finish your pleading, then run Insert Into Current Document from the MacPac menu to create additional Cover Pages.", vbInformation, App.Title
        Exit Sub
    End If
    m_bOtherFormDisplayed = True
    '---9.7.1 - 4131
    Set g_ParentForm = Me
    Set oDB = g_oMPO.db
    Set oTDefs = g_oMPO.Templates
    
    Set oCPTemplate = oTDefs.ItemFromClass("CPleadingCoverPage")
    Set oTemplate = oTDefs.ItemFromClass("CPleading")
    Set oSegDefs = oDB.SegmentDefs(oTemplate.ID)
    
    For i = 1 To oSegDefs.Count
        Set oSegDef = oSegDefs.ItemAtIndex(CLng(i))
        '---seg def sourcetemplate prop is same as template.ID prop, i.e. "pleadingcoverpage.dot"
        If oSegDef.SourceTemplate = oCPTemplate.ID Then
            '---grab the actual fldID from related table
            '---this can be used to create a new doc segment without using segment user interface
            lID = oSegDef.ID
            Exit For
        End If
        Set oSegDef = Nothing
    Next
    
    If Not oSegDef Is Nothing Then
        Set oSeg = g_oMPO.NewDocumentSegment(lID)
        If Not oSeg Is Nothing Then
            Dim iLoc As Double
            With oSeg
                
                '---9.6.1
                iLoc = Max(iLoc, 1)
                
                .Location = iLoc
                '---end 9.6.1
                On Error GoTo ProcError

                .UseExistingPageSetup = False
                g_oMPO.InsertingSegment = True  '9.7.1 #4310
                .Insert
                g_oMPO.InsertingSegment = False  '9.7.1 #4310
            End With
        End If
    End If

    '---9.7.1 - 4131
    Me.PleadingCaption.RetrieveVariables = True
    UpdateForm
    g_oMPO.ForceItemUpdate = True

Bypass:
    '---reset bleeding bookmark (bug fix)
    If ActiveDocument.Bookmarks.Exists("zzmpCourtTitle") Then  ' ***New
        With ActiveDocument.Bookmarks("zzmpCourtTitle")
            .End = .End
            .Start = .Range.Paragraphs.Last.Range.Start
        End With
    End If

    With Me.Pleading
        .CourtTitle = .CourtTitle
        .CourtTitle1 = .CourtTitle1
        .CourtTitle2 = .CourtTitle2
    End With
    With Me.PleadingCaption
        .CaseNumber1 = .CaseNumber1
        .CaseNumber2 = .CaseNumber2
        .CaseNumber3 = .CaseNumber3
        .CaseNumber4 = .CaseNumber4
        .CaseNumber5 = .CaseNumber5
        .CaseNumber6 = .CaseNumber6
        .CourtTitle = .CourtTitle
        .CourtTitle1 = .CourtTitle1
        .CourtTitle2 = .CourtTitle2
        .CourtTitle3 = .CourtTitle3
        .DateTimeInfo = .DateTimeInfo
        .CaseTitle = .CaseTitle
        .Party1Name = .Party1Name
        .Party1Title = .Party1Title
        .Party2Name = .Party2Name
        .Party2Title = .Party2Title
        .Party3Name = .Party3Name
        .Party3Title = .Party3Title
        .CaseTitle = .CaseTitle
    End With

    '---force focus
    If Me.vsIndexTab1.CurrTab = 0 Then
        Me.txtCourtTitle.SetFocus
    Else
        Me.grdCaseNumbers.SetFocus
    End If
    EchoOn
    '---9.7.1 - 4131
    Set g_ParentForm = Nothing
    Exit Sub
ProcError:
    EchoOn
    Application.ScreenRefresh
    g_oError.Show Err
    '---9.7.1 - 4131
    Set g_ParentForm = Nothing
    Exit Sub
End Sub


Public Sub ChangePleadingType()
    Dim bRet As Boolean
'    Dim oPleading As MPO.CPleading
    Dim lL0 As Long
    Dim lL1 As Long
    Dim lL2 As Long
    Dim lL3 As Long
    Dim bDyn As Boolean

'---redisplays court types dialog while main pleading dialog is up
'---if accepted, creates new pleading objects, loads new bp & refreshes w/ previous values
    
    On Error GoTo ProcError
    
'---prompt for levels
    bRet = g_oMPO.GetPleadingLevels(lL0, lL1, lL2, lL3)

    If bRet Then
'           user did not cancel levels dlg
        
        '---9.7.1 -4173 - get rid of all sigs, captions and cross actions
        Dim iSigCount As Integer
        Dim iCapCount As Integer
        Dim i As Integer
        Dim lRet As VbMsgBoxResult
        
        iCapCount = Me.Pleading.Captions.Count
        iSigCount = Me.Pleading.Signatures.Count
        
        '---9.7.1 -4173 - get rid of all captions and cross actions
        If iCapCount + iSigCount > 2 Or Me.Pleading.Counsels.Count > 1 Then
        
            lRet = MsgBox("Using the Change Court feature requires rebuilding the existing Co-Counsels, Signatures, and Cross-Actions segments with those defined for the newly chosen court. Additional counsel, signature and cross-actions added for the original pleading will need to be recreated.  Click OK to continue changing courts, or Cancel to return to the existing pleading.", vbInformation + vbOKCancel, App.Title)
            If lRet <> vbOK Then
                Exit Sub
            End If
        
        End If
        
        '---pleading.initialize adds one sig
        Me.Pleading.Initialize lL0, lL1, lL2, lL3, True
        
        If Me.Pleading.Counsels.Count > 1 Then
            For i = Me.Pleading.Counsels.Count To 2 Step -1
                Me.Pleading.Counsels.Remove (i)
            Next i
            Me.Pleading.Counsels.ClearCopiedDocVars
        End If
        
        If Me.Pleading.Signatures.Count > 1 Then
            For i = Me.Pleading.Signatures.Count To 2 Step -1
                Me.Pleading.Signatures.Remove (i)
            Next i
            Me.Pleading.Signatures.ClearCopiedDocVars
        End If
    
        
        If iCapCount > 1 Then
            For i = iCapCount To 2 Step -1
                Me.Pleading.Captions.Remove (i)
            Next i
            Me.Pleading.Captions.ClearCopiedDocVars
        End If
        '---end 4173
    '---9.7.1 3766
    Else
        Exit Sub
    End If

'---turn off screen updating
    Screen.MousePointer = vbHourglass
    Application.ScreenUpdating = False
    m_bCourtChanged = True

    bDyn = g_oMPO.DynamicEditing

'---refresh form-level object props
    With Me.Pleading
        If .Definition.DefaultSignatureType Then
            .Signatures(1).TypeID = .Definition.DefaultSignatureType
            Signature = .Signatures(1)
            Me.Signature.DynamicUpdating = bDyn
        End If
        If .Definition.DefaultCounselType Then
            If .Counsels.Count Then
                .Counsels(1).TypeID = .Definition.DefaultCounselType
                Counsel = .Counsels(1)
                Counsel.Position = Counsel.Definition.DefaultPosition
            Else
                .Counsels.Add
                .Counsels(1).TypeID = .Definition.DefaultCounselType
                Counsel = .Counsels.Item(1)
                Counsel.Position = Counsel.Definition.DefaultPosition
            End If
            Me.Counsel.DynamicUpdating = bDyn
        Else
            '---delete any existing counsel docvars to prevent counsel creation on reuse
            Me.Counsel = Nothing
            Pleading.Document.DeleteSegmentValues "PleadingCounsel"
        End If
        If .Definition.DefaultCaptionType Then
            .Captions(1).TypeID = .Definition.DefaultCaptionType
            PleadingCaption = .Captions(1)
            Me.PleadingCaption.DynamicUpdating = bDyn
        End If
    
'---9.4.1 class action support
        
        If .Definition.DefaultCaptionType2 And .Definition.IsMultiParty Then
            .Captions.Add
            .Captions(2).TypeID = .Definition.DefaultCaptionType2
            .Captions(2).Position = 2
            PleadingCaption2 = .Captions(2)
            Me.PleadingCaption2.DynamicUpdating = bDyn
        End If
        
'---End 9.4.1

    End With

'---recreate bp
'    With oPleading
    With Me.Pleading
         If .Definition.DefaultPleadingPaperType Then
            Set .PleadingPaper.PleadingPaperDef = _
                g_oMPO.db.PleadingPaperDefs.Item(.Definition.DefaultPleadingPaperType)
            If g_oMPO.UseDefOfficeInfo Then
                Set .PleadingPaper.Office = g_oMPO.Offices.Default
            Else
                Set .PleadingPaper.Office = .Author.Office
            End If
            .PleadingPaper.Update , False, False
        End If
        
        '---9.5.0 -- clear only last section if there's a court change
        
        If .Document.IsCreated And g_oMPO.ReuseAction = mpReUseAction_Append Then
            .Document.ClearSection ActiveDocument.Sections.Last, True
        Else
            .Document.ClearContent
        End If
        '---9.5.0 end
        
        .InsertBoilerplate
    
        '---9.5.0
        Me.ChangeFontOnReuse
       
        '---end 9.5.0
    End With
    
'---recreate content, set interface
    LoadAssignedSignatureList
    If Me.PleadingCaption.Definition.IncludesOnAppealFromCourt Then
        LoadAppealFromCourtsList
    End If
    DisplayControls
    PositionControls
    
    '---9.6.2 synch the doc title properties of caption and Pleading
    Dim bForce As Boolean
    
    bForce = g_oMPO.ForceItemUpdate
    
    g_oMPO.ForceItemUpdate = True
    
    Dim bHasPleadingDocTitle As Boolean
    Dim bHasCaptionCaseTitle As Boolean
    
    bHasPleadingDocTitle = Pleading.Definition.RequiresDocTitle
    bHasCaptionCaseTitle = PleadingCaption.Definition.IncludesCaseTitle
    
    If bHasPleadingDocTitle And bHasCaptionCaseTitle Then   '---just refresh props
        Pleading.DocumentTitle = Pleading.DocumentTitle
        PleadingCaption.CaseTitle = PleadingCaption.CaseTitle
    Else
    
        If bHasPleadingDocTitle Then
            If Pleading.DocumentTitle = "" Then
                Pleading.DocumentTitle = PleadingCaption.CaseTitle
                Me.txtDocumentTitle = Pleading.DocumentTitle
            Else
                '---refresh prop
                Pleading.DocumentTitle = Pleading.DocumentTitle
            End If
        ElseIf bHasCaptionCaseTitle Then
            If PleadingCaption.CaseTitle = "" Then
                PleadingCaption.CaseTitle = Pleading.DocumentTitle
                Me.txtCaseTitle = PleadingCaption.CaseTitle
            Else
                '---refresh prop
                PleadingCaption.CaseTitle = PleadingCaption.CaseTitle
            
            End If
        End If
    
    End If
    
    
    g_oMPO.ForceItemUpdate = bForce
    '---end 9.6.2
    
    
    UpdateObjectDefaults
    LoadPreviousValues
    
'---9.6.1 - refresh caption borders
'9.7.2/9.7.1040 #4680
    If Pleading.Definition.DefaultCaptionType Then
       UpdatePleadingBorderSpecial
    End If
    
    
    Me.chkIncludeClosingParagraph = vbUnchecked
    Me.chkIncludeClosingParagraph.Enabled = Me.Signature.Definition.AllowClosingParagraph
    
    Application.ScreenRefresh
    Application.ScreenUpdating = True
    m_bCourtChanged = False
    Screen.MousePointer = vbDefault
        
    Exit Sub

ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.ChangePleadingType"), _
              g_oError.Desc(Err.Number)
    m_bCourtChanged = False
    Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    Exit Sub
End Sub


Public Sub LoadAssignedSignatureList()
    Dim oList As mpDB.CList
    
    On Error GoTo ProcError
    Me.Initializing = True
    With g_oMPO.Lists

'---load sig types list valid for pleading type - filter by ID
        Set oList = .Item("PleadingSignatures")
        With oList
            .FilterField = "tblPleadingSignatureAssignments.fldPleadingTypeID"
            .FilterValue = Me.Pleading.Definition.ID
            .Refresh
            Me.cmbSignatureType.Array = .ListItems.Source
            Me.cmbSignatureType.Rebind
        End With
    End With
    ResizeTDBCombo cmbSignatureType, 4
    Me.Initializing = False
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.LoadAssignedSignatureList"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

'Public Sub LoadAppealFromCourtsList()
'    Dim lFilter As Long
'    Dim oList As CList
'
''---load mlc dropdown for appellate pleading original court title
''---note:  the lFilter IDs are California-specific, this branch may have to be expanded
''---if other states' appeals courts includes onappealfromcourt info
''---lFilter values are IDs from tblPleadingCourtLevel1
'
'    If Me.Pleading.Level0String = "California" Then
'        If Me.Pleading.Level1String = "U.S. Court of Appeals" Then
'            lFilter = 3
'        Else
'            lFilter = 1
'        End If
'
'    ElseIf InStr(Me.Pleading.Level0String, "Carolina") <> 0 Then
'        Exit Sub
'    Else
'        If Me.Pleading.Level1String = "U.S. Court of Appeals" Or _
'                InStr(Me.Pleading.Level1String, "Appeal") <> 0 Or _
'                InStr(Me.Pleading.Level1String, "Supreme") <> 0 Then
'            lFilter = 3
'        Else
'            lFilter = 1
'        End If
'    End If
'
'        With g_oMPO.Lists
'            Set oList = .Item("PleadingCourtsLevel2")
'            With oList
'                .FilterValue = lFilter
'                .Refresh
'                Me.txtOriginalCourtTitle.List = .ListItems.Source
'            End With
'        End With
'
'    Exit Sub
'ProcError:
'    g_oError.Raise Err.Number, _
'              CurErrSource("frmPleading.LoadAppealFromCourtList"), _
'              g_oError.Desc(Err.Number)
'    Exit Sub
'End Sub

Public Sub LoadAppealFromCourtsList()
    Dim lFilter As Long
    Dim oList As CList

'---load mlc dropdown for appellate pleading original court title
'---note:  the lFilter IDs are California-specific, this branch may have to be expanded
'---if other states' appeals courts includes onappealfromcourt info
'---lFilter values are IDs from tblPleadingCourtLevel1
    
    lFilter = Me.Pleading.Captions(1).Definition.OnAppealFromJurisdictionsID
    If lFilter <> 0 Then
        With g_oMPO.Lists
            Set oList = .Item("PleadingCourtsLevel2")
            With oList
                .FilterValue = lFilter
                .Refresh
                Me.txtOriginalCourtTitle.List = .ListItems.Source
            End With
        End With
    End If
   
   
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.LoadAppealFromCourtList"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub



Public Sub LoadPreviousValues()
    Dim oOff As COffice
    Dim xTitle As String
    
'---this routine called from changepleadingtype
'---refreshes props of new pleading type from dbox contents

    On Error GoTo ProcError
    g_oMPO.ForceItemUpdate = True
    
   '---handle split court title
    If Me.Pleading.Definition.CourtTitle1 = "" Then
        Me.txtCourtTitle = Me.Pleading.DefaultCourtTitle
        Me.Pleading.CourtTitle = Me.txtCourtTitle
    Else
        With Me.Pleading
            If .CourtTitle1 <> "" Then
                xTitle = .CourtTitle & vbCrLf & .CourtTitle1
                If .CourtTitle2 <> "" Then
                    xTitle = xTitle & vbCrLf & .CourtTitle2
                End If
                If .CourtTitle3 <> "" Then
                    xTitle = xTitle & vbCrLf & .CourtTitle3
                End If
                Me.txtCourtTitle = xTitle
            Else
                Me.txtCourtTitle = xSubstitute(.DefaultCourtTitle, vbLf, vbCrLf)
            End If
            SplitCourtTitle
        End With
    End If
        
    With Me.Signature
        .Position = .Definition.DefaultPosition
        If g_oMPO.UseDefOfficeInfo Then
            Set oOff = g_oMPO.Offices.Default
        Else
            Set oOff = Me.Pleading.Author.Office
        End If
        '---9.7.1 - 4082
        .FirmName = oOff.FirmName
        
        '---9.9.1 - 4954
        .FirmNameESig = oOff.FirmName
        
        .FirmSlogan = oOff.Slogan   '*c
        '---9.7.1 - 4030
        If .Definition.OfficeAddressFormat = 0 Then
            .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
        Else
            .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
        End If
        
        .FirmPhone = oOff.Phone1
        .FirmFax = oOff.Fax1
        .FirmCity = oOff.City
        .FirmState = oOff.State
        .SignerEMail = Me.Pleading.Author.EMail
        .PartyName = Me.txtClientName
        .PartyTitle = Me.cmbAttorneysFor.Text
    End With
        
    Me.cmbDateType.BoundText = Me.Signature.Definition.DateFormat
    DoEvents
    Me.cmbSignatureType.BoundText = Me.Pleading.Definition.DefaultSignatureType
            
    With Me.PleadingCaption
        .CaseTitle = Me.txtCaseTitle
        .Party1Name = Me.txtParty1Name
        .Party2Name = Me.txtParty2Name
        .Party3Name = Me.txtParty3Name
        .Party1Title = Me.cmbParty1Title.Text
        .Party2Title = Me.cmbParty2Title.Text
        .Party3Title = Me.cmbParty3Title.Text
        .AppealFromCourtTitle = Me.txtOriginalCourtTitle.Text
        .PresidingJudge = Me.txtPresidingJudge
    End With

    If Not Me.Counsel Is Nothing Then
        With Me.Counsel
            .ClientPartyName = Me.txtClientName
            .ClientPartyTitle = Me.cmbAttorneysFor.Text
        End With
    End If
    g_oMPO.ForceItemUpdate = False
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.LoadValues"), _
              g_oError.Desc(Err.Number)
    g_oMPO.ForceItemUpdate = False
    Exit Sub
End Sub

Private Function GridMoveNext(grdCur As TDBGrid, Optional bNextRecord As Boolean = False)
    ' Move to next column or row
    ' Redim array and add blank row if necessary
    On Error Resume Next
    With grdCur
        If .Bookmark < grdCur.Array.UpperBound(1) Then
            '---
            .MoveNext
        ElseIf Not GridCurrentRowIsEmpty(grdCur) Then
            .Update
            .Array.ReDim 0, .Bookmark + 1, 0, .Columns.Count - 1
            .Rebind
            .MoveLast
            .Col = 0
            .EditActive = True
        End If
    End With
End Function

Private Function GridCurrentRowIsEmpty(grdTest As TDBGrid) As Boolean
    Dim i As Integer
    On Error Resume Next
    GridCurrentRowIsEmpty = True
    With grdTest
        For i = 0 To .Columns.Count - 1
            If .Columns(i).Text <> "" Then
                GridCurrentRowIsEmpty = False
                Exit Function
            End If
        Next i
    End With
End Function

Public Function ChangeFont()
    Dim lSize As Long
    Dim xName As String
    Dim oDoc As CDocument
    Dim bUpdate As Boolean
    
    Set oDoc = New MPO.CDocument
    bUpdate = g_oMPO.ForceItemUpdate
    
    With oDoc
        xName = .NormalFontName
        lSize = .NormalFontSize
'       show restricted font & size dialog
        g_oMPO.ChangeDocumentFont True, True
        If xName <> .NormalFontName Then
            g_oMPO.ForceItemUpdate = True
            Me.Pleading.NormalFontName = .NormalFontName
            Application.ScreenRefresh
            g_oMPO.ForceItemUpdate = bUpdate
        End If
        If lSize <> .NormalFontSize Then
            g_oMPO.ForceItemUpdate = True
            Me.Pleading.NormalFontSize = Val(.NormalFontSize)
            Application.ScreenRefresh
            g_oMPO.ForceItemUpdate = bUpdate
        End If
    End With
       
End Function

Public Function ChangeFontOnReuse()
    Dim lSize As Long
    Dim xName As String
    Dim oDoc As CDocument
    
    Set oDoc = New MPO.CDocument
    
    With Me.Pleading
        xName = .NormalFontName
        lSize = .NormalFontSize
        
        If xName <> oDoc.NormalFontName Then
            oDoc.NormalFontName = .NormalFontName
        End If
        If lSize <> oDoc.NormalFontSize And lSize Then
            oDoc.NormalFontSize = .NormalFontSize
        End If
    End With
       
End Function

Public Sub SplitCourtTitle()
    Dim xTemp As String
    Dim xTitle() As String
    Dim iIndex As Integer
    Dim i As Integer
    Dim iRets As Integer
    
    On Error Resume Next
    xTitle() = Split(Me.txtCourtTitle, vbCrLf)
    
    With Pleading
        .CourtTitle = ""
        .CourtTitle1 = ""
        .CourtTitle2 = ""
        .CourtTitle3 = ""
        If UBound(xTitle(), 1) > 0 Then
            .CourtTitle = xTitle(0)
            .CourtTitle1 = xTitle(1)
        End If
        
        If UBound(xTitle(), 1) > 1 Then _
          .CourtTitle2 = xTitle(2)
       
        If UBound(xTitle(), 1) > 2 Then _
          .CourtTitle3 = xTitle(3)
    
        If UBound(xTitle(), 1) > 3 Then
            xTemp = xTitle(3)
            For i = 4 To UBound(xTitle(), 1)
                xTemp = xTemp & vbCrLf & xTitle(i)
            Next i
            .CourtTitle3 = xTemp
        End If
    End With
    '---9.4.1
    With Me.PleadingCaption
        .CourtTitle = Pleading.CourtTitle
        .CourtTitle1 = Pleading.CourtTitle1
        .CourtTitle2 = Pleading.CourtTitle2
        .CourtTitle3 = Pleading.CourtTitle3
    End With
    With Me.PleadingCaption2
        .CourtTitle = Pleading.CourtTitle
        .CourtTitle1 = Pleading.CourtTitle1
        .CourtTitle2 = Pleading.CourtTitle2
        .CourtTitle3 = Pleading.CourtTitle3
    End With
    '---end 9.4.1
End Sub

Private Sub UpdateClosingParagraph(tInclude As mpTriState)
    On Error GoTo ProcError
        With Me.Pleading.Signatures
            .ClosingParagraph = tInclude
            Me.Pleading.Signatures.ClosingParagraphCity = Me.Pleading.Signatures(1).FirmCity
            Me.Pleading.Signatures.ClosingParagraphState = Me.Pleading.Signatures(1).FirmState
            .ClosingParagraphDateText = Me.Signature.Dated
            .RefreshClosingParagraph
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Function CaptionDisplayName() As String
    Dim xP1N As String
    Dim xP1T As String
    
    If Me.cmbParty1Title.Enabled = False Then
        If txtParty1Name <> "" Then
            xP1N = " - " & txtParty1Name
        Else
            xP1N = txtParty1Name
        End If
    Else
        If txtParty1Name <> "" Then
            xP1N = " - " & txtParty1Name
        Else
            xP1N = txtParty1Name
        End If
    
        If cmbParty1Title <> "" Then
            xP1T = " - " & cmbParty1Title.Text
        Else
            xP1T = ""
        End If
    End If

    CaptionDisplayName = Me.PleadingCaption.Definition.Description & xP1T & xP1N

End Function

'GLOG : 5718 : ceh
Private Sub UpdateNoticeAttorneyControl()
    Dim bEnable As Boolean
    bEnable = Me.saGrid.Signers.Count(1) > 1
    Me.lblNoticeAttorney.Enabled = bEnable
    Me.cmbNoticeAttorney.Enabled = bEnable
End Sub
