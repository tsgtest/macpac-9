Attribute VB_Name = "mdlDialog"
Option Explicit

Public Sub GridTabOut(grdGrid As TDBGrid, bEnter As Boolean, iColtoCheck As Integer)
    '---this sub allows a tabout from grid when navigation param is set to grid navigation
    '---normally tabbing halts at last item in grid
    On Error Resume Next
    Select Case bEnter
        Case True
            grdGrid.TabAction = 2
        Case False
            If grdGrid.RowBookmark(grdGrid.Row) = grdGrid.Array.UpperBound(1) Or _
                    grdGrid.RowBookmark(grdGrid.Row) = grdGrid.Array.LowerBound(1) Or _
                    IsNull(grdGrid.RowBookmark(grdGrid.Row)) Then
                If grdGrid.Col = iColtoCheck Then
                    '---change grid navigation to enable tab out
                    grdGrid.TabAction = 1
                End If
            End If
        Case Else
    End Select
End Sub

Public Function OnFormKeyDown(KeyCode As Integer)
    If KeyCode = vbKeyF11 Then
        ScrollDocumentUp
        KeyCode = 0
    ElseIf KeyCode = vbKeyF12 Then
        ScrollDocumentDown
        KeyCode = 0
    End If
End Function

Public Sub ScrollDocumentUp()
    Word.ActiveDocument.ActiveWindow.LargeScroll , 1
End Sub

Public Sub ScrollDocumentDown()
    Word.ActiveDocument.ActiveWindow.LargeScroll 1
End Sub

Public Sub ScrollDocumentToTop()
    Word.ActiveDocument.ActiveWindow.VerticalPercentScrolled = 0
End Sub

Public Sub ScrollDocumentToEnd()
    Word.ActiveDocument.ActiveWindow.VerticalPercentScrolled = 100
End Sub

Public Function SetControlBackColor(oControl As VB.Control)
    Dim lBackColor As Long
    
    DoEvents
    
'   set previous control's backcolor
    On Error Resume Next
    If TypeOf g_oPrevControl Is CheckBox Or _
            TypeOf g_oPrevControl Is OptionButton Or _
            TypeOf g_oPrevControl Is CommandButton Then
        lBackColor = &H8000000F
    Else
        lBackColor = g_lInactiveCtlColor
    End If
    g_oPrevControl.BackColor = lBackColor
    On Error GoTo ProcError
    
    If Not (TypeOf oControl Is CommandButton) Then
        oControl.BackColor = g_lActiveCtlColor
    End If

'   set active control's backcolor
    Set g_oPrevControl = oControl
    
    Exit Function
ProcError:
    Err.Clear
    Exit Function
End Function

Public Function OnControlGotFocus(oCtl As Control, _
                                  Optional ByVal xBookmarkToSelect As String, _
                                  Optional bForceSelection As Boolean = False, _
                                  Optional bLastCharSelectOnly As Boolean = False)
                                  
'called from GotFocus event procedure of controls -
'handles generic got focus requirements
    Static oDoc As MPO.CDocument
    Dim oBmk As Word.Bookmark
    
    On Error GoTo ProcError
    If oDoc Is Nothing Then
        Set oDoc = New MPO.CDocument
    End If
    DoEvents
    bEnsureSelectedContent oCtl, bLastCharSelectOnly, bForceSelection
    SetControlBackColor oCtl
    
    On Error Resume Next
    Set oBmk = Word.ActiveDocument.Bookmarks(xBookmarkToSelect)
    On Error GoTo ProcError
    
    If Not (oBmk Is Nothing) Then
        oDoc.SelectItem xBookmarkToSelect
    Else
        With Word.Application
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    End If
    Exit Function
    
ProcError:
    Exit Function
End Function

Function ControlHasValue(ctlP As Control) As Boolean
'---for some reason, type of isn't returning CheckBox
    If InStr(ctlP.Name, "chk") Or _
        InStr(ctlP.Name, "CheckBox") Then
        ControlHasValue = True
        Exit Function
    End If
  
    If TypeOf ctlP Is TextBox Or _
        TypeOf ctlP Is TrueDBList60.TDBCombo Or _
        TypeOf ctlP Is VB.ComboBox Or _
        TypeOf ctlP Is CheckBox Or _
        TypeOf ctlP Is TrueDBList60.TDBList Or _
        TypeOf ctlP Is VB.ListBox Or _
        TypeOf ctlP Is OptionButton Then
        ControlHasValue = True
    Else
        ControlHasValue = False
    End If
End Function

Function bEnsureSelectedContent(ctlP As VB.Control, _
                                Optional bLastCharOnly As Boolean = False, _
                                Optional bForceSelection As Boolean = False) As Boolean
    Dim ctlData As VB.Data
    
    On Error Resume Next
    
'   does not support spinner textbox;
'   both branches below will result in text being deselected
    If TypeOf ctlP Is SpinTextInternational Then Exit Function
    
    If bForceSelection Then
        With ctlP
            If (TypeOf ctlP Is ListBox) Then
                If .ListIndex < 0 Then _
                    .ListIndex = 0
            Else
                .SelStart = 0
                .SelLength = Len(.Text)
            End If
        End With
    Else
        If IsPressed(9) Then
            With ctlP
                If (TypeOf ctlP Is ListBox) Then
                    If .ListIndex < 0 Then _
                        .ListIndex = 0
                Else
                    If bLastCharOnly = False Then
                        .SelStart = 0
                        .SelLength = Len(.Text)
                    Else
                        .SelStart = Len(.Text)
                        .SelLength = 1
                    End If
                End If
            End With
        End If
    End If
End Function

Function bForceCaps(ctlText As VB.Control) As Boolean
'force uppercase in control ctlText
'useful in change event of any ctl
'having a text property - needed
'because UCase will force repositioning
'of insertion point

    Dim iStartPos As Integer
    
    With ctlText
        iStartPos = .SelStart
        .Text = UCase(.Text)
        .SelStart = iStartPos
    End With
        
End Function

Function IsPressed(lKey As Long) As Boolean
    IsPressed = (GetKeyState(lKey) < 0)
End Function


Sub GetTabOrder(frmP As VB.Form, xControls() As String)
    Dim ctlP As VB.Control
    
    ReDim xControls(frmP.Controls.Count)
    On Error Resume Next
    For Each ctlP In frmP.Controls
        xControls(ctlP.TabIndex) = ctlP.Name
    Next ctlP
End Sub


Public Function DisableInactiveTabs(frmP As VB.Form)
    Dim ctlP As VB.Control
    On Error Resume Next
    With frmP
        For Each ctlP In .Controls
            If (ctlP.Container Is .MultiPage1) Then
               ctlP.Enabled = (ctlP.Left >= 0)
            End If
        Next
    End With
End Function

Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xarP As XArray
    Dim iRows As Integer
    On Error Resume Next
    With tdbCBX
        Set xarP = tdbCBX.Array
        iRows = Min(xarP.Count(1), CDbl(iMaxRows))
        .DropdownHeight = (iRows * .RowHeight)
        If Err.Number = 6 Then
 '---use hard value if there's a processor problem -- err 6 is overflow
            .DropdownHeight = (iRows * 239.811)
        End If
    End With
End Sub

Public Sub MatchInXArrayList(KeyCode As Integer, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'first character is KeyCode
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As XArray
    Dim lStartRow As Long
    Dim xChr As String
    
'   get starting row
    lStartRow = ctlP.Bookmark
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1)
    
'   get the char that we're searching for
    xChr = UCase(Chr(KeyCode))
    
'   loop through xarray until match with first letter is found
    While (xChr <> UCase(Left(xarP.value(lCurRow, 1), 1)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        
    Wend
    
'   select the matched item
    With ctlP
        .Bookmark = lCurRow
        .SelBookmarks.Remove 0
        .SelBookmarks.Add lCurRow
        .SetFocus
    End With
End Sub

Public Sub MatchCompleteInXArrayList(xString As String, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'full string is xString
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As XArray
    Dim lStartRow As Long
    Dim xChrs As String
    
'   start at the top
    lStartRow = 0
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1)
    
'   get the char that we're searching for
    xChrs = UCase(xString)
    
'   loop through xarray until match with first letter is found
    While (xChrs <> UCase(xarP.value(lCurRow, 0)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        
    Wend
    
'   select the matched item
    ctlP.Bookmark = lCurRow
    
End Sub
Public Sub UnderConstruction()
    MsgBox "Sorry, but this feature is under construction!", vbInformation, App.Title
End Sub

Public Sub RefreshPeople(Optional vID As Variant, Optional oAuthorList As Control)
    Screen.MousePointer = vbHourglass
    g_oMPO.People.Refresh
    g_oMPO.Attorneys.Refresh
    If TypeOf oAuthorList Is TrueDBList60.TDBCombo Then
        If Not (oAuthorList Is Nothing) Then
            With oAuthorList
                .Rebind
                .Bookmark = 0
                On Error Resume Next
                If Not IsMissing(vID) Then
                    SelectPerson CLng(vID), oAuthorList
                    DoEvents
                End If
                .SetFocus
            End With
        End If
    ElseIf TypeOf oAuthorList Is mpControls.SigningAttorneysGrid Then
        If Not (oAuthorList Is Nothing) Then
            With oAuthorList
                .Attorneys = g_oMPO.Attorneys.ListSource
                .SetFocus
            End With
        End If
    End If
    Screen.MousePointer = vbDefault
End Sub

Public Sub SelectPerson(ByVal lID As Long, oAuthorList As Control)
'select row with specified ID -
'if ID does not exist, then
'select the first row
    If TypeOf oAuthorList Is TrueDBList60.TDBCombo Then
        With oAuthorList
            .BoundText = lID
            If .Row = -1 Then
                .Row = 0
            End If
        End With
    ElseIf TypeOf oAuthorList Is mpControls.SigningAttorneysGrid Then
'---do nothing if saGrid
    End If
End Sub

Public Sub ShowSuppliedOptions(oOptions As mpDB.CPersonOptions, oForm As Form)
'   fill controls with user options -
'   cycle through flds and assign controls
'   of same name the value of the field
    Dim i As Integer
    Dim ctlP As Control
    Dim xValue As String
    Dim optP As mpDB.CPersonOption
    
    For i = 2 To oOptions.Count
        Set optP = oOptions(i)
'       field name is option set item name
'       with different prefix
        If Not IsNull(optP.value) Then
            xValue = optP.value
        Else
            xValue = xNullToString(optP.value)
        End If
        Set ctlP = Nothing
        
        On Error Resume Next
        Set ctlP = oForm.Controls(optP.Name)
        On Error GoTo 0
        
        If Not (ctlP Is Nothing) Then
            If (TypeOf ctlP Is TrueDBList60.TDBCombo) Then
                ctlP.BoundText = xValue
            ElseIf (TypeOf ctlP Is VB.CheckBox) Then
                If xValue Then
                    ctlP = vbChecked
                Else
                    ctlP = vbUnchecked
                End If
            ElseIf (TypeOf ctlP Is mpControls.SpinnerTextBox) Then
                ctlP.value = xValue
            Else
                ctlP = xValue
            End If
        End If
    Next i
End Sub

Public Sub ShowOptions(oPerson As mpDB.CPerson, xTemplate As String, frmP As Form)
'   fill controls with user options -
'   cycle through flds and assign controls
'   of same name the value of the field
    Dim i As Integer
    Dim ctlP As Control
    Dim xValue As String
    Dim optP As mpDB.CPersonOption
    Dim opsP As mpDB.CPersonOptions
    
    Set opsP = oPerson.Options(xTemplate, frmP)
    
    opsP.Refresh
    
    For i = 2 To opsP.Count
        Set optP = opsP(i)
'       field name is option set item name
'       with different prefix
        
        If Not IsNull(optP.value) Then
            xValue = optP.value
        Else
            xValue = xNullToString(optP.value)
        End If
        Set ctlP = Nothing
        
        On Error Resume Next
        Set ctlP = frmP.Controls(optP.Name)
        On Error GoTo 0
        
        If Not (ctlP Is Nothing) Then
            If (TypeOf ctlP Is TrueDBList60.TDBCombo) Then
                ctlP.BoundText = xValue
            ElseIf (TypeOf ctlP Is VB.CheckBox) Then
                If xValue Then
                    ctlP = vbChecked
                Else
                    ctlP = vbUnchecked
                End If
            ElseIf (TypeOf ctlP Is mpControls.SpinnerTextBox) Or _
                    (TypeOf ctlP Is mpControls3.SpinTextInternational) Then '*c
                ctlP.value = xValue
            Else
                ctlP = xValue
            End If
       End If
    Next i
End Sub

Public Function lGetActiveControlColor() As Long
    Dim lColor As Long
    On Error Resume Next
    lColor = GetMacPacIni("General", "ActiveControlColor")
    If lColor = 0 Then
        lColor = vbCyan
    End If
    lGetActiveControlColor = lColor
End Function

Public Function lGetInactiveControlColor() As Long
    Dim lColor As Long
    On Error Resume Next
    lColor = GetMacPacIni("General", "InactiveControlColor")
    If lColor = 0 Then
        lColor = vbWhite
    End If
    lGetInactiveControlColor = lColor
End Function

Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    Dim oNum As CNumbers
    
    Set oNum = New CNumbers
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = oNum.Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, App.Title
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Public Sub SelectControlText()
'selects the text in the active control if appropriate
    Static oPrevCtl As VB.Control
    Dim oCurCtl As VB.Control

    On Error Resume Next
    Set oCurCtl = Screen.ActiveControl
    On Error GoTo 0

    If oCurCtl Is Nothing Then
        Exit Sub
    End If

    With oCurCtl
        If Not oCurCtl Is oPrevCtl Then
            If TypeOf oCurCtl Is TextBox Or _
                TypeOf oCurCtl Is mpControls.MultiLineCombo Then
                .SelStart = 0
                .SelLength = Len(.Text)
            ElseIf TypeOf oCurCtl Is mpControls.SpinnerTextBox Then
                .SelStart = 0
                .SelLength = Len(.value)
            End If
            Set oPrevCtl = oCurCtl
        End If
    End With
End Sub

Public Function SetControlColorTimer()
'sets the background color to the active control
'if appropriate - removes background color
'from previously active control
    Static oPrevCtl As VB.Control
    Dim oCurCtl As VB.Control
    On Error GoTo ProcError

'   get active control
    Set oCurCtl = Screen.ActiveControl

    If oCurCtl Is Nothing Then
        Exit Function
    End If

    If Not (oCurCtl Is oPrevCtl) Then
'       focus has changed
        If Not oPrevCtl Is Nothing Then
'           remove color from previous control
            If (TypeOf oPrevCtl Is CheckBox) Then
                oPrevCtl.BackColor = Screen.ActiveForm.BackColor
            ElseIf Not (TypeOf oPrevCtl Is C1Tab Or _
                TypeOf oPrevCtl Is VB.CommandButton) Then
                oPrevCtl.BackColor = g_lInactiveCtlColor
            End If
        End If

'       set color of active control
        If Not (TypeOf oCurCtl Is C1Tab Or _
            TypeOf oCurCtl Is VB.CommandButton) Then
            oCurCtl.BackColor = g_lActiveCtlColor
        End If

        DoEvents

'       get control with focus -
'       set back color if appropriate
        Set oPrevCtl = oCurCtl
    End If
    Exit Function
ProcError:
    g_oError.Show Err, "Could not set color background color."
    Exit Function
End Function

Public Sub bResetLetterheadCombo(oTDBCombo As TDBCombo, _
                                 oLetterhead As MPO.CLetterhead)
    Dim oEnv As MPO.CEnvironment
'   resets letterhead combo controls to letterhead type saved in document
'   necessary when letterhead type associated with author is invalid
    With oTDBCombo
        If .BoundText = "" Then
            Set oEnv = New MPO.CEnvironment
            oEnv.SetMacPacState
            .BoundText = oLetterhead.LetterheadType
            .Text = oLetterhead.Definition.Description
            oLetterhead.Refresh
            oEnv.RestoreState
        End If
    End With
End Sub

Public Function ShowRelineOptions(oPerson As mpDB.CPerson, xTemplate As String, frmP As Form) As Long
'   9.7.1 - 4013 interpret options checkboxes to load relineformatvalues bitwise value to reline control
'   cycle through flds

    Dim i As Integer
    Dim ctlP As Control
    Dim xValue As String
    Dim optP As mpDB.CPersonOption
    Dim opsP As mpDB.CPersonOptions
    Dim lTemp As Long
    Dim lValues As Long
    
    Set opsP = oPerson.Options(xTemplate, frmP)
    opsP.Refresh
    
    For i = 2 To opsP.Count
        Set optP = opsP(i)
'       field name is option set item name
'       with different prefix
        If Not IsNull(optP.value) Then
            xValue = optP.value
        Else
            xValue = xNullToString(optP.value)
        End If
        '---we check name instead of type of since we're not directly assigning a checkbox value
        '---to a checkbox
        Select Case optP.Name
            Case "chkRelineBold"
                If xValue = "True" Then
                    lTemp = mpRelineVI_RelineBold
                    lValues = lTemp + lValues
                End If
            Case "chkRelineUnderline"
                If xValue = "True" Then
                    lTemp = mpRelineVI_RelineUnderscored
                    lValues = lTemp + lValues
                End If
            Case "chkRelineItalic"
                If xValue = "True" Then
                    lTemp = mpRelineVI_RelineItalic
                    lValues = lTemp + lValues
                End If
            Case "chkRelineUnderlineLast"
                If xValue = "True" Then
                    lTemp = mpRelineVI_RelineUnderscoreLast
                    lValues = lTemp + lValues
                End If
        End Select
        
        lTemp = 0
        
    Next i
    '---assign assembled bitwise value
    
    ShowRelineOptions = lValues
    
End Function

Public Sub SetInterfaceOptions(frmP As VB.Form, xTemplate As String)
    Dim oTemplate As mpDB.CTemplateDef
    Dim oCtl As VB.Control
    Dim oCtlR As VB.Control
    Dim vRelatedControls As Variant
    Dim xRelated
    Dim oList As mpDB.CList
    Dim oList2 As mpDB.CList
    Dim xID As String
    Dim xCaption As String
    Dim bHide As Boolean
    Dim i As Long
    Dim r As Integer
    
    On Error GoTo ProcError
    
    If Not g_oMPO.db.Lists.Exists("InterfaceControls") Then Exit Sub
    Set oList2 = g_oMPO.db.Lists("InterfaceControls")
    On Error Resume Next
    Set oTemplate = g_oMPO.db.TemplateDefinitions(xTemplate)
    On Error GoTo ProcError
    If oTemplate Is Nothing Then Exit Sub ' Template doesn't exist (should never happen...)
    Select Case UCase(oTemplate.ClassName)
        Case "CLETTER"
            If Not g_oMPO.db.Lists.Exists("CustomInterfaceLetter") Then Exit Sub
            Set oList = g_oMPO.db.Lists("CustomInterfaceLetter")
            oList2.FilterValue = "cLetter"
            oList2.Refresh
        Case "CMEMO"
            If Not g_oMPO.db.Lists.Exists("CustomInterfaceMemo") Then Exit Sub
            Set oList = g_oMPO.db.Lists("CustomInterfaceMemo")
            oList2.FilterValue = "cMemo"
            oList2.Refresh
        Case "CFAX"
            If Not g_oMPO.db.Lists.Exists("CustomInterfaceFax") Then Exit Sub
            Set oList = g_oMPO.db.Lists("CustomInterfaceFax")
            oList2.FilterValue = "cFax"
            oList2.Refresh
        Case Else
            ' Class not supported
            Exit Sub
    End Select
    oList.FilterValue = xTemplate
    oList.Refresh
    If oList.ListItems.Count = 0 And oTemplate.BaseTemplate <> "" Then
        ' No customizations, so check base template
        oList.FilterValue = oTemplate.BaseTemplate
        oList.Refresh
    End If
    If oList.ListItems.Count = 0 Then Exit Sub ' No Customizations
    With oList.ListItems
        For i = 0 To .Count - 1
            xID = .Item(i).ID
            Set oCtl = Nothing
            On Error Resume Next
            Set oCtl = frmP.Controls(xID)
            On Error GoTo ProcError
            ' Check if control exists
            If Not oCtl Is Nothing Then
                xCaption = "" & .Item(i).DisplayText
                bHide = CBool(.Item(i).SuppValue)
                xRelated = ""
                On Error Resume Next
                xRelated = "" & oList2.ListItems.Item(, xID).DisplayText
                On Error GoTo ProcError
                If xRelated <> "" Then
                    vRelatedControls = Split(xRelated, "|")
                Else
                    vRelatedControls = Empty
                End If
                If xCaption <> "" Then
                    oCtl.Caption = xCaption
                End If
                Select Case UCase(xID)
                    Case "LBLAUTHOR" ' Can't hide author control
                    Case Else
                        ' Hide control if its visibility doesn't match setting
                        If oCtl.Visible = bHide Then
                            oCtl.Visible = Not bHide
                            If IsArray(vRelatedControls) Then
                                On Error Resume Next
                                ' Hide/show related controls
                                For r = 0 To UBound(vRelatedControls)
                                    frmP.Controls.Item(CStr(vRelatedControls(r))).Visible = Not bHide
                                Next r
                                On Error GoTo ProcError
                            End If
                        End If
                End Select
            End If
        Next i
    End With
    Exit Sub
ProcError:
    g_oError.Show Err, "Error applying interface customizations."
    Exit Sub
    
End Sub
Public Sub SelectFirstAvailableControl(frmP As VB.Form, iStartIndex As Integer, iEndIndex As Integer)
    Dim arrCtls() As String
    Dim i As Integer
    Dim oCtl As VB.Control
    
    ReDim arrCtls(frmP.Controls.Count)
    On Error Resume Next
    For Each oCtl In frmP.Controls
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next oCtl
    
    For i = iStartIndex To iEndIndex
        If arrCtls(i) <> "" Then
            Set oCtl = frmP.Controls(arrCtls(i))
            If Not (TypeOf oCtl Is VB.Label Or _
                TypeOf oCtl Is VB.Frame Or _
                TypeOf oCtl Is VB.Shape) Then
                If oCtl.Visible And oCtl.Enabled Then
                    oCtl.SetFocus
                    Exit Sub
                End If
            End If
        End If
    Next i
End Sub
Public Function PositionLetterheadControls(oFrm As VB.Form, iLabelIndex As Integer, sOffset As Single, sIncrement As Single) As Single
    Dim arrCtls() As String
    Dim i As Integer
    Dim oCtl As VB.Control
    Dim bSetCols As Boolean
    Dim sMaxWidth As Single
    Dim sCols(4) As Single
    Dim iCol As Integer
    
    ReDim arrCtls(oFrm.Controls.Count)
    On Error Resume Next
    For Each oCtl In oFrm.Controls
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next oCtl
    On Error GoTo 0
    For i = iLabelIndex To UBound(arrCtls())
        Set oCtl = oFrm.Controls(arrCtls(i))
        If i = iLabelIndex Then
            oCtl.Top = sOffset + 30
        ElseIf TypeOf oCtl Is VB.CheckBox Then
            If Not bSetCols Then
                ' Set starting position for checkboxes
                sCols(1) = oCtl.Left
                sMaxWidth = ((oCtl.Container.Width - 50) - sCols(1)) / 3
                sCols(2) = sCols(1) + sMaxWidth
                sCols(3) = sCols(2) + sMaxWidth
                sCols(4) = sCols(3) + sMaxWidth ' Dummy value for Do Loop below
                iCol = 1
                bSetCols = True
            End If
            If iCol > 3 Then
                'Start new row
                iCol = 1
                sOffset = sOffset + oCtl.Height + 85
            End If

            If oCtl.Visible Then
                oCtl.Left = sCols(iCol)
                oCtl.Top = sOffset
                
                ' If checkbox is wider than the column width, then skip to the next column
                Do
                    iCol = iCol + 1
                Loop While oCtl.Left + oCtl.Width > sCols(iCol) And iCol < 3
            End If
        Else
            ' End of checkboxes has been reached
            Exit For
        End If
    Next i
    If iCol > 1 Then
        sOffset = sOffset + oCtl.Height + 85
    End If
    ' Add extra space after last row
    sOffset = sOffset + Min(50, (sIncrement - 85))
    PositionLetterheadControls = sOffset
End Function

Public Sub HideUnusedLetterheadControls(oFrm As VB.Form, xTemplate As String)
    Dim oDefs As mpDB.CLetterheadDefs
    Dim oCtl As VB.Control
    Dim i As Integer
    Dim bHide As Boolean
    Dim bVisible As Boolean
    
    On Error Resume Next
    
    Set oDefs = g_oMPO.db.LetterheadDefs(xTemplate)
    If oDefs.Count = 0 Then Exit Sub
    
    bVisible = False
    On Error Resume Next
    Set oCtl = oFrm.Controls("lblInclude")
    If oCtl Is Nothing Then
        Exit Sub ' Form doesn't have letterhead controls
    Else
        If Not oCtl.Visible Then Exit Sub ' Controls have already been hidden
    End If
        
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadName")
    If Not oCtl Is Nothing Then
        ' If already hidden, don't do anything
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowAuthorName Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadPhone")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowAuthorPhone Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadFax")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowAuthorFax Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadEmail")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowAuthorEMail Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadAdmittedIn")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowAuthorAdmittedIn Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadTitle")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowAuthorTitle Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadCustom1")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowCustomDetail1 Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadCustom2")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowCustomDetail2 Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    Set oCtl = Nothing
    Set oCtl = oFrm.Controls("chkIncludeLetterheadCustom3")
    If Not oCtl Is Nothing Then
        If oCtl.Visible Then
            bHide = True
            For i = 0 To oDefs.Count - 1
                If oDefs.Item(oDefs.ListSource.value(i, 0)).AllowCustomDetail3 Then
                   bHide = False
                   Exit For
                End If
            Next i
            If bHide Then
                oCtl.Visible = False
            End If
        End If
        bVisible = bVisible Or oCtl.Visible
    End If
    oFrm.Controls("lblInclude").Visible = bVisible
End Sub
