VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Object = "{76ACB981-5B88-45A3-8021-6C736CAC7713}#8.0#0"; "mpControls4.ocx"
Object = "{FC715925-5921-4AC0-B4BD-36B619407BAE}#1.1#0"; "MPCM.ocx"
Object = "{EB6DB63D-06AD-436A-8A37-3252EDE53C0C}#1.0#0"; "mpSal.ocx"
Begin VB.Form frmLetter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Letter"
   ClientHeight    =   6600
   ClientLeft      =   1932
   ClientTop       =   636
   ClientWidth     =   5484
   Icon            =   "frmLetter.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6600
   ScaleWidth      =   5484
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   63
      Top             =   6132
      Width           =   650
   End
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   3330
      Picture         =   "frmLetter.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   61
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   6132
      Width           =   705
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4065
      TabIndex        =   62
      Top             =   6132
      Width           =   650
   End
   Begin C1SizerLibCtl.C1Tab tabPages 
      Height          =   6648
      Left            =   -48
      TabIndex        =   64
      TabStop         =   0   'False
      Top             =   -96
      Width           =   5520
      _cx             =   9737
      _cy             =   11726
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|Si&gnature|&Letterhead"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "5780"
         Height          =   6204
         Left            =   6252
         TabIndex        =   22
         Top             =   12
         Width           =   5496
         Begin VB.TextBox txtCustom3 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1800
            TabIndex        =   60
            Top             =   4470
            Visible         =   0   'False
            Width           =   3585
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom1 
            Appearance      =   0  'Flat
            Caption         =   "Custom 1"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1845
            TabIndex        =   50
            Top             =   1380
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom2 
            Appearance      =   0  'Flat
            Caption         =   "Custom 2"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3030
            TabIndex        =   51
            Top             =   1380
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom3 
            Appearance      =   0  'Flat
            Caption         =   "Custom 3"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   4215
            TabIndex        =   52
            Top             =   1380
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeDPhrases2InHeader 
            Appearance      =   0  'Flat
            Caption         =   "Include &Confidential Phrases in Header"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1860
            TabIndex        =   58
            Top             =   4020
            Width           =   3135
         End
         Begin VB.CheckBox chkIncludeLetterheadAdmittedIn 
            Appearance      =   0  'Flat
            Caption         =   "Admi&tted In"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   4215
            TabIndex        =   49
            Top             =   1065
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadFax 
            Appearance      =   0  'Flat
            Caption         =   "Fa&x"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3030
            TabIndex        =   48
            Top             =   1065
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadTitle 
            Appearance      =   0  'Flat
            Caption         =   "T&itle"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1845
            TabIndex        =   47
            Top             =   1065
            Width           =   1155
         End
         Begin TrueDBList60.TDBCombo cmbLetterheadType 
            Height          =   360
            Left            =   1770
            OleObjectBlob   =   "frmLetter.frx":0CAC
            TabIndex        =   42
            Top             =   225
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbDateType 
            Height          =   360
            Left            =   1785
            OleObjectBlob   =   "frmLetter.frx":3355
            TabIndex        =   54
            Top             =   1860
            Width           =   3585
         End
         Begin VB.TextBox txtHeaderAdditionalText 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   945
            Left            =   1800
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   56
            Top             =   2400
            Width           =   3585
         End
         Begin VB.CheckBox chkIncludeDPhrasesInHeader 
            Appearance      =   0  'Flat
            Caption         =   "Include Delivery &Phrases in Header"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1860
            TabIndex        =   57
            Top             =   3570
            Width           =   3075
         End
         Begin VB.CheckBox chkIncludeLetterheadEMail 
            Appearance      =   0  'Flat
            Caption         =   "&E-Mail"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   4215
            TabIndex        =   46
            Top             =   735
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadPhone 
            Appearance      =   0  'Flat
            Caption         =   "P&hone"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3030
            TabIndex        =   45
            Top             =   735
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadName 
            Appearance      =   0  'Flat
            Caption         =   "&Name"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1845
            TabIndex        =   44
            Top             =   735
            Width           =   1155
         End
         Begin VB.Label lblCustom3 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 3:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   30
            TabIndex        =   59
            Top             =   4530
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Line Line3 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5550
            Y1              =   6156
            Y2              =   6156
         End
         Begin VB.Label lblInclude 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Include Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   43
            Top             =   765
            Width           =   1275
         End
         Begin VB.Label lblAdditionalText 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Page &2 Header:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   300
            TabIndex        =   55
            Top             =   2400
            Width           =   1275
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Letterhead T&ype:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   41
            Top             =   270
            Width           =   1275
         End
         Begin VB.Label lblDateType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Date Format:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   270
            TabIndex        =   53
            Top             =   1905
            Width           =   1275
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H80000003&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H80000003&
            Height          =   6185
            Left            =   -45
            Top             =   0
            Width           =   1725
         End
      End
      Begin VB.Frame fraPage2 
         BorderStyle     =   0  'None
         Height          =   6204
         Left            =   6012
         TabIndex        =   21
         Top             =   12
         Width           =   5496
         Begin VB.TextBox txtCustom2 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            TabIndex        =   39
            Top             =   5010
            Visible         =   0   'False
            Width           =   3585
         End
         Begin VB.CheckBox chkElectronicSignature 
            Appearance      =   0  'Flat
            Caption         =   "&Electronic Signature Format"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1860
            TabIndex        =   27
            Top             =   1080
            Width           =   3315
         End
         Begin VB.TextBox txtAuthorInitials 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1770
            TabIndex        =   31
            Top             =   1950
            Width           =   3600
         End
         Begin VB.CheckBox chkIncludeSignature 
            Appearance      =   0  'Flat
            Caption         =   "Include &Signature Block"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1860
            TabIndex        =   40
            Top             =   5475
            Value           =   1  'Checked
            Width           =   2400
         End
         Begin VB.TextBox txtTypistInitials 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1770
            TabIndex        =   29
            Top             =   1500
            Width           =   3600
         End
         Begin VB.CheckBox chkIncludeAuthorTitle 
            Appearance      =   0  'Flat
            Caption         =   "Include &Title"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1860
            TabIndex        =   25
            Top             =   660
            Width           =   1245
         End
         Begin VB.CheckBox chkIncludeFirmName 
            Appearance      =   0  'Flat
            Caption         =   "Incl&ude Firm Name"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   3390
            TabIndex        =   26
            Top             =   660
            Width           =   4215
         End
         Begin VB.TextBox txtBCC 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   915
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   37
            Top             =   3930
            Width           =   3600
         End
         Begin VB.TextBox txtCC 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   915
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   35
            Top             =   2850
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbClosingPhrases 
            Height          =   360
            Left            =   1770
            OleObjectBlob   =   "frmLetter.frx":5568
            TabIndex        =   24
            Top             =   150
            Width           =   3600
         End
         Begin TrueDBList60.TDBCombo cmbEnclosures 
            Height          =   585
            Left            =   1770
            OleObjectBlob   =   "frmLetter.frx":779D
            TabIndex        =   33
            Top             =   2370
            Width           =   3600
         End
         Begin VB.Label lblCustom2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 2:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   60
            TabIndex        =   38
            Top             =   5070
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblAuthorInitials 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Auth&or Initials:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   195
            TabIndex        =   30
            Top             =   1980
            Width           =   1320
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000005&
            X1              =   -132
            X2              =   5418
            Y1              =   6156
            Y2              =   6156
         End
         Begin VB.Label lblTypistInitials 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " T&ypist Initials:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   195
            TabIndex        =   28
            Top             =   1530
            Width           =   1320
         End
         Begin VB.Label lblEnclosures 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " E&nclosures:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   195
            TabIndex        =   32
            Top             =   2400
            Width           =   1320
         End
         Begin VB.Label lblClosingPhrase 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " Closing &Phrase:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   195
            TabIndex        =   23
            Top             =   210
            Width           =   1320
         End
         Begin VB.Label lblBCC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   " &bcc:"
            ForeColor       =   &H00FFFFFF&
            Height          =   840
            Left            =   195
            TabIndex        =   36
            Top             =   3885
            Width           =   1320
         End
         Begin VB.Label lblCC 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            BackStyle       =   0  'Transparent
            Caption         =   " &cc:"
            ForeColor       =   &H00FFFFFF&
            Height          =   870
            Left            =   195
            TabIndex        =   34
            Top             =   2850
            Width           =   1320
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00C0C0C0&
            FillColor       =   &H00800080&
            Height          =   6170
            Left            =   -45
            Top             =   0
            Width           =   1725
         End
      End
      Begin VB.Frame fraPage1 
         BorderStyle     =   0  'None
         Height          =   6204
         Left            =   12
         TabIndex        =   65
         Top             =   12
         Width           =   5496
         Begin TrueDBList60.TDBCombo cmbAuthor 
            Height          =   585
            Left            =   1740
            OleObjectBlob   =   "frmLetter.frx":99BA
            TabIndex        =   1
            Top             =   105
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbAuthor2 
            Height          =   585
            Left            =   1740
            OleObjectBlob   =   "frmLetter.frx":D8FF
            TabIndex        =   3
            Top             =   510
            Visible         =   0   'False
            Width           =   3585
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase 
            Height          =   510
            Left            =   1740
            TabIndex        =   5
            Top             =   915
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   910
            ListRows        =   16
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase2 
            Height          =   510
            Left            =   1740
            TabIndex        =   7
            Top             =   1500
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   910
            ListRows        =   8
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin MPCM.ClientMatterComboBox cmbCM 
            Height          =   315
            Left            =   1740
            TabIndex        =   12
            Top             =   3210
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   550
            SourceClass     =   "mpCMIA.CClientMatter"
            Col1Caption     =   "No."
            ColWidth1       =   1020
            ColWidth2       =   1020
            ListRows        =   6
            ComboType       =   2
            SelectTextOnFocus=   -1  'True
            PopulateOnLoad  =   0   'False
            AdditionalClientFilter=   ""
            AdditionalMatterFilter=   ""
         End
         Begin VB.TextBox txtCustom1 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            TabIndex        =   20
            Top             =   5550
            Visible         =   0   'False
            Width           =   3585
         End
         Begin mpControls4.RelineControl rlGrid 
            Height          =   1035
            Left            =   1740
            TabIndex        =   16
            Top             =   4020
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   1820
            BackColor       =   -2147483643
            Caption         =   "Incl&ude Special Re Line"
            FontName        =   "Arial"
            FontBold        =   0   'False
            FontSize        =   8.4
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin mpSal.SalutationCombo cmbSalutation 
            Height          =   345
            Left            =   1740
            TabIndex        =   18
            Top             =   5130
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   593
            SelectionType   =   1
            FontName        =   "MS Sans Serif"
            FontSize        =   7.8
         End
         Begin VB.TextBox txtRecipients 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   1068
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   9
            Top             =   2100
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbInsertText 
            Height          =   390
            Left            =   1725
            OleObjectBlob   =   "frmLetter.frx":11845
            TabIndex        =   14
            Top             =   3600
            Width           =   3600
         End
         Begin VB.CheckBox chkRecipientsTableFormat 
            Appearance      =   0  'Flat
            Caption         =   "Use Ta&ble Formatting for Recipients"
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   1800
            TabIndex        =   10
            Top             =   2880
            Width           =   3245
         End
         Begin VB.Label lblInsertText 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Insert Te&xt:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   690
            TabIndex        =   13
            Top             =   3630
            Width           =   810
         End
         Begin VB.Label lblDeliveryPhrase2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Confidential Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   420
            Left            =   30
            TabIndex        =   6
            Top             =   1515
            Width           =   1545
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblCustom1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 1:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   30
            TabIndex        =   19
            Top             =   5610
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblAuthor2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&2nd Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            TabIndex        =   2
            Top             =   510
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Line Line8 
            BorderColor     =   &H80000005&
            X1              =   -120
            X2              =   5430
            Y1              =   6156
            Y2              =   6156
         End
         Begin VB.Label lblClientMatter 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Fil&e No.:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   75
            TabIndex        =   11
            Top             =   3240
            Width           =   1455
         End
         Begin VB.Label lblAuthor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   90
            TabIndex        =   0
            Top             =   150
            Width           =   1455
         End
         Begin VB.Label lblDeliveryPhrase 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   " Delivery &Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   225
            TabIndex        =   4
            Top             =   915
            Width           =   1320
         End
         Begin VB.Label lblTo 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&To:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   285
            TabIndex        =   8
            Top             =   2115
            Width           =   1260
         End
         Begin VB.Label lblReline 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   " &Re Line:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   870
            TabIndex        =   15
            Top             =   4005
            Width           =   630
         End
         Begin VB.Label lblSalutation 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   " &Salutation:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   750
            TabIndex        =   17
            Top             =   5175
            Width           =   795
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00C0C0C0&
            FillColor       =   &H00800080&
            Height          =   6350
            Left            =   -120
            Top             =   -180
            Width           =   1800
         End
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000005&
         X1              =   6492
         X2              =   11988
         Y1              =   12
         Y2              =   6216
      End
   End
   Begin VB.Line Line6 
      BorderColor     =   &H00E0E0E0&
      Index           =   5
      X1              =   120
      X2              =   6120
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Line Line6 
      BorderColor     =   &H00E0E0E0&
      Index           =   1
      X1              =   0
      X2              =   6000
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Menu mnuCarrierReline 
      Caption         =   "Carrier Reline"
      Visible         =   0   'False
      Begin VB.Menu mnuCarrierReline_Delete 
         Caption         =   "&Delete Row"
      End
      Begin VB.Menu mnuCarrierReline_Insert 
         Caption         =   "&Insert Row"
      End
      Begin VB.Menu mnuCarrierReline_Sep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCarrierReline_ClearAll 
         Caption         =   "&Clear All Rows"
      End
      Begin VB.Menu mnuCarrierReline_Reset 
         Caption         =   "&Reset to Defaults"
      End
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_EditAuthorOptions 
         Caption         =   "&Edit Author Preferences..."
      End
      Begin VB.Menu mnuAuthor_CopyOptions 
         Caption         =   "Cop&y Author Preferences..."
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
   Begin VB.Menu mnuAuthor2 
      Caption         =   "Author2"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor2_Clear 
         Caption         =   "&Clear"
      End
   End
End
Attribute VB_Name = "frmLetter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_AuthorInitSep As String

Private WithEvents m_Letter As MPO.CLetter
Attribute m_Letter.VB_VarHelpID = -1
Private m_PersonsForm As MPO.CPersonsForm
Private m_oContacts As MPO.CContacts
Private m_oMenu As MacPac90.CAuthorMenu
Private WithEvents m_oOptForm As MPO.COptionsForm
Attribute m_oOptForm.VB_VarHelpID = -1

Private m_bAuthorDirty As Boolean
Private b_Opened As Boolean
Private m_bAuthorManual As Boolean
Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private m_bDyn As Boolean
Private m_bCanceled As Boolean
Private m_bTimerCanceled As Boolean
Private m_ReuseAction As String
Private m_xAuthorInit As String
Private m_bItemChanged As Boolean
Private m_bChangingRecipsWithoutTyping As Boolean

Private m_bAuthor2Dirty As Boolean
Private b_Opened2 As Boolean
Private m_bItem2Changed As Boolean
Private m_xDefaultCCLabel As String '9.7.1 #4151
Private m_xDefaultBCCLabel As String '9.7.1 #4151
Private m_bUpdateDefaults As Boolean    'GLOG : 5598 : ceh

Property Get Cancelled() As Boolean
    Cancelled = m_bCanceled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCanceled = bNew
End Property

Public Property Get Letter() As MPO.CLetter
    Set Letter = m_Letter
End Property

Public Property Let Letter(ltrNew As MPO.CLetter)
    Set m_Letter = ltrNew
End Property

Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property

Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = Not bNew
End Property

Public Property Let bAuthorIsDirty(bNew As Boolean)
    m_bAuthorDirty = bNew
End Property

Public Property Get bAuthorIsDirty() As Boolean
    bAuthorIsDirty = m_bAuthorDirty
End Property

Public Property Let AuthorIsManual(bNew As Boolean)
    m_bAuthorManual = bNew
End Property

Public Property Get AuthorIsManual() As Boolean
    AuthorIsManual = m_bAuthorManual
End Property

Public Sub ShowOptions(lID As Long)
    On Error GoTo ProcError
    Dim bCascade As Boolean
    Dim lTemp As Long
    
    
'   get original state of property
    bCascade = Me.CascadeUpdates
    
'   ensure no cascading
    Me.CascadeUpdates = False
        
    lTemp = mpbase2.CurrentTick
'   show options
    mdlDialog.ShowOptions Me.Letter.Author, Me.Letter.Template.ID, Me
    DebugOutput "MacPac90.ShowOptions - mdlDialog.ShowOptions " & vbTab & vbTab & vbTab & mpbase2.ElapsedTime(lTemp)
    
'---9.7.1 - 4013 - collect reline formating options and feed grid
    rlGrid.FormatValues = mdlDialog.ShowRelineOptions(Me.Letter.Author, Me.Letter.Template.ID, Me)
    
    SetRelineUnderline
    
    
    '*** 9.7.1 #3675
    If UCase(xGetTemplateConfigVal(Me.Letter.Template.ID, "ForceTitleForNonAttorney")) = "TRUE" Then
        If Not Me.Letter.Author.IsAttorney Then
            Me.CascadeUpdates = True
            Me.chkIncludeAuthorTitle = 1
            Me.chkIncludeAuthorTitle.Enabled = False
        Else
            Me.chkIncludeAuthorTitle.Enabled = True
        End If
    End If

'   return to original state
    Me.CascadeUpdates = bCascade
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not display Author Options"
    Exit Sub
End Sub


Private Sub btnAddressBooks_Click()

    GetContacts ciSelectionList_To
    
    On Error GoTo ProcError
     Select Case Me.tabPages.CurrTab
        Case 0
            Me.txtRecipients.SetFocus
        Case 1
            Me.txtCC.SetFocus
    End Select
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub ConfigureCarrierReline()
    
    '---9.7.1 - 4013
    
''''    If Me.chkIncludeCarrierReline.value = vbChecked Then
''''        If UCase(xGetTemplateConfigVal(Letter.Template.ID, "ToggleCarrierReline")) = "TRUE" Then
''''            Me.rlGrid.Style = [Show Grid Only]
''''            lblReline.Caption = "S&pecial Re Line:"
''''        Else
''''            Me.rlGrid.Style = [Show Both]
''''            Me.rlGrid.DefaultTab = [Default to Grid]
''''            lblReline.Caption = "Re Lines:"
''''        End If
''''    Else
''''        Me.rlGrid.Style = [Show Reline Only]
''''        lblReline.Caption = "&Re Line:"
''''    End If

''''    Me.Letter.ShowCarrierRelineGrid = Me.chkIncludeCarrierReline.value = vbChecked
    
''''    If Me.chkIncludeCarrierReline = vbUnchecked Then
''''        Letter.CarrierReline = ""
''''    End If
    
''''    rlGrid_LostFocus
''''
''''    Application.ScreenRefresh

End Sub

Private Sub chkIncludeDPhrasesInHeader_GotFocus()
    OnControlGotFocus Me.chkIncludeDPhrasesInHeader, "zzmpHeaderDeliveryPhrases"
End Sub

Private Sub chkIncludeSignature_Click()
    Dim bCheck As Boolean
    '---9.7.1 - 4148
    Me.Letter.IncludeSignature = (chkIncludeSignature.value = vbChecked)
    EnableSignatureControls
    
    Application.ScreenRefresh
End Sub

Private Sub chkIncludeSignature_GotFocus()
    '---9.7.1 -4148
    OnControlGotFocus chkIncludeSignature
End Sub

Private Sub chkRecipientsTableFormat_Click() '---9.9.1 #5122
    Application.ScreenUpdating = False
    If chkRecipientsTableFormat = 1 Then
        Letter.UserRecipientTableThreshold = lCountChrs(Me.txtRecipients, vbCrLf & vbCrLf) + 1
    Else
        Letter.UserRecipientTableThreshold = 0
    End If
    Letter.Recipients = ""  '!gm  Refresh recipients
    Letter.Recipients = Me.txtRecipients
    Application.ScreenUpdating = True
End Sub

Private Sub chkRecipientsTableFormat_GotFocus()
    OnControlGotFocus Me.chkRecipientsTableFormat
End Sub

Private Sub cmbAuthor_Change()
    bAuthorIsDirty = True
End Sub

Private Sub cmbAuthor_Click()
    If b_Opened Then
        bAuthorIsDirty = False
    End If
End Sub

Private Sub cmbAuthor_Close()
    b_Opened = False
End Sub

Private Sub cmbAuthor_LostFocus()
    Dim bCascade As Boolean

'   update for author only if itemchange event ran
    If m_bItemChanged Then

        With Me
'           update values in form
            bCascade = .CascadeUpdates
            .CascadeUpdates = False
        
'           update object with values of selected person
            If Not .Initializing Then
                EchoOffDesktop
                .Letter.UpdateForAuthor
            End If
        
'           show options in form
            .ShowOptions .Letter.Author.ID
            
            bResetLetterheadCombo Me.cmbLetterheadType, _
                                  Me.Letter.Letterhead
        
            .CascadeUpdates = bCascade
        End With
        
        '---9.7.1 - 4013
        rlGrid_FormatChanged
        
        '---9.7.1 4149
        If Letter.Signature.AllowAuthorInitials Then
            txtAuthorInitials.Text = BuildAuthorInitials
            Letter.Signature.AuthorInitials = txtAuthorInitials.Text
            '*c new code to prevent lines above making author title visible
            'when it shouldn't
            Letter.Signature.IncludeAuthorTitle = mpUndefined
            chkIncludeAuthorTitle_Click '*c
        End If
        
        m_bItemChanged = False
    End If
    

    Application.ScreenUpdating = False
    DoEvents
    If Not Me.Initializing Then
        Application.ScreenUpdating = True
        Application.ScreenRefresh
    End If
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Open()
    b_Opened = True
End Sub

Private Sub cmbAuthor_GotFocus()
    DoEvents
    OnControlGotFocus Me.cmbAuthor, "zzmpAuthorName", True
End Sub

Private Sub cmbAuthor_ItemChange()
    Dim lID As Long
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If
    
    g_oMPO.ScreenUpdating = False
    
    With Letter
'       set new author
        lID = Me.cmbAuthor.BoundText
        .Author = g_oMPO.People(lID)
    End With
    
    If Me.ActiveControl.Name = "cmbAuthor" Then _
        cmbAuthor_GotFocus
        
    If Not Me.Initializing Then
        g_oMPO.ScreenUpdating = True
        Word.Application.ScreenUpdating = True
    End If
    m_bItemChanged = True
    Me.bAuthorIsDirty = False

    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 38 Or KeyCode = 40 Then
        bAuthorIsDirty = True
    ElseIf KeyCode = wdKeyF10 And Shift = 1 Then
        ShowAuthorMenu
    End If
End Sub

Private Sub cmbAuthor_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
        bAuthorIsDirty = False
        If Len(Me.cmbAuthor.BoundText) Then
            cmbAuthor_ItemChange
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        If Not Me.cmbAuthor.IsOpen Then
            ShowAuthorMenu
        End If
    End If
End Sub

Private Sub ShowAuthorMenu()
    Dim bEnabled As Boolean
    On Error GoTo ProcError
    
'   enable specified menu items only if author
'   is currently in the db
    If TypeOf Letter.Author Is CReUseAuthor Then
        bEnabled = False
    Else
        bEnabled = True
    End If
    
    Me.mnuAuthor_EditAuthorOptions.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Enabled = bEnabled
    Me.mnuAuthor_SetDefault.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Checked = Me.Letter.Author.Favorite
    Me.PopupMenu mnuAuthor
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If bAuthorIsDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor2_Change()
    m_bAuthor2Dirty = True
End Sub

Private Sub cmbAuthor2_Click()
    If b_Opened2 Then
        m_bAuthor2Dirty = False
    End If
End Sub

Private Sub cmbAuthor2_Close()
    b_Opened2 = False
End Sub

Private Sub cmbAuthor2_GotFocus()
    DoEvents
    OnControlGotFocus Me.cmbAuthor2, "zzmpAuthorName2"
End Sub


Private Sub cmbAuthor2_ItemChange()
    Dim lID As Long
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If
    
    g_oMPO.ScreenUpdating = False
    
    With Letter
'       set new author
        lID = Me.cmbAuthor2.BoundText
        
        If lID > 0 Then
            .Author2 = g_oMPO.People(lID)
            .Signature.IncludeAuthorTitle = 0 - Me.chkIncludeAuthorTitle
        Else
            .Author2 = Nothing
        End If
        txtAuthorInitials = BuildAuthorInitials
        txtAuthorInitials_LostFocus
    End With
    
    If Me.ActiveControl.Name = "cmbAuthor2" Then _
        cmbAuthor2_GotFocus
        
    m_bItem2Changed = True
    m_bAuthor2Dirty = False
    Me.Refresh
    DoEvents
    If Not Me.Initializing Then
        g_oMPO.ScreenUpdating = True
        Word.Application.ScreenUpdating = True
    End If
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbAuthor2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthor2Dirty = True
    ElseIf KeyCode = wdKeyF10 And Shift = 1 Then
        Me.PopupMenu mnuAuthor2
    End If
End Sub
Private Sub cmbAuthor2_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthor2Dirty = False
        If Len(Me.cmbAuthor2.BoundText) Then
            cmbAuthor2_ItemChange
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        If Not Me.cmbAuthor2.IsOpen Then
            Me.PopupMenu mnuAuthor2
            'ShowAuthorMenu
        End If
    End If
End Sub

Private Sub cmbAuthor2_Open()
    b_Opened2 = True
End Sub

Private Sub cmbAuthor2_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If Me.cmbAuthor2.Text = Me.cmbAuthor.Text Then
        MsgBox "Second author must be different.", vbExclamation, App.Title
        Cancel = True
    ElseIf m_bAuthor2Dirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor2)
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbInsertText_GotFocus()
    OnControlGotFocus Me.cmbInsertText
End Sub

Private Sub cmbInsertText_ItemChange()
    On Error GoTo ProcError
    If Not Me.Initializing Then
        Me.Letter.InsertType = Me.cmbInsertText.BoundText
        '9.7.1 #4446
        If Me.Letter.InsertType = 0 Then
            Me.rlGrid.RelineValues = ""
            rlGrid_LostFocus
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbInsertText_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbInsertText, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbInsertText_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbInsertText)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbCM_ConnectionFailed(ByVal Message As String)

    MsgBox IIf(Message = "", "Could not connect to the client matter data source.  ", Message & vbCr & vbCr) _
            & "Instead of selecting client matter numbers from a list," & _
            " you can manually type the numbers instead.", _
            vbExclamation, App.Title
        
    With Me.cmbCM
        .RunConnected = False
        .ComboType = ComboType_TextOnly
    End With
    
End Sub

Private Sub cmbCM_GotFocus()
    OnControlGotFocus Me.cmbCM, "zzmpReline"
End Sub

Private Sub cmbCM_LostFocus()
    On Error GoTo ProcError
    Application.ScreenUpdating = False '#4158
    With Me.Letter
        .ClientMatterNumber = Me.cmbCM.value
        If .ClientMatterNumber = Empty Then
            .ClientNumber = ""
            .ClientName = ""
            .MatterNumber = ""
            .MatterName = ""
        ElseIf cmbCM.RunConnected Then  '#4457
            If cmbCM.Validity = cmValidationStatus_Valid Then  '#4158
                .ClientNumber = Me.cmbCM.ClientData()(cmClientField_Number)
                .ClientName = Me.cmbCM.ClientData()(cmClientField_Name)
                .MatterNumber = Me.cmbCM.MatterData()(cmMatterField_Number)
                .MatterName = Me.cmbCM.MatterData()(cmMatterField_Name)
            End If
        ElseIf g_xCM_Separator <> "" Then
            ' Parse out Client and Matter Numbers using separator
            Dim i As Integer
            i = InStr(cmbCM.value, g_xCM_Separator)
            If i > 0 Then
                .ClientNumber = Left(cmbCM.value, i - 1)
                .MatterNumber = Mid(cmbCM.value, i + Len(g_xCM_Separator))
                .ClientName = ""
                .MatterName = ""
            Else
                .ClientNumber = ""
                .ClientName = ""
                .MatterNumber = ""
                .MatterName = ""
            End If
        Else
            .ClientNumber = ""
            .ClientName = ""
            .MatterNumber = ""
            .MatterName = ""
        End If
    End With ' end #4158
    'save SQL for reuse
    Letter.Document.SaveItem "CMFilterString", _
        "Letter", , Me.cmbCM.AdditionalClientFilter
    If Not Me.Initializing Then
        Application.ScreenUpdating = True '#4158
        Application.ScreenRefresh '#4158
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Application.ScreenUpdating = True '#4158
    Exit Sub
End Sub

Private Sub cmbCM_Validate(Cancel As Boolean)
    Dim iIsValid As cmValidationStatus
                
    If g_bCM_AllowValidation Then
        
        If Me.cmbCM.RunConnected And Me.cmbCM.value <> Empty Then
            
            iIsValid = Me.cmbCM.Validity
            
            If iIsValid = cmValidationStatus_Invalid Then
                MsgBox "Invalid client matter number."
                Cancel = True
            End If
        End If
    
    End If
End Sub

Private Sub cmbDateType_GotFocus()
    OnControlGotFocus Me.cmbDateType, "zzmpDateType"
End Sub

Private Sub cmbDateType_LostFocus()
    On Error GoTo ProcError
    Letter.DateType = cmbDateType.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDateType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbDateType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_GotFocus()
    OnControlGotFocus Me.cmbLetterheadType
End Sub

Private Sub cmbLetterheadType_ItemChange()
    On Error GoTo ProcError
'*  enable/disable author details per LH type def
    Application.ScreenUpdating = False
    With g_oMPO.db.LetterheadDefs.Item(Me.cmbLetterheadType.BoundText)
        Me.chkIncludeLetterheadEMail.Enabled = .AllowAuthorEMail
        Me.chkIncludeLetterheadName.Enabled = .AllowAuthorName
        Me.chkIncludeLetterheadPhone.Enabled = .AllowAuthorPhone
        Me.chkIncludeLetterheadTitle.Enabled = .AllowAuthorTitle
        Me.chkIncludeLetterheadFax.Enabled = .AllowAuthorFax
        Me.chkIncludeLetterheadAdmittedIn.Enabled = .AllowAuthorAdmittedIn
        '9.7.1 #4028
        Me.chkIncludeLetterheadCustom1.Enabled = .AllowCustomDetail1
        Me.chkIncludeLetterheadCustom2.Enabled = .AllowCustomDetail2
        Me.chkIncludeLetterheadCustom3.Enabled = .AllowCustomDetail3
        Me.lblInclude.Enabled = .AllowAuthorEMail Or .AllowAuthorName Or _
                                .AllowAuthorPhone Or .AllowAuthorFax Or _
                                .AllowAuthorTitle Or .AllowAuthorAdmittedIn Or _
                                .AllowCustomDetail1 Or .AllowCustomDetail2 Or _
                                .AllowCustomDetail3
        '---
    End With
    If Not Me.Initializing Then
        Application.ScreenRefresh
        EchoOn
    End If
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_LostFocus()
'set letterhead as appropriate, if necessary
    Dim lType As Long
    Dim bRefresh As Boolean
    
    On Error GoTo ProcError
    
'    If Not bValidateBoundTDBCombo(Me.cmbLetterheadType) Then
'        Me.tabPages.CurrTab = 2
'        Me.cmbLetterheadType.SetFocus
'        Exit Sub
'    End If
    
    With Letter.Letterhead
'       get selected letterhead type
        lType = Me.cmbLetterheadType.BoundText
        
'       compare with previous letterhead type
        If .LetterheadType <> lType Then
        
            Word.Application.ScreenUpdating = False
            
'           type is different - set
'           type to selected type
            .LetterheadType = lType

'           if dynamic editing is on, refresh letterhead
            If g_oMPO.DynamicEditing Then
                .Refresh
            End If
        
            If Not Me.Initializing Then
                Word.Application.ScreenUpdating = True
            End If
        End If
    End With
    
    Exit Sub
ProcError:
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Could not change letterhead type."
End Sub

Private Sub cmbLetterheadType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLetterheadType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLetterheadType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbSalutation_GotFocus()
    
    OnControlGotFocus Me.cmbSalutation, "zzmpSalutation"

End Sub

Private Sub cmbSalutation_LostFocus()
    On Error GoTo ProcError
    Letter.Salutation = Me.cmbSalutation.Text
    
    'save salutation list for reuse
    Letter.Document.SaveItem "SalParsedData", _
        "Letter", , Me.cmbSalutation.ParsedContactData
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF2 Then
        btnAddressBooks_Click
    Else
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_Letter_ReLineChanged()
'---special handling for climat appended to reline'
    With Me.Letter
        If .ClientMatterHasBookmark Or .ClientMatterNumber = "" Then
            Me.rlGrid.RelineValues = .Reline
        Else
            Me.rlGrid.RelineValues = xSubstitute(.Reline, .ClientMatterNumber, "")
        End If
    End With
End Sub

Private Sub m_oOptForm_CurrentDocAuthorOptionsChange()
'   the options of the author of the current letter
'   have changed - update dlg and doc to reflect changes
    On Error GoTo ProcError
    g_oMPO.ForceItemUpdate = True
    With Me.Letter
        .Author.Options(.Template.ID).Refresh
    End With
    cmbAuthor_ItemChange
    If Not Me.Initializing Then
        g_oMPO.ScreenUpdating = True
        g_oMPO.ForceItemUpdate = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oOptForm_DefaultOptionsChange()
'options of the default author have changed -
'write these changes to the template
    On Error GoTo ProcError
    With Me.Letter
        .Author.Options(.Template.ID).Refresh
        'GLOG : 5598 : ceh
        If g_bUpdatePreferencesInDialog Then
            .Template.UpdateDefaults .DefaultAuthor.ID, Me.Letter
             m_bUpdateDefaults = False
        Else
             m_bUpdateDefaults = True
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_BeforeItemAdded(xItem As String)
    On Error GoTo ProcError
    xItem = mpbase2.AppendToDPhrase(xItem)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_GotFocus()
    DoEvents
    OnControlGotFocus Me.mlcDeliveryPhrase, "zzmpDeliveryPhrases"
End Sub

Private Sub mlcDeliveryPhrase_LostFocus()
    On Error GoTo ProcError
    Letter.DeliveryPhrases = _
        Me.mlcDeliveryPhrase.Text
    DoEvents
    If Not Me.Initializing Then
        Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub mlcDeliveryPhrase2_GotFocus() '9.7.1 #4024
    OnControlGotFocus Me.mlcDeliveryPhrase2, "zzmpDeliveryPhrases2"
End Sub

Private Sub mlcDeliveryPhrase2_LostFocus() '9.7.1 #4024
    On Error GoTo ProcError
    Letter.DeliveryPhrases2 = Me.mlcDeliveryPhrase2.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'******************************************************************************
'Menu events
'******************************************************************************
Private Sub mnuAuthor_ManageLists_Click()
'   get current selection
    
    Dim lID As Long
    
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ManageAuthors Me.cmbAuthor, lID
    Else
        m_oMenu.ManageAuthors Me.cmbAuthor
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_AddNew_Click()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.cmbAuthor.Columns("fldID")
    m_oMenu.AddPerson Me.cmbAuthor, lID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Favorite_Click()
    Dim lID As Long
    
    On Error Resume Next
    DoEvents
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ToggleFavorite Me.cmbAuthor, lID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    On Error GoTo ProcError
    With Letter.Template
        .DefaultAuthor = Me.Letter.Author
        'GLOG : 5598 : ceh
        If g_bUpdatePreferencesInDialog Then
            .UpdateDefaults .DefaultAuthor.ID, Me.Letter
             m_bUpdateDefaults = False
        Else
             m_bUpdateDefaults = True
        End If
    End With
    
    If Not Me.Initializing Then
        Application.ScreenUpdating = True
        Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Copy_Click()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
    On Error GoTo ProcError
'   if author isn't in list, the
'   author is the reuse author
    'If Me.cmbAuthor.Row = -1 Then
    If TypeOf Me.Letter.Author Is CReUseAuthor Then '***4337
'       use reuse author as source
        Set oSource = Me.Letter.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oMPO.People(Me.cmbAuthor.BoundText)
    End If
    
'   copy the person
    Set oCopy = m_PersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_CopyOptions_Click()
    On Error GoTo ProcError
    m_oOptForm.CopyOptions Me.Letter.Author, Me.Letter.Template.ID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_EditAuthorOptions_Click()
    On Error GoTo ProcError
    DoEvents
    m_oOptForm.Show Me.Letter
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor2_Clear_Click()
    Me.cmbAuthor2.Bookmark = 0
    Letter.Author2 = Nothing
    
End Sub

Private Sub rlGrid_FormatChanged()
    Application.ScreenUpdating = False
    SetRelineUnderline
    If Not Me.Initializing Then
        rlGrid_LostFocus
    End If
    If Not Me.Initializing Then
        Application.ScreenUpdating = True
        Application.ScreenRefresh
    End If
    EchoOn
End Sub

Private Sub rlGrid_GotFocus()
    '---9.7.1 - 4013r929
    Dim xBm As String
    If rlGrid.VisiblePane = [Reline Grid] Then
        xBm = "zzmpCarrierReline"
    Else
        xBm = "zzmpReline"
    End If
    
    If xBm = "" Then
        xBm = "zzmpReline"
    End If
    If Not Me.Initializing Then
        Application.ScreenUpdating = True
        OnControlGotFocus rlGrid, xBm
        Application.ScreenRefresh
    End If
End Sub

Private Sub rlGrid_LostFocus()
    On Error GoTo ProcError
'---9.7.1 - 4013r929
    Dim bDyn As Boolean
    bDyn = g_oMPO.ForceItemUpdate
    g_oMPO.ForceItemUpdate = True
    '---format reline - capture value from grid right click menu
    Me.Letter.RelineFormatValues = rlGrid.FormatValues
    SetRelineUnderline

    Application.ScreenUpdating = False
'---modify if client allows both relines in doc
    '---9.7.1 - 4251
    '9.7.2/9.7.1050 - #4702
    '9.7.3 #4730
    If Letter.ToggleCarrierReline = False Then
        Me.Letter.CarrierReline = CarrierReLineNew
        Letter.Reline = Me.rlGrid.RelineValues
    Else
        If Me.rlGrid.VisiblePane = [Reline Textbox] Then
            Me.Letter.Reline = rlGrid.RelineValues
        Else
            Me.Letter.Reline = CarrierReLineNew
        End If
    End If
    
    FormatReline
    
    If Not Me.Initializing Then
        Application.ScreenUpdating = True
        Application.ScreenRefresh
        g_oMPO.ForceItemUpdate = bDyn
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    g_oMPO.ForceItemUpdate = bDyn
    Exit Sub

End Sub

Private Sub rlGrid_TogglePane()
    '---9.7.1 - 4013r929
    Dim xBm As String
    If rlGrid.VisiblePane = [Reline Grid] Then
        xBm = "zzmpCarrierReline"
    Else
        xBm = "zzmpReline"
    End If
    
    If xBm = "" Then
        xBm = "zzmpReline"
    End If
    
    Me.Letter.DefaultRelineGridPane = Me.rlGrid.VisiblePane
    
    If Not Me.Initializing Then
        Application.ScreenUpdating = True
        OnControlGotFocus rlGrid, xBm
        Application.ScreenRefresh
    End If
End Sub

Private Sub txtBCC_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_BCC
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_LostFocus()
    On Error GoTo ProcError
    With Me.Letter.Signature
        .BCC = Me.txtBCC
        If .BCC = "" Then
            .BCCFromCI = ""
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCC_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_CC
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCC_LostFocus()
    On Error GoTo ProcError
    With Me.Letter.Signature
        .CC = Me.txtCC
        If .CC = "" Then
            .CCFromCI = ""
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludePhone = _
                -(Me.chkIncludeLetterheadPhone)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadPhone, "zzmpLetterHeadPhone"
End Sub

Private Sub chkIncludeLetterheadEMail_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeEMail = _
                -(Me.chkIncludeLetterheadEMail)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadEMail_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadEMail, "zzmpLetterheadEMail"
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeAdmittedIn = _
                -(Me.chkIncludeLetterheadAdmittedIn)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadAdmittedIn, "zzmpLetterheadAdmittedIn"
End Sub
Private Sub chkIncludeLetterheadCustom1_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeCustom1Detail = _
                -(Me.chkIncludeLetterheadCustom1)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom1_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom1, "zzmpLetterheadCustom1Detail"
End Sub
Private Sub chkIncludeLetterheadCustom2_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeCustom2Detail = _
                -(Me.chkIncludeLetterheadCustom2)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom2_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom2, "zzmpLetterheadCustom2Detail"
End Sub
Private Sub chkIncludeLetterheadCustom3_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeCustom3Detail = _
                -(Me.chkIncludeLetterheadCustom3)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom3_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom3, "zzmpLetterheadCustom3Detail"
End Sub

Private Sub chkIncludeLetterheadFax_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeFax = _
                -(Me.chkIncludeLetterheadFax)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadFax_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadFax, "zzmpLetterheadFax"
End Sub

Private Sub chkIncludeLetterheadTitle_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeTitle = _
                -(Me.chkIncludeLetterheadTitle)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadTitle, "zzmpLetterheadTitle"
End Sub

Private Sub chkIncludeLetterheadName_Click()
    On Error GoTo ProcError
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Letterhead.IncludeName = _
                -(Me.chkIncludeLetterheadName)
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadName_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadName, "zzmpLetterheadName"
End Sub

Private Sub cmbClosingPhrases_GotFocus()
    OnControlGotFocus Me.cmbClosingPhrases, "zzmpClosingPhrase"
End Sub

Private Sub cmbClosingPhrases_LostFocus()
    On Error GoTo ProcError
    Letter.Signature.ClosingPhrase = Me.cmbClosingPhrases
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbClosingPhrases_SelChange(Cancel As Integer)
    On Error GoTo ProcError
    cmbClosingPhrases_LostFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbEnclosures_GotFocus()
    OnControlGotFocus Me.cmbEnclosures, "zzmpEnclosures"
End Sub

Private Sub cmbEnclosures_LostFocus()
    On Error GoTo ProcError
    Letter.Signature.Enclosures = Me.cmbEnclosures
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub chkIncludeAuthorTitle_Click()
    On Error GoTo ProcError
    If Me.CascadeUpdates Then
        g_oMPO.ScreenUpdating = False
        Letter.Signature.IncludeAuthorTitle = _
            0 - Me.chkIncludeAuthorTitle
        g_oMPO.ScreenUpdating = True
        Word.Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeAuthorTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeAuthorTitle, "zzmpAuthorTitle"
End Sub
Private Sub chkElectronicSignature_Click()
    Dim bFirmName As Boolean
    
    On Error GoTo ProcError
    If Me.CascadeUpdates Then
        Word.Application.ScreenUpdating = False
        Letter.Signature.UseElectronicSignature = _
            0 - Me.chkElectronicSignature
            
        'v9.8.1010 - #4792 - force IncludeFirmName value to update
        'note: should be debugged & fixed in MPO
        bFirmName = Letter.Signature.IncludeFirmName
        Letter.Signature.IncludeFirmName = mpTrue
        Letter.Signature.IncludeFirmName = bFirmName
        
        Word.Application.ScreenUpdating = True
        Word.Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkElectronicSignature_GotFocus()
    OnControlGotFocus Me.chkElectronicSignature
End Sub

Private Sub chkIncludeDPhrasesInHeader_Click()
    On Error GoTo ProcError
    If Me.CascadeUpdates Then
        Application.ScreenUpdating = False
        Letter.IncludeDPhrasesInHeader = _
            0 - Me.chkIncludeDPhrasesInHeader
            Application.ScreenUpdating = True
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub chkIncludeDPhrases2InHeader_Click() '9.7.1 #4024
    On Error GoTo ProcError
    If Me.CascadeUpdates Then
        Application.ScreenUpdating = False
        Letter.IncludeDPhrases2InHeader = _
            0 - Me.chkIncludeDPhrases2InHeader
            Application.ScreenUpdating = True
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub chkIncludeDPhrases2InHeader_GotFocus() '9.7.1 #4024
    OnControlGotFocus Me.chkIncludeDPhrases2InHeader, "zzmpHeaderDeliveryPhrases2"
End Sub

Private Sub chkIncludeFirmName_Click()
    Dim xFirmName As String
    On Error GoTo ProcError
     '---9.4.1 -- handle firm name change from database
    With Me.Letter
        'v 9.8.1010
        On Error Resume Next
        xFirmName = Letter.Author.Office.FirmName
        On Error GoTo ProcError
        
        If xFirmName <> "" Then
            If UCase(.Signature.AuthorFirmName) <> UCase(xFirmName) Then
                .Signature.AuthorFirmName = Letter.Author.Office.FirmName
                '---this must be unhidden too
                Letter.Document.HideItem "zzmpFirmName", Me.chkIncludeFirmName <> vbChecked
            End If
        End If
    End With
    '---End 9.4.1
    If Me.CascadeUpdates Then
        With Word.Application
            g_oMPO.ScreenUpdating = False
            Letter.Signature.IncludeFirmName = 0 - Me.chkIncludeFirmName
            g_oMPO.ScreenUpdating = True
            .ScreenRefresh
        End With
    End If
    Exit Sub
ProcError:
        Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeFirmName_GotFocus()
    OnControlGotFocus Me.chkIncludeFirmName, "zzmpFirmName"
End Sub

Private Sub btnFinish_Click()
    Dim oRange As Word.Range
    On Error GoTo ProcError
    g_oMPO.SendToDebugLog "Start", "MacPac90.frmLetter.btnFinish_Click"
    DoEvents
    Me.Hide
    Me.Cancelled = False

        '9.7.3 #4752
    If Not Me.rlGrid.IncludeSpecialReline Then
        Me.Letter.CarrierReline = ""
    End If
    '---9.7.1 - 4013
    FormatReline
    
    DoEvents
    
    'GLOG : 5598 : ceh
    If m_bUpdateDefaults Then
        With Me.Letter.Template
            .UpdateDefaults .DefaultAuthor.ID, Me.Letter
            m_bUpdateDefaults = False
        End With
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.btnFinish.SetFocus
    Me.Hide
    Me.Cancelled = True
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtHeaderAdditionalText_GotFocus()
    OnControlGotFocus Me.txtHeaderAdditionalText, "zzmpHeaderAdditionalText"
End Sub

Private Sub txtHeaderAdditionalText_LostFocus()
    On Error GoTo ProcError
    Letter.HeaderAdditionalText = Me.txtHeaderAdditionalText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtRecipients_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_To
    Me.txtRecipients.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtRecipients_Change()
    Dim iNumEntries As Integer
    On Error GoTo ProcError
    If Me.txtRecipients <> "" Then
        iNumEntries = lCountChrs(Me.txtRecipients, vbCrLf & vbCrLf) + 1
        If iNumEntries = 1 Then
            Me.lblTo = " &To:" & vbCrLf & " (1 recipient)"
        Else
            Me.lblTo = " &To:" & vbCrLf & " (" & iNumEntries & " recipients)"
        End If
    Else
        Me.lblTo = " &To:"
    
'       clear ClientMatter filter
        With Me.cmbCM
            If g_bCM_RunConnected And g_bCM_AllowFilter Then
                Me.cmbCM.AdditionalClientFilter = ""
                Me.cmbCM.AdditionalMatterFilter = ""
                Me.cmbCM.Refresh
            End If
        End With
        
'       clear Salutation list
        Me.cmbSalutation.ClearList
        
    End If
    
    With Me.cmbSalutation
        If Not m_bChangingRecipsWithoutTyping And .AllowDropdown Then
            'the recips textbox is being changed by keystrokes-
            'since cmbSalutation doesn't know the changes, we
            'disable the dropdown salutation list
            .AllowDropdown = False
            .ShowDropdown False
        
            'save salutation list for reuse
            Letter.Document.SaveItem "SalParsedData", "Letter", , ""
        End If
    End With
    
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCC_Change()
    Dim iNumEntries As Integer
    On Error GoTo ProcError
    UpdateLabelCaption txtCC, Me.lblCC, m_xDefaultCCLabel, "copy", "copies" '9.7.1 #4151
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_Change()
    Dim iNumEntries As Integer
    On Error GoTo ProcError
    UpdateLabelCaption txtBCC, Me.lblBCC, m_xDefaultBCCLabel, "copy", "copies" '9.7.1 #4151
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtTypistInitials_LostFocus()
    On Error GoTo ProcError
    Letter.Signature.TypistInitials = Me.txtTypistInitials
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtAuthorInitials_LostFocus()
    '---9.7.1 - 4149
    On Error GoTo ProcError
    Letter.Signature.AuthorInitials = Me.txtAuthorInitials
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtAuthorInitials_GotFocus()
    '---9.7.1 - 4149
    OnControlGotFocus Me.txtAuthorInitials ', "zzmpAuthorInitials"
End Sub


Private Sub txtBCC_GotFocus()
    OnControlGotFocus Me.txtBCC, "zzmpBCC"
End Sub

Private Sub txtRecipients_GotFocus()
    '---9.7.1 - 4013
    DoEvents
    OnControlGotFocus Me.txtRecipients, "zzmpRecipients"
End Sub

Private Sub txtCC_GotFocus()
    OnControlGotFocus Me.txtCC, "zzmpCC"
End Sub

Private Sub txtTypistInitials_GotFocus()
    OnControlGotFocus Me.txtTypistInitials, "zzmpTypistInitials"
End Sub

Private Sub txtRecipients_LostFocus()
    On Error GoTo ProcError

    If Me.chkRecipientsTableFormat = vbChecked Then _
        Letter.UserRecipientTableThreshold = lCountChrs(Me.txtRecipients, vbCrLf & vbCrLf) + 1  '9.9.1 #5122
    
    Application.ScreenUpdating = False
    Letter.Recipients = Me.txtRecipients
    
    Letter.HeaderAdditionalText = ExtractFirstLineWithSeparators(txtRecipients, , _
                                                                 Me.Letter.HeaderNamesSeparator, _
                                                                 Me.Letter.HeaderNamesFinalSeparator)  '9.7.1 #4162
                                                                 
    Me.txtHeaderAdditionalText = Letter.HeaderAdditionalText '9.7.1 - #4162
    
    If Not Me.Initializing = True Then
        Application.ScreenUpdating = True
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub tabPages_Click()
    On Error GoTo ProcError
     Select Case Me.tabPages.CurrTab
        Case 0
            Me.cmbAuthor.SetFocus
        Case 1
            SelectFirstAvailableControl Me, Me.cmbClosingPhrases.TabIndex, Me.chkIncludeSignature.TabIndex
        Case 2
            SelectFirstAvailableControl Me, Me.cmbLetterheadType.TabIndex, Me.chkIncludeDPhrases2InHeader.TabIndex
    End Select
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Form Event Procedures
'**********************************************************
Private Sub Form_Activate()
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    On Error GoTo ProcError
    If Me.Initializing Then

        Me.tabPages = 0
        '---9.7.1 - 4039/4149
        DisplayControls
        SetInterfaceOptions Me, Me.Letter.Template.ID '9.7.1 - #4160
        m_xDefaultBCCLabel = Me.lblBCC.Caption '9.7.1 #4151
        m_xDefaultCCLabel = Me.lblCC.Caption '9.7.1 #4151
        PositionControls ' 9.7.1 - #4160
        
        With Letter
            If .Document.IsCreated Then
                UpdateForm
                '---9.7.1 - 4013
                SetRelineUnderline
                FormatReline

            Else
'               set author to default author
                Me.cmbAuthor.BoundText = .Author.ID
                
                DoEvents

'               get prefills
                Me.cmbSalutation.Text = .DefaultSalutation
                .Salutation = .DefaultSalutation
                
'               get typist initials from user ini
                With .Signature
                    .TypistInitials = _
                        GetUserIni(.Template.ID, "TypistInitials")
                    Me.txtTypistInitials = .TypistInitials
                End With
                
'               show options for author in dlg
                Me.ShowOptions .Author.ID
                
                bResetLetterheadCombo Me.cmbLetterheadType, _
                                     .Letterhead

                '---9.7.1 4149
                If Letter.Signature.AllowAuthorInitials Then
                    txtAuthorInitials.Text = BuildAuthorInitials
                    Letter.Signature.AuthorInitials = txtAuthorInitials
                End If
                
                '---9.7.1 - 4147
                Letter.Author2 = Nothing
                cmbAuthor2.Bookmark = 0
                
                Me.cmbInsertText.Bookmark = 0 '9.7.1 #4221
                Me.bAuthorIsDirty = False
                
                Letter.UserRecipientTableThreshold = 0 '9.9.1 #5122
                
            End If
            
            '---9.7.1 - no code there anymore - replace by displaycontrols and positioncontrols
            SetInterfaceControls
            
            SendKeys "+"
            
            
        End With

        Me.Initializing = False
    End If
    
    g_oMPO.ForceItemUpdate = False
    DoEvents
    Screen.MousePointer = vbDefault
    
    ActiveDocument.ActiveWindow.View.ShowHiddenText = False
    
    Me.cmbAuthor.SetFocus
    '9.7.1 #4302
    
'   move dialog to last position
    'MoveToLastPosition Me
    On Error Resume Next
    ActiveWindow.PageScroll , 1
    EchoOn
    Word.Application.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oMPO.ForceItemUpdate = False
    EchoOn
    Word.Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    Me.Initializing = False
    g_oError.Show Err, "Error Activating Letter Form"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim xTemplate As String
    Dim xMsg As String
    Dim xVal As String
    Dim xarTemp As XArray
    Dim iDefault As Integer
    Dim i As Integer
    
    

    On Error GoTo ProcError
    g_oMPO.ScreenUpdating = False
    Me.Initializing = True
    
    Me.Top = 20000
        
    Set m_PersonsForm = New MPO.CPersonsForm
    Set m_oContacts = New MPO.CContacts
    Set m_oOptForm = New MPO.COptionsForm
    Set m_oMenu = New MacPac90.CAuthorMenu

    Me.Cancelled = True
    
'   ensure that we have a Letter object
    If Me.Letter Is Nothing Then
        Me.Letter = g_oMPO.NewLetter
    End If

    With Me.Letter
        With .Document
            .StatusBar = "Initializing.  Please wait..."
            If .File.ProtectionType <> wdNoProtection Then
                    .File.Unprotect
            End If
        End With
    
        With g_oMPO.Lists
            '9.7.1 #4024
            If Me.Letter.SplitDeliveryPhrases Then
                Me.mlcDeliveryPhrase.List = .Item("DPhrases1").ListItems.Source
                Me.mlcDeliveryPhrase2.List = .Item("DPhrases2").ListItems.Source
                Me.lblDeliveryPhrase2.Visible = True
                Me.mlcDeliveryPhrase2.Visible = True
                Me.chkIncludeDPhrases2InHeader.Visible = True
            Else
                Me.mlcDeliveryPhrase.Height = Me.mlcDeliveryPhrase.Height + 150
                Me.mlcDeliveryPhrase.List = .Item("DPhrases").ListItems.Source
                Me.lblDeliveryPhrase2.Visible = False
                Me.mlcDeliveryPhrase2.Visible = False
                Me.chkIncludeDPhrases2InHeader.Visible = False
            End If
            Me.cmbClosingPhrases.Array = .Item("ClosingPhrases").ListItems.Source
            Me.cmbEnclosures.Array = .Item("Enclosures").ListItems.Source
            Me.cmbDateType.Array = .Item("DateFormats").ListItems.Source
            
'9.7.1 #4221
            Dim oList As mpDB.CList
            Set oList = .Item("DocumentInserts")
            With oList
                .FilterValue = Me.Letter.Template.ID
                .Refresh
                If .ListItems.Count Then
                    oList.ListItems.Source.Insert 1, 0
                    oList.ListItems.Source.value(0, 0) = "(none)"
                    oList.ListItems.Source.value(0, 1) = 0
                    Me.cmbInsertText.Array = oList.ListItems.Source
                    ResizeTDBCombo Me.cmbInsertText, 5
                Else
                    Me.cmbInsertText.Visible = False
                    Me.lblInsertText.Visible = False
                End If
            End With
        'end #4221
            
            '---9.7.1 - 4013 - we have to call new function because we need 4 col array to support default
            Dim oHelp As New mpDB.CHelper
            Set xarTemp = oHelp.TableToXArray("mpPublic", "tblSpecialRelinePrefills", , "fldDescription")
            iDefault = Val(xGetTemplateConfigVal(Me.Letter.Template.ID, "SpecialRelinePrefillsDefault"))
            'set default based on correspondence types
            If iDefault Then
                With xarTemp
                    For i = 0 To .UpperBound(1)
                        If .value(i, 0) = iDefault Then
                            .value(i, 3) = "True"
                        Else
                            .value(i, 3) = "False"
                        End If
                    Next i
                End With
            End If
            Me.rlGrid.CarrierPrefillLists = xarTemp
    
        End With
        xTemplate = .Template.ID

'       set dialog caption to short name
        Me.Caption = "Create a " & .Template.ShortName
        
        With g_oMPO.db.LetterheadDefs(xTemplate)
            If .Count Then
                Me.cmbLetterheadType.Array = .ListSource
            Else
                xMsg = "No letterhead types have been assigned to " & xTemplate & _
                    ".  Either assign types to " & xTemplate & " in tblLetterheadAssignments, or specify a base template in tblTemplates.BaseTemplate that has letterhead assignments."
                MsgBox xMsg, vbExclamation, App.Title
            End If
        End With
    End With
    
    ResizeTDBCombo Me.cmbLetterheadType, 6
    ResizeTDBCombo Me.cmbClosingPhrases, 10
    ResizeTDBCombo Me.cmbEnclosures, 4
    ResizeTDBCombo Me.cmbDateType, 4

    '---9.7.1 - 2136, 4013, 4251
    Me.rlGrid.GridRows = Max(3, CDbl(Val(xGetTemplateConfigVal(Letter.Template.ID, "RelineGridRows"))))

    g_oMPO.People.Refresh
    
    With Me.cmbAuthor
        .Array = g_oMPO.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
        DoEvents
    End With
    
    '---9.7.1 - 4147
    With Me.cmbAuthor2
        .Array = xARClone(g_oMPO.People.ListSource)
        .Rebind
        .Array.Insert 1, 0
        .Array.value(0, 0) = -1
        .Array.value(0, 1) = "-none-"
        .Array.value(0, 2) = -1
        ResizeTDBCombo Me.cmbAuthor2, 10
        DoEvents
    End With

'   update client\matter label
    Me.lblClientMatter.Caption = Me.Letter.ClientMatterDBoxCaption
    
'   set Client Matter Control properties
    With Me.cmbCM
        .RunConnectedToolTip = g_xCM_RunConnectedTooltip
        .RunDisconnectedToolTip = g_xCM_RunDisconnectedTooltip
        .RunConnected = g_bCM_RunConnected
        If Not .RunConnected Then
            .ComboType = ComboType_TextOnly
        Else
            .SourceClass = g_xCM_Backend
            If g_bCM_SearchByMatter Then .SearchMode = 1 '***9.7.1 - #4186
            .AllowUserToClearFilters = g_bCM_AllowUserToClearFilters
            .ShowLoadMessage = g_bCM_ShowLoadMsg
            .Column1Heading = g_xCM_Column1Heading
            .Column2Heading = g_xCM_Column2Heading
            .Separator = g_xCM_Separator
        End If
    End With
    
'   set Salutation control properties
    With Me.cmbSalutation
        .Schema = xGetTemplateConfigVal(Me.Letter.Template.ID, _
                    "SalutationSchema")
                    
        xVal = xGetTemplateConfigVal(Me.Letter.Template.ID, _
                    "MultiRecipientFormats")
        If UCase(xVal) = "TRUE" Then
            .MultiRecipientFormats = True
        End If
        
        xVal = xGetTemplateConfigVal(Me.Letter.Template.ID, _
                    "MultiRecipientSeparator")
        If xVal <> "" Then
            .MultiRecipientSeparator = xVal
        End If

    End With
    
    '9.9.1 #5122 !gm
    Me.chkRecipientsTableFormat.Visible = Me.Letter.AllowUserRecipientThreshold
    
    '9.7.1 #4028
    If g_xLHCustom1Caption <> "" Then
        Me.chkIncludeLetterheadCustom1.Caption = g_xLHCustom1Caption
        Me.chkIncludeLetterheadCustom1.Visible = True
    Else
        Me.chkIncludeLetterheadCustom1.Visible = False
    End If
    If g_xLHCustom2Caption <> "" Then
        Me.chkIncludeLetterheadCustom2.Caption = g_xLHCustom2Caption
        Me.chkIncludeLetterheadCustom2.Visible = True
    Else
        Me.chkIncludeLetterheadCustom2.Visible = False
    End If
    If g_xLHCustom3Caption <> "" Then
        Me.chkIncludeLetterheadCustom3.Caption = g_xLHCustom3Caption
        Me.chkIncludeLetterheadCustom3.Visible = True
    Else
        Me.chkIncludeLetterheadCustom3.Visible = False
    End If
    '---
'   ensure date update
    g_oMPO.ForceItemUpdate = True
    Me.Letter.DateType = Me.Letter.DateType
    g_oMPO.ForceItemUpdate = False

'   show AddressBooks button
    Me.btnAddressBooks.Visible = g_bShowAddressBooks

    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Error Loading Letter Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    
    'SetLastPosition Me
    
    Set m_PersonsForm = Nothing
    Set m_oContacts = Nothing
    Set m_Letter = Nothing
    Set m_oOptForm = Nothing
    Set m_oMenu = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateForm()
    Dim xTemp As String
    Dim xCR As String
    On Error GoTo ProcError
'---get properties, load controls on reuse
    With Letter
        Me.cmbAuthor.Text = .Document.ReuseAuthor.DisplayName
        m_bChangingRecipsWithoutTyping = True
        Me.txtRecipients = .Recipients
        m_bChangingRecipsWithoutTyping = False
        
        '---9.7.1 - 4148
        Me.chkIncludeSignature.value = Abs(CInt(.IncludeSignature))
        
        '---9.7.1 - 4147
        If Not Letter.Author2 Is Nothing Then
            Me.cmbAuthor2.BoundText = Letter.Author2.ID
        Else
            Me.cmbAuthor2.Bookmark = 0
        End If
        
        '---9.7.1 - 4149
        ' Make sure initials are updated
        .Signature.AuthorInitials = .Signature.AuthorInitials
        Me.txtAuthorInitials = .Signature.AuthorInitials

'---special handling for climat appended to reline
'---9.7.1 - 4013

        If .ClientMatterNumber = "" Then
            Me.rlGrid.RelineValues = .Reline
        Else
            Me.rlGrid.RelineValues = xSubstitute(.Reline, .ClientMatterNumber, "")
        End If
        
        '9.7.1 - 4013 - initialize proper reline tab
        If .AllowCarrierReline = False Then
            rlGrid.DefaultTab = [Default to Reline]
        Else
            rlGrid.DefaultTab = .DefaultRelineGridPane
        End If
        
        Me.txtCustom1 = .Custom1 '9.7.1 - #4268
        Me.txtCustom2 = .Custom2 '9.7.1 - #4268
        Me.txtCustom3 = .Custom3 '9.7.1 - #4268
        
        Me.mlcDeliveryPhrase.Text = .DeliveryPhrases
        If .SplitDeliveryPhrases Then '9.7.1 #4024
            Me.mlcDeliveryPhrase2.Text = .DeliveryPhrases2
            Me.chkIncludeDPhrases2InHeader = Abs(.IncludeDPhrases2InHeader)
        End If
        Me.txtHeaderAdditionalText = .HeaderAdditionalText
        Me.cmbDateType.BoundText = .DateType
        If .IncludeDPhrasesInHeader <> mpUndefined Then _
            Me.chkIncludeDPhrasesInHeader = Abs(.IncludeDPhrasesInHeader)
        
'---handle reline grid
        '---9.7.1 - 4251
        If Me.Letter.AllowCarrierReline = True Then
            If Letter.ToggleCarrierReline = False Then
                xCR = .CarrierReline
            Else
                xCR = .Reline
            End If
            
            '---9.7.1 - 4013 - load rlGrid
            xTemp = xSubstitute(xCR, .CarrierRelineSeparator, "|")
            xTemp = xSubstitute(xTemp, vbCrLf, "|")
            xTemp = xSubstitute(xTemp, vbTab, "")
            xTemp = xSubstitute(xTemp, Chr(11), "|")
            rlGrid.CarrierValues = xTemp
            rlGrid.FormatValues = Val(Letter.RelineFormatValues)
            FormatReline
        Else
            If .CarrierReline <> "" Then

                '---9.7.1 - 4013 load grid on reuse - this branch may never run
                '---but is here for back compatibility
                
                xTemp = xSubstitute(.CarrierReline, .CarrierRelineSeparator, "|")
                xTemp = xSubstitute(xTemp, vbCrLf, "|")
                xTemp = xSubstitute(xTemp, vbTab, "")
                xTemp = xSubstitute(xTemp, Chr(11), "|")
                rlGrid.CarrierValues = xTemp
                rlGrid.FormatValues = Letter.RelineFormatValues

            End If


        End If
            
        With .Signature
            Me.txtCC = .CC
            Me.txtBCC = .BCC
            Me.txtTypistInitials = .TypistInitials
            Me.cmbEnclosures.Text = .Enclosures
            Me.cmbClosingPhrases.Text = .ClosingPhrase
            '*** 9.7.1 #3675
            If UCase(xGetTemplateConfigVal(.Template.ID, "ForceTitleForNonAttorney")) = "TRUE" And _
                Not .Author.IsAttorney Then
                .IncludeAuthorTitle = mpTrue
                Me.chkIncludeAuthorTitle = 1
                Me.chkIncludeAuthorTitle.Enabled = False
            Else
                If .IncludeAuthorTitle <> mpUndefined Then _
                    Me.chkIncludeAuthorTitle = Abs(.IncludeAuthorTitle)
            End If
            '*** end #3675
            If .IncludeFirmName <> mpUndefined Then _
                Me.chkIncludeFirmName = Abs(.IncludeFirmName)
             Me.chkElectronicSignature = Abs(.UseElectronicSignature) '9.7.1 #4166
        End With
        
        With .Letterhead
            Me.cmbLetterheadType.BoundText = .LetterheadType
            If .IncludeEMail <> mpUndefined Then _
                Me.chkIncludeLetterheadEMail = Abs(.IncludeEMail)
            If .IncludeName <> mpUndefined Then _
                Me.chkIncludeLetterheadName = Abs(.IncludeName)
            If .IncludePhone <> mpUndefined Then _
                Me.chkIncludeLetterheadPhone = Abs(.IncludePhone)
            If .IncludeFax <> mpUndefined Then _
                Me.chkIncludeLetterheadFax = Abs(.IncludeFax)
            If .IncludeTitle <> mpUndefined Then _
                Me.chkIncludeLetterheadTitle = Abs(.IncludeTitle)
            If .IncludeAdmittedIn <> mpUndefined Then _
                Me.chkIncludeLetterheadAdmittedIn = Abs(.IncludeAdmittedIn)
            '9.7.1 #4028
            If .IncludeCustom1Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom1 = Abs(.IncludeCustom1Detail)
            If .IncludeCustom2Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom2 = Abs(.IncludeCustom2Detail)
            If .IncludeCustom3Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom3 = Abs(.IncludeCustom3Detail)
            '---
        End With
    End With
    
    With Me.cmbSalutation
        'set up list
        .ParsedContactData = Letter.Document.GetVar("Letter_1_SalParsedData")
        If .ParsedContactData = Empty And Letter.Salutation <> Empty Then
            .AllowDropdown = False
        End If
        
        .Text = Letter.Salutation
    End With

    With Me.cmbCM
        If g_bCM_AllowFilter Then  '#3818 - 9.6.2
            'set up list
            .AdditionalClientFilter = Letter.Document.GetVar("Letter_1_CMFilterString")
            .AdditionalMatterFilter = Letter.Document.GetVar("Letter_1_CMFilterString")
        End If
        .value = Me.Letter.ClientMatterNumber
    End With
    
   '9.7.1 #4221
    If Me.cmbInsertText.Visible Then
        Me.cmbInsertText.BoundText = Me.Letter.InsertType
        DoEvents
        If Len(Me.cmbInsertText.BoundText) = 0 Then
            Me.cmbInsertText.Bookmark = 0
        End If
    End If
    'end #4221
    Me.bAuthorIsDirty = False
    
    '9.9.1 #5122
    If Me.chkRecipientsTableFormat.Visible Then _
        Me.chkRecipientsTableFormat = 0 - (IIf(Letter.UserRecipientTableThreshold = "", 0, Letter.UserRecipientTableThreshold) > 0)      'GLOG : 5398 : ceh
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLetter.UpdateForm"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Public Function AppendClientMatter(xSource As String, _
                                   xNumber As String, _
                                   Optional xSep As String = " ", _
                                   Optional xLabel As String = "") As String

'---cleans out number from original string, then append to end
'---called from txtReline, txtClientMatterNumber_LostFocus
'---accommodates appended client matter number in reline

    Dim xTemp As String
    On Error GoTo ProcError
    
    If xSource <> "" Then
        If xNumber <> "" Then
            xTemp = xSubstitute(xSource, xNumber, "")
        Else
            xTemp = xSource
        End If
    Else
        xSep = ""
    End If
    If xNumber <> "" Then
        xTemp = xTemp & xSep & xLabel & xNumber
    End If
    AppendClientMatter = xTemp
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLetter.AppendClientMatter"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Sub UpdateLabelCaption(txtB As TextBox, _
                               lblL As Label, _
                               xCaptionRoot As String, _
                               Optional xRecip As String = "recipient", _
                               Optional xRecipPlural As String = "recipients")
    Dim iNumEntries As Integer
    Dim iNumEntriesComma As Integer
    
    On Error GoTo ProcError
    
    If txtB.Text <> "" Then
        iNumEntries = lCountChrs(txtB.Text, vbCrLf) + 1
        iNumEntriesComma = lCountChrs(txtB.Text, ",") + 1
        iNumEntries = CInt(Max(CDbl(iNumEntries), CDbl(iNumEntriesComma)))
        If iNumEntries = 1 Then
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (1 " & xRecip & ")"
        Else
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (" & iNumEntries & " " & xRecipPlural & ")"
        End If
    Else
        lblL = " " & xCaptionRoot
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLetterSig.UpdateLabelCaption"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub GetContacts(iDefSelection As ciSelectionLists)
    Dim vTo As Variant
    Dim vCC As Variant
    Dim vBCC As Variant
    Dim vCCList As Variant
    Dim vBCCList As Variant
    Static bCIActive As Boolean
    Dim xSQL As String
    Dim oContacts As Object
    Dim bAllowDropdown As Boolean
    
    On Error GoTo ProcError
  
    If Not g_bShowAddressBooks Then Exit Sub
    
    If bCIActive Then Exit Sub
    bCIActive = True
    
    g_oMPO.GetLetterRecipients vTo, _
                               vCC, _
                               vBCC, True, True, _
                               iDefSelection, _
                               oContacts, _
                               True, _
                               vCCList, _
                               vBCCList
                                
    m_bChangingRecipsWithoutTyping = True

    If vTo <> "" Then
        If Me.txtRecipients <> "" Then
            Me.txtRecipients = xTrimTrailingChrs(Me.txtRecipients, vbCrLf) & _
                                    vbCrLf & vbCrLf & vTo
        Else
            Me.txtRecipients = vTo
        End If
    End If
    
    '9.7.2/9.7.1040 #4676/9.7.3 #4721
    With Me.Letter.Signature
        If vCC <> "" Then
            .CCFromCI = IIf(.CCFromCI <> Empty, _
                            .CCFromCI & vbCrLf & vCCList, vCCList)
            .CCFromCIFullDetail = IIf(.CCFromCIFullDetail <> Empty, _
                                      .CCFromCIFullDetail & vbCrLf & vbCrLf & vCC, vCC)
            If Me.txtCC <> "" Then
                Me.txtCC = xTrimTrailingChrs(Me.txtCC, vbCrLf) & vbCrLf & vCCList
            Else
                Me.txtCC = vCCList
            End If
        End If
        
        If vBCC <> "" Then
            .BCCFromCI = IIf(.BCCFromCI <> Empty, _
                             .BCCFromCI & vbCrLf & vBCCList, vBCCList)
            .BCCFromCIFullDetail = IIf(.BCCFromCIFullDetail <> Empty, _
                                       .BCCFromCIFullDetail & vbCrLf & vbCrLf & vBCC, vBCC)
                                      
            If Me.txtBCC <> "" Then
                Me.txtBCC = xTrimTrailingChrs(Me.txtBCC, vbCrLf) & vbCrLf & vBCCList
            Else
                Me.txtBCC = vBCCList
            End If
        End If
    End With
    DoEvents
    
'   filter client/matter based on contacts retrieved
    If g_bCM_RunConnected And g_bCM_AllowFilter Then
        If oContacts.Count Then
            '---9.6.1
            Dim l_CIVersion As Long
            l_CIVersion = g_oMPO.Contacts.CIVersion
            
            If Me.cmbCM.AdditionalClientFilter <> "" Then
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter(oContacts)
                Else
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter2X(oContacts)
                End If
            Else
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = xGetClientMatterFilter(oContacts)
                Else
                    xSQL = xGetClientMatterFilter2X(oContacts)
                End If
            End If
            
            If xSQL <> "" Then
                Me.cmbCM.AdditionalClientFilter = xSQL
                Me.cmbCM.AdditionalMatterFilter = xSQL
                Me.cmbCM.Refresh
            End If
            '---end 9.6.1
                    
        End If
    End If
    
    On Error Resume Next
    bAllowDropdown = xGetTemplateConfigVal( _
        Me.Letter.Template.ID, "ShowSalutationDropdown")
    On Error GoTo ProcError
    
    'fill salutation dropdown if there are contacts
    'and the dropdown is to be used
    If oContacts.Count And bAllowDropdown Then
        With Me.cmbSalutation
            .AllowDropdown = True
            .ShowDropdown True

            'add the contacts to salutations
            If g_oMPO.Contacts.CIVersion = mpCIVersion_1 Then
                .AddContacts oContacts
            Else
                .AddContacts20 oContacts
            End If
            
            .RefreshList
            
            'refresh salutation in document
            DoEvents
            cmbSalutation_LostFocus
        End With
    End If
    
    Set oContacts = Nothing
    
    txtRecipients_LostFocus
    txtCC_LostFocus
    txtBCC_LostFocus
    Me.SetFocus
    bCIActive = False
    m_bChangingRecipsWithoutTyping = False
    Exit Sub
ProcError:
    bCIActive = False
    g_oError.Show Err
    Exit Sub
End Sub


Public Function CarrierReLineNew()
    '---9.7.1 - 4013
    Dim xTemp As String
    Dim xarTemp As XArray
    Dim i As Integer
    Dim iUB As Integer
    On Error Resume Next
    Dim iDeleted As Integer
    
    xTemp = rlGrid.CarrierValues
    
    If xTemp = "" Then
        CarrierReLineNew = xTemp
        Exit Function
    End If
    
    If UCase(xGetTemplateConfigVal(Letter.Template.ID, "IncludeEmptyCarrierItems")) <> "TRUE" Then
        '---convert to xArray, cycle through and delete array elements with empty values in 1 col
        xTemp = xSubstitute(xTemp, "~", "|") '& "|"
        Set xarTemp = xarStringToxArray(xTemp, 2, "|")
        If Not xarTemp Is Nothing Then
            iUB = xarTemp.UpperBound(1)
            If iUB > 0 Then
                xTemp = ""
                For i = 0 To iUB 'To 0 Step -1
                    If xarTemp.value(i, 1) <> "" Then
                        xTemp = xTemp & xarTemp.value(i, 0) & Letter.CarrierRelineSeparator & _
                                    xarTemp.value(i, 1) & vbCrLf
                    End If
                Next i
                xTemp = xTrimTrailingChrs(xTemp, vbCrLf)
            End If
        End If
    End If
    
    xTemp = xSubstitute(xTemp, "|", Letter.CarrierRelineSeparator)
    xTemp = xSubstitute(xTemp, "~", vbCrLf)
    
    CarrierReLineNew = xTemp
End Function


Private Function GridMoveNext(grdCur As TDBGrid, Optional bNextRecord As Boolean = False)
    ' Move to next column or row
    ' Redim array and add blank row if necessary
    On Error Resume Next
    With grdCur
        If .Bookmark < grdCur.Array.UpperBound(1) Then
            '---
            .MoveNext
        ElseIf Not GridCurrentRowIsEmpty(grdCur) Then
            .Update
            .Array.ReDim 0, .Bookmark + 1, 0, .Columns.Count - 1
            .Rebind
            .MoveLast
            .Col = 0
            .EditActive = True
        End If
    End With
End Function

Private Function GridCurrentRowIsEmpty(grdTest As TDBGrid) As Boolean
    Dim i As Integer
    On Error Resume Next
    GridCurrentRowIsEmpty = True
    With grdTest
        For i = 0 To .Columns.Count - 1
            If .Columns(i).Text <> "" Then
                GridCurrentRowIsEmpty = False
                Exit Function
            End If
        Next i
    End With
End Function

Private Sub SetInterfaceControls()
'''    '---9.7.1 - 4013 this function no longer used in generic, add custom code here
End Sub


Private Sub FormatReline()
    Dim bLastLine As Boolean
    Dim bSpecialBold As Boolean
    Dim bSpecialItalic As Boolean
    
    '---9.7.1 - 4013r928 capture reline format values
    With rlGrid
        '--reattribute remaining styles (this uses bitwise comparison to see whether formatvalues long contains any of these 4 constants
        On Error Resume Next
        '9.7.1020 #4321
        'save Special Reline settings
        bSpecialBold = ActiveDocument.Styles("Special Reline").Font.Bold
        bSpecialItalic = ActiveDocument.Styles("Special Reline").Font.Italic
        
        ActiveDocument.Styles("Reline").Font.Bold = (.FormatValues And mpRelineVI_RelineBold) = mpRelineVI_RelineBold
        ActiveDocument.Styles("Reline").Font.Italic = (.FormatValues And mpRelineVI_RelineItalic) = mpRelineVI_RelineItalic
        
        'reset Special Reline style
        With ActiveDocument.Styles("Special Reline").Font
            .Bold = bSpecialBold
            .Italic = bSpecialItalic
        End With
    End With
    Application.ScreenRefresh
End Sub


Private Sub SetRelineUnderline()
    Dim bLastLine As Boolean
    Dim bAll As Boolean
'---9.7.1 - 4013
'9.7.1020 #4321

    With rlGrid
        Letter.UnderlineCarrierReline = False
        bLastLine = (.FormatValues And mpRelineVI_RelineUnderscoreLast) = mpRelineVI_RelineUnderscoreLast
        bAll = (.FormatValues And mpRelineVI_RelineUnderscored) = mpRelineVI_RelineUnderscored
        
        '---underline last line of carrier if no reline present; underline last line of reline if no carrier present
'        If Me.CarrierReLineNew <> "" Then
'            If UCase(xGetTemplateConfigVal(Letter.Template.ID, "ToggleCarrierReline")) = "TRUE" Then
'                Me.Letter.UnderlineReline = bLastLine
'            Else
'                If .CarrierValues <> "" Then
'                    Letter.UnderlineReline = False
'                    Letter.UnderlineCarrierReline = bLastLine
'                Else
'                    Letter.UnderlineReline = bLastLine
'                    Letter.UnderlineCarrierReline = False
'                End If
'            End If
'        Else
            Me.Letter.UnderlineReline = bLastLine
'        End If
        
        '---set underline props
'        If .CarrierValues <> "" Then
'            Me.Letter.UnderlineCarrierRelineAll = bAll
'        End If
        
        If .RelineValues <> "" Then
            Me.Letter.UnderlineRelineAll = bAll
        End If
    
    End With
End Sub

Private Sub PositionControls()
    Dim sIncrement As Single
    Dim sOffset As Single
    Dim sAdjustIncrement As Single
    Dim i As Integer
    Dim oCtl As VB.Control
    Dim arrCtls() As String
    Dim bVisible As Boolean
    Dim iCurrTab As Integer
    Dim sTopOffset As Single
    Dim sExtraSpace As Single
    Dim iHidden As Integer

    On Error GoTo ProcError
    HideUnusedLetterheadControls Me, Me.Letter.Template.ID '9.7.1 #4158
    ReDim arrCtls(Me.Controls.Count)
'   place each control in tab order sort
    On Error Resume Next
    For Each oCtl In Me.Controls
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next
    On Error GoTo ProcError
    sIncrement = 58
    For i = cmbAuthor2.TabIndex To txtCustom1.TabIndex ' adjust if controls added on first tab after Custom2
        ' See which controls are visible to set correct spacing
        Set oCtl = Me.Controls(arrCtls(i))
        
'       do only if visible
        If Not oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.CheckBox Or _
                TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                TypeOf oCtl Is mpSal.SalutationCombo Or _
                TypeOf oCtl Is VB.TextBox) Then
                iHidden = iHidden + Int(oCtl.Height / 340) + 1
                sIncrement = sIncrement + 60
                sTopOffset = sTopOffset + 20
            ElseIf TypeOf oCtl Is mpControls4.RelineControl Then
                iHidden = iHidden + 3
                sIncrement = sIncrement + 70
                sTopOffset = sTopOffset + 20
            ElseIf (TypeOf oCtl Is mpControls.MultiLineCombo) Then
                If Me.Letter.SplitDeliveryPhrases Then
                    iHidden = iHidden + 1
                    sIncrement = sIncrement + 60
                Else
                    iHidden = iHidden + 2
                    sIncrement = sIncrement + 80
                End If
                sTopOffset = sTopOffset + 20
            End If
            
        End If
    Next i
    sIncrement = sIncrement + (2 ^ iHidden - 1)
    
    If sIncrement > 220 Then sIncrement = 220   '9.9.1 #5122
    
    ' Adjust top of first control
    Me.cmbAuthor.Top = Me.cmbAuthor.Top + sTopOffset
    Me.lblAuthor.Top = Me.cmbAuthor.Top + 30
    
    If iHidden = 0 Then
        ' Minimize spacing if all controls are visible
        sIncrement = 65
    ElseIf iHidden <= 3 Then
        ' Slightly more spacing if a 1 or 2 controls are hidden
        sIncrement = 125
    ElseIf iHidden > 0 Then
        ' More room for some other controls
        If Me.txtRecipients.Visible And (Not Me.cmbInsertText.Visible) Then _
            Me.txtRecipients.Height = Min(1625, Me.txtRecipients.Height + ((2 ^ iHidden) * iHidden))
    End If
    
    iCurrTab = 0
    bVisible = True
    sOffset = Me.cmbAuthor.Top + Me.cmbAuthor.Height + sIncrement
    
    For i = cmbAuthor2.TabIndex To txtCustom3.TabIndex 'Adjust if controls added to last tab after Include DPhrases
        Set oCtl = Me.Controls(arrCtls(i))
        If Not TypeOf oCtl Is VB.Frame Then
            If i > Me.txtCustom1.TabIndex And iCurrTab = 0 Then
                iCurrTab = 1
                sIncrement = 200
                sOffset = 200
                bVisible = False
            ElseIf i > Me.chkIncludeSignature.TabIndex And iCurrTab = 1 Then
                If Not bVisible Then
                    ' All controls on tab are hidden
                    Me.tabPages.TabVisible(iCurrTab) = False
                End If
                iCurrTab = 2
                sIncrement = 250
                sOffset = 200
                bVisible = False
            End If
            
    '       do only if visible
            If oCtl.Visible Then
                bVisible = True
                If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                    TypeOf oCtl Is VB.TextBox Or _
                    TypeOf oCtl Is VB.CheckBox Or _
                    TypeOf oCtl Is mpControls.MultiLineCombo Or _
                    TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                    TypeOf oCtl Is mpControls4.RelineControl Or _
                    TypeOf oCtl Is mpSal.SalutationCombo) Then
                    
                    If TypeOf oCtl Is mpControls.MultiLineCombo Then
                        oCtl.Top = sOffset
                        If Me.Letter.SplitDeliveryPhrases Then
                            sOffset = sOffset + (Me.cmbAuthor.Height * 1.5) + sIncrement
                        Else
                            sOffset = sOffset + (Me.cmbAuthor.Height * 2) + sIncrement
                        End If
                    ElseIf TypeOf oCtl Is MPCM.ClientMatterComboBox Then
                        oCtl.Top = sOffset
                        sOffset = sOffset + Me.cmbAuthor.Height + sIncrement
                    ElseIf TypeOf oCtl Is mpControls4.RelineControl Then
                        oCtl.Top = sOffset
'---9.7.1 - 4251
                        sOffset = sOffset + oCtl.Height + sIncrement
                    ElseIf TypeOf oCtl Is VB.CheckBox Then
    '               position control relative to previous control
                        sAdjustIncrement = 0
                        Select Case UCase(oCtl.Name)
'---9.7.1 - 4251
                            Case "CHKINCLUDEFIRMNAME"
                                If chkIncludeAuthorTitle.Visible Then
                                    sOffset = chkIncludeAuthorTitle.Top
                                Else
                                    ' Move over to fill in space
                                    oCtl.Left = chkIncludeAuthorTitle.Left
                                End If
                                oCtl.Top = sOffset + sAdjustIncrement
                                sOffset = oCtl.Top + oCtl.Height + sIncrement
                            Case "CHKRECIPIENTSTABLEFORMAT" '9.9.1 #5122
                                oCtl.Top = sOffset + sAdjustIncrement - 100
                                sOffset = oCtl.Top + oCtl.Height + sIncrement
                            Case Else
                                If InStr(UCase(oCtl.Name), "CHKINCLUDELETTERHEAD") = 0 Then
                                    oCtl.Top = sOffset + sAdjustIncrement
                                    sOffset = oCtl.Top + oCtl.Height + sIncrement
                                End If
                        End Select
                    Else
                        oCtl.Top = sOffset
                        sOffset = sOffset + oCtl.Height + sIncrement
                        If UCase(oCtl.Name) = "TXTRECIPIENTS" Then
                            sOffset = sOffset
                        End If
                        If UCase(oCtl.Name) = "CMBLETTERHEADTYPE" Then
                            sOffset = sOffset - 50
                            sOffset = PositionLetterheadControls(Me, i + 1, sOffset, sIncrement)
                        End If
                    End If
    '               set accompanying label vertical position
                    If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                        Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                    End If
                    
                End If
            End If
        End If
    Next i
    If Not bVisible Then
        ' All controls on tab are hidden
        Me.tabPages.TabVisible(iCurrTab) = False
    End If
    DoEvents
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmLetter.PositionControls"
End Sub

Public Sub DisplayControls()
    '---9.7.1 -4251
    If Me.Letter.AllowCarrierReline = True Then
        Me.rlGrid.Style = [Show Both]
    Else
        Me.rlGrid.Style = [Show Reline Only]
    End If

    '---9.7.1 - 4013 (note to JTS, xGetTemplateConfig here?
    Me.rlGrid.FormatMenuConfiguration = Val(xGetTemplateConfigVal(Letter.Template.ID, "RelineFormats"))
    
    '---9.7.1 - 4148
    Me.chkIncludeSignature.Visible = Abs(CInt(Letter.AllowOptionalSignature))
    
    '---9.7.1 - 4149
    Me.lblAuthorInitials.Visible = Abs(CInt(Letter.Signature.AllowAuthorInitials))
    Me.txtAuthorInitials.Visible = Me.lblAuthorInitials.Visible
    
    '---9.7.1 - 4147
    Me.lblAuthor2.Visible = bIniToBoolean(xGetTemplateConfigVal(Me.Letter.Template.ID, "ShowAuthor2"))
    Me.cmbAuthor2.Visible = bIniToBoolean(xGetTemplateConfigVal(Me.Letter.Template.ID, "ShowAuthor2"))
    
    Me.chkElectronicSignature.Visible = bIniToBoolean(xGetTemplateConfigVal(Me.Letter.Template.ID, _
        "AllowElectronicSignature")) '9.7.1 #4166
    
End Sub


Public Sub EnableSignatureControls()
    '---9.7.1 - 4148
    Dim bCheck As Boolean
    
    bCheck = Me.chkIncludeSignature.value = vbChecked
    
    With Me
        .lblClosingPhrase.Enabled = bCheck
        .cmbClosingPhrases.Enabled = bCheck
        .chkIncludeAuthorTitle.Enabled = bCheck
        .chkIncludeFirmName.Enabled = bCheck
        .lblTypistInitials.Enabled = bCheck
        .txtTypistInitials.Enabled = bCheck
        .txtAuthorInitials.Enabled = bCheck
        .lblAuthorInitials.Enabled = bCheck
        .lblEnclosures.Enabled = bCheck
        .cmbEnclosures.Enabled = bCheck
        .lblCC.Enabled = bCheck
        .txtCC.Enabled = bCheck
        .lblBCC.Enabled = bCheck
        .txtBCC.Enabled = bCheck
    End With

End Sub

Public Function BuildAuthorInitials() As String
    '---9.7.1 - 4148
    Dim xTemp As String
    Dim xSep As String
    
    xSep = Letter.Signature.MultiInitialsSeparator
    
    If Letter.Author2 Is Nothing Then
        xTemp = Letter.Author.Initials
    Else
        xTemp = Letter.Author.Initials & xSep & Letter.Author2.Initials
    End If
    BuildAuthorInitials = xTemp
End Function
Private Sub txtCustom1_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom1, "zzmpCustom1"
End Sub

Private Sub txtCustom1_LostFocus() '9.7.1 - #4268
    Me.Letter.Custom1 = Me.txtCustom1

End Sub
Private Sub txtCustom2_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom2, "zzmpCustom2"
End Sub

Private Sub txtCustom2_LostFocus() '9.7.1 - #4268
    Me.Letter.Custom2 = Me.txtCustom2
End Sub
Private Sub txtCustom3_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom3, "zzmpCustom3"
End Sub

Private Sub txtCustom3_LostFocus() '9.7.1 - #4268
    Me.Letter.Custom3 = Me.txtCustom3
End Sub

