VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmPleadingPaper 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert Pleading Paper"
   ClientHeight    =   4572
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   5472
   Icon            =   "frmPleadingPaper.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4572
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.TextBox txtFooterText 
      Appearance      =   0  'Flat
      Height          =   660
      Left            =   1800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   2745
      Width           =   3615
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4395
      TabIndex        =   13
      Top             =   4110
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3330
      TabIndex        =   12
      Top             =   4110
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbOffice 
      Height          =   600
      Left            =   1800
      OleObjectBlob   =   "frmPleadingPaper.frx":058A
      TabIndex        =   5
      Top             =   1485
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbPleadingPaperType 
      Height          =   600
      Left            =   1800
      OleObjectBlob   =   "frmPleadingPaper.frx":279B
      TabIndex        =   1
      Top             =   270
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbSidebar 
      Height          =   600
      Left            =   1800
      OleObjectBlob   =   "frmPleadingPaper.frx":49BF
      TabIndex        =   3
      Top             =   870
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbApplyTo 
      Height          =   600
      Left            =   1800
      OleObjectBlob   =   "frmPleadingPaper.frx":6BD9
      TabIndex        =   7
      Top             =   2085
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cbxSize 
      Height          =   600
      Left            =   1785
      OleObjectBlob   =   "frmPleadingPaper.frx":8DF3
      TabIndex        =   11
      Top             =   3600
      Width           =   3615
   End
   Begin VB.Label lblSize 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Font &Size:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   810
      TabIndex        =   10
      Top             =   3660
      Width           =   720
   End
   Begin VB.Label lblFooterText 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Footer Te&xt:"
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   -165
      TabIndex        =   8
      Top             =   2745
      Width           =   1695
   End
   Begin VB.Label lblOffice 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Sidebar  &Office:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   -60
      TabIndex        =   4
      Top             =   1560
      Width           =   1590
   End
   Begin VB.Label lblType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "T&ype:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   -60
      TabIndex        =   0
      Top             =   330
      Width           =   1590
   End
   Begin VB.Label lblSideBar 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Sidebar Format:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   -60
      TabIndex        =   2
      Top             =   930
      Width           =   1590
   End
   Begin VB.Label lblApplyTo 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Apply To:"
      ForeColor       =   &H00FFFFFF&
      Height          =   225
      Left            =   -60
      TabIndex        =   6
      Top             =   2160
      Width           =   1590
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   4980
      Left            =   -15
      Top             =   -240
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmPleadingPaper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oPleadingPaper As MPO.CPleadingPaper
Private m_oTypes As mpDB.CPleadingPaperDefs
Private m_oDB As mpDB.CDatabase
Private m_oPleadingType As mpDB.CPleadingDef
Private m_oPleadingCoverPageType As mpDB.CPleadingCoverPageDef
Private m_oPOSType As mpDB.CServiceDef
Private m_oVerificationType As mpDB.CVerificationDef
Private m_oParent As Form
Private m_oDocument As MPO.CDocument
Private m_oPrevDef As mpDB.CPleadingPaperDef
Private m_oPleading As MPO.CPleading
Private m_oVerification As MPO.CVerification
Private m_oService As MPO.CService
Private m_lFooterTextMaxLength  As Long
Private m_bFooterTextWarning As Boolean

Private m_bInit As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property
Public Property Get PleadingPaper() As MPO.CPleadingPaper
    Set PleadingPaper = m_oPleadingPaper
End Property
Public Property Set PleadingPaper(oNew As MPO.CPleadingPaper)
    Set PleadingPaper = m_oPleadingPaper
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property
Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get PleadingType() As mpDB.CPleadingDef
    Set PleadingType = m_oPleadingType
End Property
Public Property Let PleadingType(oNew As mpDB.CPleadingDef)
    Set m_oPleadingType = oNew
End Property
Public Property Get PleadingCoverPageType() As mpDB.CPleadingCoverPageDef
    Set PleadingCoverPageType = m_oPleadingCoverPageType
End Property
Public Property Let PleadingCoverPageType(oNew As mpDB.CPleadingCoverPageDef)
    Set m_oPleadingCoverPageType = oNew
End Property
Public Property Get POSType() As mpDB.CServiceDef
    Set POSType = m_oPOSType
End Property
Public Property Let POSType(oNew As mpDB.CServiceDef)
    Set m_oPOSType = oNew
End Property
Public Property Get VerificationType() As mpDB.CVerificationDef
    Set VerificationType = m_oVerificationType
End Property
Public Property Let VerificationType(oNew As mpDB.CVerificationDef)
    Set m_oVerificationType = oNew
End Property
Public Property Get Level0() As Long
    On Error Resume Next
    Level0 = m_oDocument.RetrieveItem("Level0", "Pleading")
End Property
Public Property Get Level1() As Long
    On Error Resume Next
    Level1 = m_oDocument.RetrieveItem("Level1", "Pleading")
End Property
Public Property Get Level2() As Long
    On Error Resume Next
    Level2 = m_oDocument.RetrieveItem("Level2", "Pleading")
End Property
Public Property Get Level3() As Long
    On Error Resume Next
    Level3 = m_oDocument.RetrieveItem("Level3", "Pleading")
End Property
'---9.3.1
Public Property Get Pleading() As MPO.CPleading
    Set Pleading = m_oPleading
End Property
Public Property Let Pleading(oNew As MPO.CPleading)
    Set m_oPleading = oNew
End Property
Public Property Get Service() As MPO.CService
    Set Service = m_oService
End Property
Public Property Let Service(oNew As MPO.CService)
    Set m_oService = oNew
End Property

Public Property Get Verification() As MPO.CVerification
    Set Verification = m_oVerification
End Property
Public Property Let Verification(oNew As MPO.CVerification)
    Set m_oVerification = oNew
End Property
Public Property Get FooterTextWarning() As Boolean
    FooterTextWarning = m_bFooterTextWarning
End Property
Public Property Let FooterTextWarning(bNew As Boolean)
    m_bFooterTextWarning = bNew
End Property
Public Property Get FooterTextMaxLength() As Long
    FooterTextMaxLength = m_lFooterTextMaxLength
End Property
Public Property Let FooterTextMaxLength(lNew As Long)
    m_lFooterTextMaxLength = lNew
End Property



'***********************************************
'---form event code
'***********************************************

Private Sub btnCancel_Click()
    Me.Hide
    Unload Me
End Sub

Private Sub btnCancel_GotFocus()
    SetControlBackColor Me.btnCancel
End Sub

Private Sub btnOK_Click()
    
    With Me.PleadingPaper
        '*** 9.7.1 Footer doesn't always get updated if using hotkey to click OK - 4085
        If txtFooterText.Enabled Then
            txtFooterText_LostFocus
        End If
        '*** end 9.7.1
        Me.Hide
        
        Application.ScreenRefresh
        Application.ScreenUpdating = False
        
        If Not Me.PleadingType Is Nothing Then
            '---grab numbering props from created pleading type def
            .HasCoverPageSection = Me.PleadingType.HasCoverPageSection
            .StartingPageNumber = Me.PleadingType.FirstPageNumber
            .StartPageNumberingAt = Me.PleadingType.FirstNumberedPage
            .UseBoilerplateFooterSetup = Me.PleadingType.UseBoilerplateFooterSetup
        ElseIf Not Me.POSType Is Nothing Then
            '---grab numbering props from created POS type def
            .StartingPageNumber = Me.POSType.FirstPageNumber
            .StartPageNumberingAt = Me.POSType.FirstNumberedPage
            .UseBoilerplateFooterSetup = Me.POSType.UseBoilerplateFooterSetup
        ElseIf Not Me.VerificationType Is Nothing Then
            '---grab numbering props from created Verification type def
            .StartingPageNumber = Me.VerificationType.FirstPageNumber
            .StartPageNumberingAt = Me.VerificationType.FirstNumberedPage
            .UseBoilerplateFooterSetup = Me.VerificationType.UseBoilerplateFooterSetup
        End If
        
        Set .Office = m_oDB.Offices(Me.cmbOffice.BoundText)
        
        
        With Me.PleadingPaper
        
            '---9.6.2
            '---customize sidebar office address string here.  If no string supplied
            '---default format is author.office.sentence
            
            '''.OfficeAddress = "Test Address"
        
            .SideBarType = Me.cmbSidebar.BoundText
            .Update Me.cmbApplyTo.BoundText
        End With

'---9.3.1
        If Not Me.Pleading Is Nothing Then
            '---call method to set normal and body text line spacing per pleading def
           Me.Pleading.FormatLineSpacing
        End If

'       reset paperdef if pleading paper was run
'       on 1 section only in a multi sections doc
        If Me.cmbApplyTo.BoundText <> 0 And ActiveDocument.Sections.Count > 1 Then
            Set Me.PleadingPaper.PleadingPaperDef = m_oPrevDef
        End If
    
        '---9.6.1 -- reset footer text doc size
        On Error Resume Next
        If Me.cbxSize.Enabled = True Then
            ActiveDocument.Styles("Footer Document Title").Font.Size = Me.cbxSize.Text
        End If
    End With
    Unload Me
    Application.ScreenUpdating = True

End Sub

Private Sub btnOK_GotFocus()
    SetControlBackColor Me.btnOK
End Sub

Private Sub cbxSize_GotFocus()
    OnControlGotFocus cbxSize
End Sub

Private Sub cmbApplyTo_GotFocus()
    SetControlBackColor Me.cmbApplyTo
End Sub

Private Sub cmbOffice_GotFocus()
    SetControlBackColor Me.cmbOffice
End Sub

Private Sub cmbPleadingPaperType_GotFocus()
    SetControlBackColor Me.cmbPleadingPaperType
End Sub

Private Sub cmbPleadingPaperType_ItemChange()
    '---9.5.0
    'If Me.Initializing Then Exit Sub
    Set Me.PleadingPaper.PleadingPaperDef = m_oTypes.Item(Me.cmbPleadingPaperType.BoundText)
    With Me.PleadingPaper.PleadingPaperDef
        Me.cmbSidebar.BoundText = .DefaultSidebarType
        Me.cmbSidebar.Enabled = .AllowSideBar
        Me.lblSideBar.Enabled = .AllowSideBar
        Me.lblOffice.Enabled = .AllowSideBar And UCase(cmbSidebar.Text) <> "NONE" '***9.7.1 - 4086
        Me.cmbOffice.Enabled = .AllowSideBar And UCase(cmbSidebar.Text) <> "NONE" '***9.7.1 - 4086
        '---9.6.1
        
        Dim bAllow As Boolean
        
        If Not Me.Pleading Is Nothing Then
            bAllow = Me.Pleading.Definition.FooterTextMaxChars > 0 _
                     And (.UsesFirstPageFooter Or .UsesPrimaryFooter) '***9.7.1 - 3985
        ElseIf Not Me.Verification Is Nothing Then
            bAllow = Me.Verification.Definition.FooterTextMaxChars > 0 _
                     And (.UsesFirstPageFooter Or .UsesPrimaryFooter) '***9.7.1 - 3985
        ElseIf Not Me.Service Is Nothing Then
            bAllow = Me.Service.Definition.FooterTextMaxChars > 0 _
                     And (.UsesFirstPageFooter Or .UsesPrimaryFooter) '***9.7.1 - 3985
        ElseIf Not Me.PleadingCoverPageType Is Nothing Then
            bAllow = False
        End If
        
        
        Me.txtFooterText.Enabled = bAllow
        Me.lblFooterText.Enabled = bAllow
        Me.cbxSize.Enabled = bAllow
        Me.lblSize.Enabled = bAllow
    End With
    
End Sub

Private Sub cmbSidebar_GotFocus()
    SetControlBackColor Me.cmbSidebar
End Sub

Private Sub cmbSidebar_ItemChange()
'    If Me.Initializing Then Exit Sub
    On Error Resume Next
    Me.cmbOffice.Enabled = UCase(cmbSidebar.Text) <> "NONE"
    Me.lblOffice.Enabled = UCase(cmbSidebar.Text) <> "NONE"  '***9.7.1 - 4086
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Private Sub Form_Activate()
    Dim lRet As Long
    cmbApplyTo.Bookmark = 0
    If Me.PleadingPaper.Exists Then
        '---9.4.1
        Me.cmbPleadingPaperType.BoundText = _
            Me.PleadingPaper.PleadingPaperDef.ID
        UpdateForm
        '---9.4.1
    Else
        Me.cmbOffice.BoundText = m_oDB.Offices.Default.ID
        Me.cmbPleadingPaperType.SelectedItem = 0
        If Me.cmbSidebar = Empty Then
            Me.cmbSidebar.SelectedItem = 0
        End If
        '---9.5.0
        On Error Resume Next
        If Not Me.Pleading Is Nothing Then
            If Me.Pleading.Definition.RequiresFooter = True Then
                Me.lblFooterText.Enabled = True
                Me.txtFooterText.Enabled = True
                lRet = Me.Pleading.Definition.FooterTextMaxChars
                If lRet > 0 Then
                    Me.txtFooterText.MaxLength = lRet
                    Me.FooterTextMaxLength = lRet
                End If
            End If
        End If
        If Not Me.Verification Is Nothing Then
            If Me.Verification.Definition.FooterTextMaxChars <> 0 Then
                Me.lblFooterText.Enabled = True
                Me.txtFooterText.Enabled = True
                lRet = Me.Verification.Definition.FooterTextMaxChars
                If lRet > 0 Then
                    Me.txtFooterText.MaxLength = lRet
                    Me.FooterTextMaxLength = lRet
                End If
            End If
        End If
    
        If Not Me.Service Is Nothing Then
            If Me.Service.Definition.FooterTextMaxChars <> 0 Then
                Me.lblFooterText.Enabled = True
                Me.txtFooterText.Enabled = True
                lRet = Me.Service.Definition.FooterTextMaxChars
                If lRet > 0 Then
                    Me.txtFooterText.MaxLength = lRet
                    Me.FooterTextMaxLength = lRet
                End If
            End If
        End If
    End If
    
    
    
    
    
    Me.Initializing = False
    Me.cmbPleadingPaperType.SetFocus

'   save existing paper def
    Set m_oPrevDef = Me.PleadingPaper.PleadingPaperDef
    Application.ScreenUpdating = True
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    OnFormKeyDown KeyCode
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oList As mpDB.CList
    Dim xTemp As String
    Dim at As Word.Template
    Dim xFilterField As String
    Dim lFilterValue As Long
    Dim lType As Long
    
    
    Application.ScreenUpdating = False
    Set m_oPleadingPaper = New CPleadingPaper
    Set m_oDocument = New MPO.CDocument
    Set m_oDB = g_oMPO.db
    Set m_oTypes = m_oDB.PleadingPaperDefs
    
    With Me
        .Initializing = True
    
        .cmbOffice.Array = m_oDB.Offices.ListSource
        .cmbOffice.Rebind
        
        Set at = ActiveDocument.AttachedTemplate
        
'---add branches for templates with assigned ppaper types filtered by fldXXXTypeID in tblPleadingPaperAssignments
        If InStr(UCase(at.Name), "PLEADING.DOT") Then
            If Not Me.ParentForm Is Nothing Then
                .PleadingType = Me.ParentForm.Pleading.Definition
            Else
                .PleadingType = m_oDB.PleadingDefs.ItemFromCourt( _
                    Me.Level0, Me.Level1, Me.Level2, Me.Level3)
            End If
            lFilterValue = Me.PleadingType.ID
            xFilterField = "tblPleadingPaperAssignments.fldPleadingTypeID"
        ElseIf InStr(UCase(at.Name), "VERIFICATION") Then
            If Not Me.ParentForm Is Nothing Then
                .VerificationType = Me.ParentForm.Verification.Definition
            Else
                Set m_oDocument = New CDocument
                lType = m_oDocument.RetrieveItem("TypeID", "Verification", , mpItemType_Integer)
                If lType Then _
                    .VerificationType = m_oDB.VerificationDefs.Item(lType)
            End If
            lFilterValue = Me.VerificationType.ID
            xFilterField = "tblPleadingPaperAssignments.fldVerificationTypeID"
        ElseIf InStr(UCase(at.Name), "COVERPAGE") Then
            If Not Me.ParentForm Is Nothing Then
                .PleadingCoverPageType = Me.ParentForm.PleadingCoverPage.Definition
            Else
                .PleadingCoverPageType = m_oDB.PleadingCoverPageDefs.ItemFromCourt( _
                    Me.Level0, Me.Level1, Me.Level2, Me.Level3)
            End If
            lFilterValue = Me.PleadingCoverPageType.ID
            xFilterField = "tblPleadingPaperAssignments.fldCoverPageTypeID"
        Else
            If Not Me.ParentForm Is Nothing Then
                .POSType = Me.ParentForm.Service.Definition
            Else
                Set m_oDocument = New CDocument
                lType = m_oDocument.RetrieveItem("TypeID", "Service", , mpItemType_Integer)
                If lType Then _
                    .POSType = m_oDB.ServiceDefs.Item(lType)
            End If
            lFilterValue = Me.POSType.ID
            xFilterField = "tblPleadingPaperAssignments.fldPOSTypeID"
        End If
         
        With g_oMPO.Lists
            Set oList = .Item("PleadingPapers")
            With oList
                .FilterField = xFilterField
                .FilterValue = lFilterValue
                .Refresh
                Me.cmbPleadingPaperType.Array = .ListItems.Source
            End With
        End With
        
        
        Set oList = New mpDB.CList
        With oList
            .IsDBList = False
            With .ListItems
                .Add 0, "Entire Document", Null
                .Add Me.PleadingPaper.Document.Selection.Sections(1).Index, _
                        "Current Section (Section " & Me.PleadingPaper.Document.Selection.Sections(1).Index & ")", _
                        Null
            End With
            cmbApplyTo.Array = .ListItems.Source
        End With
        
        Set oList = New mpDB.CList
        With oList
            .IsDBList = False
            With .ListItems
                .Add mpPleadingSideBarNone, "None", Null
                .Add mpPleadingSideBarPortrait, "Portrait (at bottom left)", Null
                .Add mpPleadingSideBarLandscape, "Landscape (in left margin)", Null
            End With
            cmbSidebar.Array = .ListItems.Source
            cmbSidebar.Rebind
        End With
        
        '---9.6.1
        Dim xFontSizes() As String
        Dim xSizes As String
        With Me.cbxSize
            GetIntegerSequence xFontSizes(), 6, 24
            xSizes = arrPToString(xFontSizes, "|")
            .Array = xarStringToxArray(xSizes)
            .Rebind
        End With
        
        On Error Resume Next
        Dim sSize As Single
        
        sSize = ActiveDocument.Styles("Footer Document Title").Font.Size
        
        If sSize = 0 Then sSize = 10
        Me.cbxSize.Text = sSize
        
        ResizeTDBCombo cbxSize, 4
        '---end 9.6.1
        ResizeTDBCombo cmbOffice, 8
        ResizeTDBCombo cmbSidebar, 3
        ResizeTDBCombo cmbApplyTo, 3
        ResizeTDBCombo Me.cmbPleadingPaperType, 4
    End With
    'MoveToLastPosition Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
    Set m_oPleadingPaper = Nothing
    Set m_oDocument = Nothing
    Set m_oTypes = Nothing
    Set g_oPrevControl = Nothing
End Sub

Private Sub UpdateForm()
    Dim xTemp As String
    Dim bAllow As Boolean
    Dim lRet As Long
    
    ' Load values from document variables
    With Me.PleadingPaper
   
        On Error Resume Next
        '---9.5.0
        If Not Me.Pleading Is Nothing Then
            bAllow = Me.Pleading.Definition.RequiresFooter
            lRet = Me.Pleading.Definition.FooterTextMaxChars '***9.7.1 - 4092
        ElseIf Not Me.Verification Is Nothing Then
            bAllow = (Me.Verification.Definition.FooterTextMaxChars <> 0)
            lRet = Me.Verification.Definition.FooterTextMaxChars '***9.7.1 - 4092
        ElseIf Not Me.Service Is Nothing Then
            bAllow = (Me.Service.Definition.FooterTextMaxChars <> 0)
            lRet = Me.Service.Definition.FooterTextMaxChars '***9.7.1 - 4092
        End If
        
        '---9.6.1
        If Me.PleadingPaper.DocTitle <> "" Or _
                bAllow = True Then
            
            Me.lblFooterText.Enabled = True
            Me.txtFooterText.Enabled = True
            Me.txtFooterText = Me.PleadingPaper.DocTitle
            Me.cbxSize.Enabled = True
            Me.lblSize.Enabled = True
        
            lRet = Me.Pleading.Definition.FooterTextMaxChars
            If lRet > 0 Then
                Me.txtFooterText.MaxLength = lRet
                Me.FooterTextMaxLength = lRet
            End If
        Else
            Me.lblFooterText.Enabled = False
            Me.txtFooterText.Enabled = False
            Me.cbxSize.Enabled = False
            Me.lblSize.Enabled = False
            
            Me.txtFooterText = ""
            Me.PleadingPaper.DocTitle = ""
''''            Me.btnCancel.Top = Me.txtFooterText.Top
''''            Me.btnOK.Top = Me.btnCancel.Top
''''            Me.Height = Me.Height - (Me.txtFooterText.Height + 100)
        End If
        '---end 9.6.1
        cmbOffice.BoundText = .Office.ID
        '---9.7.1 - 4109
'''        DoEvents
'''        If cmbOffice = "" Then _
'''            cmbOffice.Bookmark = 0
'''        DoEvents
        cmbPleadingPaperType.BoundText = .PleadingPaperDef.ID
''''        If cmbPleadingPaperType = "" Then _
''''            cmbPleadingPaperType.Bookmark = 0
''''        DoEvents
        cmbSidebar.BoundText = .SideBarType
''''        DoEvents
''''        If IsNull(cmbSidebar.Bookmark) Then _
''''            cmbSidebar.Bookmark = 0
    End With
End Sub

Private Sub txtFooterText_GotFocus()
    OnControlGotFocus txtFooterText
End Sub

Private Sub txtFooterText_KeyDown(KeyCode As Integer, Shift As Integer)
    '---9.7.1 - 4092 HF 9.7.1030
    Dim ShiftKey As Integer
    Dim xMsg As String
       ShiftKey = Shift And 7
    Select Case ShiftKey
        Case 4 ' or vbAltMask
           'ALT key
           Exit Sub
        Case Else
    End Select
    Select Case KeyCode
        Case vbKeyTab, vbKeyBack, vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
        
        Case Else
            If Me.FooterTextMaxLength = 0 Then Exit Sub
            If Len(txtFooterText) = Me.FooterTextMaxLength Then
                xMsg = "The footer text is" & Str(Me.FooterTextMaxLength) & " characters and might not fit in the footer table cell." & _
                    vbLf & vbLf & "Please edit the text in the footer text box."
                MsgBox xMsg, vbInformation, App.Title
            End If
    End Select
End Sub

Private Sub txtFooterText_LostFocus()
    On Error GoTo ProcError
    Me.PleadingPaper.DocTitle = Me.txtFooterText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


