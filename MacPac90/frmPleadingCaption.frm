VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmPleadingCaption 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Create a Pleading Caption or Cross-Action"
   ClientHeight    =   6468
   ClientLeft      =   3288
   ClientTop       =   1080
   ClientWidth     =   5472
   Icon            =   "frmPleadingCaption.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmPleadingCaption.frx":058A
   ScaleHeight     =   6468
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.CheckBox chkLoadPreviousValues 
      Appearance      =   0  'Flat
      Caption         =   "Load Previous &Values"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   1755
      TabIndex        =   31
      ToolTipText     =   "Load fields with values from previously created caption"
      Top             =   5580
      Width           =   3240
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3810
      TabIndex        =   32
      Top             =   5985
      Width           =   750
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4635
      TabIndex        =   33
      Top             =   5985
      Width           =   750
   End
   Begin VB.CommandButton btnNew 
      Height          =   400
      Left            =   3960
      Picture         =   "frmPleadingCaption.frx":08CC
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Add a new caption (F5)"
      Top             =   90
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4425
      Picture         =   "frmPleadingCaption.frx":0E0E
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Save new caption (F6)"
      Top             =   90
      Width           =   435
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4905
      Picture         =   "frmPleadingCaption.frx":0F8C
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Delete caption (F7)"
      Top             =   90
      Width           =   435
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   4680
      Left            =   -15
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   1710
      Width           =   5925
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|Case &Info|&Layout"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   400
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   4260
         Left            =   12
         TabIndex        =   36
         Top             =   12
         Width           =   5904
         Begin mpControls.MultiLineCombo mlcRelatedActions 
            Height          =   885
            Left            =   1755
            TabIndex        =   7
            Top             =   705
            Visible         =   0   'False
            Width           =   3630
            _ExtentX        =   6414
            _ExtentY        =   1566
            BackColor       =   -2147483643
            Separator       =   ""
            SeparatorSpecial=   0
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.TextBox txtParty3Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1760
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   17
            Top             =   3150
            Width           =   3615
         End
         Begin VB.TextBox txtParty1Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1755
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   10
            Top             =   1020
            Width           =   3615
         End
         Begin VB.TextBox txtParty2Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1760
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   14
            Top             =   2085
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbCaptionType 
            Height          =   390
            Left            =   1760
            OleObjectBlob   =   "frmPleadingCaption.frx":1116
            TabIndex        =   6
            Top             =   210
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbParty2Title 
            Height          =   390
            Left            =   1740
            OleObjectBlob   =   "frmPleadingCaption.frx":3350
            TabIndex        =   13
            Top             =   1755
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbParty1Title 
            Height          =   390
            Left            =   1760
            OleObjectBlob   =   "frmPleadingCaption.frx":5572
            TabIndex        =   9
            Top             =   690
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbParty3Title 
            Height          =   390
            Left            =   1760
            OleObjectBlob   =   "frmPleadingCaption.frx":7794
            TabIndex        =   16
            Top             =   2820
            Width           =   3630
         End
         Begin VB.Label lblVersus 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "v."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Left            =   3225
            TabIndex        =   11
            Top             =   1560
            Width           =   180
         End
         Begin VB.Label lblVersus3 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "And"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   825
            TabIndex        =   38
            Top             =   2415
            Visible         =   0   'False
            Width           =   780
         End
         Begin VB.Label lblVersus2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "v."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   1155
            TabIndex        =   37
            Top             =   1395
            Visible         =   0   'False
            Width           =   450
         End
         Begin VB.Label lblParty3Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &3:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   150
            TabIndex        =   15
            Tag             =   "Party &3:"
            Top             =   2850
            Width           =   1455
         End
         Begin VB.Label lblParty2Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &2:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   150
            TabIndex        =   12
            Tag             =   "Party &2:"
            Top             =   1800
            Width           =   1455
         End
         Begin VB.Label lblParty1Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &1:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   150
            TabIndex        =   8
            Tag             =   "Party &1:"
            Top             =   720
            Width           =   1455
         End
         Begin VB.Label lblCaptionType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   150
            TabIndex        =   5
            Top             =   240
            Width           =   1455
         End
         Begin VB.Shape Shape3 
            BorderStyle     =   0  'Transparent
            Height          =   6075
            Left            =   1770
            Top             =   -135
            Width           =   5805
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   3
            X1              =   -120
            X2              =   5550
            Y1              =   4212
            Y2              =   4212
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   4275
            Left            =   0
            Tag             =   "Party &3:"
            Top             =   -30
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   4260
         Left            =   6420
         TabIndex        =   35
         Top             =   12
         Width           =   5904
         Begin VB.TextBox txtPresidingJudge 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1760
            MultiLine       =   -1  'True
            TabIndex        =   27
            Top             =   3480
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtCaseTitle 
            Appearance      =   0  'Flat
            Height          =   660
            Left            =   1760
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   21
            Top             =   1140
            Width           =   3615
         End
         Begin VB.TextBox txtAdditionalInformation 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1760
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   25
            Top             =   3270
            Width           =   3615
         End
         Begin TrueDBGrid60.TDBGrid grdCaseNumbers 
            Height          =   780
            Left            =   1760
            OleObjectBlob   =   "frmPleadingCaption.frx":99B6
            TabIndex        =   19
            ToolTipText     =   "Use CTL/Enter to create multiline case numbers, Up/Down arrows to scroll individual case numbers"
            Top             =   90
            Width           =   3615
         End
         Begin TrueDBGrid60.TDBGrid grdDateTime 
            Height          =   975
            Left            =   1760
            OleObjectBlob   =   "frmPleadingCaption.frx":C0BE
            TabIndex        =   23
            Top             =   2060
            Width           =   3615
         End
         Begin VB.Label lblPresidingJudge 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Presiding &Judge(s):"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   135
            TabIndex        =   26
            Top             =   3525
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblDateTimeInfo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Date && &Time Info:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -420
            TabIndex        =   22
            Top             =   2060
            Width           =   2010
         End
         Begin VB.Label lblCaseNumber 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Case &Noxxxx:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -420
            TabIndex        =   18
            Top             =   120
            Width           =   2010
         End
         Begin VB.Label lblDocumentTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Document Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -420
            TabIndex        =   20
            Top             =   1140
            Width           =   2010
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   1
            X1              =   -120
            X2              =   5795
            Y1              =   4212
            Y2              =   4212
         End
         Begin VB.Label lblAdditionalInformation 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Additional Info:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -105
            TabIndex        =   24
            Top             =   3270
            Width           =   1695
         End
         Begin VB.Shape Shape5 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   4240
            Left            =   0
            Top             =   0
            Width           =   1695
         End
         Begin VB.Shape Shape4 
            BorderStyle     =   0  'Transparent
            Height          =   6015
            Left            =   30
            Top             =   -150
            Visible         =   0   'False
            Width           =   5895
         End
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Caption         =   "b"
         Height          =   4260
         Left            =   6660
         TabIndex        =   34
         Top             =   12
         Width           =   5904
         Begin TrueDBGrid60.TDBGrid grdLayout 
            DragIcon        =   "frmPleadingCaption.frx":EACF
            Height          =   1950
            Left            =   1760
            OleObjectBlob   =   "frmPleadingCaption.frx":EC21
            TabIndex        =   29
            Top             =   255
            Width           =   3555
         End
         Begin VB.Label lblLayout 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Captions La&yout:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   120
            TabIndex        =   28
            Top             =   255
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   0
            X1              =   -168
            X2              =   5472
            Y1              =   4212
            Y2              =   4212
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   4740
            Left            =   0
            Top             =   -500
            Width           =   1695
         End
      End
   End
   Begin TrueDBList60.TDBList lstCaptions 
      Height          =   915
      Left            =   1770
      OleObjectBlob   =   "frmPleadingCaption.frx":11618
      TabIndex        =   4
      Top             =   570
      Width           =   3570
   End
   Begin VB.Label lblCaptions 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Caption:"
      ForeColor       =   &H00FFFFFF&
      Height          =   915
      Left            =   150
      TabIndex        =   3
      Top             =   555
      Width           =   1455
   End
   Begin VB.Shape Shape6 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00800080&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00800080&
      Height          =   5900
      Left            =   0
      Top             =   0
      Width           =   1695
   End
   Begin VB.Menu mnuLayout 
      Caption         =   "Caption Layout"
      Visible         =   0   'False
      Begin VB.Menu mnuCLAdd 
         Caption         =   "&Add Caption"
      End
      Begin VB.Menu mnuCLChangeDisplayName 
         Caption         =   "&Change Display Name..."
      End
      Begin VB.Menu mnuCLDeleteOne 
         Caption         =   "&Delete Caption"
      End
      Begin VB.Menu mnuCLDeleteAll 
         Caption         =   "Delete &All Captions"
      End
   End
   Begin VB.Menu mnuDateTime 
      Caption         =   "Date/Time Info"
      Visible         =   0   'False
      Begin VB.Menu mnuDateTime_Delete 
         Caption         =   "&Delete Row"
      End
      Begin VB.Menu mnuDateTime_Insert 
         Caption         =   "&Insert Row"
      End
      Begin VB.Menu mnuDateTime_Sep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDateTime_ClearAll 
         Caption         =   "&Clear All Rows"
      End
      Begin VB.Menu mnuDateTime_Reset 
         Caption         =   "&Reset to Defaults"
      End
   End
End
Attribute VB_Name = "frmPleadingCaption"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const mpCancelAdd As String = "Cancel unsaved caption (F7)"
Const mpDelete As String = "Delete this caption (F7)"
Const mpCancelChange As String = "Cancel unsaved changes (F7)"
Const mpSaveChanges As String = "Save changes to this caption (F6)"
Const mpSaveNew As String = "Save new caption (F6)"
'---Lieff
Const mpCapDateTimeSeparator As String = "|"
'---End Lieff

'---drag and drop support
Private xDragFrom() As String
Private is_SourceRow As Integer
Private is_SourceCol As Integer
Private is_DestRow As Integer
Private is_DestCol As Integer

Private m_Cap As MPO.CPleadingCaption
Private m_Caps As MPO.CPleadingCaptions
Private m_Document As MPO.CDocument
Private m_oPleadingType As mpDB.CPleadingDef
Private m_oDB As mpDB.CDatabase
Private m_bCascadeUpdates As Boolean
Private m_bCancelled As Boolean
Private m_bAdded As Boolean
Private m_bChanged As Boolean
Private m_oParent As Form
Private WithEvents m_oCap As MPO.CPleadingCaption
Attribute m_oCap.VB_VarHelpID = -1
Private WithEvents m_oCaps As MPO.CPleadingCaptions
Attribute m_oCaps.VB_VarHelpID = -1

'---9.7.1 - 2545
Private m_lPrevCapID As Long

Private m_bInitializing As Boolean

'****************************************************************
'Properties
'****************************************************************
Public Property Get PleadingCaption() As MPO.CPleadingCaption
    Set PleadingCaption = m_oCap
End Property
Public Property Let PleadingCaption(pldCapNew As MPO.CPleadingCaption)
    Set m_oCap = pldCapNew
End Property
Public Property Get PleadingCaptions() As MPO.CPleadingCaptions
    Set PleadingCaptions = m_oCaps
End Property
Public Property Let PleadingCaptions(pldCapsNew As MPO.CPleadingCaptions)
    Set m_oCaps = pldCapsNew
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property
Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get PleadingType() As mpDB.CPleadingDef
    Set PleadingType = m_oPleadingType
End Property
Public Property Let PleadingType(oNew As mpDB.CPleadingDef)
    Set m_oPleadingType = oNew
End Property
Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property
Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
    Me.CascadeUpdates = False
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = True
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Public Property Get Added() As Boolean
    Added = m_bAdded
End Property
Public Property Let Added(bNew As Boolean)
    m_bAdded = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    Me.lstCaptions.Enabled = Not bNew
    '---9.6.2
    '''''Me.chkLoadPreviousValues.Enabled = Not bNew
    Me.grdLayout.Enabled = Not bNew
    Me.lblLayout.Enabled = Not bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelAdd
        Me.btnSave.ToolTipText = mpSaveNew
    Else
        Me.btnDelete.ToolTipText = mpDelete
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Changed() As Boolean
    Changed = m_bChanged
End Property
Public Property Let Changed(bNew As Boolean)
    If Me.Initializing Then Exit Property
    m_bChanged = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelChange
        Me.btnSave.ToolTipText = mpSaveChanges
    Else
        Me.btnDelete.ToolTipText = mpDelete
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Level0() As Long
    On Error Resume Next
    Level0 = m_Document.RetrieveItem("Level0", "Pleading")
End Property
Public Property Get Level1() As Long
    On Error Resume Next
    Level1 = m_Document.RetrieveItem("Level1", "Pleading")
End Property
Public Property Get Level2() As Long
    On Error Resume Next
    Level2 = m_Document.RetrieveItem("Level2", "Pleading")
End Property
Public Property Get Level3() As Long
    On Error Resume Next
    Level3 = m_Document.RetrieveItem("Level3", "Pleading")
End Property

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Cancelled = True
    Me.Hide
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnDelete_Click()
    On Error GoTo ProcError
    If Me.Changed Then
        Me.Changed = False
        ChangeActiveCaption
    Else
        DeleteCaption
        Me.Changed = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    
    Me.Cancelled = False
    
     If Me.Added Then
        If PromptToSave = True Then
            '9.7.1 - #3984
            RefreshList
            If Me.vsIndexTab1.CurrTab = 0 Then
                Me.cmbCaptionType.SetFocus
            Else
                Me.btnFinish.SetFocus
            End If
            Exit Sub
        End If
    End If
    
    If Me.Changed Then
'       current record is dirty -
'       save automatically if there is
'       only 1 recipient, else prompt
        If Me.PleadingCaptions.Count = 1 Then
            UpdateCaption
        Else
            PromptToSave
        End If
    End If
    
    DoEvents
    Me.Hide
    Application.ScreenUpdating = False
    If ParentForm Is Nothing Then
        PleadingCaptions.Finish
    Else
        PleadingCaptions.Refresh , True, True
    End If
    Application.ScreenUpdating = True
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error finishing caption."
    Exit Sub
End Sub

Private Sub btnNew_Click()
    Dim i
    Dim iCount As Integer
    
    On Error GoTo ProcError
    iCount = CInt(Max(mpPleadingCaptionsLimit, Val(GetMacPacIni("Pleading", "MaxCaptions"))))
    
    If Me.PleadingCaptions.Count >= iCount Then
        MsgBox "You can create a maximum of " & Str$(iCount) & " pleading captions per document.  Please contact your administrator.", vbInformation, App.Title
        RefreshLayoutGrid
        Me.Changed = False
        Me.Added = False
        Exit Sub
    End If
    Me.Changed = False
    CascadeUpdates = False
    
    '---9.7.1 - 2545
    If Not IsNull(Me.lstCaptions.SelectedItem) Then
        m_lPrevCapID = Me.lstCaptions.SelectedItem
    Else
        m_lPrevCapID = 1
    End If
    
    NewCaption
    
    '---9.6.1
    Me.vsIndexTab1.CurrTab = 0

    If Me.vsIndexTab1.CurrTab = 0 Then
        Me.cmbCaptionType.SetFocus
    Else
        Me.chkLoadPreviousValues.SetFocus
    End If
    For i = 1 To 30
        DoEvents
    Next
    Me.Added = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnSave_Click()
    Dim iRow As Integer
    Dim bRet As Boolean
    On Error GoTo ProcError
    EchoOff
    iRow = lstCaptions.Bookmark
    
    bRet = SaveCaption
    If Not bRet Then
        Exit Sub
    End If
    
    '---Lieff
    RefreshLayoutGrid
    '---End Lieff
    
    RefreshList
    lstCaptions.Bookmark = iRow
    
    If Me.Added Then
        Me.Added = False
    Else
        Me.Changed = False
    End If
    lstCaptions.SetFocus
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Exit Sub
End Sub

Private Sub chkLoadPreviousValues_Click()
    
    If chkLoadPreviousValues = vbChecked Then
        With Me.lstCaptions
''''            '---9.6.2
''''            ShowCaption Me.PleadingCaptions(.Array.value(Max(.SelectedItem - 1, 1), 1)), True
            '---9.7.1 - 2545
            ShowCaption Me.PleadingCaptions(.Array.value(Max(CDbl(m_lPrevCapID), 1), 1)), True
''''            If Not Me.Added Then
                Me.Changed = True
''''            Else
''''            End If
        
        End With
    End If
End Sub

''**************************************************
''Control Event Procedures
''**************************************************
Private Sub cmbCaptionType_GotFocus()
    OnControlGotFocus cmbCaptionType
    CascadeUpdates = True
End Sub

Private Sub cmbCaptionType_ItemChange()
    If CascadeUpdates = True Then Me.Changed = True

''''    ClearGrid Me.grdCaseNumbers
''''    ClearGrid Me.grdDateTime
    DoEvents
    DisplayControls
    SetInterfaceControls
    CascadeUpdates = False
End Sub

Private Sub cmbCaptionType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCaptionType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCaptionType_Validate(Cancel As Boolean)
    If cmbCaptionType.Text = "" Then Exit Sub
    On Error GoTo ProcError
    On Error Resume Next
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCaptionType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbParty1Title_ItemChange()
    Me.Changed = True
End Sub

Private Sub cmbParty2Title_ItemChange()
    Me.Changed = True
End Sub

Private Sub cmbParty3Title_ItemChange()
    Me.Changed = True
End Sub

Private Sub Form_Activate()
    Dim bFlag As Boolean
    Dim iCount As Integer
    
    On Error GoTo ProcError
    If Not Me.ParentForm Is Nothing Then
        With Me.PleadingCaption
            .TypeID = ParentForm.PleadingCaption.TypeID
        End With
    ElseIf PleadingCaptions.Count = 0 Then
    '---use defaults to set values
    
    Else
        
    End If
    
    Me.vsIndexTab1.CurrTab = 0
    
    Me.btnDelete.ToolTipText = mpDelete
    Me.btnSave.ToolTipText = mpSaveChanges
    
    Me.CascadeUpdates = False
    DoEvents
''---this will force new input if there's only one caption

    iCount = CInt(Max(mpPleadingCaptionsLimit, Val(GetMacPacIni("Pleading", "MaxCaptions"))))
    If Me.PleadingCaptions.Count < iCount Then
'    If PleadingCaptions.Count = 1 Then
        btnNew_Click
        bFlag = True
    Else
        Me.Added = False
        Me.btnNew.SetFocus
    End If
    
    If Me.ParentForm Is Nothing Then
        'mpbase2.MoveToLastPosition Me
    Else
        Me.Top = Me.ParentForm.Top
        Me.Left = Me.ParentForm.Left
    End If
    
    If Me.vsIndexTab1.CurrTab = 0 Then
        Me.cmbCaptionType.SetFocus
    Else
        Me.chkLoadPreviousValues.SetFocus
    End If
    
    LockWindowUpdate (0&)
    Application.ScreenUpdating = True
    DoEvents
    Me.Initializing = False
    Me.Changed = False
    
    If bFlag = True Then
        Me.Added = True
        btnNew.Enabled = False
        btnSave.Enabled = True
        Me.btnDelete.ToolTipText = mpCancelAdd
    End If
        
    Exit Sub

ProcError:
    g_oError.Show Err, "Error activating pleading caption form."
    LockWindowUpdate (0&)
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oParties As XArray
    Dim oList As mpDB.CList
    On Error GoTo ProcError
    
    Me.Initializing = True
    Me.CascadeUpdates = False
    Application.ScreenUpdating = False
    Me.Top = 20000
    Me.Cancelled = True
    
    If Me.PleadingCaptions Is Nothing Then
        Me.PleadingCaptions = g_oMPO.NewPleadingCaptions
        Me.PleadingCaptions.LoadValues
        If Me.PleadingCaptions.Count = 0 Then PleadingCaptions.Add
    End If

    Set m_Document = New MPO.CDocument
    Set m_oDB = g_oMPO.db
    If PleadingCaptions.Count Then PleadingCaption = Me.PleadingCaptions(1)

    If Not Me.ParentForm Is Nothing Then
        Me.PleadingType = Me.ParentForm.Pleading.Definition
    Else
        Me.PleadingType = m_oDB.PleadingDefs.ItemFromCourt( _
            Me.Level0, Me.Level1, Me.Level2, Me.Level3)
    End If
    
'---fill combo boxes
    Set oParties = g_oMPO.Lists("PleadingParties").ListItems.Source
    Me.cmbParty1Title.Array = oParties
    Me.cmbParty2Title.Array = oParties
    Me.cmbParty3Title.Array = oParties
    
'---9.7.1 4080 fill multiline combo with related actions list
    Me.mlcRelatedActions.List = g_oMPO.Lists("PleadingRelatedActionText").ListItems.Source
    
'---fill  caption list box
    Me.lstCaptions.Array = Me.PleadingCaptions.ListArray(True)
        
'---fill caption types combo box with types appropriate to pleading type
    Set oList = g_oMPO.Lists.Item("PleadingCaptions")
    With oList
        .FilterField = "tblPleadingCaptionAssignments.fldPleadingTypeID"
        .FilterValue = Me.PleadingType.ID
        .Refresh
        Me.cmbCaptionType.Array = .ListItems.Source
    End With

    ResizeTDBCombo Me.cmbCaptionType, 4
    ResizeTDBCombo Me.cmbParty3Title, 10
    ResizeTDBCombo Me.cmbParty2Title, 10
    ResizeTDBCombo Me.cmbParty1Title, 10
    LockWindowUpdate Me.hwnd
   
    Exit Sub

ProcError:
    g_oError.Show Err, "Error loading Pleading Caption Form."
    LockWindowUpdate (0&)
    Exit Sub
End Sub

Private Sub grdCaseNumbers_AfterColUpdate(ByVal ColIndex As Integer)
    '---Lieff
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    Me.Changed = True
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_Change()
    Me.Changed = True
End Sub

Private Sub grdCaseNumbers_GotFocus()
    On Error Resume Next
    grdCaseNumbers.EditActive = True
    GridTabOut Me.grdCaseNumbers, True, 1
    OnControlGotFocus Me.grdCaseNumbers
End Sub

Private Sub grdCaseNumbers_LostFocus()
    On Error Resume Next
    grdCaseNumbers.Update
End Sub

Private Sub grdCaseNumbers_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
'    If Button = 2 Then
'        Me.PopupMenu mnuCaseNumbers
'    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub grdCaseNumbers_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
'    With grdCaseNumbers
'        '.EditActive = True
'        If Not IsNull(.Bookmark) Then
'            Me.Pleading.Document.SelectItem "zzmpCaseNumber" & .Bookmark + 1
'        End If
'    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim xText As String
    On Error GoTo ProcError
    
    '---deal with grid selecting next row and transmitting returns that overwrite existing next row text
    If Shift = 2 Then
        If KeyCode = 13 Then
            With grdCaseNumbers
                If .SelLength = 0 Then
                    KeyCode = 0
                    xText = xTrimTrailingChrs(.Columns(.Col).Text, vbCrLf)
                    .Columns(.Col).Text = xText
                    .SelStart = Len(.Columns(.Col).Text)
                End If
            End With
        End If
    End If
    
    Select Case KeyCode
        Case 9
            GridTabOut Me.grdCaseNumbers, False, 1
        Case 39
            With grdCaseNumbers
                If .SelLength > 1 Then
                    .SelStart = .SelLength - 1
                    .SelLength = 1
                End If
            End With
        Case Else
    End Select
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdDateTime_AfterColUpdate(ByVal ColIndex As Integer)
    Me.Changed = True
End Sub

Private Sub grdDateTime_GotFocus()
    GridTabOut Me.grdDateTime, True, 1
    OnControlGotFocus grdDateTime
End Sub

Private Sub grdDateTime_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If Shift = 1 Then
        If KeyCode = 9 Then
            grdDateTime.TabAction = 0
        End If
    Else
        If KeyCode = 13 Then    '---change signer if in col 1
            If grdDateTime.Col = 1 Then
                KeyCode = 0
                GridMoveNext grdDateTime
            End If
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdDateTime_LostFocus()
    grdDateTime.Update
End Sub

Private Sub grdDateTime_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
'---9.7.1 - 4182
    If Button = 2 Then
        Me.PopupMenu mnuDateTime
    End If
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdDateTime_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    
    On Error Resume Next
    With grdDateTime
        If .RowBookmark(.Row) = .Array.UpperBound(1) Then
            If .Col = 1 Then GridTabOut Me.grdDateTime, False, .Col
        ElseIf .RowBookmark(.Row) >= .Array.LowerBound(1) Then
            If .Col = 0 Then GridTabOut Me.grdDateTime, False, .Col
        End If
    GridTabOut Me.grdDateTime, False, LastCol
    End With
End Sub


Private Sub grdLayout_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
    Dim i As Integer
    
    On Error GoTo ProcError
    is_SourceRow = RowBookmark
    is_SourceCol = ColIndex

    If grdLayout.Text = Empty Then
        Exit Sub
    End If
'    Debug.Print RowBookmark
    With grdLayout.Array
    
        ReDim xDragFrom(0, 0, .UpperBound(3))
        grdLayout.MarqueeStyle = dbgHighlightCell
        
        For i = 0 To .UpperBound(3)
            xDragFrom(0, 0, i) = .value(RowBookmark, ColIndex, i)
        Next i

    End With

' Use Visual Basic manual drag support
    grdLayout.Drag vbBeginDrag
    Exit Sub
ProcError:
    g_oError.Show Err, "Error dragging cell."
    Exit Sub
End Sub


Private Sub grdLayout_DragDrop(Source As Control, x As Single, y As Single)
    Dim idestCol As Integer
    Dim idestRow As Integer
    Dim i, j, r, c As Integer
    Dim temp() As String
    Dim vSourceID As Variant
    Dim iSourcePosition As Integer
    Dim iDestPosition As Integer
    Dim m_Caption As MPO.CPleadingCaption
    Dim xARLayout As XArray

    On Error Resume Next
    
    Set xARLayout = grdLayout.Array
    
    r = Val(grdLayout.RowContaining(y))
    c = grdLayout.ColContaining(x)

'---don't allow drop if cell is occupied
'    If Len(grdLayout.Text) Then _
'        Exit Sub
    
    On Error GoTo ProcError

'---create row space in array if necessary
    If r > xARLayout.UpperBound(1) Then
        xARLayout.Insert 1, xARLayout.UpperBound(1) + r
    End If

    If Source.Name = "grdLayout" Then

'---Get coordinates of drop zone
        idestCol = Abs(grdLayout.ColContaining(x))
        idestRow = grdLayout.RowBookmark(grdLayout.RowContaining(y))
'---exit if invalid row
        If idestRow < 0 Then Exit Sub
        iDestPosition = (idestRow + 1) * (idestCol + 1)

'---Capture ID, Position of source item
        If PleadingCaptions.Count > 0 Then
            vSourceID = xARLayout(is_SourceRow, is_SourceCol, 1)
            Set m_Caption = PleadingCaptions.Item(vSourceID)
            If Not m_Caption Is Nothing Then
                iSourcePosition = m_Caption.Position
            Else
                Exit Sub
            End If
        End If

'---empty out source item in grid
        For i = 0 To xARLayout.UpperBound(3)
            xARLayout(is_SourceRow, is_SourceCol, i) = Empty
        Next i
        grdLayout.Rebind

'---create row space in array if necessary
        If idestRow > xARLayout.UpperBound(1) Then
            xARLayout.Insert 1, xARLayout.UpperBound(1) + idestRow
        End If

        m_Document.StatusBar = "Moving..."

'---move down only within dropped columns
        ReDim temp(0, 0, xARLayout.UpperBound(3)) As String

        For i = idestRow To idestRow
            For j = 0 To xARLayout.UpperBound(3)
                temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                xDragFrom(0, 0, j) = temp(0, 0, j)
            Next j
        Next i

        If temp(0, 0, 0) <> Empty Then
            For i = idestRow + 1 To xARLayout.UpperBound(1)
                For j = 0 To xARLayout.UpperBound(3)
                    temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                    xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                    xDragFrom(0, 0, j) = temp(0, 0, j)
                Next j
            Next i
        End If
        
        grdLayout.Bookmark = Null
        grdLayout.Rebind

        grdLayout.MarqueeStyle = dbgHighlightCell
        grdLayout.Bookmark = idestRow
        grdLayout.Col = idestCol

        m_Document.StatusBar = "Caption moved"

'---refresh position props
    RefreshCaptionPositions

'---refresh Captions List
    RefreshList
    
    End If
    grdLayout.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err, "Error completing drag and drop operation."
    Exit Sub
End Sub

Private Sub grdLayout_DragOver(Source As Control, x As Single, y As Single, State As Integer)
' DragOver provides  visual feedback

    Dim dragFrom As String
    Dim overCol As Integer
    Dim overRow As Long

    On Error GoTo ProcError
    overCol = grdLayout.ColContaining(x)
    overRow = grdLayout.RowContaining(y)
    
    If overCol >= 0 Then grdLayout.Col = overCol
'    If overRow >= 0 Then grdLayout.Bookmark = overRow
    If overRow >= 0 Then grdLayout.Row = overRow
    grdLayout.MarqueeStyle = dbgHighlightCell

    Exit Sub
ProcError:
    g_oError.Show Err, "Error on drag over."
    Exit Sub
End Sub

Private Sub grdLayout_GotFocus()
    OnControlGotFocus grdLayout
    GridTabOut Me.grdLayout, True, 1
End Sub

Private Sub grdLayout_LostFocus()
    grdLayout.Columns(0).BackColor = grdLayout.Columns(1).BackColor
End Sub

Private Sub grdLayout_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'---sets bookmark and col
    Dim overCol As Integer
    Dim overRow As Integer
    Dim iPos As Integer
    Dim xKey As String

    On Error GoTo ProcError
    overCol = grdLayout.ColContaining(x)
    overRow = grdLayout.RowContaining(y)
    If overCol >= 0 Then grdLayout.Col = overCol
    If overRow >= 0 Then grdLayout.Bookmark = overRow

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstCaptions_GotFocus()
    OnControlGotFocus lstCaptions
End Sub

Private Sub lstCaptions_RowChange()
    On Error GoTo ProcError
    If PleadingCaptions.Count Then
        If IsNull(lstCaptions.Bookmark) Then
            lstCaptions.Bookmark = 1
        End If
        ChangeActiveCaption
        Me.grdCaseNumbers.EditActive = True
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oCaps_ItemAdded(iIndex As Integer)
'---load layout grid with captions
   ' RefreshList
    'RefreshLayoutGrid
End Sub

Private Sub m_oCaps_ItemRemoved(iIndex As Integer)
    RefreshList
'---load layout grid with captions
    RefreshLayoutGrid
End Sub

Private Sub mlcRelatedActions_BeforeItemAdded(xItem As String)
    mlcRelatedActions.Text = ""
End Sub

Private Sub mlcRelatedActions_Change()
    Me.Changed = True
End Sub

Private Sub mlcRelatedActions_GotFocus()
    OnControlGotFocus Me.mlcRelatedActions
End Sub

Private Sub mnuDateTime_ClearAll_Click()
'---9.7.1 - 4182
    On Error GoTo ProcError
    With Me.grdDateTime
        .Array.ReDim 0, -1, 0, 1
        .Rebind
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuDateTime_Delete_Click()
'---9.7.1 - 4182
    On Error GoTo ProcError
    Me.grdDateTime.Delete
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuDateTime_Insert_Click()
'---9.7.1 - 4182
    On Error Resume Next
    With Me.grdDateTime
        .Update
        .Array.Insert 1, .Bookmark
        .Rebind
    End With
End Sub

Private Sub mnuDateTime_Reset_Click()
'---9.7.1 - 4182
    Dim xTemp As String
    Dim xarNew As XArray
    On Error GoTo ProcError
    With Me.PleadingCaption.Definition
        xTemp = .DateTimeInfoString
        Set xarNew = xarStringToxArray(xTemp, 2, mpCapDateTimeSeparator)
    End With
    With Me.grdDateTime
        .Array = xarNew
        .Rebind
        .MoveFirst
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtAdditionalInformation_Change()
    Me.Changed = True
End Sub
Private Sub txtAdditionalInformation_GotFocus()
    OnControlGotFocus txtAdditionalInformation
End Sub

Private Sub txtCaseTitle_Change()
    Me.Changed = True
End Sub

Private Sub txtCaseTitle_GotFocus()
    OnControlGotFocus Me.txtCaseTitle
End Sub

Private Sub txtParty1Name_Change()
    Me.Changed = True
End Sub
Private Sub txtParty1Name_GotFocus()
    OnControlGotFocus txtParty1Name
End Sub
Private Sub txtParty2Name_Change()
    Me.Changed = True
End Sub
Private Sub txtParty3Name_Change()
    Me.Changed = True
End Sub
Private Sub txtParty3Name_GotFocus()
    OnControlGotFocus txtParty3Name
End Sub
Private Sub txtParty2Name_GotFocus()
    OnControlGotFocus txtParty2Name
End Sub
Private Sub cmbParty1Title_GotFocus()
    OnControlGotFocus cmbParty1Title
End Sub
Private Sub cmbParty2Title_GotFocus()
    OnControlGotFocus cmbParty2Title
End Sub
Private Sub cmbParty3Title_GotFocus()
    OnControlGotFocus cmbParty3Title
End Sub
Private Sub Form_Unload(Cancel As Integer)
    If Me.Added And Me.PleadingCaptions.Count > 1 Then
        Me.Changed = False
        DeleteCaption True
    End If
    Set m_Document = Nothing
    Set frmPleadingCaption = Nothing
    Set m_oCap = Nothing
    Set m_oCaps = Nothing
End Sub

'******************************************
'---module level routines
'******************************************
Private Sub ClearInput()
'clears all recipient detail fields
    On Error GoTo ProcError
    Me.txtParty1Name = ""
    Me.txtParty2Name = ""
    Me.txtAdditionalInformation = ""
    Me.cmbCaptionType.Text = ""
    Me.txtParty3Name = ""
    Me.txtCaseTitle = ""
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.ClearInput"
    Exit Sub
End Sub

Private Sub NewCaption()
    Dim lPrevHighPos As Long
    Dim lPrevCount As Long
    
'creates a new, empty Caption
    
'   save Caption if necessary
    On Error GoTo ProcError
    If Me.Changed Then
        PromptToSave
    End If
    
'---clear the load values checkbox
    Me.chkLoadPreviousValues.value = vbUnchecked

'---capture present counsel pos & count
    lPrevCount = Me.PleadingCaptions.Count
    lPrevHighPos = Me.PleadingCaptions.HighPosition

'   add Caption
    Me.PleadingCaption = Me.PleadingCaptions.Add
'---assign temporary type ID -- Necessary for forced add mode if cancel hit
    PleadingCaption.TypeID = Me.PleadingType.DefaultCaptionType

'---For multi-party, insert adds at high position - 1

    If Me.PleadingType.IsMultiParty And lPrevCount > 1 Then
        PleadingCaption.Position = lPrevHighPos
        PleadingCaptions(Me.lstCaptions.Array.value(Me.lstCaptions.Array.UpperBound(1), 1)).Position = lPrevHighPos + 1
    Else
        PleadingCaption.Position = lPrevHighPos + 1
    End If
    
    RefreshList
    RefreshLayoutGrid
    'ClearInput
    
    On Error Resume Next
 '   select last entry in list
    If Me.PleadingType.IsMultiParty And lPrevCount > 1 Then
        Me.lstCaptions.SelectedItem = _
            Me.lstCaptions.Array.UpperBound(1) - 1
    
    Else
        Me.lstCaptions.SelectedItem = Me.lstCaptions.ApproxCount
    End If
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.NewCaption"
    Exit Sub
End Sub

Private Function PromptToSave() As Boolean
    Dim xMsg As String
    Dim iChoice As Integer
    
    On Error GoTo ProcError
    xMsg = "Save current entry?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        UpdateCaption
    Else
        If Me.Added Then
            Me.Added = False
            DeleteCaption True
        End If
    End If
    PromptToSave = (iChoice = vbYes)
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.PromptToSave"
    PromptToSave = (iChoice = vbYes)
    Exit Function
End Function

    
Private Function SaveCaption() As Boolean
'saves the current Caption
    On Error GoTo ProcError
    If Me.cmbCaptionType.Text = "" Then
        MsgBox "Please select a caption type.", vbInformation, App.Title
        Me.cmbCaptionType.SetFocus
        Exit Function
    End If
'   ensure that there is a new Caption object
    If Me.PleadingCaptions.Count = 0 Then
'       add Caption
        Set PleadingCaption = Me.PleadingCaptions.Add()
    End If
    UpdateCaption
    Me.chkLoadPreviousValues.value = vbUnchecked
    SaveCaption = True
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.SaveCaption"
    Exit Function
End Function

Private Sub RefreshList()
    Dim iNum As Integer
    
    With Me.lstCaptions
        .Array = PleadingCaptions.ListArray(True)
        .Rebind
        
        iNum = Me.lstCaptions.Array.Count(1)

        On Error GoTo ProcError
        
        '---9.7.1 - 2546
        If iNum > 1 Then
            Me.lblCaptions.Caption = _
                " &Cross-Actions (" & iNum & "):"
        ElseIf iNum = 1 And Me.txtParty1Name <> Empty Then
            Me.lblCaptions.Caption = " &Cross-Actions (1):"
        Else
            Me.lblCaptions.Caption = " &Cross-Actions:"
        End If
    End With

Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub UpdateCaption()
'updates the current Caption object
'with dialog field values
    Dim xP1N As String
    Dim xP1T As String
    
    '---Lieff
    Dim xARDateTime As XArray
    '---End Lieff

    On Error GoTo ProcError
    With PleadingCaption
        .DynamicUpdating = True
        If Me.cmbParty1Title.Enabled = False Then
            If txtParty1Name <> "" Then
                xP1N = " - " & txtParty1Name
            Else
                xP1N = txtParty1Name
            End If
        Else
            If txtParty1Name <> "" Then
                xP1N = " - " & txtParty1Name
            Else
                xP1N = txtParty1Name
            End If
        
            If cmbParty1Title <> "" Then
                xP1T = " - " & cmbParty1Title.Text
            Else
                xP1T = ""
            End If
        End If
        .DisplayName = Me.cmbCaptionType.Text & xP1T & xP1N
        .Party1Name = Me.txtParty1Name
        .Party2Name = Me.txtParty2Name
        .Party3Name = Me.txtParty3Name
        .AdditionalInfo = Me.txtAdditionalInformation
        .Party1Title = Me.cmbParty1Title.Text
        .Party2Title = Me.cmbParty2Title.Text
        .Party3Title = Me.cmbParty3Title.Text
        .TypeID = Me.cmbCaptionType.BoundText
        .DateTimeInfo = xDateTimeString
        .CaseTitle = Me.txtCaseTitle
        .PresidingJudge = Me.txtPresidingJudge
        
        '---9.7.1 - 4080
        .RelatedActionText = Me.mlcRelatedActions
        
        On Error Resume Next
        grdCaseNumbers.Update
        .CaseNumber1 = grdCaseNumbers.Array.value(0, 1)
        .CaseNumber2 = grdCaseNumbers.Array.value(1, 1)
        .CaseNumber3 = grdCaseNumbers.Array.value(2, 1)
        .CaseNumber4 = grdCaseNumbers.Array.value(3, 1)
        .CaseNumber5 = grdCaseNumbers.Array.value(4, 1)
        .CaseNumber6 = grdCaseNumbers.Array.value(5, 1)
        On Error GoTo ProcError
        
       
        .DynamicUpdating = False
    End With
    Me.Changed = False
    Me.Added = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.UpdateCaption"
    Exit Sub
End Sub
'---9.6.2 - new argument
Private Sub ShowCaption(Optional oNewCap As CPleadingCaption, _
                    Optional bSuppressTypeChange As Boolean = False)
    '---Lieff
    Dim sHeight As Single
    Dim sgH As Single
    Dim xLC As String
    Dim bReload As Boolean
    Dim xTemp As String
    Dim xARDateTime As XArray
    '---9.6.2
    Dim oCap As CPleadingCaption
    
    '---End Lieff
    
    bReload = True
    
    '---9.6.2
    If oNewCap Is Nothing Then
        Set oCap = PleadingCaption
    Else
        Set oCap = oNewCap
    End If
    
    On Error GoTo ProcError
    With oCap
        '---Lieff
        '---set case numbers grid & label
        '---control height is adjusted here so 2 case nos display 2 lines each, 3 displays one line each
        '---adjust case 2 if you want 2 line display for 3 case nos, etc.
        With grdCaseNumbers
            If sHeight = 0 Then
                sHeight = .RowHeight
            End If
            If sgH = 0 Then
                sgH = .Height
            End If
            '---9.6.2
            .Array = CaseNumberArray(bReload, oCap.Definition, oCap)
            .Rebind
            .Col = 1
            .Bookmark = 0
            .EditActive = True
           
            Select Case .Array.UpperBound(1)
                Case 0, -1
                    .Height = sgH
                    xLC = "Case &Number:"
                    .RowHeight = .Height - 15
                Case 1
                    .Height = 890
                    xLC = "Case &Numbers:"
                    .RowHeight = (.Height / 2) - 15
                Case 2
                    .Height = 890
                    xLC = "Case &Numbers:"
                    .RowHeight = (.Height / 3) - 15
                Case Else
                    .Height = sgH
                    xLC = "Case &Numbers:"
                    .RowHeight = sHeight
            End Select
            lblCaseNumber.Caption = xLC
        End With

        
'---If the .datetimeinfo is empty we're gonna reload the grid with defaults
'---you may not want to do this
'        xTemp = xSubstitute(.DateTimeInfo, vbLf, "")
'        xTemp = xSubstitute(.DateTimeInfo, vbTab, "")
'        xTemp = xSubstitute(.DateTimeInfo, .Definition.DateTimeInfoSeparator & "|", "|")
        xTemp = xSubstitute(.DateTimeInfo, vbLf, "")
        xTemp = xSubstitute(xTemp, vbTab, "")
        xTemp = xSubstitute(xTemp, .Definition.DateTimeInfoSeparator & "|", "|")
        xTemp = xSubstitute(xTemp, Chr(11), "|")
        If xTemp <> "" Then
            Set xARDateTime = xarStringToxArray(xTemp, 2, mpCapDateTimeSeparator)
            Me.grdDateTime.Array = xARDateTime
        Else
'---initialize date/time grid
            With .Definition
                Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                With grdDateTime
                    .Array = xARDateTime
                End With
            End With
        End If
        Me.grdDateTime.Rebind
        Me.grdDateTime.MoveFirst
        
        '---End Lieff
        Me.txtAdditionalInformation = .AdditionalInfo
        Me.txtPresidingJudge = .PresidingJudge
        Me.txtCaseTitle = .CaseTitle
        Me.txtParty1Name = .Party1Name
        Me.txtParty2Name = .Party2Name
        Me.txtParty3Name = .Party3Name
        Me.cmbParty1Title.Text = .Party1Title
        Me.cmbParty2Title.Text = .Party2Title
        Me.cmbParty3Title.Text = .Party3Title
        
        'GLOG : 5467
        DoEvents ' Slow down process to ensure proper type is displayed
        If .TypeID > 0 And Not bSuppressTypeChange Then Me.cmbCaptionType.BoundText = .TypeID
        '---9.7.1
        Me.mlcRelatedActions.Text = .RelatedActionText
        
    End With
    DoEvents

'---9.7.1 - 2545 rem here - conflicts with desired code in RefreshList
'''    If Me.lstCaptions.SelectedItem = 1 Then
'''        Me.lblCaptions.Caption = "&Caption:"
'''    Else
'''        Me.lblCaptions.Caption = "&Cross-Action:"
'''    End If
    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.ShowCaption"
    Exit Sub
End Sub

Private Sub ChangeActiveCaption()
    Dim lBM As Long

'   prompt to save current Caption if necessary
    On Error GoTo ProcError
    DoEvents
    If Me.Changed Then
        PromptToSave
    End If
    
    lBM = Me.lstCaptions.Bookmark
    
    If lBM < 0 Then
        lBM = Min(lstCaptions.Array.UpperBound(1), Me.PleadingCaptions.Count)
    End If
    On Error Resume Next
    PleadingCaption = Me.PleadingCaptions(lstCaptions.Array.value(lBM, 1))
    If Err = 0 Then
        ShowCaption
    End If
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmPleadingCaption.ChangeActiveCaption"
    Exit Sub
End Sub

Private Sub DeleteCaption(Optional bSuppressMessage As Boolean = False)
    Dim i As Integer
    Dim iNum As Integer
    Dim mbr As VbMsgBoxResult
    
    On Error GoTo ProcError
    i = Me.lstCaptions.Row
    If i = 0 Then
        MsgBox "You cannot delete the main caption of a pleading.  Please select another caption.", vbInformation, App.Title
        Exit Sub
    End If
    
    If bSuppressMessage = False Then
        mbr = MsgBox("Are you sure you want to delete this cross-action?", vbYesNo, "Pleading Caption")
        If mbr <> vbYes Then
            If Me.Added Then
                Me.Added = True
            End If
            Exit Sub
        Else
            Me.Added = False
        End If
    End If
    
    If Me.lstCaptions.Row > -1 Then
        PleadingCaptions.Remove (PleadingCaption.Key)
        DoEvents
        ClearInput
        If m_oCaps.Count Then
            Me.Changed = False
            Me.lstCaptions.Row = Min(CDbl(i), m_oCaps.Count - 1)
        End If
    End If
    
    If bSuppressMessage = False Then
        MsgBox "Caption deleted.  Please check layout grid for position of remaining captions.", vbInformation, App.Title
    End If
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.DeleteCaption"
End Sub

Private Sub SetInterfaceControls()
    Dim oDef As CPleadingCaptionDef
    Dim xARDateTime As XArray
    Dim sHeight As Single
    Dim sgH As Single
    Dim xLC As String
    
    On Error GoTo ProcError
    Set oDef = g_oMPO.db.PleadingCaptionDefs.Item(cmbCaptionType.BoundText)
    If Me.PleadingCaption.Party1Title <> "" Then
        Me.cmbParty1Title = Me.PleadingCaption.Party1Title
    Else
        With oDef
            Me.cmbParty1Title = .Party1TitleDefault
        End With
    End If
    
    If Me.PleadingCaption.Party2Title <> "" Then
        Me.cmbParty2Title = Me.PleadingCaption.Party2Title
    Else
        With oDef
            Me.cmbParty2Title = .Party2TitleDefault
        End With
    End If
    
    If Me.PleadingCaption.Party3Title <> "" Then
        Me.cmbParty3Title = Me.PleadingCaption.Party3Title
    Else
        With oDef
            Me.cmbParty3Title = .Party3TitleDefault
        End With
    End If
    
    With oDef
        If .Party1Label <> "" Then
            Me.lblParty1Name.Caption = .Party1Label
        Else
            Me.lblParty1Name.Caption = Me.lblParty1Name.Tag
        End If
        If .Party2Label <> "" Then
            Me.lblParty2Name.Caption = .Party2Label
        Else
            Me.lblParty2Name.Caption = Me.lblParty2Name.Tag
        End If
        If .Party3Label <> "" Then
            Me.lblParty3Name.Caption = .Party3Label
        Else
            Me.lblParty3Name.Caption = Me.lblParty3Name.Tag
        End If
        '---9.7.1 - 4080
        If mlcRelatedActions.Visible = True Then
            Me.lblParty1Name.Caption = "&Related Action Text:"
            Me.lblParty1Name.Enabled = True
        End If
    
    End With
    
    
    '---End Lieff
    '---set case numbers grid & label
    '---control height is adjusted here so 2 case nos display 2 lines each, 3 displays one line each
    '---adjust case 2 if you want 2 line display for 3 case nos, etc.
    With grdCaseNumbers
        If sHeight = 0 Then
            sHeight = .RowHeight
        End If
        If sgH = 0 Then
            sgH = .Height
        End If
        
        If Me.Initializing Then
            .Array = CaseNumberArray(True)
        Else
            .Array = CaseNumberArray(True, oDef)
        End If
        
        .Rebind
        .Col = 1
        .Bookmark = 0
        .EditActive = True
        
        Select Case .Array.UpperBound(1)
            Case 0, -1
                .Height = sgH
                xLC = "Case &Number:"
                .RowHeight = .Height - 15
            Case 1
                .Height = 890
                xLC = "Case &Numbers:"
                .RowHeight = (.Height / 2) - 15
            Case 2
                .Height = 890
                xLC = "Case &Numbers:"
                .RowHeight = (.Height / 3) - 15
            Case Else
                .Height = sgH
                xLC = "Case &Numbers:"
                .RowHeight = sHeight
        End Select
        lblCaseNumber.Caption = xLC
    End With
        
    If Not Me.Initializing Then

'---If the .datetimeinfo is empty we're gonna reload the grid with defaults
'---you may not want to do this
        Dim xTemp As String
'        xTemp = xSubstitute(PleadingCaption.DateTimeInfo, vbLf, "")
'        xTemp = xSubstitute(PleadingCaption.DateTimeInfo, vbTab, "")
'        xTemp = xSubstitute(PleadingCaption.DateTimeInfo, PleadingCaption.Definition.DateTimeInfoSeparator & "|", "|")
        
        xTemp = xSubstitute(PleadingCaption.DateTimeInfo, vbLf, "")
        xTemp = xSubstitute(xTemp, vbTab, "")
        xTemp = xSubstitute(xTemp, PleadingCaption.Definition.DateTimeInfoSeparator & "|", "|")
        xTemp = xSubstitute(xTemp, Chr(11), "|")
        
        If xTemp <> "" Then
            Set xARDateTime = xarStringToxArray(xTemp, 2, mpCapDateTimeSeparator)
            Me.grdDateTime.Array = xARDateTime
        Else
'---initialize date/time grid
            With g_oMPO.db.PleadingCaptionDefs.Item(Me.cmbCaptionType.BoundText)
                Set xARDateTime = xarStringToxArray(.DateTimeInfoString, 2, mpCapDateTimeSeparator)
                With grdDateTime
                    .Array = xARDateTime
                End With
            End With
        End If
        Me.grdDateTime.Rebind
        Me.grdDateTime.MoveFirst

    End If
    '---End Lieff
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.SetInterfaceControls"
End Sub

Private Sub DisplayControls()
    Dim oDef As CPleadingCaptionDef
    Dim bAllows As Boolean
    
    On Error GoTo ProcError
    Set oDef = g_oMPO.db.PleadingCaptionDefs.Item(cmbCaptionType.BoundText)
    bAllows = oDef.AllowsAdditionalInformation
    Me.txtAdditionalInformation.Enabled = bAllows
    Me.lblAdditionalInformation.Enabled = bAllows
    Me.lblCaseNumber.Enabled = oDef.CaseNumber1Label <> ""
    Me.grdCaseNumbers.Enabled = oDef.CaseNumber1Label <> ""
    Me.lblDocumentTitle.Enabled = oDef.IncludesCaseTitle
    Me.txtCaseTitle.Enabled = oDef.IncludesCaseTitle
    Me.lblDateTimeInfo.Enabled = oDef.IncludesDateTimeInfo
    Me.grdDateTime.Enabled = oDef.IncludesDateTimeInfo
    Me.txtPresidingJudge.Visible = oDef.IncludesPresidingJudge
    Me.lblPresidingJudge.Visible = oDef.IncludesPresidingJudge
    If Me.txtPresidingJudge.Visible = True Then
        Me.lblAdditionalInformation.Visible = False
        Me.txtAdditionalInformation.Visible = False
        Me.lblPresidingJudge.Top = Me.lblAdditionalInformation.Top
        Me.txtPresidingJudge.Top = Me.txtAdditionalInformation.Top
        Me.txtPresidingJudge.Height = Me.txtAdditionalInformation.Height
    Else
        Me.lblAdditionalInformation.Visible = True
        Me.txtAdditionalInformation.Visible = True
    End If
    
    If oDef.PartiesInCaption = 0 Then
        bAllows = oDef.PartiesInCaption = 0
        Me.cmbParty1Title.Enabled = Not bAllows
        Me.lblParty1Name.Enabled = Not bAllows
        Me.txtParty1Name.Enabled = Not bAllows
        Me.cmbParty2Title.Enabled = Not bAllows
        Me.lblParty2Name.Enabled = Not bAllows
        Me.txtParty2Name.Enabled = Not bAllows
        Me.cmbParty3Title.Visible = Not bAllows
        Me.lblParty3Name.Visible = Not bAllows
        Me.txtParty3Name.Visible = Not bAllows
    
        Me.lblVersus.Enabled = Not bAllows
        Me.lblVersus2.Visible = Not bAllows
        Me.lblVersus3.Visible = Not bAllows
    
    ElseIf oDef.PartiesInCaption = 1 Then
        bAllows = (oDef.PartiesInCaption = 1)
        Me.cmbParty1Title.Enabled = bAllows
        Me.lblParty1Name.Enabled = bAllows
        Me.txtParty1Name.Enabled = bAllows
       
        Me.cmbParty2Title.Visible = Not bAllows
        Me.lblParty2Name.Visible = Not bAllows
        Me.txtParty2Name.Visible = Not bAllows
        Me.cmbParty3Title.Visible = Not bAllows
        Me.lblParty3Name.Visible = Not bAllows
        Me.txtParty3Name.Visible = Not bAllows
        Me.lblVersus.Visible = Not bAllows
        Me.lblVersus2.Visible = Not bAllows
        Me.lblVersus3.Visible = Not bAllows
           
        Me.cmbParty1Title.Enabled = oDef.Party1TitleDefault <> "None"
    
    ElseIf oDef.PartiesInCaption = 2 Then
    
        bAllows = (oDef.PartiesInCaption = 2)
        
        Me.cmbParty1Title.Enabled = bAllows
        Me.lblParty1Name.Enabled = bAllows
        Me.txtParty1Name.Enabled = bAllows
        Me.cmbParty2Title.Visible = bAllows
        Me.lblParty2Name.Visible = bAllows
        Me.txtParty2Name.Visible = bAllows
        Me.cmbParty2Title.Enabled = bAllows
        Me.lblParty2Name.Enabled = bAllows
        Me.txtParty2Name.Enabled = bAllows
        Me.cmbParty3Title.Visible = Not bAllows
        Me.lblParty3Name.Visible = Not bAllows
        Me.txtParty3Name.Visible = Not bAllows
        Me.lblVersus.Visible = bAllows
        Me.lblVersus.Enabled = bAllows
        Me.lblVersus2.Visible = Not bAllows
        Me.lblVersus3.Visible = Not bAllows
        Me.cmbParty1Title.Enabled = oDef.Party1TitleDefault <> "None"
        Me.cmbParty2Title.Enabled = oDef.Party2TitleDefault <> "None"
    
    Else
        
        bAllows = (oDef.PartiesInCaption = 3)
        
        Me.cmbParty1Title.Enabled = bAllows
        Me.lblParty1Name.Enabled = bAllows
        Me.txtParty1Name.Enabled = bAllows
        Me.cmbParty2Title.Enabled = bAllows
        Me.lblParty2Name.Enabled = bAllows
        Me.txtParty2Name.Enabled = bAllows
        Me.cmbParty2Title.Visible = bAllows
        Me.lblParty2Name.Visible = bAllows
        Me.txtParty2Name.Visible = bAllows
        
        Me.cmbParty3Title.Visible = bAllows
        Me.lblParty3Name.Visible = bAllows
        Me.txtParty3Name.Visible = bAllows
        
        Me.lblVersus.Visible = Not bAllows
        Me.lblVersus2.Visible = bAllows
        Me.lblVersus3.Visible = bAllows
        Me.lblVersus2.Enabled = bAllows
        Me.lblVersus3.Enabled = bAllows
        Me.cmbParty1Title.Enabled = oDef.Party1TitleDefault <> "None"
        Me.cmbParty2Title.Enabled = oDef.Party2TitleDefault <> "None"
        Me.cmbParty3Title.Enabled = oDef.Party3TitleDefault <> "None"
        
    End If
        
    '---9.7.1 - 4080
    Me.mlcRelatedActions.Visible = oDef.AllowRelatedActionInput
   
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.DisplayControls"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF5 And btnNew.Enabled Then
        btnNew_Click
    ElseIf KeyCode = vbKeyF6 And btnSave.Enabled Then
        btnSave_Click
    ElseIf KeyCode = vbKeyF7 And btnDelete.Enabled Then
        btnDelete_Click
    ElseIf Shift = 4 And KeyCode = 67 Then  '---alt/C force focus to list
        If Me.lstCaptions.Enabled = False Then
           KeyCode = Empty
           Shift = Empty
        End If
    Else
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Function CaseNumberArray(Optional bLoadValues As Boolean = False, _
                                                        Optional oDef As mpDB.CPleadingCaptionDef, _
                                                        Optional oCap As CPleadingCaption) As XArray
    Dim xarTemp As XArray
    Dim i As Integer
    Dim opDef As CPleadingCaptionDef
    Dim opCap As CPleadingCaption
    
    On Error GoTo ProcError
    Set xarTemp = g_oMPO.NewXArray
    xarTemp.ReDim 0, -1, 0, 1
    
    If Not oDef Is Nothing Then
        Set opDef = oDef
    Else
        Set opDef = Me.PleadingCaption.Definition
    End If
        
    If Not oCap Is Nothing Then
        Set opCap = oCap
    Else
        Set opCap = Me.PleadingCaption
    End If
    
    With opDef
            If .CaseNumber1Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber1Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = opCap.CaseNumber1
                End If
            End If
            If .CaseNumber2Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber2Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = opCap.CaseNumber2
                End If
            End If
            If .CaseNumber3Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber3Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = opCap.CaseNumber3
                End If
            End If
            If .CaseNumber4Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber4Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = opCap.CaseNumber4
                End If
            End If
            If .CaseNumber5Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber5Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = opCap.CaseNumber5
                End If
            End If
            If .CaseNumber6Label <> "" Then
                xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
                xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber6Label
                If bLoadValues Then
                    xarTemp(xarTemp.UpperBound(1), 1) = opCap.CaseNumber6
                End If
            End If
    
    End With
    Set CaseNumberArray = xarTemp
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.CaseNumberArray"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function


Private Sub RefreshLayoutGrid()
    Dim m_Cap As MPO.CPleadingCaption
    Dim xDisplayName As String
    Dim xKey As String
    Dim iPos As Integer
    Dim iRow As Integer
    Dim iCol As Integer
    Dim iCount As Integer
    Dim iSigLimit As Integer
    Dim xARLayout As XArray
    
'---fills grid and listbox with displayname/selection key arrays
'---load layout grid & sig list with Counsels from signature collection
   On Error Resume Next
    
    iSigLimit = Max(Val(GetMacPacIni("Pleading", "MaxCaptions")), CDbl(mpPleadingCaptionsLimit))
    iSigLimit = Max(CDbl(iSigLimit), Me.PleadingCaptions.HighPosition)
    On Error GoTo ProcError
    Set xARLayout = g_oMPO.NewXArray
    xARLayout.ReDim 0, iSigLimit, 0, 1, 0, 1
    
    xARLayout.value(0, 0, 0) = ""
    xARLayout.value(0, 0, 1) = ""
    iCount = 0
    
    For Each m_Cap In Me.PleadingCaptions
        iPos = m_Cap.Position

        If m_Cap.DisplayName = "" Then
            m_Cap.DisplayName = "No Display Name"
        End If

        xKey = m_Cap.Key
        iRow = iPos - 1
        iCol = 0
     
'---update grid array
        xARLayout.value(iRow, iCol, 0) = m_Cap.DisplayName
        xARLayout.value(iRow, iCol, 1) = xKey
    
    Next m_Cap

    With Me.grdLayout
        .Array = xARLayout
        .Rebind
        .Bookmark = Null
        .Col = 1
    End With
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCaption.RefreshLayoutGrid"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


Public Sub RefreshCaptionPositions()
    Dim oCouns As MPO.CPleadingCaption
    Dim xarTemp As XArray
    Dim i, j As Integer
'---If DD performed, reassign sig.position based on grid position

    On Error GoTo ProcError
    Set xarTemp = Me.grdLayout.Array

    With xarTemp
        For i = .LowerBound(1) To .UpperBound(1)
            For j = .LowerBound(2) To .UpperBound(2)
                On Error Resume Next
                If .value(i, j, 1) <> "" Then
                    Set oCouns = Me.PleadingCaptions(.value(i, j, 1))
                    If Not oCouns Is Nothing Then
                        oCouns.Position = i + 1
                        Set oCouns = Nothing
                    End If
                End If
            Next j
        Next i
    End With
    Set xarTemp = Nothing
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCaption.RefreshCaptionPositions"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Function xDateTimeString() As String
    Dim xTemp As String
    Dim i As Integer
    Dim xSep As String
    Dim xLabelSep As String
    On Error GoTo ProcError
    xSep = mpCapDateTimeSeparator
    xLabelSep = Me.PleadingCaption.Definition.DateTimeInfoSeparator
    If Not bIncludeEmptyDateTime Then
        With grdDateTime.Array
            For i = 0 To .UpperBound(1)
                '  Only include if value is entered or if entire row is blank
                '  (meaning an extra line should be included before next entry
                ' in caption)
                If .value(i, 0) & .value(i, 1) = "" Then
                    '---handle spacer rows inside array of labels with no values
                    '---9.4
                    If xTemp <> "" Then
                        xTemp = xTemp & xSep _
                            & vbLf & xSep
                    End If
                ElseIf .value(i, 1) <> "" Then
                    xTemp = xTemp & .value(i, 0) & xLabelSep & xSep & _
                        xTrimTrailingChrs(.value(i, 1), Chr(10)) & vbLf & xSep
                End If
            Next i
            xDateTimeString = xTrimTrailingChrs(xTemp, vbLf & xSep)
        End With
    Else
'        xDateTimeString = xTrimTrailingChrs(XArrayToString(Me.grdDateTime.Array, xLabelSep, , True, Chr(11)), vbLf)
        xDateTimeString = xTrimTrailingChrs(XArrayToString(Me.grdDateTime.Array, xLabelSep & xSep, , True, vbLf & xSep), vbLf) & xSep
    End If
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.xDateTimeString"), _
              g_oError.Desc(Err.Number)
    Exit Function

End Function

Private Function bIncludeEmptyDateTime() As Boolean
    On Error GoTo ProcError
    bIncludeEmptyDateTime = mpbase2.bIniToBoolean(GetMacPacIni("Pleading", "IncludeEmptyDateTime"))
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.bIncludeEmptyDateTime"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Function GridMoveNext(grdCur As TDBGrid, Optional bNextRecord As Boolean = False)
    ' Move to next column or row
    ' Redim array and add blank row if necessary
    On Error Resume Next
    With grdCur
        If .Bookmark < grdCur.Array.UpperBound(1) Then
            '---
            .MoveNext
        ElseIf Not GridCurrentRowIsEmpty(grdCur) Then
            .Update
            .Array.ReDim 0, .Bookmark + 1, 0, .Columns.Count - 1
            .Rebind
            .MoveLast
            .Col = 0
            .EditActive = True
        End If
    End With
End Function


Private Function GridCurrentRowIsEmpty(grdTest As TDBGrid) As Boolean
    Dim i As Integer
    On Error Resume Next
    GridCurrentRowIsEmpty = True
    With grdTest
        For i = 0 To .Columns.Count - 1
            If .Columns(i).Text <> "" Then
                GridCurrentRowIsEmpty = False
                Exit Function
            End If
        Next i
    End With
End Function

Private Sub txtPresidingJudge_Change()
    Me.Changed = True
End Sub

Private Sub txtPresidingJudge_GotFocus()
    OnControlGotFocus txtPresidingJudge
End Sub

Private Sub vsIndexTab1_Click()
    If Me.Initializing Then Exit Sub

    On Error GoTo ProcError
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            If btnNew.Enabled Then
                Me.btnNew.SetFocus
            Else
                Me.btnSave.SetFocus
            End If
        Case 1
            If Me.grdCaseNumbers.Enabled = True Then
                Me.grdCaseNumbers.SetFocus
            ElseIf Me.txtCaseTitle.Enabled = True Then
                Me.txtCaseTitle.SetFocus
            ElseIf Me.grdDateTime.Enabled = True Then
                Me.grdDateTime.SetFocus
            ElseIf Me.txtAdditionalInformation.Enabled = True Then
                Me.txtAdditionalInformation.SetFocus
            End If
        Case 2
            '---9.6.2 - prompt or save before allowing trip to layout grid tab
            If Me.Added Or Me.Changed Then
                MsgBox "You must first save your new or changed caption", vbInformation, App.Title
                Me.vsIndexTab1.CurrTab = 0
                Me.btnSave.SetFocus
                Exit Sub
            Else
                Me.grdLayout.SetFocus
            End If
    End Select
    Exit Sub
ProcError:
End Sub

Private Sub ClearGrid(oGrd As TDBGrid)
    Dim xarTemp As XArray
    Set xarTemp = oGrd.Array
    On Error Resume Next
    xarTemp.ReDim -1, -1, -1, -1
    oGrd.Array = xarTemp
    oGrd.Rebind

End Sub





