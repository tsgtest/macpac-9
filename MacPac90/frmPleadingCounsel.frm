VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmPleadingCounsel 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Pleading Counsel/Co-Counsel"
   ClientHeight    =   6444
   ClientLeft      =   1920
   ClientTop       =   20328
   ClientWidth     =   5472
   Icon            =   "frmPleadingCounsel.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6444
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   3240
      Picture         =   "frmPleadingCounsel.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   29
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   5985
      Width           =   650
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4905
      Picture         =   "frmPleadingCounsel.frx":0CAC
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Delete Signature"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4440
      Picture         =   "frmPleadingCounsel.frx":0E36
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Save new signature"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnNew 
      Height          =   400
      Left            =   3975
      Picture         =   "frmPleadingCounsel.frx":0FB4
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Add a New Counsel (F5)"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4725
      TabIndex        =   31
      Top             =   5985
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3990
      TabIndex        =   30
      Top             =   5985
      Width           =   650
   End
   Begin TrueDBList60.TDBList lstCounsels 
      Height          =   1188
      Left            =   1752
      OleObjectBlob   =   "frmPleadingCounsel.frx":14F6
      TabIndex        =   4
      Top             =   540
      Width           =   3576
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   4185
      Left            =   -15
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   2205
      Width           =   5925
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|A&ddress Info|&Layout"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   400
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   3768
         Left            =   6660
         TabIndex        =   34
         Top             =   15
         Width           =   5904
         Begin TrueDBGrid60.TDBGrid grdLayout 
            DragIcon        =   "frmPleadingCounsel.frx":3439
            Height          =   1950
            Left            =   1765
            OleObjectBlob   =   "frmPleadingCounsel.frx":358B
            TabIndex        =   27
            Top             =   135
            Width           =   3555
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   0
            X1              =   -192
            X2              =   5448
            Y1              =   3720
            Y2              =   3720
         End
         Begin VB.Label lblLayout 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Counsels La&yout:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   30
            TabIndex        =   26
            Top             =   135
            Width           =   1455
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   4250
            Left            =   0
            Top             =   -500
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   3768
         Left            =   6420
         TabIndex        =   32
         Top             =   15
         Width           =   5904
         Begin VB.TextBox txtFirmSlogan 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            MultiLine       =   -1  'True
            TabIndex        =   17
            Top             =   585
            Width           =   3570
         End
         Begin VB.TextBox txtEMail 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            MultiLine       =   -1  'True
            TabIndex        =   25
            Top             =   3255
            Width           =   3570
         End
         Begin VB.TextBox txtFirmFax 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            MultiLine       =   -1  'True
            TabIndex        =   23
            Top             =   2730
            Width           =   3570
         End
         Begin VB.TextBox txtFirmName 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   1740
            MultiLine       =   -1  'True
            TabIndex        =   15
            Top             =   135
            Width           =   3570
         End
         Begin VB.TextBox txtFirmAddress 
            Appearance      =   0  'Flat
            Height          =   900
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   19
            Top             =   1065
            Width           =   3570
         End
         Begin VB.TextBox txtFirmPhone 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1740
            MultiLine       =   -1  'True
            TabIndex        =   21
            Top             =   2190
            Width           =   3570
         End
         Begin VB.Label lblFirmSlogan 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm Slo&gan:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   0
            TabIndex        =   16
            Top             =   615
            Width           =   1455
         End
         Begin VB.Label lblEMail 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm &Email:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   24
            Top             =   3225
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   1
            X1              =   -72
            X2              =   5598
            Y1              =   3720
            Y2              =   3720
         End
         Begin VB.Label lblFirmFax 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm Fa&x:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   22
            Top             =   2700
            Width           =   1455
         End
         Begin VB.Label lblFirmName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm &Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   14
            Top             =   150
            Width           =   1455
         End
         Begin VB.Label lblFirmAddress 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm &Address:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   18
            Top             =   1065
            Width           =   1455
         End
         Begin VB.Label lblFirmPhone 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm P&hone:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   20
            Top             =   2160
            Width           =   1455
         End
         Begin VB.Shape Shape4 
            BorderStyle     =   0  'Transparent
            Height          =   6015
            Left            =   30
            Top             =   -135
            Visible         =   0   'False
            Width           =   5895
         End
         Begin VB.Shape Shape5 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   3860
            Left            =   0
            Top             =   -120
            Width           =   1695
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   3768
         Left            =   12
         TabIndex        =   33
         Top             =   15
         Width           =   5904
         Begin VB.CheckBox chkCreateSignature 
            Appearance      =   0  'Flat
            Caption         =   "Create &Signature for this Co-Counsel"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1890
            TabIndex        =   13
            Top             =   3324
            Width           =   3240
         End
         Begin mpControls.SigningAttorneysGrid saGrid 
            Height          =   1536
            Left            =   1740
            TabIndex        =   8
            Tag             =   "975"
            Top             =   552
            Width           =   3588
            _ExtentX        =   6329
            _ExtentY        =   3852
            BackColor       =   -2147483643
            RecordSelectors =   0   'False
            Separator       =   "; "
            BarIDSuffix     =   ""
            BarIDPrefix     =   ", Bar No. "
            SignerEMailPrefix=   ", "
            SeparatorSpecial=   2
            FontName        =   "Arial"
            FontBold        =   0   'False
            FontSize        =   8.4
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.TextBox txtClientName 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   510
            Left            =   1752
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   12
            Top             =   2580
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbAttorneysFor 
            Height          =   300
            Left            =   1740
            OleObjectBlob   =   "frmPleadingCounsel.frx":5F9A
            TabIndex        =   10
            Top             =   2268
            Width           =   3588
         End
         Begin TrueDBList60.TDBCombo cmbCounselType 
            Height          =   390
            Left            =   1725
            OleObjectBlob   =   "frmPleadingCounsel.frx":81BD
            TabIndex        =   6
            Top             =   30
            Visible         =   0   'False
            Width           =   3630
         End
         Begin VB.Label lblCounselType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   120
            TabIndex        =   5
            Top             =   60
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblAttorneysFor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   " Att&orneys For:"
            ForeColor       =   &H00FFFFFF&
            Height          =   228
            Left            =   168
            TabIndex        =   9
            Top             =   2292
            Width           =   1416
         End
         Begin VB.Label lblClientName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Client &Name(s):"
            ForeColor       =   &H00FFFFFF&
            Height          =   288
            Left            =   120
            TabIndex        =   11
            Top             =   2580
            Width           =   1452
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   3
            X1              =   -216
            X2              =   5454
            Y1              =   3720
            Y2              =   3720
         End
         Begin VB.Label lblAttorneys 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Attorneys:"
            ForeColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   120
            TabIndex        =   7
            Top             =   555
            Width           =   1455
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   3765
            Left            =   0
            Top             =   -30
            Width           =   1695
         End
         Begin VB.Shape Shape3 
            BorderStyle     =   0  'Transparent
            Height          =   6075
            Left            =   1770
            Top             =   -135
            Width           =   5805
         End
      End
   End
   Begin TrueDBList60.TDBCombo cmbNoticeAttorney 
      Height          =   588
      Left            =   1740
      OleObjectBlob   =   "frmPleadingCounsel.frx":A3F7
      TabIndex        =   35
      Top             =   1800
      Width           =   3588
   End
   Begin VB.Label lblNoticeAttorney 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Not&ice Attorney:"
      ForeColor       =   &H00FFFFFF&
      Height          =   312
      Left            =   120
      TabIndex        =   36
      Top             =   1824
      Width           =   1452
   End
   Begin VB.Label lblCounsels 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Counsel:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   960
      TabIndex        =   3
      Tag             =   "&Signature Blocks:"
      Top             =   540
      Width           =   630
   End
   Begin VB.Shape Shape6 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00800080&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00800080&
      Height          =   5940
      Left            =   0
      Top             =   0
      Width           =   1695
   End
   Begin VB.Menu mnuLayoutGrid 
      Caption         =   "Layout Grid"
      Visible         =   0   'False
      Begin VB.Menu mnuLayoutGridAdd 
         Caption         =   "&Add Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridChangeDisplayName 
         Caption         =   "&Change Display Name..."
      End
      Begin VB.Menu mnuLayoutGridCopy 
         Caption         =   "Cop&y Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridDelete1 
         Caption         =   "&Delete Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridDeleteAll 
         Caption         =   "Delete All &Signatures"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridPaste 
         Caption         =   "&Paste Signature"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
End
Attribute VB_Name = "frmPleadingCounsel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const mpDefaultDateFormat As String = "MMMM d, yyyy"
Const mpCancelAdd As String = "Cancel unsaved counsel (F7)"
Const mpDeleteSig As String = "Delete this counsel (F7)"
Const mpCancelChange As String = "Cancel unsaved changes (F7)"
Const mpSaveChanges As String = "Save changes to this counsel (F6)"
Const mpSaveNewSignature As String = "Save new counsel (F6)"

'---drag and drop support
Private xDragFrom() As String
Private is_SourceRow As Integer
Private is_SourceCol As Integer
Private is_DestRow As Integer
Private is_DestCol As Integer

'---macpac object vars
Private WithEvents m_oCounsel As MPO.CPleadingCounsel
Attribute m_oCounsel.VB_VarHelpID = -1
Private m_Counsels As MPO.CPleadingCounsels
Private m_Document As MPO.CDocument
Private m_oPleadingType As mpDB.CPleadingDef
Private m_oDB As mpDB.CDatabase
Private m_oPSig As MPO.CPleadingSignature
Private m_oPleading As MPO.CPleading
Private m_oParent As Form
Private xARLayout As XArrayObject.XArray
Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private m_bCancelled As Boolean
Private m_bAdded As Boolean
Private m_bChanged As Boolean
Private m_xKey As String
Private m_bSignatureAdded As Boolean
Private m_xFirmAddress As String
'---Lieff
Private m_bFullRowFormat As Boolean
Private m_bFormatChanged As Boolean
'---End Lieff

'---these vars required for change/save validation routines
Private m_xSigners As String

Private bDeleteRun As Boolean
Private xMsg As String
Public Property Get Counsels() As MPO.CPleadingCounsels
    Set Counsels = m_Counsels
End Property
Public Property Let Counsels(oNew As MPO.CPleadingCounsels)
    Set m_Counsels = oNew
End Property
Public Property Get Counsel() As MPO.CPleadingCounsel
    Set Counsel = m_oCounsel
End Property
Public Property Let Counsel(oNew As MPO.CPleadingCounsel)
    Set m_oCounsel = oNew
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property
Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get PleadingType() As mpDB.CPleadingDef
    Set PleadingType = m_oPleadingType
End Property
Public Property Let PleadingType(oNew As mpDB.CPleadingDef)
    Set m_oPleadingType = oNew
End Property
Public Property Get Pleading() As CPleading
    Set Pleading = m_oPleading
End Property
Public Property Let Pleading(oNew As CPleading)
    Set m_oPleading = oNew
End Property
Public Property Get PleadingSignature() As CPleadingSignature
    Set PleadingSignature = m_oPSig
End Property
Public Property Let PleadingSignature(oNew As CPleadingSignature)
    Set m_oPSig = oNew
End Property
Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property
Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property
Public Property Get Added() As Boolean
    Added = m_bAdded
End Property
Public Property Let Added(bNew As Boolean)
    m_bAdded = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    Me.lstCounsels.Enabled = Not bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelAdd
        Me.btnSave.ToolTipText = mpSaveNewSignature
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Changed() As Boolean
    Changed = m_bChanged
End Property
Public Property Let Changed(bNew As Boolean)
    DoEvents
    m_bChanged = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelChange
        Me.btnSave.ToolTipText = mpSaveChanges
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get SignatureAdded() As Boolean
    SignatureAdded = m_bSignatureAdded
End Property
Public Property Let SignatureAdded(bNew As Boolean)
    m_bSignatureAdded = bNew
End Property

'---Lieff
Public Property Get FullRowFormat() As Boolean
    FullRowFormat = m_bFullRowFormat
End Property
Public Property Let FullRowFormat(bNew As Boolean)
    If bNew <> m_bFullRowFormat Then
        Me.FormatChanged = True
    Else
        Me.FormatChanged = False
    End If
    m_bFullRowFormat = bNew
End Property
Public Property Get FormatChanged() As Boolean
    FormatChanged = m_bFormatChanged
End Property
Public Property Let FormatChanged(bNew As Boolean)
    m_bFormatChanged = bNew
End Property
'---End Lieff

Public Property Get Level0() As Long
    On Error Resume Next
    Level0 = m_Document.RetrieveItem("Level0", "Pleading")
End Property
Public Property Get Level1() As Long
    On Error Resume Next
    Level1 = m_Document.RetrieveItem("Level1", "Pleading")
End Property
Public Property Get Level2() As Long
    On Error Resume Next
    Level2 = m_Document.RetrieveItem("Level2", "Pleading")
End Property
Public Property Get Level3() As Long
    On Error Resume Next
    Level3 = m_Document.RetrieveItem("Level3", "Pleading")
End Property
'---9.5.0
Private Sub btnAddressBooks_Click()
    GetContact
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
'            vsIndexTab1.CurrTab = 1
'            Me.txtFirmName.SetFocus
        Case 1
            Me.txtFirmAddress.SetFocus
    End Select
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub
'---end 9.5.0

Private Sub btnCancel_GotFocus()
    SetControlBackColor btnCancel
End Sub

Private Sub btnDelete_Click()
    On Error GoTo ProcError
    If Me.Changed Then
        Me.Changed = False
        ChangeActiveCounsel
    Else
        Me.Changed = False
        DeleteCounsel
        ChangeActiveCounsel
    End If
    DoEvents
    Me.Added = False
    Me.Changed = False
    Me.lstCounsels.Enabled = True
    Me.lstCounsels.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnDelete_GotFocus()
    SetControlBackColor btnDelete
End Sub

Private Sub btnFinish_GotFocus()
    SetControlBackColor btnFinish
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    If Me.Added = True Then
        Me.Changed = False
        DeleteCounsel True
        Me.Added = False
    End If
    Me.Hide
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnNew_Click()
    Dim i As Integer
    On Error GoTo ProcError
    '---9.7.1 - 4260
    If Me.Counsels.Count >= Val(GetMacPacIni("Pleading", "MaxCounsels")) Then
        MsgBox "The maximum number of Co-Counsels has been reached.  Please check with your administrator for more information.", vbInformation, App.Title
        Exit Sub
    End If
    
    DoEvents
    NewCounsel
    '#3681
'    Me.lstCounsels.SelectedItem = Me.lstCounsels.ApproxCount ' - 1 'Me.Counsels.Count - 1
    For i = 1 To 30
        DoEvents
    Next
    Me.Added = True
    Exit Sub
ProcError:
    g_oError.Show Err, "Error adding new counsel."
    Exit Sub
End Sub

Private Sub btnNew_GotFocus()
    SetControlBackColor btnNew
End Sub

Private Sub btnSave_Click()
    Dim iRow As Integer
    On Error Resume Next
    If Not ValidateSigners Then Exit Sub
    
    'iRow = Me.lstCounsels.Row
    iRow = Me.lstCounsels.SelectedItem
    
    On Error GoTo ProcError
    EchoOff
    
    
 '---9.5.0
        If GetMacPacIni("Pleading", "CounselTableThreshold") > 0 Then
            If Me.Counsels.Count >= GetMacPacIni("Pleading", "CounselTableThreshold") Then
                Me.FullRowFormat = False
            Else
                Me.FullRowFormat = g_oMPO.db.PleadingCounselDefs.Item(Me.Counsel.Definition.ID).FullRowFormat
            End If
        Else
            Me.FullRowFormat = g_oMPO.db.PleadingCounselDefs.Item(Me.Counsel.Definition.ID).FullRowFormat
        End If
'---End 9.5.0
        
    If Me.FormatChanged Then
        SetLayoutGridDisplay
        ResetCounselPositions
    End If

    SaveCounsel
    RefreshList
    RefreshLayoutGrid
    lstCounsels.SelectedItem = iRow
    Me.Added = False
    Me.Changed = False
    
    DoEvents
    On Error Resume Next
    Me.lstCounsels.SetFocus
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err, "Error saving new or changed counsel."
    EchoOn
    Exit Sub
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    
    If Not ValidateSigners Then Exit Sub
    
    Me.Cancelled = False
    '---save changes or new counsel if dirty
    If Me.Added Or Me.Changed Then
        If PromptToSave = True Then
            RefreshList
            If Me.vsIndexTab1.CurrTab = 0 Then
                Me.saGrid.SetFocus
            Else
                Me.btnFinish.SetFocus
            End If
            Me.Changed = False
            Me.Added = False
            Exit Sub
        End If
    End If
    DoEvents
    Me.Hide
    Application.ScreenUpdating = False
    
    If ParentForm Is Nothing Then
        If Me.SignatureAdded Then
            Me.Pleading.Signatures.Finish
        End If
        DoEvents
        Counsels.Finish
        'GLOG : 5718 : ceh
        Me.Pleading.UpdateNoticeAttorney
    Else
        If Me.SignatureAdded Then
            Me.Pleading.Signatures.Refresh
        End If
        DoEvents
        '---9.7.1 - 4025
        If Me.Pleading.Definition.AllowCoCounsel = True Then
            Counsels.Refresh
        End If
    End If
    
    
    Application.ScreenUpdating = True
    Me.Added = False
    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Show Err, "Error finishing Counsel."
    Exit Sub
End Sub

Private Sub btnSave_GotFocus()
    SetControlBackColor btnSave
End Sub

Private Sub chkCreateSignature_Click()
    If Me.CascadeUpdates = True Then Exit Sub
    Me.SignatureAdded = (chkCreateSignature = vbChecked)   ' 0 - chkCreateSignature
    Me.Changed = True
End Sub

Private Sub cmbAttorneysFor_ItemChange()
    If Me.CascadeUpdates = True Then Exit Sub
    Me.Changed = True
End Sub

Private Sub cmbAttorneysFor_GotFocus()
    OnControlGotFocus cmbAttorneysFor
End Sub

'Private Sub cmbCounselType_Change()
'    Me.Changed = True
'End Sub
'
'Private Sub cmbCounselType_GotFocus()
'    OnControlGotFocus cmbCounselType
'End Sub

Private Sub cmbCounselType_GotFocus()
    OnControlGotFocus cmbCounselType
    CascadeUpdates = True
End Sub

Private Sub cmbCounselType_ItemChange()
    If CascadeUpdates = True Then Me.Changed = True
    'DisplayControls
    Me.Counsel.TypeID = Me.cmbCounselType.BoundText
    If Not (Me.Counsel.Definition.AttorneyRequired) Then
        ClearInput
    Else
        CascadeUpdates = True
        ShowCounsel False
    End If
    SetInterfaceControls
    CascadeUpdates = False
End Sub

Private Sub cmbCounselType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCounselType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCounselType_Validate(Cancel As Boolean)
    If cmbCounselType.Text = "" Then Exit Sub
    On Error GoTo ProcError
    On Error Resume Next
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCounselType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub



Private Sub cmbNoticeAttorney_GotFocus()
    OnControlGotFocus Me.cmbNoticeAttorney
End Sub

Private Sub cmbNoticeAttorney_LostFocus()
    On Error GoTo ProcError
    
    If Me.cmbNoticeAttorney.BoundText > 0 And _
    Me.cmbNoticeAttorney.Enabled Then
        Me.Pleading.NoticeAttorney = Me.cmbNoticeAttorney.BoundText
    End If
    
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbNoticeAttorney_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbNoticeAttorney, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbNoticeAttorney_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbNoticeAttorney)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstCounsels_GotFocus()
    OnControlGotFocus lstCounsels
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF5 And btnNew.Enabled Then
        btnNew_Click
    ElseIf KeyCode = vbKeyF6 And btnSave.Enabled Then
        btnSave_Click
    ElseIf KeyCode = vbKeyF7 And btnDelete.Enabled Then
        btnDelete_Click
   '---9.5.0
    ElseIf KeyCode = vbKeyF2 And btnAddressBooks.Visible Then
        btnAddressBooks_Click
    '---end 9.5.0
    Else
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstCounsels_RowChange()
    On Error GoTo ProcError
    If Not Me.Initializing Then
        Me.CascadeUpdates = True
        ChangeActiveCounsel
        DoEvents
        Me.CascadeUpdates = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "Error displaying signature."
    Exit Sub
End Sub

Private Sub saGrid_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error Resume Next
    If Me.CascadeUpdates = True Then Exit Sub
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
End Sub

Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String, xSignerEmail As String)
    Dim oPerson As CPerson
    On Error Resume Next
    Set oPerson = g_oMPO.People.Item(lAuthorID)
    xLicenseID = oPerson.Licenses.Default.Number
    xAttorneyName = oPerson.FullName '(or use Short Name or whatever client prefers)
    xSignerEmail = oPerson.EMail
End Sub

'Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String)
'    Dim colLic As CLicenses
'
'    On Error Resume Next
'
'    Set colLic = g_oMPO.Attorneys.Item(lAuthorID).Licenses
'    If colLic.Default Is Nothing Then Exit Sub
'    On Error GoTo ProcError
'    With colLic
'        If .Count <> 0 Then
'            xLicenseID = .Default.Number
'        Else
'            xLicenseID = ""
'        End If
'    End With
'    xAttorneyName = g_oMPO.Attorneys.Item(lAuthorID).FullName  '(or use Short Name or whatever client prefers)
'    Exit Sub
'ProcError:
'    g_oError.Show Err, "Error loading attorney license."
'    Exit Sub
'
'End Sub

'''Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String)
'''    Dim oPerson As CPerson
'''    On Error Resume Next
'''    Set oPerson = g_oMPO.People.Item(lAuthorID)
'''    xLicenseID = oPerson.Licenses.Default.Number
'''    xAttorneyName = oPerson.FullName '(or use Short Name or whatever client prefers)
'''End Sub

Private Sub saGrid_SignerChange()
    On Error Resume Next
    Me.Counsel.DisplayName = g_oMPO.People(saGrid.Author).FullName & Me.txtFirmName
    If Me.CascadeUpdates = True Then Exit Sub
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
End Sub

Private Sub saGrid_SignersChange()
    On Error Resume Next
    If Me.CascadeUpdates = True Then Exit Sub
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
End Sub

Private Sub txtClientName_GotFocus()
    OnControlGotFocus txtClientName
End Sub

Private Sub txtClientName_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    Me.Changed = True
End Sub

Private Sub Form_Activate()
    Dim bFlag As Boolean
    
    On Error GoTo ProcError
    
    Me.vsIndexTab1.CurrTab = 0
    
    If Not Me.ParentForm Is Nothing Then
        With Me.Counsel
            .TypeID = ParentForm.Counsel.TypeID
        End With
    End If
    
    Me.btnDelete.ToolTipText = mpDeleteSig
    Me.btnSave.ToolTipText = mpSaveChanges
    
    DoEvents
    
    Me.CascadeUpdates = False
    Me.lstCounsels.SelectedItem = 1

    'GLOG : 5718 : ceh
    Me.cmbNoticeAttorney.BoundText = Me.Pleading.NoticeAttorney
    If Me.cmbNoticeAttorney.BoundText = "" Then
        Me.cmbNoticeAttorney.SelectedItem = 0
    End If

    Me.FullRowFormat = Counsel.Definition.FullRowFormat
    If Me.Counsels.Count < Val(GetMacPacIni("Pleading", "MaxCounsels")) Then
        btnNew_Click
        bFlag = True
    End If
    
    If Me.cmbCounselType.Text = "" Then
        Me.cmbCounselType.BoundText = Me.Counsel.TypeID
    End If
    
    If Me.ParentForm Is Nothing Then
        'mpbase2.MoveToLastPosition Me
    Else
        Me.Top = Me.ParentForm.Top
        Me.Left = Me.ParentForm.Left
    End If
        
    '---position visible controls
    PositionControls
    
    LockWindowUpdate (0&)
    Application.ScreenUpdating = True
    DoEvents
    Me.Initializing = False
    Me.Changed = False
    
    If bFlag = True Then
        Me.Added = True
        btnNew.Enabled = False
        btnSave.Enabled = True
        Me.btnDelete.ToolTipText = mpCancelAdd
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error activating pleading signature form."
    LockWindowUpdate (0&)
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim xKey As String
    Dim oList As mpDB.CList
    Dim m_Counsel As MPO.CPleadingCounsel


    On Error GoTo ProcError

'---park form offscreen during form init
    Application.ScreenUpdating = False
    LockWindowUpdate Me.hwnd
    Me.Top = 20000
    Me.Initializing = True
    Me.CascadeUpdates = False
    Me.Cancelled = True

    Set m_Document = New MPO.CDocument
    Set m_oDB = g_oMPO.db
    Set xARLayout = g_oMPO.NewXArray
    
    
    If Counsels Is Nothing Then
        Counsels = g_oMPO.NewPleadingCounsels
        Counsels.LoadValues
        If Counsels.Count = 0 Then Counsels.Add
    End If

    Counsel = Me.Counsels(1)

    If Not Me.ParentForm Is Nothing Then
        Me.PleadingType = Me.ParentForm.Pleading.Definition
        Me.Pleading = Me.ParentForm.Pleading
    Else
'---9.5.0
'        Me.Pleading = g_oMPO.NewPleading(Level0, Level1, Level2, Level3)
        Me.Pleading = g_oMPO.ExistingPleading(True)
'---end 9.5.0
        Me.PleadingType = Me.Pleading.Definition
        If Me.Counsel.TypeID = 0 Then
            Me.Counsel.TypeID = Me.PleadingType.DefaultCounselType
        End If
    End If
    
 '---set signers grid with bar id props from def
    With Me.Counsel.Definition
        If Counsel.Position = 0 Then Counsel.Position = .DefaultPosition
        saGrid.IncludeBarID = .IncludeBarID
        saGrid.DisplayBarID = .IncludeBarID
        saGrid.BarIDPrefix = .BarIDPrefix
        saGrid.BarIDSuffix = .BarIDSuffix
        
        '---9.6.2
        saGrid.IncludeSignerEmail = .IncludeSignerEmail
        saGrid.SignerEmailPrefix = .SignerEmailPrefix
        '---9.7.1 4067
        saGrid.DisplayEMail = .AllowSignerEmailInput
        '---9.7.1 4105
        saGrid.SignerEmailSuffix = .SignerEmailSuffix
        '--- 9.7.1 4096
        saGrid.SignerNameCase = .SignerNameCase
    End With

'---initialize controls
    Me.saGrid.Attorneys = g_oMPO.Attorneys.ListSource
    Me.cmbAttorneysFor.Array = g_oMPO.Lists("PleadingParties").ListItems.Source
    ResizeTDBCombo Me.cmbAttorneysFor, 10
    
    'GLOG : 5718 : ceh
    With Me.cmbNoticeAttorney
        .Array = xARClone(g_oMPO.Attorneys.ListSource)
        .Rebind
        .Array.Insert 1, 0
        .Array.value(0, 0) = -1
        .Array.value(0, 1) = "-none-"
        .Array.value(0, 2) = -1
        ResizeTDBCombo Me.cmbNoticeAttorney, 10
    End With

    'GLOG : 5718 : ceh
    Me.lblNoticeAttorney.Enabled = False
    Me.cmbNoticeAttorney.Enabled = False
    
'---initialize interface
    SetInterfaceControls

'---fill  caption list box
    RefreshList

'---load layout grid with counsels
    RefreshLayoutGrid

'---9.4.1 - fill caption types combo box with types appropriate to pleading type
    On Error Resume Next
    Set oList = g_oMPO.Lists.Item("PleadingCounsels")
    If Err = 0 Then
        With oList
            .FilterField = "tblPleadingCounselAssignments.fldPleadingTypeID"
            .FilterValue = Me.PleadingType.ID
            .Refresh
            Me.cmbCounselType.Array = .ListItems.Source
        End With
    End If
    
    ResizeTDBCombo Me.cmbCounselType, 4
'---end 9.4.1
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCounsel.Form_Load"), _
              g_oError.Desc(Err.Number)
    LockWindowUpdate (0&)
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    If Me.Added = True Then
        Me.Changed = False
        DeleteCounsel True
    End If
    
    'SetLastPosition Me
    
    Set xARLayout = Nothing
    Set m_Counsels = Nothing
    Set m_oCounsel = Nothing
    Set m_Document = Nothing
    Set frmPleadingCounsel = Nothing
    Set g_oPrevControl = Nothing
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error unloading form."
    Exit Sub
End Sub
Private Sub grdLayout_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
    Dim i As Integer
    
    On Error GoTo ProcError
    is_SourceRow = RowBookmark
    is_SourceCol = ColIndex

    If grdLayout.Text = Empty Then
        Exit Sub
    End If
    
    With grdLayout.Array
    
        ReDim xDragFrom(0, 0, .UpperBound(3))
        grdLayout.MarqueeStyle = dbgHighlightCell
        
        For i = 0 To .UpperBound(3)
            xDragFrom(0, 0, i) = .value(RowBookmark, ColIndex, i)
        Next i

    End With

' Use Visual Basic manual drag support
    grdLayout.Drag vbBeginDrag
    Exit Sub
ProcError:
    g_oError.Show Err, "Error dragging cell."
    Exit Sub
End Sub


Private Sub grdLayout_DragDrop(Source As Control, x As Single, y As Single)
    Dim idestCol As Integer
    Dim idestRow As Integer
    Dim i, j, r, c As Integer
    Dim temp() As String
    Dim vSourceID As Variant
    Dim iSourcePosition As Integer
    Dim iDestPosition As Integer
    Dim m_Counsel As MPO.CPleadingCounsel
    Dim xARLayout As XArray

    On Error Resume Next
    
    Set xARLayout = grdLayout.Array
    
    r = Val(grdLayout.RowContaining(y))
    c = grdLayout.ColContaining(x)

'---don't allow drop if cell is occupied
    If Len(grdLayout.Text) Then _
        Exit Sub
    
    '****9.7.1 - 3855
    ' Don't allow drop on disabled cell
    If Me.FullRowFormat And c = 1 Then Exit Sub
    '****end 9.7.1
    
    On Error GoTo ProcError
    
'---create row space in array if necessary
    If r > xARLayout.UpperBound(1) Then
        xARLayout.Insert 1, xARLayout.UpperBound(1) + r
    End If

    If Source.Name = "grdLayout" Then

'---Get coordinates of drop zone
        idestCol = Abs(grdLayout.ColContaining(x))
        idestRow = grdLayout.RowBookmark(grdLayout.RowContaining(y))
'---exit if invalid row
        If idestRow < 0 Then Exit Sub
        iDestPosition = (idestRow + 1) * (idestCol + 1)

'---Capture ID, Position of source item
        If Counsels.Count > 0 Then
            vSourceID = xARLayout(is_SourceRow, is_SourceCol, 1)
            Set m_Counsel = Counsels.Item(vSourceID)
            If Not m_Counsel Is Nothing Then
                iSourcePosition = m_Counsel.Position
            Else
                Exit Sub
            End If
        End If

'---empty out source item in grid
        For i = 0 To xARLayout.UpperBound(3)
            xARLayout(is_SourceRow, is_SourceCol, i) = Empty
        Next i
        grdLayout.Rebind

'---create row space in array if necessary
        If idestRow > xARLayout.UpperBound(1) Then
            xARLayout.Insert 1, xARLayout.UpperBound(1) + idestRow
        End If

        m_Document.StatusBar = "Moving..."

'---move down only within dropped columns
        ReDim temp(0, 0, xARLayout.UpperBound(3)) As String

        For i = idestRow To idestRow
            For j = 0 To xARLayout.UpperBound(3)
                temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                xDragFrom(0, 0, j) = temp(0, 0, j)
            Next j
        Next i

        If temp(0, 0, 0) <> Empty Then
            For i = idestRow + 1 To xARLayout.UpperBound(1)
                For j = 0 To xARLayout.UpperBound(3)
                    temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                    xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                    xDragFrom(0, 0, j) = temp(0, 0, j)
                Next j
            Next i
        End If
        
        grdLayout.Bookmark = Null
        grdLayout.Rebind

        grdLayout.MarqueeStyle = dbgHighlightCell
        grdLayout.Bookmark = idestRow
        grdLayout.Col = idestCol

        m_Document.StatusBar = "Counsel moved"

'---refresh position props
    RefreshCounselPositions

'---refresh counsels List
    RefreshList
    
    End If
    grdLayout.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err, "Error completing drag and drop operation."
    Exit Sub
End Sub
Private Sub grdLayout_DragOver(Source As Control, x As Single, y As Single, State As Integer)
' DragOver provides  visual feedback

    Dim dragFrom As String
    Dim overCol As Integer
    Dim overRow As Long

    On Error GoTo ProcError
    overCol = grdLayout.ColContaining(x)
    overRow = grdLayout.RowContaining(y)
    
    If overCol >= 0 Then grdLayout.Col = overCol
    If overRow >= 0 Then grdLayout.Bookmark = overRow
    grdLayout.MarqueeStyle = dbgHighlightCell

    Exit Sub
ProcError:
    g_oError.Show Err, "Error on drag over."
    Exit Sub
End Sub

Private Sub grdLayout_GotFocus()
    OnControlGotFocus grdLayout
    If Not Me.FullRowFormat Then
        grdLayout.Columns(1).BackColor = grdLayout.Columns(0).BackColor
    End If
    GridTabOut Me.grdLayout, True, 1
End Sub

Private Sub grdLayout_KeyDown(KeyCode As Integer, Shift As Integer)

    On Error GoTo ProcError
    If KeyCode = 9 Then GridTabOut Me.grdLayout, False, 1
    If KeyCode = 40 And Shift = 2 Then Me.PopupMenu mnuLayoutGrid
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdLayout_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'---sets bookmark and col
    Dim overCol As Integer
    Dim overRow As Integer
    Dim iPos As Integer
    Dim xKey As String

    On Error GoTo ProcError
    overCol = grdLayout.ColContaining(x)
    overRow = grdLayout.RowContaining(y)
    If overCol >= 0 Then grdLayout.Col = overCol
    If overRow >= 0 Then grdLayout.Bookmark = overRow

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_GotFocus()
    m_xSigners = saGrid.SignerText & saGrid.SignersText
    OnControlGotFocus saGrid
End Sub

Private Sub saGrid_LostFocus()
    Dim oPerson As CPerson
    Dim oOff As COffice
    
    On Error GoTo ProcError
        
    Set oPerson = g_oMPO.db.People(saGrid.Author)
    
    If Not oPerson Is Nothing Then
        If g_oMPO.UseDefOfficeInfo Then
            Set oOff = g_oMPO.Offices.Default
        Else
            Set oOff = oPerson.Office
        End If
'---Fill Address fields if signer for sig is valid MacPac Person
        With oOff
            
            '---9.7.1 - 4030
            If Me.Counsel.Definition.OfficeAddressFormat = 0 Then
                 Me.txtFirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
            Else
                 Me.txtFirmAddress = xOfficeAddress(oOff, _
                                                    Me.Counsel.Definition.OfficeAddressFormat, _
                                                    True)
            End If
            
            Me.txtFirmFax = .Fax1
            'Me.txtFirmID = ""
            Me.txtFirmPhone = .Phone1
            Me.txtEMail = oPerson.EMail
            Me.txtFirmName = .FirmName
            Me.txtFirmSlogan = .Slogan
        End With
    End If
    
    If Me.CascadeUpdates = True Then Exit Sub
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 28 Then KeyCode = 0
    If Me.CascadeUpdates = True Then Exit Sub
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub saGrid_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim bEnabled As Boolean
    Dim lTestID As Long

    On Error GoTo ProcError
    If Button = 2 Then
        With saGrid
            On Error Resume Next
            lTestID = m_oDB.Attorneys(.Author).ID
        '   enable specified menu items only if author
        '   is currently in the db
            If lTestID = 0 Then
                bEnabled = False
            Else
                bEnabled = True
            End If
        End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_AuthorNotInList(lAuthorID As Long)
    MsgBox "Author " & lAuthorID & " not in loaded list.", vbInformation, "Signing Attorney Grid"
End Sub


Private Sub saGrid_OnLicenseColDropDown(lSignerID As Long)
    On Error GoTo ProcError
    saGrid.SignerBarIDs = g_oMPO.Attorneys.Item(lSignerID).Licenses.ListSource
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_SelectedSigner(lAuthorID As Long, bCancelChange As Boolean)
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "Error selecting signer."
    Exit Sub
End Sub

Private Sub txtEMail_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    Me.Changed = True
End Sub

Private Sub txtEMail_GotFocus()
    OnControlGotFocus txtEMail
End Sub

Private Sub txtFirmAddress_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    If txtFirmAddress.Text <> m_xFirmAddress Then
        Me.Changed = True
        m_xFirmAddress = txtFirmAddress.Text
    End If
End Sub

Private Sub txtFirmPhone_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    Me.Changed = True
End Sub

Private Sub txtFirmFax_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    Me.Changed = True
End Sub

Private Sub txtFirmAddress_DblClick()
    Dim vAddress
    Dim vName
    On Error GoTo ProcError
    '---9.5.0
    GetContact
    '---9.5.0
'''    g_oMPO.GetContactNameAddress vAddress, vName
'''    txtFirmAddress = vAddress
'''    txtFirmName = UCase(vName)
    Exit Sub
ProcError:
    g_oError.Show Err, "Error loading contacts."
    Exit Sub
End Sub

Private Sub txtFirmAddress_GotFocus()
    OnControlGotFocus txtFirmAddress
    m_xFirmAddress = txtFirmAddress
End Sub

Private Sub txtFirmPhone_GotFocus()
    OnControlGotFocus txtFirmPhone
End Sub

Private Sub txtFirmFax_GotFocus()
    OnControlGotFocus txtFirmFax
End Sub

Private Sub txtFirmName_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    If Me.Initializing = True Then Exit Sub
    Me.Changed = True
End Sub

Private Sub txtFirmName_GotFocus()
    OnControlGotFocus txtFirmName
End Sub

Private Sub txtFirmSlogan_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    If Me.Initializing = True Then Exit Sub
    Me.Changed = True
End Sub

Private Sub txtFirmSlogan_GotFocus()
    OnControlGotFocus txtFirmSlogan
End Sub

Private Sub vsIndexTab1_Click()
    On Error GoTo ProcError
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            If btnNew.Enabled Then
                Me.btnNew.SetFocus
            Else
                Me.btnSave.SetFocus
            End If
        Case 1
            If Me.txtFirmName.Visible = True Then Me.txtFirmName.SetFocus
        Case 2
            '---9.6.2 - prompt or save before allowing trip to layout grid tab
            If Me.Added Or Me.Changed Then
                '---9.7.1 - 4083
'''                MsgBox "You must first save your added or changed counsel", vbInformation, App.Title
'''                Me.vsIndexTab1.CurrTab = 0
'''                Me.btnSave.SetFocus
                btnSave_Click
                Exit Sub
            Else
                Me.grdLayout.SetFocus
            End If
    
    End Select
    Exit Sub
ProcError:
End Sub

'******************************************************************
'private functions
'******************************************************************

Public Sub SetInterfaceControls()
    Dim bEnabled As Boolean
    Dim bVisible As Boolean
    
'---sets controls displayed in interface
    On Error GoTo ProcError

    With saGrid
 '---these menu items are not available for signature -- they are problematic for various reasons,
 '---ie, counsels don't require authors from db, there are no options sets for pleadings, etc.
        .MenuAddNewVisible = False
        .MenuCopyVisible = False
        .MenuCopyOptionsVisible = False
        .MenuFavoriteVisible = False
        .MenuSetDefaultVisible = False
        .MenuManageListsVisible = False
        .MenuSep1Visible = False
        .MenuSep2Visible = False
        .Visible = Not bEnabled
    End With

    With Me.Counsel.Definition
        SetLayoutGridDisplay
''---set layout grid for appropriate display
'        If .FullRowFormat Then
'            With grdLayout.Columns(1)
'                .AllowFocus = False
'                .BackColor = vbButtonFace
'                .Caption = ""
'                grdLayout.Columns(0).Caption = "Left Column"
'            End With
'        Else
'            With grdLayout.Columns(0)
'                .AllowFocus = True
'                .BackColor = g_lActiveCtlColor
'                .Caption = "Left Column"
'                grdLayout.Columns(1).Caption = "Right Column"
'            End With
'        End If
    
        '---9.4.1
        Me.saGrid.Enabled = .AttorneyRequired
        Me.lblAttorneysFor.Enabled = .AttorneyRequired
        Me.cmbAttorneysFor.Enabled = .AttorneyRequired
        Me.lblAttorneys.Enabled = .AttorneyRequired
        Me.txtClientName.Enabled = .AttorneyRequired
        Me.lblClientName.Enabled = .AttorneyRequired
        If .AttorneyRequired = False Then
            Me.chkCreateSignature.Enabled = .AttorneyRequired
        End If
        Me.vsIndexTab1.TabVisible(1) = .AttorneyRequired
        '---end 9.4.1
        
'---display various controls
        Me.lblEMail.Visible = Abs(CInt(.AllowSignerEMail))
        Me.txtEMail.Visible = Me.lblEMail.Visible
        Me.lblFirmFax.Visible = Abs(CInt(.AllowFirmFax))
        Me.txtFirmFax.Visible = Me.lblFirmFax.Visible
        Me.lblFirmSlogan.Visible = Abs(CInt(.AllowFirmSlogan))
        Me.txtFirmSlogan.Visible = Me.lblFirmSlogan.Visible
        
        'GLOG : 5718 : ceh
        Me.lblNoticeAttorney.Visible = Me.Pleading.Definition.NoticeAttorney
        Me.cmbNoticeAttorney.Visible = Me.Pleading.Definition.NoticeAttorney
        
'---9.6.2 -- hide/display CI button
        Me.btnAddressBooks.Visible = g_bShowAddressBooks
        
        
    End With
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.SetInterfaceControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Function PositionControls()
    On Error GoTo ProcError
    
    Dim sOffset As Single
'---adjust this if you want more or less space between controls on tab
    Dim sIncrement As Single
    
'---9.4.1 tab 1
    sIncrement = 300
    
    With Me
        '---set combo and label = true if you want to expose counsel types
        .cmbCounselType.Visible = False
        .lblCounselType.Visible = .cmbCounselType.Visible
        If .cmbCounselType.Visible = False Then
            sOffset = (.saGrid.Top - .cmbCounselType.Top) - sIncrement
            .saGrid.Top = .cmbCounselType.Top
            .lblAttorneys.Top = .lblCounselType.Top
            .lblAttorneysFor.Top = .lblAttorneysFor.Top - sOffset
            .cmbAttorneysFor.Top = .cmbAttorneysFor.Top - sOffset
            .lblClientName.Top = .lblClientName.Top - sOffset
            .txtClientName.Top = .txtClientName.Top - sOffset
            .chkCreateSignature.Top = .chkCreateSignature.Top - sOffset
        End If
        
        'GLOG : 5718 : ceh
        If Me.lblNoticeAttorney.Visible Then
            .lstCounsels.Height = 1188
        Else
            .lstCounsels.Height = 1400
        End If
    End With
    
    
'---tab 2
'---end 9.4.1

    sIncrement = 235

    With Me
        If .txtFirmSlogan.Visible = True Then
            sOffset = .txtFirmName.Top + .txtFirmName.Height + sIncrement
        
            .lblFirmAddress.Top = sOffset
            .txtFirmAddress.Top = sOffset
            sOffset = sOffset + .txtFirmSlogan.Height + sIncrement
        Else
            sOffset = .txtFirmName.Top + .txtFirmName.Height + sIncrement
        End If
        
        If .txtFirmAddress.Visible = True Then
            .lblFirmAddress.Top = sOffset
            .txtFirmAddress.Top = sOffset
            sOffset = sOffset + .txtFirmAddress.Height + sIncrement
        End If
       
        If .txtFirmPhone.Visible = True Then
            .lblFirmPhone.Top = sOffset
            .txtFirmPhone.Top = sOffset
            sOffset = sOffset + .txtFirmPhone.Height + sIncrement
        End If

        If .txtFirmFax.Visible = True Then
            .lblFirmFax.Top = sOffset
            .txtFirmFax.Top = sOffset
            sOffset = sOffset + .txtFirmFax.Height + sIncrement
        End If

        If .txtEMail.Visible = True Then
            .lblEMail.Top = sOffset
            .txtEMail.Top = sOffset
            sOffset = sOffset + .txtEMail.Height + sIncrement
        End If
    
    End With
    
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.PositionControls"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Public Sub NewCounsel()
'creates a new, empty Counsel
    Dim lPrevPos As Long
    Dim lPrevCount As Long
    Dim lPrevID As Long
    
    
'   save Counsel if necessary
    DoEvents
    If Me.Added Then Exit Sub
    On Error GoTo ProcError
    If Me.Changed Then
        PromptToSave
    End If
    
'---capture present counsel pos & count
    lPrevCount = Me.Counsels.Count
    lPrevPos = Me.Counsel.Position

'   add Counsel -- set position
    Me.Counsel = Me.Counsels.Add
    Me.Counsel.TypeID = Me.PleadingType.DefaultCounselType
    
    '---9.5.0
    If Me.FullRowFormat = True Then
    '---end 9.5.0
        Counsel.Position = Counsels.HighPosition + 2
    Else

'---fill empty position if second counsel added and counsel 1 position = 2
        If lPrevCount = 1 And lPrevPos = 2 Then
            Counsel.Position = 1
        Else
            Counsel.Position = Counsels.HighPosition + 1
        End If
    End If

'---update listbox with new array
    Me.lstCounsels.Array = Counsels.ListArray(True)
    Me.lstCounsels.Rebind
    
'   select appropriate entry in list
    If Counsel.Position = 1 Then
        Me.lstCounsels.SelectedItem = lstCounsels.ApproxCount - 1
    Else
        Me.lstCounsels.SelectedItem = _
            Me.lstCounsels.ApproxCount
    End If
    
    DoEvents
    On Error Resume Next
    
    Me.vsIndexTab1.CurrTab = 0
    Me.saGrid.SetFocus
    Me.Added = True
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCounsel.NewCounsel"
    Exit Sub
End Sub

Private Function PromptToSave() As Boolean
    Dim xMsg As String
    Dim iChoice As Integer
    
    On Error GoTo ProcError
    xMsg = "Save current entry?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        UpdateCounsel
    Else
        If Me.Added Then
            Me.Added = False
            DeleteCounsel True
        End If
    End If
    PromptToSave = (iChoice = vbYes)
    Exit Function
ProcError:
    PromptToSave = (iChoice = vbYes)
    g_oError.Raise "MacPac90.frmPleadingCounsel.PromptToSave"
    Exit Function
End Function

Private Sub SaveCounsel()
'saves the current Counsel
    On Error GoTo ProcError
'   ensure that there is a new Counsel object
    If Me.Counsels.Count = 0 Then
'       add Counsel
        Set Counsel = Me.Counsels.Add()
    End If
    UpdateCounsel
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCounsel.SaveCounsel"
    Exit Sub
End Sub

Private Sub RefreshList()
    Dim iNum As Integer
    
    With Me.lstCounsels
        .Array = Counsels.ListArray(True)
        .Rebind
        
        '---9.7.1 - 2546 - unrem to provide count
        iNum = Me.lstCounsels.Array.Count(1)

        On Error GoTo ProcError
        If iNum > 1 Then
            Me.lblCounsels = _
                " &Counsels (" & iNum & "):"
        ElseIf iNum = 1 And Me.txtClientName <> Empty Then
            Me.lblCounsels = " &Counsels (1):"
        Else
            Me.lblCounsels = " &Counsels:"
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub UpdateCounsel()
'updates the current Counsel object
'with dialog field values
    Dim bIncludeID
    Dim xSep As String
    Dim xTemp As String
    Dim xDFN As String
    
    On Error GoTo ProcError
    With Counsel
        .DynamicUpdating = True
        Set .Signers = saGrid.Signers
        bIncludeID = saGrid.IncludeBarID
        saGrid.IncludeBarID = False
        '---9.6.2
        saGrid.IncludeSignerEmail = False
        
        If txtFirmName <> "" Then
            xDFN = " - " & txtFirmName
        Else
            xDFN = txtFirmName
        End If
        If .Definition.AttorneyRequired Then
            '---9.7.1 - 4096
            .DisplayName = StrConv(saGrid.SignerText, vbProperCase) & xDFN
        Else
            .DisplayName = .Definition.Description
        End If
        saGrid.IncludeBarID = bIncludeID
        
        '---9.6.2
        saGrid.IncludeSignerEmail = .Definition.IncludeSignerEmail
        
        .ClientPartyName = Me.txtClientName
        .ClientPartyTitle = Me.cmbAttorneysFor.Text
        '---9.7.1 - 4035, 4067
        On Error Resume Next
        '---9.7.1 - 4035
        If saGrid.Author = 0 Or g_oMPO.UseDefOfficeInfo Then
            .Office = g_oMPO.Offices.Default
        Else
            .Office = g_oMPO.Attorneys(saGrid.Author).Office
        End If
        .FirmName = Me.txtFirmName
        .FirmSlogan = Me.txtFirmSlogan
        .FirmSlogan2 = Me.txtFirmSlogan
        '9.7.2 #4564
        .FirmAddress = xReformatAddress(xSubstitute(Me.txtFirmAddress, vbLf, ""), .Definition.OfficeAddressFormat)
        .FirmPhone = Me.txtFirmPhone
        .FirmFax = Me.txtFirmFax
        .SignerEMail = Me.txtEMail
        '---9.7.1 4104
        .SignersEmail = saGrid.SignersEmailText
        
        On Error Resume Next
        .TypeID = Me.cmbCounselType.BoundText
        With Me.saGrid
            Set Counsel.Signers = .Signers
            xSep = .Separator
            If .SignersText = "" Then
                xTemp = .SignerText
            Else
                xTemp = .SignersText
            End If
            xTemp = xSubstitute(xTemp, xSep, vbLf)
             Me.Counsel.AttorneyNames = xTemp
            xTemp = ""
        End With
    
    End With
    DoEvents
'---create matching sig if specified
'GLOG : 5093 : CEH
    If Me.chkCreateSignature.value = vbChecked Then
        If Me.Counsel.HasMatchingSignature = False Then
            Me.CreateMatchingSignature
            Me.SignatureAdded = True
        Else
            Me.UpdateMatchingSignature
        End If
    End If
    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCounsel.UpdateCounsel"
    Exit Sub
End Sub

Private Function ShowCounsel(Optional bResetTypeID As Boolean = True)
    Dim xAddress As String
    On Error GoTo ProcError
    With Counsel
        xAddress = .FirmAddress
        '---9.7.1 - 4030
        xAddress = xSubstitute(xAddress, vbCr, vbCrLf)
        xAddress = xSubstitute(xAddress, vbLf & vbCrLf, vbCrLf)
        
        Me.txtFirmAddress = xAddress
        Me.txtFirmPhone = .FirmPhone
        Me.txtFirmFax = .FirmFax
        Me.txtFirmName = .FirmName
        Me.txtFirmSlogan = .FirmSlogan
        Me.cmbAttorneysFor.Text = .ClientPartyTitle
        If bResetTypeID Then _
                Me.cmbCounselType.BoundText = .TypeID
        Me.txtClientName = .ClientPartyName
        Me.txtEMail = .SignerEMail
        Me.chkCreateSignature.Enabled = Not .HasMatchingSignature
        If .HasMatchingSignature Then
            Me.chkCreateSignature.value = vbChecked
            Me.chkCreateSignature.Caption = "Counsel has matching signature"
        Else
            Me.chkCreateSignature = vbUnchecked
            Me.chkCreateSignature.Caption = "Create &Signature for this Co-Counsel"
        End If
        On Error Resume Next
        Me.saGrid.Clear
'---don't assign .signers prop directly to sa grid control, otherwise
'---control grid array *IS* the property array and changes made to grid signers prop
'---changes the actual sig property arrray outside of control of object model
        If Not .Signers Is Nothing Then saGrid.Signers = xARClone(.Signers)
    
'---set  change variable for first control in form
    m_xSigners = saGrid.SignerText & saGrid.SignersText
    
    End With
    DoEvents
     
''''    '---9.7.1 - 2546 - rem out in favor of code in .RefreshList
''''    If lstCounsels.SelectedItem = 1 Then
''''        lblCounsels.Caption = "&Counsel:"
''''    Else
''''        lblCounsels.Caption = "&Co-Counsel:"
''''    End If
   
    Me.Changed = False
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCounsel.ShowCounsel"
    Exit Function
End Function

Private Sub ChangeActiveCounsel()
'   prompt to save current Counsel if necessary
    On Error GoTo ProcError
    DoEvents
    If Me.Changed Then
        PromptToSave
    End If

    '9.8.1030
    If Me.lstCounsels.Bookmark = 0 Then
        Me.lstCounsels.Bookmark = 1
    End If
    
'   switch Counsel and show in dialog
    Counsel = Me.Counsels(lstCounsels.Array.value(Min(Me.lstCounsels.Bookmark, Me.Counsels.Count), 1))
    ShowCounsel
    
    'GLOG : 5718 : ceh
    Me.cmbNoticeAttorney.Enabled = (Counsel.ID = 1 And Me.saGrid.Signers.Count(1) > 1)
    Me.lblNoticeAttorney.Enabled = Me.cmbNoticeAttorney.Enabled

    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmPleadingCounsel.ChangeActiveCounsel"
    Exit Sub
End Sub

Private Sub DeleteCounsel(Optional bSuppressMessage As Boolean = False)
    Dim i As Integer
    Dim iNum As Integer
    Dim mbr As VbMsgBoxResult
    
    On Error GoTo ProcError
    i = Me.Counsels.Count
    If i = 1 Then
        If bSuppressMessage = False Then
            MsgBox "A pleading must contain at least one counsel.", vbInformation, App.Title
        End If
        Exit Sub
    End If
    
    If bSuppressMessage = False Then
        mbr = MsgBox("Are you sure you want to delete this co-counsel?", vbYesNo, "Pleading Counsel")
        If mbr <> vbYes Then Exit Sub
    End If
    
    If Me.lstCounsels.Row > -1 Then
        Counsels.Remove (Counsel.Key)
        ClearInput
        
 '---9.5.0
        If GetMacPacIni("Pleading", "CounselTableThreshold") > 0 Then
            If Me.Counsels.Count >= GetMacPacIni("Pleading", "CounselTableThreshold") Then
                Me.FullRowFormat = False
            Else
                Me.FullRowFormat = g_oMPO.db.PleadingCounselDefs.Item(Me.Counsel.Definition.ID).FullRowFormat
            End If
        Else
            Me.FullRowFormat = g_oMPO.db.PleadingCounselDefs.Item(Me.Counsel.Definition.ID).FullRowFormat
        End If
'---End 9.5.0
        
        If Me.FormatChanged Then
            ResetCounselPositions
            SetLayoutGridDisplay
        End If


        RefreshList
        RefreshLayoutGrid
        If Counsels.Count Then
            Me.Changed = False
'           select appropriate entry in list
            If Counsel.Position = 1 Then
                Me.lstCounsels.SelectedItem = lstCounsels.ApproxCount - 1
            Else
                Me.lstCounsels.SelectedItem = _
                    Me.lstCounsels.ApproxCount
            End If

        End If
        
        '---9.7.1 - 2545 rem out in favor of code in .RefreshList
'''        If lstCounsels.Row = 0 Then
'''            lblCounsels.Caption = "&Counsel:"
'''        Else
'''            lblCounsels.Caption = "&Co-Counsel:"
'''        End If
        '---9.7.1 - 4083
'''        If bSuppressMessage = False Then
'''            MsgBox "Co-counsel deleted.  Please check layout grid for position of remaining co-counsel.", vbInformation, App.Title
'''        End If
    End If
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCounsel.DeleteCounsel"
End Sub

Private Sub ClearInput()
'clears all detail fields
    On Error GoTo ProcError
    Me.txtClientName = ""
    Me.txtFirmAddress = ""
    Me.txtFirmName = ""
    Me.txtFirmSlogan = ""
    Me.txtFirmFax = ""
    Me.txtFirmPhone = ""
    Me.txtEMail = ""
    Me.cmbAttorneysFor.Text = ""
    Me.saGrid.Clear
    Me.cmbAttorneysFor.Text = ""
    Me.txtClientName.Text = ""
    Me.chkCreateSignature.value = vbUnchecked
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.ClearInput"
    Exit Sub
End Sub

Private Sub RefreshLayoutGrid()
    Dim m_Couns As MPO.CPleadingCounsel
    Dim xDisplayName As String
    Dim xKey As String
    Dim iPos As Integer
    Dim iRow As Integer
    Dim iCol As Integer
    Dim iCount As Integer
    Dim iSigLimit As Integer
    Dim xARLayout As XArray
    Dim iLimit As Integer
    On Error GoTo ProcError
    
    '---9.4.1
    iLimit = Max(Val(GetMacPacIni("Pleading", "MaxCounsels")), CDbl(mpPleadingCounselLimit))
    iLimit = Max(CDbl(iLimit), Me.Counsels.HighPosition)
'---fills grid and listbox with displayname/selection key arrays
'---load layout grid & sig list with Counsels from counsel collection
   On Error Resume Next
'---Lieff
   If Me.FullRowFormat Then
        iSigLimit = iLimit
    Else
        iSigLimit = iLimit / 2 + 1
    End If
'---Lieff
    On Error GoTo ProcError
    Set xARLayout = g_oMPO.NewXArray
    xARLayout.ReDim 0, iSigLimit, 0, 1, 0, 1
    
    xARLayout.value(0, 0, 0) = ""
    xARLayout.value(0, 0, 1) = ""
    iCount = 0
    
    For Each m_Couns In Me.Counsels
        iPos = m_Couns.Position

        If m_Couns.DisplayName = "" Then
            m_Couns.DisplayName = "No Display Name"
        End If

        xKey = m_Couns.Key
'---calculate grid position based on iPos Value
        If iPos / 2 - Int(iPos / 2) = 0.5 Then
            iRow = CInt((iPos + 1) / 2) - 1
            iCol = 0
        Else
            iRow = CInt(iPos / 2) - 1
            iCol = 1
        End If
     
'---update grid array
        xARLayout.value(iRow, iCol, 0) = m_Couns.DisplayName
        xARLayout.value(iRow, iCol, 1) = xKey
    
    Next m_Couns

    With Me.grdLayout
        .Array = xARLayout
        .Rebind
        .Bookmark = Null
        .Col = 1
    End With
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCounsel.RefreshLayoutGrid"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Public Sub RefreshCounselPositions()
    Dim oCouns As MPO.CPleadingCounsel
    Dim xarTemp As XArray
    Dim i, j As Integer
'---If DD performed, reassign sig.position based on grid position

    On Error GoTo ProcError
    Set xarTemp = Me.grdLayout.Array

    With xarTemp
        For i = .LowerBound(1) To .UpperBound(1)
            For j = .LowerBound(2) To .UpperBound(2)
                On Error Resume Next
                If .value(i, j, 1) <> "" Then
                    Set oCouns = Me.Counsels(.value(i, j, 1))
                    If Not oCouns Is Nothing Then
                        oCouns.Position = (2 * i) + j + 1
                        Set oCouns = Nothing
                    End If
                End If
            Next j
        Next i
    End With
    Set xarTemp = Nothing
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCounsel.RefreshCounselPosition"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


Public Sub ResetCounselPositions()
'---Lieff
    Dim oCouns As MPO.CPleadingCounsel
    Dim xarTemp As XArray
    Dim i As Integer
'---If table threshold reached reassign position based on row format

    On Error GoTo ProcError
    Set xarTemp = Me.Counsels.ListArray(True)
    
    For i = xarTemp.LowerBound(1) To xarTemp.UpperBound(1)
        Set oCouns = Me.Counsels(xarTemp.value(i, 1))
        If Not oCouns Is Nothing Then
            If Me.FullRowFormat = True Then
                oCouns.Position = Max((2 * i) - 1, 1)
            Else
                oCouns.Position = Max(CDbl(i), 1)
            End If
        End If
        Set oCouns = Nothing
    Next i
    Set xarTemp = Nothing
    Exit Sub

ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCounsel.ResetCounselPosition"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


Public Sub CreateMatchingSignature()
    Dim xFormat As String
    On Error GoTo ProcError
    
    Me.PleadingSignature = Me.Pleading.Signatures.Add
    With PleadingSignature

        '---9.6.2
        saGrid.IncludeSignerEmail = False
        .TypeID = Pleading.Definition.DefaultSignatureType
        
        '---9.7.1 4097
        saGrid.SignerNameCase = .Definition.SignerNameCase
        
        '---9.7.1 - 4096
        .DisplayName = StrConv(Me.saGrid.SignerText, vbProperCase)
        
        If .Definition.FullRowFormat Then
            .Position = Me.Pleading.Signatures.HighPosition + 2
        Else
            .Position = Me.Pleading.Signatures.HighPosition + 1
        End If
        
        If saGrid.Author = 0 Or g_oMPO.UseDefOfficeInfo Then
            .Office = g_oMPO.Offices.Default
        Else
            .Office = g_oMPO.Attorneys(saGrid.Author).Office
        End If
        
        .PartyName = Me.txtClientName
        .PartyTitle = Me.cmbAttorneysFor.Text
        .FirmName = Me.txtFirmName
        .FirmSlogan = Me.txtFirmSlogan
        .FirmAddress = Me.txtFirmAddress
        .FirmPhone = Me.txtFirmPhone
        .SignerEMail = Me.txtEMail
        xFormat = Me.PleadingSignature.Definition.DateFormat
        If InStr(xFormat, "Field") Then
            .Dated = xFormat
        ElseIf xFormat <> "" Then
            ' #4237 Replace Ordinal suffix token if necessary
            .Dated = xSubstitute(Format(Date, xFormat), "%or%", mpbase2.GetOrdinalSuffix(Day(Date)))  '9.7.1 #4402
            .Dated = xSubstitute(Format(Date, xFormat), "%o%", mpbase2.GetOrdinalSuffix(Day(Date)))
        End If '---set up signers
        
        
        With .Definition
            
            saGrid.IncludeBarID = .IncludeBarID
            saGrid.BarIDPrefix = .BarIDPrefix
            saGrid.BarIDSuffix = .BarIDSuffix
        End With

        '---9.6.2
        If saGrid.OtherSignersText <> "" Then
            .OtherSigners = xSubstitute(saGrid.OtherSignersText, saGrid.Separator, vbLf)
        Else
            .OtherSigners = saGrid.OtherSignersText
        End If
        '---end 9.6.2
        
        '9.5.0
        If saGrid.SignersText <> "" Then '*c
            .AllSigners = xSubstitute(saGrid.SignersText, saGrid.Separator, vbLf) '*c
        Else '*c
            .AllSigners = saGrid.SignerText '*c
        End If '*c
        'end
        
        .Signer2 = saGrid.SignerText
        '---9.7.1 - 4167
        saGrid.IncludeSignerEmail = False
        saGrid.IncludeBarID = False
        .ESigner = saGrid.SignerText
        saGrid.IncludeSignerEmail = .Definition.IncludeSignerEmail
        saGrid.IncludeBarID = .Definition.IncludeBarID
        
        .Signer = saGrid.SignerText
        Set .Signers = saGrid.Signers
    
    End With
    
'---reset grid props
    With Me.Counsel.Definition
           '---9.6.2
           saGrid.IncludeSignerEmail = .IncludeSignerEmail
           saGrid.SignerEmailPrefix = .SignerEmailPrefix
            '---9.7.1 4105
           saGrid.SignerEmailSuffix = .SignerEmailSuffix
           
           saGrid.IncludeBarID = .IncludeBarID
           saGrid.BarIDPrefix = .BarIDPrefix
           saGrid.BarIDSuffix = .BarIDSuffix
    End With
    
'---flag sig creation in counsel OM
    Me.Counsel.HasMatchingSignature = True
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCounsel.CreateMatchingSignature"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

'GLOG : 5093 : CEH
Public Sub UpdateMatchingSignature()
    Dim xFormat As String
    
    On Error GoTo ProcError
    
    Me.PleadingSignature = Me.Pleading.Signatures(Me.Counsel.ID)
    
    With PleadingSignature
        '---9.6.2
        saGrid.IncludeSignerEmail = False
        .TypeID = Pleading.Definition.DefaultSignatureType
        
        '---9.7.1 4097
        saGrid.SignerNameCase = .Definition.SignerNameCase
        
        '---9.7.1 - 4096
        .DisplayName = StrConv(Me.saGrid.SignerText, vbProperCase)
        
        If saGrid.Author = 0 Or g_oMPO.UseDefOfficeInfo Then
            .Office = g_oMPO.Offices.Default
        Else
            .Office = g_oMPO.Attorneys(saGrid.Author).Office
        End If
        
        .PartyName = Me.txtClientName
        .PartyTitle = Me.cmbAttorneysFor.Text
        .FirmName = Me.txtFirmName
        .FirmSlogan = Me.txtFirmSlogan
        .FirmAddress = Me.txtFirmAddress
        .FirmPhone = Me.txtFirmPhone
        .SignerEMail = Me.txtEMail
        xFormat = Me.PleadingSignature.Definition.DateFormat
        If InStr(xFormat, "Field") Then
            .Dated = xFormat
        ElseIf xFormat <> "" Then
            ' #4237 Replace Ordinal suffix token if necessary
            .Dated = xSubstitute(Format(Date, xFormat), "%or%", mpbase2.GetOrdinalSuffix(Day(Date)))  '9.7.1 #4402
            .Dated = xSubstitute(Format(Date, xFormat), "%o%", mpbase2.GetOrdinalSuffix(Day(Date)))
        End If '---set up signers
        
        With .Definition
            
            saGrid.IncludeBarID = .IncludeBarID
            saGrid.BarIDPrefix = .BarIDPrefix
            saGrid.BarIDSuffix = .BarIDSuffix
        End With

        '---9.6.2
        If saGrid.OtherSignersText <> "" Then
            .OtherSigners = xSubstitute(saGrid.OtherSignersText, saGrid.Separator, vbLf)
        Else
            .OtherSigners = saGrid.OtherSignersText
        End If
        '---end 9.6.2
        
        '9.5.0
        If saGrid.SignersText <> "" Then '*c
            .AllSigners = xSubstitute(saGrid.SignersText, saGrid.Separator, vbLf) '*c
        Else '*c
            .AllSigners = saGrid.SignerText '*c
        End If '*c
        'end
        
        .Signer2 = saGrid.SignerText
        '---9.7.1 - 4167
        saGrid.IncludeSignerEmail = False
        saGrid.IncludeBarID = False
        .ESigner = saGrid.SignerText
        saGrid.IncludeSignerEmail = .Definition.IncludeSignerEmail
        saGrid.IncludeBarID = .Definition.IncludeBarID
        
        .Signer = saGrid.SignerText
        Set .Signers = saGrid.Signers
    End With
    
'---reset grid props
    With Me.Counsel.Definition
           '---9.6.2
           saGrid.IncludeSignerEmail = .IncludeSignerEmail
           saGrid.SignerEmailPrefix = .SignerEmailPrefix
            '---9.7.1 4105
           saGrid.SignerEmailSuffix = .SignerEmailSuffix
           
           saGrid.IncludeBarID = .IncludeBarID
           saGrid.BarIDPrefix = .BarIDPrefix
           saGrid.BarIDSuffix = .BarIDSuffix
    End With
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCounsel.UpdateMatchingSignature"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


Private Function ValidateSigners() As Boolean
    On Error Resume Next
    ValidateSigners = True
    If Me.Counsel.Definition.AttorneyRequired = False Then Exit Function
    
    If saGrid.Signers.UpperBound(1) = -1 Then
        xMsg = "You must select an attorney"
        saGrid.SetFocus
        MsgBox xMsg, vbExclamation
        ValidateSigners = False
        Exit Function
    End If

    If saGrid.SignerText = "" Then
        xMsg = "You must enter at least one attorney name"
        MsgBox xMsg, vbExclamation
        saGrid.SetFocus
        ValidateSigners = False
        Exit Function
    End If
End Function

Private Sub SetLayoutGridDisplay()

'---set layout grid for appropriate display
        If Me.FullRowFormat Then
            With grdLayout.Columns(1)
                .AllowFocus = False
                .Caption = ""
                .BackColor = vbButtonFace
            End With
        Else
'            With grdLayout.Columns(0)
'                .AllowFocus = True
'                .BackColor = g_lActiveCtlColor
'                .Caption = "Left Column"
'            End With
            With grdLayout.Columns(1)
                .AllowFocus = True
                .Caption = "Right Column"
                .BackColor = grdLayout.Columns(0).BackColor
            End With
        End If

End Sub

Private Sub GetContact()
    '---9.5.0 - loads address fields
    Dim xFirmName As String
    Dim xAddress As String
    Dim xPhone As String
    Dim xFax As String
    Dim xEmail As String
    Dim xCity As String
    Dim xState As String
    Dim xZipCode As String
    Dim xSeparator As String
    Dim xTemp As String
    Dim xName As String
    
    Dim colContacts As MPO.CContacts
    On Error GoTo ProcError

    DoEvents

    Set colContacts = New MPO.CContacts
    
    colContacts.Retrieve_ALT bIncludePhone:=True, _
                         bIncludeFax:=True, _
                         bPromptForPhones:=True, _
                         iOnEmpty:=ciEmptyAddress_ReturnEmpty, _
                         bIncludeAll:=True

    If colContacts.Count = 0 Then Exit Sub

    '---9.6.1
    If colContacts.Count > 1 Then
        Dim xMsg
        xMsg = "You have selected more than one contact.  Only the address info for the first contact will be entered into the address info fields on the address tab."
        MsgBox xMsg, vbInformation, App.Title
    End If
    
    With colContacts
        On Error GoTo ProcError
        xName = .Item(1).FullName
        xFirmName = .Item(1).Company
        xFax = .Item(1).Fax
        xPhone = .Item(1).Phone
        xCity = .Item(1).City
        xState = .Item(1).State
        xAddress = .Item(1).StreetAddress
        xEmail = .Item(1).EMail
        xZipCode = .Item(1).ZipCode
    End With

    '---create address detail
    
    xSeparator = vbCrLf
    
    If xAddress <> "" Then
        xTemp = xTemp & xAddress & xSeparator
    End If
    
    If xCity <> "" Then _
        xTemp = xTemp & xCity & ", "
    
    If xState <> "" Then _
        xTemp = xTemp & xState
        
    If xZipCode <> "" Then _
        xTemp = xTemp & "  " & xZipCode
    
    
    With Me
        .txtFirmName = xFirmName
        .txtFirmAddress = xTemp
        .txtFirmPhone = xPhone
        .txtFirmFax = xFax
        .txtEMail = xEmail
        
        '---9.6.1 - load contact name into sa grid last position
        '---you have to tweak the signers array for saGrid to do this
        
        Dim oX As XArray
        
        Set oX = saGrid.Signers
        If saGrid.Signers.value(saGrid.Signers.UpperBound(1), 2) <> "" Then
            oX.Insert 1, oX.UpperBound(1) + 1
        End If
        
        oX.value(oX.UpperBound(1), 2) = xName
        '9.7.2 #4554
        oX.value(oX.UpperBound(1), 5) = xEmail
        
'---9.6.2
''''        '---clear present saGrid pen-holder
''''        Dim i As Integer
''''
''''        For i = 0 To saGrid.Signers.UpperBound(1)
''''            If saGrid.Signers.UpperBound(1) > 0 Then
''''                oX.value(i, 1) = 0
''''            End If
''''        Next
        
        '---this gives the pen to the first signer, if grid was empty after contact insertion
        If oX.Count(1) = 1 Then _
                oX.value(oX.UpperBound(1), 1) = -1
'---end 9.6.2
        
        saGrid.Signers = oX
    
    End With

    Set colContacts = Nothing
    Exit Sub
ProcError:
    Set colContacts = Nothing
    Err.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.GetContact"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


