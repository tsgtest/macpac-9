VERSION 5.00
Begin VB.Form frmAlignWithPleadingPaper 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Pleading Paper Alignment"
   ClientHeight    =   2184
   ClientLeft      =   48
   ClientTop       =   432
   ClientWidth     =   3552
   Icon            =   "frmAlignWithPleadingPaper.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2184
   ScaleWidth      =   3552
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton optParagraph 
      Appearance      =   0  'Flat
      Caption         =   "&Current Paragraph"
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   285
      TabIndex        =   4
      Top             =   285
      Width           =   2295
   End
   Begin VB.OptionButton optToSignature 
      Appearance      =   0  'Flat
      Caption         =   "&From Cursor Position to Signature Block"
      ForeColor       =   &H80000008&
      Height          =   390
      Left            =   285
      TabIndex        =   3
      Top             =   1005
      Width           =   3150
   End
   Begin VB.OptionButton optSelection 
      Appearance      =   0  'Flat
      Caption         =   "&Selected Text"
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   285
      TabIndex        =   2
      Top             =   675
      Width           =   2295
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   1320
      TabIndex        =   0
      Top             =   1695
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   2445
      TabIndex        =   1
      Top             =   1695
      Width           =   1000
   End
End
Attribute VB_Name = "frmAlignWithPleadingPaper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_iPPaperAlignScope As PPaperAlignScope
Private m_bCancelled As Boolean

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Property Get Scope() As PPaperAlignScope
    Scope = m_iPPaperAlignScope
End Property

Property Let Scope(iNew As PPaperAlignScope)
    m_iPPaperAlignScope = iNew
End Property

Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnFinish_Click()
    m_bCancelled = False
    
    'set scope property
    If Me.optParagraph = True Then
        Me.Scope = PPaperAlignScope_Paragraph
    ElseIf Me.optSelection Then
        Me.Scope = PPaperAlignScope_Selection
    Else
        Me.Scope = PPaperAlignScope_ToSignature
    End If
    
    Me.Hide
    DoEvents
    
End Sub

Private Sub Form_Load()

    Me.optParagraph = True
    
    Application.ScreenRefresh
    
End Sub
