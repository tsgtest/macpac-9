VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COptions"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**********************************************************
'   COptionsForm Class
'   created 12/31/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage user options

'   Member of
'   Container for
'**********************************************************
Private m_bHasOptions As Boolean
Private m_bIsDirty As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Let IsDirty(bNew As Boolean)
    m_bIsDirty = bNew
End Property

Public Property Get IsDirty() As Boolean
    IsDirty = m_bIsDirty
End Property

Public Property Get HasOptions(ByVal lID As Long, ByVal xTemplate As String) As Boolean
    Dim oOpt As mpDB.CPersonOptions
    Set oOpt = g_oMPO.People(lID).Options(xTemplate)
    HasOptions = oOpt.Exists
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub SaveOptions(ByVal lID As Long, ByVal xTemplate As String, frmP As Form)
    Dim oOpt As mpDB.CPersonOptions
    Set oOpt = g_oMPO.People(lID).Options(xTemplate, frmP)
    oOpt.IsDirty = Me.IsDirty
    oOpt.Save
End Sub

Public Sub DeleteOptions(ByVal lID As Long, ByVal xTemplate As String)
    Dim oOpt As mpDB.CPersonOptions
    Set oOpt = g_oMPO.People(lID).Options(xTemplate)
    oOpt.Delete lID, xTemplate
End Sub

Public Sub CopyOptions(oOptions As mpDB.CPersonOptions, _
                       ByVal xTemplate As String, _
                       Optional ByVal lPersonID As Long)
'copies the option set oOptions
'to the current user's options-
'typically used to copy the reuse author
'options to the private db
    Dim oPerson As mpDB.CPerson
    Dim iChoice As VbMsgBoxResult
    Dim oSourceOptions As mpDB.CPersonOptions
    Dim xMsg As String
    Dim i As Integer
    Dim lID As Long
    
    With frmCopyOptions
'       get author to copy options to
        .Show vbModal
        If Not .Cancelled Then
            lID = .cmbAuthor.BoundText
            Set oPerson = g_oMPO.People(lID)
            
'           if author already has specified
'           options, prompt to replace
            If HasOptions(oPerson.ID, xTemplate) Then
                xMsg = "You have previously edited the " & xTemplate & _
                       " options for " & oPerson.FullName & "." & vbCr & _
                       "Do you want to replace these options?"
                iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
                If iChoice = vbNo Then
                    Exit Sub
                End If
            End If
            
'           edit options of selected author
            Set oSourceOptions = oPerson.Options(xTemplate)
            
'           cycle through fields, assigning new value
            For i = 1 To oSourceOptions.Count
                With oSourceOptions.Item(i)
                    If UCase(.Name) <> "FLDID" Then
                        .Value = oOptions.Item(.Name).Value
                    End If
                End With
            Next i
            
'           save option set
            With oSourceOptions
                .IsDirty = True
                .Save
            End With
        End If
    End With
    Exit Sub
    
ProcError:
    Err.Raise Err.Number, _
              "MacPac90.COptions", _
              Err.Description
    Exit Sub
End Sub

'Public Sub CopySharedAuthorOptions(oPerson As mpDB.CPerson, oOptions As mpDB.CPersonOptions, ByVal xTemplate As String)
'    Dim bPersonExists As Boolean
''           test if user exists in db
'            On Error Resume Next
'            bPersonExists = (g_oMPO.People(oPerson.ID) <> Nothing)
'            On Error GoTo ProcError
'
''           alert and exit if person does not exist
'            If Not bPersonExists Then
'                xMsg = "The author whose option set you are trying to copy " & _
'                       "is not currently in your list of people.  Please " & _
'                       "add this person to your list before proceeding."
'                MsgBox xMsg, vbExclamation, App.Title
'                Exit Sub
'            End If
'    Exit Sub
'
'ProcError:
'    Err.Raise Err.Number, "MacPac90.COptions", Err.Description
'    Exit Sub
'End Sub
