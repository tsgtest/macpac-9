VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Object = "{FC715925-5921-4AC0-B4BD-36B619407BAE}#1.1#0"; "MPCM.ocx"
Begin VB.Form frmLabels 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create Labels"
   ClientHeight    =   5856
   ClientLeft      =   5700
   ClientTop       =   1560
   ClientWidth     =   5496
   Icon            =   "frmLabels.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5856
   ScaleWidth      =   5496
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   2580
      Picture         =   "frmLabels.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   34
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   5385
      Width           =   690
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4410
      TabIndex        =   36
      Top             =   5385
      Width           =   1000
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3345
      TabIndex        =   35
      Top             =   5385
      Width           =   1000
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   5805
      Left            =   -15
      TabIndex        =   37
      TabStop         =   0   'False
      Top             =   -15
      Width           =   5520
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|E&xtras"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   5364
         Left            =   15
         TabIndex        =   39
         Top             =   15
         Width           =   5490
         Begin TrueDBList60.TDBCombo cmbBarCode 
            Height          =   345
            Left            =   1755
            OleObjectBlob   =   "frmLabels.frx":0CAC
            TabIndex        =   7
            Top             =   3930
            Width           =   3615
         End
         Begin VB.CheckBox chkUSPS 
            Appearance      =   0  'Flat
            Caption         =   "USPS Stan&dards"
            ForeColor       =   &H80000008&
            Height          =   300
            Left            =   3240
            TabIndex        =   5
            Top             =   2820
            Width           =   1590
         End
         Begin TrueDBList60.TDBCombo cmbLabelTypes 
            Height          =   345
            Left            =   1770
            OleObjectBlob   =   "frmLabels.frx":2CF6
            TabIndex        =   9
            Top             =   4395
            Width           =   3615
         End
         Begin VB.CheckBox chkFullPage 
            Appearance      =   0  'Flat
            Caption         =   "Full &Page of Same Label"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   3240
            TabIndex        =   4
            Top             =   2490
            Width           =   2085
         End
         Begin TrueDBList60.TDBCombo cmbStartingRow 
            Height          =   375
            Left            =   8025
            OleObjectBlob   =   "frmLabels.frx":4D43
            TabIndex        =   31
            Top             =   2115
            Visible         =   0   'False
            Width           =   705
         End
         Begin VB.TextBox txtAddresses 
            Appearance      =   0  'Flat
            Height          =   2070
            Left            =   1785
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   1
            Top             =   180
            Width           =   3570
         End
         Begin TrueDBList60.TDBCombo cmbStartingColumn 
            Height          =   375
            Left            =   8025
            OleObjectBlob   =   "frmLabels.frx":6F41
            TabIndex        =   33
            Top             =   2520
            Visible         =   0   'False
            Width           =   705
         End
         Begin TrueDBList60.TDBCombo cmbOutputTo 
            Height          =   555
            Left            =   1770
            OleObjectBlob   =   "frmLabels.frx":9142
            TabIndex        =   11
            Top             =   4875
            Width           =   3615
         End
         Begin TrueDBGrid60.TDBGrid grdPreview 
            Height          =   1365
            Left            =   1785
            OleObjectBlob   =   "frmLabels.frx":B355
            TabIndex        =   3
            Top             =   2415
            Width           =   1185
         End
         Begin VB.Label lblBarCode 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " &Bar Code:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   810
            TabIndex        =   6
            Top             =   3990
            Width           =   765
         End
         Begin VB.Label lblOutputTo 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Output Labels To:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   225
            TabIndex        =   10
            Top             =   4950
            Width           =   1350
         End
         Begin VB.Label lblPreview 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Starting Label:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   540
            TabIndex        =   2
            Top             =   2430
            Width           =   1035
         End
         Begin VB.Label lblStartingRow 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Starting &Row:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   6315
            TabIndex        =   30
            Top             =   2205
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblStartingColumn 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Starting &Column:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   6315
            TabIndex        =   32
            Top             =   2580
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblAddresses 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Recipients:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   780
            TabIndex        =   0
            Top             =   180
            Width           =   795
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000005&
            X1              =   -15
            X2              =   5770
            Y1              =   5295
            Y2              =   5295
         End
         Begin VB.Label lblLabelTypes 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Label Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   240
            TabIndex        =   8
            Top             =   4455
            Width           =   1335
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6285
            Left            =   -45
            Top             =   -975
            Width           =   1725
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   5364
         Left            =   6012
         TabIndex        =   38
         Top             =   15
         Width           =   5490
         Begin MPCM.ClientMatterComboBox cmbCM 
            Height          =   315
            Left            =   1740
            TabIndex        =   13
            Top             =   180
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            SourceClass     =   "mpCMIA.CClientMatter"
            Col1Caption     =   "No."
            ColWidth1       =   1020
            ColWidth2       =   1020
            ListRows        =   6
            ComboType       =   2
            SelectTextOnFocus=   -1  'True
            PopulateOnLoad  =   0   'False
            AdditionalClientFilter=   ""
            AdditionalMatterFilter=   ""
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase 
            Height          =   690
            Left            =   1740
            TabIndex        =   17
            Top             =   1185
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   1207
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin mpControls.MultiLineCombo mlcDeliveryPhrase2 
            Height          =   690
            Left            =   1740
            TabIndex        =   19
            Top             =   2010
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   1207
            ListRows        =   10
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin TrueDBList60.TDBCombo cmbOffice 
            Height          =   555
            Left            =   1740
            OleObjectBlob   =   "frmLabels.frx":DD51
            TabIndex        =   21
            Top             =   2835
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbNormalFontName 
            Height          =   555
            Left            =   1740
            OleObjectBlob   =   "frmLabels.frx":FF62
            TabIndex        =   23
            Top             =   3330
            Width           =   3615
         End
         Begin VB.TextBox txtAuthorInitials 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   1740
            TabIndex        =   15
            Top             =   690
            Width           =   3615
         End
         Begin mpControls3.SpinTextInternational spnNormalFontSize 
            Height          =   315
            Left            =   1755
            TabIndex        =   25
            Top             =   3855
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            MaxValue        =   37
            Value           =   12
         End
         Begin mpControls3.SpinTextInternational spnLeftIndent 
            Height          =   315
            Left            =   1755
            TabIndex        =   27
            Top             =   4380
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   5
            AppendSymbol    =   -1  'True
            Value           =   0.25
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin mpControls3.SpinTextInternational spnTopMargin 
            Height          =   315
            Left            =   1755
            TabIndex        =   29
            Top             =   4905
            Width           =   3615
            _ExtentX        =   6371
            _ExtentY        =   550
            IncrementValue  =   0.1
            MaxValue        =   5
            AppendSymbol    =   -1  'True
            Value           =   0.5
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin VB.Label lblDeliveryPhrase2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "&Confidential Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   420
            Left            =   0
            TabIndex        =   18
            Top             =   2040
            Width           =   1545
            WordWrap        =   -1  'True
         End
         Begin VB.Line Line5 
            BorderColor     =   &H00FFFFFF&
            X1              =   -150
            X2              =   5740
            Y1              =   5295
            Y2              =   5295
         End
         Begin VB.Label lblLeftIndent 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "L&eft Indent:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   1
            Left            =   60
            TabIndex        =   26
            Top             =   4410
            Width           =   1485
         End
         Begin VB.Label lblNormalFontSize 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Font &Size:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Index           =   0
            Left            =   60
            TabIndex        =   24
            Top             =   3885
            Width           =   1485
         End
         Begin VB.Label lblNormalFontName 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Font &Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   60
            TabIndex        =   22
            Top             =   3390
            Width           =   1485
         End
         Begin VB.Label lblTopMargin 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Top Margin:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   60
            TabIndex        =   28
            Top             =   4920
            Width           =   1485
         End
         Begin VB.Label lblClientMatter 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Client-Matter No.:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   60
            TabIndex        =   12
            Top             =   210
            Width           =   1485
         End
         Begin VB.Label lblDeliveryPhrase 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   " &Delivery Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   60
            TabIndex        =   16
            Top             =   1200
            Width           =   1485
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblOffice 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Office:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   60
            TabIndex        =   20
            Top             =   2880
            Width           =   1485
         End
         Begin VB.Label lblAuthorInitials 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " &Author Name/Initials:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   -330
            TabIndex        =   14
            Top             =   735
            Width           =   1875
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6390
            Left            =   -195
            Top             =   -1080
            Width           =   1860
         End
      End
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000005&
      X1              =   0
      X2              =   5445
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Line Line6 
      BorderColor     =   &H80000005&
      X1              =   0
      X2              =   5355
      Y1              =   0
      Y2              =   0
   End
End
Attribute VB_Name = "frmLabels"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private WithEvents m_oLabels As MPO.CLabel
Attribute m_oLabels.VB_VarHelpID = -1
Private m_bInitializing As Boolean
Private m_bCanceled As Boolean
Private m_oArray As XArrayObject.XArray

Property Get Cancelled() As Boolean
    Cancelled = m_bCanceled
End Property
Property Let Cancelled(bNew As Boolean)
    m_bCanceled = bNew
End Property

Private Sub btnAddressBooks_Click()
    GetContacts
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    Me.Cancelled = True
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    On Error GoTo ProcError
    '9.7.1 - #2715
    With Me.Labels
        If Val(Word.Application.Version) = 10 And _
        .Definition.HasGraphic And _
        Me.cmbOutputTo.BoundText = mpLabelOutput_Printer Then
            MsgBox "This feature is not available in Word XP." & _
            " Please choose another output location for this label type.", _
                   vbInformation, App.Title
            Me.cmbOutputTo.SetFocus
            Exit Sub
        End If
    End With

    Me.Hide
    DoEvents
    Me.Cancelled = False
    
    Finish
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub btnFinish_KeyDown(KeyCode As Integer, Shift As Integer)
    btnFinish_Click
End Sub

Private Sub btnFinish_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    btnFinish_Click
End Sub

Private Sub chkFullPage_GotFocus()
    OnControlGotFocus Me.chkFullPage
End Sub

Private Sub chkUSPS_GotFocus()
    OnControlGotFocus Me.chkUSPS
End Sub

Private Sub cmbBarCode_GotFocus()
    OnControlGotFocus Me.cmbBarCode
End Sub

Private Sub cmbBarCode_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbBarCode, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLabelTypes_GotFocus()
    OnControlGotFocus Me.cmbLabelTypes
End Sub

Private Sub cmbLabelTypes_ItemChange()
    Dim iRow As Integer
    Dim iCol As Integer
    Dim cDB As CDatabase
    Dim oDef As CLabelDef
    
    On Error GoTo ProcError
    
    '*********************
    Labels = g_oMPO.NewLabel
    Labels.LabelType = Me.cmbLabelTypes.BoundText
    Set oDef = Labels.Definition
    '**********************
    With oDef
        DrawPreviewGrid .NumberDown, .NumberAcross
        LoadRowColDropDown Me.cmbStartingRow, .NumberDown
        LoadRowColDropDown Me.cmbStartingColumn, .NumberAcross
        DisableOptionalControls
        Me.spnLeftIndent.value = .LeftIndent
        Me.spnTopMargin.value = .TopMargin
        Me.Caption = "Create Labels - " & .Name
        Me.cmbLabelTypes.ToolTipText = .Tooltip
    End With
    
    GetDefaults
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oLabels_BeforeOutput(bSuppressPageSetup As Boolean)
    '---9.7.1 - 4232
    bSuppressPageSetup = UCase(GetMacPacIni("Labels", "ShowPageSetupDialog")) = "FALSE"
End Sub
Private Sub spnLeftIndent_Validate(Cancel As Boolean)
    If Not Me.spnLeftIndent.IsValid Then _
        Cancel = True
End Sub

Private Sub spnNormalFontSize_Validate(Cancel As Boolean)
    If Not Me.spnNormalFontSize.IsValid Then _
        Cancel = True
End Sub

Private Sub spnTopMargin_Validate(Cancel As Boolean)
    If Not Me.spnTopMargin.IsValid Then _
        Cancel = True
End Sub

Private Sub cmbLabelTypes_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLabelTypes, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLabelTypes_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLabelTypes)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCM_ConnectionFailed(ByVal Message As String)

    MsgBox IIf(Message = "", "Could not connect to the client matter data source.  ", Message & vbCr & vbCr) _
            & "Instead of selecting client matter numbers from a list," & _
            " you can manually type the numbers instead.", _
            vbExclamation, App.Title
        
    With Me.cmbCM
        .RunConnected = False
        .ComboType = ComboType_TextOnly
    End With
    
End Sub

Private Sub cmbCM_GotFocus()
    OnControlGotFocus Me.cmbCM, "zzmpReline"
End Sub

Private Sub cmbCM_LostFocus()
    On Error GoTo ProcError
    Me.Labels.ClientMatterNumber = Me.cmbCM.value
    
    'save SQL for reuse
    Me.Labels.Document.SaveItem "CMFilterString", _
        "Labels", , Me.cmbCM.AdditionalClientFilter
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCM_Validate(Cancel As Boolean)
    Dim iIsValid As cmValidationStatus
    
    If g_bCM_AllowValidation Then
        If Me.cmbCM.RunConnected And Me.cmbCM.value <> Empty Then
            
            iIsValid = Me.cmbCM.Validity
            
            If iIsValid = cmValidationStatus_Invalid Then
                MsgBox "Invalid client matter number."
                Cancel = True
            End If
        End If
    End If
    
End Sub

Private Sub cmbNormalFontName_GotFocus()
    OnControlGotFocus Me.cmbNormalFontName
End Sub

Private Sub cmbOffice_GotFocus()
    OnControlGotFocus Me.cmbOffice
End Sub

Private Sub cmbOffice_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbOffice, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOutputTo_GotFocus()
    OnControlGotFocus Me.cmbOutputTo
End Sub

Private Sub cmbOutputTo_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbOutputTo, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOutputTo_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbOutputTo)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbStartingColumn_ItemChange()
    On Error GoTo ProcError
    Me.grdPreview.Col = (cmbStartingColumn.Text) - 1
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbStartingRow_ItemChange()
    On Error Resume Next
    Me.grdPreview.Row = (cmbStartingRow.Text) - 1
    Me.grdPreview.Col = (cmbStartingColumn.Text) - 1
End Sub

Private Sub SaveDefaults()
    On Error GoTo ProcError
    With Me.Labels
'       save default type
        SetUserIni "Labels", "Type", Me.cmbLabelTypes.BoundText
        SetUserIni "Labels_" & .LabelType, "FontName", Me.cmbNormalFontName
        SetUserIni "Labels_" & .LabelType, "Output", Me.cmbOutputTo.BoundText
        SetUserIni "Labels_" & .LabelType, "FontSize", Me.spnNormalFontSize.value
        SetUserIni "Labels_" & .LabelType, "LeftIndent", Me.spnLeftIndent.value
        SetUserIni "Labels_" & .LabelType, "TopMargin", Me.spnTopMargin.value
        SetUserIni "Labels_" & .LabelType, "Initials", Me.txtAuthorInitials.Text
        SetUserIni "Labels_" & .LabelType, "BarCodeFormat", Me.cmbBarCode.BoundText
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLabels.SaveDefaults"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub GetDefaults()
    Dim xFont As String
    Dim iOutput As Integer
    Dim vLabelType As Variant
    Dim sFontSize As Single
    Dim sLeftIndent As Single
    Dim sTopMargin As Single
    Dim xInitials As String
    Dim bIncludeBarCode As Boolean
    Dim xIncludeBarCode As String
    Dim xBarCode As String
    
    DoEvents
    
    On Error Resume Next
    With Me.Labels
        xFont = GetUserIni("Labels_" & .LabelType, "FontName")
        If xFont = "" Then
            xFont = GetUserIni("Labels", "FontName")
        End If
        iOutput = GetUserIni("Labels_" & .LabelType, "Output")
        If iOutput = 0 Then
            iOutput = GetUserIni("Labels", "Output")
        End If
        sFontSize = GetUserIniNumeric("Labels_" & .LabelType, "FontSize")
        If sFontSize = 0 Then
            sFontSize = GetUserIni("Labels", "FontSize")
        End If
        sLeftIndent = GetUserIniNumeric("Labels_" & .LabelType, "LeftIndent")
        If sLeftIndent = 0 Then
            sLeftIndent = .Definition.LeftIndent
        End If
        sTopMargin = GetUserIniNumeric("Labels_" & .LabelType, "TopMargin")
        If sTopMargin = 0 Then
            sTopMargin = .Definition.TopMargin
        End If
        If .Definition.AllowAuthorInitials Then
            xInitials = GetUserIni("Labels_" & .LabelType, "Initials")
            If xInitials = "" Then
                xInitials = GetUserIni("Labels", "Initials")
            End If
        Else
            xInitials = ""
        End If
        
        If .Definition.AllowBarCode Then
            xBarCode = GetUserIni("Labels_" & .LabelType, "BarCodeFormat")
            If xBarCode = "" Then
                xBarCode = GetUserIni("Labels", "BarCodeFormat")
            End If
        End If
        
        If xBarCode <> "" Then
            Me.cmbBarCode.BoundText = xBarCode
        Else
            Me.cmbBarCode.Bookmark = 0
        End If

        
    End With
    On Error GoTo ProcError
    Me.cmbOffice.BoundText = g_oMPO.Offices.Default.ID
    
'   assign default font
    Me.cmbNormalFontName.BoundText = xFont
    If Me.cmbNormalFontName.BoundText = Empty Then
'       invalid default font - select first item in list
        Me.cmbNormalFontName.SelectedItem = 0
    End If
    
'   assign output location
    Me.cmbOutputTo.BoundText = iOutput
    If Me.cmbOutputTo.BoundText = Empty Then
'       invalid output value - select first item in list
        Me.cmbOutputTo.SelectedItem = 0
    End If
        
    On Error Resume Next
'   assign font size
    With Me.spnNormalFontSize
        .value = 10
        If sFontSize Then
            .value = sFontSize
        End If
        DoEvents
    End With
        
'   assign left indent
    With Me.spnLeftIndent
        .value = 0
        .value = sLeftIndent
        DoEvents
    End With
    
'   assign Top Margin
    With Me.spnTopMargin
        .value = 0
        .value = sTopMargin
        DoEvents
    End With
    
'   assign initials
    Me.txtAuthorInitials = xInitials
    DoEvents
    
'   edit object
    With Me.Labels
        .LabelType = Me.cmbLabelTypes.BoundText
        .OutputTo = Me.cmbOutputTo.BoundText
        .AuthorInitials = Me.txtAuthorInitials.Text
        .LeftIndent = Me.spnLeftIndent.value
        .FontName = Me.cmbNormalFontName.BoundText
        .FontSize = Me.spnNormalFontSize.value
        .TopMargin = Me.spnTopMargin.value
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLabels.GetDefaults"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub Form_Activate()
    
'   set defaults from ini
    On Error GoTo ProcError

'   assign last label type
    Me.cmbLabelTypes.BoundText = GetUserIni("Labels", "Type")
    If Me.cmbLabelTypes.BoundText = Empty Then
'       invalid label type - select first item in list
        Me.cmbLabelTypes.SelectedItem = 0
    End If

    cmbLabelTypes_ItemChange

    Me.Refresh
    DoEvents
    Me.Initializing = False
'    Me.Timer1.Enabled = True
    Me.vsIndexTab1.CurrTab = 0
    Exit Sub
ProcError:
    Me.Initializing = False
    g_oError.Show Err, "Error Activating Label Form"
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        btnAddressBooks_Click
    End If
End Sub

Private Sub Form_Load()
    Dim oDoc As MPO.CDocument
    Dim xAddresses As String
    Dim xDPhrases As String
    
    On Error GoTo ProcError

    Me.Initializing = True
    Me.Cancelled = True

    g_oMPO.GetSelectedAddresses xAddresses, xDPhrases
    '9.7.1 - #4143
    Set oDoc = New MPO.CDocument
    Me.cmbCM.value = oDoc.ClientMatter
    
    If xAddresses = Empty Then  'load recipients
        xAddresses = oDoc.GetRecipients
    End If
    
    xAddresses = xCleanAddress(xAddresses) '9.9.1010 #5141  !gm
    
    'load cc & bccs - from CLetter only
    Me.txtAddresses = xAddMoreRecipients(xAddresses, True)
    Me.mlcDeliveryPhrase.Text = xDPhrases

'   assign preview array
    Set m_oArray = g_oMPO.NewXArray
    Me.grdPreview.Array = m_oArray

'   assign row source to label types
    Me.cmbLabelTypes.Array = g_oMPO.db.LabelDefs.ListSource

'   assign row source
    With g_oMPO.Lists
        Me.cmbLabelTypes.Array = .Item("Labels").ListItems.Source
        Me.cmbNormalFontName.Array = .Item("Fonts").ListItems.Source
        ' Me.mlcDeliveryPhrase.List = .Item("DPhrases").ListItems.Source '9.7.1 #4024
        Me.cmbOutputTo.Array = .Item("LabelsOutput").ListItems.Source
        Me.cmbBarCode.Array = xarStringToxArray("0|(none)|1|Above Address|2|Below Address", 2, "|")
    End With

'---office list
    Me.cmbOffice.Array = g_oMPO.Offices.ListSource

'   resize combos
    ResizeTDBCombo Me.cmbOffice, 8
    ResizeTDBCombo Me.cmbNormalFontName, 8
    ResizeTDBCombo Me.cmbOutputTo, 4
    ResizeTDBCombo Me.cmbLabelTypes, 5
    ResizeTDBCombo Me.cmbBarCode, 3

'   update client\matter label
    Me.lblClientMatter.Caption = GetMacPacIni _
                                 ("Labels", "ClientMatterDBoxCaption")

'   set Client Matter Control properties
    With Me.cmbCM
        .RunConnectedToolTip = g_xCM_RunConnectedTooltip
        .RunDisconnectedToolTip = g_xCM_RunDisconnectedTooltip
        .RunConnected = g_bCM_RunConnected
        If Not .RunConnected Then
            .ComboType = ComboType_TextOnly
        Else
            .SourceClass = g_xCM_Backend
            If g_bCM_SearchByMatter Then .SearchMode = 1 '***9.7.1 - #4186
            .AllowUserToClearFilters = g_bCM_AllowUserToClearFilters
            .ShowLoadMessage = g_bCM_ShowLoadMsg
            .Column1Heading = g_xCM_Column1Heading
            .Column2Heading = g_xCM_Column2Heading
            .Separator = g_xCM_Separator
        End If
    End With

'   show AddressBooks button
    Me.btnAddressBooks.Visible = g_bShowAddressBooks
    
'   show USPS Standards checkbox
    Me.chkUSPS.Visible = g_bUSPSStandards

'   set spinners to display Word measurement unit
    With Word.Options
        Me.spnLeftIndent.DisplayUnit = .MeasurementUnit
        Me.spnTopMargin.DisplayUnit = .MeasurementUnit
    End With
    
    'mpbase2.MoveToLastPosition Me

    Exit Sub
ProcError:
    g_oError.Show Err, "Error Loading Labels Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'mpbase2.'SetLastPosition Me
    Set frmLabels = Nothing
    Set m_oLabels = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Unloading Labels Form"
    Exit Sub
End Sub

'******************************************
'---properties
'******************************************
Public Property Get Labels() As MPO.CLabel
    Set Labels = m_oLabels
End Property

Public Property Let Labels(oNew As MPO.CLabel)
    Set m_oLabels = oNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property


'******************************************
'---methods
'******************************************
Private Function AddressBook(iSelectionList As Integer) As Boolean
    Dim i As Integer
    Dim xTemp As String
    Dim xSep As String
    
    On Error GoTo ProcError
    If g_appCI Is Nothing Then
        Set g_appCI = New mpCI.Application
        With g_appCI.Options
            .IncludeEAddress = False
            .IncludeFax = False
            .IncludePhone = False
        End With
    End If
    
    g_appCI.Contacts.Retrieve iSelectionList
    
    DoEvents
        
    xSep = vbCrLf & vbCrLf
    
    If g_appCI.Contacts.Count Then
        For i = 1 To g_appCI.Contacts.Count
            Select Case g_appCI.Contacts(i).ContactType
                Case ciContactType_To
                    xTemp = xTemp & g_appCI.Contacts(i).Detail & xSep
            End Select
            xTemp = xTrimTrailingChrs(xTemp, vbCr & vbCr & vbCr)
            xTemp = xTrimTrailingChrs(xTemp, Chr(10))
            xTemp = xTrimTrailingChrs(xTemp, vbCr)
            xTemp = xTrimTrailingChrs(xTemp, Chr(10))
            xTemp = xTrimTrailingChrs(xTemp, vbCr)
        Next i
        
        xTemp = xTrimTrailingChrs(xTemp, vbCrLf & vbCrLf)
     
        If Me.txtAddresses <> "" Then
            Me.txtAddresses = Me.txtAddresses & xSep & xTemp
        Else
            Me.txtAddresses = xTemp
        End If
    End If
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLabels.AddressBook"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Sub DisableOptionalControls(Optional xPs As XArray)
    Dim lNumEntries As Long
    
    On Error GoTo ProcError
    With Me
        With g_oMPO.db.LabelDefs.Item(Me.cmbLabelTypes.BoundText)
            Me.mlcDeliveryPhrase.Enabled = .AllowDPhrases
            '9.7.1 #4024
            If .DPhrase1Caption <> "" Then _
                Me.lblDeliveryPhrase.Caption = .DPhrase1Caption
            Me.mlcDeliveryPhrase2.Visible = .AllowDPhrases And .SplitDPhrases
            Me.lblDeliveryPhrase2.Visible = .AllowDPhrases And .SplitDPhrases
            If .SplitDPhrases Then
                mlcDeliveryPhrase.List = g_oMPO.db.Lists("DPhrases1").ListItems.Source
                mlcDeliveryPhrase2.List = g_oMPO.db.Lists("DPhrases2").ListItems.Source
                If .DPhrase2Caption <> "" Then _
                    Me.lblDeliveryPhrase2.Caption = .DPhrase2Caption
            Else
                mlcDeliveryPhrase2.Text = ""
                mlcDeliveryPhrase.List = g_oMPO.db.Lists("DPhrases").ListItems.Source
            End If
            '---
            If .AllowBarCode Then
                Me.lblBarCode.Enabled = True
                Me.cmbBarCode.Enabled = True
                'GLOG 4969 - 9.8.1030
                Me.lblBarCode.Visible = True
                Me.cmbBarCode.Visible = True
            Else
                Me.lblBarCode.Enabled = False
                Me.cmbBarCode.Enabled = False
                'GLOG 4969 - 9.8.1030
                Me.lblBarCode.Visible = False
                Me.cmbBarCode.Visible = False
            End If
            Me.cmbOffice.Enabled = .AllowOfficeAddress
            Me.cmbCM.Enabled = .AllowBillingNumber
            Me.cmbCM.TabStop = .AllowBillingNumber
            Me.txtAuthorInitials.Enabled = .AllowAuthorInitials
            Me.lblDeliveryPhrase.Enabled = .AllowDPhrases
            Me.lblOffice.Enabled = .AllowOfficeAddress
            Me.lblClientMatter.Enabled = .AllowBillingNumber
            Me.lblAuthorInitials.Enabled = .AllowAuthorInitials
        End With

        If .mlcDeliveryPhrase.Enabled = False Then
            .mlcDeliveryPhrase.Text = ""
        End If
        
        If .cmbCM.Enabled = False Then
            .txtAuthorInitials.Text = ""
        End If
        
        lNumEntries = lCountChrs(Me.txtAddresses, vbCrLf & vbCrLf) + 1
        With Labels.Definition
            Me.chkFullPage.Enabled = ((lNumEntries = 1) And _
                (.NumberAcross + .NumberDown > 2))
        End With
        If Not Me.chkFullPage.Enabled Then
            Me.chkFullPage.value = 0
        End If
    End With
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLabels.DisableOptionalControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

'*****************************************
'---internal procedures
'*****************************************

Private Sub DrawPreviewGrid(iRows As Integer, iCols As Integer)
'Private Sub DrawPreviewGrid(grdPreview As TDBGrid, iRows As Integer, iCols As Integer)
    Dim sOffset As Single

    On Error GoTo ProcError
'---refresh array & assign (allows cell selection)
    Dim xAR As XArrayObject.XArray
    Set xAR = g_oMPO.NewXArray
    xAR.ReDim 1, iRows, 1, iCols
    grdPreview.Array = xAR
    grdPreview.Rebind
    grdPreview.Bookmark = 1


'---clear out existing columns

    While grdPreview.Columns.Count <> 0
        grdPreview.Columns.Remove 0
    Wend

'---add number of cols corresponding to label grid

'*** APEX Migration Utility Code Change ***
'    Dim c As TrueDBGrid50.Column
    Dim c As TrueDBGrid60.Column
    Dim i As Integer
    Dim j As Integer
    
    For i = 0 To iCols - 1
    
    Set c = grdPreview.Columns.Add(i)
'---Initialize the new column
        With c
            .Width = grdPreview.Width / iCols
            .AllowSizing = False
            .Visible = True        ' Make it visible
            .AllowFocus = True
        End With
    Next i
'---set row height according to label count
'---you may have to tweak offset if you change size of control
'---if too close the grid rows scroll up or down
    
    If iRows < 6 Then sOffset = 10
    grdPreview.RowHeight = (grdPreview.Height / iRows) - sOffset
    If iRows = 7 Then
        grdPreview.Height = (iRows * grdPreview.RowHeight) + 8
    End If
    
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLabels.DrawPreviewGrid"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub grdPreview_GotFocus()
    OnControlGotFocus Me.grdPreview
End Sub

Private Sub grdPreview_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    On Error GoTo ProcError
    With Me.grdPreview
        Me.cmbStartingColumn.Text = .Col + 1
        Me.cmbStartingRow.Text = .Row + 1
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub LoadRowColDropDown(cmbCMB As TDBCombo, iMax As Integer)
    Dim i As Integer
    Dim oArray As XArray
    
    On Error GoTo ProcError
    Set oArray = g_oMPO.NewXArray
    oArray.ReDim 1, iMax
    For i = 1 To iMax
        oArray(i) = i
    Next i
    With cmbCMB
        .Array = oArray
        .Rebind
    End With
    ResizeTDBCombo cmbCMB, iMax
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, "frmLabels.LoadRowColDropDown"
    Exit Sub
End Sub

Private Sub m_oLabels_BeforeLabelsPrint()
    Me.FormatLabelTable
End Sub

Private Sub mlcDeliveryPhrase_BeforeItemAdded(xItem As String)
    On Error GoTo ProcError
    xItem = mpbase2.AppendToDPhrase(xItem)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_GotFocus()
    OnControlGotFocus Me.mlcDeliveryPhrase
End Sub

Private Sub mlcDeliveryPhrase_LostFocus()
    Me.mlcDeliveryPhrase.ListVisible = False
End Sub

Private Sub spnLeftIndent_GotFocus()
    OnControlGotFocus Me.spnLeftIndent
End Sub
Private Sub mlcDeliveryPhrase2_GotFocus() '9.7.1 #4024
    OnControlGotFocus Me.mlcDeliveryPhrase2
End Sub

Private Sub spnNormalFontSize_GotFocus()
    OnControlGotFocus Me.spnNormalFontSize
End Sub

Private Sub spnTopMargin_GotFocus()
    OnControlGotFocus Me.spnTopMargin
End Sub

Private Sub txtAddresses_Change()
    On Error GoTo ProcError
    UpdateLabelCaption txtAddresses, Me.lblAddresses, "Label &Text"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtAddresses_LostFocus()
    Me.txtAddresses = xCleanAddress(Me.txtAddresses) '9.9.1010 #5141  !gm
End Sub
Private Sub UpdateLabelCaption(txtB As TextBox, lblL As Label, xCaptionRoot As String)
    Dim lNumEntries As Long
    
    On Error GoTo ProcError
    If txtB.Text <> "" Then
        lNumEntries = lCountChrs(txtB.Text, vbCrLf & vbCrLf) + 1
         If lNumEntries = 1 Then
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (1 address)"
            If Not Me.Initializing Then
                With Labels.Definition
                    Me.chkFullPage.Enabled = (.NumberAcross + .NumberDown > 2)
                End With
                If Not Me.chkFullPage.Enabled Then
                    Me.chkFullPage.value = 0
                End If
            End If
        Else
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (" & lNumEntries & " addresses)"
            Me.chkFullPage.value = 0
            Me.chkFullPage.Enabled = 0
        End If
    Else
        lblL = " " & xCaptionRoot
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLabels.UpdateLabelCaption"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Public Sub Finish()
'cleans up doc after all user input is complete
    Dim rngP As Range

    On Error GoTo ProcError
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    SaveDefaults
    
'---assign all properties
    With Me.Labels
        .LabelType = Me.cmbLabelTypes.BoundText
        .USPSStandards = Me.chkUSPS
        .Addresses = Me.txtAddresses
        .OutputTo = Me.cmbOutputTo.BoundText
        
        If Me.cmbStartingColumn.BoundText = Empty Then
            .StartCol = 1
        Else
            .StartCol = Me.cmbStartingColumn
        End If
        
        If Me.cmbStartingRow.BoundText = Empty Then
            .StartRow = 1
        Else
            .StartRow = Me.cmbStartingRow
        End If
        
        .FullPage = Me.chkFullPage
        .IncludeBarCode = Me.cmbBarCode.Bookmark > 0
        .DeliveryPhrase = Me.mlcDeliveryPhrase.Text
        If .Definition.SplitDPhrases Then _
            .DeliveryPhrase2 = Me.mlcDeliveryPhrase2.Text '9.7.1 #4024
        If .Definition.AllowBillingNumber Then _
            .ClientMatterNumber = Me.cmbCM.value
        If .Definition.AllowAuthorInitials Then _
            .AuthorInitials = Me.txtAuthorInitials
        .Office = Me.cmbOffice.BoundText
        .FontName = Me.cmbNormalFontName.Text
        .FontSize = Me.spnNormalFontSize.value
        .LeftIndent = Me.spnLeftIndent.value
        .TopMargin = Me.spnTopMargin.value
        .BarCodePosition = Me.cmbBarCode.BoundText
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLabels.Finish"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub txtAddresses_DblClick()
    GetContacts
End Sub

Private Sub txtAddresses_GotFocus()
    OnControlGotFocus Me.txtAddresses
End Sub

Private Sub txtAuthorInitials_GotFocus()
    OnControlGotFocus Me.txtAuthorInitials
End Sub

Private Sub vsIndexTab1_Click()
    DoEvents
    On Error Resume Next
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            Me.txtAddresses.SetFocus
        Case 1
            If Me.cmbCM.Enabled Then
                Me.cmbCM.SetFocus
            ElseIf Me.txtAuthorInitials.Enabled Then
                Me.txtAuthorInitials.SetFocus
            ElseIf Me.mlcDeliveryPhrase.Enabled Then
                Me.mlcDeliveryPhrase.SetFocus
            Else
                Me.cmbNormalFontName.SetFocus
            End If
    End Select
End Sub

Private Sub GetContacts()
'   get contacts - place in text box
    Dim vTo As Variant
    Static bCIActive As Boolean
    Dim xSQL As String
    Dim oContacts As Object
    
    On Error GoTo ProcError
   
    If Not g_bShowAddressBooks Then Exit Sub
    
    If bCIActive Then Exit Sub
    bCIActive = True
    
    g_oMPO.GetLetterRecipients vTo, , , , , , oContacts
                                
    If vTo <> "" Then
        If Me.txtAddresses <> "" Then
            Me.txtAddresses = xTrimTrailingChrs(Me.txtAddresses, vbCrLf) & _
                                vbCrLf & vbCrLf & vTo
        Else
            Me.txtAddresses = vTo
        End If
    End If
    
'   filter client/matter based on contacts retrieved
    If g_bCM_RunConnected And g_bCM_AllowFilter Then
        If oContacts.Count Then
            '---9.6.1
            Dim l_CIVersion As Long
            l_CIVersion = g_oMPO.Contacts.CIVersion
            
            If Me.cmbCM.AdditionalClientFilter <> "" Then
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter(oContacts)
                Else
                    xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                               xGetClientMatterFilter2X(oContacts)
                End If
            Else
                If l_CIVersion = mpCIVersion_1 Then
                    xSQL = xGetClientMatterFilter(oContacts)
                Else
                    xSQL = xGetClientMatterFilter2X(oContacts)
                End If
            End If
            
            If xSQL <> "" Then
                Me.cmbCM.AdditionalClientFilter = xSQL
                Me.cmbCM.AdditionalMatterFilter = xSQL
                Me.cmbCM.Refresh
            End If
            '---end 9.6.1
        End If
    End If
    
    Me.SetFocus
    bCIActive = False
    Exit Sub
ProcError:
    bCIActive = False
    g_oError.Show Err
    Exit Sub
End Sub

Public Sub FormatLabelTable()
    Dim t As Word.Table
    Dim s As Word.Section
    Dim r As Word.Row
    
'   top align all cells
    Select Case Me.Labels.OutputTo
        Case mpLabelOutput_StartofDocument
            Set s = ActiveDocument.Sections.First
        Case mpLabelOutput_EndOfDocument
            Set s = ActiveDocument.Sections.Last
        Case mpLabelOutput_NewDocument
            Set s = ActiveDocument.Sections.First
        Case mpLabelOutput_Printer
            Set s = ActiveDocument.Sections.First
    End Select

    For Each t In s.Range.Tables
        For Each r In t.Rows
            DoEvents
            r.Cells.VerticalAlignment = Me.Labels.Definition.VerticalAlignment
        Next r
    Next t
    
End Sub

Private Sub grdPreview_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error Resume Next
    With Me.grdPreview
        .Col = .ColContaining(x)
        .Row = .RowContaining(y)
    End With
End Sub
