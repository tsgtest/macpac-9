Attribute VB_Name = "Global"
Public Const mpColorNonFocus As Long = &HFFFFFF
Public Const mpColorFocus As Long = &HFFFF&

Option Explicit
Public Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Public Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Public Function BoolToInt(bBool As Boolean)
    If bBool Then
        BoolToInt = 1
    Else
        BoolToInt = 0
    End If
End Function

Public Function xNullToString(vValue As Variant) As String
    If Not IsNull(vValue) Then _
        xNullToString = CStr(vValue)
End Function


Public Function xSubstitute(ByVal xString As String, _
                     xSearch As String, _
                     xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Integer
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While iSeachPos
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
        iSeachPos = InStr(UCase(xString), _
                          UCase(xSearch))
    
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function

'*** APEX Migration Utility Code Change ***
'Public Sub ResizeTDBCombo(tdbCBX As TrueDBList50.TDBCombo, Optional iMaxRows As Integer)
Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xarP As XArray
    Dim iRows As Integer
    On Error Resume Next
    With tdbCBX
        Set xarP = tdbCBX.Array
        iRows = Min(xarP.Count(1), CDbl(iMaxRows))
        .DropdownHeight = ((iRows) * .RowHeight)
    End With
End Sub


