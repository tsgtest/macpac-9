Attribute VB_Name = "mdlFunctions"
Option Explicit

Public Function bValidatePeople(Optional bAttysOnly As Boolean = False) As Boolean

'   function will return true if there is
'   at least one Author/Attorney in
'   the database

    Dim xMsg As String
    Dim iAnswer As VbMsgBoxResult
    Dim m_oPersonsForm As MPO.CPersonsForm
    Dim bTemp As Boolean
    
'   initialize vars
    Set m_oPersonsForm = New MPO.CPersonsForm
    bTemp = False
    
    If bAttysOnly Then
        xMsg = "You must have at least one Attorney in your database for this function" & _
               " to be available." & vbCr & vbCr & "Would you like to add one now?"
    Else
        xMsg = "You must have at least one Author in your database for MacPac" & _
               "to function." & vbCr & vbCr & "Would you like to add one now?"
    End If
    
'   loop
    Do
        If bAttysOnly Then
            If g_oMPO.Attorneys.Count Then
                bTemp = True
            End If
        Else
            If g_oMPO.People.Count Then
                bTemp = True
            End If
        End If
        
        If bTemp Then Exit Do
        
        iAnswer = MsgBox(xMsg, vbExclamation + vbYesNo, App.Title)
        
        If iAnswer = vbYes Then
'           bring up 'add person' dialog
            m_oPersonsForm.AddPerson
'           refresh People and Attorneys
            g_oMPO.People.Refresh
            g_oMPO.Attorneys.Refresh
        Else
            Exit Do
        End If
        
    Loop Until bTemp = True
        
    bValidatePeople = bTemp
    
    Set m_oPersonsForm = Nothing

End Function

Public Function xOfficeAddress(oOffice As COffice, _
                               iFormat As Long, _
                               Optional bResetForDisplay As Boolean = False) As String
    '---9.7.1 - 4030
    On Error Resume Next

    Dim oFormat As COfficeAddressFormat

    Set oFormat = g_oMPO.DB.OfficeAddressFormats(iFormat)

    If Not oFormat Is Nothing Then
        Dim xTemp As String
        xTemp = oOffice.Address(oFormat)
        If bResetForDisplay Then
            If InStr(xTemp, vbCrLf) = 0 And InStr(xTemp, vbCr) > 0 Then
                xTemp = xSubstitute(xTemp, vbCr, vbCrLf)
            End If
        End If
        xOfficeAddress = xTemp
    End If

End Function

Public Function xReformatAddress(xAddress As String, _
                                 iFormat As Long) As String
'this function reformats address string
'per supplied office address format

    Dim xTemp As String
    
    '---9.7.1 - 4030
    On Error Resume Next

    Dim oFormat As COfficeAddressFormat

    xTemp = xAddress
    
    Set oFormat = g_oMPO.DB.OfficeAddressFormats(iFormat)

    If Not oFormat Is Nothing Then
        xTemp = xSubstitute(xTemp, vbCrLf, oFormat.Separator)
        xTemp = mpbase2.xTrimTrailingChrs(xTemp, oFormat.Separator)
    End If

    xReformatAddress = xTemp

End Function

'9.9.1010 #5141
Public Function xCleanAddress(ByVal xAddress As String)
'!gm  For Envelopes and Labels: Clean address lines that are just spaces/tabs
    xAddress = Replace(xAddress, Chr(160), Chr(32))     'non-breaking space -> regular space
    xAddress = Replace(xAddress, Chr(9), Chr(32))   'tab -> space
    Do While Replace(xAddress, Chr(32) & Chr(32) & vbCrLf, Chr(32) & vbCrLf) <> xAddress
        xAddress = Replace(xAddress, Chr(32) & Chr(32) & vbCrLf, Chr(32) & vbCrLf)  'two spaces before break -> one space before break
    Loop                                                                            '(repeat until reduced to one space before break)
    xAddress = Replace(xAddress, vbCrLf & Chr(32) & vbCrLf, vbCrLf & vbCrLf)    'remove leftover space between two breaks
    xCleanAddress = xAddress
End Function

'9.7.1 #4143
'9.7.2010 #4720
Public Function xAddMoreRecipients(xAddresses As String, bIsLabel As Boolean) As String
    Dim xAddedAddresses As String
    Dim xTemp As String
    Dim oDoc As MPO.CDocument
    Dim bPromptForCC As Boolean
    Dim bPromptForBCC As Boolean
    Dim iAns As VbMsgBoxResult
    Dim xMsg As String
    Dim xLabelEnv As String
    
    On Error GoTo ProcError
    
    Set oDoc = New MPO.CDocument
    
    xAddedAddresses = xAddresses
    
    If oDoc.CCFromCI <> Empty Or oDoc.BCCFromCI <> Empty Then
    
        On Error Resume Next
        
        xTemp = mpbase2.GetMacPacIni(IIf(bIsLabel, "Labels", "Envelopes"), "PromptForCC")
        bPromptForCC = (UCase(xTemp) = "TRUE") And (oDoc.CCFromCI <> Empty)
        
        xTemp = mpbase2.GetMacPacIni(IIf(bIsLabel, "Labels", "Envelopes"), "PromptForBCC")
        bPromptForBCC = (UCase(xTemp) = "TRUE") And (oDoc.BCCFromCI <> Empty)
        
        On Error GoTo ProcError
        
        xLabelEnv = IIf(bIsLabel, "labels", "envelopes")
        
        If bPromptForCC And bPromptForBCC Then
            If xAddedAddresses <> "" Then
                xMsg = "In addition to the recipients, would you like to create " & xLabelEnv & " for the cc and bcc names?"
            Else
                xMsg = "Would you like to create " & xLabelEnv & " for the cc and bcc names?"
            End If
        ElseIf bPromptForCC Then
            If xAddedAddresses <> "" Then
                xMsg = "In addition to the recipients, would you like to create " & xLabelEnv & " for the cc names?"
            Else
                xMsg = "Would you like to create " & xLabelEnv & " for the cc names?"
            End If
        ElseIf bPromptForBCC Then
            If xAddedAddresses <> "" Then
                xMsg = "In addition to the recipients, would you like to create " & xLabelEnv & " for the bcc names?"
            Else
                xMsg = "Would you like to create " & xLabelEnv & " for the bcc names?"
            End If
        End If
        
        If xMsg <> Empty Then   'there is at least one prompt
            iAns = MsgBox(xMsg, vbInformation + vbYesNo, App.Title)
            If iAns = vbYes Then
                xAddedAddresses = IIf(xAddresses <> "", _
                                 xAddresses & vbCrLf & vbCrLf & IIf(bPromptForCC, oDoc.CCFromCIFullDetail & vbCrLf & vbCrLf, "") & IIf(bPromptForBCC, oDoc.BCCFromCIFullDetail, ""), _
                                 IIf(bPromptForCC, oDoc.CCFromCIFullDetail & vbCrLf & vbCrLf, "") & IIf(bPromptForBCC, oDoc.BCCFromCIFullDetail, ""))
            End If
        Else
            xAddedAddresses = IIf(xAddresses <> "", _
                             xAddresses & vbCrLf & vbCrLf & IIf(oDoc.CCFromCI <> Empty, oDoc.CCFromCIFullDetail & vbCrLf & vbCrLf, "") & IIf(oDoc.BCCFromCI <> Empty, oDoc.BCCFromCIFullDetail, ""), _
                             IIf(oDoc.CCFromCI <> Empty, oDoc.CCFromCIFullDetail & vbCrLf & vbCrLf, "") & IIf(oDoc.BCCFromCI <> Empty, oDoc.BCCFromCIFullDetail, ""))
        End If
    End If
    
    xAddMoreRecipients = mpbase2.xTrimTrailingChrs(xAddedAddresses, vbCrLf & vbCrLf)
    
    Exit Function
ProcError:
    g_oError.Show Err, "MacPac90.mdlFunctions.xAddMoreRecipients"
    Exit Function
End Function
Public Sub UpdatePleadingBorderSpecial()
    Dim xID As Long
    Dim oDoc As MPO.CDocument
    Dim oCaps As MPO.CPleadingCaptions
    
    On Error Resume Next
'---get caption ID
    Set oDoc = New MPO.CDocument
    xID = oDoc.GetVar("PleadingCaption_1_ID")
    If xID = 0 Then
        MsgBox "No valid MacPac pleading caption exists in this document.", vbInformation, App.Title
        Exit Sub
    End If
    On Error GoTo ProcError
    EchoOff
    Set oCaps = g_oMPO.NewPleadingCaptions
    oCaps.LoadValues
    
    If oCaps.Count > 0 Then
        oCaps(1).UpdateSpecialCharacterRightBorder oCaps(1)
    End If
    EchoOn
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err, "MacPac90.mdlFunctions.UpdatePleadingBorderSpecial"
    Exit Sub
End Sub
