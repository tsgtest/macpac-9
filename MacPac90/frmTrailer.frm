VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmTrailer 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert Doc ID"
   ClientHeight    =   3300
   ClientLeft      =   3825
   ClientTop       =   2055
   ClientWidth     =   5160
   Icon            =   "frmTrailer.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   5160
   ShowInTaskbar   =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CheckBox chkReplace 
      Caption         =   "&Replace Existing Trailer"
      Height          =   300
      Left            =   1860
      TabIndex        =   5
      Top             =   2340
      Value           =   1  'Checked
      Width           =   2055
   End
   Begin TrueDBList60.TDBList cmbLocation 
      Height          =   2025
      Left            =   1815
      OleObjectBlob   =   "frmTrailer.frx":000C
      TabIndex        =   4
      Top             =   210
      Width           =   3255
   End
   Begin VB.CommandButton btnRemove 
      Caption         =   "&Remove"
      Height          =   400
      Left            =   1860
      TabIndex        =   1
      Top             =   2805
      Width           =   1000
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   2955
      TabIndex        =   2
      Top             =   2805
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4050
      TabIndex        =   3
      Top             =   2805
      Width           =   1000
   End
   Begin VB.Label lblLocation 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Location:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   60
      TabIndex        =   0
      Top             =   225
      Width           =   1455
   End
   Begin VB.Shape Shape3 
      BackColor       =   &H80000003&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00800080&
      Height          =   3915
      Left            =   -30
      Top             =   -60
      Width           =   1695
   End
End
Attribute VB_Name = "frmTrailer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_oTrailer As MPO.CTrailer
Private trailerNew As MPO.CTrailer
Private Const mpVersionSep As String = ":"
Private Const mpDraftSep As String = ": "
Private Const mpDateTimeSep As String = " "
'****************************************************************
'Properties
'****************************************************************


Public Property Get Trailer() As MPO.CTrailer
    Set Trailer = m_oTrailer
End Property

Public Property Let Trailer(trailerNew As MPO.CTrailer)
    Set m_oTrailer = trailerNew
End Property

Private Sub btnFinish_Click()
    Me.Hide
    With m_oTrailer
        .Location = Me.cmbLocation.BoundText
        .DateStamp = Format(Now, "MM/dd/yy")
        .TimeStamp = Format(Now, "hh:mm am/pm")
        .Draft = ""
        .DocID = Word.ActiveDocument.FullName & vbCr
        .Version = "V3"
        .Insert
    End With
    Unload Me
End Sub

Private Sub btnRemove_Click()
    Trailer.Delete
End Sub

Private Sub Form_Activate()
    cmbLocation.BoundText = 0
    Me.cmbLocation.BoundText = mpTrailerLocation_Footer
End Sub

Private Sub Form_Load()
    Dim oList As CList
    
    Set m_oTrailer = New MPO.CTrailer
    
'---initialize lists
    Set oList = New CList
    With oList
        .IsDBList = True
        .Name = "Trailers"
        .FilterValue = ""
        .Refresh
        cmbLocation.Array = .ListItems.Source
    End With
'    ResizeTDBCombo Me.cmbLocation, 6

    Me.cmbLocation.BoundText = m_oTrailer.Location
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oTrailer = Nothing
End Sub
