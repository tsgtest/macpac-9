VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Begin VB.Form frmPleadingCoverPage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Pleading Cover Page / Litigation Back"
   ClientHeight    =   6444
   ClientLeft      =   3072
   ClientTop       =   828
   ClientWidth     =   5484
   Icon            =   "frmPleadingCoverPage.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6444
   ScaleWidth      =   5484
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnMore 
      Caption         =   "M&ore..."
      Height          =   380
      Left            =   2055
      TabIndex        =   20
      Top             =   6015
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   22
      Top             =   6000
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4005
      TabIndex        =   21
      Top             =   6000
      Width           =   690
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   6480
      Left            =   -45
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   -75
      Width           =   5925
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Parties|Detai&ls"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   400
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   1
         Left            =   30
         Top             =   45
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   6060
         Left            =   6420
         TabIndex        =   24
         Top             =   12
         Width           =   5904
         Begin VB.CheckBox chkCopyCounselTable 
            Caption         =   "Cop&y Pleading Counsel Table"
            Height          =   405
            Left            =   1740
            TabIndex        =   18
            Top             =   2467
            Width           =   2685
         End
         Begin VB.CheckBox chkCopyCaptionTable 
            Caption         =   "&Copy Pleading Caption Table"
            Height          =   405
            Left            =   1740
            TabIndex        =   17
            Top             =   1950
            Width           =   2685
         End
         Begin VB.TextBox txtCaseTitle 
            Appearance      =   0  'Flat
            Height          =   660
            Left            =   1740
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   16
            Top             =   1125
            Width           =   3615
         End
         Begin TrueDBGrid60.TDBGrid grdCaseNumbers 
            Height          =   780
            Left            =   1740
            OleObjectBlob   =   "frmPleadingCoverPage.frx":058A
            TabIndex        =   14
            ToolTipText     =   "Use CTL/Enter to create multiline case numbers, Up/Down arrows to scroll individual case numbers"
            Top             =   150
            Width           =   3615
         End
         Begin VB.Line Line3 
            X1              =   3045
            X2              =   3045
            Y1              =   135
            Y2              =   915
         End
         Begin VB.Label lblDocumentTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Document Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -480
            TabIndex        =   15
            Top             =   1125
            Width           =   2010
         End
         Begin VB.Label lblCaseNumber 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Case &Noxxxx:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -480
            TabIndex        =   13
            Top             =   150
            Width           =   2010
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   1
            X1              =   -48
            X2              =   5952
            Y1              =   6012
            Y2              =   6012
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H80000003&
            Height          =   6370
            Left            =   -30
            Top             =   -345
            Width           =   1695
         End
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   6060
         Left            =   12
         TabIndex        =   23
         Top             =   12
         Width           =   5904
         Begin VB.TextBox txtParty3Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1725
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   12
            Top             =   5055
            Visible         =   0   'False
            Width           =   3615
         End
         Begin VB.TextBox txtParty1Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1725
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   6
            Top             =   2535
            Width           =   3615
         End
         Begin VB.TextBox txtCourtTitle 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   975
            Left            =   1725
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   3
            Top             =   840
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbParty2Title 
            Height          =   300
            Left            =   1725
            OleObjectBlob   =   "frmPleadingCoverPage.frx":2C92
            TabIndex        =   8
            Top             =   3390
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbParty1Title 
            Height          =   450
            Left            =   1725
            OleObjectBlob   =   "frmPleadingCoverPage.frx":4EB4
            TabIndex        =   5
            Top             =   2220
            Width           =   3630
         End
         Begin TrueDBList60.TDBCombo cmbParty3Title 
            Height          =   300
            Left            =   1725
            OleObjectBlob   =   "frmPleadingCoverPage.frx":70D6
            TabIndex        =   11
            Top             =   4740
            Visible         =   0   'False
            Width           =   3630
         End
         Begin VB.TextBox txtParty2Name 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1725
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   9
            Top             =   3705
            Width           =   3615
         End
         Begin TrueDBList60.TDBCombo cmbAuthor 
            Height          =   600
            Left            =   1725
            OleObjectBlob   =   "frmPleadingCoverPage.frx":92F8
            TabIndex        =   1
            Top             =   90
            Width           =   3615
         End
         Begin VB.Label lblAuthor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   45
            TabIndex        =   0
            Top             =   135
            Width           =   1455
         End
         Begin VB.Label lblParty2Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &2:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   7
            Top             =   3420
            Width           =   1455
         End
         Begin VB.Label lblCourtTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Court &Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Left            =   45
            TabIndex        =   2
            Top             =   900
            Width           =   1455
         End
         Begin VB.Label lblParty1Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &1:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   4
            Top             =   2250
            Width           =   1455
         End
         Begin VB.Label lblVersus1 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "v."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   1305
            TabIndex        =   27
            Top             =   3030
            Visible         =   0   'False
            Width           =   195
         End
         Begin VB.Label lblParty3Name 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &3:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   10
            Top             =   4740
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblVersus2 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "and"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   1200
            TabIndex        =   26
            Top             =   4260
            Visible         =   0   'False
            Width           =   300
         End
         Begin VB.Label lblVersus 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "v."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   210
            Left            =   3360
            TabIndex        =   25
            Top             =   3420
            Width           =   135
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   5
            X1              =   -36
            X2              =   5409
            Y1              =   6012
            Y2              =   6012
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H80000003&
            Height          =   6340
            Left            =   -30
            Top             =   -300
            Width           =   1695
         End
      End
   End
   Begin VB.Menu mnuOther 
      Caption         =   "&Other Functions"
      Visible         =   0   'False
      Begin VB.Menu mnuOther_Counsels 
         Caption         =   "Co-Co&unsel..."
      End
      Begin VB.Menu mnuOther_CrossActions 
         Caption         =   "&Captions/Cross-Actions..."
      End
      Begin VB.Menu mnuOther_Signatures 
         Caption         =   "Si&gnatures..."
      End
      Begin VB.Menu mnuOther_ChangeCourt 
         Caption         =   "C&hange Court..."
      End
      Begin VB.Menu mnuOther_ChangeFont 
         Caption         =   "Change &Font..."
      End
      Begin VB.Menu mnuOther_TitlePage 
         Caption         =   "&Title Page"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuOther_BlueBack 
         Caption         =   "&Blue Back"
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuDateTime 
      Caption         =   "Date/Time Info"
      Visible         =   0   'False
      Begin VB.Menu mnuDateTime_Delete 
         Caption         =   "&Delete Row"
      End
      Begin VB.Menu mnuDateTime_Insert 
         Caption         =   "&Insert Row"
      End
      Begin VB.Menu mnuDateTime_Sep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuDateTime_ClearAll 
         Caption         =   "&Clear All Rows"
      End
      Begin VB.Menu mnuDateTime_Reset 
         Caption         =   "&Reset to Defaults"
      End
   End
   Begin VB.Menu mnuCaseNumbers 
      Caption         =   "Case Numbers"
      Visible         =   0   'False
      Begin VB.Menu mnuCaseNumbers_DeleteCaseNumber 
         Caption         =   "&Delete Case Number"
      End
      Begin VB.Menu mnuCaseNumbers_DeleteAllNumbers 
         Caption         =   "Delete &All Case Numbers"
      End
      Begin VB.Menu mnuCaseNumbers_Sep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuCaseNumbers_Reset 
         Caption         =   "&Reset to Defaults"
      End
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmPleadingCoverPage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oPersonsForm As MPO.CPersonsForm
Private m_oMenu As MacPac90.CAuthorMenu
Private WithEvents m_oPleading As MPO.CPleading
Attribute m_oPleading.VB_VarHelpID = -1
Private WithEvents m_oCnsl As MPO.CPleadingCounsel
Attribute m_oCnsl.VB_VarHelpID = -1
Private WithEvents m_oCap As MPO.CPleadingCaption
Attribute m_oCap.VB_VarHelpID = -1
Private WithEvents m_oCP As MPO.CPleadingCoverPage
Attribute m_oCP.VB_VarHelpID = -1
Private m_oParent As Form
Private WithEvents m_oSig As MPO.CPleadingSignature
Attribute m_oSig.VB_VarHelpID = -1

Private m_PersonsForm As MPO.CPersonsForm
Private m_bCancelled As Boolean
Private m_bAuthorDirty As Boolean
Private m_ShowHiddenText As Boolean
Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private m_bOtherFormDisplayed As Boolean
Private m_bCourtChanged As Boolean
Private m_oOffice As mpDB.COffice

Private xMsg As String
Private m_lSubForm As Long

Private Enum pldSubForm
    pldSubForm_Signature = 1
    pldSubForm_Counsels = 2
    pldSubForm_Captions = 3
    pldSubForm_ChangeCourt = 4
    pldSubForm_TitlePage = 5
    pldSubForm_BlueBack = 6
    pldSubForm_ChangeFont = 7
End Enum

Private Const mpCaseNoSep As String = "  "

'******************************************************************
'---Properties
'******************************************************************


Public Property Get Pleading() As MPO.CPleading
    Set Pleading = m_oPleading
End Property

Public Property Let Pleading(oNew As MPO.CPleading)
    Set m_oPleading = oNew
End Property
'---9.7.1 - 4131
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property

Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property

Public Property Let Signature(oNew As MPO.CPleadingSignature)
    Set m_oSig = oNew
End Property

Public Property Get Signature() As MPO.CPleadingSignature
    Set Signature = m_oSig
End Property

Public Property Let Counsel(oNew As MPO.CPleadingCounsel)
    Set m_oCnsl = oNew
End Property

Public Property Get Counsel() As MPO.CPleadingCounsel
    Set Counsel = m_oCnsl
End Property

Public Property Let CoverPage(oNew As MPO.CPleadingCoverPage)
    Set m_oCP = oNew
End Property

Public Property Get CoverPage() As MPO.CPleadingCoverPage
    Set CoverPage = m_oCP
End Property

Public Property Let PleadingCaption(oNew As MPO.CPleadingCaption)
    Set m_oCap = oNew
End Property

Public Property Get PleadingCaption() As MPO.CPleadingCaption
    Set PleadingCaption = m_oCap
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property
Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property
Public Property Let AuthorIsDirty(bNew As Boolean)
    m_bAuthorDirty = bNew
End Property
Public Property Get AuthorIsDirty() As Boolean
    AuthorIsDirty = m_bAuthorDirty
End Property
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
    Me.CascadeUpdates = False
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = True
End Property

Public Property Get Office() As mpDB.COffice
    Set Office = m_oOffice
End Property

Public Property Let Office(oOff As mpDB.COffice)
    Set m_oOffice = oOff
End Property

Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnCancel_GotFocus()
    SetControlBackColor btnCancel
End Sub

Private Sub btnFinish_Click()

    On Error GoTo ProcError
    m_bCancelled = False
    
    Me.Hide
    DoEvents
    Exit Sub

ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    SetControlBackColor btnFinish
End Sub

Private Sub btnMore_Click()
    On Error GoTo ProcError
    DoEvents
    Me.PopupMenu mnuOther, y:=btnMore.Top + btnMore.Height, x:=btnMore.Left
    Application.ScreenRefresh
    Application.ScreenUpdating = True
    EchoOn
    g_oMPO.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnMore_GotFocus()
    SetControlBackColor btnMore
End Sub

Private Sub chkCopyCaptionTable_Click()
    Me.CoverPage.CopyExistingCaption = chkCopyCaptionTable = vbChecked
End Sub

Private Sub chkCopyCounselTable_Click()
    Me.CoverPage.CopyExistingCounsel = chkCopyCounselTable = vbChecked
End Sub

Private Sub cmbAuthor_GotFocus()
    OnControlGotFocus cmbAuthor, "zzmpCPFirmAddress"
End Sub

Private Sub cmbAuthor_LostFocus()
    UpdateForAuthor
End Sub

Private Sub cmbAuthor_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        If Not Me.cmbAuthor.IsOpen Then
            ShowAuthorMenu
        End If
    End If
End Sub

Private Sub ShowAuthorMenu()
    Dim bEnabled As Boolean
    On Error GoTo ProcError
    
'   enable specified menu items only if author
'   is currently in the db
    If TypeOf Me.CoverPage.Author Is CReUseAuthor Then
        bEnabled = False
    Else
        bEnabled = True
    End If
    
    Me.mnuAuthor_Favorite.Enabled = bEnabled
    Me.mnuAuthor_SetDefault.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Checked = Me.CoverPage.Author.Favorite
    Me.PopupMenu mnuAuthor
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbParty3Title_GotFocus()
    OnControlGotFocus cmbParty3Title, "zzmpParty3Title" & "_1"
End Sub
Private Sub cmbParty3Title_LostFocus()
    On Error GoTo ProcError
    Me.PleadingCaption.Party3Title = cmbParty3Title.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub Form_Initialize()
    Set m_oPersonsForm = New MPO.CPersonsForm
    Set m_oMenu = New MacPac90.CAuthorMenu
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    OnFormKeyDown KeyCode
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_AfterColUpdate(ByVal ColIndex As Integer)
    
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    
    With Me.CoverPage
        Select Case grdCaseNumbers.Bookmark
            Case 0
                .CaseNumber1 = grdCaseNumbers.Columns(1).Text
            Case 1
                .CaseNumber2 = grdCaseNumbers.Columns(1).Text
            Case 2
                .CaseNumber3 = grdCaseNumbers.Columns(1).Text
        End Select
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_FirstRowChange(ByVal SplitIndex As Integer)
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    If Not IsNull(grdCaseNumbers.Bookmark) Then
        Me.Pleading.Document.SelectItem "zzmpCaseNumber" & grdCaseNumbers.Bookmark + 1 & "_" & CoverPage.ID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdCaseNumbers_GotFocus()
    On Error Resume Next
    grdCaseNumbers.EditActive = True
    GridTabOut Me.grdCaseNumbers, True, 1
    OnControlGotFocus grdCaseNumbers, "zzmpCaseNumber" & grdCaseNumbers.Bookmark + 1 & "_1"
End Sub

Private Sub grdCaseNumbers_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
    If Button = 2 Then
        Me.PopupMenu mnuCaseNumbers
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub grdCaseNumbers_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    With grdCaseNumbers
        '.EditActive = True
        If Not IsNull(.Bookmark) Then
            Me.Pleading.Document.SelectItem "zzmpCaseNumber" & .Bookmark + 1 & "_" & CoverPage.ID
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub mnuCaseNumbers_ClearAll_Click()
    Dim iRows As Integer
    
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iRows = .Array.Count(1)
        .Array.ReDim 0, -1, 0, 1
        .Array.ReDim 0, iRows, 0, 1
        .Rebind
        .Columns(0).AllowFocus = True
        .Columns(0).Locked = False
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCaseNumbers_DeleteAllNumbers_Click()
    Dim iRow As Integer
    Dim iCount As Integer
    Dim i As Integer
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iCount = .Array.UpperBound(1)
        For i = 0 To iCount
            .Array.value(i, 1) = Empty
        Next i
        .Rebind
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCaseNumbers_DeleteCaseNumber_Click()
    Dim iRow As Integer
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iRow = .RowBookmark(.Row)
        .Array.value(iRow, 1) = Empty
        .Rebind
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCaseNumbers_DeleteRow_Click()
    Dim iRow As Integer
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        iRow = .RowBookmark(.Row)
        .Array.value(iRow, 0) = Empty
        .Array.value(iRow, 1) = Empty
        .Rebind
        If iRow = 0 Then
            .Columns(0).AllowFocus = True
            .Columns(0).Locked = False
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuCaseNumbers_Reset_Click()
    Dim xTemp As String
    Dim xarNew As XArray
    On Error GoTo ProcError
    With Me.grdCaseNumbers
        .Array = CaseNumberArray(True)
        .Rebind
        .Col = 1
        .Bookmark = 0
        .EditActive = True
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

'******************************************************************************
'Menu events
'******************************************************************************
Private Sub mnuAuthor_ManageLists_Click()
'   get current selection
    
    Dim lID As Long
    
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ManageAuthors Me.cmbAuthor, lID
    Else
        m_oMenu.ManageAuthors Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_AddNew_Click()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.cmbAuthor.Columns("fldID")
    m_oMenu.AddPerson Me.cmbAuthor, lID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Favorite_Click()
    Dim lID As Long
    
    On Error Resume Next
    DoEvents
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ToggleFavorite Me.cmbAuthor, lID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    On Error GoTo ProcError
    With CoverPage.Template
        Me.CoverPage.Author = g_oMPO.People(cmbAuthor.BoundText)
        .DefaultAuthor = Me.CoverPage.Author
        .UpdateDefaults .DefaultAuthor.ID, Me.CoverPage
    End With
    
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Copy_Click()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
    On Error GoTo ProcError
'   if author isn't in list, the
'   author is the reuse author
    If Me.cmbAuthor.Row = -1 Then
'       use reuse author as source
        Set oSource = Me.CoverPage.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oMPO.People(Me.cmbAuthor.BoundText)
    End If
    
'   copy the person
    Set oCopy = m_PersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuOther_BlueBack_Click()
    m_lSubForm = pldSubForm_BlueBack
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_ChangeCourt_Click()
    m_lSubForm = pldSubForm_ChangeCourt
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_ChangeFont_Click()
    m_lSubForm = pldSubForm_ChangeFont
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_TitlePage_Click()
    With mnuOther_TitlePage
        .Checked = Not .Checked
        Me.Pleading.CoverPage = .Checked
        UnderConstruction
    End With
End Sub


Private Sub mnuOther_Counsels_Click()
    m_lSubForm = pldSubForm_Counsels
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_CrossActions_Click()
    m_lSubForm = pldSubForm_Captions
    Timer2.Enabled = True
End Sub

Private Sub mnuOther_Signatures_Click()
    m_lSubForm = pldSubForm_Signature
    Timer2.Enabled = True
End Sub

Private Sub Timer2_Timer()
   '!!! do we want the more button?
   '---note:  this routine is called from the more button popup menu
   '---it's a workaround that enables popup menus to be displayed by
   '---any form called by a popup menu
   '---if these routines were called directly from the more button popup
   '---no popup menus would display in sig or caption forms.  This is a VB bug
   '---around since version 1!
    Timer2.Enabled = False
    Select Case m_lSubForm
        Case pldSubForm_Signature
            
        Case pldSubForm_Captions
            'CreatePleadingCaptions
        Case pldSubForm_Counsels
            'CreatePleadingCounsels
        Case pldSubForm_ChangeCourt
            'ChangePleadingType
        Case pldSubForm_ChangeFont
            'ChangeFont
        Case pldSubForm_BlueBack
            'UnderConstruction
        Case Else
    End Select
End Sub

Private Sub Form_Activate()
'    On Error GoTo ProcError
    Dim bUpdate As Boolean
    
    DoEvents
    '---form activate fires after subforms are released
    '---R&D needed -- some subforms don't cause this behavior
    
    If m_bOtherFormDisplayed Then
        m_bOtherFormDisplayed = False
        Exit Sub
    End If
    
    Me.vsIndexTab1.CurrTab = 0
'---display controls appropriate to pleading type def
    DisplayControls True
'adjust control position based on above
    PositionControls
       
    If Me.Initializing Then
        DoEvents
        With Me.CoverPage
            If .Document.IsCreated Then
                Me.cmbAuthor.BoundText = .Author.ID
                '---Log 3965
                If cmbAuthor.BoundText = "" Then
                    '---reset to first author in the list - author is no longer in dbase
                    cmbAuthor.BoundText = cmbAuthor.Array.value(0, 2)
                End If
            Else
'''---Assign signer's pen to default author only if default author is attorney
'''---accounts for .defaultauthor = .authors.listsource.first condition
                If .Author.IsAttorney Then Me.cmbAuthor.BoundText = .Author.ID
                Me.AuthorIsDirty = False
'---set properties based on preloads
                LoadDefaultValues
            End If
        End With
'---refresh office detail
        UpdateForAuthor
'---load controls with default or reuse values
        LoadControls
        
        Screen.MousePointer = vbDefault
        DoEvents
'---wake up the form for SDI
        SendKeys "+", True
        DoEvents
        Me.Initializing = False
    End If
    btnMore.Enabled = True
'   move dialog to last position -- handles condition if subform of pleading
    If Forms.Count > 1 Then
        Me.Top = Forms(0).Top
        Me.Left = Forms(0).Left
    Else
        'MoveToLastPosition Me
    End If
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    Screen.MousePointer = vbDefault
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
    
    Set m_oMenu = Nothing
    Set m_oPersonsForm = Nothing
    Set m_oPleading = Nothing
    Set m_oCnsl = Nothing
    Set m_oSig = Nothing
    Set g_oPrevControl = Nothing
    
End Sub
'******************************************************************************
'Menu events
'******************************************************************************

Private Sub grdCaseNumbers_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim xText As String
    On Error GoTo ProcError
    
    '---deal with grid selecting next row and transmitting returns that overwrite existing next row text
    If Shift = 2 Then
        If KeyCode = 13 Then
            With grdCaseNumbers
                If .SelLength = 0 Then
                    KeyCode = 0
                    xText = xTrimTrailingChrs(.Columns(.Col).Text, vbCrLf)
                    .Columns(.Col).Text = xText
                    .SelStart = Len(.Columns(.Col).Text)
                End If
            End With
        End If
    End If
    
    Select Case KeyCode
        Case 9
            GridTabOut Me.grdCaseNumbers, False, 1
        Case 39
            With grdCaseNumbers
                If .SelLength > 1 Then
                    .SelStart = .SelLength - 1
                    .SelLength = 1
                End If
            End With
        Case Else
    End Select
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oParties As XArray
    Dim datStart As Date
    Dim xFilter As String
    Dim i As Integer
    Dim oList As mpDB.CList
    Dim bDyn As Boolean
    Dim oArray As XArray
    Dim xOrdSuffix As String

    On Error GoTo ProcError
    
    Me.Initializing = True
    m_bCancelled = True
    Me.Top = 20000
    Application.ScreenUpdating = False
    EchoOff
    DoEvents
    
    bDyn = g_oMPO.DynamicEditing
    
'---set pleading sub object props
    With Me.CoverPage
        If Pleading.Definition.DefaultCounselType Then
            Counsel = .Counsels(1)
        End If
        If Pleading.Definition.DefaultSignatureType Then
            Signature = Pleading.Signatures(1)
        End If
        If Pleading.Definition.DefaultCaptionType Then
            PleadingCaption = .Captions(1)
        End If
    End With
    
'---set office
    If g_oMPO.UseDefOfficeInfo Then
        Me.Office = g_oMPO.Offices.Default
    Else
        Me.Office = Pleading.Author.Office
    End If
    
'---fill combos with lists
'---refresh attorneys first
    g_oMPO.Attorneys.Refresh
    Me.cmbAuthor.Array = g_oMPO.Attorneys.ListSource
    DoEvents
    Set oParties = g_oMPO.Lists("PleadingParties").ListItems.Source
    Me.cmbParty1Title.Array = oParties
    Me.cmbParty2Title.Array = oParties
    Me.cmbParty3Title.Array = oParties
    
    
'   set combo dropdown height values
    ResizeTDBCombo Me.cmbParty1Title, 10
    ResizeTDBCombo Me.cmbParty2Title, 10
    ResizeTDBCombo Me.cmbParty3Title, 10
    ResizeTDBCombo Me.cmbAuthor, 10
    txtCourtTitle.ZOrder 1
    cmbAuthor.ZOrder 1
    
''   move dialog to last position
'    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Application.ScreenUpdating = True
    Exit Sub
End Sub

Private Sub grdCaseNumbers_LostFocus()
    On Error GoTo ProcError
    With grdCaseNumbers
        .Update
        .Col = 1
        '.Row = 0
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Function bLoadTDGDropDown(grdGrid As TDBGrid, iTargCol As Integer, x_XAR As XArray, iColIndex As Integer)
    Dim ctl As Control
    Dim g_Grid As TDBGrid
    Dim i As Integer
    Dim VItem As New TrueDBGrid60.ValueItem
        
    On Error GoTo ProcError
        
    Set g_Grid = grdGrid

'---load dropdown
    Dim xAR As XArray
    With g_Grid
        .Columns(iTargCol).ValueItems.Clear
        For i = 0 To x_XAR.UpperBound(1)
            VItem.value = x_XAR(i, 0)
            VItem.DisplayValue = x_XAR(i, 1)
            .Columns(iTargCol).ValueItems.Add VItem
        Next i

    End With
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCoverPage.bLoadTDGDropDown"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Sub txtCourtTitle_GotFocus()
    OnControlGotFocus txtCourtTitle, "zzmpCPCourtTitle_" & Me.CoverPage.ID
'---wake up the form for SDI
    SendKeys "+", True
End Sub
Private Sub txtCourtTitle_LostFocus()
    On Error GoTo ProcError
    
    If Me.CoverPage.Definition.CourtTitle1 = "" Then
        Me.CoverPage.CourtTitle = Me.txtCourtTitle
    Else
        '---split court title
        SplitCourtTitle
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCaseTitle_GotFocus()
    OnControlGotFocus txtCaseTitle, "zzmpCPDocumentTitle_" & Me.CoverPage.ID
End Sub
Private Sub txtCaseTitle_LostFocus()
    Dim xTemp As String
    On Error GoTo ProcError
    Me.CoverPage.DocumentTitle = txtCaseTitle
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtParty1Name_GotFocus()
    OnControlGotFocus txtParty1Name, "zzmpCPParty1Name_" & Me.CoverPage.ID
End Sub
Private Sub txtParty1Name_LostFocus()
    Dim xP1N As String
    Dim xP1T As String
    On Error GoTo ProcError
    With Me.CoverPage
        .Party1Name = txtParty1Name
        If txtParty1Name <> "" Then
            xP1N = " - " & txtParty1Name
        Else
            xP1N = txtParty1Name
        End If
    
        If cmbParty1Title <> "" Then
            xP1T = " - " & cmbParty1Title.Text
        Else
            xP1T = ""
        End If
        
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtParty2Name_GotFocus()
    OnControlGotFocus txtParty2Name, "zzmpCPParty2Name_" & Me.CoverPage.ID
End Sub
Private Sub txtParty2Name_LostFocus()
    On Error GoTo ProcError
    Me.CoverPage.Party2Name = txtParty2Name
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtParty3Name_GotFocus()
    OnControlGotFocus txtParty3Name, "zzmpCPParty3Name_" & Me.CoverPage.ID
End Sub

Private Sub txtParty3Name_LostFocus()
    On Error GoTo ProcError
    Me.CoverPage.Party3Name = txtParty3Name
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbParty1Title_GotFocus()
    OnControlGotFocus cmbParty1Title, "zzmpCPParty1Title_" & Me.CoverPage.ID
End Sub
Private Sub cmbParty1Title_LostFocus()
    On Error GoTo ProcError
    Me.CoverPage.Party1Title = cmbParty1Title.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbParty2Title_GotFocus()
    OnControlGotFocus cmbParty2Title, "zzmpCPParty2Title_" & Me.CoverPage.ID
End Sub

Private Sub cmbParty2Title_LostFocus()
    On Error GoTo ProcError
    Me.CoverPage.Party2Title = cmbParty2Title.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Sub LoadControls()
    Dim xTemp As String
    Dim xARDateTime As XArray
    Dim xTitle As String
    
    On Error GoTo ProcError
     '---get properties, load controls on reuse
    With CoverPage
        
        Me.txtCaseTitle = .DocumentTitle
        '---handle split court title
        If .CourtTitle1 = "" Then
            Me.txtCourtTitle = .CourtTitle
        Else
            xTitle = CoverPage.CourtTitle
            If .CourtTitle1 <> "" Then
                xTitle = xTitle & vbCrLf & .CourtTitle1
            End If
            If .CourtTitle2 <> "" Then
                xTitle = xTitle & vbCrLf & .CourtTitle2
            End If
            If .CourtTitle3 <> "" Then
                xTitle = xTitle & vbCrLf & .CourtTitle3
            End If
            Me.txtCourtTitle = xTitle
        End If
        DoEvents
        
        Me.cmbParty1Title.Text = .Party1Title
        Me.txtParty1Name = .Party1Name
        Me.cmbParty2Title.Text = .Party2Title
        Me.txtParty2Name = .Party2Name
        Me.cmbParty3Title.Text = .Party3Title
        Me.txtParty3Name = .Party3Name
        Me.txtCaseTitle = .DocumentTitle
        
        DoEvents
    End With
    Me.AuthorIsDirty = False
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCoverPage.LoadControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
    
End Sub


Private Sub vsIndexTab1_Click()
    On Error GoTo ProcError
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            '!!!Me.saGrid.SetFocus
        Case 1
            Me.grdCaseNumbers.SetFocus
    End Select

    Exit Sub
ProcError:
    Exit Sub
End Sub
Private Function CaseNumberArray(Optional bLoadValues As Boolean = False) As XArray
    Dim xarTemp As XArray
    Dim i As Integer
    
    On Error GoTo ProcError
    Set xarTemp = g_oMPO.NewXArray
    xarTemp.ReDim 0, -1, 0, 1
    
    With Me.PleadingCaption.Definition
        If .CaseNumber1Label <> "" Then
            xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
            xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber1Label
            If bLoadValues Then
                xarTemp(xarTemp.UpperBound(1), 1) = Me.CoverPage.CaseNumber1
            End If
        End If
        If .CaseNumber2Label <> "" Then
            xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
            xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber2Label
            If bLoadValues Then
                xarTemp(xarTemp.UpperBound(1), 1) = Me.CoverPage.CaseNumber2
            End If
        End If
        If .CaseNumber3Label <> "" Then
            xarTemp.Insert 1, xarTemp.UpperBound(1) + 1
            xarTemp(xarTemp.UpperBound(1), 0) = .CaseNumber3Label
            If bLoadValues Then
                xarTemp(xarTemp.UpperBound(1), 1) = Me.CoverPage.CaseNumber3
            End If
        End If
            
    End With
    
    Set CaseNumberArray = xarTemp
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCoverPage.CaseNumberArray"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Function PositionControls()
    On Error GoTo ProcError
    Dim sOffset As Single

'---adjust this if you want more or less space between controls on second tab
    Const sIncrement As Single = 200

'---first tab -- this is hard coded because controls are not equally spaced
    If Me.PleadingCaption.Definition.PartiesInCaption < 3 Then
        With Me
            .lblAuthor.Top = 135
            .cmbAuthor.Top = 90
            .lblCourtTitle.Top = 900
            .txtCourtTitle.Top = 840
            .lblParty1Name.Top = 2250
            .cmbParty1Title.Top = 2250
            .txtParty1Name.Top = 2585
            .lblParty2Name.Top = 3920
            .cmbParty2Title.Top = 3890
            .txtParty2Name.Top = 4210
            .lblParty3Name.Top = .txtParty2Name.Top
            .cmbParty3Title.Top = .cmbParty2Title.Top
            .txtParty3Name.Top = .txtParty2Name.Top
        End With
    Else
        With Me
            .lblAuthor.Top = 135
            .cmbAuthor.Top = 90
            .lblCourtTitle.Top = 900
            .txtCourtTitle.Top = 840
            .lblParty1Name.Top = 2250
            .cmbParty1Title.Top = 2220
            .txtParty1Name.Top = 2535
            .lblParty2Name.Top = 3420
            .cmbParty2Title.Top = 3390
            .txtParty2Name.Top = 3705
            .lblParty3Name.Top = 4600   '4740
            .cmbParty3Title.Top = 4600   '4740
            .txtParty3Name.Top = 4915   '5055
        End With
    End If
'
'
''---second tab
    With Me
'        sOffset = .grdCaseNumbers.Top + grdCaseNumbers.Height + sIncrement

        If .chkCopyCaptionTable.Visible = False Then
            Dim lTop As Long
            lTop = .chkCopyCounselTable.Top
            If .chkCopyCounselTable.Visible = True Then
                .chkCopyCounselTable.Top = .chkCopyCaptionTable.Top
            End If
        End If
    
    
    End With
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCoverPage.PositionControls"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function



Private Function DisplayControls(Optional bReload As Boolean = False)
    On Error GoTo ProcError
    
    Dim bRet As Boolean
    Dim lRet As Long
    Dim xCap As String
    Dim xLC As String
    Static sHeight As Single
    Static sgH As Single
    
    On Error GoTo ProcError
    With Me.Pleading
        
        
        Select Case Me.PleadingCaption.Definition.PartiesInCaption
            Case 0
            Case 1
                Me.lblParty2Name.Visible = False
                Me.txtParty2Name.Visible = False
                Me.cmbParty2Title.Visible = False
                Me.lblVersus.Visible = False
                Me.lblVersus1.Visible = False
                Me.lblVersus2.Visible = False
            
            Case 2
                Me.lblParty2Name.Visible = True
                Me.txtParty2Name.Visible = True
                Me.cmbParty2Title.Visible = True
                Me.lblVersus.Visible = True
                Me.lblVersus1.Visible = False
                Me.lblVersus2.Visible = False
           
                Me.lblParty3Name.Visible = False
                Me.txtParty3Name.Visible = False
                Me.cmbParty3Title.Visible = False
           
           Case 3
                Me.lblParty2Name.Visible = True
                Me.txtParty2Name.Visible = True
                Me.cmbParty2Title.Visible = True
                Me.lblVersus.Visible = False
                Me.lblVersus1.Visible = True
                Me.lblVersus2.Visible = True
                
                Me.lblParty3Name.Visible = True
                Me.txtParty3Name.Visible = True
                Me.cmbParty3Title.Visible = True
        
        End Select
        

'---display copy controls
    Me.chkCopyCounselTable.Visible = Me.Pleading.Counsels.Count > 1
    Me.chkCopyCaptionTable.Visible = Me.Pleading.Captions.Count > 1
    
'---set case numbers grid & label
'---control height is adjusted here so 2 case nos display 2 lines each, 3 displays one line each
'---adjust case 2 if you want 2 line display for 3 case nos, etc.
        With grdCaseNumbers
            If sHeight = 0 Then
                sHeight = .RowHeight
            End If
            If sgH = 0 Then
                sgH = .Height
            End If
            .Array = CaseNumberArray(bReload)
            .Rebind
            Select Case .Array.UpperBound(1)
                Case 0, -1
                    .Height = sgH
                    xLC = "Case &Number:"
                    .RowHeight = .Height - 15
                Case 1
                    .Height = 890
                    xLC = "Case &Numbers:"
                    .RowHeight = (.Height / 2) - 15
                Case 2
                    .Height = 890
                    xLC = "Case &Numbers:"
                    .RowHeight = (.Height / 3) - 15
                Case Else
                    .Height = sgH
                    xLC = "Case &Numbers:"
                    .RowHeight = sHeight
            End Select
            lblCaseNumber.Caption = xLC
        End With

'---set menu items
'!!!
'        Me.mnuOther_BlueBack.Visible = .Definition.AllowsBlueBack
'        Me.mnuOther_TitlePage.Visible = .Definition.AllowsCoverPage
'        Me.mnuOther_CrossActions.Visible = .Definition.AllowCrossAction
'        Me.mnuOther_Counsels.Visible = .Definition.AllowCoCounsel
'        Me.mnuOther_ChangeFont.Visible = .Definition.AllowsFontChange

''---set the dbox caption
''!!!---you might want to include a display name prop in the types table
'        xCap = Me.Caption
'        If InStr(xCap, " - ") Then
'            xCap = Left(xCap, InStr(xCap, " - ") - 1)
'        End If
'        Me.Caption = xCap & " - " & .Level0String
    End With
    
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCoverPage.DisplayControls"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function


Public Sub SetInterfaceControls()
    Dim xARDateTime As XArray
    Dim oCaption As MPO.CPleadingCaption
    Dim oOffice As COffice
    Dim lFilter As Long
    Dim oList As CList
    Dim xTitle As String
    
    On Error Resume Next
    
    If Not Me.Pleading.Document.IsCreated Then
'---set defaults for lists/combos
        With Me.Pleading
            Set oCaption = Me.PleadingCaption
            
        End With
    Else

    End If
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCoverPage.SetInterfaceControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


Private Sub LoadDefaultValues()
    Dim xTemp As String
    Dim i As Integer
    Dim bForce As Boolean
'    Dim xSep As String
'    Dim xRep As String
    
'---set pleading properties based on preloaded values
'---necessary because we don't prewrite default values for
'---the template
'---runs on new doc create or court change from main db only

    On Error GoTo ProcError
        
    If g_oMPO.DynamicEditing Then
        Application.ScreenUpdating = False
        bForce = g_oMPO.ForceItemUpdate
        g_oMPO.ForceItemUpdate = True
            With Me.CoverPage
                If Not m_bCourtChanged Then
                    .CourtTitle = .CourtTitle
'---split court title
                    .CourtTitle1 = .CourtTitle1
                    .CourtTitle2 = .CourtTitle2
                    .CourtTitle3 = .CourtTitle3
                
'---party info
                    If .Party1Title = "" Then
                        .Party1Title = .Captions(1).Definition.Party1TitleDefault
                    Else
                        .Party1Title = .Party1Title
                    End If
                    If .Party2Title = "" Then
                        .Party2Title = .Captions(1).Definition.Party2TitleDefault
                    Else
                        .Party2Title = .Party2Title
                    End If
                    If .Party3Title = "" Then
                        .Party3Title = .Captions(1).Definition.Party3TitleDefault
                    Else
                        .Party3Title = .Party3Title
                    End If
                    .Party1Name = .Party1Name
                    .Party2Name = .Party2Name
                    .Party3Name = .Party3Name
                
'---load case number grid with defaults if empty & visible
               
               For i = 0 To Me.grdCaseNumbers.Array.UpperBound(1)
                    xTemp = grdCaseNumbers.Array.value(i, 1)
                    Select Case i
                        Case 0
                            .CaseNumber1 = xTemp
                        Case 1
                            .CaseNumber2 = xTemp
                        Case 2
                            .CaseNumber3 = xTemp
                    End Select
                Next i
    
               .DocumentTitle = .DocumentTitle
                If .FirmName = "" Then
                    .FirmName = Me.Office.FirmName
                Else
                    .FirmName = .FirmName
                End If
                .FirmAddress = .FirmAddress
                .FirmPhone = .FirmPhone

                End If
           End With
        g_oMPO.ForceItemUpdate = bForce
    End If
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingCoverPage.LoadDefaultValues"), _
              g_oError.Desc(Err.Number)
    If g_oMPO.ForceItemUpdate = True Then g_oMPO.ForceItemUpdate = False
    Application.ScreenUpdating = True
    Exit Sub
End Sub


Public Sub LoadPreviousValues()
'    Dim oOffice As COffice
'    Dim xTitle As String
'
''---this routine called from changepleadingtype
''---refreshes props of new pleading type from dbox contents
'
'    On Error GoTo ProcError
'    g_oMPO.ForceItemUpdate = True
'
'    '---handle split court title
'    If Me.Pleading.CourtTitle1 = "" Then
'        Me.txtCourtTitle = Me.Pleading.DefaultCourtTitle
'        Me.Pleading.CourtTitle = Me.txtCourtTitle
'    Else
'        With Me.Pleading
'            If .CourtTitle1 <> "" Then
'                xTitle = .CourtTitle & vbCrLf & .CourtTitle1
'                If .CourtTitle2 <> "" Then
'                    xTitle = xTitle & vbCrLf & .CourtTitle2
'                End If
'                If .CourtTitle3 <> "" Then
'                    xTitle = xTitle & vbCrLf & .CourtTitle3
'                End If
'                Me.txtCourtTitle = xTitle
'                SplitCourtTitle
'            End If
'        End With
'    End If
'
'    With Me.Signature
'        .Position = .Definition.DefaultPosition
'        If g_oMPO.UseDefOfficeInfo Then
'            Set oOffice = g_oMPO.Offices.Default
'        Else
'            Set oOffice = Me.Pleading.Author.Office
'        End If
'        .FirmName = oOffice.FirmNameUpperCase
'        .FirmAddress = oOffice.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
'        .FirmPhone = oOffice.Phone1
'        .FirmFax = oOffice.Fax1
'        .FirmCity = oOffice.City
'        .FirmState = oOffice.State
'        .SignerEMail = Me.Pleading.Author.Email
'        .PartyName = Me.txtClientName
'        .PartyTitle = Me.cmbAttorneysFor.Text
'    End With
'
'    Me.cmbDateType.BoundText = Me.Signature.Definition.DateFormat
'    DoEvents
'    Me.cmbSignatureType.BoundText = Me.Pleading.Definition.DefaultSignatureType
'
'    With Me.PleadingCaption
'        .CaseTitle = Me.txtCaseTitle
'        .Party1Name = Me.txtParty1Name
'        .Party2Name = Me.txtParty2Name
'        .Party3Name = Me.txtParty3Name
'        .Party1Title = Me.cmbParty1Title.Text
'        .Party2Title = Me.cmbParty2Title.Text
'        .Party3Title = Me.cmbParty3Title.Text
'        .AppealFromCourtTitle = Me.txtOriginalCourtTitle.Text
'        .PresidingJudge = Me.txtPresidingJudge
'    End With
'
'    If Not Me.Counsel Is Nothing Then
'        With Me.Counsel
'            .ClientPartyName = Me.txtClientName
'            .ClientPartyTitle = Me.cmbAttorneysFor.Text
'        End With
'    End If
'    g_oMPO.ForceItemUpdate = False
'    Exit Sub
'ProcError:
'    g_oError.Raise Err.Number, _
'              CurErrSource("frmPleadingCoverPage.LoadValues"), _
'              g_oError.Desc(Err.Number)
'    g_oMPO.ForceItemUpdate = False
'    Exit Sub
End Sub

Private Function GridMoveNext(grdCur As TDBGrid, Optional bNextRecord As Boolean = False)
    ' Move to next column or row
    ' Redim array and add blank row if necessary
    On Error Resume Next
    With grdCur
        If .Bookmark < grdCur.Array.UpperBound(1) Then
            '---
            .MoveNext
        ElseIf Not GridCurrentRowIsEmpty(grdCur) Then
            .Update
            .Array.ReDim 0, .Bookmark + 1, 0, .Columns.Count - 1
            .Rebind
            .MoveLast
            .Col = 0
            .EditActive = True
        End If
    End With
End Function

Private Function GridCurrentRowIsEmpty(grdTest As TDBGrid) As Boolean
    Dim i As Integer
    On Error Resume Next
    GridCurrentRowIsEmpty = True
    With grdTest
        For i = 0 To .Columns.Count - 1
            If .Columns(i).Text <> "" Then
                GridCurrentRowIsEmpty = False
                Exit Function
            End If
        Next i
    End With
End Function


Public Sub SplitCourtTitle()
    Dim xTemp As String
    Dim xTitle() As String
    Dim iIndex As Integer
    Dim i As Integer
    Dim iRets As Integer
    
    On Error Resume Next
    xTitle() = Split(Me.txtCourtTitle, vbCrLf)
    
    With CoverPage
        .CourtTitle = ""
        .CourtTitle1 = ""
        .CourtTitle2 = ""
        .CourtTitle3 = ""
        If UBound(xTitle(), 1) > 0 Then
            .CourtTitle = xTitle(0)
            .CourtTitle1 = xTitle(1)
        End If
        
        If UBound(xTitle(), 1) > 1 Then _
          .CourtTitle2 = xTitle(2)
       
        If UBound(xTitle(), 1) > 2 Then _
          .CourtTitle3 = xTitle(3)
    
        If UBound(xTitle(), 1) > 3 Then
            xTemp = xTitle(3)
            For i = 4 To UBound(xTitle(), 1)
                xTemp = xTemp & vbCrLf & xTitle(i)
            Next i
            .CourtTitle3 = xTemp
        End If
    End With

End Sub

Private Sub UpdateForAuthor()

'---reset author
    
    Me.CoverPage.Author = g_oMPO.People(cmbAuthor.BoundText)
'---set office
    If Not g_oMPO.UseDefOfficeInfo Then
        Me.Office = Me.CoverPage.Author.Office
    End If
'---refresh office properties
    With Me.CoverPage
        .FirmName = Me.Office.FirmName
        .FirmAddress = Me.Office.AddressOLD(Chr(11))
        .FirmPhone = Me.Office.Phone1
    End With

End Sub
