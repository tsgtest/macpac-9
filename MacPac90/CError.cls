VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Enum ciErrors
    mpError_Dummy = 1
End Enum

Public Sub Show(oErr As ErrObject)
    Dim lNum As Long
    Dim xDesc As String
    Dim xSource As String
    Dim xMsg As String

    With oErr
        xDesc = .Description
        xSource = .Source
        lNum = .Number
    End With
    
    xMsg = "The following error occurred:" & vbCr & _
           "Error: " & vbTab & lNum & vbCr & _
           "Description:" & vbTab & xDesc & "." & _
           "Source:" & vbTab & lNum
    MsgBox xMsg, vbExclamation, App.Title
End Sub

Public Function Raise(lErr As Long, Optional xSource As String, Optional xSupp As String) As Boolean
'raises error lerr - xSource gets displayed in
'message only when the error is not a ciError

    Dim xDescription As String
    Dim iLevel As Integer
    Dim fEndApp As Boolean
    Dim xMsg As String
    
    iLevel = vbExclamation
    Screen.MousePointer = vbDefault
    
    Select Case lErr
        Case Else
            iLevel = vbCritical
            xMsg = "The following error occurred"
            
            If xSource <> "" Then
                xMsg = xMsg & " in " & xSource
            End If
            
            xMsg = xMsg & ":" & vbCr & _
                   lErr & "::" & Error(lErr)
    End Select
    
    Screen.MousePointer = vbDefault
    
    If xMsg <> "" Then
        If xSupp <> Empty Then
            MsgBox xMsg & vbCrLf & xSupp, iLevel ', Application.Caption
        Else
            MsgBox xMsg, iLevel ', Application.Caption
        End If
    ElseIf xSupp <> Empty Then
        MsgBox xSupp, iLevel ', Application.Caption
    End If
End Function

Function Desc(ByVal lErr As Long) As String
'returns the description associated with Error lErr
    Const mpContactAdmin As String = "  Please contact your administrator."
    
    Select Case lErr
        Case Else
            Desc = "No error message defined. The Visual Basic " & vbCr & _
                   "error associated with this error number is" & vbCrLf & Error(lErr) & "."
    End Select
End Function
