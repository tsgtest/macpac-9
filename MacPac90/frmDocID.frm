VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmDocID 
   Caption         =   "Insert/Remove Trailer/Doc ID"
   ClientHeight    =   3696
   ClientLeft      =   60
   ClientTop       =   456
   ClientWidth     =   4260
   Icon            =   "frmDocID.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3696
   ScaleWidth      =   4260
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnOK 
      Appearance      =   0  'Flat
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   1995
      TabIndex        =   8
      Top             =   3210
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3075
      TabIndex        =   7
      Top             =   3210
      Width           =   1000
   End
   Begin VB.CheckBox chkDraft 
      Caption         =   "&Draft"
      Height          =   300
      Left            =   255
      TabIndex        =   3
      Top             =   2235
      Width           =   1100
   End
   Begin VB.CheckBox chkTime 
      Caption         =   "Ti&me"
      Height          =   285
      Left            =   2895
      TabIndex        =   5
      Top             =   2220
      Width           =   1100
   End
   Begin VB.CheckBox chkDate 
      Caption         =   "D&ate"
      Height          =   285
      Left            =   1575
      TabIndex        =   4
      Top             =   2235
      Width           =   1100
   End
   Begin VB.CheckBox chkInsertDateAsField 
      Caption         =   "&Insert Date/Time as Field Code"
      Height          =   300
      Left            =   255
      TabIndex        =   6
      Top             =   2640
      Width           =   2760
   End
   Begin TrueDBList60.TDBList lstType 
      Height          =   1530
      Left            =   135
      OleObjectBlob   =   "frmDocID.frx":1DA2
      TabIndex        =   1
      Top             =   315
      Width           =   3945
   End
   Begin VB.Label lblType 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "&Type:"
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   165
      TabIndex        =   0
      Top             =   90
      Width           =   465
   End
   Begin VB.Label lblInclude 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Include:"
      ForeColor       =   &H00000000&
      Height          =   240
      Left            =   195
      TabIndex        =   2
      Top             =   1965
      Width           =   555
   End
End
Attribute VB_Name = "frmDocID"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private WithEvents m_oDocID As MPO.CDocumentStamp
Attribute m_oDocID.VB_VarHelpID = -1
Private m_bCancelled As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Private Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Public Property Set DocumentStamp(oNew As MPO.CDocumentStamp)
    Set m_oDocID = oNew
End Property

Public Property Get DocumentStamp() As MPO.CDocumentStamp
    Set DocumentStamp = m_oDocID
End Property
'**********************************************************
'   Internal Procedures
'**********************************************************

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    m_bCancelled = True
End Sub
Private Sub btnOK_Click()
    On Error GoTo ProcError
    Me.Hide
    SetUserIni "Doc ID", "Include Draft", Me.chkDraft
    SetUserIni "Doc ID", "Include Date", Me.chkDate
    SetUserIni "Doc ID", "Include Time", Me.chkTime
    SetUserIni "Doc ID", "Date as Field", Me.chkInsertDateAsField
    
    '---9.7.1 4033 cycle through dynamic checkboxes, set user ini and prep array
    Dim i As Integer
    
    If g_bAllowTrailerOptions = True Then
        For i = 0 To g_xAROptionalControls.UpperBound(1)
            With g_xAROptionalControls
                '''MsgBox Me.Controls("chk" & .value(i, 0)).value
                .value(i, 2) = Me.Controls("chk" & .value(i, 0)).value
                SetUserIni "Doc ID", "Include " & .value(i, 0), .value(i, 2)
            End With
        Next
    End If
    
    DoEvents
    m_bCancelled = False
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkDate_Click()
    Me.chkInsertDateAsField.Enabled = (Me.chkDate = vbChecked Or _
                                       Me.chkTime = vbChecked)
End Sub

Private Sub chkTime_Click()
    Me.chkInsertDateAsField.Enabled = (Me.chkDate = vbChecked Or _
                                       Me.chkTime = vbChecked)
End Sub

Private Sub Form_Activate()
    Dim at As MPO.CTemplate
    
    On Error GoTo ProcError
    If g_bUse90Trailer And m_oDocID.Exists Then
        UpdateForm
    ElseIf Not g_bUse90Trailer And mpTrailer.Trailer85Exists Then
        UpdateForm
    Else
        'REMOVE---Initializing = False '9.7.1020 #4512
        'mp10trailer
        If MP10TrailerExists Then
            If g_iMP10TrailerType = 100 Then
                Me.lstType.Text = "Footer"
            Else
                Me.lstType.Text = "End of Document"
            End If
        Else
            Me.lstType.BoundText = Me.DocumentStamp.TypeID
        End If
        Me.chkDraft = Val(mpbase2.GetUserIni("Doc ID", "Include Draft"))
        Me.chkDate = Val(mpbase2.GetUserIni("Doc ID", "Include Date"))
        Me.chkTime = Val(mpbase2.GetUserIni("Doc ID", "Include Time"))
        Me.chkInsertDateAsField = Val(mpbase2.GetUserIni("Doc ID", "Date as Field"))
        
        '---9.7.1 4033 - grab user.ini values and set dynamic checkboxes
        Dim i As Integer
        On Error Resume Next
        If g_bAllowTrailerOptions = True Then
'            lstType_Click
        '---9.7.1 4033r927 - create option checkboxes on the fly
            DisplayOptionCheckboxes
            For i = 0 To g_xAROptionalControls.UpperBound(1)
                With g_xAROptionalControls
                    If mpbase2.UserIniKeyExists("Doc ID", "Include " & .value(i, 0)) = True Then
                        Me.Controls("chk" & .value(i, 0)).value = Val(mpbase2.GetUserIni("Doc ID", "Include " & .value(i, 0)))
                    Else
                        Me.Controls("chk" & .value(i, 0)).value = .value(i, 2)
                    End If
                End With
            Next i
        End If
        
    End If
    
    On Error GoTo ProcError
    
'   if no default stamp type is selected, attempt to
'   select the "footer" or "end of document" stamp type - if still no selection,
'   select the first item in the list - this is a temporary
'   patch necessitated by the addition of Letterhead-specific
'   default trailers - we need to add DefaultTrailer as a
'   property of mpDB.LetterheadDef
    On Error Resume Next
    If Me.lstType.BoundText = Empty Then
        Set at = g_oMPO.Templates(ActiveDocument.AttachedTemplate)
        If at Is Nothing Then _
            Set at = g_oMPO.Templates(NormalTemplate.Name)
        If InStr(UCase(g_oMPO.db.TemplateDefinitions(at.ID).BaseTemplate), "LETTER") Then
            Me.lstType.Text = "End of Document"
        Else
            Me.lstType.Text = "Footer"
        End If
    End If
    
    If Me.lstType.BoundText = Empty Then
        Me.lstType.SelectedItem = 0
    End If
'---9.7.1020 #4512
    Initializing = False
    DisplayOptions
    On Error GoTo 0
    '---9.7.1 - 4033
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Activating MacPac90.frmDocID"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim xTemplate As String
    Dim xVarName As String
    Dim lValue As Long
    Dim oDoc As MPO.CDocument
    Dim oTemplate As MPO.CTemplate
    
    Initializing = True

'   get template
    On Error Resume Next
    Set oTemplate = g_oMPO.Templates(Word.ActiveDocument.AttachedTemplate)
    On Error GoTo ProcError
    
    If oTemplate Is Nothing Then _
        Set oTemplate = g_oMPO.Templates(NormalTemplate.Name)
        
    g_oMPO.db.TemplateDefinitions.Refresh

'   get letterhead type - stored in doc var
    xVarName = g_oMPO.db.TemplateDefinitions(oTemplate.ID).ShortName & "LH_1_LetterheadType"
    
'   get letterhead type - value will be 0
    Set oDoc = New MPO.CDocument
    
    On Error Resume Next
    Dim xValue As String
    xValue = oDoc.GetVar(xVarName)
    
    If IsNumeric(xValue) Then
        lValue = CLng(xValue)
    End If
    
    Set oDoc = Nothing
    On Error GoTo ProcError
    
'   get available doc stamps
    lstType.Array = g_oMPO.db.DocumentStampDefs(1, oTemplate.ID, lValue).ListSource
    
    
    'MoveToLastPosition Me
    m_bCancelled = True
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Loading MacPac90.frmDocID"
    Exit Sub
End Sub

Private Sub UpdateForm()
    Dim oDoc As MPO.CDocument
    '---9.7.1 - 4033r927
    Dim i As Integer
    
    On Error GoTo ProcError
    
    
    Set oDoc = New MPO.CDocument
    
    If Not g_bUse90Trailer Then
        On Error Resume Next
        Dim lID As Long
        With oDoc
            lID = .GetVar("NewDocStampType")
            If lID > 0 Then
                Me.lstType.BoundText = lID
                Me.chkDraft = .GetVar("85TrailerDraft")
                Me.chkDate = .GetVar("85TrailerDate")
                Me.chkTime = .GetVar("85TrailerTime")
                Me.chkInsertDateAsField = .GetVar("85TrailerDateField")
                
                '---9.7.1 4033r927 - create option checkboxes on the fly
                If g_bAllowTrailerOptions = True Then
                    DisplayOptionCheckboxes
                End If
                '---9.7.1 4033 - grab any values for checkboxes generated on the fly and set
                
                For i = 0 To g_xAROptionalControls.UpperBound(1)
                    With g_xAROptionalControls
                        Me.Controls("chk" & .value(i, 0)).value = oDoc.GetVar("85Trailer" & .value(i, 0))
                    End With
                Next i
                
            End If
        End With
    Else
        With m_oDocID
            Me.lstType.BoundText = .TypeID
            Me.chkDraft = Abs(.IncludeDraftText)
            Me.chkDate = Abs(.IncludeDate)
            Me.chkTime = Abs(.IncludeTime)
            Me.chkInsertDateAsField = Abs(.InsertDateTimeAsField)
            '---9.7.1 4033r927 - create option checkboxes on the fly
            If g_bAllowTrailerOptions = True Then
                DisplayOptionCheckboxes
            End If
        
            '---9.7.1 4033 - grab any values for checkboxes generated on the fly and set -
            '---g_xAROptionalControls has been stored as a m_oDocID prop
            On Error Resume Next
            For i = 0 To .OptionalControlValues.UpperBound(1)
                With .OptionalControlValues
                    Me.Controls("chk" & .value(i, 0)).value = .value(i, 2)
                End With
            Next i
        
        
        End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "MacPac90.frmDocID.UpdateForm"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    
    'SetLastPosition Me
    
    Set m_oDocID = Nothing
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstType_Click()
    '9.7.1020 #4512
    On Error GoTo ProcError
    If Not Me.Initializing Then
        DisplayOptions
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstType_DblClick()
    btnOK_Click
End Sub

Private Sub UpdateDateFieldCheck()

    Me.chkInsertDateAsField.Enabled = (Me.chkDate = vbChecked Or Me.chkTime = vbChecked) And _
                                      (Me.chkDate.Enabled And Me.chkTime.Enabled)

End Sub


Private Sub DisplayOptionCheckboxes(Optional bEnable As Boolean = True)
    '---9.7.1 4033r927 - adds/configures checkboxes
    'according to contents of primary/alt text prop of selected doc stamp def
    Dim xIDString As String
    Dim j As Integer
    Dim k As Integer
    Dim i As Integer
    Dim ctlNew As VB.CheckBox
    Dim iNumRows As Integer
    Dim iVAdj As Integer
    Dim iHAdj As Integer
    Dim m As Integer
    Dim oDMS As CDMS
    Static lOriginalHeight As Long
    Static lBtnOKTop As Long
    Dim oStamp As CDocumentStampDef
    
    If Not g_bAllowTrailerOptions Then Exit Sub
    
    On Error Resume Next
    Set oStamp = g_oMPO.db.DocumentStampDefs.Item(Me.lstType.BoundText)
    
    On Error GoTo 0
    If oStamp Is Nothing Then
        Set oStamp = m_oDocID.Definition
    End If
    
    If lOriginalHeight = 0 Then
        lOriginalHeight = Me.Height
        lBtnOKTop = Me.btnOK.Top
    End If
    
    If g_xAROptionalControls Is Nothing Then
        Set g_xAROptionalControls = g_oMPO.NewXArray
    Else
        '---hide dynamic controls from collection - this allows refresh when changing types w/ different token strings
        For i = 0 To g_xAROptionalControls.UpperBound(1)
            With g_xAROptionalControls
                On Error Resume Next
                Me.Controls("chk" & .value(i, 0)).Visible = False
            End With
        Next
    End If
    
    CreateOptionsArray oStamp
    
    '---determine the number of extra control rows (3 per row)
'    iNumRows = Max(Int((g_xAROptionalControls.UpperBound(1) + 1) / 3), 1)
    iNumRows = IIf(g_xAROptionalControls.UpperBound(1) <> -1, _
                   Max(Int((g_xAROptionalControls.UpperBound(1) + 1) / 3), 1), _
                   0)
    
    'iVAdj = 360
    iVAdj = 360
    iHAdj = 1320
    
    '---resize the form
    Me.Height = lOriginalHeight + (iNumRows * iVAdj)
    Me.btnOK.Top = lBtnOKTop + (iNumRows * iVAdj)
    Me.btnCancel.Top = btnOK.Top
    
    
    '---cycle through array and either create controls or move/display existing hidden controls
    For j = 0 To g_xAROptionalControls.UpperBound(1)
        
        On Error Resume Next
        Err = 0
        Set ctlNew = Me.Controls("chk" & g_xAROptionalControls.value(j, 0))
        
        '---control does not exist, create one
        If Err <> 0 Then
            Set ctlNew = Controls.Add("VB.CheckBox", "chk" & g_xAROptionalControls.value(j, 0), Me)
        End If
        
        With ctlNew
    
            '---rows of 3  reset left adjust every 3rd control
            If CInt(j / 3) = CSng(j / 3) Then
                k = 0
            Else
                k = k + 1
            End If
            
            .Left = chkDraft.Left + k * iHAdj
            
            '---adjust top values for each 3 in a row
            If j > 2 Then
                m = Fix(j / 3) + 1
            Else
                m = 1
            End If
            
            .Top = (chkInsertDateAsField.Top - 120) + iVAdj * m
            
            .Visible = True
            .ZOrder 1
''''            .Caption = "&" & xSubstitute(g_xAROptionalControls.value(j, 0), "_", " ")
            .Caption = g_xAROptionalControls.value(j, 1)
            .Appearance = Me.chkTime.Appearance
            .BackColor = Me.chkTime.BackColor
''''            .Tag = g_xAROptionalControls.value(j, 1)
            .Width = 1300
            .TabIndex = chkInsertDateAsField.TabIndex + j + 1
''''            .value = g_xAROptionalControls.value(j, 2)
            .Enabled = bEnable
        End With
        Set ctlNew = Nothing
    Next

End Sub
Private Sub DisplayOptions()
    Dim oStamp As CDocumentStampDef
    Dim bEnabled As Boolean
    
    On Error GoTo ProcError
    
    Set oStamp = g_oMPO.db.DocumentStampDefs.Item(Me.lstType.BoundText)
    
    bEnabled = oStamp.InsertFirstPage + oStamp.InsertPrimary <> 0
    Me.chkDate.Enabled = bEnabled
    Me.chkDraft.Enabled = bEnabled
    Me.chkTime.Enabled = bEnabled
    UpdateDateFieldCheck
    '---9.7.1 4033 - call the new dynamic checkbox functionality
    DisplayOptionCheckboxes bEnabled
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
    
End Sub
