VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmPleadingCoverPageLocation 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Cover Page / Litigation Back Location"
   ClientHeight    =   1605
   ClientLeft      =   20040
   ClientTop       =   20325
   ClientWidth     =   5490
   Icon            =   "frmPleadingCoverPageLocation.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1605
   ScaleWidth      =   5490
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   4335
      TabIndex        =   2
      Top             =   1080
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   3270
      TabIndex        =   1
      Top             =   1080
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbLocation 
      Height          =   600
      Left            =   1770
      OleObjectBlob   =   "frmPleadingCoverPageLocation.frx":030A
      TabIndex        =   0
      Top             =   180
      Width           =   3615
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      X1              =   120
      X2              =   5595
      Y1              =   975
      Y2              =   975
   End
   Begin VB.Label lblLocation 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "&Location:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   150
      TabIndex        =   3
      Top             =   240
      Width           =   1410
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   1260
      Left            =   -30
      Top             =   -375
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmPleadingCoverPageLocation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_bInitializing As Boolean
Private m_iLocation As Integer

Private m_sTop As Single


'**********************************************************
'   Properties
'**********************************************************
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Let Location(iNew As Integer)
    m_iLocation = iNew
End Property

Public Property Get Location() As Integer
    Location = m_iLocation
End Property


Private Sub btnOK_Click()
    Me.Cancelled = False
    Me.Location = cmbLocation.BoundText
    Me.Hide
End Sub

Private Sub Form_Activate()
        LockWindowUpdate Me.hwnd
        DoEvents
        LockWindowUpdate &O0
        Me.Top = (Screen.Height / 2 - Me.Height / 2)
        Me.Left = (Screen.Width / 2 - Me.Width / 2)
        Me.cmbLocation.BoundText = mpSegmentLocations_NewFirstSection
End Sub

Private Sub Form_Load()
    
    On Error GoTo ProcError
    m_bCancelled = True

'---9.6.1 - load new position combo
    Dim oX As XArray
    
    Set oX = g_oMPO.NewXArray
    oX.ReDim 0, 1, 0, 1
    With oX
        .value(0, 0) = "Start of Document"
        .value(0, 1) = mpSegmentLocations_NewFirstSection
        .value(1, 0) = "End of Document"
        .value(1, 1) = mpSegmentLocations_NewLastSection
    End With
    Me.cmbLocation.Array = oX
    ResizeTDBCombo Me.cmbLocation, 5
'---end 9.6.1
    
    
    Exit Sub
    
ProcError:
    'RaiseError "MPO.frmSelectCoverPageLocation.Form_Load"
    Exit Sub
End Sub

