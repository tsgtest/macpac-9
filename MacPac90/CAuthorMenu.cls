VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAuthorMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'**********************************************************
'   CAuthorMenu Class
'   created 12/15/99 by Daniel Fisherman-
'   momshead@earthlink.net
'   Contains properties and methods that are
'   available to authors in a dialog -
'**********************************************************

Private m_oPersonsForm As MPO.CPersonsForm
Private m_oDoc As MPO.CDocument

'**********************************************************
'   Methods
'**********************************************************
Public Sub AddPerson(oAuthorCombo As Control, ByVal lCurAuthor As Long)
    m_oPersonsForm.AddPerson
    RefreshPeople lCurAuthor, oAuthorCombo
End Sub

Public Sub ToggleFavorite(oAuthorCombo As Control, ByVal lCurAuthor As Long)
    With g_oMPO.People(lCurAuthor)
        .Favorite = Not .Favorite
    End With
    
'   refresh list
    RefreshPeople lCurAuthor, oAuthorCombo
End Sub

Public Sub ManageAuthors(oAuthorCombo As Control, Optional ByVal lCurAuthor As Long)
    If lCurAuthor Then
        g_oMPO.EditPeople lCurAuthor
        RefreshPeople lCurAuthor, oAuthorCombo
    Else
        g_oMPO.EditPeople
        RefreshPeople , oAuthorCombo
    End If
End Sub

Public Sub CopyPerson(oAuthorCombo As Control, ByVal lCurAuthor As Long)
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson

    If TypeOf oAuthorCombo Is TrueDBList60.TDBCombo Then
    '   if author isn't in list, the
    '   author is the reuse author
        If oAuthorCombo.Row = -1 Then
    '       use reuse author as source
            Set oSource = m_oDoc.ReuseAuthor
        Else
    '       use selected author in list as source
            Set oSource = g_oMPO.People(oAuthorCombo.BoundText)
        End If
    ElseIf TypeOf oAuthorCombo Is mpControls.SigningAttorneysGrid Then
    '   if author isn't in list, the
    '   author is the reuse author
        If oAuthorCombo.Array.Row = -1 Then
    '       use reuse author as source 3.22 NOTE:  more work required - test to see if sa grid support reuse author
            Set oSource = m_oDoc.ReuseAuthor
        Else
    '       use selected author in list as source
            Set oSource = g_oMPO.People(oAuthorCombo.Author)
        End If
    End If
'   copy the person
    Set oCopy = m_oPersonsForm.CopyPerson(oSource)

'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, oAuthorCombo
    End If
End Sub

Private Sub Class_Initialize()
    Set m_oPersonsForm = New MPO.CPersonsForm
    Set m_oDoc = New MPO.CDocument
End Sub

Private Sub Class_Terminate()
    Set m_oPersonsForm = Nothing
End Sub
