VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "XXX"
   ClientHeight    =   6210
   ClientLeft      =   1935
   ClientTop       =   630
   ClientWidth     =   5475
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6210
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   7
      Left            =   1935
      TabIndex        =   33
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   6
      Left            =   1920
      TabIndex        =   32
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "Sa&ve"
      Height          =   400
      Left            =   2265
      TabIndex        =   16
      Top             =   5730
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   400
      Left            =   4410
      TabIndex        =   15
      Top             =   5730
      Width           =   1000
   End
   Begin VB.CommandButton btnReset 
      Caption         =   "&Reset"
      Height          =   400
      Left            =   3315
      TabIndex        =   14
      Top             =   5730
      Width           =   1035
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   1785
      TabIndex        =   11
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   1785
      TabIndex        =   10
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   1785
      TabIndex        =   9
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   1785
      TabIndex        =   8
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   4
      Left            =   1785
      TabIndex        =   7
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.TextBox txtCustom 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   5
      Left            =   1785
      TabIndex        =   6
      Top             =   1140
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   0
      Left            =   1920
      TabIndex        =   5
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   1
      Left            =   1920
      TabIndex        =   4
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   2
      Left            =   1920
      TabIndex        =   3
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   3
      Left            =   1920
      TabIndex        =   2
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   4
      Left            =   1920
      TabIndex        =   1
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.CheckBox chkCustom 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   5
      Left            =   1920
      TabIndex        =   0
      Top             =   2280
      Visible         =   0   'False
      Width           =   1500
   End
   Begin TrueDBList60.TDBCombo cmbAuthor 
      Height          =   375
      Left            =   1770
      OleObjectBlob   =   "frmOptions.frx":000C
      TabIndex        =   12
      Top             =   120
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   0
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":3DB9
      TabIndex        =   13
      Top             =   1680
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   1
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":5FBD
      TabIndex        =   41
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   2
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":81C1
      TabIndex        =   42
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   3
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":A3C5
      TabIndex        =   43
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   4
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":C5C9
      TabIndex        =   44
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   5
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":E7CD
      TabIndex        =   45
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   6
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":109D1
      TabIndex        =   46
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbCustom 
      Height          =   315
      Index           =   7
      Left            =   1785
      OleObjectBlob   =   "frmOptions.frx":12BD5
      TabIndex        =   47
      Top             =   1695
      Visible         =   0   'False
      Width           =   3600
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   6
      Left            =   135
      TabIndex        =   40
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   5
      Left            =   135
      TabIndex        =   39
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   4
      Left            =   135
      TabIndex        =   38
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   3
      Left            =   135
      TabIndex        =   37
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   2
      Left            =   135
      TabIndex        =   36
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   1
      Left            =   135
      TabIndex        =   35
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomLabel 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustomLabel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   0
      Left            =   135
      TabIndex        =   34
      Top             =   2850
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   7
      Left            =   135
      TabIndex        =   31
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   6
      Left            =   135
      TabIndex        =   30
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   0
      Left            =   135
      TabIndex        =   29
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   1
      Left            =   135
      TabIndex        =   28
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   2
      Left            =   135
      TabIndex        =   27
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   3
      Left            =   135
      TabIndex        =   26
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   4
      Left            =   135
      TabIndex        =   25
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomCombo 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Combo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   5
      Left            =   135
      TabIndex        =   24
      Top             =   1755
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblAuthor 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Author:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Left            =   105
      TabIndex        =   23
      Top             =   165
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   5
      Left            =   105
      TabIndex        =   22
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   4
      Left            =   105
      TabIndex        =   21
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   3
      Left            =   105
      TabIndex        =   20
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   2
      Left            =   105
      TabIndex        =   19
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   1
      Left            =   105
      TabIndex        =   18
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCustomText 
      Alignment       =   1  'Right Justify
      BackStyle       =   0  'Transparent
      Caption         =   "lblCustom Text"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   425
      Index           =   0
      Left            =   105
      TabIndex        =   17
      Top             =   1125
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H80000003&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      FillColor       =   &H00800080&
      Height          =   9495
      Left            =   -120
      Top             =   -225
      Width           =   1800
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_EditAuthorOptions 
         Caption         =   "&Edit Options..."
      End
      Begin VB.Menu mnuAuthor_CopyOptions 
         Caption         =   "Cop&y Options..."
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Const mpCaptionRoot As String = "Author Defaults"
Private Const mpTemplate As String = "XXX"
Private Const mpMaxTextBoxes As Byte = 6
Private Const mpMaxComboBoxes As Byte = 8
Private Const mpMaxCheckBoxes As Byte = 6
Private Const mpMaxLabels As Byte = 6

Private m_iNextPosition As Integer
Private m_iNumTextBoxes As Integer
Private m_iNumComboBoxes As Integer
Private m_iNumCheckBoxes As Integer
Private m_iNumLabels As Integer

Private m_bInitializing As Boolean
Private m_bUpdateDoc As Boolean
Private m_oOptions As mpDB.CPersonOptions
Private m_oPrevCtl As Control
Private m_oOptCtls As mpDB.cOptionsControls
Private m_bCancelled As Boolean
Private m_DefaultAuthor As mpDB.CPerson
Private m_oAuthor As mpDB.CPerson
Private m_oDocObj As MPO.IDocObj
Private m_bDirty As Boolean
Public Event DefaultOptionsChange()
Public Event CurrentDocAuthorOptionsChange()

Public Property Let DocObject(oNew As MPO.IDocObj)
    Set m_oDocObj = oNew
End Property

Public Property Get DocObject() As MPO.IDocObj
    Set DocObject = m_oDocObj
End Property

Public Property Get Template() As String
    Template = Me.DocObject.Template.ID
End Property

Public Property Let Author(oNew As mpDB.CPerson)
    Set m_oAuthor = oNew
    
'   update form to reflect new Author
    Set m_oOptions = m_oAuthor.Options(Me.Template)
End Property

Public Property Get Author() As mpDB.CPerson
    Set Author = m_oAuthor
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property

Private Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property

Public Sub UpdateForm()
'cycle through visible controls, getting
'appropriate value from DocObject property
    Dim oCtl As Control

    
    For Each oCtl In Me.Controls
'       check for tag - only controls with a tag
'       are hooked up to DocObject properties
        If Len(oCtl.Tag) Then
            If TypeOf oCtl Is VB.TextBox Then
                oCtl.Text = m_oOptions.Item(oCtl.Tag).Value
            ElseIf TypeOf oCtl Is VB.CheckBox Then
                If m_oOptions.Item(oCtl.Tag).Value Then
                    oCtl.Value = vbChecked
                Else
                    oCtl.Value = vbUnchecked
                End If
            ElseIf TypeOf oCtl Is TDBCombo Then
                oCtl.BoundText = m_oOptions.Item(oCtl.Tag).Value
            End If
        End If
    Next oCtl
    
    SetFormCaption
    DoEvents
    m_bDirty = False
End Sub

Private Sub chkCustom_Click(Index As Integer)
    m_bDirty = True
End Sub

Private Sub cmbCustom_Change(Index As Integer)
    m_bDirty = True
End Sub

Private Sub Form_Activate()
'   select person if supplied - else
'   select the first person in the list
    With Me.cmbAuthor
        If (Me.DocObject.Author Is Nothing) Then
            .BoundText = .Columns(1)
        Else
            .BoundText = Me.DocObject.Author.ID
        End If
    End With
    UpdateForm
End Sub

'**********************************************************
'   Form Event Procedures
'**********************************************************
Private Sub Form_Load()
    Dim i As Integer
    Dim oCurCtl As Control
    Dim iCtlIndex As Integer
    
    On Error GoTo ProcError
    Word.Application.ScreenUpdating = False
    m_bInitializing = True
    
'   set up dialog
    SetUpDialog
    
'   get controls for form
    Set m_oOptCtls = g_oDBs.OptionsControls(Me.Template)

    For i = 1 To m_oOptCtls.Count
        With m_oOptCtls(i)
'           get control to which this custom
'           control will be assigned
            Select Case .ControlType
                Case mpOptionsControl_TextBox
                    Set oCurCtl = SetUpTextBox(m_oOptCtls(i))
                Case mpOptionsControl_ComboBox, _
                     mpOptionsControl_ListBox
                    Set oCurCtl = SetUpComboList(m_oOptCtls(i))
                Case mpOptionsControl_CheckBox
                    Set oCurCtl = SetUpCheckBox(m_oOptCtls(i))
                Case mpOptionsControl_Label
                    Set oCurCtl = SetUpLabel(m_oOptCtls(i))
            End Select
            
'           set this control as the previous control
            Set m_oPrevCtl = oCurCtl
        End With
    Next i
    
'   set tab indexes of cmd buttons
    Me.btnSave.TabIndex = 1000
    Me.btnReset.TabIndex = 1000
    Me.btnCancel.TabIndex = 1000
    
'   set dlg title
    SetFormCaption
    
'   setup author
    Me.Author = Me.DocObject.Author
    With Me.cmbAuthor
        .Array = g_oDBs.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
        DoEvents
    End With
    
    mpBase2.MoveToLastPosition Me
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              "MPO.frmOptions.FormLoad", _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub SetUpDialog()
'assigns dialog properties
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              "MPO.frmCustomDocObj.Form_Load", _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Function SetUpLabel(oCustCtl As mpDB.cOptionsControl) As VB.Label
'sets up the next available textbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.Label
    
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumLabels = mpMaxLabels Then
            xMsg = "Too many labels defined.  " & _
                   "Each options dialog is limited to " & _
                    mpMaxLabels & " textboxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumLabels
        
'       set up the text box in the array with specified index
        Set oCtl = Me.lblCustomLabel(iCtlIndex)
        oCtl.Caption = oCustCtl.Caption
        oCtl.Visible = True
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomLabel(iCtlIndex)
        
'       increment the number of labels used
        m_iNumLabels = m_iNumLabels + 1
    End With

'   return control
    Set SetUpLabel = oCtl
End Function


Private Function SetUpTextBox(oCustCtl As mpDB.cOptionsControl) As VB.TextBox
'sets up the next available textbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCtl As VB.TextBox
    
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumTextBoxes = mpMaxTextBoxes Then
            xMsg = "Too many text boxes defined.  " & _
                   "Each options dialog is limited to " & mpMaxTextBoxes & " textboxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumTextBoxes
        
        
'       set up the text box in the array with specified index
        Set oCtl = Me.txtCustom(iCtlIndex)
        oCtl.Tag = .OptionsField
        oCtl.Visible = True
        
'       set up corresponding label
        Me.lblCustomText(iCtlIndex).Caption = .Caption & ":"
        Me.lblCustomText(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, oCtl, Me.lblCustomText(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumTextBoxes = m_iNumTextBoxes + 1
    End With

'   return control
    Set SetUpTextBox = oCtl
End Function

Private Function SetUpCheckBox(oCustCtl As mpDB.cOptionsControl) As VB.CheckBox
'sets up the next available checkbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumCheckBoxes = mpMaxCheckBoxes Then
            xMsg = "Too many check boxes defined.  " & _
                   "Each options dialog is limited to " & _
                    mpMaxCheckBoxes & " check boxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumCheckBoxes
        
'       set up the text box in the array with specified index
        Me.chkCustom(iCtlIndex).Tag = .OptionsField
        Me.chkCustom(iCtlIndex).Caption = .Caption
        Me.chkCustom(iCtlIndex).Visible = True
        
'       position controls
        PositionControl oCustCtl, Me.chkCustom(iCtlIndex)
        
'       increment the number of textboxes used on the specified tab
        m_iNumCheckBoxes = m_iNumCheckBoxes + 1
    End With

'   return control
    Set SetUpCheckBox = Me.chkCustom(iCtlIndex)
End Function

Private Function SetUpComboList(oCustCtl As mpDB.cOptionsControl) As TDBCombo
'sets up the next available textbox on the specified tab
'to have the properties of the MacPac custom control definition
    Dim iCtlIndex As Integer
    Dim oCombo As TDBCombo
    Dim oLabel As VB.Label
    
    With oCustCtl
'       alert & exit if too many textboxes have been defined
        If m_iNumComboBoxes = mpMaxComboBoxes Then
            xMsg = "Too many combo/list boxes defined .  " & _
                   "Each options dialog is limited to " & mpMaxComboBoxes & " combo/list boxes."
            MsgBox xMsg, vbExclamation, App.Title
            Exit Function
        End If
                   
'       get next available text box for the specified tab
        iCtlIndex = m_iNumComboBoxes
        
'       set up the combo box with specified index in the array
        Set oCombo = Me.cmbCustom(iCtlIndex)
        oCombo.Tag = .OptionsField
        
'       set to list box if limited to list entries
        If .ControlType = mpOptionsControl_ListBox Then
            oCombo.ComboStyle = 2
        End If
        
        oCombo.Visible = True
        
        If Not (.List Is Nothing) Then
            oCombo.Array = .List.ListItems.Source
            ResizeTDBCombo oCombo, 5
        End If
        
'       set up label
        Set oLabel = Me.lblCustomCombo(iCtlIndex)
        oLabel.Caption = .Caption & ":"
        oLabel.Visible = True
        
'       position controls
        PositionControl oCustCtl, _
                        oCombo, _
                        oLabel
        
'       increment the number of combos used on the specified tab
        m_iNumComboBoxes = m_iNumComboBoxes + 1
    End With

'   return control
    Set SetUpComboList = oCombo
End Function

Private Sub PositionControl(oDef As mpDB.cOptionsControl, _
                            oCtl As Control, _
                            Optional oLabel As VB.Label)
'   position control and corresponding
'   label based on position of previous
'   control and space before for current control
    Dim sPrevTop As Single
    Dim sPrevHeight As Single
    Dim sPrevLeft As Single
    Dim iPrevTabIndex As Integer
    Dim oPrevCtl As Control
    
    Set oPrevCtl = m_oPrevCtl
    
'   test for previous control
    If (oPrevCtl Is Nothing) Then
'       no previous control - base position on author control
        With Me.cmbAuthor
            sPrevTop = .Top
            sPrevHeight = .Height
            sPrevLeft = .Left
            iPrevTabIndex = .TabIndex
        End With
    Else
        With oPrevCtl
'           position control based on previous control
            sPrevTop = .Top
            sPrevHeight = .Height
            sPrevLeft = .Left
            iPrevTabIndex = .TabIndex
        End With
    End If

'   execute
    With oCtl
        .Left = Me.cmbAuthor.Left + oDef.HorizontalOffset
        .Top = sPrevTop + oDef.VerticalOffset
        If oDef.Width > 0 Then
            .Width = oDef.Width
        End If
        If Not (oLabel Is Nothing) Then
'           chkboxes don't have associated label controls-
'           position labels for all other controls
            oLabel.Left = 105
            oLabel.Top = .Top + 50
        End If

'       set tab index
        If Not (oLabel Is Nothing) Then
            oLabel.TabIndex = iPrevTabIndex + 1
            .TabIndex = iPrevTabIndex + 2
        Else
            .TabIndex = iPrevTabIndex + 1
        End If
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    mpBase2.SetLastPosition Me
    
    Set m_oOptCtls = Nothing
End Sub

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub SetFormCaption()
    Me.Caption = Me.DocObject.Template.ShortName & " " & mpCaptionRoot
End Sub

Private Sub SaveOptions()
    Me.MousePointer = vbHourglass

    If m_bDirty Then
'       update options with dlg values
        UpdateObject
        
'       save record
        m_oOptions.Save
        
'       broadcast that options have changed
'       for default letter author
        If Me.Author.ID = Me.DocObject.DefaultAuthor.ID Then
            RaiseEvent DefaultOptionsChange
        End If

'       broadcast that options have changed
'       for current letter author
        If Me.Author.ID = Me.DocObject.Author.ID Then
            m_bUpdateDoc = True
        End If
        
        m_bDirty = False
    End If
    Me.MousePointer = vbDefault
End Sub

Private Sub UpdateObject()
'update each member of the option set
'with the value in the associated control
    Dim i As Integer
    Dim oCtl As Control
    Dim oOpt As mpDB.CPersonOption
    
'   cycle through all controls
    For Each oCtl In Me.Controls
'       check only tagged controls - these
'       are the ones associated with an option
        If Len(oCtl.Tag) Then
'           get option to update from the tag
            Set oOpt = m_oOptions.Item(oCtl.Tag)
            
'           set option value = value of control
            If TypeOf oCtl Is TDBCombo Then
                If Len(oCtl.BoundText) Then
                    oOpt.Value = oCtl.BoundText
                Else
                    oOpt.Value = oCtl.Text
                End If
            ElseIf TypeOf oCtl Is TDBList Then
                If Len(oCtl.BoundText) Then
                    oOpt.Value = oCtl.BoundText
                Else
                    oOpt.Value = oCtl.Text
                End If
            ElseIf TypeOf oCtl Is VB.TextBox Then
                oOpt.Value = oCtl.Text
            Else
                If Not IsNull(oCtl.Value) Then
                    oOpt.Value = oCtl.Value
                End If
            End If
        End If
    Next oCtl
    m_bDirty = True
    m_oOptions.IsDirty = True
End Sub

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
End Sub

Private Sub btnReset_Click()
    Dim iResult As VbMsgBoxResult
    Dim xMsg As String
    
'   confirm deletion
    xMsg = "Reset the letter options for " & _
        Me.Author.FullName & " to the firm defaults?"
    iResult = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
    
    If iResult = vbYes Then
'       delete letter options for selected person
        m_oOptions.Delete
        m_oOptions.Refresh
        
'       refresh dlg values
        UpdateForm
    End If
    
    Me.cmbAuthor.SetFocus
End Sub

Private Sub btnSave_Click()
    SaveOptions
    Me.cmbAuthor.SetFocus
End Sub

Private Sub cmbAuthor_ItemChange()
    Me.Author = g_oDBs.People.Item(Me.cmbAuthor.BoundText)
    UpdateForm
End Sub

Private Sub txtCustom_Change(Index As Integer)
    m_bDirty = True
End Sub
