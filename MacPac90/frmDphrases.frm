VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmDPhrases 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert Delivery Phrases"
   ClientHeight    =   6540
   ClientLeft      =   3825
   ClientTop       =   1080
   ClientWidth     =   5460
   Icon            =   "frmDphrases.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6540
   ScaleWidth      =   5460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin TrueDBList60.TDBList lstAction 
      Height          =   480
      Left            =   1800
      OleObjectBlob   =   "frmDphrases.frx":000C
      TabIndex        =   7
      Top             =   4725
      Width           =   3540
   End
   Begin VB.CommandButton btnClear 
      Caption         =   "&Clear"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   4350
      TabIndex        =   3
      Top             =   2895
      Width           =   1000
   End
   Begin VB.CommandButton btnAdd 
      Caption         =   "&Add"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   3270
      TabIndex        =   2
      Top             =   2895
      Width           =   1000
   End
   Begin VB.TextBox txtDPhrases 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   1785
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   3405
      Width           =   3555
   End
   Begin TrueDBList60.TDBList lstDphrases 
      Height          =   2550
      Left            =   1770
      OleObjectBlob   =   "frmDphrases.frx":20F5
      TabIndex        =   1
      Top             =   225
      Width           =   3555
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   4335
      TabIndex        =   11
      Top             =   6060
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   3270
      TabIndex        =   10
      Top             =   6060
      Width           =   1000
   End
   Begin TrueDBList60.TDBList lstScope 
      Height          =   480
      Left            =   1800
      OleObjectBlob   =   "frmDphrases.frx":40B0
      TabIndex        =   9
      Top             =   5370
      Width           =   3540
   End
   Begin VB.Label lblScope 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Scope:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1035
      TabIndex        =   8
      Top             =   5385
      Width           =   510
   End
   Begin VB.Label lblAction 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Act&ion:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1020
      TabIndex        =   6
      Top             =   4740
      Width           =   510
   End
   Begin VB.Label lblCurrent 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "C&urrent Phrases:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   315
      TabIndex        =   4
      Top             =   3390
      Width           =   1230
   End
   Begin VB.Label lblDPhrases 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Available &Phrases:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   195
      TabIndex        =   0
      Top             =   225
      Width           =   1350
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H80000003&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6975
      Left            =   -30
      Top             =   -390
      Width           =   1695
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmDPhrases"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_oDoc As CDocument

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

'**********************************************************
'   Form Procedures
'**********************************************************

Private Sub Form_Activate()
    Me.Initializing = False
End Sub

Private Sub Form_Load()
    Dim xDocDPhrase As String
    
    On Error Resume Next
    Set m_oDoc = New CDocument
    Me.Initializing = True
    
'*  fill list
    With Me.lstDphrases
        .Array = g_oDBs.Lists.Item("DPhrases") _
                    .ListItems.Source
        .SelectedItem = .FirstRow
    End With

'*  get dphrase text
    xDocDPhrase = m_oDoc.GetDocStyleText("Delivery Phrase", , , , True)
    xDocDPhrase = xTrimTrailingChrs(xDocDPhrase, vbCr, True)
    Me.txtDPhrases.Text = xSubstitute(xDocDPhrase, Chr(11), vbCrLf)
    
'*  set action controls
    With Me.lstAction
        .Array = xarStringToxArray("Insert|Replace")
        .SelectedItem = .FirstRow
    End With
    
'*  set scope controls
    With Me.lstScope
        .Array = xarStringToxArray("Current Section|Document")
        .SelectedItem = .FirstRow
    End With
    
    bUpdateScope
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    Set m_oDoc = Nothing
End Sub

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
End Sub

Private Sub btnOK_Click()
    UpdateDPhrases
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
End Sub

Private Sub btnAdd_Click()
    bAddDPhrase Me.lstDphrases.Text
    With Me.lstDphrases
        .Refresh
        .SetFocus
    End With
End Sub

Private Sub btnClear_Click()
    Me.txtDPhrases = ""
End Sub

Private Sub lstAction_Click()
    Call bUpdateScope
End Sub

Private Sub lstAction_RowChange()
    Call bUpdateScope
End Sub

Private Sub lstDphrases_DblClick()
    With Me.lstDphrases
        bAddDPhrase .Text
        .Refresh
    End With
End Sub

Private Sub lstDphrases_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        With Me.lstDphrases
            bAddDPhrase .Text
            .Refresh
        End With
    End If
End Sub

Private Sub txtDPhrases_GotFocus()
    bEnsureSelectedContent Me.txtDPhrases, True
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************

Private Sub UpdateDPhrases()
    Dim bRet As Boolean
    
    If InStr(UCase(Me.lstAction.Text), "INSERT") Then
        m_oDoc.InsertText Me.txtDPhrases, _
                          "Delivery Phrase", _
                          , _
                          True
    Else
        If InStr(UCase(Me.lstScope.Text), "SECTION") Then
'*          update any text with 'dphrase' style applied
            bRet = m_oDoc.bReplaceStyle(Me.txtDPhrases, _
                                        "Delivery Phrase", , _
                                        True)
        
        Else
'*          update any text with 'dphrase' style applied
            bRet = m_oDoc.bReplaceStyle(Me.txtDPhrases, _
                                        "Delivery Phrase")
        End If
    End If

'*  save doc var for reuse
    m_oDoc.SaveItem "DeliveryPhrases", , , _
                    Me.txtDPhrases
End Sub

Private Function bAddDPhrase(xDPhrase As String) As Boolean

    Dim xCurDPhrase As String
    Dim xNewDPhrase As String
    Dim xSep As String
    
    xNewDPhrase = AppendToDPhrase(xDPhrase)
    xCurDPhrase = Me.txtDPhrases
    
'   trim trailing paras to
'   ensure proper insertion
    xCurDPhrase = xTrimTrailingChrs(xCurDPhrase, vbCrLf)
    
    If xCurDPhrase <> "" Then
        xSep = vbCrLf
    End If

    Me.txtDPhrases = xCurDPhrase & xSep & xNewDPhrase
    
End Function

Private Sub bUpdateScope()
    Dim bEnable As Boolean
    
    With Me
        bEnable = InStr(UCase(Me.lstAction.Text), _
                        "REPLACE")
        .lstScope.Visible = bEnable
        .lblScope.Visible = bEnable
    End With
    
End Sub
