VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmPleadingCaptionNOCASEINFO 
   Appearance      =   0  'Flat
   BackColor       =   &H80000004&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Create a Pleading Caption/Cross-Action"
   ClientHeight    =   6465
   ClientLeft      =   3285
   ClientTop       =   1080
   ClientWidth     =   5475
   Icon            =   "frmPleadingCaptionNOCASEINFO.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6465
   ScaleWidth      =   5475
   ShowInTaskbar   =   0   'False
   WhatsThisHelp   =   -1  'True
   Begin VB.TextBox txtParty2Name 
      Appearance      =   0  'Flat
      Height          =   510
      Left            =   1770
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   15
      Top             =   3465
      Width           =   3615
   End
   Begin VB.TextBox txtParty1Name 
      Appearance      =   0  'Flat
      Height          =   510
      Left            =   1770
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   2385
      Width           =   3615
   End
   Begin VB.TextBox txtAdditionalInformation 
      Appearance      =   0  'Flat
      Height          =   510
      Left            =   1770
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   20
      Top             =   5235
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtParty3Name 
      Appearance      =   0  'Flat
      Height          =   510
      Left            =   1770
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   18
      Top             =   4515
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4905
      Picture         =   "frmPleadingCaptionNOCASEINFO.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Delete caption (F7)"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4425
      Picture         =   "frmPleadingCaptionNOCASEINFO.frx":0494
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Save new caption (F6)"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnNew 
      Height          =   400
      Left            =   3960
      Picture         =   "frmPleadingCaptionNOCASEINFO.frx":0612
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Add a new caption (F5)"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   3795
      TabIndex        =   21
      Top             =   5970
      Width           =   750
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   4605
      TabIndex        =   22
      Top             =   5970
      Width           =   750
   End
   Begin TrueDBList60.TDBList lstCaptions 
      Height          =   855
      Left            =   1770
      OleObjectBlob   =   "frmPleadingCaptionNOCASEINFO.frx":0B54
      TabIndex        =   4
      Top             =   540
      Width           =   3570
   End
   Begin TrueDBList60.TDBCombo cmbCaptionType 
      Height          =   390
      Left            =   1770
      OleObjectBlob   =   "frmPleadingCaptionNOCASEINFO.frx":2A97
      TabIndex        =   6
      Top             =   1590
      Width           =   3630
   End
   Begin TrueDBList60.TDBCombo cmbParty2Title 
      Height          =   390
      Left            =   1770
      OleObjectBlob   =   "frmPleadingCaptionNOCASEINFO.frx":4CD1
      TabIndex        =   13
      Top             =   3150
      Width           =   3630
   End
   Begin TrueDBList60.TDBCombo cmbParty1Title 
      Height          =   390
      Left            =   1770
      OleObjectBlob   =   "frmPleadingCaptionNOCASEINFO.frx":6EF3
      TabIndex        =   8
      Top             =   2070
      Width           =   3630
   End
   Begin TrueDBList60.TDBCombo cmbParty3Title 
      Height          =   390
      Left            =   1770
      OleObjectBlob   =   "frmPleadingCaptionNOCASEINFO.frx":9115
      TabIndex        =   17
      Top             =   4200
      Visible         =   0   'False
      Width           =   3630
   End
   Begin VB.Label lblVersus 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "v."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   3420
      TabIndex        =   11
      Top             =   2910
      Width           =   135
   End
   Begin VB.Label lblCaptionType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Type:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   90
      TabIndex        =   5
      Top             =   1650
      Width           =   1455
   End
   Begin VB.Label lblParty1Name 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Party &1:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   90
      TabIndex        =   7
      Top             =   2100
      Width           =   1455
   End
   Begin VB.Label lblParty2Name 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Party &2:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   90
      TabIndex        =   12
      Top             =   3180
      Width           =   1455
   End
   Begin VB.Label lblAdditionalInformation 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Additional Info:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   405
      Left            =   -150
      TabIndex        =   19
      Top             =   5235
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblParty3Name 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Party &3:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   90
      TabIndex        =   16
      Top             =   4230
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblVersus2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "v."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1410
      TabIndex        =   9
      Top             =   2610
      Visible         =   0   'False
      Width           =   135
   End
   Begin VB.Label lblVersus3 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "And"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   1215
      TabIndex        =   14
      Top             =   3690
      Visible         =   0   'False
      Width           =   330
   End
   Begin VB.Label lblCaptions 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Caption:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   960
      TabIndex        =   3
      Top             =   540
      Width           =   585
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H80000003&
      Height          =   7350
      Left            =   30
      Top             =   -855
      Width           =   1695
   End
   Begin VB.Menu mnuLayout 
      Caption         =   "Caption Layout"
      Visible         =   0   'False
      Begin VB.Menu mnuCLAdd 
         Caption         =   "&Add Caption"
      End
      Begin VB.Menu mnuCLChangeDisplayName 
         Caption         =   "&Change Display Name..."
      End
      Begin VB.Menu mnuCLDeleteOne 
         Caption         =   "&Delete Caption"
      End
      Begin VB.Menu mnuCLDeleteAll 
         Caption         =   "Delete &All Captions"
      End
   End
End
Attribute VB_Name = "frmPleadingCaptionNOCASEINFO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const mpCancelAdd As String = "Cancel unsaved caption (F7)"
Const mpDelete As String = "Delete this caption (F7)"
Const mpCancelChange As String = "Cancel unsaved changes (F7)"
Const mpSaveChanges As String = "Save changes to this caption (F6)"
Const mpSaveNew As String = "Save new caption (F6)"


Private m_Cap As MPO.CPleadingCaption
Private m_Caps As MPO.CPleadingCaptions
Private m_Document As MPO.CDocument
Private m_oPleadingType As mpDB.CPleadingDef
Private m_oDB As mpDB.CDatabase
Private m_bCascadeUpdates As Boolean
Private m_bCancelled As Boolean
Private m_bAdded As Boolean
Private m_bChanged As Boolean
Private m_oParent As Form
Private WithEvents m_oCap As MPO.CPleadingCaption
Attribute m_oCap.VB_VarHelpID = -1
Private WithEvents m_oCaps As MPO.CPleadingCaptions
Attribute m_oCaps.VB_VarHelpID = -1

Private m_bInitializing As Boolean

'****************************************************************
'Properties
'****************************************************************
Public Property Get PleadingCaption() As MPO.CPleadingCaption
    Set PleadingCaption = m_oCap
End Property
Public Property Let PleadingCaption(pldCapNew As MPO.CPleadingCaption)
    Set m_oCap = pldCapNew
End Property
Public Property Get PleadingCaptions() As MPO.CPleadingCaptions
    Set PleadingCaptions = m_oCaps
End Property
Public Property Let PleadingCaptions(pldCapsNew As MPO.CPleadingCaptions)
    Set m_oCaps = pldCapsNew
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property
Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get PleadingType() As mpDB.CPleadingDef
    Set PleadingType = m_oPleadingType
End Property
Public Property Let PleadingType(oNew As mpDB.CPleadingDef)
    Set m_oPleadingType = oNew
End Property
Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property
Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
    Me.CascadeUpdates = False
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = True
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Public Property Get Added() As Boolean
    Added = m_bAdded
End Property
Public Property Let Added(bNew As Boolean)
    m_bAdded = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    Me.lstCaptions.Enabled = Not bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelAdd
        Me.btnSave.ToolTipText = mpSaveNew
    Else
        Me.btnDelete.ToolTipText = mpDelete
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Changed() As Boolean
    Changed = m_bChanged
End Property
Public Property Let Changed(bNew As Boolean)
    If Me.Initializing Then Exit Property
    m_bChanged = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelChange
        Me.btnSave.ToolTipText = mpSaveChanges
    Else
        Me.btnDelete.ToolTipText = mpDelete
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Level0() As Long
    On Error Resume Next
    Level0 = m_Document.RetrieveItem("Level0", "Pleading")
End Property
Public Property Get Level1() As Long
    On Error Resume Next
    Level1 = m_Document.RetrieveItem("Level1", "Pleading")
End Property
Public Property Get Level2() As Long
    On Error Resume Next
    Level2 = m_Document.RetrieveItem("Level2", "Pleading")
End Property
Public Property Get Level3() As Long
    On Error Resume Next
    Level3 = m_Document.RetrieveItem("Level3", "Pleading")
End Property

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Cancelled = True
    Me.Hide
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnDelete_Click()
    On Error GoTo ProcError
    If Me.Changed Then
        Me.Changed = False
        ChangeActiveCaption
    Else
        DeleteCaption
        Me.Changed = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    
    Me.Cancelled = False
    
     If Me.Added Then
        If PromptToSave = True Then
            RefreshList
            Me.cmbCaptionType.SetFocus
            Exit Sub
        End If
    End If
    
    If Me.Changed Then
'       current record is dirty -
'       save automatically if there is
'       only 1 recipient, else prompt
        If Me.PleadingCaptions.Count = 1 Then
            UpdateCaption
        Else
            PromptToSave
        End If
    End If
    
    DoEvents
    Me.Hide
    Application.ScreenUpdating = False
    If ParentForm Is Nothing Then
        PleadingCaptions.Finish
    Else
        PleadingCaptions.Refresh , True, True
    End If
    Application.ScreenUpdating = True
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error finishing caption."
    Exit Sub
End Sub

Private Sub btnNew_Click()
    Dim i
    On Error GoTo ProcError
    
    Me.Changed = False
    CascadeUpdates = False
    NewCaption
    Me.cmbCaptionType.SetFocus
    For i = 1 To 30
        DoEvents
    Next
    Me.Added = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnSave_Click()
    Dim iRow As Integer
    Dim bRet As Boolean
    On Error GoTo ProcError
    EchoOff
    iRow = lstCaptions.Bookmark
    
    bRet = SaveCaption
    If Not bRet Then
        Exit Sub
    End If
    
    RefreshList
    lstCaptions.Bookmark = iRow
    
    If Me.Added Then
        Me.Added = False
    Else
        Me.Changed = False
    End If
    lstCaptions.SetFocus
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Exit Sub
End Sub

''**************************************************
''Control Event Procedures
''**************************************************
Private Sub cmbCaptionType_GotFocus()
    OnControlGotFocus cmbCaptionType
    CascadeUpdates = True
End Sub

Private Sub cmbCaptionType_ItemChange()
    'If Me.Initializing Then Exit Sub
    If CascadeUpdates = True Then Me.Changed = True
    DisplayControls
    SetInterfaceControls
    CascadeUpdates = False
End Sub

Private Sub cmbCaptionType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCaptionType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCaptionType_Validate(Cancel As Boolean)
    If cmbCaptionType.Text = "" Then Exit Sub
    On Error GoTo ProcError
    On Error Resume Next
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCaptionType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbParty1Title_ItemChange()
    Me.Changed = True
End Sub

Private Sub cmbParty2Title_ItemChange()
    Me.Changed = True
End Sub

Private Sub cmbParty3Title_ItemChange()
    Me.Changed = True
End Sub

Private Sub Form_Activate()
    Dim bFlag As Boolean
    On Error GoTo ProcError
    If Not Me.ParentForm Is Nothing Then
        With Me.PleadingCaption
            .TypeID = ParentForm.PleadingCaption.TypeID
        End With
    ElseIf PleadingCaptions.Count = 0 Then
    '---use defaults to set values
    
    
    Else
        
    End If
    
    Me.btnDelete.ToolTipText = mpDelete
    Me.btnSave.ToolTipText = mpSaveChanges
    
    Me.CascadeUpdates = False
    DoEvents
''---this will force new input if there's only one caption
'    If PleadingCaptions.Count = 1 Then
        btnNew_Click
        bFlag = True
'    Else
'        Me.Added = False
'        Me.btnNew.SetFocus
'    End If
    
    If Me.ParentForm Is Nothing Then
        mpBase2.MoveToLastPosition Me
    Else
        Me.Top = Me.ParentForm.Top
        Me.Left = Me.ParentForm.Left
    End If
    LockWindowUpdate (0&)
    Application.ScreenUpdating = True
    DoEvents
    Me.Initializing = False
    Me.Changed = False
    
    If bFlag = True Then
        Me.Added = True
        btnNew.Enabled = False
        btnSave.Enabled = True
        Me.btnDelete.ToolTipText = mpCancelAdd
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error activating pleading caption form."
    LockWindowUpdate (0&)
    Application.ScreenUpdating = True
    Application.ScreenUpdating = True
    Exit Sub
End Sub
Private Sub Form_Load()
    Dim oParties As XArray
    Dim oList As mpDB.CList
    On Error GoTo ProcError
    
    Me.Initializing = True
    Me.CascadeUpdates = False
    Application.ScreenUpdating = False
    Me.Top = 20000
    Me.Cancelled = True
    
    If Me.PleadingCaptions Is Nothing Then
        Me.PleadingCaptions = g_oMPO.NewPleadingCaptions
        Me.PleadingCaptions.LoadValues
        If Me.PleadingCaptions.Count = 0 Then PleadingCaptions.Add
    End If

    Set m_Document = New MPO.CDocument
    Set m_oDB = g_oMPO.DB
    If PleadingCaptions.Count Then PleadingCaption = Me.PleadingCaptions(1)

    If Not Me.ParentForm Is Nothing Then
        Me.PleadingType = Me.ParentForm.Pleading.Definition
    Else
        Me.PleadingType = m_oDB.PleadingDefs.ItemFromCourt( _
            Me.Level0, Me.Level1, Me.Level2, Me.Level3)
    End If
    
'---fill combo boxes
    Set oParties = g_oMPO.Lists("PleadingParties").ListItems.Source
    Me.cmbParty1Title.Array = oParties
    Me.cmbParty2Title.Array = oParties
    Me.cmbParty3Title.Array = oParties
    
'---fill  caption list box
    Me.lstCaptions.Array = Me.PleadingCaptions.ListArray
        
'---fill caption types combo box with types appropriate to pleading type
    Set oList = g_oMPO.Lists.Item("PleadingCaptions")
    With oList
        .FilterField = "tblPleadingCaptionAssignments.fldPleadingTypeID"
        .FilterValue = Me.PleadingType.ID
        .Refresh
        Me.cmbCaptionType.Array = .ListItems.Source
    End With

    ResizeTDBCombo Me.cmbCaptionType, 4
    ResizeTDBCombo Me.cmbParty3Title, 10
    ResizeTDBCombo Me.cmbParty2Title, 10
    ResizeTDBCombo Me.cmbParty1Title, 10
    LockWindowUpdate Me.hwnd
   
    Exit Sub

ProcError:
    g_oError.Show Err, "Error loading Pleading Caption Form."
    Application.ScreenUpdating = True
    LockWindowUpdate (0&)
    Exit Sub
End Sub

Private Sub lstCaptions_GotFocus()
    OnControlGotFocus lstCaptions
End Sub

Private Sub lstCaptions_RowChange()
    On Error GoTo ProcError
    If PleadingCaptions.Count Then
        If IsNull(lstCaptions.Bookmark) Then
            lstCaptions.Bookmark = 1
        End If
        ChangeActiveCaption
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oCaps_ItemAdded(iIndex As Integer)
    RefreshList
End Sub

Private Sub m_oCaps_ItemRemoved(iIndex As Integer)
    RefreshList
    SetCaptionPositions
End Sub

Private Sub txtAdditionalInformation_Change()
    Me.Changed = True
End Sub
Private Sub txtAdditionalInformation_GotFocus()
    OnControlGotFocus txtAdditionalInformation
End Sub
Private Sub txtParty1Name_Change()
    Me.Changed = True
End Sub
Private Sub txtParty1Name_GotFocus()
    OnControlGotFocus txtParty1Name
End Sub
Private Sub txtParty2Name_Change()
    Me.Changed = True
End Sub
Private Sub txtParty3Name_Change()
    Me.Changed = True
End Sub
Private Sub txtParty3Name_GotFocus()
    OnControlGotFocus txtParty3Name
End Sub
Private Sub txtParty2Name_GotFocus()
    OnControlGotFocus txtParty2Name
End Sub
Private Sub cmbParty1Title_GotFocus()
    OnControlGotFocus cmbParty1Title
End Sub
Private Sub cmbParty2Title_GotFocus()
    OnControlGotFocus cmbParty2Title
End Sub
Private Sub cmbParty3Title_GotFocus()
    OnControlGotFocus cmbParty3Title
End Sub
Private Sub Form_Unload(Cancel As Integer)
    If Me.Added And Me.PleadingCaptions.Count > 1 Then
        Me.Changed = False
        DeleteCaption True
    End If
    Set m_Document = Nothing
    Set frmPleadingCaption = Nothing
    Set m_oCap = Nothing
    Set m_oCaps = Nothing
End Sub

'******************************************
'---module level routines
'******************************************
Private Sub ClearInput()
'clears all recipient detail fields
    On Error GoTo ProcError
    Me.txtParty1Name = ""
    Me.txtParty2Name = ""
    Me.txtAdditionalInformation = ""
    Me.cmbCaptionType.Text = ""
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.ClearInput"
    Exit Sub
End Sub

Private Sub NewCaption()
'creates a new, empty Caption
    
'   save Caption if necessary
    On Error GoTo ProcError
    If Me.Changed Then
        PromptToSave
    End If
    
'   add Caption -- position equals count for now because we are not allowing repositioning
    Me.PleadingCaption = Me.PleadingCaptions.Add
    '---assign temporary type ID -- Necessary for forced add mode if cancel hit
    PleadingCaption.TypeID = Me.PleadingType.DefaultCaptionType
    PleadingCaption.Position = PleadingCaptions.Count
    'ClearInput
    
    On Error Resume Next
 '   select last entry in list
    Me.lstCaptions.SelectedItem = _
        Me.lstCaptions.ApproxCount
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.NewCaption"
    Exit Sub
End Sub

Private Function PromptToSave() As Boolean
    Dim xMsg As String
    Dim iChoice As Integer
    
    On Error GoTo ProcError
    xMsg = "Save current entry?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        UpdateCaption
    Else
        If Me.Added Then
            Me.Added = False
            DeleteCaption True
        End If
    End If
    PromptToSave = (iChoice = vbYes)
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.PromptToSave"
    PromptToSave = (iChoice = vbYes)
    Exit Function
End Function

    
Private Function SaveCaption() As Boolean
'saves the current Caption
    On Error GoTo ProcError
    If Me.cmbCaptionType.Text = "" Then
        MsgBox "Please select a caption type.", vbInformation, App.Title
        Me.cmbCaptionType.SetFocus
        Exit Function
    End If
'   ensure that there is a new Caption object
    If Me.PleadingCaptions.Count = 0 Then
'       add Caption
        Set PleadingCaption = Me.PleadingCaptions.Add()
    End If
    UpdateCaption
    SaveCaption = True
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.SaveCaption"
    Exit Function
End Function

Private Sub RefreshList()
    Dim iNum As Integer
    
    With Me.lstCaptions
        .Array = PleadingCaptions.ListArray
        .Rebind
        
        iNum = Me.lstCaptions.Array.Count(1)

        On Error GoTo ProcError
        If iNum > 1 Then
            Me.lblCaptions = _
                " &Captions (" & iNum & "):"
        ElseIf iNum = 1 And Me.txtParty1Name <> Empty Then
            Me.lblCaptions = " &Captions (1):"
        Else
            Me.lblCaptions = " &Captions:"
        End If
    End With

Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub UpdateCaption()
'updates the current Caption object
'with dialog field values
    Dim xP1N As String
    Dim xP1T As String

    On Error GoTo ProcError
    With PleadingCaption
        .DynamicUpdating = True
        If Me.cmbParty1Title.Enabled = False Then
            .DisplayName = Me.cmbCaptionType.Text
        Else
            If txtParty1Name <> "" Then
                xP1N = " - " & txtParty1Name
            Else
                xP1N = txtParty1Name
            End If
        
            If cmbParty1Title <> "" Then
                xP1T = " - " & cmbParty1Title.Text
            Else
                xP1T = ""
            End If
            .DisplayName = Me.cmbCaptionType.Text & xP1T & xP1N
        End If
        .Party1Name = Me.txtParty1Name
        .Party2Name = Me.txtParty2Name
        .Party3Name = Me.txtParty3Name
        .AdditionalInfo = Me.txtAdditionalInformation
        .Party1Title = Me.cmbParty1Title.Text
        .Party2Title = Me.cmbParty2Title.Text
        .Party3Title = Me.cmbParty3Title.Text
        .TypeID = Me.cmbCaptionType.BoundText
        .DynamicUpdating = False
    End With
    Me.Changed = False
    Me.Added = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.UpdateCaption"
    Exit Sub
End Sub

Private Sub ShowCaption()
    On Error GoTo ProcError
    With PleadingCaption
        Me.txtAdditionalInformation = .AdditionalInfo
        Me.txtParty1Name = .Party1Name
        Me.txtParty2Name = .Party2Name
        Me.txtParty3Name = .Party3Name
        '---9.5.0
        Me.cmbParty1Title.BoundText = .Party1Title
        Me.cmbParty2Title.BoundText = .Party2Title
        Me.cmbParty3Title.BoundText = .Party3Title
        '---9.5.0
        If .TypeID > 0 Then Me.cmbCaptionType.BoundText = .TypeID
    End With
    DoEvents
    If Me.lstCaptions.SelectedItem = 1 Then
        Me.lblCaptions.Caption = "&Caption:"
    Else
        Me.lblCaptions.Caption = "&Cross-Action:"
    End If
    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.ShowCaption"
    Exit Sub
End Sub

Private Sub ChangeActiveCaption()
'   prompt to save current Caption if necessary
    On Error GoTo ProcError
    DoEvents
    If Me.Changed Then
        PromptToSave
    End If
    
    PleadingCaption = Me.PleadingCaptions(Min(Me.lstCaptions.Bookmark, Me.PleadingCaptions.Count))
    ShowCaption
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmPleadingCaption.ChangeActiveCaption"
    Exit Sub
End Sub

Private Sub DeleteCaption(Optional bSuppressMessage As Boolean = False)
    Dim i As Integer
    Dim iNum As Integer
    Dim mbr As VbMsgBoxResult
    
    On Error GoTo ProcError
    i = Me.lstCaptions.Row
    If i = 0 Then
        MsgBox "You cannot delete the main caption of a pleading.  Please select another caption.", vbInformation, App.Title
        Exit Sub
    End If
    
    If bSuppressMessage = False Then
        mbr = MsgBox("Are you sure you want to delete this cross-action?", vbYesNo, "Pleading Caption")
        If mbr <> vbYes Then
            If Me.Added Then
                Me.Added = True
            End If
            Exit Sub
        Else
            Me.Added = False
        End If
    End If
    
    If Me.lstCaptions.Row > -1 Then
        PleadingCaptions.Remove (PleadingCaption.Key)
        DoEvents
        ClearInput
        If m_oCaps.Count Then
            Me.Changed = False
            Me.lstCaptions.Row = Min(CDbl(i), m_oCaps.Count - 1)
        End If
    End If
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.DeleteCaption"
End Sub

Private Sub SetInterfaceControls()
    Dim oDef As CPleadingCaptionDef
    
    On Error GoTo ProcError
    Set oDef = g_oMPO.DB.PleadingCaptionDefs.Item(cmbCaptionType.BoundText)
    If Me.PleadingCaption.Party1Title <> "" Then
        '---9.5.0
        Me.cmbParty1Title.BoundText = Me.PleadingCaption.Party1Title
    Else
        With oDef
            '---9.5.0
            Me.cmbParty1Title.BoundText = .Party1TitleDefault
        End With
    End If
    
    If Me.PleadingCaption.Party2Title <> "" Then
        '---9.5.0
        Me.cmbParty2Title.BoundText = Me.PleadingCaption.Party2Title
    Else
        With oDef
            '---9.5.0
            Me.cmbParty2Title.BoundText = .Party2TitleDefault
        End With
    End If
    
    If Me.PleadingCaption.Party3Title <> "" Then
        '---9.5.0
        Me.cmbParty3Title = Me.PleadingCaption.Party3Title
    Else
        With oDef
            '---9.5.0
            Me.cmbParty3Title.BoundText = .Party3TitleDefault
        End With
    End If
    
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.SetInterfaceControls"
End Sub

Private Sub DisplayControls()
    Dim oDef As CPleadingCaptionDef
    Dim bAllows As Boolean
    
    On Error GoTo ProcError
    Set oDef = g_oMPO.DB.PleadingCaptionDefs.Item(cmbCaptionType.BoundText)
    bAllows = oDef.AllowsAdditionalInformation
    Me.txtAdditionalInformation.Visible = bAllows
    Me.lblAdditionalInformation.Visible = bAllows
    
    If oDef.PartiesInCaption = 0 Then
        bAllows = oDef.PartiesInCaption = 0
        Me.cmbParty1Title.Enabled = Not bAllows
        Me.lblParty1Name.Enabled = Not bAllows
        Me.txtParty1Name.Enabled = Not bAllows
        Me.cmbParty2Title.Enabled = Not bAllows
        Me.lblParty2Name.Enabled = Not bAllows
        Me.txtParty2Name.Enabled = Not bAllows
        Me.cmbParty3Title.Visible = Not bAllows
        Me.lblParty3Name.Visible = Not bAllows
        Me.txtParty3Name.Visible = Not bAllows
    
        Me.lblVersus.Enabled = Not bAllows
        Me.lblVersus2.Visible = Not bAllows
        Me.lblVersus3.Visible = Not bAllows
    
    ElseIf oDef.PartiesInCaption = 1 Then
        bAllows = (oDef.PartiesInCaption = 1)
        Me.cmbParty1Title.Enabled = bAllows
        Me.lblParty1Name.Enabled = bAllows
        Me.txtParty1Name.Enabled = bAllows
       
        Me.cmbParty2Title.Visible = Not bAllows
        Me.lblParty2Name.Visible = Not bAllows
        Me.txtParty2Name.Visible = Not bAllows
        Me.cmbParty3Title.Visible = Not bAllows
        Me.lblParty3Name.Visible = Not bAllows
        Me.txtParty3Name.Visible = Not bAllows
        Me.lblVersus.Visible = Not bAllows
        Me.lblVersus2.Visible = Not bAllows
        Me.lblVersus3.Visible = Not bAllows
    
        Me.lblAdditionalInformation.Top = Me.lblParty2Name.Top
        Me.txtAdditionalInformation.Top = Me.cmbParty2Title.Top
    
    ElseIf oDef.PartiesInCaption = 2 Then
    
        bAllows = (oDef.PartiesInCaption = 2)
        
        Me.cmbParty1Title.Enabled = bAllows
        Me.lblParty1Name.Enabled = bAllows
        Me.txtParty1Name.Enabled = bAllows
        Me.cmbParty2Title.Visible = bAllows
        Me.lblParty2Name.Visible = bAllows
        Me.txtParty2Name.Visible = bAllows
        Me.cmbParty2Title.Enabled = bAllows
        Me.lblParty2Name.Enabled = bAllows
        Me.txtParty2Name.Enabled = bAllows
        Me.cmbParty3Title.Visible = Not bAllows
        Me.lblParty3Name.Visible = Not bAllows
        Me.txtParty3Name.Visible = Not bAllows
        Me.lblVersus.Visible = bAllows
        Me.lblVersus.Enabled = bAllows
        Me.lblVersus2.Visible = Not bAllows
        Me.lblVersus3.Visible = Not bAllows
    
        Me.lblAdditionalInformation.Top = Me.lblParty3Name.Top
        Me.txtAdditionalInformation.Top = Me.cmbParty3Title.Top
    
    Else
        
        bAllows = (oDef.PartiesInCaption = 3)
        
        Me.cmbParty1Title.Enabled = bAllows
        Me.lblParty1Name.Enabled = bAllows
        Me.txtParty1Name.Enabled = bAllows
        Me.cmbParty2Title.Enabled = bAllows
        Me.lblParty2Name.Enabled = bAllows
        Me.txtParty2Name.Enabled = bAllows
        Me.cmbParty2Title.Visible = bAllows
        Me.lblParty2Name.Visible = bAllows
        Me.txtParty2Name.Visible = bAllows
        
        Me.cmbParty3Title.Visible = bAllows
        Me.lblParty3Name.Visible = bAllows
        Me.txtParty3Name.Visible = bAllows
        
        Me.lblVersus.Visible = Not bAllows
        Me.lblVersus2.Visible = bAllows
        Me.lblVersus3.Visible = bAllows
        Me.lblVersus2.Enabled = bAllows
        Me.lblVersus3.Enabled = bAllows
   
        Me.lblAdditionalInformation.Top = 5235
        Me.txtAdditionalInformation.Top = 5235
    End If
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.DisplayControls"
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF5 And btnNew.Enabled Then
        btnNew_Click
    ElseIf KeyCode = vbKeyF6 And btnSave.Enabled Then
        btnSave_Click
    ElseIf KeyCode = vbKeyF7 And btnDelete.Enabled Then
        btnDelete_Click
    Else
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub SetCaptionPositions()
    Dim oCap As CPleadingCaption
    Dim i As Integer
    For i = 1 To Me.lstCaptions.Array.UpperBound(1)
        Set oCap = Me.PleadingCaptions(lstCaptions.Array.value(i, 1))
        oCap.Position = i
    Next
End Sub



