VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmCalendar 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Calendar"
   ClientHeight    =   4440
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   5472
   Icon            =   "frmCalendar.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4440
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3330
      TabIndex        =   34
      Top             =   3960
      Width           =   1000
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4395
      TabIndex        =   35
      Top             =   3960
      Width           =   1000
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   4395
      Left            =   -15
      TabIndex        =   36
      TabStop         =   0   'False
      Top             =   -15
      Width           =   5535
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|E&xtras"
      Align           =   0
      CurrTab         =   1
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   " "
         Height          =   3945
         Left            =   -6000
         TabIndex        =   38
         Top             =   15
         Width           =   5505
         Begin TrueDBList60.TDBCombo cmbMonthEnd 
            Height          =   555
            Left            =   1770
            OleObjectBlob   =   "frmCalendar.frx":058A
            TabIndex        =   6
            Top             =   1005
            Width           =   1890
         End
         Begin mpControls.SpinnerTextBox spnYearEnd 
            Height          =   300
            Left            =   3795
            TabIndex        =   7
            Top             =   1005
            Width           =   1515
            _ExtentX        =   2667
            _ExtentY        =   529
            MinValue        =   1995
            MaxValue        =   2099
         End
         Begin VB.OptionButton optPortrait 
            Caption         =   "&Portrait"
            Height          =   285
            Left            =   3150
            TabIndex        =   15
            Top             =   2610
            Width           =   1110
         End
         Begin VB.OptionButton optLandscape 
            Caption         =   "&Landscape"
            Height          =   285
            Left            =   1860
            TabIndex        =   14
            Top             =   2610
            Width           =   1110
         End
         Begin VB.CheckBox chkAbbrDays 
            Appearance      =   0  'Flat
            Caption         =   "&Days of the Week"
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   1875
            TabIndex        =   11
            Top             =   2160
            Width           =   1650
         End
         Begin VB.CheckBox chkAbbrMonths 
            Appearance      =   0  'Flat
            Caption         =   "Mo&nths"
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   3660
            TabIndex        =   12
            Top             =   2160
            Width           =   870
         End
         Begin TrueDBList60.TDBCombo cmbStartDay 
            Height          =   345
            Left            =   1770
            OleObjectBlob   =   "frmCalendar.frx":25D5
            TabIndex        =   9
            Top             =   1665
            Width           =   3555
         End
         Begin mpControls3.SpinTextInternational spnCalendarTextFontSize 
            Height          =   285
            Left            =   3795
            TabIndex        =   20
            Top             =   3330
            Width           =   1515
            _ExtentX        =   2667
            _ExtentY        =   508
            MaxValue        =   45
            Value           =   11
         End
         Begin TrueDBList60.TDBCombo cmbCalendarTextFontName 
            Height          =   555
            Left            =   1770
            OleObjectBlob   =   "frmCalendar.frx":4620
            TabIndex        =   19
            Top             =   3315
            Width           =   1890
         End
         Begin mpControls.SpinnerTextBox spnYearStart 
            Height          =   285
            Left            =   3795
            TabIndex        =   4
            Top             =   450
            Width           =   1515
            _ExtentX        =   2667
            _ExtentY        =   508
            MinValue        =   1995
            MaxValue        =   2099
         End
         Begin TrueDBList60.TDBCombo cmbMonthStart 
            Height          =   555
            Left            =   1770
            OleObjectBlob   =   "frmCalendar.frx":6837
            TabIndex        =   3
            Top             =   435
            Width           =   1890
         End
         Begin VB.Label Label2 
            Alignment       =   2  'Center
            Caption         =   "Size"
            Height          =   240
            Left            =   3810
            TabIndex        =   17
            Top             =   3060
            Width           =   1260
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "Font"
            Height          =   240
            Left            =   1965
            TabIndex        =   16
            Top             =   3060
            Width           =   1260
         End
         Begin VB.Label lblCalendarTextFont 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Calendar Text:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   120
            TabIndex        =   18
            Top             =   3375
            Width           =   1440
         End
         Begin VB.Label lblYear 
            Alignment       =   2  'Center
            Caption         =   "Year"
            Height          =   240
            Left            =   3780
            TabIndex        =   1
            Top             =   180
            Width           =   1260
         End
         Begin VB.Label lblMonth 
            Alignment       =   2  'Center
            Caption         =   "Month"
            Height          =   240
            Left            =   1980
            TabIndex        =   0
            Top             =   180
            Width           =   1260
         End
         Begin VB.Label lblStartDay 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Start &Week at:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   540
            TabIndex        =   8
            Top             =   1725
            Width           =   1020
         End
         Begin VB.Label lblAbbr 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Abbreviate:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   120
            TabIndex        =   10
            Top             =   2205
            Width           =   1440
         End
         Begin VB.Label lblPageLayout 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Page Layout:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   225
            TabIndex        =   13
            Top             =   2625
            Width           =   1335
         End
         Begin VB.Label lblEndMonthYear 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&End Date:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   225
            TabIndex        =   5
            Top             =   1050
            Width           =   1335
         End
         Begin VB.Line Line3 
            BorderColor     =   &H00FFFFFF&
            X1              =   -15
            X2              =   5540
            Y1              =   3870
            Y2              =   3870
         End
         Begin VB.Label lblStartMonthYear 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "&Start Date:"
            ForeColor       =   &H00FFFFFF&
            Height          =   315
            Left            =   225
            TabIndex        =   2
            Top             =   465
            Width           =   1335
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   4905
            Left            =   -15
            Top             =   -1020
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   3945
         Left            =   15
         TabIndex        =   37
         Top             =   15
         Width           =   5505
         Begin TrueDBList60.TDBCombo cmbMonthYearFontName 
            Height          =   555
            Left            =   1755
            OleObjectBlob   =   "frmCalendar.frx":8884
            TabIndex        =   24
            Top             =   450
            Width           =   2325
         End
         Begin mpControls3.SpinTextInternational spnMonthYearFontSize 
            Height          =   300
            Left            =   4230
            TabIndex        =   25
            Top             =   465
            Width           =   1170
            _ExtentX        =   2074
            _ExtentY        =   529
            MaxValue        =   45
            Value           =   11
         End
         Begin TrueDBList60.TDBCombo cmbDaysOfWeekFontName 
            Height          =   555
            Left            =   1755
            OleObjectBlob   =   "frmCalendar.frx":AA98
            TabIndex        =   27
            Top             =   1080
            Width           =   2325
         End
         Begin mpControls3.SpinTextInternational spnDaysOfWeekFontSize 
            Height          =   285
            Left            =   4230
            TabIndex        =   28
            Top             =   1095
            Width           =   1170
            _ExtentX        =   2074
            _ExtentY        =   508
            MaxValue        =   45
            Value           =   11
         End
         Begin TrueDBList60.TDBCombo cmbDateFontName 
            Height          =   555
            Left            =   1755
            OleObjectBlob   =   "frmCalendar.frx":CCAD
            TabIndex        =   30
            Top             =   1695
            Width           =   2325
         End
         Begin mpControls3.SpinTextInternational spnDateFontSize 
            Height          =   300
            Left            =   4230
            TabIndex        =   31
            Top             =   1710
            Width           =   1170
            _ExtentX        =   2074
            _ExtentY        =   529
            MaxValue        =   45
            Value           =   11
         End
         Begin TrueDBList60.TDBCombo cmbDateNumberAlignment 
            Height          =   555
            Left            =   1755
            OleObjectBlob   =   "frmCalendar.frx":EEBC
            TabIndex        =   33
            Top             =   2340
            Width           =   2325
         End
         Begin VB.Label lblSize 
            Alignment       =   2  'Center
            Caption         =   "Size"
            Height          =   240
            Left            =   4065
            TabIndex        =   22
            Top             =   195
            Width           =   1260
         End
         Begin VB.Label lblFont 
            Alignment       =   2  'Center
            Caption         =   "Font"
            Height          =   240
            Left            =   2145
            TabIndex        =   21
            Top             =   195
            Width           =   1260
         End
         Begin VB.Label lblDateFont 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "&Date:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   135
            TabIndex        =   29
            Top             =   1755
            Width           =   1440
         End
         Begin VB.Label lblDaysOfWeekFont 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Days of &Week:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   135
            TabIndex        =   26
            Top             =   1140
            Width           =   1440
         End
         Begin VB.Label lblMonthYearFont 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "M&onth/Year:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   135
            TabIndex        =   23
            Top             =   510
            Width           =   1440
         End
         Begin VB.Label lblDateNumberAlignment 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "D&ate Alignment:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   150
            TabIndex        =   32
            Top             =   2370
            Width           =   1440
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   4380
            Left            =   0
            Top             =   -495
            Width           =   1695
         End
         Begin VB.Line Line2 
            BorderColor     =   &H00FFFFFF&
            X1              =   -30
            X2              =   5525
            Y1              =   3870
            Y2              =   3870
         End
      End
   End
End
Attribute VB_Name = "frmCalendar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const mpIniSec As String = "Calendar.dot"

Private WithEvents m_oCalendar As MPO.CCalendar
Attribute m_oCalendar.VB_VarHelpID = -1
Private m_bInit As Boolean
Private m_bCanceled As Boolean
Private m_iYearMin As Integer
Private m_iYearMax As Integer

'******************************************
'---properties
'******************************************

Property Get Cancelled() As Boolean
    Cancelled = m_bCanceled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCanceled = bNew
End Property

Public Property Get Calendar() As MPO.CCalendar
    Set Calendar = m_oCalendar
End Property

Public Property Let Calendar(oNew As MPO.CCalendar)
    Set m_oCalendar = oNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property


Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    Me.Cancelled = True
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    
    If Me.spnYearEnd.value = Me.spnYearStart.value Then
        If Val(Me.cmbMonthStart.BoundText) > Val(Me.cmbMonthEnd.BoundText) Then
            MsgBox "The date range is not valid.", vbExclamation, App.Title
            Me.cmbMonthStart.SetFocus
            Exit Sub
        End If
    End If
    
    Me.Hide
    DoEvents
    Me.Cancelled = False
    
    SaveDefaults
    UpdateObject
    Application.ScreenUpdating = False
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub chkAbbrDays_GotFocus()
    OnControlGotFocus Me.chkAbbrDays
End Sub

Private Sub chkAbbrMonths_GotFocus()
    OnControlGotFocus Me.chkAbbrMonths
End Sub

Private Sub cmbCalendarTextFontName_GotFocus()
    OnControlGotFocus Me.cmbCalendarTextFontName
End Sub

Private Sub cmbDateFontName_GotFocus()
    OnControlGotFocus Me.cmbDateFontName
End Sub

Private Sub cmbDateNumberAlignment_GotFocus()
    OnControlGotFocus Me.cmbDateNumberAlignment
End Sub

Private Sub cmbDateNumberAlignment_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDateNumberAlignment, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateNumberAlignment_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbDateNumberAlignment)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDaysOfWeekFontName_GotFocus()
    OnControlGotFocus Me.cmbDaysOfWeekFontName
End Sub

Private Sub cmbMonthEnd_GotFocus()
    OnControlGotFocus Me.cmbMonthEnd
End Sub

Private Sub cmbMonthYearFontName_GotFocus()
    OnControlGotFocus Me.cmbMonthYearFontName
End Sub

Private Sub cmbStartDay_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbStartDay, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbStartDay_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbStartDay)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbMonthStart_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbMonthStart, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbMonthStart_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbMonthStart)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbMonthEnd_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbMonthEnd, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbMonthEnd_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbMonthEnd)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbStartDay_GotFocus()
    OnControlGotFocus Me.cmbStartDay
End Sub

Private Sub cmbMonthStart_GotFocus()
    OnControlGotFocus Me.cmbMonthStart
End Sub

Private Sub Form_Activate()
    Dim sFontSize As Single
    Dim oDoc As MPO.CDocument
    
'   set defaults from ini
    On Error GoTo ProcError
    
    If Me.Calendar.Document.IsCreated Then
        UpdateForm
    Else
        'Init start and end month to current month
        Me.cmbMonthStart.BoundText = Month(Date) - 1
        Me.cmbMonthEnd.BoundText = Month(Date) - 1
        
        'Init start and end years to current year
        Me.spnYearStart.value = Year(Date)
        Me.spnYearEnd.value = Year(Date)
        
        GetDefaults
    End If
    
    Me.Refresh
    
    Set oDoc = New MPO.CDocument

    DoEvents
    Me.Initializing = False
    Me.vsIndexTab1.CurrTab = 0
    Me.cmbMonthEnd.SetFocus
    cmbMonthEnd_GotFocus
    DoEvents
    Screen.MousePointer = vbDefault
    Exit Sub
ProcError:
    Me.Initializing = False
    g_oError.Show Err, "Error Activating Calendar Form"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim xAddresses As String
    Dim xDPhrases As String
    Dim oAlignments As XArray
    
    On Error GoTo ProcError
    
    Me.Initializing = True
    Me.Cancelled = True
    
'    Set m_oCalendar = g_oMPO.NewEnvelope
        
    Set oAlignments = g_oMPO.NewXArray
    
'   set alignments list
    With oAlignments
        .ReDim 0, 2, 0, 1
        .value(0, 1) = "Left"
        .value(0, 0) = wdAlignParagraphLeft
        .value(1, 1) = "Center"
        .value(1, 0) = wdAlignParagraphCenter
        .value(2, 1) = "Right"
        .value(2, 0) = wdAlignParagraphRight
    End With

'   assign row source
    With g_oMPO.Lists
        Me.cmbCalendarTextFontName.Array = .Item("Fonts").ListItems.Source
        Me.cmbDateFontName.Array = .Item("Fonts").ListItems.Source
        Me.cmbDaysOfWeekFontName.Array = .Item("Fonts").ListItems.Source
        Me.cmbMonthYearFontName.Array = .Item("Fonts").ListItems.Source
        Me.cmbDateNumberAlignment.Array = oAlignments
        Me.cmbStartDay.Array = Me.Calendar.DayNames
        Me.cmbMonthStart.Array = Me.Calendar.MonthNames
        Me.cmbMonthEnd.Array = Me.Calendar.MonthNames
    End With
    
'   resize combos
    ResizeTDBCombo Me.cmbDateNumberAlignment, 3
    ResizeTDBCombo Me.cmbCalendarTextFontName, 6
    ResizeTDBCombo Me.cmbDateFontName, 6
    ResizeTDBCombo Me.cmbDaysOfWeekFontName, 6
    ResizeTDBCombo Me.cmbMonthYearFontName, 6
    ResizeTDBCombo Me.cmbStartDay, 7
    ResizeTDBCombo Me.cmbMonthStart, 7
    ResizeTDBCombo Me.cmbMonthEnd, 7
    
'   set minimun & maximum values to module level vars
    m_iYearMin = Me.spnYearEnd.MinValue
    m_iYearMax = Me.spnYearEnd.MaxValue
    
    'mpbase2.MoveToLastPosition Me
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Loading Calendar Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'mpbase2.'SetLastPosition Me
    Set frmCalendar = Nothing
    Set m_oCalendar = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Unloading Calendar Form"
    Exit Sub
End Sub

Private Sub GetDefaults()
    Dim xFont As String
    Dim sFontSize As Single
    Dim xAlignment As String
    
    On Error GoTo ProcError
    
'   get styles from main boilerplate.  Takes care of Admin settings for Cal styles
    Me.Calendar.Document.File.CopyStylesFromTemplate mpbase2.BoilerplateDirectory & _
                                                     Me.Calendar.Template.BoilerplateFile

'   init start day
    Me.optLandscape.value = True
    
    On Error Resume Next    '0 is Sunday
    Me.cmbStartDay.BoundText = CInt(mpbase2.GetMacPacIni("Calendar", "StartWeek"))
    On Error GoTo ProcError
    
    If Me.cmbStartDay.BoundText = "" Then
        Me.cmbStartDay.BoundText = 0
    End If
    
'   Text Font
    On Error Resume Next
    xFont = GetUserIni(mpIniSec, "CalendarTextFontName")
    sFontSize = GetUserIniNumeric(mpIniSec, "CalendarTextFontSize")
    On Error GoTo ProcError
    With Me.cmbCalendarTextFontName
        If xFont <> "" Then
            .BoundText = xFont
        Else
            .BoundText = ActiveDocument.Styles("Cal Text").Font.Name
        End If
    End With

    If sFontSize Then
        Me.spnCalendarTextFontSize.value = sFontSize
    Else
        Me.spnCalendarTextFontSize.value = ActiveDocument.Styles("Cal Text").Font.Size
    End If
    
'   Date Font
    On Error Resume Next
    xFont = GetUserIni(mpIniSec, "DateFontName")
    sFontSize = GetUserIniNumeric(mpIniSec, "DateFontSize")
    On Error GoTo ProcError

    With Me.cmbDateFontName
        If xFont <> "" Then
            .BoundText = xFont
        Else
            .BoundText = ActiveDocument.Styles("Cal Date").Font.Name
        End If
    End With

    If sFontSize Then
        Me.spnDateFontSize.value = sFontSize
    Else
        Me.spnDateFontSize.value = ActiveDocument.Styles("Cal Date").Font.Size
    End If
    
'       Days of the week Font
    On Error Resume Next
    xFont = GetUserIni(mpIniSec, "DaysOfWeekFontName")
    sFontSize = GetUserIniNumeric(mpIniSec, "DaysOfWeekFontSize")
    On Error GoTo ProcError
    
    With Me.cmbDaysOfWeekFontName
        If xFont <> "" Then
            .BoundText = xFont
        Else
            .BoundText = ActiveDocument.Styles("Cal Days").Font.Name
        End If
    End With

    If sFontSize Then
        Me.spnDaysOfWeekFontSize.value = sFontSize
    Else
        Me.spnDaysOfWeekFontSize.value = ActiveDocument.Styles("Cal Days").Font.Size
    End If
    
'   Month/Year Font
    On Error Resume Next
    xFont = GetUserIni(mpIniSec, "MonthYearFontName")
    sFontSize = GetUserIniNumeric(mpIniSec, "MonthYearFontSize")
    On Error GoTo ProcError
    
    With Me.cmbMonthYearFontName
        If xFont <> "" Then
            .BoundText = xFont
        Else
            .BoundText = ActiveDocument.Styles("Cal Month").Font.Name
        End If
    End With

    If sFontSize Then
        Me.spnMonthYearFontSize.value = sFontSize
    Else
        Me.spnMonthYearFontSize.value = ActiveDocument.Styles("Cal Month").Font.Size
    End If
    
'       Date Alignment
    xAlignment = GetUserIni(mpIniSec, "DateNumberAlignment")
    
    If xAlignment = "" Then
        Me.cmbDateNumberAlignment.SelectedItem = ActiveDocument.Styles("Cal Date").ParagraphFormat.Alignment
    Else
        Me.cmbDateNumberAlignment.SelectedItem = CInt(xAlignment)
    End If
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmCalendar.GetDefaults"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub SaveDefaults()
    On Error GoTo ProcError
    SetUserIni mpIniSec, "CalendarTextFontName", Me.cmbCalendarTextFontName.BoundText
    SetUserIni mpIniSec, "CalendarTextFontSize", Me.spnCalendarTextFontSize.value
    SetUserIni mpIniSec, "DateFontName", Me.cmbDateFontName.BoundText
    SetUserIni mpIniSec, "DateFontSize", Me.spnDateFontSize.value
    SetUserIni mpIniSec, "DaysOfWeekFontName", Me.cmbDaysOfWeekFontName.BoundText
    SetUserIni mpIniSec, "DaysOfWeekFontSize", Me.spnDaysOfWeekFontSize.value
    SetUserIni mpIniSec, "MonthYearFontName", Me.cmbMonthYearFontName.BoundText
    SetUserIni mpIniSec, "MonthYearFontSize", Me.spnMonthYearFontSize.value
    SetUserIni mpIniSec, "DateNumberAlignment", Me.cmbDateNumberAlignment.BoundText
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmCalendar.SaveDefaults"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub UpdateObject()
    With Me.Calendar
        .AbbreviatedDays = Me.chkAbbrDays
        .AbbreviatedMonth = Me.chkAbbrMonths
        .Portrait = Me.optPortrait
        .DateNumberAlignment = Me.cmbDateNumberAlignment.BoundText
        .MonthEnd = Me.cmbMonthEnd.BoundText
        .StartDay = Me.cmbStartDay.BoundText
        .MonthStart = Me.cmbMonthStart.BoundText
        .YearStart = Me.spnYearStart.value
        .YearEnd = Me.spnYearEnd.value
        .MonthYearFontName = Me.cmbMonthYearFontName
        .MonthYearFontSize = Me.spnMonthYearFontSize.value
        .DaysOfWeekFontName = Me.cmbDaysOfWeekFontName
        .DaysOfWeekFontSize = Me.spnDaysOfWeekFontSize.value
        .DateFontName = Me.cmbDateFontName
        .DateFontSize = Me.spnDateFontSize.value
        .TextFontName = Me.cmbCalendarTextFontName
        .TextFontSize = Me.spnCalendarTextFontSize.value
    End With
End Sub
    
Private Sub spnCalendarTextFontSize_Validate(Cancel As Boolean)
    If Not Me.spnCalendarTextFontSize.IsValid Then _
        Cancel = True
End Sub

Private Sub spnDateFontSize_Validate(Cancel As Boolean)
    If Not Me.spnDateFontSize.IsValid Then _
        Cancel = True
End Sub

Private Sub spnDaysOfWeekFontSize_Validate(Cancel As Boolean)
    If Not Me.spnDaysOfWeekFontSize.IsValid Then _
        Cancel = True
End Sub

Private Sub spnMonthYearFontSize_Validate(Cancel As Boolean)
    If Not Me.spnMonthYearFontSize.IsValid Then _
        Cancel = True
End Sub

Private Sub optLandscape_GotFocus()
    OnControlGotFocus Me.optLandscape
End Sub

Private Sub optPortrait_GotFocus()
    OnControlGotFocus Me.optPortrait
End Sub

Private Sub spnCalendarTextFontSize_GotFocus()
    OnControlGotFocus Me.spnCalendarTextFontSize
End Sub

Private Sub spnDateFontSize_GotFocus()
    OnControlGotFocus Me.spnDateFontSize
End Sub

Private Sub spnDaysOfWeekFontSize_GotFocus()
    OnControlGotFocus Me.spnDaysOfWeekFontSize
End Sub

Private Sub spnMonthYearFontSize_GotFocus()
    OnControlGotFocus Me.spnMonthYearFontSize
End Sub

Private Sub spnYearEnd_SpinDown()
    Dim iYearEnd As Integer
    Dim iYearStart As Integer

    iYearEnd = CInt(Me.spnYearEnd.value)
    iYearStart = CInt(Me.spnYearStart.value)

    If iYearEnd < iYearStart Then
        If iYearStart > spnYearEnd.value Then
            Me.spnYearStart.value = Me.spnYearEnd.value
            If Me.cmbMonthStart.BoundText > Me.cmbMonthEnd.BoundText Then _
                Me.cmbMonthEnd.Text = Me.cmbMonthStart.Text
        End If
    End If
End Sub

Private Sub spnYearEnd_SpinUp()
    Dim iYearEnd As Integer
    Dim iMaxDiff As Integer
    
    iYearEnd = CInt(Me.spnYearEnd.value)
    If Me.cmbMonthStart.BoundText = Me.cmbMonthEnd.BoundText Then
        iMaxDiff = 10
    Else
        iMaxDiff = 9
    End If
'    If (iYearEnd < m_iYearMax) And (iYearEnd < (CInt(Me.spnYearStart.value) + iMaxDiff)) Then _
'        Me.spnYearEnd.value = iYearEnd + 1

End Sub

Private Sub spnYearStart_GotFocus()
    OnControlGotFocus Me.spnYearStart
End Sub

Private Sub spnYearEnd_GotFocus()
    OnControlGotFocus Me.spnYearEnd
End Sub

Private Sub spnYearStart_SpinDown()
    Dim iYearStart As Integer
    Dim iMaxDiff As Integer
    
    If Me.cmbMonthStart.BoundText = Me.cmbMonthEnd.BoundText Then
        iMaxDiff = 10
    Else
        iMaxDiff = 9
    End If
    iYearStart = CInt(spnYearStart.value)
    If (iYearStart > m_iYearMin) And (iYearStart > (CInt(spnYearEnd.value) - iMaxDiff)) Then _
        spnYearStart.value = iYearStart - 1

End Sub

Private Sub spnYearStart_SpinUp()
    Dim iYearEnd As Integer
    Dim iYearStart As Integer
    
    iYearEnd = spnYearEnd.value
    iYearStart = spnYearStart.value

    If iYearStart <= iYearEnd Then
        If spnYearStart.value = iYearEnd Then
            If Me.cmbMonthStart.BoundText > Me.cmbMonthEnd.BoundText Then _
                Me.cmbMonthEnd.BoundText = Me.cmbMonthStart.BoundText
        End If
    Else
        If (iYearStart < m_iYearMax) Then
            spnYearEnd.value = iYearEnd + 1
        End If
    End If

End Sub

Private Sub vsIndexTab1_Click()
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            Me.cmbMonthStart.SetFocus
        Case 1
            Me.cmbMonthYearFontName.SetFocus
    End Select

End Sub

Public Sub UpdateForm()
    On Error GoTo ProcError
     '---get properties, load controls on reuse
    With Me.Calendar
        
        Me.cmbCalendarTextFontName.BoundText = .TextFontName
        Me.spnCalendarTextFontSize.value = .TextFontSize
        
        Me.cmbDateFontName.BoundText = .DateFontName
        Me.spnDateFontSize.value = .DateFontSize
        
        Me.cmbDaysOfWeekFontName.BoundText = .DaysOfWeekFontName
        Me.spnDaysOfWeekFontSize.value = .DaysOfWeekFontSize
        
        Me.cmbMonthYearFontName.BoundText = .MonthYearFontName
        Me.spnMonthYearFontSize.value = .MonthYearFontSize
        
        Me.cmbDateNumberAlignment.BoundText = .DateNumberAlignment
        
        Me.cmbMonthStart.BoundText = .MonthStart
        Me.spnYearStart.value = .YearStart
        
        Me.cmbMonthEnd.BoundText = .MonthEnd
        Me.spnYearEnd.value = .YearEnd
        
        Me.cmbStartDay.BoundText = .StartDay
        
        Me.chkAbbrDays = .AbbreviatedDays
        Me.chkAbbrMonths = .AbbreviatedMonth
        
        If .Portrait Then
            Me.optPortrait = True
        Else
            Me.optLandscape = True
        End If
        
    End With
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmCalendar.UpdateForm"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub


