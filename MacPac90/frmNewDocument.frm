VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmNewDocument 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "New Document"
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7395
   Icon            =   "frmNewDocument.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   7395
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnWord 
      Caption         =   "Native &Word..."
      Height          =   420
      Left            =   2925
      TabIndex        =   4
      Top             =   4515
      Width           =   1350
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   4935
      TabIndex        =   5
      Top             =   4515
      Width           =   1100
   End
   Begin VB.CommandButton Command1 
      Height          =   390
      Left            =   6585
      TabIndex        =   7
      Top             =   -210
      Visible         =   0   'False
      Width           =   495
   End
   Begin MSComctlLib.ImageList ilNormal 
      Left            =   1155
      Top             =   4980
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewDocument.frx":000C
            Key             =   "Normal Document"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwCategories 
      Height          =   3975
      Left            =   60
      TabIndex        =   1
      Top             =   315
      Width           =   2265
      _ExtentX        =   3995
      _ExtentY        =   7011
      _Version        =   393217
      Indentation     =   441
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      Checkboxes      =   -1  'True
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ImageList ilSmall 
      Left            =   690
      Top             =   4965
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   15
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmNewDocument.frx":085E
            Key             =   "SmallDocument"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   6135
      TabIndex        =   6
      Top             =   4515
      Width           =   1100
   End
   Begin MSComctlLib.ListView lstTemplates 
      Height          =   3975
      Left            =   2370
      TabIndex        =   3
      Top             =   315
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   7011
      Arrange         =   2
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      GridLines       =   -1  'True
      PictureAlignment=   4
      _Version        =   393217
      Icons           =   "ilNormal"
      SmallIcons      =   "ilSmall"
      ForeColor       =   -2147483640
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   0
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "&Templates:"
      Height          =   255
      Left            =   2430
      TabIndex        =   2
      Top             =   90
      Width           =   810
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "&Groups:"
      Height          =   255
      Left            =   90
      TabIndex        =   0
      Top             =   90
      Width           =   645
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   570
      Left            =   120
      Picture         =   "frmNewDocument.frx":0DA0
      Top             =   4380
      Width           =   2100
   End
   Begin VB.Menu mnuTemplates 
      Caption         =   "Templates"
      Visible         =   0   'False
      Begin VB.Menu mnuTemplates_AddToFavorites 
         Caption         =   "&Add To Favorites"
      End
      Begin VB.Menu mnuTemplates_RemoveFromFavorites 
         Caption         =   "&Remove From Favorites"
      End
   End
End
Attribute VB_Name = "frmNewDocument"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Enum mpNewDocumentActions
    mpNewDocumentAction_Cancelled = 0
    mpNewDocumentAction_OK = 1
    mpNewDocumentAction_Word = 2
End Enum

Private m_bAction As mpNewDocumentActions

Public Property Get Action() As mpNewDocumentActions
    Action = m_bAction
End Property

Public Property Get SelectedID() As String
    SelectedID = Me.lstTemplates.SelectedItem.Key
End Property

Private Sub btnCancel_Click()
    m_bAction = mpNewDocumentAction_Cancelled
    
    Me.Hide
    DoEvents
    
'   return to all templates
    With g_oDBs.TemplateDefinitions
        .Group = Empty
        .Refresh
    End With
End Sub

Private Sub btnOK_Click()
    m_bAction = mpNewDocumentAction_OK
    
    Me.Hide
    DoEvents
    
'   return to all templates
    With g_oDBs.TemplateDefinitions
        .Group = Empty
        .Refresh
    End With
End Sub

Private Sub btnWord_Click()
    m_bAction = mpNewDocumentAction_Word
    Me.Hide
    DoEvents
End Sub

Private Sub Command1_Click()
    With Me.lstTemplates
        If .View = lvwIcon Then
            .View = lvwList
        Else
            .View = lvwIcon
        End If
    End With
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim oNode As Node
    Dim bExpand As Boolean
    
    On Error GoTo ProcError
    m_bAction = mpNewDocumentAction_Cancelled
    
    If g_oDBs.TemplateGroups.Count = 0 Then
        Me.btnOK.Enabled = False
        Err.Raise mpError_NoTemplatesSpecified
    End If

'   show/hide native file new
    Me.btnWord.Visible = GetMacPacIni("General", "AllowNativeWordNewDlg")
    
'   add favorites and groups
    With Me.tvwCategories.Nodes
        .Add , , "xFavorites", "Favorites"
        .Add , , "xGroups", "Groups"
        On Error Resume Next
        bExpand = GetUserIni("TemplateGroups", "Expanded")
        .Item("xGroups").Expanded = bExpand
    End With
    
    With g_oDBs.TemplateGroups
        For i = 1 To .Count
            With .Item(i)
'               add node as child of groups node
                Set oNode = Me.tvwCategories.Nodes.Add( _
                            Key:="x" & .ID, Text:=.Name, _
                            Relative:="xGroups", _
                            Relationship:=tvwChild)

'               value of checkbox is determined by sticky field
                On Error Resume Next
                oNode.Checked = GetUserIni("TemplateGroups", _
                                           "x" & .ID)
                On Error GoTo ProcError
            End With
        Next i
    End With

'   add template groups to tree
    Me.tvwCategories.Nodes("xGroups").Checked = _
        GetUserIni("TemplateGroups", "xGroups")
    ShowSelectedGroups
    Exit Sub
ProcError:
    RaiseError "MPO.frmNewDocument_FormLoad", _
               g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    SetUserIni "TemplateGroups", "Expanded", _
        Me.tvwCategories.Nodes("xGroups").Expanded
End Sub

Private Sub lstTemplates_DblClick()
    btnOK_Click
End Sub

Private Sub lstTemplates_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oItem As ListItem
    
    If Button = 2 Then
        With Me.lstTemplates
            On Error Resume Next
            
'           exit if an icon was not clicked
            Set oItem = .HitTest(X, Y)
            If (oItem Is Nothing) Then
                Exit Sub
            End If
            
'           ensure that icon that has been
'           right-clicked on is selected
            oItem.Selected = True
        End With
        
'       show only those menu items that
'       are relevant to the visible icons
        With Me.tvwCategories.Nodes
            On Error Resume Next
            Me.mnuTemplates_AddToFavorites.Enabled = _
                .Item("xGroups").Checked
            Me.mnuTemplates_RemoveFromFavorites.Enabled = _
                .Item("xFavorites").Checked
        End With
        
'       show context-sensitive menu
        Me.PopupMenu Me.mnuTemplates
    End If
End Sub

Private Sub mnuTemplates_AddToFavorites_Click()
    On Error GoTo ProcError
    g_oDBs.TemplateDefinitions.AddToFavorites _
         Me.lstTemplates.SelectedItem.Key
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not successfully add to favorites."
    Exit Sub
End Sub

Private Sub mnuTemplates_RemoveFromFavorites_Click()
    On Error GoTo ProcError
    With Me.lstTemplates
        g_oDBs.TemplateDefinitions _
            .RemoveFromFavorites .SelectedItem.Key
    
    '   remove item from list
        .ListItems.Remove .SelectedItem.Key
    End With
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not successfully remove from favorites."
    Exit Sub
End Sub

Private Sub tvwCategories_NodeCheck(ByVal Node As MSComctlLib.Node)
    Static bOnce As Boolean
    Dim oNodes As MSComctlLib.Nodes
    Dim oGroups As MSComctlLib.Node
    Dim oFaves As MSComctlLib.Node
    
'   prevent recursion
    If bOnce Then
        Exit Sub
    Else
        bOnce = True
    End If
    
    Set oNodes = Me.tvwCategories.Nodes
    Set oGroups = oNodes("xGroups")
    Set oFaves = oNodes("xFavorites")
    
    If Node Is oFaves Then
'       clear current list
        Me.lstTemplates.ListItems.Clear
        
'       user clicked favorites
        oGroups.Checked = Not oFaves.Checked
        If oFaves.Checked Then
'           user checked favorites -
'           show favorites
            ShowTemplates -1
        Else
'           user unchecked favorites -
'           show groups
            ShowSelectedGroups
        End If
    ElseIf Node Is oGroups Then
'       clear current list
        Me.lstTemplates.ListItems.Clear

'       user clicked groups
        oFaves.Checked = Not oGroups.Checked
        If oGroups.Checked Then
'           user checked groups -
'           show groups
            ShowSelectedGroups
        Else
'           user unchecked groups -
'           show favorites
            ShowFavorites
        End If
    ElseIf oGroups.Checked Then
'       clear current list
        Me.lstTemplates.ListItems.Clear

'       user clicked a group and groups are shown
        ShowSelectedGroups
    End If

'   enable/disable ok button as appropriate
    Me.btnOK.Enabled = Me.lstTemplates.ListItems.Count
        
'   set sticky field value
    SetUserIni "TemplateGroups", _
                Node.Key, _
                Node.Checked
    bOnce = False
End Sub

Private Sub ShowFavorites()
    ShowTemplates -1
End Sub

Private Sub ShowSelectedGroups()
'shows all templates in selected groups in dlg
    Dim oNode As Node
    
'   show templates of each selected group
    For Each oNode In Me.tvwCategories.Nodes
        If Not oNode.Parent Is Nothing Then
            If oNode.Checked Then
                ShowTemplates Mid(oNode.Key, 2)
            End If
        End If
    Next oNode
    
'   select first item
    With Me.lstTemplates.ListItems
        If .Count Then
            .Item(1).Selected = True
        Else
            Me.btnOK.Enabled = False
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "MPO.frmNewDocument.ShowGroups"
    Exit Sub
End Sub

Private Sub ShowTemplates(lGroupID As Long)
    Dim i As Integer
    Dim iRefs As Integer
    Dim xID As String
    Dim oTemplates As CTemplates
    
    Set oTemplates = New CTemplates
    
    With oTemplates
'       filter templates with specified group
        .Group = lGroupID
        
'       add each template to list
        For i = 1 To .Count
            With .Item(i)
                xID = .ID
                On Error Resume Next
                Me.lstTemplates.ListItems.Add Key:=xID, _
                                              Text:=.ShortName, _
                                              Icon:="Normal Document", _
                                              SmallIcon:="SmallDocument"
                                              
'               increment the number of references to this icon
                With Me.lstTemplates.ListItems(xID)
                    iRefs = 0
                    iRefs = .Tag
                    If iRefs = Empty Then
                        .Tag = 1
                    Else
                        .Tag = iRefs + 1
                    End If
                End With
            End With
        Next i
        Me.lstTemplates.Sorted = True
    End With
End Sub

Private Sub HideTemplates(lGroupID As Long)
    Dim i As Integer
    Dim xID As String
    Dim oTemplates As CTemplates
    
    Set oTemplates = New CTemplates
    
    With oTemplates
'       filter templates with specified group
        .Group = lGroupID
        .Refresh
        
'       decrement icon references of each template in collection
        For i = 1 To .Count
            xID = .Item(i).ID
            On Error Resume Next
'           decrement the number of references to this icon
            With Me.lstTemplates.ListItems(xID)
                .Tag = Max(.Tag - 1, 0)
'               if there are no refs left, remove icon from list
                If .Tag = 0 Then
                    Me.lstTemplates.ListItems.Remove xID
                End If
            End With
        Next i
        Me.lstTemplates.Sorted = True
    End With
End Sub


Private Sub tvwCategories_NodeClick(ByVal Node As MSComctlLib.Node)
    Node.Selected = False
    Me.tvwCategories.Nodes(1).Selected = False
End Sub
