VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmTitlePage 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert/Update Title Page"
   ClientHeight    =   7032
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   5472
   Icon            =   "frmTitlePage.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7032
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtPartyName3 
      Appearance      =   0  'Flat
      Height          =   780
      Left            =   1815
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   4215
      Width           =   3570
   End
   Begin VB.TextBox txtPartyName2 
      Appearance      =   0  'Flat
      Height          =   780
      Left            =   1815
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   3225
      Width           =   3570
   End
   Begin TrueDBList60.TDBCombo cmbType 
      Height          =   555
      Left            =   1815
      OleObjectBlob   =   "frmTitlePage.frx":058A
      TabIndex        =   1
      Top             =   195
      Width           =   3570
   End
   Begin VB.TextBox txtPartyName 
      Appearance      =   0  'Flat
      Height          =   780
      Left            =   1815
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   2235
      Width           =   3570
   End
   Begin VB.TextBox txtDocumentTitle 
      Appearance      =   0  'Flat
      Height          =   1350
      Left            =   1815
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   675
      Width           =   3570
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4365
      TabIndex        =   15
      Top             =   6540
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3300
      TabIndex        =   14
      Top             =   6540
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbLocation 
      Height          =   555
      Left            =   1815
      OleObjectBlob   =   "frmTitlePage.frx":27A1
      TabIndex        =   13
      Top             =   5670
      Width           =   3585
   End
   Begin TrueDBList60.TDBCombo cmbDateType 
      Height          =   375
      Left            =   1815
      OleObjectBlob   =   "frmTitlePage.frx":49BC
      TabIndex        =   11
      Top             =   5190
      Width           =   3585
   End
   Begin VB.Label lblPartyName3 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Party 3 Na&me(s):"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   8
      Top             =   4245
      Width           =   1455
   End
   Begin VB.Label lblPartyName2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Party 2 N&ame(s):"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   6
      Top             =   3255
      Width           =   1455
   End
   Begin VB.Label lblType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "T&ype:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   0
      Top             =   225
      Width           =   1455
   End
   Begin VB.Label lblDateFormat 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Date Format:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   10
      Top             =   5220
      Width           =   1455
   End
   Begin VB.Label lblPartyName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Party Name(s):"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   4
      Top             =   2265
      Width           =   1455
   End
   Begin VB.Label lblLocation 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "I&nsert At:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   90
      TabIndex        =   12
      Top             =   5745
      Width           =   1455
   End
   Begin VB.Label lblDocumentTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Document &Title:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   2
      Top             =   705
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   8310
      Left            =   -60
      Top             =   -390
      Width           =   1740
   End
   Begin VB.Menu mnuExhibits 
      Caption         =   "Exhibits"
      Visible         =   0   'False
      Begin VB.Menu mnuExhibitsDelete1 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuExhibitsDeleteAll 
         Caption         =   "Delete &All"
      End
   End
End
Attribute VB_Name = "frmTitlePage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_bCancelled As Boolean
Private m_oParent As Form
Private m_oTitlePage As MPO.CBusinessTitlePage

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Public Property Let TitlePage(oNew As MPO.CBusinessTitlePage)
    Set m_oTitlePage = oNew
End Property

Public Property Get TitlePage() As MPO.CBusinessTitlePage
    Set TitlePage = m_oTitlePage
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property

Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnOK_Click()
    Dim bForce As Boolean
    
    On Error GoTo ProcError
    
    With m_oTitlePage
        bForce = g_oMPO.ForceItemUpdate
        g_oMPO.ForceItemUpdate = True
        
        .DocumentTitle = Me.txtDocumentTitle
        .PartyName = Me.txtPartyName
        .PartyName2 = Me.txtPartyName2
        .PartyName3 = Me.txtPartyName3
        .DateType = Me.cmbDateType.BoundText    '9.7.1020 #4515
        .Location = Me.cmbLocation.BoundText
        
        g_oMPO.ForceItemUpdate = bForce
        
'       set sticky field
        If UCase(Me.cmbLocation.Text) <> "CURRENT LOCATION" Then
            SetUserIni "Business", "TitlePageLocation", .Location
        End If
        
        Me.Hide
        
        If Me.ParentForm Is Nothing Then
            EchoOff
            .Finish
        End If
        
    End With
    
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
End Sub

Private Sub btnOK_GotFocus()
    OnControlGotFocus Me.btnOK
End Sub

Private Sub cmbDateType_GotFocus()
    OnControlGotFocus Me.cmbDateType
End Sub

Private Sub cmbLocation_GotFocus()
    OnControlGotFocus Me.cmbLocation
End Sub

Private Sub cmbLocation_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLocation, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbLocation_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLocation)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbType_GotFocus()
    OnControlGotFocus Me.cmbType
End Sub

Private Sub cmbType_ItemChange()
    ChangeType
End Sub

Private Sub cmbType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Form_Activate()
    Dim lID As Long
    
    On Error GoTo ProcError
    EchoOffDesktop
    
    If m_oTitlePage.BoilerplateExists Then
        UpdateForm
    Else
        If m_oTitlePage.TypeID = 0 Then
            '9.7.1 #4456
            If m_oTitlePage.BusinessTypeID > 0 Then
                lID = g_oMPO.db.BusinessDefs(m_oTitlePage.BusinessTypeID).DefaultTitlePage
            Else
                ' Default to first type in the list
                lID = cmbType.Array.value(0, 0)
            End If
            '****
            m_oTitlePage.TypeID = lID
        End If
        Me.cmbType.BoundText = m_oTitlePage.TypeID
        DoEvents        '9.7.1020 #4516
        LoadDefaults
    End If
    
    ArrangeControls

'   get sticky setting for title page location
    With Me.cmbLocation
        .BoundText = GetUserIni("Business", "TitlePageLocation")
        If .BoundText = "" Then
            If m_oTitlePage.BoilerplateExists Then
'               current location
                .BoundText = 0
            Else
'               start of document
                .BoundText = 1
            End If
        End If
    End With
    
    EchoOn
    Application.ScreenRefresh
    
    Me.cmbType.SetFocus
    Me.Initializing = False

    Exit Sub
ProcError:
    EchoOn
    Word.Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    g_oError.Show Err, "Error Activating Title Page Form"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim xarLocations As XArray
    Dim oDateFormats As CList
    Dim oList As CList
    
    On Error GoTo ProcError
    Me.Initializing = True
    
'   set up Title page list control
    Set oList = g_oMPO.Lists("BusinessTitlePageTypes")
    With oList
        .FilterValue = m_oTitlePage.BusinessTypeID
        .Refresh
    End With

    Me.cmbType.Array = oList.ListItems.Source
    
    Set xarLocations = g_oMPO.NewXArray
    With xarLocations
        If m_oTitlePage.BoilerplateExists Then
            .ReDim 0, 0, 0, 1
            .value(0, 0) = 0
            .value(0, 1) = "Current Location"
            Me.cmbLocation.Enabled = False
            Me.lblLocation.Enabled = False
        Else
            .ReDim 0, 2, 0, 1
            .value(0, 0) = 2
            .value(0, 1) = "End of Document"
            .value(1, 0) = 1
            .value(1, 1) = "Start of Document"
            .value(2, 0) = 3
            .value(2, 1) = "Insertion Point"
        End If
    End With
    cmbLocation.Array = xarLocations
            
    LoadDateControl 1
    
    ResizeTDBCombo cmbDateType, 8
    ResizeTDBCombo cmbLocation, 3
    ResizeTDBCombo cmbType, 5
    
    'MoveToLastPosition Me
    
    Exit Sub
ProcError:
    EchoOn
    Word.Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    g_oError.Show Err, "Error loading Title Page Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
    Set m_oTitlePage = Nothing
    Set frmTitlePage = Nothing
    Set g_oPrevControl = Nothing

    'GLOG : 5411 : ceh
    Word.Application.ScreenUpdating = True
    Word.Application.ScreenRefresh
End Sub

Private Sub UpdateForm()
    With m_oTitlePage
        Me.cmbType.BoundText = .TypeID
        Me.txtDocumentTitle = .DocumentTitle
        Me.txtPartyName = .PartyName
        Me.txtPartyName2 = .PartyName2
        Me.txtPartyName3 = .PartyName3
        '9.7.1020 #4515
        Me.cmbDateType.BoundText = .DateType
        If Me.cmbDateType.BoundText = "" Then
            MatchCompleteInXArrayList .DateType, cmbDateType
            If Me.cmbDateType.BoundText = "" Then
                'match unsuccessfull so select first in list
                Me.cmbDateType.SelectedItem = 0
            End If
        End If
        Me.cmbLocation.BoundText = .Location
    End With
End Sub

Private Sub txtDocumentTitle_GotFocus()
    OnControlGotFocus Me.txtDocumentTitle
End Sub

Private Sub txtPartyName_GotFocus()
    OnControlGotFocus Me.txtPartyName
End Sub

Private Sub txtPartyName2_GotFocus()
    OnControlGotFocus Me.txtPartyName2
End Sub

Private Sub txtPartyName3_GotFocus()
    OnControlGotFocus Me.txtPartyName3
End Sub

Private Sub ChangeType()
    
    On Error GoTo ProcError
    
    If Me.cmbType.BoundText <> Empty And _
    Me.cmbType.BoundText <> Me.TitlePage.TypeID Then
'       set type
        Me.TitlePage.TypeID = Me.cmbType.BoundText
        LoadDefaults
        ArrangeControls
    End If
    Exit Sub
ProcError:
    Word.Application.ScreenRefresh
    g_oError.Show Err, "Could not successfully change Title Page type."
End Sub

Private Sub LoadDefaults()
    With Me.TitlePage.Definition
        LoadDateControl .DateFormats
        Me.cmbDateType.BoundText = .DefaultDate
        Me.txtDocumentTitle = Me.TitlePage.DocumentTitle
        Me.txtPartyName = Me.TitlePage.PartyName
        Me.txtPartyName2 = Me.TitlePage.PartyName2
        Me.txtPartyName3 = Me.TitlePage.PartyName3
    End With
End Sub

Private Sub LoadDateControl(lFormat As Long)
    Dim oDateFormats As CList
    Dim xarDates As XArray
    Dim i As Integer
    Dim j As Integer
    Dim lID As Long
    Dim xOrdSuffix As String
    
    On Error GoTo ProcError
    Set oDateFormats = g_oMPO.Lists("DateFormatsVariable")
    
    With oDateFormats
'       the date formats value of the definition
'       is the id of the group of date formats
        .FilterValue = lFormat
        .Refresh
    End With
    
    Set xarDates = oDateFormats.ListItems.Source
    
    Set cmbDateType.Array = g_oMPO.NewXArray
    
'----#4516
'   get ordinal suffix based on current date - eg "rd", "th"
    xOrdSuffix = GetOrdinalSuffix(Day(Date))
'   add 'none' option
    With cmbDateType.Array
        .ReDim 0, -1, 0, 1
        For i = 0 To xarDates.UpperBound(1)
            '----#4516
            xarDates.value(i, 0) = xSubstitute(xarDates.value(i, 0), "%o%", xOrdSuffix)
            xarDates.value(i, 0) = xSubstitute(xarDates.value(i, 0), "%or%", xOrdSuffix)
            .Insert 1, i
            For j = 0 To 1
                .value(i, j) = xarDates(i, j)
            Next j
        Next i
        .Insert 1, i
        .value(i, 0) = "None"
        .value(i, 1) = "None"
    End With

    cmbDateType.Refresh
    Exit Sub
ProcError:
    Word.Application.ScreenRefresh
    g_oError.Show Err, "Could not successfully load the date control."
    Exit Sub
End Sub

Private Sub ArrangeControls()
    On Error GoTo ProcError
    Const iSpaceBefore As Integer = 150
    Dim oCtl As Control
    Dim oLastVisible As Control
    Dim arrCtls() As String
    Dim i As Integer
    
    EchoOffDesktop
    
    With Me.TitlePage.Definition
        Me.lblPartyName.Visible = .AllowPartyName1
        Me.txtPartyName.Visible = .AllowPartyName1
        Me.lblPartyName2.Visible = .AllowPartyName2
        Me.txtPartyName2.Visible = .AllowPartyName2
        Me.lblPartyName3.Visible = .AllowPartyName3
        Me.txtPartyName3.Visible = .AllowPartyName3
        Me.lblDocumentTitle.Visible = .AllowDocumentTitle
        Me.txtDocumentTitle.Visible = .AllowDocumentTitle
        If Me.lblPartyName.Visible And Me.lblPartyName2.Visible Then
            Me.lblPartyName.Caption = "&Party 1 Name(s):"
        Else
            Me.lblPartyName.Caption = "&Party Name(s):"
        End If
    End With
    
    ReDim arrCtls(Me.Controls.Count)
    
'   place each control in tab order sort
    For Each oCtl In Me.Controls
        On Error Resume Next
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next

'   position controls based on visibility and tab index -
'   skip author and signer name controls - they'll always
'   appear at the top of the dlg at the same y position
    For i = 2 To 13
'       get control
        Set oCtl = Me.Controls(arrCtls(i))
'       do only if visible
        
        If oCtl.Visible Then
'            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
'                TypeOf oCtl Is VB.TextBox Or _
'                TypeOf oCtl Is VB.CheckBox) Then
            If Not (TypeOf oCtl Is VB.Label) Then
                If oLastVisible Is Nothing Then
'                   position as 1st control
                    oCtl.Top = Me.cmbType.Top + 500
                Else
'                   position control relative to previous control
                    If Left(oLastVisible.Name, 3) = "mlc" Then
                        oCtl.Top = oLastVisible.Top + _
                                   680 + _
                                   iSpaceBefore
                    Else
                        oCtl.Top = oLastVisible.Top + _
                                   oLastVisible.Height + _
                                   iSpaceBefore
                    End If
                End If
                
'               set accompanying label vertical position
                If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                    Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                End If
                                
'               set control as last visible control
                Set oLastVisible = oCtl
            End If
        End If
    Next i
        
'   finish formatting form
    Me.btnOK.Top = oLastVisible.Top + _
        oLastVisible.Height + 210
        
    Me.btnCancel.Top = Me.btnOK.Top
    Me.Height = Me.btnOK.Top + Me.btnOK.Height + 700
    
    EchoOn
    Word.Application.ScreenRefresh
        
    Exit Sub
ProcError:
    EchoOn
    Word.Application.ScreenRefresh
    g_oError.Show Err, "Could not successfully arrange controls on the form."
    Exit Sub
End Sub
