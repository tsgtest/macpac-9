VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmBusiness 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Business Agreement Document"
   ClientHeight    =   4308
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   6240
   Icon            =   "frmBusiness.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4308
   ScaleWidth      =   6240
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TrueDBList60.TDBCombo cmbDocType 
      Height          =   1395
      Left            =   1665
      OleObjectBlob   =   "frmBusiness.frx":058A
      TabIndex        =   1
      Top             =   210
      Width           =   4485
   End
   Begin mpControls.MultiLineCombo mlcDeliveryPhrase 
      Height          =   690
      Left            =   1680
      TabIndex        =   3
      Top             =   720
      Width           =   4455
      _ExtentX        =   7853
      _ExtentY        =   1207
      ListRows        =   8
      FontName        =   "MS Sans Serif"
      FontBold        =   0   'False
      FontSize        =   7.8
      FontItalic      =   0   'False
      FontUnderline   =   0   'False
   End
   Begin mpControls.MultiLineCombo mlcDeliveryPhrase2 
      Height          =   690
      Left            =   1695
      TabIndex        =   5
      Top             =   1590
      Width           =   4455
      _ExtentX        =   7853
      _ExtentY        =   1207
      ListRows        =   8
      FontName        =   "MS Sans Serif"
      FontBold        =   0   'False
      FontSize        =   7.8
      FontItalic      =   0   'False
      FontUnderline   =   0   'False
   End
   Begin VB.CommandButton btnExhibits 
      Caption         =   "E&xhibits/Schedules"
      Height          =   400
      Left            =   2685
      TabIndex        =   9
      Top             =   3840
      Width           =   1530
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4335
      TabIndex        =   10
      Top             =   3840
      Width           =   870
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5265
      TabIndex        =   11
      Top             =   3840
      Width           =   885
   End
   Begin VB.CommandButton btnTitlePage 
      Caption         =   "Title Pa&ge"
      Height          =   400
      Left            =   1680
      TabIndex        =   8
      Top             =   3840
      Width           =   930
   End
   Begin VB.TextBox txtDocumentTitle 
      Appearance      =   0  'Flat
      Height          =   1215
      Left            =   1680
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   2475
      Width           =   4440
   End
   Begin VB.Label lblDeliveryPhrase2 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Confidential Phrases:"
      ForeColor       =   &H00FFFFFF&
      Height          =   450
      Left            =   0
      TabIndex        =   4
      Top             =   1590
      Width           =   1470
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblType 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Document Type:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   150
      TabIndex        =   0
      Top             =   270
      Width           =   1320
   End
   Begin VB.Label lblDocumentTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Document &Title:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   150
      TabIndex        =   6
      Top             =   2445
      Width           =   1320
   End
   Begin VB.Label lblDPhrases 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   " Delivery &Phrases:"
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   150
      TabIndex        =   2
      Top             =   720
      Width           =   1320
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6075
      Left            =   -30
      Top             =   -390
      Width           =   1605
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmBusiness"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_oBus As CBusiness
Private m_bCancelled As Boolean
Private m_oTitlePage As MPO.CBusinessTitlePage
Private m_oExhibits As MPO.CExhibits

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Let Business(oNew As CBusiness)
    Set m_oBus = oNew
End Property

Public Property Get Business() As CBusiness
    Set Business = m_oBus
End Property

Public Property Let Exhibits(oNew As MPO.CExhibits)
    Set m_oExhibits = oNew
End Property
Public Property Get Exhibits() As MPO.CExhibits
    Set Exhibits = m_oExhibits
End Property

Public Property Let TitlePage(oNew As MPO.CBusinessTitlePage)
    Set m_oTitlePage = oNew
End Property
Public Property Get TitlePage() As MPO.CBusinessTitlePage
    Set TitlePage = m_oTitlePage
End Property

Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnExhibits_GotFocus()
    OnControlGotFocus Me.btnExhibits
End Sub

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.Hide
    DoEvents
    mpbase2.SetUserIni "Business", "DocType", Me.cmbDocType.BoundText
End Sub

Private Sub btnOK_GotFocus()
    OnControlGotFocus Me.btnOK
End Sub

Private Sub btnTitlePage_Click()
    CallTitlePage
End Sub

Private Sub btnExhibits_Click()
    CallExhibits
End Sub

Private Sub btnTitlePage_GotFocus()
    OnControlGotFocus Me.btnTitlePage
End Sub

Private Sub cmbDocType_GotFocus()
    OnControlGotFocus Me.cmbDocType
End Sub

Private Sub cmbDocType_ItemChange()
    ChangeType
End Sub
Private Sub cmbDocType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDocType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub ChangeType()
    Dim lID As Long
    On Error GoTo ProcError
    If Me.cmbDocType.BoundText <> Empty Then
        If Me.cmbDocType.BoundText <> Me.Business.TypeID Or _
                Me.Initializing Then
            Word.Application.StatusBar = "Changing the Business Document Type.  Please wait..."
            Me.MousePointer = vbHourglass
            DoEvents
            
            lID = Me.cmbDocType.BoundText
            Word.Application.ScreenUpdating = False
            EchoOff
            
'           enable controls first
            With g_oMPO.db.BusinessDefs(lID)
                Me.txtDocumentTitle.Enabled = .AllowDocumentTitle
                Me.lblDocumentTitle.Enabled = .AllowDocumentTitle
                Me.mlcDeliveryPhrase.Enabled = .AllowDeliveryPhrases
                Me.lblDPhrases.Enabled = .AllowDeliveryPhrases
                '9.7.1 #4024
                If .SplitDPhrases Then
                    Me.mlcDeliveryPhrase.List = g_oMPO.db.Lists("DPhrases1").ListItems.Source
                    Me.mlcDeliveryPhrase2.List = g_oMPO.db.Lists("DPhrases2").ListItems.Source
                    Me.lblDeliveryPhrase2.Enabled = .AllowDeliveryPhrases
                    Me.mlcDeliveryPhrase2.Enabled = .AllowDeliveryPhrases
                    If .DPhrase1Caption <> "" Then _
                        Me.lblDPhrases.Caption = .DPhrase1Caption
                    If .DPhrase2Caption <> "" Then _
                        Me.lblDeliveryPhrase2.Caption = .DPhrase2Caption
                Else
                    Me.mlcDeliveryPhrase.List = g_oMPO.db.Lists("DPhrases").ListItems.Source
                    If Me.mlcDeliveryPhrase2.Visible Then
                        Me.mlcDeliveryPhrase2.Enabled = False
                        Me.lblDeliveryPhrase2.Enabled = False
                    End If
                    If .DPhrase1Caption <> "" Then _
                        Me.lblDPhrases.Caption = .DPhrase1Caption
                End If
                '---
            End With
        
'           set type - this will apply the type to the doc
            Me.Business.TypeID = lID
            
'           this is necessary due to a Word2000 bug where the style
'           box does not always reflect the selection's style
            g_oMPO.RefreshSelectedStyle
            
            Word.Application.ScreenUpdating = True
            EchoOn
            Word.Application.ScreenRefresh
            
            Me.MousePointer = vbDefault
            Word.Application.StatusBar = ""
        End If
    End If
    Exit Sub
ProcError:
    Me.MousePointer = vbDefault
    Word.Application.ScreenUpdating = True
    EchoOn
    Word.Application.ScreenRefresh
    g_oError.Show Err, "Could not successfully change document type."
End Sub

Private Sub cmbDocType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbDocType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    If Me.Initializing Then ' 9.5.2
        If Me.Business.Document.IsCreated Then
            UpdateForm
        Else
            Me.cmbDocType.BoundText = GetUserIni("Business", "DocType")
            If Me.cmbDocType.BoundText = "" Then
                Me.cmbDocType.SelectedItem = 0
            End If
        End If
        'MoveToLastPosition Me
    End If ' End 9.5.2
    Me.Initializing = False
    Screen.MousePointer = vbDefault
    Word.Application.ScreenUpdating = True
    EchoOn
    ' 'MoveToLastPosition Me ' 9.5.2
End Sub

Private Sub Form_Load()
    Me.Initializing = True
    m_bCancelled = True
    Dim bSplitPhrases As Boolean '9.7.1 #4024
    Dim sDiff As Single '9.7.1 #4024
    Dim i As Integer
    
    Application.ScreenUpdating = False
    
    Me.Top = 20000
    
    '9.7.1 #4024
    With g_oMPO.db.BusinessDefs
        ' Show 2nd Dphrase control if it's enabled for any type
        For i = 0 To .Count - 1
            If .Item(.ListSource.value(i, 1)).SplitDPhrases Then
                bSplitPhrases = True
                Exit For
            End If
        Next i
    End With
    If bSplitPhrases Then
        Me.lblDeliveryPhrase2.Visible = True
        Me.mlcDeliveryPhrase2.Visible = True
    Else
        sDiff = Me.txtDocumentTitle.Top - Me.mlcDeliveryPhrase2.Top
        Me.lblDeliveryPhrase2.Visible = False
        Me.mlcDeliveryPhrase2.Visible = False
        Me.lblDocumentTitle.Top = Me.lblDocumentTitle.Top - sDiff
        Me.txtDocumentTitle.Top = Me.txtDocumentTitle.Top - sDiff
        Me.btnOK.Top = Me.btnOK.Top - sDiff
        Me.btnCancel.Top = Me.btnOK.Top
        Me.btnExhibits.Top = Me.btnOK.Top
        Me.btnTitlePage.Top = Me.btnOK.Top
        Me.Height = Me.Height - sDiff
    End If
    '---
    Me.cmbDocType.Array = g_oMPO.db.BusinessDefs.ListSource
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oPrevControl = Nothing
    'mpbase2.'SetLastPosition Me
End Sub

Private Sub mlcDeliveryPhrase_BeforeItemAdded(xItem As String)
    On Error GoTo ProcError
    xItem = mpbase2.AppendToDPhrase(xItem)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcDeliveryPhrase_GotFocus()
    OnControlGotFocus Me.mlcDeliveryPhrase
End Sub

Private Sub mlcDeliveryPhrase_LostFocus()
    Me.Business.DeliveryPhrases = Me.mlcDeliveryPhrase.Text
End Sub
Private Sub mlcDeliveryPhrase2_GotFocus() '9.7.1 #4024
    OnControlGotFocus Me.mlcDeliveryPhrase2
End Sub

Private Sub mlcDeliveryPhrase2_LostFocus() '9.7.1 #4024
    Me.Business.DeliveryPhrases2 = Me.mlcDeliveryPhrase2.Text
End Sub

Private Sub txtDocumentTitle_GotFocus()
    OnControlGotFocus Me.txtDocumentTitle
End Sub

Private Sub txtDocumentTitle_LostFocus()
    Me.Business.DocumentTitle = Me.txtDocumentTitle
    Application.ScreenRefresh
End Sub

Private Sub UpdateForm()
    With Me.Business
        Me.cmbDocType.BoundText = .TypeID
        cmbDocType_ItemChange
        Me.txtDocumentTitle = .DocumentTitle
        Me.mlcDeliveryPhrase.Text = .DeliveryPhrases
        Me.mlcDeliveryPhrase2.Text = .DeliveryPhrases2 '9.7.1 #4024
    End With
End Sub

Private Sub CallTitlePage()
    Dim oForm As frmTitlePage
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    Set oForm = New frmTitlePage
    
    With oForm
        DoEvents
        
        'GLOG : 5594|5381
        'force accelerator cues in Word 2013 & newer
        If g_bIsWord15x Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If

        If m_oTitlePage Is Nothing Then
            Set m_oTitlePage = g_oMPO.NewTitlePage
            m_oTitlePage.TypeID = Me.Business.Definition.DefaultTitlePage
        End If
        
        .TitlePage = m_oTitlePage
        .ParentForm = Me
        .Left = Me.Left
        .Top = Me.Top
        .Show vbModal
        
        If Not .Cancelled Then
            Set m_oTitlePage = .TitlePage
        Else
            Set m_oTitlePage = Nothing
        End If
        
        Unload oForm
        Set oForm = Nothing
        
        'GLOG : 5594|5381
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    End With
    
    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    g_oError.Show Err
    Exit Sub

End Sub

Private Sub CallExhibits()
    Dim oForm As frmExhibits
    Dim lKeyboardCues As Long
    Dim bRestoreNoCues As Boolean
    
    Set oForm = New frmExhibits
    
    With oForm
        DoEvents
        
        'GLOG : 5594|5381
        'force accelerator cues in Word 2013 & newer
        If g_bIsWord15x Then
            lRet = GetSystemParametersInfo(SPI_GETKEYBOARDCUES, 0, lKeyboardCues, 0)
            If lKeyboardCues = 0 Then
                SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 1, 2
                bRestoreNoCues = True
            End If
        End If

        If Not (m_oExhibits Is Nothing) Then
            .Exhibits = m_oExhibits
        End If
        
        .ParentForm = Me
        .Left = Me.Left
        .Top = Me.Top
        .Show vbModal
        
        If Not .Cancelled Then
            Set m_oExhibits = .Exhibits
        End If
        
        Unload oForm
        Set oForm = Nothing
        
        'GLOG : 5594|5381
        'turn off accelerator cues if necessary
        If bRestoreNoCues Then _
            SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2
        
    End With
    
    Exit Sub
ProcError:
    'GLOG : 5594|5381
    'turn off accelerator cues if necessary
    If bRestoreNoCues Then _
        SetSystemParametersInfo SPI_SETKEYBOARDCUES, 0, 0, 2

    g_oError.Show Err
    Exit Sub

End Sub

