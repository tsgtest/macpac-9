VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmVerification 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   8220
   ClientLeft      =   1932
   ClientTop       =   636
   ClientWidth     =   5484
   Icon            =   "frmVerification.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8220
   ScaleWidth      =   5484
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkManualEntry 
      Appearance      =   0  'Flat
      Caption         =   "En&ter Signer Info Manually"
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   1815
      TabIndex        =   3
      Top             =   480
      Width           =   2235
   End
   Begin TrueDBList60.TDBCombo cmbAuthor 
      Height          =   600
      Left            =   1755
      OleObjectBlob   =   "frmVerification.frx":058A
      TabIndex        =   1
      Top             =   135
      Width           =   3615
   End
   Begin VB.TextBox txtSignerName 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   150
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtCity 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      ScrollBars      =   2  'Vertical
      TabIndex        =   21
      Top             =   3465
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtNotaryName 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      ScrollBars      =   2  'Vertical
      TabIndex        =   27
      Top             =   4410
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtDeceasedName 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      ScrollBars      =   2  'Vertical
      TabIndex        =   13
      Top             =   2370
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtPartyName 
      Appearance      =   0  'Flat
      Height          =   480
      Left            =   1755
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Top             =   2160
      Width           =   3615
   End
   Begin VB.TextBox txtFooterText 
      Appearance      =   0  'Flat
      Height          =   480
      Left            =   1755
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   33
      Top             =   5610
      Width           =   3615
   End
   Begin VB.TextBox txtDocumentTitle 
      Appearance      =   0  'Flat
      Height          =   765
      Left            =   1755
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   7
      Top             =   1320
      Width           =   3615
   End
   Begin VB.TextBox txtCustom1 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      ScrollBars      =   2  'Vertical
      TabIndex        =   35
      Top             =   6195
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtCustom2 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      ScrollBars      =   2  'Vertical
      TabIndex        =   37
      Top             =   6600
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtPartyTitle 
      Appearance      =   0  'Flat
      Height          =   480
      Left            =   1755
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   11
      Top             =   2160
      Width           =   3615
   End
   Begin VB.TextBox txtCorporationName 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      ScrollBars      =   2  'Vertical
      TabIndex        =   15
      Top             =   2730
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtAttorneysFor 
      Appearance      =   0  'Flat
      Height          =   480
      Left            =   1755
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   25
      Top             =   4260
      Width           =   3615
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4635
      TabIndex        =   39
      Top             =   7050
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3945
      TabIndex        =   38
      Top             =   7050
      Width           =   650
   End
   Begin TrueDBList60.TDBCombo cmbOffice 
      Height          =   600
      Left            =   1755
      OleObjectBlob   =   "frmVerification.frx":44CF
      TabIndex        =   5
      Top             =   885
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbExecuteDate 
      Height          =   375
      Left            =   1755
      OleObjectBlob   =   "frmVerification.frx":66E0
      TabIndex        =   29
      Top             =   4785
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbGender 
      Height          =   375
      Left            =   1755
      OleObjectBlob   =   "frmVerification.frx":8902
      TabIndex        =   31
      Top             =   5190
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbCorporatePosition 
      Height          =   375
      Left            =   1755
      OleObjectBlob   =   "frmVerification.frx":AB1F
      TabIndex        =   17
      Top             =   3090
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbState 
      Height          =   375
      Left            =   1755
      OleObjectBlob   =   "frmVerification.frx":CD47
      TabIndex        =   23
      Top             =   3840
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbCounty 
      Height          =   375
      Left            =   1755
      OleObjectBlob   =   "frmVerification.frx":EF63
      TabIndex        =   19
      Top             =   3465
      Width           =   3615
   End
   Begin VB.Label lblNotaryName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "No&tary Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   26
      Top             =   4455
      Width           =   1455
   End
   Begin VB.Label lblDeceasedName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Na&me of Deceased:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   12
      Top             =   2415
      Width           =   1455
   End
   Begin VB.Label lblCounty 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Co&unty:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   18
      Top             =   3510
      Width           =   1455
   End
   Begin VB.Label lblState 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&State:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   22
      Top             =   3915
      Width           =   1455
   End
   Begin VB.Label lblCity 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "C&ity:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   20
      Top             =   3510
      Width           =   1455
   End
   Begin VB.Label lblPartyName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Party &Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   105
      TabIndex        =   8
      Top             =   2160
      Width           =   1455
   End
   Begin VB.Label lblGender 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Gende&r:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   105
      TabIndex        =   30
      Top             =   5250
      Width           =   1455
   End
   Begin VB.Label lblFooterText 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Footer Te&xt:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   105
      TabIndex        =   32
      Top             =   5610
      Width           =   1455
   End
   Begin VB.Label lblDocumentTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Document Title(s):"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   105
      TabIndex        =   6
      Top             =   1305
      Width           =   1455
   End
   Begin VB.Label lblExecuteDate 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Date &Executed:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   105
      TabIndex        =   28
      Top             =   4845
      Width           =   1455
   End
   Begin VB.Label lblCustom1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Custom #1:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   34
      Top             =   6240
      Width           =   1455
   End
   Begin VB.Label lblCustom2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Custom #2:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   36
      Top             =   6645
      Width           =   1455
   End
   Begin VB.Label lblOffice 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Office:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   4
      Top             =   930
      Width           =   1455
   End
   Begin VB.Label lblAuthor 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Author:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   0
      Top             =   180
      Width           =   1455
   End
   Begin VB.Label lblPartyTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Party &Title:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   105
      TabIndex        =   10
      Top             =   2325
      Width           =   1455
   End
   Begin VB.Label lblCorporationName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Corporation:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   14
      Top             =   2775
      Width           =   1455
   End
   Begin VB.Label lblCorporatePosition 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Position:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   16
      Top             =   3135
      Width           =   1455
   End
   Begin VB.Label lblAttorneysFor 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Attorne&ys For:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   105
      TabIndex        =   24
      Top             =   4275
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      FillColor       =   &H00800080&
      Height          =   9495
      Left            =   -120
      Top             =   -15
      Width           =   1800
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmVerification"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oVerification As MPO.CVerification
Private m_bCancelled As Boolean
Private m_bInit As Boolean
Private m_oPersonsForm As MPO.CPersonsForm
Private m_oMenu As MacPac90.CAuthorMenu
Private m_bAuthorIsDirty As Boolean
Private m_bItemChanged As Boolean
Private m_lFooterTextMaxLength  As Long

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Get FooterTextMaxLength() As Long
    FooterTextMaxLength = m_lFooterTextMaxLength
End Property
Public Property Let FooterTextMaxLength(lNew As Long)
    m_lFooterTextMaxLength = lNew
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Set Verification(oNew As MPO.CVerification)
    Set m_oVerification = oNew
End Property
 
Public Property Get Verification() As MPO.CVerification
    Set Verification = m_oVerification
End Property
Public Sub PositionControls()
'adjust dialog for manual entry / no manual entry
    Const iSpaceBefore As Integer = 165
    Dim oCtl As Control
    Dim oLastVisible As Control
    Dim arrCtls() As String
    Dim i As Integer
    
    On Error GoTo ProcError

    ReDim arrCtls(Me.Controls.Count)
    
'   place each control in tab order sort
    For Each oCtl In Me.Controls
        On Error Resume Next
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next

'   position controls based on visibility and tab index -
'   skip author and sifgner name controls - they'll always
'   appear at the top of the dlg at the same y position
    Set oLastVisible = Me.cmbOffice
    For i = 7 To 37
'       get control
        Set oCtl = Me.Controls(arrCtls(i))
'       do only if visible
        If oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is VB.CheckBox) Then
'               position control relative to previous control
                oCtl.Top = oLastVisible.Top + _
                           oLastVisible.Height + _
                           iSpaceBefore
                
'               set accompanying label vertical position
                If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                    Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                End If
                
'               set control as last visible control
                Set oLastVisible = oCtl
            End If
        End If
    Next i
    
    Me.btnFinish.Top = oLastVisible.Top + _
        oLastVisible.Height + 210
        
    Me.btnCancel.Top = Me.btnFinish.Top

'   size dialog
    '*Word15
    Me.Height = Me.btnFinish.Top + Me.btnFinish.Height + IIf(g_bIsWord15x, 550 + g_sWord15HeightAdjustment, 550)
    
    Word.Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmVerification.Resize"
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    m_bCancelled = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    SetControlBackColor Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    m_bCancelled = False
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Sub ChangeAuthor(ByVal lID As Long)
    Dim xMsg As String
    Dim bForce As Boolean
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If
    
    On Error GoTo 0

    With Me.Verification
'       set new author
        .Author = g_oMPO.People(lID)
        
        If g_oMPO.UseDefOfficeInfo Then
            Me.cmbOffice.BoundText = g_oMPO.Offices.Default.ID
        Else
            Me.cmbOffice.BoundText = .Author.Office.ID
        End If
        
'        Me.txtCity = .Author.Office.City
'        Me.cmbState.BoundText = .Author.Office.State
    End With
    
    
    If Me.ActiveControl.Name = "cmbAuthor" Then _
        cmbAuthor_GotFocus
        
    Word.ActiveDocument.Fields.Update
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    SetControlBackColor Me.btnFinish
End Sub

Private Sub chkManualEntry_Click()
    Dim bForce As Boolean
    Dim bMan As Boolean
    
    On Error GoTo ProcError
    
    DoEvents
    
    bMan = Me.chkManualEntry.value = vbChecked
    SetManualEntry bMan
    PositionControls
    bForce = g_oMPO.ForceItemUpdate
    If bMan Then
'       don't force update if the dialog is intializing -
'       if we're in this branch and the dialog is initializing,
'       we must be defaulting to Manual input - if so, we
'       don't want the document placeholders wiped out when
'       there's going to be no input
        If Not m_bInit Then
            g_oMPO.ForceItemUpdate = True
        End If
        Me.txtSignerName.SetFocus
    Else
        g_oMPO.ForceItemUpdate = True
        Me.Verification.UpdateForAuthor
    End If
    
    Me.Verification.ManualEntry = Me.chkManualEntry
    
    g_oMPO.ForceItemUpdate = bForce
    Exit Sub
ProcError:
    g_oError.Show Err, "An error occurred while setting entry options."

End Sub

Private Sub cmbAuthor_GotFocus()
    On Error GoTo ProcError
    OnControlGotFocus Me.cmbAuthor, , True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = wdKeyF10 And Shift = 1 Then
        ShowAuthorMenu
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthorIsDirty = False
        If Len(Me.cmbAuthor.BoundText) Then
            cmbAuthor_ItemChange
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_LostFocus()
'   update for author only if item change event ran
    On Error GoTo ProcError
    If m_bItemChanged Then
        With Me
'           update object with values of selected person
            If Not .Initializing Then
                .Verification.UpdateForAuthor
            End If
        End With
        
        Word.ActiveDocument.Fields.Update

        m_bItemChanged = False
    End If
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
    If Button = 2 Then ShowAuthorMenu
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub ShowAuthorMenu()
    Dim bEnabled As Boolean
    On Error GoTo ProcError
    
'   enable specified menu items only if author
'   is currently in the db
    If TypeOf Me.Verification.Author Is CReUseAuthor Then
        bEnabled = False
    Else
        bEnabled = True
    End If
    
    Me.mnuAuthor_Favorite.Enabled = bEnabled
    Me.mnuAuthor_SetDefault.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Checked = Me.Verification.Author.Favorite
    Me.PopupMenu mnuAuthor
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If m_bAuthorIsDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_ItemChange()
    On Error GoTo ProcError
    With Me.cmbAuthor
        If .BoundText <> Me.Verification.Author.ID Then
            ChangeAuthor .BoundText
            m_bAuthorIsDirty = True
            m_bItemChanged = True
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCorporatePosition_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCorporatePosition, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOffice_ItemChange()
    On Error GoTo ProcError
    If Not Me.Initializing Or Me.txtCity = "" Then
    With Me
        .txtCity = g_oMPO.Offices(.cmbOffice.BoundText).City
        .cmbState.BoundText = g_oMPO.Offices(.cmbOffice.BoundText).State
    End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbState_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbState, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbCounty_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCounty, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbGender_ItemChange()
    On Error GoTo ProcError
    Me.Verification.SignerGender = cmbGender.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOffice_GotFocus()
    On Error GoTo ProcError
    OnControlGotFocus Me.cmbOffice
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOffice_LostFocus()
    On Error GoTo ProcError
    With Me.Verification
        .Office = g_oMPO.db.Offices(Me.cmbOffice.BoundText)
        .UpdateForOffice
    End With
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not successfully change the office."
    Exit Sub
End Sub

Private Sub cmbOffice_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbOffice, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbOffice_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbOffice)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    Dim xDocTitle As String
    Dim xOrdSuffix As String
    Dim xDefDate As String
    Dim oDoc As MPO.CDocument
    
    On Error GoTo ProcError
    If Me.Initializing Then
        LockWindowUpdate (Me.hwnd)
        With Me.Verification
            If .Document.IsCreated And _
                .Document.Template = .Template.FileName Then
                UpdateForm
            Else
'               set author to default author
                With .Author
                    Me.cmbAuthor.BoundText = .ID
                    Me.Verification.UpdateForAuthor
                    If g_oMPO.UseDefOfficeInfo Then
                        With g_oMPO.Offices.Default
                            Me.cmbOffice.BoundText = .ID
                            Me.cmbState.BoundText = .State
                            Me.txtCity = .City
                            Me.Verification.Office = g_oMPO.Offices(g_oMPO.Offices.Default.ID)
                            Me.Verification.UpdateForOffice
                        End With
                    Else
                        With Me.Verification.Author.Office
                            Me.Verification.Office = Me.Verification.Author.Office
                            Me.cmbOffice.BoundText = .ID
                            Me.cmbState.BoundText = .State
                            Me.txtCity = .City
                        End With
                    End If
                End With
                m_bAuthorIsDirty = False
    
                Set oDoc = New MPO.CDocument

'               set starting selections
                On Error Resume Next
                'GLOG : 5052 : CEH
                
                GetExistingValues
                                
                Me.cmbGender.SelectedItem = 0
                .Custom1 = .Definition.Custom1DefaultText
                Me.txtCustom1 = .Custom1
                .Custom2 = .Definition.Custom2DefaultText
                Me.txtCustom2 = .Custom2
                
                With .Definition
                    If .AllowExecuteDate Then
                        Me.cmbExecuteDate.BoundText = .DefaultExecuteDate
                    End If
                End With
                On Error GoTo ProcError

'               set props based on selected items
                If Me.cmbGender.BoundText <> Empty Then
                    .SignerGender = Me.cmbGender.BoundText
                End If
                
'               could not get .text property below to return
'               the correct values without a preceding DoEvents
                DoEvents
                .ExecuteDate = Me.cmbExecuteDate.Text
                Me.cmbCorporatePosition.SelectedItem = 0
                
            End If

            DoEvents
            .CorporatePosition = Me.cmbCorporatePosition.Text
            
            With .Definition
                If .DefaultFooterText <> Empty Then _
                    Me.Verification.PleadingPaper.DocTitle = .DefaultFooterText
                
                If .FooterTextMaxChars Then
                    Me.txtFooterText = Me.Verification.PleadingPaper.DocTitle
                End If
            End With
        End With
        'GLOG : 5052 : CEH
'       force reposition of controls
        PositionControls
        
'       update any fields
        Me.Verification.Document.UpdateFields
        
        g_oMPO.ForceItemUpdate = False
        
'       move dialog to last position
        'MoveToLastPosition Me
        LockWindowUpdate (0&)
        
        DoEvents
        Screen.MousePointer = vbDefault
        
        EchoOn
        Word.Application.ScreenUpdating = True
        Word.Application.ScreenRefresh
        
        If Me.chkManualEntry.value <> vbChecked Then
            Me.cmbAuthor.SetFocus
            cmbAuthor_GotFocus
        End If
        
    End If
    SendKeys "+", True
    m_bInit = False
    
    Exit Sub
ProcError:
    g_oMPO.ForceItemUpdate = False
    Word.Application.ScreenUpdating = True
    EchoOn
    Screen.MousePointer = vbDefault
    m_bInit = False
    g_oError.Show Err, "Error Activating Verification Form"
    Exit Sub
End Sub

Private Sub cmbExecuteDate_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.cmbExecuteDate, "zzmpExecuteDate"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbExecuteDate_LostFocus()
    On Error GoTo ProcError
    Me.Verification.ExecuteDate = Me.cmbExecuteDate.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbCorporatePosition_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.cmbCorporatePosition, "zzmpCorporatePosition"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCorporatePosition_LostFocus()
    On Error GoTo ProcError
    Me.Verification.CorporatePosition = Me.cmbCorporatePosition.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbState_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.cmbState, "zzmpVerificationState"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbState_LostFocus()
    On Error GoTo ProcError
    Me.Verification.VerificationState = Me.cmbState.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCity_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtCity, "zzmpVerificationCity"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCity_LostFocus()
    On Error GoTo ProcError
    Me.Verification.VerificationCity = Me.txtCity.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbCounty_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.cmbCounty, "zzmpVerificationCounty"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCounty_LostFocus()
    On Error GoTo ProcError
    Me.Verification.VerificationCounty = Me.cmbCounty.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbGender_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.cmbGender
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbGender)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbGender, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    OnFormKeyDown KeyCode
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oGenders, oPositions As XArray
    Dim oList As CList
    Dim oDateFormats As CList
    Dim oDef As CVerificationDef
    Dim i As Integer
    Dim oArray As XArray
    Dim xOrdSuffix As String
    
    On Error GoTo ProcError
    Word.Application.StatusBar = "Initializing.  Please wait..."
    Application.ScreenUpdating = False

    m_bInit = True
    
    Me.Cancelled = True

'   move dialog way down - form activate
'   resizes the dialog - to prevent the user
'   from seeing weirdness, resize off the screen
'   then move into position
    Me.Top = 20000
    
'   ensure date update
    g_oMPO.ForceItemUpdate = True
    
    Set m_oPersonsForm = New MPO.CPersonsForm
    Set m_oMenu = New MacPac90.CAuthorMenu
    Set oDef = Me.Verification.Definition
    
'   set up date controls
    Set oDateFormats = g_oMPO.Lists("DateFormatsVariable")
    With oDateFormats
'       the date formats value of the definition
'       is the id of the group of date formats
        .FilterValue = oDef.DateFormats
        .Refresh
    End With
    
'   set up county control if allowed
    If oDef.AllowCounty Then
        Set oList = g_oMPO.Lists("Counties")
        With oList
            .FilterValue = oDef.Level0
            .Refresh
            Me.cmbCounty.Array = .ListItems.Source
        End With
        Me.cmbCounty.Rebind
        ResizeTDBCombo Me.cmbCounty, 6
    End If

'   Load 'state' cmbox
    If oDef.AllowState Then
        With g_oMPO.Lists
            Me.cmbState.Array = _
                .Item("PleadingCourtsLevel0").ListItems.Source
        End With
        ResizeTDBCombo Me.cmbState, 6
    End If
    
    With oDef
'       enable/disable footer text
        Me.txtFooterText.Visible = .FooterTextMaxChars
        Me.lblFooterText.Visible = .FooterTextMaxChars
'       enable/disable date as appropriate
        Me.cmbExecuteDate.Visible = .AllowExecuteDate
        Me.lblExecuteDate.Visible = .AllowExecuteDate
'       enable/disable gender control
        Me.cmbGender.Visible = .AllowGender
        Me.lblGender.Visible = .AllowGender
'       enable/disable "attorneys for" control
        Me.txtAttorneysFor.Visible = .AllowAttorneyFor
        Me.lblAttorneysFor.Visible = .AllowAttorneyFor
'       enable/disable state control
        Me.cmbState.Visible = .AllowState
        Me.lblState.Visible = .AllowState
'       enable/disable county control
        Me.cmbCounty.Visible = .AllowCounty
        Me.lblCounty.Visible = .AllowCounty
'       enable/disable city control
        Me.txtCity.Visible = .AllowCity
        Me.lblCity.Visible = .AllowCity
'       enable/disable corporate position control
        Me.cmbCorporatePosition.Visible = .AllowCorporatePosition
        Me.lblCorporatePosition.Visible = .AllowCorporatePosition
'       enable/disable corporation name control
        Me.txtCorporationName.Visible = .AllowCorporationName
        Me.lblCorporationName.Visible = .AllowCorporationName
'       enable/disable deceased name control
        Me.txtDeceasedName.Visible = .AllowDeceasedName
        Me.lblDeceasedName.Visible = .AllowDeceasedName
'       enable/disable notary name control
        Me.txtNotaryName.Visible = .AllowNotaryName
        Me.lblNotaryName.Visible = .AllowNotaryName
'       enable/disable party name control
        Me.txtPartyName.Visible = .AllowPartyName
        Me.lblPartyName.Visible = .AllowPartyName
'       enable/disable party title control
        Me.txtPartyTitle.Visible = .AllowPartyTitle
        Me.lblPartyTitle.Visible = .AllowPartyTitle
            
        '---4092 - footer text max length

        On Error GoTo ProcError
        Dim lRet As Long
        lRet = .FooterTextMaxChars
        If lRet > 0 Then
            Me.txtFooterText.MaxLength = lRet
            Me.FooterTextMaxLength = lRet
        End If
'       get date list
        Set oArray = oDateFormats.ListItems.Source
        
'       get ordinal suffix based on current date - eg "rd", "th"
        xOrdSuffix = GetOrdinalSuffix(Day(Date))
        
'       replace all instances of ordinal variable
'       with ordinal suffix-this is done here because
'       there is no way in vb to format a dateas an ordinal
        For i = oArray.LowerBound(1) To oArray.UpperBound(1)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%o%", xOrdSuffix)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%or%", xOrdSuffix)  '9.7.1 #4402
        Next i
        
'       set up date controls
        If .AllowExecuteDate Then
            Me.cmbExecuteDate.Array = oArray
            Me.cmbExecuteDate.Rebind
            ResizeTDBCombo Me.cmbExecuteDate, 4
        End If
                
        If .Custom1Label <> "" Then
            Me.lblCustom1 = .Custom1Label
            Me.txtCustom1.Visible = True
            Me.lblCustom1.Visible = True
        Else
            Me.lblCustom1.Visible = False
            Me.txtCustom1.Visible = False
        End If
        
        If .Custom2Label <> "" Then
            Me.lblCustom2 = .Custom2Label
            Me.lblCustom2.Visible = True
            Me.txtCustom2.Visible = True
        Else
            Me.lblCustom2.Visible = False
            Me.txtCustom2.Visible = False
        End If
    End With
    
'   set up gender control if allowed
    If oDef.AllowGender Then
        Set oGenders = xarStringToxArray( _
            "Indefinite (he/she/they)|0|Female " & _
            "(she)|1|Male (he)|2|Plural (they)|3|Unknown (____)|4", 2)
        Me.cmbGender.Array = oGenders
        Me.cmbGender.Rebind
        ResizeTDBCombo Me.cmbGender, 5
    End If

'   set up corporate position control if allowed
    If oDef.AllowCorporatePosition Then
        Set oPositions = xarStringToxArray( _
            "an officer|0|a partner|1|", 2)
        Me.cmbCorporatePosition.Array = oPositions
        Me.cmbCorporatePosition.Rebind
        ResizeTDBCombo Me.cmbCorporatePosition, 4
    End If
    
'   set up county control if allowed
    If oDef.AllowCounty Then
        Set oList = g_oMPO.Lists("Counties")
        With oList
            .FilterValue = oDef.Level0
            .Refresh
            Me.cmbCounty.Array = .ListItems.Source
        End With
        Me.cmbCounty.Rebind
        ResizeTDBCombo Me.cmbCounty, 6
    End If
    
    With oDef
        Me.Caption = .DialogCaption
    End With

    g_oMPO.People.Refresh
    
    With Me.cmbAuthor
        .Array = g_oMPO.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
    End With

    With Me.cmbOffice
        .Array = g_oMPO.Offices.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbOffice, 10
    End With

    If Not Me.Verification.Document.IsSegment Then
        g_oMPO.ForceItemUpdate = False
    End If

    LockWindowUpdate Me.hwnd
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oPersonsForm = Nothing
    Set m_oMenu = Nothing
    Set g_oPrevControl = Nothing
    'SetLastPosition Me
End Sub



Private Sub txtCustom1_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtCustom1, "zzmpVerificationCustom1"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCustom1_LostFocus()
    On Error GoTo ProcError
    Me.Verification.Custom1 = Me.txtCustom1
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCustom2_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtCustom2, "zzmpVerificationCustom2"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub txtCustom2_LostFocus()
    On Error GoTo ProcError
    Me.Verification.Custom2 = Me.txtCustom2
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFooterText_Change()
    FooterLengthWarningIfNecessary
End Sub

Private Sub txtFooterText_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyTab, vbKeyBack, vbKeyDelete, vbKeyLeft, vbKeyRight, vbKeyUp, vbKeyDown
        Case Else
            FooterLengthWarningIfNecessary
    End Select
End Sub


Private Sub txtFooterText_LostFocus()
    On Error GoTo ProcError
    Me.Verification.PleadingPaper.DocTitle = Me.txtFooterText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPartyName_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtPartyName, "zzmpPartyName"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtAttorneysFor_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtAttorneysFor, "zzmpAttorneysFor"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtAttorneysFor_LostFocus()
    On Error GoTo ProcError
    Me.Verification.AttorneysFor = Me.txtAttorneysFor
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPartyName_LostFocus()
    On Error GoTo ProcError
    Me.Verification.PartyName = Me.txtPartyName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtCorporationName_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtCorporationName, "zzmpCorporationName"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCorporationName_LostFocus()
    On Error GoTo ProcError
    Me.Verification.CorporationName = Me.txtCorporationName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtNotaryName_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtNotaryName, "zzmpNotaryName"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtNotaryName_LostFocus()
    On Error GoTo ProcError
    Me.Verification.NotaryName = Me.txtNotaryName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDeceasedName_GotFocus()
    On Error GoTo ProcError
    DoEvents
    bEnsureSelectedContent Me.txtDeceasedName
    SetControlBackColor Me.txtDeceasedName
    Me.Verification.Document.SelectItem "zzmpDeceasedName"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDeceasedName_LostFocus()
    On Error GoTo ProcError
    Me.Verification.DeceasedName = Me.txtDeceasedName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPartyTitle_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtPartyTitle, "zzmpPartyTitle"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPartyTitle_LostFocus()
    On Error GoTo ProcError
    Me.Verification.PartyTitle = Me.txtPartyTitle
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDocumentTitle_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtDocumentTitle, "zzmpDocumentTitles"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDocumentTitle_LostFocus()
    On Error GoTo ProcError
    Me.Verification.DocumentTitles = Me.txtDocumentTitle
    If Me.Verification.Definition.DefaultFooterText = "" And _
    Me.txtFooterText.Visible Then
        Me.txtFooterText = Me.txtDocumentTitle
        Me.Verification.PleadingPaper.DocTitle = Me.txtFooterText    'more accurate than txtDocumentTitle, based on MaxFooterLength
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub txtFooterText_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.txtFooterText, "zzmpDocTitle"
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub UpdateForm()
    On Error GoTo ProcError
    With Me.Verification
        Me.cmbAuthor.Text = .Document.ReuseAuthor.DisplayName
        Me.cmbOffice.BoundText = .Office.ID
        Me.txtDocumentTitle = .DocumentTitles
        If .Definition.FooterTextMaxChars Then _
            Me.txtFooterText = .PleadingPaper.DocTitle
        
        Me.cmbCounty.BoundText = .VerificationCounty
        Me.txtCity = .VerificationCity
        Me.cmbState.BoundText = .VerificationState
        
        Me.cmbExecuteDate.Text = .ExecuteDate
        Me.cmbGender.BoundText = .SignerGender
        Me.txtPartyName = .PartyName
        Me.txtPartyTitle = .PartyTitle
        Me.txtCorporationName = .CorporationName
        Me.cmbCorporatePosition.BoundText = .CorporatePosition
        Me.txtDeceasedName = .DeceasedName
        Me.txtNotaryName = .NotaryName
        Me.txtAttorneysFor = .AttorneysFor
        Me.txtSignerName = .AuthorName
        If .ManualEntry <> mpUndefined Then _
                Me.chkManualEntry = Abs(.ManualEntry)
        
        If .Definition.Custom1Label <> Empty Then _
            Me.txtCustom1 = .Custom1
        If .Definition.Custom2Label <> Empty Then _
            Me.txtCustom2 = .Custom2
    End With
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmVerification.UpdateForm"
    Exit Sub
End Sub
'******************************************************************************
'Menu events
'******************************************************************************
Private Sub mnuAuthor_ManageLists_Click()
'   get current selection
    
    Dim lID As Long
    
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ManageAuthors Me.cmbAuthor, lID
    Else
        m_oMenu.ManageAuthors Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_AddNew_Click()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.cmbAuthor.Columns("fldID")
    m_oMenu.AddPerson Me.cmbAuthor, lID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Favorite_Click()
    Dim lID As Long
    
    On Error Resume Next
    DoEvents
    lID = Me.cmbAuthor.BoundText
    
    On Error GoTo ProcError
    If lID Then
        m_oMenu.ToggleFavorite Me.cmbAuthor, lID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    On Error GoTo ProcError
    With Me.Verification
        .Template.DefaultAuthor = .Author
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Copy_Click()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
    On Error GoTo ProcError
'   if author isn't in list, the
'   author is the reuse author
    ' If Me.cmbAuthor.Row = -1 Then
    If TypeOf Me.Verification.Author Is CReUseAuthor Then '***4337
'       use reuse author as source
        Set oSource = Me.Verification.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oMPO.People(Me.cmbAuthor.BoundText)
    End If
    
'   copy the person
    Set oCopy = m_oPersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Sub SetManualEntry(ByVal bOn As Boolean)
    On Error GoTo ProcError
    Me.cmbAuthor.Visible = Not bOn
    Me.txtSignerName.Visible = bOn
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmVerification.SetManualEntry"
    Exit Sub
End Sub


Private Sub txtSignerName_GotFocus()
    OnControlGotFocus Me.txtSignerName, "zzmpAuthorName"
End Sub

Private Sub txtSignerName_LostFocus()
    On Error GoTo ProcError
    Me.Verification.AuthorName = Me.txtSignerName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub FooterLengthWarningIfNecessary()
    Dim xMsg As String
    
    If Me.FooterTextMaxLength = 0 Then Exit Sub
    
    If Len(txtFooterText) >= Me.FooterTextMaxLength Then
        xMsg = "The footer text is" & Str(Me.FooterTextMaxLength) & " characters and might not fit in the footer table cell." & _
            vbLf & vbLf & "Please edit the text in the footer text box."
        MsgBox xMsg, vbInformation, App.Title
    End If

End Sub

'GLOG : 5052 : CEH
Private Sub GetExistingValues()
    Dim xDocTitle As String
    Dim oDoc As MPO.CDocument
    Dim xTemp As String
    Dim lID As Long
    Dim oPleading As MPO.CPleading
    Dim xPartyTitle As String
    Dim xPartyName As String
    Dim xFooterText As String
    
    On Error GoTo ProcError
    Set oDoc = New MPO.CDocument
        
'   Get pleading document title
    xDocTitle = oDoc.GetVar("PleadingCaption_1_CaseTitle")
    
    If xDocTitle = "" Then
        xDocTitle = oDoc.GetVar("Pleading_1_DocumentTitle")
    End If
    
    If xDocTitle <> "" Then
        Me.txtDocumentTitle = xDocTitle
        Me.Verification.DocumentTitles = xDocTitle
    End If
    
'   Get Pleading Party Title & Description/Name
    xPartyTitle = oDoc.RetrieveItem("PartyTitle", "PleadingCounsel")
    If xPartyTitle = Empty Then
        xPartyTitle = oDoc.RetrieveItem("PartyTitle", "PleadingSig")
    End If
    xPartyName = oDoc.RetrieveItem("PartyName", "PleadingCounsel")
    If xPartyName = Empty Then
        xPartyName = oDoc.RetrieveItem("PartyName", "PleadingSig")
    End If
    
    Me.txtPartyTitle.Text = xPartyTitle
    Me.txtPartyName.Text = xPartyName
    Me.Verification.PartyTitle = xPartyTitle
    Me.Verification.PartyName = xPartyName
    
'   Get Pleading Footer text
    xFooterText = oDoc.RetrieveItem("PleadingPaperDocTitle", "PleadingPaper")
    
    If Me.Verification.Definition.DefaultFooterText = Empty Then
        Me.Verification.PleadingPaper.DocTitle = xFooterText
    End If
    
'   Get Pleading Signer
    lID = g_oMPO.GetPleadingSignerID()
    
    If lID <> 0 Then
        m_bInit = False
        Me.cmbAuthor.BoundText = lID
        If Me.cmbAuthor.BoundText = Empty Then
            Me.cmbAuthor.SelectedItem = 0
        End If
        cmbAuthor_ItemChange
        cmbAuthor_LostFocus
        m_bInit = True
    End If

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

