VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CNumbers"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Function GetIntegerSequence(xArray() As String, _
                             iLBound As Integer, _
                             iUBound As Integer, _
                             Optional bIncludeBlankLine = False) As Boolean
    Dim i As Integer
    
    If bIncludeBlankLine Then
        ReDim xArray(0 To (iUBound - iLBound + 1))
        xArray(0) = String(4, "_")
        For i = iLBound To iUBound
            xArray(i - iLBound + 1) = i
        Next i
    Else
        ReDim xArray(0 To (iUBound - iLBound))
        For i = iLBound To iUBound
            xArray(i - iLBound) = i
        Next i
    End If
    
End Function

Function GetOrdinalSequence(xArray() As String, _
                             iLBound As Integer, _
                             iUBound As Integer, _
                             Optional bIncludeBlankLine = False) As Boolean
    Dim i As Integer
    
    If bIncludeBlankLine Then
        ReDim xArray(0 To (iUBound - iLBound + 1))
        xArray(0) = String(4, "_")
        For i = iLBound To iUBound
            xArray(i - iLBound + 1) = GetOrdinal(i)
        Next i
    Else
        ReDim xArray(0 To (iUBound - iLBound))
        For i = iLBound To iUBound
            xArray(i - iLBound) = GetOrdinal(i)
        Next i
    End If
    
End Function


Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function



Function GetOrdinal(i As Integer) As String
    If i >= 4 And i <= 20 Then
        GetOrdinal = i & "th"
    ElseIf Right(i, 1) = 1 Then
        GetOrdinal = i & "st"
    ElseIf Right(i, 1) = 2 Then
        GetOrdinal = i & "nd"
    ElseIf Right(i, 1) = 3 Then
        GetOrdinal = i & "rd"
    Else
        GetOrdinal = i & "th"
    End If
End Function





