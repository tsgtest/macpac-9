VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmSeparateStatement 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Separate Statement"
   ClientHeight    =   3372
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   5508
   Icon            =   "frmSeparateStatement.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3372
   ScaleWidth      =   5508
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin TrueDBList60.TDBCombo cmbSeparateStatementTypes 
      Height          =   600
      Left            =   1785
      OleObjectBlob   =   "frmSeparateStatement.frx":058A
      TabIndex        =   1
      Top             =   270
      Width           =   3570
   End
   Begin VB.CheckBox chkIncludeTitle 
      Appearance      =   0  'Flat
      Caption         =   "&Include Title"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   1875
      TabIndex        =   2
      Top             =   690
      Width           =   2745
   End
   Begin VB.CheckBox chkIncludeTableBorders 
      Appearance      =   0  'Flat
      Caption         =   "Include Table &Borders"
      ForeColor       =   &H80000008&
      Height          =   315
      Left            =   1875
      TabIndex        =   7
      Top             =   2160
      Width           =   2745
   End
   Begin VB.TextBox txtRightColumnTitle 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1785
      TabIndex        =   6
      Top             =   1680
      Width           =   3570
   End
   Begin VB.TextBox txtLeftColumnTitle 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1785
      TabIndex        =   4
      Top             =   1155
      Width           =   3570
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4335
      TabIndex        =   9
      Top             =   2865
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3270
      TabIndex        =   8
      Top             =   2865
      Width           =   1000
   End
   Begin VB.Label lblRightColumnTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Right Col Heading:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   60
      TabIndex        =   5
      Top             =   1710
      Width           =   1455
   End
   Begin VB.Label lbSeparateStatementTypes 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Statement &Type:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   60
      TabIndex        =   0
      Top             =   345
      Width           =   1455
   End
   Begin VB.Label lblLeftColumnTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Left Col Heading:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   60
      TabIndex        =   3
      Top             =   1185
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6030
      Left            =   -30
      Top             =   -390
      Width           =   1740
   End
   Begin VB.Menu mnuExhibits 
      Caption         =   "Exhibits"
      Visible         =   0   'False
      Begin VB.Menu mnuExhibitsDelete1 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuExhibitsDeleteAll 
         Caption         =   "Delete &All"
      End
   End
End
Attribute VB_Name = "frmSeparateStatement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_bCancelled As Boolean
Private m_oPleadingSepStatement As MPO.CPleadingSeparateStatement
''''Private m_oPSS As MPO.CPleadingSeparateStatement

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Public Property Let PleadingSeparateStatement(oNew As MPO.CPleadingSeparateStatement)
    Set m_oPleadingSepStatement = oNew
End Property
Public Property Get PleadingSeparateStatement() As MPO.CPleadingSeparateStatement
    Set PleadingSeparateStatement = m_oPleadingSepStatement
End Property

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
End Sub
Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub
Private Sub btnOK_Click()
    
    On Error GoTo ProcError
    SetUserIni Me.PleadingSeparateStatement.Template.ID, "Type", Me.cmbSeparateStatementTypes.BoundText
    Me.Cancelled = False
    Me.Hide
    
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
End Sub

Private Sub btnOK_GotFocus()
    OnControlGotFocus Me.btnOK
End Sub

Private Sub chkIncludeTableBorders_Click()
    Me.PleadingSeparateStatement.IncludeTableBorders = (Me.chkIncludeTableBorders = vbChecked)
    Application.ScreenRefresh
End Sub

Private Sub chkIncludeTableBorders_GotFocus()
    OnControlGotFocus Me.chkIncludeTableBorders
End Sub

Private Sub chkIncludeTitle_Click()
    On Error GoTo ProcError
    Me.PleadingSeparateStatement.IncludeTitle = (Me.chkIncludeTitle = vbChecked)
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeTitle, "zzmpSeparateStatementTitle"
End Sub

Private Sub cmbSeparateStatementTypes_GotFocus()
    OnControlGotFocus cmbSeparateStatementTypes
End Sub

Private Sub cmbSeparateStatementTypes_ItemChange()
    Dim lID As Long
    Dim bForce As Boolean
    
    On Error GoTo ProcError
    
    With cmbSeparateStatementTypes
        lID = cmbSeparateStatementTypes.BoundText
        If lID <> 0 Then
            EchoOff
            '---Set Type
            Dim oPST As CPleadingSeparateStatement
            Set m_oPleadingSepStatement = g_oMPO.NewPleadingSeparateStatement(lID)
            '---9.4.1 - 3888
            bForce = g_oMPO.ForceItemUpdate
            g_oMPO.ForceItemUpdate = True
            
            Me.txtLeftColumnTitle = Me.PleadingSeparateStatement.Definition.LeftColumnHeading
            txtLeftColumnTitle_LostFocus
            Me.txtRightColumnTitle = Me.PleadingSeparateStatement.Definition.RightColumnHeading
            txtRightColumnTitle_LostFocus
            
            With Me.PleadingSeparateStatement
                .IncludeTableBorders = (chkIncludeTableBorders = vbChecked)
                .IncludeTitle = (chkIncludeTitle = vbChecked)
            End With
            g_oMPO.ForceItemUpdate = bForce
            EchoOn
            Application.ScreenRefresh
        End If
    End With
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbSeparateStatementTypes_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbSeparateStatementTypes, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbSeparateStatementTypes_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbSeparateStatementTypes)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Form_Activate()
    
    On Error GoTo ProcError
    If Me.Initializing Then
        If Me.PleadingSeparateStatement.Document.IsCreated Then
            UpdateForm
        Else
        '---preload sticky setting
            cmbSeparateStatementTypes.BoundText = _
                GetUserIni(Me.PleadingSeparateStatement.Template.ID, "Type")
            If Len(cmbSeparateStatementTypes.BoundText) = 0 Then _
                Me.cmbSeparateStatementTypes.Bookmark = 0
            chkIncludeTitle_Click
        End If

        EchoOn
        Application.ScreenUpdating = True
        DoEvents
        Me.Initializing = False
        Screen.MousePointer = vbDefault

'       move dialog to last position
        Me.cmbSeparateStatementTypes.SetFocus
        'MoveToLastPosition Me
        g_oMPO.ForceItemUpdate = False
    End If
    Exit Sub
ProcError:
    EchoOn
    g_oMPO.ScreenUpdating = True
    g_oMPO.ForceItemUpdate = False
    Word.Application.ScreenUpdating = True
    
    '---9.7.1 - 3961
    g_oError.Show Err, "Error Activating Separate Statement Form"
    '---end
    Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    Exit Sub
End Sub
Private Sub Form_Load()
    On Error GoTo ProcError
    Application.ScreenUpdating = False
    Me.Initializing = True
    g_oMPO.ForceItemUpdate = True
    Me.Top = 20000
    
    Me.Cancelled = True

    Me.cmbSeparateStatementTypes.Array = g_oMPO.db.PleadingSeparateStatementDefs.ListSource
    
    ResizeTDBCombo Me.cmbSeparateStatementTypes, 4
    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Error Loading Separate Statement Form"
    g_oMPO.ForceItemUpdate = False
    Application.ScreenUpdating = True
    Exit Sub
End Sub
Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
End Sub
Private Sub UpdateForm()
    On Error Resume Next
    With Me.PleadingSeparateStatement
        cmbSeparateStatementTypes.BoundText = .TypeID
        If Len(cmbSeparateStatementTypes.BoundText) = 0 Then _
            cmbSeparateStatementTypes.Bookmark = 0
        chkIncludeTableBorders = CInt(.IncludeTableBorders)
        chkIncludeTitle = CInt(.IncludeTitle)
        DoEvents
        txtLeftColumnTitle = .LeftColumnHeading
        txtRightColumnTitle = .LeftColumnHeading
    End With
End Sub

Private Sub txtLeftColumnTitle_LostFocus()
    Me.PleadingSeparateStatement.LeftColumnHeading = txtLeftColumnTitle
    Application.ScreenRefresh
End Sub

Private Sub txtRightColumnTitle_GotFocus()
    OnControlGotFocus Me.txtRightColumnTitle, "zzmpRightColumnHeading"
End Sub

Private Sub txtLeftColumnTitle_GotFocus()
    OnControlGotFocus Me.txtLeftColumnTitle, "zzmpLeftColumnHeading"
End Sub

Private Sub txtRightColumnTitle_LostFocus()
    Me.PleadingSeparateStatement.RightColumnHeading = txtRightColumnTitle
    Application.ScreenRefresh
End Sub
