VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmDraftStamp 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert/Remove Draft/Watermark Stamp"
   ClientHeight    =   3540
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   4272
   Icon            =   "frmDraftStamp.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3540
   ScaleWidth      =   4272
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtVariableText 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   690
      Left            =   135
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Top             =   2220
      Width           =   3960
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3060
      TabIndex        =   5
      Top             =   3045
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   1980
      TabIndex        =   4
      Top             =   3045
      Width           =   1000
   End
   Begin TrueDBList60.TDBList lstType 
      Height          =   1530
      Left            =   135
      OleObjectBlob   =   "frmDraftStamp.frx":058A
      TabIndex        =   1
      Top             =   330
      Width           =   3960
   End
   Begin VB.Label lblVariableText 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Variable Text:"
      Height          =   210
      Left            =   180
      TabIndex        =   2
      Top             =   1995
      Width           =   1005
   End
   Begin VB.Label lblType 
      BackStyle       =   0  'Transparent
      Caption         =   "&Type:"
      Height          =   240
      Left            =   165
      TabIndex        =   0
      Top             =   105
      Width           =   465
   End
   Begin VB.Menu mnuAuthorOptions 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "&Set As Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Person..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
End
Attribute VB_Name = "frmDraftStamp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private WithEvents m_oDocID As MPO.CDocumentStamp
Attribute m_oDocID.VB_VarHelpID = -1
Private m_bCancelled As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Private Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Public Property Set DocumentStamp(oNew As MPO.CDocumentStamp)
    Set m_oDocID = oNew
End Property

Public Property Get DocumentStamp() As MPO.CDocumentStamp
    Set DocumentStamp = m_oDocID
End Property
'**********************************************************
'   Internal Procedures
'**********************************************************

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    m_bCancelled = True
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub btnOK_Click()
    On Error GoTo ProcError
    Me.Hide
'---set stick field for type list
    SetUserIni "Draft Stamp", "Type ID", Me.lstType.BoundText
    DoEvents
    m_bCancelled = False
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub Form_Activate()
    Dim lType As Long
    
    On Error GoTo ProcError
    On Error Resume Next
'    lType = Val(GetUserIni("Draft Stamp", "Type ID"))
'    Me.lstType.BoundText = lType

'   if no default stamp type is selected, attempt to
'   select the "footer" or "end of document" stamp type - if still no selection,
'   select the first item in the list - this is a temporary
'   patch necessitated by the addition of Letterhead-specific
'   default trailers - we need to add DefaultTrailer as a
'   property of mpDB.LetterheadDef
'    If Me.lstType.BoundText = Empty Then
        Me.lstType.SelectedItem = 0
'    End If
    On Error GoTo 0
    Initializing = False
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Activating MacPac90.frmDraftStamp"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim xTemplate As String
    Dim xVarName As String
    Dim lValue As Long
    Dim oDoc As MPO.CDocument
    Dim oTemplate As MPO.CTemplate
    
    On Error GoTo ProcError
    Initializing = True
        
'   get template
    On Error Resume Next
    Set oTemplate = g_oMPO.Templates(Word.ActiveDocument.AttachedTemplate)
    If oTemplate Is Nothing Then _
        Set oTemplate = g_oMPO.Templates(NormalTemplate.Name)
        
    g_oMPO.db.TemplateDefinitions.Refresh

'   get letterhead type - stored in doc var
    xVarName = g_oMPO.db.TemplateDefinitions(oTemplate.ID).ShortName & "LH_1_LetterheadType"

    Set oDoc = New MPO.CDocument
    
    On Error Resume Next
'   get letterhead type - value will be 0
'   if no letterhead exists in this document
    lValue = CLng(oDoc.GetVar(xVarName))
    On Error GoTo 0
    
'   get available doc stamps
    lstType.Array = g_oMPO.db.DocumentStampDefs( _
        2, oTemplate.ID, lValue).ListSource

    
    'MoveToLastPosition Me
    m_bCancelled = True
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Loading MacPac90.frmDraftStamp"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    
    'SetLastPosition Me
    
    Set m_oDocID = Nothing
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub SetUpDialog()
    Dim bEnabled As Boolean
    
    On Error GoTo ProcError
    With Me.DocumentStamp
        .TypeID = Me.lstType.BoundText
        bEnabled = .Definition.AllowVariableText
        Me.txtVariableText.Enabled = bEnabled
        Me.lblVariableText.Enabled = bEnabled
        If Not bEnabled Then
            Me.txtVariableText.Text = ""
        Else
            Me.txtVariableText.Text = xSubstitute( _
                .Definition.PrimaryTextTemplate, "|", "")
        End If
    End With
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstType_DblClick()
    On Error GoTo ProcError
    btnOK_Click
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstType_RowChange()
    On Error GoTo ProcError
    SetUpDialog
    Exit Sub
    
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtVariableText_GotFocus()
    On Error GoTo ProcError
    With Me.txtVariableText
        .SelStart = Len(Me.txtVariableText.Text)
        .SelLength = 0
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
