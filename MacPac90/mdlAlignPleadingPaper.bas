Attribute VB_Name = "mdlAlignPleadingPaper"
Function GetLinedPaperTextFrame(oShape As Word.Shape) As Word.TextFrame
    Dim bIsCorrectTextFrame As Boolean
    Dim oTextFrame As Word.TextFrame
    
    If HasTextFrameText(oShape) Then
        Set oTextFrame = oShape.TextFrame
        'GLOG 4867: Avoid error if textbox is empty
        If oTextFrame.TextRange.Characters.Count > 1 Then
            'make sure this is the correct TextFrame
            If oTextFrame.TextRange.Characters(1).Text = "1" Or oTextFrame.TextRange.Characters(2).Text = "1" Then
                If oTextFrame.TextRange.ParagraphFormat.LineSpacing = 24 Then
                    If HasLineNumbers(oTextFrame) Then
                        bIsCorrectTextFrame = True
                    End If
                End If
            End If
        End If
    End If
    
    If Not bIsCorrectTextFrame Then
        Set oTextFrame = Nothing
    End If
    
    Set GetLinedPaperTextFrame = oTextFrame
    
End Function

Public Function Floor(value As Double)

    Floor = CDbl(CLng(value - 0.5))

End Function

Public Function GetTextFrame(oShape As Word.Shape) As Word.TextFrame
'returns the TextFrame of the specified shape
    Const mpThisFunction As String = "MacPac90.Application.GetTextFrame"
    Dim oFrame As Word.TextFrame
    
    If oShape.Type = 17 Then 'msoTextBox Then
        On Error Resume Next
        Set oFrame = oShape.TextFrame
        On Error GoTo ProcError
        
        Set GetTextFrame = oFrame
    End If
    Exit Function
    
ProcError:
    Exit Function
End Function

Public Function HasTextFrameText(oShape As Word.Shape) As Boolean
    'GLOG 7009
    Const mpThisFunction As String = "MacPac90.Application.HasTextFrameText"
    'On Error GoTo ProcError
    Dim oTextFrame As Word.TextFrame
    Set oTextFrame = GetTextFrame(oShape)
    If Not oTextFrame Is Nothing Then
        HasTextFrameText = oTextFrame.HasText
    End If
    Exit Function
ProcError:
    'Return false if shape causes an error
    HasTextFrameText = False
End Function

Public Function GetTextFrameText(oShape As Word.Shape) As String
    'GLOG 7009
    Const mpThisFunction As String = "MacPac90.Application.GetTextFrameText"
    On Error GoTo ProcError
    Dim oTextFrame As Word.TextFrame
    Set oTextFrame = GetTextFrame(oShape)
    If Not oTextFrame Is Nothing Then
        If oTextFrame.HasText Then
            GetTextFrameText = oTextFrame.TextRange.Text
        End If
    End If
    Exit Function
ProcError:
    g_oError.Show Err, mpThisFunction
End Function

Public Function HasLineNumbers(oTextFrame As Word.TextFrame) As Boolean
'returns true if text box contains line numbers from 1 to 23
    Dim i As Integer
    Dim vChars As Variant
    Const mpThisFunction As String = "MacPac90.Application.HasLineNumbers"
    
    On Error GoTo ProcError
    
    vChars = Split(oTextFrame.TextRange.Text, Chr(11))
    
    If IsArray(vChars) Then
        If UBound(vChars, 1) < 22 Then
            HasLineNumbers = False
            Exit Function
        End If
    Else
        HasLineNumbers = False
        Exit Function
    End If
    
    For i = 0 To 22
        If vChars(i) = i + 1 Then
            HasLineNumbers = True
        Else
            HasLineNumbers = False
            Exit For
        End If
    Next
Exit Function
ProcError:
    g_oError.Show Err, mpThisFunction
    Exit Function
End Function

