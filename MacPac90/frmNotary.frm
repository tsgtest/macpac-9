VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmNotary 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "###"
   ClientHeight    =   7344
   ClientLeft      =   1932
   ClientTop       =   636
   ClientWidth     =   5568
   Icon            =   "frmNotary.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7344
   ScaleWidth      =   5568
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSignerTitle 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   31
      Top             =   6795
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtCompanyState 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   29
      Top             =   6465
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtCompanyName 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   27
      Top             =   6165
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtDocumentDate 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   23
      Top             =   5535
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtOtherSigners 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   25
      Top             =   5865
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtNumberOfPages 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   21
      Top             =   5190
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.CheckBox chkIncludeCountyHeading 
      Appearance      =   0  'Flat
      Caption         =   "Include County/State &Heading"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1965
      TabIndex        =   4
      Top             =   1107
      Value           =   1  'Checked
      Width           =   2475
   End
   Begin VB.TextBox txtNotaryName 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   12
      Top             =   2790
      Width           =   3615
   End
   Begin VB.TextBox txtPersonsAppearing 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   2430
      Width           =   3615
   End
   Begin VB.TextBox txtDocumentTitle 
      Appearance      =   0  'Flat
      Height          =   510
      Left            =   1890
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   19
      Top             =   4665
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtCustom1 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   33
      Top             =   3975
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.TextBox txtCustom2 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1890
      ScrollBars      =   2  'Vertical
      TabIndex        =   35
      Top             =   4335
      Visible         =   0   'False
      Width           =   3615
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   37
      Top             =   6900
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4065
      TabIndex        =   36
      Top             =   6900
      Width           =   650
   End
   Begin TrueDBList60.TDBCombo cmbExecuteDate 
      Height          =   375
      Left            =   1890
      OleObjectBlob   =   "frmNotary.frx":058A
      TabIndex        =   8
      Top             =   2019
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbGender 
      Height          =   375
      Left            =   1890
      OleObjectBlob   =   "frmNotary.frx":27AC
      TabIndex        =   14
      Top             =   3180
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbAppearanceDate 
      Height          =   375
      Left            =   1890
      OleObjectBlob   =   "frmNotary.frx":49C9
      TabIndex        =   6
      Top             =   1518
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbPersonalProved 
      Height          =   375
      Left            =   1890
      OleObjectBlob   =   "frmNotary.frx":6BEE
      TabIndex        =   16
      Top             =   3495
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbState 
      Height          =   345
      Left            =   1890
      OleObjectBlob   =   "frmNotary.frx":8E13
      TabIndex        =   1
      Top             =   165
      Width           =   3615
   End
   Begin TrueDBList60.TDBCombo cmbCounty 
      Height          =   345
      Left            =   1890
      OleObjectBlob   =   "frmNotary.frx":ADEB
      TabIndex        =   3
      Top             =   636
      Width           =   3615
   End
   Begin VB.CheckBox chkIncludeCommission 
      Appearance      =   0  'Flat
      Caption         =   "Include 'My Commission &Expires'"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1965
      TabIndex        =   17
      Top             =   3765
      Width           =   2745
   End
   Begin VB.Label lblSignerTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Si&gnerTitle:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   30
      Top             =   6825
      Width           =   1455
   End
   Begin VB.Label lblCompanyState 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Compan&y State:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   28
      Top             =   6495
      Width           =   1455
   End
   Begin VB.Label lblCompanyName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "C&ompany Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   26
      Top             =   6195
      Width           =   1455
   End
   Begin VB.Label lblDocumentDate 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Docu&ment Date:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   22
      Top             =   5565
      Width           =   1455
   End
   Begin VB.Label lblOtherSigners 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Other Signers:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   24
      Top             =   5895
      Width           =   1455
   End
   Begin VB.Label lblNumberOfPages 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Num&ber Of Pages:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   20
      Top             =   5220
      Width           =   1455
   End
   Begin VB.Label lblState 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&State:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   255
      TabIndex        =   0
      Top             =   225
      Width           =   1455
   End
   Begin VB.Label lblPersonalProved 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Known By:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   270
      TabIndex        =   15
      Top             =   3495
      Width           =   1455
   End
   Begin VB.Label lblNotaryName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Notary Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   255
      TabIndex        =   11
      Top             =   2760
      Width           =   1455
   End
   Begin VB.Label lblPersonsAppearing 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Person(s) Appearing:"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   90
      TabIndex        =   9
      Top             =   2295
      Width           =   1620
   End
   Begin VB.Label lblAppearanceDate 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Dated:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   255
      TabIndex        =   5
      Top             =   1410
      Width           =   1455
   End
   Begin VB.Label lblCounty 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&County:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   255
      TabIndex        =   2
      Top             =   660
      Width           =   1455
   End
   Begin VB.Label lblGender 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Gender:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   270
      TabIndex        =   13
      Top             =   3210
      Width           =   1455
   End
   Begin VB.Label lblDocumentTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Document &Title(s):"
      ForeColor       =   &H00FFFFFF&
      Height          =   270
      Left            =   270
      TabIndex        =   18
      Top             =   4650
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblExecuteDate 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Date E&xecuted:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   255
      TabIndex        =   7
      Top             =   1890
      Width           =   1455
   End
   Begin VB.Label lblCustom1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Custom #1:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   285
      TabIndex        =   32
      Top             =   4080
      Width           =   1455
   End
   Begin VB.Label lblCustom2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Custom #2:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   270
      TabIndex        =   34
      Top             =   4380
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00C0C0C0&
      FillColor       =   &H00800080&
      Height          =   9495
      Left            =   -15
      Top             =   -240
      Width           =   1800
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmNotary"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oNotary As MPO.CNotary
Private m_bCancelled As Boolean
Private m_bInit As Boolean

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Set Notary(oNew As MPO.CNotary)
    Set m_oNotary = oNew
End Property
 
Public Property Get Notary() As MPO.CNotary
    Set Notary = m_oNotary
End Property

Public Sub PositionControls()
'adjust dialog for manual entry / no manual entry
    Const iSpaceBefore As Integer = 165
    Dim oCtl As Control
    Dim oLastVisible As Control
    Dim arrCtls() As String
    Dim i As Integer
    
    On Error GoTo ProcError

    ReDim arrCtls(Me.Controls.Count)
    
'   place each control in tab order sort
    For Each oCtl In Me.Controls
        On Error Resume Next
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next

'   position controls based on visibility and tab index -
'   skip author and signer name controls - they'll always
'   appear at the top of the dlg at the same y position
    For i = 1 To 35
'       get control
        Set oCtl = Me.Controls(arrCtls(i))
'       do only if visible
        If oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is VB.CheckBox) Then
                If oLastVisible Is Nothing Then
'                   position as 1st control
                    oCtl.Top = 200
                Else
'                   position control relative to previous control
                    oCtl.Top = oLastVisible.Top + _
                               oLastVisible.Height + _
                               iSpaceBefore
                End If
                
'               set accompanying label vertical position
                If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                    Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                End If
                
'               set control as last visible control
                Set oLastVisible = oCtl
            End If
        End If
    Next i
    
    Me.btnFinish.Top = oLastVisible.Top + _
        oLastVisible.Height + 210
        
    Me.btnCancel.Top = Me.btnFinish.Top
    
    '*Word15
    Me.Height = Me.btnFinish.Top + Me.btnFinish.Height + IIf(g_bIsWord15x, 550 + g_sWord15HeightAdjustment, 550)
    
    Word.Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmNotary.PositionControls"
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    m_bCancelled = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Me.Hide
    g_oMPO.ForceItemUpdate = False
    DoEvents
    m_bCancelled = False
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub chkIncludeCountyHeading_GotFocus()
    OnControlGotFocus Me.chkIncludeCountyHeading, "zzmpCountyHeading"
End Sub

Private Sub chkIncludeCountyHeading_LostFocus()
    On Error GoTo ProcError
    Me.Notary.IncludeCountyHeading = Abs(Me.chkIncludeCountyHeading)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeCommission_GotFocus()
    OnControlGotFocus Me.chkIncludeCommission, "zzmpCommission"
End Sub

Private Sub chkIncludeCommission_Click()
    On Error GoTo ProcError
    Me.Notary.IncludeCommission = Abs(Me.chkIncludeCommission)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_LostFocus()
    On Error GoTo ProcError
    With Me.Notary
        Word.Application.ScreenUpdating = False
        EchoOff
        .Gender = cmbGender.BoundText
        EchoOn
        Word.Application.ScreenUpdating = True
    End With
    Exit Sub
ProcError:
        EchoOn
        Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbPersonalProved_GotFocus()
    OnControlGotFocus Me.cmbPersonalProved, "zzmpPersonalProved"
    OnControlGotFocus Me.cmbPersonalProved, "zzmpFixedCheckPersonalProved1"
End Sub

Private Sub cmbPersonalProved_LostFocus()
    On Error GoTo ProcError
    Me.Notary.PersonalProved = Me.cmbPersonalProved.BoundText
    Word.Application.ScreenUpdating = True
    Word.Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbState_GotFocus()
    OnControlGotFocus Me.cmbState, "zzmpState"
End Sub

Private Sub cmbState_LostFocus()
    On Error GoTo ProcError
    Me.Notary.State = Me.cmbState.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    Dim xDocTitle As String
    Dim xOrdSuffix As String
    Dim xDefDate As String
    
    On Error GoTo ProcError
    If Me.Initializing Then
        With Me.Notary
            LockWindowUpdate (Me.hwnd)
            If .Document.IsCreated And _
                .Document.Template = .Template.FileName Then
                UpdateForm
            Else
                'GLOG : 5052 : CEH
'               set starting selections
                GetExistingValues

'               set starting selections
                On Error Resume Next
    
'               default to including county heading and commission
                .IncludeCountyHeading = Abs(Me.chkIncludeCountyHeading)
                .IncludeCommission = Abs(Me.chkIncludeCommission)
                
                With .Definition
                '   set Notary state
                    Me.Notary.State = g_oMPO.Lists("PleadingCourtsLevel0") _
                        .ListItems.Item(, .Level0).DisplayText
    
                    If .AllowGender Then
                        Me.cmbGender.BoundText = mpGenderIndefinite
                    End If
                    
                    If .AllowCounty Then
                        Me.cmbCounty.BoundText = _
                            g_oMPO.Offices.Default.CountyID
                            
                        If Me.cmbCounty.BoundText = Empty Then
'                           select first in the list
                            Me.cmbCounty.SelectedItem = 0
                        End If
                    End If
                    
                    If .AllowState Then
                        Me.cmbState.SelectedItem = 0
                    End If
                    
                    If .AllowPersonalProved Then
                        Me.cmbPersonalProved.SelectedItem = 0
                    End If
                    
                    If .AllowAppearanceDate Then
                        Me.cmbAppearanceDate.BoundText = _
                            .DefaultAppearanceDateFormat
                    End If
                    
                    If .AllowExecuteDate Then
                        Me.cmbExecuteDate.BoundText = _
                            .DefaultExecuteDateFormat
                    End If
                    
'                   set custom text
                    g_oMPO.ForceItemUpdate = True
                    Notary.Custom1 = .Custom1DefaultText
                    Me.txtCustom1 = .Custom1DefaultText
                    Notary.Custom2 = .Custom2DefaultText
                    Me.txtCustom2 = .Custom2DefaultText
                End With
                
'               set props based on selected items
                If Me.cmbGender.BoundText <> Empty Then
                    .Gender = Me.cmbGender.BoundText
                End If
                If Me.cmbCounty.BoundText <> Empty Then
                    .County = Me.cmbCounty.BoundText
                End If
                If Me.cmbPersonalProved.BoundText <> Empty Then
                    .PersonalProved = Me.cmbPersonalProved.BoundText
                End If
                
'               could not get .text property below to return
'               the correct values without a preceding DoEvents
                DoEvents
                .AppearanceDate = Me.cmbAppearanceDate.Text
                .ExecuteDate = Me.cmbExecuteDate.Text
            End If
            
'           force reposition of controls
            PositionControls
        End With
        
'       update any fields
        Me.Notary.Document.UpdateFields
        
'****CHANGED**************************************************
'       Bug workaround - If new Notary is being inserted into document
'       that already had a notary inserted, bookmarks will not be populated
'       for controls set to the same text as the previous Notary
        g_oMPO.ForceItemUpdate = True
'******************************************************************

'       move dialog to last position
        'MoveToLastPosition Me
        LockWindowUpdate (0&)
        
        DoEvents
        Screen.MousePointer = vbDefault
        
        EchoOn
        Word.Application.ScreenUpdating = True
        Word.Application.ScreenRefresh
    End If
    
    m_bInit = False
    
    Exit Sub
ProcError:
    g_oMPO.ForceItemUpdate = False
    Word.Application.ScreenUpdating = True
    EchoOn
    Screen.MousePointer = vbDefault
    m_bInit = False
    g_oError.Show Err, "Error Activating Notary Form"
    Exit Sub
End Sub

Private Sub cmbCounty_GotFocus()
    OnControlGotFocus Me.cmbCounty, "zzmpCounty"
End Sub

Private Sub cmbCounty_LostFocus()
    Dim oRng As Word.Range
    On Error GoTo ProcError
    Me.Notary.County = IIf((UCase(Me.cmbCounty.Text) = "(BLANK)"), "", Me.cmbCounty.Text)   '9.7.1 #4272
    
'   9.9.1010 #5305 !gm
    If Me.Notary.State = "Colorado" Then
        Set oRng = ActiveDocument.Bookmarks("zzmpCountyHeading").Range
        If Me.Notary.County = "Denver" _
            And InStr(LCase(ActiveDocument.Bookmarks("zzmpCounty").Range.Paragraphs.First), "city and") = 0 Then
            With oRng.Find
                .Text = "county"
                .Replacement.Text = "city and county"
            End With
            oRng.Find.Execute Replace:=wdReplaceAll
        ElseIf Me.Notary.County <> "Denver" _
            And InStr(LCase(ActiveDocument.Bookmarks("zzmpCounty").Range.Paragraphs.First), "city and") <> 0 Then
            With oRng.Find
                .Text = "city and county"
                .Replacement.Text = "county"
            End With
            oRng.Find.Execute Replace:=wdReplaceAll
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbPersonalProved_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbPersonalProved)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbPersonalProved_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbPersonalProved, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbExecuteDate_GotFocus()
    OnControlGotFocus Me.cmbExecuteDate, "zzmpExecuteDate"
End Sub

Private Sub cmbExecuteDate_LostFocus()
    On Error GoTo ProcError
    Me.Notary.ExecuteDate = Me.cmbExecuteDate.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAppearanceDate_GotFocus()
    OnControlGotFocus Me.cmbAppearanceDate, "zzmpAppearanceDate"
End Sub

Private Sub cmbAppearanceDate_LostFocus()
    On Error GoTo ProcError
    Me.Notary.AppearanceDate = Me.cmbAppearanceDate.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_GotFocus()
    OnControlGotFocus Me.cmbGender
End Sub

Private Sub cmbGender_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbGender)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbGender_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbGender, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    OnFormKeyDown KeyCode
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oGenders As XArray
    Dim oList As CList
    Dim oDateFormats As CList
    Dim oDef As CNotaryDef
    Dim i As Integer
    Dim oArray As XArray
    Dim xOrdSuffix As String
    
    On Error GoTo ProcError
    Word.Application.StatusBar = "Initializing.  Please wait..."
    Application.ScreenUpdating = False

    m_bInit = True

    Me.Cancelled = True
    
'   move dialog way down - form activate
'   resizes the dialog - to prevent the user
'   from seeing weirdness, resize off the screen
'   then move into position
    Me.Top = 20000

'   ensure date update
    g_oMPO.ForceItemUpdate = True
    
    Set oDef = Me.Notary.Definition
    
'   set up date controls
    Set oDateFormats = g_oMPO.Lists("DateFormatsVariable")
    With oDateFormats
'       the date formats value of the definition
'       is the id of the group of date formats
        .FilterValue = oDef.DateFormats
        .Refresh
    End With
    
    With oDef
'       show/hide date as appropriate
        Me.cmbExecuteDate.Visible = .AllowExecuteDate
        Me.lblExecuteDate.Visible = .AllowExecuteDate
        Me.cmbAppearanceDate.Visible = .AllowAppearanceDate
        Me.lblAppearanceDate.Visible = .AllowAppearanceDate
'       show/hide gender control
        Me.cmbGender.Visible = .AllowGender
        Me.lblGender.Visible = .AllowGender
'       show/hide county control
        Me.cmbCounty.Visible = .AllowCounty
        Me.lblCounty.Visible = .AllowCounty
'       show/hide state control
        Me.cmbState.Visible = .AllowState
        Me.lblState.Visible = .AllowState
'       show/hide persons appearing control
        Me.txtPersonsAppearing.Visible = .AllowAppearingPerson
        Me.lblPersonsAppearing.Visible = .AllowAppearingPerson
'       show/hide Notary Name control
        Me.txtNotaryName.Visible = .AllowNotaryName
        Me.lblNotaryName.Visible = .AllowNotaryName
'       show/hide Document Title control
        Me.txtDocumentTitle.Visible = .AllowDocumentTitle
        Me.lblDocumentTitle.Visible = .AllowDocumentTitle
'       show/hide Document Date control
        Me.txtDocumentDate.Visible = .AllowDocumentDate
        Me.lblDocumentDate.Visible = .AllowDocumentDate
'       show/hide Number Of Pages control
        Me.txtNumberOfPages.Visible = .AllowNumberOfPages
        Me.lblNumberOfPages.Visible = .AllowNumberOfPages
'       show/hide Number Of Pages control
        Me.txtOtherSigners.Visible = .AllowOtherSigners
        Me.lblOtherSigners.Visible = .AllowOtherSigners
'       show/hide Known By control
        Me.cmbPersonalProved.Visible = .AllowPersonalProved
        Me.lblPersonalProved.Visible = .AllowPersonalProved
'       show/hide Company Name control
        Me.txtCompanyName.Visible = .AllowCompanyName
        Me.lblCompanyName.Visible = .AllowCompanyName
'       show/hide Company State control
        Me.txtCompanyState.Visible = .AllowCompanyState
        Me.lblCompanyState.Visible = .AllowCompanyState
'       show/hide Signer Title control
        Me.txtSignerTitle.Visible = .AllowSignerTitle
        Me.lblSignerTitle.Visible = .AllowSignerTitle
        
'       show/hide checkboxes
        Me.chkIncludeCountyHeading.Visible = .AllowCountyHeading
        Me.chkIncludeCommission.Visible = .AllowCommission

'       get date list
        Set oArray = oDateFormats.ListItems.Source
        
'       get ordinal suffix based on current date - eg "rd", "th"
        xOrdSuffix = GetOrdinalSuffix(Day(Date))
        
'       replace all instances of ordinal variable
'       with ordinal suffix-this is done here because
'       there is no way in vb to format a dateas an ordinal
        For i = oArray.LowerBound(1) To oArray.UpperBound(1)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%o%", xOrdSuffix)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%or%", xOrdSuffix)      '9.7.1 #4402
        Next i
        
'       set up date controls
        If .AllowExecuteDate Then
            Me.cmbExecuteDate.Array = oArray
            Me.cmbExecuteDate.Rebind
            ResizeTDBCombo Me.cmbExecuteDate, 4
        End If
        
        If .AllowAppearanceDate Then
            Me.cmbAppearanceDate.Array = oArray
            Me.cmbAppearanceDate.Rebind
            ResizeTDBCombo Me.cmbAppearanceDate, 4
        End If
        
        If .Custom1Label <> "" Then
            Me.lblCustom1 = .Custom1Label
            Me.txtCustom1.Visible = True
            Me.lblCustom1.Visible = True
        Else
            Me.lblCustom1.Visible = False
            Me.txtCustom1.Visible = False
        End If
        
        If .Custom2Label <> "" Then
            Me.lblCustom2 = .Custom2Label
            Me.lblCustom2.Visible = True
            Me.txtCustom2.Visible = True
        Else
            Me.lblCustom2.Visible = False
            Me.txtCustom2.Visible = False
        End If
    End With
    
'   set up gender control if allowed
    If oDef.AllowGender Then
        Set oGenders = xarStringToxArray( _
            "Indefinite (he/she/they)|0|Female " & _
            "(she)|1|Male (he)|2|Plural (they)|3|Unknown (____)|4", 2)
        Me.cmbGender.Array = oGenders
        Me.cmbGender.Rebind
        ResizeTDBCombo Me.cmbGender, 5
    End If
    
'   set up county control if allowed
    If oDef.AllowCounty Then
        Set oList = g_oMPO.Lists("Counties")
        With oList
            .FilterValue = oDef.Level0
            .Refresh
            'add (blank) record - 9.7.1 #4272
            With .ListItems
                .Source.Insert 1, 0
                .Source(0, 0) = "(Blank)"
                .Source(0, 1) = -9999
                Me.cmbCounty.Array = .Source
            End With
        End With
        Me.cmbCounty.Rebind
        ResizeTDBCombo Me.cmbCounty, 6
    End If

'   set up state control if allowed
    If oDef.AllowState Then
        Me.cmbState.Array = _
            xarStringToxArray(g_oMPO.Lists("PleadingCourtsLevel0") _
                        .ListItems.Item(, oDef.Level0).DisplayText)
        Me.cmbState.Rebind
        ResizeTDBCombo Me.cmbState, 4
    End If
    
'   set up "known by" control
    If oDef.AllowPersonalProved Then
        Me.cmbPersonalProved.Array = xarStringToxArray(oDef.KnownByList, 2)
        Me.cmbPersonalProved.Rebind
        ResizeTDBCombo Me.cmbPersonalProved, 2
    End If
    
    Me.Caption = oDef.DialogCaption
    
    If Not Me.Notary.Document.IsSegment Then
        g_oMPO.ForceItemUpdate = False
    End If
    
    EchoOff
    LockWindowUpdate (Me.hwnd)
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
'   must clear out global var that contains the
'   the last active control - otherwise form
'   will get reloaded
    Set g_oPrevControl = Nothing
    'SetLastPosition Me
End Sub

Private Sub txtCustom1_GotFocus()
    OnControlGotFocus Me.txtCustom1, "zzmpNotaryCustom1"
End Sub

Private Sub txtCustom1_LostFocus()
    On Error GoTo ProcError
    Me.Notary.Custom1 = Me.txtCustom1
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCustom2_GotFocus()
    OnControlGotFocus Me.txtCustom2, "zzmpNotaryCustom2"
End Sub
Private Sub txtCustom2_LostFocus()
    On Error GoTo ProcError
    Me.Notary.Custom2 = Me.txtCustom2
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDocumentTitle_GotFocus()
    OnControlGotFocus Me.txtDocumentTitle, "zzmpDocumentTitle"
End Sub

Private Sub txtDocumentTitle_LostFocus()
    On Error GoTo ProcError
    Me.Notary.DocumentTitle = Me.txtDocumentTitle
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDocumentDate_GotFocus()
    OnControlGotFocus Me.txtDocumentDate, "zzmpDocumentDate"
End Sub

Private Sub txtDocumentDate_LostFocus()
    On Error GoTo ProcError
    Me.Notary.DocumentDate = Me.txtDocumentDate
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtNumberOfPages_GotFocus()
    OnControlGotFocus Me.txtNumberOfPages, "zzmpNumberOfPages"
End Sub

Private Sub txtNumberOfPages_LostFocus()
    On Error GoTo ProcError
    Me.Notary.NumberOfPages = Me.txtNumberOfPages
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtOtherSigners_GotFocus()
    OnControlGotFocus Me.txtOtherSigners, "zzmpOtherSigners"
End Sub

Private Sub txtOtherSigners_LostFocus()
    On Error GoTo ProcError
    Me.Notary.OtherSigners = Me.txtOtherSigners
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub UpdateForm()
    Dim oDef As mpDB.CNotaryDef
    
    On Error GoTo ProcError
    With Me.Notary
        Set oDef = .Definition
        
        If Me.cmbState.Visible Then
'           populate state control on reuse '9.7.1 - #4138
            Me.cmbState.Text = .State
        Else
'           set state as selected in types dialog
            Me.Notary.State = g_oMPO.Lists("PleadingCourtsLevel0") _
                .ListItems.Item(, oDef.Level0).DisplayText
        End If
        
'       set county
        If oDef.AllowCounty Then
            Me.cmbCounty.BoundText = .County
            Me.cmbCounty.Text = .County
            If Me.cmbCounty.BoundText = Empty Then
'               select "(Blank)" county if it exists - 9.7.1 #4272
                Me.cmbCounty.BoundText = "(Blank)"
                If Me.cmbCounty.BoundText = Empty Then
'                   select default county
                    Me.cmbCounty.BoundText = _
                        g_oMPO.Offices.Default.CountyID
                    If Me.cmbCounty.BoundText = Empty Then
    '                   select first in the list
                        Me.cmbCounty.SelectedItem = 0
                    End If
                End If
            End If
        End If
        
        Me.cmbExecuteDate.Text = .ExecuteDate
        Me.cmbAppearanceDate.Text = .AppearanceDate
        Me.txtPersonsAppearing.Text = .PersonsAppearing
        Me.txtCompanyName.Text = .CompanyName
        Me.txtCompanyState.Text = .CompanyState
        Me.txtSignerTitle.Text = .SignerTitle
        Me.txtNotaryName.Text = .NotaryName
        Me.cmbPersonalProved.BoundText = .PersonalProved
        Me.txtDocumentTitle.Text = .DocumentTitle
        Me.txtDocumentDate.Text = .DocumentDate
        Me.txtNumberOfPages = .NumberOfPages
        Me.txtOtherSigners = .OtherSigners
        If Me.cmbPersonalProved.BoundText = Empty Then
            Me.cmbPersonalProved.SelectedItem = 0
            cmbPersonalProved_LostFocus
        End If
        Me.chkIncludeCountyHeading.value = Abs(.IncludeCountyHeading)
        Me.chkIncludeCommission.value = Abs(.IncludeCommission)
        Me.cmbGender.BoundText = .Gender
        If oDef.Custom1Label <> Empty Then _
            Me.txtCustom1 = .Custom1
        If oDef.Custom2Label <> Empty Then _
            Me.txtCustom2 = .Custom2
    End With
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmNotary.UpdateForm"
    Exit Sub
End Sub

Private Sub txtNotaryName_GotFocus()
    OnControlGotFocus Me.txtNotaryName, "zzmpNotaryName"
End Sub

Private Sub txtNotaryName_LostFocus()
    On Error GoTo ProcError
    Me.Notary.NotaryName = Me.txtNotaryName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub txtCompanyName_GotFocus()
    OnControlGotFocus Me.txtCompanyName, "zzmpCompanyName"
End Sub

Private Sub txtCompanyName_LostFocus()
    On Error GoTo ProcError
    Me.Notary.CompanyName = Me.txtCompanyName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCompanyState_GotFocus()
    OnControlGotFocus Me.txtCompanyState, "zzmpCompanyState"
End Sub

Private Sub txtCompanyState_LostFocus()
    On Error GoTo ProcError
    Me.Notary.CompanyState = Me.txtCompanyState
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignerTitle_GotFocus()
    OnControlGotFocus Me.txtSignerTitle, "zzmpSignerTitle"
End Sub

Private Sub txtSignerTitle_LostFocus()
    On Error GoTo ProcError
    Me.Notary.SignerTitle = Me.txtSignerTitle
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPersonsAppearing_GotFocus()
    OnControlGotFocus Me.txtPersonsAppearing, "zzmpPersonsAppearing"
End Sub

Private Sub txtPersonsAppearing_LostFocus()
    On Error GoTo ProcError
    Me.Notary.PersonsAppearing = Me.txtPersonsAppearing
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'GLOG : 5052 : CEH
Private Sub GetExistingValues()
    Dim xDocTitle As String
    Dim oDoc As MPO.CDocument

    On Error GoTo ProcError
    Set oDoc = New MPO.CDocument
    
'   Get pleading document title
    xDocTitle = oDoc.GetVar("PleadingCaption_1_CaseTitle")

    If xDocTitle = "" Then
        xDocTitle = oDoc.GetVar("Pleading_1_DocumentTitle")
    End If

    If xDocTitle <> "" Then
        Me.txtDocumentTitle = xDocTitle
        Me.Notary.DocumentTitle = xDocTitle
    End If

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

