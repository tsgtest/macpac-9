VERSION 5.00
Begin VB.Form frmDocVars 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Manage Document Variables"
   ClientHeight    =   5220
   ClientLeft      =   1725
   ClientTop       =   2280
   ClientWidth     =   6900
   Icon            =   "frmDocVars.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   6900
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox lstVarNames 
      Height          =   2460
      IntegralHeight  =   0   'False
      Left            =   180
      TabIndex        =   7
      Top             =   300
      Width           =   5055
   End
   Begin VB.CommandButton btnDeleteOne 
      Caption         =   "Delete &One"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5580
      TabIndex        =   6
      Top             =   1215
      Width           =   1080
   End
   Begin VB.CommandButton btnReset 
      Caption         =   "&Reset"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5580
      TabIndex        =   3
      Top             =   345
      Width           =   1080
   End
   Begin VB.TextBox txtValue 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1785
      Left            =   150
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   3240
      Width           =   5115
   End
   Begin VB.CommandButton btnDeleteAll 
      Caption         =   "Delete &All"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5580
      TabIndex        =   4
      Top             =   780
      Width           =   1080
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Left            =   5580
      TabIndex        =   5
      Top             =   4635
      Width           =   1080
   End
   Begin VB.Label lblOffices 
      AutoSize        =   -1  'True
      Caption         =   "&Variables:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   195
      TabIndex        =   0
      Top             =   90
      Width           =   705
   End
   Begin VB.Label lblValue 
      AutoSize        =   -1  'True
      Caption         =   "V&alue:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   210
      TabIndex        =   1
      Top             =   3015
      Width           =   450
   End
End
Attribute VB_Name = "frmDocVars"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private DocVars() As String
Private vbControls() As String
Public m_bFinished As Boolean
'Private Sub lstVarNames_LostFocus()
'    VBACtlLostFocus Me.lstVarNames, vbControls()
'End Sub
'Private Sub txtValue_GotFocus()
'    bEnsureSelectedContent Me.txtValue
'End Sub



Private Function bCreateDocVarArray() As Boolean
    Dim oDocVar As Variable
    Dim iCount As Integer
    Dim iVCount As Integer
    
    On Error Resume Next
'---create array
    With ActiveDocument
        iCount = .Variables.Count
        If iCount = 0 Then iCount = 1
        ReDim DocVars(iCount - 1, 1)
        
        For Each oDocVar In .Variables
            With oDocVar
                DocVars(iVCount, 0) = .Name
                DocVars(iVCount, 1) = .Value
                iVCount = iVCount + 1
            End With
        Next oDocVar

    End With

End Function
Private Function DeleteOneDocVar() As Boolean
    ActiveDocument.Variables(Me.lstVarNames.Text).Delete

End Function


Private Function DeleteAllDocVars() As Boolean
    Dim oDocVar As Variable
    Dim iCount As Integer
    Dim iVCount As Integer
    
    On Error Resume Next
'---create array
    With ActiveDocument
        For Each oDocVar In .Variables
            With oDocVar
                .Delete
            End With
        Next oDocVar

    End With

End Function
Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnDeleteAll_Click()
    Dim bRet As Boolean
    DeleteAllDocVars
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()
End Sub

Private Sub btnDeleteOne_Click()
   Dim bRet As Boolean
    DeleteOneDocVar
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()

End Sub

Private Sub btnReset_Click()
    Dim iPlace As Integer
    Dim bRet As Boolean
    iPlace = lstVarNames.ListIndex
    ActiveDocument.Variables(lstVarNames.Text) = txtValue
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()
    'lstVarNames.ListIndex = iPlace

End Sub

Private Sub lstVarNames_Click()
    Me.txtValue = DocVars(Me.lstVarNames.ListIndex, 1)
End Sub

Private Sub Form_Load()

    Dim bRet As Boolean
    Me.m_bFinished = False
      
    bRet = bCreateDocVarArray
    bLSTList lstVarNames, DocVars()

End Sub

Function bLSTList(lstPVBA As VB.ListBox, _
                  xArray() As String, _
                  Optional lstPVB As VB.ListBox) As Boolean
'assigns array xArray() to listbox lstP

    Dim i As Integer
    Dim j As Integer
    Dim bIsOneDimensional As Boolean
    Dim iArrayUBound As Integer

'////////////////////////
'note -- this is for one column list boxes only! (bug fix
'for pleadings & business dboxes

    Dim lstP As Control
    
    If Not lstPVB Is Nothing Then
        Set lstP = lstPVB
    Else
        Set lstP = lstPVBA
    End If
    
'////////////////////////
'   test for number of dimensions in array
    On Error Resume Next
    iArrayUBound = UBound(xArray, 2)
    bIsOneDimensional = (Err.Number > 0)
    Err.Number = 0
    
'   clear existing if necessary
    If lstP.ListCount Then _
        lstP.Clear
        
    If bIsOneDimensional Then
'       add items from one dimensional array
        If Not (UBound(xArray) = 0 And xArray(0) = "") Then
            For i = LBound(xArray) To UBound(xArray)
                lstP.AddItem xArray(i)
                'DoEvents
            Next i
        End If
    Else
'       add items from two dimensional array
        If Not (UBound(xArray) = 0 And xArray(0, 0) = "") Then
            For i = LBound(xArray, 1) To UBound(xArray, 1)
                With lstP
                    .AddItem xArray(i, 0)
                    For j = 1 To iArrayUBound
                        .List(i, j) = xArray(i, j)
                    Next j
                End With
            Next i
        End If
    End If
End Function


