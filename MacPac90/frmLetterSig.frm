VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmLetterSig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Letter Signature"
   ClientHeight    =   6324
   ClientLeft      =   3360
   ClientTop       =   672
   ClientWidth     =   5460
   Icon            =   "frmLetterSig.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6324
   ScaleWidth      =   5460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkElectronicSignature 
      Appearance      =   0  'Flat
      Caption         =   "&Electronic Signature Format"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   1980
      TabIndex        =   8
      Top             =   1800
      Width           =   3210
   End
   Begin VB.TextBox txtAuthorInitials 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1800
      TabIndex        =   12
      Top             =   2655
      Width           =   3600
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3270
      TabIndex        =   19
      Top             =   5820
      Width           =   1000
   End
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   2475
      Picture         =   "frmLetterSig.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   21
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   5820
      Width           =   705
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4350
      TabIndex        =   20
      Top             =   5820
      Width           =   1000
   End
   Begin VB.TextBox txtCC 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   1005
      Left            =   1800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   16
      Top             =   3525
      Width           =   3600
   End
   Begin VB.TextBox txtBCC 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   1005
      Left            =   1800
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   18
      Top             =   4680
      Width           =   3600
   End
   Begin VB.CheckBox chkIncludeFirmName 
      Appearance      =   0  'Flat
      Caption         =   "Incl&ude Firm Name"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   3375
      TabIndex        =   7
      Top             =   1410
      Width           =   1740
   End
   Begin VB.CheckBox chkIncludeAuthorTitle 
      Appearance      =   0  'Flat
      Caption         =   "Include &Title"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   1980
      TabIndex        =   6
      Top             =   1410
      Width           =   1215
   End
   Begin VB.TextBox txtTypistInitials 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1800
      TabIndex        =   10
      Top             =   2220
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbClosingPhrases 
      Height          =   555
      Left            =   1800
      OleObjectBlob   =   "frmLetterSig.frx":0CAC
      TabIndex        =   5
      Top             =   975
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbEnclosures 
      Height          =   555
      Left            =   1800
      OleObjectBlob   =   "frmLetterSig.frx":2EE1
      TabIndex        =   14
      Top             =   3090
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbAuthor 
      Height          =   375
      Left            =   1800
      OleObjectBlob   =   "frmLetterSig.frx":50FE
      TabIndex        =   1
      Top             =   120
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbAuthor2 
      Height          =   375
      Left            =   1800
      OleObjectBlob   =   "frmLetterSig.frx":9033
      TabIndex        =   3
      Top             =   540
      Width           =   3600
   End
   Begin VB.Label lblAuthor2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&2nd Author:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   135
      TabIndex        =   2
      Top             =   570
      Width           =   1455
   End
   Begin VB.Label lblAuthorInitials 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Auth&or Initials:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   270
      TabIndex        =   11
      Top             =   2685
      Width           =   1320
   End
   Begin VB.Label lblCC 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   " &cc:"
      ForeColor       =   &H00FFFFFF&
      Height          =   690
      Left            =   135
      TabIndex        =   15
      Top             =   3495
      Width           =   1455
   End
   Begin VB.Label lblBCC 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   " &bcc:"
      ForeColor       =   &H00FFFFFF&
      Height          =   840
      Left            =   135
      TabIndex        =   17
      Top             =   4680
      Width           =   1455
   End
   Begin VB.Label lblAuthor 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   " &Author:"
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   135
      TabIndex        =   0
      Top             =   150
      Width           =   1455
   End
   Begin VB.Label lblClosingPhrase 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   " Closing &Phrase:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   4
      Top             =   1020
      Width           =   1455
   End
   Begin VB.Label lblEnclosures 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   " E&nclosures:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   13
      Top             =   3135
      Width           =   1455
   End
   Begin VB.Label lblTypistInitials 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   " T&ypist Initials:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   135
      TabIndex        =   9
      Top             =   2250
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000004&
      FillColor       =   &H80000004&
      Height          =   6600
      Left            =   -15
      Top             =   -30
      Width           =   1740
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
   End
   Begin VB.Menu mnuAuthor2 
      Caption         =   "Author2"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor2_Clear 
         Caption         =   "&Clear"
      End
   End
End
Attribute VB_Name = "frmLetterSig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_oSig As MPO.CLetterSig
Private m_PersonsForm As MPO.CPersonsForm
Private m_oOptions As MPO.COptionsForm

Private m_ShowHiddenText As Boolean
Private m_bAuthorDirty As Boolean
Private m_bInitializing As Boolean
Private b_Opened As Boolean
Private xMsg As String
Private m_xDefaultCCLabel As String '9.7.1 #4151
Private m_xDefaultBCCLabel As String '9.7.1 #4151
Private m_bAuthorChanged As Boolean

'---9.7.1 - 4147
Private m_bAuthor2Dirty As Boolean
Private m_bOpened2 As Boolean

Public Property Get Signature() As MPO.CLetterSig
    Set Signature = m_oSig
End Property

Public Property Let Signature(lsgNew As MPO.CLetterSig)
    Set m_oSig = lsgNew
End Property

Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property

Public Sub ShowOptions(lID As Long)
    On Error GoTo ProcError
    
'   show options
    With m_oSig
        '9.7.1 #4153
        Dim xTemplate As String
        If UCase(.Template.ClassName) = "CLETTER" Then
            xTemplate = .Template.ID
        Else
            xTemplate = g_oMPO.Templates.ItemFromClass("CLetter").ID
        End If
        mdlDialog.ShowOptions g_oMPO.People(lID), _
                              xTemplate, _
                              Me
        '---
    End With
    
    '*** 9.7.1 #3675
    If UCase(xGetTemplateConfigVal(m_oSig.Template.ID, "ForceTitleForNonAttorney")) = "TRUE" Then
        If Not g_oMPO.People(lID).IsAttorney Then
            Me.chkIncludeAuthorTitle = 1
            Me.chkIncludeAuthorTitle.Enabled = False
        Else
            Me.chkIncludeAuthorTitle.Enabled = True
        End If
    End If
    '*** end #3675
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Could not successfully show author preferences."
    Exit Sub
End Sub

Public Sub UpdateForm()
    Dim bInitializing As Boolean
    
    On Error GoTo ProcError
'---get properties, load controls on reuse
    With Signature
        Me.txtCC = .CC
        Me.txtBCC = .BCC
        Me.txtTypistInitials = .TypistInitials
        Me.cmbEnclosures.Text = .Enclosures
        Me.cmbClosingPhrases.Text = .ClosingPhrase
        If .IncludeFirmName <> mpUndefined Then _
                Me.chkIncludeFirmName = Abs(.IncludeFirmName)
        '*** 9.7.1 #3675
        If UCase(xGetTemplateConfigVal(.Template.ID, "ForceTitleForNonAttorney")) = "TRUE" And _
           Not .Author.IsAttorney Then
           Me.chkIncludeAuthorTitle = 1
           Me.chkIncludeAuthorTitle.Enabled = False
        Else
            If .IncludeAuthorTitle <> mpUndefined Then _
                Me.chkIncludeAuthorTitle = Abs(.IncludeAuthorTitle)
        End If
        '*** end #3675
        m_bAuthorDirty = False
        Me.cmbAuthor.Text = .Document.ReuseAuthor.DisplayName
        m_bAuthorDirty = False
    
        '---9.7.1 - 4147
        If Not .Author2 Is Nothing Then
            Me.cmbAuthor2.BoundText = .Author2.ID
        Else
            Me.cmbAuthor2.Bookmark = 0
        End If
        
        '---9.7.1 - 4149/4614
        ' Make sure initials are updated
        If Not m_bAuthorChanged Then
            Me.txtAuthorInitials = .AuthorInitials
        Else
            'author was changed (letterhead), get defaults
            Me.cmbAuthor.BoundText = Me.Signature.Author.ID
            If Len(Me.cmbAuthor.BoundText) Then
'                m_bAuthorDirty = True
                bInitializing = Me.Initializing
                Me.Initializing = False
                cmbAuthor_ItemChange
                Me.Initializing = bInitializing
            End If
        End If
        Me.chkElectronicSignature = Abs(.UseElectronicSignature) '9.7.1 #4166
    End With
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmLetterSig.UpdateForm"
    Exit Sub
End Sub

Private Sub GetContacts(iDefSelection As ciSelectionLists)
    Const mpSep As String = vbCrLf
    Static bCIActive As Boolean
    
    On Error GoTo ProcError
    
    Dim vCC As Variant
    Dim vBCC As Variant
    Dim vCCList As Variant
    Dim vBCCList As Variant

    If Not g_bShowAddressBooks Then Exit Sub
    If bCIActive Then Exit Sub
    bCIActive = True
    
    g_oMPO.GetLetterRecipients , vCC, _
                               vBCC, True, True, _
                               iDefSelection, , , _
                               vCCList, _
                               vBCCList
                                
    '9.7.3 #4721
    With Me.Signature
           
        If vCC <> "" Then
            .CCFromCI = IIf(.CCFromCI <> Empty, _
                            .CCFromCI & vbCrLf & vCCList, vCCList)
            .CCFromCIFullDetail = IIf(.CCFromCIFullDetail <> Empty, _
                                      .CCFromCIFullDetail & vbCrLf & vbCrLf & vCC, vCC)
            
            If Me.txtCC <> "" Then
                Me.txtCC = xTrimTrailingChrs(Me.txtCC, vbCrLf) & mpSep & vCCList
            Else
                Me.txtCC = vCCList
            End If
            
        End If
        
        If vBCC <> "" Then
                .BCCFromCI = IIf(.BCCFromCI <> Empty, _
                                 .BCCFromCI & vbCrLf & vBCCList, vBCCList)
                .BCCFromCIFullDetail = IIf(.BCCFromCIFullDetail <> Empty, _
                                           .BCCFromCIFullDetail & vbCrLf & vbCrLf & vBCC, vBCC)
            
            
            If Me.txtBCC <> "" Then
                Me.txtBCC = xTrimTrailingChrs(Me.txtBCC, vbCrLf) & mpSep & vBCCList
            Else
                Me.txtBCC = vBCCList
            End If
        End If
    End With
    
    DoEvents
    
    Me.SetFocus
    bCIActive = False
    Exit Sub
ProcError:
    bCIActive = False
    g_oError.Show Err
    Exit Sub
End Sub


Private Sub btnAddressBooks_Click()
    GetContacts ciSelectionList_CC
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    DoEvents
    Application.ScreenUpdating = True
    Unload Me
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    DoEvents
    Me.Hide
    EchoOff
    
'*  set signature props
    SetSigProperties
    
    With Me.Signature
        .Rebuild
        .Finish
        
        'GLOG : 5317
        'assume that if cc/bcc have been cleared out, then
        'env/labels should not include either
        If .CC = "" Then
            .CCFromCI = ""
            .CCFromCIFullDetail = ""
        End If
        
        If .BCC = "" Then
            .BCCFromCI = ""
            .BCCFromCIFullDetail = ""
        End If
        
    End With
    EchoOn
    If Len(Me.cmbAuthor.BoundText) And Me.Signature.Document.HasReuseAuthor Then    '9.7.1 - #3827
        bPromptForLetterheadChange Me.Signature.Template
    End If
    Unload Me
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub chkElectronicSignature_Click()
    OnControlGotFocus Me.chkElectronicSignature '9.7.1 #4166
End Sub

Private Sub chkIncludeAuthorTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeAuthorTitle
End Sub

Private Sub chkIncludeFirmName_GotFocus()
    OnControlGotFocus Me.chkIncludeFirmName
End Sub

Private Sub cmbAuthor_GotFocus()
    DoEvents
    OnControlGotFocus Me.cmbAuthor, , True
End Sub

'9.7.2 #4614
Private Sub cmbAuthor_Open()
    b_Opened = True
End Sub

'9.7.2 #4614
Private Sub cmbAuthor_ItemChange()
    On Error GoTo ProcError
    Dim lID As Long

    If Me.Initializing Then
        Exit Sub
    End If
    
    With Me.cmbAuthor
        If .Text = "" Then
'               validate
            xMsg = "Author is required information"
            .SetFocus
            MsgBox xMsg, vbExclamation, App.Title
            Exit Sub
        End If
    End With

    
'       write to object
    lID = Me.cmbAuthor.BoundText
    
    Me.ShowOptions lID
    
    '---9.7.1 4149
    If Me.Signature.AllowAuthorInitials Then
        txtAuthorInitials.Text = BuildAuthorInitials
    End If
    
    
    Me.Refresh
    DoEvents
    
    m_bAuthorDirty = False
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthorDirty = True
    End If

End Sub

Private Sub cmbAuthor_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthorDirty = False
        If Len(Me.cmbAuthor.BoundText) Then
            cmbAuthor_ItemChange
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Change()
    m_bAuthorDirty = True
End Sub

Private Sub cmbAuthor_Click()
    If b_Opened Then
        m_bAuthorDirty = False
    End If
End Sub

Private Sub cmbAuthor_Close()
    b_Opened = False
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    mdlDialog.CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If m_bAuthorDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbClosingPhrases_GotFocus()
    OnControlGotFocus Me.cmbClosingPhrases
End Sub

Private Sub cmbEnclosures_GotFocus()
    OnControlGotFocus Me.cmbEnclosures
End Sub

Private Sub cmbAuthor2_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        Me.PopupMenu mnuAuthor2
    End If

End Sub
Private Sub mnuAuthor2_Clear_Click()
    Me.cmbAuthor2.Bookmark = 0
End Sub
Private Sub cmbAuthor2_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor2, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor2_Open()
    m_bOpened2 = True
End Sub

Private Sub cmbAuthor2_Change()
    m_bAuthor2Dirty = True
End Sub

Private Sub cmbAuthor2_Click()
    If m_bOpened2 Then
        m_bAuthor2Dirty = False
    End If
End Sub

Private Sub cmbAuthor2_Close()
    m_bOpened2 = False
End Sub

Private Sub cmbAuthor2_GotFocus()
    DoEvents
    OnControlGotFocus Me.cmbAuthor2
End Sub
Private Sub cmbAuthor2_ItemChange()
    Dim lID As Long
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If
    
        '---9.7.1 4149
    If Me.Signature.AllowAuthorInitials = "True" Then
        txtAuthorInitials.Text = BuildAuthorInitials
    End If
        
    m_bAuthor2Dirty = False
    Me.Refresh
    DoEvents
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    m_bAuthor2Dirty = False
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbAuthor2_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If Me.cmbAuthor2.Text = Me.cmbAuthor.Text Then
        MsgBox "Second author must be different.", vbExclamation, App.Title
        Cancel = True
    ElseIf m_bAuthor2Dirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor2)
    End If
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor2_KeyUp(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 38 Or KeyCode = 40 Then
         m_bAuthor2Dirty = False
        If Len(Me.cmbAuthor2.BoundText) Then
            cmbAuthor2_ItemChange
        End If
     End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbAuthor2_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 38 Or KeyCode = 40 Then
        m_bAuthor2Dirty = True
    End If
End Sub


Private Sub Form_Activate()
    Dim lID As Long
    Dim iSource As Integer
    Dim datStart As Date
    
    On Error GoTo ProcError
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    If Me.Initializing Then
        On Error Resume Next
        '---9.7.1 4149
        DisplayControls
        SetInterfaceOptions Me, Me.Signature.Template.ID  '9.7.1 - #4160
        m_xDefaultCCLabel = Me.lblCC.Caption '9.7.1 #4151
        m_xDefaultBCCLabel = Me.lblBCC.Caption '9.7.1 #4151
        PositionControls
        
        With Me.Signature
'            If .Document.IsCreated Then
            If .Document.HasReuseAuthor Then    '9.7.1 - #3827
                UpdateForm
            Else
                Me.cmbAuthor.BoundText = .Author.ID
                Me.ShowOptions .Author.ID
                DoEvents

                '---9.7.1 - 4147
                Me.cmbAuthor2.Bookmark = 0

'               get typist initials from user ini
                With Me.Signature
                    Me.txtTypistInitials = GetUserIni(.Template.ID, _
                                                      "TypistInitials")
                End With
                
                m_bAuthorDirty = False
            End If
            
            With .Document.File
                .ActiveWindow.View.ShowHiddenText = False
                DoEvents
            
                If .ProtectionType <> wdNoProtection Then
                    .Unprotect
                End If
                
                Application.ScreenRefresh
            End With
        End With
        
        Me.Initializing = False
    End If
'    Me.Timer1.Enabled = True
    Exit Sub
ProcError:
    Me.Initializing = False
    g_oError.Show Err, "Error Activating Letter Signature Form"
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF2 Then
        GetContacts ciSelectionList_CC
    End If
End Sub

Private Sub Form_Load()
    Dim oTemplate As MPO.CTemplate
    
    On Error GoTo ProcError
    Me.Initializing = True
    
    Set m_oSig = New MPO.CLetterSig
    Set m_PersonsForm = New MPO.CPersonsForm
    Set m_oOptions = New MPO.COptionsForm
    
    '9.7.1 #4153/#4614
    ' Document may not be a letter
    If UCase(m_oSig.Template.ClassName) = "CLETTER" Then
        'GLOG : 5103 : CEH
        On Error Resume Next
        m_bAuthorChanged = (Val(m_oSig.Document.ReuseAuthor.ID) <> Val(m_oSig.LetterAuthor.ID))
        On Error GoTo ProcError
        m_oSig.GetStartingAuthor
    Else
        If Not m_oSig.Template.DefaultAuthor Is Nothing Then
            m_oSig.Author = m_oSig.Template.DefaultAuthor
        Else
            m_oSig.Author = g_oMPO.db.People.First
        End If
    End If
    '---
    
    m_ShowHiddenText = Word.ActiveWindow.View.ShowHiddenText
    
    With g_oMPO.Lists
        Me.cmbClosingPhrases.Array = .Item("ClosingPhrases").ListItems.Source
        Me.cmbEnclosures.Array = .Item("Enclosures").ListItems.Source
    End With
   
    ResizeTDBCombo Me.cmbClosingPhrases, 10
    ResizeTDBCombo Me.cmbEnclosures, 4
    
    g_oMPO.People.Refresh
    
    With Me.cmbAuthor
        .Array = g_oMPO.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
        DoEvents
    End With
    
    '---9.7.1 - 4147
    If bIniToBoolean(xGetTemplateConfigVal(m_oSig.Template.ID, "ShowAuthor2")) Then
        With Me.cmbAuthor2
            .Array = xARClone(g_oMPO.People.ListSource)
            .Rebind
            .Array.Insert 1, 0
            .Array.value(0, 0) = -1
            .Array.value(0, 1) = "-none-"
            .Array.value(0, 2) = -1
            ResizeTDBCombo Me.cmbAuthor2, 10
            DoEvents
        End With
    Else
        Me.lblAuthor2.Visible = False
        Me.cmbAuthor2.Visible = False
    End If

'   show AddressBooks button
    Me.btnAddressBooks.Visible = g_bShowAddressBooks

    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Loading Error Signature Form"
    Exit Sub
End Sub


Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'SetLastPosition Me
    Set m_oSig = Nothing
    Set m_PersonsForm = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_BCC
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_GotFocus()
    OnControlGotFocus Me.txtBCC
End Sub

Private Sub txtCC_Change()
    On Error GoTo ProcError
    UpdateLabelCaption txtCC, Me.lblCC, m_xDefaultCCLabel, "copy", "copies" '9.7.1 #4151
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtBCC_Change()
    On Error GoTo ProcError
    UpdateLabelCaption txtBCC, Me.lblBCC, m_xDefaultBCCLabel, "copy", "copies" '9.7.1 #4151
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Public Function SetSigProperties()
    With m_oSig
        If Len(Me.cmbAuthor.BoundText) Then
            .Author = g_oMPO.People.Item(Me.cmbAuthor.BoundText)
        End If
        '9.7.1 #4153
        ' Document may not be a letter
        If UCase(.Template.ClassName) = "CLETTER" Then
            .UpdateForAuthor
        Else
            .AuthorName = .Author.FullName
            .AuthorTitle = .Author.Title
            .AuthorFirmName = .Author.Office.FirmName
            .AuthorFirmSlogan = .Author.Office.Slogan
            .ElectronicSignerName = .Author.FullName '9.7.1 #4166
        End If
        '---
        
        '---9.7.1 -4147
        If bIniToBoolean(xGetTemplateConfigVal(.Template.ID, "ShowAuthor2")) Then
            If Len(Me.cmbAuthor2.BoundText) > 0 Then
                .Author2 = g_oMPO.People.Item(Me.cmbAuthor2.BoundText)
            Else
                .Author2 = Nothing
            End If
         End If
        .BCC = Me.txtBCC
        .CC = Me.txtCC
        .ClosingPhrase = Me.cmbClosingPhrases
        .Enclosures = Me.cmbEnclosures
        .IncludeAuthorTitle = 0 - Me.chkIncludeAuthorTitle
        .IncludeFirmName = 0 - Me.chkIncludeFirmName
        .TypistInitials = Me.txtTypistInitials
        '---9.7.1 4149
        .AuthorInitials = Me.txtAuthorInitials
        .UseElectronicSignature = 0 - Me.chkElectronicSignature '9.7.1 #4166
    End With
End Function

Private Sub txtCC_DblClick()
    On Error GoTo ProcError
    GetContacts ciSelectionList_CC
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtCC_GotFocus()
    OnControlGotFocus Me.txtCC
End Sub

Private Sub txtTypistInitials_GotFocus()
    OnControlGotFocus Me.txtTypistInitials
End Sub

Private Sub UpdateLabelCaption(txtB As TextBox, _
                               lblL As Label, _
                               xCaptionRoot As String, _
                               Optional xRecip As String = "recipient", _
                               Optional xRecipPlural As String = "recipients")
    Dim iNumEntries As Integer
    Dim iNumEntriesComma As Integer
    
    On Error GoTo ProcError
    
    If txtB.Text <> "" Then
        iNumEntries = lCountChrs(txtB.Text, vbCrLf) + 1
        iNumEntriesComma = lCountChrs(txtB.Text, ",") + 1
        iNumEntries = CInt(Max(CDbl(iNumEntries), CDbl(iNumEntriesComma)))
        If iNumEntries = 1 Then
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (1 " & xRecip & ")"
        Else
            lblL = " " & xCaptionRoot & " " & vbCrLf & " (" & iNumEntries & " " & xRecipPlural & ")"
        End If
    Else
        lblL = " " & xCaptionRoot
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmLetterSig.UpdateLabelCaption"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub
Public Sub DisplayControls()
    
    '---9.7.1 - 4149
    Me.lblAuthorInitials.Visible = Abs(CInt(Me.Signature.AllowAuthorInitials))
    Me.txtAuthorInitials.Visible = Me.lblAuthorInitials.Visible
    Me.chkElectronicSignature.Visible = bIniToBoolean(xGetTemplateConfigVal(Me.Signature.Template.ID, _
        "AllowElectronicSignature")) '9.7.1 #4166
    
End Sub

Public Function BuildAuthorInitials() As String
    '---9.7.1 - 4148
    Dim xTemp As String
    Dim xSep As String
    Dim oAuthor As CPerson
    Dim oAuthor2 As CPerson
    
    On Error GoTo ProcError
    '---we dont set the actual authors until finish, so
    If Len(Me.cmbAuthor.BoundText) > 0 Then
        Set oAuthor = g_oMPO.People.Item(Me.cmbAuthor.BoundText)
    Else
        Set oAuthor = m_oSig.Author
    End If
    
    If Len(Me.cmbAuthor2.BoundText) > 0 Then
        If Val(Me.cmbAuthor2.BoundText) > 0 Then
            Set oAuthor2 = g_oMPO.People.Item(Me.cmbAuthor2.BoundText)
        End If
    End If
   
    xSep = Signature.MultiInitialsSeparator
    
    If oAuthor2 Is Nothing Then
        xTemp = oAuthor.Initials
    Else
        xTemp = oAuthor.Initials & xSep & oAuthor2.Initials
    End If
    BuildAuthorInitials = xTemp
    Exit Function
ProcError:
    g_oError.Raise Err, "MacPac90.frmLetterSig.BuildAuthorInitials"

End Function

Private Sub PositionControls()
    Dim sIncrement As Single
    Dim sOffset As Single
    Dim i As Integer
    Dim oCtl As VB.Control
    Dim arrCtls() As String

    On Error GoTo ProcError
    
    ReDim arrCtls(Me.Controls.Count)
'   place each control in tab order sort
    On Error Resume Next
    For Each oCtl In Me.Controls
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next
    On Error GoTo ProcError
    sIncrement = 175
    For i = cmbAuthor2.TabIndex To txtBCC.TabIndex
        ' See which controls are visible to set correct spacing
        Set oCtl = Me.Controls(arrCtls(i))
        
'       do only if visible
        If Not oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.CheckBox Or _
                TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                TypeOf oCtl Is mpSal.SalutationCombo) Then
'               position control relative to previous control
                sIncrement = sIncrement + 30
            ElseIf (TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is mpControls4.RelineControl) Then
                sIncrement = sIncrement + ((oCtl.Height / 30) * 30)
            ElseIf (TypeOf oCtl Is mpControls.MultiLineCombo) Then
                sIncrement = sIncrement + 60
            End If
            
        End If
    Next i
    '9.7.1020 #4582
    If sIncrement > 180 Then
        sIncrement = 180
    ElseIf sIncrement > 140 Then
        sIncrement = 140
    End If
    sOffset = Me.cmbAuthor.Top + Me.cmbAuthor.Height + sIncrement
    For i = cmbAuthor2.TabIndex To txtBCC.TabIndex
        Set oCtl = Me.Controls(arrCtls(i))
'       do only if visible
        If oCtl.Visible Then
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is VB.CheckBox Or _
                TypeOf oCtl Is mpControls.MultiLineCombo Or _
                TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                TypeOf oCtl Is mpControls4.RelineControl Or _
                TypeOf oCtl Is mpSal.SalutationCombo) Then
                
                If TypeOf oCtl Is mpControls.MultiLineCombo Then
                    oCtl.Top = sOffset
                    sOffset = sOffset + (Me.cmbAuthor.Height * 2) + sIncrement
                ElseIf TypeOf oCtl Is MPCM.ClientMatterComboBox Then
                    oCtl.Top = sOffset
                    sOffset = sOffset + Me.cmbAuthor.Height + sIncrement
                ElseIf TypeOf oCtl Is mpControls4.RelineControl Then
                    oCtl.Top = sOffset
                    sOffset = sOffset + (oCtl.Height - 300) + sIncrement
                ElseIf TypeOf oCtl Is VB.CheckBox Then
'               position control relative to previous control
                    Select Case UCase(oCtl.Name)
                        Case "CHKINCLUDEFIRMNAME"
                            If chkIncludeAuthorTitle.Visible Then
                                sOffset = chkIncludeAuthorTitle.Top
                            End If
                    End Select
                    oCtl.Top = sOffset
                    sOffset = oCtl.Top + oCtl.Height + sIncrement
                Else
                    oCtl.Top = sOffset
                    sOffset = sOffset + oCtl.Height + sIncrement
                End If
'               set accompanying label vertical position
                If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                    Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                End If
            End If
        End If
    Next i
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmLetterSig.PositionControls"
End Sub

