VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmExhibits 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert Exhibits/Schedules"
   ClientHeight    =   5532
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   5472
   Icon            =   "frmExhibits.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5532
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TrueDBList60.TDBCombo cmbLocation 
      Height          =   600
      Left            =   1755
      OleObjectBlob   =   "frmExhibits.frx":058A
      TabIndex        =   10
      Top             =   4215
      Width           =   3570
   End
   Begin VB.CheckBox chkFormatPageNumber 
      Appearance      =   0  'Flat
      Caption         =   "Special &Page Numbering (A-1, B-1, etc.)"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1815
      TabIndex        =   11
      Top             =   4695
      Width           =   3375
   End
   Begin VB.CommandButton btnNew 
      Height          =   400
      Left            =   3915
      Picture         =   "frmExhibits.frx":27A5
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Add a new exhibit (F5)"
      Top             =   1995
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4395
      Picture         =   "frmExhibits.frx":2CE7
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Save new exhibit (F6)"
      Top             =   1995
      Width           =   435
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4875
      Picture         =   "frmExhibits.frx":2E65
      Style           =   1  'Graphical
      TabIndex        =   6
      ToolTipText     =   "Delete current exhibit (F7)"
      Top             =   1995
      Width           =   435
   End
   Begin VB.ListBox lstExhibits 
      Appearance      =   0  'Flat
      Height          =   1560
      Left            =   1755
      TabIndex        =   8
      Top             =   2475
      Width           =   3570
   End
   Begin VB.TextBox txtExhibitTitle 
      Appearance      =   0  'Flat
      Height          =   1185
      Left            =   1755
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   720
      Width           =   3570
   End
   Begin VB.TextBox txtExhibitNumber 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   1755
      TabIndex        =   1
      Top             =   180
      Width           =   3570
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4320
      TabIndex        =   13
      Top             =   5085
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3255
      TabIndex        =   12
      Top             =   5085
      Width           =   1000
   End
   Begin VB.Label lblLocation 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "I&nsert At:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   9
      Top             =   4260
      Width           =   1455
   End
   Begin VB.Label lblExhibits 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Exhibits/Schedules List:"
      ForeColor       =   &H00FFFFFF&
      Height          =   525
      Left            =   105
      TabIndex        =   7
      Top             =   2490
      Width           =   1455
   End
   Begin VB.Label lblExhibitTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Exhibits/Schedules &Title:"
      ForeColor       =   &H00FFFFFF&
      Height          =   435
      Left            =   105
      TabIndex        =   2
      Top             =   750
      Width           =   1455
   End
   Begin VB.Label lblExhibitNumber 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Number/&Letter:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   0
      Top             =   210
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   5955
      Left            =   -30
      Top             =   -390
      Width           =   1695
   End
   Begin VB.Menu mnuExhibits 
      Caption         =   "Exhibits"
      Visible         =   0   'False
      Begin VB.Menu mnuExhibitsDelete1 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuExhibitsDeleteAll 
         Caption         =   "Delete &All"
      End
   End
End
Attribute VB_Name = "frmExhibits"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_bCancelled As Boolean
Private m_bEditing As Boolean
Private m_bChanged As Boolean
Private m_bAdded As Boolean
Private m_oParent As Form

Const mpCancelAdd As String = "Cancel unsaved exhibit (F7)"
Const mpDeleteSig As String = "Delete this exhibit (F7)"
Const mpCancelChange As String = "Cancel unsaved changes (F7)"
Const mpSaveChanges As String = "Save changes to this exhibit (F6)"
Const mpSaveNewSignature As String = "Save new exhibit (F6)"

Private m_oExhibits As MPO.CExhibits
Private xarExhibits As XArrayObject.XArray
Const mpDefaultHeading As String = "Exhibit "

'**********************************************************
'   Properties
'**********************************************************

Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property

Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property

Public Property Let Exhibits(oNew As MPO.CExhibits)
    Set m_oExhibits = oNew
End Property

Public Property Get Exhibits() As MPO.CExhibits
    Set Exhibits = m_oExhibits
End Property

Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property

Public Property Get Added() As Boolean
    Added = m_bAdded
End Property

Public Property Let Added(bNew As Boolean)
    m_bAdded = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelAdd
        Me.btnSave.ToolTipText = mpSaveNewSignature
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property

Public Property Get Changed() As Boolean
    Changed = m_bChanged
End Property

Public Property Let Changed(bNew As Boolean)
    m_bChanged = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelChange
        Me.btnSave.ToolTipText = mpSaveChanges
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnOK_Click()
    Dim i As Integer
    
    On Error GoTo ProcError
    If Me.Changed Then
        PromptToSave
    End If
    Me.Hide
    EchoOff

    If xarExhibits.UpperBound(1) > -1 Then
        With m_oExhibits
            .Location = cmbLocation.BoundText
            .FormatPageNumber = Me.chkFormatPageNumber
            For i = 0 To xarExhibits.UpperBound(1)
                If i <= .Count - 1 Then
                'Edit existing entries
                    DoEvents
                    .Item(i + 1).value = xarExhibits(i, 1)
                    .Item(i + 1).Description = xarExhibits(i, 0)
                Else
                'add new entries
                    .AddItem xarExhibits(i, 0), xarExhibits(i, 1)
                End If
            Next i
            If Me.ParentForm Is Nothing Then
                .Finish
                '---9.6.1 - handle pleading paper if added to pleading
                Dim oPleading As CPleading
                Dim iSec As Integer
                
                Set oPleading = g_oMPO.ExistingPleading
                '9.7.1020 #4510
                If Not oPleading Is Nothing Then
                    With oPleading
                        .PleadingPaper.CaseNumberSeparator = .Definition.FooterCaseNumberSeparator
                        .PleadingPaper.HasCoverPageSection = False
                        .PleadingPaper.StartPageNumberingAt = 1
                        .PleadingPaper.StartingPageNumber = 1
                    End With
                    
                    Select Case Me.cmbLocation.BoundText
                        Case 1 '---BOF
                            oPleading.PleadingPaper.Update 1
                        Case 2  '---EOF
                            oPleading.PleadingPaper.Update , , , , _
                                ActiveDocument.Sections.Last.Index - xarExhibits.UpperBound(1)
                        Case Else   '---cursor location.
                            oPleading.PleadingPaper.Update Selection.Sections.First.Index
                    End Select
                    
                
                End If
            End If
            
        End With
    End If
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
End Sub

Private Sub btnOK_GotFocus()
    OnControlGotFocus Me.btnOK
End Sub

Private Sub chkFormatPageNumber_GotFocus()
    OnControlGotFocus Me.chkFormatPageNumber
End Sub

Private Sub cmbLocation_GotFocus()
    OnControlGotFocus Me.cmbLocation
End Sub
Private Sub cmbLocation_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLocation, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbLocation_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLocation)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Form_Activate()
    If Not (Me.ParentForm Is Nothing) Then
        UpdateForm
    End If
    
    With Me.cmbLocation
        .BoundText = Exhibits.Location
        If .BoundText = "" Then
            .Bookmark = 0
        End If
    End With
    If Exhibits.FormatPageNumber Then
        Me.chkFormatPageNumber.value = 1
    End If
    '#3837 - 9.6.2
    If InStr(UCase(Me.Exhibits.Document.Template), "PLEADING.DOT") <> 0 Then
        Me.chkFormatPageNumber.value = vbUnchecked
        Me.chkFormatPageNumber.Visible = False
    End If
    btnNew_Click
    Me.Initializing = False
End Sub

Private Sub UpdateForm()
    Dim i As Integer
    Dim j As Integer
    
    With m_oExhibits
        For j = 1 To .Count
            With xarExhibits
                i = .UpperBound(1) + 1
                .ReDim 0, i, 0, 1
                DoEvents
                .value(i, 0) = m_oExhibits.Item(j).value
                .value(i, 1) = m_oExhibits.Item(j).Description
            End With
            DoEvents
            With lstExhibits
                .AddItem m_oExhibits.Item(j).value
                .ListIndex = .ListCount - 1
            End With
        Next j
    End With

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF5 And btnNew.Enabled Then
        btnNew_Click
    ElseIf KeyCode = vbKeyF6 And btnSave.Enabled Then
        btnSave_Click
    ElseIf KeyCode = vbKeyF7 And btnDelete.Enabled Then
        btnDelete_Click
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub Form_Load()
    Me.Initializing = True
    
    If m_oExhibits Is Nothing Then _
        Set m_oExhibits = g_oMPO.NewExhibits
        
    Set xarExhibits = g_oMPO.NewXArray
    xarExhibits.ReDim 0, -1, 0, 1
    
    Dim xarLocations As XArray
    Set xarLocations = g_oMPO.NewXArray
    With xarLocations
        .ReDim 0, 2, 0, 1
        .value(0, 0) = 2
        .value(0, 1) = "End of Document"
        .value(1, 0) = 1
        .value(1, 1) = "Start of Document"
        .value(2, 0) = 3
        .value(2, 1) = "Insertion Point"
    End With
    cmbLocation.Array = xarLocations
    ResizeTDBCombo cmbLocation, 3
    
'   set up captions
    Dim xCaption As String
    
    xCaption = mpbase2.GetMacPacIni("Exhibits", "DialogCaption")
    If xCaption = Empty Then
        Me.Caption = "Insert Exhibits"
    Else
        Me.Caption = xCaption
    End If
    
    xCaption = mpbase2.GetMacPacIni("Exhibits", "TitleCaption")
    If xCaption = Empty Then
        Me.lblExhibitTitle.Caption = "Exhibit &Title:"
    Else
        Me.lblExhibitTitle.Caption = xCaption & ":"
    End If
    
    xCaption = mpbase2.GetMacPacIni("Exhibits", "ListCaption")
    If xCaption = Empty Then
        Me.lblExhibits.Caption = "&Exhibit List:"
    Else
        Me.lblExhibits.Caption = xCaption & ":"
    End If
    
    'MoveToLastPosition Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
    Set xarExhibits = Nothing
    Set m_oExhibits = Nothing
    Set frmExhibits = Nothing
    Set g_oPrevControl = Nothing
End Sub

Private Sub lstExhibits_Click()
    With lstExhibits
        If .ListIndex > -1 Then
            txtExhibitNumber = xarExhibits(.ListIndex, 0)
            txtExhibitTitle = xarExhibits(.ListIndex, 1)
            Me.Added = False
            Me.Changed = False
        End If
    End With
End Sub
Private Sub lstExhibits_GotFocus()
    OnControlGotFocus Me.lstExhibits
End Sub
Private Sub lstExhibits_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton And lstExhibits.ListCount > 0 Then
        PopupMenu mnuExhibits
    End If
End Sub
Private Sub mnuExhibitsDelete1_Click()
    Dim i As Integer
    If lstExhibits.ListCount = 1 Then
        mnuExhibitsDeleteAll_Click
    Else
        i = lstExhibits.ListIndex
        lstExhibits.RemoveItem i
        xarExhibits.Delete 1, i
        If i >= lstExhibits.ListCount Then
            i = i - 1
        End If
        lstExhibits.ListIndex = Max(CDbl(i), lstExhibits.ListCount - 1)
    End If
End Sub
Private Sub mnuExhibitsDeleteAll_Click()
    lstExhibits.Clear
    xarExhibits.ReDim 0, -1, 0, 1
    ClearFields
End Sub
Private Sub txtExhibitNumber_GotFocus()
    txtExhibitNumber.SelStart = Len(txtExhibitNumber) + 1
    txtExhibitNumber.SelLength = 0
    DoEvents
    SetControlBackColor Me.txtExhibitNumber
End Sub
Private Sub txtExhibitTitle_Change()
    Me.Changed = True
End Sub
Private Sub txtExhibitNumber_Change()
    Me.Changed = True
End Sub
Private Sub txtExhibitTitle_GotFocus()
    txtExhibitTitle.SelStart = 0
    txtExhibitTitle.SelLength = Len(txtExhibitTitle)
    SetControlBackColor Me.txtExhibitTitle
End Sub

Private Sub txtExhibitTitle_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = vbRightButton And Shift = vbCtrlMask Then
        txtExhibitTitle = ""
    End If
End Sub
Private Sub ClearFields()
    Dim xNum As String
    
    Me.txtExhibitTitle = ""
    xNum = mpbase2.GetUserIni("Exhibits", "NumberPrefill") & " "
    If xNum = Empty Or xNum = " " Then
        xNum = "Exhibit "
    End If
    
    With Me.txtExhibitNumber
        .Text = xNum
        .SelStart = Len(Me.txtExhibitNumber.Text)
        .SetFocus
    End With
    
    Me.Changed = False
    Me.Added = True
End Sub
Private Sub btnDelete_GotFocus()
    OnControlGotFocus Me.btnDelete
End Sub

Private Sub btnDelete_Click()
    Dim i As Integer
    On Error GoTo ProcError
    
    If Me.Changed Then
        ClearFields
        i = lstExhibits.ListIndex
        If i < lstExhibits.ListCount - 1 Then
            lstExhibits.ListIndex = i
        Else
            lstExhibits.ListIndex = lstExhibits.ListCount - 1
        End If
        lstExhibits_Click
    Else
'       prompt for deletion
        Dim iChoice As VbMsgBoxResult
        iChoice = MsgBox("Delete the selected exhibit?", vbQuestion + vbYesNo, App.Title)
        If iChoice = vbNo Then
            Exit Sub
        End If
        
        With lstExhibits
            i = .ListIndex
            If i > -1 Then
                If .ListCount > 1 Then
                    xarExhibits.Delete 1, i
                Else
                    xarExhibits.ReDim 0, -1, 0, 1
                End If
                lstExhibits.RemoveItem i
                DoEvents
                If i < lstExhibits.ListCount - 1 Then
                    lstExhibits.ListIndex = i
                Else
                    lstExhibits.ListIndex = lstExhibits.ListCount - 1
                    If lstExhibits.ListCount = 0 Then
                        ClearFields
                    End If
                End If
            End If
        End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub btnSave_Click()
    Dim xArr() As String
    Dim xPrefill As String
    Dim i As Integer
    
    '9.7.1 - #4191
    If Me.txtExhibitNumber.Text = Empty Then
        MsgBox "Exhibit Number/Letter cannot be blank.", vbExclamation, App.Title
        Me.txtExhibitNumber.SetFocus
        Exit Sub
    End If
    
    On Error GoTo ProcError
    If Me.Added Then
        With xarExhibits
            i = .UpperBound(1) + 1
            .ReDim 0, i, 0, 1
            .value(i, 0) = txtExhibitNumber.Text
            .value(i, 1) = txtExhibitTitle.Text
        End With
            
'       store exhibits prefill for next use-
'       get everything up to last word
        xArr() = Split(Me.txtExhibitNumber.Text, " ")
        xArr(UBound(xArr)) = ""
        xPrefill = Join(xArr, " ")
        '9.7.1 #4332
        mpbase2.DeleteUserIniKey "Exhibits", "NumberPrefill"
        mpbase2.SetUserIni "Exhibits", "NumberPrefill", mpbase2.xTrimTrailingChrs(xPrefill, " ")
        
        Me.Changed = False
        DoEvents
        With lstExhibits
            .AddItem txtExhibitNumber
            .ListIndex = .ListCount - 1
        End With
'        btnNew_Click       9.7.1 - #3880
    Else
        With lstExhibits
            i = .ListIndex
            If i > -1 Then
                xarExhibits.value(i, 0) = txtExhibitNumber.Text
                xarExhibits.value(i, 1) = txtExhibitTitle.Text
                .List(i) = txtExhibitNumber
            End If
        End With
        Me.Changed = False
    End If
    Me.txtExhibitNumber.SetFocus
    txtExhibitNumber_GotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub btnSave_GotFocus()
    OnControlGotFocus Me.btnSave
End Sub
Private Sub btnNew_Click()
    ClearFields
    DoEvents
    Me.txtExhibitNumber.SetFocus
    txtExhibitNumber_GotFocus
End Sub
Private Sub btnNew_GotFocus()
    OnControlGotFocus Me.btnNew
End Sub
Private Sub PromptToSave()
    Dim xMsg As String
    Dim iChoice As Integer
    
    On Error GoTo ProcError
    xMsg = "Save current exhibit?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        btnSave_Click
    End If
    Exit Sub
ProcError:
    g_oError.Raise "MacPac90.frmExhibits.PromptToSave"
    Exit Sub
End Sub

