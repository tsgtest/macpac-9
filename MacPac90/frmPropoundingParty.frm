VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmPropoundingParty 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create Propounding Party Information"
   ClientHeight    =   3612
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   5724
   Icon            =   "frmPropoundingParty.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3612
   ScaleWidth      =   5724
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.TextBox txtSetNo 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2040
      TabIndex        =   9
      Top             =   2370
      Width           =   3570
   End
   Begin VB.TextBox txtRespondingPartyName 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2040
      TabIndex        =   5
      Top             =   1251
      Width           =   3570
   End
   Begin VB.TextBox txtPropoundingPartyName 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2040
      TabIndex        =   1
      Top             =   135
      Width           =   3570
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4590
      TabIndex        =   11
      Top             =   3120
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3525
      TabIndex        =   10
      Top             =   3120
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbPropoundingPartyTitle 
      Height          =   315
      Left            =   2040
      OleObjectBlob   =   "frmPropoundingParty.frx":058A
      TabIndex        =   3
      Top             =   690
      Width           =   3600
   End
   Begin TrueDBList60.TDBCombo cmbRespondingPartyTitle 
      Height          =   315
      Left            =   2040
      OleObjectBlob   =   "frmPropoundingParty.frx":27B6
      TabIndex        =   7
      Top             =   1815
      Width           =   3585
   End
   Begin VB.Label lblRespondingPartyTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "Party &Title:"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   420
      TabIndex        =   6
      Tag             =   "Party &2:"
      Top             =   1860
      Width           =   1455
   End
   Begin VB.Label lblPropoundingPartyTitle 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "P&arty Title:"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   405
      TabIndex        =   2
      Tag             =   "Party &1:"
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label lblSetNo 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "S&et No:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   435
      TabIndex        =   8
      Top             =   2400
      Width           =   1455
   End
   Begin VB.Label lblRespondingPartyName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Responding Party Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   105
      TabIndex        =   4
      Top             =   1275
      Width           =   1785
   End
   Begin VB.Label lblPropoundingPartyName 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Propounding Party Name:"
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   -15
      TabIndex        =   0
      Top             =   165
      Width           =   1890
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   6030
      Left            =   -30
      Top             =   -390
      Width           =   1995
   End
   Begin VB.Menu mnuExhibits 
      Caption         =   "Exhibits"
      Visible         =   0   'False
      Begin VB.Menu mnuExhibitsDelete1 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuExhibitsDeleteAll 
         Caption         =   "Delete &All"
      End
   End
End
Attribute VB_Name = "frmPropoundingParty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_bCancelled As Boolean
Private m_oPleadingPropoundingParty As MPO.CPleadingPropoundingParty

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Public Property Let PleadingPropoundingParty(oNew As MPO.CPleadingPropoundingParty)
    Set m_oPleadingPropoundingParty = oNew
End Property
Public Property Get PleadingPropoundingParty() As MPO.CPleadingPropoundingParty
    Set PleadingPropoundingParty = m_oPleadingPropoundingParty
End Property

Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
End Sub
Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub
Private Sub btnOK_Click()
    
    On Error GoTo ProcError
    Me.Cancelled = False
    Me.Hide
    
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
End Sub

Private Sub btnOK_GotFocus()
    OnControlGotFocus Me.btnOK
End Sub

Private Sub cmbPropoundingPartyTitle_GotFocus()
    OnControlGotFocus cmbPropoundingPartyTitle, "zzmpPropoundingPartyTitle"
End Sub
Private Sub cmbPropoundingPartyTitle_LostFocus()
    On Error GoTo ProcError
    Me.PleadingPropoundingParty.PropoundingPartyTitle = cmbPropoundingPartyTitle.Text
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbRespondingPartyTitle_GotFocus()
    OnControlGotFocus cmbRespondingPartyTitle, "zzmpRespondingPartyTitle"
End Sub

Private Sub cmbRespondingPartyTitle_LostFocus()
    On Error GoTo ProcError
    Me.PleadingPropoundingParty.RespondingPartyTitle = cmbRespondingPartyTitle.Text
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Form_Activate()
    
    On Error GoTo ProcError
    If Me.Initializing Then
        If Me.PleadingPropoundingParty.Document.IsCreated Then
            UpdateForm
        Else
            Me.PleadingPropoundingParty.RefreshEmpty
        End If

        EchoOn
        Application.ScreenUpdating = True
        DoEvents
        Me.Initializing = False
        Screen.MousePointer = vbDefault

'       move dialog to last position
        'MoveToLastPosition Me
        g_oMPO.ForceItemUpdate = False
    End If
    Exit Sub
ProcError:
    EchoOn
    g_oMPO.ScreenUpdating = True
    g_oMPO.ForceItemUpdate = False
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Error Activating Propounding Party Form"
    Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oParties As XArray

    On Error GoTo ProcError
    Application.ScreenUpdating = False
    Me.Initializing = True
    g_oMPO.ForceItemUpdate = True
    Me.Top = 20000
    
    DoEvents
    Set oParties = g_oMPO.Lists("PleadingParties").ListItems.Source
    Me.cmbPropoundingPartyTitle.Array = oParties
    Me.cmbRespondingPartyTitle.Array = oParties

    ResizeTDBCombo Me.cmbPropoundingPartyTitle, 10
    ResizeTDBCombo Me.cmbRespondingPartyTitle, 10


    Me.Cancelled = True

    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Error Loading Propounding Party Form"
    g_oMPO.ForceItemUpdate = False
    Application.ScreenUpdating = True
    Exit Sub
End Sub
Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
End Sub
Private Sub UpdateForm()
    On Error Resume Next
    With Me.PleadingPropoundingParty
        DoEvents
        Me.txtPropoundingPartyName = .PropoundingPartyName
        Me.txtRespondingPartyName = .RespondingPartyName
        Me.cmbPropoundingPartyTitle.BoundText = .PropoundingPartyTitle
        Me.cmbRespondingPartyTitle.BoundText = .RespondingPartyTitle
    End With
End Sub

Private Sub txtPropoundingPartyName_LostFocus()
    Me.PleadingPropoundingParty.PropoundingPartyName = txtPropoundingPartyName
    Application.ScreenRefresh
End Sub

Private Sub txtPropoundingPartyName_GotFocus()
    OnControlGotFocus Me.txtPropoundingPartyName, "zzmpPropoundingPartyName"
End Sub

Private Sub txtRespondingPartyName_LostFocus()
    Me.PleadingPropoundingParty.RespondingPartyName = txtRespondingPartyName
    Application.ScreenRefresh
End Sub

Private Sub txtRespondingPartyName_GotFocus()
    OnControlGotFocus Me.txtRespondingPartyName, "zzmpRespondingPartyName"
End Sub


Private Sub txtSetNo_LostFocus()
    Me.PleadingPropoundingParty.SetNo = txtSetNo
    Application.ScreenRefresh
End Sub

Private Sub txtSetNo_GotFocus()
    OnControlGotFocus Me.txtSetNo, "zzmpSetNo"
End Sub

