Attribute VB_Name = "mdlGlobal"
Option Explicit

Public Declare Function GetKeyState Lib "user32" ( _
    ByVal nVirtKey As Long) As Integer
Public Declare Function GetFocus Lib "user32" ( _
    ) As Long
Public Declare Function SetWindowText Lib "user32" Alias "SetWindowTextA" ( _
    ByVal hwnd As Long, ByVal lpString As String) As Long
Public Declare Function LockWindowUpdate Lib "user32" ( _
    ByVal hwndLock As Long) As Long
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
    ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
Public Declare Function GetDesktopWindow Lib "user32.dll" () As Long
'GLOG : 5594|5381
Public Declare Function GetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByRef lpvParam As Long, ByVal lWinIni As Long) As Long
Public Declare Function SetSystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" ( _
    ByVal lAction As Long, ByVal lParam As Long, ByVal lpvParam As Long, ByVal lWinIni As Long) As Long

Public Const SPI_GETKEYBOARDCUES = 4106
Public Const SPI_SETKEYBOARDCUES = 4107

Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)
    
Public g_lFormLoad As Long

'fax
Public Const mpFaxCoverPageLimit As Integer = 20

'pleading
Public Const mpPleadingSignaturesLimit As Integer = 10
Public Const mpPleadingCaptionsLimit As Integer = 10
Public Const mpPleadingCounselLimit As Integer = 10
Public Const mpPleadingSignaturesDisplayFormat As String = "Signature "
Public Const mpPleadingCaptionsDisplayFormat As String = "Caption "
Public Const mpPleadingCounselsDisplayFormat As String = "Counsel "

'Pleading Paper Align
Public Enum PPaperAlignScope
    PPaperAlignScope_Paragraph = 0
    PPaperAlignScope_Selection = 1
    PPaperAlignScope_ToSignature = 2
End Enum

Public Const mpKeyShift = &H10
Public Const mpKeyCtrl = &H11
Public Const mpKeyMenu = &H12
Public Const mpKeyF1 = &H70
Public Const mpKeyF5 = &H74
Public Const mpKeya = 65
Public Const mpKeyDel = 46
Public Const mpKeyEnter = 13

'focus colors
Public g_lTicks As Long
Public g_lActiveCtlColor As Long
Public g_lInactiveCtlColor As Long

Public g_oMPO As MPO.CApplication
Public g_appCI As mpCI.Application
Public g_oError As mpError.CError
Public g_bDynEdit As Boolean
Public g_bShowAddressBooks As Boolean
Public g_bUSPSStandards As Boolean
Public g_bUse90Trailer As Boolean
Public g_bInitialized As Boolean
Public g_bTrailerFlag As Boolean
Public g_oPrevControl As VB.Control
Public g_lDefaultDocID As Long
Public g_bTrailerMacro As Boolean

'ClientMatter control
Public g_bCM_RunConnected As Boolean
Public g_xCM_RunConnectedTooltip As String
Public g_xCM_RunDisconnectedTooltip As String
Public g_bCM_ShowLoadMsg As Boolean
Public g_bCM_AllowFilter As Boolean
Public g_bCM_AllowUserToClearFilters As Boolean
Public g_bCM_AllowValidation As Boolean
Public g_xCM_Column1Heading As String
Public g_xCM_Column2Heading As String
Public g_xCM_Separator As String
Public g_xCM_Backend As String
Public g_bCM_SearchByMatter As Boolean '***9.7.1 - #4186

Public g_bForceNewTrailerTemplate As Boolean
Public g_bForceNewTrailerTemplateDialog As Boolean
Public g_bForceForceTrailerFont As Boolean
Public g_bAllowTrailerInFormsWorkflowDocs As Boolean

'---9.7.1 4033 Dynamic Trailer checkbox support
Public g_xAROptionalControls As XArray
Public g_bAllowTrailerOptions As Boolean

'GLOG : 5598 : ceh
Public g_bUpdatePreferencesInDialog As Boolean

'---9.7.1 - 4133
Public g_ParentForm As Form

'9.7.1 #4028
Public g_xLHCustom1Caption As String
Public g_xLHCustom2Caption As String
Public g_xLHCustom3Caption As String

'9.7.2 Word 12x
Public g_xPleadingTemplate As String

'mp10trailer
Public g_iMP10TrailerType As Integer
'Trailer
Public g_iFooterThreshold As Integer

Public Const g_xMixedPleadingSignatureWarning As String = "This pleading contains mixed signature formats.  " & _
                    "All signatures must use the same formatting - either table or non-table." & _
                    vbCr & vbCr & "You will be returned to the dialog to reselect signature types."
                    
Public g_bIsWord12x As Boolean
Public g_bIsWord14x As Boolean
Public g_bIsWord15x As Boolean

Public Const g_sWord15HeightAdjustment As Single = 150

Public lRet As Long



Sub Main()
End Sub

Sub EchoOff()
    Call LockWindowUpdate(FindWindow("OpusApp", 0&))
End Sub

Sub EchoOn()
    Call LockWindowUpdate(0&)
End Sub

Sub EchoOffDesktop()
    Dim lDhdl As Long
    Dim l As Long
    lDhdl = GetDesktopWindow
    Call LockWindowUpdate(lDhdl)
End Sub

Public Function CurErrSource(ByVal xNewSource As String) As String
'returns the description of the current error
    CurErrSource = g_oError.Source(xNewSource, Err.Source)
End Function

Public Sub NoOpenWindow()
    MsgBox "Please open a document before running this function.", _
        vbExclamation, App.Title
End Sub

Function MacPacIniIsValid(xInvalid As String) As Boolean
'returns TRUE if essential MacPac.ini keys are valid
    Dim aKeys(14, 1) As String
    Dim i As Integer
    Dim bInvalid As Boolean
    
    aKeys(0, 0) = "AllowReuseAppend"
    aKeys(0, 1) = "General"
    aKeys(1, 0) = "HideFixedMacPacBookmarks"
    aKeys(1, 1) = "General"
    aKeys(2, 0) = "AllowNativeWordNewDlg"
    aKeys(2, 1) = "General"
    aKeys(3, 0) = "DisplayTrailerDialog"
    aKeys(3, 1) = "Trailer"
    aKeys(4, 0) = "DMS"
    aKeys(4, 1) = "DMS"
    aKeys(5, 0) = "ActiveControlColor"
    aKeys(5, 1) = "General"
    aKeys(6, 0) = "InactiveControlColor"
    aKeys(6, 1) = "General"
    aKeys(7, 0) = "TemplatesDir"
    aKeys(7, 1) = "File Paths"
    aKeys(8, 0) = "UserDir"
    aKeys(8, 1) = "File Paths"
    aKeys(9, 0) = "BoilerplateDir"
    aKeys(9, 1) = "File Paths"
    
    aKeys(10, 0) = "Default Office"
    aKeys(10, 1) = "Firm"
    
    aKeys(11, 0) = "ShowSplash"
    aKeys(11, 1) = "General"
    aKeys(12, 0) = "CloseDocOnCancel"
    aKeys(12, 1) = "General"
    aKeys(13, 0) = "ShowAddressBooks"
    aKeys(13, 1) = "General"
    aKeys(14, 0) = "StartDot"
    aKeys(14, 0) = "General"
    
    For i = 0 To UBound(aKeys)
        If mpbase2.GetMacPacIni(aKeys(i, 1), aKeys(i, 0)) = Empty Then
            bInvalid = True
            xInvalid = xInvalid & vbCr & aKeys(i, 1) & "\" & aKeys(i, 0)
        End If
    Next i
    MacPacIniIsValid = Not bInvalid
End Function


Public Sub ResetEmptyHeader()
    WordBasic.ViewHeader
    WordBasic.ViewHeader
End Sub

Public Function xGetTemplateConfigVal(ByVal xTemplate As String, ByVal xParam As String) As String
'returns the value for the specified configuration parameter
    
    On Error GoTo ProcError
    
    xGetTemplateConfigVal = g_oMPO.GetTemplateSetting(xTemplate, xParam)
    Exit Function
ProcError:
    g_oError.Raise Err.Number, "MacPac90.mdlGlobal.xGetTemplateConfigVal"
    Exit Function
End Function

'***********************************
'   this is coded for InterAction 4+
'***********************************

Public Function xGetClientMatterFilter(oContacts As mpCI.Contacts) As String
    Dim i As Integer
    Dim xSQL As String
    
    For i = 1 To oContacts.Count
        xSQL = xSQL & "(INT_AUX_LST_CUSTOM.LISTING_ID=" & oContacts(i).ListingID & _
               " AND INT_AUX_LST_CUSTOM.LISTING_SRC_ID=" & Right(oContacts(i).StoreID, 1) + 1 & ") OR "
    Next i

    xSQL = "(" & Left(xSQL, Len(xSQL) - 3) & ")"

    xGetClientMatterFilter = xSQL
End Function

Public Function xGetClientMatterFilter2X(oContacts As CIO.CContacts) As String
    Dim i As Integer
    Dim xSQL As String
    
    
    Dim oUNID As CIO.CUNID
    Set oUNID = New CIO.CUNID
    
    For i = 1 To oContacts.Count
        With oContacts(i)
            '---9.6.1 - only IA4 and IA5 Win are supported for CM retrieval
            '---CE = 103
            '---IA5web = 101
            '---MAPI = 100
            '---MP9 = 104
            '---PL = 105
            '---GW = 106
            '---Notes = 110
            '---IA4/5win = 102
            
            If oUNID.GetListing(.UNID).BackendID = 102 Then
                xSQL = xSQL & "(INT_AUX_LST_CUSTOM.LISTING_ID=" & Right(oUNID.GetListing(.UNID).ID, (Len(oUNID.GetListing(.UNID).ID) - InStr(oUNID.GetListing(.UNID).ID, "."))) & _
                       " AND INT_AUX_LST_CUSTOM.LISTING_SRC_ID=" & Left(oUNID.GetListing(.UNID).ID, InStr(oUNID.GetListing(.UNID).ID, ".") - 1) & ") OR "
            End If
        End With
    Next i

    If xSQL <> "" Then
        xSQL = "(" & Left(xSQL, Len(xSQL) - 3) & ")"
    End If
    
    xGetClientMatterFilter2X = xSQL
End Function

'this functions requires a reference to DAO
Public Function UpdatePrivateDB()
'  resets field

    Dim xUserPath As String
    Dim db As DAO.Database
    Dim td As DAO.TableDef
    Dim xSQL As String
    Dim oRS As DAO.Recordset
    
    On Error GoTo bUpdate_ERROR

'   get user path
    xUserPath = mpbase2.UserFilesDirectory

'   get db object
    Set db = DBEngine(0).OpenDatabase(xUserPath & "mpPrivate.mdb")
    
    If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Then    'make sure g_bIsWord12x is set in Application.Initialize before calling this function
    'Retrofit Private db tables
        xSQL = "SELECT tblTemplateDefAuthors.fldTemplate From tblTemplateDefAuthors WHERE tblTemplateDefAuthors.fldTemplate Like """ & "*.dot"" ;"
        
        Set oRS = db.OpenRecordset(xSQL)
        
        On Error Resume Next
        
        With oRS
            If .RecordCount Then
                .MoveLast
                .MoveFirst
    '           fill array with mapping info
                While Not .EOF
                    .Edit
                    .Fields("fldTemplate") = Replace(.Fields("fldTemplate"), ".dot", ".dotx")
                    .Update
                    .MoveNext
                Wend
            End If
        End With
            
        xSQL = "SELECT tblFavoriteTemplates.fldTemplateID From tblFavoriteTemplates WHERE tblFavoriteTemplates.fldTemplateID Like """ & "*.dot"" ;"
        
        Set oRS = db.OpenRecordset(xSQL)
        
        With oRS
            If .RecordCount Then
                .MoveLast
                .MoveFirst
    '           fill array with mapping info
                While Not .EOF
                    .Edit
                    If .Fields("fldTemplateID") = "Normal.dot" Or .Fields("fldTemplateID") = "Bates Labels.dot" Then
                        .Fields("fldTemplateID") = Replace(.Fields("fldTemplateID"), ".dot", ".dotm")
                    Else
                        .Fields("fldTemplateID") = Replace(.Fields("fldTemplateID"), ".dot", ".dotx")
                    End If
                    .Update
                    .MoveNext
                Wend
            End If
        End With
        
        On Error GoTo bUpdate_ERROR
    End If
    
    
''  get tabledef object
'    Set td = db.TableDefs("tblPeopleInput")
'
'    On Error Resume Next
''  append new field - this will error if the field already exists
'    td.Fields.Append td.CreateField("fldIsPartner", dbBoolean)
'
''  get tabledef object
'    Set td = db.TableDefs("tblPreferencesLetter")
'
''  append new field - this will error if the field already exists
'    td.Fields.Append td.CreateField("chkIncludePer", dbBoolean)
'
bUpdate_ERROR:
End Function

Public Function bPromptForSignatureChange(oTemplate As MPO.CTemplate) As Boolean
    Dim xPrompt As String
    
    If UCase(oTemplate.ClassName) Like "CLETTER" Then
        xPrompt = xGetTemplateConfigVal(oTemplate.ID, "SkipSignatureAuthorChangeWarning")
        If UCase(xPrompt) = "FALSE" Then
            If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Then
                MsgBox "Changing the author in the Letterhead does not automatically update the " & _
                   "letter signature.  To update the letter signature, select Letter " & _
                   "Signature from the MacPac Ribbon/Macros.", vbInformation, App.Title
            Else
                MsgBox "Changing the author in the Letterhead does not automatically update the " & _
                   "letter signature.  To update the letter signature, select Letter " & _
                   "Signature from the MacPac Menu.", vbInformation, App.Title
            End If
        End If
    End If

End Function

Public Function bPromptForLetterheadChange(oTemplate As MPO.CTemplate) As Boolean
    Dim xPrompt As String
    
    If UCase(oTemplate.ClassName) Like "CLETTER" Then
        xPrompt = xGetTemplateConfigVal(oTemplate.ID, "SkipLetterheadAuthorChangeWarning")
        If UCase(xPrompt) = "FALSE" Then
            If g_bIsWord12x Or g_bIsWord14x Or g_bIsWord15x Then
                MsgBox "Changing the author in the letter signature does not automatically" & _
                       " update the letterhead.  To update the letterhead, select Letterhead from" & _
                       " the MacPac Ribbon/Macros.", vbInformation, App.Title
            Else
                MsgBox "Changing the author in the letter signature does not automatically" & _
                       " update the letterhead.  To update the letterhead, select Letterhead from" & _
                       " the MacPac Menu.", vbInformation, App.Title
            End If
        End If
    End If

End Function

Public Sub DebugOutput(ByVal OutputString As String)
     
    Call OutputDebugString(OutputString)
     
End Sub

'Fixes issue where the status bar
'is stuck containing inserted
'boilerplate information
Sub ResetStatusBar()
    Word.Application.StatusBar = " "
    SendKeys "{ESC}"
End Sub
