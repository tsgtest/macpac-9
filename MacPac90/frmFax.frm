VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Object = "{FC715925-5921-4AC0-B4BD-36B619407BAE}#1.1#0"; "MPCM.ocx"
Begin VB.Form frmFax 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Fax"
   ClientHeight    =   6468
   ClientLeft      =   3288
   ClientTop       =   1308
   ClientWidth     =   5472
   Icon            =   "frmFax.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6468
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   3315
      Picture         =   "frmFax.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   50
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   6015
      Width           =   705
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4755
      TabIndex        =   52
      Top             =   6015
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   4065
      TabIndex        =   51
      Top             =   6015
      Width           =   650
   End
   Begin C1SizerLibCtl.C1Tab tabPages 
      Height          =   6510
      Left            =   -30
      TabIndex        =   54
      TabStop         =   0   'False
      Top             =   -75
      Width           =   5520
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|E&xtras|&Letterhead"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   415
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   6060
         Left            =   6264
         TabIndex        =   56
         Top             =   15
         Width           =   5508
         Begin VB.TextBox txtCustom3 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1785
            TabIndex        =   49
            Top             =   3990
            Visible         =   0   'False
            Width           =   3585
         End
         Begin VB.CheckBox chkIncludeCPhrasesInHeader 
            Appearance      =   0  'Flat
            Caption         =   "Include &Confidential Phrases in Header"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1875
            TabIndex        =   47
            Top             =   3480
            Width           =   3135
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom3 
            Appearance      =   0  'Flat
            Caption         =   "Custom 3"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   4230
            TabIndex        =   42
            Top             =   1410
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom2 
            Appearance      =   0  'Flat
            Caption         =   "Custom 2"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3045
            TabIndex        =   41
            Top             =   1410
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadCustom1 
            Appearance      =   0  'Flat
            Caption         =   "Custom 1"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1860
            TabIndex        =   40
            Top             =   1410
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadAdmittedIn 
            Appearance      =   0  'Flat
            Caption         =   "Ad&mitted In"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   4230
            TabIndex        =   39
            Top             =   1065
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadTitle 
            Appearance      =   0  'Flat
            Caption         =   "T&itle"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1860
            TabIndex        =   37
            Top             =   1065
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadFax 
            Appearance      =   0  'Flat
            Caption         =   "Fa&x"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3045
            TabIndex        =   38
            Top             =   1065
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadName 
            Appearance      =   0  'Flat
            Caption         =   "&Name"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1860
            TabIndex        =   34
            Top             =   720
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadPhone 
            Appearance      =   0  'Flat
            Caption         =   "P&hone"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   3045
            TabIndex        =   35
            Top             =   720
            Width           =   1155
         End
         Begin VB.CheckBox chkIncludeLetterheadEMail 
            Appearance      =   0  'Flat
            Caption         =   "&E-Mail"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   4230
            TabIndex        =   36
            Top             =   720
            Width           =   1155
         End
         Begin VB.TextBox txtHeaderAdditionalText 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   975
            Left            =   1785
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   46
            Top             =   2280
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbLetterheadType 
            Height          =   360
            Left            =   1800
            OleObjectBlob   =   "frmFax.frx":0CAC
            TabIndex        =   32
            Top             =   240
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbDateType 
            Height          =   555
            Left            =   1785
            OleObjectBlob   =   "frmFax.frx":3355
            TabIndex        =   44
            Top             =   1800
            Width           =   3585
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 3:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   60
            TabIndex        =   48
            Top             =   4050
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblDateType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Date Format:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   255
            TabIndex        =   43
            Top             =   1815
            Width           =   1275
         End
         Begin VB.Label lblType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Letterhead T&ype:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   285
            TabIndex        =   31
            Top             =   285
            Width           =   1275
         End
         Begin VB.Label lblPage2HeaderText 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   "Page &2 Header:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   120
            TabIndex        =   45
            Top             =   2250
            Width           =   1455
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblInclude 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Include Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   285
            TabIndex        =   33
            Top             =   780
            Width           =   1275
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6060
            Left            =   15
            Top             =   -30
            Width           =   1680
         End
         Begin VB.Line Line3 
            BorderColor     =   &H80000005&
            X1              =   -15
            X2              =   5530
            Y1              =   6015
            Y2              =   6015
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   6060
         Left            =   6024
         TabIndex        =   55
         Top             =   15
         Width           =   5508
         Begin VB.TextBox txtCustom2 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1785
            TabIndex        =   30
            Top             =   5520
            Visible         =   0   'False
            Width           =   3570
         End
         Begin VB.TextBox txtMessage 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Height          =   3000
            Left            =   1785
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   19
            Top             =   240
            Width           =   3585
         End
         Begin VB.TextBox txtContactName 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1800
            TabIndex        =   22
            Top             =   3645
            Width           =   3570
         End
         Begin VB.TextBox txtContactPhone 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1785
            TabIndex        =   24
            Top             =   4110
            Width           =   3570
         End
         Begin VB.TextBox txtSenderPhone 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1785
            TabIndex        =   28
            Top             =   5040
            Width           =   3570
         End
         Begin VB.TextBox txtSenderName 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1785
            TabIndex        =   26
            Top             =   4590
            Width           =   3570
         End
         Begin VB.Label lblCustom2 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 2:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   30
            TabIndex        =   29
            Top             =   5580
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblIfProblems 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "If problems:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   180
            TabIndex        =   20
            Top             =   3360
            Width           =   1365
         End
         Begin VB.Label lblContactName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Contact Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   90
            TabIndex        =   21
            Top             =   3675
            Width           =   1455
         End
         Begin VB.Label lblContactPhone 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Contact P&hone:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   75
            TabIndex        =   23
            Top             =   4170
            Width           =   1455
         End
         Begin VB.Label lblMessage 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "M&essage:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   90
            TabIndex        =   18
            Top             =   255
            Width           =   1455
         End
         Begin VB.Label lblSenderPhone 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Sender &Phone:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   90
            TabIndex        =   27
            Top             =   5100
            Width           =   1455
         End
         Begin VB.Label lblSenderName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Sender Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   90
            TabIndex        =   25
            Top             =   4650
            Width           =   1455
         End
         Begin VB.Line Line2 
            BorderColor     =   &H80000005&
            X1              =   -60
            X2              =   5485
            Y1              =   6015
            Y2              =   6015
         End
         Begin VB.Shape Shape3 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6105
            Left            =   -135
            Top             =   -75
            Width           =   1830
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   6060
         Left            =   12
         TabIndex        =   53
         Top             =   15
         Width           =   5508
         Begin TrueDBList60.TDBCombo cmbAuthor 
            Height          =   600
            Left            =   1785
            OleObjectBlob   =   "frmFax.frx":5568
            TabIndex        =   1
            Top             =   240
            Width           =   3615
         End
         Begin mpControls.MultiLineCombo mlcConfidentialPhrase 
            Height          =   735
            Left            =   1800
            TabIndex        =   6
            Top             =   2415
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   1291
            ListRows        =   13
            FontName        =   "MS Sans Serif"
            FontBold        =   0   'False
            FontSize        =   7.8
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.TextBox txtCustom1 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1785
            TabIndex        =   17
            Top             =   5550
            Visible         =   0   'False
            Width           =   3570
         End
         Begin MPCM.ClientMatterComboBox cmbCM 
            Height          =   315
            Left            =   1800
            TabIndex        =   8
            Top             =   3315
            Width           =   3525
            _ExtentX        =   6223
            _ExtentY        =   550
            SourceClass     =   "mpCMIA.CClientMatter"
            Col1Caption     =   "No."
            ColWidth1       =   1020
            ColWidth2       =   1020
            ListRows        =   6
            ComboType       =   2
            SelectTextOnFocus=   -1  'True
            PopulateOnLoad  =   0   'False
            AdditionalClientFilter=   ""
            AdditionalMatterFilter=   ""
         End
         Begin VB.TextBox txtReline 
            Appearance      =   0  'Flat
            Height          =   705
            Left            =   1785
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   10
            Top             =   3765
            Width           =   3570
         End
         Begin VB.TextBox txtPages 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1785
            TabIndex        =   12
            Top             =   4650
            Width           =   1680
         End
         Begin VB.CheckBox chkSeparatePages 
            Appearance      =   0  'Flat
            Caption         =   "Separate Co&ver Page for Each"
            ForeColor       =   &H80000008&
            Height          =   315
            Left            =   1935
            TabIndex        =   4
            Top             =   2010
            Width           =   2520
         End
         Begin TrueDBGrid60.TDBGrid grdRecipients 
            Height          =   1065
            Left            =   1770
            OleObjectBlob   =   "frmFax.frx":94AD
            TabIndex        =   3
            Top             =   900
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbDeliveryComments 
            Height          =   570
            Left            =   1770
            OleObjectBlob   =   "frmFax.frx":CA38
            TabIndex        =   15
            Top             =   5100
            Width           =   3600
         End
         Begin VB.Label lblCPhrases 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Confidential &Phrases:"
            ForeColor       =   &H00FFFFFF&
            Height          =   210
            Left            =   75
            TabIndex        =   5
            Top             =   2415
            Width           =   1530
         End
         Begin VB.Label lblCustom1 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Custom 1:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   60
            TabIndex        =   16
            Top             =   5610
            Visible         =   0   'False
            Width           =   1515
         End
         Begin VB.Label lblReLine 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Re Line:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   525
            TabIndex        =   9
            Top             =   3810
            Width           =   1035
         End
         Begin VB.Label lblRecipPhone 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Phone:"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Left            =   45
            TabIndex        =   58
            Top             =   1665
            Width           =   1515
         End
         Begin VB.Label lblRecipFax 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Fax:"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Left            =   45
            TabIndex        =   59
            Top             =   1425
            Width           =   1515
         End
         Begin VB.Label lblRecipName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   240
            Left            =   45
            TabIndex        =   60
            Top             =   930
            Width           =   1515
         End
         Begin VB.Label lblClientMatter 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "File &Number:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   60
            TabIndex        =   7
            Top             =   3345
            Width           =   1515
         End
         Begin VB.Label lblPages 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Number of &Pages:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   90
            TabIndex        =   11
            Top             =   4695
            Width           =   1515
         End
         Begin VB.Label lblOriginalsWillFollow 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Delivery Comments:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   60
            TabIndex        =   14
            Top             =   5145
            Width           =   1515
         End
         Begin VB.Label lblAuthor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00000040&
            BackStyle       =   0  'Transparent
            Caption         =   " &Author:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   225
            TabIndex        =   0
            Top             =   300
            Width           =   1335
         End
         Begin VB.Label lblNumRecips 
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Re&cipient 1 of 1"
            ForeColor       =   &H00000000&
            Height          =   255
            Left            =   1800
            TabIndex        =   2
            Top             =   660
            Width           =   1455
         End
         Begin VB.Label lblRecipCompany 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Company:"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   45
            TabIndex        =   57
            Top             =   1170
            Width           =   1515
         End
         Begin VB.Label lblPagesIncluding 
            Caption         =   "(including this page)"
            Height          =   285
            Left            =   3630
            TabIndex        =   13
            Top             =   4725
            Width           =   1695
         End
         Begin VB.Line Line1 
            BorderColor     =   &H80000005&
            X1              =   0
            X2              =   5520
            Y1              =   6015
            Y2              =   6015
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6135
            Left            =   -135
            Top             =   -90
            Width           =   1830
         End
      End
   End
   Begin VB.Menu mnuRecipients 
      Caption         =   "Recipients"
      Visible         =   0   'False
      Begin VB.Menu mnuRecipients_Delete 
         Caption         =   "&Delete Recipient"
      End
      Begin VB.Menu mnuRecipients_DeleteAll 
         Caption         =   "De&lete All Recipients"
      End
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_EditAuthorOptions 
         Caption         =   "&Edit Author Preferences..."
      End
      Begin VB.Menu mnuAuthor_CopyOptions 
         Caption         =   "Cop&y Author Preferences..."
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "&Copy Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
   End
End
Attribute VB_Name = "frmFax"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const mpRecipientFields As String = "Name|Company|Phone|Fax"

Private WithEvents m_oFaxCover As MPO.CFax
Attribute m_oFaxCover.VB_VarHelpID = -1
Private m_PersonsForm As MPO.CPersonsForm
Private m_oMenu As MacPac90.CAuthorMenu

Private m_bAuthorDirty As Boolean
Private b_Opened As Boolean
Private m_bCancelled As Boolean
Private m_bAuthorManual As Boolean
Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private m_ReuseAction As String
Private m_bForcedUpdate As Boolean
Private m_bGridIsDirty As Boolean
Private m_bNewRecord As Boolean
Private m_bItemChanged As Boolean
Private m_bUpdateDefaults As Boolean

Private WithEvents m_oOptForm As MPO.COptionsForm
Attribute m_oOptForm.VB_VarHelpID = -1

Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub

Private Sub chkIncludeDPhrases2InHeader_Click()

End Sub

Private Sub chkIncludeCPhrasesInHeader_Click() '9.7.1 #4157
    On Error GoTo ProcError
    If Me.CascadeUpdates Then
        Application.ScreenUpdating = False
        Me.FaxCover.IncludeDPhrases2InHeader = _
            0 - Me.chkIncludeCPhrasesInHeader
            Application.ScreenUpdating = True
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub chkIncludeCPhrasesInHeader_GotFocus() '9.7.1 #4157
    OnControlGotFocus Me.chkIncludeCPhrasesInHeader, "zzmpHeaderConfidentialPhrases"
End Sub

Private Sub cmbAuthor_Change()
    AuthorIsDirty = True
End Sub

Private Sub cmbAuthor_Close()
    b_Opened = False
End Sub

Private Sub cmbAuthor_Click()
    If b_Opened Then
        AuthorIsDirty = False
    End If
End Sub

Private Sub cmbAuthor_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = wdKeyF10 And Shift = 1 Then
        ShowAuthorMenu
    End If
End Sub

Private Sub cmbAuthor_LostFocus()
    Dim bCascade As Boolean

    If m_bItemChanged Then
        With Me
'           update values in form
            bCascade = .CascadeUpdates
            .CascadeUpdates = False
        
'           update object with values of selected person
            If (Not .Initializing) Then
                .FaxCover.UpdateForAuthor
            End If
        
'           show options in form
            .ShowOptions .FaxCover.Author.ID
            
            bResetLetterheadCombo .cmbLetterheadType, _
                                  .FaxCover.Letterhead
                        
            Me.CascadeUpdates = bCascade
        End With
        
        m_bItemChanged = False
        
    End If
    
End Sub

Private Sub cmbAuthor_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAuthor, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAuthor_Open()
    b_Opened = True
End Sub

Private Sub cmbAuthor_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If m_bAuthorDirty Then
        Cancel = Not bValidateBoundTDBCombo(Me.cmbAuthor)
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCM_ConnectionFailed(ByVal Message As String)

    MsgBox IIf(Message = "", "Could not connect to the client matter data source.  ", Message & vbCr & vbCr) _
            & "Instead of selecting client matter numbers from a list," & _
            " you can manually type the numbers instead.", _
            vbExclamation, App.Title
        
    With Me.cmbCM
        .RunConnected = False
        .ComboType = ComboType_TextOnly
    End With
    
End Sub

Private Sub cmbCM_GotFocus()
    OnControlGotFocus Me.cmbCM, "zzmpFaxCover_ClientMatterNumber"
End Sub

Private Sub cmbCM_LostFocus()
    On Error GoTo ProcError
    Application.ScreenUpdating = False '#4158
    With Me.FaxCover
        .FileNumber = Me.cmbCM.value
        If .FileNumber = Empty Then
            .ClientNumber = ""
            .ClientName = ""
            .MatterNumber = ""
            .MatterName = ""
        ElseIf cmbCM.RunConnected Then  '#4457
            If cmbCM.Validity = cmValidationStatus_Valid Then  '#4158
                .ClientNumber = Me.cmbCM.ClientData()(cmClientField_Number)
                .ClientName = Me.cmbCM.ClientData()(cmClientField_Name)
                .MatterNumber = Me.cmbCM.MatterData()(cmMatterField_Number)
                .MatterName = Me.cmbCM.MatterData()(cmMatterField_Name)
            End If
        ElseIf g_xCM_Separator <> "" Then
            ' Parse out Client and Matter Numbers using separator
            Dim i As Integer
            i = InStr(cmbCM.value, g_xCM_Separator)
            If i > 0 Then
                .ClientNumber = Left(cmbCM.value, i - 1)
                .MatterNumber = Mid(cmbCM.value, i + Len(g_xCM_Separator))
                .ClientName = ""
                .MatterName = ""
            Else
                .ClientNumber = ""
                .ClientName = ""
                .MatterNumber = ""
                .MatterName = ""
            End If
        Else
            .ClientNumber = ""
            .ClientName = ""
            .MatterNumber = ""
            .MatterName = ""
        End If
    End With ' end #4158
    'save SQL for reuse
    FaxCover.Document.SaveItem "CMFilterString", _
        "FaxCover", , Me.cmbCM.AdditionalClientFilter
    Application.ScreenUpdating = True '#4158
    Application.ScreenRefresh '#4158
    Exit Sub
ProcError:
    g_oError.Show Err
    Application.ScreenUpdating = True '#4158
    Exit Sub
End Sub

Private Sub cmbCM_Validate(Cancel As Boolean)
    Dim iIsValid As cmValidationStatus
    
    If g_bCM_AllowValidation Then
        If Me.cmbCM.RunConnected And Me.cmbCM.value <> Empty Then
            
            iIsValid = Me.cmbCM.Validity
            
            If iIsValid = cmValidationStatus_Invalid Then
                MsgBox "Invalid client matter number."
                Cancel = True
            End If
        End If
    End If
End Sub

Private Sub cmbDateType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDateType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbDateType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_GotFocus()
    OnControlGotFocus Me.cmbLetterheadType
End Sub

Private Sub cmbLetterheadType_ItemChange()
    On Error GoTo ProcError
'*  enable/disable author details per LH type def
    With g_oMPO.db.LetterheadDefs.Item(Me.cmbLetterheadType.BoundText)
        Me.chkIncludeLetterheadEMail.Enabled = .AllowAuthorEMail
        Me.chkIncludeLetterheadName.Enabled = .AllowAuthorName
        Me.chkIncludeLetterheadPhone.Enabled = .AllowAuthorPhone
        Me.chkIncludeLetterheadTitle.Enabled = .AllowAuthorTitle
        Me.chkIncludeLetterheadFax.Enabled = .AllowAuthorFax
        Me.chkIncludeLetterheadAdmittedIn.Enabled = .AllowAuthorAdmittedIn
        '9.7.1 #4028
        Me.chkIncludeLetterheadCustom1.Enabled = .AllowCustomDetail1
        Me.chkIncludeLetterheadCustom2.Enabled = .AllowCustomDetail2
        Me.chkIncludeLetterheadCustom3.Enabled = .AllowCustomDetail3
        Me.lblInclude.Enabled = .AllowAuthorEMail Or .AllowAuthorName Or _
                                .AllowAuthorPhone Or .AllowAuthorFax Or _
                                .AllowAuthorTitle Or .AllowAuthorAdmittedIn Or _
                                .AllowCustomDetail1 Or .AllowCustomDetail2 Or _
                                .AllowCustomDetail3
        '---
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_LostFocus()
    Dim lType As Long
    Dim bRefresh As Boolean
    
    On Error GoTo ProcError
    With FaxCover.Letterhead
'       get selected letterhead type
        lType = Me.cmbLetterheadType.BoundText
        
'       compare with previous letterhead type
        If .LetterheadType <> lType Then
            Word.Application.ScreenUpdating = False
            
'           type is different - set
'           type to selected type
            .LetterheadType = lType

'           if dynamic editing is on, refresh
'           the letterhead
            If g_oMPO.DynamicEditing Then
                .Refresh
            End If
            Word.Application.ScreenUpdating = True
        End If
    End With
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLetterheadType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLetterheadType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLetterheadType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If IsPressed(vbKeyF2) Then
        btnAddressBooks_Click
    Else
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdRecipients_AfterUpdate()
    Dim iRow As Integer
    Dim iCol As Integer
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    
    If m_bGridIsDirty Then
        With grdRecipients
'            DoEvents   '#3465
            If m_bNewRecord Then
                iRow = .Array.UpperBound(1) + 1
            Else
                iRow = .Bookmark + 1
            End If
            If Me.Recipients.Count < iRow Then Me.Recipients.Add
            For iCol = 0 To .Columns.Count - 1
                Me.Recipients(iRow).Item(iCol + 1) = .Array.value(iRow - 1, iCol) & ""
            Next iCol
'            DoEvents   '#3465
            If m_bNewRecord Then
                .Bookmark = iRow - 1
                m_bNewRecord = False
            End If
        End With
        If Not GridCurrentRowIsEmpty(grdRecipients) And _
        (Me.chkSeparatePages = 0 Or iRow = 1) Then
            Application.ScreenUpdating = False
            Me.FaxCover.RefreshRecipients iRow, True
            Application.ScreenUpdating = True
        End If
        m_bGridIsDirty = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdRecipients_BeforeUpdate(Cancel As Integer)
    On Error GoTo ProcError
    If grdRecipients.AddNewMode = 2 Then
        m_bNewRecord = True
    Else
        m_bNewRecord = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdRecipients_Change()
    On Error GoTo ProcError
    If m_bForcedUpdate Or Me.Initializing Then Exit Sub
    m_bGridIsDirty = True
    On Error Resume Next
    With grdRecipients
        ' Check if current record is AddNew Row
        If .AddNewMode = 2 Then
            ShowNumRecipients
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdRecipients_DblClick()
    btnAddressBooks_Click
End Sub

Private Sub grdRecipients_FirstRowChange(ByVal SplitIndex As Integer)
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub

    Dim rngRecip As Word.Range
    Dim iStartRow As Integer

    With grdRecipients
        If .AddNewMode > 0 Then
            .MousePointer = 3
        End If
        If Not m_bForcedUpdate Then _
            .Col = 0
        
        .Row = 0
        If Me.chkSeparatePages = 1 Then
            iStartRow = 1
        Else
            If .AddNewMode > 0 Then
                iStartRow = Max(Me.Recipients.Count, 1)
            ElseIf Not IsNull(.Bookmark) Then
                iStartRow = .Bookmark + 1
            Else
                iStartRow = Max(Me.Recipients.Count, 1)
            End If
        End If
        On Error GoTo ProcError
        Set rngRecip = ActiveDocument.Bookmarks("zzmpFaxCover_Recipients_" & iStartRow).Range
        rngRecip.Select
        Application.ScreenUpdating = True
        g_oMPO.ScreenUpdating = True
    End With
    ShowNumRecipients
    Exit Sub
ProcError:
    ShowNumRecipients
End Sub

Private Sub grdRecipients_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    
    Select Case KeyCode
        Case vbKeyUp, vbKeyPageUp:
            KeyCode = 0
            With Me.grdRecipients
                If .Col = 0 Then
                    If Me.Recipients.Count = 0 Then Exit Sub
                    If .Bookmark <> 0 Or .AddNewMode > 0 Then
                        If .AddNewMode > 0 And GridCurrentRowIsEmpty(grdRecipients) Then
                            m_bForcedUpdate = True
                            .Bookmark = Me.Recipients.Count - 1
                        Else
                            If .AddNewMode > 0 Or m_bGridIsDirty Then _
                                .Update
                            m_bForcedUpdate = True
                            .MovePrevious
                            grdRecipients_FirstRowChange 0
                        End If
                        DoEvents
                        .Col = 3
                        m_bForcedUpdate = False
                        ShowNumRecipients
                    End If
                Else
                    .Col = .Col - 1
                End If
            End With
        Case vbKeyDown, vbKeyReturn, vbKeyPageDown:
            KeyCode = 0
            With Me.grdRecipients
                If .Col = .Columns.Count - 1 Then
                    If .AddNewMode > 0 And GridCurrentRowIsEmpty( _
                        grdRecipients) Then _
                        Exit Sub
                    m_bForcedUpdate = True
                    If .AddNewMode = 0 Then
                        If m_bGridIsDirty Then .Update
                        DoEvents
                        .Scroll 0, 1
                        LockWindowUpdate Me.grdRecipients.hwnd
                        .Col = 0
                        LockWindowUpdate &O0
                        DoEvents
                    Else
                        LockWindowUpdate Me.grdRecipients.hwnd
                        .Update
                        .Col = 0
                        LockWindowUpdate &O0
                        DoEvents
                        .Scroll 0, 1
                    End If
                    m_bForcedUpdate = False
                    ShowNumRecipients
                Else
                    LockWindowUpdate Me.grdRecipients.hwnd
                   .Col = .Col + 1
                    LockWindowUpdate &O0
                End If
            End With
        Case vbKeyLeft
'           remove any selections and move char by char
            If Me.grdRecipients.SelLength Then
                KeyCode = vbKeyRight
                Me.grdRecipients.SelLength = 0
            End If
        Case vbKeyRight
'           remove any selections and move char by char
            Me.grdRecipients.SelLength = 0
    End Select
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub grdRecipients_Scroll(Cancel As Integer)
    On Error GoTo ProcError
    With grdRecipients
        If .AddNewMode = 2 Then
            .Update
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdRecipients_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If Me.Initializing Then Exit Sub
    
    On Error Resume Next
    With grdRecipients
        If m_bGridIsDirty And .AddNewMode > 0 Then
            m_bNewRecord = True
            .Update
        ElseIf m_bGridIsDirty Then
            .Update
        End If
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oFaxCover_RecipientsChanged(rngRecip As Word.Range, iIndex As Integer)
    On Error GoTo ProcError
    ' Add code here to setup bookmarks for new recipients when not using
    ' the standard table or sequential paragraphs formats
    '
    ' Each recipient with index 'x' should have a bookmark named
    ' "zzmpFaxCover_Recipients_x" which includes a text form field
    ' for each item in mpRecipientFields
    
    Me.FaxCover.SetupBookmarks rngRecip, iIndex
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub m_oOptForm_CurrentDocAuthorOptionsChange()
    On Error GoTo ProcError
'   the options of the author of the current fax
'   have changed - update dlg and doc to reflect changes
    g_oMPO.ForceItemUpdate = True
    With Me.FaxCover
        .Author.Options(.Template.ID).Refresh
    End With
    cmbAuthor_ItemChange
    g_oMPO.ScreenUpdating = True
    g_oMPO.ForceItemUpdate = False

    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub m_oOptForm_DefaultOptionsChange()
    On Error GoTo ProcError
'   options of the default author have changed -
'   write these changes to the template
    With Me.FaxCover
        .Author.Options(.Template.ID).Refresh
        'GLOG : 5598 : ceh
        If g_bUpdatePreferencesInDialog Then
             .Template.UpdateDefaults .DefaultAuthor.ID, Me.FaxCover
             m_bUpdateDefaults = False
        Else
             m_bUpdateDefaults = True
        End If
    End With
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mlcConfidentialPhrase_GotFocus() '9.7.1 #4157, 9.7.1020 #4560
    OnControlGotFocus Me.mlcConfidentialPhrase, "zzmpDeliveryPhrases2"
End Sub

Private Sub mlcConfidentialPhrase_LostFocus() '9.7.1 - #4157
    On Error GoTo ProcError
    FaxCover.DeliveryPhrases2 = Me.mlcConfidentialPhrase.Text
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Copy_Click()
    Dim oSource As mpDB.CPerson
    Dim oCopy As mpDB.CPerson
    
    On Error GoTo ProcError
'   if author isn't in list, the
'   author is the reuse author
    'If Me.cmbAuthor.Row = -1 Then
    If TypeOf Me.FaxCover.Author Is CReUseAuthor Then '***4337
'       use reuse author as source
        Set oSource = Me.FaxCover.Document.ReuseAuthor
    Else
'       use selected author in list as source
        Set oSource = g_oMPO.People(Me.cmbAuthor.BoundText)
    End If
    
'   copy the person
    Set oCopy = m_PersonsForm.CopyPerson(oSource)
    
'   refresh the list
    If Not (oCopy Is Nothing) Then
        RefreshPeople oCopy.ID, Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_CopyOptions_Click()
    On Error GoTo ProcError
    m_oOptForm.CopyOptions Me.FaxCover.Author, Me.FaxCover.Template.ID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub


'***********************************************************
'popup menu items
'***********************************************************
Private Sub mnuAuthor_EditAuthorOptions_Click()
    On Error GoTo ProcError
    DoEvents
    m_oOptForm.Show Me.FaxCover
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_ManageLists_Click()
'   get current selection
    
    Dim lID As Long
    
    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    On Error GoTo ProcError
    
    If lID Then
        m_oMenu.ManageAuthors Me.cmbAuthor, lID
    Else
        m_oMenu.ManageAuthors Me.cmbAuthor
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_AddNew_Click()
    Dim lID As Long
    On Error GoTo ProcError
    lID = Me.cmbAuthor.Columns("fldID")
    m_oMenu.AddPerson Me.cmbAuthor, lID
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_Favorite_Click()
    Dim lID As Long

    On Error Resume Next
    lID = Me.cmbAuthor.BoundText
    On Error GoTo ProcError
    
    If lID Then
        m_oMenu.ToggleFavorite Me.cmbAuthor, lID
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    On Error GoTo ProcError
    With FaxCover.Template
        .DefaultAuthor = Me.FaxCover.Author
        'GLOG : 5598 : ceh
        If g_bUpdatePreferencesInDialog Then
            .UpdateDefaults .DefaultAuthor.ID, Me.FaxCover
             m_bUpdateDefaults = False
        Else
             m_bUpdateDefaults = True
        End If
    End With
        
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub mnuRecipients_Delete_Click()
    Dim oArray As XArray
    Dim i As Integer
    
    On Error GoTo ProcError
    With grdRecipients
        If (GridCurrentRowIsEmpty(grdRecipients) And .Array.UpperBound(1) < 1) Or _
            IsNull(.Bookmark) Then Exit Sub
        i = .Bookmark + 1
        If i <= Me.Recipients.Count Then
            Me.Recipients.Remove i
            Application.ScreenUpdating = False
            g_oMPO.ScreenUpdating = False
            Me.FaxCover.RefreshRecipients i
        End If
        .Delete
        ' Reset array to Recipients array
        If Me.Recipients.Count > 0 Then
            Set .Array = Me.Recipients.ListArray
        Else
            .Array.ReDim 0, -1, 0, .Columns.Count - 1
        End If
        .Rebind
        If i - 1 < .Array.UpperBound(1) Then
            .Bookmark = i - 1
        Else
            .Bookmark = .Array.UpperBound(1)
        End If
        .Col = 0
    End With
    ShowNumRecipients
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub mnuRecipients_DeleteAll_Click()
    Dim i As Integer
    Dim iCount As Integer
    Dim xTemp As XArray
    
    On Error GoTo ProcError
    Set xTemp = g_oMPO.NewXArray
    iCount = m_oFaxCover.Recipients.Count
    
    If iCount = 0 Then
        Exit Sub
    End If
    

    With grdRecipients
        .Array.ReDim 0, -1, 0, Me.Recipients.FieldCount - 1
        .Rebind
    End With
    g_oMPO.ScreenUpdating = False
    Me.Recipients.RemoveAll
    g_oMPO.ScreenUpdating = True
    Application.ScreenRefresh
    ShowNumRecipients
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
'****************************************************************
'Properties
'****************************************************************
Public Property Get FaxCover() As MPO.CFax
    Set FaxCover = m_oFaxCover
End Property
Public Property Let FaxCover(faxcNew As MPO.CFax)
    Set m_oFaxCover = faxcNew
End Property
Public Property Let Recipients(oNew As MPO.CRecipients)
    On Error GoTo ProcError
    Set m_oFaxCover.Recipients = oNew
    Set Me.grdRecipients.Array = m_oFaxCover.Recipients.ListArray
    grdRecipients.Rebind
    Exit Property
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmFax.Recipients"), _
              g_oError.Desc(Err.Number)
    Exit Property
End Property
Public Property Get Recipients() As MPO.CRecipients
    Set Recipients = m_oFaxCover.Recipients
End Property
Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property
Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property
'9.7.1 - #4157
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
    Me.CascadeUpdates = Not bNew
End Property
'end of
Public Property Let AuthorIsDirty(bNew As Boolean)
    m_bAuthorDirty = bNew
End Property
Public Property Get AuthorIsDirty() As Boolean
    AuthorIsDirty = m_bAuthorDirty
End Property
Public Property Let AuthorIsManual(bNew As Boolean)
    m_bAuthorManual = bNew
End Property
Public Property Get AuthorIsManual() As Boolean
    AuthorIsManual = m_bAuthorManual
End Property
Private Sub btnAddressBooks_Click()
    GetContacts
End Sub
Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Hide
    Me.Cancelled = True
    DoEvents
    Application.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Me.Hide
    Me.Cancelled = False
    DoEvents
    Application.ScreenRefresh
    'GLOG : 5598 : ceh
    If m_bUpdateDefaults Then
        With Me.FaxCover.Template
            .UpdateDefaults .DefaultAuthor.ID, Me.FaxCover
            m_bUpdateDefaults = False
        End With
    End If
    Exit Sub


ProcError:
    EchoOn
    MsgBox Err.Number & "::" & Err.Description
    Word.Application.ScreenUpdating = True
End Sub
Private Sub chkSeparatePages_Click()
    On Error GoTo ProcError
    g_oMPO.ScreenUpdating = False
    Me.FaxCover.CoverPerRecipient = 0 - chkSeparatePages
    UpdateHeaderText
    g_oMPO.ScreenUpdating = True
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkSeparatePages_GotFocus()
    OnControlGotFocus Me.chkSeparatePages
End Sub

Private Sub cmbAuthor_GotFocus()
    OnControlGotFocus Me.cmbAuthor, "zzmpFaxCover_AuthorName"
End Sub

Private Sub cmbAuthor_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        If Not Me.cmbAuthor.IsOpen Then
            ShowAuthorMenu
        End If
    End If
End Sub
Private Sub cmbDateType_GotFocus()
    OnControlGotFocus Me.cmbDateType, "zzmpFaxCover_DateType"
End Sub
Private Sub cmbDateType_LostFocus()
    On Error GoTo ProcError
    FaxCover.DateType = cmbDateType.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbOffice_ItemChange()
    On Error GoTo ProcError
    'UnderConstruction
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub Form_Activate()
    Dim lID As Long
    Dim iSource As Integer
    Dim datStart As Date
    Dim oDefAuthor As mpDB.CPerson
    Dim oVar As Word.Variable
    Dim xMessage As String
    Dim oAutoText As Word.AutoTextEntry
    Dim xVarValue As String
    
    
    On Error GoTo ProcError
'   run only during initialization - unfortunately , the tdbl
'   will initialize only in Form_Activate.
    If Me.Initializing Then
        SetInterfaceOptions Me, Me.FaxCover.Template.ID '9.7.1 - #4160
        PositionControls '9.7.1 - #4160
        Me.tabPages = 0
        DoEvents
        With Me.FaxCover
            If .Document.IsCreated Then
                UpdateForm
            Else
'               set author to default author
                Me.cmbAuthor.BoundText = .Author.ID
                
                DoEvents
                
'               show options for author in dlg
                Me.ShowOptions .Author.ID
                
                bResetLetterheadCombo Me.cmbLetterheadType, _
                                      .Letterhead
                
                Me.AuthorIsDirty = False
                
                
                If .Document.IsSegment Then
                    .Recipients.RemoveAll
                End If
                
            End If
            SendKeys "+", True
        End With

'       fill message control with 'attach to' or 'reuse' text, if it exists
        xMessage = Me.FaxCover.GetMessageText
        If xMessage <> Empty Then
            Me.txtMessage = xMessage
        End If
        
        ShowNumRecipients
        
        Me.Initializing = False
    End If
    
    g_oMPO.ForceItemUpdate = False
    
    ActiveDocument.ActiveWindow.View.ShowHiddenText = False
    On Error Resume Next
    ActiveWindow.PageScroll , 1
    EchoOn
    Word.Application.ScreenUpdating = True
    
    Me.cmbAuthor.SetFocus
    Screen.MousePointer = vbDefault
    Application.ScreenRefresh
'   move dialog to last position
    'MoveToLastPosition Me
    Exit Sub
    
ProcError:
    ActiveDocument.ActiveWindow.View.ShowHiddenText = False
    g_oMPO.ForceItemUpdate = False
    EchoOn
    Word.Application.ScreenUpdating = True
    Screen.MousePointer = vbDefault
    Me.Initializing = False
    g_oError.Show Err, "Error Activating Fax Form"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim bShow As Boolean
    Dim oList As mpDB.CList
    
    On Error GoTo ProcError
    
    g_oMPO.ScreenUpdating = False
    
    Me.Initializing = True
    
    Me.Top = 20000
    
    Me.Cancelled = True
    
    Set m_PersonsForm = New MPO.CPersonsForm
    Set m_oOptForm = New MPO.COptionsForm
    Set m_oMenu = New MacPac90.CAuthorMenu
    
    If Me.FaxCover Is Nothing Then
        Me.FaxCover = g_oMPO.NewFax
    End If
    
    FaxCover.Document.StatusBar = "Initializing.  Please wait..."
    
    If FaxCover.Document.File.ProtectionType <> wdNoProtection Then
           FaxCover.Document.File.Unprotect
    End If
    
    With Me.FaxCover
        Me.cmbLetterheadType.Array = g_oMPO.db.LetterheadDefs( _
                                     .Template.ID).ListSource
    End With
    
'*  get lists
    With g_oMPO.Lists
        Me.cmbDateType.Array = _
            .Item("DateFormats").ListItems.Source
             
        Me.cmbDeliveryComments.Array = _
            .Item("DeliveryComments").ListItems.Source
            
        Me.mlcConfidentialPhrase.List = _
            .Item("DPhrases2").ListItems.Source  '9.7.1 - #4157

    End With
    
    ResizeTDBCombo Me.cmbDeliveryComments, 5
    ResizeTDBCombo Me.cmbLetterheadType, 6
    ResizeTDBCombo Me.cmbDateType, 4
    
    g_oMPO.People.Refresh
    
    With Me.cmbAuthor
        .Array = g_oMPO.People.ListSource
        .Rebind
        ResizeTDBCombo Me.cmbAuthor, 10
        DoEvents
    End With
    
    m_bForcedUpdate = True
    
    With grdRecipients
        Dim xarTemp As XArray
        Set xarTemp = g_oMPO.NewXArray
        xarTemp.ReDim 0, -1, 0, Me.Recipients.FieldCount - 1
        .Array = xarTemp
        .Rebind
    End With
    
'   do sticky fields
    With Me.FaxCover
'       assign to dialog
        Me.txtSenderName = .SenderName
        Me.txtSenderPhone = .SenderPhone
        
'       write to doc
        g_oMPO.ForceItemUpdate = True
        .SenderName = .SenderName
        .SenderPhone = .SenderPhone
        g_oMPO.ForceItemUpdate = False
    End With
    
'   show page 2 header controls?
    bShow = Empty
    Dim xTemplate As String
    With g_oMPO.db.TemplateDefinitions(Me.FaxCover.Template.ID)
'       get Template Name
        xTemplate = .FileName
        
'       set caption to short name
        Me.Caption = "Create a " & .ShortName
    End With
    Dim xVal As String
    xVal = xGetTemplateConfigVal(xTemplate, "ShowDlgPage2Header")
    If Len(xVal) Then
        bShow = CBool(xVal)
    Else
        Err.Raise mpError_InvalidTemplateConfiguration, , "No value has been specified " & _
            "for the macpac.ini key " & xTemplate & "\ShowDlgPage2Header. Either " & _
            "add this key or specify a base template for " & xTemplate & _
            " in mpPublic.tblTemplates."
    End If
    
    Me.lblPage2HeaderText.Visible = bShow
    Me.txtHeaderAdditionalText.Visible = bShow
    
'   show sender controls
    bShow = Empty
    
    xVal = xGetTemplateConfigVal(xTemplate, "ShowDlgSenderControls")
    If Len(xVal) Then
        bShow = CBool(xVal)
    Else
        Err.Raise mpError_InvalidTemplateConfiguration, , "No value has been specified " & _
            "for the macpac.ini key " & xTemplate & "\ShowDlgSenderControls. Either " & _
            "add this key or specify a base template for " & xTemplate & _
            " in mpPublic.tblTemplates."
    End If
    
    Me.lblSenderName.Visible = bShow
    Me.lblSenderPhone.Visible = bShow
    Me.txtSenderName.Visible = bShow
    Me.txtSenderPhone.Visible = bShow
    
    '9.7.1 #4157
    bShow = bIniToBoolean(xGetTemplateConfigVal(Me.FaxCover.Template.ID, _
        "AllowCPhrases"))
    Me.lblCPhrases.Visible = bShow
    Me.mlcConfidentialPhrase.Visible = bShow
    Me.chkIncludeCPhrasesInHeader.Visible = bShow

'   update client\matter label
    xVal = xGetTemplateConfigVal(xTemplate, "ClientMatterDBoxCaption")
    Me.lblClientMatter.Caption = xVal

'   set Client Matter Control properties
    With Me.cmbCM
        .RunConnectedToolTip = g_xCM_RunConnectedTooltip
        .RunDisconnectedToolTip = g_xCM_RunDisconnectedTooltip
        .RunConnected = g_bCM_RunConnected
        If Not .RunConnected Then
            .ComboType = ComboType_TextOnly
        Else
            .SourceClass = g_xCM_Backend
            If g_bCM_SearchByMatter Then .SearchMode = 1 '***9.7.1 - #4186
            .AllowUserToClearFilters = g_bCM_AllowUserToClearFilters
            .ShowLoadMessage = g_bCM_ShowLoadMsg
            .Column1Heading = g_xCM_Column1Heading
            .Column2Heading = g_xCM_Column2Heading
            .Separator = g_xCM_Separator
        End If
    End With
    '9.7.1 #4028
    If g_xLHCustom1Caption <> "" Then
        Me.chkIncludeLetterheadCustom1.Caption = g_xLHCustom1Caption
        Me.chkIncludeLetterheadCustom1.Visible = True
    Else
        Me.chkIncludeLetterheadCustom1.Visible = False
    End If
    If g_xLHCustom2Caption <> "" Then
        Me.chkIncludeLetterheadCustom2.Caption = g_xLHCustom2Caption
        Me.chkIncludeLetterheadCustom2.Visible = True
    Else
        Me.chkIncludeLetterheadCustom2.Visible = False
    End If
    If g_xLHCustom3Caption <> "" Then
        Me.chkIncludeLetterheadCustom3.Caption = g_xLHCustom3Caption
        Me.chkIncludeLetterheadCustom3.Visible = True
    Else
        Me.chkIncludeLetterheadCustom3.Visible = False
    End If
    '---

'   ensure date update
    g_oMPO.ForceItemUpdate = True
    Me.FaxCover.DateType = Me.FaxCover.DateType
    g_oMPO.ForceItemUpdate = False
    
    m_bForcedUpdate = False

'   show AddressBooks button
    Me.btnAddressBooks.Visible = g_bShowAddressBooks
    
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    Err.Source = "MacPac90.frmFax.Form_Load"
    g_oError.Show Err, "Error Loading Fax Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'SetLastPosition Me
    
    Set m_PersonsForm = Nothing
    Set m_oFaxCover = Nothing
    Set m_oOptForm = Nothing
    Set m_oMenu = Nothing
    Set g_oPrevControl = Nothing
    Exit Sub
ProcError:
    g_oError.Show Err, "Error Unloading Form"
    Exit Sub
End Sub

Private Sub cmbAuthor_ItemChange()
    Dim lID As Long
    
    On Error GoTo ProcError
    If Me.Initializing Then
        Exit Sub
    End If
    
    Me.AuthorIsDirty = True
    
    g_oMPO.ScreenUpdating = False
    
    With FaxCover
'       set new author
        lID = Me.cmbAuthor.BoundText
        .Author = g_oMPO.People(lID)
    End With
    
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    
    m_bItemChanged = True
    Me.AuthorIsDirty = False
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub ShowNumRecipients()
    Dim i As Integer
    On Error Resume Next
    On Error GoTo ProcError
    i = Me.grdRecipients.Array.UpperBound(1)
    
    If grdRecipients.AddNewMode = 2 Then _
        i = i + 1
    
    If i < 1 Then
        Me.lblNumRecips.Caption = "Re&cipient 1 of 1"
    Else
        Me.lblNumRecips.Caption = "Re&cipient " & IIf(grdRecipients.AddNewMode > 0, i + 1, Me.grdRecipients.Bookmark + 1) & " of " & (i + 1)
    End If
    Me.chkSeparatePages.Enabled = i > 0
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmFax.ShowNumRecipients"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub
Private Sub grdRecipients_GotFocus()
    On Error GoTo ProcError
    If Me.Initializing Or m_bForcedUpdate Then Exit Sub
    OnControlGotFocus Me.grdRecipients
    With grdRecipients
        m_bGridIsDirty = False
        grdRecipients_FirstRowChange 0
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub grdRecipients_LostFocus()
    On Error GoTo ProcError
    If Me.chkSeparatePages = vbUnchecked Then
        UpdateHeaderText
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdRecipients_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error GoTo ProcError
    m_bForcedUpdate = True
    If Button = 2 Then
        Me.PopupMenu mnuRecipients
    ElseIf Button = 1 Then
'        grdRecipients.SetFocus
        If GridCurrentRowIsEmpty(grdRecipients) Then _
            grdRecipients.Col = 0
    End If
    m_bForcedUpdate = False
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub cmbDeliveryComments_GotFocus()
    OnControlGotFocus Me.cmbDeliveryComments, "zzmpFaxCover_DeliveryComments"
End Sub
Private Sub cmbDeliveryComments_LostFocus()
    On Error GoTo ProcError
    If Me.cmbDeliveryComments.Text = "" Then
        m_oFaxCover.DeliveryComments = ""
    Else
        m_oFaxCover.DeliveryComments = Me.cmbDeliveryComments.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub tabPages_Click()
    On Error GoTo ProcError
     Select Case Me.tabPages.CurrTab
        Case 0
            Me.cmbAuthor.SetFocus
        Case 1
            SelectFirstAvailableControl Me, Me.txtMessage.TabIndex, Me.txtSenderPhone.TabIndex '9.7.1 - #4160
        Case 2
            SelectFirstAvailableControl Me, Me.cmbLetterheadType.TabIndex, Me.txtHeaderAdditionalText.TabIndex '9.7.1 - #4160
    End Select
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtContactName_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.ContactName = txtContactName
    On Error Resume Next
    If Not Me.ActiveControl.Name = "txtContactPhone" Then
        With FaxCover.Document
            If .Selection.StoryType <> wdMainTextStory Then
                If .File.ActiveWindow.View.Type = wdNormalView Then
                    .File.ActiveWindow.ActivePane.Close
                Else
                    .File.ActiveWindow.View.SeekView = wdSeekMainDocument
                End If
            End If
        End With
        DoEvents
        Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtContactPhone_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.ContactPhone = txtContactPhone
    On Error Resume Next
    If Not Me.ActiveControl.Name = "txtContactName" Then
        With FaxCover.Document
            If .Selection.StoryType <> wdMainTextStory Then
                If .File.ActiveWindow.View.Type = wdNormalView Then
                    .File.ActiveWindow.ActivePane.Close
                Else
                    .File.ActiveWindow.View.SeekView = wdSeekMainDocument
                End If
            End If
        End With
        DoEvents
        Application.ScreenRefresh
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtContactName_GotFocus()
    OnControlGotFocus Me.txtContactName, "zzmpFaxCover_ContactName"
    Application.ScreenRefresh
    DoEvents
End Sub
Private Sub txtContactPhone_GotFocus()
    OnControlGotFocus Me.txtContactPhone, "zzmpFaxCover_ContactPhone"
    Application.ScreenRefresh
    DoEvents
End Sub

Private Sub txtCustom1_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom1, "zzmpFaxCover_Custom1"
End Sub

Private Sub txtCustom1_LostFocus() '9.7.1 - #4268
    Me.FaxCover.Custom1 = Me.txtCustom1
End Sub
Private Sub txtCustom2_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom2, "zzmpFaxCover_Custom2"
End Sub

Private Sub txtCustom2_LostFocus() '9.7.1 - #4268
    Me.FaxCover.Custom2 = Me.txtCustom2
End Sub
Private Sub txtCustom3_GotFocus() '9.7.1 - #4268
    OnControlGotFocus Me.txtCustom3, "zzmpFaxCover_Custom3"
End Sub

Private Sub txtCustom3_LostFocus() '9.7.1 - #4268
    Me.FaxCover.Custom3 = Me.txtCustom3
End Sub

Private Sub txtHeaderAdditionalText_GotFocus()
    OnControlGotFocus Me.txtHeaderAdditionalText, "zzmpHeaderAdditionalText"
End Sub

Private Sub txtHeaderAdditionalText_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.HeaderAdditionalText = Me.txtHeaderAdditionalText
    With FaxCover.Document
        If .Selection.StoryType <> wdMainTextStory Then
            If .File.ActiveWindow.View.Type = wdNormalView Then
                .File.ActiveWindow.ActivePane.Close
            Else
                .File.ActiveWindow.View.SeekView = wdSeekMainDocument
            End If
        End If
    End With
    DoEvents
    Application.ScreenRefresh
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtMessage_GotFocus()
    OnControlGotFocus Me.txtMessage, "zzmpFaxCover_Message"
End Sub

Private Sub txtMessage_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.Message = Me.txtMessage
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPages_GotFocus()
    OnControlGotFocus Me.txtPages, "zzmpFaxCover_NumberOfPages"
End Sub
Private Sub txtPages_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.NumberOfPages = Me.txtPages
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub txtReline_GotFocus()
    OnControlGotFocus Me.txtReline, "zzmpFaxCover_ReLine"
End Sub
Private Sub txtReline_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.Reline = Me.txtReline
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Public Sub UpdateForm()
    On Error GoTo ProcError
     '---get properties, load controls on reuse
    With FaxCover
    
        m_bForcedUpdate = True
        g_oMPO.ForceItemUpdate = True
        Me.cmbAuthor.Text = .Document.ReuseAuthor.DisplayName
        g_oMPO.ForceItemUpdate = False
        
        Me.txtReline = .Reline
        Me.txtPages = .NumberOfPages
        Me.cmbDeliveryComments.Text = .DeliveryComments
        
        With Me.cmbCM
            If g_bCM_AllowFilter Then  '#3818 - 9.6.2
                'set up list
                .AdditionalClientFilter = FaxCover.Document.GetVar("FaxCover_1_CMFilterString")
                .AdditionalMatterFilter = FaxCover.Document.GetVar("FaxCover_1_CMFilterString")
            End If
            .value = Me.FaxCover.FileNumber
        End With
    
        Me.txtSenderName = .SenderName
        Me.txtSenderPhone = .SenderPhone
        Me.txtContactName = .ContactName
        Me.txtContactPhone = .ContactPhone
        Me.cmbDateType.BoundText = .DateType
        Me.cmbDateType.BoundText = .DateType
        Me.txtCustom1 = .Custom1 '9.7.1 - #4268
        Me.txtCustom2 = .Custom2 '9.7.1 - #4268
        Me.txtCustom3 = .Custom3 '9.7.1 - #4268
        
        '9.7.1 - #4157
        Me.mlcConfidentialPhrase.Text = .DeliveryPhrases2
        If .IncludeDPhrases2InHeader <> mpUndefined Then _
            Me.chkIncludeCPhrasesInHeader = Abs(.IncludeDPhrases2InHeader)
        

'       save additional text because setting
'       separate pages = TRUE will automatically
'       wipe out additional text - then reset
'       addtional text below
        Dim xAdditional As String
        xAdditional = .HeaderAdditionalText
        If .CoverPerRecipient <> mpUndefined Then _
            Me.chkSeparatePages = Abs(.CoverPerRecipient)
        
'       reset additional text
        Me.txtHeaderAdditionalText = xAdditional
        
        Set Me.grdRecipients.Array = Me.Recipients.ListArray
        Me.grdRecipients.Rebind
        If Me.Recipients.Count > 0 Then _
            grdRecipients.Bookmark = 0
            
        Me.txtMessage = .Message
        
        With .Letterhead
            Me.cmbLetterheadType.BoundText = .LetterheadType
            If .IncludeEMail <> mpUndefined Then _
                Me.chkIncludeLetterheadEMail = Abs(.IncludeEMail)
            If .IncludeName <> mpUndefined Then _
                Me.chkIncludeLetterheadName = Abs(.IncludeName)
            If .IncludePhone <> mpUndefined Then _
                Me.chkIncludeLetterheadPhone = Abs(.IncludePhone)
            If .IncludeFax <> mpUndefined Then _
                Me.chkIncludeLetterheadFax = Abs(.IncludeFax)
            If .IncludeTitle <> mpUndefined Then _
                Me.chkIncludeLetterheadTitle = Abs(.IncludeTitle)
            If .IncludeAdmittedIn <> mpUndefined Then _
                Me.chkIncludeLetterheadAdmittedIn = Abs(.IncludeAdmittedIn)
            '9.7.1 #4028
            If .IncludeCustom1Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom1 = Abs(.IncludeCustom1Detail)
            If .IncludeCustom2Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom2 = Abs(.IncludeCustom2Detail)
            If .IncludeCustom3Detail <> mpUndefined Then _
                Me.chkIncludeLetterheadCustom3 = Abs(.IncludeCustom3Detail)
            '---
        End With
        
        m_bForcedUpdate = False
        
    End With
    Me.AuthorIsDirty = False
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmFax.UpdateForm"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub
Private Sub txtSenderName_GotFocus()
    OnControlGotFocus Me.txtSenderName, "zzmpFaxCover_SenderName"
End Sub
Private Sub txtSenderName_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.SenderName = Me.txtSenderName
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSenderPhone_GotFocus()
    OnControlGotFocus Me.txtSenderPhone, "zzmpFaxCover_SenderPhone"
End Sub
Private Sub txtSenderPhone_LostFocus()
    On Error GoTo ProcError
    Me.FaxCover.SenderPhone = Me.txtSenderPhone
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Public Sub ShowOptions(lID As Long)
    On Error GoTo ProcError
    Dim bCascade As Boolean
    
'   get original state of property
    bCascade = Me.CascadeUpdates
    
'   ensure no cascading
    Me.CascadeUpdates = False
    
'   show options
'************************************
'multitype:
    mdlDialog.ShowOptions Me.FaxCover.Author, Me.FaxCover.Template.ID, Me
    'mdlDialog.ShowOptions Me.FaxCover.Author, "Fax.dot", Me
'************************************

'   return to original state
    Me.CascadeUpdates = bCascade
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmFax.ShowOptions"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub
Private Sub chkIncludeLetterheadPhone_Click()
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludePhone = _
                -(Me.chkIncludeLetterheadPhone)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadPhone_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadPhone, "zzmpLetterHeadPhone"
End Sub

Private Sub chkIncludeLetterheadEMail_Click()
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeEMail = _
                -(Me.chkIncludeLetterheadEMail)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadEMail_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadEMail, "zzmpLetterheadEMail"
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_Click()
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeAdmittedIn = _
                -(Me.chkIncludeLetterheadAdmittedIn)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadAdmittedIn_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadAdmittedIn, "zzmpLetterheadAdmittedIn"
End Sub

Private Sub chkIncludeLetterheadName_Click()
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeName = _
                -(Me.chkIncludeLetterheadName)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadName_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadName, "zzmpLetterheadName"
End Sub

Private Sub chkIncludeLetterheadTitle_Click()
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeTitle = _
                -(Me.chkIncludeLetterheadTitle)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadTitle_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadTitle, "zzmpLetterheadTitle"
End Sub

Private Sub chkIncludeLetterheadFax_GotFocus()
    OnControlGotFocus Me.chkIncludeLetterheadFax, "zzmpLetterheadFax"
End Sub

Private Sub chkIncludeLetterheadFax_Click()
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeFax = _
                -(Me.chkIncludeLetterheadFax)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Function GridCurrentRowIsEmpty(grdTest As TDBGrid) As Boolean
    Dim i As Integer
    On Error GoTo ProcError
    GridCurrentRowIsEmpty = True
    With grdTest
        For i = 0 To .Columns.Count - 1
            If .Columns(i).Text <> "" Then
                GridCurrentRowIsEmpty = False
                Exit Function
            End If
        Next i
    End With
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmFax.GridCurrentRowIsEmpty"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Sub UpdateHeaderText()
    Dim i As Integer
    Dim xHeader As String
    
    On Error GoTo ProcError
    If Me.chkSeparatePages = 1 Then
        xHeader = ""
    ElseIf Me.Recipients.Count > 0 Then
        ' 9.7.1 #4162
        For i = 1 To Recipients.Count
            If xHeader <> "" Then
                If i < Recipients.Count Then
                    xHeader = xHeader & Me.FaxCover.HeaderNamesSeparator
                Else
                    xHeader = xHeader & Me.FaxCover.HeaderNamesFinalSeparator
                End If
            End If
            If Recipients(i).Item(1) <> "" Then
                xHeader = xHeader & Recipients(i).Item(1)
            Else
                xHeader = xHeader & Recipients(i).Item(2)
            End If
        Next i
        ' end #4162
    End If
    FaxCover.HeaderAdditionalText = xHeader
    Me.txtHeaderAdditionalText = FaxCover.HeaderAdditionalText
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              CurErrSource("frmFax.UpdateHeaderText"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub ShowAuthorMenu()
    Dim bEnabled As Boolean
    On Error GoTo ProcError
    
'   enable specified menu items only if author
'   is currently in the db
    If TypeOf FaxCover.Author Is CReUseAuthor Then
        bEnabled = False
    Else
        bEnabled = True
    End If
    
    Me.mnuAuthor_EditAuthorOptions.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Enabled = bEnabled
    Me.mnuAuthor_SetDefault.Enabled = bEnabled
    Me.mnuAuthor_Favorite.Checked = Me.FaxCover.Author.Favorite
    Me.PopupMenu mnuAuthor
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub GetContacts()
    Dim xarTemp As XArray
    Dim xContacts As String
    Dim iCount As Integer
    Dim i As Integer
    Dim j As Integer
    Dim rngTable As Word.Range
    Static bCIActive As Boolean
    Dim oContacts As Object
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    If Not g_bShowAddressBooks Then Exit Sub

    If bCIActive Then Exit Sub
    bCIActive = True
    
    Set xarTemp = Me.Recipients.ListArray
    iCount = xarTemp.UpperBound(1)
    
    g_oMPO.GetFaxRecipients xarTemp, , , , oContacts
    
    m_bForcedUpdate = True
    If xarTemp.UpperBound(1) > iCount Then
        ' Reset recipients list only if new names have been added
        Set grdRecipients.Array = xarTemp
        grdRecipients.Rebind
        
        ' Update Recipients in Fax Cover
        Me.Recipients.UpdateFromXArray xarTemp, False, False
        g_oMPO.ScreenUpdating = False
        Me.FaxCover.RefreshRecipients mpbase2.Max(1, iCount + 1)
        g_oMPO.ScreenUpdating = True
        Application.ScreenRefresh
        grdRecipients.FirstRow = iCount + 1
'       setfocus to grdRecipients only if in first tab
        If Me.tabPages = 0 Then
            grdRecipients.SetFocus
        End If
        ShowNumRecipients
        
'       filter client/matter based on contacts retrieved
        If g_bCM_RunConnected And g_bCM_AllowFilter Then
            If Me.Recipients.Count Then
                '---9.6.1
                Dim l_CIVersion As Long
                l_CIVersion = g_oMPO.Contacts.CIVersion
                
                If Me.cmbCM.AdditionalClientFilter <> "" Then
                    If l_CIVersion = mpCIVersion_1 Then
                        xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                                   xGetClientMatterFilter(oContacts)
                    Else
                        xSQL = Me.cmbCM.AdditionalClientFilter & " OR " & _
                                   xGetClientMatterFilter2X(oContacts)
                    End If
                Else
                    If l_CIVersion = mpCIVersion_1 Then
                        xSQL = xGetClientMatterFilter(oContacts)
                    Else
                        xSQL = xGetClientMatterFilter2X(oContacts)
                    End If
                End If
                
                If xSQL <> "" Then
                    Me.cmbCM.AdditionalClientFilter = xSQL
                    Me.cmbCM.AdditionalMatterFilter = xSQL
                    Me.cmbCM.Refresh
                End If
                '---end 9.6.1
            End If
        End If
        
    End If
    m_bForcedUpdate = False
    m_bGridIsDirty = False
    DoEvents
    bCIActive = False
    Me.SetFocus
    Exit Sub
ProcError:
    bCIActive = False
    g_oError.Show Err
    Exit Sub

End Sub
Private Sub PositionControls() '9.7.1 - #4160
    Dim sIncrement As Single
    Dim sOffset As Single
    Dim sAdjustIncrement As Single
    Dim i As Integer
    Dim oCtl As VB.Control
    Dim arrCtls() As String
    Dim bVisible As Boolean
    Dim iCurrTab As Integer

    On Error GoTo ProcError
    HideUnusedLetterheadControls Me, Me.FaxCover.Template.ID '9.7.1 #4158
    ReDim arrCtls(Me.Controls.Count)
'   place each control in tab order sort
    On Error Resume Next
    For Each oCtl In Me.Controls
        arrCtls(oCtl.TabIndex) = oCtl.Name
    Next
    On Error GoTo ProcError
    sIncrement = 160
    For i = chkSeparatePages.TabIndex To txtCustom1.TabIndex ' adjust if controls added on first tab after Custom2
        ' See which controls are visible to set correct spacing
        Set oCtl = Me.Controls(arrCtls(i))
        
'       do only if visible
        If Not oCtl.Visible Then
             Debug.Print oCtl.Name & " not visible"
            If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                TypeOf oCtl Is VB.CheckBox Or _
                TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                TypeOf oCtl Is mpSal.SalutationCombo) Then
'               position control relative to previous control
                sIncrement = sIncrement + 30
            ElseIf (TypeOf oCtl Is VB.TextBox Or _
                TypeOf oCtl Is mpControls4.RelineControl) Then
                sIncrement = sIncrement + ((oCtl.Height / 30) * 30)
            ElseIf (TypeOf oCtl Is mpControls.MultiLineCombo) Then
                sIncrement = sIncrement + 60
            End If
            
        End If
    Next i
    If sIncrement > 250 Then sIncrement = 250
    iCurrTab = 0
    bVisible = True
    sOffset = Me.grdRecipients.Top + Me.grdRecipients.Height + 70
    For i = chkSeparatePages.TabIndex To Me.chkIncludeCPhrasesInHeader.TabIndex ' Adjust if controls added to last tab after Header 2 Text
        Set oCtl = Me.Controls(arrCtls(i))
        If Not TypeOf oCtl Is VB.Frame Then
            If i > Me.txtCustom1.TabIndex And iCurrTab = 0 Then
                iCurrTab = 1
                sIncrement = 170
                sOffset = 200
                bVisible = False
            ElseIf i > Me.txtCustom2.TabIndex And iCurrTab = 1 Then
                If Not bVisible Then
                    ' All controls on tab are hidden
                    Me.tabPages.TabVisible(iCurrTab) = False
                End If
                iCurrTab = 2
                sIncrement = 250
                sOffset = 200
                bVisible = False
            End If
            
    '       do only if visible
            If oCtl.Visible Then
                bVisible = True
                If (TypeOf oCtl Is TrueDBList60.TDBCombo Or _
                    TypeOf oCtl Is VB.TextBox Or _
                    TypeOf oCtl Is VB.CheckBox Or _
                    TypeOf oCtl Is mpControls.MultiLineCombo Or _
                    TypeOf oCtl Is MPCM.ClientMatterComboBox Or _
                    TypeOf oCtl Is mpControls4.RelineControl Or _
                    TypeOf oCtl Is mpSal.SalutationCombo) Then
                    
                    If TypeOf oCtl Is mpControls.MultiLineCombo Then
                        oCtl.Top = sOffset - 75
                        sOffset = sOffset + (Me.cmbAuthor.Height * 2.5) + sIncrement - 120
                        sIncrement = sIncrement - 45
                    ElseIf TypeOf oCtl Is MPCM.ClientMatterComboBox Then
                        oCtl.Top = sOffset
                        sOffset = sOffset + Me.cmbAuthor.Height + sIncrement
                    ElseIf TypeOf oCtl Is mpControls4.RelineControl Then
                        oCtl.Top = sOffset
                        sOffset = sOffset + (oCtl.Height - 300) + sIncrement
                    ElseIf TypeOf oCtl Is VB.CheckBox Then
    '               position control relative to previous control
                        If InStr(UCase(oCtl.Name), "CHKINCLUDELETTERHEAD") = 0 Then
                            sAdjustIncrement = 0
                            oCtl.Top = sOffset + sAdjustIncrement
                            sOffset = oCtl.Top + oCtl.Height + sIncrement
                        End If
                    Else
                        oCtl.Top = sOffset
                        sOffset = sOffset + oCtl.Height + sIncrement
                        If UCase(oCtl.Name) = "CMBLETTERHEADTYPE" Then
                            sOffset = sOffset - 50
                            sOffset = PositionLetterheadControls(Me, i + 1, sOffset, sIncrement)
                        End If
                    End If
    '               set accompanying label vertical position
                    If TypeOf Me.Controls(arrCtls(i - 1)) Is Label Then
                        Me.Controls(arrCtls(i - 1)).Top = oCtl.Top + 30
                    End If
                ElseIf TypeOf oCtl Is VB.Label Then
                    If oCtl.Name = "lblPagesIncluding" Then
                        oCtl.Top = Me.lblPages.Top
                    ElseIf oCtl.Name = "lblIfProblems" Then
                        oCtl.Top = sOffset - 50
                        sOffset = oCtl.Top + oCtl.Height + 50
                    End If
                End If
            End If
        End If
    Next i
    If Not bVisible Then
        ' All controls on tab are hidden
        Me.tabPages.TabVisible(iCurrTab) = False
    End If
    Exit Sub
ProcError:
    g_oError.Raise Err, "MacPac90.frmFax.PositionControls"
End Sub

Private Sub chkIncludeLetterheadCustom1_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeCustom1Detail = _
                -(Me.chkIncludeLetterheadCustom1)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom1_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom1, "zzmpLetterheadCustom1Detail"
End Sub
Private Sub chkIncludeLetterheadCustom2_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeCustom2Detail = _
                -(Me.chkIncludeLetterheadCustom2)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom2_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom2, "zzmpLetterheadCustom2Detail"
End Sub
Private Sub chkIncludeLetterheadCustom3_Click() '9.7.1 #4028
    On Error GoTo ProcError
        With Word.Application
            .ScreenUpdating = False
            FaxCover.Letterhead.IncludeCustom3Detail = _
                -(Me.chkIncludeLetterheadCustom3)
            .ScreenUpdating = True
            .ScreenRefresh
        End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeLetterheadCustom3_GotFocus() '9.7.1 #4028
    OnControlGotFocus Me.chkIncludeLetterheadCustom3, "zzmpLetterheadCustom3Detail"
End Sub


