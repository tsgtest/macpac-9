VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{5EA08114-E9CE-405F-818F-7D031E6BC28B}#1.1#0"; "mpControls.ocx"
Begin VB.Form frmPleadingSignature 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Pleading Signature"
   ClientHeight    =   6468
   ClientLeft      =   1920
   ClientTop       =   20328
   ClientWidth     =   5472
   Icon            =   "frmPleadingSignature.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6468
   ScaleWidth      =   5472
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnAddressBooks 
      Height          =   400
      Left            =   3240
      Picture         =   "frmPleadingSignature.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   43
      ToolTipText     =   "Get Contacts (F2)"
      Top             =   6024
      Width           =   650
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4905
      Picture         =   "frmPleadingSignature.frx":0CAC
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Delete Signature"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4440
      Picture         =   "frmPleadingSignature.frx":0E36
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Save new signature"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnNew 
      Height          =   400
      Left            =   3975
      Picture         =   "frmPleadingSignature.frx":0FB4
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Add a New Signature (F5)"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4725
      TabIndex        =   45
      Top             =   6024
      Width           =   650
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3990
      TabIndex        =   44
      Top             =   6024
      Width           =   650
   End
   Begin TrueDBList60.TDBList lstSignatures 
      Height          =   1035
      Left            =   1770
      OleObjectBlob   =   "frmPleadingSignature.frx":14F6
      TabIndex        =   4
      Top             =   540
      Width           =   3570
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   4635
      Left            =   -15
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   1800
      Width           =   5925
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|Address &Info|&Layout"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   400
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Height          =   4212
         Left            =   6660
         TabIndex        =   48
         Top             =   15
         Width           =   5904
         Begin TrueDBGrid60.TDBGrid grdLayout 
            DragIcon        =   "frmPleadingSignature.frx":343B
            Height          =   1950
            Left            =   1765
            OleObjectBlob   =   "frmPleadingSignature.frx":358D
            TabIndex        =   41
            Top             =   180
            Width           =   3555
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   0
            X1              =   -165
            X2              =   5475
            Y1              =   4170
            Y2              =   4170
         End
         Begin VB.Label lblLayout 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Signatures Layout:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   30
            TabIndex        =   40
            Top             =   180
            Width           =   1455
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6000
            Left            =   0
            Top             =   -1800
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   4212
         Left            =   6420
         TabIndex        =   46
         Top             =   15
         Width           =   5904
         Begin VB.TextBox txtFirmPhone 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   33
            Top             =   2085
            Width           =   3570
         End
         Begin VB.TextBox txtFirmID 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   39
            Top             =   3510
            Width           =   3570
         End
         Begin VB.TextBox txtFirmFax 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   35
            Top             =   2550
            Width           =   3570
         End
         Begin VB.TextBox txtSignerEmail 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   37
            Top             =   3030
            Width           =   3570
         End
         Begin VB.TextBox txtFirmState 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   31
            Top             =   1635
            Width           =   3570
         End
         Begin VB.TextBox txtFirmCity 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   29
            Top             =   1185
            Width           =   3570
         End
         Begin VB.TextBox txtFirmAddress 
            Appearance      =   0  'Flat
            Height          =   900
            Left            =   1770
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   27
            Top             =   90
            Width           =   3570
         End
         Begin VB.Shape Shape4 
            BorderStyle     =   0  'Transparent
            Height          =   6015
            Left            =   -60
            Top             =   -225
            Visible         =   0   'False
            Width           =   5895
         End
         Begin VB.Label lblFirmCity 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm &City:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   28
            Top             =   1215
            Width           =   1455
         End
         Begin VB.Label lblFirmState 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Fir&m State:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   30
            Top             =   1635
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   1
            X1              =   -45
            X2              =   5625
            Y1              =   4170
            Y2              =   4170
         End
         Begin VB.Label lblSignerEmail 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signer &Email:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   36
            Top             =   3045
            Width           =   1455
         End
         Begin VB.Label lblFirmFax 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm Fa&x:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   34
            Top             =   2565
            Width           =   1455
         End
         Begin VB.Label lblFirmID 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm &ID:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   38
            Top             =   3540
            Width           =   1455
         End
         Begin VB.Label lblFirmAddress 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm &Address:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   26
            Top             =   90
            Width           =   1455
         End
         Begin VB.Label lblFirmPhone 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm P&hone:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   105
            TabIndex        =   32
            Top             =   2100
            Width           =   1455
         End
         Begin VB.Shape Shape5 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   6030
            Left            =   0
            Top             =   -1845
            Width           =   1695
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   4212
         Left            =   12
         TabIndex        =   47
         Top             =   15
         Width           =   5904
         Begin VB.TextBox txtFirmSlogan 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   25
            Top             =   3825
            Width           =   3570
         End
         Begin TrueDBList60.TDBCombo cmbOneOf 
            Height          =   300
            Left            =   1770
            OleObjectBlob   =   "frmPleadingSignature.frx":5F9C
            TabIndex        =   8
            Top             =   480
            Visible         =   0   'False
            Width           =   3585
         End
         Begin VB.CheckBox chkIncludeClosingParagraph 
            Appearance      =   0  'Flat
            BackColor       =   &H80000004&
            Caption         =   "E&xecuted At..."
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   1875
            TabIndex        =   9
            Top             =   535
            Width           =   2490
         End
         Begin VB.TextBox txtFirmName 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   23
            Top             =   3435
            Width           =   3570
         End
         Begin mpControls.SigningAttorneysGrid saGrid 
            Height          =   975
            Left            =   1770
            TabIndex        =   15
            Tag             =   "975"
            Top             =   1455
            Width           =   3585
            _ExtentX        =   6329
            _ExtentY        =   4403
            BackColor       =   -2147483643
            RecordSelectors =   0   'False
            Separator       =   "; "
            BarIDSuffix     =   ""
            BarIDPrefix     =   ", Bar No. "
            SignerEMailPrefix=   ", "
            SeparatorSpecial=   2
            FontName        =   "Arial"
            FontBold        =   0   'False
            FontSize        =   8.4
            FontItalic      =   0   'False
            FontUnderline   =   0   'False
         End
         Begin VB.TextBox txtSignatureJudge 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   13
            Top             =   1455
            Visible         =   0   'False
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbDateType 
            Height          =   390
            Left            =   1770
            OleObjectBlob   =   "frmPleadingSignature.frx":81B4
            TabIndex        =   11
            Top             =   945
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbSignatureType 
            Height          =   390
            Left            =   1770
            OleObjectBlob   =   "frmPleadingSignature.frx":A3CB
            TabIndex        =   6
            Top             =   30
            Width           =   3585
         End
         Begin VB.TextBox txtClientName 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   21
            Top             =   3000
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbAttorneysFor 
            Height          =   300
            Left            =   1770
            OleObjectBlob   =   "frmPleadingSignature.frx":C5FF
            TabIndex        =   19
            Top             =   2625
            Width           =   3585
         End
         Begin VB.TextBox txtSignerName 
            Appearance      =   0  'Flat
            Height          =   315
            Left            =   1770
            MultiLine       =   -1  'True
            TabIndex        =   17
            Top             =   2490
            Width           =   3570
         End
         Begin VB.Label lblFirmSlogan 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm Slo&gan:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   24
            Top             =   3855
            Width           =   1455
         End
         Begin VB.Label lblSignerName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Si&gner Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   16
            Top             =   2505
            Width           =   1455
         End
         Begin VB.Label lblOneOf 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "B&y...:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   60
            TabIndex        =   7
            Top             =   480
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblFirmName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Firm &Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   22
            Top             =   3450
            Width           =   1455
         End
         Begin VB.Label lblSignatureDate 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Date:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   75
            TabIndex        =   10
            Top             =   975
            Width           =   1455
         End
         Begin VB.Label lblSignatureType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   60
            TabIndex        =   5
            Top             =   60
            Width           =   1455
         End
         Begin VB.Label lblAttorneysFor 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   " Att&orneys For:"
            ForeColor       =   &H00FFFFFF&
            Height          =   225
            Left            =   105
            TabIndex        =   18
            Top             =   2670
            Width           =   1410
         End
         Begin VB.Label lblClientName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Client Name(s):"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   45
            TabIndex        =   20
            Top             =   3000
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   3
            X1              =   -195
            X2              =   5475
            Y1              =   4170
            Y2              =   4170
         End
         Begin VB.Label lblAttorneys 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Attorneys:"
            ForeColor       =   &H00FFFFFF&
            Height          =   330
            Left            =   45
            TabIndex        =   14
            Top             =   1440
            Width           =   1455
         End
         Begin VB.Label lblSignatureJudge 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Judge Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   0
            TabIndex        =   12
            Top             =   1440
            Visible         =   0   'False
            Width           =   1500
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   5940
            Left            =   0
            Top             =   -1740
            Width           =   1695
         End
         Begin VB.Shape Shape3 
            BorderStyle     =   0  'Transparent
            Height          =   6075
            Left            =   1770
            Top             =   -135
            Width           =   5805
         End
      End
   End
   Begin VB.Label lblSignatures 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Signature Blocks:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   285
      TabIndex        =   3
      Tag             =   "&Signature Blocks:"
      Top             =   540
      Width           =   1230
   End
   Begin VB.Shape Shape6 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00800080&
      Height          =   5940
      Left            =   0
      Top             =   0
      Width           =   1695
   End
   Begin VB.Menu mnuLayoutGrid 
      Caption         =   "Layout Grid"
      Visible         =   0   'False
      Begin VB.Menu mnuLayoutGridAdd 
         Caption         =   "&Add Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridChangeDisplayName 
         Caption         =   "&Change Display Name..."
      End
      Begin VB.Menu mnuLayoutGridCopy 
         Caption         =   "Cop&y Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridDelete1 
         Caption         =   "&Delete Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridDeleteAll 
         Caption         =   "Delete All &Signatures"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridPaste 
         Caption         =   "&Paste Signature"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
End
Attribute VB_Name = "frmPleadingSignature"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const mpDefaultDateFormat As String = "MMMM d, yyyy"
Const mpCancelAdd As String = "Cancel unsaved signature (F7)"
Const mpDeleteSig As String = "Delete this signature (F7)"
Const mpCancelChange As String = "Cancel unsaved changes (F7)"
Const mpSaveChanges As String = "Save changes to this signature (F6)"
Const mpSaveNewSignature As String = "Save new signature (F6)"


'---drag and drop support
Private xDragFrom() As String
Private is_SourceRow As Integer
Private is_SourceCol As Integer
Private is_DestRow As Integer
Private is_DestCol As Integer

'---macpac object vars
Private m_Sigs As MPO.CPleadingSignatures
Private m_oSig As MPO.CPleadingSignature
Private m_Document As MPO.CDocument
Private m_oPleadingType As mpDB.CPleadingDef
Private m_oDB As mpDB.CDatabase
Private m_oParent As Form
Private m_oDraggedSig As CPleadingSignature

Private xARLayout As XArrayObject.XArray
Private xARSigs As XArrayObject.XArray

Private m_bCascadeUpdates As Boolean
Private m_bInitializing As Boolean
Private m_bCancelled As Boolean
Private m_bAdded As Boolean
Private m_bChanged As Boolean
Private m_bDD As Boolean
Private m_bClosingParagraph As mpTriState
Private m_xKey As String

'---these vars required for change/save validation routines
Private m_lSigType As Long
Private m_xDateType As String
Private m_xClientName As String
Private m_xAttyFor As String
Private m_xFirmName As String
Private m_xFirmNameESig As String
Private m_xFirmSlogan As String
Private m_xFirmPhone As String
Private m_xFirmFax As String
Private m_xSignerEmail As String
Private m_xFirmAddress As String
Private m_xFirmID As String
Private m_xFirmCity As String
Private m_xFirmState As String
Private m_xJudgeName As String
Private m_xSigners As String
Private m_xSignerName As String
Private m_xDated As String
Private bDeleteRun As Boolean
Private m_xOneOf As String
Private xMsg As String
Public Property Get Signatures() As MPO.CPleadingSignatures
    Set Signatures = m_Sigs
End Property
Public Property Let Signatures(oNew As MPO.CPleadingSignatures)
    Set m_Sigs = oNew
End Property
Public Property Get Signature() As MPO.CPleadingSignature
    Set Signature = m_oSig
End Property
Public Property Let Signature(oNew As MPO.CPleadingSignature)
    Set m_oSig = oNew
End Property
Public Property Get DraggedSignature() As MPO.CPleadingSignature
    Set DraggedSignature = m_oDraggedSig
End Property
Public Property Let DraggedSignature(oNew As MPO.CPleadingSignature)
    Set m_oDraggedSig = oNew
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property
Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get PleadingType() As mpDB.CPleadingDef
    Set PleadingType = m_oPleadingType
End Property
Public Property Let PleadingType(oNew As mpDB.CPleadingDef)
    Set m_oPleadingType = oNew
End Property
Public Property Get CascadeUpdates() As Boolean
    CascadeUpdates = m_bCascadeUpdates
End Property
Public Property Let CascadeUpdates(bNew As Boolean)
    m_bCascadeUpdates = bNew
End Property
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property
Public Property Get Added() As Boolean
    Added = m_bAdded
End Property
Public Property Let Added(bNew As Boolean)
    m_bAdded = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    Me.grdLayout.Enabled = Not bNew
    Me.lblLayout.Enabled = Not bNew
    Me.lstSignatures.Enabled = Not bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelAdd
        Me.btnSave.ToolTipText = mpSaveNewSignature
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property

Public Property Get Changed() As Boolean
    Changed = m_bChanged
End Property

Public Property Let Changed(bNew As Boolean)
    m_bChanged = bNew
    Me.btnNew.Enabled = Not bNew
    Me.btnSave.Enabled = bNew
    Me.grdLayout.Enabled = Not bNew
    Me.lblLayout.Enabled = Not bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelChange
        Me.btnSave.ToolTipText = mpSaveChanges
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Level0() As Long
    On Error Resume Next
    Level0 = m_Document.RetrieveItem("Level0", "Pleading")
End Property
Public Property Get Level1() As Long
    On Error Resume Next
    Level1 = m_Document.RetrieveItem("Level1", "Pleading")
End Property
Public Property Get Level2() As Long
    On Error Resume Next
    Level2 = m_Document.RetrieveItem("Level2", "Pleading")
End Property
Public Property Get Level3() As Long
    On Error Resume Next
    Level3 = m_Document.RetrieveItem("Level3", "Pleading")
End Property

'******************************************************************
'private functions
'******************************************************************
Private Function UpdateSignature(iTargetRow As Integer, _
                                 Optional iTargetCol As Integer = -9999) As Boolean
    Dim xKey As String
    Dim iSigner As Integer
        
    '---updates/edits sig chosen either in combo or display grid
    '---display grid selection not implemented in this version of user interface
    '---it can be accessed by supplying col argument
    On Error Resume Next
    
    If iTargetCol = -9999 Then
        xKey = lstSignatures.Array.value(iTargetRow, 1)
    Else
        xKey = grdLayout.Array.value(iTargetRow, iTargetCol, 1)
    End If

    '---9.4.0
    If Len(xKey) = 0 Then
        If Me.Signatures.Count = 1 Then
            xKey = Me.Signatures(1).Key
        End If
    End If
    
    On Error GoTo ProcError
    
    '---fill controls with properties
    If Len(xKey) > 0 Then
        With Signatures.Item(xKey)
            .TypeID = Me.cmbSignatureType.BoundText
            If Me.cmbDateType.Text = "None" Then
                .Dated = ""
            Else
                .Dated = Me.cmbDateType.Text
            End If
            If .Definition.DisplayJudgeName Then
                If Me.txtSignatureJudge = "" Then
                    txtSignatureJudge = "(Judge's Name)"
                End If
                .JudgeName = Me.txtSignatureJudge
                .DisplayName = .JudgeName
                .Signer = ""
            Else
                UpdateSignersFromGrid Signatures.Item(xKey), Me.saGrid
            End If
            .PartyName(mpFalse) = Me.txtClientName
            .PartyTitle(mpFalse) = Me.cmbAttorneysFor.Text
            
            If .Definition.ShowFirmID = True Then
                .FirmID = Me.txtFirmID
            Else
                .FirmID(mpTrue) = ""
            End If
            
            '9.7.1 - 4030
            .FirmAddress = xReformatAddress(Me.txtFirmAddress, .Definition.OfficeAddressFormat)
            
            '---9.7.1 - 4035
            '9.7.1020 - 4567
            If .SignerID <> 0 And g_oMPO.db.People.IsInPeopleList(.SignerID) Then
                .Office = g_oMPO.Attorneys(.SignerID).Office
            Else
                .Office = g_oMPO.Offices.Default
            End If
            
            .FirmName = Me.txtFirmName  '9.7.1 #4035
            .FirmNameESig = Me.txtFirmName  '9.9.1 #4954
            .FirmSlogan = Me.txtFirmSlogan  '*c
            .FirmPhone = Me.txtFirmPhone
            .FirmFax = Me.txtFirmFax
            .SignerEMail = Me.txtSignerEmail
            .FirmCity = Me.txtFirmCity
            .FirmState = Me.txtFirmState
            '---9.4.1
            .OneOfSignerText = Me.cmbOneOf.Text
            
            '---9.7.1 - 4081
            If .Definition.DisplaySignerName = True Then
                .InputSigner = Me.txtSignerName
                .Signer = Me.txtSignerName
            End If
            
            '---9.4.0
            If .Definition.ShowFirmAddress = True Then
                .FirmID = Me.txtFirmID
            Else
                .FirmID(mpTrue) = ""
            End If
            
            '---handle closing paragraph
            If Signatures(1).Key = xKey Then
                Signatures.ClosingParagraphCity = Me.txtFirmCity
                Signatures.ClosingParagraphState = Me.txtFirmState
            End If
            
    '---update sig after adding to grid
            '.Refresh
        End With
    End If
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.UpdateSignature"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Function DisplaySignature(Optional iTargetRow As Integer = -9999, _
                                            Optional iTargetCol As Integer = -9999) As Boolean
    Dim xKey As String
    '---loads controls with sig selected from combo or grid -- note
    '---that grid selection not implemented in this version of user interface
    '---supplying col argument activates grid sig selection
    '---of no row supplied then function clears controls
    
    On Error Resume Next
    If iTargetRow = -9999 Then
        xKey = ""
    Else
        If iTargetCol = -9999 Then
            xKey = lstSignatures.Array.value(iTargetRow, 1)
        Else
            xKey = grdLayout.Array.value(iTargetRow, iTargetCol, 1)
        End If
    End If
    
    On Error GoTo ProcError
    If Len(xKey) > 0 Then
        With Signatures.Item(xKey)
            cmbSignatureType.BoundText = .TypeID
            'lstSignatures.Text = lstSignatures.Array.Value(lstSignatures.Bookmark, 0)
            lstSignatures.SelectedItem = lstSignatures.Bookmark
            If .Dated = "" Then
                Me.cmbDateType.Text = "None"
            Else
                Me.cmbDateType.Text = .Dated
            End If
'---don't assign .signers prop directly to sa grid control, otherwise
'---control grid array *IS* the property array and changes made to grid signers prop
'---changes the actual sig property arrray outside of control of object model
            If Not .Signers Is Nothing Then
                saGrid.Clear
                saGrid.Signers = xARClone(.Signers)
            End If
            txtSignatureJudge.Text = .JudgeName
            cmbAttorneysFor.Text = .PartyTitle
            txtClientName = .PartyName
            txtFirmName = .FirmName     '9.7.1 #4035
            txtFirmSlogan = .FirmSlogan '*c
            txtFirmAddress = .FirmAddress
            txtFirmPhone = .FirmPhone
            txtFirmID = .FirmID
            txtFirmCity = .FirmCity
            txtFirmState = .FirmState
            txtFirmFax = .FirmFax
            txtSignerEmail = .SignerEMail
            '---9.7.1 - 4081
            txtSignerName = .InputSigner
            '---9.4.1
            On Error Resume Next
''''            cmbOneOf.BoundText = .OneOfSignerText
            '---9.6.2
            cmbOneOf.Text = .OneOfSignerText
            '---end 9.4.1
            Me.Added = False
        End With
    Else
        With Me
            .saGrid.Clear
            .txtSignatureJudge.Text = ""
            .txtClientName = ""
            .txtFirmID = ""
            .txtFirmName = ""
            .txtFirmSlogan = ""
            .txtFirmAddress = ""
            .txtFirmPhone = ""
            .txtFirmFax = ""
            .txtSignerEmail = ""
            .txtFirmCity = ""
            .txtFirmState = ""
            .txtSignerName = ""  '---9.7.1 - 4081
            .Added = True
        End With
    End If
    'DoEvents
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.DisplaySignature"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function
Private Function bValidateSigners() As Boolean
    On Error Resume Next
    bValidateSigners = True
    '9.7.1 - 4081
    If Me.txtSignatureJudge.Visible Or Me.txtSignerName.Visible Then Exit Function

    If saGrid.Signers.UpperBound(1) = -1 Then
        xMsg = "You must select at least one signing attorney"
        MsgBox xMsg, vbExclamation
'        saGrid.SetFocus
        bValidateSigners = False
        Exit Function
    End If

    If saGrid.SignerText = "" Then
        xMsg = "You must select a signer"
        MsgBox xMsg, vbExclamation
'        saGrid.SetFocus
        bValidateSigners = False
        Exit Function
    End If
End Function
Private Function ValidateType() As Boolean
    On Error Resume Next

    If Me.cmbSignatureType.Text = "" Then
        xMsg = "You must select a signature type"
        MsgBox xMsg, vbExclamation
        cmbSignatureType.SetFocus
        ValidateType = False
        Exit Function
    End If
    ValidateType = True
End Function
Private Function AddSignature(iTargetRow, iTargetCol, Optional iPos As Integer)
    Dim xDisplayName As String
    Dim iRow As Integer
    '---adds sig to collection, updates control combo and layout grid
    '---note that sig is added only in memory; sigs will not be written until
    '---refresh method called -- this allows cancellation of add from interface
    
    On Error GoTo ProcError
    Signatures.Add
    Signature = Signatures(Signatures.Count)
    Signature.TypeID = Me.cmbSignatureType.BoundText
    If Signature.Definition.DisplayJudgeName = True Then
        xDisplayName = Me.txtSignatureJudge
        If xDisplayName = "" Then
            xDisplayName = "(Judge's Name)"
        End If
    ElseIf Signature.Definition.DisplaySignerName = True Then '9.7.1---4081
        xDisplayName = Me.txtSignerName
        If xDisplayName = "" Then
            xDisplayName = "No Display Name"
        End If
    Else
        If InStr(saGrid.SignerText, ",") Then
            xDisplayName = Left(saGrid.SignerText, InStr(saGrid.SignerText, ",") - 1)
        Else
            xDisplayName = saGrid.SignerText
        End If
    End If
    
    If iPos = 0 Then iPos = (iTargetRow + 1) * (iTargetCol + 1)

    With grdLayout
        .Array.Insert (1), .Array.UpperBound(1) + 1
        .Array.value(iTargetRow, iTargetCol, 1) = Signature.Key
        .Bookmark = Null
        .Col = iTargetCol
        .Columns(iTargetCol).Text = .Array.value(iTargetRow, iTargetCol, 0)
    End With

    With Signature
        .TypeID = Me.cmbSignatureType.BoundText
        If Me.cmbDateType.Text = "None" Then
            .Dated = ""
        Else
            .Dated = Me.cmbDateType.Text
        End If
        .Position = iPos
        .JudgeName = Me.txtSignatureJudge
        .PartyName = Me.txtClientName
        .PartyTitle = Me.cmbAttorneysFor.Text
        '---9.7.1 - 4035 - 4081
        If .Definition.DisplayJudgeName = False And .Definition.DisplaySignerName = False Then
            If saGrid.Author = 0 Or g_oMPO.UseDefOfficeInfo Then
                .Office = g_oMPO.Offices.Default
            Else
                .Office = g_oMPO.Attorneys(saGrid.Author).Office
            End If
        End If
        .FirmName = Me.txtFirmName  '9.7.1 #4035
        .FirmNameESig = Me.txtFirmName  '9.9.1 #4954
        .FirmSlogan = Me.txtFirmSlogan  '*c
        .FirmAddress = xReformatAddress(Me.txtFirmAddress, .Definition.OfficeAddressFormat)
        .FirmPhone = Me.txtFirmPhone
        .FirmFax = Me.txtFirmFax
        .FirmCity = Me.txtFirmCity
        .FirmState = Me.txtFirmState
        .SignerEMail = Me.txtSignerEmail
        .InputSigner = Me.txtSignerName '9.7.1  #4081
        If .Definition.ShowFirmID = True Then
            .FirmID = Me.txtFirmID
        Else
            .FirmID(mpTrue) = ""
        End If
        '---9.4
        .DisplayName = xDisplayName
        
        '---9.4.1
        .OneOfSignerText = Me.cmbOneOf.BoundText
        '---end 9.4.1
        
        '9.7.1---4081
        If .Definition.DisplayJudgeName = False And .Definition.DisplaySignerName = False Then
            UpdateSignersFromGrid Signature, Me.saGrid
        End If
    End With
    ClearFields
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.AddSignature"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function
Private Function bResetDragDrop(xSource As String, xTarget As String) As Boolean
    Dim g_Source As TDBGrid
    Dim g_Target As TDBGrid
    Dim ctl As Control


    On Error GoTo ProcError
    For Each ctl In Me.Controls
        If ctl.Name = xSource Then
            Set g_Source = ctl
        ElseIf ctl.Name = xTarget Then
            Set g_Target = ctl
        End If
    Next

' Turn off drag-and-drop by resetting the highlight and data
' control caption.

    If g_Source.MarqueeStyle = dbgSolidCellBorder Then Exit Function
    g_Source.MarqueeStyle = dbgSolidCellBorder
    g_Target.MarqueeStyle = dbgSolidCellBorder

    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.bResetDragDrop"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function
'---9.5.0
Private Sub btnAddressBooks_Click()
    GetContact
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            Me.txtFirmName.SetFocus
        Case 1
            Me.txtFirmAddress.SetFocus
    End Select
End Sub

Private Sub btnAddressBooks_GotFocus()
    OnControlGotFocus Me.btnAddressBooks
End Sub
'---end 9.5.0

Private Sub btnCancel_GotFocus()
    SetControlBackColor btnCancel
End Sub

Private Sub btnDelete_Click()
    
    On Error GoTo ProcError
    EchoOff
    With Me.lstSignatures
        Me.CascadeUpdates = True
        '---9.7.1 - 3880
        If Me.Added And Not Me.Changed Then
            
            Dim mbr As VbMsgBoxResult
            
            mbr = MsgBox("Are you sure you want to delete this signature?", _
                         vbYesNo, _
                         "Pleading Signature")
                         
            If mbr <> vbYes Then Exit Sub
            
            '---9.6.1
            With Me.lstSignatures
                Dim oXAr As XArray
                Set oXAr = lstSignatures.Array
                oXAr.Delete 1, oXAr.UpperBound(1)
                .Array = oXAr
                .Rebind
                Me.lstSignatures.SelectedItem = Me.Signatures.Count - 1
            End With
            '---end 9.6.1
            
            '---cancel add
            ClearFields
            DisplaySignature Val(Trim(xNullToString(.SelectedItem)))
            If IsNull(.SelectedItem) Then
                If Signatures.Count = 1 Then
                    .SelectedItem = 0
                End If
            End If
            ResetValidationValues
            DoEvents
'**********9.3.1 REQUIRED CODE**********
            Me.Added = False
            Me.Changed = False
'***************************************
       ElseIf Me.Changed Then
            '---cancel changes
            ClearFields
            DisplaySignature Val(Trim(xNullToString(.SelectedItem)))
            ResetValidationValues
            DoEvents
'**********9.3.1 REQUIRED CODE**********

'---9.7.1 - 3880
''''            Me.Added = False
            Me.Changed = False
'***************************************
        Else
            If Me.Signatures.Count > 1 Then
                '---delete sig
                DeleteSignature Val(Trim(xNullToString(.SelectedItem)))
                ResetValidationValues
                DoEvents
'**********9.3.1 REQUIRED CODE**********
                Me.Added = False
                Me.Changed = False
'***************************************
            Else
                MsgBox "A pleading requires at least one signature.  You cannot delete this signature.", vbInformation, App.Title
            End If
        End If
    End With
'---9.6.1 - no longer needed
'--9.4.0
''''    lstSignatures.Array.ReDim 0, Me.Signatures.Count - 1, lstSignatures.Array.LowerBound(1), lstSignatures.Array.UpperBound(1)
''''    lstSignatures.Refresh
    '---9.6.1 - ensure form is refreshed
    lstSignatures_RowChange
    
    DoEvents
'---9.7.1 - 3880
''''    Me.Added = False
''''    Me.Changed = False
    
    Me.CascadeUpdates = False
    bDeleteRun = True
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err, "Error deleting signature."
    EchoOn
    Exit Sub
End Sub


Private Sub btnDelete_GotFocus()
    SetControlBackColor btnDelete
End Sub

Private Sub btnDelete_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'    If Not bDeleteRun Then
'        btnDelete_Click
'    End If
'    bDeleteRun = False
End Sub

Private Sub btnFinish_GotFocus()
    SetControlBackColor btnFinish
End Sub

Private Sub btnNew_Click()
    Dim iPos As Integer
    Dim iTargetRow As Integer
    Dim iTargetCol As Integer
    Dim iLimit As Integer
'--9.4.1
    On Error GoTo ProcError
    iLimit = Max(Val(GetMacPacIni("Pleading", "MaxSignatures")), CDbl(mpPleadingSignaturesLimit))
    
    If Signatures.Count = iLimit Then
        MsgBox "There is a limit of " & _
               iLimit & _
               " signatures.", _
               vbInformation & _
               vbOKOnly
        Exit Sub
    End If
    
'---9.6.1

    With Me.lstSignatures
        Dim oXAr As XArray
        Set oXAr = lstSignatures.Array

        oXAr.Insert 1, oXAr.UpperBound(1) + 1
        .Array = oXAr
        .Rebind
        Me.lstSignatures.SelectedItem = Me.Signatures.Count
    End With
 '---end 9.6.1
    
    DisplaySignature
    Me.Added = True

    Me.vsIndexTab1.CurrTab = 0

    '---9.5.0
    Me.cmbSignatureType.SetFocus
    SetControlBackColor Me.cmbSignatureType

'    If Me.saGrid.Visible Then
'        Me.saGrid.SetFocus
'        SetControlBackColor Me.saGrid
'    ElseIf Me.txtSignatureJudge.Visible Then
'        Me.cmbSignatureType.SetFocus
'        SetControlBackColor Me.cmbSignatureType
'    End If
    
    DoEvents
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error adding new signature."
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    On Error GoTo ProcError
    Me.Cancelled = True
    Me.Hide
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnNew_GotFocus()
    SetControlBackColor btnNew
End Sub
Private Sub btnSave_Click()
    
    Dim i As Integer
    Dim iPos As Double
    Dim iTargetRow As Integer
    Dim iTargetCol As Integer
    Dim iSel As Integer
    Dim iHigh As Integer
    Dim iInc As Integer
    
    On Error GoTo ProcError
    EchoOff
    If Me.Added Then
        If Not ValidateType Then Exit Sub
        If Not bValidateSigners Then Exit Sub
        '---9.4.0
        btnSave.Enabled = False
        '---save new added signature
        '---9.4.1
        iHigh = Signatures.HighPosition
        If g_oMPO.db.PleadingSignatureDefs.Item(cmbSignatureType.BoundText).FullRowFormat = True Then
            If iHigh Mod 2 > 0 Then
                iInc = 1
            Else
                iInc = 2
            End If
            iPos = iHigh + iInc
        Else
            iPos = iHigh + 1
        End If
        
        iTargetRow = CInt(iPos / 2) - 1
        iTargetCol = 1
        AddSignature iTargetRow, iTargetCol, CInt(iPos)
        RefreshSelectionControls
        DoEvents
        Me.lstSignatures.Bookmark = Me.Signatures.Count - 1
        Me.Changed = False '9.7.1---3880
        Me.Added = False
        Me.btnNew.SetFocus
        'btnSave.Enabled = True
    Else
        '---change the existing sig
        '---9.4.0
        btnSave.Enabled = False
        m_Document.StatusBar = "Saving..."
        iSel = lstSignatures.SelectedItem
        UpdateSignature lstSignatures.SelectedItem
        RefreshSelectionControls
        '---9.6.2
        'DisplaySignature iSel
        
        m_Document.StatusBar = "Signature changed"
        DoEvents
        '---9.6.2
        lstSignatures.SelectedItem = iSel
        
        Me.Changed = False
    End If
    'DoEvents
    
    SendKeys "+", True
    
    EchoOn
    Exit Sub
ProcError:
    g_oError.Show Err, "Error saving new or changed signature."
    EchoOn
    Exit Sub
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    
    If Not bValidateSigners Then Exit Sub
    
    '---save changes or new sig if dirty
    If Me.Added Or Me.Changed Then
        PromptToSave
        Exit Sub
    End If
'   check to see if signature formats match
    If Signatures.ContainMixedFormats Then
        MsgBox g_xMixedPleadingSignatureWarning, vbExclamation
        Exit Sub
    End If
    DoEvents
    Me.Hide
    Application.ScreenUpdating = False
    UpdateClosingParagraph
'---9.6.2
    Dim i As Integer
    With Signatures '*c
        For i = 1 To .Count '*c
            If .Item(i).Dated = "None" Then _
                .Item(i).Dated = "" '*c
                '---9.7.1 - 2437
                Signatures(i).Signer2 = Signatures(i).Signer
                '---9.7.1 - 4167
'                Signatures(i).ESigner = Signatures(i).Signer
        Next i '*c
    End With '*c
'---End 9.6.2
    If ParentForm Is Nothing Then
        Signatures.Finish
    Else
        Signatures.Refresh , True, True
    End If
    
    'GLOG : 5717 : gm
    Dim oPleading As MPO.CPleading
    Set oPleading = g_oMPO.ExistingPleading
    oPleading.CertificateSigner = Signatures(1).Signer
    oPleading.CertificateDate = Signatures(1).Dated
    oPleading.CertificatePartyName = Signatures(1).PartyName
    oPleading.CertificatePartyTitle = Signatures(1).PartyTitle
    
    Application.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err, "Error finishing signature."
    Exit Sub
End Sub

Private Sub btnSave_GotFocus()
    SetControlBackColor btnSave
End Sub


Private Sub cmbAttorneysFor_ItemChange()
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If cmbAttorneysFor.Text <> m_xAttyFor Then 'And Not Me.Added Then
        Me.Changed = True
        m_xAttyFor = cmbAttorneysFor.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAttorneysFor_GotFocus()
    OnControlGotFocus cmbAttorneysFor
    m_xAttyFor = cmbAttorneysFor.Text
End Sub

Private Sub cmbDateType_GotFocus()
    OnControlGotFocus cmbDateType
    m_xDated = cmbDateType.Text
End Sub

Private Sub cmbDateType_ItemChange()
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If cmbDateType.Text <> m_xDated Then 'And Not Me.Added Then
        Me.Changed = True
        If Me.cmbDateType.Text = "None" Then
            m_xDated = ""
        Else
            m_xDated = Me.cmbDateType.Text
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "Error changing date type."
    Exit Sub
End Sub

Private Sub cmbOneOf_GotFocus()
    OnControlGotFocus cmbOneOf
    m_xOneOf = cmbOneOf.Text
End Sub

Private Sub cmbOneOf_ItemChange()
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If cmbOneOf.Text <> m_xOneOf Then 'And Not Me.Added Then
        Me.Changed = True
        m_xOneOf = cmbOneOf.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdLayout_LostFocus()
'---9.4.1
    With grdLayout.Columns
        If .Item(1).BackColor <> .Item(0).BackColor Then
            .Item(1).BackColor = g_lInactiveCtlColor
        Else
            .Item(1).BackColor = g_lInactiveCtlColor
            .Item(0).BackColor = g_lInactiveCtlColor
        End If
    End With

End Sub

Private Sub lstSignatures_GotFocus()
    OnControlGotFocus lstSignatures
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF5 And btnNew.Enabled Then
        btnNew_Click
    ElseIf KeyCode = vbKeyF6 And btnSave.Enabled Then
        btnSave_Click
    ElseIf KeyCode = vbKeyF7 And btnDelete.Enabled Then
        btnDelete_Click
    '---9.5.0
    ElseIf KeyCode = vbKeyF2 And btnAddressBooks.Visible Then
        btnAddressBooks_Click
    '---end 9.5.0
    Else
        OnFormKeyDown KeyCode
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub lstSignatures_RowChange()
    On Error GoTo ProcError
    If Not Me.Initializing Then
        Me.CascadeUpdates = True
        If Not IsNull(lstSignatures.SelectedItem) Then
            DisplaySignature lstSignatures.SelectedItem
            DoEvents
        End If
        Me.CascadeUpdates = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "Error displaying signature."
    Exit Sub
End Sub

Private Sub saGrid_ColumnEdit(iCol As Integer)
    Me.Changed = True
End Sub

Private Sub saGrid_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    On Error Resume Next
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners Then 'And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
End Sub

Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String, xSignerEmail As String)
    Dim oPerson As CPerson
    On Error Resume Next
    Set oPerson = g_oMPO.People.Item(lAuthorID)
    xLicenseID = oPerson.Licenses.Default.Number
    xAttorneyName = oPerson.FullName '(or use Short Name or whatever client prefers)
    '---9.6.2
    xSignerEmail = oPerson.EMail
End Sub

'Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String)
'    Dim colLic As CLicenses
'
'    On Error Resume Next
'
'    xAttorneyName = g_oMPO.Attorneys.Item(lAuthorID).FullName  '(or use Short Name or whatever client prefers)
'    Set colLic = g_oMPO.Attorneys.Item(lAuthorID).Licenses
'    If colLic.Default Is Nothing Then Exit Sub
'    On Error GoTo ProcError
'    With colLic
'        If .Count <> 0 Then
'            xLicenseID = .Default.Number
'        Else
'            xLicenseID = ""
'        End If
'    End With
'    Exit Sub
'ProcError:
'    g_oError.Show Err, "Error loading attorney license."
'    Exit Sub
'
'End Sub

''''Private Sub saGrid_OnAuthorLoad(lAuthorID As Long, xLicenseID As String, xAttorneyName As String)
''''    Dim oPerson As CPerson
''''    On Error Resume Next
''''    Set oPerson = g_oMPO.People.Item(lAuthorID)
''''    xLicenseID = oPerson.Licenses.Default.Number
''''    xAttorneyName = oPerson.FullName '(or use Short Name or whatever client prefers)
''''End Sub

Private Sub saGrid_SignerChange()
    On Error Resume Next
    Me.Signature.DisplayName = g_oMPO.People(saGrid.Author).FullName
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners Then 'And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
End Sub

Private Sub saGrid_SignersChange()
    On Error Resume Next
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners Then 'And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
End Sub
Private Sub txtClientName_GotFocus()
    OnControlGotFocus txtClientName
    m_xClientName = txtClientName.Text
End Sub

Private Sub txtClientName_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtClientName.Text <> m_xClientName Then 'And Not Me.Added Then
        Me.Changed = True
        m_xClientName = txtClientName.Text
    End If
End Sub

Private Sub cmbSignatureType_ItemChange()
    Dim bEnabled As Boolean
    On Error GoTo ProcError

    SetInterfaceControls
    PositionControls
    If Me.CascadeUpdates = True Then Exit Sub
    If Me.Initializing = True Then Exit Sub
    '---9.7.1 - 3880
    Me.Changed = True
    If Not Me.Added Then
''''        Me.Changed = True
        m_lSigType = cmbSignatureType.BoundText
        Me.cmbDateType.BoundText = g_oMPO.db.PleadingSignatureDefs.Item(m_lSigType).DateFormat
    End If
    '---9.3.1
    If Me.vsIndexTab1.CurrTab = 0 Then
        cmbSignatureType.SetFocus
        cmbSignatureType_GotFocus
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "Error changing signature type."
    Exit Sub
End Sub

Private Sub cmbSignatureType_GotFocus()
    OnControlGotFocus cmbSignatureType
End Sub

Private Sub cmbSignatureType_LostFocus()
    On Error GoTo ProcError
    If Len(cmbSignatureType.BoundText) Then m_lSigType = cmbSignatureType.BoundText
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    '---set display of an TDBCombos by feeding ID as Bound Text
    '---note that you must set bound column property to the ID field
    '---and set the list field property to the Display Text fields
    
    '---load controls with values from first signature in collection
    Dim oOff As COffice
    Dim bNewSig As Boolean '9.7.1 #4153
    On Error Resume Next
    
    Me.vsIndexTab1.CurrTab = 0
    
    On Error GoTo ProcError
    If Not Me.ParentForm Is Nothing Then
        Set oOff = ParentForm.Pleading.Author.Office
        With Me.Signature
            .TypeID = ParentForm.cmbSignatureType.BoundText
            .Dated = ParentForm.cmbDateType.Text
            .PartyTitle = ParentForm.cmbAttorneysFor.Text
            .PartyName = ParentForm.txtClientName
            .JudgeName = ParentForm.txtJudgeName
            If .FirmName = "" Then _
                .FirmName = oOff.FirmNameUpperCase
            If .FirmNameESig = "" Then _
                .FirmNameESig = oOff.FirmNameUpperCase  '9.9.1 #4954
            
            '---9.7.1 - 4030
            If .FirmAddress = "" Then
                If .Definition.OfficeAddressFormat = 0 Then
                     .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
                Else
                     .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
                End If
            End If
            
            .FirmPhone = oOff.Phone1
            .FirmFax = oOff.Fax1
            .FirmState = oOff.State
            .FirmCity = oOff.City
            '---9.4.0
            If .Definition.ShowFirmAddress = True Then
                .FirmID = oOff.FirmID
            Else
                .FirmID(mpTrue) = ""
            End If
            .SignerEMail = ParentForm.Pleading.Author.EMail
            Me.lstSignatures.Text = .DisplayName
            DoEvents
            DisplaySignature 0, 1
        End With
    ElseIf Signatures.Count = 0 Then
        Set oOff = g_oMPO.Offices.Default
        With cmbSignatureType
            If Len(.BoundText) = 0 Then .BoundText = .Array.value(0, 1)
        End With
        DoEvents
        With cmbDateType
            If Len(.BoundText) = 0 Then .BoundText = mpDefaultDateFormat
        End With
        DoEvents
        Me.cmbAttorneysFor.Text = "Defendant"
        '---9.7.1 - 4030 - leave this one alone, there is no sig defined, so we use this default format
        Me.txtFirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
        
        Me.txtFirmName = oOff.FirmName  '9.7.1 #4035
        Me.txtFirmSlogan = oOff.Slogan  '*c
        Me.txtFirmPhone = oOff.Phone1
        Me.txtFirmFax = oOff.Fax1
        Me.txtFirmCity = oOff.City
        Me.txtFirmState = oOff.State
        '---9.4.0
        Me.txtFirmID = oOff.FirmID
       
    Else
        '9.7.1 #4153
        Dim oTemplate As MPO.CTemplate
        Dim xClass As String
        Dim lID As Long
        On Error Resume Next
        Set oTemplate = g_oMPO.Templates(ActiveDocument.AttachedTemplate)
        On Error GoTo ProcError
        If Not oTemplate Is Nothing Then
            xClass = oTemplate.ClassName
        End If
        If UCase(xClass) <> "CPLEADING" And Me.Signature.Signer = "" Then
            On Error Resume Next
            If oTemplate Is Nothing Then
                lID = 0
            Else
                '9.7.1020 #4547
                'if this is a signature for letter, and the author
                'is not an attorney it raises an error message.
                If g_oMPO.Templates(g_xPleadingTemplate).DefaultAuthor.IsAttorney = True Then
                    lID = g_oMPO.Templates(g_xPleadingTemplate).DefaultAuthor.ID
                Else
                    lID = 0
                End If
            End If
            If lID = 0 Then
                lID = g_oMPO.Attorneys.First.ID
            End If
            If g_oMPO.UseDefOfficeInfo Then
                Set oOff = g_oMPO.Offices.Default
            Else
                Set oOff = g_oMPO.People(lID).Office
            End If
            DoEvents
            With Signature
                .TypeID = Me.PleadingType.DefaultSignatureType
                cmbDateType.BoundText = .Definition.DateFormat
                DoEvents
                .Dated = cmbDateType.Text
                saGrid.Author = lID
                DoEvents
                Set .Signers = Me.saGrid.Signers
                .Signer = saGrid.SignerText
                m_xSigners = saGrid.SignerText & saGrid.SignersText
                .PartyTitle = "Defendant"
                .FirmName = oOff.FirmNameUpperCase
                .FirmNameESig = oOff.FirmNameUpperCase  '9.9.1 #4954
''''                .FirmAddress = oOff.AddressOld(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
                
                '---9.7.1 - 4030
                If .FirmAddress = "" Then
                    If .Definition.OfficeAddressFormat = 0 Then
                         .FirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
                    Else
                         .FirmAddress = xOfficeAddress(oOff, .Definition.OfficeAddressFormat)
                    End If
                End If
                
                .FirmPhone = oOff.Phone1
                .FirmFax = oOff.Fax1
                .FirmCity = oOff.City
                .FirmState = oOff.State
                lstSignatures.SelectedItem = 0
                lstSignatures.Array.value(0, 1) = .Key
            End With
            DisplaySignature 0, 0
            bNewSig = True
            Me.Added = False
            Me.Changed = False
        Else
            m_xDated = Me.Signature.Dated
            DisplaySignature 0, 1
        End If
    End If
    
    RefreshSelectionControls False
    RefreshSelectionControlsFromGrid
    Me.chkIncludeClosingParagraph = Abs(Me.Signatures.ClosingParagraph)
    
    DoEvents
    
 ''---this will force new input if there's only one signature
    If Not bNewSig Then '9.7.1 #4153
        btnNew_Click
        Me.Added = True
        btnNew.Enabled = False
        btnSave.Enabled = True
        Me.btnDelete.ToolTipText = mpCancelAdd
        '---9.6.2
        If Signature.Dated = "" Then '*c
            Me.cmbDateType.Text = "None" '*c
        Else '*c
            Me.cmbDateType.Text = Signature.Dated
        End If '*c
        
        
        '---End 9.6.2
        Me.cmbDateType.Text = Signature.Dated
        Me.cmbAttorneysFor.Text = Signature.PartyTitle
        Me.cmbSignatureType.BoundText = Signature.TypeID
        
        '---9.6.2
        '---9.7.1 - 4112
        Me.cmbOneOf.SelectedItem = 0
        DoEvents
        '---9.7.1 - 3880
        Me.Changed = False
    End If
    
    Me.CascadeUpdates = False
    Me.Initializing = False
    If Me.ParentForm Is Nothing Then
'       move dialog to last position
        'mpbase2.MoveToLastPosition Me
    Else
        Me.Top = Me.ParentForm.Top
        Me.Left = Me.ParentForm.Left
    End If
    Application.ScreenUpdating = True
    EchoOn
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    g_oError.Show Err, "Error activating pleading signature form."
    Exit Sub
    End Sub

Private Sub Form_Load()
    Dim xKey As String
    Dim oList As mpDB.CList
    Dim m_Sig As MPO.CPleadingSignature
    Dim oArray As XArray
    Dim xOrdSuffix As String
    Dim i As Integer

    On Error GoTo ProcError
    Me.Initializing = True
    Me.CascadeUpdates = False

'---park form offscreen during form init
    Application.ScreenUpdating = False
    LockWindowUpdate Me.hwnd
    Me.Top = 20000
    If Signatures Is Nothing Then
        Signatures = g_oMPO.NewPleadingSignatures
        Signatures.LoadValues
        If Signatures.Count = 0 Then Signatures.Add
    End If

    Set m_Document = New MPO.CDocument
    Set m_oDB = g_oMPO.db
    Set xARLayout = g_oMPO.NewXArray
    Set xARSigs = g_oMPO.NewXArray
    Signature = Me.Signatures(1)

    If Not Me.ParentForm Is Nothing Then
        Me.PleadingType = Me.ParentForm.Pleading.Definition
    Else
        Me.PleadingType = m_oDB.PleadingDefs.ItemFromCourt( _
            Me.Level0, Me.Level1, Me.Level2, Me.Level3)
         If Me.Signature.TypeID = 0 Then
            Me.Signature.TypeID = Me.PleadingType.DefaultSignatureType
        End If
        
   End If
    
 '---set signers grid with bar id props from def
        With Me.Signature.Definition
            If Me.Signature.Position = 0 Then Me.Signature.Position = .DefaultPosition
            saGrid.IncludeBarID = .IncludeBarID
            saGrid.DisplayBarID = .IncludeBarID
            saGrid.BarIDPrefix = .BarIDPrefix
            saGrid.BarIDSuffix = .BarIDSuffix
            
            '---9.6.2
            '---9.7.1 - 4188
            saGrid.DisplayEMail = .AllowSignerEmailInput
            saGrid.IncludeSignerEmail = .IncludeSignerEmail
            saGrid.SignerEmailPrefix = .SignerEmailPrefix
            saGrid.SignerEmailSuffix = .SignerEmailSuffix
            
            '---9.7.1 - 4097
            saGrid.SignerNameCase = .SignerNameCase
        End With
     
    With g_oMPO.Lists
'---load date array with non-jurat choices
'        Me.cmbDateType.Array = _
'            .Item("DateFormats").ListItems.Source
        
'---if you want to load/support Jurat type dates, unrem code below and load date combo with result
        Set oArray = g_oMPO.NewXArray
        Set oArray = .Item("DateFormatsPleading").ListItems.Source
        
'       get ordinal suffix based on current date - eg "rd", "th"
        xOrdSuffix = GetOrdinalSuffix(Day(Date))
        
'       replace all instances of ordinal variable
'       with ordinal suffix-this is done here because
'       there is no way in vb to format a dateas an ordinal
        For i = oArray.LowerBound(1) To oArray.UpperBound(1)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%o%", xOrdSuffix)
            oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%or", xOrdSuffix)
        Next i
        
        Me.cmbDateType.Array = oArray
        '---9.4.1
        Me.cmbOneOf.Array = g_oMPO.Lists("PleadingPartiesOneOf").ListItems.Source
        '---end 9.4.1
        Set oList = .Item("PleadingSignatures")
        With oList
            .FilterField = "tblPleadingSignatureAssignments.fldPleadingTypeID"
            .FilterValue = Me.PleadingType.ID
            .Refresh
            Me.cmbSignatureType.Array = .ListItems.Source
        End With
    End With
    ResizeTDBCombo Me.cmbDateType, 6
    ResizeTDBCombo Me.cmbSignatureType, 6
    '---9.4.1
    ResizeTDBCombo Me.cmbOneOf, 6

'---initialize controls
    g_oMPO.Attorneys.Refresh
    Me.saGrid.Attorneys = g_oMPO.Attorneys.ListSource
    Me.cmbAttorneysFor.Array = g_oMPO.Lists("PleadingParties").ListItems.Source
    ResizeTDBCombo Me.cmbAttorneysFor, 10

'---offices initialization
    Me.txtFirmName = g_oMPO.Offices.Default.FirmName    '9.7.1 #4035
    Me.txtFirmSlogan = g_oMPO.Offices.Default.Slogan '*c
    
    Exit Sub
ProcError:
    Application.ScreenUpdating = True
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.Form_Load"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ProcError
    'SetLastPosition Me

    Set xARLayout = Nothing
    Set xARSigs = Nothing
    Set m_Sigs = Nothing
    Set m_oSig = Nothing
    Set m_Document = Nothing
    Set frmPleadingSignature = Nothing
    Set m_oDB = Nothing
    Set g_oPrevControl = Nothing
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error unloading form."
    Exit Sub
End Sub
Private Sub grdLayout_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
    Dim i As Integer
    
    On Error GoTo ProcError
    is_SourceRow = RowBookmark
    is_SourceCol = ColIndex

    If grdLayout.Text = Empty Then
        Exit Sub
    End If
    
    'If Me.Signature.Definition.FullRowFormat Then
    '---9.4.1
    If Me.Signatures(xARLayout.value(RowBookmark, ColIndex, 1)).Definition.FullRowFormat Then
        If ColIndex = 0 Then Exit Sub
    End If
    
    With xARLayout
    
        ReDim xDragFrom(0, 0, .UpperBound(3))
        grdLayout.MarqueeStyle = dbgHighlightCell
        
        For i = 0 To .UpperBound(3)
            xDragFrom(0, 0, i) = .value(RowBookmark, ColIndex, i)
        Next i
        
        SetLayoutGrid Me.Signatures(.value(RowBookmark, ColIndex, 1)).Definition.FullRowFormat
        DraggedSignature = Nothing
        DraggedSignature = Me.Signatures(.value(RowBookmark, ColIndex, 1))

    End With

' Use Visual Basic manual drag support
    grdLayout.Drag vbBeginDrag
    Exit Sub
ProcError:
    g_oError.Show Err, "Error dragging cell."
    Exit Sub
End Sub

Private Function bKeyMoveComplete(KeyCode As Integer, _
                            is_SourceRow As Integer, _
                            is_SourceCol As Integer) As Boolean
    Dim idestCol As Integer
    Dim idestRow As Integer
    Dim temp() As String
    Dim i As Integer
    Dim j As Integer
    
'---this sub supports a movement of sig position via arrow keystrokes in layout grid
'---not currently called/supported
    On Error GoTo ProcError

    ReDim xDragFrom(0, 0, xARLayout.UpperBound(3))

    grdLayout.MarqueeStyle = dbgHighlightCell
    'xDragFrom = grdLayout.Columns(ColIndex).Text
    For i = 0 To xARLayout.UpperBound(3)
        xDragFrom(0, 0, i) = xARLayout(grdLayout.Bookmark, grdLayout.Col, i)
    Next i

'---Get coordinates of drop zone
    idestCol = grdLayout.Col
    idestRow = grdLayout.Bookmark
    Select Case KeyCode
        Case 37 '---left
            idestCol = idestCol - 1
            If idestCol < 0 Then idestCol = 0
        Case 38 '---up
            idestRow = idestRow - 1
            If idestRow < 0 Then idestRow = 0
        Case 39 '---right
            idestCol = idestCol + 1
            If idestCol > xARLayout.UpperBound(2) Then idestCol = xARLayout.UpperBound(2)
        Case 40 '---down
            idestRow = idestRow + 1
'---create row space in array if necessary
            If idestRow > xARLayout.UpperBound(1) Then
                xARLayout.Insert 1, xARLayout.UpperBound(1) + idestRow
            End If
        End Select

'---empty out source item
    For i = 0 To xARLayout.UpperBound(3)
        xARLayout(is_SourceRow, is_SourceCol, i) = Empty
    Next i

'---move down only within dropped columns
    ReDim temp(0, 0, xARLayout.UpperBound(3)) As String

    For i = idestRow To idestRow
        For j = 0 To xARLayout.UpperBound(3)
            temp(0, 0, j) = xARLayout.value(i, idestCol, j)
            xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
            xDragFrom(0, 0, j) = temp(0, 0, j)
        Next j
    Next i

    If temp(0, 0, 0) <> Empty Then
        For i = idestRow + 1 To xARLayout.UpperBound(1)
            For j = 0 To xARLayout.UpperBound(3)
                temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                xDragFrom(0, 0, j) = temp(0, 0, j)
            Next j
        Next i
    End If

    grdLayout.Bookmark = Null
    grdLayout.Rebind
    grdLayout.MarqueeStyle = dbgHighlightCell
    'grdLayout.Bookmark = idestRow
    grdLayout.Col = idestCol
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.bKeyMoveComplete"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Private Sub grdLayout_DragDrop(Source As Control, x As Single, y As Single)
    Dim idestCol As Integer
    Dim idestRow As Integer
    Dim i, j, r, c As Integer
    Dim temp() As String
    Dim vSourceID As Variant
    Dim iSourcePosition As Integer
    Dim iDestPosition As Integer
    Dim m_Sig As MPO.CPleadingSignature

    On Error Resume Next
    
    r = Val(grdLayout.RowContaining(y))
    c = grdLayout.ColContaining(x)

'---don't allow drop if cell is occupied
    If Len(grdLayout.Text) Then
        Exit Sub
    ElseIf r < 0 Or c < 0 Then
        Exit Sub
    End If
    
    On Error GoTo ProcError
    If Me.DraggedSignature.Definition.FullRowFormat Then
        If c = 0 Then Exit Sub
    Else
        If c + r < 1 And Me.Signatures(1).Dated <> "" Then
            MsgBox "This cell is reserved for the date.  A signature block cannot be placed in this location.", vbInformation, App.Title
            Exit Sub
        End If
    End If

'---create row space in array if necessary
    If r > xARLayout.UpperBound(1) Then
        xARLayout.Insert 1, xARLayout.UpperBound(1) + r
    End If

    If Source.Name = "grdLayout" Then

'---Get coordinates of drop zone
        idestCol = Abs(grdLayout.ColContaining(x))
        idestRow = grdLayout.RowBookmark(grdLayout.RowContaining(y))
'---exit if invalid row
        If idestRow < 0 Then Exit Sub
        iDestPosition = (idestRow + 1) * (idestCol + 1)

'---Capture ID, Position of source item
        If Signatures.Count > 0 Then
            vSourceID = xARLayout(is_SourceRow, is_SourceCol, 1)
            Set m_Sig = Signatures.Item(vSourceID)
            If Not m_Sig Is Nothing Then
                iSourcePosition = m_Sig.Position
            Else
                Exit Sub
            End If
        End If

'---empty out source item in grid
        For i = 0 To xARLayout.UpperBound(3)
            xARLayout(is_SourceRow, is_SourceCol, i) = Empty
        Next i
        grdLayout.Rebind

'---create row space in array if necessary
        If idestRow > xARLayout.UpperBound(1) Then
            xARLayout.Insert 1, xARLayout.UpperBound(1) + idestRow
        End If

        m_Document.StatusBar = "Moving..."

'---move down only within dropped columns
        ReDim temp(0, 0, xARLayout.UpperBound(3)) As String

        For i = idestRow To idestRow
            For j = 0 To xARLayout.UpperBound(3)
                temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                xDragFrom(0, 0, j) = temp(0, 0, j)
            Next j
        Next i
 
        If temp(0, 0, 0) <> Empty Then
            For i = idestRow + 1 To xARLayout.UpperBound(1)
                For j = 0 To xARLayout.UpperBound(3)
                    temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                    xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                    xDragFrom(0, 0, j) = temp(0, 0, j)
                Next j
            Next i
        End If
        
        grdLayout.Bookmark = Null
        grdLayout.Rebind

        grdLayout.MarqueeStyle = dbgHighlightCell
        grdLayout.Bookmark = idestRow
        grdLayout.Col = idestCol

        m_Document.StatusBar = "Signature moved"
        RefreshSigPosition
        RefreshSelectionControlsFromGrid
        
        '---9.5.0
        If xARLayout(0, 0) & xARLayout(0, 1) = "" Then
            If Me.Signature.Definition.FullRowFormat = False Then
                MsgBox "First row of signature layout is empty.  No date will display.  You must have a signature in the second column of the first row to display a signature date.", vbInformation, App.Title
            
            End If
        End If
        '---End 9.5.0
        
    End If
    SendKeys "+", True
    grdLayout.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err, "Error completing drag and drop operation."
    Exit Sub
End Sub
Private Sub grdLayout_DragOver(Source As Control, x As Single, y As Single, State As Integer)
' DragOver provides  visual feedback

    Dim dragFrom As String
    Dim overCol As Integer
    Dim overRow As Long

    On Error GoTo ProcError
    overCol = grdLayout.ColContaining(x)
    overRow = grdLayout.RowContaining(y)
    
    If overCol >= 0 Then grdLayout.Col = overCol
    If overRow >= 0 Then grdLayout.Bookmark = overRow
    grdLayout.MarqueeStyle = dbgHighlightCell

    Exit Sub
ProcError:
    g_oError.Show Err, "Error on drag over."
    Exit Sub
End Sub

Private Sub grdLayout_GotFocus()
'---9.4.1
    OnControlGotFocus grdLayout
    With grdLayout.Columns
        If .Item(1).BackColor <> .Item(0).BackColor Then
            .Item(1).BackColor = g_lActiveCtlColor
        Else
            .Item(1).BackColor = g_lActiveCtlColor
            .Item(0).BackColor = g_lActiveCtlColor
        End If
    End With
End Sub


Private Sub grdLayout_KeyDown(KeyCode As Integer, Shift As Integer)

    On Error GoTo ProcError
    If KeyCode = 9 Then GridTabOut Me.grdLayout, False, 1
    If KeyCode = 40 And Shift = 2 Then Me.PopupMenu mnuLayoutGrid

'---this code calls the key-operated d&d
'        If Shift = 3 Then
'            If KeyCode >= 37 And KeyCode <= 40 Then
'                bKeyMoveComplete KeyCode, grdLayout.Bookmark, grdLayout.Col
'                KeyCode = 0
'            End If
'        End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub grdLayout_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'---sets bookmark and col
    Dim overCol As Integer
    Dim overRow As Integer
    Dim iPos As Integer
    Dim xKey As String

    On Error GoTo ProcError
    overCol = grdLayout.ColContaining(x)
    overRow = grdLayout.RowContaining(y)
    If overCol >= 0 Then grdLayout.Col = overCol
    If overRow >= 0 Then grdLayout.Bookmark = overRow

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_GotFocus()
    OnControlGotFocus saGrid
    m_xSigners = saGrid.SignerText & saGrid.SignersText
End Sub

Private Sub saGrid_LostFocus()
    Dim oOff As COffice
    Dim oPerson As CPerson
    On Error GoTo ProcError
    
    Set oPerson = g_oMPO.db.People(saGrid.Author)
    
    If Not oPerson Is Nothing Then
        If g_oMPO.UseDefOfficeInfo Then
            Set oOff = g_oMPO.Offices.Default
        Else
            Set oOff = oPerson.Office
        End If
'---Fill Address fields if signer for sig is valid MacPac Person
        With oOff
            
            '---9.7.1 - 4030
            If Me.Signature.Definition.OfficeAddressFormat = 0 Then
                 Me.txtFirmAddress = oOff.AddressOLD(vbCrLf, mpOfficeAddressFormat_CountryIfForeign)
            Else
                 Me.txtFirmAddress = xOfficeAddress(oOff, _
                                                    Me.Signature.Definition.OfficeAddressFormat, _
                                                    True)
            End If
            
            Me.txtFirmCity = .City
            Me.txtFirmState = .State
            Me.txtFirmFax = .Fax1
            Me.txtFirmID = .FirmID
            Me.txtFirmPhone = .Phone1
            Me.txtSignerEmail = oPerson.EMail
            Me.txtFirmName = .FirmName
            Me.txtFirmSlogan = .Slogan  '*c
            
        End With
    End If
    
    If Me.CascadeUpdates = True Then Exit Sub
    
    '---9.7.1 - 4081
    If m_oDB.PleadingSignatureDefs.Item(cmbSignatureType.BoundText).DisplaySignerName = True Then
        txtSignerName = saGrid.SignerText
    End If
    
    '---9.7.1 - 3880
    If saGrid.SignerText & saGrid.SignersText <> m_xSigners Then 'And Not Me.Added Then
        Me.Changed = True
        m_xSigners = saGrid.SignerText & saGrid.SignersText
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = 28 Then KeyCode = 0
    If Me.CascadeUpdates = True Then Exit Sub
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
Private Sub saGrid_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim bEnabled As Boolean
    Dim lTestID As Long

    On Error GoTo ProcError
    If Button = 2 Then
        With saGrid
            On Error Resume Next
            lTestID = m_oDB.Attorneys(.Author).ID
        '   enable specified menu items only if author
        '   is currently in the db
            If lTestID = 0 Then
                bEnabled = False
            Else
                bEnabled = True
            End If
        End With
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_AuthorNotInList(lAuthorID As Long)
    MsgBox "Author " & lAuthorID & " not in loaded list.", vbInformation, "Signing Attorney Grid"
End Sub


Private Sub saGrid_OnLicenseColDropDown(lSignerID As Long)
    On Error GoTo ProcError
    saGrid.SignerBarIDs = g_oMPO.Attorneys.Item(lSignerID).Licenses.ListSource
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub saGrid_SelectedSigner(lAuthorID As Long, bCancelChange As Boolean)
    On Error GoTo ProcError
    If Me.Signature.ID <= 2 Then
        If Not Me.Added Then
            MsgBox "Input signer cannot be the author of this document.  Please select an author from the dropdown list", vbInformation, "Signing Attorney Grid"
            bCancelChange = True
        End If
    Else
        If Me.CascadeUpdates = True Then Exit Sub
        '---9.7.1 - 3880
        If saGrid.SignerText & saGrid.SignersText <> m_xSigners Then 'And Not Me.Added Then
            Me.Changed = True
            m_xSigners = saGrid.SignerText & saGrid.SignersText
        End If
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "Error selecting signer."
    Exit Sub
End Sub

Private Sub txtFirmAddress_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtFirmAddress.Text <> m_xFirmAddress Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmAddress = txtFirmAddress.Text
    End If
End Sub

Private Sub txtFirmPhone_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtFirmPhone.Text <> m_xFirmPhone Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmPhone = txtFirmPhone.Text
    End If
End Sub

Private Sub txtFirmCity_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtFirmCity.Text <> m_xFirmCity Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmCity = txtFirmCity.Text
    End If
End Sub

Private Sub txtFirmState_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtFirmState.Text <> m_xFirmState Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmState = txtFirmState.Text
    End If
End Sub

Private Sub txtSignerEmail_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtSignerEmail.Text <> m_xSignerEmail Then 'And Not Me.Added Then
        Me.Changed = True
        m_xSignerEmail = txtSignerEmail.Text
    End If
End Sub

Private Sub txtFirmFax_Change()
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtFirmFax.Text <> m_xFirmFax Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmFax = txtFirmFax.Text
    End If
End Sub

Private Sub txtFirmAddress_DblClick()
    Dim vAddress
    Dim vName
    On Error GoTo ProcError
    '---9.5.0
    GetContact
    '---9.5.0
'''    g_oMPO.GetContactNameAddress vAddress, vName
'''    txtFirmAddress = vAddress
'''    txtFirmName = UCase(vName)
    Exit Sub
ProcError:
    g_oError.Show Err, "Error loading contacts."
    Exit Sub
End Sub

Private Sub txtFirmAddress_GotFocus()
    OnControlGotFocus txtFirmAddress
    m_xFirmAddress = txtFirmAddress
End Sub

Private Sub txtFirmPhone_GotFocus()
    OnControlGotFocus txtFirmPhone
    m_xFirmPhone = txtFirmPhone
End Sub

Private Sub txtFirmState_GotFocus()
    OnControlGotFocus txtFirmState
    m_xFirmState = txtFirmState
End Sub

Private Sub txtFirmCity_GotFocus()
    OnControlGotFocus txtFirmCity
    m_xFirmCity = txtFirmCity
End Sub

Private Sub txtFirmFax_GotFocus()
    OnControlGotFocus txtFirmFax
    m_xFirmFax = txtFirmFax
End Sub

Private Sub txtSignerEmail_GotFocus()
    OnControlGotFocus txtSignerEmail
    m_xSignerEmail = txtSignerEmail
End Sub

Private Sub txtFirmID_Change()
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtFirmID.Text <> m_xFirmID Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmID = txtFirmID.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFirmID_GotFocus()
    OnControlGotFocus txtFirmID
    m_xFirmID = txtFirmID
End Sub

Private Sub txtFirmName_Change()
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    If Me.Initializing = True Then Exit Sub
    '---9.7.1 - 3880
    If txtFirmName.Text <> m_xFirmName Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmName = txtFirmName.Text
        m_xFirmNameESig = txtFirmName.Text  '9.9.1 #4954
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFirmName_GotFocus()
    OnControlGotFocus txtFirmName
    m_xFirmName = txtFirmName
    m_xFirmNameESig = txtFirmName  '9.9.1 #4954
End Sub

'*c
Private Sub txtFirmSlogan_Change()
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    If Me.Initializing = True Then Exit Sub
    If txtFirmSlogan.Text <> m_xFirmSlogan Then 'And Not Me.Added Then
        Me.Changed = True
        m_xFirmSlogan = txtFirmSlogan.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFirmSlogan_GotFocus()
    OnControlGotFocus txtFirmSlogan
    m_xFirmSlogan = txtFirmSlogan
End Sub

Private Sub txtSignatureJudge_Change()
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    '---9.7.1 - 3880
    If txtSignatureJudge.Text <> m_xJudgeName Then 'And Not Me.Added Then
        Me.Changed = True
        m_xJudgeName = txtSignatureJudge.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtSignatureJudge_GotFocus()
    OnControlGotFocus txtSignatureJudge
    m_xJudgeName = txtSignatureJudge
End Sub

Private Sub txtSignerName_GotFocus()
    '---9.7.1 - 4081
    OnControlGotFocus txtSignerName
    m_xSignerName = txtSignerName
End Sub

Private Sub txtSignerName_Change()
    '---9.7.1 - 4081
    On Error GoTo ProcError
    If Me.CascadeUpdates = True Then Exit Sub
    If Me.Initializing = True Then Exit Sub
    '---9.7.1 - 3880
    If txtSignerName.Text <> m_xSignerName Then
        Me.Changed = True
        m_xSignerName = Me.txtSignerName.Text
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub

End Sub

Private Sub vsIndexTab1_Click()
    On Error GoTo ProcError
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            Me.btnNew.SetFocus
        Case 1
            If Me.txtFirmAddress.Visible = True Then Me.txtFirmAddress.SetFocus
        Case 2
            '---9.6.2 - prompt or save before allowing trip to layout grid tab
            If Me.Added Or Me.Changed Then
                MsgBox "You must first save your added or changed signature", vbInformation, App.Title
                Me.vsIndexTab1.CurrTab = 0
                Me.btnSave.SetFocus
                Exit Sub
            Else
                Me.grdLayout.SetFocus
            End If
    End Select
ProcError:
End Sub
Private Sub ClearFields()
    With Me
        .txtClientName = ""
        .txtFirmAddress = ""
        .txtFirmPhone = ""
        .txtFirmName = ""
        .txtFirmSlogan = ""
        .txtFirmID = ""
        .txtSignatureJudge = ""
    End With
End Sub
Private Sub DeleteSignature(iRow As Integer, Optional iCol As Integer = -9999)
    Dim xKey As String
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim iPos As Integer
    Dim mbr As VbMsgBoxResult
    Dim bIsLast As Boolean
    
    mbr = MsgBox("Are you sure you want to delete this signature?", _
                 vbYesNo, _
                 "Pleading Signature")
                 
    If mbr <> vbYes Then Exit Sub
    
    If iRow = lstSignatures.Array.UpperBound(1) Then _
        bIsLast = True
        
    Dim m_Sig As MPO.CPleadingSignature

    On Error Resume Next
    If iCol = -9999 Then
        xKey = lstSignatures.Array.value(iRow, 1)
    Else
        xKey = grdLayout.Array.value(iRow, iCol, 1)
    End If

    On Error GoTo ProcError
'---fill controls with properties
    If Len(xKey) > 0 Then
        Set m_Sig = Signatures.Item(xKey)
        iPos = m_Sig.Position
    End If
    If Not m_Sig Is Nothing Then
        Signatures.Remove m_Sig.Key
        On Error Resume Next
''---service combo box
        With Me.lstSignatures.Array
            .Delete (1), iRow
            lstSignatures.Rebind
            If Signatures.Count Then
                'lstSignatures.Row = 0
                lstSignatures.BoundText = .value(.UpperBound(1), 0)
            End If
            DisplaySignature Signatures.Count - 1
            lstSignatures.SelectedItem = Signatures.Count - 1
            UpdateCounter
        End With

'---service grid
        With grdLayout
            For i = xARLayout.LowerBound(1) To xARLayout.UpperBound(1)
                For j = xARLayout.LowerBound(2) To xARLayout.UpperBound(2)
                    If iPos = (2 * i) + (j + 1) Then
                        '---empty grid array & cell of deleted item
                        For k = xARLayout.LowerBound(3) To xARLayout.UpperBound(3)
                            xARLayout(i, j, k) = Empty
                        Next
                    End If
                Next j
            Next
            .Rebind
        End With
        MsgBox "Signature deleted.  Please check layout grid for position of remaining signatures.", vbInformation, App.Title
        Me.lstSignatures.SetFocus
   End If
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.DeleteSignature"), _
              g_oError.Desc(Err.Number)
    Application.ScreenUpdating = True
    Exit Sub
End Sub
Private Sub UpdateSignersFromGrid(ByRef m_Sig As MPO.CPleadingSignature, grdSig As SigningAttorneysGrid)
    Dim xTemp As String
    Dim xarTemp As XArray
    Dim i As Integer
    Dim j As Integer
    Dim x As Integer
    Dim y As Integer
    
'---this assigns signature .Signer and .Signers props

    On Error GoTo ProcError
    With grdSig
        
        
'---9.3.1
        With m_Sig.Definition
            grdSig.IncludeBarID = .IncludeBarID
            grdSig.DisplayBarID = .IncludeBarID
            grdSig.BarIDPrefix = .BarIDPrefix
            grdSig.BarIDSuffix = .BarIDSuffix
        End With
        
        m_Sig.OtherSigners = xSubstitute(.OtherSignersText, .Separator, vbLf)
        m_Sig.Signer = .SignerText
        
        '---9.7.1 - 4081
        If m_Sig.Definition.DisplaySignerName = True Then
            m_Sig.InputSigner = txtSignerName
            m_Sig.Signer = txtSignerName
        End If
        
        '---9.4.1
        m_Sig.Signer2 = .SignerText
        '---9.7.1 - es
        saGrid.IncludeSignerEmail = False
        saGrid.IncludeBarID = False
        m_Sig.ESigner = .SignerText
        saGrid.IncludeSignerEmail = m_Sig.Definition.IncludeSignerEmail
        saGrid.IncludeBarID = m_Sig.Definition.IncludeBarID
        m_Sig.AllSigners = xSubstitute(.SignersText, .Separator, vbLf)
        
        '---end 9.4.1
        Set m_Sig.Signers = grdSig.Signers
    End With

    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.UpdateSignersFromGrid"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub ResetValidationValues()
    m_lSigType = 0
    m_xDated = ""

End Sub
Public Sub RefreshSigPosition()
    Dim oSig As MPO.CPleadingSignature
    Dim xarTemp As XArray
    Dim i, j As Integer
'---If DD performed, reassign sig.position based on grid position

    On Error GoTo ProcError
    Set xarTemp = Me.grdLayout.Array

   With xarTemp
        For i = .LowerBound(1) To .UpperBound(1)
            For j = .LowerBound(2) To .UpperBound(2)
                On Error Resume Next
                If .value(i, j, 0) <> "" Then
                    Set oSig = Me.Signatures(.value(i, j, 1))
                    If Not oSig Is Nothing Then
                        oSig.Position = (2 * i) + j + 1
                        Set oSig = Nothing
                    End If
                End If
            Next j
        Next i
    End With
    
    Set xarTemp = Nothing
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.RefreshSigPosition"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Public Sub RefreshSelectionControls(Optional bRefreshCombo As Boolean = True)
    Dim m_Sig As MPO.CPleadingSignature
    Dim xDisplayName As String
    Dim xKey As String
    Dim iPos As Integer
    Dim iRow As Integer
    Dim iCol As Integer
    Dim iCount As Integer
    Dim iSigLimit As Integer
    Dim iLimit As Integer
    
    iLimit = Max(Val(GetMacPacIni("Pleading", "MaxSignatures")), CDbl(mpPleadingSignaturesLimit))
       
'---fills grid and listbox with displayname/selection key arrays
'---load layout grid & sig list with signatures from signature collection
   On Error Resume Next
   If Signature.Definition.FullRowFormat Then
        iSigLimit = iLimit
    Else
        iSigLimit = iLimit / 2 + 1
    End If
    On Error GoTo ProcError
    xARLayout.ReDim 0, iSigLimit, 0, 1, 0, 1
    xARSigs.ReDim 0, Signatures.Count - 1, 0, 1
    xARLayout.value(0, 0, 0) = ""
    xARLayout.value(0, 0, 1) = ""
    iCount = 0
    For Each m_Sig In Signatures
        iPos = m_Sig.Position
        If m_Sig.Signer <> "" Then
'---9.6.2
''''            If InStr(m_Sig.Signer, ",") Then
''''                xDisplayName = Left(m_Sig.Signer, InStr(m_Sig.Signer, ",") - 1)
''''            Else
                xDisplayName = m_Sig.Signer
''''            End If
        
        Else
            xDisplayName = m_Sig.DisplayName
        End If
        
        If Not Me.Initializing Then
            If m_Sig.DisplayName = "" Then
                m_Sig.DisplayName = "No Display Name"
            Else
                m_Sig.DisplayName = xDisplayName
            End If
        Else
            xDisplayName = m_Sig.DisplayName
        End If
        
        xKey = m_Sig.Key
    '---calculate grid position based on iPos Value
        If iPos / 2 - Int(iPos / 2) = 0.5 Then
            iRow = CInt((iPos + 1) / 2) - 1
            iCol = 0
        Else
            iRow = CInt(iPos / 2) - 1
            iCol = 1
        End If
     '---update grid array
        xARLayout.value(iRow, iCol, 0) = xDisplayName
        xARLayout.value(iRow, iCol, 1) = xKey
    '---update list array
        If bRefreshCombo Then
            xARSigs.value(iCount, 0) = xDisplayName
            xARSigs.value(iCount, 1) = xKey
            iCount = iCount + 1
        End If
    Next m_Sig

    With Me.grdLayout
        .Array = xARLayout
        .Rebind
        .Bookmark = Null
        .Col = 1
    End With
    
    If bRefreshCombo Then
        With Me.lstSignatures
'---9.4.1
            Me.RefreshSelectionControlsFromGrid
            .Array = xARSigs
            .Rebind
        End With
    End If
    UpdateCounter
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.RefreshSelectionControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Public Sub RefreshSelectionControlsFromGrid()
    Dim i, j, k As Integer
    Dim xARSigs As XArray
    Dim iCount As Integer
    Dim iSigCount As Integer
    Dim xKey As String
   
'---refreshes sig combo box after grid drag and drop operation
    On Error GoTo ProcError
    Set xARSigs = g_oMPO.NewXArray
    iSigCount = Me.Signatures.Count

    With grdLayout.Array
        xARSigs.ReDim .LowerBound(1), .UpperBound(1), .LowerBound(2), .UpperBound(2)
        
        For i = .LowerBound(1) To .UpperBound(1)
            For j = .LowerBound(2) To .UpperBound(2)
                If .value(i, j, 1) <> "" Then
                    '---make sure you've got a display name in the cell
                    If .value(i, j, 0) <> "" Then
                        xARSigs.value(iCount, 0) = .value(i, j, 0)
                        xARSigs.value(iCount, 1) = .value(i, j, 1)
                        iCount = iCount + 1
                    End If
                End If
            Next j

        Next i
        xKey = .value(grdLayout.RowBookmark(grdLayout.Row), grdLayout.Col, 1)
    End With
    
    With Me.lstSignatures
        .Array = xARSigs
        '--9.4.0
        .Array.ReDim 0, Me.Signatures.Count - 1, .Array.LowerBound(1), .Array.UpperBound(1)
        .Rebind
    End With
    
    '---9.5.0 - synch the grid selection w/ the sig list
    If Not Me.Initializing Or Not Me.Added Then
        For i = 0 To xARSigs.Count(1) - 1
            If lstSignatures.Array.value(i, 1) = xKey Then Exit For
        Next
        
        lstSignatures.SelectedItem = i
    End If
    '---end 9.5.0
    
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.RefreshSelectionControlsFromGrid"), _
              g_oError.Desc(Err.Number)
    Exit Sub

End Sub

Public Sub SetInterfaceControls()
    Dim bEnabled As Boolean
    Dim bVisible As Boolean
    
'---sets controls displayed in interface
    On Error GoTo ProcError
    bEnabled = m_oDB.PleadingSignatureDefs.Item(cmbSignatureType.BoundText).DisplayJudgeName
    Me.lblSignatureJudge.Visible = bEnabled
    Me.txtSignatureJudge.Visible = bEnabled
    
    '--9.7.1 - 4081
    bEnabled = m_oDB.PleadingSignatureDefs.Item(cmbSignatureType.BoundText).DisplayJudgeName Or _
        m_oDB.PleadingSignatureDefs.Item(cmbSignatureType.BoundText).DisplaySignerName
    
    Me.lblFirmName.Visible = Not bEnabled
    Me.txtFirmName.Visible = Not bEnabled
    
    '---9.7.1 - 4081
    Me.txtSignerName.Visible = m_oDB.PleadingSignatureDefs.Item(cmbSignatureType.BoundText).DisplaySignerName
    Me.lblSignerName.Visible = m_oDB.PleadingSignatureDefs.Item(cmbSignatureType.BoundText).DisplaySignerName
    
    With saGrid
 '---these menu items are not available for signature -- they are problematic for various reasons,
 '---ie, sigs don't require authors from db, there are no options sets for pleadings, etc.
        .MenuAddNewVisible = False
        .MenuCopyVisible = False
        .MenuCopyOptionsVisible = False
        .MenuFavoriteVisible = False
        .MenuSetDefaultVisible = False
        .MenuManageListsVisible = False
        .MenuSep1Visible = False
        .MenuSep2Visible = False
        .Visible = Not bEnabled
    End With
    
    Me.lblAttorneys.Visible = Not bEnabled
    
'    With Me.Signature.Definition
    With m_oDB.PleadingSignatureDefs.Item(cmbSignatureType.BoundText)
        Me.lblAttorneysFor.Visible = Not bEnabled
        Me.txtClientName.Visible = Not bEnabled
        Me.lblClientName.Visible = Not bEnabled
        Me.cmbAttorneysFor.Visible = Not bEnabled
        
        Me.lblFirmSlogan.Visible = Abs(CInt(.AllowFirmSlogan))
        Me.txtFirmSlogan.Visible = Abs(CInt(.AllowFirmSlogan))
        
        '---9.7.1 - 4188
        saGrid.DisplayEMail = .AllowSignerEmailInput
        saGrid.IncludeSignerEmail = .IncludeSignerEmail
        saGrid.SignerEmailPrefix = .SignerEmailPrefix
        saGrid.SignerEmailSuffix = .SignerEmailSuffix
        
        '---9.4.1
        Me.cmbOneOf.Visible = .AllowOneOfSignerText
        Me.lblOneOf.Visible = .AllowOneOfSignerText
        '---end 9.4.1

        '---9.6.1
''''        Me.cmbOneOf.SelectedItem = 1
        
        

'---display address info elements
        bVisible = .AllowAddressTab
                
        Me.vsIndexTab1.TabVisible(1) = bVisible
        '---9.6.2 -- hide/display CI button
        
        Me.btnAddressBooks.Visible = g_bShowAddressBooks
        
        If bVisible Then
            Me.txtFirmAddress.Visible = .ShowFirmAddress
            Me.lblFirmAddress.Visible = .ShowFirmAddress
            Me.lblFirmPhone.Visible = .AllowFirmPhone
            Me.txtFirmPhone.Visible = .AllowFirmPhone
            Me.lblFirmFax.Visible = .AllowFirmFax
            Me.txtFirmFax.Visible = .AllowFirmFax
            Me.lblSignerEmail.Visible = .AllowSignerEMail
            Me.txtSignerEmail.Visible = .AllowSignerEMail
            Me.lblFirmCity.Visible = .AllowFirmCity
            Me.txtFirmCity.Visible = .AllowFirmCity
            Me.lblFirmState.Visible = .AllowFirmState
            Me.txtFirmState.Visible = .AllowFirmState
            Me.lblFirmID.Visible = .ShowFirmID
            Me.txtFirmID.Visible = .ShowFirmID
        End If

        Me.chkIncludeClosingParagraph.Visible = .AllowClosingParagraph
        If Me.chkIncludeClosingParagraph.Visible = False Then
            Me.chkIncludeClosingParagraph.value = vbUnchecked
        End If
        
'---set layout grid for appropriate display
        On Error Resume Next
        '---9.6.2
        
        Dim iInd As Integer
        If Me.Added Then
            iInd = lstSignatures.SelectedItem - 1
        Else
            iInd = lstSignatures.SelectedItem
        End If
        
        SetLayoutGrid Me.Signatures(lstSignatures.Array.value(iInd, 1)).Definition.FullRowFormat
        '---end 9.6.2
    End With
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.SetInterfaceControls"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Function PositionControls()
    On Error GoTo ProcError
    
    Dim sOffset As Single
'---adjust this if you want more or less space between controls on tab
    Dim sIncrement As Single
    
    '---9.4.1
    sIncrement = 135
    
    With Me
        If .cmbSignatureType.Visible = True Then
            sOffset = .cmbSignatureType.Top + .cmbSignatureType.Height + sIncrement
        Else
            sOffset = .cmbSignatureType.Top
        End If
       
        If .cmbOneOf.Visible = True Then
            .lblOneOf.Top = sOffset
            .cmbOneOf.Top = sOffset
            sOffset = sOffset + .cmbOneOf.Height + sIncrement
        End If
        
        If .chkIncludeClosingParagraph.Visible = True Then
            .chkIncludeClosingParagraph.Top = sOffset
            sOffset = sOffset + .chkIncludeClosingParagraph.Height + sIncrement
        End If
    
        If .cmbDateType.Visible = True Then
            .lblSignatureDate.Top = sOffset
            .cmbDateType.Top = sOffset
            sOffset = sOffset + .cmbDateType.Height + sIncrement
        End If
    
        If .txtSignatureJudge.Visible = True Then
            .lblSignatureJudge.Top = sOffset
            .txtSignatureJudge.Top = sOffset
            sOffset = sOffset + .txtSignatureJudge.Height + sIncrement
        End If

        If .saGrid.Visible = True Then
            .lblAttorneys.Top = sOffset
            .saGrid.Top = sOffset
            'sOffset = sOffset + .saGrid.Height + sIncrement
            '---saGrid height returns 975 plus dropdown
            sOffset = sOffset + 975 + sIncrement
        End If

        '---9.7.1 - 4081
        If .txtSignerName.Visible = True Then
            .lblSignerName.Top = sOffset
            .txtSignerName.Top = sOffset
            sOffset = sOffset + .txtSignerName.Height + sIncrement
        End If

     
        If .cmbAttorneysFor.Visible = True Then
            .lblAttorneysFor.Top = sOffset
            .cmbAttorneysFor.Top = sOffset
            sOffset = sOffset + .cmbAttorneysFor.Height + sIncrement
        End If

        If .txtClientName.Visible = True Then
            .lblClientName.Top = sOffset - sIncrement + 15
            .txtClientName.Top = sOffset - sIncrement + 15
            sOffset = sOffset + .txtClientName.Height + sIncrement
        End If

        If .txtFirmName.Visible = True Then
            .lblFirmName.Top = sOffset
            .txtFirmName.Top = sOffset
            sOffset = sOffset + .txtFirmName.Height + sIncrement
        End If

        '*c
        If .txtFirmSlogan.Visible = True Then
            .lblFirmSlogan.Top = sOffset
            .txtFirmSlogan.Top = sOffset
            sOffset = sOffset + .txtFirmSlogan.Height + sIncrement
        End If

    End With
    
    '---end 9.4.1
    
    '---tab 2
    sIncrement = 235

    With Me
        If .txtFirmAddress.Visible = True Then
            sOffset = .txtFirmAddress.Top + .txtFirmAddress.Height + sIncrement
        Else
            sOffset = .txtFirmAddress.Top
        End If
       
        If .txtFirmCity.Visible = True Then
            .lblFirmCity.Top = sOffset
            .txtFirmCity.Top = sOffset
            sOffset = sOffset + .txtFirmCity.Height + sIncrement
        End If

         If .txtFirmState.Visible = True Then
            .lblFirmState.Top = sOffset
            .txtFirmState.Top = sOffset
            sOffset = sOffset + .txtFirmState.Height + sIncrement
        End If

        If .txtFirmPhone.Visible = True Then
            .lblFirmPhone.Top = sOffset
            .txtFirmPhone.Top = sOffset
            sOffset = sOffset + .txtFirmPhone.Height + sIncrement
        End If

        If .txtFirmFax.Visible = True Then
            .lblFirmFax.Top = sOffset
            .txtFirmFax.Top = sOffset
            sOffset = sOffset + .txtFirmFax.Height + sIncrement
        End If

        If .txtSignerEmail.Visible = True Then
            .lblSignerEmail.Top = sOffset
            .txtSignerEmail.Top = sOffset
            sOffset = sOffset + .txtSignerEmail.Height + sIncrement
        End If
         
         If .txtFirmID.Visible = True Then
            .lblFirmID.Top = sOffset
            .txtFirmID.Top = sOffset
            sOffset = sOffset + .txtFirmID.Height + sIncrement
        End If

    End With
    
    Exit Function
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmPleading.PositionControls"), _
              g_oError.Desc(Err.Number)
    Exit Function
End Function

Public Sub UpdateCounter()
    
'---update sig grid label
    Select Case Me.Signatures.Count
        Case 0
            Me.lblSignatures.Caption = Me.lblSignatures.Tag
        Case 1
            Me.lblSignatures.Caption = Me.lblSignatures.Tag & "" & vbCrLf & " (1 Signature)"
        Case Else
            Me.lblSignatures.Caption = Me.lblSignatures.Tag & "" & vbCrLf & " (" & Me.Signatures.Count & " Signatures)"
    End Select

End Sub

Private Function PromptToSave() As Boolean
    Dim xMsg As String
    Dim iChoice As Integer
    
    On Error GoTo ProcError
    xMsg = "Save current entry?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        btnSave_Click
        '---9.6.2
        PromptToSave = True
    End If
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.frmPleadingCaption.PromptToSave"
    Exit Function
End Function

Private Sub UpdateClosingParagraph()
    On Error Resume Next
    Dim oPerson As CPerson
    Dim lID As Long
    Dim i As Integer
    
    Me.Signatures.ClosingParagraph = 0 - chkIncludeClosingParagraph
    '---get person ID from .Signers array property, since Signature does not have an author prop
    For i = Me.Signatures(1).Signers.LowerBound(1) To Me.Signatures(1).Signers.UpperBound(1)
        If Me.Signatures(1).Signers.value(i, 1) = -1 Then
            lID = Me.Signatures(1).Signers.value(i, 3)
            Exit For
        End If
    Next
    
    If lID > 0 Then
        Set oPerson = g_oMPO.db.People(lID)
    End If
    If oPerson Is Nothing Then
        Signatures.ClosingParagraphCity = Me.Signatures.ClosingParagraphCity
        Signatures.ClosingParagraphState = Me.Signatures.ClosingParagraphState
    Else
        If Signatures.ClosingParagraphCity <> Signatures(1).FirmCity Then
            Signatures.ClosingParagraphCity = Signatures(1).FirmCity
        End If
        If Signatures.ClosingParagraphState <> Signatures(1).FirmState Then
            Signatures.ClosingParagraphState = Signatures(1).FirmState
        End If
    End If
    Signatures.ClosingParagraphDateText = Signatures(1).Dated
End Sub


Private Sub SetLayoutGrid(bFullRow As Boolean)
    Dim i As Integer
    
'---set layout grid for appropriate display
        If bFullRow Then
            With grdLayout.Columns(0)
                .AllowFocus = False
                .BackColor = vbButtonFace
                .Caption = "Signature Left Side"
                grdLayout.Columns(1).Caption = "Signature Right Side"
            End With
        Else
            With grdLayout.Columns(0)
                .AllowFocus = True
                '.BackColor = g_lActiveCtlColor
                .BackColor = grdLayout.Columns(1).BackColor
                .Caption = "Signature Left Side"
                grdLayout.Columns(1).Caption = "Signature Right Side"
            End With
        End If
End Sub

Private Sub GetContact()
    '---9.5.0 - loads address fields
    Dim xFirmName As String
    Dim xAddress As String
    Dim xPhone As String
    Dim xFax As String
    Dim xEmail As String
    Dim xCity As String
    Dim xState As String
    Dim xZipCode As String
    Dim xSeparator As String
    Dim xTemp As String
    
    Dim colContacts As Object
    On Error GoTo ProcError

    DoEvents

    Set colContacts = New MPO.CContacts
    colContacts.Retrieve_ALT bIncludePhone:=True, _
                         bIncludeFax:=True, _
                         bPromptForPhones:=True, _
                         iOnEmpty:=ciEmptyAddress_ReturnEmpty, _
                         bIncludeAll:=True

    If colContacts.Count = 0 Then Exit Sub

    With colContacts
        On Error GoTo ProcError
        '---9.6.2
        Dim xName As String
        xName = .Item(1).FullName
        '---end 9.6.2
        xFirmName = .Item(1).Company
        xFax = .Item(1).Fax
        xPhone = .Item(1).Phone
        xCity = .Item(1).City
        xState = .Item(1).State
        xAddress = .Item(1).StreetAddress
        xEmail = .Item(1).EMail
        xZipCode = .Item(1).ZipCode
    End With

    '---create address detail
    
    xSeparator = vbCrLf
    
    If xAddress <> "" Then
        xTemp = xTemp & xAddress & xSeparator
    End If
    
    If xCity <> "" Then _
        xTemp = xTemp & xCity & ", "
    
    If xState <> "" Then _
        xTemp = xTemp & xState
        
    If xZipCode <> "" Then _
        xTemp = xTemp & "  " & xZipCode
    
    
    With Me
        .txtFirmName = xFirmName
        .txtFirmCity = xCity
        .txtFirmState = xState
        .txtFirmAddress = xTemp
        .txtFirmPhone = xPhone
        .txtFirmFax = xFax
        .txtSignerEmail = xEmail

    
        '---9.6.1 - load contact name into sa grid last position
        '---you have to tweak the signers array for saGrid to do this
        
        Dim oX As XArray
        
        Set oX = saGrid.Signers
        If saGrid.Signers.value(saGrid.Signers.UpperBound(1), 2) <> "" Then
            oX.Insert 1, oX.UpperBound(1) + 1
        End If
        
        oX.value(oX.UpperBound(1), 2) = xName
        '9.7.2 #4554
        oX.value(oX.UpperBound(1), 5) = xEmail
        
'---9.6.2
''''        '---clear present saGrid pen-holder
''''        Dim i As Integer
''''
''''        For i = 0 To saGrid.Signers.UpperBound(1)
''''            If saGrid.Signers.UpperBound(1) > 0 Then
''''                oX.value(i, 1) = 0
''''            End If
''''        Next
        
        '---this gives the pen to the first signer, if grid was empty after contact insertion
        If oX.Count(1) = 1 Then _
                oX.value(oX.UpperBound(1), 1) = -1
'---end 9.6.2
        
        saGrid.Signers = oX
    
    
    
    End With

    Set colContacts = Nothing
    Exit Sub
ProcError:
    Set colContacts = Nothing
    Err.Raise Err.Number, _
              CurErrSource("frmPleadingSignature.GetContact"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub
