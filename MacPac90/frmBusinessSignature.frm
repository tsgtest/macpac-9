VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Begin VB.Form frmBusinessSignature 
   Appearance      =   0  'Flat
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a Business Signature"
   ClientHeight    =   6660
   ClientLeft      =   3408
   ClientTop       =   972
   ClientWidth     =   5460
   Icon            =   "frmBusinessSignature.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6660
   ScaleWidth      =   5460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton btnRemoveAll 
      Caption         =   "Remo&ve All"
      Height          =   405
      Left            =   2265
      TabIndex        =   33
      Top             =   6210
      Width           =   1170
   End
   Begin VB.CommandButton btnNew 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   3975
      Picture         =   "frmBusinessSignature.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Add a New Signature (F5)"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnSave 
      Height          =   400
      Left            =   4440
      Picture         =   "frmBusinessSignature.frx":0ACC
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Save new signature"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnDelete 
      Height          =   400
      Left            =   4905
      Picture         =   "frmBusinessSignature.frx":0C4A
      Style           =   1  'Graphical
      TabIndex        =   3
      ToolTipText     =   "Delete Signature"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnNewSubEntity 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   3510
      Picture         =   "frmBusinessSignature.frx":0DD4
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Add a New Sub-Entity Signature (F4)"
      Top             =   60
      Width           =   435
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4575
      TabIndex        =   32
      Top             =   6215
      Width           =   750
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Height          =   400
      Left            =   3750
      TabIndex        =   31
      Top             =   6215
      Width           =   750
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   4905
      Left            =   -15
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1710
      Width           =   5925
      _cx             =   5080
      _cy             =   5080
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   -2147483633
      ForeColor       =   -2147483630
      FrontTabColor   =   -2147483633
      BackTabColor    =   -2147483633
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Main|&Layout"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   0   'False
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   400
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Height          =   4470
         Left            =   6420
         TabIndex        =   8
         Top             =   15
         Width           =   5904
         Begin TrueDBGrid60.TDBGrid grdSubSigners 
            DragIcon        =   "frmBusinessSignature.frx":1316
            Height          =   1155
            Left            =   1785
            OleObjectBlob   =   "frmBusinessSignature.frx":1468
            TabIndex        =   35
            Top             =   2040
            Visible         =   0   'False
            Width           =   2025
         End
         Begin TrueDBGrid60.TDBGrid grdLayout 
            DragIcon        =   "frmBusinessSignature.frx":39DF
            Height          =   1590
            Left            =   1800
            OleObjectBlob   =   "frmBusinessSignature.frx":3B31
            TabIndex        =   30
            Top             =   90
            Width           =   3555
         End
         Begin VB.Label lblSubSigners 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "S&ub-Signers Layout:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   30
            TabIndex        =   34
            Top             =   2040
            Visible         =   0   'False
            Width           =   1500
         End
         Begin VB.Label lblSigningParty 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Signatures Layout:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   60
            TabIndex        =   29
            Top             =   105
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   0
            X1              =   -75
            X2              =   5460
            Y1              =   4425
            Y2              =   4425
         End
         Begin VB.Shape Shape2 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00800080&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   5820
            Left            =   -15
            Top             =   -1380
            Width           =   1740
         End
         Begin VB.Shape Shape4 
            BorderStyle     =   0  'Transparent
            Height          =   5715
            Left            =   15
            Top             =   -240
            Visible         =   0   'False
            Width           =   5895
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   4470
         Left            =   15
         TabIndex        =   7
         Top             =   15
         Width           =   5904
         Begin TrueDBList60.TDBCombo cmbSignatureType 
            DataField       =   "fldID"
            Height          =   300
            Left            =   1785
            OleObjectBlob   =   "frmBusinessSignature.frx":6540
            TabIndex        =   10
            Top             =   165
            Width           =   3585
         End
         Begin TrueDBList60.TDBCombo cmbDate 
            Height          =   375
            Left            =   1770
            OleObjectBlob   =   "frmBusinessSignature.frx":8770
            TabIndex        =   23
            Top             =   3015
            Width           =   3615
         End
         Begin VB.OptionButton optRightColumn 
            Appearance      =   0  'Flat
            Caption         =   "&Right Column"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   3795
            TabIndex        =   25
            Top             =   3420
            Width           =   1455
         End
         Begin VB.OptionButton optLeftColumn 
            Appearance      =   0  'Flat
            Caption         =   "Left Col&umn"
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1860
            TabIndex        =   24
            Top             =   3420
            Value           =   -1  'True
            Width           =   1410
         End
         Begin VB.TextBox txtDocType 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   1785
            TabIndex        =   28
            Top             =   4080
            Visible         =   0   'False
            Width           =   3570
         End
         Begin VB.CheckBox chkInWitness 
            Appearance      =   0  'Flat
            Caption         =   "In &Witness Whereof..."
            ForeColor       =   &H80000008&
            Height          =   330
            Left            =   1815
            TabIndex        =   26
            Top             =   3705
            Width           =   2940
         End
         Begin VB.CheckBox chkIncludeIts 
            Appearance      =   0  'Flat
            Caption         =   "In&clude ""Its"" Line"
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   1815
            TabIndex        =   21
            Top             =   2715
            Width           =   3345
         End
         Begin VB.TextBox txtSignerTitle 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   1785
            TabIndex        =   20
            Top             =   2340
            Width           =   3570
         End
         Begin VB.TextBox txtPartyName 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   1785
            TabIndex        =   14
            Top             =   945
            Width           =   3585
         End
         Begin VB.TextBox txtSignerName 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   1785
            TabIndex        =   18
            Top             =   1950
            Width           =   3570
         End
         Begin VB.TextBox txtEntityTitle 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   1785
            TabIndex        =   12
            Top             =   555
            Width           =   3585
         End
         Begin VB.TextBox txtPartyDescription 
            Appearance      =   0  'Flat
            Height          =   510
            Left            =   1785
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   16
            Top             =   1335
            Width           =   3585
         End
         Begin VB.Label lblDocType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "T&ype of Document:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   15
            TabIndex        =   27
            Top             =   4110
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblDate 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "D&ate:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   15
            TabIndex        =   22
            Top             =   3045
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.Label lblSignerTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Si&gner Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   15
            TabIndex        =   19
            Top             =   2400
            Width           =   1455
         End
         Begin VB.Label lblPartyName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Party Name(s):"
            ForeColor       =   &H00FFFFFF&
            Height          =   405
            Left            =   -30
            TabIndex        =   13
            Top             =   990
            Width           =   1500
         End
         Begin VB.Label lblSignerName 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Signer &Name:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   15
            TabIndex        =   17
            Top             =   1965
            Width           =   1455
         End
         Begin VB.Label lblSignatureType 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Type:"
            ForeColor       =   &H00FFFFFF&
            Height          =   300
            Left            =   15
            TabIndex        =   9
            Top             =   195
            Width           =   1455
         End
         Begin VB.Label lblPartyDescription 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "Party &Description:"
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   15
            TabIndex        =   15
            Top             =   1365
            Width           =   1455
         End
         Begin VB.Line Line6 
            BorderColor     =   &H00E0E0E0&
            Index           =   3
            X1              =   -195
            X2              =   5475
            Y1              =   4440
            Y2              =   4440
         End
         Begin VB.Label lblEntityTitle 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00800080&
            BackStyle       =   0  'Transparent
            Caption         =   "&Entity Title:"
            ForeColor       =   &H00FFFFFF&
            Height          =   270
            Left            =   -30
            TabIndex        =   11
            Top             =   585
            Width           =   1500
         End
         Begin VB.Shape Shape1 
            BackColor       =   &H00404040&
            BackStyle       =   1  'Opaque
            BorderColor     =   &H00000000&
            BorderStyle     =   0  'Transparent
            FillColor       =   &H00800080&
            Height          =   5430
            Left            =   0
            Top             =   -975
            Width           =   1725
         End
         Begin VB.Shape Shape3 
            BorderStyle     =   0  'Transparent
            Height          =   6015
            Left            =   -15
            Top             =   -390
            Width           =   5805
         End
      End
   End
   Begin TrueDBList60.TDBList lstSignatures 
      Height          =   1080
      Left            =   1800
      OleObjectBlob   =   "frmBusinessSignature.frx":A98B
      TabIndex        =   5
      Top             =   510
      Width           =   3540
   End
   Begin VB.Label lblSignaturesCount 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H00FFFFFF&
      Height          =   210
      Left            =   60
      TabIndex        =   36
      Top             =   750
      Width           =   1455
   End
   Begin VB.Label lblSignatures 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00800080&
      BackStyle       =   0  'Transparent
      Caption         =   "&Signatures:"
      ForeColor       =   &H00FFFFFF&
      Height          =   300
      Left            =   60
      TabIndex        =   4
      Top             =   525
      Width           =   1455
   End
   Begin VB.Shape Shape5 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H00000000&
      BorderStyle     =   6  'Inside Solid
      FillColor       =   &H00800080&
      Height          =   1800
      Left            =   0
      Top             =   0
      Width           =   1710
   End
   Begin VB.Menu mnuLayoutGrid 
      Caption         =   "Layout Grid"
      Visible         =   0   'False
      Begin VB.Menu mnuLayoutGridAdd 
         Caption         =   "&Add Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridChangeDisplayName 
         Caption         =   "&Change Display Name..."
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridCopy 
         Caption         =   "Cop&y Signature"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLayoutGridDelete1 
         Caption         =   "&Delete Signature"
      End
      Begin VB.Menu mnuLayoutGridDeleteAll 
         Caption         =   "Delete All &Signatures"
      End
      Begin VB.Menu mnuLayoutGridPaste 
         Caption         =   "&Paste Signature"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuAuthor 
      Caption         =   "Author"
      Visible         =   0   'False
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_EditAuthorOptions 
         Caption         =   "&Edit Options..."
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage People..."
      End
   End
   Begin VB.Menu mnuSubSigners 
      Caption         =   "Sub-Signatures"
      Visible         =   0   'False
      Begin VB.Menu mnuSubSignersDelete1 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuSubSignersDeleteAll 
         Caption         =   "Delete All &Sub-Signatures"
      End
   End
   Begin VB.Menu mnuDropSignature 
      Caption         =   "Drop "
      Visible         =   0   'False
      Begin VB.Menu mnuDropSignatureInsert 
         Caption         =   "&Insert Before"
      End
      Begin VB.Menu mnuDropSignatureAddSub 
         Caption         =   "&Add Sub-Signature"
      End
      Begin VB.Menu mnuDropSignatureCancel 
         Caption         =   "&Cancel"
      End
   End
End
Attribute VB_Name = "frmBusinessSignature"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const mpCancelAdd As String = "Cancel unsaved signature (F7)"
Const mpDeleteSig As String = "Delete this signature (F7)"
Const mpCancelChange As String = "Cancel unsaved changes (F7)"
Const mpSaveChanges As String = "Save changes to this signature (F6)"
Const mpSaveNewSignature As String = "Save new signature (F6)"

Private xDragFrom() As String
Private is_SourceRow As Integer
Private is_SourceCol As Integer
Private is_DestRow As Integer
Private is_DestCol As Integer
Private m_bClicked As Boolean
Private m_bSkipGridChange As Boolean
Private m_bSkipListUpdate As Boolean
Private m_bDropSubEntity As Boolean
Private m_bCancelDrag As Boolean
Private m_bDragActive As Boolean
Private m_bAdded As Boolean
Private m_bChanged As Boolean

Private m_oParent As VB.Form
Private m_Sigs As MPO.CBusinessSignatures
Private m_oSig As MPO.CBusinessSignature
Private m_Document As MPO.CDocument
Private m_oDB As mpDB.CDatabase
Private xARLayout As XArrayObject.XArray
Private xarSubs As XArrayObject.XArray
Private m_ShowHiddenText As Boolean
Private m_bInitializing As Boolean
Private m_bCancelled As Boolean
Private m_bBookmarkNotFound As Boolean
Private m_bFullRowFormat As Boolean
Private xMsg As String
Private m_bDeleting As Boolean


Private Const mpBusinessSignaturesLimit As Integer = 12

Public Property Let FullRowFormat(bNew As Boolean)
    m_bFullRowFormat = bNew
End Property
Public Property Get FullRowFormat() As Boolean
    FullRowFormat = m_bFullRowFormat
End Property
Public Property Get Signatures() As MPO.CBusinessSignatures
    Set Signatures = m_Sigs
End Property
Public Property Let Signatures(oNew As MPO.CBusinessSignatures)
    Set m_Sigs = oNew
End Property
Private Property Get Signature() As MPO.CBusinessSignature
    Set Signature = m_oSig
End Property
Private Property Set Signature(oNew As MPO.CBusinessSignature)
    Set m_oSig = oNew
End Property
Public Property Get Changed() As Boolean
    Changed = m_bChanged
End Property
Public Property Let Changed(bNew As Boolean)
    m_bChanged = bNew
    Me.btnNew.Enabled = Not bNew And m_Sigs.Count > 0
    Me.btnNewSubEntity.Enabled = Not bNew And m_Sigs.Count > 0
    Me.btnSave.Enabled = bNew
    Me.grdLayout.Enabled = Not bNew
    Me.lblSigningParty.Enabled = Not bNew
    Me.lblSubSigners.Enabled = Not bNew
    Me.grdSubSigners.Enabled = Not bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelChange
        Me.btnSave.ToolTipText = mpSaveChanges
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Added() As Boolean
    Added = m_bAdded
End Property
Public Property Let Added(bNew As Boolean)
    m_bAdded = bNew
    Me.btnNew.Enabled = (Not bNew) And m_Sigs.Count > 0
    Me.btnNewSubEntity.Enabled = (Not bNew) And m_Sigs.Count > 0
    Me.btnSave.Enabled = bNew
    Me.grdLayout.Enabled = Not bNew
    Me.lblSigningParty.Enabled = Not bNew
    Me.lblSubSigners.Enabled = Not bNew
    Me.grdSubSigners.Enabled = Not bNew
    Me.lstSignatures.Enabled = Not bNew
    If bNew Then
        Me.btnDelete.ToolTipText = mpCancelAdd
        Me.btnSave.ToolTipText = mpSaveNewSignature
    Else
        Me.btnDelete.ToolTipText = mpDeleteSig
        Me.btnSave.ToolTipText = mpSaveChanges
    End If
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Public Property Get ParentForm() As Form
    Set ParentForm = m_oParent
End Property
Public Property Let ParentForm(oNew As Form)
    Set m_oParent = oNew
End Property
Public Property Get Initializing() As Boolean
    Initializing = m_bInitializing
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInitializing = bNew
End Property
'******************************************************************
'private functions
'******************************************************************
Private Function bSigUpdate(oSig As CBusinessSignature) As Boolean
    Dim iSigner As Integer
    On Error Resume Next
'---fill controls with properties
    If Not oSig Is Nothing Then
        With oSig
            .TypeID = Me.cmbSignatureType.BoundText
            If oSig.ParentID = 0 Then
                .EntityTitle = Me.txtEntityTitle
            Else
                .EntityTitle = ""
            End If
            .PartyName = Me.txtPartyName
            .PartyDescription = Me.txtPartyDescription
            .SignerName = Me.txtSignerName
            .SignerTitle = Me.txtSignerTitle
            .IncludeIts = 0 - Me.chkIncludeIts
'            .DateText = Me.txtDate
            .DateText = Me.cmbDate.Text
    '---update sig after adding to grid
'            .Refresh
        End With
    End If
    'Application.ScreenUpdating = True
End Function
Private Function bAddSigToGrid(iTargetRow, iTargetCol, Optional iPos As Integer) As Boolean
    Dim oSig As CBusinessSignature
    

    Set oSig = m_Sigs.Add

    If iPos = 0 Then iPos = (iTargetRow + 1) * (iTargetCol + 1)
    
    oSig.Position = iPos
    bSigUpdate oSig
    With grdLayout
        .Array.value(iTargetRow, iTargetCol, 0) = oSig.DisplayName
        .Array.value(iTargetRow, iTargetCol, 1) = oSig.Key
        .Array.value(iTargetRow, iTargetCol, 2) = oSig.SubEntities.Count
        .Bookmark = iTargetRow
        .Col = iTargetCol
        .Columns(iTargetCol).Text = .Array.value(iTargetRow, iTargetCol, 0)
    End With

    Set Signature = oSig
End Function
Private Function bResetDragDrop(xSource As String, xTarget As String) As Boolean
    Dim g_Source As TDBGrid
    Dim g_Target As TDBGrid
    Dim ctl As Control


    For Each ctl In Me.Controls
        If ctl.Name = xSource Then
            Set g_Source = ctl
        ElseIf ctl.Name = xTarget Then
            Set g_Target = ctl
        End If
    Next

' Turn off drag-and-drop by resetting the highlight and data
' control caption.

    If g_Source.MarqueeStyle = dbgSolidCellBorder Then Exit Function
    g_Source.MarqueeStyle = dbgSolidCellBorder
    g_Target.MarqueeStyle = dbgSolidCellBorder

End Function

Private Sub btnCancel_GotFocus()
    OnControlGotFocus Me.btnCancel
End Sub

Private Sub btnFinish_GotFocus()
    OnControlGotFocus Me.btnFinish
End Sub


Private Sub btnNew_Click()
    Dim iLimit As Integer
    
    On Error GoTo ProcError
    iLimit = Max(Val(GetMacPacIni("Business", "MaxSignatures")), CDbl(mpBusinessSignaturesLimit))
    
    If Signatures.Count = iLimit Then
        MsgBox "There is a limit of " & _
               iLimit & _
               " signatures.", _
               vbInformation & _
               vbOKOnly
        Exit Sub
    End If
    
    Set Signature = New CBusinessSignature
    Me.optLeftColumn.Enabled = True
    Me.optRightColumn.Enabled = True
    Me.lstSignatures.Enabled = False
    
'    Me.vsIndexTab1.CurrTab = 0
'
    '***** 9.6.2  #3668
    With Me.lstSignatures
        Dim oXAr As XArray
        Set oXAr = lstSignatures.Array
        oXAr.Insert 1, oXAr.UpperBound(1) + 1
        .Array = oXAr
        .Rebind
        .SelectedItem = .Array.UpperBound(1)
    End With
    '***** end 9.6.2  #3668
    
    Me.Added = True
    ClearFields
    
    Me.cmbDate.BoundText = m_oDB.BusinessSignatureDefs.Item(cmbSignatureType.BoundText).DefaultDate
    
    Me.vsIndexTab1.CurrTab = 0
    
    DisplayControls
    
    Exit Sub
ProcError:
    g_oError.Show Err, "Error adding signature."
    Exit Sub
End Sub
Private Sub btnNewSubEntity_Click()
    Dim sParentKey As String
    On Error GoTo ProcError
    Me.lstSignatures.Enabled = False
    If Signature.ParentID = 0 Then
        sParentKey = Signature.Key
    Else
        sParentKey = m_Sigs(Signature.ParentID).Key
    End If
    
    Set Signature = New CBusinessSignature
    Signature.ParentID = m_Sigs(sParentKey).ID
    
    Me.optLeftColumn.Enabled = False
    Me.optRightColumn.Enabled = False
    Me.lstSignatures.Enabled = False
    
    '****9.6.2  #3668
    With Me.lstSignatures
        Dim oXAr As XArray
        Dim i As Integer
        ' Insert new sub-entity after any existing sub-entities
        For i = lstSignatures.Bookmark To lstSignatures.Array.UpperBound(1)
            If Left(lstSignatures.Array.value(i, 1), 3) <> sParentKey Then
                Exit For
            End If
        Next i
        Set oXAr = lstSignatures.Array
        oXAr.Insert 1, i
        .Array = oXAr
        .Rebind
        Me.lstSignatures.SelectedItem = i
    End With
    '****end 9.6.2  #3668
    
    Me.Added = True
    ClearFields
    
    Me.txtEntityTitle = "Sub-Entity Signature"
    
    DisplayControls
'    Me.txtEntityTitle.Enabled = False
'
'    With m_oDB.BusinessSignatureDefs.Item(cmbSignatureType.BoundText)
'        Me.lblPartyName.Enabled = .AllowSubPartyName
'        Me.txtPartyName.Enabled = .AllowSubPartyName
'        Me.lblPartyDescription.Enabled = .AllowSubPartyDescription
'        Me.txtPartyDescription.Enabled = .AllowSubPartyDescription
'        Me.lblSignerName.Enabled = .AllowSubSignerName
'        Me.txtSignerName.Enabled = .AllowSubSignerName
'        Me.lblSignerTitle.Enabled = .AllowSubSignerTitle
'        Me.txtSignerTitle.Enabled = .AllowSubSignerTitle
'
'        If .AllowSubIts Then
'            Me.chkIncludeIts.Enabled = True
'        Else
'            Me.chkIncludeIts.Enabled = False
'            Me.chkIncludeIts = 0
'        End If

    If Me.txtPartyName.Enabled Then
        Me.txtPartyName.SetFocus
    ElseIf Me.txtSignerName.Enabled Then
        Me.txtSignerName.SetFocus
    End If
    Exit Sub
ProcError:
    g_oError.Show Err, "Error adding signature."
    Exit Sub
End Sub


Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub
Private Sub btnDelete_Click()
    Dim parKey As String
    Dim subKey As String
    
    m_bDeleting = True
    
    On Error GoTo ProcError
    With lstSignatures
        If Len(.BoundText) Then
            parKey = Left(.BoundText, 3)
            If Len(.BoundText) > 3 Then _
                subKey = Mid(.BoundText, 4)
            If subKey = "" Then
                Set Signature = m_Sigs(parKey)
            Else
                Set Signature = m_Sigs(parKey).SubEntities(subKey)
            End If
        Else
            Set Signature = Nothing
        End If
        If Me.Added Then
            '**** 9.6.2  #3668
            With Me.lstSignatures
                Dim oXAr As XArray
                Dim i As Integer
                i = lstSignatures.Bookmark
                Set oXAr = .Array
                oXAr.Delete 1, i
                If oXAr.Count(1) = 0 Then
                    oXAr.ReDim 0, -1, 0, 1
                End If
                .Array = oXAr
                .Rebind
                If .Array.Count(1) > 0 Then
                    If i > .Array.UpperBound(1) Then i = .Array.UpperBound(1)
                    .SelectedItem = i
                End If
            End With
            '**** end 9.6.2  #3668
            
            ClearFields
            If Not Signature Is Nothing Then _
                DisplaySignature Signature
            If IsNull(.SelectedItem) Then
                If Signatures.Count = 1 Then
                    .SelectedItem = 0
                End If
            End If
        Else
            If Me.Changed Then
                DisplaySignature Signature
                lstSignatures.BoundText = parKey & subKey
            Else
                If MsgBox("Are you sure you want to delete this signature?", _
                vbYesNo, "Business Signature") <> vbYes Then
                    Exit Sub
                End If
                ClearFields
                DeleteSignature Signature, True
            End If
        End If
        If m_Sigs.Count = 0 Then
            btnNew_Click
            Me.Added = True
        Else
            Me.Added = False
            Me.Changed = False
        End If
        Me.lstSignatures.Enabled = True
    End With
    m_bDeleting = False
    Exit Sub
ProcError:
    m_bDeleting = False
    g_oError.Show Err, "Error Deleting signature."
    Exit Sub
End Sub

Private Sub btnNewSubEntity_GotFocus()
    OnControlGotFocus Me.btnNewSubEntity
End Sub
Private Sub btnNew_GotFocus()
    OnControlGotFocus Me.btnNew
End Sub
Private Sub btnRemoveAll_Click()
    Dim iRet As Integer
    iRet = MsgBox("Delete all signatures from current document?", vbYesNo + vbQuestion, App.Title)
    If iRet = vbYes Then
        Me.Hide
        m_Sigs.RemoveAll
    Else
        Exit Sub
    End If
    
End Sub
Private Sub btnSave_GotFocus()
    OnControlGotFocus Me.btnSave
End Sub
Private Sub btnDelete_GotFocus()
    OnControlGotFocus Me.btnDelete
End Sub

Private Sub btnSave_Click()
    Dim iPos As Integer
    Dim i As Integer
    Dim iTargetRow As Integer
    Dim iTargetCol As Integer
    Dim xParKey As String
    Dim xKey As String
    Dim iLastPos As Integer
    
    Dim oSig As CBusinessSignature
    
    On Error GoTo ProcError
    m_Document.StatusBar = "Saving..."
    If Me.Added Then
        Me.Added = False
        If Signature.ParentID = 0 Then
            '---add after last item on the chosen side of grid
            If m_oDB.BusinessSignatureDefs.Item(cmbSignatureType.BoundText).FullRowFormat Or _
            optLeftColumn Then
                If m_Sigs.Count Then
                    iLastPos = m_Sigs(m_Sigs.Count).Position
                End If
                iPos = IIf(mpbase2.IsEven(iLastPos + 1), _
                           iLastPos + 2, _
                           iLastPos + 1)
            
            Else
                If m_Sigs.Count Then
                    iLastPos = m_Sigs(m_Sigs.Count).Position
                End If
                iPos = IIf(mpbase2.IsEven(iLastPos + 1), _
                           iLastPos + 1, _
                           iLastPos + 2)
            End If
            
            iTargetRow = (IIf(mpbase2.IsEven(CDbl(iPos)), iPos / 2, (iPos + 1) / 2)) - 1
            iTargetCol = 1 - (iPos Mod 2)
        
            bAddSigToGrid iTargetRow, iTargetCol, CInt(iPos)
            xKey = Signature.Key
            xParKey = ""
            If Not m_bSkipListUpdate Then _
                RefreshSignaturesList xKey
            DoEvents
            DisplaySignature m_Sigs(xKey)
        Else
            Set oSig = m_Sigs(Signature.ParentID).SubEntities.Add
            With m_Sigs(Signature.ParentID)
                iTargetRow = Int((.Position - 1) / 2)
                iTargetCol = 1 - (.Position Mod 2)
                xARLayout.value(iTargetRow, iTargetCol, 2) = .SubEntities.Count
            End With
            With oSig
                .EntityTitle = ""
                .PartyName = Me.txtPartyName
                .PartyDescription = Me.txtPartyDescription
                .SignerName = Me.txtSignerName
                .SignerTitle = Me.txtSignerTitle
                .IncludeIts = 0 - Me.chkIncludeIts
'                .DateText = Me.txtDate
                .DateText = Me.cmbDate.Text
            End With
            Set Signature = oSig
            xParKey = m_Sigs(Signature.ParentID).Key
            xKey = Signature.Key
            If Not m_bSkipListUpdate Then _
                RefreshSignaturesList xParKey & xKey
            DoEvents
            DisplaySignature m_Sigs(xParKey).SubEntities(xKey)
        End If
        m_Document.StatusBar = "Signature saved"
    ElseIf Me.Changed Then
        bSigUpdate Signature
        '*c - new - update layout grid
        With grdLayout
            .Array.value(Int((Signature.Position - 1) / 2), IIf(Signature.Definition.FullRowFormat, 0, 1 - (Signature.Position Mod 2)), 0) = Signature.DisplayName
            .Rebind
        End With
        
        m_Document.StatusBar = "Signature changed"
        If Signature.ParentID = 0 Then
            xKey = Signature.Key
        Else
            xKey = m_Sigs(Signature.ParentID).Key & Signature.Key
        End If
        If Not m_bSkipListUpdate Then _
            RefreshSignaturesList xKey
    End If
    
    Me.btnRemoveAll.Enabled = True
    
    Me.lstSignatures.Enabled = True
    Me.Added = False
    Me.Changed = False
'---wake up the form for SDI
    SendKeys "+", True
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err, "Error saving signature."
    Exit Sub
    
End Sub
Private Sub btnFinish_Click()

    If Me.Added Or Me.Changed Then
        PromptToSave
        Exit Sub
    End If
    DoEvents
    Me.Hide
    EchoOff
    If ParentForm Is Nothing Then
        m_Sigs.Finish
    Else
        m_Sigs.Refresh
    End If
    EchoOn
End Sub

Private Sub chkIncludeIts_Click()
    If Not Me.Added Then _
        Me.Changed = True
End Sub

Private Sub chkIncludeIts_GotFocus()
    OnControlGotFocus Me.chkIncludeIts
End Sub

Private Sub chkInWitness_Click()
    Dim bEnabled As Boolean
    bEnabled = chkInWitness
    Me.lblDocType.Visible = bEnabled
    Me.txtDocType.Visible = bEnabled
    Me.Signatures.IncludeIntroParagraph = bEnabled
    Me.Changed = True
End Sub

Private Sub chkInWitness_GotFocus()
    OnControlGotFocus Me.chkInWitness
End Sub

Private Sub cmbDate_GotFocus()
    On Error GoTo ProcError
    DoEvents
    OnControlGotFocus Me.cmbDate
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDate_ItemChange()
    If Not Me.Added Then _
        Me.Changed = True
End Sub

Private Sub lstSignatures_RowChange()
    Dim parKey As String
    Dim subKey As String
    Dim i As Integer
    
    On Error GoTo ProcError
    If Me.Initializing Or m_bSkipListUpdate Then
        Exit Sub
    End If
    
    If m_Sigs.Count = 0 Then Exit Sub
    
    With lstSignatures
        If Len(.BoundText) Then
            DoEvents
            parKey = Left(.BoundText, 3)
            If Len(.BoundText) > 3 Then _
                subKey = Mid(.BoundText, 4)
            If subKey = "" Then
                Set Signature = m_Sigs(parKey)
            Else
                Set Signature = m_Sigs(parKey).SubEntities(subKey)
            End If
    '9.6.2  #3668
            DisplaySignature Signature
            Me.Added = False
            Me.Changed = False
        End If
    End With
    '*new
'    Me.Added = False
'    Me.Changed = False
    Exit Sub
ProcError:
    g_oError.Show Err, "Error selecting signature."
    Exit Sub
End Sub
Private Sub cmbSignatureType_ItemChange()
    Dim bEnabled As Boolean
    
    On Error GoTo ProcError
    With m_oDB.BusinessSignatureDefs.Item(cmbSignatureType.BoundText)
        
        bEnabled = .AllowSubEntities
        Me.btnNewSubEntity.Visible = bEnabled
        
        bEnabled = .IncludesDate
        Me.cmbDate.Visible = bEnabled
        Me.lblDate.Visible = bEnabled
        
        bEnabled = .FullRowFormat
        Me.optLeftColumn.Visible = Not bEnabled
        Me.optRightColumn.Visible = Not bEnabled

        LoadDateControl .DateFormats
        
        DisplayControls
        
        'set to default
        If (Not Me.Changed) And (Me.Added) Then
            Me.cmbDate.BoundText = .DefaultDate
        End If
        
    End With
    
    If Me.vsIndexTab1.CurrTab = 0 Then
        Me.cmbSignatureType.SetFocus
    End If
    
    If Not Me.Added Then _
        Me.Changed = True
    Exit Sub
ProcError:
    g_oError.Show Err, "Error selecting signature type."
    Exit Sub
End Sub

Private Sub cmbSignatureType_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbSignatureType, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbSignatureType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbSignatureType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    'set display of an TDBCombos by feeding ID as Bound Text
    '---note that you must set bound column property to the ID field
    '---and set the list field property to the Display Text fields

    '---load controls with values from first signature
    On Error Resume Next

    Me.vsIndexTab1.CurrTab = 0
    If m_Sigs.Count = 0 Then
        If Len(Me.cmbSignatureType.BoundText) = 0 Then Me.cmbSignatureType.BoundText = Me.cmbSignatureType.Array.value(0, 0)
        DoEvents
        Me.Added = True
        Set Signature = New CBusinessSignature
        RefreshSignaturesList '**** 9.6.2  #3668
        lstSignatures.Enabled = False
        Me.btnRemoveAll.Enabled = False
    Else
        RefreshSignaturesList m_Sigs(1).Key
        If Not grdLayout.Array.value(0, 0) Is Null Then
            Me.grdLayout.Bookmark = 0
            Me.grdLayout.Col = 0
        Else
            Me.grdLayout.Bookmark = 0
            Me.grdLayout.Col = 1
        End If
        Me.chkInWitness = Abs(m_Sigs.IncludeIntroParagraph)
        Me.txtDocType = m_Sigs.DocumentType
        DisplaySignature m_Sigs(1)
        Me.btnRemoveAll.Enabled = True
    End If
    btnNew_Click
    Me.Changed = False
    Me.Added = True
    If Me.txtEntityTitle.Enabled Then
        Me.txtEntityTitle.SetFocus
    Else
        Me.txtPartyName.SetFocus
    End If
    DoEvents
    g_oMPO.ScreenUpdating = True
    Me.Initializing = False
End Sub
Private Sub Form_Load()
    Dim i As Integer
    Dim j As Integer
    Dim iPos As Integer
    Dim xKey As String
    Dim iRow As Integer
    Dim iCol As Integer
    Dim oList As mpDB.CList
    Dim m_Sig As MPO.CBusinessSignature

    On Error GoTo ProcError
    
    g_oMPO.ScreenUpdating = False
    m_bClicked = False
    Me.Initializing = True

    If m_Sigs Is Nothing Then
        Set m_Sigs = g_oMPO.NewBusinessSignatures
        m_Sigs.LoadValues
    End If

    Set m_Document = New MPO.CDocument
    Set m_oDB = g_oMPO.db
    Set xARLayout = g_oMPO.NewXArray
    Set xarSubs = g_oMPO.NewXArray
    xarSubs.ReDim 0, -1, 0, 2

    With Me.cmbSignatureType
        m_oDB.BusinessSignatureDefs.FullRowFormat = m_bFullRowFormat
        .Array = m_oDB.BusinessSignatureDefs.ListSource
        .Rebind
    End With
'    If Me.FullRowFormat Then
'        Me.optLeftColumn.Visible = False
'        Me.optRightColumn.Visible = False
'        Me.grdLayout.ColumnHeaders = False
'        Me.grdLayout.Columns(1).Visible = False
'    End If
'---load layout grid with signatures from signature collection
    xARLayout.ReDim 0, mpBusinessSignaturesLimit - 1, 0, 1, 0, 2
    
    For Each m_Sig In m_Sigs
        iPos = m_Sig.Position
        xKey = m_Sig.Key
    '---calculate grid position based on iPos Value
'        If m_bFullRowFormat Then
'            iRow = iPos - 1
'            iCol = 0
'        Else
            If iPos / 2 - Int(iPos / 2) = 0.5 Then
                iRow = CInt((iPos + 1) / 2) - 1
                iCol = 0
            Else
                iRow = CInt(iPos / 2) - 1
                iCol = 1
            End If
'        End If

     '---update array
        xARLayout.value(iRow, iCol, 0) = m_Sig.DisplayName
        xARLayout.value(iRow, iCol, 1) = xKey
        xARLayout.value(iRow, iCol, 2) = m_Sig.SubEntities.Count
    Next m_Sig

    Me.grdLayout.Array = xARLayout
    Me.grdLayout.Rebind
    Me.grdSubSigners.Array = xarSubs
    Me.grdSubSigners.Rebind
    
    LoadDateControl 1
    
    ResizeTDBCombo Me.cmbDate, 8
    ResizeTDBCombo cmbSignatureType, 6

'   move dialog to last position
    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oMPO.ScreenUpdating = True
    Word.Application.ScreenUpdating = True
    g_oError.Show Err, "Letter Loading Business Signature Form"
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me

    Set xARLayout = Nothing
    Set m_Sigs = Nothing
    Set m_oSig = Nothing
    Set m_Document = Nothing
    Set frmBusinessSignature = Nothing
    Set g_oPrevControl = Nothing
End Sub

Private Sub grdLayout_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
    Dim i As Integer
    On Error GoTo ProcError
    is_SourceRow = RowBookmark
    is_SourceCol = ColIndex

    With xARLayout
        ReDim xDragFrom(0, 0, .UpperBound(3))

        grdLayout.MarqueeStyle = dbgHighlightCell

        For i = 0 To .UpperBound(3)
            xDragFrom(0, 0, i) = .value(RowBookmark, ColIndex, i)
        Next i
    End With
    
    m_bDragActive = True
' Use Visual Basic manual drag support
    grdLayout.Drag vbBeginDrag
    Exit Sub
ProcError:
    g_oError.Show Err, "Error dragging cell."
    Exit Sub
End Sub
Private Function bKeyMoveComplete(KeyCode As Integer, _
                            is_SourceRow As Integer, _
                            is_SourceCol As Integer) As Boolean
    Dim idestCol As Integer
    Dim idestRow As Integer
    Dim temp() As String
    Dim i As Integer
    Dim j As Integer

    ReDim xDragFrom(0, 0, xARLayout.UpperBound(3))

    grdLayout.MarqueeStyle = dbgHighlightCell
    'xDragFrom = grdLayout.Columns(ColIndex).Text
    For i = 0 To xARLayout.UpperBound(3)
        xDragFrom(0, 0, i) = xARLayout(grdLayout.Bookmark, grdLayout.Col, i)
    Next i

'---Get coordinates of drop zone
    idestCol = grdLayout.Col
    idestRow = grdLayout.Bookmark
    Select Case KeyCode
        Case 37 '---left
            idestCol = idestCol - 1
            If idestCol < 0 Then idestCol = 0
        Case 38 '---up
            idestRow = idestRow - 1
            If idestRow < 0 Then idestRow = 0
        Case 39 '---right
            idestCol = idestCol + 1
            If idestCol > xARLayout.UpperBound(2) Then idestCol = xARLayout.UpperBound(2)
        Case 40 '---down
            idestRow = idestRow + 1
'---create row space in array if necessary
            If idestRow > xARLayout.UpperBound(1) Then
                xARLayout.Insert 1, xARLayout.UpperBound(1) + idestRow
            End If
        End Select

'---empty out source item
    For i = 0 To xARLayout.UpperBound(3)
        xARLayout(is_SourceRow, is_SourceCol, i) = Empty
    Next i

'---move down only within dropped columns
    ReDim temp(0, 0, xARLayout.UpperBound(3)) As String

    For i = idestRow To idestRow
        For j = 0 To xARLayout.UpperBound(3)
            temp(0, 0, j) = xARLayout.value(i, idestCol, j)
            xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
            xDragFrom(0, 0, j) = temp(0, 0, j)
        Next j
    Next i

    If temp(0, 0, 0) <> Empty Then
        For i = idestRow + 1 To xARLayout.UpperBound(1)
            For j = 0 To xARLayout.UpperBound(3)
                temp(0, 0, j) = xARLayout.value(i, idestCol, j)
                xARLayout(i, idestCol, j) = xDragFrom(0, 0, j)
                xDragFrom(0, 0, j) = temp(0, 0, j)
            Next j
        Next i
    End If

    grdLayout.Bookmark = Null
    grdLayout.Rebind
    grdLayout.MarqueeStyle = dbgHighlightCell
    grdLayout.Bookmark = idestRow
    grdLayout.Col = idestCol
End Function
Private Sub grdLayout_DragDrop(Source As Control, x As Single, y As Single)
    
    Dim idestCol As Integer
    Dim idestRow As Integer
    Dim iDestPosition As Integer
    Dim sSourceKey As String
    Dim sSourceParent As String
    Dim sDestKey As String
    Dim i As Integer
    Dim r As Integer
    Dim c As Integer
    Dim m_Sig As MPO.CBusinessSignature
    Dim iSubs As Integer
    Dim bTargetIsFullRow As Boolean
    Dim bSourceIsFullRow As Boolean
    Dim xKey As String
    m_bDragActive = False
    m_bSkipGridChange = True
    On Error Resume Next

    
    If Source.Name <> "grdSubSigners" Then
        sSourceKey = xARLayout(is_SourceRow, is_SourceCol, 1)
        Set m_Sig = m_Sigs(sSourceKey)
    End If

'---Get coordinates of drop zone
    If m_Sig.Definition.FullRowFormat Then
        idestCol = 0
    Else
        idestCol = Abs(grdLayout.ColContaining(x))
    End If
    
    idestRow = grdLayout.RowBookmark(grdLayout.RowContaining(y))
    iDestPosition = (idestRow * 2) + idestCol + 1
    
    '* new
    xKey = xARLayout(idestRow, 0, 1)
    If xKey <> Empty Then
        If m_Sigs(xKey).Definition.FullRowFormat Then
            bTargetIsFullRow = True
        End If
    ElseIf xARLayout(idestRow, 1, 1) <> "" Then
'        bTargetIsFullRow = True
    End If
    bSourceIsFullRow = m_Sig.Definition.FullRowFormat
    
    sDestKey = xARLayout(idestRow, idestCol, 1)
    If Source.Name = "grdSubSigners" Then
        If xDragFrom(0, 0, 2) & "" = "" Then
            grdLayout.MousePointer = dbgMPDefault
            Source.Drag vbCancel
            Exit Sub
        End If
        sSourceKey = xDragFrom(0, 0, 1)
        sSourceParent = xDragFrom(0, 0, 2)
        If sSourceParent = sDestKey Then m_bCancelDrag = True
    Else
        sSourceKey = xARLayout(is_SourceRow, is_SourceCol, 1)
        iSubs = m_Sigs(sSourceKey).SubEntities.Count
    End If
    
    On Error GoTo ProcError
    ' If signature allows sub-entities, prompt to make
    ' Dropped signature a sub-entity or insert before
    ' destination position
    If sDestKey <> Empty And Not m_bCancelDrag And iSubs = 0 Then
        If m_Sigs(sDestKey).Definition.AllowSubEntities Then
            PopupMenu mnuDropSignature
        End If
        ' Cancel if entry is being inserted before next entry
        ' (No change to make)
        If Not m_bDropSubEntity And _
           ((is_SourceCol = idestCol) And _
           (is_SourceRow = idestRow)) Then
                m_bCancelDrag = True
        ElseIf bSourceIsFullRow And xARLayout(idestRow, 1, 1) <> Empty Then
            If Not (m_Sigs(xARLayout(idestRow, 1, 1)).Definition.FullRowFormat) Then
                m_bCancelDrag = True
            End If
        End If
        
    ElseIf sDestKey = Empty And bTargetIsFullRow And (Not bSourceIsFullRow) Then      '*c
        m_bCancelDrag = True
        m_bDropSubEntity = False
    ElseIf sDestKey = Empty And bSourceIsFullRow And xARLayout(idestRow, 1, 1) <> Empty Then       '*c
        If Not (m_Sigs(xARLayout(idestRow, 1, 1)).Definition.FullRowFormat) Then
            m_bCancelDrag = True
        End If
        m_bDropSubEntity = False
    Else
        If (is_SourceCol = idestCol) And _
           (is_SourceRow = idestRow) Then
                m_bCancelDrag = True
        End If
        
        m_bDropSubEntity = False
    End If
    
    If Source.Name = "grdSubSigners" Then
        grdSubSigners.MarqueeStyle = dbgHighlightCell
        grdSubSigners.MousePointer = dbgMPDefault
    End If
    
    If m_bCancelDrag Then
        m_bCancelDrag = False
        Source.Drag vbCancel
        
        grdLayout.MarqueeStyle = dbgHighlightCell
        grdLayout.Bookmark = is_SourceRow
        grdLayout.Col = is_SourceCol
        grdLayout.MousePointer = dbgMPDefault
        Exit Sub
    End If
    
    If Source.Name = "grdLayout" Then
            
    '---empty out source item in grid
        For i = 0 To xARLayout.UpperBound(3)
            xARLayout(is_SourceRow, is_SourceCol, i) = Empty
        Next i
        grdLayout.Rebind
    
    '---create row space in array if necessary
        If idestRow > xARLayout.UpperBound(1) Then
            xARLayout.Insert 1, xARLayout.UpperBound(1) + idestRow
        End If
    
        m_Document.StatusBar = "Moving..."
        ' First move down contents of destination cell to make room
        If Not m_bDropSubEntity Then
            If sDestKey <> Empty Or bTargetIsFullRow Or bSourceIsFullRow Then
                If idestCol = is_SourceCol Then
                    MoveCells idestRow, idestCol, False, is_SourceRow - 1
                Else
                    MoveCells idestRow, idestCol, False
                End If
            End If
            If Len(sSourceKey) Then _
                m_Sigs(sSourceKey).Position = iDestPosition
                
            For i = 0 To xARLayout.UpperBound(3)
                xARLayout(idestRow, idestCol, i) = xDragFrom(0, 0, i)
            Next i
        
        Else
            Set m_Sig = m_Sigs(sSourceKey)
            m_Sigs.Remove sSourceKey
            m_Sigs(sDestKey).SubEntities.Add m_Sig
            xARLayout.value(idestRow, idestCol, 2) = _
                m_Sigs(sDestKey).SubEntities.Count
        End If
        
        ' Now move up cells in source column to fill gap
'        If idestCol <> is_SourceCol Or idestRow > is_SourceRow Then
'            MoveCells is_SourceRow + 1, is_SourceCol, True
'        End If
'        If idestCol = is_SourceCol And idestRow > is_SourceRow Then _
'            idestRow = idestRow - 1
        
    ElseIf Source.Name = "grdSubSigners" Then
    '---create row space in array if necessary
        If idestRow > xARLayout.UpperBound(1) Then
            xARLayout.Insert 1, xARLayout.UpperBound(1) + idestRow
        End If
    
        m_Document.StatusBar = "Moving..."
        ' First move down contents of destination cell to make room
        If Not m_bDropSubEntity Then
            MoveCells idestRow, idestCol, False
            
            Set m_Sig = m_Sigs.Add
            With m_Sigs(sSourceParent).SubEntities(sSourceKey)
                m_Sig.EntityTitle = .EntityTitle
                m_Sig.PartyName = .PartyName
                m_Sig.PartyDescription = .PartyDescription
                m_Sig.DateText = .DateText
                m_Sig.TypeID = .TypeID
                m_Sig.IncludeIts = .IncludeIts
                m_Sig.SignerName = .SignerName
                m_Sig.SignerTitle = .SignerTitle
                m_Sig.Position = iDestPosition
            End With
                
            xARLayout(idestRow, idestCol, 0) = m_Sig.DisplayName
            xARLayout(idestRow, idestCol, 1) = m_Sig.Key
            xARLayout(idestRow, idestCol, 2) = m_Sig.SubEntities.Count
        Else
            Set m_Sig = m_Sigs(sSourceParent).SubEntities(sSourceKey)
            m_Sigs(sDestKey).SubEntities.Add m_Sig
            xARLayout.value(idestRow, idestCol, 2) = _
                m_Sigs(sDestKey).SubEntities.Count
        End If
        Dim iTargetRow As Integer
        Dim iTargetCol As Integer
        With m_Sigs(sSourceParent)
            .SubEntities.Remove sSourceKey
            iTargetRow = CInt((.Position + 1) / 2) - 1
            iTargetCol = 1 - (.Position Mod 2)
            xARLayout.value(iTargetRow, iTargetCol, 2) = _
                .SubEntities.Count
        End With
    End If
    
    Set m_Sig = Nothing
    Source.Drag vbEndDrag
    grdLayout.Bookmark = Null

    
    RefreshSignaturesList
    
    
    grdLayout.MarqueeStyle = dbgHighlightCell
    grdLayout.Bookmark = idestRow
    grdLayout.MousePointer = dbgMPDefault
    grdLayout.Col = idestCol

    m_Document.StatusBar = "Signature moved"
    m_bSkipGridChange = False
    grdLayout.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err, "Error completing drag and drop operation."
    Exit Sub
End Sub
Private Sub grdLayout_DragOver(Source As Control, x As Single, y As Single, State As Integer)
' DragOver provides  visual feedback

    Dim dragFrom As String
    Dim overCol As Integer
    Dim overRow As Long

    On Error GoTo ProcError
    Select Case State
        Case vbEnter
            If Source.Name = "grdSubSigners" Then
                ' Only allow dragging of sub-entities from
                ' grdSubsigners
                If xDragFrom(0, 0, 2) & "" = "" Then
                    grdLayout.MousePointer = dbgMPNoDrop
                    DoEvents
                    Exit Sub
                End If
            Else
                grdLayout.MousePointer = dbgMPNoDrop
                Exit Sub
            End If
            overCol = grdLayout.ColContaining(x)
            overRow = grdLayout.RowContaining(y)
            If overCol >= 0 Then grdLayout.Col = overCol
            If overRow >= 0 Then grdLayout.Bookmark = overRow
            grdLayout.MousePointer = dbgMPDefault
            grdLayout.MarqueeStyle = dbgHighlightCell
        Case vbOver
            overCol = grdLayout.ColContaining(x)
            overRow = grdLayout.RowContaining(y)
            If overCol >= 0 Then grdLayout.Col = overCol
            If overRow >= 0 Then grdLayout.Bookmark = overRow
            grdLayout.MousePointer = dbgMPDefault
            grdLayout.MarqueeStyle = dbgHighlightCell
        
    End Select
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err, "Error in drag over."
    Exit Sub
End Sub

Private Sub grdLayout_GotFocus()
    OnControlGotFocus Me.grdLayout
    GridTabOut Me.grdLayout, True, 1
    m_bSkipGridChange = True
End Sub

Private Sub grdLayout_KeyDown(KeyCode As Integer, Shift As Integer)

        If KeyCode = 9 Then GridTabOut Me.grdLayout, False, 1

        If KeyCode = 40 And Shift = 2 Then Me.PopupMenu mnuLayoutGrid

        If Shift = 3 Then
            If KeyCode >= 37 And KeyCode <= 40 Then
                bKeyMoveComplete KeyCode, grdLayout.Bookmark, grdLayout.Col
                KeyCode = 0
            End If
        End If
End Sub

Private Sub grdLayout_LostFocus()
'    grdLayout.BackColor = g_lInactiveCtlColor
    m_bSkipGridChange = False
End Sub

Private Sub grdLayout_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    Dim xKey As String
    Dim i As Integer
    If Me.Initializing Or m_Sigs.Count = 0 Then Exit Sub
    If m_bDragActive Then Exit Sub
    
    On Error GoTo ProcError
    m_bSkipGridChange = True
    With grdLayout
        xKey = xARLayout(.Bookmark, .Col, 1)
    End With
    bDisplaySubs xKey
    
ProcError:
    m_bSkipGridChange = False
End Sub
Private Sub grdSubSigners_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    Dim xKey As String
    
    If Me.Initializing Or m_bSkipGridChange Then Exit Sub
    With grdSubSigners
        If Len(xarSubs(0, 0)) = 0 Then Exit Sub
        xKey = xarSubs(.Bookmark, 2) & xarSubs(.Bookmark, 1)
    End With
End Sub
Private Sub lstSignatures_GotFocus()
    OnControlGotFocus lstSignatures
End Sub
Private Sub mnuDropSignatureAddSub_Click()
    m_bDropSubEntity = True
    m_bCancelDrag = False
End Sub
Private Sub mnuDropSignatureInsert_Click()
    m_bDropSubEntity = False
    m_bCancelDrag = False
End Sub
Private Sub mnuDropSignatureCancel_Click()
    m_bDropSubEntity = False
    m_bCancelDrag = True
End Sub
Private Sub mnuLayoutGridChangeDisplayName_Click()
    Dim iRow, iCol As Integer
    Dim xDisplayName As String
    Dim m_Sig As MPO.CBusinessSignature
    On Error Resume Next

    xDisplayName = InputBox("Enter new name here", "Change Display Name", grdLayout.Text)
    If xDisplayName = "" Then
        Exit Sub
    Else
        iRow = grdLayout.Bookmark
        iCol = grdLayout.Col

        Set m_Sig = m_Sigs.Item(grdLayout.Array.value(grdLayout.Bookmark, grdLayout.Col, 1))
        If Not m_Sig Is Nothing Then
'***chucky
'            m_Sig.DisplayName = xDisplayName
            grdLayout.Array.value(grdLayout.Bookmark, grdLayout.Col, 0) = xDisplayName
            grdLayout.Rebind
            grdLayout.Bookmark = iRow
            grdLayout.Col = iCol
            RefreshSignaturesList m_Sig.Key
        End If
    End If
End Sub
Private Sub mnuLayoutGridDelete1_Click()

    Dim oSig As CBusinessSignature
    Dim xKey As String
    Dim iRow As Integer
    Dim iCol As Integer
    
    On Error Resume Next
    With grdLayout
        iRow = .Bookmark
        iCol = .Col
        xKey = xARLayout(iRow, iCol, 1)
        If xKey <> Empty Then
            Set oSig = m_Sigs(xKey)
            If Not oSig Is Nothing Then
                DeleteSignature oSig
            End If
            .Bookmark = Min(CDbl(iRow), xARLayout.UpperBound(1))
            .Col = iCol
        End If
    End With
End Sub
Private Sub mnuLayoutGridDeleteAll_Click()
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim m_Sig As MPO.CBusinessSignature

    '---delete all signatures in collection
    If m_Sigs.Count = 0 Then Exit Sub

    For Each m_Sig In m_Sigs
        m_Sigs.Remove m_Sig.Key
    Next m_Sig

    bDisplaySubs ""
    RefreshSignaturesList
    '---clear grid
    With grdLayout
        For i = 0 To xARLayout.UpperBound(1)
            For j = 0 To xARLayout.UpperBound(2)
                For k = 0 To xARLayout.UpperBound(3)
                    If xARLayout(i, j, k) <> Empty Then _
                        xARLayout(i, j, k) = Empty
                Next k
            Next j
        Next i
        .Rebind
    End With

    Set Signature = Nothing
    ClearFields
    grdLayout.Bookmark = Null
End Sub
Private Sub mnuSubSignersDelete1_Click()
    
    Dim xKey As String
    Dim xParKey As String
    Dim iRow As Integer
    Dim iCol As Integer
    
    On Error Resume Next
    With grdSubSigners
        iRow = .Bookmark
        iCol = .Col
        If iRow = 0 Then
            MsgBox "Cannot delete parent entity in Sub-Signers grid." & vbCr & _
                "Delete from Layout grid instead."
            Exit Sub
        End If
        xKey = xarSubs(iRow, 1)
        xParKey = xarSubs(iRow, 2)
        If xKey <> Empty Then
            m_Sigs(xParKey).SubEntities.Remove xKey
            bDisplaySubs xParKey
            .Bookmark = Min(CDbl(iRow), xarSubs.UpperBound(1))
            RefreshSignaturesList xParKey & xarSubs(.Bookmark, 1)
        End If
    End With
    
End Sub

Private Sub mnuSubSignersDeleteAll_Click()
    Dim oSig As CBusinessSignature
    Dim xKey As String
    Dim xParKey As String
    Dim iRow As Integer
    Dim iCol As Integer
    
    On Error GoTo ProcError
    xParKey = xarSubs(1, 2)
    For Each oSig In m_Sigs(xParKey).SubEntities
        m_Sigs(xParKey).SubEntities.Remove oSig.Key
    Next oSig
    bDisplaySubs xParKey
    RefreshSignaturesList xParKey
    Exit Sub
ProcError:
End Sub

Private Sub optLeftColumn_GotFocus()
    OnControlGotFocus Me.optLeftColumn
End Sub

Private Sub optRightColumn_GotFocus()
    OnControlGotFocus Me.optRightColumn
End Sub

Private Sub txtDocType_GotFocus()
    OnControlGotFocus Me.txtDocType
End Sub
Private Sub txtDocType_LostFocus()
    Me.Signatures.DocumentType = txtDocType
End Sub
Private Sub txtEntityTitle_Change()
    If Not Me.Added Then _
        Me.Changed = True
End Sub
Private Sub txtEntityTitle_GotFocus()
    OnControlGotFocus Me.txtEntityTitle
End Sub

Private Sub txtPartyDescription_Change()
    If Not Me.Added Then _
        Me.Changed = True
End Sub
Private Sub txtPartyDescription_GotFocus()
    OnControlGotFocus Me.txtPartyDescription
End Sub
Private Sub txtPartyName_Change()
    If Not Me.Added Then _
        Me.Changed = True
End Sub
Private Sub txtPartyName_GotFocus()
    OnControlGotFocus Me.txtPartyName
End Sub

Private Sub txtSignerName_Change()
    If Not Me.Added Then _
        Me.Changed = True
End Sub

Private Sub txtSignerName_GotFocus()
    OnControlGotFocus Me.txtSignerName
End Sub

Private Sub txtSignerTitle_Change()
    If Not Me.Added Then _
        Me.Changed = True
End Sub

Private Sub txtSignerTitle_GotFocus()
    OnControlGotFocus Me.txtSignerTitle
End Sub

Private Sub vsIndexTab1_Click()
    On Error GoTo ProcError
    Select Case Me.vsIndexTab1.CurrTab
        Case 0
            If txtEntityTitle.Enabled Then
                Me.txtEntityTitle.SetFocus
            ElseIf Me.txtPartyName.Enabled Then
                Me.txtPartyName.SetFocus
            ElseIf Me.txtSignerName.Enabled Then
                Me.txtSignerName.SetFocus
            End If
        Case 1
            With grdLayout
                If .Enabled Then
                .SetFocus
                grdLayout_RowColChange .Bookmark, .Col
                End If
            End With
            'Me.txtFirmName.SetFocus
    End Select

    Exit Sub
ProcError:
    RaiseError "MacPac90.frmBusinessSignature.vsIndexTab1_Click"
    Exit Sub
End Sub
Private Sub ClearFields()
    With Me
        .txtEntityTitle = ""
        .txtPartyName = ""
        .txtPartyDescription = ""
        .txtSignerName = ""
        .txtSignerTitle = ""
        .chkIncludeIts = 0
        .cmbDate.Text = ""
    End With
End Sub
Private Sub DeleteSignature(oSig As MPO.CBusinessSignature, _
                            Optional bCheckLayout As Boolean = False)

    Dim i As Integer
    Dim j As Integer
    Dim iRow As Integer
    Dim iCol As Integer
    Dim oTest As MPO.CBusinessSignature
    Dim bShowMsg As Boolean
    Dim mbr As Integer

    If Not oSig Is Nothing Then
        On Error Resume Next
        If oSig.ParentID = 0 Then
	    'GLOG : 5434 : ceh
            iRow = Int((oSig.Position - 1) / 2)
            If oSig.Definition.FullRowFormat Then
                iCol = 0
            Else
                iCol = 1 - (oSig.Position Mod 2)
            End If
            If bCheckLayout Then
                For Each oTest In m_Sigs
                    If oTest.Position > oSig.Position And _
                    (1 - (oTest.Position Mod 2)) = iCol Then
                        bShowMsg = True
                        Exit For
                    End If
                Next oTest
            End If
            m_Sigs.Remove oSig.Key
        
            For i = 0 To xARLayout.UpperBound(3)
                xARLayout(iRow, iCol, i) = Empty
            Next i
            grdLayout.Rebind
            RefreshSignaturesList
            If bShowMsg Then _
                MsgBox "Signature has been deleted.  You may need to adjust arrangement of remaining signatures on Layout tab."
        Else
            m_Sigs(oSig.ParentID).SubEntities.Remove oSig.Key
            RefreshSignaturesList m_Sigs(oSig.ParentID).Key
        End If
        DoEvents
        Set Signature = Nothing
        
    End If
End Sub
Private Sub RefreshSignaturesList(Optional sSelectItem As String)
    Dim xarTemp As XArray
    Dim oSig As CBusinessSignature
    Dim oSubSig As CBusinessSignature
    
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    
    Dim sKey As String
    Dim sIndex As String
    
    On Error GoTo ProcError
    Set xarTemp = g_oMPO.NewXArray
    
    xarTemp.ReDim 0, -1, 0, 1
    
    m_bSkipListUpdate = True
    If m_Sigs.Count > 0 Then
        With xarTemp
            For i = 0 To xARLayout.UpperBound(1)
                For j = 0 To xARLayout.UpperBound(2)
                    'v9.8.1010
                    If m_bDeleting Then
                        If (i = 0 And j = 0) And xARLayout.value(i, j, 0) = "" Then
                            i = 1
                            j = 0
                        End If
                    End If
                    If xARLayout.value(i, j, 0) <> "" Then
                        .Insert 1, .UpperBound(1) + 1
                        sKey = xARLayout(i, j, 1)
                        .value(.UpperBound(1), 1) = sKey
                        .value(.UpperBound(1), 0) = m_Sigs(sKey).DisplayName
                        Set oSig = m_Sigs(sKey)
                        For Each oSubSig In oSig.SubEntities
                            .Insert 1, .UpperBound(1) + 1
                            .value(.UpperBound(1), 1) = sKey & oSubSig.Key
                            .value(.UpperBound(1), 0) = "   " & oSubSig.DisplayName
                        Next oSubSig
                    End If
                Next j
            Next i
        End With
        On Error Resume Next
        With lstSignatures
            If sSelectItem = "" Then
                sSelectItem = .BoundText
            End If
            .Array = xarTemp
            .Rebind
            DoEvents
            For i = 0 To .Array.UpperBound(1)
                If .Array.value(i, 1) = sSelectItem Then
                    .Bookmark = i
                    .BoundText = .Array.value(i, 1)
                    .Text = .Array.value(i, 0)
                    Exit For
                End If
                If i = .Array.UpperBound(1) Then
                    .Bookmark = 0
                    .BoundText = .Array.value(0, 1)
                    .Text = .Array.value(0, 0)
                End If
            Next i
            DoEvents
            m_bSkipListUpdate = False
            If Len(.BoundText) = 0 Then
                .BoundText = .Array.value(0, 1)
            End If
            If .Array.UpperBound(1) = 0 Then
                Me.lblSignaturesCount = "(1 signature)"
            Else
                Me.lblSignaturesCount = "(" & .Array.UpperBound(1) + 1 & " signatures)"
            End If
        End With
    Else
'        lstSignatures.Array.ReDim 0, -1, 0, 1
        lstSignatures.Array = xarTemp  '**** 9.6.2  #3668
        lstSignatures.Rebind
        Me.lblSignaturesCount = ""
    End If
    m_bSkipListUpdate = False
    Set xarTemp = Nothing
    Exit Sub
ProcError:
    RaiseError "MacPac90.frmBusinessSignature.RefreshSignaturesList"
    Exit Sub
End Sub
Private Sub DisplaySignature(oSig As CBusinessSignature)
    With oSig
        Set Signature = oSig
        cmbSignatureType.BoundText = .TypeID
        If .ParentID = 0 Then
            txtEntityTitle = .EntityTitle
            txtEntityTitle.Enabled = True
            lstSignatures.Enabled = True
            optLeftColumn.Enabled = False
            optRightColumn.Enabled = False
        Else
            txtEntityTitle = "Sub-Entity Signature"
            txtEntityTitle.Enabled = False
            lstSignatures.Enabled = False
            optLeftColumn.Enabled = False
            optRightColumn.Enabled = False
        End If
        txtPartyDescription = .PartyDescription
        txtPartyName = .PartyName
        txtSignerName = .SignerName
        Me.cmbDate.Text = .DateText
        txtSignerTitle = .SignerTitle
        chkIncludeIts = Abs(.IncludeIts)
        optLeftColumn = .Position Mod 2 > 0
        optRightColumn = .Position Mod 2 = 0
        Me.Added = False
    End With
    Me.btnNew.Enabled = True
    Me.btnSave.Enabled = True
    DoEvents
End Sub
Private Sub MoveCells(iStartRow As Integer, iCol As Integer, _
    bMoveUp As Boolean, Optional iLastRow As Integer = -1)
    
    Dim iOffset As Integer
    Dim iStartCount As Integer
    Dim iEndCount As Integer
    Dim m_Sig As CBusinessSignature
    Dim xKey As String
    
    Dim iDim1 As Integer
    Dim iDim2 As Integer
    Dim iDim3 As Integer
    
    Dim temp() As String
    Dim i As Integer
    Dim j As Integer
    
    If iLastRow = -1 Then
        iLastRow = xARLayout.UpperBound(1)
    End If
    
    If bMoveUp Then
        If iStartRow = 0 Then Exit Sub
        iOffset = -2
        iStartCount = iStartRow
        iEndCount = iLastRow
    Else
        iOffset = 2
        iStartCount = iLastRow
        iEndCount = iStartRow
    End If
    
    ' Update position of signatures
    ' at the same time grid is rearranged
    If bMoveUp Then
        For i = iStartCount To iEndCount
            For j = 0 To xARLayout.UpperBound(3)
                xARLayout(i - 1, iCol, j) = xARLayout(i, iCol, j)
            Next j
            xKey = xARLayout(i - 1, iCol, 1)
            If xKey <> Empty Then
                m_Sigs(xKey).Position = m_Sigs(xKey).Position + iOffset
            End If
        Next i
        For i = iEndCount To xARLayout.UpperBound(1)
            For j = 0 To xARLayout.UpperBound(3)
                xARLayout(i, iCol, j) = Empty
            Next j
        Next i
    Else
        For i = iStartCount To iEndCount Step -1
            If i = xARLayout.UpperBound(1) Then
                xARLayout.Insert 1, i + 1
            End If
            For j = 0 To xARLayout.UpperBound(3)
                xARLayout(i + 1, iCol, j) = xARLayout(i, iCol, j)
            Next j
            xKey = xARLayout(i + 1, iCol, 1)
            If xKey <> Empty Then
                m_Sigs(xKey).Position = m_Sigs(xKey).Position + iOffset
                '*c- new
                If m_Sigs(xKey).Definition.FullRowFormat Then
                    xKey = xARLayout(i + 1, 1, 1)
                    If xKey <> Empty Then
                        For j = 0 To xARLayout.UpperBound(3)
                            xARLayout(i + 2, 1, j) = xARLayout(i + 1, 1, j)
                            xARLayout(i + 1, 1, j) = ""
                        Next j
                        m_Sigs(xKey).Position = m_Sigs(xKey).Position + iOffset
                    End If
                End If
            End If
        Next i
    End If
    grdLayout.Rebind
End Sub
Private Sub grdSubSigners_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
    Dim i As Integer
    is_SourceRow = RowBookmark
    is_SourceCol = ColIndex

    On Error GoTo ProcError
    ReDim xDragFrom(0, 0, xarSubs.UpperBound(2))

    grdSubSigners.MarqueeStyle = dbgHighlightCell

    For i = 0 To xarSubs.UpperBound(2)
        xDragFrom(0, 0, i) = xarSubs(RowBookmark, i)
    Next i
    m_bDragActive = True
    
' Use Visual Basic manual drag support
    grdSubSigners.Drag vbBeginDrag
    grdSubSigners.MousePointer = dbgMPDefault
    Exit Sub
ProcError:
    g_oError.Show Err, "Error dragging cell."
    Exit Sub
End Sub
Private Sub grdSubSigners_DragOver(Source As Control, x As Single, y As Single, State As Integer)
' DragOver provides  visual feedback

    Dim dragFrom As String
    Dim overCol As Integer
    Dim overRow As Long

    On Error GoTo ProcError
    Select Case State
        Case vbEnter
            ' Don't allow dropping from other grid
            grdSubSigners.MousePointer = dbgMPNoDrop
            grdSubSigners.MarqueeStyle = dbgNoMarquee
        Case vbLeave
            grdSubSigners.MousePointer = dbgMPDefault
            grdSubSigners.MarqueeStyle = dbgNoMarquee
        Case vbOver
            overCol = grdSubSigners.ColContaining(x)
            overRow = grdSubSigners.RowContaining(y)
            If overCol >= 0 Then grdSubSigners.Col = overCol
            If overRow >= 0 Then grdSubSigners.Bookmark = overRow
            grdSubSigners.MousePointer = dbgMPDefault
            grdSubSigners.MarqueeStyle = dbgHighlightCell
    End Select
    DoEvents
    Exit Sub
ProcError:
    g_oError.Show Err, "Error in drag over."
    Exit Sub
End Sub
Private Sub grdSubSigners_GotFocus()
    OnControlGotFocus Me.grdSubSigners
    GridTabOut Me.grdSubSigners, True, 1
End Sub
Private Sub grdSubSigners_DragDrop(Source As Control, x As Single, y As Single)
    
    Dim idestCol As Integer
    Dim idestRow As Integer
    Dim iStart As Integer
    Dim iEnd As Integer
    Dim xarTemp As XArray
    Dim xSourceKey As String
    Dim xDestKey As String
    Dim xParentKey As String
    
    Dim i As Integer
    Dim r As Integer
    Dim m_Sig As MPO.CBusinessSignature
    
    m_bDragActive = False
    If Source.Name <> "grdSubSigners" Then Exit Sub
    
    Set xarTemp = grdSubSigners.Array
    
    r = Val(grdSubSigners.RowContaining(y))
'---Get coordinates of drop zone
    idestCol = Abs(grdSubSigners.ColContaining(x))
    On Error GoTo ProcError
    If r > -1 Then
        idestRow = grdSubSigners.RowBookmark(grdSubSigners.RowContaining(y))
    Else
        idestRow = xarTemp.UpperBound(1)
    End If
    m_Document.StatusBar = "Moving..."
    xParentKey = xarTemp(0, 1)
    Set m_Sig = New CBusinessSignature
    If idestRow = 0 And is_SourceRow > 0 Then
        xSourceKey = xarTemp(is_SourceRow, 1)
        With m_Sig
            .PartyName = m_Sigs(xParentKey).PartyName
            .PartyDescription = m_Sigs(xParentKey).PartyDescription
            .SignerName = m_Sigs(xParentKey).SignerName
            .SignerTitle = m_Sigs(xParentKey).SignerTitle
            .IncludeIts = m_Sigs(xParentKey).IncludeIts
            .DateText = m_Sigs(xParentKey).DateText
        End With
        With m_Sigs(xParentKey)
            .PartyName = .SubEntities(xSourceKey).PartyName
            .PartyDescription = .SubEntities(xSourceKey).PartyDescription
            .SignerName = .SubEntities(xSourceKey).SignerName
            .SignerTitle = .SubEntities(xSourceKey).SignerTitle
            .IncludeIts = .SubEntities(xSourceKey).IncludeIts
            .DateText = .SubEntities(xSourceKey).DateText
        End With
        For i = is_SourceRow To 2 Step -1
            xSourceKey = xarTemp(i - 1, 1)
            xDestKey = xarTemp(i, 1)
            With m_Sigs(xParentKey)
                .SubEntities(xDestKey).DateText = .SubEntities(xSourceKey).DateText
                .SubEntities(xDestKey).PartyName = .SubEntities(xSourceKey).PartyName
                .SubEntities(xDestKey).PartyDescription = .SubEntities(xSourceKey).PartyDescription
                .SubEntities(xDestKey).IncludeIts = .SubEntities(xSourceKey).IncludeIts
                .SubEntities(xDestKey).SignerName = .SubEntities(xSourceKey).SignerName
                .SubEntities(xDestKey).SignerTitle = .SubEntities(xSourceKey).SignerTitle
            End With
        Next i
        xDestKey = xarTemp(1, 1)
        With m_Sigs(xParentKey).SubEntities(xDestKey)
            .PartyName = m_Sig.PartyName
            .PartyDescription = m_Sig.PartyDescription
            .SignerName = m_Sig.SignerName
            .SignerTitle = m_Sig.SignerTitle
            .IncludeIts = m_Sig.IncludeIts
            .DateText = m_Sig.DateText
        End With
    ElseIf is_SourceRow = 0 And idestRow > 0 Then
        xParentKey = xarTemp(is_SourceRow, 1)
        With m_Sig
            .PartyName = m_Sigs(xParentKey).PartyName
            .PartyDescription = m_Sigs(xParentKey).PartyDescription
            .SignerName = m_Sigs(xParentKey).SignerName
            .SignerTitle = m_Sigs(xParentKey).SignerTitle
            .IncludeIts = m_Sigs(xParentKey).IncludeIts
            .DateText = m_Sigs(xParentKey).DateText
        End With
        xSourceKey = xarTemp(1, 1)
        With m_Sigs(xParentKey)
            .DateText = .SubEntities(xSourceKey).DateText
            .PartyName = .SubEntities(xSourceKey).PartyName
            .PartyDescription = .SubEntities(xSourceKey).PartyDescription
            .IncludeIts = .SubEntities(xSourceKey).IncludeIts
            .SignerName = .SubEntities(xSourceKey).SignerName
            .SignerTitle = .SubEntities(xSourceKey).SignerTitle
        End With
        For i = 1 To idestRow - 1
            xSourceKey = xarTemp(i + 1, 1)
            xDestKey = xarTemp(i, 1)
            With m_Sigs(xParentKey)
                .SubEntities(xDestKey).DateText = .SubEntities(xSourceKey).DateText
                .SubEntities(xDestKey).PartyName = .SubEntities(xSourceKey).PartyName
                .SubEntities(xDestKey).PartyDescription = .SubEntities(xSourceKey).PartyDescription
                .SubEntities(xDestKey).IncludeIts = .SubEntities(xSourceKey).IncludeIts
                .SubEntities(xDestKey).SignerName = .SubEntities(xSourceKey).SignerName
                .SubEntities(xDestKey).SignerTitle = .SubEntities(xSourceKey).SignerTitle
            End With
        Next i
        xDestKey = xarTemp(idestRow, 1)
        With m_Sigs(xParentKey).SubEntities(xDestKey)
            .PartyName = m_Sig.PartyName
            .PartyDescription = m_Sig.PartyDescription
            .SignerName = m_Sig.SignerName
            .SignerTitle = m_Sig.SignerTitle
            .IncludeIts = m_Sig.IncludeIts
            .DateText = m_Sig.DateText
        End With
    ElseIf idestRow <> is_SourceRow Then
        If idestRow > is_SourceRow Then
            iStart = is_SourceRow
            iEnd = idestRow
        Else
            iStart = idestRow
            iEnd = is_SourceRow
        End If
        xSourceKey = xarTemp(iStart, 1)
        With m_Sigs(xParentKey)
            m_Sig.PartyName = .SubEntities(xSourceKey).PartyName
            m_Sig.PartyDescription = .SubEntities(xSourceKey).PartyDescription
            m_Sig.SignerName = .SubEntities(xSourceKey).SignerName
            m_Sig.SignerTitle = .SubEntities(xSourceKey).SignerTitle
            m_Sig.IncludeIts = .SubEntities(xSourceKey).IncludeIts
            m_Sig.DateText = .SubEntities(xSourceKey).DateText
        End With
        
        For i = iStart To iEnd - 1
            xDestKey = xarTemp(i, 1)
            xSourceKey = xarTemp(i + 1, 1)
            With m_Sigs(xParentKey)
                .SubEntities(xDestKey).DateText = .SubEntities(xSourceKey).DateText
                .SubEntities(xDestKey).PartyName = .SubEntities(xSourceKey).PartyName
                .SubEntities(xDestKey).PartyDescription = .SubEntities(xSourceKey).PartyDescription
                .SubEntities(xDestKey).IncludeIts = .SubEntities(xSourceKey).IncludeIts
                .SubEntities(xDestKey).SignerName = .SubEntities(xSourceKey).SignerName
                .SubEntities(xDestKey).SignerTitle = .SubEntities(xSourceKey).SignerTitle
            End With
        Next i
        xDestKey = xarTemp(iEnd, 1)
        
        With m_Sigs(xParentKey).SubEntities(xDestKey)
            .PartyName = m_Sig.PartyName
            .PartyDescription = m_Sig.PartyDescription
            .SignerName = m_Sig.SignerName
            .SignerTitle = m_Sig.SignerTitle
            .IncludeIts = m_Sig.IncludeIts
            .DateText = m_Sig.DateText
        End With
            
    End If
    
    grdSubSigners.Bookmark = Null

    bDisplaySubs xParentKey
    RefreshSignaturesList
    
    grdSubSigners.MarqueeStyle = dbgHighlightCell
    grdSubSigners.Bookmark = idestRow
    grdSubSigners.MousePointer = dbgMPDefault

    m_Document.StatusBar = "Signature moved"
    grdSubSigners.SetFocus
    Exit Sub
ProcError:
    g_oError.Show Err, "Error completing drag and drop operation."
    Exit Sub
End Sub
Private Sub bDisplaySubs(xKey As String)
    Dim i As Integer
    Dim oSig As CBusinessSignature
    Dim oParent As CBusinessSignature
    
    m_bSkipListUpdate = True
    m_bSkipGridChange = True
    If xKey <> "" Then
        On Error Resume Next
        Set oParent = m_Sigs(xKey)
    End If
    
    On Error GoTo ProcError
    If Not oParent Is Nothing Then
        With oParent
            If .SubEntities.Count > 0 Then
                xarSubs.ReDim 0, .SubEntities.Count, 0, 2
                xarSubs.value(0, 0) = .DisplayName
                xarSubs.value(0, 1) = .Key
                xarSubs.value(0, 2) = ""
                i = 1
                For Each oSig In .SubEntities
                    xarSubs.value(i, 0) = oSig.DisplayName
                    xarSubs.value(i, 1) = oSig.Key
                    xarSubs.value(i, 2) = m_Sigs(oSig.ParentID).Key
                    i = i + 1
                Next oSig
                With Me.grdSubSigners
                    .Array = xarSubs
                    .Rebind
                End With
                Me.grdSubSigners.Visible = True
                Me.lblSubSigners.Visible = True
                grdSubSigners.Bookmark = Null
            Else
                xarSubs.ReDim 0, -1, 0, 2
                grdSubSigners.Rebind
                Me.grdSubSigners.Visible = False
                Me.lblSubSigners.Visible = False
            End If
        End With
    Else
        xarSubs.ReDim 0, -1, 0, 2
        grdSubSigners.Rebind
        Me.grdSubSigners.Visible = False
        Me.lblSubSigners.Visible = False
    End If
    m_bSkipListUpdate = False
    m_bSkipGridChange = False
    Exit Sub
ProcError:
    g_oError.Show Err, "Error displaying sub-entities."
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyF4 And btnNewSubEntity.Enabled And _
            btnNewSubEntity.Visible Then
        btnNewSubEntity_Click
    ElseIf KeyCode = vbKeyF5 And btnNew.Enabled Then
        btnNew_Click
    ElseIf KeyCode = vbKeyF6 And btnSave.Enabled Then
        btnSave_Click
    ElseIf KeyCode = vbKeyF7 And btnDelete.Enabled Then
        btnDelete_Click
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Function PromptToSave() As Boolean
    Dim xMsg As String
    Dim iChoice As Integer
    
    On Error GoTo ProcError
    xMsg = "Save current entry?"
    iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
    If iChoice = vbYes Then
        btnSave_Click
    End If
    Exit Function
ProcError:
    g_oError.Raise "MacPac90.frmBusinessSignature.PromptToSave"
    Exit Function
End Function


Private Sub LoadDateControl(lFormat As Long)
    Dim oDateFormats As CList
    Dim oArray As XArray
    Dim lID As Long
    Dim xOrdSuffix As String
    Dim i As Integer
    
    On Error GoTo ProcError
    Set oDateFormats = g_oMPO.Lists("DateFormatsVariable")
    
    With oDateFormats
'       the date formats value of the definition
'       is the id of the group of date formats
        .FilterValue = lFormat
        .Refresh
    End With

'   get date list
    Set oArray = oDateFormats.ListItems.Source

    '   get ordinal suffix based on current date - eg "rd", "th"
    xOrdSuffix = GetOrdinalSuffix(Day(Date))
    
'       replace all instances of ordinal variable
'       with ordinal suffix-this is done here because
'       there is no way in vb to format a dateas an ordinal
    For i = oArray.LowerBound(1) To oArray.UpperBound(1)
        oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%o%", xOrdSuffix)
        oArray.value(i, 0) = xSubstitute(oArray(i, 0), "%or%", xOrdSuffix)      '9.7.1 #4402
    Next i
    
'   set up date controls
    Me.cmbDate.Array = oArray
    Me.cmbDate.Rebind
    
    
    Exit Sub
ProcError:
    Word.Application.ScreenRefresh
    g_oError.Show Err, "Could not successfully load the date control."
    Exit Sub
End Sub

Private Sub DisplayControls()
    On Error GoTo ProcError
    With m_oDB.BusinessSignatureDefs.Item(cmbSignatureType.BoundText)
        If UCase(Me.txtEntityTitle) = "SUB-ENTITY SIGNATURE" Then
            Me.txtEntityTitle.Enabled = False
            Me.lblPartyName.Enabled = .AllowSubPartyName
            Me.txtPartyName.Enabled = .AllowSubPartyName
            Me.lblPartyDescription.Enabled = .AllowSubPartyDescription
            Me.txtPartyDescription.Enabled = .AllowSubPartyDescription
            Me.lblSignerName.Enabled = .AllowSubSignerName
            Me.txtSignerName.Enabled = .AllowSubSignerName
            Me.lblSignerTitle.Enabled = .AllowSubSignerTitle
            Me.txtSignerTitle.Enabled = .AllowSubSignerTitle
    
            If .AllowSubIts Then
                Me.chkIncludeIts.Enabled = True
            Else
                Me.chkIncludeIts.Enabled = False
                Me.chkIncludeIts = 0
            End If
        Else
            Me.lblEntityTitle.Enabled = .AllowEntityTitle
            Me.txtEntityTitle.Enabled = .AllowEntityTitle
            Me.lblPartyName.Enabled = .AllowPartyName
            Me.txtPartyName.Enabled = .AllowPartyName
            Me.lblPartyDescription.Enabled = .AllowPartyDescription
            Me.txtPartyDescription.Enabled = .AllowPartyDescription
            Me.lblSignerName.Enabled = .AllowSignerName
            Me.txtSignerName.Enabled = .AllowSignerName
            Me.lblSignerTitle.Enabled = .AllowSignerTitle
            Me.txtSignerTitle.Enabled = .AllowSignerTitle
            
            If .AllowIts Then
                Me.chkIncludeIts.Enabled = True
            Else
                Me.chkIncludeIts.Enabled = False
                Me.chkIncludeIts = 0
            End If
        End If
    End With
    
    Exit Sub
ProcError:
    RaiseError "MacPac90.frmBusinessSignature.DisplayControls"
    Exit Sub
End Sub
