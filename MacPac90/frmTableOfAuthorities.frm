VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmTableOfAuthorities 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insert Table of Authorities"
   ClientHeight    =   4668
   ClientLeft      =   3828
   ClientTop       =   1080
   ClientWidth     =   5028
   Icon            =   "frmTableOfAuthorities.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4668
   ScaleWidth      =   5028
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkRestartPageNumbering 
      Appearance      =   0  'Flat
      Caption         =   "&Restart Page Numbering"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1805
      TabIndex        =   4
      Top             =   1200
      Width           =   2955
   End
   Begin VB.CheckBox chkUsePassim 
      Appearance      =   0  'Flat
      Caption         =   "Use &Passim"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1805
      TabIndex        =   3
      Top             =   870
      Width           =   2955
   End
   Begin VB.OptionButton optParaCenter 
      Appearance      =   0  'Flat
      Caption         =   "&Center Aligned"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3320
      TabIndex        =   12
      Top             =   2580
      Width           =   1440
   End
   Begin VB.OptionButton optParaLeft 
      Appearance      =   0  'Flat
      Caption         =   "&Left Aligned"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1805
      TabIndex        =   11
      Top             =   2580
      Value           =   -1  'True
      Width           =   1350
   End
   Begin VB.CheckBox chkFontSmallCaps 
      Appearance      =   0  'Flat
      Caption         =   "&Small Caps"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3320
      TabIndex        =   9
      Top             =   2085
      Width           =   1140
   End
   Begin VB.CheckBox chkFontAllCaps 
      Appearance      =   0  'Flat
      Caption         =   "&All Caps"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3320
      TabIndex        =   8
      Top             =   1770
      Width           =   1005
   End
   Begin VB.CheckBox chkFontUnderline 
      Appearance      =   0  'Flat
      Caption         =   "&Underline"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1805
      TabIndex        =   7
      Top             =   2085
      Width           =   1005
   End
   Begin VB.CheckBox chkFontBold 
      Appearance      =   0  'Flat
      Caption         =   "&Bold"
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1805
      TabIndex        =   6
      Top             =   1770
      Width           =   1005
   End
   Begin VB.CheckBox chkSectionOnly 
      Appearance      =   0  'Flat
      Caption         =   "Setup Section &Only (Don't Generate)"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1805
      TabIndex        =   2
      Top             =   540
      Width           =   2955
   End
   Begin VB.CheckBox chkKeepFormat 
      Appearance      =   0  'Flat
      Caption         =   "K&eep Original Formatting"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1805
      TabIndex        =   1
      Top             =   210
      Width           =   2955
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3845
      TabIndex        =   18
      Top             =   4215
      Width           =   1000
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   2780
      TabIndex        =   17
      Top             =   4215
      Width           =   1000
   End
   Begin TrueDBList60.TDBCombo cmbLocation 
      Height          =   600
      Left            =   1805
      OleObjectBlob   =   "frmTableOfAuthorities.frx":058A
      TabIndex        =   14
      Top             =   3075
      Width           =   3060
   End
   Begin TrueDBList60.TDBCombo cmbPageNumberFormat 
      Height          =   555
      Left            =   1800
      OleObjectBlob   =   "frmTableOfAuthorities.frx":27A5
      TabIndex        =   16
      Top             =   3620
      Width           =   3060
   End
   Begin VB.Label lblPageNumberFormat 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Page &Number Format:"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   60
      TabIndex        =   15
      Top             =   3660
      Width           =   1545
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Heading Paragraph:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   155
      TabIndex        =   10
      Top             =   2580
      Width           =   1455
   End
   Begin VB.Label lblFont 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Headings Font:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   155
      TabIndex        =   5
      Top             =   1785
      Width           =   1455
   End
   Begin VB.Label lblLocation 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "&Insert At:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   155
      TabIndex        =   13
      Top             =   3135
      Width           =   1455
   End
   Begin VB.Label lblOptions 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00000040&
      BackStyle       =   0  'Transparent
      Caption         =   "Options:"
      ForeColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   155
      TabIndex        =   0
      Top             =   255
      Width           =   1455
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00404040&
      BackStyle       =   1  'Opaque
      BorderColor     =   &H80000003&
      FillColor       =   &H80000003&
      Height          =   5070
      Left            =   -30
      Top             =   -390
      Width           =   1745
   End
   Begin VB.Menu mnuExhibits 
      Caption         =   "Exhibits"
      Visible         =   0   'False
      Begin VB.Menu mnuExhibitsDelete1 
         Caption         =   "&Delete"
      End
      Begin VB.Menu mnuExhibitsDeleteAll 
         Caption         =   "Delete &All"
      End
   End
End
Attribute VB_Name = "frmTableOfAuthorities"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bInit As Boolean
Private m_bCancelled As Boolean
Private m_oTOA As MPO.CTableOfAuthorities

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Initializing() As Boolean
    Initializing = m_bInit
End Property
Public Property Let Initializing(bNew As Boolean)
    m_bInit = bNew
End Property
Public Property Let TOA(oNew As MPO.CTableOfAuthorities)
    Set m_oTOA = oNew
End Property
Public Property Get TOA() As MPO.CTableOfAuthorities
    Set TOA = m_oTOA
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property
Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
End Sub
Private Sub btnOK_Click()
    
    On Error GoTo ProcError
    Me.Hide
    EchoOff
    Application.ScreenUpdating = False
    With m_oTOA
        .KeepFormatting = 0 - Me.chkKeepFormat
        .SectionOnly = 0 - Me.chkSectionOnly
        .Location = cmbLocation.BoundText
        .UsePassim = 0 - Me.chkUsePassim
        .RestartPageNumbering = 0 - Me.chkRestartPageNumbering
        .PageNumberFormat = cmbPageNumberFormat.BoundText
        With .HeadingStyle
            With .Font
                .Name = ActiveDocument.Styles(wdStyleNormal).Font.Name
                .Size = ActiveDocument.Styles(wdStyleNormal).Font.Size
                .Bold = 0 - Me.chkFontBold
                .Underline = Me.chkFontUnderline
                .AllCaps = 0 - Me.chkFontAllCaps
                .SmallCaps = 0 - Me.chkFontSmallCaps
            End With
            If Me.optParaCenter Then
                .ParagraphFormat.Alignment = wdAlignParagraphCenter
            Else
                .ParagraphFormat.Alignment = wdAlignParagraphLeft
            End If
        End With
        .Finish
    End With
    
    SetUserIni "TOA", "Type", cmbPageNumberFormat.BoundText
    SetUserIni "TOA", "Location", chkRestartPageNumbering.value
    
    EchoOn
    Application.ScreenUpdating = True
    Exit Sub
ProcError:
    g_oError.Show Err
    EchoOn
    Application.ScreenUpdating = True
End Sub

Private Sub chkFontAllCaps_Click()
    If chkFontAllCaps = 1 Then _
        chkFontSmallCaps = 0
End Sub

Private Sub chkFontSmallCaps_Click()
    If chkFontSmallCaps = 1 Then _
        chkFontAllCaps = 0
End Sub


'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Form_Activate()
    Dim oDoc As MPO.CDocument
    Dim lTOCSecIndex As Long
    Set oDoc = New MPO.CDocument
    cmbLocation.Bookmark = 0
    lTOCSecIndex = oDoc.lTOCExists()
    
    If GetUserIni("TOA", "Location") <> "" Then     'GLOG : 5710 : gm
        Me.chkRestartPageNumbering.value = GetUserIni("TOA", "Location")
    ElseIf InStr(LCase(oDoc.GetVar("bpfile")), "pleadingca") And _
        lTOCSecIndex = 0 Then
        Me.chkRestartPageNumbering.value = vbUnchecked
    Else
        Me.chkRestartPageNumbering.value = vbChecked
    End If
    If GetUserIni("TOA", "Type") <> "" Then         'GLOG : 5708 : gm
        cmbPageNumberFormat.BoundText = GetUserIni("TOA", "Type")
    ElseIf InStr(LCase(oDoc.GetVar("bpfile")), "pleadingca") Then
        cmbPageNumberFormat.SelectedItem = 0 'Arabic
    Else
        cmbPageNumberFormat.SelectedItem = 2 'Lower Case Roman
    End If
    
    Me.Initializing = False
End Sub

Private Sub Form_Load()
    Dim oDoc As MPO.CDocument
    Dim xarLocations As XArray
    Dim lTOCSecIndex As Long
    
    On Error GoTo ProcError
    Me.Initializing = True
    
    If m_oTOA Is Nothing Then _
        Set m_oTOA = g_oMPO.NewTableOfAuthorities
        
    'old TOA exists, add new TOA bookmark
    If Not m_oTOA.BoilerplateExists Then
        If m_oTOA.Document.File.Bookmarks.Exists("mpTableOfAuthorities") Then
            ActiveDocument.Bookmarks.Add "_zzmpFIXED_TableOfAuthorities", _
                                         ActiveDocument.Bookmarks("mpTableOfAuthorities").Range
        End If
    End If
    
    Set xarLocations = g_oMPO.NewXArray
    Set oDoc = New MPO.CDocument
    
    lTOCSecIndex = oDoc.lTOCExists()
    
    With xarLocations
        If m_oTOA.BoilerplateExists Then
            .ReDim 0, 0, 0, 1
            .value(0, 0) = 0
            .value(0, 1) = "Current Location"
            cmbLocation.Enabled = False
            chkRestartPageNumbering.Enabled = False
            cmbPageNumberFormat.Enabled = False
        ElseIf lTOCSecIndex <> 0 Then   'TOC Section exists
            m_oTOA.SectionToInsertAfter = lTOCSecIndex
            .ReDim 0, 3, 0, 1
            .value(0, 0) = 4
            .value(0, 1) = "Below Table of Contents"  'GLOG : 5019 : CEH
            .value(1, 0) = 2
            .value(1, 1) = "End of Document"
            .value(2, 0) = 1
            .value(2, 1) = "Start of Document"
            .value(3, 0) = 3
            .value(3, 1) = "Insertion Point"
        Else
            .ReDim 0, 2, 0, 1
            .value(0, 0) = 2
            .value(0, 1) = "End of Document"
            .value(1, 0) = 1
            .value(1, 1) = "Start of Document"
            .value(2, 0) = 3
            .value(2, 1) = "Insertion Point"
        End If
    End With
    Me.cmbLocation.Array = xarLocations
    ResizeTDBCombo cmbLocation, 3
    
    Me.cmbPageNumberFormat.Array = xarStringToxArray("0|1, 2, 3...|1|I, II, III...|2|i, ii, iii...|3|A, B, C...|4|a, b, c...", 2, "|")
    Me.cmbPageNumberFormat.Rebind
    ResizeTDBCombo Me.cmbPageNumberFormat, 5
    
    UpdateForm
    'MoveToLastPosition Me
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmTableofAuthorities.Form_Load"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'SetLastPosition Me
    Set m_oTOA = Nothing
    Set frmTableOfAuthorities = Nothing
    Set g_oPrevControl = Nothing
End Sub

Private Sub UpdateForm()
    On Error GoTo ProcError
    With m_oTOA
        Me.chkKeepFormat = Abs(.KeepFormatting)
        Me.chkSectionOnly = Abs(.SectionOnly)
        Me.chkUsePassim = Abs(.UsePassim)
        With .HeadingStyle
            With .Font
                Me.chkFontBold = Abs(.Bold)
                Me.chkFontUnderline = .Underline
                Me.chkFontAllCaps = Abs(.AllCaps)
                Me.chkFontSmallCaps = Abs(.SmallCaps)
            End With
            If .ParagraphFormat.Alignment = wdAlignParagraphCenter Then
                Me.optParaCenter = True
            Else
                Me.optParaLeft = True
            End If
        End With
    End With
    Exit Sub
ProcError:
    g_oError.Raise Err.Number, _
              CurErrSource("frmTableofAuthorities.UpdateForm"), _
              g_oError.Desc(Err.Number)
    Exit Sub
End Sub
