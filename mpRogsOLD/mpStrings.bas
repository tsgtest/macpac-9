Attribute VB_Name = "mpStrings"
Function xTrimSpaces(ByVal xString As String) As String

    xTrimSpaces = xSubstitute(xString, _
                              Chr$(32), _
                              "")
End Function

Function xSubstitute(ByVal xString As String, _
                     xSearch As String, _
                     xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Integer
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While iSeachPos
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
        iSeachPos = InStr(UCase(xString), _
                          UCase(xSearch))
    
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function
Function xNullToString(vValue As Variant) As String
    If Not IsNull(vValue) Then _
        xNullToString = CStr(vValue)
End Function

