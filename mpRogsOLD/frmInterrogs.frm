VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Begin VB.Form frmInterrogs 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Interrogatories & Requests"
   ClientHeight    =   6555
   ClientLeft      =   3345
   ClientTop       =   1815
   ClientWidth     =   4800
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInterrogs.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6555
   ScaleWidth      =   4800
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDefaultHelp 
      Caption         =   "Help"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   370
      Left            =   135
      TabIndex        =   33
      Top             =   6110
      Width           =   720
   End
   Begin VB.CommandButton btnUpdate 
      Caption         =   "U&pdate Field Codes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   370
      Left            =   1200
      TabIndex        =   32
      ToolTipText     =   "Click to update automatic numbering field codes"
      Top             =   6110
      Width           =   1816
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   370
      Left            =   3150
      TabIndex        =   30
      ToolTipText     =   "Click to create heading"
      Top             =   6110
      Width           =   720
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   370
      Left            =   3960
      TabIndex        =   31
      Top             =   6110
      Width           =   720
   End
   Begin TabDlg.SSTab MultiPage1 
      Height          =   5940
      Left            =   90
      TabIndex        =   34
      TabStop         =   0   'False
      Top             =   50
      Width           =   4606
      _ExtentX        =   8123
      _ExtentY        =   10478
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      Tab             =   1
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "&1-Headings"
      TabPicture(0)   =   "frmInterrogs.frx":000C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "btnClear"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "txtCustomRogResponse"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "txtCustomRog"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "btnBSSetClear"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "txtHeadingSetRogs"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "txtNumberRog"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "chkAutoNumRog"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "obtnCustomRogResponse"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "obtnCustomRog"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "chkAlternate"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "lstInterrogatories"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "lblType"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "lblStartNumberAt"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "lblSet"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "lblPositionMessage"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "lblResponse"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "lblQuestion"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).ControlCount=   17
      TabCaption(1)   =   "&2-Format"
      TabPicture(1)   =   "frmInterrogs.frx":0028
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "pnlBSCharacter"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "pnlBSParagraph"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).ControlCount=   2
      Begin VB.Frame pnlBSParagraph 
         Caption         =   "Paragraph:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2820
         Left            =   60
         TabIndex        =   20
         Top             =   2720
         Width           =   4457
         Begin mpControls3.SpinTextInternational spnHLeftMargin 
            Height          =   315
            Left            =   2160
            TabIndex        =   26
            Top             =   1500
            Width           =   1000
            _ExtentX        =   1773
            _ExtentY        =   556
            IncrementValue  =   0.1
            MaxValue        =   8.5
            AppendSymbol    =   -1  'True
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin VB.OptionButton obtnBSSep 
            Caption         =   "&Interrogatory Heading On Its Own Line"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   435
            TabIndex        =   23
            Top             =   450
            Value           =   -1  'True
            Width           =   3225
         End
         Begin VB.OptionButton obtnBSRunIn 
            Caption         =   "&Run-in Interrogatory Heading"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   435
            TabIndex        =   24
            Top             =   825
            Width           =   2535
         End
         Begin mpControls3.SpinTextInternational spnLeftMargin 
            Height          =   315
            Left            =   2160
            TabIndex        =   29
            Top             =   2015
            Width           =   1000
            _ExtentX        =   1773
            _ExtentY        =   556
            IncrementValue  =   0.1
            MaxValue        =   8.5
            AppendSymbol    =   -1  'True
            Value           =   0.5
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin VB.Label lblLeftMargin 
            Caption         =   "Ind&ent (in inches) For Following Paragraph:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   435
            TabIndex        =   28
            Top             =   1940
            Width           =   1560
         End
         Begin VB.Label lbBSHIndent 
            Caption         =   "Indent (in inc&hes) For Interrogatory Heading:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   480
            Left            =   435
            TabIndex        =   25
            Top             =   1425
            Width           =   1695
         End
      End
      Begin VB.Frame pnlBSCharacter 
         Caption         =   "Character:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2250
         Left            =   75
         TabIndex        =   27
         Top             =   395
         Width           =   4441
         Begin VB.CheckBox chkBSAllCaps 
            Caption         =   "&All Caps"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   713
            TabIndex        =   18
            Top             =   442
            Value           =   1  'Checked
            Width           =   975
         End
         Begin VB.CheckBox chkBSSmallCaps 
            Caption         =   "&Small Caps"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   437
            Left            =   713
            TabIndex        =   21
            Top             =   780
            Width           =   1126
         End
         Begin VB.CheckBox chkBSBold 
            Caption         =   "&Bold Text"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2109
            TabIndex        =   19
            Top             =   442
            Value           =   1  'Checked
            Width           =   1126
         End
         Begin VB.CheckBox chkBSUnderlined 
            Caption         =   "&Underlined"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   437
            Left            =   2109
            TabIndex        =   22
            Top             =   780
            Value           =   1  'Checked
            Width           =   1126
         End
      End
      Begin VB.CommandButton btnClear 
         Caption         =   "Clear ^"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   340
         Left            =   -71370
         TabIndex        =   9
         Top             =   3960
         Width           =   766
      End
      Begin VB.TextBox txtCustomRogResponse 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   331
         Left            =   -73695
         TabIndex        =   8
         ToolTipText     =   "Enter Custom Interrogatory Response heading here"
         Top             =   3570
         Width           =   3090
      End
      Begin VB.TextBox txtCustomRog 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   331
         Left            =   -73695
         TabIndex        =   5
         ToolTipText     =   "Enter Custom Interrogatory heading here"
         Top             =   3120
         Width           =   3090
      End
      Begin VB.CommandButton btnBSSetClear 
         Caption         =   "< Clear"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   370
         Left            =   -71490
         TabIndex        =   16
         Top             =   5190
         Width           =   840
      End
      Begin VB.TextBox txtHeadingSetRogs 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -72450
         TabIndex        =   15
         ToolTipText     =   "Enter a number here greater than starting number to generate a set of headings/response headings in your document."
         Top             =   5370
         Width           =   765
      End
      Begin VB.TextBox txtNumberRog 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -72450
         TabIndex        =   13
         Top             =   4980
         Width           =   765
      End
      Begin VB.CheckBox chkAutoNumRog 
         Caption         =   "&Use Automatic Numbering"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -74850
         TabIndex        =   11
         ToolTipText     =   "Click to have Word automatically number the interrogatory headings; clear to enter numbers manually"
         Top             =   4575
         Value           =   1  'Checked
         Width           =   2250
      End
      Begin VB.OptionButton obtnCustomRogResponse 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74025
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   3585
         Width           =   195
      End
      Begin VB.OptionButton obtnCustomRog 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74025
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   3150
         Width           =   225
      End
      Begin VB.CheckBox chkAlternate 
         Caption         =   "&Alternate Between Questions && Responses"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -74850
         TabIndex        =   10
         Top             =   4275
         Value           =   1  'Checked
         Width           =   4230
      End
      Begin MSForms.ListBox lstInterrogatories 
         Height          =   2100
         Left            =   -74880
         TabIndex        =   1
         Top             =   585
         Width           =   4290
         VariousPropertyBits=   746586139
         BackColor       =   -2147483644
         ScrollBars      =   3
         DisplayStyle    =   2
         Size            =   "7569;3704"
         MatchEntry      =   0
         ListStyle       =   1
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin VB.Label lblType 
         Caption         =   "&Type:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74880
         TabIndex        =   0
         Top             =   345
         Width           =   1350
      End
      Begin VB.Label lblStartNumberAt 
         Caption         =   "Start &Numbering At:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -74070
         TabIndex        =   12
         Top             =   4995
         Width           =   1620
      End
      Begin VB.Label lblSet 
         Caption         =   "&Generate Heading/Response Set from Starting Number Through:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   -74865
         TabIndex        =   14
         Top             =   5340
         Width           =   2430
      End
      Begin VB.Label lblPositionMessage 
         Alignment       =   2  'Center
         Caption         =   "(Heading inserted at cursor position)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   -74850
         TabIndex        =   2
         Top             =   2715
         Width           =   4260
      End
      Begin VB.Label lblResponse 
         Caption         =   "Custom &Response:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   -74865
         TabIndex        =   6
         Top             =   3540
         Width           =   765
      End
      Begin VB.Label lblQuestion 
         Caption         =   "Custom &Question:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         Left            =   -74865
         TabIndex        =   3
         Top             =   2925
         Width           =   720
      End
   End
   Begin VB.PictureBox picCycleTab1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   200
      Left            =   400
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   17
      Top             =   6010
      Width           =   200
   End
   Begin VB.PictureBox picCycleTab2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   200
      Left            =   600
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   35
      Top             =   6010
      Width           =   200
   End
End
Attribute VB_Name = "frmInterrogs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public bCustomDefined As Boolean
Dim bFormInit As Boolean
Private vbControls() As String
Public m_bFinished As Boolean

Private Sub btnCancel_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnCancel_Click
End Sub

Private Sub btnFinish_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnFinish_Click
End Sub

Private Sub btnUpdate_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    btnUpdate_Click
End Sub

Private Sub chkBSAllCaps_Click()
    On Error Resume Next
    If chkBSAllCaps = 1 Then _
        Me.chkBSSmallCaps = 0
End Sub

Private Sub chkBSSmallCaps_Click()
    On Error Resume Next
    If chkBSSmallCaps = 1 Then _
        Me.chkBSAllCaps = 0
End Sub

Private Sub cmdDefaultHelp_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    cmdDefaultHelp_Click
End Sub

Private Sub lstInterrogatories_GotFocus()
    bEnsureSelectedContent lstInterrogatories
End Sub

Private Sub obtnCustomRogResponse_GotFocus()
    SendKeys "+{tab}"
End Sub

Private Sub obtnCustomRogResponse_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    SendKeys "{tab}{tab}", 1
End Sub

Private Sub picCycleTab2_GotFocus()
    CycleTabs 2, vbControls()
End Sub
Private Sub picCycleTab1_GotFocus()
    CycleTabs 1, vbControls()
End Sub
Private Sub spnHLeftMargin_LostFocus()
    VBACtlLostFocus Me.spnHLeftMargin, vbControls()
End Sub

Private Sub spnHLeftMargin_Validate(Cancel As Boolean) '*c
    If Not spnHLeftMargin.IsValid Then _
        Cancel = True
End Sub

Private Sub spnLeftMargin_LostFocus()
    VBACtlLostFocus Me.spnLeftMargin, vbControls()
End Sub
Private Sub lstInterrogatories_LostFocus()
    VBACtlLostFocus Me.lstInterrogatories, vbControls()
End Sub
Private Sub MultiPage1_Click(PreviousTab As Integer)
    If Not g_ActivatingTabProgrammatically Then
        DisableInactiveTabs Me
        SelectFirstCtlOnTab Me.MultiPage1, vbControls()
    End If
End Sub

Private Sub spnLeftMargin_Validate(Cancel As Boolean) '*c
    If Not spnLeftMargin.IsValid Then _
        Cancel = True
End Sub

Private Sub txtCustomRog_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then _
        btnFinish_Click
End Sub

Private Sub txtCustomRog_LostFocus()
    'If txtCustomRog = "" Then _
        obtnCustomRog.Value = False
End Sub

Private Sub txtCustomRogResponse_GotFocus()
    bEnsureSelectedContent Me.txtCustomRogResponse
    obtnCustomRogResponse = True
    If txtCustomRogResponse <> "" Then
        bCustomDefined = True
    End If
End Sub
Private Sub txtCustomRog_GotFocus()
    obtnCustomRog.Value = True
    bEnsureSelectedContent Me.txtCustomRog
End Sub

Private Sub txtCustomRogResponse_LostFocus()
    If txtCustomRogResponse = "" Then
        'obtnCustomRogResponse = False
    Else
        bCustomDefined = True
    End If
End Sub

Private Sub txtHeadingSetRogs_GotFocus()
    bEnsureSelectedContent Me.txtHeadingSetRogs
End Sub
Private Sub txtNumberRog_GotFocus()
    bEnsureSelectedContent Me.txtNumberRog
End Sub

Function bIsEven(iInteger As Double) As Boolean
'---this function s/b ported to mpNumbers
    
    Dim iCompare As Double
    iCompare = iInteger / 2
    If Fix(iCompare) = iInteger / 2 Then
        bIsEven = True
        Exit Function
    End If
End Function


Function bWriteInterrogs() As Boolean
    Dim rngLocation As Word.Range
    Dim xRogHeading As String
    Dim xSeqName As String
    Dim bViewFields As Boolean
    Dim iReset As Integer
    Dim iEnd As Integer
    Dim iStart As Integer
    Dim i As Double
    Dim iCount As Double
    Dim iTrim As Integer
    Dim bExactSpace As Boolean
    
'    With Application
'        .ScreenRefresh
        'Screen.Mousepointer = vbHourglass
        '.StatusBar = mpMsgCreatingDoc
'        .ScreenUpdating = False
'    End With
    
    With ActiveWindow.View
        bViewFields = .ShowFieldCodes
        .ShowFieldCodes = False
    End With
    
    If ActiveDocument.Styles(wdStyleNormal).ParagraphFormat.LineSpacingRule = wdLineSpaceExactly Then
        bExactSpace = True
    End If
    
    Set rngLocation = Selection.Range
    If Right(rngLocation.Text, 1) = vbCr Then
        rngLocation.MoveEnd wdCharacter, -1
    End If
    rngLocation.Text = Empty
    On Error Resume Next
    iEnd = Val(Me.txtHeadingSetRogs)
    iStart = Val(Me.txtNumberRog)
    
    '---store docvars
    '---NOTE this line's for Orrick
    '---remove the false switch for projects based on new macpac
    '---with SetDocVars function that does not delete old docvars
    
    SetDocVars frmInterrogs
    
    If iEnd = 0 Then
        iStart = 1
        iEnd = 1
    Else
        iEnd = ((iEnd - iStart) + 1)
        If chkAlternate Then _
            iEnd = iEnd * 2
        iStart = 1
    End If
    
    iCount = Val(Me.txtNumberRog)
    'If iCount = 0 Then iCount = 1
    
    For i = iStart To iEnd
        iReset = Val(ActiveDocument.Variables("iRogReset"))
        
        If lstInterrogatories.ListIndex = -1 Then
            If obtnCustomRog Then
                xRogHeading = txtCustomRog
                If chkAlternate Then _
                    obtnCustomRogResponse = 1
            Else
                xRogHeading = txtCustomRogResponse
                If chkAlternate Then _
                    obtnCustomRog = 1
            End If
        Else
            If iStart <> iEnd Then
                If chkAlternate And bIsEven(i) Then
                    xRogHeading = lstInterrogatories.List(lstInterrogatories.ListIndex + 1, 0)
                Else
                    xRogHeading = lstInterrogatories
                End If
            Else
                xRogHeading = lstInterrogatories
            End If
        End If
        
        xSeqName = UCase(xTrimSpaces(xRogHeading))
        
'---rog formatting here
        With rngLocation
            
            With .ParagraphFormat
'---check if line spacing is double or exactly single and set accordingly
                If bExactSpace Then
                    .LineSpacingRule = wdLineSpaceExactly
                    .LineSpacing = 24
                Else
                    .LineSpacingRule = wdLineSpaceDouble
                End If
                .KeepTogether = True
                .KeepWithNext = True
                .FirstLineIndent = InchesToPoints(Me.spnHLeftMargin.Value) '*c
            End With
            
            .InsertAfter xRogHeading
            If Me.chkAutoNumRog = 1 Then
                .InsertAfter " "
            End If
            
            With .Font
                .AllCaps = Me.chkBSAllCaps
                .SmallCaps = Me.chkBSSmallCaps
                .Bold = Me.chkBSBold
                If Me.chkBSUnderlined Then
                    .Underline = wdUnderlineSingle
                End If
            End With
            
            If Me.chkAutoNumRog = 1 Then
                If txtNumberRog <> "" Or Me.txtHeadingSetRogs <> "" Then
                    If iReset < 3 And iReset > 0 Then
                        xSeqName = xSeqName & "\R" & LTrim(txtNumberRog)
                    End If
                    If chkAlternate Then
                        If iReset > 0 Then iReset = iReset + 1
                        If iReset = 3 Then iReset = 0
                    Else
                        iReset = 0
                    End If
                    With ActiveDocument
                        On Error Resume Next
                        .Variables.Add "iRogReset", iReset
                        .Variables("iRogReset") = iReset
                    End With
               End If
                .Collapse wdCollapseEnd
                .Fields.Add Range:=rngLocation, _
                            Type:=wdFieldSequence, _
                            Text:=xSeqName
                .MoveEnd Unit:=wdWord, Count:=1
            Else
                If iCount = 0 Then
                    .InsertAfter ""
                Else
                    .InsertAfter LTrim(Str(iCount))
                End If
            End If
            
            .Collapse wdCollapseEnd
            .InsertAfter ":"
            .Font.Underline = wdUnderlineNone
            If Me.obtnBSSep Then
                .InsertAfter vbCr
            Else
                .InsertAfter "  "
            End If
            .Collapse wdCollapseEnd
            
            With .ParagraphFormat
                .KeepTogether = False
                .KeepWithNext = False
                If Me.obtnBSSep Then _
                    .FirstLineIndent = InchesToPoints(Me.spnLeftMargin.Value) '*c
            End With
            If iEnd > 1 Then
                If Me.obtnBSSep Then
                    .InsertAfter vbCr
                    .Collapse wdCollapseEnd
                    With .Font
                        .Reset
                        .AllCaps = False
                        .Bold = False
                    End With
                Else
                   .Collapse wdCollapseEnd
                   .InsertAfter " "
                    With .Font
                        .Reset
                        .AllCaps = False
                        .Bold = False
                    End With
                    .InsertAfter vbCr
                End If
                .Collapse wdCollapseEnd
            Else
                .Select
                With Selection.Font
                    .Reset
                    .AllCaps = False
                    .Bold = False
                End With
            End If
        End With
'---9.4.1/#2490/MC
'        If bIsEven(i) Or _
'            Not chkAlternate Then iCount = iCount + 1
        
        If bIsEven(i) Then
            iCount = iCount + 1
        Else
            If chkAlternate = vbUnchecked Then
                iCount = iCount + 1
            End If
        End If
        
    Next i

    ActiveWindow.View.ShowFieldCodes = bViewFields
'    Application.ScreenUpdating = True
End Function

Private Sub btnBSSetClear_Click()
    txtNumberRog = ""
    txtHeadingSetRogs = ""
    txtNumberRog.SetFocus
End Sub

Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnClear_Click()
    With Me
        .txtCustomRog = ""
        .txtCustomRogResponse = ""
        .txtCustomRog.SetFocus
        bCustomDefined = False
    End With
End Sub

Private Sub btnFinish_Click()

'   alternate custom heading set and either is missing
    On Error GoTo ProcError
    If (Me.obtnCustomRog Or Me.obtnCustomRogResponse = 1) And _
            (Me.txtCustomRog = "" Or Me.txtCustomRogResponse = "") And _
            chkAlternate And txtHeadingSetRogs <> "" Then
        MsgBox "You must enter a custom heading and response.", vbInformation
        If txtCustomRog = "" Then
            txtCustomRog.SetFocus
        Else
            txtCustomRogResponse.SetFocus
        End If
        Exit Sub
    End If
    
'   custom heading with missing text
    If Me.obtnCustomRog And Me.txtCustomRog = "" Then
        MsgBox "You must enter a custom heading.", vbInformation
        txtCustomRog.SetFocus
        Exit Sub
    ElseIf Me.obtnCustomRogResponse And Me.txtCustomRogResponse = "" Then
        MsgBox "You must enter a custom response.", vbInformation
        txtCustomRogResponse.SetFocus
        Exit Sub
    End If
    
'   ending number is less than starting number
    If txtHeadingSetRogs <> "" Then
        If Val(Me.txtNumberRog) > Val(Me.txtHeadingSetRogs) Then
            MsgBox "Ending number must be greater than starting number", vbInformation
            txtHeadingSetRogs.SetFocus
            Exit Sub
        ElseIf Me.txtNumberRog = "" Then
            Me.txtNumberRog = "1"
        End If
    End If
    
'   set variables
    On Error Resume Next
    If txtNumberRog <> "" Or txtHeadingSetRogs <> "" Then
        'restart numbering
        With ActiveDocument
            .Variables.Add "iRogReset", "1"
            .Variables("iRogReset") = "1"
        End With
        If txtHeadingSetRogs <> "" Then
            'when macro is rerun, don't change list selection
            With ActiveDocument
                .Variables.Add "bWasHeadingSet", "True"
                .Variables("bWasHeadingSet") = "True"
            End With
        Else
            Dim bResetResponseNumbering As Boolean
            bResetResponseNumbering = _
                ActiveDocument.Variables("bResetResponseNumbering")
            If bResetResponseNumbering Then
                ActiveDocument.Variables("bResetResponseNumbering") = "False"
            Else
                If txtNumberRog <> "" And chkAlternate Then
                    'when macro is rerun, don't clear "start at" text box
                    With ActiveDocument
                        .Variables.Add "bResetResponseNumbering", "True"
                        .Variables("bResetResponseNumbering") = "True"
                    End With
                End If
            End If
        End If
    End If
    On Error GoTo ProcError
    
    Me.Hide
    Screen.MousePointer = vbHourglass
    DoEvents
    Word.Application.ScreenUpdating = False
    bWriteInterrogs
    Screen.MousePointer = vbDefault
    Unload Me
    Exit Sub
ProcError:
    EchoOn
    g_oError.Show Err, "Could not successfully insert interrogatories."
    Exit Sub
End Sub

Private Sub btnUpdate_Click()
    ActiveDocument.Fields.Update
    Unload Me
End Sub

Private Sub chkAutoNumRog_Click()
    If chkAutoNumRog = False Then
        lblStartNumberAt.Caption = "Enter &Number Here:"
    Else
        lblStartNumberAt.Caption = "Start &Numbering At:"
    End If
End Sub

Private Sub cmdDefaultHelp_Click()
    Dim message, helpheading
    Select Case MultiPage1.Tab
        Case 0      '---first tab
            helpheading = "Interrogatories and Requests"
            message = "Select a type of Question and Response from the list " & _
                "or type a custom Question and Response label in the " & _
                "textboxes provided.  Once you have selected a type, the macro " & _
                "will alternate between the Question and corresponding Response " & _
                "each time it is run." & vbCr & vbCr & _
                "Headings will be numbered automatically using field codes " & _
                "unless the ""Use Automatic Numbering"" checkbox is unselected, " & _
                "in which case the numbers will be inserted as text.  A range of " & _
                "paired questions and responses can be inserted at one time by " & _
                "entering a starting and ending number in the boxes provided." & vbCr & vbCr & _
                "After moving document text by cutting and pasting, clicking on " & _
                """Update Numbers Only"" will renumber headings, starting with the " & _
                "value indicated as the Starting Number.  This will only work if the " & _
                "headings were originally inserted using automatic numbering."
        Case Else      '---options tab
            helpheading = "Setting Formatting Options for Interrogatory Headings"
            message = "Select the character formatting of the interrogatory " & _
            "headings using the checkboxes at the top." + String$(2, 10) + _
            "Select option to insert the heading on a line by itself with the " + _
            "interrogatory text following in a separate paragraph, or use the run-in " & _
            "option to have the text following immediately after the heading on the same line.  " & _
            "Indents for headings and following paragraphs can be set using the controls provided."
        End Select
    
    Call HelpBalloon(helpheading, message)
End Sub

Private Sub lstInterrogatories_Click()
    obtnCustomRog = 0
    obtnCustomRogResponse = 0
End Sub

Private Sub lstInterrogatories_KeyPress(KeyAscii As MSForms.ReturnInteger)
    If KeyAscii = 13 Then _
        btnFinish_Click
End Sub

Private Sub obtnBSRunIn_Click()
    lblLeftMargin.Enabled = False
'    txtBSIndent.Enabled = False'*c
    spnLeftMargin.Enabled = False
End Sub

Private Sub obtnBSSep_Click()
    lblLeftMargin.Enabled = True
'    txtBSIndent.Enabled = True'*c
    spnLeftMargin.Enabled = True
End Sub

Private Sub obtnCustomRogResponse_Click()
    'Me.chkQuestionBeforeResponse = 1
    If obtnCustomRogResponse = True Then _
        lstInterrogatories.ListIndex = -1
End Sub

Private Sub obtnCustomRog_Click()
    'Me.chkQuestionBeforeResponse = 1
    If obtnCustomRog = True Then _
        lstInterrogatories.ListIndex = -1
End Sub

Private Sub txtCustomRog_KeyDown(KeyCode As Integer, Shift As Integer)
    obtnCustomRog.Value = 1
End Sub
Private Sub txtCustomRog_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    obtnCustomRog.Value = 1
End Sub


Private Sub txtCustomRogResponse_KeyPress(KeyAscii As Integer)
    If bCustomDefined Then _
        obtnCustomRogResponse = 1

    If KeyAscii = 13 Then _
        btnFinish_Click

End Sub

Private Sub txtCustomRogResponse_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    obtnCustomRogResponse = 1
End Sub

Private Sub Form_Activate()
    If Me.obtnCustomRog Then
        Me.txtCustomRog.SetFocus
    ElseIf Me.obtnCustomRogResponse Then
        Me.txtCustomRogResponse.SetFocus
    End If
    bFormInit = False
End Sub

Private Sub Form_Load()
    Dim iRetry As Integer
    Dim i As Integer
    Dim iListCount As Integer
    Dim iListFields As Integer
    Dim xInterrogs() As String
    Dim bCustom As Boolean
    Dim bWasHeadingSet As Boolean
    Dim bResetResponseNumbering As Boolean
    Dim xBSIndent As String
    
    Me.m_bFinished = False
    GetTabOrder Me, vbControls()
    DisableInactiveTabs Me
    
    ' * REMMED FOR WORD 2000 * Application.Statusbar  = mpMsgInitializing
    bFormInit = True
    
    xRogTitlesFile = CStr(App.Path) & "\mpRogs.dat"
'   get non-dbox control variables
    On Error Resume Next
    With ActiveDocument
        bWasHeadingSet = .Variables("bWasHeadingSet")
        bResetResponseNumbering = .Variables("bResetResponseNumbering")
    End With
    
'---reload global arrays if link
'---dropped for any reason
    On Error GoTo ReInitialize

'---get template specific lists
    bRet = bGetRogTitles(xInterrogs())

'---assign arrays to list boxes
    With Me
        bLSTList .lstInterrogatories, xInterrogs()

'---assign initial values
        If .lstInterrogatories.ListCount Then .lstInterrogatories.ListIndex = 0

'---use Word measurement unit for spinners'*c
        .spnHLeftMargin.DisplayUnit = Word.Options.MeasurementUnit '*c
        .spnLeftMargin.DisplayUnit = Word.Options.MeasurementUnit '*c
    End With

    obtnBSSep_Click

100:
    On Error Resume Next

'---restart options
    If bDocIsRestarted(ActiveDocument) Then

'--- initialize control values
        GetDocVars frmInterrogs
'--- toggle list choice
        Dim iIndex As Integer
        iIndex = lstInterrogatories.ListIndex
        
        With ActiveDocument
            If .Variables("obtnCustomRog") = "True" Then bCustom = True
            If .Variables("obtnCustomRogResponse") = "True" Then bCustom = True
        End With
        
'chCore - 9.2.1/9.3.0
        Err.Clear
        
        If Not bResetResponseNumbering Then _
            txtNumberRog = ""
        txtHeadingSetRogs = ""
        
        If bCustom = False Then
            If chkAlternate And Not bWasHeadingSet Then
                bRet = bIsEven(iIndex + 1)
                If bRet = True Then
                    lstInterrogatories.ListIndex = iIndex - 1
                Else
                    lstInterrogatories.ListIndex = iIndex + 1
                End If
            End If
            obtnCustomRog = False
            obtnCustomRogResponse = False
        Else
            If chkAlternate And Not bWasHeadingSet Then
                With ActiveDocument
                    If .Variables("obtnCustomRog") Then
                        If Err <> 5825 Then
                            obtnCustomRog = False
                            obtnCustomRogResponse = True
                        End If
                        Err = 0
                    ElseIf .Variables("obtnCustomRogResponse") = "True" Then
                        If Err <> 5825 Then
                            obtnCustomRog = True
                            obtnCustomRogResponse = False
                        End If
                        Err = 0
                    End If
                End With
            End If
        End If      'bCustom
   Else
        obtnBSSep_Click

'       default to default Body Text indent as set in the MacPac.ini
        xBSIndent = mpBase2.GetMacPacIniNumeric("Interrogs", "BodyTextIndent") '*c

        If xBSIndent = "" Then xBSIndent = "0.5"
        
        Me.spnLeftMargin.Value = xBSIndent '*c
        
   End If      'bDocIsRestarted
    
    If Me.txtCustomRog <> "" And Me.txtCustomRogResponse <> "" Then _
            bCustomDefined = True
            
'   set non-dbox control variables
    On Error Resume Next
    With ActiveDocument
        .Variables.Add "bWasHeadingSet", "False"
        .Variables("bWasHeadingSet") = "False"
    End With
        
    MultiPage1.Tab = 0
    
    ' * REMMED FOR WORD 2000 * Application.Statusbar  = mpMsgReady
'    bResetFocus
    bFormInit = False
    Exit Sub

ReInitialize:
    Select Case Err
        Case Else
            Beep
            MsgBox "Error " & Str(Err) & vbCr & _
                Error(Err), vbExclamation
        
    End Select

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmInterrogs = Nothing
End Sub
Function bGetRogTitles(xlistarray() As String) As Boolean
    Dim TextLine
    On Error GoTo Title_ERROR
    ReDim xlistarray(0)
    Open xRogTitlesFile For Input As #1 ' Open file.
    Do While Not EOF(1) ' Loop until end of file.
        Line Input #1, TextLine ' Read line into variable.
        If TextLine <> "" Then xlistarray(UBound(xlistarray)) = TextLine
        ReDim Preserve xlistarray(UBound(xlistarray) + 1)
    Loop
    If xlistarray(UBound(xlistarray)) = "" Then ReDim Preserve xlistarray(UBound(xlistarray) - 1)
Title_ERROR:
    Close #1    ' Close file.
End Function

