Attribute VB_Name = "mpDocument"
Function bDocIsRestarted(docDoc As Word.Document) As Boolean
'returns true for documents
'whose macros have been run already
    On Error GoTo DocIsRestarted_Error

    bDocIsRestarted = docDoc.Variables("Restarted")
    Exit Function

DocIsRestarted_Error:
    bDocIsRestarted = False
    Exit Function
End Function
Function mpMax(i As Double, j As Double) As Double
    If i > j Then
        mpMax = i
    Else
        mpMax = j
    End If
End Function

Function mpMin(i As Double, j As Double) As Double
    If i > j Then
        mpMin = j
    Else
        mpMin = i
    End If
End Function

