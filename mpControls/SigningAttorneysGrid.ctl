VERSION 5.00
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.UserControl SigningAttorneysGrid 
   BackStyle       =   0  'Transparent
   ClientHeight    =   4752
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4560
   EditAtDesignTime=   -1  'True
   ScaleHeight     =   4752
   ScaleWidth      =   4560
   ToolboxBitmap   =   "SigningAttorneysGrid.ctx":0000
   Begin TrueDBGrid60.TDBDropDown ddBarID 
      Height          =   930
      Left            =   0
      OleObjectBlob   =   "SigningAttorneysGrid.ctx":0312
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   3345
   End
   Begin TrueDBList60.TDBList lstAttys 
      DragIcon        =   "SigningAttorneysGrid.ctx":2DEF
      Height          =   2550
      Left            =   0
      OleObjectBlob   =   "SigningAttorneysGrid.ctx":2F41
      TabIndex        =   2
      ToolTipText     =   "Click down-arrow to close."
      Top             =   1215
      Visible         =   0   'False
      Width           =   4530
   End
   Begin VB.CheckBox chkShowAttys 
      Appearance      =   0  'Flat
      DownPicture     =   "SigningAttorneysGrid.ctx":6EE9
      ForeColor       =   &H80000008&
      Height          =   245
      Left            =   2910
      MaskColor       =   &H8000000F&
      Picture         =   "SigningAttorneysGrid.ctx":7133
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Click down-arrow to open."
      Top             =   30
      Width           =   255
   End
   Begin TrueDBGrid60.TDBGrid grdSigners 
      DragIcon        =   "SigningAttorneysGrid.ctx":737D
      Height          =   1245
      Left            =   0
      OleObjectBlob   =   "SigningAttorneysGrid.ctx":74CF
      TabIndex        =   1
      ToolTipText     =   "Click down-arrow to open."
      Top             =   15
      Width           =   4530
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      NegotiatePosition=   1  'Left
      Visible         =   0   'False
      WindowList      =   -1  'True
      Begin VB.Menu mnuAuthor_Favorite 
         Caption         =   "&Favorite"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuAuthor_SetDefault 
         Caption         =   "Set As &Default"
      End
      Begin VB.Menu mnuAuthor_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_AddNew 
         Caption         =   "&Add New Author..."
      End
      Begin VB.Menu mnuAuthor_Copy 
         Caption         =   "Cop&y Author..."
      End
      Begin VB.Menu mnuAuthor_ManageLists 
         Caption         =   "&Manage Authors..."
      End
      Begin VB.Menu mnuAuthor_Sep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuAuthor_Up1 
         Caption         =   "&Move Up"
      End
      Begin VB.Menu mnuAuthor_Down1 
         Caption         =   "Move &Down"
      End
      Begin VB.Menu mnuAuthor_Delete1 
         Caption         =   "&Delete Attorney"
      End
      Begin VB.Menu mnuAuthor_DeleteAll 
         Caption         =   "Delete &All Attorneys"
      End
   End
End
Attribute VB_Name = "SigningAttorneysGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Public Event SignerChange()
Public Event SignersChange()
Public Event KeyDown(KeyCode As Integer, Shift As Integer)
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event MenuAddNewClick()
Public Event MenuCopyClick()
Public Event MenuCopyOptionsClick()
Public Event MenuEditAuthorOptionsClick()
Public Event MenuAuthorFavoriteClick()
Public Event MenuManageListsClick()
Public Event MenuSetDefaultClick()
Public Event OnLicenseColDropDown(lSignerID As Long)
Public Event AuthorNotInList(lAuthorID As Long)
Public Event OnAuthorLoad(lAuthorID As Long, xLicenseID As String, _
                            ByRef xAttorneyName As String, xSignerEmail As String)
Public Event SelectedSigner(lAuthorID As Long, bCancelChange As Boolean)

Public Event ColumnEdit(iCol As Integer)

Public Enum AttyArrayCols
    AttyArrayCol_Source = 0
    Attyarraycol_signer = 1
    AttyArrayCol_Name = 2
    AttyArrayCol_ID = 3
    AttyArrayCol_BarID = 4
    AttyArrayCol_SignerEmail = 5
End Enum

Public Enum SignerNameCase
    SignerNameCase_None = 0
    SignerNameCase_Upper = 1
    SignerNameCase_Proper = 3
End Enum

Public Enum SA_SEPARATORS
    s_Comma = 1
    s_Semicolon = 2
    s_VBCRLineFeed = 3
    s_VBLineFeed = 4
    s_VBTab = 5
    s_UserDefined = 6
End Enum

Private m_Sep As SA_SEPARATORS
Private m_Separator As String
Private m_xBarIDPrefix As String
Private m_xBarIDSuffix As String
Private m_xarSigners As XArrayObject.XArray
Private m_xarBarIDs As XArrayObject.XArray
Private m_xarAttys As XArrayObject.XArray
Private m_xarTemp As XArrayObject.XArray
Private m_UserSeparator As String
Private m_iListRows As Integer
Private is_SourceRow As Integer
Private is_SourceCol As Integer
Private xDragFrom() As String
Private b_LoadCtlFlag As Boolean
Private m_bIncludeBarID As Boolean
Private m_bDisplayBarID As Boolean
Private m_iMaxSigners As Integer
Private m_iButtonPosScroll As Integer
Private m_iButtonPosNoScroll As Integer
Private m_iButtonPosChangeThreshhold As Integer
Private m_xSignerEmailPrefix As String
Private m_xSignerEmailSuffix As String
Private m_bIncludeSignerEmail As Boolean
Private m_bDisplayEmail As Boolean
Private m_iSignerIndex

Private m_lSignerNameCase As SignerNameCase
Private m_sRowHeight As Single

'Private WithEvents m_oFont As StdFont
Private m_ofont As StdFont


Public Property Set Font(ByVal NewFont As StdFont)
    Set m_ofont = NewFont

    With m_ofont
        .Bold = NewFont.Bold
        .Name = NewFont.Name
        .Italic = NewFont.Italic
        .Size = NewFont.Size
        .Underline = NewFont.Underline
    End With
    
    Set UserControl.grdSigners.Font = m_ofont
    With grdSigners.Font
        .Bold = m_ofont.Bold
        .Name = m_ofont.Name
        .Italic = m_ofont.Italic
        .Size = m_ofont.Size
        .Underline = m_ofont.Underline
    End With
    
'    Set UserControl.lstAttys.Font = NewFont
    With lstAttys.Font
        .Bold = m_ofont.Bold
        .Name = m_ofont.Name
        .Italic = m_ofont.Italic
        .Size = m_ofont.Size
        .Underline = m_ofont.Underline
    End With
    
    Set UserControl.lstAttys.HighlightRowStyle.Font = NewFont
    With lstAttys.HighlightRowStyle.Font
        .Bold = m_ofont.Bold
        .Name = m_ofont.Name
        .Italic = m_ofont.Italic
        .Size = m_ofont.Size
        .Underline = m_ofont.Underline
    End With
    
    Refresh
    
    PropertyChanged "Font"
End Property

Public Property Get Font() As StdFont
    Set Font = m_ofont
End Property

Public Property Let BackColor(ByVal NewValue As OLE_COLOR)
    UserControl.grdSigners.BackColor = NewValue
    UserControl.lstAttys.BackColor = NewValue
    PropertyChanged "BackColor"
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.grdSigners.BackColor
End Property

Public Property Let Visible(ByVal NewValue As Boolean)
    UserControl.Extender.Visible = NewValue
    PropertyChanged "Visible"
End Property

Public Property Get Visible() As Boolean
    Visible = UserControl.Extender.Visible
End Property

Public Property Let IncludeBarID(ByVal NewValue As Boolean)
    m_bIncludeBarID = NewValue
    PropertyChanged "IncludeBarID"
End Property

Public Property Get IncludeBarID() As Boolean
    IncludeBarID = m_bIncludeBarID
End Property

Public Property Let IncludeSignerEmail(ByVal NewValue As Boolean)
    m_bIncludeSignerEmail = NewValue
    PropertyChanged "IncludeSignerEmail"
End Property

Public Property Get IncludeSignerEmail() As Boolean
    IncludeSignerEmail = m_bIncludeSignerEmail
End Property

Public Property Let DisplayBarID(ByVal NewValue As Boolean)
    m_bDisplayBarID = NewValue
    UserControl.grdSigners.Columns(4).Visible = NewValue
    If Me.DisplayEMail = True And NewValue = True Then
        UserControl.grdSigners.Splits(0).ScrollBars = dbgBoth
    Else
        UserControl.grdSigners.Splits(0).ScrollBars = dbgVertical
    End If
    MoveButton
    PropertyChanged "DisplayBarID"
End Property

Public Property Get DisplayBarID() As Boolean
    DisplayBarID = m_bDisplayBarID
End Property

Public Property Get Enabled() As Boolean
Attribute Enabled.VB_UserMemId = -514
    Enabled = UserControl.Enabled
End Property

Public Property Let Enabled(ByVal NewValue As Boolean)
    UserControl.Enabled = NewValue
    PropertyChanged "Enabled"
End Property

Public Property Let RecordSelectors(bNew As Boolean)
    UserControl.grdSigners.RecordSelectors = bNew
    UserControl_Resize
    PropertyChanged "RecordSelectors"
End Property

Public Property Get RecordSelectors() As Boolean
    RecordSelectors = UserControl.grdSigners.RecordSelectors
End Property

Public Property Let ListVisible(bNew As Boolean)
    chkShowAttys.Value = BoolToInt(bNew)
    If lstAttys.Visible <> bNew Then
        lstAttys.Visible = bNew
    End If
End Property

Public Property Get ListVisible() As Boolean
    ListVisible = lstAttys.Visible
End Property

Public Property Let DisplayEMail(bNew As Boolean)
    m_bDisplayEmail = bNew
    If bNew = True Then
        If Me.DisplayBarID = True Then
            UserControl.grdSigners.Splits(0).ScrollBars = dbgBoth
        Else
            UserControl.grdSigners.Splits(0).ScrollBars = dbgVertical
        End If
        grdSigners.Columns(5).Width = 1450
        grdSigners.Columns(4).Width = 750
    Else
        UserControl.grdSigners.Splits(0).ScrollBars = dbgVertical
    End If
    grdSigners.Columns(5).Visible = bNew
    MoveButton
End Property

Public Property Get DisplayEMail() As Boolean
    DisplayEMail = m_bDisplayEmail
End Property


Public Property Get SelectedRow() As Integer
    On Error Resume Next
    If IsNull(grdSigners.RowBookmark(grdSigners.Row)) Then
        SelectedRow = -1
    Else
        SelectedRow = grdSigners.RowBookmark(grdSigners.Row)
    End If
End Property

Public Property Let SelectedRow(iNew As Integer)
End Property

Public Property Get SeparatorSpecial() As SA_SEPARATORS
    SeparatorSpecial = m_Sep
End Property
Public Property Let SeparatorSpecial(b_NewSep As SA_SEPARATORS)

    m_Sep = b_NewSep
    Select Case b_NewSep
        Case s_Comma
            m_Separator = ", "
        Case s_Semicolon
            m_Separator = "; "
        Case s_VBCRLineFeed
            m_Separator = vbCrLf
        Case s_VBTab
            m_Separator = vbTab
        Case s_VBLineFeed
            m_Separator = vbLf
        Case s_UserDefined
            If m_Separator = "" Then m_Separator = ", "
        Case Else
        End Select
    m_Sep = b_NewSep
    PropertyChanged "SeparatorSpecial"
End Property

Public Property Get Separator() As String
    Separator = m_Separator
End Property

Public Property Let Separator(xNew As String)
    m_Separator = xNew
    PropertyChanged "Separator"
End Property

Public Property Get BarIDPrefix() As String
    BarIDPrefix = m_xBarIDPrefix
End Property

Public Property Let BarIDPrefix(xNew As String)
    m_xBarIDPrefix = xNew
    PropertyChanged "BarIDPrefix"
End Property

Public Property Get SignerEmailPrefix() As String
    SignerEmailPrefix = m_xSignerEmailPrefix
End Property

Public Property Let SignerEmailPrefix(xNew As String)
    m_xSignerEmailPrefix = xNew
    PropertyChanged "SignerEmailPrefix"
End Property

Public Property Get SignerEmailSuffix() As String
    SignerEmailSuffix = m_xSignerEmailSuffix
End Property

Public Property Let SignerEmailSuffix(xNew As String)
    m_xSignerEmailSuffix = xNew
    PropertyChanged "SignerEmailSuffix"
End Property

Public Property Get BarIDSuffix() As String
    BarIDSuffix = m_xBarIDSuffix
End Property

Public Property Let BarIDSuffix(xNew As String)
    m_xBarIDSuffix = xNew
    PropertyChanged "BarIDSuffix"
End Property

Public Property Get MaxSigners() As Integer
    If m_iMaxSigners = 0 Then _
        m_iMaxSigners = 4
    MaxSigners = m_iMaxSigners
End Property

Public Property Let MaxSigners(iNew As Integer)
    m_iMaxSigners = iNew
    PropertyChanged "MaxSigners"
End Property

Public Property Get RowHeight() As Single
    If m_sRowHeight < 250 Then
        m_sRowHeight = 250
    End If
    
    RowHeight = m_sRowHeight
End Property

Public Property Let RowHeight(sNew As Single)
    m_sRowHeight = Max(CDbl(sNew), 250)
    UserControl.grdSigners.RowHeight = sNew
    PropertyChanged "RowHeight"
End Property

Public Property Get ListRows() As Integer
    ListRows = m_iListRows
End Property

Public Property Let ListRows(iNew As Integer)
    Dim iMax As Integer
    On Error Resume Next
    '---change max row value based on how much form space exists
    DoEvents
    With UserControl
        iMax = Fix((.Parent.Height - (Extender.Top + .Height)) / .lstAttys.RowHeight) - 2
        If iMax < 0 Then iMax = 0
        If iNew = 0 Then iNew = 5
        iNew = Min(CDbl(iMax), CDbl(iNew))
    End With
    m_iListRows = iNew
    ResizeTDBList lstAttys, iNew
    PropertyChanged ListRows
End Property

Public Property Get Attorneys() As XArrayObject.XArray
    Set Attorneys = m_xarAttys
End Property

Public Property Let Attorneys(xarNew As XArrayObject.XArray)
    b_LoadCtlFlag = True
    Set m_xarAttys = xarNew
    lstAttys.Array = xarNew
    lstAttys.ReBind
    ResizeTDBList lstAttys, Me.ListRows
End Property

Public Property Let SignerBarIDs(xarNew As XArrayObject.XArray)
    b_LoadCtlFlag = True
    Dim i As Integer
    On Error Resume Next

'-delete old array elements -- necessary because of xarray "linking"
    UserControl.ddBarID.Array = New XArray
    UserControl.ddBarID.ReBind
    i = xarNew.Count(1)
    If i = 0 Then
        i = 1
        xarNew.ReDim 0, 0, 0, 3
        xarNew.Value(0, 1) = "No Bar ID available"
    End If
    UserControl.ddBarID.Array = xarNew
    UserControl.ddBarID.ReBind
    UserControl.ddBarID.Height = UserControl.ddBarID.RowHeight * i + 15

End Property

Public Property Get Signers() As XArrayObject.XArray
'    Set Signers = m_xarSigners
    Set Signers = grdSigners.Array
End Property

Public Property Let Signers(xarNew As XArrayObject.XArray)
    On Error Resume Next
    grdSigners.Array = xarNew
    grdSigners.ReBind
    grdSigners.Refresh
    grdSigners.Bookmark = Null
    RaiseEvent SignersChange
End Property


Public Property Let Author(lNew As Long)
    'grdSigners.Bookmark = lNew
    If lNew = 0 Then Exit Property
    Dim i As Integer
    Dim iSource As Integer
    Dim xName As String
    Dim lID As Long
    Dim xBarID As String
    Dim xSignerEmail As String
    
    On Error Resume Next
    With UserControl.grdSigners
        If .Array.UpperBound(1) = 0 Then       'grid's empty -- grab entry from list
'---determine if lNew exists in List
            UserControl.lstAttys.BoundText = lNew
            If UserControl.lstAttys.BoundText = "" Then
                RaiseEvent AuthorNotInList(lNew)
                'Exit Property
            End If
'            With m_xarSigners
            With grdSigners.Array
                UserControl.grdSigners.Bookmark = Null
                If .Value(.UpperBound(1), AttyArrayCol_Name) <> "" Then _
                        .Insert 1, .UpperBound(1) + 1
                With UserControl.lstAttys
                    iSource = .Array.Value(.RowBookmark(.Row), 0)
                    lID = .Array.Value(.RowBookmark(.Row), 2)
                    xBarID = .Array.Value(.RowBookmark(.Row), 3)
''''                    xSignerEmail = .Array.Value(.RowBookmark(.Row), 4)
                End With
                
                RaiseEvent OnAuthorLoad(lID, xBarID, xName, xSignerEmail)
                .Value(.UpperBound(1), 0) = iSource
                .Value(.UpperBound(1), 1) = Empty
                .Value(.UpperBound(1), 2) = xName
                .Value(.UpperBound(1), 3) = lID
                .Value(.UpperBound(1), 4) = xBarID
                .Value(.UpperBound(1), 5) = xSignerEmail
            End With
            grdSigners.ReBind
            grdSigners.Refresh
            grdSigners.Bookmark = grdSigners.Array.UpperBound(1)
        End If
        For i = 0 To .Array.UpperBound(1)
            If .Array.Value(i, AttyArrayCol_ID) = lNew Then
                grdSigners.Bookmark = i
                ChangeSigner
            End If
        Next i
    End With

End Property

Public Property Get Author() As Long
    Dim i As Integer
    On Error Resume Next
    With UserControl.grdSigners
        For i = 0 To .Array.UpperBound(1)
            '---4241 - explicit value works better here
            If .Array.Value(i, 1) = -1 Then
                Author = .Array.Value(i, 3)
                Exit For
            Else
                Author = -1
            End If
        Next i
    End With
End Property

Public Property Get SignersText() As String
    Dim xTemp As String
    Dim i As Integer
'''    Dim xPref As String
'''    Dim xSuff As String
    Dim xBarID As String
    Dim xSignerEmail As String
    Dim xSep As String
    Dim xName As String

    xSep = m_Separator
    
    
    If UserControl.grdSigners.Array.UpperBound(1) >= 0 Then
        For i = 0 To UserControl.grdSigners.Array.UpperBound(1)
''''            '---include bar ID & specified pref & suff
''''            If Me.IncludeBarID Then
''''                If Len(Me.BarIDPrefix) > 0 Then
''''                    If Len(UserControl.grdSigners.Array.Value(i, 4)) > 0 Then
''''                        xPref = Me.BarIDPrefix
''''                        xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & xPref & _
''''                                        UserControl.grdSigners.Array.Value(i, 4) & Me.BarIDSuffix & m_Separator
''''                    Else
''''                        '---return signer name only
''''                        xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & m_Separator
''''                    End If
''''                Else
''''                    xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & m_Separator & m_Separator & _
''''                                    UserControl.grdSigners.Array.Value(i, 4) & m_Separator
''''                End If
''''            Else
''''                '---return signer name only
''''                xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & m_Separator
''''            End If
''''        Next
    
    
            
            If Me.IncludeBarID Then
                xBarID = UserControl.grdSigners.Array.Value(i, AttyArrayCol_BarID)
                If xBarID <> "" Then
                    xBarID = Me.BarIDPrefix & xBarID & Me.BarIDSuffix
                End If
                If Len(Me.BarIDPrefix) = 0 Then
                    xSep = String(2, m_Separator)
                End If
            End If
    
            If Me.IncludeSignerEmail Then
                xSignerEmail = UserControl.grdSigners.Array.Value(i, AttyArrayCol_SignerEmail)
                If xSignerEmail <> "" Then
                    xSignerEmail = Me.SignerEmailPrefix & xSignerEmail & Me.SignerEmailSuffix
                End If
            End If
    
            xName = UserControl.grdSigners.Array.Value(i, AttyArrayCol_Name)
    
            If Me.SignerNameCase > SignerNameCase_None Then
                xName = StrConv(xName, Me.SignerNameCase)
            End If
    
    
            xTemp = xTemp & xName & xBarID & xSignerEmail & m_Separator
    
    
        Next
    
    xTemp = Left(xTemp, Len(xTemp) - Len(m_Separator))
    End If
    SignersText = xTemp
End Property

Public Property Get SignersEmailText() As String
    Dim xTemp As String
    Dim i As Integer
'''    Dim xPref As String
'''    Dim xSuff As String
    Dim xBarID As String
    Dim xSignerEmail As String
    Dim xSep As String
    Dim xName As String

    xSep = m_Separator
    
    
    If UserControl.grdSigners.Array.UpperBound(1) >= 0 Then
        For i = 0 To UserControl.grdSigners.Array.UpperBound(1)
''''            If Me.IncludeSignerEmail Then
                xSignerEmail = UserControl.grdSigners.Array.Value(i, AttyArrayCol_SignerEmail)
                If xSignerEmail <> "" Then
                    xSignerEmail = Me.SignerEmailPrefix & xSignerEmail & Me.SignerEmailSuffix
                End If
''''            End If
            '---trim leading & trailing line feed or tab chars from string
            xSignerEmail = xTrimTrailingChrs(xSignerEmail, vbCrLf, True)
            xSignerEmail = xTrimTrailingChrs(xSignerEmail, Chr(11), True)
            xSignerEmail = xTrimTrailingChrs(xSignerEmail, Chr(13), True)
            xSignerEmail = xTrimTrailingChrs(xSignerEmail, Chr(10), True)
            xSignerEmail = xTrimTrailingChrs(xSignerEmail, Chr(9), True)
            
            xTemp = xTemp & xSignerEmail & Chr(11)
    
        Next
    
        xTemp = xTrimTrailingChrs(xTemp, Chr(11)) 'Left(xTemp, Len(xTemp) - Len(m_Separator))
    End If
    SignersEmailText = xTemp
End Property



Public Property Get SignerText() As String
    Dim xTemp As String
    Dim xPref As String
    Dim xSuff As String
    Dim i As Integer
    Dim xBarID As String
    Dim xSignerEmail As String
    Dim xSep As String
    Dim xName As String

    xSep = m_Separator
    
    On Error Resume Next
    If UserControl.grdSigners.Array.UpperBound(1) > -1 Then
        For i = 0 To UserControl.grdSigners.Array.UpperBound(1)
            '---9.4.1
            If UserControl.grdSigners.Array.Value(i, Attyarraycol_signer) = -1 Then
            '---9.4.1
''''                '---include bar ID & specified pref & suff
''''                If Me.IncludeBarID Then
''''                    If Len(Me.BarIDPrefix) > 0 Then
''''                        If Len(UserControl.grdSigners.Array.Value(i, 4)) > 0 Then
''''                            xPref = Me.BarIDPrefix
''''                            xTemp = UserControl.grdSigners.Array.Value(i, 2) & xPref & _
''''                                            UserControl.grdSigners.Array.Value(i, 4) & Me.BarIDSuffix
''''                        Else
''''                            '---return signer name only
''''                            xTemp = UserControl.grdSigners.Array.Value(i, 2)
''''                        End If
''''                    Else
''''                        xTemp = UserControl.grdSigners.Array.Value(i, 2) & m_Separator & m_Separator & _
''''                                        UserControl.grdSigners.Array.Value(i, 4) & m_Separator
''''                        xTemp = Left(xTemp, Len(xTemp) - Len(m_Separator))
''''                    End If
''''                Else
''''                    '---return signer name only
''''                    xTemp = UserControl.grdSigners.Array.Value(i, 2)
''''                End If
                
                    If Me.IncludeBarID Then
                        xBarID = UserControl.grdSigners.Array.Value(i, AttyArrayCol_BarID)
                        If xBarID <> "" Then
                            xBarID = Me.BarIDPrefix & xBarID & Me.BarIDSuffix
                        End If
                        If Len(Me.BarIDPrefix) = 0 Then
                            xSep = String(2, m_Separator)
                        End If
                    End If
            
                    If Me.IncludeSignerEmail Then
                        xSignerEmail = UserControl.grdSigners.Array.Value(i, AttyArrayCol_SignerEmail)
                        If xSignerEmail <> "" Then
                            xSignerEmail = Me.SignerEmailPrefix & xSignerEmail & Me.SignerEmailSuffix
                        End If
                    End If
            
                    xName = UserControl.grdSigners.Array.Value(i, AttyArrayCol_Name)
                    
                    If Me.SignerNameCase > SignerNameCase_None Then
                        xName = StrConv(xName, Me.SignerNameCase)
                    End If
                    
                    xTemp = xTemp & xName & xBarID & xSignerEmail & m_Separator
                
                Exit For
            End If
        Next
        '---trim trailing sep
        xTemp = Left(xTemp, Len(xTemp) - Len(m_Separator))
    End If
    SignerText = xTemp
End Property

Public Property Get OtherSignersText() As String
    Dim xTemp As String
    Dim i As Integer
    Dim xPref As String
    Dim xSuff As String
    Dim xBarID As String
    Dim xSignerEmail As String
    Dim xSep As String
    Dim xName As String

    xSep = m_Separator
    
    If UserControl.grdSigners.Array.UpperBound(1) > -1 Then
        For i = 0 To UserControl.grdSigners.Array.UpperBound(1)
            If IsEmpty(UserControl.grdSigners.Array.Value(i, Attyarraycol_signer)) Or _
                            Len(UserControl.grdSigners.Array.Value(i, Attyarraycol_signer)) = 0 Then
''''                '---include bar ID & specified pref & suff
''''                If Me.IncludeBarID Then
''''                    If Len(Me.BarIDPrefix) > 0 Then
''''                        If Len(UserControl.grdSigners.Array.Value(i, 4)) > 0 Then
''''                            xPref = Me.BarIDPrefix
''''                            xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & xPref & _
''''                                            UserControl.grdSigners.Array.Value(i, 4) & Me.BarIDSuffix & m_Separator
''''                        Else
''''                            '---return signer name only
''''                            xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & m_Separator
''''                        End If
''''                    Else
''''                        xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & m_Separator & m_Separator & _
''''                                        UserControl.grdSigners.Array.Value(i, 4) & m_Separator
''''                    End If
''''                Else
''''                    '---return signer name only
''''                    xTemp = xTemp & UserControl.grdSigners.Array.Value(i, 2) & m_Separator
''''                End If
            
            
                If Me.IncludeBarID Then
                    xBarID = UserControl.grdSigners.Array.Value(i, AttyArrayCol_BarID)
                    If xBarID <> "" Then
                        xBarID = Me.BarIDPrefix & xBarID & Me.BarIDSuffix
                    End If
                    If Len(Me.BarIDPrefix) = 0 Then
                        xSep = String(2, m_Separator)
                    End If
                End If
        
                If Me.IncludeSignerEmail Then
                    xSignerEmail = UserControl.grdSigners.Array.Value(i, AttyArrayCol_SignerEmail)
                    If xSignerEmail <> "" Then
                        xSignerEmail = Me.SignerEmailPrefix & xSignerEmail & Me.SignerEmailSuffix
                    End If
                End If
        
                xName = UserControl.grdSigners.Array.Value(i, AttyArrayCol_Name)
                
                If Me.SignerNameCase > SignerNameCase_None Then
                    xName = StrConv(xName, Me.SignerNameCase)
                End If
        
                xTemp = xTemp & xName & xBarID & xSignerEmail & m_Separator
    
            End If
        Next
    If xTemp <> "" Then xTemp = Left(xTemp, Len(xTemp) - Len(m_Separator))
    End If
    OtherSignersText = xTemp
End Property

Public Property Let SignerNameCase(ByVal NewValue As SignerNameCase)
Attribute SignerNameCase.VB_Description = "Sets Font Attribute for Signer Name portion of SignerText, SignersText, OtherSignersText"
    m_lSignerNameCase = NewValue
    PropertyChanged "SignerNameCase"
End Property

Public Property Get SignerNameCase() As SignerNameCase
    SignerNameCase = m_lSignerNameCase
End Property

Private Sub ResizeTDBList(tdbCBX As TrueDBList60.TDBList, Optional iMaxRows As Integer)
    Dim ctlArray As XArrayObject.XArray
    Dim iRows, iRowC  As Integer
    On Error Resume Next
    With tdbCBX
        Set ctlArray = .Array
        iRowC = ctlArray.Count(1)
        iRows = Min(CDbl(iRowC), CDbl(iMaxRows))
        .Height = ((iRows) * .RowHeight) + 10
    End With
    UserControl.Height = tdbCBX.Height + UserControl.grdSigners.Height + 15
    
End Sub

'************************************************
'menu methods
'************************************************

Public Property Let MenuEditAuthorOptionsEnabled(bNew As Boolean)

End Property

Public Property Let MenuFavoriteEnabled(bNew As Boolean)
    UserControl.mnuAuthor_Favorite.Enabled = bNew
End Property

Public Property Let MenuSetDefaultEnabled(bNew As Boolean)
    UserControl.mnuAuthor_SetDefault.Enabled = bNew
End Property

Public Property Let MenuFavoriteChecked(bNew As Boolean)
    UserControl.mnuAuthor_Favorite.Checked = bNew
End Property

Public Property Let MenuAddNewEnabled(bNew As Boolean)
    UserControl.mnuAuthor_AddNew.Enabled = bNew
End Property

Public Property Let MenuCopyEnabled(bNew As Boolean)
    UserControl.mnuAuthor_Copy.Enabled = bNew
End Property

Public Property Let MenuCopyOptionsEnabled(bNew As Boolean)

End Property

Public Property Let MenuManageListsEnabled(bNew As Boolean)
    UserControl.mnuAuthor_ManageLists.Enabled = bNew
End Property

Public Property Let MenuEditAuthorOptionsVisible(bNew As Boolean)

End Property

Public Property Let MenuFavoriteVisible(bNew As Boolean)
    UserControl.mnuAuthor_Favorite.Visible = bNew
End Property

Public Property Let MenuSetDefaultVisible(bNew As Boolean)
    UserControl.mnuAuthor_SetDefault.Visible = bNew
End Property

Public Property Let MenuAddNewVisible(bNew As Boolean)
    UserControl.mnuAuthor_AddNew.Visible = bNew
End Property

Public Property Let MenuCopyVisible(bNew As Boolean)
    UserControl.mnuAuthor_Copy.Visible = bNew
End Property

Public Property Let MenuCopyOptionsVisible(bNew As Boolean)

End Property

Public Property Let MenuManageListsVisible(bNew As Boolean)
    UserControl.mnuAuthor_ManageLists.Visible = bNew
End Property

Public Property Let MenuSep1Visible(bNew As Boolean)
    UserControl.mnuAuthor_Sep2.Visible = bNew
End Property

Public Property Let MenuSep2Visible(bNew As Boolean)
    UserControl.mnuAuthor_Sep3.Visible = bNew
End Property

Private Sub chkShowAttys_Click()
    On Error Resume Next
    grdSigners.Update
    With lstAttys
        .ZOrder 0
        .Visible = (chkShowAttys.Value = vbChecked)
        DoEvents
    End With

    If lstAttys.Visible Then
        grdSigners.ToolTipText = "Click down-arrow to close."
        chkShowAttys.ToolTipText = "Click down-arrow to close."
    Else
        grdSigners.ToolTipText = "Click down-arrow to open."
        chkShowAttys.ToolTipText = "Click down-arrow to open."
    End If
    
End Sub

Private Sub chkShowAttys_KeyDown(KeyCode As Integer, Shift As Integer)
    RaiseEvent KeyDown(KeyCode, Shift)
    If KeyCode = 27 Or KeyCode = 9 Then    'tab or escape hides list
        If lstAttys.Visible = True Then
            chkShowAttys.Value = False
            If KeyCode = 27 Then KeyCode = 0
        End If
    End If
End Sub

Private Sub ddBarID_DropDownClose()
    On Error Resume Next
    UserControl.grdSigners.Update
    UserControl.ddBarID.Array = New XArray
    UserControl.ddBarID.ReBind
End Sub

Private Sub ddBarID_DropDownOpen()
    With UserControl.grdSigners
        If .Col = AttyArrayCol_BarID Then
            RaiseEvent OnLicenseColDropDown(.Array.Value(.Row, AttyArrayCol_ID))
        End If
    End With
End Sub

Private Sub ddBarID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim overRow As Long
    On Error Resume Next
    overRow = ddBarID.RowContaining(Y)
    If overRow >= 0 Then
        ddBarID.Row = overRow
    End If
End Sub

Private Sub grdSigners_BeforeColEdit(ByVal ColIndex As Integer, ByVal KeyAscii As Integer, Cancel As Integer)
    If ColIndex = 1 Then
        Me.ChangeSigner
        DoEvents
    End If
    If ColIndex = 2 And grdSigners.Array.UpperBound(1) = -1 Then
        grdSigners.Update
    End If

    Select Case ColIndex
        Case 2, 4, 5
            RaiseEvent ColumnEdit(ColIndex)
    End Select


End Sub

Private Sub grdSigners_ButtonClick(ByVal ColIndex As Integer)
    RaiseEvent OnLicenseColDropDown(UserControl.grdSigners.Array.Value(UserControl.grdSigners.Row, AttyArrayCol_ID))
End Sub

Private Sub grdSigners_GotFocus()
    grdSigners.TabAction = 2
End Sub

Private Sub grdSigners_Change()
    On Error Resume Next
    RaiseEvent SignersChange
End Sub

Private Sub grdSigners_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
    Dim i As Integer
    ReDim xDragFrom(0, grdSigners.Array.UpperBound(2))
    
    For i = 0 To grdSigners.Array.UpperBound(2)
        xDragFrom(0, i) = grdSigners.Array.Value(RowBookmark, i)
    Next i
    
    is_SourceRow = RowBookmark
    is_SourceCol = ColIndex

    grdSigners.Row = Min(CDbl(RowBookmark), CDbl(grdSigners.VisibleRows) - 1)
    
'    grdSigners.SelBookmarks.Add (RowBookmark)
' Use Visual Basic manual drag support
    grdSigners.Drag vbBeginDrag

End Sub

Private Sub grdSigners_DragDrop(Source As Control, X As Single, Y As Single)
    Dim i As Integer
    Dim iSource As Integer
    Dim xName As String
    Dim lID As Long
    Dim xBarID As String
    Dim iBookmark As Integer
    Dim idestCol As Integer
    Dim iDestRow As Integer
    Dim j, r, c As Integer
    Dim temp() As String
    Dim xARTemp As XArray
    Dim xSignerEmail As String

    If Source Is lstAttys Then
        On Error GoTo ProcError
        With grdSigners.Array
            If .UpperBound(1) = -1 Then UserControl.grdSigners.Bookmark = Null
            If grdSigners.Array.Value(grdSigners.Array.UpperBound(1), 2) <> "" Then
                .Insert 1, .UpperBound(1) + 1
            End If
            'Bookmark = lstAttys.SelBookmarks(i)
            iBookmark = lstAttys.RowBookmark(lstAttys.Row)
            
            With Me.Attorneys
                iSource = .Value(iBookmark, 0)
                'xName = xFlipName(.Value(iBookmark, 1), ",")
                lID = .Value(iBookmark, 2)
'---poll the front end for a barID
                RaiseEvent OnAuthorLoad(lID, xBarID, xName, xSignerEmail)
                'xBarID = .Value(iBookmark, 3)
            End With
            
            .Value(.UpperBound(1), 0) = iSource
            .Value(.UpperBound(1), 1) = Empty
            .Value(.UpperBound(1), 2) = xName
            .Value(.UpperBound(1), 3) = lID
            .Value(.UpperBound(1), 4) = xBarID
            .Value(.UpperBound(1), 5) = xSignerEmail
        End With
        
        If grdSigners.Array.UpperBound(1) = 0 Then
            grdSigners.Array.Value(0, 1) = -1
            grdSigners.ReBind
            grdSigners.Refresh
            RaiseEvent SignerChange
        End If
        grdSigners.ReBind
        grdSigners.Refresh
        grdSigners.Bookmark = Null
        grdSigners.Bookmark = grdSigners.Array.UpperBound(1)
    ElseIf Source Is grdSigners Then

'---set xarray
        Set xARTemp = New XArray
        With grdSigners.Array
            xARTemp.ReDim .LowerBound(1), .UpperBound(1), .LowerBound(2), .UpperBound(2)
            For i = .LowerBound(1) To .UpperBound(1)
                For j = .LowerBound(2) To .UpperBound(2)
                    xARTemp.Value(i, j) = .Value(i, j)
                Next
            Next
        End With
'---Get drop zone
        On Error Resume Next
        iDestRow = grdSigners.RowBookmark(grdSigners.RowContaining(Y))

'---drop at end of list
        If iDestRow = 0 And grdSigners.RowContaining(Y) <> 0 Then iDestRow = xARTemp.UpperBound(1) + 1

'---empty out source Record
        For i = 0 To xARTemp.UpperBound(2)
            xARTemp(is_SourceRow, i) = Empty
        Next i

'---create row space in array if necessary
        If iDestRow >= xARTemp.UpperBound(1) Then
            xARTemp.Insert 1, xARTemp.UpperBound(1) + iDestRow
        Else
            xARTemp.Insert 1, iDestRow
        End If

'---move down only within dropped columns
        ReDim temp(0, xARTemp.UpperBound(2)) As String

        For i = iDestRow To iDestRow
            For j = 0 To xARTemp.UpperBound(2)
                temp(0, j) = xARTemp.Value(i, j)
                xARTemp(i, j) = xDragFrom(0, j)
                xDragFrom(0, j) = temp(0, j)
            Next j
        Next i

        If temp(0, 0) <> Empty Then
            For i = iDestRow + 1 To xARTemp.UpperBound(1)
                For j = 0 To xARTemp.UpperBound(2)
                    temp(0, j) = xARTemp.Value(i, j)
                    xARTemp(i, j) = xDragFrom(0, j)
                    xDragFrom(0, j) = temp(0, j)
                Next j
            Next i
        End If

'---remove empty rows
        For i = 0 To xARTemp.UpperBound(1)
            If xARTemp(i, 2) = Empty Then
                xARTemp.Delete 1, i
            End If
        Next
        
        grdSigners.Array = xARTemp
        grdSigners.Bookmark = Null
        grdSigners.ReBind
        grdSigners.Refresh
        
        If iDestRow > is_SourceRow Then
            grdSigners.Row = iDestRow - 1
        Else
            grdSigners.Row = iDestRow
        End If
    
    End If
    RaiseEvent SignersChange
    Exit Sub
    
ProcError:
    Select Case Err.Number
        Case 9
            xARTemp.ReDim 0, -1, 0, 5
            Resume
        Case Else
            Err.Raise Err.Number
    End Select

End Sub
Private Sub grdSigners_DragOver(Source As Control, X As Single, Y As Single, State As Integer)
' DragOver provides  visual feedback
    
    Dim overCol As Integer
    Dim overRow As Long

    overRow = grdSigners.RowContaining(Y)
    If overRow >= 0 Then grdSigners.Row = overRow

End Sub

Private Sub grdSigners_KeyDown(KeyCode As Integer, Shift As Integer)
    RaiseEvent KeyDown(KeyCode, Shift)
    On Error Resume Next
    
    Select Case Shift
        Case 3  '---control/shift
            Select Case KeyCode
                Case 38 '---up
                    MoveCurrentRecord -1
                Case 40 '---down
                    MoveCurrentRecord 2
                Case Else
            End Select
        Case 4
            If KeyCode = 40 And grdSigners.Col = 2 Then
                chkShowAttys.Value = vbChecked
            ElseIf KeyCode = 40 And grdSigners.Col = 4 Then
                RaiseEvent OnLicenseColDropDown(UserControl.grdSigners.Array.Value(UserControl.grdSigners.Row, AttyArrayCol_ID))
            End If
        Case 5  '--alt/shift
            If KeyCode = 40 Then
                UserControl.PopupMenu mnuPopup, X:=UserControl.CurrentX + UserControl.Width, Y:=UserControl.CurrentY + grdSigners.Height
            End If
        Case Else
                If KeyCode = 13 Then    '---change signer if in col 1
                    If grdSigners.Col = 1 Then
                        ChangeSigner
                    Else
                        KeyCode = 0
                        GridMoveNext UserControl.grdSigners
                    End If
                End If
                If KeyCode = 27 Or KeyCode = 9 Then    'tab or escape hides list
                    If lstAttys.Visible = True Then
                        chkShowAttys.Value = False
                        If KeyCode = 27 Then KeyCode = 0
                    End If

                    If grdSigners.RowBookmark(grdSigners.Row) = grdSigners.Array.UpperBound(1) Or _
                        IsNull(grdSigners.RowBookmark(grdSigners.Row)) Then
                        If grdSigners.Col = 2 Or grdSigners.Col = 4 Then
                            '---change grid navigation to enable tab out
                            grdSigners.TabAction = 1
                        End If
                    End If
                
                End If
   End Select

End Sub

Private Sub grdSigners_LeftColChange(ByVal SplitIndex As Integer)
    MoveButton
End Sub

Private Sub grdSigners_LostFocus()
    If Me.Author = -1 Then
        If grdSigners.Columns(2).Text <> "" Then
            grdSigners.Update
            ChangeSigner
        Else
            grdSigners.Update
        End If
    End If
End Sub

Private Sub grdSigners_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, _
            ScaleX(X, UserControl.ScaleMode, vbContainerPosition), _
            ScaleY(Y, UserControl.ScaleMode, vbContainerPosition))

    If lstAttys.Visible = True Then
        chkShowAttys.Value = False
    End If
 
    If Button = 2 Then
        UserControl.PopupMenu mnuPopup
    End If

End Sub

Private Sub grdSigners_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    RaiseEvent MouseUp(Button, Shift, _
            ScaleX(X, UserControl.ScaleMode, vbContainerPosition), _
            ScaleY(Y, UserControl.ScaleMode, vbContainerPosition))
    
    Dim overRow As Long
    Dim overCol As Long
    Dim i As Integer
    On Error Resume Next
    With grdSigners
        If .Array.Value(.RowContaining(Y), 1) = -1 Then Exit Sub
        .Update
        .RowBookmark(.Row).Delete
        overRow = .RowContaining(Y)
        If overRow >= 0 Then grdSigners.Row = overRow
        If .ColContaining(X) = 1 Then
            DoEvents
            ChangeSigner
            grdSigners.SetFocus
        End If
    
        If grdSigners.Col = 4 Then
            If grdSigners.Array.Value(grdSigners.Row, 3) <> Empty Then
                grdSigners.Columns(4).DropDown = UserControl.ddBarID
            Else
                grdSigners.Columns(4).DropDown = ""
            End If
        End If
    
    End With
   
   
End Sub

Private Sub grdSigners_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    '---turn on dropdown lookup only if item is inserted via lookup ie, is a macpac person
    On Error Resume Next
    If grdSigners.Col = 4 Then
        If grdSigners.Array.Value(grdSigners.Row, 3) <> Empty Then
            grdSigners.Columns(4).DropDown = UserControl.ddBarID
        Else
            grdSigners.Columns(4).DropDown = ""
        End If
    End If
    
    With grdSigners
        .Update
    End With
    
    If LastRow = 0 And LastCol = 2 Then
        If Me.Author = -1 Then
            If grdSigners.Columns(2).Text <> "" Then
                ChangeSigner
                grdSigners.Col = 4
            End If
        End If
    End If

End Sub

'Private Sub lstAttys_DblClick()
'    Dim i As Integer
'    Dim iSource As Integer
'    Dim xName As String
'    Dim lID As Long
'    Dim xBarID As String
'    Dim iBookmark As Integer
'
'    On Error GoTo ProcError
'    With grdSigners.Array
'        If .UpperBound(1) = -1 Then UserControl.grdSigners.Bookmark = Null
'        If grdSigners.Array.Value(grdSigners.Array.UpperBound(1), 2) <> "" Then
'            .Insert 1, .UpperBound(1) + 1
'        End If
'        iBookmark = lstAttys.RowBookmark(lstAttys.Row)
'
'        With Me.Attorneys
'            iSource = .Value(iBookmark, 0)
'            lID = .Value(iBookmark, 2)
''---poll the front end for a barID
'            RaiseEvent OnAuthorLoad(lID, xBarID, xName)
'        End With
'
'        .Value(.UpperBound(1), 0) = iSource
'        If grdSigners.Array.UpperBound(1) = 0 Then
'            .Value(.UpperBound(1), 1) = -1
'            RaiseEvent SignerChange
'        Else
'            .Value(.UpperBound(1), 1) = Empty
'        End If
'        .Value(.UpperBound(1), 2) = xName
'        .Value(.UpperBound(1), 3) = lID
'        .Value(.UpperBound(1), 4) = xBarID
'    End With
'    grdSigners.ReBind
'    grdSigners.Refresh
'    grdSigners.Bookmark = grdSigners.Array.UpperBound(1)
'    RaiseEvent SignersChange
'    Exit Sub
'
'ProcError:
'    Select Case Err.Number
'        Case 9
'            grdSigners.Array.ReDim 0, 0, 0, 4
'            Resume
'        Case Else
'            Err.Raise Err.Number
'            Resume
'    End Select
'End Sub

Private Sub lstAttys_DblClick()
    Dim i As Integer
    Dim iSource As Integer
    Dim xName As String
    Dim lID As Long
    Dim xBarID As String
    Dim iBookmark As Integer
    Dim xSignerEmail As String
    
    On Error GoTo ProcError
    
    With grdSigners.Array
    '---bail if # of sigs = max sigs
        If Me.MaxSigners = 1 Then
            If Me.Signers.Count(1) = 1 Then
                '---swap out existing #1 record
                iBookmark = lstAttys.RowBookmark(lstAttys.Row)
                GoTo LoadGrid
            End If
        ElseIf Me.Signers.Count(1) = Me.MaxSigners Then
            Exit Sub
        End If
        
        If .UpperBound(1) = -1 Then UserControl.grdSigners.Bookmark = Null
        If grdSigners.Array.Value(grdSigners.Array.UpperBound(1), 2) <> "" Then
            .Insert 1, .UpperBound(1) + 1
        End If
        iBookmark = lstAttys.RowBookmark(lstAttys.Row)
        
LoadGrid:
        With Me.Attorneys
            iSource = .Value(iBookmark, 0)
            lID = .Value(iBookmark, 2)
'---poll the front end for a barID
            RaiseEvent OnAuthorLoad(lID, xBarID, xName, xSignerEmail)
        End With
        
        .Value(.UpperBound(1), 0) = iSource
        If grdSigners.Array.UpperBound(1) = 0 Then
            .Value(.UpperBound(1), 1) = -1
            RaiseEvent SignerChange
        Else
            .Value(.UpperBound(1), 1) = Empty
        End If
        .Value(.UpperBound(1), 2) = xName
        .Value(.UpperBound(1), 3) = lID
        .Value(.UpperBound(1), 4) = xBarID
        .Value(.UpperBound(1), 5) = xSignerEmail
    End With
    grdSigners.ReBind
    grdSigners.Refresh
    grdSigners.Bookmark = grdSigners.Array.UpperBound(1)
    RaiseEvent SignersChange
    
    '---9.7.1
    MoveButton
    
    Exit Sub
    
ProcError:
    Select Case Err.Number
        Case 9
            grdSigners.Array.ReDim 0, 0, 0, 5
            Resume
        Case Else
            Err.Raise Err.Number
            Resume
    End Select
End Sub




Private Sub lstAttys_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
        lstAttys.Drag vbBeginDrag
End Sub

Private Sub lstAttys_KeyDown(KeyCode As Integer, Shift As Integer)
    RaiseEvent KeyDown(KeyCode, Shift)
    If KeyCode = 27 Or KeyCode = 9 Then    'tab or escape hides list
        If lstAttys.Visible = True Then
            chkShowAttys.Value = False
            If KeyCode = 27 Then KeyCode = 0
        End If
    ElseIf KeyCode = 13 Then
        lstAttys_DblClick
    End If
End Sub

Private Sub lstAttys_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    RaiseEvent MouseDown(Button, Shift, _
            ScaleX(X, UserControl.ScaleMode, vbContainerPosition), _
            ScaleY(Y, UserControl.ScaleMode, vbContainerPosition))

End Sub

Private Sub lstAttys_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim overRow As Long
    On Error Resume Next
    overRow = lstAttys.RowContaining(Y)
    If overRow >= 0 Then
        lstAttys.Row = overRow
    End If
End Sub

Private Sub mnuClearCell_Click()
    With grdSigners
        .Columns(.Col).Text = Empty
        .Update
    End With
End Sub

'Private Sub m_oFont_FontChanged(ByVal PropertyName As String)
'    Set UserControl.grdSigners.Font = m_ofont
'    Set UserControl.lstAttys.Font = m_ofont
'    Set UserControl.lstAttys.HighlightRowStyle.Font = m_ofont
'    Refresh
'End Sub

Private Sub mnuAuthor_Delete1_Click()
    On Error Resume Next
    Dim i As Integer
    Dim iPen As Integer
    
    With grdSigners
        .Delete
    End With
    '---capture pen if it hasn't been deleted
    For i = 0 To grdSigners.Array.UpperBound(1)
        If grdSigners.Array.Value(i, 1) = -1 Then
            iPen = i
            Exit For
        End If
    
    Next
           
    If grdSigners.Array.UpperBound(1) = -1 Then
        grdSigners.Array.ReDim 0, -1, 0, 5
        grdSigners.ReBind
        grdSigners.Refresh
        grdSigners.Array.ReDim 0, 0, 0, 5
        grdSigners.ReBind
        grdSigners.Refresh
    
    Else
        grdSigners.Array.Value(iPen, 1) = -1
        grdSigners.Refresh
    End If
    
    grdSigners.Bookmark = iPen
    RaiseEvent SignersChange
End Sub

Private Sub mnuAuthor_DeleteAll_Click()
    On Error Resume Next
    With grdSigners
        .Array.ReDim 0, -1, 0, 5
        .ReBind
        .Array.ReDim 0, 0, 0, 5
        .ReBind
        .Refresh
    End With
    
    grdSigners.Bookmark = Null
    RaiseEvent SignersChange

End Sub

Private Sub mnuAuthor_AddNew_Click()
    RaiseEvent MenuAddNewClick
End Sub

Private Sub mnuAuthor_Copy_Click()
    RaiseEvent MenuCopyClick
End Sub

Private Sub mnuAuthor_CopyOptions_Click()
    RaiseEvent MenuCopyOptionsClick
End Sub

Private Sub mnuAuthor_EditAuthorOptions_Click()
    RaiseEvent MenuEditAuthorOptionsClick
End Sub

Private Sub mnuAuthor_Favorite_Click()
    RaiseEvent MenuAuthorFavoriteClick
End Sub

Private Sub mnuAuthor_ManageLists_Click()
    RaiseEvent MenuManageListsClick
End Sub

Private Sub mnuAuthor_SetDefault_Click()
    RaiseEvent MenuSetDefaultClick
End Sub


Private Sub mnuAuthor_Up1_Click()
    MoveCurrentRecord -1
    If grdSigners.RowBookmark(grdSigners.Row) > 0 Then _
        grdSigners.Row = grdSigners.RowBookmark(grdSigners.Row) - 1
End Sub

Private Sub mnuAuthor_Down1_Click()
    MoveCurrentRecord 2
    On Error Resume Next
    grdSigners.Row = grdSigners.RowBookmark(grdSigners.Row) + 1
End Sub

Private Sub UserControl_ExitFocus()
    Dim i As Integer
    UserControl.grdSigners.Update
    UserControl.chkShowAttys.Value = 0
    For i = UserControl.lstAttys.SelBookmarks.Count - 1 To 0 Step -1
        UserControl.lstAttys.SelBookmarks.Remove (i)
    Next i
    UserControl.grdSigners.ReBind
    End Sub

Private Sub UserControl_Initialize()
    Set m_xarSigners = New XArray
    m_xarSigners.ReDim 0, 0, 0, 5
    Set m_xarAttys = New XArray
    m_xarAttys.ReDim 0, 0, 0, 2
    Set m_ofont = New StdFont
    lstAttys.Array = m_xarAttys
    lstAttys.ReBind
    grdSigners.Array = m_xarSigners
    grdSigners.ReBind
    grdSigners.Refresh
    grdSigners.Bookmark = 0
End Sub

Private Function GridMoveNext(grdCur As TDBGrid, Optional bNextRecord As Boolean = False)
        
    ' Move to next column or row
    ' Redim array and add blank row if necessary
    With grdCur
        If .Bookmark < grdCur.Array.UpperBound(1) Then
            '---
            .MoveNext
        ElseIf Not GridCurrentRowIsEmpty(grdCur) Then
            .Update
            '.Array.ReDim 0, .Bookmark + 1, 0, .Columns.Count - 1
            '---9.6.2 - TEMP!! - we will be adding a column to the grid for 9.7.1
            .Array.ReDim 0, .Bookmark + 1, 0, 5
            .ReBind
            .Refresh
            .MoveLast
            .Col = 2
            .EditActive = True
        End If
    End With
End Function

Private Sub MoveButton()
    Dim iAdjust As Integer
    Dim sFCol As Single
    
    If Me.RecordSelectors = True Then
        iAdjust = chkShowAttys.Width + 40
    End If
    
    If (Me.DisplayBarID = False And Me.DisplayEMail = False) Or grdSigners.LeftCol >= 3 Then
        UserControl.chkShowAttys.Left = UserControl.Width - (UserControl.chkShowAttys.Width * 2) ' - iAdjust
    Else
        If UserControl.grdSigners.LeftCol = 1 Then
            sFCol = grdSigners.Columns(1).Width
        Else
            sFCol = 0
        End If
        chkShowAttys.Left = (grdSigners.Columns(2).Width + sFCol) - chkShowAttys.Width + iAdjust
    End If

End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)
    RaiseEvent KeyDown(KeyCode, Shift)
End Sub

Private Sub UserControl_Paint()
    If Me.ListRows = 0 Then Me.ListRows = 5
End Sub

Private Sub UserControl_Resize()
    
    If b_LoadCtlFlag = True Then Exit Sub
    Dim lCol1r, lCol2r, lCol3r As Long
    Dim lOldW, lOldCW As Long
    Static bChanged As Boolean
    Dim iAdjust As Long
    Dim sRowH As Single
    Dim iRows As Single
    
    lOldW = grdSigners.Width
    lOldCW = grdSigners.Columns(2).Width
    
    grdSigners.Width = UserControl.Width
    grdSigners.Columns(2).Width = grdSigners.Columns(2).Width - (lOldW - grdSigners.Width)
    
    MoveButton
    
    lstAttys.Width = UserControl.Width
    lstAttys.Columns(1).Width = grdSigners.Columns(1).Width + grdSigners.Columns(2).Width
    
    sRowH = grdSigners.RowHeight
    iRows = (UserControl.Height / sRowH)
    grdSigners.Height = UserControl.Height - 25
   
    lstAttys.Top = grdSigners.Height + 15
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error Resume Next
    BackColor = PropBag.ReadProperty("BackColor", vbWhite)
    Enabled = PropBag.ReadProperty("Enabled", True)
    RecordSelectors = PropBag.ReadProperty("RecordSelectors", True)
    ListVisible = PropBag.ReadProperty("ListVisible", False)
    RowHeight = PropBag.ReadProperty("RowHeight", 250)
    DisplayBarID = PropBag.ReadProperty("DisplayBarID", True)
    DisplayEMail = PropBag.ReadProperty("DisplayEMail", False)
    IncludeBarID = PropBag.ReadProperty("IncludeBarID", True)
    Separator = PropBag.ReadProperty("Separator", ";")
    BarIDPrefix = PropBag.ReadProperty("BarIDPrefix", ", Bar No. ")
    SignerEmailPrefix = PropBag.ReadProperty("SignerEmailPrefix", ", ")
    SignerEmailSuffix = PropBag.ReadProperty("SignerEmailSuffix", "")
    SignerNameCase = PropBag.ReadProperty("SignerNameCase", SignerNameCase_None)
    BarIDSuffix = PropBag.ReadProperty("BarIDSuffix", "")
    MaxSigners = PropBag.ReadProperty("MaxSigners", 4)
    SeparatorSpecial = PropBag.ReadProperty("SeparatorSpecial", s_UserDefined)
    ListRows = PropBag.ReadProperty("ListRows", 5)
    IncludeSignerEmail = PropBag.ReadProperty("IncludeSignerEmail", False)
'---handle font object
    With m_ofont
        .Name = PropBag.ReadProperty("FontName", "MS San Serif")
        .Size = PropBag.ReadProperty("FontSize", 8)
        .Bold = PropBag.ReadProperty("FontBold")
        .Underline = PropBag.ReadProperty("FontUnderline")
        .Italic = PropBag.ReadProperty("FontItalic")
    End With
    Set Me.Font = m_ofont
    
    'Set UserControl.Font = m_ofont
End Sub

Private Sub UserControl_Terminate()
    Set m_xarSigners = Nothing
    Set m_xarAttys = Nothing
    Set m_xarBarIDs = Nothing
    Set m_xarTemp = Nothing
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    PropBag.WriteProperty "BackColor", Me.BackColor, vbWhite
    PropBag.WriteProperty "Enabled", Me.Enabled, True
    PropBag.WriteProperty "IncludeBarID", Me.IncludeBarID, True
    PropBag.WriteProperty "DisplayBarID", Me.DisplayBarID, True
    PropBag.WriteProperty "DisplayEMail", Me.DisplayEMail, False
    PropBag.WriteProperty "RecordSelectors", Me.RecordSelectors, True
    PropBag.WriteProperty "ListRows", Me.ListRows, 5
    PropBag.WriteProperty "RowHeight", Me.RowHeight, 250
    PropBag.WriteProperty "ListVisible", Me.ListVisible, False
    PropBag.WriteProperty "Separator", Me.Separator, ";"
    PropBag.WriteProperty "BarIDSuffix", Me.BarIDSuffix, ", Bar No. "
    PropBag.WriteProperty "BarIDPrefix", Me.BarIDPrefix, ""
    PropBag.WriteProperty "SignerEMailPrefix", Me.SignerEmailPrefix, ""
    PropBag.WriteProperty "SignerEMailSuffix", Me.SignerEmailSuffix, ""
    PropBag.WriteProperty "SignerNameCase", Me.SignerNameCase, SignerNameCase_None
    PropBag.WriteProperty "MaxSigners", Me.MaxSigners, "4"
    PropBag.WriteProperty "SeparatorSpecial", Me.SeparatorSpecial, s_UserDefined
    PropBag.WriteProperty "IncludeSignerEmail", Me.IncludeSignerEmail, False
    With m_ofont
        PropBag.WriteProperty "FontName", .Name, "MS Sans Serif"
        PropBag.WriteProperty "FontBold", .Bold
        PropBag.WriteProperty "FontSize", .Size, 8
        PropBag.WriteProperty "FontItalic", .Italic
        PropBag.WriteProperty "FontUnderline", .Underline
    End With
End Sub

'**********************************************************************************************
'---methods
'**********************************************************************************************

Public Sub Clear()
    Dim xAR As New XArray
    xAR.ReDim 0, 0, 0, 5
    With grdSigners
        .Array = xAR
        .ReBind
    End With

End Sub
Public Sub ListRebind()
    With lstAttys
        
        .ReBind
    End With

End Sub

Public Sub ChangeSigner()
    Dim i As Integer
    Dim iSel As Integer
    Dim iPrev As Integer
    Dim bCancel As Boolean
    On Error Resume Next
    If grdSigners.Columns(3) = 0 Then
        RaiseEvent SelectedSigner(grdSigners.Array.Value(grdSigners.Row, AttyArrayCol_ID), bCancel)
        If bCancel Then Exit Sub
    End If
    iSel = grdSigners.Bookmark
    iPrev = Val(xNullToString(grdSigners.Array.Value(iSel, 1)))
    For i = 0 To grdSigners.Array.Count(1) - 1
            grdSigners.Array.Value(i, 1) = Empty
    Next i
    If grdSigners.Array.Count = 1 Then
        grdSigners.Array.Value(iSel, 1) = -1
    Else
        grdSigners.Array.Value(iSel, 1) = Not iPrev '-1
    End If
    grdSigners.ReBind
    grdSigners.Refresh
    grdSigners.Bookmark = Null
    grdSigners.Bookmark = iSel
    grdSigners.Col = 2
    RaiseEvent SignerChange
End Sub

Private Function GridCurrentRowIsEmpty(grdTest As TDBGrid) As Boolean
    Dim i As Integer
    GridCurrentRowIsEmpty = True
    With grdTest
        For i = 0 To .Columns.Count - 1
            If .Columns(i).Text <> "" Then
                GridCurrentRowIsEmpty = False
                Exit Function
            End If
        Next i
    End With
End Function


Private Function MoveCurrentRecord(iDirection As Integer)
    Dim i As Integer
    Dim j As Integer
    Dim iRow As Integer
    Dim xARTemp As XArray
    Dim iDestRow As Integer
    
    ReDim xDragFrom(0, grdSigners.Array.UpperBound(2))
    
'---capture current record
    iRow = grdSigners.RowBookmark(grdSigners.Row)
  
    If iDirection > 0 And grdSigners.Array.Value(grdSigners.Array.UpperBound(1), AttyArrayCol_Name) = "" Then
        Exit Function
    End If
    
    If iDirection > 0 Then
        If grdSigners.Array.UpperBound(1) = iRow Then _
            Exit Function
    End If
    
    For i = 0 To grdSigners.Array.UpperBound(2)
        xDragFrom(0, i) = grdSigners.Array.Value(iRow, i)
    Next i
    
    is_SourceRow = iRow

'---set xarray
    Set xARTemp = New XArray
    With grdSigners.Array
        xARTemp.ReDim .LowerBound(1), .UpperBound(1), .LowerBound(2), .UpperBound(2)
        For i = .LowerBound(1) To .UpperBound(1)
            For j = .LowerBound(2) To .UpperBound(2)
                xARTemp.Value(i, j) = .Value(i, j)
            Next
        Next
    End With

'---Get drop zone
    On Error Resume Next
    iDestRow = iRow + iDirection
    If iDestRow < 0 Then iDestRow = 0

'---empty out source Record
    For i = 0 To xARTemp.UpperBound(2)
        xARTemp(is_SourceRow, i) = Empty
        Debug.Print xARTemp(0, i)
    Next i

'---create row space in array if necessary
    If iDestRow >= xARTemp.UpperBound(1) Then
        xARTemp.Insert 1, xARTemp.UpperBound(1) + iDestRow
    Else
        xARTemp.Insert 1, iDestRow
    End If

'---move  only within dropped columns
    ReDim temp(0, xARTemp.UpperBound(2)) As String
    '---9.7.1 - 4241 - simplify this operation
    For i = iDestRow To iDestRow
        For j = 0 To xARTemp.UpperBound(2)
''''            Debug.Print xDragFrom(0, j)
''''            temp(0, j) = xARTemp.Value(i, j)
            xARTemp(i, j) = xDragFrom(0, j)
''''            xDragFrom(0, j) = temp(0, j)
        Next j
    Next i

''''    If temp(0, 0) <> Empty Then
''''        For i = iDestRow + 1 To xARTemp.UpperBound(1)
''''            For j = 0 To xARTemp.UpperBound(2)
''''                temp(0, j) = xARTemp.Value(i, j)
''''                xARTemp(i, j) = xDragFrom(0, j)
''''                xDragFrom(0, j) = temp(0, j)
''''            Next j
''''        Next i
''''    End If

'---remove empty rows
    For i = 0 To xARTemp.UpperBound(1)
        If xARTemp(i, 2) = Empty Then
            xARTemp.Delete 1, i
        End If
    Next
    
    grdSigners.Array = xARTemp
    grdSigners.Bookmark = Null
    grdSigners.ReBind
    grdSigners.Refresh
    grdSigners.Row = iRow
    
    
    RaiseEvent SignersChange
    Exit Function
    
ProcError:
    Select Case Err.Number
        Case 9
            xARTemp.ReDim 0, -1, 0, 5
            Resume
        Case Else
            Err.Raise Err.Number
    End Select
End Function
