VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.UserControl MultiLineCombo 
   BackStyle       =   0  'Transparent
   ClientHeight    =   3300
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3828
   EditAtDesignTime=   -1  'True
   ScaleHeight     =   3300
   ScaleWidth      =   3828
   ToolboxBitmap   =   "MultiLineCombo.ctx":0000
   Begin VB.CheckBox chkShowList 
      DownPicture     =   "MultiLineCombo.ctx":0312
      Height          =   702
      Left            =   3570
      Picture         =   "MultiLineCombo.ctx":055C
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   0
      Width           =   225
   End
   Begin VB.TextBox txtList 
      Appearance      =   0  'Flat
      Height          =   705
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   0
      Width           =   3570
   End
   Begin TrueDBList60.TDBList lstList 
      Height          =   2556
      Left            =   -12
      OleObjectBlob   =   "MultiLineCombo.ctx":07A6
      TabIndex        =   2
      Top             =   708
      Visible         =   0   'False
      Width           =   3804
   End
   Begin VB.Menu mlcMenu 
      Caption         =   "mlcMenu"
      Visible         =   0   'False
      Begin VB.Menu mlcMenu_Clear 
         Caption         =   "&Clear"
      End
   End
End
Attribute VB_Name = "MultiLineCombo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_xToolTip As String
Private m_xarList As XArrayObject.XArray
Private m_xText As String
Private m_iListRows As Integer
Private m_ItemToBeAdded As String
Private b_LoadCtlFlag As Boolean
Private b_ForceCaps As Boolean
Private m_Sep As MLC_SEPARATORS
Private m_Separator As String
'Charliecore 9.2.0
'Private WithEvents m_oFont As StdFont
Private m_ofont As StdFont

Public Enum MLC_SEPARATORS
    Comma = 1
    Semicolon = 2
    Space = 3
    VB_CRLineFeed = 4
    VB_LineFeed = 5
    VB_Tab = 6
    UserDefined = 7
End Enum

Public Event BeforeItemAdded(ByRef xItem As String)
Public Event Change()

Public Property Let SelLength(ByVal lNew As Long)
    UserControl.txtList.SelLength = lNew
End Property

Public Property Get SelLength() As Long
    SelLength = UserControl.txtList.SelLength
End Property

Public Property Let SelStart(ByVal lNew As Long)
    UserControl.txtList.SelStart = lNew
End Property

Public Property Get SelStart() As Long
    SelStart = UserControl.txtList.SelStart
End Property

Public Property Let SelText(ByVal xNew As String)
    UserControl.txtList.SelText = xNew
End Property

Public Property Get SelText() As String
    SelText = UserControl.txtList.SelText
End Property

Public Property Let Visible(ByVal NewValue As Boolean)
    UserControl.Extender.Visible = NewValue
    PropertyChanged "Visible"
End Property

Public Property Get Visible() As Boolean
    Visible = UserControl.Extender.Visible
End Property

Public Property Let BackColor(ByVal NewValue As OLE_COLOR)
    UserControl.txtList.BackColor = NewValue
    UserControl.lstList.BackColor = NewValue
    PropertyChanged "BackColor"
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.txtList.BackColor
End Property

Public Property Set Font(ByVal NewFont As StdFont)
'Charliecore 9.2.0
    Set UserControl.lstList.Font = NewFont
    Set UserControl.txtList.Font = NewFont
    Set UserControl.lstList.HighlightRowStyle.Font = NewFont
    Refresh
    With m_ofont
        .Bold = NewFont.Bold
        .Name = NewFont.Name
        .Italic = NewFont.Italic
        .Size = NewFont.Size
        .Underline = NewFont.Underline
    End With
    PropertyChanged "Font"
End Property

Public Property Get Font() As StdFont
    Set Font = m_ofont
End Property
Public Property Get ColumnValue(iCol As Integer) As String
    Dim xValue As String
    On Error Resume Next
    With UserControl.lstList
        xValue = .Array.Value(.SelectedItem, iCol)
    End With
    ColumnValue = xValue
End Property
Friend Property Get ItemToBeAdded() As String
    ItemToBeAdded = m_ItemToBeAdded
End Property

Friend Property Let ItemToBeAdded(xNew As String)
    m_ItemToBeAdded = xNew
End Property

Public Property Get Enabled() As Boolean
Attribute Enabled.VB_UserMemId = -514
    Enabled = UserControl.Enabled
End Property

Public Property Let Enabled(ByVal NewValue As Boolean)
    If NewValue = False Then
        UserControl.lstList.Visible = NewValue
        UserControl.chkShowList.Value = False
    End If
    UserControl.Enabled = NewValue
    UserControl.txtList.Enabled = NewValue
    UserControl.chkShowList.Enabled = NewValue
    PropertyChanged "Enabled"
End Property

Public Property Get ListVisible() As Boolean
    ListVisible = lstList.Visible
End Property

Public Property Let ListVisible(bNew As Boolean)
    chkShowList.Value = BoolToInt(bNew)
    If lstList.Visible <> bNew Then
        lstList.Visible = bNew
    End If
End Property
Public Property Get ForceCaps() As Boolean
    ForceCaps = b_ForceCaps
End Property
Public Property Let ForceCaps(ByVal bNew As Boolean)
    b_ForceCaps = bNew
    PropertyChanged "ForceCaps"
End Property

Public Property Get List() As XArrayObject.XArray
    Set List = m_xarList
End Property

Public Property Let List(xarNew As XArrayObject.XArray)
    b_LoadCtlFlag = True
    Set m_xarList = xarNew
    lstList.Array = xarNew
    lstList.ReBind
    ResizeTDBList lstList, Me.ListRows
End Property

Public Property Get Text() As String
Attribute Text.VB_UserMemId = 0
    Text = txtList.Text
End Property

Public Property Let Text(xNew As String)
    txtList.Text = xNew
'    RaiseEvent Change
End Property
Public Property Get ListRows() As Integer
    ListRows = m_iListRows
End Property

Public Property Let ListRows(iNew As Integer)
    Dim iMax As Integer
    '---change max row value based on how much form space exists
    With UserControl
        On Error Resume Next
        iMax = Fix((.Parent.Height - (.Extender.Top + .Height)) / .lstList.RowHeight) - 2
        If iMax < 0 Then iMax = 0
        If iNew = 0 Then iNew = 5
        iNew = Min(CDbl(iMax), CDbl(iNew))
    End With
    m_iListRows = iNew
    PropertyChanged "ListRows"
End Property

Public Property Get SeparatorSpecial() As MLC_SEPARATORS
    SeparatorSpecial = m_Sep
End Property

Public Property Let SeparatorSpecial(b_NewSep As MLC_SEPARATORS)
    m_Sep = b_NewSep
    Select Case b_NewSep
        Case Comma
            m_Separator = ", "
        Case Semicolon
            m_Separator = "; "
        Case Space
            m_Separator = " "
        Case VB_CRLineFeed
            m_Separator = vbCrLf
        Case VB_Tab
            m_Separator = vbTab
        Case VB_LineFeed
            m_Separator = vbLf
        Case UserDefined
            If m_Separator = "" Then m_Separator = ", "
        Case Else
        End Select
    m_Sep = b_NewSep
    PropertyChanged "SeparatorSpecial"
End Property

Public Property Get Separator() As String
    Separator = m_Separator
End Property

Public Property Let Separator(xNew As String)
    m_Separator = xNew
    PropertyChanged "Separator"
End Property

Private Sub chkShowList_Click()
    Me.ListVisible = CBool(chkShowList.Value)
End Sub

Private Sub lstList_DblClick()
    Dim xItem As String
        With txtList
            If .Text <> "" Then
                .Text = .Text & vbCrLf
            End If
            If lstList.Row > -1 Then
                xItem = m_xarList(lstList.RowBookmark(lstList.Row))
                RaiseEvent BeforeItemAdded(xItem)
                .Text = .Text & xItem
            End If
            txtList.SelStart = Len(txtList)
        End With
        chkShowList.Value = False
End Sub

Private Sub lstList_Click()
    Dim xItem As String
        With txtList
            If .Text <> "" Then
                .Text = .Text & Me.Separator
            End If
            If lstList.Row > -1 Then
                xItem = m_xarList(lstList.RowBookmark(lstList.Row))
                RaiseEvent BeforeItemAdded(xItem)
                .Text = .Text & xItem
            End If
            txtList.SelStart = Len(txtList)
        End With
        chkShowList.Value = False
End Sub

Private Sub lstList_DragCell(ByVal SplitIndex As Integer, RowBookmark As Variant, ByVal ColIndex As Integer)
    lstList.Drag vbBeginDrag
End Sub

Private Sub lstList_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 27 Or KeyCode = 9 Then    'escape hides list
        If lstList.Visible = True Then
            chkShowList.Value = False
        End If
     If KeyCode = 9 Then
        SendKeys "{tab}"
    End If
   
    ElseIf KeyCode = 13 Then
        lstList_DblClick
    End If

End Sub

Private Sub lstList_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = 2 Then
        If Shift = 4 Then
            txtList.Text = Empty
        Else
            UserControl.PopupMenu mlcMenu
        End If
    End If
End Sub

Private Sub lstList_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim overRow As Long
    On Error Resume Next
    overRow = lstList.RowContaining(Y)
    If overRow >= 0 Then
        lstList.Row = overRow
    End If
End Sub

Private Sub mlcMenu_Clear_Click()
    On Error Resume Next
    txtList.Text = Empty
    txtList.SetFocus
End Sub

Private Sub txtList_Change()
    RaiseEvent Change
End Sub

Private Sub txtList_Click()
    If lstList.Visible = True Then
        chkShowList.Value = False
    End If
End Sub

Private Sub txtList_DblClick()
    chkShowList.Value = Abs(CInt(CBool(CInt(lstList.Visible) = vbUnchecked)))
End Sub

Private Sub txtList_DragDrop(Source As Control, X As Single, Y As Single)
    Dim i As Integer
    Dim xItem As String
'    For i = 0 To lstList.SelBookmarks.Count - 1
        With txtList
            If .Text <> "" Then
                .Text = .Text & vbCrLf
            End If
            xItem = m_xarList(lstList.RowBookmark(lstList.Row))
            RaiseEvent BeforeItemAdded(xItem)
            .Text = .Text & xItem
        End With
'    Next i
    Exit Sub
    
ProcError:
    Select Case Err.Number
        Case Else
            Err.Raise Err.Number
    End Select
End Sub

Private Sub txtList_KeyDown(KeyCode As Integer, Shift As Integer)
        
   Select Case Shift
        Case 4
            If KeyCode = 40 Then
                chkShowList.Value = vbChecked
            End If
        Case 27, 9
            If lstList.Visible = True Then
                chkShowList.Value = False
            End If
    End Select

End Sub

Private Sub txtList_LostFocus()
    If Me.ForceCaps Then txtList.Text = UCase(txtList.Text)
End Sub

Private Sub txtList_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim m_Ctl As VB.Control

    If Button = 2 Then
'---fool windows into disabling default popup (necessary for textboxes)
        LockWindowUpdate txtList.hWnd
        UserControl.chkShowList.SetFocus
        Set m_Ctl = txtList
        txtList.Enabled = False
        DoEvents
        UserControl.PopupMenu mlcMenu
        txtList.Enabled = True
        LockWindowUpdate 0&
        txtList.SetFocus
        Set m_Ctl = Nothing
    End If

End Sub


Private Sub UserControl_ExitFocus()
    Dim i As Integer
    UserControl.chkShowList.Value = 0
    For i = UserControl.lstList.SelBookmarks.Count - 1 To 0 Step -1
        UserControl.lstList.SelBookmarks.Remove (i)
    Next i
End Sub

Private Sub UserControl_Initialize()
   Set m_ofont = New StdFont
End Sub

Private Sub UserControl_LostFocus()
    Set m_xarList = Nothing
End Sub

Private Sub UserControl_Paint()
    If Me.ListRows = 0 Then Me.ListRows = 5
End Sub

Private Sub UserControl_Resize()
    On Error Resume Next
    If b_LoadCtlFlag = True Then Exit Sub
    Dim lOldW, lOldTH As Long
    Dim iNumRows As Integer
    
    lOldW = lstList.Width
    lOldTH = txtList.Height
    
    txtList.Width = UserControl.Width - chkShowList.Width
    lstList.Width = UserControl.Width

    txtList.Height = UserControl.Height
    lstList.Top = txtList.Height
'---move button
    chkShowList.Left = chkShowList.Left - (lOldW - lstList.Width)
    chkShowList.Height = txtList.Height - 5
    
End Sub
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error Resume Next
    BackColor = PropBag.ReadProperty("BackColor", vbWhite)
    Enabled = PropBag.ReadProperty("Enabled", True)
    ListRows = PropBag.ReadProperty("ListRows", 5)
    ListVisible = PropBag.ReadProperty("ListVisible", False)
    Separator = PropBag.ReadProperty("Separator", vbCrLf)
    SeparatorSpecial = PropBag.ReadProperty("SeparatorSpecial", VB_CRLineFeed)
    ForceCaps = PropBag.ReadProperty("ForceCaps", False)
    Text = PropBag.ReadProperty("Text", "")
'---handle font object
    With m_ofont
        .Name = PropBag.ReadProperty("FontName", "Arial")
        .Size = PropBag.ReadProperty("FontSize", 8)
        .Bold = PropBag.ReadProperty("FontBold")
        .Underline = PropBag.ReadProperty("FontUnderline")
        .Italic = PropBag.ReadProperty("FontItalic")
    End With
    Set Me.Font = m_ofont
End Sub

Private Sub UserControl_Terminate()
    Set m_ofont = Nothing
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    On Error Resume Next
    PropBag.WriteProperty "BackColor", Me.BackColor, vbWhite
    PropBag.WriteProperty "Enabled", Me.Enabled, True
    PropBag.WriteProperty "ListRows", Me.ListRows, 5
    PropBag.WriteProperty "ListVisible", Me.ListVisible, False
    PropBag.WriteProperty "ForceCaps", Me.ForceCaps, False
    PropBag.WriteProperty "Text", Me.Text, ""
    PropBag.WriteProperty "Separator", Me.Separator, vbCrLf
    PropBag.WriteProperty "SeparatorSpecial", Me.SeparatorSpecial, VB_CRLineFeed
    With m_ofont
        PropBag.WriteProperty "FontName", .Name, "Arial"
        PropBag.WriteProperty "FontBold", .Bold
        PropBag.WriteProperty "FontSize", .Size, 8
        PropBag.WriteProperty "FontItalic", .Italic
        PropBag.WriteProperty "FontUnderline", .Underline
    End With
End Sub

Private Sub ResizeTDBList(tdbCBX As TrueDBList60.TDBList, Optional iMaxRows As Integer)
    Dim ctlArray As XArrayObject.XArray
    Dim iRows, iRowC, iRowMax  As Integer
    On Error Resume Next
    With tdbCBX
        Set ctlArray = .Array
        iRowC = ctlArray.Count(1)
        iRows = Min(CDbl(iRowC), CDbl(iMaxRows))
        .Height = ((iRows) * .RowHeight)
    End With
    UserControl.Height = tdbCBX.Height + txtList.Height
End Sub

