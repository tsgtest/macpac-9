VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.UserControl SpinnerTextBox 
   ClientHeight    =   900
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2850
   LockControls    =   -1  'True
   ScaleHeight     =   900
   ScaleWidth      =   2850
   ToolboxBitmap   =   "SpinnerTextBox.ctx":0000
   Begin MSComCtl2.UpDown spnText 
      Height          =   315
      Left            =   720
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   0
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   556
      _Version        =   393216
      OrigLeft        =   1680
      OrigTop         =   405
      OrigRight       =   1920
      OrigBottom      =   720
      Enabled         =   -1  'True
   End
   Begin VB.TextBox txtText 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   720
   End
End
Attribute VB_Name = "SpinnerTextBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Private m_IncrementValue As Double
Private m_MinValue As Double
Private m_MaxValue As Double
Private m_bSupplyQuotes As Boolean
Private m_DefaultValue As String
Private m_Value As String

Private Const m_MinHeight As Integer = 315

Public Event KeyDown(KeyCode As Integer, Shift As Integer)
Public Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Public Event Change()
Public Event SpinUp()
Public Event SpinDown()
Public Event Click()

'*************************************************************************
'   Properties
'*************************************************************************

Public Property Let Visible(ByVal NewValue As Boolean)
    UserControl.Extender.Visible = NewValue
    PropertyChanged "Visible"
End Property

Public Property Get Visible() As Boolean
    Visible = UserControl.Extender.Visible
End Property

Public Property Let BackColor(ByVal NewValue As OLE_COLOR)
    UserControl.txtText.BackColor = NewValue
    PropertyChanged "BackColor"
End Property

Public Property Get BackColor() As OLE_COLOR
    BackColor = UserControl.txtText.BackColor
End Property

Public Property Get Enabled() As Boolean
    Enabled = UserControl.Enabled
End Property

Public Property Let Enabled(ByVal NewValue As Boolean)
    UserControl.Enabled = NewValue
    UserControl.txtText.Enabled = NewValue
    PropertyChanged "Enabled"
End Property

Public Property Let IncrementValue(ByVal dNew As Double)
    m_IncrementValue = dNew
    PropertyChanged "IncrementValue"
End Property

Public Property Get IncrementValue() As Double
    IncrementValue = m_IncrementValue
End Property

Public Property Let MinValue(ByVal dNew As Double)
    m_MinValue = dNew
    PropertyChanged "MinValue"
End Property

Public Property Get MinValue() As Double
    MinValue = m_MinValue
End Property
Public Property Let MaxValue(ByVal dNew As Double)
    m_MaxValue = dNew
    PropertyChanged "MaxValue"
End Property

Public Property Get MaxValue() As Double
    MaxValue = m_MaxValue
End Property

Public Property Let SupplyQuotes(ByVal bNew As Boolean)
    m_bSupplyQuotes = bNew
    If bNew Then
        AddQuotes
    Else
        UserControl.txtText.Text = xSubstitute(UserControl.txtText.Text, Chr(34), "")
    End If
    PropertyChanged "SupplyQuotes"
End Property

Public Property Get SupplyQuotes() As Boolean
    SupplyQuotes = m_bSupplyQuotes
End Property

Public Property Let DefaultValue(ByVal xNew As String)
    m_DefaultValue = xNew
    UserControl.txtText.Text = m_DefaultValue
    PropertyChanged "DefaultValue"
End Property

Public Property Get DefaultValue() As String
    DefaultValue = xSubstitute(m_DefaultValue, Chr(34), "")
End Property

Public Property Let Value(ByVal xNew As String)
    If Me.Value <> xSubstitute(xNew, Chr(34), "") Then RaiseEvent Change
    m_Value = xNew
    UserControl.txtText.Text = m_Value
    If Me.SupplyQuotes Then AddQuotes
    PropertyChanged "Value"
End Property

Public Property Get Value() As String
    Value = xSubstitute(m_Value, Chr(34), "")
End Property

Public Property Let SelLength(ByVal lNew As Long)
    UserControl.txtText.SelLength = lNew
End Property

Public Property Get SelLength() As Long
    SelLength = UserControl.txtText.SelLength
End Property

Public Property Let SelStart(ByVal lNew As Long)
    UserControl.txtText.SelStart = lNew
End Property

Public Property Get SelStart() As Long
    SelStart = UserControl.txtText.SelStart
End Property

Public Property Let SelText(ByVal xNew As String)
    UserControl.txtText.SelText = xNew
End Property

Public Property Get SelText() As String
    SelText = UserControl.txtText.SelText
End Property


'******************************************************************
'---internal functions
'******************************************************************

Private Sub spnText_DownClick()
    If Val(txtText.Text) > Me.MinValue Then
        txtText.Text = Max(Val(txtText.Text) - Me.IncrementValue, Me.MinValue)
        If Me.SupplyQuotes Then AddQuotes
        Me.Value = txtText.Text
    End If
    RaiseEvent SpinDown
    RaiseEvent Click
End Sub

Private Sub spnText_UpClick()
    If Val(txtText.Text) < Me.MaxValue Then
        txtText.Text = Min(Val(txtText.Text) + Me.IncrementValue, Me.MaxValue)
        If Me.SupplyQuotes Then AddQuotes
        Me.Value = txtText.Text
    End If
    RaiseEvent SpinUp
    RaiseEvent Click
End Sub

Private Sub txtText_GotFocus()
    txtText.SelStart = 0
    txtText.SelLength = Len(txtText.Text)
End Sub

Private Sub txtText_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 40 Then
        spnText_DownClick
    ElseIf KeyCode = 38 Then
        spnText_UpClick
    End If

End Sub

Private Sub AddQuotes()
    Dim xTemp As String
    If txtText = "" Then Exit Sub
    xTemp = xSubstitute(txtText.Text, Chr(34), "")
    xTemp = xTemp & Chr(34)
    txtText.Text = xTemp
End Sub


Private Sub txtText_LostFocus()
    If Val(xSubstitute(txtText.Text, Chr(34), "")) > Me.MaxValue Then Me.Value = Me.MaxValue
    If Val(xSubstitute(txtText.Text, Chr(34), "")) < Me.MinValue Then Me.Value = Me.MinValue
    Me.Value = txtText.Text

    If Me.SupplyQuotes Then AddQuotes
End Sub


'****************************************************************
'---methods
'****************************************************************
Private Sub UserControl_Resize()

    With UserControl
        .txtText.Width = UserControl.Width - .spnText.Width - 15
        .txtText.Height = UserControl.Height
        .spnText.Height = UserControl.Height
        .spnText.Left = .txtText.Width + 15
    End With
    If Me.SupplyQuotes Then AddQuotes
   
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    BackColor = PropBag.ReadProperty("BackColor", vbWhite)
    Enabled = PropBag.ReadProperty("Enabled", True)
    Visible = PropBag.ReadProperty("Visible", True)
    IncrementValue = PropBag.ReadProperty("IncrementValue", 1)
    MaxValue = PropBag.ReadProperty("MaxValue", 10)
    MinValue = PropBag.ReadProperty("MinValue", 0)
    SupplyQuotes = PropBag.ReadProperty("SupplyQuotes", False)
    DefaultValue = PropBag.ReadProperty("DefaultValue", "")
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    PropBag.WriteProperty "BackColor", Me.BackColor, vbWhite
    PropBag.WriteProperty "Enabled", Me.Enabled, True
    PropBag.WriteProperty "Visible", Me.Visible, True
    PropBag.WriteProperty "IncrementValue", Me.IncrementValue, 1
    PropBag.WriteProperty "MinValue", Me.MinValue, 0
    PropBag.WriteProperty "MaxValue", Me.MaxValue, 10
    PropBag.WriteProperty "SupplyQuotes", Me.SupplyQuotes, False
    PropBag.WriteProperty "DefaultValue", Me.DefaultValue, ""
End Sub

