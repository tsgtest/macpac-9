Attribute VB_Name = "mpControlFunctions"
Public Function DisableInactiveTabs(frmP As VB.Form)
    Dim ctlP As VB.Control
    On Error Resume Next
    With frmP
        For Each ctlP In .Controls
            If (ctlP.Container Is .MultiPage1) Then
               ctlP.Enabled = (ctlP.Left >= 0)
            End If
        Next
    End With
End Function
Function bCBXList(lstP As MSForms.ComboBox, _
                  xArray() As String) As Boolean
'***********goes in mpDialog'***************
'assigns array xArray() to combobox lstP
    Dim i As Integer
    Dim j As Integer
    Dim bIsOneDimensional As Boolean
    Dim iArrayUBound As Integer
    
'   test for number of dimensions in array
    On Error Resume Next
    iArrayUBound = UBound(xArray, 2)
    bIsOneDimensional = (Err.Number > 0)
    Err.Number = 0
    
'   clear existing if necessary
    If lstP.ListCount Then _
        lstP.Clear
        
    If bIsOneDimensional Then
'       add items from one dimensional array
'        If Not (UBound(xArray) = 0 And xArray(0) = "") Then
            For i = LBound(xArray) To UBound(xArray)
                lstP.AddItem xArray(i)
            Next i
'        End If
    Else
'       add items from two dimensional array
        If Not (UBound(xArray) = 0 And xArray(0, 0) = "") Then
            For i = LBound(xArray, 1) To UBound(xArray, 1)
                With lstP
                    .AddItem xArray(i, 0)
                    For j = 1 To iArrayUBound
                        .List(i, j) = xArray(i, j)
                    Next j
                End With
            Next i
        End If
    End If
End Function
Public Function bReleaseVShift() As Boolean
    Dim altscan As Integer
    altscan% = MapVirtualKey(VK_SHIFT, 0)
    If g_VShiftDown = True Then
        keybd_event VK_SHIFT, altscan, KEYEVENTF_KEYUP, 0
        g_VShiftDown = False
    End If
End Function
Public Function HelpBalloon(vHeading As Variant, vMessage As Variant) As Boolean
    Dim blnNew As Balloon
    Dim lAnimate As Long
    
    If Left(Application.Version, 1) <> "8" Then
        Dim obj As Object
        Set obj = Application.Assistant
        If Not obj.On Then
            On Error GoTo 0
            GoTo AssistantNotAvailable
        End If
    End If

    On Error GoTo AssistantNotAvailable
    lAnimate = Application.Assistant.Animation
    On Error GoTo 0
    
    Set blnNew = Application.Assistant.NewBalloon
    Application.ScreenUpdating = True
    With blnNew
        
        .Animation = msoAnimationBeginSpeaking
        .Heading = vHeading
        .Text = vMessage
        .BalloonType = msoBalloonTypeNumbers
        .Mode = msoModeModal
        .Button = msoButtonSetOK
        .Show
    End With
    Application.Assistant.Animation = lAnimate
    Exit Function
    
AssistantNotAvailable:
    MsgBox vMessage, vbOKOnly, vHeading
End Function
Sub GetTabOrder(frmP As VB.Form, xControls() As String)
    Dim ctlP As VB.Control
    
    ReDim xControls(frmP.Controls.Count)
    On Error Resume Next
    For Each ctlP In frmP.Controls
        xControls(ctlP.TabIndex) = ctlP.Name
    Next ctlP
End Sub


Function SelectFirstCtlOnTab(ctlTab As VB.Control, xControls() As String)
'selects the first control on tab iTabIndex
'on active form
    Dim iTabIndex As Integer
    On Error Resume Next
    iTabIndex = GetFirstCtlOnTab(ctlTab.Tab)
    SelectTabIndex iTabIndex, xControls()
End Function
Function bEnsureSelectedContent(ctlP As VB.Control, _
                                Optional bLastCharOnly As Boolean = False) As Boolean
    Dim ctlData As VB.Data
    On Error Resume Next
    With ctlP
        If (TypeOf ctlP Is ListBox) Or (TypeOf ctlP Is MSForms.ListBox) Then
            If .ListIndex < 0 Then _
                .ListIndex = 0
        ElseIf TypeOf ctlP Is DBList Then
'           assumes data control is always named datPublic
            Set ctlData = ctlP.Parent.datPublic
            If ctlP.BoundText = "" Then
                ctlData.Recordset.MoveFirst
                ctlP.BoundText = ctlData.Recordset!fldID
            End If
        Else
            If bLastCharOnly = False Then
                .SelStart = 0
                .SelLength = Len(.Text)
            Else
                .SelStart = Len(.Text)
                .SelLength = 1
            End If
        End If
    End With
End Function


Function CycleTabs(iCurTab As Integer, vbControls() As String)
'either activates next or previous tab
'depending on value of iCurTab and relative
'value of cycle control
    With Screen.ActiveForm.MultiPage1
        If .Tab = iCurTab - 1 Then
            ActivateNextTab vbControls()
        Else
            ActivatePrevTab vbControls()
        End If
    End With
End Function
Function bCycleComboBox(ctlP As MSForms.ComboBox, KeyCode As MSForms.ReturnInteger, Shift As Integer, _
                        Optional bFakeCombo As Boolean = False) As Boolean
    Dim bAltIsDown As Boolean
    Debug.Print g_VShiftDown & Str(KeyCode) & "fakecombo = " & bFakeCombo
    
    iKey = KeyCode
'---lock virtual shift down
    Dim altscan%
    altscan% = MapVirtualKey(VK_SHIFT, 0)
    If KeyCode = 40 Or KeyCode = 38 Then
        If Shift = 4 Then
            keybd_event VK_SHIFT, altscan, 0, 0
            g_VShiftDown = True
        End If
    ElseIf KeyCode = 9 Then
'---tab releases v-shift, closes dropdown,
        If g_VShiftDown = True Then
            keybd_event VK_SHIFT, altscan, KEYEVENTF_KEYUP, 0
            g_VShiftDown = False
            KeyCode = 27    'escape closes dropdown
        End If
        If bFakeCombo Then SendKeys "+{tab}"
'---convert esc key to tab if combo is dropped to prevent lost focus
    ElseIf KeyCode = 27 Then
        If g_VShiftDown = True Then
            keybd_event VK_SHIFT, altscan, KEYEVENTF_KEYUP, 0
            g_VShiftDown = False
        End If
        If bFakeCombo Then
            KeyCode = 0
            SendKeys "+{tab}"
        End If
'---disable right/left arrow (or you could convert to tab, etc)
    ElseIf KeyCode = 39 Or KeyCode = 37 Then
        KeyCode = 0
    End If
    
    On Error Resume Next
    bAltIsDown = (GetKeyState(&H12) < 0)
    If Not bAltIsDown Then
        Select Case KeyCode
            Case mpKey_DownArrow
'               cancel key if at bottom of list
                If ctlP.ListIndex = ctlP.ListCount - 1 Then
                    KeyCode = 0
                End If
            Case mpKey_UpArrow
'               cancel key if at top of list
                If ctlP.ListIndex = 0 Then
                    KeyCode = 0
                End If
        End Select
    End If
End Function
Function VBACtlLostFocus(ctlP As VB.Control, _
                         xControls() As String, _
                         Optional bShiftRelease As Boolean = True)
'this is a patch necessary to handle shift tab
'with VBA controls - YUK! - use in conjuntion with
'SelectCtlIfShiftTabbed - call only from VBA controls
'lost focus event procedure

'---release virtual shift down if present
        If g_VShiftDown And bShiftRelease Then
            Dim altscan%
            altscan% = MapVirtualKey(VK_SHIFT, 0)
            keybd_event VK_SHIFT, altscan, KEYEVENTF_KEYUP, 0
            g_VShiftDown = False
        End If

'   ensure that this only runs one at a time
    If Not g_bInVBALostFocus Then
        
        g_bInVBALostFocus = True
        SelectCtlIfShiftTabbed ctlP, xControls()
'        bEnsureSelectedContent Screen.ActiveControl
        g_bInVBALostFocus = False
    End If
End Function

Function ControlHasValue(ctlP As Control) As Boolean
'---for some reason, type of isn't returning CheckBox
    If InStr(ctlP.Name, "chk") Or _
        InStr(ctlP.Name, "CheckBox") Then
        ControlHasValue = True
        Exit Function
    End If
  
    If TypeOf ctlP Is TextBox Or _
        TypeOf ctlP Is MSForms.ComboBox Or _
        TypeOf ctlP Is MSForms.TextBox Or _
        TypeOf ctlP Is VB.ComboBox Or _
        TypeOf ctlP Is CheckBox Or _
        TypeOf ctlP Is MSForms.ToggleButton Or _
        TypeOf ctlP Is MSForms.ListBox Or _
        TypeOf ctlP Is VB.ListBox Or _
        TypeOf ctlP Is OptionButton Or _
        TypeOf ctlP Is mpControls3.SpinTextInternational Then
        ControlHasValue = True
    Else
        ControlHasValue = False
    End If
End Function

Function GetFirstCtlOnTab(iTab As Integer) As Integer
'returns the index of the first control on tab iTab
    Dim xCycleCtl As String
    Dim iLastTabIndex As Integer
    
    If iTab = 0 Then
        GetFirstCtlOnTab = 0
    Else
        xCycleCtl = "picCycleTab" & iTab
        GetFirstCtlOnTab = Screen.ActiveForm _
                        .Controls(xCycleCtl).TabIndex + 1
    End If
    Exit Function
    
ProcError:
    MsgBox Error(Err), vbExclamation, mpAppName
    Exit Function
End Function


Function SelectTabIndex(iTabIndex As Integer, _
                        xControls() As String, _
                        Optional iIncr As Integer = 1)
'sets focus on control whose tab index = itabIndex.
'if control can't take focus, next/prev control is
'attempted (iIncr determines whether it's next or prev)
    Dim bCanGetFocus As Boolean
    Dim ctlFocus As VB.Control
    Dim xCtl As String
    Dim frmP As VB.Form
    
    Set frmP = Screen.ActiveForm
    
    Do
'       get control of previous tab index
        xCtl = xControls(iTabIndex)
        Set ctlFocus = frmP.Controls(xCtl)
            
'       test for enabled, visible, tabstop
        bCanGetFocus = False
        On Error Resume Next
        With ctlFocus
            bCanGetFocus = .Enabled And _
                           .Visible And _
                           .TabStop
        End With
        On Error GoTo 0
'''        DoEvents
        iTabIndex = iTabIndex + iIncr
    Loop Until (bCanGetFocus)
    ctlFocus.SetFocus
End Function


Function ActivateNextTab(xControls() As String) As Long
'selects next tab -
'skips disabled tabs-
'if last tab, goes to tab 1
    Dim iStartTabIndex As Integer
    Dim iTabState As Integer
    Dim iTab As Integer
    Dim iFirstCtl As Integer
    Dim iShiftState As Integer
    Dim bIsShiftTabbing As Boolean
    
'   get state of tab/shift tab
    iTabState = GetKeyState(&H9)
    iShiftState = GetKeyState(&H10)
    
'   set global flag
    g_ActivatingTabProgrammatically = True
    
    bIsShiftTabbing = iTabState < 0 And _
                      iShiftState < 0
                      
    If Not bIsShiftTabbing Then
        With Screen.ActiveForm.MultiPage1
            iStartTabIndex = .Tab
            'On Error Resume Next
            
'           force to first tab if currently on last tab
            If iStartTabIndex = (.Tabs - 1) Then
                iTab = 0
            Else
                '---bug fix if last tab disabled as in notary
                If .TabEnabled(.Tabs - 1) = False And _
                        iStartTabIndex <> 0 Then
                    iTab = 0
                Else
                    iTab = iStartTabIndex + 1
                End If
            End If
            
            While Not .TabEnabled(iTab)
'               if current disabled tab is also last tab
'               select first tab and leave
                If iTab >= .Tabs - 1 Then
'                   set to Finish button - questionable line
                    Screen.ActiveForm.cmdFinish.SetFocus
                    Exit Function
                    iTab = 0
                    GoTo SelectTab
                End If
'               force to first tab if iTab is last tab
                If iTab = (.Tabs - 1) Then
                    iTab = 0
                Else
                    iTab = iTab + 1
                End If
            Wend
SelectTab:
            
'           select tab
            .Tab = iTab
            
            DisableInactiveTabs Screen.ActiveForm
           
'           get index of first ctl on tab
            iFirstCtl = GetFirstCtlOnTab(iTab)
            
        End With

        
'       select first control on selected tab
        SelectTabIndex iFirstCtl, _
                       xControls()
    End If
    g_ActivatingTabProgrammatically = False
End Function


Function ActivatePrevTab(xControls() As String) As Long
'selects next tab -
'skips disabled tabs-
'if last tab, goes to tab 1
    Dim iStartTabIndex As Integer
    Dim iTab As Integer
    Dim iLastControl As Integer
    
    Dim ctlP As VB.Control
    g_ActivatingTabProgrammatically = True
    With Screen.ActiveForm.MultiPage1
        iTab = .Tab
        If .Tab > 0 Then
            iStartTabIndex = iTab
            On Error Resume Next
            iTab = iTab - 1
            While Not .TabEnabled(iTab)
                iTab = mpMax(iTab - 1, 0)
            Wend
        End If
        
'       select tab
        .Tab = iTab
        
        DisableInactiveTabs Screen.ActiveForm
           
'       get tab index of last control
'       on selected tab
        iLastControl = GetLastCtlOnTab(.Tab)
    End With
    
'   select last control on selected tab
    SelectTabIndex iLastControl, xControls(), -1
    g_ActivatingTabProgrammatically = False
End Function

Sub SelectCtlIfShiftTabbed(ctlP As VB.Control, _
                           xTabOrder() As String)
'selects ctlTab/ctlShiftTab if
'user has tab/shift tabbed

    Dim iTabState As Integer
    Dim iShiftState As Integer
    Dim bIsShiftTabbing As Boolean
    
'   get state of tab/shift tab
    iTabState = GetKeyState(&H9)
    iShiftState = GetKeyState(&H10)
    
    bIsShiftTabbing = iTabState < 0 And _
                      iShiftState < 0
                      
    If bIsShiftTabbing Then
        SelNextPrevCtl ctlP, xTabOrder(), False
    End If
End Sub
Function GetLastCtlOnTab(iTab As Integer) As Integer
'returns the index of the last control on tab iTab
    Dim xCycleCtl As String
    Dim iLastTabIndex As Integer
    
    xCycleCtl = "picCycleTab" & (iTab + 1)
    GetLastCtlOnTab = Screen.ActiveForm _
                    .Controls(xCycleCtl).TabIndex - 1
    Exit Function
    
ProcError:
    MsgBox Error(Err), vbExclamation, mpAppName
    Exit Function
End Function


Sub SelNextPrevCtl(ctlP As VB.Control, _
                   xControls() As String, _
                   Optional bNext As Boolean = True)
'selects the first enabled control possible
'with the smallest tab index greater than the
'tab index of ctlP.  Useful with "cycler"
'controls in dlgs. xControls() is an array of all
'control names whose array index is each controls
'tab index.
    Dim i As Integer
    Dim iIncr As Integer
    Dim iLastTabIndex As Integer
    Dim iTabIndex As Integer
    Dim xCtl As String
    Dim ctlFocus As VB.Control
    Dim bCanGetFocus As Boolean
    
    iTabIndex = ctlP.TabIndex

'   next/previous ctl incrementer
    If bNext Then
        iIncr = 1
    Else
        iIncr = -1
    End If
    
'''    DoEvents
    
'   get last control index to test
    If bNext Then
        iLastTabIndex = ctlP.Parent.Controls.Count - 1
    Else
        iLastTabIndex = 0
    End If
Debug.Print "Source: " & ctlP.Name
    
    Do
        On Error Resume Next
'       get control of previous tab index
        iTabIndex = iTabIndex + iIncr
        If iTabIndex < 0 Then iTabIndex = 0
        xCtl = xControls(iTabIndex)
        Set ctlFocus = ctlP.Parent(xCtl)
        
'       test for enabled, visible, tabstop
        bCanGetFocus = False
        With ctlFocus
            bCanGetFocus = .Enabled And _
                           .Visible And _
                           (.TabStop Or Left(.Name, 11) = "picCycleTab")
        End With
        On Error GoTo 0
'''        DoEvents
        Loop Until (bCanGetFocus Or iTabIndex = iLastTabIndex)
Debug.Print "Dest: " & ctlFocus.Name & ":" & bCanGetFocus
    
'   if the correct control has been found
'   move focus, else keep focus on same control -
'   also prevent focus from going from tab 1 to tab 3
'   (ie recycling backwards)
    If bCanGetFocus Then
        ctlFocus.SetFocus
'''        DoEvents
    Else
        ctlP.SetFocus
    End If
End Sub


