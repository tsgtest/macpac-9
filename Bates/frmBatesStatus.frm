VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Begin VB.Form frmBatesStatus 
   ClientHeight    =   2808
   ClientLeft      =   60
   ClientTop       =   60
   ClientWidth     =   5112
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   2808
   ScaleWidth      =   5112
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.ProgressBar prbStatus 
      Height          =   270
      Left            =   60
      TabIndex        =   0
      Top             =   1860
      Width           =   4935
      _ExtentX        =   8700
      _ExtentY        =   487
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Default         =   -1  'True
      Height          =   360
      Left            =   1845
      TabIndex        =   1
      Top             =   2310
      Width           =   1365
   End
   Begin VB.Label lblTitle 
      Caption         =   "Creating Bates Labels..."
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   75
      Width           =   1875
   End
   Begin VB.Label lblStatus 
      Caption         =   "lblStatus"
      Height          =   1290
      Left            =   120
      TabIndex        =   2
      Top             =   510
      Width           =   4890
   End
End
Attribute VB_Name = "frmBatesStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public m_bJobCancelled As Boolean

Sub bCancelJob()
    Dim rTableLoc  As Range
    Dim dlgBates As Form
        
    On Error Resume Next
'---clean up manual page breaks
    Me.lblTitle = "Job Cancelled..."
    Application.StatusBar = "Bates job cancelled.  Cleaning up format, one moment please"

    Me.m_bJobCancelled = True
End Sub

Private Sub btnCancel_Click()
    bCancelJob
End Sub

Private Sub Form_Load()
    Dim xBlnText As String
    Me.m_bJobCancelled = False
    xBlnText = "Click Cancel Job to halt processing." & vbCr & vbCr & _
               "To create more than 2500 labels it will be faster to run several smaller batches of labels." & _
               "  Use the Reuse macro to set up your Bates Labels job again."
    
    Me.lblStatus.Caption = xBlnText

End Sub

