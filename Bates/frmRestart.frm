VERSION 5.00
Begin VB.Form frmRestart 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Restart form - Caption created on initialization"
   ClientHeight    =   2505
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4965
   Icon            =   "frmRestart.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2505
   ScaleWidth      =   4965
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton optAdd 
      Appearance      =   0  'Flat
      Caption         =   "opt&Add"
      ForeColor       =   &H80000008&
      Height          =   340
      Left            =   270
      TabIndex        =   4
      Top             =   1110
      Width           =   4260
   End
   Begin VB.OptionButton optNew 
      Appearance      =   0  'Flat
      Caption         =   "opt&New"
      ForeColor       =   &H80000008&
      Height          =   340
      Left            =   270
      TabIndex        =   3
      Top             =   240
      Value           =   -1  'True
      Width           =   4260
   End
   Begin VB.OptionButton optDelete 
      Appearance      =   0  'Flat
      Caption         =   "opt&Delete"
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   270
      TabIndex        =   2
      Top             =   710
      Width           =   4260
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   2550
      TabIndex        =   0
      Top             =   1995
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3735
      TabIndex        =   1
      Top             =   1995
      Width           =   1100
   End
End
Attribute VB_Name = "frmRestart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public bFinishSelected As Boolean

Sub btnHelp_Click()
    Dim xTemplate As String
    Dim xHeading As String
    Dim xMsg As String
    
    With ActiveDocument
        'xTemplate = Left(.AttachedTemplate, Len(.AttachedTemplate) - 4)
        xTemplate = Left(.AttachedTemplate, InStr(.AttachedTemplate, ".") - 1)
    End With
            
    xMsg = "This macro will create a new " & xTemplate & _
        " based on the current document variables " & _
        "such as name, address, subject, author, etc.  " & _
        "A new dialog box will open with the current variables.  " & _
        "Only the variables that will be different " & _
        "for the new " & xTemplate & " will need to be changed." & _
        vbCr & vbCr & _
        "Create a New " & xTemplate & vbCr & _
        "Creates new " & xTemplate & " with a new Doc ID or Version# -" & vbCr & _
         "current " & xTemplate & " is not modified" & vbCr & vbCr & _
        "Replace the Existing " & xTemplate & vbCr & _
         "Deletes and replaces the current " & xTemplate & " permanently -" & vbCr & _
         "uses current Doc ID for new " & xTemplate & vbCr & vbCr & _
        "Append New " & xTemplate & vbCr & _
         "Appends new " & xTemplate & " at the end of the current " & xTemplate & _
         " in a new section - " & vbCr & "uses current Doc ID"
    xHeading = "Reuse Current " & xTemplate
    HelpBalloon xHeading, xMsg
End Sub

Private Sub btnCancel_Click()
    Me.Hide
End Sub

Private Sub btnHelp_KeyDown(KeyCode As Integer, Shift As Integer)
    'If KeyCode = 117 And Shift = 2 Then 'Ctrl-F6
    '     frmEgg.Show vbModal
    'End If
End Sub

Private Sub cmdFinish_Click()
    Me.Hide
    bFinishSelected = True
    Application.ScreenRefresh
End Sub

Private Sub optAdd_DblClick()
    cmdFinish_Click
End Sub


Private Sub optDelete_DblClick()
    cmdFinish_Click
End Sub

Private Sub optNew_DblClick()
    cmdFinish_Click
End Sub

Private Sub Form_Load()
    Dim xTemplate As String
    Dim iWidthFraOld As Single

'***Get original frame width
'    iWidthFraOld = Me.fraOptions.Width
    
    With ActiveDocument
        'xTemplate = Left(.AttachedTemplate, Len(.AttachedTemplate) - 4)
        xTemplate = Left(.AttachedTemplate, InStr(.AttachedTemplate, ".") - 1)
    End With
    
    With Me
        .Caption = "Reuse the Current " & xTemplate & " Document"
        .optNew.Caption = "Create a &New " & xTemplate & " Document"
        .optDelete.Caption = "&Replace the Existing " & xTemplate & " Document"
        .optAdd.Caption = "&Append New " & xTemplate & " Document"
        
#If bImanageInstalled Then
' Remove "Create New" option for iManage
    .optNew.Visible = False
    .optDelete = True
    .optAdd.Top = .optDelete.Top
    .optDelete.Top = .optNew.Top
    .fraOptions.Height = .fraOptions.Height - .optAdd.Height
    .btnHelp.Top = .btnHelp.Top - .optAdd.Height
    .btnCancel.Top = .btnCancel.Top - .optAdd.Height
    .cmdFinish.Top = .cmdFinish.Top - .optAdd.Height
    .Height = .Height - .optAdd.Height
#End If
        
''***    optDelete will always be longest control
'        .optDelete.AutoSize = True
''***    check for minimum width so Finish button doesn't run into Help
'        If .optDelete.Width < 135 * mpTwipsPerPoint Then _
'            .optDelete.Width = 135 * mpTwipsPerPoint
''***    resize form/frame
'        .fraOptions.Width = .optDelete.Width + 10 * mpTwipsPerPoint
'        .Width = .fraOptions.Width + 20 * mpTwipsPerPoint
''***    reposition Finish/cancel buttons
'        .cmdFinish.Left = .cmdFinish.Left - _
'                    (iWidthFraOld - .fraOptions.Width)
'        .btnCancel.Left = .btnCancel.Left - _
'                    (iWidthFraOld - .fraOptions.Width)
    End With
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmRestart = Nothing
End Sub

