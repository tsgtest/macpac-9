VERSION 5.00
Begin VB.Form frmBatesPrint 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Print Bates Labels"
   ClientHeight    =   3435
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4185
   ControlBox      =   0   'False
   Icon            =   "frmBatesPrint.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3435
   ScaleWidth      =   4185
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdFinish 
      Caption         =   "O&K"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   370
      Left            =   2085
      TabIndex        =   5
      Top             =   2985
      Width           =   840
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   370
      Left            =   3210
      TabIndex        =   6
      Top             =   2985
      Width           =   840
   End
   Begin VB.Frame frmTrays 
      Caption         =   "Select Tray Option for Bates Labels"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1456
      Left            =   105
      TabIndex        =   2
      Top             =   1395
      Width           =   3975
      Begin VB.OptionButton obtnDefault 
         Caption         =   "&Default Tray"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   345
         TabIndex        =   1
         Top             =   285
         Value           =   -1  'True
         Width           =   1305
      End
      Begin VB.OptionButton obtnManual 
         Caption         =   "&Manual Feed"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   345
         TabIndex        =   3
         Top             =   645
         Width           =   1320
      End
      Begin VB.OptionButton obtnByPass 
         Caption         =   "&Bypass (paper source previously set)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   345
         TabIndex        =   4
         Top             =   990
         Width           =   3075
      End
   End
   Begin VB.Label lblMessage 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1065
      Left            =   105
      TabIndex        =   0
      Top             =   170
      Width           =   3795
   End
End
Attribute VB_Name = "frmBatesPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit
Private vbControls() As String
Public m_bFinished As String

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdFinish_Click()
    Me.Hide
    DoEvents
    bSetPrinterDrawer
    Unload Me
End Sub

Private Function bSetPrinterDrawer() As Boolean
    Dim frmSPT As Form
    Dim secSection As Word.Section
    Dim iSection As Integer
    Dim iCount As Integer
    
    '---Get Current Section
    iSection = Selection.Information(wdActiveEndSectionNumber)
    Set secSection = ActiveDocument.Sections(iSection)
    'iCount = UserForms.count
    
    Set frmSPT = frmBatesPrint
    
    With frmSPT
        If .obtnDefault = True Then
            With secSection.PageSetup
                .FirstPageTray = wdPrinterDefaultBin
                .OtherPagesTray = wdPrinterDefaultBin
            End With
        End If
        
        If .obtnManual = True Then
            With secSection.PageSetup
                .FirstPageTray = wdPrinterManualFeed
                .OtherPagesTray = wdPrinterManualFeed
            End With
        End If
        

        If .obtnByPass = True Then
            '---do nothing
        End If

    End With
'---display printer dialog
    
    Dialogs(wdDialogFilePrint).Show ' vbModal

End Function

Private Sub Form_Load()

    GetTabOrder Me, vbControls()

    Dim xSheets As String
    Dim xCol As String
    Dim xRow As String
    Dim xMsg As String
    Dim xPlur As String
    
    On Error Resume Next
    With ActiveDocument
        xSheets = .Variables("lblSheets")
        xPlur = " sheet"
        If Val(xSheets) > 1 Then xPlur = xPlur & "s"
        xRow = .Variables("spnStartingLabel")
        If xRow = "" Then
            xRow = .Variables("txtStartingLabel")
        End If
        If .Variables("obtn1") = True Then
            xCol = 1
        ElseIf .Variables("obtn2") = True Then
            xCol = 2
        ElseIf .Variables("obtn3") = True Then
            xCol = 3
        Else
            xCol = 4
        End If
    End With
    
    If xSheets <> "" Then
        xMsg = "Please insert " & xSheets & xPlur & _
            " of Avery #5267 labels into the printer." & vbCr & vbCr
        xMsg = xMsg & "Labels will begin on row " & xRow & _
            ", column " & xCol & "."
        Me.lblMessage.Caption = xMsg

    End If
    
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmBatesPrint = Nothing
End Sub

