Attribute VB_Name = "mpConstants"
Public Const mpProfileBackEnd = False
Public Const mpMsgFinished As String = "Finished"
Public Const mpMsgReady As String = "Ready"
Public Const mpNoListArrays As Integer = 380
Public Const mpInitializeRetry As Integer = 3
Public Const mpMsgInitializing As String = "Initializing.  Please wait..."
Public Const mpBlockNotSet As Integer = 91
Public Const mpRestartReplaceExisting = 1
Public Const mpRestartAppend = 2
Public Const mpRestartNewProfile = 4
Public Const mpAppName As String = "MacPac 9"
Public Const mpStatusMsg As String = "Click Cancel to halt job processing." & vbCr & vbCr & _
               "To create more than 2500 labels it will be faster to run several smaller batches of labels." & _
               "  Use the Reuse macro to set up your Bates Labels job again."
Public g_b125 As Boolean


