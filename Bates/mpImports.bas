Attribute VB_Name = "mpIMPORTS"
Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
Declare Function MapVirtualKey Lib "user32" Alias "MapVirtualKeyA" (ByVal wCode As Long, ByVal wMapType As Long) As Long

Function bDocIsRestarted(docDoc As Word.Document) As Boolean
'returns true for documents
'whose macros have been run already
    On Error GoTo DocIsRestarted_Error

    bDocIsRestarted = docDoc.Variables("Restarted")
    Exit Function

DocIsRestarted_Error:
    bDocIsRestarted = False
    Exit Function
End Function
Public Sub GetDocVars(frmP As Form)
'assigns stored doc variable values to
'controls of same name - forces author list
'controls to evaluate first to avoid change
'events of those controls from affecting
'the values of other controls

    Dim ctlControl As Control
    Dim vCurValue As Variant
    Dim xPriorityCtls(1 To 9) As String
    Dim bIsPriorityControl As Boolean
    Dim i As Integer
    Dim xName As String
    Dim SkipListUpdate As Boolean
    
'   enumerate trouble controls - this is the only part,
'   if any, that should require modification
    xPriorityCtls(1) = "cmbAuthorLists"
    xPriorityCtls(2) = "lstAttyList"
    xPriorityCtls(3) = "cmbAuthor"
    xPriorityCtls(4) = "cmbPrefLists"
    xPriorityCtls(5) = "cmbSetAuthorPref"
    xPriorityCtls(6) = "cmbOptOffices"  'needed for Verification
    xPriorityCtls(7) = "cmbRecipient"
    xPriorityCtls(8) = "cmbRecipientLists"
    xPriorityCtls(9) = "cmbState"  'needed for Certificate of Service
    
'   run through priority controls, setting their values-
'   remember, change events will occur, but they should
'   be irrelevant after non-priority controls are set below
    For i = 1 To UBound(xPriorityCtls)
        On Error Resume Next
        Set ctlControl = frmP.Controls(xPriorityCtls(i))
        vCurValue = ActiveDocument.Variables(ctlControl.Name)
        If Not IsEmpty(vCurValue) Then
            SkipListUpdate = True
            ctlControl = vCurValue
        End If
    Next i
    
    On Error GoTo 0
    
'   run through non-priority controls,
'   setting their values
    For Each ctlControl In frmP.Controls
        If ControlHasValue(ctlControl) Then
            xName = ctlControl.Name
            
            bIsPriorityControl = _
                (xName = xPriorityCtls(1)) Or _
                (xName = xPriorityCtls(2)) Or _
                (xName = xPriorityCtls(3)) Or _
                (xName = xPriorityCtls(4)) Or _
                (xName = xPriorityCtls(5)) Or _
                (xName = xPriorityCtls(6)) Or _
                (xName = xPriorityCtls(7)) Or _
                (xName = xPriorityCtls(8)) Or _
                (xName = xPriorityCtls(9))
                
            If Not bIsPriorityControl Then
                On Error Resume Next
                    vCurValue = ActiveDocument.Variables(ctlControl.Name)
                If Not IsEmpty(vCurValue) Then
                    SkipListUpdate = True
                    If InStr(xName, "chk") Then
                        ctlControl = Abs(CBool(vCurValue))
                    ElseIf InStr(xName, "spn") Then
                        ctlControl.Value = _
                            mpbase2.xLocalizeNumericString(CStr(vCurValue))
                    Else
                        ctlControl = vCurValue
                    End If
                End If
            End If
        End If
    Next ctlControl
End Sub

Function xSubstitute(ByVal xString As String, _
                     xSearch As String, _
                     xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Integer
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While iSeachPos
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
        iSeachPos = InStr(UCase(xString), _
                          UCase(xSearch))
    
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function
    


Function mpMax(i As Double, j As Double) As Double
    If i > j Then
        mpMax = i
    Else
        mpMax = j
    End If
End Function

Function mpMin(i As Double, j As Double) As Double
    If i > j Then
        mpMin = j
    Else
        mpMin = i
    End If
End Function

Function secSetScope(Optional bAppend As Boolean = False, _
                        Optional bAppendFirst As Boolean = False, _
                        Optional bLinkHeaders As Boolean = False, _
                        Optional bLinkFooters As Boolean = False)
'---set scope- if bAppend then
'   insert section break, else
'   clear whole document
    Dim secScope As Word.Section
    
    If bAppend Then
        If bAppendFirst Then
'---if first section set break at start of doc
            ActiveDocument.Characters.First. _
                InsertBreak wdSectionBreakNextPage
            Set secScope = ActiveDocument.Sections.First
'---Unlink the headers & Footers in section following new sec.
            bUnlinkHeadersFooters ActiveDocument.Sections(2), bLinkHeaders, _
                bLinkFooters
        Else
            ActiveDocument.Characters.Last. _
                InsertBreak wdSectionBreakNextPage
            Set secScope = ActiveDocument.Sections.Last
'---Unlink the headers & Footers
            bUnlinkHeadersFooters secScope, bLinkHeaders, bLinkFooters
        
        End If

            
'---clear out new section -
'       especially headers/footers
'CharlieCore - changed for MP 9.2.0 release - template now contains marker for no trailer
'              in footer
        If bAppendFirst = False Then
            bClearContent False, secScope, True, Not bLinkHeaders
'            bClearContent False, secScope, True, Not bLinkHeaders, Not bLinkFooters
        Else
            bClearContent False, secScope, False, Not bLinkHeaders
'            bClearContent False, secScope, False, Not bLinkHeaders, Not bLinkFooters
        End If
        
        Set secSetScope = secScope
    Else
'---    clear out all
'CharlieCore - changed for MP 9.2.0 release - template now contains marker for no trailer
'              in footer
'        bClearContent True, , True, True, True
        bClearContent True, , True, True
        Set secSetScope = ActiveDocument.Sections.Last
    End If

End Function


Function bUnlinkHeadersFooters(xSection As Word.Section, _
    Optional bLinkHeaders As Boolean = False, _
    Optional bLinkFooters As Boolean = False) As Boolean
            
    With xSection
        If Not bLinkHeaders Then
            With .Headers(wdHeaderFooterPrimary)
                .LinkToPrevious = False
            End With
            With .Headers(wdHeaderFooterFirstPage)
                .LinkToPrevious = False
            End With
        End If
        If Not bLinkFooters Then
            With .Footers(wdHeaderFooterPrimary)
                .LinkToPrevious = False
            End With
            With .Footers(wdHeaderFooterFirstPage)
                .LinkToPrevious = False
            End With
        End If
    End With

End Function


Function bClearContent(Optional bAllSections As Boolean = True, _
                        Optional secCurSection As Word.Section, _
                        Optional bMainStory As Boolean = False, _
                        Optional bHeaders As Boolean = False, _
                        Optional bFooters As Boolean = False) _
                        As Boolean

'clears contents of specified stories
'of specified sections

'/////////MODIFIED COMPLETELY
    Dim hdrHeader As HeaderFooter
    Dim ftrFooter As HeaderFooter
    Dim secSection As Word.Section
    Dim secSections As Word.Sections
    
    If bAllSections Then
        For Each secSection In ActiveDocument.Sections
            With secSection
                If bMainStory Then
                    .Range.Delete
                End If
                    
                If bHeaders Then
                    For Each hdrHeader In .Headers
                        hdrHeader.Range.Delete
                    Next hdrHeader
                End If
        
                If bFooters Then
                    For Each ftrFooter In .Footers
                        ftrFooter.Range.Delete
                    Next ftrFooter
                End If
            End With
        Next secSection
    Else
        With secCurSection
            If bMainStory Then
                .Range.Delete
            End If
                
            If bHeaders Then
                For Each hdrHeader In .Headers
                    hdrHeader.Range.Delete
                Next hdrHeader
            End If
    
            If bFooters Then
                For Each ftrFooter In .Footers
                    ftrFooter.Range.Delete
                Next ftrFooter
            End If
        End With
    End If
End Function
Function bGetDocRanges(Optional rngMain As Word.Range, _
                       Optional rngPrimaryHeader As Word.Range, _
                       Optional rngPrimaryFooter As Word.Range, _
                       Optional rngFirstHeader As Word.Range, _
                       Optional rngFirstFooter As Word.Range, _
                       Optional vDoc As Variant, _
                       Optional vSection As Variant) As Boolean
'fills args with range values
    If IsMissing(vDoc) Then _
        Set vDoc = ActiveDocument
        
    If IsMissing(vSection) = True Then _
        Set vSection = vDoc.Sections.First
        
    Set rngMain = vSection.Range
    
    With vSection
        Set rngPrimaryHeader = .Headers(wdHeaderFooterPrimary).Range
        Set rngPrimaryFooter = .Footers(wdHeaderFooterPrimary).Range
        Set rngFirstHeader = .Headers(wdHeaderFooterFirstPage).Range
        Set rngFirstFooter = .Footers(wdHeaderFooterFirstPage).Range
    End With
End Function


Function bDeleteAllBookmarks(Optional bMacPacOnly _
                    As Boolean = False) As Boolean
                    
'-7/29 this is a slightly different version, which
'-leaves zzmpFixed - prefixed bookmarks alone, as w/ docvars

'   deletes all bookmarks or all
'   MacPac (zzmp) bookmarks

'---safety if boilerplate doc
    
    If InStr(UCase(ActiveDocument.Name), ".ATE") Then
        Exit Function
    End If

    Dim bmkBookmark As Bookmark
    
    If bMacPacOnly Then
        For Each bmkBookmark In ActiveDocument.Bookmarks
            If Left(bmkBookmark.Name, 4) = "zzmp" Then _
                If InStr(bmkBookmark.Name, "zzmpFixed") = 0 Then _
                    bmkBookmark.Delete
        Next bmkBookmark
    Else
        For Each bmkBookmark In ActiveDocument.Bookmarks
            bmkBookmark.Delete
        Next bmkBookmark
    End If
End Function


Function UUDisplayMacroDuration(datTestStartTime As Date) As Boolean
    If mpDisplayBenchmarks Then
        Dim datTestEndTime As Date
        datTestEndTime = Now
        Application.StatusBar = "Macro took " & _
            Format((datTestEndTime - datTestStartTime) * 3600 * 24, "0.0000") & " seconds"
    End If
End Function
Function envGetDocEnvironment() As mpDocEnvironment
'fills type mpDocEnvironment with
'current doc environment settings -
'envMpDoc below is a global var

'   some properties are not available
'   in preview mode - this will cause
'   error - hence turn off trapping
    On Error Resume Next
    
    envMpDoc.iProtectionType = ActiveDocument.ProtectionType
    
'   get selection parameters
    With Selection
        envMpDoc.iSelectionStory = .StoryType
        envMpDoc.lSelectionStartPos = .Start
        envMpDoc.lSelectionEndPos = .End
    End With
        
    With ActiveDocument.ActiveWindow.ActivePane
        envMpDoc.sVScroll = .VerticalPercentScrolled
        envMpDoc.sHScroll = .HorizontalPercentScrolled
        With .View
            envMpDoc.bShowAll = .ShowAll
            envMpDoc.bFieldCodes = .ShowFieldCodes
            envMpDoc.bBookmarks = .ShowBookmarks
            envMpDoc.bTabs = .ShowTabs
            envMpDoc.bSpaces = .ShowSpaces
            envMpDoc.bParagraphs = .ShowParagraphs
            envMpDoc.bHyphens = .ShowHyphens
            envMpDoc.bHiddenText = .ShowHiddenText
            envMpDoc.bTextBoundaries = .ShowTextBoundaries
            envMpDoc.iView = .Type
        End With
    
    End With
    
    envGetDocEnvironment = envMpDoc
    
End Function
Function envGetAppEnvironment() As mpAppEnvironment
'fills type mpAppEnvironment with
'application environment settings -
'envMpApp below is a global var
    On Error Resume Next
    With Application
        envMpApp.lBrowserTarget = .Browser.Target
        .Assistant.Animation = msoAnimationWritingNotingSomething
    End With
        
    envGetAppEnvironment = envMpApp
    
End Function
Function bSetAppEnvironment(envCurrent As mpAppEnvironment, _
                            Optional bClearUndo As Boolean = False) As Boolean
'sets application environment settings
'using values in type mpAppEnvironment

'   added to ensure that Edit Find is always cleared
    bEditFindReset
    
    With Application
        On Error Resume Next
        .Browser.Target = envCurrent.lBrowserTarget
        
        If bClearUndo Then
'           clear "undo" list
            ActiveDocument.UndoClear
        End If
    
'       assistant animation sometimes
'       sticks, so reset
        With .Assistant
            .Animation = msoAnimationIdle
        End With
        On Error GoTo 0
    End With
    Screen.MousePointer = vbDefault
    
    bSetAppEnvironment = True
End Function

Function bSetDocEnvironment(envCur As mpDocEnvironment, _
                            Optional bScroll As Boolean = False, _
                            Optional bSelect As Boolean = False) As Boolean
'   sets the doc/window related vars
'   based on type mpDocEnvironment var (global)

    Dim rngStory As Word.Range
    
'   some properties are not available
'   in preview mode - this will cause
'   error - hence turn off trapping
    On Error Resume Next
    With Word.ActiveDocument.ActiveWindow.ActivePane
        With .View
            .ShowAll = envCur.bShowAll
            .ShowFieldCodes = envCur.bFieldCodes
            .ShowBookmarks = envCur.bBookmarks
            .ShowTabs = envCur.bTabs
            .ShowSpaces = envCur.bSpaces
            .ShowParagraphs = envCur.bParagraphs
            .ShowHyphens = envCur.bHyphens
            .ShowHiddenText = envCur.bHiddenText
            .ShowTextBoundaries = envCur.bTextBoundaries
            .Type = envCur.iView
        End With
        
        If bSelect Then
            With envCur
                Set rngStory = ActiveDocument.StoryRanges(.iSelectionStory)
                rngStory.SetRange .lSelectionStartPos, .lSelectionEndPos
                rngStory.Select
'               this needs to happen again here
'               because selecting the headers/footers
'               automatically puts you in normal view.
                Word.ActiveDocument.ActiveWindow.View.Type = .iView
            End With
        End If
        
        If bScroll Then
            .VerticalPercentScrolled = envCur.sVScroll
            .HorizontalPercentScrolled = envCur.sHScroll
        End If
        
    End With
    
    bSetDocEnvironment = True

End Function



Public Sub SetDocVars(frmP As Form, _
                      Optional bMarkAsRestarted As Boolean = True)
    
    Dim ctlControl As VB.Control
    Dim xName As String
    Dim vValue As Variant
    Dim varReuse As Variable
    
    For Each ctlControl In frmP.Controls
        If ControlHasValue(ctlControl) Then
                On Error Resume Next
'---        assign true false values for chkboxes, tglbuttons, Optionbuttons
                xName = ctlControl.Name
                If InStr(xName, "spn") Then
                    vValue = ctlControl.Value
                Else
                    vValue = ctlControl
                End If
                If InStr(xName, "chk") Or InStr(xName, "tgl") Or InStr(xName, "opt") Then
                    If vValue = False Then
                        vValue = "False"
                    Else
                        vValue = "True"
                    End If
                End If
                
'               if doc var exists, set value, else add doc var
                With ActiveDocument
                    On Error Resume Next
                    Set varReuse = Nothing
                    Set varReuse = .Variables(xName)
                    On Error GoTo 0

                    If varReuse Is Nothing Then
                        .Variables.Add xName, xNullToString(vValue)
                    Else
                        varReuse.Value = xNullToString(vValue)
                    End If
                End With
        End If
    Next ctlControl
    
'   mark as restarted
    If bMarkAsRestarted Then _
        ActiveDocument.Variables("Restarted") = True
End Sub


Function xNullToString(vValue As Variant) As String
    If Not IsNull(vValue) Then _
        xNullToString = CStr(vValue)
End Function


'Function bAppSetBatesINIValue(xSection As String, _
'                             xKey As String, _
'                             xValue As String) As Boolean
'    Dim xBatesIni As String
''    xBatesIni = Application.Options.DefaultFilePath(wdWorkgroupTemplatesPath) & "\mpBates.ini"
'    xBatesIni = App.Path & "\mpBates.ini"
'    System.PrivateProfileString(xBatesIni, xSection, xKey) = xValue
'End Function


Public Function bEditFindReset() As Boolean
'required becuase EditFind in WordBasic modifies
'the Edit/Find dialog - trailer and underline to
'longest both use WordBasic EditFind
    On Error Resume Next
    With WordBasic
        .EditReplace Find:="", _
            Replace:="", _
            Direction:=0, _
            WholeWord:=0, _
            MatchCase:=0, _
            PatternMatch:=0, _
            SoundsLike:=0, _
            Wrap:=0, _
            Format:=0
        .EditFindClearFormatting
        .EditReplaceClearFormatting
    End With
End Function

Function bRestart() As Long
'   show generic restart form
    
    Dim dlgRestart As New frmRestart
    Dim xCaption As String
    Dim iReplace As Integer
    Dim bProfile As Boolean
    Dim xTemplate As String
    Dim v As Variant
    Dim vRet As Variant
    
    On Error GoTo bRestart_Error
    
    g_bRestart = True
    
    With dlgRestart
        bRefreshReset
        .Show vbModal
        
        If .bFinishSelected Then
'           create doc based on restart options
            If .optAdd Then
                iReplace = mpRestartAppend
                bProfile = False
            ElseIf .optDelete Then
                iReplace = mpRestartReplaceExisting
                bProfile = False
            ElseIf .optNew Then
                vRet = vPromptToSave(ActiveDocument)
                If Not IsNull(vRet) Then
                    iReplace = mpRestartReplaceExisting
                    bProfile = True
                Else
                    Exit Function
                End If
            End If
    
            If iReplace = mpRestartReplaceExisting Then
                On Error Resume Next
                ActiveDocument.Variables("zzmpFixedDOC_ID").Delete
                On Error GoTo bRestart_Error
            End If
            
            '*c - O12
'            Select Case UCase(Word.ActiveDocument.AttachedTemplate)
'                Case "BATES LABELS.DOT"
                    lRet = mpBates.bDocCreate(iReplace, bProfile)
'                Case "BATES LABELS.DOT"
'                    lRet = mpBates.bDocCreate(iReplace, bProfile)
'            End Select
            
        End If
    End With
    
    Unload dlgRestart
    bRestart = lRet
    Exit Function

bRestart_Error:
    Select Case Err
        Case mpBlockNotSet  'control button cancel
            Unload frmRestart
            Exit Function
        Case Else
            Beep
            MsgBox "Error " & Str(Err) & vbCr & _
                Error(Err), vbExclamation, mpAppName + "(Restart)"
    End Select
    bRestart = Err
    Exit Function

End Function

Function bRefreshReset()
'ag This is similar to bRefresh but maintains _
    current scr updating value
    Dim bScrUpdatingStatus As Boolean
    With Application
        bScrUpdatingStatus = .ScreenUpdating
        .ScreenUpdating = True
        .ScreenRefresh
        .ScreenUpdating = bScrUpdatingStatus
    End With
    
End Function

Function vPromptToSave(docPointer As Word.Document) As Variant
'prompts to save current doc if
'necessary - saves if specified
'returns TRUE if saved, FALSE if
'not saved, ??? if cancelled
    If Not docPointer.Saved Then
        xMsg = "Do you want to save the changes " & _
               "you made to " & docPointer.Name & "?"
        iUserChoice = MsgBox(xMsg, vbExclamation + vbYesNoCancel)
        Select Case iUserChoice
            Case vbYes
                docPointer.Save 'FileSave
                If docPointer.Saved Then
                    vPromptToSave = True
                Else
                    vPromptToSave = False
                End If
            Case vbNo
                vPromptToSave = False
            Case vbCancel
                vPromptToSave = Null
        End Select
    End If
End Function

