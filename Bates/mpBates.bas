Attribute VB_Name = "mpBates"
Option Explicit

Public Declare Function LockWindowUpdate Lib "user32" ( _
    ByVal hwndLock As Long) As Long
Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" ( _
    ByVal lpClassName As String, ByVal lpWindowName As Long) As Long
    
Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)

Dim m_xPrefix As String
Dim m_xSuffix As String
Dim m_iCurNumber As Integer
Dim m_iNumLabels As Integer
Dim m_iStartNumber As Integer
Dim m_iEndNumber As Integer
Dim m_iDigits As Integer
Dim m_iHorizAlign As WdParagraphAlignment
Dim m_iVerticalAlign As WdCellVerticalAlignment
Dim m_iColumnCount As Integer
Dim m_iRowCount As Integer
Dim m_iStartCol As Integer
Dim m_iStartRow As Integer
Dim m_iInitialRow As Integer
Dim m_iInitialColumn As Integer
Dim m_bSideBySide As Boolean
Dim m_xConfidentialPhrases As String
Dim m_xConfidentialPosition As String
Dim m_xRAlignment As String
Dim m_vHAlignment As String

Private m_oStatusForm As Object

Sub EchoOff()
    Call LockWindowUpdate(FindWindow("OpusApp", 0&))
End Sub

Sub EchoOn()
    Call LockWindowUpdate(0&)
End Sub
Function bFilePrint() As Long
    On Error Resume Next
    frmBatesPrint.Show vbModal
    bFilePrint = Err
End Function

Function bWriteDoc(dlgBates As frmBates, _
                   Optional bAppend As Boolean) As Long
                   
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim iLead As Integer
    Dim rngMain As Range
    Dim secScope As Section
    Dim xNum As String
    Dim xCon As String
    Dim xFile As String
    Dim pTopMar As String
    Dim pBotMar As String
    Dim iTables As Integer
    Dim iNumRows As Integer
    Dim iNumCols As Integer
    Dim iSaveCounter As Integer
    Dim dInches As Double
    Dim fpsSection As PageSetup
    Dim tTable As Table
    Dim cCol As Column
    Dim bBorder As Border
    Dim rTableLoc As Range
    Dim iCompleted As Long
    Dim iCurNumber As Long
    Dim iEndNumber As Long
    Dim iStartNumber As Long
    Dim iStartCol As Integer
    Dim blnCancel As Balloon
    Dim xBlnText As String
    Dim CurrentRow As Long
    Dim RowCount As Long
    Dim CurrentColumn As Long
    Dim InitialColumn As Long
    Dim InitialRow As Long
    Dim ColumnCount As Long
    Dim iDigits As Long
    Dim iSpace As Integer
    Dim vHAlignment As Integer
    Dim lAdjust As Double
    Dim iRow As Integer
    Dim iCol As Integer
    Dim lStartTime As Long
    Dim lTemp As Long
    Dim oRange As Word.Range
    Dim oTable As Word.Table
    Dim oTableRng As Word.Range
    Dim xRAlignment As String
    Dim iStatus As Integer
    Dim oRow As Word.Row
    Dim xWindow As String
    Dim iTableColumns As Integer
    
    On Error GoTo bWriteDoc_Error
    
    bWriteDoc = False
    
'code later
    If g_b125 Then
        iRow = 25
        iCol = 5
    Else
        iRow = 20
        iCol = 4
    End If

'---show status form
    DoEvents
    
    Set m_oStatusForm = CreateObject("mpAXE.CStatus")

    With m_oStatusForm
        .Title = "Creating Bates Labels"
        .ProgressBarVisible = True
        .ShowCancel = True
        .Show 4, mpStatusMsg
    End With
    
    xWindow = Word.ActiveDocument.ActiveWindow.Caption
    Windows(xWindow).Activate
    
    DoEvents
    
    EchoOff
    Application.ScreenUpdating = False
    
    With dlgBates
        m_iCurNumber = Val(.txtStartNo)
        m_iNumLabels = Val(.txtNOLabels)
        m_iStartNumber = Val(.txtStartNo)
        m_iEndNumber = (m_iNumLabels + m_iStartNumber) - 1
        m_iDigits = .spnLeadZero.Value
        m_xPrefix = .txtPrefix
        m_xSuffix = .txtSuffix
        m_vHAlignment = .cmbAlignment.Column(1, .cmbAlignment.ListIndex)
        m_bSideBySide = True '  .chkSidebySide
        m_xConfidentialPhrases = .txtConfidential.Text
        m_xConfidentialPosition = .cmbCP.Text
        m_xRAlignment = .cmbAlignmentH
    
        If g_b125 Then
            m_iColumnCount = 5
            m_iRowCount = 25
        Else
            m_iColumnCount = 4
            m_iRowCount = 20
        End If
                
        'get start column & start row
        If m_bSideBySide Then
            m_iStartRow = .spnStartingColumn.Value
            m_iStartCol = .spnStartingLabel.Value
        Else
            m_iStartRow = .spnStartingLabel.Value
            m_iStartCol = .spnStartingColumn.Value
        End If
        
        If m_iStartCol = 0 Then m_iStartCol = 1
        If m_iStartRow = 0 Then m_iStartRow = 1
        
'---    get document ranges
        Set secScope = secSetScope(bAppend)
        
        bRet = bGetDocRanges(rngMain, _
                             , _
                             , _
                             , _
                             , , _
                             secScope)
                             
'       get specified left/right margins
        Set fpsSection = secScope.PageSetup
        Select Case .cmbAlignment.Column(1, .cmbAlignment.ListIndex)
            Case wdAlignParagraphLeft
                lAdjust = 0.2
            Case wdAlignPageNumberRight
                lAdjust = -0.05
            Case Else
        End Select
    
    End With
    
    With fpsSection
        .LeftMargin = InchesToPoints(0.3 + lAdjust)
        .RightMargin = InchesToPoints(0.3)
        .TopMargin = InchesToPoints(dlgBates.spnTopMargin.Value)
        If g_b125 Then
            .HeaderDistance = InchesToPoints(0.2)
            .FooterDistance = InchesToPoints(0.2)
            .BottomMargin = InchesToPoints(-0.2)
        Else
            .HeaderDistance = InchesToPoints(0.3)
            .FooterDistance = InchesToPoints(0.3)
            .BottomMargin = InchesToPoints(-0.3)
        End If
    End With
    
    rngMain.Characters.Last.Select
    On Error Resume Next
    
    ' ---- Insert and format table
    ActiveDocument.Tables.Add Range:=Selection.Range, _
                              NumRows:=m_iRowCount, _
                              NumColumns:=m_iColumnCount
                                             
    If g_b125 Then
        WordBasic.WW2_TableRowHeight RulerStyle:=1, LeftIndent:="-0.2 in", LineSpacingRule:=2, _
            LineSpacing:="0.41 in", Alignment:=0
        WordBasic.WW2_TableRowHeight RulerStyle:=0, LineSpacingRule:=2, LineSpacing:="0.41 in"
        WordBasic.TableSelectColumn
        WordBasic.WW2_TableColumnWidth ColumnWidth:="1.63 in", SpaceBetweenCols:="0 in", RulerStyle:=1
        WordBasic.WW2_TableColumnWidth NextColumn:=1
        WordBasic.WW2_TableColumnWidth ColumnWidth:="1.63 in", SpaceBetweenCols:="0 in", RulerStyle:=1
        WordBasic.WW2_TableColumnWidth NextColumn:=1
        WordBasic.WW2_TableColumnWidth ColumnWidth:="1.63 in", SpaceBetweenCols:="0 in", RulerStyle:=1
        WordBasic.WW2_TableColumnWidth NextColumn:=1
        WordBasic.WW2_TableColumnWidth ColumnWidth:="1.63 in", SpaceBetweenCols:="0 in", RulerStyle:=1
    Else
        WordBasic.WW2_TableRowHeight RulerStyle:=1, LeftIndent:="-0.2 in", LineSpacingRule:=2, _
            LineSpacing:="36 pt", Alignment:=0
        WordBasic.TableSelectColumn
        WordBasic.WW2_TableColumnWidth ColumnWidth:="2.04 in", SpaceBetweenCols:="0 in", RulerStyle:=1
        WordBasic.WW2_TableColumnWidth NextColumn:=1
        WordBasic.WW2_TableColumnWidth ColumnWidth:="2.06 in", SpaceBetweenCols:="0 in", RulerStyle:=1
        WordBasic.WW2_TableColumnWidth NextColumn:=1
        WordBasic.WW2_TableColumnWidth ColumnWidth:="2.06 in", SpaceBetweenCols:="0 in", RulerStyle:=1
        WordBasic.WW2_TableColumnWidth NextColumn:=1
        WordBasic.WW2_TableColumnWidth ColumnWidth:="2.04 in", SpaceBetweenCols:="0 in", RulerStyle:=1
    End If
        
'---set row alignment
    With oTableRng.Cells
        Select Case m_xRAlignment
            Case "Top"
                .VerticalAlignment = wdCellAlignVerticalTop
            Case "Centered"
                .VerticalAlignment = wdCellAlignVerticalCenter
            Case "Bottom"
                .VerticalAlignment = wdCellAlignVerticalBottom
            Case Else
        End Select
    End With
    
    Set oTable = Selection.Tables(1)

    Set oTableRng = oTable.Range

    'write labels
    WriteLabels oTable

WrapUp:
'---clean up manual page breaks, format
    Application.StatusBar = "Formatting, one moment please"
    
    With ActiveDocument
        With .Styles(wdStyleNormal).Font
            .Name = dlgBates.cmbFonts
            .Size = dlgBates.cmbSize
'---reset all to false before capturing dbox prefs --  otherwise
'---it toggles on restart!
            .AllCaps = False
            .Bold = False
            .SmallCaps = False
            .AllCaps = dlgBates.chkBSAllCaps
            .Bold = dlgBates.chkBSBold
            .SmallCaps = dlgBates.chkBSSmallCaps
            If dlgBates.chkBSUnderlined = 1 Then
                .Underline = wdUnderlineSingle
            Else
                .Underline = wdUnderlineNone
            End If
        End With
        
'           set column alignment ie: set styles
        With ActiveDocument.Styles
            Select Case UCase(dlgBates.cmbAlignment)
                Case "FLUSH RIGHT"
                    .Item("Bates Number").ParagraphFormat.Alignment = wdAlignParagraphRight
                    .Item("Confidential").ParagraphFormat.Alignment = wdAlignParagraphRight
                Case "CENTERED"
                    .Item("Bates Number").ParagraphFormat.Alignment = wdAlignParagraphCenter
                    .Item("Confidential").ParagraphFormat.Alignment = wdAlignParagraphCenter
                Case "FLUSH LEFT"
                    .Item("Bates Number").ParagraphFormat.Alignment = wdAlignParagraphLeft
                    .Item("Confidential").ParagraphFormat.Alignment = wdAlignParagraphLeft
                Case Else
            End Select
        End With
        
    End With
            
    WordBasic.StartOfDocument
        
NukeBreaks:
''        WordBasic.EditFindClearFormatting
''        WordBasic.WW2_EditFind Find:="^d^p", WholeWord:=0, MatchCase:=0, Direction:=2, Format:=0
''        If WordBasic.EditFindFound() Then
''            WordBasic.WW6_EditClear '        nuke page break and paragraph
''            GoTo NukeBreaks '    repeat
''        End If
''        ' Set font size for final para mark to prevent extra
''        ' page caused by font size greater than 12 pt.
''        ActiveDocument.Paragraphs.Last.Range.Font.Size = 2
''    End With    'dlgBates
    
'   finish & cleanup
    bRet = bDeleteAllBookmarks(bMacPacOnly:=True)
    Application.StatusBar = mpMsgFinished
    m_oStatusForm.Hide
    Set m_oStatusForm = Nothing
    EchoOn
    Application.ScreenUpdating = True
    Application.ScreenRefresh
    'FOR TESTING ONLY - performance timing code
    UUDisplayMacroDuration datTestStartTime
    
    bWriteDoc = Err
    Word.ActiveDocument.ActiveWindow.WindowState = wdWindowStateMaximize
    Exit Function
    
bWriteDoc_Error:
    EchoOn
    Select Case Err
        Case Else
            Beep
            MsgBox "Error " & Str(Err) & vbCr & _
                Error(Err), vbExclamation, mpAppName + "(Bates.bWriteDoc)"
            bWriteDoc = Err
    End Select
    Exit Function
End Function

Function bDocCreate(Optional iRestart As Integer = mpRestartReplaceExisting, _
                    Optional bNewProfile As Boolean = False) As Boolean
                    
    Dim dlgBates As New frmBates
    Dim PackedArray As String
    Dim i As Integer
    
    'bDocCreate = False
    
    On Error GoTo bDocCreate_Error

    With dlgBates
        Application.ScreenUpdating = True
        
'        If dbMPPrivate Is Nothing Then _
'            bAppInitialize
        
        
        .Show vbModal
        
'       create doc if finish was selected
'        If .ActiveControl.Name = "cmdFinish" Then
        If .m_bFinished = True Then
'            If Err Then GoTo bDocCreate_Error
            Err.Clear
'           get doc/application environment
            envMpDoc = envGetDocEnvironment()
            envMpApp = envGetAppEnvironment()
                
            Word.ActiveDocument.ActiveWindow.View.Type = wdNormalView

'           store document vars
            SetDocVars dlgBates '.Controls
                
            On Error Resume Next
            ActiveDocument.Variables.Add _
                    Name:="lblSheets", Value:=dlgBates.lblSheets.Caption
            ActiveDocument.Variables("lblSheets") = dlgBates.lblSheets.Caption
            ActiveDocument.Variables.Add _
                    Name:="lblEnding", Value:=dlgBates.lblEnding.Caption
            
            On Error GoTo bDocCreate_Error
        
'           set any sticky fields to user.ini
            Dim xSuffix As String
            Dim xPrefix As String
            xSuffix = .txtSuffix
            xPrefix = .txtPrefix
            
            If xSuffix <> "" Then
                If Left(xSuffix, 1) = " " Then
                    '---use placeholder for lead zero
                    xSuffix = "~" & xSuffix
                End If
            End If
            
            If xPrefix <> "" Then
                If Right(xPrefix, 1) = " " Then
                    '---use placeholder for lead zero
                    xPrefix = xPrefix & "~"
                End If
            End If
            
'           update user ini
            mpbase2.SetUserIni "Bates", "Suffix", xSuffix
            mpbase2.SetUserIni "Bates", "Prefix", xPrefix
            mpbase2.SetUserIni "Bates", "Start", .txtStartNo
            mpbase2.SetUserIni "Bates", "Quantity", .txtNOLabels
            mpbase2.SetUserIni "Bates", "Digits", .spnLeadZero.Value
            mpbase2.SetUserIni "Bates", "TopMargin", .spnTopMargin.Value '*** 9.7.1 - #4061
            mpbase2.SetUserIni "Bates", "Phrase", .txtConfidential
            mpbase2.SetUserIni "Bates", "Alignment", .cmbAlignment
            mpbase2.SetUserIni "Bates", "HorizontalAlignment", .cmbAlignmentH
            mpbase2.SetUserIni "Bates", "Font", .cmbFonts
            mpbase2.SetUserIni "Bates", "Type", .cmbLabelType
            mpbase2.SetUserIni "Bates", "ConfidentialPosition", .cmbCP
            mpbase2.SetUserIni "Bates", "FontSize", .cmbSize
            mpbase2.SetUserIni "Bates", "Bold", .chkBSBold
            mpbase2.SetUserIni "Bates", "SmallCaps", .chkBSSmallCaps
            mpbase2.SetUserIni "Bates", "Underlined", .chkBSUnderlined
            mpbase2.SetUserIni "Bates", "AllCaps", .chkBSAllCaps
            mpbase2.SetUserIni "Bates", "SideBySide", .chkSidebySide
            
''---count windows, stop execution if more than 1
'            If Application.Windows.Count > 1 Then
'                MsgBox "You must close all other documents to run a Bates Labels batch.  Click OK, close all documents except this one and choose Reuse from the MacPac Menu to rerun the template.  All dialog box choices will be preserved.", vbInformation, mpAppName
'                GoTo Reset
'            End If

'---minimize all windows except active one

            On Error Resume Next
            Dim xWindow As String
            Dim xWindowStates() As String
            Dim iWindowCount As Integer
            Dim iIndexCount As Integer
            
            xWindow = Word.ActiveDocument.ActiveWindow.Caption
            iWindowCount = Application.Windows.Count
            
            ReDim xWindowStates((iWindowCount - 1), 1)
            
'---also store each window's window state for end of routine
'---sadly, this code doesn't seem to read a minimized window
            
            With Application.Windows
                For i = .Count To 1 Step -1
'                    xWindowStates(iIndexCount, 0) = .Item(i).Caption
'                    xWindowStates(iIndexCount, 1) = .Item(i).WindowState
'                    iIndexCount = iIndexCount + 1
''''                    .Item(i).Activate
''''                    If .Item(i).Caption <> xWindow Then _
''''                        .Item(i).WindowState = wdWindowStateMinimize
                Next
                                                       
                .Item(xWindow).Activate
            End With

'           pack arrays to be stored as docvars for restart
'           write doc - either replace or
'           append document content
            Select Case iRestart
                Case mpRestartReplaceExisting
                    lRet = bWriteDoc(dlgBates, False)
                Case mpRestartAppend
                    lRet = bWriteDoc(dlgBates, True)
                Case Else
            End Select
            
'---return all windows to original state
            'On Error GoTo 0
            For i = iWindowCount To 1 Step -1
                With Application.Windows
'                    .Item(xWindowStates(i - 1, 0)).Activate
'                    .Item(xWindowStates(i - 1, 0)).WindowState = _
'                            .Item(xWindowStates(i - 1, 1))
''''                    .Item(i).Activate
''''                    .Item(i).WindowState = _
''''                            wdWindowStateMaximize
                End With
            Next
            Application.Windows(xWindow).Activate

'           reset doc/application environment
'           envMpApp/envMpDoc below are global vars
Reset:
            bSetDocEnvironment envMpDoc, True
            bSetAppEnvironment envMpApp
    
            Application.ScreenRefresh
            
'           save if specified
            If bNewProfile Then Application.Run "FileSaveAs"
            
            bDocCreate = lRet
        ElseIf Not g_bRestart Then  'doc was cancelled
            ActiveDocument.Variables.Add "zzmpBatesCancelled", "TRUE"
        End If
        
    End With
    Unload dlgBates
    
    Exit Function

bDocCreate_Error:
    Select Case Err
        Case mpBlockNotSet  'control button cancel
            Unload dlgBates
            Exit Function
        Case 4605   'we're in print preview mode
            Word.ActiveDocument.ActiveWindow.View.Type = wdNormalView
            Resume
        Case Else
            Beep
            MsgBox "Error " & Str(Err) & vbCr & _
                Error(Err), vbExclamation, mpAppName + "(bDocCreate)"
            Unload dlgBates
            bDocCreate = Err
    End Select
End Function

Public Function WriteLabels(ByVal oTable As Word.Table)
    Dim iCellsPerPage As Integer
    Dim iNumCells As Integer
    Dim iPages As Integer
    Dim oTableRng As Word.Range
    Dim xNum As String
    Dim i As Integer
    Dim xCon As String
    Dim iCurNumber As Integer
    Dim iLastNumberInserted As Integer
    Dim xRAlignment As String
    Dim iStatus As Integer
    Dim oRow As Word.Row
    Dim lStatus As Long
    
    iCellsPerPage = m_iRowCount * m_iColumnCount
    iNumCells = (m_iStartRow - 1) * m_iColumnCount + (m_iStartCol - 1) + m_iNumLabels

    If iNumCells Mod iCellsPerPage = 0 Then
        iPages = iNumCells / iCellsPerPage
    ElseIf (iNumCells Mod iCellsPerPage) / iCellsPerPage >= 0.5 Then
        'GLOG : 6377 : CEH - cInt uses Banker's Rounding for 0.5
        'need to use Format function instead
        iPages = Format(iNumCells / iCellsPerPage, "0")
    Else
        iPages = CInt(iNumCells / iCellsPerPage) + 1
    End If

    'copy and paste first page accordingly
    Set oTableRng = oTable.Range
    
    Select Case m_xRAlignment
        Case "Top"
            oTableRng.Cells.VerticalAlignment = wdCellAlignVerticalTop
        Case "Centered"
            oTableRng.Cells.VerticalAlignment = wdCellAlignVerticalCenter
        Case "Bottom"
            oTableRng.Cells.VerticalAlignment = wdCellAlignVerticalBottom
        Case Else
    End Select

    DoEvents
    m_oStatusForm.StatusValue = 10
    If m_bSideBySide Then
        'fill in labels on the first page
        FillLabels oTable
    
        DoEvents
        m_oStatusForm.StatusValue = 25
    
        'copy the table
        oTableRng.Copy
    
        'clear out the first page cells that should be empty
        EmptyStartingCells oTable
    
        'set the start number for the first filled cell
        SetStartNumber oTable
    Else
        'copy the table
        oTableRng.Copy
    End If
    
    DoEvents
    
    m_oStatusForm.StatusValue = 30


    iStatus = m_oStatusForm.StatusValue

    For i = 2 To iPages

        If i = 2 Then
            'paste the table at the end of the first page
            oTableRng.EndOf

            'add a blank row, and paste onto it -
            'this is a workaround for the issue whereby
            'the different styles in each cell get misapplied
            'in the pasted table
            Set oRow = oTableRng.Tables(1).Rows.Add
            oRow.Select
            oTableRng.Paste

            'delete the added empty row
            Set oTableRng = oTableRng.Tables(1).Range
            Set oRow = oTableRng.Tables(1).Rows.Last
            oRow.Delete
        Else
            'add remaining pages
            oTableRng.EndOf
            oTableRng.Paste
            Set oTableRng = oTableRng.Tables(1).Range
        End If
        DoEvents
        lStatus = m_oStatusForm.StatusValue + ((95 - iStatus) / (iPages - 1))
        m_oStatusForm.StatusValue = IIf(lStatus > 100, 100, lStatus)
        If m_oStatusForm.JobCancelled Then
            Exit For
        End If
    Next i

    If m_bSideBySide Then
        If Not m_oStatusForm.JobCancelled Then
            'clear out ending cells that should be empty
            EmptyEndingCells oTable
        End If
    
        'update the fields and unlink
        oTable.Range.Fields.Update
        oTable.Range.Fields.Unlink
    Else
        FillLabelsDown oTable
    End If
    
    m_oStatusForm.StatusValue = 100
    
End Function

Private Function FillLabelsDown(ByVal oTable As Word.Table)
    Dim oRng As Word.Range
    Dim xCode As String
    Dim i As Integer
    
'    xCode = "SEQ BATES \r" + CStr(m_iStartNumber) + " \# """ & String(m_iDigits, "0") & """"
    Set oRng = oTable.Cell(m_iStartRow, m_iStartCol).Range

    For i = 0 To m_iNumLabels - 1
        'insert cell text into first cell
        If m_xConfidentialPhrases <> "" Then
            If UCase(m_xConfidentialPosition) = "ABOVE" Then
                oRng.StartOf
                oRng.InsertAfter m_xConfidentialPhrases
                oRng.Style = "Confidential"
                oRng.InsertParagraphAfter
                oRng.EndOf
                If m_xPrefix <> "" Then
                    oRng.InsertAfter m_xPrefix
                    oRng.EndOf
                End If
                oRng.InsertAfter i + m_iStartNumber
                'GLOG 6705: Make sure range is at end of cell
                Set oRng = oRng.Cells(1).Range
                oRng.EndOf
                oRng.Move wdCharacter, -1
                oRng.InsertAfter m_xSuffix
                oRng.Style = "Bates Number"
            Else
                oRng.StartOf
                If m_xPrefix <> "" Then
                    oRng.InsertAfter m_xPrefix
                    oRng.EndOf
                End If
                oRng.InsertAfter i + m_iStartNumber
                Set oRng = oRng.Cells(1).Range
                oRng.EndOf
                oRng.Move wdCharacter, -1
                oRng.InsertAfter m_xSuffix
                oRng.Style = "Bates Number"
                oRng.InsertParagraphAfter
                oRng.EndOf
                oRng.InsertAfter m_xConfidentialPhrases
                oRng.Style = "Confidential"
            End If
        Else
            oRng.StartOf
            If m_xPrefix <> "" Then
                oRng.InsertAfter m_xPrefix
                oRng.EndOf
            End If
            oRng.InsertAfter i + m_iStartNumber
            Set oRng = oRng.Cells(1).Range
            oRng.EndOf
            oRng.Move wdCharacter, -1
            oRng.InsertAfter m_xSuffix
            oRng.Style = "Bates Number"
        End If
        
        'move to next cell
        If oRng.Information(wdStartOfRangeRowNumber) = oTable.Rows.Count Then
            Set oRng = oTable.Rows(1).Range.Columns(2).Cells(1).Range
        Else
            oRng.Move wdRow
        End If
        
    Next i
    

End Function

Private Function FillLabels(ByVal oTable As Word.Table) As Integer
    Dim i As Integer
    Dim xNum As String
    Dim oCell As Word.Cell
    Dim xCode As String
    Dim oRng As Word.Range
    Dim oColRng As Word.Range

    xCode = "SEQ BATES\# """ & String(m_iDigits, "0") & """"
    
    Set oCell = oTable.Range.Cells(1)
    Set oRng = oCell.Range

    'insert cell text into first cell
    If m_xConfidentialPhrases <> "" Then
        If UCase(m_xConfidentialPosition) = "ABOVE" Then
            oRng.StartOf
            oRng.InsertAfter m_xConfidentialPhrases
            oRng.Style = "Confidential"
            oRng.InsertParagraphAfter
            oRng.EndOf
            If m_xPrefix <> "" Then
                oRng.InsertAfter m_xPrefix
                oRng.EndOf
            End If
            oRng.Fields.Add oRng, , xCode
            Set oRng = oCell.Range
            oRng.EndOf
            oRng.Move wdCharacter, -1
            oRng.InsertAfter m_xSuffix
            oRng.Style = "Bates Number"
            oCell.Range.Copy
        Else
            oRng.StartOf
            If m_xPrefix <> "" Then
                oRng.InsertAfter m_xPrefix
                oRng.EndOf
            End If
            oRng.Fields.Add oRng, , xCode
            Set oRng = oCell.Range
            oRng.EndOf
            oRng.Move wdCharacter, -1
            oRng.InsertAfter m_xSuffix
            oRng.Style = "Bates Number"
            oRng.InsertParagraphAfter
            oRng.EndOf
            oRng.InsertAfter m_xConfidentialPhrases
            oRng.Style = "Confidential"
            oCell.Range.Copy
        End If
    Else
        oRng.StartOf
        If m_xPrefix <> "" Then
            oRng.InsertAfter m_xPrefix
            oRng.EndOf
        End If
        oRng.Fields.Add oRng, , xCode
        Set oRng = oCell.Range
        oRng.EndOf
        oRng.Move wdCharacter, -1
        oRng.InsertAfter m_xSuffix
        oCell.Range.Style = "Bates Number"
        oCell.Range.Copy
    End If

    For i = 2 To oTable.Columns.Count
        Set oCell = oTable.Range.Cells(i)
        oCell.Range.Paste
    Next i

    oTable.Rows(1).Range.Copy

    For i = 2 To oTable.Rows.Count
        oTable.Rows(i).Range.Paste
    Next i

    Set oRng = oTable.Rows(i).Range
    oRng.MoveEnd wdTable
    oRng.Rows.Delete

End Function

Private Function EmptyStartingCells(ByVal oTable As Word.Table)
    Dim oRng As Word.Range
    Dim i As Integer
    Dim j As Integer
    Dim oCell As Word.Cell

    If Not (m_iStartCol = 1 And m_iStartRow = 1) Then
        Set oRng = oTable.Cell(m_iStartRow, m_iStartCol).Previous.Range

        oRng.MoveStart wdRow, -1
        oRng.Delete

        If m_iStartRow > 1 Then
            Set oRng = oTable.Rows(m_iStartRow - 1).Range
            oRng.MoveStart wdTable, -1
            oRng.Delete
        End If
    End If
End Function

Private Function EmptyEndingCells(ByVal oTable As Word.Table)
    Dim iLastCellIndex As Integer
    Dim oRng As Word.Range
    Dim i As Integer
    Dim j As Integer
    Dim oCell As Word.Cell

    'find ending cell
    iLastCellIndex = m_iNumLabels + (m_iStartCol - 1 + (m_iStartRow - 1) * oTable.Columns.Count)

    If iLastCellIndex < oTable.Range.Cells.Count Then
        'get first cell to clear
        Set oCell = oTable.Range.Cells(iLastCellIndex + 1)

        Set oRng = oCell.Range
        oRng.MoveEnd wdRow, 1
        oRng.Delete

        Set oRng = oTable.Rows(oCell.RowIndex + 1).Range
        oRng.MoveEnd wdTable
        oRng.Delete
    End If
End Function

Private Function SetStartNumber(ByVal oTable As Word.Table)
    Dim oRng As Word.Range
    Dim xCode As String

    xCode = "SEQ BATES \r" + CStr(m_iStartNumber) + " \# """ & String(m_iDigits, "0") & """"
    Set oRng = oTable.Cell(m_iStartRow, m_iStartCol).Range
    oRng.Delete

    'insert cell text into first cell
    If m_xConfidentialPhrases <> "" Then
        If UCase(m_xConfidentialPosition) = "ABOVE" Then
            oRng.StartOf
            oRng.InsertAfter m_xConfidentialPhrases
            oRng.Style = "Confidential"
            oRng.InsertParagraphAfter
            oRng.EndOf
            If m_xPrefix <> "" Then
                oRng.InsertAfter m_xPrefix
                oRng.EndOf
            End If
            oRng.Fields.Add oRng, , xCode
            'GLOG 6705: Make sure range is at end of cell
            Set oRng = oRng.Cells(1).Range
            oRng.EndOf
            oRng.Move wdCharacter, -1
            oRng.InsertAfter m_xSuffix
            oRng.Style = "Bates Number"
        Else
            oRng.StartOf
            If m_xPrefix <> "" Then
                oRng.InsertAfter m_xPrefix
                oRng.EndOf
            End If
            oRng.Fields.Add oRng, , xCode
            Set oRng = oRng.Cells(1).Range
            oRng.EndOf
            oRng.Move wdCharacter, -1
            oRng.InsertAfter m_xSuffix
            oRng.Style = "Bates Number"
            oRng.InsertParagraphAfter
            oRng.EndOf
            oRng.InsertAfter m_xConfidentialPhrases
            oRng.Style = "Confidential"
        End If
    Else
        oRng.StartOf
        If m_xPrefix <> "" Then
            oRng.InsertAfter m_xPrefix
            oRng.EndOf
        End If
        oRng.Fields.Add oRng, , xCode
        Set oRng = oRng.Cells(1).Range
        oRng.EndOf
        oRng.Move wdCharacter, -1
        oRng.InsertAfter m_xSuffix
        oRng.Style = "Bates Number"
    End If

End Function


Public Sub DebugOutput(ByVal OutputString As String)
     
    Call OutputDebugString(OutputString)
     
End Sub

