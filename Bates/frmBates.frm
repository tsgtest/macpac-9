VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{1FD322CE-1996-4D30-91BC-D225C9668D1B}#1.1#0"; "mpControls3.ocx"
Begin VB.Form frmBates 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create Bates Labels"
   ClientHeight    =   6312
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   4980
   Icon            =   "frmBates.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6312
   ScaleWidth      =   4980
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnHelp 
      Caption         =   "Help"
      Height          =   370
      Left            =   195
      TabIndex        =   49
      Top             =   5865
      Width           =   840
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "&Finish"
      Height          =   370
      Left            =   3056
      TabIndex        =   47
      ToolTipText     =   "Click to create Bates Labels"
      Top             =   5850
      Width           =   840
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   370
      Left            =   3956
      TabIndex        =   48
      Top             =   5850
      Width           =   840
   End
   Begin TabDlg.SSTab MultiPage1 
      Height          =   5715
      Left            =   60
      TabIndex        =   50
      TabStop         =   0   'False
      ToolTipText     =   "##set by mouse move"
      Top             =   45
      Width           =   4875
      _ExtentX        =   8594
      _ExtentY        =   10075
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&1-Details"
      TabPicture(0)   =   "frmBates.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblCP"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblSuffix"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "lblStartingLabel"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "lblConfidential"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "lblStartingCol"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "lblPrefix"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "lblNOLabels"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "lblStartNo"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmbCP"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "spnStartingColumn"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "pnlSummary"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "txtPrefix"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "txtSuffix"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "txtConfidential"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "spnStartingLabel"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "chkSidebySide"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "txtNOLabels"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "txtStartNo"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).ControlCount=   18
      TabCaption(1)   =   "&2-Format"
      TabPicture(1)   =   "frmBates.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "pnlCharacter"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "pnlOptions"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "pnlType"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).ControlCount=   3
      Begin VB.TextBox txtStartNo 
         Appearance      =   0  'Flat
         Height          =   360
         Left            =   1920
         TabIndex        =   1
         Top             =   465
         Width           =   1560
      End
      Begin VB.TextBox txtNOLabels 
         Appearance      =   0  'Flat
         Height          =   360
         Left            =   1920
         TabIndex        =   3
         Top             =   915
         Width           =   1560
      End
      Begin VB.CheckBox chkSidebySide 
         Appearance      =   0  'Flat
         Caption         =   "Fill si&de by side"
         ForeColor       =   &H80000008&
         Height          =   437
         Left            =   2025
         TabIndex        =   12
         Top             =   2790
         Visible         =   0   'False
         Width           =   1500
      End
      Begin mpControls3.SpinTextInternational spnStartingLabel 
         Height          =   360
         Left            =   1920
         TabIndex        =   17
         Top             =   3795
         Width           =   795
         _ExtentX        =   1397
         _ExtentY        =   635
         MinValue        =   1
         MaxValue        =   20
         Value           =   1
      End
      Begin VB.Frame pnlCharacter 
         Caption         =   "Character"
         Height          =   2280
         Left            =   -74790
         TabIndex        =   13
         Top             =   525
         Width           =   4395
         Begin VB.CheckBox chkBSAllCaps 
            Appearance      =   0  'Flat
            Caption         =   "&All Caps"
            ForeColor       =   &H80000008&
            Height          =   437
            Left            =   564
            TabIndex        =   33
            Top             =   930
            Width           =   975
         End
         Begin VB.CheckBox chkBSSmallCaps 
            Appearance      =   0  'Flat
            Caption         =   "&Small Caps"
            ForeColor       =   &H80000008&
            Height          =   405
            Left            =   564
            TabIndex        =   34
            Top             =   1395
            Width           =   1126
         End
         Begin VB.CheckBox chkBSBold 
            Appearance      =   0  'Flat
            Caption         =   "&Bold Text"
            ForeColor       =   &H80000008&
            Height          =   437
            Left            =   2586
            TabIndex        =   35
            Top             =   930
            Width           =   1126
         End
         Begin VB.CheckBox chkBSUnderlined 
            Appearance      =   0  'Flat
            Caption         =   "&Underlined"
            ForeColor       =   &H80000008&
            Height          =   405
            Left            =   2586
            TabIndex        =   36
            Top             =   1395
            Width           =   1126
         End
         Begin MSForms.ComboBox cmbSize 
            Height          =   345
            Left            =   3150
            TabIndex        =   32
            Top             =   345
            Width           =   765
            BorderStyle     =   1
            DisplayStyle    =   7
            Size            =   "1349;609"
            MatchEntry      =   1
            ShowDropButtonWhen=   2
            DropButtonStyle =   3
            Value           =   "10"
            SpecialEffect   =   0
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
         End
         Begin MSForms.ComboBox cmbFonts 
            Height          =   345
            Left            =   570
            TabIndex        =   30
            Top             =   345
            Width           =   2100
            BorderStyle     =   1
            DisplayStyle    =   7
            Size            =   "3704;609"
            MatchEntry      =   1
            ShowDropButtonWhen=   2
            DropButtonStyle =   3
            SpecialEffect   =   0
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
         End
         Begin VB.Label lblFSize 
            AutoSize        =   -1  'True
            Caption         =   "Si&ze:"
            Height          =   210
            Left            =   2775
            TabIndex        =   31
            Top             =   405
            Width           =   360
         End
         Begin VB.Label lblfonts 
            Caption         =   "Fo&nt:"
            Height          =   225
            Left            =   150
            TabIndex        =   29
            Top             =   405
            Width           =   450
         End
      End
      Begin VB.Frame pnlOptions 
         Caption         =   "Position"
         Height          =   2520
         Left            =   -74790
         TabIndex        =   39
         Top             =   3000
         Width           =   4395
         Begin mpControls3.SpinTextInternational spnLeadZero 
            Height          =   345
            Left            =   2655
            TabIndex        =   43
            Top             =   1440
            Width           =   945
            _ExtentX        =   1672
            _ExtentY        =   614
            MaxValue        =   100
            Value           =   4
         End
         Begin mpControls3.SpinTextInternational spnTopMargin 
            Height          =   345
            Left            =   2655
            TabIndex        =   46
            Top             =   1950
            Width           =   945
            _ExtentX        =   1672
            _ExtentY        =   614
            IncrementValue  =   0.1
            MaxValue        =   1
            AppendSymbol    =   -1  'True
            Value           =   0.5
            ValueUnit       =   0
            DisplayUnit     =   0
         End
         Begin MSForms.ComboBox cmbAlignment 
            Height          =   346
            Left            =   2340
            TabIndex        =   41
            Top             =   751
            Width           =   1650
            VariousPropertyBits=   679479323
            BorderStyle     =   1
            DisplayStyle    =   7
            Size            =   "2910;610"
            ColumnCount     =   2
            cColumnInfo     =   2
            MatchEntry      =   1
            ShowDropButtonWhen=   2
            DropButtonStyle =   3
            SpecialEffect   =   0
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
            Object.Width           =   "705;0"
         End
         Begin MSForms.ComboBox cmbAlignmentH 
            Height          =   346
            Left            =   2340
            TabIndex        =   38
            Top             =   299
            Width           =   1650
            VariousPropertyBits=   679479323
            BorderStyle     =   1
            DisplayStyle    =   7
            Size            =   "2910;610"
            ColumnCount     =   2
            cColumnInfo     =   2
            MatchEntry      =   1
            ShowDropButtonWhen=   2
            DropButtonStyle =   3
            SpecialEffect   =   0
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
            Object.Width           =   "705;0"
         End
         Begin VB.Label lblLeadZeros 
            AutoSize        =   -1  'True
            Caption         =   "Use &Leading Zeros to Force:   "
            Height          =   210
            Left            =   225
            TabIndex        =   42
            Top             =   1455
            Width           =   2205
         End
         Begin VB.Label lblTopMargin 
            Caption         =   "Ad&just label sheet top margin to:"
            Height          =   390
            Left            =   225
            TabIndex        =   45
            Top             =   1965
            Width           =   2355
         End
         Begin VB.Label lblLeadZero2 
            Caption         =   "Digits"
            Height          =   225
            Left            =   3690
            TabIndex        =   44
            Top             =   1470
            Width           =   435
         End
         Begin VB.Label lblAlignment 
            Caption         =   "Label Alignment (Ro&w):"
            Height          =   225
            Left            =   225
            TabIndex        =   37
            Top             =   359
            Width           =   1947
         End
         Begin VB.Label lblAlignmentH 
            Caption         =   "Label Alignment (C&olumn):"
            Height          =   225
            Left            =   225
            TabIndex        =   40
            Top             =   810
            Width           =   2112
         End
      End
      Begin VB.TextBox txtConfidential 
         Appearance      =   0  'Flat
         Height          =   360
         Left            =   1920
         TabIndex        =   9
         Top             =   2355
         Width           =   1844
      End
      Begin VB.TextBox txtSuffix 
         Appearance      =   0  'Flat
         Height          =   360
         Left            =   1920
         TabIndex        =   7
         Top             =   1875
         Width           =   1560
      End
      Begin VB.TextBox txtPrefix 
         Appearance      =   0  'Flat
         Height          =   360
         Left            =   1920
         TabIndex        =   5
         Top             =   1395
         Width           =   1560
      End
      Begin VB.Frame pnlSummary 
         Height          =   1441
         Left            =   60
         TabIndex        =   22
         Top             =   4185
         Width           =   4725
         Begin VB.Label lblLastLabel 
            Caption         =   "Ending Bates Number:"
            Height          =   228
            Left            =   132
            TabIndex        =   23
            Top             =   192
            Width           =   1860
         End
         Begin VB.Label lblEnding 
            Height          =   225
            Left            =   2251
            TabIndex        =   24
            Top             =   194
            Width           =   2010
         End
         Begin VB.Label lblSheets 
            Height          =   225
            Left            =   2251
            TabIndex        =   25
            Top             =   749
            Width           =   1995
         End
         Begin VB.Label lblAvery 
            AutoSize        =   -1  'True
            Caption         =   "Avery #5267 Laser  Label Sheets Required:"
            Height          =   420
            Left            =   120
            TabIndex        =   26
            Top             =   552
            Width           =   1872
            WordWrap        =   -1  'True
         End
         Begin VB.Label lblTime 
            Height          =   225
            Left            =   2251
            TabIndex        =   27
            Top             =   1079
            Width           =   1995
         End
         Begin VB.Label lblTR 
            Caption         =   "Time Required:"
            Height          =   324
            Left            =   120
            TabIndex        =   28
            Top             =   1080
            Width           =   1836
         End
      End
      Begin mpControls3.SpinTextInternational spnStartingColumn 
         Height          =   360
         Left            =   1920
         TabIndex        =   15
         Top             =   3285
         Width           =   795
         _ExtentX        =   1397
         _ExtentY        =   635
         MinValue        =   1
         MaxValue        =   4
         Value           =   1
      End
      Begin VB.Frame pnlType 
         Caption         =   "Type"
         Height          =   660
         Left            =   -74790
         TabIndex        =   19
         Top             =   435
         Visible         =   0   'False
         Width           =   4365
         Begin VB.Label lblLabels 
            Caption         =   "Lab&els per Page:"
            Height          =   285
            Left            =   135
            TabIndex        =   20
            Top             =   255
            Visible         =   0   'False
            Width           =   1260
         End
         Begin MSForms.ComboBox cmbLabelType 
            Height          =   345
            Left            =   1410
            TabIndex        =   21
            Top             =   195
            Visible         =   0   'False
            Width           =   1245
            DisplayStyle    =   7
            Size            =   "2196;609"
            ListRows        =   3
            MatchEntry      =   1
            ShowDropButtonWhen=   2
            DropButtonStyle =   3
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
         End
      End
      Begin MSForms.ComboBox cmbCP 
         Height          =   360
         Left            =   3855
         TabIndex        =   11
         Top             =   2355
         Width           =   915
         VariousPropertyBits=   679479323
         BorderStyle     =   1
         DisplayStyle    =   7
         Size            =   "1616;635"
         MatchEntry      =   0
         ShowDropButtonWhen=   2
         DropButtonStyle =   3
         SpecialEffect   =   0
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin VB.Label lblStartNo 
         Caption         =   "&Starting Bates Number:"
         Height          =   225
         Left            =   195
         TabIndex        =   0
         Top             =   540
         Width           =   1695
      End
      Begin VB.Label lblNOLabels 
         Caption         =   "&Number of Labels:"
         Height          =   225
         Left            =   195
         TabIndex        =   2
         Top             =   990
         Width           =   1695
      End
      Begin VB.Label lblPrefix 
         Caption         =   "Bates &Prefix:"
         Height          =   225
         Left            =   195
         TabIndex        =   4
         Top             =   1485
         Width           =   1440
      End
      Begin VB.Label lblStartingCol 
         Caption         =   "Startin&g Column (1-4):"
         Height          =   285
         Left            =   195
         TabIndex        =   14
         Top             =   3345
         Width           =   1680
      End
      Begin VB.Label lblConfidential 
         Caption         =   "&Confidential Phrase:"
         Height          =   225
         Left            =   195
         TabIndex        =   8
         Top             =   2430
         Width           =   1755
      End
      Begin VB.Label lblStartingLabel 
         Caption         =   "S&tarting Label (1-20):"
         Height          =   225
         Left            =   195
         TabIndex        =   16
         Top             =   3855
         Width           =   1695
      End
      Begin VB.Label lblSuffix 
         Caption         =   "Bates Suffi&x:"
         Height          =   225
         Left            =   195
         TabIndex        =   6
         Top             =   1980
         Width           =   1440
      End
      Begin VB.Label lblCP 
         Caption         =   "P&osition:"
         Height          =   225
         Left            =   3885
         TabIndex        =   10
         Top             =   2145
         Width           =   885
      End
   End
   Begin VB.PictureBox picCycleTab1 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   200
      Left            =   400
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   18
      Top             =   5805
      Width           =   200
   End
   Begin VB.PictureBox picCycleTab2 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   200
      Left            =   600
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   51
      Top             =   5805
      Width           =   200
   End
End
Attribute VB_Name = "frmBates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim bFormInit As Boolean
Public m_bFinished As Boolean
Private vbControls() As String


Private Sub btnCancel_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    btnCancel_Click
End Sub

Private Sub btnHelp_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    btnHelp_Click
End Sub

Private Sub chkSidebySide_Click()
    If g_b125 Then
'        If Me.chkSidebySide Then
            Me.lblStartingCol.Caption = "Startin&g Row (1-25):"
            Me.spnStartingColumn.MaxValue = 25
            Me.lblStartingLabel.Caption = "S&tarting Label (1-5):"
            Me.spnStartingLabel.MaxValue = 5
'        Else
'            Me.lblStartingCol.Caption = "Startin&g Column (1-5):"
'            Me.spnStartingColumn.MaxValue = 5
'            Me.lblStartingLabel.Caption = "S&tarting Label (1-25):"
'            Me.spnStartingLabel.MaxValue = 25
'        End If
    Else
'        If Me.chkSidebySide Then
            Me.lblStartingCol.Caption = "Startin&g Row (1-20):"
            Me.spnStartingColumn.MaxValue = 20
            Me.lblStartingLabel.Caption = "S&tarting Label (1-4):"
            Me.spnStartingLabel.MaxValue = 4
'        Else
'            Me.lblStartingCol.Caption = "Startin&g Column (1-4):"
'            Me.spnStartingColumn.MaxValue = 4
'            Me.lblStartingLabel.Caption = "S&tarting Label (1-20):"
'            Me.spnStartingLabel.MaxValue = 20
'        End If
    End If
    Me.spnStartingLabel.Value = 1
    Me.spnStartingColumn.Value = 1
End Sub

Private Sub cmbLabelType_Change()
    g_b125 = (cmbLabelType.Text Like "*125*")
    chkSidebySide_Click
End Sub

Private Sub cmdFinish_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    cmdFinish_Click
End Sub

Private Sub MultiPage1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim xTip As String
    If (0 < x And x < 930) And y < 300 Then
        xTip = "Set label range, prefix, suffix and starting position"
    ElseIf (930 < x And x < 1725) And y < 300 Then
        xTip = "Set formatting, font attributes"
    End If
    MultiPage1.ToolTipText = xTip

End Sub

Private Sub picCycleTab2_GotFocus()
    CycleTabs 2, vbControls()
End Sub
Private Sub picCycleTab1_GotFocus()
    CycleTabs 1, vbControls()
End Sub
Private Sub cmbSize_KeyDown(KeyCode As MSForms.ReturnInteger, Shift As Integer)
    bCycleComboBox Me.cmbSize, KeyCode, Shift
End Sub
Private Sub cmbFonts_KeyDown(KeyCode As MSForms.ReturnInteger, Shift As Integer)
    bCycleComboBox Me.cmbFonts, KeyCode, Shift
End Sub
Private Sub cmbLabeltype_KeyDown(KeyCode As MSForms.ReturnInteger, Shift As Integer)
    bCycleComboBox Me.cmbLabelType, KeyCode, Shift
End Sub
Private Sub cmbAlignmentH_KeyDown(KeyCode As MSForms.ReturnInteger, Shift As Integer)
    bCycleComboBox Me.cmbAlignmentH, KeyCode, Shift
End Sub
Private Sub cmbAlignment_KeyDown(KeyCode As MSForms.ReturnInteger, Shift As Integer)
    bCycleComboBox Me.cmbAlignment, KeyCode, Shift
End Sub
Private Sub cmbCP_KeyDown(KeyCode As MSForms.ReturnInteger, Shift As Integer)
    bCycleComboBox Me.cmbCP, KeyCode, Shift
End Sub
Private Sub cmbSize_LostFocus()
    VBACtlLostFocus Me.cmbSize, vbControls()
End Sub
Private Sub cmbFonts_LostFocus()
    VBACtlLostFocus Me.cmbFonts, vbControls()
End Sub
Private Sub cmbLabelType_LostFocus()
    VBACtlLostFocus Me.cmbLabelType, vbControls()
End Sub
'Private Sub spnFSize_LostFocus()
'    VBACtlLostFocus Me.spnFSize, vbControls()
'End Sub
Private Sub cmbAlignmentH_LostFocus()
    VBACtlLostFocus Me.cmbAlignmentH, vbControls()
End Sub
Private Sub cmbAlignment_LostFocus()
    VBACtlLostFocus Me.cmbAlignment, vbControls()
End Sub

Private Sub spnLeadZero_Validate(Cancel As Boolean)
    With Me.spnLeadZero
        If Not .IsValid(False) Or _
                (.Value <> CLng(.Value)) Then
            MsgBox "The value must be a whole number between " & _
                .MinValue & " and " & .MaxValue & ".", vbExclamation, _
                "MacPac 9"
            Cancel = True
        End If
    End With
End Sub

Private Sub spnStartingColumn_Change()
    bDisplay
End Sub

Private Sub spnStartingColumn_Validate(Cancel As Boolean)
    With Me.spnStartingColumn
        If Not .IsValid(False) Or _
                (.Value <> CLng(.Value)) Then
            MsgBox "The value must be a whole number between " & _
                .MinValue & " and " & .MaxValue & ".", vbExclamation, _
                "MacPac 9"
            Cancel = True
        End If
    End With
End Sub

Private Sub spnStartingLabel_Change()
    bDisplay
End Sub

Private Sub spnStartingLabel_Validate(Cancel As Boolean)
    With Me.spnStartingLabel
        If Not .IsValid(False) Or _
                (.Value <> CLng(.Value)) Then
            MsgBox "The value must be a whole number between " & _
                .MinValue & " and " & .MaxValue & ".", vbExclamation, _
                "MacPac 9"
            Cancel = True
        End If
    End With
End Sub

Private Sub spnTopMargin_LostFocus()
    VBACtlLostFocus Me.spnTopMargin, vbControls()
End Sub
Private Sub spnLeadZero_LostFocus()
    VBACtlLostFocus Me.spnLeadZero, vbControls()
End Sub
Private Sub cmbCP_LostFocus()
    VBACtlLostFocus Me.cmbCP, vbControls()
End Sub
Private Sub spnStartingLabel_LostFocus()
    VBACtlLostFocus Me.spnStartingLabel, vbControls()
End Sub
Private Sub spnStartingColumn_LostFocus()
    VBACtlLostFocus Me.spnStartingColumn, vbControls()
End Sub
Private Sub MultiPage1_Click(PreviousTab As Integer)
    If Not g_ActivatingTabProgrammatically Then
        DisableInactiveTabs Me
        SelectFirstCtlOnTab Me.MultiPage1, vbControls()
    End If
End Sub

Private Sub spnTopMargin_Validate(Cancel As Boolean)
    If Not Me.spnTopMargin.IsValid Then _
        Cancel = True
End Sub

'Private Sub txtFSize_GotFocus()
'    bEnsureSelectedContent Me.txtFSize
'End Sub
'
'Private Sub txtFSize_KeyPress(KeyAscii As Integer)
'    bRet = bIsInteger(Chr(KeyAscii))
'    If bRet = False Then KeyAscii = 0
'
'End Sub

Private Sub txtStartNo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 8 Then Exit Sub
    bRet = bIsInteger(Chr(KeyAscii))
    If bRet = False Then KeyAscii = 0

End Sub

Private Sub txtStartNo_LostFocus()
    VBACtlLostFocus Me.txtStartNo, vbControls()
End Sub

Private Sub txtConfidential_GotFocus()
    bEnsureSelectedContent Me.txtConfidential
End Sub
Private Sub txtSuffix_GotFocus()
    bEnsureSelectedContent Me.txtSuffix
End Sub
Private Sub txtPrefix_GotFocus()
    bEnsureSelectedContent Me.txtPrefix
End Sub
Private Sub txtNOLabels_GotFocus()
    bEnsureSelectedContent Me.txtNOLabels
End Sub
Private Sub txtStartNo_GotFocus()
    bEnsureSelectedContent Me.txtStartNo
End Sub

Function bDisplay() As Boolean
    Dim iStart As Long
    Dim iEnd As Long
    Dim iNum As Long
    Dim iSheet As Long
    Dim dSheet As Double
    Dim iMod As Long
    Dim iCol As Long
    Dim iTime As Long
    Dim xMins As String
    Dim iRow As Long
    
    Dim ctlButton As Control
    
    If bFormInit = True Then Exit Function
    On Error Resume Next
    iStart = Val(txtStartNo)
    iNum = Val(txtNOLabels)
    
'    If Me.chkSidebySide Then
        iRow = Me.spnStartingColumn.Value
'    Else
'        iCol = Me.spnStartingColumn.Value
'    End If
    
    iCol = Me.spnStartingColumn.Value
    
    If iStart >= 0 Then
        If iNum > 0 Then
            iEnd = (iStart + iNum) - 1
                Me.lblEnding.Caption = _
                    Me.txtPrefix & String(Me.spnLeadZero.Value - _
                    Len(Trim(Str(iEnd))), "0") & Trim(Str(iEnd)) & _
                    Me.txtSuffix
        
'---calculate no of sheets required
                If g_b125 Then
'                    If Me.chkSidebySide Then
                        If iRow > 0 Then
                            iNum = iNum + (Me.spnStartingLabel.Value + (5 * (iRow - 1)))
                        End If
                        dSheet = iNum / 125
                        iSheet = iNum / 125
                        
                        If iNum < 126 Then
                            iSheet = 1
                        Else
                            If dSheet - iSheet > 0 Then
                                iSheet = iSheet + 1
                            End If
                        End If
'                    Else
'                        If iCol > 0 Then
'                            iNum = iNum + (25 * (iCol - 1)) + _
'                                Me.spnStartingLabel.Value - 1
'                        End If
'
'                        dSheet = iNum / 125
'                        iSheet = iNum / 125
'
'                        If iNum < 126 Then
'                            iSheet = 1
'                        Else
'                            If dSheet - iSheet > 0 Then
'                                iSheet = iSheet + 1
'                            End If
'                        End If
'                    End If
                Else
'                    If Me.chkSidebySide Then
                        If iRow > 0 Then
                            iNum = iNum + (Me.spnStartingLabel.Value + (4 * (iRow - 1)))
                        End If
                        dSheet = iNum / 80
                        iSheet = iNum / 80
                        
                        If iNum < 81 Then
                            iSheet = 1
                        Else
                            If dSheet - iSheet > 0 Then
                                iSheet = iSheet + 1
                            End If
                        End If
'                    Else
'                        If iCol > 0 Then
'                            iNum = iNum + (20 * (iCol - 1)) + _
'                                Me.spnStartingLabel.Value - 1
'                        End If
'
'                        dSheet = iNum / 80
'                        iSheet = iNum / 80
'
'                        If iNum < 81 Then
'                            iSheet = 1
'                        Else
'                            If dSheet - iSheet > 0 Then
'                                iSheet = iSheet + 1
'                            End If
'                        End If
'                    End If
                End If
                
                lblSheets.Caption = iSheet

'---calculate time (formula is a guess!)
                iTime = iNum / 2000
                If iTime = 1 Then
                    xMins = " minute"
                Else
                    xMins = " minutes"
                End If
                lblTime.Caption = "Less than" & Str(iTime) & xMins
        
        End If
    End If

End Function
Function bIsInteger(xKey As String) As Boolean
    
    If InStr("0123456789", xKey) = 0 Then
        MsgBox "Numeric input only.", vbCritical, mpAppName
        bIsInteger = False
    Else
        bIsInteger = True
    End If



End Function
Private Sub btnCancel_Click()
    bReleaseVShift
    Unload Me
End Sub

Private Sub btnHelp_Click()
    Dim message, helpheading
    Select Case MultiPage1.Tab
        Case 0      '---first tab
            helpheading = "Setting up a Bates Labels Batch"
            message = "Enter a starting number for your Bates labels sequence, then enter the total number of labels you wish to create.  You can create a maximum of 5000 labels per batch." & _
                      String(2, vbCr) & "Note that it is usually faster to create several smaller batches of labels than to run one large batch." & _
                      String(2, vbCr) & "Enter a Bates prefix or suffix, if desired, including padding spaces." & _
                      "  Enter any confidential phrase to be included in position selected on the options tab." & _
                      "  There is a limit of twenty characters for the phrase." & _
                      String(2, vbCr) & "You can print using partially used sheets of label stock by specifying a starting column, and/or a starting row for your first page of Bates labels."
        Case Else
            helpheading = "Setting Format Options for a Bates Labels Batch"
            message = "Select a font and font size for your labels, along with any font attributes." & _
                      "  You can also select alignment for your labels." & _
                      "  Note that using either flush left and right alignment may result in cut off labels in the left or right margins of each sheet." & _
                      String(2, vbCr) & "Each label will be created with the number in Bates style, and the confidential phrase in the Confidential Phrase style." & _
                      "  You can use Word's Format, Style commands to assign different attributes to each line of the labels." & String(2, vbCr) & _
                      " Select a length for each Bates number, to be padded by leading zeros." & _
                      "  For example, if you specify 5 digits, the first number of the sequence will be 00001, the hundredth number will be 00100, etc." & _
                      String(2, vbCr) & "Click the spin button to adjust the top margin so that the labels will align correctly on your printer." & _
                      "  Note that this setting will be used as the default setting for subsequent sets of labels."
        End Select
    
    Call HelpBalloon(helpheading, message)

End Sub

Private Sub chkBSAllCaps_Click()
    If chkBSAllCaps.Value = 1 Then _
        chkBSSmallCaps.Value = 0
End Sub

Private Sub chkBSSmallCaps_Click()
    If chkBSSmallCaps.Value = 1 Then _
        chkBSAllCaps.Value = 0
End Sub

Private Sub cmdFinish_Click()
    Dim xText As String
    
    Dim bNotValid As Boolean
            
    If Me.txtStartNo = "" Then
        xText = "You must supply a valid starting number."
        MsgBox xText, vbCritical, mpAppName
        MultiPage1.Tab = 0
        txtStartNo.SetFocus
        Exit Sub
    ElseIf Me.txtNOLabels = "" Then
        xText = "You must specify a valid number of labels to create."
        MsgBox xText, vbCritical, mpAppName
        MultiPage1.Tab = 0
        txtNOLabels.SetFocus
        Exit Sub
    End If
    
    m_bFinished = True
    bReleaseVShift
    Me.Hide
    DoEvents

End Sub


'Private Sub obtn1_Click()
'    bDisplay
'End Sub
'
'Private Sub obtn2_Click()
'    bDisplay
'End Sub
'
'Private Sub obtn3_Click()
'    bDisplay
'End Sub
'
'Private Sub obtn4_Click()
'    bDisplay
'End Sub

'Private Sub spnFSize_Change()
'    If spnFSize.Value > 9 And spnFSize.Value < 15 Then _
'        Me.txtFSize = spnFSize.Value
'        With spnFSize
'            If .Value > 14 Then
'                .Value = 14
'            ElseIf .Value < 10 Then
'                .Value = 10
'            End If
'
'        End With
'End Sub

Private Sub spnLeadZero_Change()
    bDisplay
End Sub

Private Sub txtConfidential_Change()
    If bFormInit Then Exit Sub
'*************************************
'per log item 1816 - df
    If Len(txtConfidential) = 30 Then
        MsgBox "Thirty character limit for confidential phrase.", vbInformation, mpAppName
        txtConfidential = Left(txtConfidential, 30)
        txtConfidential.SetFocus
    End If
'*************************************
End Sub

'Private Sub txtFSize_Change()
'    Me.spnFSize.Value = Val(txtFSize)
'
'End Sub

Private Sub txtNOLabels_Change()
    If Val(txtNOLabels) > 5000 Then
        MsgBox "You can create a maximum of 5000 labels.", vbInformation, mpAppName
        txtNOLabels = ""
        txtNOLabels.SetFocus
        Exit Sub
    End If
    bDisplay
End Sub

Private Sub txtNOLabels_KeyPress(KeyAscii As Integer)
    If KeyAscii = 8 Then Exit Sub
    bRet = bIsInteger(Chr(KeyAscii))
    If bRet = False Then
        KeyAscii = 0
    Else
    End If
End Sub

Private Sub txtPrefix_Change()
    bDisplay
End Sub

Private Sub txtStartNo_Change()
    bDisplay
End Sub
'Private Sub txtStartNo_KeyPress(KeyAscii As Integer)
'    bRet = bIsInteger(Chr(KeyAscii))
'    If bRet = False Then KeyAscii = 0
'End Sub

Private Sub txtSuffix_Change()
    bDisplay
End Sub

Private Sub Form_Load()

    Dim xAlignments() As String
    Dim xAlignmentsH() As String
    Dim xPosition() As String
    Dim xLabelType() As String
    Dim xFSize() As String
    Dim iRetry As Integer
    
    Me.m_bFinished = False
    GetTabOrder Me, vbControls()
    DisableInactiveTabs Me
    
    Application.StatusBar = mpMsgInitializing
    bFormInit = True
'---reload global arrays if link
'---dropped for any reason
    On Error GoTo ReInitialize

'---get template specific lists
    ReDim xAlignments(2, 1)
        xAlignments(0, 0) = "Centered"
        xAlignments(0, 1) = wdAlignParagraphCenter
        xAlignments(1, 0) = "Flush Left"
        xAlignments(1, 1) = wdAlignParagraphLeft
        xAlignments(2, 0) = "Flush Right"
        xAlignments(2, 1) = wdAlignParagraphRight
    
    ReDim xAlignmentsH(2, 1)
        xAlignmentsH(0, 0) = "Centered"
        xAlignmentsH(0, 1) = wdAlignVerticalCenter
        xAlignmentsH(1, 0) = "Top"
        xAlignmentsH(1, 1) = wdAlignVerticalTop
        xAlignmentsH(2, 0) = "Bottom"
        xAlignmentsH(2, 1) = wdAlignVerticalBottom

    ReDim xPosition(1)
        xPosition(0) = "Above"
        xPosition(1) = "Below"
    
    ReDim xFSize(6)
        xFSize(0) = "8"
        xFSize(1) = "9"
        xFSize(2) = "10"
        xFSize(3) = "11"
        xFSize(4) = "12"
        xFSize(5) = "13"
        xFSize(6) = "14"
    
    ReDim xFontList(2)
        xFontList(0) = "Times New Roman"
        xFontList(1) = "Arial"
        xFontList(2) = "Courier"
        
    ReDim xLabelType(2)
        xLabelType(0) = "80"
        xLabelType(1) = "125"
        
'---assign arrays to list boxes
    With Me
        bCBXList .cmbFonts, xFontList()
        bCBXList .cmbLabelType, xLabelType()
        bCBXList .cmbAlignment, xAlignments()
        bCBXList .cmbAlignmentH, xAlignmentsH()
        bCBXList .cmbCP, xPosition()
        bCBXList .cmbSize, xFSize()
    End With
    
'---display Word unit of measurement
    Me.spnTopMargin.DisplayUnit = Word.Options.MeasurementUnit

100:
    On Error Resume Next

'---restart options
    If bDocIsRestarted(ActiveDocument) Then

'--- initialize control values
        GetDocVars Me
        Me.lblEnding.Caption = ActiveDocument.Variables("lblEnding").Value
    Else

'---assign initial values from inifile if they exist
        With Me
            If mpbase2.GetUserIni("Bates", "Font") <> "" Then
                .cmbFonts = mpbase2.GetUserIni("Bates", "Font")
            Else
                .cmbFonts = ActiveDocument.Styles(wdStyleNormal).Font.Name
            End If
            
            If mpbase2.GetUserIni("Bates", "Type") <> "" Then
                .cmbLabelType = mpbase2.GetUserIni("Bates", "Type")
            Else
                .cmbLabelType.ListIndex = 0
            End If
            
            If mpbase2.GetUserIni("Bates", "FontSize") <> "" Then
                .cmbSize = mpbase2.GetUserIni("Bates", "FontSize")
            Else
                .cmbSize = ActiveDocument.Styles(wdStyleNormal).Font.Size
            End If
'            .spnFSize.Value = Val(.txtFSize)
            
            If mpbase2.GetUserIni("Bates", "Digits") <> "" Then
               .spnLeadZero.Value = Val(mpbase2.GetUserIni("Bates", "Digits"))
            End If
            
            '* 9.7.1 - #4061
            If mpbase2.GetUserIni("Bates", "TopMargin") <> "" Then
                .spnTopMargin.Value = GetUserIniNumeric("Bates", "TopMargin")
            End If
            '**** END NEW CODE
            
            If mpbase2.GetUserIni("Bates", "Alignment") <> "" Then
                .cmbAlignment = mpbase2.GetUserIni("Bates", "Alignment")
            Else
                .cmbAlignment.ListIndex = 0
            End If
            
            If mpbase2.GetUserIni("Bates", "ConfidentialPosition") <> "" Then
                .cmbCP = mpbase2.GetUserIni("Bates", "ConfidentialPosition")
            Else
                .cmbCP.ListIndex = 0
            End If
            
            If mpbase2.GetUserIni("Bates", "HorizontalAlignment") <> "" Then
                .cmbAlignmentH = mpbase2.GetUserIni("Bates", "HorizontalAlignment")
            Else
                .cmbAlignmentH.ListIndex = 0
            End If
            
            .txtStartNo = mpbase2.GetUserIni("Bates", "Start")
            .txtNOLabels = mpbase2.GetUserIni("Bates", "Quantity")
            .txtConfidential = mpbase2.GetUserIni("Bates", "Phrase")
            
            
'            If mpbase2.GetUserIni("Bates", "SideBySide") <> "" Then
'                .chkSidebySide = mpbase2.GetUserIni("Bates", "SideBySide")
'
'            End If
            
            If mpbase2.GetUserIni("Bates", "TopMargin") <> "" Then
                .spnTopMargin.Value = GetUserIni("Bates", "TopMargin")
            End If
            
            Dim xSuffix As String
            Dim xPrefix As String
            
            xPrefix = mpbase2.GetUserIni("Bates", "Prefix")
            If Right(xPrefix, 1) = "~" Then
                xPrefix = xSubstitute(xPrefix, "~", "")
            End If
            .txtPrefix = xPrefix
            
            xSuffix = mpbase2.GetUserIni("Bates", "Suffix")
            If Left(xSuffix, 1) = "~" Then
                xSuffix = xSubstitute(xSuffix, "~", "")
            End If
            .txtSuffix = xSuffix
            
            If mpbase2.GetUserIni("Bates", "AllCaps") <> "" Then
                .chkBSAllCaps = mpbase2.GetUserIni("Bates", "AllCaps")
            End If
            
            If mpbase2.GetUserIni("Bates", "SmallCaps") <> "" Then
               .chkBSSmallCaps = mpbase2.GetUserIni("Bates", "SmallCaps")
            End If
            
            If mpbase2.GetUserIni("Bates", "Underlined") <> "" Then
                .chkBSUnderlined = mpbase2.GetUserIni("Bates", "Underlined")
            End If
            
            If mpbase2.GetUserIni("Bates", "Bold") <> "" Then
                .chkBSBold = mpbase2.GetUserIni("Bates", "Bold")
                   
            End If
        
        End With

    End If

'--- toggle list choice
    With Me
        .txtStartNo.SelStart = 0
        .txtStartNo.SelLength = Len(.txtStartNo.Text)
    End With
    
    MultiPage1.Tab = 0
    Application.StatusBar = mpMsgReady
    bFormInit = False
    bDisplay
   Exit Sub

ReInitialize:
    Select Case Err
        Case mpNoListArrays, 91  '---reinitialize app
            iRetry = iRetry + 1
            If iRetry < mpInitializeRetry Then
               ' bRet = bAppInitialize()
                'Set dbCurrent = dbMP
            Else
                MsgBox "Failure loading author data" & vbCr & _
                        "Author & Party lookups unavailable", _
                        vbExclamation, "Bates Labels"
                Resume 100
            End If
        Case Else
            Beep
            MsgBox "Error " & Str(Err) & vbCr & _
                Error(Err), vbExclamation, mpAppName + "(Userform.Initialize)"
    
    End Select

End Sub



Private Sub Form_Unload(Cancel As Integer)
    Set frmBates = Nothing
End Sub

