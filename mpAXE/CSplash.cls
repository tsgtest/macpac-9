VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private oSplash As frmSplash
Private m_bEval As Boolean
Private m_lDaysLeft As Long

Public Function Show()
Attribute Show.VB_MemberFlags = "40"
    DoEvents
    Set oSplash = New frmSplash
    With oSplash
'        If Me.IsEval Then
'            .lblMessage.Caption = "evaluation version" & vbCrLf & _
'                                  "( " & Me.DaysLeft & " day" & _
'                                  IIf(Me.DaysLeft = 1, "", "s") & " left )"
'        Else
'            .lblMessage.Caption = "powered by" & vbCrLf & "macpac objects"
'        End If
        .Show vbModeless
    End With
End Function

Public Function Hide()
Attribute Hide.VB_MemberFlags = "40"
    Unload oSplash
    Set oSplash = Nothing
End Function

Public Property Get IsEval() As Boolean
    IsEval = m_bEval
End Property

Public Property Let IsEval(bNew As Boolean)
    m_bEval = bNew
End Property

Public Property Get DaysLeft() As Long
    DaysLeft = m_lDaysLeft
End Property

Public Property Let DaysLeft(lNew As Long)
    m_lDaysLeft = lNew
End Property

