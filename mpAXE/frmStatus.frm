VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.4#0"; "Comctl32.ocx"
Begin VB.Form frmStatus 
   BorderStyle     =   0  'None
   Caption         =   "###"
   ClientHeight    =   2436
   ClientLeft      =   12
   ClientTop       =   -48
   ClientWidth     =   5664
   ControlBox      =   0   'False
   FillStyle       =   0  'Solid
   Icon            =   "frmStatus.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2436
   ScaleWidth      =   5664
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   360
      Left            =   2040
      TabIndex        =   1
      Top             =   2220
      Visible         =   0   'False
      Width           =   1524
   End
   Begin ComctlLib.ProgressBar pbStatus 
      Height          =   252
      Left            =   288
      TabIndex        =   0
      Top             =   1788
      Width           =   4884
      _ExtentX        =   8615
      _ExtentY        =   445
      _Version        =   327682
      Appearance      =   0
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   276
      Left            =   84
      TabIndex        =   3
      Top             =   84
      Width           =   5496
   End
   Begin VB.Image imgP 
      Height          =   384
      Left            =   276
      Picture         =   "frmStatus.frx":1DA2
      Top             =   552
      Width           =   384
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "###"
      Height          =   1032
      Left            =   936
      TabIndex        =   2
      Top             =   624
      Width           =   4584
   End
   Begin VB.Label lable2 
      BackColor       =   &H80000003&
      Height          =   372
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   5660
   End
End
Attribute VB_Name = "frmStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public m_bCancelled As Boolean

Private Sub btnCancel_Click()
    Me.Caption = "Job Cancelled..."
    m_bCancelled = True
End Sub

Private Sub Form_Load()
    m_bCancelled = False
    'ensure form is on top
    Call SetWindowPos(Me.hWnd, HWND_TOPMOST, 0, 0, 0, 0, _
                               SWP_NOSIZE Or _
                               SWP_FRAMECHANGED Or _
                               SWP_NOMOVE)
End Sub
