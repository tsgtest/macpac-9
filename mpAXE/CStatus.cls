VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CStatus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_bShowProgressBar As Boolean
Private m_bShowCancel As Boolean

Public Property Let Picture(ByVal xFile As String)
    With frmStatus.imgP
        If Len(xFile) Then
            .Picture = LoadPicture(xFile)
            .Visible = True
        Else
            .Picture = Nothing
            .Visible = False
        End If
    End With
    
    With frmStatus.lblStatus
        If Len(xFile) Then
            .Left = 825
            .Width = 4140
        Else
            .Left = 285
            .Width = 4585
        End If
    End With
End Property

Public Property Let Title(ByVal xNew As String)
    frmStatus.Caption = xNew
End Property

Public Property Get Title() As String
    Title = frmStatus.Caption
End Property

Public Property Get JobCancelled() As Boolean
    JobCancelled = frmStatus.m_bCancelled
End Property

Public Property Let ShowCancel(ByVal bNew As Boolean)
    m_bShowCancel = bNew
End Property

Public Property Get ShowCancel() As Boolean
    ShowCancel = m_bShowCancel
End Property

Public Property Let ProgressBarVisible(bNew As Boolean)
    m_bShowProgressBar = bNew
End Property

Public Property Get ProgressBarVisible() As Boolean
    ProgressBarVisible = m_bShowProgressBar
End Property

Public Property Get StatusValue() As Integer
    StatusValue = frmStatus.pbStatus.Value
End Property

Public Property Let StatusValue(iNew As Integer)
    frmStatus.pbStatus.Value = iNew
End Property

Public Function Show(Optional ByVal sPercent As Single = -1, _
                     Optional ByVal xMsg As String)
    Screen.MousePointer = vbHourglass
    
    With frmStatus
        .MousePointer = vbHourglass
        
        If sPercent <> -1 Then
            .pbStatus = sPercent
        End If
        
        If Len(xMsg) Then
            .lblStatus = xMsg
        End If
        
        .Label1.Caption = .Caption
        
        .btnCancel.Visible = Me.ShowCancel
        
        .pbStatus.Visible = Me.ProgressBarVisible
        
        If .Visible = False Then
            If InStr(xMsg, vbCr & vbCr) Then
                If m_bShowProgressBar Then
                    If m_bShowCancel Then
                        .Height = 2832
                    Else
                        .Height = 2268
                    End If
                Else
                    .Height = 1836
                End If
            Else
                If m_bShowProgressBar Then
                    If m_bShowCancel Then
                        .Height = 2332
                    Else
                        .Height = 1932
                    End If
                Else
                    .Height = 1250
                End If
            End If
                        
            frmStatus.Show
        Else
            .Refresh
        End If
    End With
    Screen.MousePointer = vbDefault
End Function

Public Function Hide()
    frmStatus.Visible = False
    frmStatus.MousePointer = vbDefault
    Unload frmStatus
    Set frmStatus = Nothing
End Function

Private Sub Class_Terminate()
    Hide
End Sub
