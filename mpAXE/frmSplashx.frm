VERSION 5.00
Begin VB.Form frmSplash 
   Appearance      =   0  'Flat
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   ClientHeight    =   4050
   ClientLeft      =   210
   ClientTop       =   1365
   ClientWidth     =   6375
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSplash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4050
   ScaleWidth      =   6375
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Label lblCustomText 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   240
      Left            =   105
      TabIndex        =   5
      Top             =   3630
      Width           =   6000
   End
   Begin VB.Label lblVersion 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Version #.#.#"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   300
      Left            =   4065
      TabIndex        =   4
      Top             =   2670
      Width           =   2055
   End
   Begin VB.Label lblMessage 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "   powered by    macpac objects"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   645
      Left            =   4050
      TabIndex        =   3
      Top             =   1815
      Width           =   2070
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "1990-2010 The Sackett Group, Inc."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   4020
      TabIndex        =   2
      Top             =   3210
      Width           =   2190
   End
   Begin VB.Label lblCopyright 
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3855
      TabIndex        =   1
      Top             =   3195
      Width           =   255
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "    For Microsoft Word    97, 2000, 2002/XP, 2003 and 2007"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   720
      Left            =   4035
      TabIndex        =   0
      Top             =   690
      Width           =   2160
   End
   Begin VB.Image imgSplash 
      Height          =   3255
      Left            =   60
      Top             =   330
      Width           =   3945
   End
   Begin VB.Image Image1 
      Height          =   3330
      Left            =   3795
      Picture         =   "frmSplash.frx":000C
      Top             =   180
      Width           =   2400
   End
   Begin VB.Shape shpBorder 
      BackColor       =   &H00FFFFFF&
      BorderColor     =   &H00404040&
      FillColor       =   &H00008000&
      Height          =   4005
      Left            =   0
      Top             =   0
      Width           =   6270
   End
   Begin VB.Shape shpCustomBack 
      BorderColor     =   &H80000003&
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00400000&
      FillStyle       =   0  'Solid
      Height          =   315
      Left            =   60
      Top             =   3585
      Width           =   6150
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long

Public Function GetIni(xSection As String, _
                        xKey As String, _
                        xIni As String) As Variant
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function

Private Sub Form_Load()
    On Error Resume Next
    Me.imgSplash = LoadPicture(App.Path & "\mpSplash.bmp")
'code below is not working.  Splash screen doesn't appear in Word
'    Dim oMPOApp As Object
'    Set oMPOApp = CreateObject("MPO.CApplication")
'    Me.lblVersion.Caption = "Version " & oMPOApp.Version
    Me.lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
    
    Me.lblVersion.ForeColor = &H404040
    Me.lblMessage.ForeColor = &H404040
    
    'get custom text for bottom of form
    Dim xCustomText As String
    xCustomText = GetIni("General", "SplashCustomText", App.Path & "\MacPac.ini")
    If xCustomText = Empty Then
        Me.Height = 3645
        Me.shpCustomBack.Visible = False
        Me.lblCustomText.Visible = False
        Me.shpBorder.Height = 3645
    Else
        Me.lblCustomText.Caption = xCustomText
    End If

End Sub
