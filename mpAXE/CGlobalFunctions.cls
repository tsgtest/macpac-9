VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGlobalFunctions"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Sub SetVersionFiles()
    Dim xWordVer As String
    Dim xVer As String
    Dim xFinalVer As String
    
    On Error GoTo ProcError
'   get Word version
    xWordVer = Word.Application.Version
    
    xFinalVer = App.Path & "\mpVer.dll"
    
'   ensure that correct version of mpVer.dll exists -
'   delete existing Word Version-Specific dll-
    If Dir(xFinalVer) <> "" Then
        Kill xFinalVer
    End If

'   get correct version of mpVer.dll
'   based on current version of Word
    If InStr(xWordVer, "8.") Then
        xVer = "\mpVer80.dll"
    Else
        xVer = "\mpVer90.dll"
    End If

'   copy correct version
    FileCopy App.Path & xVer, _
             xFinalVer
    
    Exit Sub
ProcError:
    Err.Raise Err.Number, "MPO.CApplication.SetVersionFiles", Err.Description
    Exit Sub
End Sub


