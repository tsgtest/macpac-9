Attribute VB_Name = "mdlGlobals"
'SetWindowPos Function
Declare Function SetWindowPos Lib "user32" _
   (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, _
    ByVal x As Long, ByVal y As Long, ByVal cx As Long, _
    ByVal cy As Long, ByVal wFlags As Long) As Long
    
Declare Function SetFocusAPI Lib "user32.dll" Alias "SetFocus" (ByVal hWnd As Long) As Long

Declare Function SetForegroundWindow Lib "user32.dll" (ByVal hWnd As Long) As Long

'SetWindowPos Flags
Public Const SWP_NOSIZE = &H1
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_FRAMECHANGED = &H20
Public Const SWP_NOREDRAW = &H8
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2



