VERSION 5.00
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "TDBG6.OCX"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmCreateList 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Create a List"
   ClientHeight    =   4665
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6060
   Icon            =   "frmCreateList.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4665
   ScaleWidth      =   6060
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   4770
      TabIndex        =   8
      Top             =   4140
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3615
      TabIndex        =   7
      Top             =   4140
      Width           =   1100
   End
   Begin TrueDBGrid60.TDBGrid grdListData 
      Height          =   2880
      Left            =   150
      OleObjectBlob   =   "frmCreateList.frx":000C
      TabIndex        =   4
      Top             =   1080
      Width           =   5790
   End
   Begin VB.OptionButton optTwoColumn 
      Caption         =   "&Two"
      Height          =   345
      Left            =   4545
      TabIndex        =   3
      ToolTipText     =   $"frmCreateList.frx":2719
      Top             =   405
      Width           =   690
   End
   Begin VB.OptionButton optOneColumn 
      Caption         =   "&One"
      Height          =   345
      Left            =   3825
      TabIndex        =   2
      ToolTipText     =   "In a one column list the items displayed are the ones written to the document."
      Top             =   405
      Value           =   -1  'True
      Width           =   690
   End
   Begin VB.TextBox txtListName 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   165
      TabIndex        =   0
      Top             =   405
      Width           =   2955
   End
   Begin TrueDBList60.TDBCombo cmbSortBy 
      Height          =   345
      Left            =   210
      OleObjectBlob   =   "frmCreateList.frx":27A3
      TabIndex        =   9
      Top             =   1080
      Visible         =   0   'False
      Width           =   2955
   End
   Begin VB.Label lblNumColumns 
      Caption         =   "Number of Columns:"
      Height          =   255
      Left            =   3825
      TabIndex        =   6
      Top             =   180
      Width           =   1500
   End
   Begin VB.Label lblData 
      Caption         =   "List &Data:"
      Height          =   255
      Left            =   195
      TabIndex        =   5
      Top             =   870
      Width           =   765
   End
   Begin VB.Label lblListName 
      Caption         =   "List &Name:"
      Height          =   270
      Left            =   195
      TabIndex        =   1
      Top             =   180
      Width           =   870
   End
   Begin VB.Label lblSortBy 
      Caption         =   "&Sort By:"
      Height          =   270
      Left            =   270
      TabIndex        =   10
      Top             =   885
      Visible         =   0   'False
      Width           =   705
   End
End
Attribute VB_Name = "frmCreateList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oArr As XArray
Private m_bCancelled As Boolean

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
End Sub

Private Sub btnOK_Click()
    On Error GoTo ProcError
    If IsValid() Then
        m_bCancelled = False
        Me.Hide
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Function IsValid() As Boolean
'returns true if the data input is valid
    Dim oArr As XArray
    Dim iCol As Integer
    Dim xVal1 As String
    Dim xVal2 As String
    Dim i As Integer
    
    Set oArr = Me.grdListData.Array

    On Error GoTo ProcError
    'check for empty name and empty list
    If Me.txtListName.Text = Empty Then
        Me.txtListName.SetFocus
        MsgBox "List Name is required information.", _
            vbExclamation, App.Title
        Exit Function
    ElseIf oArr.Count(1) = 0 Then
        Me.grdListData.SetFocus
        MsgBox "The list must contain at least one item.", _
            vbExclamation, App.Title
        Exit Function
    End If
    
    'check for empty cells in grid
    For i = 0 To oArr.UpperBound(1)
        If oArr.Value(i, 0) = Empty Then
            Me.grdListData.SetFocus
            MsgBox "The list can't contain empty values.", _
                vbExclamation, App.Title
            Exit Function
        End If
        
        If Me.optTwoColumn Then
            If oArr.Value(i, 1) = Empty Then
                Me.grdListData.SetFocus
                MsgBox "The list can't contain empty values.", _
                    vbExclamation, App.Title
                Exit Function
            End If
        End If
    Next i
    
    'get existing db object -
    'create one if none exists
    If m_oDB Is Nothing Then
        On Error Resume Next
        Set m_oDB = GetObject(, "mpDB.CDatabase")
        On Error GoTo ProcError
        If m_oDB Is Nothing Then
            Set m_oDB = New mpDB.CDatabase
        End If
    End If
    
    'check for list with same name
    Dim oList As mpDB.CList
    On Error Resume Next
    Set oList = m_oDB.Lists.Item(Me.txtListName.Text)
    On Error GoTo ProcError
    
    If Not oList Is Nothing Then
        Me.txtListName.SetFocus
        MsgBox "A list with the name '" & Me.txtListName.Text & _
            "' already exists.", vbExclamation, App.Title
        Exit Function
    End If
    
    'sort, then compare
    iCol = IIf(Me.optOneColumn, 0, 1)
    Sort oArr, iCol
    xVal1 = oArr.Value(0, iCol)
    For i = 1 To oArr.UpperBound(1)
        xVal2 = oArr.Value(i, iCol)
        If xVal1 = xVal2 Then
            MsgBox "The list contains multiple entries of the value '" & _
                xVal1 & "'.", vbExclamation, App.Title
            Exit Function
        End If
        xVal1 = xVal2
    Next i
    
    'if we got here, the list is valid
    IsValid = True
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpListCreator.frmCreateList.IsValid", Err.Description
    Exit Function
End Function

Private Sub Form_Load()
    m_bCancelled = True
    If m_oDB Is Nothing Then
        On Error Resume Next
        Set m_oDB = GetObject(, "mpDB.CDatabase")
        On Error GoTo 0
        If m_oDB Is Nothing Then
            Set m_oDB = New mpDB.CDatabase
        End If
    End If
    Set m_oArr = m_oDB.NewXArray
    m_oArr.ReDim 0, -1, 0, 1
    Me.grdListData.Array = m_oArr
'    FillSortList True
End Sub

Private Sub grdListData_BeforeUpdate(Cancel As Integer)
    On Error Resume Next
    Cancel = Abs(Not RowIsValid())
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Function RowIsValid() As Boolean
'returns true if row contains data
    On Error GoTo ProcError
    With Me.grdListData
        If .Columns(0).CellValue(.Bookmark) = Empty Then
            MsgBox "Invalid entry.  Cell must contain data.", vbExclamation
        ElseIf Me.optTwoColumn And .Columns(1).CellValue(.Bookmark) = Empty Then
            MsgBox "Invalid entry.  Cell must contain data.", vbExclamation
        Else
            RowIsValid = True
        End If
    End With
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpListCreator.frmCreateList.ValidateRow", Err.Description
    Exit Function
End Function

Private Sub grdListData_Error(ByVal DataError As Integer, Response As Integer)
    Response = 0
End Sub

Private Sub grdListData_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyTab Or KeyCode = vbKeyReturn Then
        KeyCode = vbKeyDown
    End If
End Sub

Private Sub grdListData_LostFocus()
    On Error GoTo ProcError
    If Me.grdListData.DataChanged Then
        If RowIsValid() Then
            Me.grdListData.Update
        Else
            Me.grdListData.SetFocus
        End If
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpListCreator.frmCreateList.grdListData_LostFocus", Err.Description
    Exit Sub
End Sub

Private Sub optOneColumn_Click()
    On Error GoTo ProcError
    With Me.grdListData
        .Columns(0).Width = .Columns(0).Width + .Columns(1).Width
        .Columns(1).Visible = False
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub optTwoColumn_Click()
    On Error GoTo ProcError
    With Me.grdListData
        .Columns(1).Visible = True
        .Columns(0).Width = .Columns(0).Width - .Columns(1).Width
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

'Private Sub FillSortList(ByVal bIncludeIDCol As Boolean)
'    Dim oSortArr As XArray
'    On Error GoTo ProcError
'    Set oSortArr = New XArray
'
'    With oSortArr
'        If bIncludeIDCol Then
'            .ReDim 0, 2, 0, 0
'            .Value(0, 0) = "Do not Sort the list"
'            .Value(1, 0) = "Column 1"
'            .Value(2, 0) = "Column 2"
'            Me.cmbSortBy.Array = oSortArr
'            Me.cmbSortBy.Rebind
'            ResizeTDBCombo Me.cmbSortBy, 3
'            Me.cmbSortBy.BoundText = "Column 2"
'        Else
'            .ReDim 0, 1, 0, 0
'            .Value(0, 0) = "Do not Sort the list"
'            .Value(1, 0) = "Column 1"
'            Me.cmbSortBy.Array = oSortArr
'            Me.cmbSortBy.Rebind
'            ResizeTDBCombo Me.cmbSortBy, 2
'            Me.cmbSortBy.BoundText = "Column 1"
'        End If
'    End With
'    Exit Sub
'ProcError:
'    RaiseError "mpAdministrator.frmCreateList.FillSortList"
'    Exit Sub
'End Sub

Public Sub Sort(vArray As XArray, SortCol As Integer, Optional Order As Variant)
    Screen.MousePointer = vbHourglass
    If IsMissing(Order) Then Order = "ASC"
    QuickSort vArray, vArray.LowerBound(1), vArray.UpperBound(1), SortCol, CStr(Order)
    Screen.MousePointer = vbDefault
End Sub

Private Sub QuickSort(vArray As XArray, ByVal First As Long, ByVal Last As Long, Col As Integer, Order As String)
    Dim PivotIndex As Long
    
    If First < Last Then
        PivotIndex = lPartition(vArray, First, Last, Col, Order)
        QuickSort vArray, First, PivotIndex - 1, Col, Order
        QuickSort vArray, PivotIndex, Last, Col, Order
    End If
End Sub

Private Function lPartition(vArray As XArray, ByVal First As Long, ByVal Last As Long, Col As Integer, Order As String) As Long
    Dim Pivot As Variant, vTemp As Variant
    Dim i As Integer
    Dim LB As Integer, UB As Integer
    
    LB = vArray.LowerBound(2)
    UB = vArray.UpperBound(2)
    Pivot = vArray((First + Last) \ 2, Col)
    While First <= Last
        If UCase(Order) = "DESC" Then
            While vArray(First, Col) > Pivot
                First = First + 1
            Wend
            
            While vArray(Last, Col) < Pivot
                Last = Last - 1
            Wend
        Else
            While vArray(First, Col) < Pivot
                First = First + 1
            Wend
            
            While vArray(Last, Col) > Pivot
                Last = Last - 1
            Wend
        End If
        
        If First <= Last Then
            For i = LB To UB
                vTemp = vArray(First, i)
                vArray(First, i) = vArray(Last, i)
                vArray(Last, i) = vTemp
            Next i
            First = First + 1
            Last = Last - 1
        End If
    Wend
    lPartition = First
End Function


