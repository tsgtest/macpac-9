VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Function Create() As String
'creates a macpac list after prompting the user-
'returns the name of the list iff the list is created
    Dim oForm As frmCreateList
    Dim m_oDB As mpDB.CDatabase
    Dim xSQL As String
    Dim oList As mpDB.CList
    Dim xSortFld As String
    Dim xName As String
    Dim oRS As DAO.Recordset
    Dim oArr As XArray
    Dim i As Integer
    Dim xTableName As String
    
    On Error GoTo ProcError
    Set oForm = New frmCreateList
    
    With oForm
        .Show vbModal
        
        If Not .Cancelled Then
            'create table
            xName = .txtListName.Text
            
            xTableName = xName
            
            Do
                xTableName = Replace(xTableName, " ", "")
            Loop While InStr(xTableName, " ") > 0
            
            'get existing db object -
            'create one if none exists
            If m_oDB Is Nothing Then
                On Error Resume Next
                Set m_oDB = GetObject(, "mpDB.CDatabase")
                On Error GoTo ProcError
                If m_oDB Is Nothing Then
                    Set m_oDB = New mpDB.CDatabase
                End If
            End If
            
            If .optOneColumn Then
                'create one column table
                m_oDB.Lists.CreateTable xTableName, "fldItem"
                xSQL = "SELECT fldItem FROM tbl" & xTableName
                
                'specify sort column for list
                If .cmbSortBy.SelectedItem = 1 Then
                    xSortFld = "fldItem"
                End If
            Else
                'create two column table
                m_oDB.Lists.CreateTable xTableName, "fldItem", "fldID"
                xSQL = "SELECT fldItem, fldID FROM tbl" & xTableName
                
                'specify sort column for list
                If .cmbSortBy.SelectedItem = 1 Then
                    xSortFld = "fldItem"
                ElseIf .cmbSortBy.SelectedItem = 2 Then
                    xSortFld = "fldID"
                End If
            End If
            
            'create list entry
            With m_oDB.Lists
                Set oList = .Add(xName, xSQL, xSortFld)
                .Save oList
            End With
            
            'save list data
            Set oRS = m_oDB.PublicDB.OpenRecordset(xSQL, , dbOpenDynamic)
            Set oArr = oForm.grdListData.Array
            
            'cycle through array, adding each item to the recordset
            With oRS
                For i = 0 To oArr.UpperBound(1)
                    .AddNew
                    .Fields("fldItem") = oArr.Value(i, 0)
                    
                    If oArr.Value(i, 1) <> Empty Then
                        'second column is in use
                        .Fields("fldID") = oArr.Value(i, 1)
                    End If
                    .Update
                Next i
            End With
            
            'if we get to this point, the list has been created
            Create = xName
        End If
    End With
    Exit Function
ProcError:
    DeleteList xName
    Err.Raise Err.Number, "mpAdministrator.frmMDIApp.CreateList", Err.Description
    Exit Function
End Function

Private Sub DeleteList(ByVal xListName As String)
    Dim xSQL As String
    Dim m_oDB As mpDB.CDatabase
    Dim xTableName As String
    
    'get existing db object -
    'create one if none exists
    On Error Resume Next
    Set m_oDB = GetObject(, "mpDB.CDatabase")
    On Error GoTo ProcError
    If m_oDB Is Nothing Then
        Set m_oDB = New mpDB.CDatabase
    End If
            
    xTableName = xListName
    
    Do
        xTableName = Replace(xTableName, " ", "")
    Loop While InStr(xTableName, " ") > 0
    
    'delete table
    xSQL = "DROP TABLE tbl" & xTableName
    m_oDB.PublicDB.Execute xSQL
    
    'delete entry in tblCustomLists
    xSQL = "DELETE FROM tblCustomLists WHERE fldListName='" & xListName & "'"
    m_oDB.PublicDB.Execute xSQL
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpAdministrator.frmMDIApp.DeleteList", Err.Description
    Exit Sub
End Sub

