Attribute VB_Name = "mdlDialog"
Option Explicit

Private Declare Function GetKeyState Lib "user32" ( _
    ByVal nVirtKey As Long) As Integer

Function bIsAlphaNumeric(iAsc As Integer) As Boolean
'returns TRUE if iasc is the ascii representation of an alphanumeric character
    bIsAlphaNumeric = ((iAsc >= 65 And iAsc <= 90) Or _
            (iAsc >= 97 And iAsc <= 122) Or _
            (iAsc >= 48 And iAsc <= 57))
End Function

Sub AllowAlphaNumericOnly(ByRef iAsc As Integer)
'sets iAsc=0 if iAsc is not the ascii
'representation of an alphanumeric character
    If Not (bIsAlphaNumeric(iAsc) Or iAsc = vbKeyBack Or iAsc = vbKeyDelete) Then
        iAsc = 0
    End If
End Sub

Sub PreventApostrophe(ByRef iAsc As Integer)
'sets iAsc=0 if iAsc is the ascii
'representation of an apostrophe
    If iAsc = 39 Then
        iAsc = 0
    End If
End Sub

Function bEnsureSelectedContent(ctlP As VB.Control, _
                                Optional bLastCharOnly As Boolean = False, _
                                Optional bForceSelection As Boolean = False) As Boolean
    Dim ctlData As VB.Data
    On Error Resume Next
    If bForceSelection Then
        With ctlP
            If (TypeOf ctlP Is ListBox) Then
                If .ListIndex < 0 Then _
                    .ListIndex = 0
            Else
                .SelStart = 0
                .SelLength = Len(.Text)
            End If
        End With
    Else
        If IsPressed(9) Then
            With ctlP
                If (TypeOf ctlP Is ListBox) Then
                    If .ListIndex < 0 Then _
                        .ListIndex = 0
                Else
                    If bLastCharOnly = False Then
                        .SelStart = 0
                        .SelLength = Len(.Text)
                    Else
                        .SelStart = Len(.Text)
                        .SelLength = 1
                    End If
                End If
            End With
        End If
    End If
End Function

Function IsPressed(lKey As Long) As Boolean
    IsPressed = (GetKeyState(lKey) < 0)
End Function

Public Sub GridTabOut(grdGrid As TDBGrid, bEnter As Boolean, iColtoCheck As Integer)
    '---this sub allows a tabout from grid when navigation param is set to grid navigation
    '---normally tabbing halts at last item in grid
    On Error Resume Next
    Select Case bEnter
        Case True
            grdGrid.TabAction = 2
        Case False
            If grdGrid.RowBookmark(grdGrid.Row) = grdGrid.Array.UpperBound(1) Or _
                    grdGrid.RowBookmark(grdGrid.Row) = grdGrid.Array.LowerBound(1) Or _
                    IsNull(grdGrid.RowBookmark(grdGrid.Row)) Then
                If grdGrid.Col = iColtoCheck Then
                    '---change grid navigation to enable tab out
                    grdGrid.TabAction = 1
                End If
            End If
        Case Else
    End Select
End Sub

Function ControlHasValue(ctlP As Control) As Boolean
'---for some reason, type of isn't returning CheckBox
    If InStr(ctlP.Name, "chk") Or _
        InStr(ctlP.Name, "CheckBox") Then
        ControlHasValue = True
        Exit Function
    End If
  
    If TypeOf ctlP Is TextBox Or _
        TypeOf ctlP Is TrueDBList60.TDBCombo Or _
        TypeOf ctlP Is VB.ComboBox Or _
        TypeOf ctlP Is CheckBox Or _
        TypeOf ctlP Is TrueDBList60.TDBList Or _
        TypeOf ctlP Is VB.ListBox Or _
        TypeOf ctlP Is OptionButton Then
        ControlHasValue = True
    Else
        ControlHasValue = False
    End If
End Function

Function bForceCaps(ctlText As VB.Control) As Boolean
'force uppercase in control ctlText
'useful in change event of any ctl
'having a text property - needed
'because UCase will force repositioning
'of insertion point

    Dim iStartPos As Integer
    
    With ctlText
        iStartPos = .SelStart
        .Text = UCase(.Text)
        .SelStart = iStartPos
    End With
        
End Function


Public Function DisableInactiveTabs(frmP As VB.Form)
    Dim ctlP As VB.Control
    On Error Resume Next
    With frmP
        For Each ctlP In .Controls
            If (ctlP.Container Is .MultiPage1) Then
               ctlP.Enabled = (ctlP.Left >= 0)
            End If
        Next
    End With
End Function

Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xarP As XArray
    Dim iRows As Integer
    Dim xDesc As String

    On Error GoTo ProcError
    With tdbCBX
        If .Array Is Nothing Then
            Err.Raise mpError_ObjectExpected, , _
                "No XArray has been assigned to " & tdbCBX.Name & "."
        End If
        Set xarP = .Array
        iRows = mpbase2.Min(xarP.Count(1), CDbl(iMaxRows))
        On Error Resume Next
        .DropdownHeight = ((iRows) * (.RowHeight) + 8)
        If Err.Number = 6 Then
 '---       use hard value if there's a processor problem -- err 6 is overflow
            .DropdownHeight = (iRows * 240)
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.mdlFC.ResizeTDBCombo"
    Exit Sub
End Sub
Public Sub MatchInXArrayList(KeyCode As Integer, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'first character is KeyCode
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As XArray
    Dim lStartRow As Long
    Dim xChr As String
    
'   get starting row
    lStartRow = ctlP.Bookmark
    
'   start with next row
    lCurRow = lStartRow + 1
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1)
    
'   get the char that we're searching for
    xChr = UCase(Chr(KeyCode))
    
'   loop through xarray until match with first letter is found
    While (xChr <> UCase(Left(xarP.Value(lCurRow, 1), 1)))
'       if at end of xarray, start from beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        Else
            lCurRow = 0
        End If
        
    Wend
    
'   select the matched item
    With ctlP
        .Bookmark = lCurRow
        .SelBookmarks.Remove 0
        .SelBookmarks.Add lCurRow
        .SetFocus
    End With
End Sub

Public Function lGetActiveControlColor() As Long
    Dim lColor As Long
    On Error Resume Next
    lColor = GetMacPacIni("General", "ActiveControlColor")
    If lColor = 0 Then
        lColor = vbCyan
    End If
    lGetActiveControlColor = lColor
End Function

Public Function lGetInactiveControlColor() As Long
    Dim lColor As Long
    On Error Resume Next
    lColor = GetMacPacIni("General", "InactiveControlColor")
    If lColor = 0 Then
        lColor = vbWhite
    End If
    lGetInactiveControlColor = lColor
End Function

Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    Dim oNum As CNumbers
    
    Set oNum = New CNumbers
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = oNum.Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, App.Title
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Public Sub SelectControlText()
'selects the text in the active control if appropriate
    Static oPrevCtl As VB.Control
    Dim oCurCtl As VB.Control

    On Error Resume Next
    Set oCurCtl = Screen.ActiveControl
    On Error GoTo 0

    If oCurCtl Is Nothing Then
        Exit Sub
    End If

    With oCurCtl
        If Not oCurCtl Is oPrevCtl Then
            If TypeOf oCurCtl Is TextBox Then
                .SelStart = 0
                .SelLength = Len(.Text)
            End If
            Set oPrevCtl = oCurCtl
        End If
    End With
End Sub
