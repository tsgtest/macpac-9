Attribute VB_Name = "mdlFc"
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long
    
Public Const SEP As String = "|"

Public Enum ControlDefFields
    Name = 0
    ControlType = 1
    Caption = 2
    SpaceBefore = 3
    ListName = 4
    NumberOfLines = 5
    CIListID = 6
    IsSticky = 7
    CMDetailField = 8
    CMRelatedField = 9
    Format = 10
End Enum

Public Const MSGBOX_TITLE As String = "MacPac Template Architect Wizard"

Public g_oDB As mpDB.CDatabase
Public g_oError As mpError.CError
Public g_xDBBak As String
Public g_bIsWord12x As Boolean


Public Sub CreateCustomTemplate(oForm As frmCreateCustomTemplate)
'shows Template Architect Wizard dialog-
'creates macpac custom template from
'dialog input

    Dim oDef As mpDB.CTemplateDef
    Dim lCIFormatID As Long
    Dim oStatus As Object
    Dim oDefs As mpDB.CTemplateDefs '*c

    Set oStatus = CreateObject("mpAXE.CStatus")
    If oForm.EditedTemplate <> Empty Then
        oStatus.Title = "Editing MacPac Custom Template"
        oStatus.Show , "Editing custom template '" & _
            oForm.txtTemplateFileName.Text & "'.  Please wait..."
    Else
        oStatus.Title = "Creating MacPac Custom Template"
        oStatus.Show , "Creating custom template '" & _
            oForm.txtTemplateFileName.Text & "'.  Please wait..."
    End If
    
    On Error GoTo ProcError
    Set oDefs = g_oDB.TemplateDefinitions '*c
    oDefs.Refresh '*c
    
    'delete any existing template records
    If oDefs.Exists(oForm.txtTemplateFileName) Then '*c
        DeleteCustomTemplate oForm.txtTemplateFileName.Text
    End If '*c

    DeleteCustomTemplate oForm.txtTemplateFileName.Text
    
    'create template
    Set oDef = CreateTemplate(oForm)

    If oForm.EditedTemplate = Empty Then
        'we're creating a new template -
        'create boilerplate - if we're
        'just editing a template, we use
        'the existing boilerplate file
        CreateBoilerplate oForm, oDef
    End If
    
    If oForm.chkUseCI.Value = vbChecked Then
        'create ci format
        lCIFormatID = CreateCIFormat(oForm)
    End If
    
    'create properties
    CreateProperties oForm

    'create letterhead assignments
    Dim bShowLetterheadTab As Boolean
    bShowLetterheadTab = _
        CreateLetterheadAssignments(oForm)

    '*c 4169
    'create menu assignments
    If oForm.optCreateNew.Value = True Then
        CreateMenuAssignments oDef.FileName, bShowLetterheadTab
    End If
    'create dialog
    CreateDialog oForm, bShowLetterheadTab, lCIFormatID

    'create controls
    CreateControls oForm
    
    'resize dialog
    SetDialogSize oForm
    
    'open boilerplate file
    Dim xBP As String
    Dim oWord As Word.Application
    
    xBP = mpBase2.BoilerplateDirectory & oDef.BoilerplateFile
    If Dir$(xBP) <> Empty Then
        On Error Resume Next
        Set oWord = GetObject(, "Word.Application")
        On Error GoTo ProcError
        
        If oWord Is Nothing Then
            Set oWord = CreateObject("Word.Application")
        End If
        
        Set oStatus = Nothing
        With oWord
            .DisplayAlerts = wdAlertsNone
            .Visible = True
            .Activate
            
            Dim oDoc As Word.Document
            On Error Resume Next
            Set oDoc = .Documents(xBP)
            On Error GoTo ProcError
            
            If oDoc Is Nothing Then
                .Documents.Open xBP
            Else
                .Documents.Item(xBP).Activate
            End If
            .DisplayAlerts = wdAlertsAll
        End With
    End If
    
    'run bookmark creator
    On Error Resume Next
    Err.Clear
    oWord.Run "zzmpInsertMacPacPropertyBookmarks"
    If Err.Number > 0 Then
        MsgBox "Could not run the Bookmark Creator.  Please ensure that " & vbCr & _
                IIf(g_bIsWord12x, "MacPac Template Architect Administration.dotm", "mpTemplateArchitectAdmin.dot") & " is in the Word startup directory, " & vbCr & _
                "then click 'Create Custom Template Bookmarks'.", vbExclamation, MSGBOX_TITLE
    End If
    Exit Sub
ProcError:
    RestorePublicDB
    Err.Description = Err.Description & "  Your Public database has been restored " & _
        "to its previous working state."
    RaiseError "mpAdministrator.mdlFc.CreateCustomTemplate"
    Exit Sub
End Sub

Private Function RestorePublicDB()
    Dim l As Long
    Dim xDB As String
    
    On Error GoTo ProcError
    Set g_oDB = Nothing
    
'    xDB = GetMacPacIni("Databases", "Public")
    xDB = mpBase2.PublicDB
    
    'get start time
    l = mpBase2.CurrentTick
    
    Do
        On Error Resume Next
        Err.Clear
        'attempt to restore backup
        FileCopy g_xDBBak, xDB
        
    'loop until the copy was successful or 10 seconds has passed
    Loop Until (Err.Number = 0) Or (mpBase2.CurrentTick - l > 10000)
    
    If Err.Number Then
        MsgBox "Could not restore the public database '" & xDB & "'  The " & _
            "backup copy can be found at '" & g_xDBBak & "'.", vbExclamation
    End If
    
    Exit Function
ProcError:
    RaiseError "TemplateArchitectWizard.mdlFc.RestorePublicDB"
    Exit Function
End Function

Private Function SetDialogSize(oForm As frmCreateCustomTemplate) As Integer
'returns an appropriate height for the custom template dialog
    On Error GoTo ProcError
    Const iMIN_DLG_HEIGHT As Integer = 3000
    
    Dim iTab1Height As Integer
    Dim iTab2Height As Integer
    Dim iTab3Height As Integer
    Dim iCtlHeight As Integer
    Dim iCtlType As mpCustomControl
    Dim iSpaceBefore As Integer
    Dim iHeight As Integer
    Dim oDef As CCustomDialogDef
    Dim oDefs As CCustomDialogDefs
    Dim iLines As Integer
    Dim iTab As Integer
    Dim oCtlDefsArr As XArray
    Dim xDefString As String
    
    'cycle through controls in list
    Set oCtlDefsArr = oForm.grdControls.Array
    Dim i As Integer
    
    For i = 0 To oCtlDefsArr.UpperBound(1)
        With oCtlDefsArr
            'get the definition for the specified index
            xDefString = .Value(i, 2)
            
            'parse out parameters of definition
            iSpaceBefore = GetDelimitedStringItem(xDefString, ControlDefFields.SpaceBefore + 1, "|")
            iTab = oCtlDefsArr.Value(i, 0)
            iLines = GetDelimitedStringItem(xDefString, ControlDefFields.NumberOfLines + 1, "|")
            iCtlType = GetDelimitedStringItem(xDefString, ControlDefFields.ControlType + 1, "|")
            
            'get control height based on control type
            If iCtlType = mpCustomControl_CheckBox Then
                iCtlHeight = 255
            ElseIf (iCtlType = mpCustomControl_TextBox Or _
                iCtlType = mpCustomControl_MultiLineCombo) And (iLines > 1) Then
                'multiply height by the number of lines -
                'the -1 exists because MacPac reads
                '1 less than the number of lines specified
                iCtlHeight = iLines * 180
            Else
                iCtlHeight = 315
            End If
            
            'calculate running sums of the height of each tab
            If iTab = 1 Then
                iTab1Height = iTab1Height + iCtlHeight + iSpaceBefore
            ElseIf iTab = 2 Then
                iTab2Height = iTab2Height + iCtlHeight + iSpaceBefore
            Else
                iTab3Height = iTab3Height + iCtlHeight + iSpaceBefore
            End If
        End With
    Next i
        
' Fix #4044 '*c
'    If oForm.chkUseAuthors Then
'        'the author control will be displayed -
'        'add 570 to the first tab
        iTab1Height = iTab1Height + 570
'    End If
       
    'get the height of tallest tab
    iHeight = Max(Max(CDbl(iTab1Height), CDbl(iTab2Height)), CDbl(iTab3Height))
    
' Fix #4044 '*c
    'account for empty space at top of all custom dialog boxes
'    If oForm.chkUseAuthors Then
'        'less space is needed without authors
'        iHeight = iHeight + 1000
'    Else
        iHeight = iHeight + 1200
'    End If
    
    'get the custom dialog
    Set oDefs = g_oDB.CustomDialogDefs
    oDefs.Refresh
    Set oDef = oDefs.Item(oForm.txtTemplateFileName.Text)
    
    'set the height of the custom dialog
    oDef.Height = Max(CDbl(iHeight) + 300, 5000)
    
    'expand dialog width if 3 tabs and letterhead
    With oDef
        If .Tab1 <> Empty And _
            .Tab2 <> Empty And _
            .Tab3 <> Empty And _
            .ShowLetterheadTab Then
            .Width = 5950
        End If
    End With
    
    'save custom dialog def
    oDefs.Save oDef
    Exit Function
ProcError:
    RaiseError "mpAdministrator.mdlFc.SetDialogSize"
    Exit Function
End Function

Private Sub CreateControls(oForm As frmCreateCustomTemplate)
'creates/saves the properties for the specified template using the form
    Dim oDefs As mpDB.CCustomControls
    Dim oDef As mpDB.CCustomControl
    Dim oPropDefs As mpDB.CCustomPropertyDefs
    Dim oPropDef As mpDB.CCustomPropertyDef
    Dim xDefString As String
    Dim xDef() As String
    Dim i As Integer
    Static iIndex As Integer
    Dim xTemplate As String
    Dim oCtlDefsArr As XArray
    
    On Error GoTo ProcError
    
    'reset static index var
    iIndex = 0
    
    xTemplate = oForm.txtTemplateFileName.Text
    
    'cycle through controls in list
    Set oCtlDefsArr = oForm.grdControls.Array
    
    For i = 0 To oCtlDefsArr.UpperBound(1)
        With oCtlDefsArr
            'get the definition for the specified index
            xDefString = .Value(i, 2)
            
            mpBase2.xStringToArray xDefString, xDef, 1, SEP
            
            'get id of property that is linked to this control
            Set oPropDefs = g_oDB.CustomProperties(xTemplate)
            Set oPropDef = oPropDefs.Item(.Value(i, 1))
            
            'get controls collection - will be empty if new
            Set oDefs = g_oDB.CustomControls(xTemplate)
            
            'add control for this property id
            Set oDef = oDefs.Add(oPropDef.ID)
        End With
        
        With oDef
            'set property values
            If xDef(ControlDefFields.NumberOfLines, 0) > 1 And _
                xDef(ControlDefFields.ControlType, 0) = "1" Then
                'definition has specified that control is a _
                'textbox with more than one line -
                'make it a multiline textbox
                .ControlType = mpCustomControl_MultilineTextBox
            Else
                .ControlType = xDef(ControlDefFields.ControlType, 0)
            End If
            
            .Caption = Trim$(xDef(ControlDefFields.Caption, 0))
            
            'property is 0-based, value in dlg is 1-based
            .TabID = oCtlDefsArr.Value(i, 0) - 1
            
            iIndex = iIndex + 1
            .Index = iIndex
            
            .SpaceBefore = xDef(ControlDefFields.SpaceBefore, 0)
            
            Dim xListName As String
            If bIsListControl(.ControlType) Then
                xListName = xDef(ControlDefFields.ListName, 0)
                If xListName <> Empty Then
                    Dim oList As mpDB.CList
                    Set oList = g_oDB.Lists(xListName)
                    If Not oList Is Nothing Then
                        Set .List = oList
                    End If
                End If
            End If
            .Format = xDef(ControlDefFields.Format, 0)
            .Lines = xDef(ControlDefFields.NumberOfLines, 0)
            .CIList = xDef(ControlDefFields.CIListID, 0)
            .IsSticky = xDef(ControlDefFields.IsSticky, 0)
            .CMLink = xDef(ControlDefFields.CMDetailField, 0)
            .CMRelatedIndex = xDef(ControlDefFields.CMRelatedField, 0)
        End With
        
        oDefs.Save oDef
    Next i
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateControls"
    Exit Sub
End Sub

Private Sub CreateProperties(oForm As frmCreateCustomTemplate)
'creates/saves the properties for the specified template using the form
    Dim oDefs As mpDB.CCustomPropertyDefs
    Dim oDef As mpDB.CCustomPropertyDef
    Dim xDefString As String
    Dim xDef() As String
    Dim i As Integer
    Dim xTemplate As String
    Dim vPropArr As Variant
    
    On Error GoTo ProcError
    
    xTemplate = oForm.txtTemplateFileName.Text
    
    'cycle through properties in list
    vPropArr = oForm.PropertyDefinitions
    Set oDefs = g_oDB.CustomProperties(xTemplate)
    
    For i = 0 To oForm.lstProperties.ListCount - 1
        'get item data - index of definition string in
        'the definition array is held in item data
        xDefString = vPropArr(oForm.lstProperties.ItemData(i))
Debug.Print xDefString
        mpBase2.xStringToArray xDefString, xDef, 1, SEP

        Set oDef = oDefs.Add()

        With oDef
            'set property values
            .Name = Trim$(xDef(0, 0))
            .Category = xTemplate

            Dim xLinkedPropCaption As String
            xLinkedPropCaption = xDef(1, 0)

            If xLinkedPropCaption Like "Office*" Then
                .PropertyType = mpCustomPropertyType_AuthorOffice
                .LinkedProperty = xDef(2, 0)
            ElseIf xLinkedPropCaption Like "Author*" Then
                .PropertyType = mpCustomPropertyType_Author
                .LinkedProperty = xDef(2, 0)
            Else
                .PropertyType = mpCustomPropertyType_Document
            End If

            .WholeParagraph = xDef(3, 0)
            .UnderlineLength = xDef(4, 0)
            .Indexed = xDef(5, 0)
            .Macro = Trim$(xDef(6, 0))
            .Action = xDef(7, 0)
            .Bookmark = xDef(8, 0)
        End With

        oDefs.Save oDef
    Next i
    
    CreateMacros xTemplate
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateProperties"
    Exit Sub
End Sub
'
'Private Sub CreateProperties(oForm As frmCreateCustomTemplate)
''creates/saves the properties for the specified template using the form
'    Dim oDefs As mpDB.CCustomPropertyDefs
'    Dim oDef As mpDB.CCustomPropertyDef
'    Dim xDefString As String
'    Dim xDef() As String
'    Dim i As Integer
'    Dim xTemplate As String
'    Dim vPropArr As Variant
'
'    On Error GoTo ProcError
'
'    xTemplate = oForm.txtTemplateFileName.Text
'
'    'cycle through properties in list
'    vPropArr = oForm.PropertyDefinitions
'    Set oDefs = g_oDB.CustomProperties(xTemplate)
'
'    For i = 0 To UBound(vPropArr, 1)
'        'get item data - index of definition string in
'        'the definition array is held in item data
'        xDefString = vPropArr(i)
'
'        mpBase2.xStringToArray xDefString, xDef, 1, SEP
'
'        Set oDef = oDefs.Add()
'
'        With oDef
'            'set property values
'            .Name = Trim$(xDef(0, 0))
'            .Category = xTemplate
'
'            Dim xLinkedPropCaption As String
'            xLinkedPropCaption = xDef(1, 0)
'
'            If xLinkedPropCaption Like "Office*" Then
'                .PropertyType = mpCustomPropertyType_AuthorOffice
'                .LinkedProperty = xDef(2, 0)
'            ElseIf xLinkedPropCaption Like "Author*" Then
'                .PropertyType = mpCustomPropertyType_Author
'                .LinkedProperty = xDef(2, 0)
'            Else
'                .PropertyType = mpCustomPropertyType_Document
'            End If
'
'            .WholeParagraph = xDef(3, 0)
'            .UnderlineLength = xDef(4, 0)
'            .Indexed = xDef(5, 0)
'            .Macro = Trim$(xDef(6, 0))
'            .Action = xDef(7, 0)
'            .Bookmark = xDef(8, 0)
'        End With
'
'        oDefs.Save oDef
'    Next i
'
'    CreateMacros xTemplate
'
'    Exit Sub
'ProcError:
'    RaiseError "mpAdministrator.mdlFc.CreateProperties"
'    Exit Sub
'End Sub

Private Sub CreateMacros(ByVal xTemplate As String)
'creates the macros for the template
    Const MACRO_ARGS As String = "(ByVal xPropName As String, ByVal xText As String, ByVal xValue As String, ByVal xBookmark As String)"
    Dim oDef As mpDB.CCustomPropertyDef
    Dim oDefs As mpDB.CCustomPropertyDefs
    Dim i As Integer
    Dim oApp As Word.Application
    
    On Error GoTo ProcError
    
    'cycle through properties in list
    Set oDefs = g_oDB.CustomProperties(xTemplate)
    
    For i = 1 To oDefs.Count
        'get item data - index of definition string in
        'the definition array is held in item data
        Set oDef = oDefs.Item(i)

        If (oDef.Macro <> Empty) Then
            'create macro in template
            If oApp Is Nothing Then
                Set oApp = CreateObject("Word.Application")
                oApp.Visible = False
            End If
            
            If oApp Is Nothing Then
                'alert that macros can't be created
                MsgBox "Macros could not be created for this template.", _
                    vbExclamation, MSGBOX_TITLE
                Exit Sub
            End If
                
            'open/activate the template file if necessary
            Dim oDoc As Word.Document
            Dim oTemplate As Word.Template
            Dim oModule As VBComponent
            
            On Error Resume Next
            Set oDoc = oApp.Documents(xTemplate)
            On Error GoTo ProcError
            
            If oDoc Is Nothing Then
                Set oDoc = oApp.Documents.Open(mpBase2.TemplatesDirectory & xTemplate)
            End If
            
            If oDoc Is Nothing Then
                'alert that macros can't be created
                    MsgBox "Macros could not be created for this template.", vbExclamation, MSGBOX_TITLE
                Exit Sub
            End If
            
           'write macro to attached template
           Set oTemplate = oDoc.AttachedTemplate
        
           Dim oComponent As VBComponent
           For Each oComponent In oTemplate.VBProject.VBComponents
               If oComponent.Name = "CustomTemplateMacros" Then
                   Set oModule = oComponent
                   Exit For
               End If
           Next
           
           Dim xProc As String
           xProc = "Sub " & oDef.Macro & MACRO_ARGS & vbCr & vbCr & "End Sub"
           With oTemplate.VBProject.VBComponents
               If oModule Is Nothing Then
                    'module doesn't exist - add
                    Set oModule = .Add(vbext_ct_StdModule)
                    oModule.Name = "CustomTemplateMacros"
                    
                    'add the procedure
                    oModule.CodeModule.AddFromString xProc
               Else
                    With oModule.CodeModule
                        If .Find("Sub " & oDef.Macro, 1, 1, _
                            .CountOfLines, 80) = False Then
                            'procedure doesn't exist - add
                            .AddFromString xProc
                        End If
                    End With
               End If
           End With
        End If
    Next i
    
    'save and close if the template was modified
    If Not oDoc Is Nothing Then
        oTemplate.Save
        oApp.Quit wdDoNotSaveChanges
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateProperties"
    Exit Sub
End Sub

Private Sub CreateDialog(oForm As frmCreateCustomTemplate, ByVal bShowLetterheadTab As Boolean, ByVal lCIFormatID As Long)
'creates and saves the dialog definition
    Dim oDefs As mpDB.CCustomDialogDefs
    Dim oDef As mpDB.CCustomDialogDef
    Dim xTemplate As String
    
    On Error GoTo ProcError
    
    xTemplate = oForm.txtTemplateFileName.Text
    
    Set oDefs = g_oDB.CustomDialogDefs
    Set oDef = oDefs.Add(xTemplate)
    With oDef
        .Caption = oForm.txtDialogTitle.Text
        Dim oFormat As mpDB.CCIFormat
        
        On Error Resume Next
        Set oFormat = g_oDB.CIFormats.Item(xTemplate)
        On Error GoTo ProcError

        If Not oFormat Is Nothing Then
            .CIFormat = oFormat.ID
        End If
        
        .ShowAuthorControl = oForm.chkUseAuthors
        .ShowLetterheadTab = bShowLetterheadTab
        
        .Tab1 = oForm.txtTab1Caption
        
        'enter caption only if the textbox is visible
        .Tab2 = oForm.txtTab2Caption.Text
        
        'enter caption only if the textbox is visible
        .Tab3 = oForm.txtTab3Caption.Text
        
        .CIFormat = lCIFormatID
    End With
    
    'save definition
    oDefs.Save oDef
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateDialog"
    Exit Sub
End Sub

Private Function CreateLetterheadAssignments(oForm As frmCreateCustomTemplate) As Boolean
'creates letterhead assignments for all checked letterheads
    
    Dim oDefs As mpDB.CLetterheadDefs
    Dim oNode As MSComctlLib.Node
    Dim bShowLetterheadTab As Boolean
    Dim xTemplate As String
    
    On Error GoTo ProcError
    xTemplate = oForm.txtTemplateFileName.Text
    
    Set oDefs = g_oDB.LetterheadDefs
    
    'cycle through nodes in letterhead tree
    For Each oNode In oForm.tvLetterheads.Nodes
        If oNode.Checked Then
            'create assignment
            oDefs.AddAssignment CLng(Mid$(oNode.Key, 2)), xTemplate
            CreateLetterheadAssignments = True
        Else
            'ensure that assignment doesn't exist
            oDefs.DeleteAssignment CLng(Mid$(oNode.Key, 2)), xTemplate
        End If
    Next oNode
    Exit Function
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateLetterheadAssignments"
    Exit Function
End Function

Private Sub CreateBoilerplate(oForm As frmCreateCustomTemplate, oDef As mpDB.CTemplateDef)
'creates boilerplate file from specified file -
'creates empty bp if no file specified
    Dim xBP As String
    On Error GoTo ProcError
    
    xBP = mpBase2.BoilerplateDirectory & oDef.BoilerplateFile
    
    Dim iPos As Integer
    If oForm.txtBPSource.Text = Empty Then
        'create blank bp file
        If g_bIsWord12x Then
            FileCopy App.Path & "\BlankBP.docx", xBP
        Else
            FileCopy App.Path & "\BlankBP.doc", xBP
        End If
    ElseIf Dir(oForm.txtBPSource.Text) = Empty Then
        'raise error - the source file doesn't exit
        Err.Raise mpError.mpError_InvalidFile, , _
            "The file '" & oForm.txtBPSource.Text & _
                "' doesn't exist."
    ElseIf Dir$(xBP) = Empty Then
        'copy/rename this file
        FileCopy oForm.txtBPSource.Text, xBP
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateBoilerplate"
    Exit Sub
End Sub

Private Function CreateTemplate(oForm As frmCreateCustomTemplate) As mpDB.CTemplateDef
'creates template db entry, template assignments
'and template .dot file
    Dim oTemplateDef As mpDB.CTemplateDef
    Dim oTemplateDefs As mpDB.CTemplateDefs
    Dim xTemplate As String
    Dim iPos As Integer
    Dim xBP As String
    
    On Error GoTo ProcError
    Set oTemplateDefs = g_oDB.TemplateDefinitions
    
    'create new def object
    Set oTemplateDef = oTemplateDefs.Add(oForm.txtTemplateFileName)
    
    With oTemplateDef
        'fill in data
        .ShortName = oForm.txtShortName
        .Description = oForm.txtDescription
        
        If oForm.EditedTemplate = Empty Then
            'we're creating a new template -
            If g_bIsWord12x Then
                If oForm.txtBPSource.Text = Empty Then
                    'create bp name from template
                    .BoilerplateFile = Replace(.FileName, ".dotx", ".mbp")
                Else
                    'use existing doc for bp
                    iPos = InStrRev(oForm.txtBPSource.Text, "\")
                    xBP = Mid$(oForm.txtBPSource.Text, iPos + 1)
                    xBP = Replace(xBP, ".docx", ".mbp")
                    .BoilerplateFile = xBP
                End If
            Else
                If oForm.txtBPSource.Text = Empty Then
                    'create bp name from template
                    .BoilerplateFile = Replace(.FileName, ".dot", ".mbp")
                Else
                    'use existing doc for bp
                    iPos = InStrRev(oForm.txtBPSource.Text, "\")
                    xBP = Mid$(oForm.txtBPSource.Text, iPos + 1)
                    xBP = Replace(xBP, ".doc", ".mbp")
                    .BoilerplateFile = xBP
                End If
            End If
        Else
            .BoilerplateFile = oForm.txtBPSource.Text
        End If
        
        .TemplateType = oForm.cmbLocation.BoundText
        .DefaultDocIDStamp = oForm.cmbDefaultDocID.BoundText
        .Macro = "zzmpCreateCustomDocument"
        
        'save definition
        oTemplateDefs.Save oTemplateDef
    
        'create template assignments
        Dim oNode As MSComctlLib.Node
        For Each oNode In oForm.tvGroups.Nodes
            If oNode.Checked Then
                'node is selected - add assignment
                oTemplateDefs.AddAssignment .FileName, _
                    CLng(Mid$(oNode.Key, 2))
            Else
                oTemplateDefs.DeleteAssignment .FileName, _
                    CLng(Mid$(oNode.Key, 2))
            End If
        Next oNode
        
        If oForm.EditedTemplate = Empty Then
            'create template file from blank .dot-
            'get destination location of physical template
            Dim xLoc As String
            If .TemplateType = mpTemplateType_MacPac Then
                xLoc = mpBase2.TemplatesDirectory()
            ElseIf .TemplateType = mpTemplateType_User Then
                xLoc = Word.Options.DefaultFilePath(wdUserTemplatesPath)
            Else
                xLoc = Word.Options.DefaultFilePath(wdWorkgroupTemplatesPath)
            End If
            
            EnsureTrailingSlash xLoc
            
            
            'get destination file - add extension if necessary
            If g_bIsWord12x Then
                xTemplate = xLoc & .FileName & _
                    IIf(LCase(Right$(.FileName, 5)) <> ".dotx", ".dotx", "")
                FileCopy App.Path & "\BlankTemplate.dotx", xTemplate
            Else
                xTemplate = xLoc & .FileName & _
                    IIf(LCase(Right$(.FileName, 4)) <> ".dot", ".dot", "")
                FileCopy App.Path & "\BlankTemplate.dot", xTemplate
            End If
            'copy blank template file
'            MsgBox xTemplate
'            MsgBox App.Path & "\BlankTemplate.dot"
        End If
    End With
    
    Set CreateTemplate = oTemplateDef
    Exit Function
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateTemplate"
    Exit Function
End Function

Private Function CreateCIFormat(oForm As frmCreateCustomTemplate) As Long
'create and save a CIFormat record
    Dim oFormats As mpDB.CCIFormats
    Dim oFormat As mpDB.CCIFormat
    
    On Error GoTo ProcError
    Set oFormats = g_oDB.CIFormats
    Set oFormat = oFormats.Add()
    
    With oForm
        oFormat.BCCNamesOnly = .chkNameOnlyBCC.Value = vbChecked
        oFormat.CCNamesOnly = .chkNamesOnlyCC.Value = vbChecked
        oFormat.DefaultList = .cmbDefaultList.BoundText
        oFormat.Description = .txtTemplateFileName.Text
        oFormat.EmailLabel = .txtEAddressLabel.Text
        oFormat.FaxLabel = .txtFaxLabel.Text
        oFormat.FromNamesOnly = .chkIncludeFrom.Value = vbChecked
        oFormat.IncludeBCC = .chkIncludeBCC.Value = vbChecked
        oFormat.IncludeCC = .chkIncludeCC.Value = vbChecked
        oFormat.IncludeEMail = .chkIncludeEMail.Value = vbChecked
        oFormat.IncludeFax = .chkIncludeFaxNumbers.Value = vbChecked
        oFormat.IncludeFrom = .chkIncludeFrom.Value = vbChecked
        oFormat.IncludePhone = .chkIncludePhoneNumbers.Value = vbChecked
        oFormat.IncludeTo = .chkIncludeTo.Value = vbChecked
        oFormat.OnEmptyAction = 1
        oFormat.PhoneLabel = .txtPhoneLabel.Text
        oFormat.PromptForPhones = .chkPromptForPhones.Value = vbChecked
        oFormat.ToNamesOnly = .chkNamesOnlyTo.Value = vbChecked
    End With
    
    oFormats.Save oFormat
   
    CreateCIFormat = oFormats.ItemAt(oFormats.Count).ID
    
    Exit Function
ProcError:
    RaiseError "mpAdministrator.mdlFc.CreateCIFormat"
    Exit Function
End Function

Public Function RaiseError(ByVal xSource As String)
'   raise the error
    Err.Raise Err.Number, _
              IIf(InStr(Err.Source, "."), Err.Source, xSource), _
              Err.Description
End Function

Public Function GetDelimitedStringItem(ByVal xStr As String, ByVal iItem As Integer, ByVal xDelimiter) As String
'returns the specified item in a delimited string
    Dim iDelimLen As Integer
    Dim iPos1 As Integer
    Dim iPos2 As Integer
    Dim i As Integer
    
    On Error GoTo ProcError
    
    If xStr = Empty Then
        Exit Function
    End If
    
    iDelimLen = Len(xDelimiter)
    
    'add starting delimiter if it doesn't exist
    If Left$(xStr, iDelimLen) <> xDelimiter Then
        xStr = xDelimiter & xStr
    End If
    
    'trim ending delimiter if it exists
    If Right$(xStr, iDelimLen) = xDelimiter Then
        xStr = Left$(xStr, Len(xStr) - iDelimLen)
    End If
    
    'find position of starting delimiter
    iPos1 = 1
    For i = 1 To iItem
        iPos1 = InStr(iPos1, xStr, xDelimiter)
        'set start pos past starting delimiter
        iPos1 = iPos1 + iDelimLen
    Next i
        
    If iPos1 = iDelimLen Then
        Exit Function
    End If
    
    iPos2 = InStr(iPos1, xStr, xDelimiter)
     
    If iPos2 = 0 Then
        'there is no ending delimiter,
        'take until the end of the string
        GetDelimitedStringItem = Mid$(xStr, iPos1)
    Else
        GetDelimitedStringItem = Mid$(xStr, iPos1, iPos2 - iPos1)
    End If
    
    Exit Function
ProcError:
    RaiseError "mpAdministrator.mdlFc.GetDelimitedStringItem"
    Exit Function
End Function

Function DeleteCustomTemplate(ByVal xTemplate As String)

    On Error GoTo ProcError
    On Error Resume Next
    With g_oDB
        'delete custom controls
        .CustomControls(xTemplate).DeleteAll
    
        'delete custom properties
        .CustomProperties(xTemplate).DeleteAll
        
        'delete custom dialogs
        .CustomDialogDefs.Delete xTemplate
    
        'delete letterhead assignments
        .LetterheadDefs(xTemplate).DeleteAssignments xTemplate
        
        With .TemplateDefinitions
            'delete template assignments
            .DeleteAssignments xTemplate
        
            'delete template record
            .Delete xTemplate
        End With
    End With
    
    Exit Function
ProcError:
    RaiseError "mpAdministrator.mdlFc.DeleteCustomTemplate"
    Exit Function
End Function

Sub Main()
    Dim xDB As String
    Dim xTemp As String
    
    On Error GoTo ProcError

'   get current version of Word
    xTemp = GetIni("Application", _
            "WordVersion", App.Path & "\TemplateArchitect.ini")

    If UCase(xTemp) = "12X" Then
        g_bIsWord12x = True
    End If

    Set g_oError = New mpError.CError

    'back up db
    xDB = mpBase2.PublicDB
    Randomize
    g_xDBBak = xDB & Mid$(Rnd(), 2) & ".bak"

    On Error Resume Next
    FileCopy xDB, g_xDBBak

    If Err.Number = 70 Then
'        MsgBox "Could not create a backup copy of '" & xDB & "'.  " & _
'            "Please ensure that both this file and Microsoft Word are not open.", vbExclamation
        MsgBox "Please ensure that both Microsoft Word and the MacPac public database '" & _
                xDB & "' are closed before you use the Template Architect Wizard.", vbExclamation
        Exit Sub
    End If

    'create db object
    Set g_oDB = New mpDB.CDatabase

    If App.StartMode = vbSModeStandalone Then
        Dim oForm As frmCreateCustomTemplate
        Set oForm = New frmCreateCustomTemplate
        oForm.Show
    End If

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Function bIsListControl(ByVal iControlType As Integer) As Boolean
'returns TRUE iff control type represents
'a control that requires a list
    On Error GoTo ProcError
    bIsListControl = iControlType = mpCustomControl_ComboBox Or _
        iControlType = mpCustomControl_ListBox Or _
        iControlType = mpCustomControl_OrdinalDateCombo Or _
        iControlType = mpCustomControl_MultiLineCombo
    Exit Function
ProcError:
    RaiseError "TemplateArchitectWizard.frmCreateCustomTemplate.IsListControl"
    Exit Function
End Function

Public Function GetIni(ByVal xSection As String, _
                        ByVal xKey As String, _
                        ByVal xIni As String) As Variant
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    
    On Error GoTo ProcError
    xValue = String(1024, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
    Exit Function
ProcError:
    RaiseError "mpBase.CIni.GetIni"
    Exit Function
End Function

Public Sub CreateMenuAssignments(xTemplateName As String, bLetterhead)

    Dim xPublicDB As String
    Dim oPublicDB As DAO.Database
    Dim oRS As DAO.Recordset
    Dim xSQL As String
    Dim xItemID As String
    Dim xCaption As String
    Dim xMenuName As String
    Dim lItemPostion As Long
    Dim iBeginGroup As Integer
    Dim xSubmenu As String
    
    On Error GoTo ProcError
    
    xPublicDB = mpBase2.PublicDB
    Set oPublicDB = DBEngine(0).OpenDatabase(xPublicDB, , True)
    If g_bIsWord12x Then
        If bLetterhead = True Then
            xSQL = "SELECT * FROM tblMenuAssignments WHERE fldScope = 'Normal.dotm' OR (fldItemID = 60 AND fldScope = 'Letter.dotx');"
        Else
            xSQL = "SELECT * FROM tblMenuAssignments WHERE fldScope = 'Normal.dotm';"
        End If
    Else
        If bLetterhead = True Then
            xSQL = "SELECT * FROM tblMenuAssignments WHERE fldScope = 'Normal.dot' OR (fldItemID = 60 AND fldScope = 'Letter.dot');"
        Else
            xSQL = "SELECT * FROM tblMenuAssignments WHERE fldScope = 'Normal.dot';"
        End If
    End If
    
    Set oRS = oPublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
    With oRS
        On Error Resume Next
        If .RecordCount Then
            .MoveFirst
            While Not .EOF
                xItemID = oRS!fldItemID
                xCaption = oRS!fldCaption
                xMenuName = oRS!fldMenuName
                lItemPostion = oRS!fldItemPosition
                If oRS!fldBeginGroup = True Then
                    iBeginGroup = 1
                Else
                    iBeginGroup = 0
                End If
                If IsNull(oRS!fldItemParent) Then
                    xSubmenu = ""
                Else
                    xSubmenu = oRS!fldItemParent
                End If
                    
                xSQL = "INSERT INTO tblMenuAssignments(fldItemID, fldCaption, fldMenuName," & _
                    "fldScope, fldItemPosition, fldBeginGroup, fldItemParent) VALUES (""" & _
                    xItemID & """,""" & _
                    xCaption & """,""" & _
                    xMenuName & """,""" & _
                    xTemplateName & """," & _
                    lItemPostion & "," & _
                    iBeginGroup & ",""" & _
                    xSubmenu & """)"
                
                Debug.Print xSQL
                oPublicDB.Execute xSQL
                .MoveNext
            Wend
            Set oRS = Nothing
            
        End If
        On Error GoTo ProcError
        Set oRS = Nothing
    End With
    Exit Sub
ProcError:
    RaiseError "mpBase.CIni.GetIni"
    Exit Sub
    
End Sub
