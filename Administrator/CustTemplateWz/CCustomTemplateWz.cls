VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomTemplateWz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Sub Run()
    Dim oForm As frmCreateCustomTemplate
    Dim oGenericForm As Form
    
    
    On Error GoTo ProcError
    For Each oGenericForm In Forms
        If oGenericForm.Name = "frmCreateCustomTemplate" Then
            Exit Sub
        End If
    Next oGenericForm
    
    
    Set oForm = New frmCreateCustomTemplate
    oForm.Show vbModal, Screen.ActiveForm
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub
