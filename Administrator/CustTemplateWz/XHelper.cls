VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "XHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "APEX XArray helper"
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   XHelper Class
'   Contains properties and methods that
'   sort and find items in an XArray object
'**********************************************************

Option Explicit

Public Function FindFirst(vArray As XArray, SearchString As Variant, Col As Integer) As Long
    Dim lRow As Long, icol As Integer
    Dim lRowFound As Long
    
    Screen.MousePointer = vbHourglass
    lRowFound = -1
    For lRow = vArray.LowerBound(1) To vArray.UpperBound(1)
        If vArray(lRow, Col) = SearchString Then
            lRowFound = lRow
            Exit For
        End If
    Next lRow
    FindFirst = lRowFound
    Screen.MousePointer = vbDefault
End Function

Public Sub Sort(vArray As XArray, SortCol As Integer, Optional Order As Variant)
    Screen.MousePointer = vbHourglass
    If IsMissing(Order) Then Order = "ASC"
    QuickSort vArray, vArray.LowerBound(1), vArray.UpperBound(1), SortCol, CStr(Order)
    Screen.MousePointer = vbDefault
End Sub

Private Sub QuickSort(vArray As XArray, ByVal First As Long, ByVal Last As Long, Col As Integer, Order As String)
    Dim PivotIndex As Long
    
    If First < Last Then
        PivotIndex = lPartition(vArray, First, Last, Col, Order)
        QuickSort vArray, First, PivotIndex - 1, Col, Order
        QuickSort vArray, PivotIndex, Last, Col, Order
    End If
End Sub

Private Function lPartition(vArray As XArray, ByVal First As Long, ByVal Last As Long, Col As Integer, Order As String) As Long
    Dim Pivot As Variant, vTemp As Variant
    Dim i As Integer
    Dim LB As Integer, UB As Integer
    
    LB = vArray.LowerBound(2)
    UB = vArray.UpperBound(2)
    Pivot = vArray((First + Last) \ 2, Col)
    While First <= Last
        If UCase(Order) = "DESC" Then
            While vArray(First, Col) > Pivot
                First = First + 1
            Wend
            
            While vArray(Last, Col) < Pivot
                Last = Last - 1
            Wend
        Else
            While vArray(First, Col) < Pivot
                First = First + 1
            Wend
            
            While vArray(Last, Col) > Pivot
                Last = Last - 1
            Wend
        End If
        
        If First <= Last Then
            For i = LB To UB
                vTemp = vArray(First, i)
                vArray(First, i) = vArray(Last, i)
                vArray(Last, i) = vTemp
            Next i
            First = First + 1
            Last = Last - 1
        End If
    Wend
    lPartition = First
End Function

