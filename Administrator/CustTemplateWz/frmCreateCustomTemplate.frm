VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Begin VB.Form frmCreateCustomTemplate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MacPac Custom Template Wizard"
   ClientHeight    =   6432
   ClientLeft      =   900
   ClientTop       =   888
   ClientWidth     =   10116
   FillStyle       =   0  'Solid
   Icon            =   "frmCreateCustomTemplate.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6432
   ScaleWidth      =   10116
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraWizardPanel 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   240
      Index           =   0
      Left            =   9600
      TabIndex        =   91
      Top             =   3180
      Width           =   6435
      Begin VB.PictureBox pctMPLogo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   3360
         Left            =   5280
         Picture         =   "frmCreateCustomTemplate.frx":1DA2
         ScaleHeight     =   3360
         ScaleWidth      =   3468
         TabIndex        =   118
         Top             =   120
         Width           =   3465
      End
      Begin VB.PictureBox pctTSGInc 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   450
         Left            =   240
         Picture         =   "frmCreateCustomTemplate.frx":2BE9
         ScaleHeight     =   456
         ScaleWidth      =   3876
         TabIndex        =   117
         Top             =   4770
         Width           =   3870
      End
      Begin VB.PictureBox pctTSGTagline 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   285
         Picture         =   "frmCreateCustomTemplate.frx":37A2
         ScaleHeight     =   396
         ScaleWidth      =   4308
         TabIndex        =   116
         Top             =   5190
         Width           =   4305
      End
      Begin VB.PictureBox pctTSGLogo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   465
         Left            =   4050
         Picture         =   "frmCreateCustomTemplate.frx":4170
         ScaleHeight     =   468
         ScaleWidth      =   576
         TabIndex        =   115
         Top             =   4590
         Width           =   570
      End
      Begin VB.OptionButton optDelete 
         BackColor       =   &H00FFFFFF&
         Caption         =   "&Delete the selected existing custom template:"
         Height          =   345
         Left            =   5400
         TabIndex        =   114
         Top             =   4710
         Width           =   3615
      End
      Begin VB.OptionButton optEditExisting 
         BackColor       =   &H00FFFFFF&
         Caption         =   "&Edit the selected existing custom template:"
         Height          =   345
         Left            =   5400
         TabIndex        =   101
         Top             =   4365
         Width           =   3435
      End
      Begin VB.OptionButton optCreateNew 
         BackColor       =   &H00FFFFFF&
         Caption         =   "&Create a new custom template"
         Height          =   375
         Left            =   5400
         TabIndex        =   100
         Top             =   4020
         Value           =   -1  'True
         Width           =   2700
      End
      Begin TrueDBList60.TDBCombo cmbTemplate 
         Height          =   360
         Left            =   5625
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":464F
         TabIndex        =   102
         Top             =   5145
         Width           =   2355
      End
      Begin VB.Label lblWelcome 
         BackStyle       =   0  'Transparent
         Caption         =   $"frmCreateCustomTemplate.frx":6566
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2685
         Left            =   270
         TabIndex        =   112
         Top             =   960
         Width           =   3990
      End
      Begin VB.Label lblTaskQuestion 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "What would you like to do?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   5325
         TabIndex        =   103
         Top             =   3735
         Width           =   2715
      End
      Begin VB.Label lblWelcomeTitle 
         BackStyle       =   0  'Transparent
         Caption         =   "Welcome to the MacPac Template Architect Wizard"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Left            =   285
         TabIndex        =   92
         Top             =   255
         Width           =   3480
      End
   End
   Begin VB.Frame fraWizardPanel 
      BackColor       =   &H80000018&
      Caption         =   "Step 4 - Select/Create the elements that are in your document"
      Enabled         =   0   'False
      Height          =   315
      Index           =   4
      Left            =   9870
      TabIndex        =   107
      Top             =   2475
      Visible         =   0   'False
      Width           =   6795
      Begin VB.ListBox lstProperties 
         Appearance      =   0  'Flat
         Height          =   3480
         Left            =   3780
         TabIndex        =   48
         Tag             =   "PropertyList"
         Top             =   570
         Width           =   2700
      End
      Begin VB.CommandButton btnDelete 
         Caption         =   "&Delete"
         Height          =   690
         Left            =   2430
         Picture         =   "frmCreateCustomTemplate.frx":6713
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   4620
         Width           =   855
      End
      Begin VB.CommandButton btnSave 
         Caption         =   "&Save"
         Height          =   690
         Left            =   1455
         Picture         =   "frmCreateCustomTemplate.frx":6AC9
         Style           =   1  'Graphical
         TabIndex        =   51
         Top             =   4620
         Width           =   855
      End
      Begin VB.CommandButton btnNew 
         Caption         =   "&New"
         Height          =   690
         Left            =   480
         Picture         =   "frmCreateCustomTemplate.frx":6FCB
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   4620
         Width           =   855
      End
      Begin VB.TextBox txtUnderlineLength 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   1590
         TabIndex        =   43
         Tag             =   "PropertyUnderlineLength"
         Top             =   3165
         Width           =   480
      End
      Begin VB.TextBox txtMacro 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   225
         TabIndex        =   46
         Tag             =   "MacroName"
         Top             =   3855
         Width           =   3390
      End
      Begin VB.CheckBox chkWholeParagraph 
         Appearance      =   0  'Flat
         BackColor       =   &H80000016&
         Caption         =   "Delete the paragraph when value is &empty"
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   255
         TabIndex        =   41
         Tag             =   "PropertyWholeParagraph"
         Top             =   2625
         Width           =   3420
      End
      Begin VB.CheckBox chkIndexed 
         BackColor       =   &H80000018&
         Caption         =   "Is &Indexed"
         Height          =   315
         Left            =   1230
         TabIndex        =   49
         Top             =   4080
         Visible         =   0   'False
         Width           =   1320
      End
      Begin VB.TextBox txtPropertyName 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   225
         TabIndex        =   36
         Tag             =   "PropertyName"
         Top             =   570
         Width           =   3390
      End
      Begin MSComCtl2.UpDown udUnderlineLength 
         Height          =   315
         Left            =   2070
         TabIndex        =   44
         TabStop         =   0   'False
         Tag             =   "PropertyUnderlineLength"
         Top             =   3165
         Width           =   240
         _ExtentX        =   445
         _ExtentY        =   550
         _Version        =   393216
         BuddyControl    =   "txtUnderlineLength"
         BuddyDispid     =   196621
         OrigLeft        =   2520
         OrigTop         =   4230
         OrigRight       =   2760
         OrigBottom      =   4485
         Max             =   1000
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin TrueDBList60.TDBCombo cmbAction 
         Height          =   300
         Left            =   225
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":7445
         TabIndex        =   38
         Tag             =   "PropertyAction"
         Top             =   1275
         Width           =   3390
      End
      Begin TrueDBList60.TDBCombo cmbValueSource 
         Height          =   285
         Left            =   240
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":9602
         TabIndex        =   40
         Tag             =   "PropertyValueSource"
         Top             =   2040
         Width           =   3390
      End
      Begin VB.Label lblValueSource 
         BackStyle       =   0  'Transparent
         Caption         =   "&Value Source:"
         Height          =   240
         Left            =   270
         TabIndex        =   39
         Top             =   1815
         Width           =   1095
      End
      Begin VB.Label lblAction 
         BackStyle       =   0  'Transparent
         Caption         =   "&Action:"
         Height          =   240
         Left            =   255
         TabIndex        =   37
         Top             =   1050
         Width           =   1095
      End
      Begin VB.Label lblMacro 
         BackStyle       =   0  'Transparent
         Caption         =   "&Macro Name:"
         Height          =   240
         Left            =   255
         TabIndex        =   45
         Top             =   3630
         Width           =   1095
      End
      Begin VB.Label lblUnderlineLength 
         BackStyle       =   0  'Transparent
         Caption         =   "&Underline Length:"
         Height          =   240
         Left            =   240
         TabIndex        =   42
         Top             =   3210
         Width           =   1350
      End
      Begin VB.Label lblPropertyName 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "P&roperty Name:"
         Height          =   240
         Left            =   255
         TabIndex        =   35
         Top             =   345
         Width           =   1560
      End
      Begin VB.Label lblProperties 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "&Properties:"
         Height          =   255
         Left            =   3795
         TabIndex        =   47
         Top             =   345
         Width           =   855
      End
   End
   Begin VB.Frame fraWizardPanel 
      BackColor       =   &H80000018&
      Caption         =   "Step 6 - Create your dialog controls"
      Enabled         =   0   'False
      Height          =   255
      Index           =   6
      Left            =   9360
      TabIndex        =   99
      Top             =   990
      Visible         =   0   'False
      Width           =   5880
      Begin VB.TextBox txtTab2Caption 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   3870
         MaxLength       =   50
         TabIndex        =   5
         Tag             =   "Tab2Caption"
         Top             =   1425
         Width           =   2580
      End
      Begin VB.TextBox txtTab3Caption 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   3870
         MaxLength       =   50
         TabIndex        =   7
         Tag             =   "Tab3Caption"
         Top             =   2115
         Width           =   2580
      End
      Begin VB.TextBox txtTab1Caption 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   3870
         MaxLength       =   50
         TabIndex        =   3
         Tag             =   "Tab1Caption"
         Top             =   735
         Width           =   2580
      End
      Begin MSComctlLib.TreeView tvLetterheads 
         Height          =   4185
         Left            =   240
         TabIndex        =   1
         Tag             =   "LetterheadList"
         Top             =   735
         Width           =   3405
         _ExtentX        =   6011
         _ExtentY        =   7387
         _Version        =   393217
         Indentation     =   353
         LabelEdit       =   1
         Style           =   7
         Checkboxes      =   -1  'True
         BorderStyle     =   1
         Appearance      =   0
      End
      Begin VB.Label lblTab1Caption 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "Tab &1 Caption:"
         Height          =   270
         Left            =   3900
         TabIndex        =   2
         Top             =   525
         Width           =   1110
      End
      Begin VB.Label lblTab2Caption 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "Tab &2 Caption:"
         Height          =   270
         Left            =   3900
         TabIndex        =   4
         Top             =   1215
         Width           =   1110
      End
      Begin VB.Label lblTab3Caption 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "Tab &3 Caption:"
         Height          =   270
         Left            =   3900
         TabIndex        =   6
         Top             =   1905
         Width           =   1110
      End
      Begin VB.Label lblLetterheads 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "&Letterheads:"
         Height          =   255
         Left            =   270
         TabIndex        =   0
         Top             =   510
         Width           =   900
      End
   End
   Begin VB.CommandButton btnFinish 
      Caption         =   "&Finish"
      Enabled         =   0   'False
      Height          =   375
      Left            =   7605
      TabIndex        =   110
      Top             =   5895
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   8700
      TabIndex        =   111
      Top             =   5895
      Width           =   1100
   End
   Begin VB.CommandButton btnNext 
      Caption         =   "Ne&xt >"
      Height          =   375
      Left            =   6510
      TabIndex        =   109
      Top             =   5895
      Width           =   1100
   End
   Begin VB.Frame fraWizardPanel 
      BackColor       =   &H80000018&
      Caption         =   "Step 2 - Assign the template to groups"
      Enabled         =   0   'False
      Height          =   240
      Index           =   2
      Left            =   9780
      TabIndex        =   93
      Top             =   405
      Visible         =   0   'False
      Width           =   4920
      Begin MSComctlLib.TreeView tvGroups 
         Height          =   4080
         Left            =   1635
         TabIndex        =   74
         Tag             =   "AssignTemplateTree"
         Top             =   765
         Width           =   3345
         _ExtentX        =   5906
         _ExtentY        =   7197
         _Version        =   393217
         Indentation     =   529
         Style           =   7
         Checkboxes      =   -1  'True
         BorderStyle     =   1
         Appearance      =   0
      End
      Begin VB.Label lblGroups 
         BackStyle       =   0  'Transparent
         Caption         =   "&Assign the template to the following groups:"
         Height          =   225
         Left            =   1665
         TabIndex        =   73
         Top             =   540
         Width           =   3480
      End
   End
   Begin VB.CommandButton btnBack 
      Caption         =   "< &Back"
      Enabled         =   0   'False
      Height          =   375
      Left            =   5415
      TabIndex        =   108
      Top             =   5895
      Width           =   1100
   End
   Begin VB.Frame fraWizardPanel 
      BackColor       =   &H80000018&
      Caption         =   "Step 1 - Create A Template"
      Height          =   240
      Index           =   1
      Left            =   9780
      TabIndex        =   90
      Top             =   2190
      Visible         =   0   'False
      Width           =   8775
      Begin VB.CheckBox chkUseAuthors 
         Appearance      =   0  'Flat
         BackColor       =   &H80000016&
         Caption         =   "&Use MacPac authors in this template"
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   870
         TabIndex        =   88
         Tag             =   "UseMacPacAuthors"
         Top             =   4590
         Value           =   1  'Checked
         Width           =   2970
      End
      Begin VB.CheckBox chkUseCI 
         Appearance      =   0  'Flat
         BackColor       =   &H80000016&
         Caption         =   "Use Contact &Integration in this template"
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   870
         TabIndex        =   89
         Tag             =   "UseCI"
         Top             =   5025
         Width           =   3150
      End
      Begin MSComDlg.CommonDialog cdlgBrowse 
         Left            =   6360
         Top             =   2550
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.TextBox txtTemplateFileName 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   780
         TabIndex        =   76
         Tag             =   "TemplateFileName"
         Top             =   555
         Width           =   3735
      End
      Begin VB.TextBox txtShortName 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   780
         TabIndex        =   78
         Tag             =   "DialogName"
         Top             =   1248
         Width           =   3735
      End
      Begin VB.TextBox txtDescription 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   780
         ScrollBars      =   2  'Vertical
         TabIndex        =   80
         Tag             =   "DialogDescription"
         Top             =   1956
         Width           =   3735
      End
      Begin VB.TextBox txtBPSource 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   780
         TabIndex        =   84
         Tag             =   $"frmCreateCustomTemplate.frx":B7C4
         Top             =   3387
         Width           =   3735
      End
      Begin VB.CommandButton btnBrowseBPFile 
         Caption         =   "Bro&wse..."
         Height          =   345
         Left            =   4650
         TabIndex        =   85
         Tag             =   "BrowseBoilerplateSource"
         Top             =   3360
         Width           =   1100
      End
      Begin TrueDBList60.TDBCombo cmbDefaultDocID 
         Height          =   330
         Left            =   780
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":B868
         TabIndex        =   87
         Tag             =   "DefaultDocID"
         Top             =   4050
         Width           =   3735
      End
      Begin TrueDBList60.TDBCombo cmbLocation 
         Height          =   330
         Left            =   780
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":D787
         TabIndex        =   82
         Tag             =   "TemplateLocation"
         Top             =   2664
         Width           =   3735
      End
      Begin VB.Label lblLocation 
         BackStyle       =   0  'Transparent
         Caption         =   "&Location:"
         Height          =   240
         Left            =   825
         TabIndex        =   81
         Top             =   2445
         Width           =   1095
      End
      Begin VB.Label lblDefaultDocID 
         BackStyle       =   0  'Transparent
         Caption         =   "Default Do&cument ID:"
         Height          =   300
         Left            =   825
         TabIndex        =   86
         Top             =   3840
         Width           =   1590
      End
      Begin VB.Label lblTemplateFileName 
         BackStyle       =   0  'Transparent
         Caption         =   "&Template File Name:"
         Height          =   240
         Left            =   825
         TabIndex        =   75
         Top             =   330
         Width           =   1530
      End
      Begin VB.Label lblShortName 
         BackStyle       =   0  'Transparent
         Caption         =   "Template &Name:"
         Height          =   240
         Left            =   825
         TabIndex        =   77
         Top             =   1020
         Width           =   2145
      End
      Begin VB.Label lblDescription 
         BackStyle       =   0  'Transparent
         Caption         =   "Template &Description:"
         Height          =   240
         Left            =   825
         TabIndex        =   79
         Top             =   1725
         Width           =   1590
      End
      Begin VB.Label lblBoilerplateFile 
         BackStyle       =   0  'Transparent
         Caption         =   "&Boilerplate File Source:"
         Height          =   300
         Left            =   825
         TabIndex        =   83
         Top             =   3180
         Width           =   1740
      End
   End
   Begin VB.Frame fraWizardPanel 
      BackColor       =   &H80000018&
      Caption         =   "Step 3 - Setup Contact Integration"
      Enabled         =   0   'False
      Height          =   240
      Index           =   3
      Left            =   9510
      TabIndex        =   94
      Top             =   615
      Visible         =   0   'False
      Width           =   5325
      Begin VB.Frame fraShowLists 
         BackColor       =   &H80000016&
         Caption         =   "Show CI Dialog Fields"
         Height          =   1665
         Left            =   240
         TabIndex        =   98
         Top             =   360
         Width           =   3675
         Begin VB.CheckBox chkIncludeTo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "&To"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   180
            TabIndex        =   53
            Tag             =   "ShowToList"
            Top             =   330
            Width           =   825
         End
         Begin VB.CheckBox chkIncludeCC 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "&CC"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   180
            TabIndex        =   55
            Tag             =   "ShowCCList"
            Top             =   945
            Width           =   825
         End
         Begin VB.CheckBox chkIncludeBCC 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "&BCC"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   180
            TabIndex        =   56
            Tag             =   "ShowBCCList"
            Top             =   1245
            Width           =   825
         End
         Begin VB.CheckBox chkIncludeFrom 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "&From"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   180
            TabIndex        =   54
            Tag             =   "ShowFromList"
            Top             =   630
            Width           =   825
         End
         Begin TrueDBList60.TDBCombo cmbDefaultList 
            Height          =   345
            Left            =   1995
            OleObjectBlob   =   "frmCreateCustomTemplate.frx":F946
            TabIndex        =   58
            Tag             =   "DefaultCIList"
            Top             =   525
            Width           =   1125
         End
         Begin VB.Label lblDefaultList 
            BackStyle       =   0  'Transparent
            Caption         =   "Defa&ult List:"
            Height          =   225
            Left            =   2025
            TabIndex        =   57
            Top             =   330
            Width           =   1020
         End
      End
      Begin VB.Frame fraNumbers 
         BackColor       =   &H80000016&
         Caption         =   "Phone/Fax/EMail Numbers"
         Height          =   2400
         Left            =   225
         TabIndex        =   97
         Top             =   2370
         Width           =   6255
         Begin VB.CheckBox chkIncludePhoneNumbers 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "Include &Phone Numbers"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   210
            TabIndex        =   63
            Tag             =   "IncludePhoneNumbers"
            Top             =   420
            Width           =   2055
         End
         Begin VB.CheckBox chkPromptForPhones 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "P&rompt user for phones when necessary"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   210
            TabIndex        =   72
            Tag             =   "PromptForPhones"
            Top             =   1800
            Width           =   3375
         End
         Begin VB.TextBox txtEAddressLabel 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   3870
            TabIndex        =   71
            Tag             =   "EMailLabel"
            Top             =   1185
            Width           =   2160
         End
         Begin VB.TextBox txtFaxLabel 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   3870
            TabIndex        =   68
            Tag             =   "FaxLabel"
            Top             =   780
            Width           =   2160
         End
         Begin VB.TextBox txtPhoneLabel 
            Appearance      =   0  'Flat
            Height          =   300
            Left            =   3870
            TabIndex        =   65
            Tag             =   "PhoneLabel"
            Top             =   390
            Width           =   2160
         End
         Begin VB.CheckBox chkIncludeEMail 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "Include E-Mail &Addresses"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   210
            TabIndex        =   69
            Tag             =   "IncludeEMailAddresses"
            Top             =   1200
            Width           =   2355
         End
         Begin VB.CheckBox chkIncludeFaxNumbers 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "Include Fax &Numbers"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   210
            TabIndex        =   66
            Tag             =   "IncludeFaxNumbers"
            Top             =   825
            Width           =   2355
         End
         Begin VB.Label lblPhoneLabel 
            BackStyle       =   0  'Transparent
            Caption         =   "P&hone Label:"
            Height          =   225
            Left            =   2790
            TabIndex        =   64
            Top             =   450
            Width           =   1020
         End
         Begin VB.Label lblEMailAddressLabel 
            BackStyle       =   0  'Transparent
            Caption         =   "E-Ma&il Label:"
            Height          =   225
            Left            =   2790
            TabIndex        =   70
            Top             =   1230
            Width           =   975
         End
         Begin VB.Label lblFaxLabel 
            BackStyle       =   0  'Transparent
            Caption         =   "Fax &Label:"
            Height          =   225
            Left            =   2790
            TabIndex        =   67
            Top             =   855
            Width           =   1020
         End
      End
      Begin VB.Frame fraReturnNamesOnly 
         BackColor       =   &H80000016&
         Caption         =   "Return Names Only For"
         Height          =   1665
         Left            =   4110
         TabIndex        =   96
         Top             =   360
         Width           =   2370
         Begin VB.CheckBox chkNamesOnlyFrom 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "'Fro&m' Field"
            Enabled         =   0   'False
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   180
            TabIndex        =   60
            Tag             =   "NamesOnlyForFromList"
            Top             =   630
            Width           =   1155
         End
         Begin VB.CheckBox chkNameOnlyBCC 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "'BCC' Fi&eld"
            Enabled         =   0   'False
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   180
            TabIndex        =   62
            Tag             =   "NamesOnlyForBCCList"
            Top             =   1245
            Width           =   1155
         End
         Begin VB.CheckBox chkNamesOnlyCC 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "'CC' Fiel&d"
            Enabled         =   0   'False
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   180
            TabIndex        =   61
            Tag             =   "NamesOnlyForCCList"
            Top             =   940
            Width           =   1155
         End
         Begin VB.CheckBox chkNamesOnlyTo 
            Appearance      =   0  'Flat
            BackColor       =   &H80000016&
            Caption         =   "'T&o' Field"
            ForeColor       =   &H80000008&
            Height          =   270
            Left            =   180
            TabIndex        =   59
            Tag             =   "NamesOnlyForToList"
            Top             =   330
            Width           =   1155
         End
      End
   End
   Begin VB.Frame fraWizardPanel 
      BackColor       =   &H80000018&
      Caption         =   "Step 5 - Create your dialog box"
      Enabled         =   0   'False
      Height          =   270
      Index           =   5
      Left            =   9870
      TabIndex        =   95
      Top             =   1635
      Visible         =   0   'False
      Width           =   6825
      Begin VB.CommandButton btnAddList 
         Caption         =   "Add L&ist..."
         Height          =   390
         Left            =   5490
         TabIndex        =   113
         TabStop         =   0   'False
         Top             =   5055
         Width           =   1100
      End
      Begin VB.TextBox txtControlLabel 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   3465
         MaxLength       =   18
         TabIndex        =   17
         Tag             =   "Label"
         Top             =   1605
         Width           =   3060
      End
      Begin VB.TextBox txtNumLines 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   3450
         TabIndex        =   19
         Tag             =   "ControlNumberOfLines"
         Text            =   "1"
         Top             =   2205
         Width           =   780
      End
      Begin VB.CheckBox chkIsSticky 
         Appearance      =   0  'Flat
         BackColor       =   &H80000016&
         Caption         =   "Is Stick&y"
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   3510
         TabIndex        =   34
         Tag             =   "ControlIsSticky"
         Top             =   5115
         Width           =   1080
      End
      Begin VB.TextBox txtSpaceBefore 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   4785
         TabIndex        =   22
         Tag             =   "ControlSpaceBefore"
         Text            =   "300"
         Top             =   2205
         Width           =   855
      End
      Begin VB.CommandButton btnUp 
         Height          =   300
         Left            =   3015
         Picture         =   "frmCreateCustomTemplate.frx":11B08
         Style           =   1  'Graphical
         TabIndex        =   12
         Tag             =   "MoveControlUp"
         Top             =   1815
         Width           =   300
      End
      Begin VB.CommandButton btnDown 
         Height          =   300
         Left            =   3015
         Picture         =   "frmCreateCustomTemplate.frx":11D7A
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   2175
         Width           =   300
      End
      Begin VB.TextBox txtDialogTitle 
         Appearance      =   0  'Flat
         Height          =   300
         Left            =   210
         MaxLength       =   50
         TabIndex        =   9
         Tag             =   "DialogTitle"
         Top             =   420
         Width           =   6255
      End
      Begin MSComCtl2.UpDown udNumLines 
         Height          =   300
         Left            =   4230
         TabIndex        =   20
         TabStop         =   0   'False
         Tag             =   "ControlNumberOfLines"
         Top             =   2205
         Width           =   240
         _ExtentX        =   445
         _ExtentY        =   529
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtNumLines"
         BuddyDispid     =   196681
         OrigLeft        =   2520
         OrigTop         =   4230
         OrigRight       =   2760
         OrigBottom      =   4485
         Max             =   20
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown udSpaceBefore 
         Height          =   300
         Left            =   5625
         TabIndex        =   23
         TabStop         =   0   'False
         Tag             =   "ControlSpaceBefore"
         Top             =   2205
         Width           =   240
         _ExtentX        =   445
         _ExtentY        =   529
         _Version        =   393216
         BuddyControl    =   "txtSpaceBefore"
         BuddyDispid     =   196683
         OrigLeft        =   2520
         OrigTop         =   4230
         OrigRight       =   2760
         OrigBottom      =   4485
         Increment       =   25
         Max             =   1000
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin TrueDBGrid60.TDBGrid grdControls 
         Height          =   3450
         Left            =   240
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":11FEC
         TabIndex        =   11
         Tag             =   "ControlList"
         Top             =   1005
         Width           =   2760
      End
      Begin TrueDBList60.TDBCombo cmbControlType 
         Height          =   345
         Left            =   3480
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":14F31
         TabIndex        =   15
         Tag             =   "ControlType"
         Top             =   975
         Width           =   3060
      End
      Begin TrueDBList60.TDBCombo cmbListSource 
         Height          =   345
         Left            =   3465
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":16E43
         TabIndex        =   25
         Tag             =   "ControlListSource"
         Top             =   2820
         Width           =   3060
      End
      Begin TrueDBList60.TDBCombo cmbDateFormat 
         Height          =   345
         Left            =   3465
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":19004
         TabIndex        =   27
         Tag             =   "ControlDateFormat"
         Top             =   3450
         Width           =   3060
      End
      Begin TrueDBList60.TDBCombo cmbCIList 
         Height          =   345
         Left            =   3465
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":1B1C5
         TabIndex        =   33
         Tag             =   "ControlCIList"
         Top             =   4710
         Width           =   3060
      End
      Begin TrueDBList60.TDBCombo cmbCMProperty 
         Height          =   345
         Left            =   3450
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":1D382
         TabIndex        =   29
         Tag             =   "ControlCMProperty"
         Top             =   4110
         Width           =   3060
      End
      Begin TrueDBList60.TDBCombo cmbCMRelatedField 
         Height          =   345
         Left            =   5145
         OleObjectBlob   =   "frmCreateCustomTemplate.frx":1F543
         TabIndex        =   31
         Tag             =   "ControlCMRelatedField"
         Top             =   4110
         Visible         =   0   'False
         Width           =   1380
      End
      Begin VB.Label lblCMRelatedDataIndex 
         BackStyle       =   0  'Transparent
         Caption         =   " &Related FieldIndex:"
         Height          =   240
         Left            =   5145
         TabIndex        =   30
         Top             =   3900
         Visible         =   0   'False
         Width           =   1425
      End
      Begin VB.Label lblCMProperty 
         BackStyle       =   0  'Transparent
         Caption         =   "Client &Matter Property:"
         Height          =   240
         Left            =   3465
         TabIndex        =   28
         Top             =   3900
         Width           =   1620
      End
      Begin VB.Label lblControlType 
         BackStyle       =   0  'Transparent
         Caption         =   "Ty&pe:"
         Height          =   240
         Left            =   3465
         TabIndex        =   14
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblControlLabel 
         BackStyle       =   0  'Transparent
         Caption         =   "L&abel:"
         Height          =   240
         Left            =   3465
         TabIndex        =   16
         Top             =   1395
         Width           =   735
      End
      Begin VB.Label lblListSource 
         BackStyle       =   0  'Transparent
         Caption         =   "&List Source:"
         Height          =   240
         Left            =   3465
         TabIndex        =   24
         Top             =   2610
         Width           =   1095
      End
      Begin VB.Label lblNumLines 
         BackStyle       =   0  'Transparent
         Caption         =   "&No. of Lines:"
         Height          =   240
         Left            =   3465
         TabIndex        =   18
         Top             =   1995
         Width           =   1020
      End
      Begin VB.Label lblDateFormat 
         BackStyle       =   0  'Transparent
         Caption         =   "&Date Format:"
         Height          =   240
         Left            =   3465
         TabIndex        =   26
         Top             =   3255
         Width           =   1095
      End
      Begin VB.Label lblCIList 
         BackStyle       =   0  'Transparent
         Caption         =   "C&I List:"
         Height          =   240
         Left            =   3480
         TabIndex        =   32
         Top             =   4515
         Width           =   570
      End
      Begin VB.Label lblControls 
         BackStyle       =   0  'Transparent
         Caption         =   "&Controls:"
         Height          =   240
         Left            =   270
         TabIndex        =   10
         Top             =   780
         Width           =   1095
      End
      Begin VB.Label lblSpaceBefore 
         BackStyle       =   0  'Transparent
         Caption         =   "&Space Before:"
         Height          =   240
         Left            =   4800
         TabIndex        =   21
         Top             =   1995
         Width           =   1095
      End
      Begin VB.Label lblDialogTitle 
         BackColor       =   &H80000018&
         BackStyle       =   0  'Transparent
         Caption         =   "Dialog &Title:"
         Height          =   270
         Left            =   270
         TabIndex        =   8
         Top             =   210
         Width           =   1110
      End
   End
   Begin VB.Shape shpWizardButtons 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H8000000F&
      FillStyle       =   0  'Solid
      Height          =   1050
      Left            =   -60
      Top             =   5640
      Width           =   9435
   End
   Begin VB.Label lblPanelHelpDescription 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H80000008&
      Height          =   4110
      Left            =   120
      TabIndex        =   106
      Top             =   720
      Width           =   2865
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblControlHelp 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H000000FF&
      Height          =   1425
      Left            =   120
      TabIndex        =   105
      Top             =   4935
      Width           =   2865
   End
   Begin VB.Label lblPanelHelpTitle 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   555
      Left            =   120
      TabIndex        =   104
      Top             =   150
      Width           =   2385
   End
   Begin VB.Shape shpHelpBar 
      BorderStyle     =   0  'Transparent
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  'Solid
      Height          =   6495
      Left            =   -30
      Top             =   -30
      Width           =   3105
   End
End
Attribute VB_Name = "frmCreateCustomTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum Steps
    StepWelcome = 0
    StepCreateTemplate
    StepAssignTemplate
    StepSetupCI
    StepCreateProperties
    StepCreateDialog
    StepCreateDialog2
    StepConfirmCreate
End Enum

Private Const VALUE_SRC_DLG_CTL_PATTERN As String = "*|Dialog|*"

Private m_iCurStep As Steps
Private m_bSkipSave As Boolean
Private m_bCancelled As Boolean
Private m_bShortNameEdited As Boolean
Private m_bDescriptionEdited As Boolean
Private m_bCustPropDirty As Boolean
Private m_xPropDef() As String
Private m_xPropDefID() As Long
Private m_xCtlDef() As String
Private m_iCurIndex As Integer
Private m_xPropertyBookmark As String
'Fix for #4048
Private m_PanelHelpDescTop As Integer '*c
Private m_PanelHelpDescHeight As Integer '*c
Private m_LabelControlHelpTop As Integer '*c

Public Property Get EditedTemplate() As String
'returns name of template that we're editing-
'returns an empty string if we're creating a new template
    On Error GoTo ProcError
    If Me.optEditExisting Then
        EditedTemplate = Me.cmbTemplate.BoundText
    End If
    Exit Property
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.EditedTemplate"
    Exit Property
End Property

Public Property Get PropertyDefinitions() As String()
    PropertyDefinitions = m_xPropDef
End Property

Public Property Get Cancelled() As Boolean
    On Error GoTo ProcError
    Cancelled = m_bCancelled
    Exit Property
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.Cancelled"
    Exit Property
End Property

Private Sub btnAddList_Click()
    AddList
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub AddList()
    Dim oLC As mpListCreator.CList
    Dim xList As String
    
    On Error GoTo ProcError
    Set oLC = New mpListCreator.CList
    xList = oLC.Create()
    If xList <> Empty Then
        With Me.cmbControlType
            'refresh list and set to display appropriate lists
            SetListsCombo (.BoundText = _
                mpDB.mpCustomControl_OrdinalDateCombo), True
        
            If bIsListControl(.BoundText) Then
                Me.cmbListSource.BoundText = xList
            End If
        End With
    End If
    Exit Sub
ProcError:
    RaiseError "TemplateArchitectWizard.frmCreateCustomTemplate.AddList"
    Exit Sub
End Sub

Private Sub btnBack_Click()
    On Error GoTo ProcError
    GoToPreviousStep
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub GoToPreviousStep()
    On Error GoTo ProcError
    
    'ensure that every that needs to happen happens
    FinalizeStep m_iCurStep
    
    'hide current panel
    Me.fraWizardPanel(m_iCurStep).Visible = False
    
    'get previous panel - make current
    If (Me.chkUseCI.Value = vbUnchecked) And _
        (m_iCurStep = StepSetupCI + 1) Then
        m_iCurStep = mpBase2.Max(m_iCurStep - 2, 1)
    Else
        m_iCurStep = mpBase2.Max(m_iCurStep - 1, 1)
    End If
    
    'make new current panel visible
    Me.fraWizardPanel(m_iCurStep).Visible = True
    Me.fraWizardPanel(m_iCurStep).ZOrder 0
    
    'set next, previous buttons
    Me.btnBack.Enabled = m_iCurStep > Steps.StepCreateTemplate
    Me.btnNext.Enabled = m_iCurStep < Me.fraWizardPanel.UBound
    
    'set help text for new current panel
    SetPanelHelp m_iCurStep
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.GoToPreviousStep"
    Exit Sub
End Sub

Private Sub btnBrowseBPFile_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnCancel_Click()
    Unload Me
End Sub

Private Sub btnDelete_Click()
    On Error GoTo ProcError
    DeleteProperty
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub DeleteProperty()
'deletes the selected property
    Dim iChoice As VbMsgBoxResult
    Dim iIndex As Integer
    
    On Error GoTo ProcError
    
        With Me.lstProperties
            iIndex = .ListIndex
            If iIndex <> -1 Then
                'confirm deletion
                iChoice = MsgBox("Delete the selected property?", _
                    vbQuestion + vbYesNo, MSGBOX_TITLE)
                    
                If iChoice = vbYes Then
'                    'remove from property definitions array
'                    m_xPropDef(.ItemData(iIndex)) = Empty
                    
                    'remove from list
                    .RemoveItem iIndex
                    
                    'select a new item
                    If .ListCount > 0 Then
                        .ListIndex = Min(CDbl(iIndex), .ListCount - 1)
                        ParsePropertyDefinition
                    End If
                End If
            Else
                MsgBox "You must select a property to delete.", _
                    vbExclamation, MSGBOX_TITLE
            End If
        End With
        m_bCustPropDirty = False
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.DeleteProperty"
    Exit Sub
End Sub

Private Sub btnFinish_Click()
    On Error GoTo ProcError
    Dim i As Integer
    
    'validate each step again
    For i = StepCreateTemplate To StepConfirmCreate
        If Not StepIsValid(i) Then
            Exit Sub
        End If
    Next i
        
    Me.Hide
    DoEvents
    CreateCustomTemplate Me
    Unload Me
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnNew_Click()
    On Error GoTo ProcError
    If PromptToSaveIfNecessary() Then
        ResetPropertyControls
        Me.txtPropertyName.Enabled = True
        Me.lblPropertyName.Enabled = True
        Me.txtPropertyName.SetFocus
        m_bCustPropDirty = False
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub ResetPropertyControls()
'resets the controls in step 4 to add a new property
    On Error GoTo ProcError
    m_xPropertyBookmark = Empty
    Me.txtPropertyName.Text = Empty
    Me.chkWholeParagraph.Value = vbUnchecked
    Me.txtUnderlineLength.Text = "0"
    Me.chkIndexed.Value = vbUnchecked
    Me.txtMacro.Text = Empty
    Me.cmbValueSource.BoundText = "Dialog"
    Me.cmbAction.BoundText = "0"
    DoEvents
    Me.lstProperties.ListIndex = -1
    m_bCustPropDirty = False
    m_iCurIndex = -1
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.ResetPropertyControls"
    Exit Sub
End Sub

Private Sub btnNext_Click()
    On Error GoTo ProcError
    GoToNextStep
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub GoToNextStep()
    Static bIsReadyToGo As Boolean
    Dim oArray As XArray
    Dim iChoice As VbMsgBoxResult
    
    On Error GoTo ProcError
    
    If Me.optDelete.Value = True Then
        'prompt to delete
        iChoice = MsgBox("Delete the selected template?", vbQuestion + vbYesNo)
        If iChoice = vbYes Then
            'delete template
            With Me.cmbTemplate
                DeleteCustomTemplate .BoundText
                Set oArray = .Array
                oArray.Delete 1, .Row + .FirstRow
                .Rebind
                ResizeTDBCombo Me.cmbTemplate, 6
                .SelectedItem = 0
                .BoundText = .Columns(1)
            End With
        End If
        Exit Sub
    End If
    
    'tidy up the current step
    FinalizeStep m_iCurStep
        
    If StepIsValid(m_iCurStep) Then
        With Me.fraWizardPanel
            'current step is valid, goto next panel
            .Item(m_iCurStep).Visible = False
            
            'skip ci step if specified and
            'the next panel is the ci step
            If Me.chkUseCI.Value = vbUnchecked And _
                m_iCurStep = StepSetupCI - 1 Then
                m_iCurStep = m_iCurStep + 2
            Else
                'set next panel as current panel
                m_iCurStep = m_iCurStep + 1
            End If
            
            
            'show panel and bring to front
            With .Item(m_iCurStep)
                'move off screen to prevent odd flashing
                'when controls are made visible/invisible
                .Top = 20000
                .Visible = True
                .Enabled = True
                .ZOrder 0
                .Left = Me.shpHelpBar.Left + Me.shpHelpBar.Width
                .Width = Me.Width - .Left
                .Height = Me.fraWizardPanel.Item(0).Height
                .BorderStyle = 0
                .BackColor = &H8000000F
            End With
            
            DoEvents
            
            'initialize the panel
            InitStep m_iCurStep
            SetPanelHelp m_iCurStep
            
            If m_iCurStep = StepCreateDialog2 Then
                bIsReadyToGo = True
            End If
            
            'make visible
            Me.fraWizardPanel.Item(m_iCurStep).Top = 0
            'force display of grdControl items '*c 4172
            On Error Resume Next
            Me.grdControls.Refresh
            Me.grdControls.Rebind
            On Error GoTo ProcError
            '*c end 4172
           'set 'next' and 'back' buttons
            Me.btnNext.Enabled = m_iCurStep < .UBound
            Me.btnBack.Enabled = m_iCurStep > Steps.StepCreateTemplate

            Me.btnFinish.Enabled = bIsReadyToGo
        End With
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.GoToNextStep"
    Exit Sub
End Sub

Private Function SaveCustomProperty(iIndex As Integer) As Boolean
'adds custom property definition to list box
    On Error GoTo ProcError
        
    'validate
    If Me.txtPropertyName.Text = Empty Then
        MsgBox "Property Name is required information.", _
            vbExclamation, MSGBOX_TITLE
        Me.txtPropertyName.SetFocus
        Exit Function
    ElseIf Me.lstProperties.ListIndex = -1 Then
        'check for duplicate property names
        With Me.lstProperties
            Dim i As Integer
            For i = 0 To .ListCount - 1
                If Me.txtPropertyName.Text = .List(i) Then
                    MsgBox "A property with name '" & .List(i) & _
                    "' already exists.", vbExclamation, MSGBOX_TITLE
                    Me.txtPropertyName.SetFocus
                    Exit Function
                End If
            Next i
        End With
    End If
    
    'continue validation
    If IsNumeric(Left(Me.txtPropertyName.Text, 1)) Then
        MsgBox "Property Name can't start with a number.", _
            vbExclamation, MSGBOX_TITLE
        Me.txtPropertyName.SetFocus
        Exit Function
    ElseIf Not IsNumeric(Me.txtUnderlineLength) Then
        MsgBox "Underline length must be a number between 0 and 1000.", _
            vbExclamation, MSGBOX_TITLE
        Me.txtUnderlineLength.SetFocus
        Exit Function
    ElseIf CLng(Me.txtUnderlineLength.Text) > 1000 Or _
        CLng(Me.txtUnderlineLength.Text) < 0 Then
        MsgBox "Underline length must be a number between 0 and 1000.", _
            vbExclamation, MSGBOX_TITLE
        Me.txtUnderlineLength.SetFocus
        Exit Function
    End If
    
    'clear dirt flag
    m_bCustPropDirty = False
    
    With Me.lstProperties
        If iIndex = -1 Then
            If .ListCount = 0 Then
                'we're adding the first in the list-
                'create a 1 slot array
                ReDim m_xPropDef(0)
                ReDim m_xPropDefID(0)
            Else
                'add a slot to the array for the new definition
                ReDim Preserve m_xPropDef(UBound(m_xPropDef) + 1)
                ReDim Preserve m_xPropDefID(UBound(m_xPropDefID) + 1)
            End If
            
            'add new item
            .AddItem Me.txtPropertyName.Text
            
            'specify as item data the index of the array that will
            'contain the definition of the property
            .ItemData(.ListCount - 1) = UBound(m_xPropDef)
            .ListIndex = .ListCount - 1
        
            'save definition to the new slot in the array
            m_xPropDef(UBound(m_xPropDef)) = GetPropertyDefinitionString( _
                Me.txtPropertyName.Text, Me.cmbValueSource.BoundText, _
                 Me.cmbValueSource.Text, Me.chkWholeParagraph.Value, _
                    Me.txtUnderlineLength.Text, Me.chkIndexed, _
                    Me.txtMacro, Me.cmbAction.BoundText, m_xPropertyBookmark)
        Else
            .List(iIndex) = Me.txtPropertyName.Text
            
            'save definition to the new slot specified in ItemData
            m_xPropDef(.ItemData(iIndex)) = GetPropertyDefinitionString( _
                Me.txtPropertyName.Text, Me.cmbValueSource.BoundText, _
                Me.cmbValueSource.Text, Me.chkWholeParagraph.Value, _
                Me.txtUnderlineLength.Text, Me.chkIndexed, _
                Me.txtMacro, Me.cmbAction.BoundText, m_xPropertyBookmark)
        End If
    End With
    
    m_bCustPropDirty = False
    'return true to indicate that item was saved
    SaveCustomProperty = True
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SaveCustomProperty"
    Exit Function
End Function

Private Function GetPropertyDefinitionString(ByVal xName As String, ByVal xValueSourceBound As String, _
    ByVal xValueSourceText As String, ByVal bWholePara As Boolean, ByVal iUnderlineLen As Integer, _
    ByVal bIndexed As Boolean, ByVal xMacro As String, ByVal iAction As mpCustomPropertyActions, ByVal xBkmk As String) As String
    
    If xBkmk = Empty Then
        'create default bookmark name
        xBkmk = "zzmp" & Replace(xName, " ", "_")
    End If
    
    GetPropertyDefinitionString = xName & SEP & _
                        xValueSourceText & SEP & xValueSourceBound & _
                        SEP & CInt(bWholePara) & SEP & _
                        iUnderlineLen & SEP & CInt(bIndexed) & SEP & _
                        xMacro & SEP & iAction & SEP & xBkmk
End Function
Private Sub btnSave_Click()
    Dim bIsNew As Boolean
    Dim bSaved As Boolean
    
    On Error GoTo ProcError
    'if there is no selection, the record
    'that is being saved is new
    bIsNew = Me.lstProperties.ListIndex = -1
    
    bSaved = SaveCustomProperty(m_iCurIndex)
    
    'if the record being saved was new
    'create a new record
    If bSaved And bIsNew Then
        btnNew_Click
    End If
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.btnSave_Click"
    Exit Sub
End Sub

Private Sub btnUp_Click()
    On Error GoTo ProcError
    MoveCurrentRow -1
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub btnDown_Click()
    On Error GoTo ProcError
    MoveCurrentRow 1
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub MoveCurrentRow(ByVal iOffset As Integer)
'moves the current row the specified
'number of rows up (-) or down (+)
    Dim xRowDef As String
    Dim xRowName As String
    Dim iRowTab As Integer
    Dim iDestTab As Integer
    Dim iTab As Integer
    Dim iRow As Integer
    Dim iDestRow As Integer
    Dim oArr As XArray
    
    On Error GoTo ProcError

    With Me.grdControls
        'get the index of the current
        'and destination rows
        iRow = .FirstRow + .Row
        iDestRow = iRow + iOffset
        Dim i As Integer
        Set oArr = .Array
        With oArr
            If (iDestRow < 0) Or (iDestRow > .Count(1) - 1) Then
                'destination row is before the first row,
                'or after the last row - exit
                Exit Sub
            End If
            
            'save any changes that are pending
            SaveControlDefinition iRow
            
             'get info for current row
            iRowTab = CInt(.Value(iRow, 0))
            xRowName = .Value(iRow, 1)
            xRowDef = .Value(iRow, 2)
           
           'get tab of destination row
           iDestTab = .Value(iDestRow, 0)
           
           'delete row
           .Delete 1, iRow
           
           'insert row at offset
           .Insert 1, iDestRow
           
           If iOffset > 0 Then
                'we're moving down - use the larger tab number
                iTab = Max(CDbl(iDestTab), CDbl(iRowTab))
            Else
                'we're moving up - use the smaller tab number
                iTab = Min(CDbl(iDestTab), CDbl(iRowTab))
            End If
            
           'make tab of moved row the
           'same as tab of the displaced row
           .Value(iDestRow, 0) = iTab
           .Value(iDestRow, 1) = xRowName
           .Value(iDestRow, 2) = xRowDef
        End With
        
        .Rebind
        On Error Resume Next '*4348 jsw
        .Row = iDestRow - .FirstRow
        On Error GoTo ProcError '*4348
        m_bSkipSave = True

    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.MoveCurrentRow"
    Exit Sub
End Sub

Private Sub chkIncludeBCC_Click()
    Me.chkNameOnlyBCC.Enabled = Me.chkIncludeBCC.Value = vbChecked
    If Not Me.chkNameOnlyBCC.Enabled Then
        Me.chkNameOnlyBCC.Value = vbUnchecked
    End If
End Sub

Private Sub chkIncludeBCC_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeCC_Click()
    On Error GoTo ProcError
    Me.chkNamesOnlyCC.Enabled = Me.chkIncludeCC.Value = vbChecked
    If Not Me.chkNamesOnlyCC.Enabled Then
        Me.chkNamesOnlyCC.Value = vbUnchecked
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeCC_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeEMail_Click()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeEMail_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeFaxNumbers_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeFrom_Click()
    On Error GoTo ProcError
    Me.chkNamesOnlyFrom.Enabled = Me.chkIncludeFrom.Value = vbChecked
    If Not Me.chkNamesOnlyFrom.Enabled Then
        Me.chkNamesOnlyFrom.Value = vbUnchecked
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeFrom_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludePhoneNumbers_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeTo_Click()
    On Error GoTo ProcError
    Me.chkNamesOnlyTo.Enabled = Me.chkIncludeTo.Value = vbChecked
    If Not Me.chkNamesOnlyTo.Enabled Then
        Me.chkNamesOnlyTo.Value = vbUnchecked
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIncludeTo_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIndexed_Click()
    m_bCustPropDirty = True
End Sub

Private Sub chkIndexed_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkIsSticky_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkNameOnlyBCC_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkNamesOnlyCC_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkNamesOnlyFrom_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkNamesOnlyTo_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkPromptForPhones_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkUseAuthors_Click()
    On Error GoTo ProcError
    SetValueSourceList
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub SetValueSourceList()
    Static oListProps As mpDB.CList
    Static oListOne As mpDB.CList
    On Error GoTo ProcError
    If Me.chkUseAuthors.Value = vbChecked Then
        'use linked properties list
        If oListProps Is Nothing Then
            Set oListProps = g_oDB.Lists.Add("LinkedProperties", _
                "SELECT ' Dialog Control', 'Dialog' " & _
                "FROM tblLinkedProperties UNION SELECT tblLinkedProperties.* FROM tblLinkedProperties")
        End If
        
        oListProps.IsDBList = True
        oListProps.Refresh
        Me.cmbValueSource.Array = oListProps.ListItems.Source
        Me.cmbValueSource.Rebind
        ResizeTDBCombo Me.cmbValueSource, 12
    Else
        'check for existing properties whose value source is something
        'other than 'Dialog Control' - prompt user that these will be changed
        'to 'Dialog Control'
        With Me.lstProperties
            Dim i As Integer
            Dim xDef As String
            
            If .ListCount = 0 And Me.EditedTemplate <> Empty Then
                'get properties, we need to check them out below
                SetupForEditCreateProperties
            End If
            
            For i = 0 To .ListCount - 1
                xDef = m_xPropDef(.ItemData(i))
                If Not ((xDef Like VALUE_SRC_DLG_CTL_PATTERN) Or (xDef = Empty)) Then
                    'some controls get their value from something
                    'other than a Dialog Control- prompt
                    Dim iChoice As VbMsgBoxResult
                    iChoice = MsgBox("Some properties of this template have been defined to get " & _
                        "their values from properties of the Author or the Author's Office. " & _
                        "If you continue, these properties will be redefined to get their value " & _
                        "from a dialog control." & vbCr & vbCr & "Do you wish to continue?", vbQuestion + vbYesNo, MSGBOX_TITLE)
                        
                    If iChoice = vbYes Then
                        'change all such controls to Dialog control
                        SetAllValueSourcesToDlgControl
                    Else
                        Me.chkUseAuthors.Value = vbChecked
                    End If
                    
                    Exit Sub
                End If
            Next i
        End With
        
        'use list with only one item
        If oListOne Is Nothing Then
            Set oListOne = New mpDB.CList
            oListOne.Name = "Dialog Control"
            oListOne.ListItems.Add " Dialog Control", "Dialog", ""
        End If
        
        Me.cmbValueSource.Array = oListOne.ListItems.Source
        Me.cmbValueSource.Rebind
        Me.cmbValueSource.DropdownHeight = 0
        Me.cmbValueSource.BoundText = "Dialog"
    End If
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetValueSourceList"
    Exit Sub
End Sub

Private Sub SetAllValueSourcesToDlgControl()
'cycles through all existing property definitions-
'changes all definitions whose value source is not
''Dialog Control' to 'Dialog'
    On Error GoTo ProcError
    With Me.lstProperties
        Dim i As Integer
        Dim xDef As String
        Dim xPart1 As String
        Dim xPart2 As String
        Dim iPosSep1 As Integer
        Dim iPosSep2 As Integer
        
        For i = 0 To .ListCount - 1
            'get def
            xDef = m_xPropDef(.ItemData(i))
            If Not (xDef Like VALUE_SRC_DLG_CTL_PATTERN) Then
                'get first pipe
                iPosSep1 = InStr(xDef, SEP)
                
                'get second pipe
                iPosSep2 = InStr(iPosSep1 + 1, xDef, SEP)
                
                'get 2 part minus the value source
                xPart1 = Left(xDef, iPosSep1)
                xPart2 = Mid(xDef, iPosSep2)
                
                'add the parts back together, including
                ''Dialog Control' as value source
                m_xPropDef(.ItemData(i)) = xPart1 & "Dialog" & xPart2
            End If
        Next i
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetAllValueSourcesToDlgControl"
    Exit Sub
End Sub

Private Sub btnBrowseBPFile_Click()
    On Error GoTo ProcError
    BrowseForBPSource
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkUseAuthors_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkUseCI_Click()
    SetPanelHelp StepCreateTemplate
End Sub

Private Sub chkUseCI_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub chkWholeParagraph_Click()
    m_bCustPropDirty = True
    SetUnderlineControls
End Sub

Private Sub SetUnderlineControls()
'enables/disables underline length controls
    Dim bWholePara As Boolean
    
    On Error GoTo ProcError
    bWholePara = Me.chkWholeParagraph.Value = vbChecked
    If bWholePara Then
        'reset length
        Me.txtUnderlineLength.Text = 0
    End If
    'disable if whole para specified
    Me.txtUnderlineLength.Enabled = Not bWholePara
    Me.lblUnderlineLength.Enabled = Not bWholePara
    Me.udUnderlineLength.Enabled = Not bWholePara
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetUnderlineControls"
    Exit Sub
End Sub

Private Sub chkWholeParagraph_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAction_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAction_ItemChange()
    On Error GoTo ProcError
    m_bCustPropDirty = True
    If Me.cmbAction.BoundText = "1" Then
        Me.cmbValueSource.SelectedItem = 0
    End If
    Me.cmbValueSource.Enabled = (Me.cmbAction.BoundText <> "1")
    Me.lblValueSource.Enabled = Me.cmbValueSource.Enabled
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAction_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbAction, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbAction_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbAction)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCIList_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCIList_Mismatch(NewEntry As String, Reposition As Integer)
    Const mpThisFunction As String = "TemplateArchitectWizard.frmCreateCustomTemplate.cmbCIList_Mismatch"
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCIList, Reposition
    Exit Sub
ProcError:
    g_oError.Raise mpThisFunction
    Exit Sub
End Sub

Private Sub cmbCIList_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCIList)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCMProperty_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCMProperty_ItemChange()
    'related data field selected
    Me.lblCMRelatedDataIndex.Enabled = Me.cmbCMProperty.BoundText = 5
    Me.cmbCMRelatedField.Enabled = Me.cmbCMProperty.BoundText = 5
End Sub

Private Sub cmbCMRelatedField_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCMRelatedField_Mismatch(NewEntry As String, Reposition As Integer)
    Const mpThisFunction As String = "TemplateArchitectWizard.frmCreateCustomTemplate.cmbCMRelatedField_Mismatch"
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCMRelatedField, Reposition
    Exit Sub
ProcError:
    g_oError.Raise mpThisFunction
    Exit Sub
End Sub

Private Sub cmbCMRelatedField_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCMRelatedField)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbCMProperty_Mismatch(NewEntry As String, Reposition As Integer)
    Const mpThisFunction As String = "TemplateArchitectWizard.frmCreateCustomTemplate.cmbCMProperty_Mismatch"
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbCMProperty, Reposition
    Exit Sub
ProcError:
    g_oError.Raise mpThisFunction
    Exit Sub
End Sub

Private Sub cmbCMProperty_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbCMProperty)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbControlType_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbControlType_ItemChange()
    Dim xDef As String
    Dim vDef As Variant
    Dim i As Integer
    Dim oArr As XArray
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    xDef = Me.grdControls.Columns(2)
    vDef = Split(xDef, "|")
    If vDef(1) = "9" And Me.cmbControlType.BoundText <> 9 Then
        'user is changing control type from
        'client matter control to something else-
        'test if there are other textboxes linked to cm
        Set oArr = Me.grdControls.Array
        For i = 0 To oArr.UpperBound(1)
            xDef = oArr.Value(i, 2)
            vDef = Split(xDef, "|")
            If vDef(8) > 0 Then
                'control is linked to CM property - alert user
                'and reset control type to client matter control
                Me.cmbControlType.BoundText = 9
                DoEvents
                xMsg = "You cannot currently remove your client matter control.  Textboxes in this dialog are linked to this control." & vbCrLf & "Unlink these textboxes, then remove the client matter control."
                MsgBox xMsg, vbExclamation
            End If
        Next i
    End If
        
    SetNumLinesControls
    SetListSourceControls
    SetDateFormatsControls
    SetCMControls
    SetCIListControls
Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbControlType_Mismatch(NewEntry As String, Reposition As Integer)
    Const mpThisFunction As String = "TemplateArchitectWizard.frmCreateCustomTemplate.cmbControlType_Mismatch"
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbControlType, Reposition
    Exit Sub
ProcError:
    g_oError.Raise mpThisFunction
    Exit Sub
End Sub

Private Sub cmbControlType_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbControlType)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateFormat_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDateFormat_Mismatch(NewEntry As String, Reposition As Integer)
    Const mpThisFunction As String = "TemplateArchitectWizard.frmCreateCustomTemplate.cmbDateFormat_Mismatch"
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDateFormat, Reposition
    Exit Sub
ProcError:
    g_oError.Raise mpThisFunction
    Exit Sub
End Sub

Private Sub cmbDateFormat_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbDateFormat)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDefaultDocID_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDefaultList_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbListSource_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbListSource_Mismatch(NewEntry As String, Reposition As Integer)
    Const mpThisFunction As String = "TemplateArchitectWizard.frmCreateCustomTemplate.cmbListSource_Mismatch"
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbListSource, Reposition
    Exit Sub
ProcError:
    g_oError.Raise mpThisFunction
    Exit Sub
End Sub

Private Sub cmbListSource_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbListSource)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLocation_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTemplate_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbValueSource_Change()
    m_bCustPropDirty = True
End Sub

Private Sub SetNumLinesControls()
    Dim bIsTextbox As Boolean
    
    On Error GoTo ProcError
    bIsTextbox = (Me.cmbControlType.BoundText = "1") Or _
                (Me.cmbControlType.BoundText = "4")
    
    If bIsTextbox Then
        Me.txtNumLines.Text = "1"
    Else
        Me.txtNumLines.Text = ""
    End If
    
    Me.txtNumLines.Enabled = bIsTextbox
    Me.udNumLines.Enabled = bIsTextbox
    Me.lblNumLines.Enabled = bIsTextbox
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetNumLinesControls"
    Exit Sub
End Sub

Private Sub SetCIListControls()
    Dim bIsTextbox As Boolean
    
    On Error GoTo ProcError
    bIsTextbox = (Me.cmbControlType.BoundText = "1")
    
    Me.cmbCIList.BoundText = 0
    
    Me.cmbCIList.Enabled = bIsTextbox
    Me.lblCIList.Enabled = bIsTextbox
    Me.cmbCIList.SelectedItem = 0
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetCIListControls"
    Exit Sub
End Sub

Private Sub SetCMControls()
    Dim bIsTextbox As Boolean
    Dim bCMControlExists As Boolean
    Dim i As Integer
    Dim oArr As XArray
    Dim xDef As String
    Dim vDef As Variant
    
    On Error GoTo ProcError
    bIsTextbox = (Me.cmbControlType.BoundText = "1")
    
    Set oArr = Me.grdControls.Array
    For i = 0 To oArr.UpperBound(1)
        xDef = oArr.Value(i, 2)
        vDef = Split(xDef, "|")
        If vDef(1) = 9 Then
            'client matter control exists in list of controls
            bCMControlExists = True
            Exit For
        End If
    Next i
    
    Me.cmbCMProperty.BoundText = 0
    Me.cmbCMRelatedField.BoundText = 0
    
    Me.lblCMProperty.Enabled = bIsTextbox And bCMControlExists
    Me.cmbCMProperty.Enabled = bIsTextbox And bCMControlExists
       
    Me.cmbCMProperty.SelectedItem = 0
    Me.cmbCMRelatedField.SelectedItem = 0
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetCMControls"
    Exit Sub
End Sub

Private Sub SetListSourceControls()
    Dim iCtlType As Integer
    
    On Error GoTo ProcError
    
    iCtlType = Me.cmbControlType.BoundText

    If bIsListControl(iCtlType) Then
        SetListsCombo iCtlType = mpCustomControl_OrdinalDateCombo
        
        Me.cmbListSource.SelectedItem = 0
    Else
        Me.cmbListSource.BoundText = Empty
    End If
    
    Me.cmbListSource.Enabled = bIsListControl(iCtlType)
    Me.lblListSource.Enabled = bIsListControl(iCtlType)
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetListSourceControls"
    Exit Sub
End Sub

Private Sub SetDateFormatsControls()
    Dim bIsCalendar As Boolean
    On Error GoTo ProcError
    bIsCalendar = (Me.cmbControlType.BoundText = "7")
    
    If bIsCalendar Then
        Me.cmbDateFormat.SelectedItem = 0
    Else
        Me.cmbDateFormat.BoundText = Empty
    End If
    
    Me.cmbDateFormat.Enabled = bIsCalendar
    Me.lblDateFormat.Enabled = bIsCalendar
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetDateFormatsControls"
    Exit Sub
End Sub

Private Sub cmbValueSource_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbValueSource_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbValueSource, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbValueSource_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbValueSource)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Activate()
    On Error GoTo ProcError
    'Fix for #4048
    m_PanelHelpDescTop = Me.lblPanelHelpDescription.Top '*c
    m_PanelHelpDescHeight = Me.lblPanelHelpDescription.Height '*c
    m_LabelControlHelpTop = Me.lblControlHelp.Top '*c
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Load()
    m_bCancelled = True
    
    InitStepWelcome
End Sub

Private Sub InitStep(ByVal iStep As Steps)
'initializes the specified step
    On Error GoTo ProcError
    Select Case iStep
        Case StepCreateTemplate
            Me.shpWizardButtons.ZOrder 1
            InitStepCreateTemplate
        Case StepAssignTemplate
            InitStepAssignTemplate
        Case StepSetupCI
            InitStepSetupCI
        Case StepCreateProperties
            InitStepCreateProperties
        Case StepCreateDialog
            InitStepCreateDialog
        Case StepCreateDialog2
            InitStepCreateDialog2
    End Select
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStep"
    Exit Sub
End Sub

Private Sub BrowseForBPSource()
'shows open dialog that allows user to select a source file to
'use as macpac boilerplate - restricted to .doc and .mbp.
    On Error GoTo ProcError
    With Me.cdlgBrowse
        If g_bIsWord12x Then
            .DefaultExt = ".docx"
            .DialogTitle = "Browse for boilerplate"
            .Filter = "Word Document (*.docx)|*.docx|MacPac Boilerplate (*.mbp)|*.mbp"
            .Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                cdlOFNLongNames Or cdlOFNPathMustExist
            .ShowOpen
            If .FileName = Empty Then
                'no file selected as bp source - do nothing
            ElseIf LCase(Right(.FileName, 5)) <> ".docx" And _
                LCase(Right(.FileName, 4)) <> ".mbp" Then
                'invalid file selected
                MsgBox "Boilerplate source file must have an " & _
                       "extension of .docx or .mbp", _
                    vbExclamation, MSGBOX_TITLE
            Else
                'use as bp source
                Me.txtBPSource.Text = .FileName
            End If
        Else
            .DefaultExt = ".doc"
            .DialogTitle = "Browse for boilerplate"
            .Filter = "Word Document (*.doc)|*.doc|MacPac Boilerplate (*.mbp)|*.mbp"
            .Flags = cdlOFNFileMustExist Or cdlOFNHideReadOnly Or _
                cdlOFNLongNames Or cdlOFNPathMustExist
            .ShowOpen
            If .FileName = Empty Then
                'no file selected as bp source - do nothing
            ElseIf LCase(Right(.FileName, 4)) <> ".doc" And _
                LCase(Right(.FileName, 4)) <> ".mbp" Then
                'invalid file selected
                MsgBox "Boilerplate source file must have an " & _
                       "extension of .doc or .mbp", _
                    vbExclamation, MSGBOX_TITLE
            Else
                'use as bp source
                Me.txtBPSource.Text = .FileName
            End If
        End If
    End With
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.BrowseForBPSource"
    Exit Sub
End Sub

Private Sub InitStepWelcome()
'initializes the first panel
    Static bIsInitialized As Boolean
    On Error GoTo ProcError
    
    With Me.fraWizardPanel(0)
        .Left = 0
        .Top = 0
        .Width = Me.Width
        .Height = Me.shpWizardButtons.Top
    End With
    
    If Not bIsInitialized Then
        'get existing custom templates
        Dim oList As mpDB.CList
        Set oList = g_oDB.Lists.Add("CustomTemplates", _
            "SELECT fldShortName, fldFileName FROM tblTemplates " & _
            "WHERE fldMacro='zzmpCreateCustomDocument'" & _
            "ORDER BY fldShortName")
            
        With oList
            .IsDBList = True
            .Refresh
            
            Me.cmbTemplate.Array = oList.ListItems.Source
        End With
        ResizeTDBCombo Me.cmbTemplate, 6
        
        bIsInitialized = True
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStepCreateTemplate"
    Exit Sub
End Sub

Private Sub InitStepCreateTemplate()
'initializes the first panel
    Static bIsInitialized As Boolean
    On Error GoTo ProcError
    
    If Not bIsInitialized Then
        'fill 'location' combo
        Dim oLocationArray As XArray
        Set oLocationArray = g_oDB.NewXArray()
        With oLocationArray
            .ReDim 0, 2, 0, 1
            .Value(0, 0) = "MacPac Templates Folder"
            .Value(0, 1) = 1
            .Value(1, 0) = "Word Workgroup Templates Folder"
            .Value(1, 1) = 2
            .Value(2, 0) = "Word User Templates Folder"
            .Value(2, 1) = 3
        End With
        
        With Me.cmbLocation
            .Array = oLocationArray
            .Rebind
        End With
        
        ResizeTDBCombo Me.cmbLocation, 3
        
        'fill 'default doc id' combo
        With Me.cmbDefaultDocID
            .Array = g_oDB.DocumentStampDefs( _
                1, "-All Templates-").ListSource
            .Rebind
        End With
        
        ResizeTDBCombo Me.cmbDefaultDocID, 8
        
        FixTDBCombo Me.cmbLocation
        FixTDBCombo Me.cmbDefaultDocID
        
        If Me.EditedTemplate <> Empty Then
            'get values for existing template
            SetupForEditCreateTemplate
        Else
            'set default values
            Me.cmbLocation.SelectedItem = 0
            Me.cmbDefaultDocID.SelectedItem = 0
        End If
        
        bIsInitialized = True
    End If
    
    'set focus
    If Me.txtTemplateFileName.Enabled Then
        Me.txtTemplateFileName.SetFocus
    Else
        Me.txtShortName.SetFocus
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStepCreateTemplate"
    Exit Sub
End Sub

Private Sub SetupForEditAssignTemplate()
'loads assignments into tree
    Dim oNode As Node
    Dim lID As Long
    Dim oDefs As mpDB.CTemplateDefs
    
    On Error GoTo ProcError
    
    'get template defs
    Set oDefs = g_oDB.TemplateDefinitions
    oDefs.Refresh

    'cycle through nodes on tree
    For Each oNode In Me.tvGroups.Nodes
        'get node ID
        lID = CLng(Mid(oNode.Key, 2))
        
        'check node if template is a member of the group
        oNode.Checked = oDefs.MemberOf(Me.EditedTemplate, lID)
    Next oNode
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetupForEditAssignTemplate"
    Exit Sub
End Sub

Private Sub SetupForEditCreateProperties()
'loads existing property definitions into the list
    Dim oDefs As mpDB.CCustomPropertyDefs
    Dim oDef As mpDB.CCustomPropertyDef
    Dim iPropType As mpCustomPropertyTypes
    Dim xValSource As String
    Dim xValSourceText As String
    Dim i As Integer
    Dim iNumDefs As Integer
    
    On Error GoTo ProcError
    
    'get property defs
    Set oDefs = g_oDB.CustomProperties(Me.EditedTemplate)
    
    iNumDefs = oDefs.Count
    
    If iNumDefs > 0 Then
        'dimension array
        ReDim m_xPropDef(iNumDefs - 1)
        ReDim m_xPropDefID(iNumDefs - 1)
        
        'cycle through defs, populating array, adding to list box
        For i = 1 To oDefs.Count
            'get definition
            Set oDef = oDefs.Item(i)
            
            With oDef
                'get type of property
                iPropType = .PropertyType
                
                'append appropriate text to value source text -
                'since there is only 1 control for two properties
                '(PropertyType and LinkedProperty), we need distinguish
                'between office and author properties, some of which
                'have the same name
                If iPropType = mpCustomPropertyType_AuthorOffice Then
                    xValSource = .LinkedProperty
                    xValSourceText = "Office " & .LinkedProperty
                ElseIf iPropType = mpCustomPropertyType_Author Then
                    xValSource = .LinkedProperty
                    xValSourceText = "Author " & .LinkedProperty
                Else
                    xValSource = "Dialog"
                    xValSourceText = " Dialog Control"
                End If
                
                'assign definition string to array element
                m_xPropDef(i - 1) = GetPropertyDefinitionString( _
                    .Name, xValSource, xValSourceText, .WholeParagraph, _
                    .UnderlineLength, .Indexed, .Macro, .Action, .Bookmark)
                    
                'assign property ID to array element - used by controls
                m_xPropDefID(i - 1) = .ID
                
                'add to list
                With Me.lstProperties
                    .AddItem oDef.Name
                    .ItemData(.ListCount - 1) = i - 1
                End With
            End With
        Next i
        
        'select first item in list if items exist
        If Me.lstProperties.ListCount > 0 Then
            'need to set this because the lstProperties_Click
            'proc runs only when the curIndex changes - by default
            'the curIndex = 0, so we need to force a change
            m_iCurIndex = -1
            
            Me.lstProperties.ListIndex = 0
        End If
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetupForEditCreateProperties"
    Exit Sub
End Sub

Private Sub SetupForEditCreateDialog2()
'loads letterheads and tab captions
    Dim oDefs As mpDB.CCustomDialogDefs
    Dim oDef As mpDB.CCustomDialogDef
    Dim oLHs As mpDB.CLetterheadDefs
    Dim oLH As mpDB.CLetterheadDef
    Dim oNode As Node
    Dim lID As Long

    'get letterhead defs
    On Error Resume Next
    Set oLHs = g_oDB.LetterheadDefs(Me.EditedTemplate)
    On Error GoTo ProcError
                
    If Not oLHs Is Nothing Then
        If oLHs.Count > 0 Then
            'cycle through nodes on letterhead tree,
            'checking those that are in the collection
            For Each oNode In Me.tvLetterheads.Nodes
                'get letterhead id of node
                lID = Mid(oNode.Key, 2)
                
                'check for existence of letterhead in collection
                Set oLH = Nothing
                On Error Resume Next
                Set oLH = oLHs.Item(lID)
                On Error GoTo ProcError
                
                'check node if letterhead exists in collection
                oNode.Checked = Not (oLH Is Nothing)
            Next oNode
        End If
    End If
            
    'get tab captions
    Set oDefs = g_oDB.CustomDialogDefs
    oDefs.Refresh
    Set oDef = oDefs.Item(Me.EditedTemplate)
    With oDef
        Me.txtTab1Caption.Text = .Tab1
        Me.txtTab2Caption.Text = .Tab2
        Me.txtTab3Caption.Text = .Tab3
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetupForEditCreateDialog2"
    Exit Sub
End Sub

Private Sub SetupForEditCreateDialog()
'loads existing control definitions into the list
    On Error GoTo ProcError
    
    'get dialog caption
    Me.txtDialogTitle = g_oDB.CustomDialogDefs _
        .Item(Me.EditedTemplate).Caption
    
    'fill Control array with default definitions
    'based on specified properties
    GetDBControls
    
    'update to reflect any changes to properties
    UpdateControlDefs
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetupForEditCreateProperties"
    Exit Sub
End Sub

Private Sub SetupForEditCISetup()
'gets values for the CI format panel
    Dim oDefs As mpDB.CCIFormats
    Dim oDef As mpDB.CCIFormat
    Dim oDlgDefs As mpDB.CCustomDialogDefs
    Dim oDlgDef As mpDB.CCustomDialogDef
    
    On Error GoTo ProcError
    
    'get custom dialog
    Set oDlgDefs = g_oDB.CustomDialogDefs
    oDlgDefs.Refresh
    Set oDlgDef = oDlgDefs.Item(Me.EditedTemplate)
    
    'no custom dialog - exit
    If oDlgDef Is Nothing Then
        Exit Sub
    End If
    
    'get ci format for dialog
    Set oDefs = g_oDB.CIFormats
    oDefs.Refresh
    
    If oDlgDef.CIFormat > 0 Then
        'ci format exists for dialog - get definition
        On Error Resume Next
        Set oDef = oDefs.Item(oDlgDef.CIFormat)
        On Error GoTo ProcError
    End If
    
    If Not oDef Is Nothing Then
        'the definition exists
        With oDef
            Me.chkIncludeTo.Value = Abs(.IncludeTo)
            Me.chkIncludeFrom.Value = Abs(.IncludeFrom)
            Me.chkIncludeBCC.Value = Abs(.IncludeBCC)
            Me.chkIncludeCC.Value = Abs(.IncludeCC)
            Me.chkNameOnlyBCC.Value = Abs(.BCCNamesOnly)
            Me.chkNamesOnlyFrom.Value = Abs(.FromNamesOnly)
            Me.chkNamesOnlyCC.Value = Abs(.CCNamesOnly)
            Me.chkNamesOnlyTo.Value = Abs(.ToNamesOnly)
            Me.chkIncludeEMail.Value = Abs(.IncludeEMail)
            Me.chkIncludeFaxNumbers.Value = Abs(.IncludeFax)
            Me.chkIncludePhoneNumbers.Value = Abs(.IncludePhone)
            Me.chkPromptForPhones.Value = Abs(.PromptForPhones)
            Me.cmbDefaultList.BoundText = .DefaultList
            Me.txtPhoneLabel.Text = .PhoneLabel
            Me.txtFaxLabel.Text = .FaxLabel
            Me.txtEAddressLabel.Text = .EmailLabel
        End With
    Else
        'no definition exists, set up defaults
        Me.cmbDefaultList.SelectedItem = 0
        Me.chkIncludeTo.Value = vbChecked
        Me.chkPromptForPhones.Value = vbChecked
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetupForEditAssignTemplate"
    Exit Sub
End Sub

Private Sub SetupForEditCreateTemplate()
    On Error GoTo ProcError
    'change caption of bp source control
    With Me.lblBoilerplateFile
        .Caption = "Boilerplate File:"
        .Enabled = False
    End With
    
    'set alignment of bp textbox to left align
    With Me.txtBPSource
        .Alignment = 0
        .Enabled = False
    End With
    
    'hide boilerplate browse button
    Me.btnBrowseBPFile.Visible = False
    
    'disable template filename textbox
    Me.txtTemplateFileName.Enabled = False
    Me.lblTemplateFileName.Enabled = False
    Me.cmbLocation.Enabled = False
    Me.lblLocation.Enabled = False
    
    'load values for the edited template
    LoadValuesStepCreateTemplate
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetupForEditCreateTemplate"
    Exit Sub
End Sub

Private Sub InitStepAssignTemplate()
'initializes the first panel
    Static bIsInitialized As Boolean
    On Error GoTo ProcError
    If Not bIsInitialized Then
        Dim i As Integer
        Dim oGroup As CTemplateGroup
        Dim oGroups As CTemplateGroups
        Set oGroups = g_oDB.TemplateGroups
        For i = 1 To oGroups.Count
            Set oGroup = oGroups.Item(i)
            If Not (oGroup Is Nothing) Then             '9.7.1030 #4642
                CreateNode oGroup
            End If
        Next i
        
        If Me.EditedTemplate <> Empty Then
            'get existing values for this panel
            SetupForEditAssignTemplate
        End If
        
        bIsInitialized = True
    End If
    Me.Caption = "Template Architect Wizard - Assign Template"
    Me.tvGroups.SetFocus
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStepAssignTemplate"
    Exit Sub
End Sub

Private Sub InitStepSetupCI()
'initializes the third panel
    Static bIsInitialized As Boolean
    On Error GoTo ProcError
    
    If Not bIsInitialized Then
        'fill 'location' combo
        Dim oLists As XArray
        Set oLists = g_oDB.NewXArray()
        With oLists
            .ReDim 0, 3, 0, 1
            .Value(0, 0) = "To"
            .Value(0, 1) = 1
            .Value(1, 0) = "From"
            .Value(1, 1) = 2
            .Value(2, 0) = "CC"
            .Value(2, 1) = 4
            .Value(3, 0) = "BCC"
            .Value(3, 1) = 8
        End With
        
        Me.cmbDefaultList.Array = oLists
        Me.cmbDefaultList.Rebind
        ResizeTDBCombo Me.cmbDefaultList, 4
        
        FixTDBCombo Me.cmbDefaultList
        
        If Me.EditedTemplate <> Empty Then
            SetupForEditCISetup
        Else
            Me.cmbDefaultList.SelectedItem = 0
            Me.chkIncludeTo.Value = vbChecked
            Me.chkPromptForPhones.Value = vbChecked
        End If
        
        bIsInitialized = True
    End If
    
    Me.chkIncludeTo.SetFocus
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStepSetupCI"
    Exit Sub
End Sub

Private Sub InitStepCreateProperties()
'initializes the third panel
    Static bIsInitialized As Boolean
    On Error GoTo ProcError
    
    SetValueSourceList

    ShowHideValueSourceControls
    
    If Not bIsInitialized Then
        'fill action combo
        Dim oArr As XArray
        Set oArr = g_oDB.NewXArray()
        With oArr
            .ReDim 0, 2, 0, 1
            .Value(0, 0) = "Insert Text"
            .Value(0, 1) = "0"
            .Value(1, 0) = "Insert Checkbox"
            .Value(1, 1) = "1"
            .Value(2, 0) = "Run Macro Only"
            .Value(2, 1) = "2"
        End With
        Me.cmbAction.Array = oArr
        Me.cmbAction.Rebind
        ResizeTDBCombo Me.cmbAction, 3
        
        FixTDBCombo Me.cmbAction
        FixTDBCombo Me.cmbValueSource
        
        If Me.EditedTemplate <> Empty And (Me.lstProperties.ListCount = 0) Then
            'add property values for this template
            SetupForEditCreateProperties
        Else
            'set default values
            ResetPropertyControls
        End If
        
        bIsInitialized = True
    End If
    
    If Me.txtPropertyName.Enabled Then
        Me.txtPropertyName.SetFocus
    Else
        Me.cmbAction.SetFocus
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStepCreateProperties"
    Exit Sub
End Sub

Private Sub ShowHideValueSourceControls()
    On Error GoTo ProcError
    If Me.chkUseAuthors.Value = vbUnchecked And Me.cmbValueSource.Visible Then
        'hide value source and move controls up
        Me.cmbValueSource.Visible = False
        Me.lblValueSource.Visible = False
        Me.chkWholeParagraph.Top = Me.chkWholeParagraph.Top - 810
        Me.lblUnderlineLength.Top = Me.lblUnderlineLength.Top - 810
        Me.txtUnderlineLength.Top = Me.txtUnderlineLength.Top - 810
        Me.udUnderlineLength.Top = Me.udUnderlineLength.Top - 810
        Me.lblMacro.Top = Me.lblMacro.Top - 810
        Me.txtMacro.Top = Me.txtMacro.Top - 810
    ElseIf Me.chkUseAuthors.Value = vbChecked And Not Me.cmbValueSource.Visible Then
        'show value source and move controls up
        Me.cmbValueSource.Visible = True
        Me.lblValueSource.Visible = True
        Me.chkWholeParagraph.Top = Me.chkWholeParagraph.Top + 810
        Me.lblUnderlineLength.Top = Me.lblUnderlineLength.Top + 810
        Me.txtUnderlineLength.Top = Me.txtUnderlineLength.Top + 810
        Me.udUnderlineLength.Top = Me.udUnderlineLength.Top + 810
        Me.lblMacro.Top = Me.lblMacro.Top + 810
        Me.txtMacro.Top = Me.txtMacro.Top + 810
    End If
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.ShowHideValueSourceControls"
    Exit Sub
End Sub

Private Sub FixTDBCombo(oCombo As TrueDBList60.TDBCombo)
    On Error GoTo ProcError
    'this is needed to make the
    'bound text stick programmatically
    With oCombo
        .OpenCombo
        .CloseCombo
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.FixTDBCombo"
    Exit Sub
End Sub

Private Sub InitStepCreateDialog()
'initializes the third panel
    Static bIsInitialized As Boolean
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    If Not bIsInitialized Then
        'populate controls combo
        Dim oList As mpDB.CList
        Set oList = g_oDB.Lists.Add("Controls")
        With oList
            .ListItems.Add "1", "TextBox", ""
            .ListItems.Add "2", "ListBox", ""
            .ListItems.Add "3", "ComboBox", ""
            .ListItems.Add "4", "MultilineComboBox", ""
            .ListItems.Add "5", "CheckBox", ""
            .ListItems.Add "7", "Calendar", ""
            .ListItems.Add "8", "Ordinal Date Combo", ""
            .ListItems.Add "9", "Client Matter Control", ""
        End With
        
        Me.cmbControlType.Array = oList.ListItems.Source
        Me.cmbControlType.Rebind
        ResizeTDBCombo Me.cmbControlType, 9
        
        Set oList = g_oDB.Lists.Add("CMProps")
        With oList
            .ListItems.Add "0", "None", ""
            .ListItems.Add "1", "Client Number", ""
            .ListItems.Add "2", "Client Name", ""
            .ListItems.Add "3", "Matter Number", ""
            .ListItems.Add "4", "Matter Name", ""
            '.ListItems.Add "5", "Other", ""
        End With
        
        Me.cmbCMProperty.Array = oList.ListItems.Source
        Me.cmbCMProperty.Rebind
        ResizeTDBCombo Me.cmbCMProperty, 9
        
        Dim oArr As XArray
        Set oArr = g_oDB.NewXArray
        With oArr
            .ReDim 0, 9, 0, 1
            .Value(0, 0) = 0
            .Value(0, 1) = 0
            .Value(1, 0) = 1
            .Value(1, 1) = 1
            .Value(2, 0) = 2
            .Value(2, 1) = 2
            .Value(3, 0) = 3
            .Value(3, 1) = 3
            .Value(4, 0) = 4
            .Value(4, 1) = 4
            .Value(5, 0) = 5
            .Value(5, 1) = 5
            .Value(6, 0) = 6
            .Value(6, 1) = 6
            .Value(7, 0) = 7
            .Value(7, 1) = 7
            .Value(8, 0) = 8
            .Value(8, 1) = 8
            .Value(9, 0) = 9
            .Value(9, 1) = 9
        End With
        
        Me.cmbCMRelatedField.Array = oArr
        Me.cmbCMRelatedField.Rebind
        ResizeTDBCombo Me.cmbCMRelatedField, 10
        
        SetListsCombo
        
        'populate date format control
        xSQL = "SELECT tblCustomDateFormats.fldFormat " & _
            "FROM tblCustomDateFormats " & _
            "ORDER By tblCustomDateFormats.fldSortOrder"
        Set oList = g_oDB.Lists.Add("CustomDateFormats", xSQL)
        With oList
            .IsDBList = True
            .Refresh
        End With
        Me.cmbDateFormat.Array = oList.ListItems.Source
        Me.cmbDateFormat.Rebind
        ResizeTDBCombo Me.cmbDateFormat, 12
        
        'populate CI Lists control
        Set oList = g_oDB.Lists.Add("CILists")
        With oList
            .ListItems.Add 0, "None", ""
            .ListItems.Add 1, "To", ""
            .ListItems.Add 2, "From", ""
            .ListItems.Add 4, "CC", ""
            .ListItems.Add 8, "BCC", ""
        End With
        Me.cmbCIList.Array = oList.ListItems.Source
        Me.cmbCIList.Rebind
        ResizeTDBCombo Me.cmbCIList, 5
        
        FixTDBCombo Me.cmbCIList
        FixTDBCombo Me.cmbControlType
        FixTDBCombo Me.cmbDateFormat
        FixTDBCombo Me.cmbListSource
        
        If Me.EditedTemplate <> Empty Then
            SetupForEditCreateDialog
        Else
            'get the controls that need to be created
            GetDefaultControls
        End If
        
        bIsInitialized = True
    Else
        'ensure that control defs match
        'up with properties - this only
        'runs if the user has backed up
        'to the previous panel, and is
        'now returning to the controls panel
        
        UpdateControlDefs
    End If
    
    Dim bEnable As Boolean
    bEnable = Me.grdControls.Array.Count(1)
    Me.grdControls.Enabled = bEnable
    Me.lblControls.Enabled = bEnable
    Me.cmbControlType.Enabled = bEnable
    Me.lblControlType.Enabled = bEnable
    Me.txtControlLabel.Enabled = bEnable
    Me.lblControlLabel.Enabled = bEnable
    Me.udNumLines.Enabled = bEnable
    Me.txtNumLines.Enabled = bEnable
    Me.lblNumLines.Enabled = bEnable
    Me.udSpaceBefore.Enabled = bEnable
    Me.txtSpaceBefore.Enabled = bEnable
    Me.lblSpaceBefore.Enabled = bEnable
    Me.cmbListSource.Enabled = bEnable
    Me.lblListSource.Enabled = bEnable
    Me.cmbDateFormat.Enabled = bEnable
    Me.lblDateFormat.Enabled = bEnable
    Me.cmbCIList.Enabled = bEnable
    Me.lblCIList.Enabled = bEnable
    Me.btnUp.Enabled = bEnable
    Me.btnDown.Enabled = bEnable
    Me.chkIsSticky.Enabled = bEnable
    Me.btnAddList.Enabled = bEnable

    'hide ci list control if ci is not being used
    Me.cmbCIList.Visible = (Me.chkUseCI.Value = vbChecked)
    Me.lblCIList.Visible = (Me.chkUseCI.Value = vbChecked)
    If Me.chkUseCI.Value = vbUnchecked Then
        Me.chkIsSticky.Top = Me.lblCIList.Top
    Else
        Me.chkIsSticky.Top = Me.cmbCIList.Top + 510
    End If
    
    'select first in list if necessary
    With Me.grdControls
        If .SelBookmarks.Count = 0 And .Array.Count(1) > 0 Then
            .SelBookmarks.Add 0
            ParseControlDefinition
        End If
    End With
    
    Me.txtDialogTitle.SetFocus
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStepCreateDialog"
    Exit Sub
End Sub

Private Sub SetListsCombo(Optional ByVal bDateListsOnly As Boolean = False, Optional ByVal bRefresh As Boolean = False)
'populates the list source control
    Dim xSQL As String
    Static oAllLists As mpDB.CList
    Static oDateLists As mpDB.CList
    
    On Error GoTo ProcError
    If bDateListsOnly Then
        If (oDateLists Is Nothing) Or bRefresh Then
            xSQL = "SELECT tblLists.fldListName FROM tblLists " & _
                "WHERE tblLists.fldListName LIKE ""*Date*"" UNION " & _
                "SELECT tblCustomLists.fldListName FROM tblCustomLists " & _
                "WHERE tblCustomLists.fldListName LIKE ""*Date*"" "
            Set oDateLists = g_oDB.Lists.Add("Lists", xSQL)
            With oDateLists
                .IsDBList = True
                .Refresh
            End With
        End If
        
        With Me.cmbListSource
            .Array = oDateLists.ListItems.Source
            .Rebind
        End With
    Else
        If (oAllLists Is Nothing) Or bRefresh Then
            xSQL = "SELECT tblLists.fldListName FROM tblLists UNION " & _
                "SELECT tblCustomLists.fldListName FROM tblCustomLists"
            Set oAllLists = g_oDB.Lists.Add("Lists", xSQL)
            With oAllLists
                .IsDBList = True
                .Refresh
            End With
        End If
        
        With Me.cmbListSource
            .Array = oAllLists.ListItems.Source
            .Rebind
        End With
    End If
    
    ResizeTDBCombo Me.cmbListSource, 10
    Exit Sub
ProcError:
    RaiseError "TemplateArchitectWizard.frmCreateCustomTemplate.SetListsCombo"
    Exit Sub
End Sub

Public Sub UpdateControlDefs()
'adds new default dialog control definitions and
'deletes definitions no longer needed, based on
'current state of the property definition array
    Dim i As Integer
    Dim j As Integer
    Dim iUBound As Integer
    Dim iUpper As Integer
    Dim xDef As String
    Dim xName As String
    Dim oArr As XArray
    Dim iDefIndex As Integer
    Dim xDef1 As String '*c 4172
    Dim xName1 As String '*c 4172
    
    On Error GoTo ProcError
    'cycle through property definitions
    Set oArr = Me.grdControls.Array
    
    For i = 0 To Me.lstProperties.ListCount - 1
        'get property definition
        xDef = m_xPropDef(Me.lstProperties.ItemData(i))

        'get name of property
        xName = GetDelimitedStringItem(xDef, 1, "|")
        'reset definition index
        iDefIndex = -1
        
        'cycle through controls, looking for one
        'with the same name
        For j = 0 To oArr.UpperBound(1)
            
            If oArr.Value(j, 1) = xName Then
                'control with same name exists
                iDefIndex = j
                Exit For
            End If
        Next j
                
        If xDef Like VALUE_SRC_DLG_CTL_PATTERN Then
            'property requires a control definition
            If iDefIndex = -1 Then
                'create default definition
                
                'get prop name
                xName = GetDelimitedStringItem(xDef, 1, "|")
                
                With oArr
                    iUpper = .UpperBound(1) + 1
                    .Insert 1, iUpper
                    '.ReDim .LowerBound(1), iUpper
                    
                    .Value(iUpper, 1) = xName
                    .Value(iUpper, 0) = 1
                    .Value(iUpper, 2) = GetDefaultControlDef(xName)
                End With

'                With oArr
                
'                    'create new array element
'                    iUBound = .UpperBound(1) + 1
'                    '.Insert 1, iUBound
'
'                    oArr.ReDim oArr.LowerBound(1), iUBound
'
'                    If iUBound = 0 Then
'                        .Value(iUBound, 0) = 1
'                    Else
'                        'set tab to tab of previous value
'                        .Value(iUBound, 0) = .Value(iUBound - 1, 0)
'                    End If
'                    .Value(iUBound, 1) = xName
'                    .Value(iUBound, 2) = GetDefaultControlDef(xName)
'                End With
            End If
        Else
            'property does not need a control
            'definition - delete if it exists
            If iDefIndex > -1 Then
                oArr.Delete 1, iDefIndex
            End If
        End If
        If i = 0 Then '*c 4172
            xName1 = xName '*c
            xDef1 = xDef '*c
        End If '*c
    Next i
    
    Dim bPropExists As Boolean
    'delete controls that no longer have an associated property
    'cycle through all controls -
    For i = oArr.UpperBound(1) To 0 Step -1
        bPropExists = False
        For j = 0 To Me.lstProperties.ListCount - 1
            If oArr.Value(i, 1) = Me.lstProperties.List(j) Then
                'control with same name exists
                bPropExists = True
                Exit For
            End If
        Next j
        
        If Not bPropExists Then
            'control exists, but prop
            'no longer exists - delete
            oArr.Delete 1, i
        End If
    Next i
    '#4172
    If IsNull(Me.grdControls.Columns(0).Value) Then
        Me.grdControls.Array = oArr
        Me.grdControls.Rebind
        Me.grdControls.Columns(1).Value = xName1
        Me.grdControls.Columns(2).Value = xDef1
    End If
    
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.UpdateControlDefs"
    Exit Sub
End Sub

Private Sub InitStepCreateDialog2()
'initializes the third panel
    Static bIsInitialized As Boolean
    Dim bUseTab2 As Boolean
    Dim bUseTab3 As Boolean
    Static iLHControlLeftOriginal As Integer
    Static iLHLabelLeftOriginal As Integer
    
    On Error GoTo ProcError
    
    If Not bIsInitialized Then
        'fill letterheads list
        Dim oLHs As mpDB.CLetterheadDefs
        Set oLHs = g_oDB.LetterheadDefs
        oLHs.Refresh
        
        'add letterhead defs to tree
        Dim i As Integer
        
        For i = 0 To oLHs.Count - 1
            Me.tvLetterheads.Nodes.Add , , _
                "%" & oLHs.ListSource.Value(i, 0), _
                    oLHs.ListSource.Value(i, 1)
        Next i
        
        If Me.EditedTemplate <> Empty Then
            SetupForEditCreateDialog2
        End If
        
        bIsInitialized = True
    End If
        
    'check for controls on other tabs
    Dim oArr As XArray
    Set oArr = Me.grdControls.Array
    With oArr
        For i = 0 To .UpperBound(1)
            If .Value(i, 0) = 2 Then
                bUseTab2 = True
            ElseIf .Value(i, 0) = 3 Then
                bUseTab3 = True
            End If
            If bUseTab2 And bUseTab3 Then
                Exit For
            End If
        Next i
    End With
          
    Dim bUseLH As Boolean
    bUseLH = bUseLetterhead()
    
    'show/hide caption controls as necessary
'    Me.txtTab1Caption.Visible = bUseTab2 Or bUseTab3 Or bUseLH
'    Me.lblTab1Caption.Visible = bUseTab2 Or bUseTab3 Or bUseLH
    Me.txtTab1Caption.Enabled = bUseTab2 Or bUseTab3 Or bUseLH
    Me.lblTab1Caption.Enabled = bUseTab2 Or bUseTab3 Or bUseLH
    Me.txtTab2Caption.Enabled = bUseTab2
    Me.lblTab2Caption.Enabled = bUseTab2
    Me.txtTab3Caption.Enabled = bUseTab3
    Me.lblTab3Caption.Enabled = bUseTab3
        
    'ensure that tab captions 2/3 are empty
    'if the tab is not being used
    If Not bUseTab2 Then
        Me.txtTab2Caption.Text = Empty
    End If
    
    If Not bUseTab3 Then
        Me.txtTab3Caption.Text = Empty
    End If
        
'    If Not (bUseTab2 Or bUseTab3 Or bUseLH) Then
'        'no tab caption textboxes will be displayed-
'        'move letterhead list to left side
'        If iLHControlLeftOriginal = Empty Then
'            iLHControlLeftOriginal = Me.tvLetterheads.Left
'            iLHLabelLeftOriginal = Me.lblLetterheads.Left
'        End If
'        Me.tvLetterheads.Left = iLHControlLeftOriginal - 900
'        Me.lblLetterheads.Left = iLHLabelLeftOriginal - 855
'    ElseIf iLHControlLeftOriginal > 0 Then
'        'control has been moved to the right - return to left
'        Me.tvLetterheads.Left = iLHControlLeftOriginal
'        Me.lblLetterheads.Left = iLHLabelLeftOriginal
'    End If
    
    Me.tvLetterheads.SetFocus
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.InitStepCreateDialog2"
    Exit Sub
End Sub

Private Sub LoadValuesStepCreateTemplate()
'loads the values for the edited template
'into the appropriate controls in the dialog
    Dim oDefs As mpDB.CTemplateDefs
    Dim oDef As mpDB.CTemplateDef
    Dim oDlgDefs As mpDB.CCustomDialogDefs
    Dim oDlgDef As mpDB.CCustomDialogDef
    Dim xTemplate As String
    
    On Error GoTo ProcError
    'get template definition
    Set oDefs = g_oDB.TemplateDefinitions
    oDefs.Refresh
    Set oDef = oDefs.Item(Me.cmbTemplate.BoundText)
    
    'get dialog definition
    Set oDlgDefs = g_oDB.CustomDialogDefs
    oDefs.Refresh
    
    On Error Resume Next
    Set oDlgDef = oDlgDefs.Item(Me.cmbTemplate.BoundText)
    On Error GoTo ProcError
    
    If oDlgDef Is Nothing Then
        xTemplate = Me.cmbTemplate.Text
        Unload Me
        Err.Raise mpError.mpError_InvalidCustomForm, , _
            "No dialog has been defined for template '" & _
                xTemplate & "'."
    End If
    
    Me.chkUseCI.Value = Abs(oDlgDef.CIFormat <> 0)
    
    With oDef
        Me.txtTemplateFileName.Text = .FileName
        Me.txtShortName.Text = .ShortName
        Me.txtDescription.Text = .Description
        Me.cmbLocation.BoundText = .TemplateType
        Me.cmbDefaultDocID.BoundText = CStr(.DefaultDocIDStamp)
        Me.chkUseAuthors.Value = Abs(oDlgDef.ShowAuthorControl)
        
        Me.txtBPSource.Text = .BoilerplateFile
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.LoadValuesStepCreateTemplate"
    Exit Sub
End Sub

Private Sub GetDefaultControls()
'cycles through properties creating default control
'definitions for those whose value source is 'Dialog Control'-
'adds all controls to controls list in dialog

    Dim i As Integer
    Dim xDef As String
    Dim xName As String
    Dim oArr As XArray
    Dim lPropID As Long
    Dim oCtlDefs As mpDB.CCustomControls
    Dim oCtlDef As mpDB.CCustomControl
    Dim xList As String
    Dim iUpper As Integer
    
    On Error GoTo ProcError

    Set oArr = g_oDB.NewXArray
    oArr.ReDim 0, -1, 0, 2
    
    'start with fresh array for definitions
    For i = 0 To Me.lstProperties.ListCount - 1
        'get property definition
        xDef = m_xPropDef(Me.lstProperties.ItemData(i))

        If xDef Like VALUE_SRC_DLG_CTL_PATTERN Then
            'the control value source is dialog control
            'create a default definition array element
            
            'get prop name
            xName = GetDelimitedStringItem(xDef, 1, "|")
            
            With oArr
                iUpper = .UpperBound(1) + 1
                .Insert 1, iUpper
                
                .Value(iUpper, 1) = xName
                .Value(iUpper, 0) = 1
                .Value(iUpper, 2) = GetDefaultControlDef(xName)
            End With
        End If
    Next i
    
    With Me.grdControls
        .Array = oArr
        .Rebind
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.GetControls"
    Exit Sub
End Sub

'Private Sub GetDefaultControls()
''cycles through properties creating default control
''definitions for those whose value source is 'Dialog Control'-
''adds all controls to controls list in dialog
'
'    Dim i As Integer
'    Dim xDef As String
'    Dim xName As String
'    Dim oArr As XArray
'    Dim lPropID As Long
'    Dim oCtlDefs As mpDB.CCustomControls
'    Dim oCtlDef As mpDB.CCustomControl
'    Dim xList As String
'    Dim iUpper As Integer
'
'    On Error GoTo ProcError
'
'    Set oArr = g_oDB.NewXArray
'    oArr.ReDim 0, -1, 0, 2
'
'    'start with fresh array for definitions
'    For i = LBound(m_xPropDef) To UBound(m_xPropDef)
'        'get property definition
'        xDef = m_xPropDef(i)
'
'        If xDef Like VALUE_SRC_DLG_CTL_PATTERN Then
'            'the control value source is dialog control
'            'create a default definition array element
'
'            'get prop name
'            xName = GetDelimitedStringItem(xDef, 1, "|")
'
'            With oArr
'                iUpper = .UpperBound(1) + 1
'                .Insert 1, iUpper
'
'                .Value(iUpper, 1) = xName
'                .Value(iUpper, 0) = 1
'                .Value(iUpper, 2) = GetDefaultControlDef(xName)
'            End With
'        End If
'    Next i
'
'    With Me.grdControls
'        .Array = oArr
'        .Rebind
'    End With
'    Exit Sub
'ProcError:
'    RaiseError "mpAdministrator.frmCreateCustomTemplate.GetControls"
'    Exit Sub
'End Sub

Private Sub GetDBControls()
'cycles through properties creating default control
'definitions for those whose value source is 'Dialog Control'-
'adds all controls to controls list in dialog

    Dim i As Integer
    Dim xDef As String
    Dim xName As String
    Dim oArr As XArray
    Dim oCtlDefs As mpDB.CCustomControls
    Dim oCtlDef As mpDB.CCustomControl
    Dim xList As String
    Dim iUpper As Integer
    
    On Error GoTo ProcError

    'start with fresh array for definitions
    Set oArr = g_oDB.NewXArray
    oArr.ReDim 0, -1, 0, 2
    
    Set oCtlDefs = g_oDB.CustomControls(Me.EditedTemplate)
    
    For i = 1 To oCtlDefs.Count
        'insert an array element
        iUpper = oArr.UpperBound(1) + 1
        oArr.Insert 1, iUpper
        
        Set oCtlDef = oCtlDefs.Item(i)
    
        With oCtlDef
            xName = .Name
            oArr.Value(iUpper, 0) = .TabID + 1
            oArr.Value(iUpper, 1) = xName
            
            'get list name
            If Not (.List Is Nothing) Then
                xList = .List.Name
            End If
            
            '9.7.3010 #4828
            'get definition
            oArr.Value(iUpper, 2) = _
                GetControlDefinition(xName, _
                 IIf(.ControlType = mpCustomControl_MultilineTextBox, mpCustomControl_TextBox, .ControlType), _
                    .Caption, .SpaceBefore, xList, .Lines, .CIList, .IsSticky, .CMLink, .CMRelatedIndex, .Format)
        End With
    Next i

    With Me.grdControls
        .Array = oArr
        .Rebind
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.GetControls"
    Exit Sub
End Sub

Public Function GetDefaultControlDef(ByVal xName As String) As String
    On Error GoTo ProcError
    If xName Like "*&[!&]*" Then
        'there is a single ampersand (i.e. hotkey)
        GetDefaultControlDef = GetControlDefinition(xName, "1", xName, "300", "", "1", 0, False, 0, 0, "")
    Else
        'there is no single ampersand (hotkey)
        GetDefaultControlDef = GetControlDefinition(xName, "1", "&" & xName, "300", "", "1", 0, False, 0, 0, "")
    End If
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.GetDefaultControlDef"
    Exit Function
End Function


Private Function GetControlDefinition(ByVal xName As String, _
    ByVal iControlType As mpDB.mpCustomControl, ByVal xLabel As String, ByVal iSpaceBefore As Integer, _
    ByVal xListSource As String, ByVal iNumLines As Integer, _
    ByVal iCIList As Integer, ByVal bIsSticky As Boolean, _
    ByVal iCMDetailField As Integer, ByVal iCMRelatedFieldIndex As Integer, _
    ByVal xFormat As String) As String
'9.7.3010 #4828

    On Error GoTo ProcError
    If Not bIsListControl(iControlType) Then
        'control is not a combo with a list -
        'ensure that there is no list source specified
        xListSource = Empty
    End If
    
    If Not (iControlType = mpCustomControl_TextBox Or _
        iControlType = mpCustomControl_MultiLineCombo) Then
        'control does not have the ability to accept
        'multiple lines - ensure that the value is 1
        iNumLines = 1
    End If
    
    GetControlDefinition = xName & SEP & _
                iControlType & SEP & xLabel & SEP & _
                iSpaceBefore & SEP & xListSource & _
                SEP & iNumLines & SEP & iCIList & SEP & bIsSticky & _
                SEP & iCMDetailField & SEP & iCMRelatedFieldIndex & SEP & xFormat
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.GetControlDefinition"
    Exit Function
End Function

Private Function CreateNode(oGroup As CTemplateGroup) As MSComctlLib.Node
'creates the specified node in tvGroups - runs recursively
    Dim oNode As MSComctlLib.Node
    Dim oExistingNode As MSComctlLib.Node
    
    On Error Resume Next
    Set oExistingNode = Me.tvGroups.Nodes.Item("%" & oGroup.ID)
    On Error GoTo ProcError
    
    If Not (oExistingNode Is Nothing) Then
        'node already exists - just return node
        Set CreateNode = oExistingNode
        Exit Function
    End If
    
    If oGroup.ParentID <> Empty Then
        Set oNode = CreateNode(g_oDB.TemplateGroups.Item(oGroup.ParentID))
    End If
        
    If oNode Is Nothing Then
        'create root node
        Set CreateNode = Me.tvGroups.Nodes.Add( _
            , , "%" & oGroup.ID, oGroup.Name)
    Else
        'create child node
        Set CreateNode = Me.tvGroups.Nodes.Add( _
        oNode, tvwChild, "%" & oGroup.ID, oGroup.Name)
    End If
    
    CreateNode.Expanded = True
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.CreateNode"
    Exit Function
End Function

Private Sub cmbDefaultDocID_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbDefaultDocID, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbDefaultDocID_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbDefaultDocID)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLocation_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbLocation, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbLocation_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbLocation)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Kill g_xDBBak
End Sub

Private Sub grdControls_ComboSelect(ByVal ColIndex As Integer)
    On Error GoTo ProcError
    MoveControlToTabGroup
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub MoveControlToTabGroup()
'moves the control adjacent to the set of
'controls that belong on the same tab
    Dim oArr As XArray
    Dim iRow As Integer
    Dim iPrevRow As Integer
    Dim iNextRow As Integer
    Dim iSelTab As Integer
    Dim iTab As Integer
    Dim i As Integer

    On Error GoTo ProcError
    With Me.grdControls
        Set oArr = .Array
        iRow = .FirstRow + .Row
        iSelTab = .Columns(0)

        'get the tab on which this
        'control will appear
        oArr.Value(iRow, 0) = iSelTab

        'loop until we get the row to
        'which it should be moved - first
        'try moving the control up in the list
        Do
            If iRow - i > 0 Then
                'the first row has not been reached-
                'get tab of previous row
                i = i + 1
                iTab = oArr.Value(iRow - i, 0)
            Else
                'we're at the first row -
                'make 'while' condition false
                'to get out of loop
                iTab = iSelTab - 1
            End If
        Loop While (iSelTab < iTab)

        If i > 1 Then
            'move it
            MoveCurrentRow -(i - 1)
            Exit Sub
        Else
            'it shouldn't be moved up -
            'try moving down to find the
            'appropriate row
            i = 0
            Do
                i = i + 1
                If iRow + i > oArr.Count(1) - 1 Then
                    'we're past the last row -
                    'make 'while' condition false
                    'to get out of loop
                   iTab = iSelTab + 1
                Else
                    iTab = oArr.Value(iRow + i, 0)
                End If
            Loop While (iSelTab >= iTab)
            If i > 1 Then
                MoveCurrentRow i - 1
                Exit Sub
            End If
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.MoveControlToTabGroup"
    Exit Sub
End Sub

Private Sub grdControls_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    On Error GoTo ProcError
    OnControlSelectionChange LastRow
    
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub OnControlSelectionChange(ByVal vPrevRow As Variant)
'saves previous row, shows controls from current row
    On Error GoTo ProcError
    If Not IsNull(vPrevRow) Then
        If vPrevRow <> "" And Me.grdControls.Array.Count(1) > 0 Then
            SaveControlDefinition vPrevRow
         End If
    End If
    
    With Me.grdControls
        'clear existing bookmark if it exists
        If .SelBookmarks.Count Then
            .SelBookmarks.Remove 0
        End If
        
        If .Row > -1 Then
            'add new bookmark
            .SelBookmarks.Add .RowBookmark(.Row)
        
            'parse the new definition
            'into the dialog controls
            ParseControlDefinition
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.OnControlSelectionChange"
    Exit Sub
End Sub

Private Sub SaveControlDefinition(ByVal iIndexToSave As Integer)
'save the current values in the dialog
'as a string to the specified array index
    Dim oArr As XArray
    
    On Error GoTo ProcError
    
    If Not m_bSkipSave Then
        Set oArr = Me.grdControls.Array '*c 4172
        If oArr.UpperBound(1) >= iIndexToSave Then '*c 4172
            Me.grdControls.Array.Value(iIndexToSave, 2) = GetControlDefinition( _
                Me.grdControls.Array.Value(iIndexToSave, 1), _
                Me.cmbControlType.BoundText, _
                Me.txtControlLabel.Text, _
                Me.txtSpaceBefore.Text, _
                Me.cmbListSource.Text, _
                IIf(Me.txtNumLines.Text <> Empty, Me.txtNumLines.Text, 1), _
                Me.cmbCIList.BoundText, _
                Me.chkIsSticky.Value = vbChecked, Me.cmbCMProperty.BoundText, _
                Me.cmbCMRelatedField.BoundText, _
                Me.cmbDateFormat.Text)
                '9.7.3010 #4828
            Me.grdControls.Refresh
        End If '*c 4172
    End If
    m_bSkipSave = False
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SaveControlDefinition"
    Exit Sub
End Sub

Private Sub lstProperties_Click()
    On Error GoTo ProcError
    With Me.lstProperties
        If (.ListIndex <> m_iCurIndex) And (.ListIndex > -1) Then
            PromptToSaveIfNecessary
            ParsePropertyDefinition
        End If
        m_bCustPropDirty = False
        m_iCurIndex = .ListIndex
    End With
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Public Function PromptToSaveIfNecessary() As Boolean
'prompts to save if the current record is dirty
    On Error GoTo ProcError
    PromptToSaveIfNecessary = True
    If m_bCustPropDirty Then
        Dim iChoice As VbMsgBoxResult
        iChoice = MsgBox("Do you want to save changes to property '" & _
            Me.txtPropertyName.Text & "'?", vbQuestion + vbYesNo, MSGBOX_TITLE)
        If iChoice = vbYes Then
            'do save
            PromptToSaveIfNecessary = SaveCustomProperty(m_iCurIndex)
        Else
            m_bCustPropDirty = False
            PromptToSaveIfNecessary = True
        End If
    End If
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.PromptToSaveIfNecessary"
    Exit Function
End Function

Private Sub ParsePropertyDefinition()
'parses the property definition string held in the
'array, and puts the values in the
'appropriate text boxes
    Dim xDef As String
    Dim xArr() As String
    
    On Error GoTo ProcError
    
    With Me.lstProperties
        'ensure that there is a selection
        .ListIndex = Max(0, .ListIndex)
        
        'get the definition by referencing the array index
        'specified as item data for the selected list row
        xDef = m_xPropDef(.ItemData(.ListIndex))
    End With
    
    If xDef = Empty Then
        Exit Sub
    End If
    
    'parse definition string into array
    mpBase2.xStringToArray xDef, xArr, 1, SEP
    
    'set values of controls
    Me.txtPropertyName.Text = xArr(0, 0)
    Me.cmbValueSource.BoundText = xArr(2, 0)
    Me.chkWholeParagraph.Value = Abs(xArr(3, 0))
    Me.txtUnderlineLength.Text = xArr(4, 0)
    Me.chkIndexed.Value = xArr(5, 0)
    Me.txtMacro.Text = xArr(6, 0)
    Me.cmbAction.BoundText = xArr(7, 0)
    m_xPropertyBookmark = xArr(8, 0)
    Me.txtPropertyName.Enabled = False
    Me.lblPropertyName.Enabled = False
    DoEvents
    m_bCustPropDirty = False
    DoEvents
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.ParsePropertyDefinition"
    Exit Sub
End Sub

Private Sub ParseControlDefinition()
'parses the control definition string held in the
'control definition array, and puts the values in the
'appropriate text boxes
    Dim xDef As String
    Dim xArr() As String
    
    On Error GoTo ProcError
    
    '*c 4172
    If IsNull(Me.grdControls.Columns(2).Value) Then
        Exit Sub
    End If
    
    xDef = Me.grdControls.Columns(2).Value
    
    If xDef = Empty Then
        Exit Sub
    End If
    
    'parse definition string into array
    mpBase2.xStringToArray xDef, xArr, 1, SEP
    
    'assign values to controls - doEvents
    'is necessary - control values don't stick otherwise
    DoEvents
    With Me.cmbControlType
        .BoundText = xArr(1, 0)
        If .BoundText <> xArr(1, 0) Then
            'this is seemingly a bug in the control -
            'try another way
            Dim oArr As XArray
            Set oArr = .Array
            Dim i As Integer
            For i = 0 To oArr.UpperBound(1)
                If xArr(1, 0) = oArr.Value(i, 1) Then
                End If
            Next i
        End If
    End With
    
    DoEvents
    Me.txtControlLabel.Text = xArr(ControlDefFields.Caption, 0)
    DoEvents
    Me.txtSpaceBefore.Text = xArr(ControlDefFields.SpaceBefore, 0)
    DoEvents
    Me.cmbListSource.BoundText = xArr(ControlDefFields.ListName, 0)
    DoEvents
    Me.udNumLines.Value = xArr(ControlDefFields.NumberOfLines, 0)
    DoEvents
    Me.cmbCIList.BoundText = xArr(ControlDefFields.CIListID, 0)
    DoEvents
    Me.cmbCMProperty.BoundText = xArr(ControlDefFields.CMDetailField, 0)
    DoEvents
    Me.cmbDateFormat.BoundText = xArr(ControlDefFields.Format, 0)
    DoEvents
    Me.cmbCMRelatedField.BoundText = xArr(ControlDefFields.CMRelatedField, 0)
    DoEvents
    If xArr(ControlDefFields.IsSticky, 0) = True Then
        Me.chkIsSticky.Value = vbChecked
    Else
        Me.chkIsSticky.Value = vbUnchecked
    End If
    DoEvents
    m_bCustPropDirty = False
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.ParseControlDefinition"
    Exit Sub
End Sub

Private Sub optCreateNew_Click()
    On Error GoTo ProcError
    Me.cmbTemplate.Enabled = (Me.optEditExisting.Value = True)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub optDelete_Click()
    On Error GoTo ProcError
    
    If Me.cmbTemplate.Enabled = False Then
        Me.cmbTemplate.Enabled = True
        
        If IsNull(Me.cmbTemplate.SelectedItem) Then
            'select the first item in the list
            Me.cmbTemplate.SelectedItem = 0
        End If
        Me.cmbTemplate.SetFocus
    End If

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub optEditExisting_Click()
    On Error GoTo ProcError
    If Me.cmbTemplate.Enabled = False Then
        Me.cmbTemplate.Enabled = True
        
        If IsNull(Me.cmbTemplate.SelectedItem) Then
            'select the first item in the list
            Me.cmbTemplate.SelectedItem = 0
        End If
        Me.cmbTemplate.SetFocus
    End If

    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTemplate_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbTemplate, Reposition
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub cmbTemplate_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(Me.cmbTemplate)
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub tvGroups_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub tvLetterheads_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub tvLetterheads_NodeCheck(ByVal Node As MSComctlLib.Node)
    On Error GoTo ProcError
    SetTab1CaptionEnable Node
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub SetTab1CaptionEnable(oSelNode As MSComctlLib.Node)
'enables tab1caption textbox if one letterhead is
'selected or tab2 or tab3 are being used
    Dim bEnableTab1Caption As Boolean
    
    On Error GoTo ProcError
    With Me.txtTab1Caption
        If Me.txtTab2Caption.Enabled Or Me.txtTab3Caption.Enabled Then
            Me.txtTab1Caption.Enabled = True
            Me.lblTab1Caption.Enabled = True
            Exit Sub
        End If
        
        If oSelNode.Checked Then
            'show tab 1 caption since letterhead tab will be visible
            .Visible = True
            .Enabled = True
            Me.lblTab1Caption.Visible = True
            Me.lblTab1Caption.Enabled = True
            If .Text = Empty Then
                'offer default tab1 caption
                .Text = "Main"
            End If
        ElseIf oSelNode.Checked = False Then
            'enable tab1caption if a letterhead node is checked
            If .Visible And Not bUseLetterhead() Then
                'disable tab1caption, but keep it visible
                .Enabled = False
                .Text = Empty
                Me.lblTab1Caption.Enabled = False
            End If
        End If
    End With

    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetTab1CaptionEnable"
    Exit Sub
End Sub

Private Function bUseLetterhead() As Boolean
'returns true if the user has specified to
'offer at least one letterhead in the dialog
    Dim oNode As MSComctlLib.Node
    On Error GoTo ProcError
    For Each oNode In Me.tvLetterheads.Nodes
        If oNode.Checked Then
            bUseLetterhead = True
            Exit For
        End If
    Next oNode
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.bUseLetterhead"
    Exit Function
End Function


Private Sub txtBPSource_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtControlLabel_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtControlLabel_KeyPress(KeyAscii As Integer)
    PreventApostrophe KeyAscii
End Sub

Private Sub txtDescription_KeyDown(KeyCode As Integer, Shift As Integer)
    m_bDescriptionEdited = True
End Sub

Private Sub txtDescription_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtDialogTitle_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtEAddressLabel_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtFaxLabel_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtMacro_Change()
    m_bCustPropDirty = True
End Sub

Private Sub txtMacro_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtMacro_KeyPress(KeyAscii As Integer)
'don't allow keys other than alphanumeric and '.'
    On Error GoTo ProcError
    If Me.txtMacro.Text = Empty And IsNumeric(Chr$(KeyAscii)) Then
        KeyAscii = 0
        MsgBox "Macro names cannot begin with a number.", vbExclamation
        Exit Sub
    End If
    AllowAlphaNumericOnly KeyAscii
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtNumLines_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPhoneLabel_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPropertyName_Change()
    m_bCustPropDirty = True
End Sub

Private Sub txtPropertyName_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtPropertyName_KeyPress(KeyAscii As Integer)
    If Me.txtPropertyName = Empty And IsNumeric(Chr$(KeyAscii)) Then
        KeyAscii = 0
        MsgBox "Property names cannot begin with a number.", vbExclamation
        Exit Sub
    End If
    AllowAlphaNumericOnly KeyAscii
End Sub

Private Sub txtShortName_KeyDown(KeyCode As Integer, Shift As Integer)
    m_bShortNameEdited = True
End Sub

Private Sub txtShortName_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtShortName_KeyPress(KeyAscii As Integer)
    PreventApostrophe KeyAscii
End Sub

Private Sub txtSpaceBefore_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtTab1Caption_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtTab2Caption_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtTab3Caption_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtTemplateFileName_Change()
    On Error GoTo ProcError
    SetDefaultNameData
    EnsureDOTExtension
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub EnsureDOTExtension()
'ensure that the filename has a .dot extension
    Dim iPos As Integer
    Dim xFile As String
    Dim iSel As Integer
    
    On Error GoTo ProcError
    
    'get selection position
    iSel = Me.txtTemplateFileName.SelStart
    
    'get template name
    xFile = Me.txtTemplateFileName.Text
    
    If g_bIsWord12x Then
        'replace any extension that is not .dot with .dot
        If Right$(xFile, 5) <> ".dotx" Then
            iPos = InStr(xFile, ".")
            If iPos Then
                'trim existing extension
                xFile = Left$(xFile, iPos - 1)
            End If
            
            'add proper extension
            xFile = xFile & ".dotx"
        End If
    Else
        'replace any extension that is not .dot with .dot
        If Right$(xFile, 4) <> ".dot" Then
            iPos = InStr(xFile, ".")
            If iPos Then
                'trim existing extension
                xFile = Left$(xFile, iPos - 1)
            End If
            
            'add proper extension
            xFile = xFile & ".dot"
        End If
    End If
    
    Me.txtTemplateFileName.Text = xFile
    
    'select original position
    Me.txtTemplateFileName.SelStart = iSel
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.EnsureDOTExtension"
    Exit Sub
End Sub
Private Sub SetDefaultNameData()
'sets the default text for ShortName and Description
'based on the text entered for filename
    Dim xTempText As String
    On Error GoTo ProcError
    With Me.txtShortName
        If Not m_bShortNameEdited Then
            Dim iPos As Integer
            'search for existence of extension in filename
            iPos = InStr(Me.txtTemplateFileName.Text, ".")
            If iPos > 0 Then
                'trim extension
                xTempText = Left(Me.txtTemplateFileName.Text, iPos - 1)
            Else
                xTempText = Me.txtTemplateFileName.Text
            End If
            
            Dim i As Integer
            Dim xChr As String
            Dim xPrevChr As String
            
            'cycle through text, adding a space
            'before all uppercase characters
            For i = 1 To Len(xTempText)
                'get character
                xChr = Mid$(xTempText, i, 1)
                
                If xPrevChr <> Empty Then
                    If (xPrevChr <> " ") And (LCase(xPrevChr) = xPrevChr) Then
                        'previous character is not a space and is lowercase
                        If Asc(xChr) >= 65 And Asc(xChr) <= 90 Then
                            'character is uppercase - add space
                           xTempText = Left$(xTempText, i - 1) & _
                            " " & Mid$(xTempText, i)
                           i = i + 1
                        End If
                    End If
                End If
                
                'set character as previous character for next iteration
                xPrevChr = xChr
            Next i
            .Text = xTempText
        End If
        
        If Not m_bDescriptionEdited Then
            Me.txtDescription.Text = xTempText & " Template"
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetDefaultNameData"
    Exit Sub
End Sub

Private Function StepIsValid(ByVal iStep As Integer) As Boolean
'returns true if the specified step contains valid data
    On Error GoTo ProcError
    Select Case iStep
        Case StepWelcome
            StepIsValid = True
        Case StepCreateTemplate
            StepIsValid = StepCreateTemplateIsValid()
        Case StepAssignTemplate
            StepIsValid = StepAssignTemplateIsValid()
        Case StepSetupCI
            StepIsValid = StepSetupCIIsValid()
        Case StepCreateProperties
            StepIsValid = StepCreatePropertiesIsValid()
        Case StepCreateDialog
            StepIsValid = StepCreateDialogIsValid()
        Case StepCreateDialog2
            StepIsValid = StepCreateDialog2IsValid()
        Case StepConfirmCreate
            StepIsValid = True
    End Select
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.StepIsValid"
    Exit Function
End Function

Private Function StepCreateTemplateIsValid() As Boolean
'returns TRUE if the 1st step contains valid data
    Dim xMsg As String
    Dim xFile As String
    
    On Error GoTo ProcError
    xFile = Me.txtTemplateFileName.Text
     
    'assume true - will be flipped if conditions
    'below are not met
    StepCreateTemplateIsValid = True

    If xFile = Empty Then
        xMsg = "The template file name is required information."
        MsgBox xMsg, vbExclamation, MSGBOX_TITLE
        Me.txtTemplateFileName.SetFocus
        OnActiveControlGotFocus False
        StepCreateTemplateIsValid = False
        Exit Function
    ElseIf Left$(xFile, 1) = "." Then
        xMsg = "Template file name cannot begin with '.'."
        MsgBox xMsg, vbExclamation, MSGBOX_TITLE
        Me.txtTemplateFileName.SetFocus
        OnActiveControlGotFocus False
        StepCreateTemplateIsValid = False
        Exit Function
    ElseIf Me.txtShortName.Text = Empty Then
        xMsg = "Template Short Name is required information."
        MsgBox xMsg, vbExclamation, MSGBOX_TITLE
        Me.txtShortName.SetFocus
        OnActiveControlGotFocus False
        StepCreateTemplateIsValid = False
        Exit Function
    ElseIf Me.txtBPSource.Text <> Empty Then
        'ensure bp file is valid - if empty we'll enter the path and name
        If Dir$(Me.txtBPSource.Text) = Empty Then
            'get template bp path
            Dim xBPPath As String
            xBPPath = mpBase2.BoilerplateDirectory
            mpBase2.EnsureTrailingSlash xBPPath
            If Dir$(xBPPath & Me.txtBPSource.Text) = Empty Then
                'bp file doesn't exist in the bp dir - raise error
                xMsg = "Invalid boilerplate file: " & Me.txtBPSource.Text
                MsgBox xMsg, vbExclamation, MSGBOX_TITLE
                Me.txtBPSource.SetFocus
                OnActiveControlGotFocus False
                StepCreateTemplateIsValid = False
                Exit Function
            End If
        End If
    Else
        Dim iPos As Integer
        iPos = InStr(xFile, ".")
        If iPos Then
            'test for proper extension
            If g_bIsWord12x Then
                If LCase(Mid(xFile, iPos)) <> ".dotx" Then
                    'extension is incorrect
                    xMsg = "Template file name must have a '.dotx' extension."
                    MsgBox xMsg, vbExclamation, MSGBOX_TITLE
                    Me.txtTemplateFileName.SetFocus
                    OnActiveControlGotFocus False
                    StepCreateTemplateIsValid = False
                    Exit Function
                End If
            Else
                If LCase(Mid(xFile, iPos)) <> ".dot" Then
                    'extension is incorrect
                    xMsg = "Template file name must have a '.dot' extension."
                    MsgBox xMsg, vbExclamation, MSGBOX_TITLE
                    Me.txtTemplateFileName.SetFocus
                    OnActiveControlGotFocus False
                    StepCreateTemplateIsValid = False
                    Exit Function
                End If
            End If
        End If
    End If
    
    If Me.EditedTemplate = Empty Then
        'we're creating a new template -
        'check for existing def with this filename
        Dim oDef As mpDB.CTemplateDef
        On Error Resume Next
        g_oDB.TemplateDefinitions.Refresh
        Set oDef = g_oDB.TemplateDefinitions.Item(xFile)
        On Error GoTo ProcError
        
        If Not (oDef Is Nothing) Then
            'fix for 4045
            xBPPath = mpBase2.BoilerplateDirectory '*c
            mpBase2.EnsureTrailingSlash xBPPath '*c
            If Dir$(xBPPath & xFile) <> Empty Then '*c
                'template def already exists - alert
                xMsg = "A template definition with the file name '" & xFile & "' already exists."
                MsgBox xMsg, vbExclamation, MSGBOX_TITLE
                Me.txtTemplateFileName.SetFocus
                OnActiveControlGotFocus False
                StepCreateTemplateIsValid = False
            End If '*c
            Exit Function
        End If
    End If
    
    Exit Function
    
ProcError:
    StepCreateTemplateIsValid = False
    RaiseError "mpAdministrator.frmCreateCustomTemplate.StepCreateTemplateIsValid"
    Exit Function
End Function

Private Function StepAssignTemplateIsValid() As Boolean
    Dim xMsg As String
    Dim oNode As Node
    
    On Error GoTo ProcError
    
    StepAssignTemplateIsValid = True
    
    For Each oNode In Me.tvGroups.Nodes
        If oNode.Checked = True Then
            Exit Function
        End If
    Next oNode
    
    'if we got here, no nodes were checked
    xMsg = "You must select at least one template group."
    MsgBox xMsg, vbExclamation, MSGBOX_TITLE
    Me.tvGroups.SetFocus
    StepAssignTemplateIsValid = False
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.StepAssignTemplateIsValid"
    Exit Function
End Function

Private Function StepSetupCIIsValid() As Boolean
    On Error GoTo ProcError
    If Me.chkUseCI.Value = vbChecked Then
        If Me.chkIncludeBCC.Value = vbUnchecked And _
            Me.chkIncludeCC.Value = vbUnchecked And _
            Me.chkIncludeFrom.Value = vbUnchecked And _
            Me.chkIncludeTo.Value = vbUnchecked Then
            
            Dim xMsg As String
            xMsg = "You must select at least one list to show."
            MsgBox xMsg, vbExclamation, MSGBOX_TITLE
            StepSetupCIIsValid = False
        Else
            StepSetupCIIsValid = True
        End If
    Else
        StepSetupCIIsValid = True
    End If
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.StepSetupCIIsValid"
    Exit Function
End Function

Private Function StepCreatePropertiesIsValid() As Boolean
    On Error GoTo ProcError
    Dim xMsg As String
    If Me.lstProperties.ListCount = 0 Then
        xMsg = "You must create at least one property for the template."
        MsgBox xMsg, vbExclamation, MSGBOX_TITLE
        Me.txtPropertyName.SetFocus
        OnActiveControlGotFocus False
        StepCreatePropertiesIsValid = False
    Else
        If Me.lstProperties.ListCount > 54 Then
            xMsg = "There is a limit of 54 properties per tab"
            MsgBox xMsg, vbExclamation, MSGBOX_TITLE
            Me.lstProperties.SetFocus
            StepCreatePropertiesIsValid = False
        Else
            StepCreatePropertiesIsValid = True
        End If
    End If
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.StepCreatePropertiesIsValid"
    Exit Function
End Function

Private Function StepCreateDialogIsValid() As Boolean
    On Error GoTo ProcError
    If Me.txtDialogTitle.Text = Empty Then
        Dim xMsg As String
        xMsg = "Dialog Title is required information."
        MsgBox xMsg, vbExclamation, MSGBOX_TITLE
        Me.txtDialogTitle.SetFocus
        OnActiveControlGotFocus False
        StepCreateDialogIsValid = False
    ElseIf Not ControlLayoutIsValid() Then
        StepCreateDialogIsValid = False
    Else
        StepCreateDialogIsValid = True
    End If
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.StepCreateDialogIsValid"
    Exit Function
End Function

Private Function ControlLayoutIsValid() As Boolean
'returns TRUE if there aren't too many of a particular
'type of control on a tab - these limits are imposed by
'the MPO.frmCustomDocObj class
    
'   first dimension is control type id,
'   second dimension is tab
    Dim iLayout(1 To 9, 1 To 3)
    
'   cycle through all controls, incrementing the number of
'   each control on each tab - use the matrix defined by the array
    Dim oArr As XArray
    On Error GoTo ProcError
    Set oArr = Me.grdControls.Array
    
    Dim i As Integer
    Dim iTab As Integer
    Dim iType As mpDB.mpCustomControl
    Dim iNumLines As Integer
    
    For i = 0 To oArr.Count(1) - 1
        'get controltype
        iType = GetDelimitedStringItem(oArr.Value(i, 2), _
            ControlDefFields.ControlType + 1, "|")
        
        'if it's a textbox, get the number of lines
        If iType = mpCustomControl_TextBox Then
            iNumLines = GetDelimitedStringItem(oArr.Value(i, 2), _
                ControlDefFields.NumberOfLines + 1, "|")
                
            'if number of lines > 1, change control type
            'to multiline textbox
            If iNumLines > 1 Then
                iType = mpCustomControl_MultilineTextBox
            End If
        End If
            
        'get tab
        iTab = oArr.Value(i, 0)
        
        'increment array element number
        iLayout(iType, iTab) = iLayout(iType, iTab) + 1
    Next i
    
    'check each array element for execessive number of controls
    For i = 1 To 3
        '9 textboxes allowed per tab
         If iLayout(mpCustomControl_TextBox, i) > 9 Then
            MsgBox "You have placed too many textboxes on tab " & i & _
                "- nine are allowed on each tab.  Please adjust your layout.", _
                vbExclamation, MSGBOX_TITLE
            Exit Function
        End If
        
        '1 multiline combo allowed per tab
         If iLayout(mpCustomControl_MultiLineCombo, i) > 1 Then
            MsgBox "You have placed too many multiline comboboxes on tab " & i & _
                "- one is allowed on each tab.  Please adjust your layout.", _
                vbExclamation, MSGBOX_TITLE
            Exit Function
        End If
        
        '1 client matter control allowed
         If iLayout(mpCustomControl_ClientMatter, i) > 1 Then
            MsgBox "You have placed too many client matter controls in the dialog " & _
                "- one is allowed.  Please adjust your layout.", _
                vbExclamation, MSGBOX_TITLE
            Exit Function
        End If
        
        '2 calendar2 allowed per tab
         If iLayout(mpCustomControl_Calendar, i) > 2 Then '*c 4046 jsw
            MsgBox "You have placed too many calendar controls on tab " & i & _
                "- two are allowed on each tab.  Please adjust your layout.", _
                vbExclamation, MSGBOX_TITLE
            Exit Function
        End If
        
        '5 multiline textboxes allowed per tab
         If iLayout(mpCustomControl_MultilineTextBox, i) > 5 Then
            MsgBox "You have placed too many multiline textboxes on tab " & i & _
                "- five are allowed on each tab.  Please adjust your layout.", _
                vbExclamation, MSGBOX_TITLE
            Exit Function
        End If
        
        '6 combos/lists/ordinal date combo allowed per tab
        Dim iNumCombos As Integer
        iNumCombos = iLayout(mpCustomControl_ComboBox, i) + _
            iLayout(mpCustomControl_ListBox, i) + _
            iLayout(mpCustomControl_OrdinalDateCombo, i)
            
         If iNumCombos > 18 Then '*c 4348
            MsgBox "You have placed too many comboboxes, listboxes, " & _
                "and/or ordinal data comboboxes on tab " & i & _
                "- a total six are allowed on each tab.  Please adjust your layout.", _
                vbExclamation, MSGBOX_TITLE
            Exit Function
        End If
    Next i
    
    'if we get here, the dialog has
    'passed layout validation
    ControlLayoutIsValid = True
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.ControlLayoutIsValid"
    Exit Function
End Function


Private Function StepCreateDialog2IsValid() As Boolean
    On Error GoTo ProcError
    'alert and return false if a visible tab caption textbox is empty
    If Me.txtTab1Caption.Enabled And Me.txtTab1Caption.Text = Empty Then
        MsgBox "Tab 1 Caption is required information.", _
            vbExclamation, MSGBOX_TITLE
        Me.txtTab1Caption.SetFocus
        OnActiveControlGotFocus False
        StepCreateDialog2IsValid = False
    ElseIf Me.txtTab2Caption.Enabled And Me.txtTab2Caption.Text = Empty Then
        MsgBox "Tab 2 Caption is required information.", _
            vbExclamation, MSGBOX_TITLE
        Me.txtTab2Caption.SetFocus
        OnActiveControlGotFocus False
        StepCreateDialog2IsValid = False
    ElseIf Me.txtTab3Caption.Enabled And Me.txtTab3Caption.Text = Empty Then
        MsgBox "Tab 3 Caption is required information.", _
            vbExclamation, MSGBOX_TITLE
        Me.txtTab1Caption.SetFocus
        OnActiveControlGotFocus False
        StepCreateDialog2IsValid = False
    Else
        StepCreateDialog2IsValid = True
    End If
    Exit Function
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.StepCreateDialog2IsValid"
    Exit Function
End Function

Private Sub txtTemplateFileName_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtTemplateFileName_KeyPress(KeyAscii As Integer)
'don't allow keys other than alphanumeric and '.'
    On Error GoTo ProcError
    If Not (bIsAlphaNumeric(KeyAscii) Or KeyAscii = 8 Or KeyAscii = 32) Then
        KeyAscii = 0
    End If
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtShortName_LostFocus()
    '#3751
    If Me.EditedTemplate = Empty Then
        Me.txtDialogTitle = "Create " & _
                            IIf(mpBase2.bIsVowel(Me.txtShortName), "an ", "a ") & _
                            Me.txtShortName
    End If
End Sub

Private Sub txtTemplateFileName_LostFocus()
    '#3751
    txtShortName_LostFocus
End Sub

Private Sub txtUnderlineLength_Change()
    m_bCustPropDirty = True
End Sub

Private Sub txtUnderlineLength_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub txtUnderlineLength_KeyPress(KeyAscii As Integer)
    'allow only digits 0-9
    If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtNumLines_KeyPress(KeyAscii As Integer)
    'allow only digits 0-9
    If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtSpaceBefore_KeyPress(KeyAscii As Integer)
    'allow only digits 0-9
    If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtControlIndex_KeyPress(KeyAscii As Integer)
    'allow only digits 0-9
    If (KeyAscii < 48 Or KeyAscii > 57) And (KeyAscii <> 8) Then
        KeyAscii = 0
    End If
End Sub

Private Sub SetPanelHelp(iStep As Steps)
    Dim xTitle As String
    Dim xDesc As String
    Dim xCaption As String
    
    On Error GoTo ProcError
    PanelHelp iStep, xCaption, xTitle, xDesc
    Me.lblPanelHelpTitle.Caption = xTitle
    Me.lblPanelHelpDescription.Caption = xDesc
    Me.Caption = "Template Architect Wizard - " & xCaption
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.SetPanelHelp"
    Exit Sub
End Sub

Private Sub PanelHelp(ByVal iStep As Steps, xDialogCaption As String, xHelpTitle As String, xHelpDescription As String)
    
    Dim iDialogStep As Integer '*c
    
    On Error GoTo ProcError
    
    
    Select Case iStep
        Case StepCreateTemplate
            xHelpTitle = "Create the Template File"
                
        Case StepAssignTemplate
            xHelpTitle = "Assign the Template"
                
        Case StepSetupCI
            xHelpTitle = "Configure Contact Integration"
                
        Case StepCreateProperties
            xHelpTitle = "Create the Template Properties"
                
        Case StepCreateDialog
            xHelpTitle = "Create the Dialog Box"
                
        Case StepCreateDialog2
            xHelpTitle = "Choose Letterhead and Dialog Tab Names"
    End Select
    
    If Me.chkUseCI = vbUnchecked And (iStep > StepSetupCI) Then
        iDialogStep = iStep - 1 '*c 4047 jsw
        'iStep = iStep - 1 '*c
    Else '*c
        iDialogStep = iStep '*c
    End If
    
    xDialogCaption = "Step " & iDialogStep & " of " & IIf(Me.chkUseCI = vbChecked, "6", "5")
    
    If g_bIsWord12x And iStep = 1 Then
        xHelpDescription = GetIni("CustomTemplateHelpCaptions", _
            "Step1_12", App.Path & "\TemplateArchitect.ini")
    Else
        xHelpDescription = GetIni("CustomTemplateHelpCaptions", _
            "Step" & iStep, App.Path & "\TemplateArchitect.ini")
    End If
    
    '*c start Fix for #4048
    'resize Panel Help labels to accomodate extra text...
    If Len(xHelpDescription) > 500 Then '*c
        Me.lblPanelHelpDescription.Top = m_PanelHelpDescTop - 200
        Me.lblPanelHelpDescription.Height = m_PanelHelpDescHeight + 200
        Me.lblControlHelp.Top = m_LabelControlHelpTop - 400
    Else
        Me.lblPanelHelpDescription.Top = m_PanelHelpDescTop
        Me.lblPanelHelpDescription.Height = m_PanelHelpDescHeight
        'Me.lblControlHelp.Top = m_LabelControlHelpTop
    End If
    '*c end
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.PanelHelp"
    Exit Sub
End Sub

Public Sub OnActiveControlGotFocus(Optional ByVal bOnTabInOnly As Boolean = True)
    'select content for textboxes and combos
    If TypeOf Me.ActiveControl Is VB.TextBox Then
        bEnsureSelectedContent Me.ActiveControl, , Not bOnTabInOnly
    ElseIf TypeOf Me.ActiveControl Is TDBCombo Then
        bEnsureSelectedContent Me.ActiveControl, , Not bOnTabInOnly
    End If
    
    'tag contains help text - show help text
    Me.lblControlHelp.Caption = GetIni("CustomTemplateHelpCaptions", _
        Me.ActiveControl.Tag, App.Path & "\TemplateArchitect.ini")
End Sub

Private Sub udNumLines_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub udSpaceBefore_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub udUnderlineLength_GotFocus()
    On Error GoTo ProcError
    OnActiveControlGotFocus
    Exit Sub
ProcError:
    g_oError.Show Err
    Exit Sub
End Sub

Private Sub FinalizeStep(ByVal iStep As Steps)
'performs any necessary functions when the user
'has finished working on the step
    On Error GoTo ProcError
    Select Case iStep
        Case StepCreateDialog
            FinalizeStepCreateDialog
    End Select
    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.FinalizeStep"
    Exit Sub
End Sub

Private Sub FinalizeStepCreateDialog()
    On Error GoTo ProcError
    With Me.grdControls
        If .Array.Count(1) > 0 Then
            SaveControlDefinition .Row + .FirstRow
        End If
    End With
    Me.grdControls.MoveFirst '*c 4175

    Exit Sub
ProcError:
    RaiseError "mpAdministrator.frmCreateCustomTemplate.FinalizeStepCreateDialog"
    Exit Sub
End Sub
