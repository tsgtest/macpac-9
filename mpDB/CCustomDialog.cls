VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
  Option Explicit

'**********************************************************
'   CCustomDialog Class
'   created 12/28/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods of a
'   MacPac Template Custom Dialog - the
'   dialog associated with a Custom Template

'   Member of App
'   Container for
'**********************************************************
Private m_xCaption As String
Private m_xTab1 As String
Private m_xTab2 As String
Private m_xTab3 As String

'**********************************************************
'   Properties
'**********************************************************
Friend Property Let Caption(xNew As String)
    m_xCaption = xNew
End Property

Public Property Get Caption() As String
    Caption = m_xCaption
End Property

Friend Property Let Tab1(xNew As String)
    m_xTab1 = xNew
End Property

Public Property Get Tab1() As String
    Tab1 = m_xTab1
End Property

Friend Property Let Tab2(xNew As String)
    m_xTab2 = xNew
End Property

Public Property Get Tab2() As String
    Tab2 = m_xTab2
End Property

Friend Property Let Tab3(xNew As String)
    m_xTab3 = xNew
End Property

Public Property Get Tab3() As String
    Tab3 = m_xTab3
End Property


