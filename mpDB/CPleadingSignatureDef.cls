VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingSignatureDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingSignatureClass
'   created 1/21/00 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define a type of MacPac Pleading

'   Member of CPleadingSignatures
'   Container for
'**********************************************************

Private m_lID As Long
Private m_xDisplayName As String
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_xSignatureBlockBookmarkSecondary As String
Private m_xClosingParagraphBookmark As String
Private m_bClosingParagraph As Boolean
Private m_bDisplayJudgeName As Boolean
Private m_bDisplaySignerName As Boolean
Private m_bFullRowFormat As Boolean
Private m_bShowFirmID As Boolean
Private m_bShowFirmAddress As Boolean
Private m_xFirmID As String
Private m_xBarIDPrefix As String
Private m_xBarIDSuffix As String
Private m_xDatedPrefix As String
Private m_xDateFormat As String
Private m_bIncludeBarID As Boolean
Private m_bAllowFirmSlogan As Boolean
Private m_bAllowAddressTab As Boolean
Private m_bAllowFirmCity As Boolean
Private m_bAllowFirmState As Boolean
Private m_bAllowOneOfSignerText As Boolean
Private m_bAllowFirmPhone As Boolean
Private m_bAllowFirmFax As Boolean
Private m_bAllowSignerEmail As Boolean
Private m_bAllowClosingParagraph As Boolean
Private m_xClosingParagraphBoilerplateFile As String
Private m_lRowsPerSignature As Long
Private m_lDefaultPosition As Long
Private m_lOfficeAddressFormat As Long
Private m_lSignerNameCase As Long
Private m_bAllowSignerEmailInput As Boolean
Private m_bIncludeSignerEmail As Boolean
Private m_xSignerEmailPrefix As String
Private m_xSignerEmailSuffix As String
Private m_bFormatFirmName As Boolean
Private m_bNonTableFormat As Boolean


Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let DisplayName(xNew As String)
    m_xDisplayName = xNew
End Property

Public Property Get DisplayName() As String
    DisplayName = m_xDisplayName
End Property

Public Property Let BarIDPrefix(xNew As String)
    m_xBarIDPrefix = xSubstitute(xNew, "|", "")
    m_xBarIDPrefix = xTranslateSpecialCharacter(m_xBarIDPrefix)
End Property

Public Property Get BarIDPrefix() As String
    BarIDPrefix = m_xBarIDPrefix
End Property

Public Property Let BarIDSuffix(xNew As String)
    m_xBarIDSuffix = xSubstitute(xNew, "|", "")
    m_xBarIDSuffix = xTranslateSpecialCharacter(m_xBarIDSuffix)
End Property

Public Property Get BarIDSuffix() As String
    BarIDSuffix = m_xBarIDSuffix
End Property

Public Property Get DatedPrefix() As String
    DatedPrefix = m_xDatedPrefix
End Property

Public Property Let DatedPrefix(xNew As String)
    m_xDatedPrefix = xSubstitute(xNew, "|", "")
    m_xDatedPrefix = xTranslateSpecialCharacter(m_xDatedPrefix)
End Property

Public Property Get DateFormat() As String
    DateFormat = m_xDateFormat
End Property

Public Property Let DateFormat(xNew As String)
    m_xDateFormat = xNew
End Property

Public Property Let DisplayJudgeName(bNew As Boolean)
    m_bDisplayJudgeName = bNew
End Property

Public Property Get DisplayJudgeName() As Boolean
    DisplayJudgeName = m_bDisplayJudgeName
End Property

Public Property Let DisplaySignerName(bNew As Boolean)
    m_bDisplaySignerName = bNew
End Property

Public Property Get DisplaySignerName() As Boolean
    DisplaySignerName = m_bDisplaySignerName
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property

Public Property Let ClosingParagraphBoilerplateFile(xNew As String)
    m_xClosingParagraphBoilerplateFile = xNew
End Property

Public Property Get ClosingParagraphBoilerplateFile() As String
    ClosingParagraphBoilerplateFile = m_xClosingParagraphBoilerplateFile
End Property

Public Property Let SignatureBlockBookmarkSecondary(xNew As String)
    m_xSignatureBlockBookmarkSecondary = xNew
End Property

Public Property Get SignatureBlockBookmarkSecondary() As String
    SignatureBlockBookmarkSecondary = m_xSignatureBlockBookmarkSecondary
End Property

Public Property Let ClosingParagraphBookmark(xNew As String)
    m_xClosingParagraphBookmark = xNew
End Property

Public Property Get ClosingParagraphBookmark() As String
    ClosingParagraphBookmark = m_xClosingParagraphBookmark
End Property

Public Property Let ClosingParagraph(bNew As Boolean)
    m_bClosingParagraph = bNew
End Property

Public Property Get ClosingParagraph() As Boolean
    ClosingParagraph = m_bClosingParagraph
End Property

Public Property Let AllowClosingParagraph(bNew As Boolean)
    m_bAllowClosingParagraph = bNew
End Property

Public Property Get AllowClosingParagraph() As Boolean
    AllowClosingParagraph = m_bAllowClosingParagraph
End Property

Public Property Let ShowFirmID(bNew As Boolean)
    m_bShowFirmID = bNew
End Property

Public Property Get ShowFirmID() As Boolean
    ShowFirmID = m_bShowFirmID
End Property

Public Property Let ShowFirmAddress(bNew As Boolean)
    m_bShowFirmAddress = bNew
End Property

Public Property Get ShowFirmAddress() As Boolean
    ShowFirmAddress = m_bShowFirmAddress
End Property

Public Property Get FullRowFormat() As Boolean
    FullRowFormat = m_bFullRowFormat
End Property

Public Property Let FullRowFormat(bNew As Boolean)
    m_bFullRowFormat = bNew
End Property

Public Property Let IncludeBarID(bNew As Boolean)
    m_bIncludeBarID = bNew
End Property

Public Property Get IncludeBarID() As Boolean
    IncludeBarID = m_bIncludeBarID
End Property

Public Property Let AllowFirmSlogan(bNew As Boolean)
    m_bAllowFirmSlogan = bNew
End Property

Public Property Get AllowFirmSlogan() As Boolean
    AllowFirmSlogan = m_bAllowFirmSlogan
End Property

Public Property Let AllowAddressTab(bNew As Boolean)
    m_bAllowAddressTab = bNew
End Property

Public Property Get AllowAddressTab() As Boolean
    AllowAddressTab = m_bAllowAddressTab
End Property

Public Property Let AllowFirmCity(bNew As Boolean)
    m_bAllowFirmCity = bNew
End Property

Public Property Get AllowFirmCity() As Boolean
    AllowFirmCity = m_bAllowFirmCity
End Property

Public Property Let AllowFirmState(bNew As Boolean)
    m_bAllowFirmState = bNew
End Property

Public Property Get AllowFirmState() As Boolean
    AllowFirmState = m_bAllowFirmState
End Property

Public Property Let AllowOneOfSignerText(bNew As Boolean)
    m_bAllowOneOfSignerText = bNew
End Property

Public Property Get AllowOneOfSignerText() As Boolean
    AllowOneOfSignerText = m_bAllowOneOfSignerText
End Property

Public Property Let AllowFirmPhone(bNew As Boolean)
    m_bAllowFirmPhone = bNew
End Property

Public Property Get AllowFirmPhone() As Boolean
    AllowFirmPhone = m_bAllowFirmPhone
End Property

Public Property Let AllowFirmFax(bNew As Boolean)
    m_bAllowFirmFax = bNew
End Property

Public Property Get AllowFirmFax() As Boolean
    AllowFirmFax = m_bAllowFirmFax
End Property

Public Property Let AllowSignerEMail(bNew As Boolean)
    m_bAllowSignerEmail = bNew
End Property

Public Property Get AllowSignerEMail() As Boolean
    AllowSignerEMail = m_bAllowSignerEmail
End Property

Public Property Let RowsPerSignature(lNew As Long)
    m_lRowsPerSignature = lNew
End Property

Public Property Get RowsPerSignature() As Long
'---defaulted to 1
    If m_lRowsPerSignature = 0 Then m_lRowsPerSignature = 1
    RowsPerSignature = m_lRowsPerSignature
End Property

Public Property Let DefaultPosition(lNew As Long)
    m_lDefaultPosition = lNew
End Property

Public Property Get DefaultPosition() As Long
'---defaulted to 2
    If m_lDefaultPosition = 0 Then m_lDefaultPosition = 2
    DefaultPosition = m_lDefaultPosition
End Property

Public Property Let OfficeAddressFormat(lNew As Long)
    m_lOfficeAddressFormat = lNew
End Property

Public Property Get OfficeAddressFormat() As Long
    OfficeAddressFormat = m_lOfficeAddressFormat
End Property

Public Property Let SignerNameCase(ByVal NewValue As SignerNameCase)
    m_lSignerNameCase = NewValue
End Property

Public Property Get SignerNameCase() As SignerNameCase
    SignerNameCase = m_lSignerNameCase
End Property

Public Property Let SignerEmailPrefix(xNew As String)
    m_xSignerEmailPrefix = xSubstitute(xNew, "|", "")
    m_xSignerEmailPrefix = xTranslateSpecialCharacter(m_xSignerEmailPrefix)
End Property

Public Property Get SignerEmailPrefix() As String
    SignerEmailPrefix = m_xSignerEmailPrefix
End Property

Public Property Let SignerEmailSuffix(xNew As String)
    m_xSignerEmailSuffix = xSubstitute(xNew, "|", "")
    m_xSignerEmailSuffix = xTranslateSpecialCharacter(m_xSignerEmailSuffix)
End Property

Public Property Get SignerEmailSuffix() As String
    SignerEmailSuffix = m_xSignerEmailSuffix
End Property

Public Property Get IncludeSignerEmail() As Boolean
    IncludeSignerEmail = m_bIncludeSignerEmail
End Property

Public Property Let IncludeSignerEmail(bNew As Boolean)
    m_bIncludeSignerEmail = bNew
End Property

Public Property Let AllowSignerEmailInput(bNew As Boolean)
    m_bAllowSignerEmailInput = bNew
End Property

Public Property Get AllowSignerEmailInput() As Boolean
    AllowSignerEmailInput = m_bAllowSignerEmailInput
End Property

Public Property Let FormatFirmName(bNew As Boolean)
    m_bFormatFirmName = bNew
End Property

Public Property Get FormatFirmName() As Boolean
    FormatFirmName = m_bFormatFirmName
End Property

Public Property Let NonTableFormat(bNew As Boolean)
    m_bNonTableFormat = bNew
End Property

Public Property Get NonTableFormat() As Boolean
    NonTableFormat = m_bNonTableFormat
End Property



