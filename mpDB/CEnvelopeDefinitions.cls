VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEnvelopeDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Envelope Definitions Collection Class
'   created 10/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Envelope defs - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_ID = 0
    xarCol_Size = 1
    xarCol_Description = 2
    xarCol_AddressLeftMin = 3
    xarCol_AddressLeftMax = 4
    xarCol_AddressTopMin = 5
    xarCol_AddressTopMax = 6
    xarCol_AllowDPhrases = 7
    xarCol_AllowBarCode = 8
    xarCol_AllowBillingNumber = 9
    xarCol_MaxLines = 10
End Enum

Private m_oRS As DAO.Recordset
Private m_oDef As mpDB.CEnvelopeDef
Private m_oArray As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
'Friend Property Get Parent() As mpDB.CApp
'    Set Parent = Application
'End Property
'
Public Property Let ListSource(xarNew As XArray)
    Set m_oArray = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshListArray
    End If
    Set ListSource = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh()
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    xSQL = "SELECT  * FROM tblEnvelopes ORDER BY fldSize"
    
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                    dbOpenSnapshot, dbReadOnly)
    
'   populate if envelopes exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If Not (m_oArray Is Nothing) Then
        RefreshListArray
    End If
End Sub

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(lID As Long) As mpDB.CEnvelopeDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim oDef As mpDB.CEnvelopeDef
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Err.Raise mpError_InvalidEnvelopeType
        Else
'           found - update current object
            UpdateObject oDef
            
'           return current object
            Set Item = oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CEnvelopesDefs.Item"
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDef = New mpDB.CEnvelopeDef
    Set m_oArray = New XArrayObject.XArray
    m_oArray.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
    Set m_oDef = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshListArray()
'fills array xArray with records
'in recordset rExisting

    Dim lRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDesc As String
    Dim vBkmk As Variant
    
    With m_oRS
        lRows = .RecordCount
    
        If lRows > 0 Then
            vBkmk = .Bookmark
            .MoveLast
            .MoveFirst
            lRows = .RecordCount
            m_oArray.ReDim 0, lRows - 1, 0, 2
            While Not .EOF
                xSize = Empty
                xID = Empty
                xDesc = Empty
                
                On Error Resume Next
                xSize = !fldSize
                xID = !fldID
                xDesc = !fldDescription
                
                m_oArray.Value(i, 0) = xID
                m_oArray.Value(i, 1) = xSize
                m_oArray.Value(i, 2) = xDesc
                .MoveNext
                i = i + 1
            Wend
            .Bookmark = vBkmk
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject(oDef As mpDB.CEnvelopeDef)
    Set oDef = New mpDB.CEnvelopeDef
    With oDef
        .ID = Empty
        .Size = Empty
        .Description = Empty
        .AddressLeftMinimum = Empty
        .AddressLeftMaximum = Empty
        .AddressTopMinimum = Empty
        .AddressTopMaximum = Empty
        .AllowBarCode = Empty
        .AllowBillingNumber = Empty
        .AllowAuthorInitials = Empty
        .AllowDPhrases = Empty
        .MaximumLines = Empty
        .BoilerplateFile = Empty
        .AllowReturnAddress = Empty
        .AddressLeftDefault = Empty
        .AddressTopDefault = Empty
        .ReturnAddressFormat = Empty
        .SplitDPhrases = Empty '9.7.1 #4024
        .DPhrase1Caption = Empty '9.7.1 #4024
        .DPhrase2Caption = Empty '9.7.1 #4024
    End With
    
    With m_oRS
        On Error Resume Next
        oDef.ID = !fldID
        oDef.Size = !fldSize
        oDef.Description = !fldDescription
        oDef.AddressLeftMinimum = !fldAddressLeftMin
        oDef.AddressLeftMaximum = !fldAddressLeftMax
        oDef.AddressTopMinimum = !fldAddressTopMin
        oDef.AddressTopMaximum = !fldAddressTopMax
        oDef.AllowBarCode = !fldAllowBarCode
        oDef.AllowBillingNumber = !fldAllowBillingNumber
        oDef.AllowAuthorInitials = !fldAllowAuthorInitials
        oDef.AllowDPhrases = !fldAllowDPhrases
        oDef.MaximumLines = !fldMaxLines
        oDef.BoilerplateFile = !fldBoilerplateFile
        oDef.AllowReturnAddress = !fldAllowReturnAddress
        oDef.AddressLeftDefault = !fldAddressLeftDefault
        oDef.AddressTopDefault = !fldAddressTopDefault
        oDef.ReturnAddressFormat = !fldReturnAddressFormat
        oDef.SplitDPhrases = !fldSplitDPhrases '9.7.1 #4024
        oDef.DPhrase1Caption = !fldDPhrase1Caption '9.7.1 #4024
        oDef.DPhrase2Caption = !fldDPhrase2Caption '9.7.1 #4024
    End With
End Function

