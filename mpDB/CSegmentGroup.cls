VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSegmentGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CSegmentGroup Class
'   created 3/8/01 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac Segment Group

'   Member of CSegmentGroups
'   Container for
'**********************************************************
Private m_lID As Long
Private m_lParentID As Long
Private m_xName As String

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let ParentID(lNew As Long)
    m_lParentID = lNew
End Property

Public Property Get ParentID() As Long
    ParentID = m_lParentID
End Property

