VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomDialogDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplate Class
'   created 2/15/00 by Mike Conner
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define a MacPac Template

'   Member of CCustomDialogDefs
'   Container for
'**********************************************************

Private m_lID As Long
Private m_xCaption As String
Private m_xTab1 As String
Private m_xTab2 As String
Private m_xTab3 As String
Private m_bShowAuthorControl As Boolean
Private m_bShowLetterheadTab As Boolean
Private m_xForm As String
Private m_lformHeight As Long
Private m_lHeight As Long
Private m_lWidth As Long
Private m_lCIFormat As Long
Private m_bIsNew As Boolean

'**********************************************************
'   Properties
'**********************************************************
Friend Property Let IsNew(bNew As Boolean)
    m_bIsNew = bNew
End Property

Friend Property Get IsNew() As Boolean
    IsNew = m_bIsNew
End Property

Public Property Let Form(xNew As String)
    m_xForm = xNew
End Property

Public Property Get Form() As String
    Form = m_xForm
End Property

Public Property Let Caption(xNew As String)
    m_xCaption = xNew
End Property

Public Property Get Caption() As String
    Caption = m_xCaption
End Property

Public Property Let Tab1(xNew As String)
    m_xTab1 = xNew
End Property

Public Property Get Tab1() As String
    Tab1 = m_xTab1
End Property

Public Property Let Tab2(xNew As String)
    m_xTab2 = xNew
End Property

Public Property Get Tab2() As String
    Tab2 = m_xTab2
End Property

Public Property Let Tab3(xNew As String)
    m_xTab3 = xNew
End Property

Public Property Get Tab3() As String
    Tab3 = m_xTab3
End Property

Public Property Let Height(lNew As Long)
    m_lHeight = lNew
End Property

Public Property Get Height() As Long
    Height = m_lHeight
End Property

Public Property Let Width(lNew As Long)
    m_lWidth = lNew
End Property

Public Property Get Width() As Long
    Width = m_lWidth
End Property

Public Property Let ShowAuthorControl(bNew As Boolean)
    m_bShowAuthorControl = bNew
End Property

Public Property Get ShowAuthorControl() As Boolean
    ShowAuthorControl = m_bShowAuthorControl
End Property

Public Property Let ShowLetterheadTab(bNew As Boolean)
    m_bShowLetterheadTab = bNew
End Property

Public Property Get ShowLetterheadTab() As Boolean
    ShowLetterheadTab = m_bShowLetterheadTab
End Property

Public Property Let CIFormat(lNew As Long)
    m_lCIFormat = lNew
End Property

Public Property Get CIFormat() As Long
    CIFormat = m_lCIFormat
End Property

'**********************************************************
'   Methods
'**********************************************************

