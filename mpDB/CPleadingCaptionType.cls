VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCaptionType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Pleading Caption Type Class
'   created 1/2/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define macpac letterhead

'   Member of
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_xBookmark As String
Private m_lCaptionSeparatorType As Long
Private m_xCaseNumber1Label As String
Private m_xCaseNumber2Label As String
Private m_xCaseNumber3Label As String
Private m_xCaseNumber4Label As String
Private m_xCaseNumber5Label As String
Private m_xCaseNumber6Label As String
Private m_xCaseNumber1PrefillText As String
Private m_xCaseNumber2PrefillText As String
Private m_xCaseNumber3PrefillText As String
Private m_xCaseNumber4PrefillText As String
Private m_xCaseNumber5PrefillText As String
Private m_xCaseNumber6PrefillText As String
Private m_bAllowDocTitleCentered As Boolean
Private m_bAllowDocTitleRight As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property
Public Property Let Bookmark(xNew As String)
    m_xBookmark = xNew
End Property
Public Property Get Bookmark() As String
    Bookmark = m_xBookmark
End Property
Public Property Let CaptionSeparatorType(lNew As Long)
    m_lCaptionSeparatorType = lNew
End Property
Public Property Get CaptionSeparatorType() As Long
    CaptionSeparatorType = m_lCaptionSeparatorType
End Property
Public Property Get CaseNumber1Label() As String
    CaseNumber1Label = m_xCaseNumber1Label
End Property
Public Property Let CaseNumber1Label(xNew As String)
    m_xCaseNumber1Label = xNew
End Property
Public Property Get CaseNumber2Label() As String
    CaseNumber2Label = m_xCaseNumber2Label
End Property
Public Property Let CaseNumber2Label(xNew As String)
    m_xCaseNumber2Label = xNew
End Property
Public Property Get CaseNumber3Label() As String
    CaseNumber3Label = m_xCaseNumber3Label
End Property
Public Property Let CaseNumber3Label(xNew As String)
    m_xCaseNumber3Label = xNew
End Property
Public Property Get CaseNumber4Label() As String
    CaseNumber4Label = m_xCaseNumber4Label
End Property
Public Property Let CaseNumber4Label(xNew As String)
    m_xCaseNumber4Label = xNew
End Property
Public Property Get CaseNumber5Label() As String
    CaseNumber5Label = m_xCaseNumber5Label
End Property
Public Property Let CaseNumber5Label(xNew As String)
    m_xCaseNumber5Label = xNew
End Property
Public Property Get CaseNumber6Label() As String
    CaseNumber6Label = m_xCaseNumber6Label
End Property
Public Property Let CaseNumber6Label(xNew As String)
    m_xCaseNumber6Label = xNew
End Property
Public Property Get CaseNumber1PrefillText() As String
    CaseNumber1PrefillText = m_xCaseNumber1PrefillText
End Property
Public Property Let CaseNumber1PrefillText(xNew As String)
    m_xCaseNumber1PrefillText = xNew
End Property
Public Property Get CaseNumber2PrefillText() As String
    CaseNumber2PrefillText = m_xCaseNumber2PrefillText
End Property
Public Property Let CaseNumber2PrefillText(xNew As String)
    m_xCaseNumber2PrefillText = xNew
End Property
Public Property Get CaseNumber3PrefillText() As String
    CaseNumber3PrefillText = m_xCaseNumber3PrefillText
End Property
Public Property Let CaseNumber3PrefillText(xNew As String)
    m_xCaseNumber3PrefillText = xNew
End Property
Public Property Get CaseNumber4PrefillText() As String
    CaseNumber4PrefillText = m_xCaseNumber4PrefillText
End Property
Public Property Let CaseNumber4PrefillText(xNew As String)
    m_xCaseNumber4PrefillText = xNew
End Property
Public Property Get CaseNumber5PrefillText() As String
    CaseNumber5PrefillText = m_xCaseNumber5PrefillText
End Property
Public Property Let CaseNumber5PrefillText(xNew As String)
    m_xCaseNumber5PrefillText = xNew
End Property
Public Property Get CaseNumber6PrefillText() As String
    CaseNumber6PrefillText = m_xCaseNumber6PrefillText
End Property
Public Property Let CaseNumber6PrefillText(xNew As String)
    m_xCaseNumber6PrefillText = xNew
End Property
Public Property Let AllowDocTitleCentered(bNew As Boolean)
    m_bAllowDocTitleCentered = bNew
End Property

Public Property Get AllowDocTitleCentered() As Boolean
    AllowDocTitleCentered = m_bAllowDocTitleCentered
End Property

Public Property Let AllowDocTitleRight(bNew As Boolean)
    m_bAllowDocTitleRight = bNew
End Property

Public Property Get AllowDocTitleRight() As Boolean
    AllowDocTitleRight = m_bAllowDocTitleRight
End Property



'**********************************************************
'   Methods
'**********************************************************

