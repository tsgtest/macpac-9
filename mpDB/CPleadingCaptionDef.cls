VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCaptionDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Pleading Caption Type Class
'   created 1/2/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define macpac letterhead

'   Member of
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_lCaptionBorderStyle As Long
Private m_lRowsPerCaption As Long
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_xLCBookmark As String
Private m_xRCBookmark As String
Private m_xTableBookmark As String
Private m_xDateTimeInfoString As String
Private m_xDateTimeInfoSeparator As String
Private m_xParty1TitleDefault As String
Private m_xParty2TitleDefault As String
Private m_xParty3TitleDefault As String
Private m_xParty1Label As String
Private m_xParty2Label As String
Private m_xParty3Label As String
Private m_lPartiesInCaption As Long
Private m_xCaseNumber1PrefillText As String
Private m_xCaseNumber2PrefillText As String
Private m_xCaseNumber3PrefillText As String
Private m_xCaseNumber4PrefillText As String
Private m_xCaseNumber5PrefillText As String
Private m_xCaseNumber6PrefillText As String
Private m_xCaseNumber1Label As String
Private m_xCaseNumber2Label As String
Private m_xCaseNumber3Label As String
Private m_xCaseNumber4Label As String
Private m_xCaseNumber5Label As String
Private m_xCaseNumber6Label As String
Private m_xCaseNumber1Separator As String
Private m_xCaseNumber2Separator As String
Private m_xCaseNumber3Separator As String
Private m_xCaseNumber4Separator As String
Private m_xCaseNumber5Separator As String
Private m_xCaseNumber6Separator As String
Private m_xOnAppealFromPrefillText As String
Private m_bIsCrossAction As Boolean
Private m_bUserBoilerplateBorders As Boolean
Private m_bIncludesCaseTitle As Boolean
Private m_bIncludesDateTimeInfo As Boolean
Private m_bIncludesPresidingJudge As Boolean
Private m_bIncludesOnAppealFromCourt As Boolean
Private m_lOnAppealFromJurisdictionsID As Long
Private m_bAllowsAdditionalInformation As Boolean
Private m_bUnderlineDocumentTitle As Boolean
Private m_bUseBoilerplateBorders As Boolean
Private m_bAllowRelatedActionInput As Boolean
Private m_lOfficeAddressFormat As Long
Private m_bFormatFirmName As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property
Public Property Let RightColBoilerplateBookmark(xNew As String)
    m_xRCBookmark = xNew
End Property
Public Property Get RightColBoilerplateBookmark() As String
    RightColBoilerplateBookmark = m_xRCBookmark
End Property
Public Property Let LeftColBoilerplateBookmark(xNew As String)
    m_xLCBookmark = xNew
End Property
Public Property Get LeftColBoilerplateBookmark() As String
    LeftColBoilerplateBookmark = m_xLCBookmark
End Property
Public Property Let PartiesInCaption(lNew As Long)
    m_lPartiesInCaption = lNew
End Property
Public Property Get PartiesInCaption() As Long
    PartiesInCaption = m_lPartiesInCaption
End Property
Public Property Let DateTimeInfoString(xNew As String)
    m_xDateTimeInfoString = xNew
End Property
Public Property Get DateTimeInfoString() As String
    DateTimeInfoString = m_xDateTimeInfoString
End Property
Public Property Let DateTimeInfoSeparator(xNew As String)
    Dim xTemp As String
    On Error Resume Next
    xTemp = xNew
'---If nothing set in db, default to two spaces
    If Len(xTemp) = 0 Then
        m_xDateTimeInfoSeparator = "  "
        Exit Property
    End If
    If InStr(xTemp, "<") Then
        xTemp = xSubstitute(xTemp, "<", "")
        xTemp = xSubstitute(xTemp, ">", "")
    End If
    If IsNumeric(Trim(xTemp)) Then
        m_xDateTimeInfoSeparator = Chr(Val(xTemp))
    Else
        m_xDateTimeInfoSeparator = xSubstitute(xTemp, "|", "")
    End If
End Property
Public Property Get DateTimeInfoSeparator() As String
    DateTimeInfoSeparator = m_xDateTimeInfoSeparator
End Property
Public Property Let Party1TitleDefault(xNew As String)
    m_xParty1TitleDefault = xNew
End Property
Public Property Get Party1TitleDefault() As String
    Party1TitleDefault = m_xParty1TitleDefault
End Property
Public Property Let Party2TitleDefault(xNew As String)
    m_xParty2TitleDefault = xNew
End Property
Public Property Get Party2TitleDefault() As String
    Party2TitleDefault = m_xParty2TitleDefault
End Property
Public Property Let Party3TitleDefault(xNew As String)
    m_xParty3TitleDefault = xNew
End Property
Public Property Get Party3TitleDefault() As String
    Party3TitleDefault = m_xParty3TitleDefault
End Property
Public Property Let Party1Label(xNew As String)
    m_xParty1Label = xNew
End Property
Public Property Get Party1Label() As String
    Party1Label = m_xParty1Label
End Property
Public Property Let Party2Label(xNew As String)
    m_xParty2Label = xNew
End Property
Public Property Get Party2Label() As String
    Party2Label = m_xParty2Label
End Property
Public Property Let Party3Label(xNew As String)
    m_xParty3Label = xNew
End Property
Public Property Get Party3Label() As String
    Party3Label = m_xParty3Label
End Property
Public Property Get CaseNumber1PrefillText() As String
    CaseNumber1PrefillText = m_xCaseNumber1PrefillText
End Property
Public Property Let CaseNumber1PrefillText(xNew As String)
    m_xCaseNumber1PrefillText = xNew
End Property
Public Property Get CaseNumber2PrefillText() As String
    CaseNumber2PrefillText = m_xCaseNumber2PrefillText
End Property
Public Property Let CaseNumber2PrefillText(xNew As String)
    m_xCaseNumber2PrefillText = xNew
End Property
Public Property Get CaseNumber3PrefillText() As String
    CaseNumber3PrefillText = m_xCaseNumber3PrefillText
End Property
Public Property Let CaseNumber3PrefillText(xNew As String)
    m_xCaseNumber3PrefillText = xNew
End Property
Public Property Get CaseNumber4PrefillText() As String
    CaseNumber4PrefillText = m_xCaseNumber4PrefillText
End Property
Public Property Let CaseNumber4PrefillText(xNew As String)
    m_xCaseNumber4PrefillText = xNew
End Property
Public Property Get CaseNumber5PrefillText() As String
    CaseNumber5PrefillText = m_xCaseNumber5PrefillText
End Property
Public Property Let CaseNumber5PrefillText(xNew As String)
    m_xCaseNumber5PrefillText = xNew
End Property
Public Property Get CaseNumber6PrefillText() As String
    CaseNumber6PrefillText = m_xCaseNumber6PrefillText
End Property
Public Property Let CaseNumber6PrefillText(xNew As String)
    m_xCaseNumber6PrefillText = xNew
End Property
Public Property Get CaseNumber1Label() As String
    CaseNumber1Label = m_xCaseNumber1Label
End Property
Public Property Let CaseNumber1Label(xNew As String)
    m_xCaseNumber1Label = xNew
End Property
Public Property Get CaseNumber2Label() As String
    CaseNumber2Label = m_xCaseNumber2Label
End Property
Public Property Let CaseNumber2Label(xNew As String)
    m_xCaseNumber2Label = xNew
End Property
Public Property Get CaseNumber3Label() As String
    CaseNumber3Label = m_xCaseNumber3Label
End Property
Public Property Let CaseNumber3Label(xNew As String)
    m_xCaseNumber3Label = xNew
End Property
Public Property Get CaseNumber4Label() As String
    CaseNumber4Label = m_xCaseNumber4Label
End Property
Public Property Let CaseNumber4Label(xNew As String)
    m_xCaseNumber4Label = xNew
End Property
Public Property Get CaseNumber5Label() As String
    CaseNumber5Label = m_xCaseNumber5Label
End Property
Public Property Let CaseNumber5Label(xNew As String)
    m_xCaseNumber5Label = xNew
End Property
Public Property Get CaseNumber6Label() As String
    CaseNumber6Label = m_xCaseNumber6Label
End Property
Public Property Let CaseNumber6Label(xNew As String)
    m_xCaseNumber6Label = xNew
End Property
Public Property Get CaseNumber1Separator() As String
    CaseNumber1Separator = m_xCaseNumber1Separator
End Property
Public Property Let CaseNumber1Separator(xNew As String)
    If IsNumeric(Trim(xNew)) Then
        m_xCaseNumber1Separator = Chr(Val(xNew))
    Else
        m_xCaseNumber1Separator = xSubstitute(xNew, "|", "")
    End If
End Property
Public Property Get CaseNumber2Separator() As String
    CaseNumber2Separator = m_xCaseNumber2Separator
End Property
Public Property Let CaseNumber2Separator(xNew As String)
    If IsNumeric(Trim(xNew)) Then
        m_xCaseNumber2Separator = Chr(Val(xNew))
    Else
        m_xCaseNumber2Separator = xSubstitute(xNew, "|", "")
    End If
End Property
Public Property Get CaseNumber3Separator() As String
    CaseNumber3Separator = m_xCaseNumber3Separator
End Property
Public Property Let CaseNumber3Separator(xNew As String)
    If IsNumeric(Trim(xNew)) Then
        m_xCaseNumber3Separator = Chr(Val(xNew))
    Else
        m_xCaseNumber3Separator = xSubstitute(xNew, "|", "")
    End If
End Property
Public Property Get CaseNumber4Separator() As String
    CaseNumber4Separator = m_xCaseNumber4Separator
End Property
Public Property Let CaseNumber4Separator(xNew As String)
    If IsNumeric(Trim(xNew)) Then
        m_xCaseNumber4Separator = Chr(Val(xNew))
    Else
        m_xCaseNumber4Separator = xSubstitute(xNew, "|", "")
    End If
End Property
Public Property Get CaseNumber5Separator() As String
    CaseNumber5Separator = m_xCaseNumber5Separator
End Property
Public Property Let CaseNumber5Separator(xNew As String)
    If IsNumeric(Trim(xNew)) Then
        m_xCaseNumber5Separator = Chr(Val(xNew))
    Else
        m_xCaseNumber5Separator = xSubstitute(xNew, "|", "")
    End If
End Property
Public Property Get CaseNumber6Separator() As String
    CaseNumber6Separator = m_xCaseNumber6Separator
End Property
Public Property Let CaseNumber6Separator(xNew As String)
    If IsNumeric(Trim(xNew)) Then
        m_xCaseNumber6Separator = Chr(Val(xNew))
    Else
        m_xCaseNumber6Separator = xSubstitute(xNew, "|", "")
    End If
End Property

Public Property Get OnAppealFromPrefillText() As String
    OnAppealFromPrefillText = m_xOnAppealFromPrefillText
End Property
Public Property Let OnAppealFromPrefillText(xNew As String)
    m_xOnAppealFromPrefillText = xNew
End Property
Public Property Let IsCrossAction(bNew As Boolean)
    m_bIsCrossAction = bNew
End Property
Public Property Get IsCrossAction() As Boolean
    IsCrossAction = m_bIsCrossAction
End Property
Public Property Let IncludesCaseTitle(bNew As Boolean)
    m_bIncludesCaseTitle = bNew
End Property
Public Property Get IncludesCaseTitle() As Boolean
    IncludesCaseTitle = m_bIncludesCaseTitle
End Property
Public Property Let IncludesDateTimeInfo(bNew As Boolean)
    m_bIncludesDateTimeInfo = bNew
End Property
Public Property Get IncludesDateTimeInfo() As Boolean
    IncludesDateTimeInfo = m_bIncludesDateTimeInfo
End Property
Public Property Let IncludesOnAppealFromCourt(bNew As Boolean)
    m_bIncludesOnAppealFromCourt = bNew
End Property
Public Property Get IncludesOnAppealFromCourt() As Boolean
    IncludesOnAppealFromCourt = m_bIncludesOnAppealFromCourt
End Property
Public Property Let IncludesPresidingJudge(bNew As Boolean)
    m_bIncludesPresidingJudge = bNew
End Property
Public Property Get IncludesPresidingJudge() As Boolean
    IncludesPresidingJudge = m_bIncludesPresidingJudge
End Property
Public Property Let AllowsAdditionalInformation(bNew As Boolean)
    m_bAllowsAdditionalInformation = bNew
End Property
Public Property Get AllowsAdditionalInformation() As Boolean
    AllowsAdditionalInformation = m_bAllowsAdditionalInformation
End Property
Public Property Let UnderlineDocumentTitle(bNew As Boolean)
    m_bUnderlineDocumentTitle = bNew
End Property
Public Property Get UnderlineDocumentTitle() As Boolean
    UnderlineDocumentTitle = m_bUnderlineDocumentTitle
End Property
Public Property Let CaptionBorderStyle(lNew As Long)
    m_lCaptionBorderStyle = lNew
End Property
Public Property Get CaptionBorderStyle() As Long
    CaptionBorderStyle = m_lCaptionBorderStyle
End Property
Public Property Let UseBoilerplateBorders(bNew As Boolean)
    m_bUseBoilerplateBorders = bNew
End Property
Public Property Get UseBoilerplateBorders() As Boolean
    UseBoilerplateBorders = m_bUseBoilerplateBorders
End Property
Public Property Let RowsPerCaption(lNew As Long)
    m_lRowsPerCaption = lNew
End Property
Public Property Get RowsPerCaption() As Long
    RowsPerCaption = Max(CDbl(m_lRowsPerCaption), 1)
End Property
Public Property Let OnAppealFromJurisdictionsID(lNew As Long)
    m_lOnAppealFromJurisdictionsID = lNew
End Property
Public Property Get OnAppealFromJurisdictionsID() As Long
    OnAppealFromJurisdictionsID = m_lOnAppealFromJurisdictionsID
End Property

Public Property Let AllowRelatedActionInput(bNew As Boolean)
    m_bAllowRelatedActionInput = bNew
End Property
Public Property Get AllowRelatedActionInput() As Boolean
    AllowRelatedActionInput = m_bAllowRelatedActionInput
End Property

Public Property Let OfficeAddressFormat(lNew As Long)
    m_lOfficeAddressFormat = lNew
End Property

Public Property Get OfficeAddressFormat() As Long
    OfficeAddressFormat = m_lOfficeAddressFormat
End Property

Public Property Let FormatFirmName(bNew As Boolean)
    m_bFormatFirmName = bNew
End Property

Public Property Get FormatFirmName() As Boolean
    FormatFirmName = m_bFormatFirmName
End Property





'**********************************************************
'   Methods
'**********************************************************
