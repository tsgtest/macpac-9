VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVerificationDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CVerificationDef Class
'   created 8/8/00 by Ajax Green-
'   ajax12@home.net

'   Contains properties and methods that define
'   a type of MacPac Verification Document

'   Member of CVerificationDefs
'   Container for COfficeAddressFormat
'**********************************************************

Private m_lID As Long
Private m_iLevel0 As Integer
Private m_xDesc As String
Private m_xBPFile As String
Private m_xDlgCaption As String
Private m_lDefaultPleadingPaperType As Long
Private m_bUpperCaseFirmName As Boolean
Private m_xDefaultFooterText As String
Private m_bytFooterTextMax As Byte
Private m_bAllowPartyName As Boolean
Private m_bAllowPartyTitle As Boolean
Private m_bAllowDeceasedName As Boolean
Private m_bAllowCorporationName As Boolean
Private m_bAllowCorporatePosition As Boolean
Private m_bAllowCity As Boolean
Private m_bAllowCounty As Boolean
Private m_bAllowState As Boolean
Private m_bAllowAttorneyFor As Boolean
Private m_bAllowGender As Boolean
Private m_bAllowNotaryName As Boolean
Private m_bAllowExecuteDate As Boolean
Private m_xDefExecuteDate As String
Private m_lDateFormats As Long
Private m_oAddressFormat As mpDB.COfficeAddressFormat
Private m_xCustom1Label As String
Private m_xCustom1Text As String
Private m_xCustom2Label As String
Private m_xCustom2Text As String
Private m_bUseBoilerplateFooterSetup As Boolean
Private m_bytFirstNumberedPage As Byte
Private m_bytFirstPageNumber As Byte
'CharlieCore 9.2.0 - BaseStyle
Private m_xBaseStyle As String

Friend Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let DateFormats(lNew As Long)
    m_lDateFormats = lNew
End Property

Public Property Get DateFormats() As Long
    DateFormats = m_lDateFormats
End Property

Public Property Let FirstNumberedPage(bytNew As Byte)
    m_bytFirstNumberedPage = bytNew
End Property

Public Property Get FirstNumberedPage() As Byte
    FirstNumberedPage = m_bytFirstNumberedPage
End Property

Public Property Let FirstPageNumber(bytNew As Byte)
    m_bytFirstPageNumber = bytNew
End Property

Public Property Get FirstPageNumber() As Byte
    FirstPageNumber = m_bytFirstPageNumber
End Property

Public Property Let UseBoilerplateFooterSetup(bNew As Boolean)
    m_bUseBoilerplateFooterSetup = bNew
End Property

Public Property Get UseBoilerplateFooterSetup() As Boolean
    UseBoilerplateFooterSetup = m_bUseBoilerplateFooterSetup
End Property

Friend Property Set AddressFormat(oNew As mpDB.COfficeAddressFormat)
    Set m_oAddressFormat = oNew
End Property

Public Property Get AddressFormat() As mpDB.COfficeAddressFormat
    Set AddressFormat = m_oAddressFormat
End Property

Friend Property Let Level0(iNew As Integer)
    m_iLevel0 = iNew
End Property

Public Property Get Level0() As Integer
    Level0 = m_iLevel0
End Property

Friend Property Let FooterTextMaxChars(lNew As Long)
    m_bytFooterTextMax = lNew
End Property

Public Property Get FooterTextMaxChars() As Long
    FooterTextMaxChars = m_bytFooterTextMax
End Property

Friend Property Let UpperCaseFirmName(bNew As Boolean)
    m_bUpperCaseFirmName = bNew
End Property

Public Property Get UpperCaseFirmName() As Boolean
    UpperCaseFirmName = m_bUpperCaseFirmName
End Property

Friend Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Friend Property Let DefaultFooterText(xNew As String)
    m_xDefaultFooterText = xNew
End Property

Public Property Get DefaultFooterText() As String
    DefaultFooterText = m_xDefaultFooterText
End Property

Friend Property Let DefaultExecuteDate(xNew As String)
    m_xDefExecuteDate = xNew
End Property

Public Property Get DefaultExecuteDate() As String
    DefaultExecuteDate = m_xDefExecuteDate
End Property

Friend Property Let AllowPartyName(bNew As Boolean)
    m_bAllowPartyName = bNew
End Property

Public Property Get AllowPartyName() As Boolean
    AllowPartyName = m_bAllowPartyName
End Property

Friend Property Let AllowPartyTitle(bNew As Boolean)
    m_bAllowPartyTitle = bNew
End Property

Public Property Get AllowPartyTitle() As Boolean
    AllowPartyTitle = m_bAllowPartyTitle
End Property

Friend Property Let AllowDeceasedName(bNew As Boolean)
    m_bAllowDeceasedName = bNew
End Property

Public Property Get AllowDeceasedName() As Boolean
    AllowDeceasedName = m_bAllowDeceasedName
End Property

Friend Property Let AllowCorporationName(bNew As Boolean)
    m_bAllowCorporationName = bNew
End Property

Public Property Get AllowCorporationName() As Boolean
    AllowCorporationName = m_bAllowCorporationName
End Property

Friend Property Let AllowCorporatePosition(bNew As Boolean)
    m_bAllowCorporatePosition = bNew
End Property

Public Property Get AllowCorporatePosition() As Boolean
    AllowCorporatePosition = m_bAllowCorporatePosition
End Property

Friend Property Let AllowGender(bNew As Boolean)
    m_bAllowGender = bNew
End Property

Public Property Get AllowGender() As Boolean
    AllowGender = m_bAllowGender
End Property

Friend Property Let AllowNotaryName(bNew As Boolean)
    m_bAllowNotaryName = bNew
End Property

Public Property Get AllowNotaryName() As Boolean
    AllowNotaryName = m_bAllowNotaryName
End Property

Friend Property Let AllowExecuteDate(bNew As Boolean)
    m_bAllowExecuteDate = bNew
End Property

Public Property Get AllowExecuteDate() As Boolean
    AllowExecuteDate = m_bAllowExecuteDate
End Property

Friend Property Let AllowCounty(bNew As Boolean)
    m_bAllowCounty = bNew
End Property

Public Property Get AllowCounty() As Boolean
    AllowCounty = m_bAllowCounty
End Property

Friend Property Let AllowState(bNew As Boolean)
    m_bAllowState = bNew
End Property

Public Property Get AllowState() As Boolean
    AllowState = m_bAllowState
End Property

Friend Property Let AllowAttorneyFor(bNew As Boolean)
    m_bAllowAttorneyFor = bNew
End Property

Public Property Get AllowAttorneyFor() As Boolean
    AllowAttorneyFor = m_bAllowAttorneyFor
End Property

Friend Property Let AllowCity(bNew As Boolean)
    m_bAllowCity = bNew
End Property

Public Property Get AllowCity() As Boolean
    AllowCity = m_bAllowCity
End Property

Friend Property Let DialogCaption(xNew As String)
    m_xDlgCaption = xNew
End Property

Public Property Get DialogCaption() As String
    DialogCaption = m_xDlgCaption
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Private Sub Class_Terminate()
    Set m_oAddressFormat = Nothing
End Sub
Public Property Get Custom1Label() As String
    Custom1Label = m_xCustom1Label
End Property
Public Property Let Custom1Label(xNew As String)
    m_xCustom1Label = xNew
End Property
Public Property Get Custom1DefaultText() As String
    Custom1DefaultText = m_xCustom1Text
End Property
Public Property Let Custom1DefaultText(xNew As String)
    m_xCustom1Text = xNew
End Property
Public Property Get Custom2Label() As String
    Custom2Label = m_xCustom2Label
End Property
Public Property Let Custom2Label(xNew As String)
    m_xCustom2Label = xNew
End Property
Public Property Get Custom2DefaultText() As String
    Custom2DefaultText = m_xCustom2Text
End Property
Public Property Let Custom2DefaultText(xNew As String)
    m_xCustom2Text = xNew
End Property
Public Property Get DefaultPleadingPaperType() As Long
    DefaultPleadingPaperType = m_lDefaultPleadingPaperType
End Property
Public Property Let DefaultPleadingPaperType(lNew As Long)
    m_lDefaultPleadingPaperType = lNew
End Property
'CharlieCore 9.2.0 - BaseStyle
Public Property Get BaseStyle() As String
    BaseStyle = m_xBaseStyle
End Property
Public Property Let BaseStyle(xNew As String)
    m_xBaseStyle = xNew
End Property

