VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingSeparateStatementDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Pleading Separate Statement Type Class
'   created 1/2/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define macpac letterhead

'   Member of
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xLeftColumnHeading As String
Private m_xRightColumnHeading As String
Private m_xDescription As String
Private m_xTitle As String
Private m_xBoilerplateFile As String



'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let LeftColumnHeading(xNew As String)
    m_xLeftColumnHeading = xNew
End Property

Public Property Get LeftColumnHeading() As String
    LeftColumnHeading = m_xLeftColumnHeading
End Property

Public Property Let RightColumnHeading(xNew As String)
    m_xRightColumnHeading = xNew
End Property

Public Property Get RightColumnHeading() As String
    RightColumnHeading = m_xRightColumnHeading
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let Title(xNew As String)
    m_xTitle = xNew
End Property

Public Property Get Title() As String
    Title = m_xTitle
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property
