VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersonOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   PersonOptions Collection Class
'   created 12/23/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of PersonOptions

'   Member of CPerson, COffice
'   Container for CPersonOption
'**********************************************************

'**********************************************************
Public Enum mpPersonOptionSources
    mpPersonOptionSources_None = 0
    mpPersonOptionSources_Personal = 1
    mpPersonOptionSources_Office = 2
    mpPersonOptionSources_Firm = 3
End Enum

Private m_colPersonOptions As Collection
Private m_colPersonOptionsForm As Collection
Private m_PersonOption As mpDB.CPersonOption
Private m_Owner As mpDB.CPerson
Private m_iSource As mpPersonOptionSources
Private m_objParent As Object
Private m_bIsDirty As Boolean
Private m_xTemplate As String
'**********************************************************

'************************************************************************
'Properties
'************************************************************************
Public Property Get LastEditTime() As Date
    LastEditTime = Me.Item("fldLastEditTime")
End Property

Public Property Let IsDirty(bNew As Boolean)
    m_bIsDirty = bNew
End Property

Public Property Get IsDirty() As Boolean
    IsDirty = m_bIsDirty
End Property

Public Property Let Template(xNew As String)
    m_xTemplate = xNew
End Property

Public Property Get Template() As String
    Template = m_xTemplate
End Property

Public Property Let Parent(objNew As Object)
    Set m_objParent = objNew
End Property

Public Property Get Parent() As Object
    Set Parent = m_objParent
End Property

Public Property Let Source(iNew As mpPersonOptionSources)
    m_iSource = iNew
End Property

Public Property Get Source() As mpPersonOptionSources
    Source = m_iSource
End Property

Public Property Let Owner(psnNew As mpDB.CPerson)
    Set m_Owner = psnNew
End Property

Public Property Get Owner() As mpDB.CPerson
    Set Owner = m_Owner
End Property

Public Property Get Exists() As Boolean
    Exists = HasPrivateOptions
End Property
'************************************************************************
' Methods
'************************************************************************
Public Function Active() As mpDB.CPersonOption
    Set Active = m_PersonOption
End Function

Public Sub Copy(oDest As mpDB.CPerson)
'copies options to person oDest
    Dim oDestOptions As mpDB.CPersonOptions
    Dim xMsg As String
    Dim iChoice As VbMsgBoxResult
    Dim i As Integer
    
'   edit options of selected author
    Set oDestOptions = oDest.Options(Me.Template)
            
'   if author already has specified
'   options, prompt to replace
    If oDestOptions.Exists Then
        xMsg = "You have previously edited the " & Me.Template & _
               " options for " & oDest.FullName & "." & vbCr & _
               "Do you want to replace these options?"
        iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
        If iChoice = vbNo Then
            Exit Sub
        End If
    End If
            
'   cycle through fields, assigning new value
    For i = 1 To oDestOptions.Count
        With oDestOptions.Item(i)
            If UCase(.Name) <> "FLDID" Then
                .Value = Me.Item(.Name).Value
            End If
        End With
    Next i
    
'   save option set
    With oDestOptions
        .IsDirty = True
        .Save
    End With
    
    Set oDestOptions = Nothing
End Sub

Public Sub Save()
' saves options to db
    Dim i As Integer
    Dim xSQL As String
    Dim rs As DAO.Recordset
    Dim fldP As DAO.Field
    Dim opiP As mpDB.CPersonOption
    Dim lID As Long
    
    On Error GoTo ProcError
    
    If Me.IsDirty Then
'       if record exists, update record
'       else create record, and update new record
'       check for existence in private db
        lID = Me.Parent.ID
        
'       err if option set is missing necessary id info
        If lID = 0 Or Me.Template = "" Then
            Err.Raise mpError_FailedSavingOptionSet_IDsMissing
        End If
    
        xSQL = "SELECT  * FROM " & Application.TemplateDefinitions(Me.Template).OptionsTable & _
                      " WHERE fldID = " & lID

'       open recordset
        Set rs = g_App.PrivateDB.OpenRecordset(xSQL, dbOpenDynaset)
        
'       add/edit as necessary
        With rs
            If Not (.BOF And .EOF) Then
                .Edit
            Else
                .AddNew
                !fldID = lID
            End If
            
'           cycle through fields in collection, updating
'           the value of corresponding collection member
            For Each fldP In .Fields
                If fldP.Name <> "fldID" Then
                    Set opiP = m_colPersonOptions.Item(fldP.Name)
'************************************************
'per log item 1792 - df
                    If opiP.Value = Empty Then
                        If fldP.Type <> dbText Then
                            opiP.Value = 0
                        End If
                    End If
'************************************************
                    fldP.Value = opiP.Value
                End If
            Next fldP

'           update last edit time
            .Fields("fldLastEditTime") = Now()

            .Update
        End With
        Me.IsDirty = False
    End If
    Exit Sub
    
ProcError:
    RaiseError "mpDB.CPersonOptions.Save"
    Exit Sub
End Sub

Public Sub Delete(Optional ByVal lID As Long, Optional ByVal xTemplate As String)
' deletes specified options from db
    Dim xSQL As String
    Dim xOptTbl As String
    
    On Error GoTo ProcError
    
'       if record exists, update record
'       else create record, and update new record
'       check for existence in private db
        If lID = 0 Then
            lID = Me.Parent.ID
        End If
        
        If xTemplate = "" Then
            xTemplate = Me.Template
        End If
        
'       err if option set is missing necessary id info
        If lID = 0 Or xTemplate = "" Then
            Err.Raise mpError_FailedSavingOptionSet_IDsMissing
        End If
    
'       get options table name
        xOptTbl = Application.TemplateDefinitions _
                  .Item(xTemplate).OptionsTable
        
        xSQL = "DELETE  * FROM " & xOptTbl & _
               " WHERE fldID = " & lID

'       execute
        g_App.PrivateDB.Execute xSQL
        Exit Sub
        
ProcError:
    RaiseError "mpDB.CPersonOptions.Delete"
    Exit Sub
End Sub

Public Function Count() As Long
    Count = m_colPersonOptions.Count
End Function

Public Function Item(vID As Variant) As mpDB.CPersonOption
Attribute Item.VB_UserMemId = 0
    On Error Resume Next
    Set Item = m_colPersonOptions.Item(vID)
    If Item Is Nothing Then
        Err.Raise mpError_InvalidItemIndex, , "'" & vID & "'" & " is not a valid person option for the " & Me.Template & " template."
    End If
End Function

Public Sub Refresh(Optional DoXArray As Boolean = True, Optional frmP As Object)
'opens the appropriate recordset as the collection
'source and fills an xarray
    Dim xSQL As String
    Dim rs As DAO.Recordset
    Dim fldP As DAO.Field
    Dim opiTemp As mpDB.CPersonOption
    Dim lID As Long
    Dim xOptionsTable As String
    
    On Error GoTo ProcError
    
    Set m_colPersonOptions = New Collection
    Set m_colPersonOptionsForm = New Collection
    
    xOptionsTable = Application.TemplateDefinitions(Me.Template).OptionsTable
    
    If xOptionsTable = Empty Then
        Exit Sub
    End If
    
    If Me.Parent.ID = Empty Then
        lID = 0
    Else
        lID = Me.Parent.ID
    End If
        
    xSQL = "SELECT * FROM " & xOptionsTable & _
           " WHERE fldID = " & lID
    
'   open recordset
    On Error GoTo ProcError_NoPrivatePrefs
    Set rs = g_App.PrivateDB.OpenRecordset(xSQL, _
                    dbOpenSnapshot, dbReadOnly)
                                      
    On Error GoTo ProcError
    
    '*** 9.7.1 8/28/05 - #4076 jts - Check for Public Author defaults in mpPublic
    If rs.RecordCount = 0 Then
        If g_App.People.IsInDB(g_App.PublicDB, lID) Then
            rs.Close
            Set rs = g_App.PublicDB.OpenRecordset(xSQL, _
                    dbOpenSnapshot, dbReadOnly)
        End If
    End If
    '*** END 9.7.1
    
    Me.Source = mpPersonOptionSources_Personal
    
    If rs.RecordCount = 0 Then
        '*** 9.7.1 #4163
        If g_App.People.IsInPeopleList(lID) Then
            If g_App.People(lID).IsAttorney Then
                rs.Close
    '           check for Attorney defaults options for office
                xSQL = "SELECT  * FROM " & xOptionsTable & _
                              " WHERE fldID = -" & (100000 + Application.People(lID).Office.ID)
    '           open recordset
                Set rs = g_App.PublicDB.OpenRecordset(xSQL, _
                                dbOpenSnapshot, dbReadOnly)
            End If
            If rs.RecordCount = 0 Then
                rs.Close
    '           attempt to get Author's default office options
                xSQL = "SELECT  * FROM " & xOptionsTable & _
                              " WHERE fldID = -" & Application.People(lID).Office.ID
    '           open recordset
                Set rs = g_App.PublicDB.OpenRecordset(xSQL, _
                                dbOpenSnapshot, dbReadOnly)
            End If
        End If
        '*** end #4163
        
        
        Me.Source = mpPersonOptionSources_Office
    
        '*** 9.7.1 #4163
        If rs.RecordCount = 0 Then
            If g_App.People.IsInPeopleList(lID) Then
                If g_App.People(lID).IsAttorney Then
                    rs.Close
                    
        '           get firm Attorney options
                    xSQL = "SELECT  * FROM " & xOptionsTable & _
                           " WHERE fldID = " & mpOptions_FirmDefaultID + 1
                    
        '           open recordset
                    Set rs = g_App.PublicDB.OpenRecordset(xSQL, _
                                dbOpenSnapshot, dbReadOnly)
                                                      
                    Me.Source = mpPersonOptionSources_Firm
                End If
            End If
        End If
        '*** end #4163
        
        If rs.RecordCount = 0 Then
            rs.Close
            
'           get firm options
            xSQL = "SELECT  * FROM " & xOptionsTable & _
                   " WHERE fldID = " & mpOptions_FirmDefaultID
            
'           open recordset
            Set rs = g_App.PublicDB.OpenRecordset(xSQL, _
                        dbOpenSnapshot, dbReadOnly)
                                              
            Me.Source = mpPersonOptionSources_Firm  '9.7.1 - #4180
        End If
        
        If rs.RecordCount = 0 Then
            rs.Close
            
'           something is dreadfully wrong
            Err.Raise mpError_NoFirmOptionsRecord, _
                    "mpDB.OptionsSetItems.Refresh"
        End If
    End If

'   cycle through fields, adding to collection
    For Each fldP In rs.Fields
        Set opiTemp = New mpDB.CPersonOption
        With opiTemp
            .Name = fldP.Name
            .Value = fldP.Value
        End With
        m_colPersonOptions.Add opiTemp, opiTemp.Name
        
        Set opiTemp = New mpDB.CPersonOption
        
        Dim i As Integer
        Dim ctlCtl As Control
        If Not frmP Is Nothing Then
            On Error Resume Next
            For Each ctlCtl In frmP.Controls
                If ctlCtl.Name = fldP.Name Then
                    If TypeOf ctlCtl Is VB.TextBox Then
                        opiTemp.Name = fldP.Name
                        opiTemp.Value = ctlCtl.Text
                    ElseIf TypeOf ctlCtl Is VB.CheckBox Then
                        opiTemp.Name = fldP.Name
                        opiTemp.Value = ctlCtl.Value
                    ElseIf InStr(ctlCtl.Name, "spn") Then
                        opiTemp.Name = fldP.Name
                        opiTemp.Value = ctlCtl.Value
                    ElseIf ctlCtl.DataMode = 4 Then     'it's  a tdbcombo
                        If InStr(fldP.Name, "spn") = 0 Then
                            opiTemp.Name = fldP.Name
                            opiTemp.Value = ctlCtl.BoundText
                        End If
                    End If
                    
                    m_colPersonOptionsForm.Add opiTemp, opiTemp.Name
                    Exit For
                End If
            Next
        End If
    
    Next fldP
    rs.Close
    Exit Sub
ProcError_NoPrivatePrefs:
    Select Case Err
        Case 3078   'no pref table in Private DB
            Set rs = CreatePrivateDBPrefTable(xOptionsTable, xSQL)
            Resume Next
        Case Else
            RaiseError "mpDB.CPersonOptions.Refresh"
            Exit Sub
    End Select
    Exit Sub
    
ProcError:
    RaiseError "mpDB.CPersonOptions.Refresh"
    Exit Sub
End Sub
    
Private Function HasPrivateOptions() As Boolean
'returns TRUE if person has a private
'options record for the specified template
    Dim xSQL As String
    Dim rs As DAO.Recordset
    Dim xOptionsTable As String
    
    xOptionsTable = Application _
        .TemplateDefinitions(Me.Template).OptionsTable
    
    If Len(xOptionsTable) Then
        xSQL = "SELECT  * FROM " & xOptionsTable & _
                " WHERE fldID = " & Me.Parent.ID
    
'       open recordset
        Set rs = g_App.PrivateDB.OpenRecordset(xSQL, _
                            dbOpenSnapshot, dbReadOnly)
                                      
        If rs.RecordCount > 0 Then
            HasPrivateOptions = True
        End If
            
        rs.Close
    End If
End Function

Private Function CreatePrivateDBPrefTable(xTableName As String, _
                                          xSQL As String) As DAO.Recordset
'creates a table xTableName
'with field fldID as primary
'key in database dbMP

    Dim tdfOptions As DAO.TableDef
    Dim tdfPublic As DAO.TableDef
    Dim indPrimaryKey As Object
    Dim fldPrimaryKey As Object
    Dim rOptions As DAO.Recordset
    Dim fld As DAO.Field
    Dim fldTemp As DAO.Field
    
    
'   set ref to public preference table
    Set tdfPublic = g_App.PublicDB.TableDefs(xTableName)
    
'   create table
    Set tdfOptions = g_App.PrivateDB.CreateTableDef()
    tdfOptions.Name = xTableName
            
'   create/add ID field
    Set fldPrimaryKey = tdfOptions.CreateField _
            (Name:="fldID", Type:=dbLong)
    tdfOptions.Fields.Append fldPrimaryKey
                
'   create primary key
    Set indPrimaryKey = tdfOptions.CreateIndex()
    With indPrimaryKey
        .Name = "Primary Key"
        .Primary = True
        .Fields = "fldID"
    End With
    
'   add index to tabledef
    tdfOptions.Indexes.Append indPrimaryKey

'   add table to db
    g_App.PrivateDB.TableDefs.Append tdfOptions
            
    On Error Resume Next
'   cycle through public pref table and add fields to private
    For Each fld In tdfPublic.Fields
        If fld.Name <> "fldID" Then
            Set fldTemp = tdfOptions.CreateField()
            With fldTemp
                .Name = fld.Name
                .Type = fld.Type
                .AllowZeroLength = True
                .DefaultValue = fld.DefaultValue
            End With
            tdfOptions.Fields.Append fldTemp
        End If
    Next fld
    
    Set CreatePrivateDBPrefTable = g_App.PrivateDB.OpenRecordset(xSQL, _
                                        dbOpenSnapshot, dbReadOnly)

End Function

'************************************************************************
'  Internal Procedures
'************************************************************************
Private Sub Class_Initialize()
    Set m_PersonOption = New mpDB.CPersonOption
    m_PersonOption.Parent = Me
End Sub

Private Sub Class_Terminate()
    Set m_PersonOption = Nothing
    Set m_colPersonOptions = Nothing
    Set m_colPersonOptionsForm = Nothing
End Sub

