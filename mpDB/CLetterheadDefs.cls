VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLetterheadDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Letterhead Defs Collection Class
'   created 12/22/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Letterhead types - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_ID = 0
    xarCol_Description = 1
    xarCol_BoilerplateFile = 2
    xarCol_HeaderBookmark = 3
    xarCol_HeaderBookmarkPrimary = 4
    xarCol_FooterBookmark = 5
    xarCol_FooterBookmarkPrimary = 6
    xarCol_TopMargin = 7
    xarCol_BottomMargin = 8
    xarCol_LeftMargin = 9
    xarCol_RightMargin = 10
    xarCol_HeaderDistance = 11
    xarCol_FooterDistance = 12
    xarCol_HeaderSpacerHeight = 13
    xarCol_TemplateToAttachTo = 14
    xarCol_AllowAuthorEmail = 15
    xarCol_AllowAuthorPhone = 16
    xarCol_AllowAuthorName = 17
    xarCol_AllowAuthorTitle = 18
    xarCol_AllowAuthorFax = 19
    xarCol_AddressFormat = 20
    xarCol_OfficeID = 21
    xarCol_AllowAuthorAdmittedIn = 22

End Enum

Private m_oRS As DAO.Recordset
Private m_oDef As mpDB.CLetterheadDef
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Private Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
        'Refresh lTemplateID, True
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional xTemplateID As String, Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source- returns
'all letterhead types if no template id is supplied
    Dim xSQL As String

    If Len(xTemplateID) Then
'       get all types for specified template-
'******************************************************************************
'multitype:
'       count number first
        Dim iNumTypes As Integer
        Dim xTemp As String
        
        xSQL = "SELECT COUNT(fldTemplateID) FROM tblLetterheadAssignments " & _
               "WHERE fldTemplateID='" & xTemplateID & "'"
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                    dbOpenForwardOnly, dbReadOnly)
        iNumTypes = m_oRS.Fields(0)
        m_oRS.Close
        
        If iNumTypes Then
'           types exist for this template, get them
            xTemp = xTemplateID
        Else
'           types don't exist for this template, get them from base
            xTemp = Application.TemplateDefinitions(xTemplateID).BaseTemplate
        End If
        
        '9.7.1 - #4202
        xSQL = "SELECT tblLetterheadTypes.*, tblLetterheadCustomTextAssignments.* " & _
               "FROM tblLetterheadTypes INNER JOIN (tblLetterheadCustomTextAssignments " & _
               "RIGHT JOIN tblLetterheadAssignments ON tblLetterheadCustomTextAssignments.fldLetterheadID = tblLetterheadAssignments.fldLetterheadID) " & _
               "ON tblLetterheadTypes.fldID = tblLetterheadAssignments." & _
               "fldLetterheadID " & _
               "WHERE tblLetterheadAssignments.fldTemplateID='" & xTemp & "' " & _
               "ORDER BY tblLetterheadTypes.fldDescription"

'        xSQL = "SELECT tblLetterheadTypes.* " & _
'               "FROM tblLetterheadTypes INNER JOIN tblLetterheadAssignments " & _
'                    "ON tblLetterheadTypes.fldID = tblLetterheadAssignments." & _
'                    "fldLetterheadID " & _
'               "WHERE tblLetterheadAssignments.fldTemplateID='" & xTemp & "' " & _
'               "ORDER BY tblLetterheadTypes.fldDescription"
        
    Else
        '9.7.1 - #4202
        xSQL = "SELECT tblLetterheadTypes.*, tblLetterheadCustomTextAssignments.* " & _
               "FROM tblLetterheadTypes LEFT JOIN tblLetterheadCustomTextAssignments ON " & _
               "tblLetterheadTypes.fldID = tblLetterheadCustomTextAssignments.fldLetterheadID " & _
               "ORDER BY tblLetterheadTypes.fldDescription"
'       get all types
'        xSQL = "SELECT * FROM tblLetterheadTypes " & _
'               "ORDER BY tblLetterheadTypes.fldDescription"
    End If
   
'******************************************************************************
''       get all types for specified template
'        xSQL = "SELECT tblLetterheadTypes.* " & _
'               "FROM tblLetterheadTypes INNER JOIN tblLetterheadAssignments " & _
'                    "ON tblLetterheadTypes.fldID = tblLetterheadAssignments." & _
'                    "fldLetterheadID " & _
'               "WHERE tblLetterheadAssignments.fldTemplateID=" & _
'                    "'" & xTemplateID & "' " & _
'               "ORDER BY tblLetterheadTypes.fldDescription"
'    Else
''       get all types
'        xSQL = "SELECT * FROM tblLetterheadTypes " & _
'               "ORDER BY tblLetterheadTypes.fldDescription"
'    End If
   
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Active() As mpDB.CLetterheadDef
'returns the office object of the current record in the recordset
    Set Active = m_oDef
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(lID As Long) As mpDB.CLetterheadDef
Attribute Item.VB_UserMemId = 0
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    On Error GoTo ProcError
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CLetterheadDefs.Item"
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDef = New mpDB.CLetterheadDef
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_oDef = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject()
    With m_oRS
        On Error Resume Next
        m_oDef.ID = Empty
        m_oDef.Description = Empty
        m_oDef.BoilerplateFile = Empty
        m_oDef.UsesFirstPageHeader = Empty
        m_oDef.UsesPrimaryHeader = Empty
        m_oDef.UsesFirstPageFooter = Empty
        m_oDef.UsesPrimaryFooter = Empty
        m_oDef.TopMargin = Empty
        m_oDef.BottomMargin = Empty
        m_oDef.LeftMargin = Empty
        m_oDef.RightMargin = Empty
        m_oDef.HeaderDistance = Empty
        m_oDef.FooterDistance = Empty
        m_oDef.HeaderSpacerHeight = Empty
        m_oDef.PageHeight = Empty
        m_oDef.PageWidth = Empty
        m_oDef.AllowAuthorEMail = Empty
        m_oDef.AllowAuthorPhone = Empty
        m_oDef.AllowAuthorName = Empty
        m_oDef.AllowAuthorTitle = Empty
        m_oDef.AllowAuthorAdmittedIn = Empty
        m_oDef.AllowAuthorFax = Empty
        m_oDef.AddressFormat = Empty
        '9.7.1 - #4202
        m_oDef.CustomText1 = Empty
        m_oDef.CustomText2 = Empty
        m_oDef.CustomText3 = Empty
        m_oDef.OfficeID = 0
        '9.7.1 #4028
        m_oDef.AllowCustomDetail1 = Empty
        m_oDef.AllowCustomDetail2 = Empty
        m_oDef.AllowCustomDetail3 = Empty
        '---9.7.1 - 4035
        m_oDef.FormatFirmName = Empty
        
        m_oDef.ID = !fldID
        m_oDef.Description = !fldDescription
        m_oDef.BoilerplateFile = !fldBoilerplateFile
        '9.7.1 - #4202
        m_oDef.CustomText1 = !fldCustomText1
        m_oDef.CustomText2 = !fldCustomText2
        m_oDef.CustomText3 = !fldCustomText3
        m_oDef.UsesFirstPageHeader = !fldUsesFirstPageHeader
        m_oDef.UsesPrimaryHeader = !fldUsesPrimaryHeader
        m_oDef.UsesFirstPageFooter = !fldUsesFirstPageFooter
        m_oDef.UsesPrimaryFooter = !fldUsesPrimaryFooter
        m_oDef.TopMargin = !fldTopMargin
        m_oDef.BottomMargin = !fldBottomMargin
        m_oDef.LeftMargin = !fldLeftMargin
        m_oDef.RightMargin = !fldRightMargin
        m_oDef.HeaderDistance = !fldHeaderDistance
        m_oDef.FooterDistance = !fldFooterDistance
        m_oDef.HeaderSpacerHeight = !fldHeaderSpacerHeight
        m_oDef.PageHeight = !fldPageHeight
        m_oDef.PageWidth = !fldPageWidth
        m_oDef.AllowAuthorEMail = !fldAllowAuthorEmail
        m_oDef.AllowAuthorPhone = !fldAllowAuthorPhone
        m_oDef.AllowAuthorName = !fldAllowAuthorName
        m_oDef.AllowAuthorTitle = !fldAllowAuthorTitle
        m_oDef.AllowAuthorFax = !fldAllowAuthorFax
        m_oDef.AllowAuthorAdmittedIn = !fldAllowAuthorAdmittedIn
        m_oDef.AddressFormat = !fldAddressFormat
        m_oDef.OfficeID = !fldOfficeID
        '9.7.1 #4028
        m_oDef.AllowCustomDetail1 = !fldAllowCustom1
        m_oDef.AllowCustomDetail2 = !fldAllowCustom2
        m_oDef.AllowCustomDetail3 = !fldAllowCustom3
        '---9.7.1 - 4035
        m_oDef.FormatFirmName = !fldFormatFirmName
    End With
End Function

Public Sub AddAssignment(ByVal lLetterheadID As Long, ByVal xTemplateID As String)
    Dim xSQL As String
    
    On Error GoTo ProcError
    xSQL = "INSERT INTO tblLetterheadAssignments(fldTemplateID, fldLetterheadID) VALUES ('" & _
            xTemplateID & "'," & lLetterheadID & ")"
            
    Application.PublicDB.Execute xSQL
    Exit Sub
ProcError:
    RaiseError "mpDB.CLetterheadDefs.AddAssignment", Err.Description
    Exit Sub
End Sub

Public Sub DeleteAssignment(ByVal lLetterheadID As Long, ByVal xTemplateID As String)
    Dim xSQL As String
    
    On Error GoTo ProcError
    xSQL = "DELETE FROM tblLetterheadAssignments WHERE fldTemplateID= '" & xTemplateID & _
           "' AND fldLetterheadID=" & lLetterheadID
            
    Application.PublicDB.Execute xSQL
    
    If Application.PublicDB.RecordsAffected > 0 Then
        Refresh
    End If
    
    Exit Sub
ProcError:
    RaiseError "mpDB.CLetterheadDefs.DeleteAssignment", Err.Description
    Exit Sub
End Sub

Public Sub DeleteAssignments(ByVal xTemplateID As String)
    Dim xSQL As String
    
    On Error GoTo ProcError
    xSQL = "DELETE FROM tblLetterheadAssignments WHERE fldTemplateID= '" & xTemplateID & "'"
            
    Application.PublicDB.Execute xSQL
    
    If Application.PublicDB.RecordsAffected > 0 Then
        Refresh
    End If
    
    Exit Sub
ProcError:
    RaiseError "mpDB.CLetterheadDefs.DeleteAssignments", Err.Description
    Exit Sub
End Sub




