VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
  Option Explicit

'**********************************************************
'   CCustomControl Class
'   created 12/28/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods of a
'   MacPac Template Custom Control - the
'   control associated with a Custom Template
'   property

'   Member of App
'   Container for
'**********************************************************
Public Enum mpCustomControl
    mpCustomControl_TextBox = 1
    mpCustomControl_ListBox = 2
    mpCustomControl_ComboBox = 3
    mpCustomControl_MultiLineCombo = 4
    mpCustomControl_CheckBox = 5
    mpCustomControl_MultilineTextBox = 6
    mpCustomControl_Calendar = 7
    mpCustomControl_OrdinalDateCombo = 8
    mpCustomControl_ClientMatter = 9 '*c
End Enum

Public Enum mpClientMatterData '*c
    mpClientMatterData_None = 0 '*c
    mpClientMatterData_ClientNumber = 1 '*c
    mpClientMatterData_ClientName = 2 '*c
    mpClientMatterData_MatterNumber = 3 '*c
    mpClientMatterData_MatterName = 4 '*c
    mpClientMatterData_Other = 5 '*c
End Enum '*c

Public Enum mpCustomTabs
    mpCustomTab_1 = 0
    mpCustomTab_2 = 1
    mpCustomTab_3 = 2
End Enum

Private m_xCaption As String
Private m_xName As String
Private m_iControlType As mpCustomControl
Private m_iTab As mpCustomTabs
Private m_iIndex As Integer
Private m_oList As CList
Private m_iLines As Integer
Private m_iSpaceBefore As Integer
Private m_xFormat As String
Private m_lCIList As Long
Private m_bIsSticky As Boolean
Private m_lID As Long
Private m_bIsNew As Boolean
Private m_iCMLink As Integer '*c
Private m_iCMRelatedIndex As Integer '*c

'**********************************************************
'   Properties
'**********************************************************
Friend Property Let IsNew(bNew As Boolean)
    m_bIsNew = bNew
End Property

Friend Property Get IsNew() As Boolean
    IsNew = m_bIsNew
End Property

Public Property Let Caption(xNew As String)
    m_xCaption = xNew
End Property

Public Property Get Caption() As String
    Caption = m_xCaption
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Format(xNew As String)
    m_xFormat = xNew
End Property

Public Property Get Format() As String
    Format = m_xFormat
End Property

Public Property Get ControlType() As mpCustomControl
    ControlType = m_iControlType
End Property

Public Property Let ControlType(iNew As mpCustomControl)
    m_iControlType = iNew
End Property

Public Property Get TabID() As mpCustomTabs
    TabID = m_iTab
End Property

Public Property Let TabID(iNew As mpCustomTabs)
    m_iTab = iNew
End Property

Public Property Get Index() As Integer
    Index = m_iIndex
End Property

Public Property Let Index(iNew As Integer)
    m_iIndex = iNew
End Property

Public Property Set List(oNew As mpDB.CList)
    Set m_oList = oNew
End Property

Public Property Get List() As mpDB.CList
    Set List = m_oList
End Property

Public Property Let Lines(iNew As Integer)
    m_iLines = iNew
End Property

Public Property Get Lines() As Integer
    Lines = m_iLines
End Property

Public Property Get SpaceBefore() As Integer
    SpaceBefore = m_iSpaceBefore
End Property

Public Property Let SpaceBefore(iNew As Integer)
    m_iSpaceBefore = iNew
End Property

Public Property Get CIList() As Long
    CIList = m_lCIList
End Property

Public Property Let CIList(lNew As Long)
    m_lCIList = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get IsSticky() As Boolean
    IsSticky = m_bIsSticky
End Property

Public Property Let IsSticky(bNew As Boolean)
    m_bIsSticky = bNew
End Property

Public Property Let CMLink(iNew As Integer) '*c
    m_iCMLink = iNew
End Property

Public Property Get CMLink() As Integer '*c
    CMLink = m_iCMLink
End Property

Public Property Let CMRelatedIndex(iNew As Integer) '*c
    m_iCMRelatedIndex = iNew
End Property

Public Property Get CMRelatedIndex() As Integer '*c
    CMRelatedIndex = m_iCMRelatedIndex
End Property

'**********************************************************
'   Methods
'**********************************************************
'**********************************************************
'   Class Events
'**********************************************************
