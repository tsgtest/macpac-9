VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CCustomControls Collection Class
'   created 12/10/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CCustomControls

'   Member of
'   Container for CCustomControl
'**********************************************************

'**********************************************************
Private m_oRS As DAO.Recordset
Private m_oCCtl As mpDB.CCustomControl
Private m_xCategory As String
'**********************************************************


'**********************************************************
'   Properties
'**********************************************************
Public Property Let Category(xNew As String)
    Dim xDesc As String
    
    On Error GoTo ProcError
    
'    If Application.TemplateDefinitions(xNew) Is Nothing Then
'        Err.Raise mpError_InvalidTemplateName, , "No template with name '" & xNew & "' was found."
'    End If
    m_xCategory = xNew
    
'   get recordset containing custom
'   properties for supplied template
    Refresh xNew
    Exit Property
ProcError:
    RaiseError "mpDB.CCustomControls.Category", Err.Description
    Exit Property
End Property

Public Property Get Category() As String
    Category = m_xCategory
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Add(ByVal lPropertyID As Long) As CCustomControl
    
    On Error GoTo ProcError
    Set m_oCCtl = New CCustomControl
    With m_oCCtl
        .ID = lPropertyID
        .IsNew = True
    End With
    Set Add = m_oCCtl
    Exit Function
ProcError:
    RaiseError "mpDB.CCustomControls.Add", Err.Description
    Exit Function
End Function

Public Function Exists(ByVal lPropertyID As Long) As Boolean
'returns TRUE if the specified custom property already exists
    Dim xSQL As String
    
    xSQL = "SELECT fldPropertyID FROM tblCustomControls WHERE " & _
           "fldPropertyID=" & lPropertyID
            
    Exists = Application.PublicDB.OpenRecordset(xSQL).RecordCount > 0
    
    Exit Function
ProcError:
    RaiseError "mpDB.CCustomControlDefs.Exists", Err.Description
    Exit Function
End Function

Public Sub Save(oCustCtl As CCustomControl)
    Dim xSQL As String
    
    On Error GoTo ProcError
    If oCustCtl Is Nothing Then
        Err.Raise mpError.mpError_InvalidParameter, , _
            "Could not save custom dialog definition.  " & _
            "The supplied variable is 'Nothing'."
    End If
    
    With oCustCtl
'       validate
        If .Caption = Empty Or .ControlType = Empty Or .Index = Empty Then
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save the control definition.  Some required data is missing."
        ElseIf .IsNew And Me.Exists(.ID) Then
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save the control definition.  A control for a custom property with ID=" & _
                    .ID & " already exists."
        End If
        
        'get list name
        Dim oList As mpDB.CList
        Dim xList As String
        
        On Error Resume Next
        Set oList = .List
        On Error GoTo ProcError
        
        If Not oList Is Nothing Then
            xList = oList.Name
        End If
        
        If .IsNew Then
'           insert
            xSQL = "INSERT INTO tblCustomControls(fldPropertyID,fldType,fldCaption," & _
                "fldTab,fldIndex," & IIf(xList = Empty, "", "fldList,") & _
                "fldLines,fldSpaceBefore," & IIf(.Format = Empty, "", "fldFormat,") & _
                "fldCIList,fldCMLink, fldCMRelatedIndex, fldIsSticky) VALUES (" & _
                .ID & "," & _
                .ControlType & ",""" & _
                .Caption & """," & _
                .TabID & "," & _
                .Index & "," & _
                IIf(xList = Empty, "", """" & xList & """,") & _
                .Lines & "," & _
                .SpaceBefore & "," & _
                IIf(.Format = Empty, "", """" & .Format & """,") & _
                .CIList & "," & _
                .CMLink & "," & _
                .CMRelatedIndex & "," & _
                .IsSticky & ")" '*c
        Else
'           save
            xSQL = "UPDATE tblCustomControls SET fldType=" & _
                .ControlType & _
                ",fldCaption=""" & .Caption & _
                """,fldTab=" & .TabID & _
                ",fldIndex=" & .Index & _
                IIf(xList = Empty, "", ",fldList=""" & xList & """") & _
                ",fldLines=" & .Lines & _
                ",fldSpaceBefore=" & .SpaceBefore & _
                IIf(.Format = Empty, "", ",fldFormat=""" & .Format & """") & _
                ",fldCIList=" & .CIList & _
                ",fldCMLink=" & .CMLink & _
                ",fldCMRelatedIndex=" & .CMRelatedIndex & _
                ",fldIsSticky=" & .IsSticky & " WHERE fldPropertyID=" & oCustCtl.ID '*c
        End If
    End With
    
    With Application.PublicDB
        'execute query
        .Execute xSQL
        If .RecordsAffected = 0 Then
            'add, update failed - raise error
            Err.Raise 600, , "Could not save the custom custom control."
        Else
            oCustCtl.IsNew = False
            Refresh Me.Category
        End If
    End With
    
   Exit Sub
ProcError:
    RaiseError "mpDB.CCustomControls.Save", Err.Description
    Exit Sub
End Sub

Public Sub Delete(ByVal lID As Long)
    Dim xSQL As String
    On Error GoTo ProcError
    
    If Not Me.Exists(lID) Then
        Exit Sub
    End If
    
    xSQL = "DELETE FROM tblCustomControls WHERE fldPropertyID =" & lID
    With Application.PublicDB
        'execute query
        .Execute xSQL
        If .RecordsAffected = 0 Then
            'add, update failed - raise error
            Err.Raise 600, , "Could not delete the custom custom control."
        Else
            Refresh Me.Category
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomControls.Delete", Err.Description
    Exit Sub
End Sub

Public Sub DeleteAll()
    Dim xSQL As String
    On Error GoTo ProcError

    xSQL = "DELETE FROM tblCustomControls WHERE fldPropertyID IN (" & _
        "SELECT fldID FROM tblCustomProperties WHERE fldCategory='" & Me.Category & "')"
        
    With Application.PublicDB
        'execute query
        .Execute xSQL
        If .RecordsAffected > 0 Then
            Refresh Me.Category
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomControls.Delete", Err.Description
    Exit Sub
End Sub

Private Sub Refresh(Optional ByVal xCategory As String)
'get recordset of property defs from db
    Dim xSQL As String
    
'   if category is not supplied, check property
    If Len(xCategory) = 0 Then
        xCategory = Me.Category
    End If
    
'   if still no category, get all custom controls,
'   else get custom controls in specified category
    If Len(xCategory) Then
        xSQL = "SELECT tblCustomControls.*, tblCustomProperties.fldName " & _
               "FROM tblCustomControls INNER JOIN tblCustomProperties ON " & _
               "     tblCustomControls.fldPropertyID = tblCustomProperties.fldID " & _
               "WHERE tblCustomProperties.fldCategory='" & xCategory & "' " & _
               "ORDER BY tblCustomControls.fldTab, tblCustomControls.fldIndex"
    Else
        xSQL = "SELECT tblCustomControls.*, tblCustomProperties.fldName " & _
               "FROM tblCustomControls INNER JOIN tblCustomProperties ON " & _
               "     tblCustomControls.fldPropertyID = tblCustomProperties.fldID " & _
               "ORDER BY tblCustomControls.fldTab, tblCustomControls.fldIndex"
    End If

'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
'   populate if possible
    If Not (m_oRS.BOF And m_oRS.EOF) Then
        m_oRS.MoveLast
        m_oRS.MoveFirst
    End If
End Sub

Public Property Get Count() As Integer
    If Not (m_oRS Is Nothing) Then
        Count = m_oRS.RecordCount
    End If
End Property

Public Function Item(vNameOrIndex As Variant) As mpDB.CCustomControl
'finds the record with ID = vID or Name = vID -
'returns the found record as a CCustomControl-
'raises error if no match - returns "Nothing" if
'no recordset exists

    Dim xCriteria As String
    Dim vBkmk As Variant
    
    On Error GoTo ProcError
    
    If m_oRS Is Nothing Then
        Exit Function
    End If
       
    With m_oRS
'       set criteria
        If IsNumeric(vNameOrIndex) Then
            On Error Resume Next
            .MoveFirst
            .AbsolutePosition = vNameOrIndex - 1
            If Err.Number Then
                On Error GoTo ProcError
'               not found
                Err.Raise mpError_InvalidMember, , _
                    "Could not find custom control with index = " & vNameOrIndex
            End If
        Else
            xCriteria = "tblCustomProperties.fldName = '" & vNameOrIndex & "'"

'           find
            .FindFirst xCriteria
            
            If .NoMatch Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            
        End If
        
'       requested item exists - update current object
        UpdateObject
                
'       return current object
        m_oCCtl.IsNew = False
        Set Item = m_oCCtl
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CCustomControls.Item", Err.Description
    Exit Function
End Function

Public Function ItemFromID(ByVal lID As Long) As mpDB.CCustomControl
'finds the record with ID = lID -
'returns "Nothing" it doesn't exist

    Dim xCriteria As String
    
    On Error GoTo ProcError
    
    If m_oRS Is Nothing Then
        Exit Function
    End If
       
    With m_oRS
'       set criteria
        xCriteria = "tblCustomControls.fldPropertyID = " & lID

'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            Err.Raise mpError_InvalidMember
        Else
'           item exists - update current object
            UpdateObject
                
'           return current object
            m_oCCtl.IsNew = False
            Set ItemFromID = m_oCCtl
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CCustomControls.Item", Err.Description
    Exit Function
End Function

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oCCtl = New mpDB.CCustomControl
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
    Set m_oCCtl = Nothing
End Sub

Private Sub UpdateObject()
    With m_oCCtl
        .ID = Empty
        .Caption = Empty
        .ControlType = Empty
        .Index = Empty
        Set .List = Nothing
        .Name = Empty
        .TabID = Empty
        .Format = Empty
        .CIList = Empty
        .CMLink = Empty
        .CMRelatedIndex = Empty
        .IsSticky = Empty
        
        On Error Resume Next
        .ID = m_oRS!fldPropertyID
        .Caption = m_oRS!fldCaption
        .ControlType = m_oRS!fldType
        .Index = m_oRS!fldIndex
        If Not IsNull(m_oRS!fldList) Then
            Set .List = Application.Lists(m_oRS!fldList)
        End If
        .Name = m_oRS!fldName
        .TabID = m_oRS!fldTab
        .Lines = m_oRS!fldLines
        .SpaceBefore = m_oRS!fldSpaceBefore
        .Format = m_oRS!fldFormat
        .CIList = m_oRS!fldCIList
        .CMLink = m_oRS!fldCMLink
        .CMRelatedIndex = m_oRS!fldCMRelatedIndex
        .IsSticky = m_oRS!fldIsSticky
    End With
End Sub
