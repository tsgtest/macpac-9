VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLicense"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CLicense Class
'   created 2/23/00 by Dan Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define an Attorney License

'   Member of CPerson
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xNum As String
Private m_xDesc As String
Private m_bDefault As Boolean

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Number(xNew As String)
    m_xNum = xNew
End Property

Public Property Get Number() As String
    Number = m_xNum
End Property

Public Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Public Property Get Default() As Boolean
    Default = m_bDefault
End Property

Public Property Let Default(bNew As Boolean)
    m_bDefault = bNew
End Property
