VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentStampDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Document Stamp Definition Class
'   created 2/2/2000 by Jeffrey Sweetland

'   Contains properties and methods that
'   define a document stamp

'   Member of CDocumentStampDefs
'**********************************************************

'**********************************************************
Public Enum mpDocStampLocations
    mpDocStampLocation_None = 0
    mpDocStampLocation_Header = 1
    mpDocStampLocation_Footer = 2
    mpDocStampLocation_Main = 3
    mpDocStampLocation_Cursor = 4
    mpDocStampLocation_EndOfDocument = 5
    mpDocStampLocation_StartOfSection = 6
End Enum

Public Enum mpDocStampHPositions
    mpDocStampHPosition_NotSpecified = 0
    mpDocStampHPosition_LMargin = 1
    mpDocStampHPosition_RMargin = 2
    mpDocStampHPosition_Selection = 3
End Enum

Public Enum mpDocStampVPositions
    mpDocStampVPosition_NotSpecified = 0
    mpDocStampVPosition_TMargin = 1
    mpDocStampVPosition_BMargin = 2
    mpDocStampVPosition_HeaderDistance = 3
    mpDocStampVPosition_FooterDistance = 4
    mpDocStampVPosition_FirstParagraph = 5
    mpDocStampVPosition_Page = 6
End Enum

Public Enum mpDocStampWidths
    mpDocStampWidth_NotSpecified = 0
    mpDocStampWidth_SpanMargins = 1
End Enum

Public Enum mpDocStampHeights
    mpDocStampHeight_NotSpecified = 2
    mpDocStampHeight_Standard = 1
End Enum

Public Enum mpDocStampVRoots
    mpDocStampVRoot_Top = 1
    mpDocStampVRoot_Bottom = 2
End Enum

Private m_lID As Long
Private m_xDescription As String
Private m_xBPFile As String
Private m_iCategory As Integer
Private m_iLocation As mpDocStampLocations
Private m_bInsertFirstPage As Boolean
Private m_bInsertPrimary As Boolean
Private m_bIdentical As Boolean
Private m_xDateFormat As String
Private m_xTimeFormat As String
Private m_bDeleteExisting As Boolean

Private m_iPrimaryHPos As mpDocStampHPositions
Private m_iPrimaryVPos As mpDocStampVPositions
Private m_iPrimaryVRoot As mpDocStampVRoots
Private m_sPrimaryHOff As Single
Private m_sPrimaryVOff As Single
Private m_xPrimaryTextTemplate As String
Private m_xPrimaryAltTextTemplate As String
Private m_iPrimaryWidth As mpDocStampWidths
Private m_iHeight As mpDocStampHeights
Private m_xOptionalControls As String

Private m_iPage1HPos As mpDocStampHPositions
Private m_iPage1VPos As mpDocStampVPositions
Private m_iPage1VRoot As mpDocStampVRoots
Private m_sPage1HOff As Single
Private m_sPage1VOff As Single
Private m_xPage1TextTemplate As String
Private m_xPage1AltTextTemplate As String
Private m_iPage1Width As mpDocStampWidths
Private m_bSameAsPrimary As Boolean
Private m_bAllowVariableText As Boolean

Private m_bPage1AnchorSpace As Boolean
Private m_bPrimaryAnchorSpace As Boolean
Private m_sPage1AdditionalSpace As Single
Private m_sPrimaryAdditionalSpace As Single


'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property
Public Property Get ID() As Long
    ID = m_lID
End Property
Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property
Public Property Get Description() As String
    Description = m_xDescription
End Property
Public Property Let ForceFirstPageAnchorSpace(bNew As Boolean)
    m_bPage1AnchorSpace = bNew
End Property
Public Property Get ForceFirstPageAnchorSpace() As Boolean
    ForceFirstPageAnchorSpace = m_bPage1AnchorSpace
End Property
Public Property Let ForcePrimaryPageAnchorSpace(bNew As Boolean)
    m_bPrimaryAnchorSpace = bNew
End Property
Public Property Get ForcePrimaryPageAnchorSpace() As Boolean
    ForcePrimaryPageAnchorSpace = m_bPrimaryAnchorSpace
End Property
Public Property Let AdditionalFirstPageSpace(sNew As Single)
    m_sPage1AdditionalSpace = sNew
End Property
Public Property Get AdditionalFirstPageSpace() As Single
    AdditionalFirstPageSpace = m_sPage1AdditionalSpace
End Property
Public Property Let AdditionalPrimaryPageSpace(sNew As Single)
    m_sPrimaryAdditionalSpace = sNew
End Property
Public Property Get AdditionalPrimaryPageSpace() As Single
    AdditionalPrimaryPageSpace = m_sPrimaryAdditionalSpace
End Property
Public Property Let DateFormat(xNew As String)
    m_xDateFormat = xNew
End Property
Public Property Get DateFormat() As String
    DateFormat = m_xDateFormat
End Property
Public Property Let TimeFormat(xNew As String)
    m_xTimeFormat = xNew
End Property
Public Property Get TimeFormat() As String
    TimeFormat = m_xTimeFormat
End Property
Public Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property
Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property
Public Property Let Category(iNew As Integer)
    m_iCategory = iNew
End Property
Public Property Get Category() As Integer
    Category = m_iCategory
End Property
Public Property Let Location(iNew As mpDocStampLocations)
    m_iLocation = iNew
End Property
Public Property Get Location() As mpDocStampLocations
    Location = m_iLocation
End Property
Public Property Let DeleteExisting(bNew As Boolean)
    m_bDeleteExisting = bNew
End Property
Public Property Get DeleteExisting() As Boolean
    DeleteExisting = m_bDeleteExisting
End Property
Public Property Let InsertPrimary(bNew As Boolean)
    m_bInsertPrimary = bNew
End Property
Public Property Get InsertPrimary() As Boolean
    InsertPrimary = m_bInsertPrimary
End Property
Public Property Let InsertFirstPage(bNew As Boolean)
    m_bInsertFirstPage = bNew
End Property
Public Property Get InsertFirstPage() As Boolean
    InsertFirstPage = m_bInsertFirstPage
End Property
Public Property Let StampsAreIdentical(bNew As Boolean)
    m_bIdentical = bNew
End Property
Public Property Get StampsAreIdentical() As Boolean
    StampsAreIdentical = m_bIdentical
End Property
Public Property Let PrimaryWidth(iNew As mpDocStampWidths)
    m_iPrimaryWidth = iNew
End Property
Public Property Get PrimaryWidth() As mpDocStampWidths
    PrimaryWidth = m_iPrimaryWidth
End Property
Public Property Let Height(iNew As mpDocStampHeights)
    m_iHeight = iNew
End Property
Public Property Get Height() As mpDocStampHeights
    Height = m_iHeight
End Property
Public Property Let PrimaryHorizontalPosition(iNew As mpDocStampHPositions)
    m_iPrimaryHPos = iNew
End Property
Public Property Get PrimaryHorizontalPosition() As mpDocStampHPositions
    PrimaryHorizontalPosition = m_iPrimaryHPos
End Property
Public Property Let PrimaryVerticalRoot(iNew As mpDocStampVRoots)
    m_iPrimaryVRoot = iNew
End Property
Public Property Get PrimaryVerticalRoot() As mpDocStampVRoots
    PrimaryVerticalRoot = m_iPrimaryVRoot
End Property
Public Property Let PrimaryVerticalPosition(iNew As mpDocStampVPositions)
    m_iPrimaryVPos = iNew
End Property
Public Property Get PrimaryVerticalPosition() As mpDocStampVPositions
    PrimaryVerticalPosition = m_iPrimaryVPos
End Property
Public Property Let PrimaryHorizontalOffset(sNew As Single)
    m_sPrimaryHOff = sNew
End Property
Public Property Get PrimaryHorizontalOffset() As Single
    PrimaryHorizontalOffset = m_sPrimaryHOff
End Property
Public Property Let PrimaryVerticalOffset(sNew As Single)
    m_sPrimaryVOff = sNew
End Property
Public Property Get PrimaryVerticalOffset() As Single
    PrimaryVerticalOffset = m_sPrimaryVOff
End Property
Public Property Let PrimaryTextTemplate(xNew As String)
    m_xPrimaryTextTemplate = xNew
End Property
Public Property Get PrimaryTextTemplate() As String
    PrimaryTextTemplate = m_xPrimaryTextTemplate
End Property
Public Property Let OptionalControls(xNew As String)
    m_xOptionalControls = xNew
End Property
Public Property Get OptionalControls() As String
    OptionalControls = m_xOptionalControls
End Property
Public Property Let Page1Width(iNew As mpDocStampWidths)
    m_iPage1Width = iNew
End Property
Public Property Get Page1Width() As mpDocStampWidths
    Page1Width = m_iPage1Width
End Property
Public Property Let Page1HorizontalPosition(iNew As mpDocStampHPositions)
    m_iPage1HPos = iNew
End Property
Public Property Get Page1HorizontalPosition() As mpDocStampHPositions
    Page1HorizontalPosition = m_iPage1HPos
End Property
Public Property Let Page1VerticalPosition(iNew As mpDocStampVPositions)
    m_iPage1VPos = iNew
End Property
Public Property Get Page1VerticalPosition() As mpDocStampVPositions
    Page1VerticalPosition = m_iPage1VPos
End Property
Public Property Let Page1VerticalRoot(iNew As mpDocStampVRoots)
    m_iPage1VRoot = iNew
End Property
Public Property Get Page1VerticalRoot() As mpDocStampVRoots
    Page1VerticalRoot = m_iPage1VRoot
End Property
Public Property Let Page1HorizontalOffset(sNew As Single)
    m_sPage1HOff = sNew
End Property
Public Property Get Page1HorizontalOffset() As Single
    Page1HorizontalOffset = m_sPage1HOff
End Property
Public Property Let Page1VerticalOffset(sNew As Single)
    m_sPage1VOff = sNew
End Property
Public Property Get Page1VerticalOffset() As Single
    Page1VerticalOffset = m_sPage1VOff
End Property
Public Property Let Page1TextTemplate(xNew As String)
    m_xPage1TextTemplate = xNew
End Property
Public Property Get Page1TextTemplate() As String
    Page1TextTemplate = m_xPage1TextTemplate
End Property
Friend Property Let Page1SameAsPrimary(bNew As Boolean)
    m_bSameAsPrimary = bNew
End Property
Public Property Get Page1SameAsPrimary() As Boolean
    Page1SameAsPrimary = m_bSameAsPrimary
End Property
Friend Property Let AllowVariableText(bNew As Boolean)
    m_bAllowVariableText = bNew
End Property
Public Property Get AllowVariableText() As Boolean
    AllowVariableText = m_bAllowVariableText
End Property
Public Property Let PrimaryAltTextTemplate(xNew As String)
    m_xPrimaryAltTextTemplate = xNew
End Property
Public Property Get PrimaryAltTextTemplate() As String
    PrimaryAltTextTemplate = m_xPrimaryAltTextTemplate
End Property
Public Property Let Page1AltTextTemplate(xNew As String)
    m_xPage1AltTextTemplate = xNew
End Property
Public Property Get Page1AltTextTemplate() As String
    Page1AltTextTemplate = m_xPage1AltTextTemplate
End Property
