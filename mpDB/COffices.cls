VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COffices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Offices Collection Class
'   created 12/23/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of Offices - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_DisplayName = 0
    xarCol_ID = 1
End Enum

Private m_oRS As DAO.Recordset
Private m_oArray As XArrayObject.XArray
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Default() As mpDB.COffice
    Dim lID As Long
    
    On Error Resume Next
    
    lID = GetUserIni("General", "Default Office")
    Set Default = Application.Offices(lID)
    If Default Is Nothing Then
        lID = GetMacPacIni("Firm", "Default Office")
        Set Default = Application.Offices(lID)
    End If
    
End Property

'Public Property Get FirmName() As String
'    Dim x As String
'    x = GetMacPacIni("Firm", "Name")
'    FirmName = x
'End Property
'
Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshListArray
    End If
    Set ListSource = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh()
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    xSQL = "SELECT  * FROM tblOffices ORDER BY fldName"
    
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                        dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until RefreshListArray
    If Not (m_oArray Is Nothing) Then
        RefreshListArray
    End If
End Sub

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.COffice
Attribute Item.VB_UserMemId = 0
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim oOffice As mpDB.COffice
    
    On Error GoTo ProcError
    
'   get offices recordset if not already gotten
    If (m_oRS Is Nothing) Then
        Me.Refresh
    End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            Set oOffice = New mpDB.COffice
            UpdateObject oOffice
            
'           return current object
            Set Item = oOffice
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.Offices.Item"
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Terminate()
    Set m_oArray = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshListArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xName As String
    Dim xID As String
    
    Set m_oArray = New XArray
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_oArray.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xName = !fldDisplayName
                xID = !fldID
                m_oArray.Value(i, 0) = xName
                m_oArray.Value(i, 1) = xID
            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 1
        End If
    End With
End Sub

Private Function UpdateObject(oOffice As mpDB.COffice)
    With m_oRS
        On Error Resume Next
        With oOffice
            .ID = Empty
            .FirmID = Empty
            .Name = Empty
            .FirmName = Empty
            .FirmNameUpperCase = Empty
            .Slogan = Empty
            .Address1 = Empty
            .Address2 = Empty
            .Address3 = Empty
            .City = Empty
            .Country = Empty
            .CountyID = Empty
            .DefaultPleading = Empty
            .DisplayName = Empty
            .DistrictID = Empty
            .Fax1 = Empty
            .Fax2 = Empty
            .Phone1 = Empty
            .Phone2 = Empty
            .Phone3 = Empty
            .PhoneCaption = Empty
            .FaxCaption = Empty
            .PostCity = Empty
            .PreCity = Empty
            .Province = Empty
            .State = Empty
            .StateAbbr = Empty
            .Zip = Empty
            .FirmURL = Empty
            .Template = Empty
            .Custom1 = Empty
            .Custom2 = Empty
            .Custom3 = Empty
            .Custom4 = Empty
            .Custom5 = Empty
            '---9.7.1 - fn
            .FirmNameFormatted = Empty
        End With
        
        oOffice.ID = !fldID
        oOffice.Name = !fldName
        oOffice.FirmName = !fldFirmName
        oOffice.FirmNameUpperCase = !fldFirmNameUpperCase
        oOffice.Slogan = !fldSlogan
        oOffice.Address1 = !fldAddress1
        oOffice.Address2 = !fldAddress2
        oOffice.Address3 = !fldAddress3
        oOffice.City = !fldCity
        oOffice.Country = !fldCountry
        oOffice.CountyID = !fldCountyID
        oOffice.DefaultPleading = !fldDefaultPleading
        oOffice.DisplayName = !fldDisplayName
        oOffice.DistrictID = !fldDistrictID
        oOffice.Fax1 = !fldFax1
        oOffice.Fax2 = !fldFax2
        oOffice.Phone1 = !fldPhone1
        oOffice.Phone2 = !fldPhone2
        oOffice.Phone3 = !fldPhone3
        oOffice.PhoneCaption = !fldPhoneCaption
        oOffice.FaxCaption = !fldFaxCaption
        oOffice.PostCity = !fldPostCity
        oOffice.PreCity = !fldPreCity
        oOffice.Province = !fldProvince
        oOffice.State = !fldState
        oOffice.StateAbbr = !fldStateAbbr
        oOffice.Zip = !fldZip
        oOffice.FirmURL = !fldFirmURL
        '---9.4.0
        oOffice.FirmID = !fldFirmID
        oOffice.Template = !fldTemplate
        oOffice.Custom1 = !fldCustom1
        oOffice.Custom2 = !fldCustom2
        oOffice.Custom3 = !fldCustom3
        oOffice.Custom4 = !fldCustom4
        oOffice.Custom5 = !fldCustom5
        '---9.7.1 - fn
        oOffice.FirmNameFormatted = !fldFirmNameFormatted
    End With
End Function
