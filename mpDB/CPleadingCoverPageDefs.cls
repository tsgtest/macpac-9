VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCoverPageDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingCoverPageDefs Collection Class
'   created 2/14/02 by Mike Conner
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CPleadingCoverPageDef

'   Member of
'   Container for CPleadingCoverPageDef
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CPleadingCoverPageDef

'Private Const mpStyleValueUndefined As Long = -777777

'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CPleadingCoverPageDef
    On Error GoTo ProcError
    
'   get item where index = vID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
        m_oRS.AbsolutePosition = lID - 1
        UpdateObject
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingCoverPageDefs.Item", _
              Application.Error.Desc(Err.Number)
End Function

Public Function ItemFromCourt(ByVal lLevel0 As Long, _
                              ByVal iLevel1 As Integer, _
                              ByVal iLevel2 As Integer, _
                              ByVal ilevel3 As Integer) As mpDB.CPleadingCoverPageDef
'returns the first court that matches the supplied court levels -
'if the supplied levels don't match, we look for a more general entry
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where levels match
    xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
            "fldLevel1 = " & iLevel1 & " AND " & _
            "fldLevel2 = " & iLevel2 & " AND " & _
            "fldLevel3 = " & ilevel3

    With m_oRS
'       attempt to find the item
        .FindFirst xCrit
        If .NoMatch Then
'           if no match found, make it less specific
            xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
                    " fldLevel1 = " & iLevel1 & " AND " & _
                    " fldLevel2 = " & iLevel2 & " AND " & _
                    " fldLevel3 Is Null"

            .FindFirst xCrit
        End If
        
        If .NoMatch Then
'           if no match found, make it less specific
            xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
                    " fldLevel1 = " & iLevel1 & " AND " & _
                    " fldLevel2 Is Null AND " & _
                    " fldLevel3 Is Null"
                    
            .FindFirst xCrit
        End If
        
        If .NoMatch Then
'           if no match found, make it less specific
            xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
                    " fldLevel1 Is Null AND " & _
                    " fldLevel2 Is Null AND " & _
                    " fldLevel3 Is Null"
            .FindFirst xCrit
        End If
        
        If .NoMatch Then
'           return nothing
            Set ItemFromCourt = Nothing
        Else
'           item found - update template object
            UpdateObject
'           return template object
            Set ItemFromCourt = m_oType
        End If
    End With
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingCoverPageDefs.ItemFromCourt", _
              Application.Error.Desc(Err.Number)
End Function

Public Function Refresh()
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get templates
    xSQL = "SELECT * FROM tblPleadingCoverPageTypes"
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplates.Refresh", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oType
        .BoilerplateFile = Empty
        .FirstNumberedPage = Empty
        .FirstPageNumber = Empty
        .UseBoilerplateFooterSetup = Empty
        .ID = Empty
        .Level0 = Empty
        .Level1 = Empty
        .Level2 = Empty
        .Level3 = Empty
        .CourtTitle = Empty
        .CourtTitle1 = Empty
        .CourtTitle2 = Empty
        .CourtTitle3 = Empty
        .DefaultPleadingPaperType = Empty
        .UnderlineDocumentTitle = Empty
        .PageHeight = Empty
        .PageWidth = Empty
        .FormatFirmName = Empty
        
        On Error Resume Next
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .FirstNumberedPage = m_oRS!fldFirstNumberedPage
        .FirstPageNumber = m_oRS!fldFirstPageNumber
        .UseBoilerplateFooterSetup = m_oRS!fldUseBoilerplateFooterSetup
        .ID = m_oRS!fldID
        .Level0 = m_oRS!fldLevel0
        .Level1 = m_oRS!fldLevel1
        .Level2 = m_oRS!fldLevel2
        .Level3 = m_oRS!fldLevel3
        .CourtTitle = m_oRS!fldCourtTitle
        .CourtTitle1 = m_oRS!fldCourtTitle1
        .CourtTitle2 = m_oRS!fldCourtTitle2
        .CourtTitle3 = m_oRS!fldCourtTitle3
        .DefaultPleadingPaperType = m_oRS!fldDefPleadingPaperType
        .UnderlineDocumentTitle = m_oRS!fldUnderlineDocumentTitle
        .PageHeight = m_oRS!fldPageHeight
        .PageWidth = m_oRS!fldPageWidth
        .FormatFirmName = m_oRS!fldFormatFirmName
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oType = New mpDB.CPleadingCoverPageDef
    Refresh
End Sub


