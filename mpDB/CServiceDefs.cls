VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CServiceDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CServiceDefs Collection Class
'   created 5/16/00 by Daniel Fisherman-

'   Contains properties/methods that manage the
'   collection of Service Document definitions -
'   collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for CServiceDef
'**********************************************************


Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CServiceDef
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CServiceDef
Attribute Item.VB_UserMemId = 0
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where index = lID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       get item where name = vID
        xCrit = "fldID = " & lID
            
'       attempt to find the item
        m_oRS.FindFirst xCrit
        If m_oRS.NoMatch Then
'           if no match found, the
'           member does not exist
            Err.Raise mpError_InvalidMember
        Else
'           item found - update template object
            UpdateObject
        End If
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CServiceDefs.Item"
End Function

Public Function Refresh(Optional ByVal lLevel0 As Long)
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get templates
    xSQL = "SELECT * FROM tblServiceTypes "
    If lLevel0 Then
        xSQL = xSQL & "WHERE fldLevel0 = " & lLevel0
    End If
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CServiceDefs.Refresh"
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oType
        .BoilerplateFile = Empty
        .AllowManualInput = Empty
        .DefaultToManualInput = Empty
        .Description = Empty
        .ID = Empty
        .Level0 = Empty
        .DialogCaption = Empty
        .FooterTextMaxChars = Empty
        .UpperCaseFirmName = Empty
        .AllowGender = Empty
        .AllowCounty = Empty
        .DateFormats = Empty
        Set .AddressFormat = Nothing
        .AllowDoNoIncludeServiceTitle = Empty
        .AllowServiceTitle = Empty
        .AllowExecuteDate = Empty
        .AllowServiceList = Empty
        .AllowServiceDate = Empty
        .DefaultExecuteDate = Empty
        .DefaultServiceDate = Empty
        .DefaultPleadingPaperType = Empty
        .AllowManualAddress = Empty
        .AllowManualCompany = Empty
        .AllowManualCity = Empty
        .AllowManualState = Empty
        .AllowManualCounty = Empty
        .AllowPartyDescription = Empty
        .AllowPartyTitle = Empty
        .AllowCourtTitle = Empty
        .AllowCaseNumber = Empty
        .Custom1Label = Empty
        .Custom1DefaultText = Empty
        .Custom2Label = Empty
        .Custom2DefaultText = Empty
        .Custom3Label = Empty
        .Custom3DefaultText = Empty
        .Custom4Label = Empty
        .Custom4DefaultText = Empty
        .DefaultFooterText = Empty
        .DefaultServiceList = Empty
        .FirstNumberedPage = Empty
        .FirstPageNumber = Empty
        .UseBoilerplateFooterSetup = Empty
        .ServiceTitles = Empty
        .ESignatureBoilerplate = Empty
'CharlieCore 9.2.0 - BaseStyle
        .BaseStyle = Empty
        
        On Error Resume Next
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .AllowManualInput = m_oRS!fldAllowManualInput
        .DefaultToManualInput = m_oRS!fldDefaultToManualInput
        .Description = m_oRS!fldDescription
        .DialogCaption = m_oRS!fldDialogCaption
        .ID = m_oRS!fldID
        .Level0 = m_oRS!fldLevel0
        .FooterTextMaxChars = m_oRS!fldFooterTextMax
        .UpperCaseFirmName = m_oRS!fldUCaseFirmName
        .AllowDoNoIncludeServiceTitle = m_oRS!fldAllowDoNoIncludeServiceTitle
        .AllowServiceTitle = m_oRS!fldAllowServiceTitle
        .AllowGender = m_oRS!fldAllowGender
        .AllowCounty = m_oRS!fldAllowCounty
        .AllowExecuteDate = m_oRS!fldAllowDateExecuted
        .AllowServiceList = m_oRS!fldAllowServiceList
        .AllowServiceDate = m_oRS!fldAllowDateServed
        .AllowManualAddress = m_oRS!fldAllowManualAddress
        .AllowManualCompany = m_oRS!fldAllowManualCompany
        .AllowManualCity = m_oRS!fldAllowManualCity
        .AllowManualState = m_oRS!fldAllowManualState
        .AllowManualCounty = m_oRS!fldAllowManualCounty
        .DefaultExecuteDate = m_oRS!fldDefaultExecuteDate
        .DefaultServiceDate = m_oRS!fldDefaultServiceDate
        .DefaultPleadingPaperType = m_oRS!fldPleadingPaper
        .AllowPartyDescription = m_oRS!fldAllowPartyDescription
        .AllowPartyTitle = m_oRS!fldAllowPartyTitle
        .AllowCourtTitle = m_oRS!fldAllowCourtTitle
        .AllowCaseNumber = m_oRS!fldAllowCaseNumber
        .Custom1Label = m_oRS!fldCustom1Label
        .Custom1DefaultText = m_oRS!fldCustom1DefaultText
        .Custom2Label = m_oRS!fldCustom2Label
        .Custom2DefaultText = m_oRS!fldCustom2DefaultText
        .Custom3Label = m_oRS!fldCustom3Label
        .Custom3DefaultText = m_oRS!fldCustom3DefaultText
        .Custom4Label = m_oRS!fldCustom4Label
        .Custom4DefaultText = m_oRS!fldCustom4DefaultText
        .DefaultFooterText = m_oRS!fldDefaultFooterText
        .DefaultServiceList = m_oRS!fldDefaultServiceList
        .FirstNumberedPage = m_oRS!fldFirstNumberedPage
        .FirstPageNumber = m_oRS!fldFirstPageNumber
        .UseBoilerplateFooterSetup = m_oRS!fldUseBoilerplateFooterSetup
        .ServiceTitles = m_oRS!fldServiceTitles
        .ESignatureBoilerplate = m_oRS!fldESignatureBoilerplate
        
'CharlieCore 9.2.0 - BaseStyle
        .BaseStyle = m_oRS!fldBaseStyle
        
        .DateFormats = m_oRS!fldDateFormats
        Set .AddressFormat = Application.OfficeAddressFormats( _
            m_oRS!fldAddressFormat)
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oType = New mpDB.CServiceDef
End Sub
