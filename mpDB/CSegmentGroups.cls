VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSegmentGroups"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CSegmentGroups Collection Class
'   created 3/5/01 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CSegmentGroups - list items collection is an XArray

'   Member of CList
'   Container for CSegmentGroups
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_ID = 0
    xarCol_Name = 1
    xarCol_Parent = 2
End Enum

Private m_oArray As XArrayObject.XArray
Private m_oGroup As CSegmentGroup
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Source(xarNew As XArray)
    Set m_oArray = xarNew
End Property

Public Property Get Source() As XArray
    Set Source = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Add(ByVal vID As Variant, ByVal xName As String, Optional ByVal lParentID As Long)
    Dim lUBound As Long
    
    With m_oArray
        lUBound = m_oArray.UpperBound(1)
        .Insert 1, lUBound + 1
        .Value(lUBound + 1, xarCol_ID) = vID
        .Value(lUBound + 1, xarCol_Name) = xName
        .Value(lUBound + 1, xarCol_Parent) = lParentID
    End With
End Sub

Friend Sub Delete(lIndex As Long)
    m_oArray.Delete 1, lIndex
End Sub

Friend Sub DeleteAll()
    m_oArray.Clear
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = m_oArray.Count(1)
End Function
'9.7.1 #4294
Public Function ItemFromIndex(iID As Integer) As CSegmentGroup
    Dim i As Integer
    Set m_oGroup = New CSegmentGroup
    
    On Error GoTo ProcError
    
    m_oGroup.Name = m_oArray.Value(iID - 1, xarCol_Name)
    m_oGroup.ID = m_oArray.Value(iID - 1, xarCol_ID)
    
    If Len(m_oArray.Value(iID - 1, xarCol_Parent)) Then
        m_oGroup.ParentID = m_oArray.Value(iID - 1, xarCol_Parent)
    End If
    
    If m_oGroup.ID = Empty Then
        Err.Raise mpError_InvalidMember
        Exit Function
    End If
        
'    m_oGroup.IsNew = False
    Set ItemFromIndex = m_oGroup
    Exit Function
ProcError:
    If Err.Number = 9 Then   'subscript out of range
        Err.Number = mpError_InvalidMember
    End If
    RaiseError "mpDB.CSegmentGroups.ItemFromIndex"
    Exit Function
End Function


'9.7.1 #4422
Public Function Item(Optional vID As Variant) As CSegmentGroup
    Dim i As Integer
    Set m_oGroup = New CSegmentGroup
    
    On Error GoTo ProcError
    
    If IsNumeric(vID) Then
'       use ID
        For i = 0 To m_oArray.UpperBound(1) ' - 1
            If Val(m_oArray.Value(i, xarCol_ID)) = vID Then
                m_oGroup.Name = m_oArray.Value(i, xarCol_Name)
                m_oGroup.ID = m_oArray.Value(i, xarCol_ID)
                If Len(m_oArray.Value(i, xarCol_Parent)) Then
                    m_oGroup.ParentID = m_oArray.Value(i, xarCol_Parent)
                End If
                Exit For
            End If
        Next i
    Else
'       use name
        For i = 0 To m_oArray.UpperBound(1) ' - 1
            If m_oArray.Value(i, xarCol_Name) = vID Then
                m_oGroup.Name = m_oArray.Value(i, xarCol_Name)
                m_oGroup.ID = m_oArray.Value(i, xarCol_ID)
                If Len(m_oArray.Value(i, xarCol_Parent)) Then
                    m_oGroup.ParentID = m_oArray.Value(i, xarCol_Parent)
                End If
                Exit For
            End If
        Next i
    End If
    
    If m_oGroup.ID = Empty Then
        Err.Raise mpError_InvalidMember
        Exit Function
    End If
        
    Set Item = m_oGroup
    Exit Function
ProcError:
    If Err.Number = 9 Then   'subscript out of range
        Err.Number = mpError_InvalidMember
    End If
    RaiseError "mpDB.CSegmentGroups.Item"
    Exit Function
End Function

Sub Refresh()
'fills xarray with Segment groups
    Set m_oArray = New XArrayObject.XArray
    m_oArray.ReDim 0, -1, 0, 1
    
'   get xarray from sql statement
    Set m_oArray = Application.xarGetTableXArray( _
        Application.PublicDB, "tblSegmentGroups")
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Refresh
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
End Sub




