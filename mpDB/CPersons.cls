VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersons"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Persons Collection Class
'   created 12/18/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage the collection of Persons - collection is
'   a recordset, though the class contains an XArray
'   member for display purposes

'   Member of CApp
'   Container for CPerson
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_Favorite = 0
    xarCol_DisplayName = 1
    xarCol_ID = 2
    xarCol_BarID = 3
    xarCol_InformalName = 4
    xarCol_Source = 5
End Enum

Public Enum mpPersonTypes
    mpPersonType_All = 0
    mpPersonType_Firm = 1
    mpPersonType_Favorite = 2
    mpPersonType_Input = 4
    mpPersonType_AllAttorneys = 5
    mpPersonType_FirmAttorneys = 6
    mpPersonType_FavoriteAttorneys = 7
    mpPersonType_InputAttorneys = 9
End Enum


Public Enum mpPersonDisplayFormats
    mpPersonDisplayFormat_LFM = 0
    mpPersonDisplayFormat_FML = 1
    mpPersonDisplayFormat_LF = 2
    mpPersonDisplayFormat_FL = 3
End Enum

'Display Name formats
'Const mpSQL_LastFirstMI As String = "SELECT DISTINCTROW *, [fldLastName] & "", "" & [fldFirstName] & IIf(IsNull([fldMI]),"""","" "" & [fldMI]) AS fldDisplayName FROM qryPeoplePublic WHERE NOT IsNull([fldID])"
'Const mpSQL_FirstMILast As String = "SELECT DISTINCTROW *, [fldFirstName] & "" "" & IIf([fldMI]<>"""",[fldMI] & "" "","""") & [fldLastName] AS fldDisplayName FROM qryPeoplePublic WHERE NOT IsNull([fldID])"
'Const mpSQL_FirstLast As String = "SELECT DISTINCTROW *, [fldFirstName] & "" ""  & [fldLastName] AS fldDisplayName FROM qryPeoplePublic WHERE NOT IsNull([fldID])"
'Const mpSQL_LastFirst As String = "SELECT DISTINCTROW *, [fldLastName]  & "", "" & [fldFirstName] AS fldDisplayName FROM qryPeoplePublic WHERE NOT IsNull([fldID])"

Private m_oPersons As DAO.Recordset
Private m_oPerson As mpDB.CPerson
Private m_oArray As XArray 'Object.xArray
Private m_iType As Integer
Private m_iDisplayFormat As Integer
'**********************************************************


'************************************************************************
'  Properties
'************************************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_oArray = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oArray
End Property

Public Property Let PersonType(iNew As Integer)
    m_iType = iNew
End Property

Public Property Get PersonType() As Integer
    PersonType = m_iType
End Property

Public Property Let DisplayFormat(iNew As Integer)
    m_iDisplayFormat = iNew
End Property

Public Property Get DisplayFormat() As Integer
    DisplayFormat = m_iDisplayFormat
End Property

Public Property Get Updatable() As Boolean
    Updatable = m_oPersons.Updatable
End Property


'************************************************************************
'  Methods
'************************************************************************
Public Function Find(ByVal xLastName As String, _
                    Optional ByVal xFirstName As String, _
                    Optional ByVal xMiddleName As String) As mpDB.CPerson
'returns the first person that matches the supplied criteria
    Dim xWHERE As String
    Dim lID As Long
    
'   build criteria string based on passed args
    xWHERE = "fldLastName='" & xLastName & "'"
    If xFirstName <> Empty Then
        xWHERE = xWHERE & "AND fldFirstName='" & xFirstName & "'"
    End If
    If xMiddleName <> Empty Then
        xWHERE = xWHERE & "AND fldMI='" & xMiddleName & "'"
    End If
    
    With m_oPersons
'       perform find
        .FindFirst xWHERE
        
        If Not .NoMatch Then
'           get id
            lID = .Fields("fldID")
            
'           get person
            UpdateObject
            
'           return person
            Set Find = m_oPerson
        Else
'           return nothing
            Set Find = Nothing
        End If
    End With
End Function

Public Function Active() As mpDB.CPerson
    Set Active = m_oPerson
End Function

Public Sub Add(psnNew As mpDB.CPerson)
    Set m_oPerson = psnNew
End Sub

Public Sub Delete(lID As Long)
    Dim psnTemp As mpDB.CPerson
    Dim rs As DAO.Recordset
    Dim xSQL As String
    Dim xTable As String
    
    On Error GoTo ProcError
    
    On Error Resume Next
    Set psnTemp = Me.Item(lID)
    On Error GoTo ProcError
    
    If Not psnTemp Is Nothing Then
'       build different SQL statement based on
'       source of data
        Select Case m_oPerson.Source
            Case mpPeopleSourceList_PeopleInput
                xTable = "tblPeopleInput"
            Case Else
                xTable = "qryPeoplePublicInt"
        End Select
        
        xSQL = "DELETE * FROM " & xTable & _
               " WHERE fldID = " & psnTemp.ID
               
'       delete record
        Application.PrivateDB.Execute xSQL
            
'       delete from favorites if it exists
        RemoveAsFavorite lID
            
'       delete all possible user options
        DeleteUserOptions lID
    End If
    Exit Sub
    
ProcError:
    RaiseError "mpDB.CPersons.Delete"
    Exit Sub
End Sub

Public Function Count() As Long
    Count = m_oPersons.RecordCount
End Function

Public Sub Save()
    Dim lID As Long
    Dim rs As DAO.Recordset
    Dim xSQL As String
    
    On Error GoTo ProcError
    
'   open updatable recordset  to save - source determines which one
    Select Case m_oPerson.Source
        Case mpPeopleSourceList_PeopleInput
            If m_oPerson.ID = Empty Then
'               no ID exists - add record
                lID = 0
                xSQL = "SELECT * FROM tblPeopleInput"
                
'               manually entered person - work on People Input table
                Set rs = Application.PrivateDB.OpenRecordset(xSQL, dbOpenDynaset)
                With rs
'                   get next available id
                    If .RecordCount Then
'                       add 1 to last record to new ID
                        .MoveLast
                        lID = !fldID + 1
                        
                        If !fldID < g_lManualEntryIDThreshold Then
'                           invalid ID, raise error
                            Err.Raise mpError_InvalidManualEntryID
                        End If
                    Else
'                       no records in input table, start with first ID
                        lID = g_lManualEntryIDThreshold
                    End If
                    
                    m_oPerson.ID = lID
                    .AddNew
                End With
            Else
'               ID exists - edit record
                lID = m_oPerson.ID
                xSQL = "SELECT * FROM tblPeopleInput WHERE fldID = " & lID
                
'               manually entered person - work on People Input table
                Set rs = Application.PrivateDB _
                    .OpenRecordset(xSQL, dbOpenDynaset)
                rs.Edit
            End If
            
        Case Else
'           person exists in firm people table - work on People table
            xSQL = "SELECT * FROM qryPeoplePublicInt WHERE fldID = " & m_oPerson.ID
            Set rs = Application.PrivateDB _
                .OpenRecordset(xSQL, dbOpenDynaset)
            rs.Edit
    End Select

'   update all recordset fields
    UpdateRecordset rs
    
'   save changes
    rs.Update
    
'   save licenses collection
    With m_oPerson.Licenses
        .PersonID = m_oPerson.ID
        .Save
    End With
    
'   close recordset
    Set rs = Nothing
    Exit Sub
    
ProcError:
    RaiseError "mpDB.CPersons.Save"
    Exit Sub
End Sub


Public Function First() As mpDB.CPerson
'returns the first person in the collection
    On Error GoTo ProcError
    
    With m_oPersons
'       move current record to specified
'       absolute position if possible
        .MoveFirst
        Set m_oPerson = New mpDB.CPerson
        UpdateObject

'       return current object
        Set First = m_oPerson
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CPersons.First"
    Exit Function
End Function

Public Function NextPerson() As mpDB.CPerson
'returns the next person in the collection
    On Error GoTo ProcError
    
    With m_oPersons
'       move current record to specified
'       absolute position if possible
        .MoveNext
        Set m_oPerson = New mpDB.CPerson
        UpdateObject

'       return current object
        Set NextPerson = m_oPerson
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CPersons.NextPerson"
    Exit Function
End Function

Public Function Last() As mpDB.CPerson
'returns the last person in the collection
    On Error GoTo ProcError
    
    With m_oPersons
'       move current record to specified
'       absolute position if possible
        .MoveLast
        Set m_oPerson = New mpDB.CPerson
        UpdateObject

'       return current object
        Set Last = m_oPerson
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CPersons.Last"
    Exit Function
End Function

Public Function Item(lID As Long) As mpDB.CPerson
Attribute Item.VB_UserMemId = 0
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oPersons
'       store bookmark for possible future reset
        On Error Resume Next
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            Set Item = Nothing
        Else
'           found - update current object
            Set m_oPerson = New mpDB.CPerson
            On Error GoTo ProcError
            UpdateObject
            If Err.Number Then
                Err.Raise Err.Number
            End If
'           return current object
            Set Item = m_oPerson
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CPersons.Item"
    Exit Function
End Function

Public Function SetAsDefault(lID As Long, xTemplate As String) As Boolean
'sets person with ID = lID from Source
    Dim i As Integer
    Dim iSource As Integer
    Dim xSQL As String
    Dim rDefaultAuthors As DAO.Recordset
    Dim xControlName As String
    
    On Error GoTo bSetDefaultAuthor_Error
    
'   retrieve record from appropriate options table
    xSQL = "SELECT * FROM tblTemplateDefAuthors"
    xSQL = xSQL & " WHERE fldTemplate = " + Chr(34) + xTemplate + Chr(34)
    
    Set rDefaultAuthors = g_App.PrivateDB.OpenRecordset(xSQL)
    
'   if record exists, update record,
'   else add new record
    With rDefaultAuthors
        If .RecordCount > 0 Then
            .Edit
        Else
            .AddNew
        End If
        
        On Error Resume Next
'       source can always be determined from ID - those
'       IDs at or above the threshold are manual entry
        If lID >= g_lManualEntryIDThreshold Then
            iSource = mpPeopleSourceList_PeopleInput
        Else
            iSource = mpPeopleSourceList_People
        End If
        
        .Fields(0) = xTemplate
        .Fields(1) = lID
        .Fields(2) = iSource
         On Error GoTo bSetDefaultAuthor_Error
        .Update
    End With
    SetAsDefault = True
    Exit Function
    
bSetDefaultAuthor_Error:
    Select Case Err
        Case Else
            MsgBox Error(Err)
    End Select
    SetAsDefault = False
    Exit Function
End Function

Public Function IsInDB(dbMP As DAO.Database, lID As Long) As Boolean
'returns TRUE if record exists
'in database dbMP such that fldID = lID

    Dim xSQL As String
    Dim rID As DAO.Recordset
    
    xSQL = "SELECT fldID FROM qryPeoplePublic WHERE fldID = " & lID
    
    Set rID = dbMP.OpenRecordset(xSQL)
    IsInDB = (rID.RecordCount > 0)

End Function

Public Function IsInPeopleList(lID As Long) As Boolean
'returns TRUE if record exists
'in database dbMP such that fldID = lID

    Dim xSQL As String
    Dim rID As DAO.Recordset
    
    xSQL = "SELECT fldID FROM qryPeopleList WHERE fldID = " & lID
    
    Set rID = g_App.PrivateDB.OpenRecordset(xSQL)
    IsInPeopleList = (rID.RecordCount > 0)

End Function

Public Sub Refresh(Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xNameExp As String
    Dim xAltNameExp As String
    Dim xFromExp As String
    Dim xSQL As String
    Dim iRSType As Integer
    Dim bReadOnly As Boolean

'   set display name format
    Select Case Me.DisplayFormat
        Case mpPersonDisplayFormat_FML
            xNameExp = "[fldFirstName] & "" "" & IIf([fldMI]<>"""",[fldMI] & "" "","""") & [fldLastName]"
        Case mpPersonDisplayFormat_LFM
            xNameExp = "[fldLastName] & "", "" & [fldFirstName] & IIf(IsNull([fldMI]),"""","" "" & [fldMI])"
        Case mpPersonDisplayFormat_FL
            xNameExp = "[fldFirstName] & "" ""  & [fldLastName]"
        Case mpPersonDisplayFormat_LF
             xNameExp = "[fldLastName]  & "", "" & [fldFirstName]"
    End Select
    
    xAltNameExp = "IIf(IsNull(fldDisplayName)," & xNameExp & ",fldDisplayName) AS fldDisplay "
    xNameExp = xNameExp & " AS fldDisplay "
    
'   open on different tables/querydefs based on type of
'   people requested - some are updatable, others are not
    Select Case Me.PersonType
        Case mpPersonType_All
             xFromExp = "FROM qryPeopleList "
             iRSType = dbOpenSnapshot
            bReadOnly = True
        Case mpPersonType_Firm
            xFromExp = "FROM qryPeoplePublic"
             iRSType = dbOpenDynaset
            bReadOnly = False
        Case mpPersonType_Input
            xFromExp = "FROM qryPeopleInput"
             iRSType = dbOpenDynaset
            bReadOnly = False
        Case mpPersonType_Favorite
            xFromExp = "FROM qryPeopleList WHERE (fldFavorite = -1)"
             iRSType = dbOpenSnapshot
            bReadOnly = True
        Case mpPersonType_AllAttorneys
            xFromExp = "FROM qryPeopleList  WHERE (fldIsAttorney = TRUE)"
             iRSType = dbOpenSnapshot
            bReadOnly = True
        Case mpPersonType_FirmAttorneys
            xFromExp = "FROM qryPeoplePublic WHERE (fldIsAttorney = TRUE)"
             iRSType = dbOpenDynaset
            bReadOnly = False
        Case mpPersonType_InputAttorneys
            xFromExp = "FROM qryPeopleInput WHERE (fldIsAttorney = TRUE)"
             iRSType = dbOpenDynaset
            bReadOnly = False
        Case mpPersonType_FavoriteAttorneys
            xFromExp = "FROM qryPeopleList WHERE (fldIsAttorney = TRUE) AND (fldFavorite = -1)"
             iRSType = dbOpenSnapshot
            bReadOnly = True
    End Select
    
    xSQL = "SELECT DISTINCTROW *, " & xAltNameExp & xFromExp
    
'   open recordset
    On Error Resume Next
    With Application.PrivateDB
        If bReadOnly Then
            Set m_oPersons = .OpenRecordset(xSQL, iRSType, dbReadOnly)
        Else
            Set m_oPersons = .OpenRecordset(xSQL, iRSType)
        End If
    End With
    
    If Err.Number = 3307 Then
        On Error GoTo ProcError
        Err.Raise mpError_MismatchedPeopleFields
    ElseIf Err.Number Then
        On Error GoTo ProcError
        Err.Raise Err.Number
    End If
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.CPersons.Refresh"
End Sub

'************************************************************************
'  Internal Procedures
'************************************************************************
Private Sub Class_Initialize()
    Set m_oPerson = New mpDB.CPerson
    Set m_oArray = g_App.NewXArray()
    m_oArray.ReDim 0, -1, 0, 5
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
    Set m_oPerson = Nothing
    Set m_oPersons = Nothing
End Sub

'Private Sub RefreshXArray()
''fills array xArray with records
''in recordset rExisting
'
'    Dim lNumRows As Long
'    Dim i As Integer
'    Dim xSource As String
'    Dim xDisplayName As String
'    Dim xShortName As String
'    Dim xID As String
'    Dim xBar_ID As String
'    Dim xFave As String
'
'    With m_oPersons
'        lNumRows = .RecordCount
'
'        If lNumRows > 0 Then
'            .MoveLast
'            .MoveFirst
'            lNumRows = .RecordCount
'            m_oArray.ReDim 0, lNumRows - 1, 0, 5
'            For i = 0 To lNumRows - 1
'                .AbsolutePosition = i
'                On Error Resume Next
'                xDisplayName = !fldDisplayName
'                xSource = !fldSource
'                xID = !fldID
'                xBar_ID = !fldBarID
'                xFave = !fldFavorite
'                With m_oArray
''                    .Value(i, xarCol_Source) = xSource
'                    .Value(i, xarCol_DisplayName) = xDisplayName
'                    .Value(i, xarCol_ID) = xID
'                    .Value(i, xarCol_Favorite) = xFave
'                    .Value(i, xarCol_BarID) = xBar_ID
'                End With
'            Next i
'        Else
'            m_oArray.ReDim 0, -1, 0, 5
'        End If
'    End With
'End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting
    Dim lNumRows As Long
    Dim i As Integer
    Dim xDisplayName As String
    Dim xID As String
    Dim xFave As String
    
    With m_oPersons
        If .RecordCount Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_oArray.ReDim 0, lNumRows - 1, 0, 2
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xDisplayName = !fldDisplay
                xID = !fldID
                xFave = !fldFavorite
                With m_oArray
                    .Value(i, xarCol_DisplayName) = xDisplayName
                    .Value(i, xarCol_ID) = xID
                    .Value(i, xarCol_Favorite) = xFave
                End With
            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject()
'updates person object with row from db
    Dim i As Integer
    Dim xField As String
    Dim oOffice As mpDB.COffice
    Dim xSQL As String
    Dim xMsg As String
            
    
    On Error GoTo ProcError
    With m_oPersons
        On Error Resume Next
        m_oPerson.ID = !fldID
        m_oPerson.Source = !fldSource
        m_oPerson.DisplayName = !fldDisplay
        m_oPerson.FullName = !fldfullname
        m_oPerson.EMail = !fldEMail
        m_oPerson.Fax = !fldFax
        m_oPerson.FirstName = !fldfirstname
        m_oPerson.InformalName = !fldShortName
        m_oPerson.Initials = !fldInitials
        m_oPerson.IsAttorney = !fldIsAttorney
        m_oPerson.LastName = !fldLastName
        m_oPerson.MiddleName = !fldMI
        m_oPerson.Phone = !fldPhone
        m_oPerson.Title = !fldTitle
        m_oPerson.AdmittedIn = !fldAdmittedIn
        
'       get office
        On Error Resume Next
        Set oOffice = Application.Offices(!fldOfficeID)
        On Error GoTo ProcError
        
        If oOffice Is Nothing Then
'           invalid office - alert and reset to default office
            m_oPerson.Office = Application.Offices.Default
            xMsg = "Invalid office (ID = " & !fldOfficeID & ") for " & m_oPerson.FirstName & _
                " " & m_oPerson.LastName & ".  MacPac will change the " & _
                "office for this person to " & m_oPerson.Office.DisplayName & "."
                
            If m_oPerson.Source = mpPeopleSourceList_People Then
'               one message for people from the public list
                xMsg = xMsg & "  Please contact your administrator."
            ElseIf m_oPerson.Source = mpPeopleSourceList_PeopleInput Then
'               another message for people from the private list
                xMsg = xMsg & "  You can change the office for this person " & _
                    "by clicking MacPac\Tools\Manage Authors, selecting the person, " & _
                    "and clicking 'Edit'."
            End If
            
            MsgBox xMsg, vbExclamation, "Legal MacPac 2000"
            
'           set default office
            xSQL = "UPDATE tblPeopleInput SET fldOfficeID = " & _
                m_oPerson.Office.ID & " WHERE fldID= " & m_oPerson.ID
            Application.PrivateDB.Execute xSQL
            Refresh
        Else
            m_oPerson.Office = oOffice
        End If
    End With
        
'   assign custom property values to object
    GetCustomProperties
    Exit Function
ProcError:
    RaiseError "mpDB.CPersons.UpdateObject"
    Exit Function
End Function

Private Function GetCustomProperties()
'fill custom properties collection of m_oPerson
'with values from qryPeoplePublic
    Dim i As Integer
    Dim xField As String
    
'   if there are custom properties specified in the
'   custom properties collection of the application,
'   fill custom properties collection of person with values in qryPeoplePublic
    With Application.CustomPersonProperties
        For i = 1 To .Count
            xField = .Item(i).Field
            On Error Resume Next
            m_oPerson.CustomProperties(i).Value = _
                xNullToString(m_oPersons.Fields(xField))
            If Err.Number = 3265 Then
                Err.Raise mpError_InvalidCustomPersonField
                Exit Function
            End If
        Next i
    End With
End Function

Private Function UpdateRecordset(rsP As DAO.Recordset)
    Dim i As Integer
        
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    With rsP
'       set id for Manual Entry records only - others are AutoNumber
        If m_oPerson.Source = mpPeopleSourceList_PeopleInput Then
            !fldID = m_oPerson.ID
        End If
        !fldfullname = m_oPerson.FullName
        !fldEMail = m_oPerson.EMail
        !fldFax = m_oPerson.Fax
        !fldfirstname = m_oPerson.FirstName
        !fldShortName = m_oPerson.InformalName
        !fldDisplayName = m_oPerson.DisplayName
        !fldInitials = m_oPerson.Initials
        !fldIsAttorney = m_oPerson.IsAttorney
        !fldLastName = m_oPerson.LastName
        !fldMI = m_oPerson.MiddleName
        !fldOfficeID = m_oPerson.Office.ID
        !fldPhone = m_oPerson.Phone
        !fldTitle = m_oPerson.Title
        'pre mp 9.5.1 might not contain this field
        On Error Resume Next
        !fldAdmittedIn = m_oPerson.AdmittedIn
        #If compHandleErrors Then
            On Error GoTo ProcError
        #End If
        
'       do custom fields
        With m_oPerson.CustomProperties
            For i = 1 To .Count
                With .Item(i)
                    If .Value <> Empty Then
                        rsP.Fields(.Field).Value = CStr(.Value)
                    End If
                End With
            Next i
        End With
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CPersons.UpdateRecordset"
    Exit Function
End Function

Public Function AddAsFavorite(lID As Long)
' ensures that lID is a record in tblPeoplePrivate
    Dim xSQL As String
    xSQL = "INSERT INTO tblFavoritePeople (fldID) Values (" & lID & ")"
    Application.PrivateDB.Execute xSQL
End Function

Public Function RemoveAsFavorite(lID As Long)
' ensures that lID is not a record in tblPeoplePrivate
    Dim xSQL As String
    xSQL = "DELETE * FROM tblFavoritePeople WHERE fldID = " & lID
    Application.PrivateDB.Execute xSQL
End Function

Private Function DeleteUserOptions(ByVal lID As Long, Optional ByVal xTemplate As String)
'delete user options record for the person whose id = lID -
'if xTemplate is not supplied, cycle through all user
'options tables and delete all records for lID
    Dim oTD As DAO.TableDef
    Dim xSQL As String
    
    With Application.PrivateDB
        If xTemplate <> "" Then
'           delete preferences for specified template
            xSQL = "DELETE * FROM tblPreferences" & xTemplate & " WHERE fldID = " & lID
            .Execute xSQL
        Else
'           delete preferences in all templates
            For Each oTD In .TableDefs
                If UCase(Left(oTD.Name, 14)) = "TBLPREFERENCES" Then
                    xSQL = "DELETE * FROM " & oTD.Name & " WHERE fldID = " & lID
                    .Execute xSQL
                End If
            Next oTD
        End If
    End With
End Function
