VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingType Class
'   created 1/11/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a type of MacPac Pleading

'   Member of CPleadingTypes
'   Container for
'**********************************************************

Private m_lID As Long
Private m_iLevel0 As Integer
Private m_iLevel1 As Integer
Private m_iLevel2 As Integer
Private m_iLevel3 As Integer
Private m_xBPFile As String
Private m_xBPCoverPageFile As String
Private m_xBPBlueBackFile As String
Private m_xFooterCaseNumberSeparator As String
Private m_xCourtTitle As String
Private m_xCourtTitle1 As String
Private m_xCourtTitle2 As String
Private m_xCourtTitle3 As String
Private m_lDefCaption As Long
Private m_lDefCaption2 As Long
Private m_lDefCounsel As Long
Private m_lDefSignature As Long
Private m_lDefPleadingPaper As Long
Private m_lFooterTextMaxChars As Long
Private m_lNormalStyleLineSpacingRule As Long
Private m_sNormalStyleLineSpacing As Single
Private m_lBodyTextStyleLineSpacingRule As Long
Private m_sBodyTextStyleLineSpacing As Single
Private m_lBodyTextStyleSpaceAfter As Long
Private m_sBodyTextStyleFirstLineIndent As Single
Private m_bHasCoverPageSection As Boolean
Private m_bRequiresFooter As Boolean
Private m_bRequiresDocTitle As Boolean
Private m_bUseBoilerplateFooterSetup As Boolean
Private m_bytFirstNumberedPage As Byte
Private m_bytFirstPageNumber As Byte
Private m_bAllowsCoverPage As Boolean
Private m_bAllowsBlueBack As Boolean
Private m_bAllowsFontChange As Boolean
Private m_bFooterTextIncludesCaseNo As Boolean
Private m_bUnderlineDocumentTitle As Boolean
Private m_bAllowCrossAction As Boolean
Private m_bAllowCoCounsel As Boolean
Private m_bIsMultiparty As Boolean
Private m_xDefaultNumberingScheme As String
Private m_bNoticeAttorney As Boolean



Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Level0(iNew As Integer)
    m_iLevel0 = iNew
End Property

Public Property Get Level0() As Integer
    Level0 = m_iLevel0
End Property

Public Property Let Level1(iNew As Integer)
    m_iLevel1 = iNew
End Property

Public Property Get Level1() As Integer
    Level1 = m_iLevel1
End Property

Public Property Let Level2(iNew As Integer)
    m_iLevel2 = iNew
End Property

Public Property Get Level2() As Integer
    Level2 = m_iLevel2
End Property

Public Property Let Level3(iNew As Integer)
    m_iLevel3 = iNew
End Property

Public Property Get Level3() As Integer
    Level3 = m_iLevel3
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Public Property Let CoverPageBoilerplateFile(xNew As String)
    m_xBPCoverPageFile = xNew
End Property
Public Property Get CoverPageBoilerplateFile() As String
    CoverPageBoilerplateFile = m_xBPCoverPageFile
End Property

Public Property Get DefaultNumberingScheme() As String
    DefaultNumberingScheme = m_xDefaultNumberingScheme
End Property

Public Property Let DefaultNumberingScheme(xNew As String)
    m_xDefaultNumberingScheme = xNew
End Property

Public Property Let BlueBackBoilerplateFile(xNew As String)
    m_xBPBlueBackFile = xNew
End Property

Public Property Get BlueBackBoilerplateFile() As String
    BlueBackBoilerplateFile = m_xBPBlueBackFile
End Property

Public Property Let DefaultCaptionType(lNew As Long)
    m_lDefCaption = lNew
End Property

Public Property Get DefaultCaptionType() As Long
    DefaultCaptionType = m_lDefCaption
End Property

Public Property Let DefaultCaptionType2(lNew As Long)
    m_lDefCaption2 = lNew
End Property

Public Property Get DefaultCaptionType2() As Long
    DefaultCaptionType2 = m_lDefCaption2
End Property

Public Property Let DefaultCounselType(lNew As Long)
    m_lDefCounsel = lNew
End Property

Public Property Get DefaultCounselType() As Long
    DefaultCounselType = m_lDefCounsel
End Property

Public Property Let DefaultSignatureType(lNew As Long)
    m_lDefSignature = lNew
End Property

Public Property Get DefaultSignatureType() As Long
    DefaultSignatureType = m_lDefSignature
End Property

Public Property Let DefaultPleadingPaperType(lNew As Long)
    m_lDefPleadingPaper = lNew
End Property

Public Property Get DefaultPleadingPaperType() As Long
    DefaultPleadingPaperType = m_lDefPleadingPaper
End Property

Public Property Let FooterTextMaxChars(lNew As Long)
    m_lFooterTextMaxChars = lNew
End Property

Public Property Get FooterTextMaxChars() As Long
    FooterTextMaxChars = m_lFooterTextMaxChars
End Property

Public Property Let CourtTitle(xNew As String)
    m_xCourtTitle = xNew
End Property

Public Property Get CourtTitle() As String
    CourtTitle = m_xCourtTitle
End Property

Public Property Let CourtTitle1(xNew As String)
    m_xCourtTitle1 = xNew
End Property

Public Property Get CourtTitle1() As String
    CourtTitle1 = m_xCourtTitle1
End Property

Public Property Let CourtTitle2(xNew As String)
    m_xCourtTitle2 = xNew
End Property

Public Property Get CourtTitle2() As String
    CourtTitle2 = m_xCourtTitle2
End Property

Public Property Let CourtTitle3(xNew As String)
    m_xCourtTitle3 = xNew
End Property

Public Property Get CourtTitle3() As String
    CourtTitle3 = m_xCourtTitle3
End Property

Public Property Let FirstNumberedPage(bytNew As Byte)
    m_bytFirstNumberedPage = bytNew
End Property

Public Property Get FirstNumberedPage() As Byte
    FirstNumberedPage = m_bytFirstNumberedPage
End Property

Public Property Let FirstPageNumber(bytNew As Byte)
    m_bytFirstPageNumber = bytNew
End Property

Public Property Get FirstPageNumber() As Byte
    FirstPageNumber = m_bytFirstPageNumber
End Property
Public Property Let UnderlineDocumentTitle(bNew As Boolean)
    m_bUnderlineDocumentTitle = bNew
End Property
Public Property Get UnderlineDocumentTitle() As Boolean
    UnderlineDocumentTitle = m_bUnderlineDocumentTitle
End Property
Public Property Let RequiresFooter(bNew As Boolean)
    m_bRequiresFooter = bNew
End Property
Public Property Get RequiresFooter() As Boolean
    RequiresFooter = m_bRequiresFooter
End Property
Public Property Let RequiresDocTitle(bNew As Boolean)
    m_bRequiresDocTitle = bNew
End Property
Public Property Get RequiresDocTitle() As Boolean
    RequiresDocTitle = m_bRequiresDocTitle
End Property
Public Property Let AllowCrossAction(bNew As Boolean)
    m_bAllowCrossAction = bNew
End Property
Public Property Get AllowCrossAction() As Boolean
    AllowCrossAction = m_bAllowCrossAction
End Property
Public Property Let AllowCoCounsel(bNew As Boolean)
    m_bAllowCoCounsel = bNew
End Property
Public Property Get AllowCoCounsel() As Boolean
    AllowCoCounsel = m_bAllowCoCounsel
End Property
Public Property Let HasCoverPageSection(bNew As Boolean)
    m_bHasCoverPageSection = bNew
End Property
Public Property Get HasCoverPageSection() As Boolean
    HasCoverPageSection = m_bHasCoverPageSection
End Property
Public Property Let UseBoilerplateFooterSetup(bNew As Boolean)
    m_bUseBoilerplateFooterSetup = bNew
End Property
Public Property Get UseBoilerplateFooterSetup() As Boolean
    UseBoilerplateFooterSetup = m_bUseBoilerplateFooterSetup
End Property
Public Property Let AllowsCoverPage(bNew As Boolean)
    m_bAllowsCoverPage = bNew
End Property
Public Property Get AllowsCoverPage() As Boolean
    AllowsCoverPage = m_bAllowsCoverPage
End Property
Public Property Let AllowsBlueBack(bNew As Boolean)
    m_bAllowsBlueBack = bNew
End Property
Public Property Get AllowsBlueBack() As Boolean
    AllowsBlueBack = m_bAllowsBlueBack
End Property
Public Property Let FooterTextIncludesCaseNo(bNew As Boolean)
    m_bFooterTextIncludesCaseNo = bNew
End Property
Public Property Get FooterTextIncludesCaseNo() As Boolean
    FooterTextIncludesCaseNo = m_bFooterTextIncludesCaseNo
End Property
Public Property Let FooterCaseNumberSeparator(xNew As String)
    m_xFooterCaseNumberSeparator = xNew
End Property
Public Property Get FooterCaseNumberSeparator() As String
    FooterCaseNumberSeparator = m_xFooterCaseNumberSeparator
End Property
Public Property Let AllowsFontChange(bNew As Boolean)
    m_bAllowsFontChange = bNew
End Property
Public Property Get AllowsFontChange() As Boolean
    AllowsFontChange = m_bAllowsFontChange
End Property
Public Property Let IsMultiParty(bNew As Boolean)
    m_bIsMultiparty = bNew
End Property
Public Property Get IsMultiParty() As Boolean
    IsMultiParty = m_bIsMultiparty
End Property
Public Property Let NormalStyleLineSpacingRule(lNew As Long)
    m_lNormalStyleLineSpacingRule = lNew
End Property
Public Property Get NormalStyleLineSpacingRule() As Long
    NormalStyleLineSpacingRule = m_lNormalStyleLineSpacingRule
End Property
Public Property Let BodyTextStyleLineSpacing(sNew As Single)
    m_sBodyTextStyleLineSpacing = sNew
End Property
Public Property Get BodyTextStyleLineSpacing() As Single
    BodyTextStyleLineSpacing = m_sBodyTextStyleLineSpacing
End Property
Public Property Let NormalStyleLineSpacing(sNew As Single)
    m_sNormalStyleLineSpacing = sNew
End Property
Public Property Get NormalStyleLineSpacing() As Single
    NormalStyleLineSpacing = m_sNormalStyleLineSpacing
End Property
Public Property Let BodyTextStyleLineSpacingRule(lNew As Long)
    m_lBodyTextStyleLineSpacingRule = lNew
End Property
Public Property Get BodyTextStyleLineSpacingRule() As Long
    BodyTextStyleLineSpacingRule = m_lBodyTextStyleLineSpacingRule
End Property
Public Property Let BodyTextStyleSpaceAfter(lNew As Long)
    m_lBodyTextStyleSpaceAfter = lNew
End Property
Public Property Get BodyTextStyleSpaceAfter() As Long
    BodyTextStyleSpaceAfter = m_lBodyTextStyleSpaceAfter
End Property
Public Property Let BodyTextStyleFirstLineIndent(sNew As Single)
    m_sBodyTextStyleFirstLineIndent = sNew
End Property
Public Property Get BodyTextStyleFirstLineIndent() As Single
    BodyTextStyleFirstLineIndent = m_sBodyTextStyleFirstLineIndent
End Property
Public Property Let NoticeAttorney(bNew As Boolean)
    m_bNoticeAttorney = bNew
End Property
Public Property Get NoticeAttorney() As Boolean
    NoticeAttorney = m_bNoticeAttorney
End Property


Private Sub Class_Initialize()
'    '---for back compatibility with pre-9.3.1 tblPleadingTypes, we'll initialize normal and body text module variables
'    m_lNormalStyleLineSpacingRule = mpStyleValueUndefined
'    m_lNormalStyleLineSpacing = mpStyleValueUndefined
'    m_lBodyTextStyleLineSpacingRule = mpStyleValueUndefined
'    m_lBodyTextStyleLineSpacing = mpStyleValueUndefined
'    m_lBodyTextStyleSpaceAfter = mpStyleValueUndefined
'    m_sBodyTextStyleFirstLineIndent = mpStyleValueUndefined
End Sub
