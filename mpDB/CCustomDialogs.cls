VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomDialogs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CCustomDialogs Collection Class
'   created 12/10/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CCustomDialogs

'   Member of
'   Container for CCustomDialog
'**********************************************************

'**********************************************************
Private m_oRS As DAO.Recordset
Private m_oCDlg As mpDB.CCustomDialog
Private m_xTemplate As String
'**********************************************************


'**********************************************************
'   Properties
'**********************************************************
Public Property Let Template(xNew As String)
    Dim xDesc As String
    
    If Application.TemplateDefinitions(xNew) Is Nothing Then
        Err.Raise mpError_InvalidTemplateName
    End If
    m_xTemplate = xNew
    
'   get recordset containing custom
'   properties for supplied template
    Refresh xNew
    Exit Property
ProcError:
    If Err.Number = mpError_InvalidTemplateName Then
        xDesc = xNew & " is an invalid template."
    Else
        xDesc = Application.Error.Desc(Err.Number)
    End If
    Err.Raise Err.Number, _
              "MPDB.CCustomDialogs.Template", _
              xDesc
End Property

Public Property Get Template() As String
    Template = m_xTemplate
End Property

'**********************************************************
'   Methods
'**********************************************************
Private Sub Refresh(Optional ByVal xTemplate As String)
'get recordset of property defs from db
    Dim xSQL As String
    
'   if Template is not supplied, check property
    If Len(xTemplate) = 0 Then
        xTemplate = Me.Template
    End If
    
'   if still no Template, get all custom controls,
'   else get custom controls in specified Template
    If Len(xTemplate) Then
        xSQL = "SELECT * " & _
               "FROM tblCustomDialogs " & _
               "WHERE fldTemplate='" & xTemplate & "' " & _
               "ORDER BY tblCustomControls.fldIndex"
    Else
        xSQL = "SELECT tblCustomControls.*, tblCustomProperties.fldName " & _
               "FROM tblCustomControls INNER JOIN tblCustomProperties ON " & _
               "     tblCustomControls.fldPropertyID = tblCustomProperties.fldID " & _
               "ORDER BY tblCustomControls.fldIndex"
    End If
           
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
'   populate if possible
    If Not (m_oRS.BOF And m_oRS.EOF) Then
        m_oRS.MoveLast
        m_oRS.MoveFirst
    End If
End Sub

Public Property Get Count() As Integer
    If Not (m_oRS Is Nothing) Then
        Count = m_oRS.RecordCount
    End If
End Property

Public Function Item(vID As Variant) As mpDB.CCustomDialog
Attribute Item.VB_UserMemId = 0
'finds the record with ID = vID or Name = vID -
'returns the found record as a CCustomDialog-
'raises error if no match - returns "Nothing" if
'no recordset exists

    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If m_oRS Is Nothing Then
        Exit Function
    End If
       
    With m_oRS
'       set criteria
        If IsNumeric(vID) Then
            On Error Resume Next
            .MoveFirst
            .AbsolutePosition = vID - 1
            If Err.Number Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            On Error GoTo ProcError
        Else
            xCriteria = "tblCustomProperties.fldName = '" & vID & "'"
    
'           find
            .FindFirst xCriteria
            
            If .NoMatch Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            
        End If
        
'       requested item exists - update current object
        UpdateObject
                
'       return current object
        Set Item = m_oCDlg
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplatePropertyDefs.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oCDlg = New mpDB.CCustomDialog
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
    Set m_oCDlg = Nothing
End Sub

Private Sub UpdateObject()
    With m_oCDlg
        .Caption = Empty
        .ControlType = Empty
        .Index = Empty
        Set .List = Nothing
        .Name = Empty
        .TabID = Empty
        
        On Error Resume Next
        .Caption = m_oRS!fldCaption
        .ControlType = m_oRS!fldType
        .Index = m_oRS!fldIndex
        If Not IsNull(m_oRS!fldList) Then
            Set .List = Application.Lists(m_oRS!fldList)
        End If
        .Name = m_oRS!fldName
        .TabID = m_oRS!fldTab
    End With
End Sub
