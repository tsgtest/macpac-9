VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplateDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplates Collection Class
'   created 11/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CTemplates

'   Member of
'   Container for
'**********************************************************

'**********************************************************
Private m_oDef As CTemplateDef
Private m_oRS As DAO.Recordset
Private m_iGroup As Integer
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Group(iNew As Integer)
    m_iGroup = iNew
    Me.Refresh
End Property

Public Property Get Group() As Integer
    Group = m_iGroup
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Add(ByVal xFileName As String) As CTemplateDef
    On Error GoTo ProcError
    
    If xFileName = Empty Then
        Err.Raise mpError.mpError_InvalidTemplateFileName, , _
            "xFileName can't be empty."
    ElseIf Application.TemplateDefinitions.Exists(xFileName) Then
        'template definition already exists
        Err.Raise mpError.mpError_InvalidParameter, , _
            "Could not add the template definition.  A definition with filename '" & _
                xFileName & "' already exists."
    End If
    
    Set m_oDef = New CTemplateDef
    m_oDef.FileName = xFileName
    
    m_oDef.IsNew = True
    Set Add = m_oDef
    
    Exit Function
ProcError:
    RaiseError "mpDB.CTemplateDefs.Add", Err.Description
    Exit Function
End Function

Public Sub Save(oDef As CTemplateDef)
'saves the specified template def
    Dim xSQL As String
    On Error GoTo ProcError
    'check to see if the filename
    
    If oDef Is Nothing Then
        'no current item to save - do nothing
        Exit Sub
    End If
    
'   validate
    With oDef
        If .FileName = Empty Or .ShortName = Empty Or .TemplateType = Empty Or _
            .BoilerplateFile = Empty Or .DefaultDocIDStamp = Empty Then
            'failed validation
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save template definition.  The definition is missing required data."
        ElseIf .IsNew And Me.Exists(.FileName) Then
            'template definition already exists
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save template definition.  A template with filename '" & _
                    .FileName & "' has already been defined."
        End If
    
        If .IsNew Then
            'run sql statement - we don't use dao to do this
            'because the recordset might not be updatable
            'given it's joins.
            xSQL = "INSERT INTO tblTemplates(fldFileName,fldShortName,fldDescription," & _
                "fldType,fldBaseTemplate,fldOptionsTable,fldBoilerplateFile," & _
                "fldDefaultDocIDStamp,fldClassName,fldMacro,fldWriteToBP," & _
                "fldProtectedSections,fldEditMacro,fldSubFolder) VALUES (""" & _
                .FileName & """,""" & _
                .ShortName & """,""" & _
                .Description & """," & _
                .TemplateType & ",""" & _
                .BaseTemplate & """,""" & _
                .OptionsTable & """,""" & _
                .BoilerplateFile & """," & _
                .DefaultDocIDStamp & ",""" & _
                .ClassName & """,""" & _
                .Macro & """," & _
                .WriteToBP & ",""" & _
                .ProtectedSections & """,""" & _
                .EditMacro & """,""" & _
                .SubFolder & """)"
        Else
            'run sql statement - we don't use dao to do this
            'because the recordset might not be updatable
            'given it""s joins.
            xSQL = "UPDATE tblTemplates SET fldFileName = """ & .FileName & _
                """,fldShortName=""" & .ShortName & _
                """,fldDescription=""" & .Description & _
                """,fldType=" & .TemplateType & _
                ",fldBaseTemplate=""" & .BaseTemplate & _
                """,fldOptionsTable=""" & .OptionsTable & _
                """,fldBoilerplateFile=""" & .BoilerplateFile & _
                """,fldDefaultDocIDStamp=" & .DefaultDocIDStamp & _
                ",fldClassName=""" & .ClassName & _
                """,fldMacro=""" & .Macro & _
                """,fldWriteToBP=" & .WriteToBP & _
                ",fldProtectedSections=""" & .ProtectedSections & _
                ",fldEditMacro=""" & .EditMacro & _
                """,fldSubFolder=""" & .SubFolder & """ " & _
                "WHERE fldFileName = """ & .FileName & """"
        End If
    End With
    
    With Application.PublicDB
        'execute query
        .Execute xSQL
        If .RecordsAffected = 0 Then
            'add, update failed - raise error
            Err.Raise 600, , "Could not save '" & oDef.FileName & "'."
        Else
            oDef.IsNew = False
            Me.Refresh
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CTemplateDefs.Save", Err.Description
    Exit Sub
End Sub

Public Sub Delete(ByVal xFileName As String)
    Dim xSQL As String
    On Error GoTo ProcError
    xSQL = "DELETE * FROM tblTemplates WHERE fldFileName = '" & xFileName & "'"
    With Application.PublicDB
        'execute query
        .Execute xSQL
        If .RecordsAffected Then
            Me.Refresh
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CTemplateDefs.Delete", Err.Description
    Exit Sub
End Sub

Friend Sub DeleteAll()
    Dim xSQL As String
    xSQL = "DELETE * FROM tblTemplates"
    Application.PublicDB.Execute xSQL
    Me.Refresh
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(ByVal vID As Variant) As CTemplateDef
Attribute Item.VB_Description = "Returns theCTemplate item with ID = vID or Index = lIndex"
Attribute Item.VB_UserMemId = 0
    Dim i As Integer
    Dim xCrit As String
    
    On Error GoTo ProcError
    
    With m_oRS
        If IsNumeric(vID) Then
'           get item where index = vID
            If (vID <= 0) Or (vID > .RecordCount) Then
                Err.Raise mpError_InvalidItemIndex
            Else
                .AbsolutePosition = vID - 1
                UpdateObject
            End If
        Else
'           get item where name = vID
            xCrit = "fldFileName = '" & vID & "'"
'           attempt to find the item
            .FindFirst xCrit
            If .NoMatch Then
'               if no match found, the
'               member does not exist
                Set Item = Nothing
                Exit Function
            Else
'               item found - update template object
                UpdateObject
            End If
        End If
    End With
    
'   return template object
    m_oDef.IsNew = False
    Set Item = m_oDef
    
    Exit Function
ProcError:
    RaiseError "mpDB.CTemplateDefs.Item"
    Exit Function
End Function

Public Sub Filter(ByVal xText As String, Optional ByVal lGroup As Long, _
                Optional ByVal bSearchName As Boolean = True, _
                Optional ByVal bSearchDescription As Boolean = False)
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get templates
    xSQL = "SELECT * , Not IsNull(tblFavoriteTemplates" & _
           ".fldTemplateID) as fldFavorite " & _
           "FROM qryTemplates LEFT JOIN tblFavoriteTemplates " & _
           "ON qryTemplates.fldFileName = " & _
           "tblFavoriteTemplates.fldTemplateID WHERE NOT ISNULL(qryTemplates.fldFileName) "
           
    If Len(xText) And (bSearchName Or bSearchDescription) Then
        xSQL = xSQL & "AND "
        If bSearchName Then
            xSQL = xSQL & "(fldShortName LIKE """ & xText & "*"" "
            If bSearchDescription Then
                xSQL = xSQL & "OR fldDescription LIKE """ & xText & "*"" "
            End If
            xSQL = xSQL & ")"
        ElseIf bSearchDescription Then
            xSQL = xSQL & "fldDescription LIKE """ & xText & "*"" "
        End If
    End If
    
    If lGroup = -1 Then
'       limit to favorites
        xSQL = xSQL & "AND Not IsNull(tblFavoriteTemplates.fldTemplateID)"
    ElseIf lGroup Then
'       limit to all templates in group
        xSQL = xSQL & "AND tblTemplateAssignments.fldGroupID = " & lGroup
    Else
'       9.7.3
'       limit to 'assigned' templates
        xSQL = xSQL & "AND tblTemplateAssignments.fldGroupID <> 0"
    End If
    
'   open the rs
    Set m_oRS = Application.PrivateDB _
        .OpenRecordset(xSQL, dbOpenSnapshot)
    
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CTemplateDefs.Filter"
    Exit Sub
End Sub

Public Sub Refresh()
    RefreshFilter
End Sub

Private Sub RefreshFilter(Optional ByVal xText As String)
    Dim xSQL As String
    
    On Error GoTo ProcError
        
    If Me.Group = -1 Then
'       get favorites
        xSQL = "SELECT * , Not IsNull(tblFavoriteTemplates" & _
               ".fldTemplateID) as fldFavorite " & _
               "FROM qryTemplates INNER JOIN tblFavoriteTemplates " & _
               "ON qryTemplates.fldFileName = " & _
               "tblFavoriteTemplates.fldTemplateID "
        
'       open the rs
        Set m_oRS = Application.PrivateDB _
            .OpenRecordset(xSQL, dbOpenSnapshot)
    Else
'       get templates
        xSQL = "SELECT * , Not IsNull(tblFavoriteTemplates" & _
               ".fldTemplateID) as fldFavorite " & _
               "FROM qryTemplates LEFT JOIN tblFavoriteTemplates " & _
               "ON qryTemplates.fldFileName = " & _
               "tblFavoriteTemplates.fldTemplateID "

        If Me.Group Then
'           limit to all templates in group
            xSQL = xSQL & "WHERE tblTemplateAssignments" & _
                    ".fldGroupID = " & Me.Group
        End If
        
'       open the rs
        Set m_oRS = Application.PrivateDB _
            .OpenRecordset(xSQL, dbOpenSnapshot)
    End If
    
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CTemplateDefs.Refresh"
    Exit Sub
End Sub

Public Function ListSource() As XArrayObject.XArray
'returns XArray of templates
    Dim oArray As XArray
    Dim xName As String
    Dim xFile As String
    
    Dim i As Integer
    
    Set oArray = New XArray
    With m_oRS
        If .RecordCount Then
'           create space
            oArray.ReDim 0, .RecordCount - 1, 0, 1
            .MoveFirst

'           cycle through records, filling array
            While Not .EOF
                On Error Resume Next
                xName = m_oRS!fldShortName
                xFile = m_oRS!fldFileName
                oArray.Value(i, 0) = xName
                oArray.Value(i, 1) = xFile
                .MoveNext
                i = i + 1
            Wend
        End If
    End With
    Set ListSource = oArray
End Function

Public Sub AddToFavorites(xID As String)
'add specified file to list of favorites
    Dim xSQL As String
'   add new favorite
    xSQL = "INSERT INTO tblFavoriteTemplates (fldTemplateID) " & _
           "VALUES ('" & xID & "')"
    On Error Resume Next
    Application.PrivateDB.Execute xSQL
End Sub

Public Sub RemoveFromFavorites(xID As String)
'remove specified file from list of favorites
    Dim xSQL As String
'   delete existing favorite
    xSQL = "DELETE * FROM tblFavoriteTemplates " & _
           "WHERE fldTemplateID = '" & xID & "'"
    Application.PrivateDB.Execute xSQL
End Sub

Public Sub AddAssignment(ByVal xTemplateID As String, ByVal lGroupID As Long)
    Dim xSQL As String
    
    xSQL = "INSERT INTO tblTemplateAssignments(fldTemplateID, fldGroupID) VALUES ('" & _
            xTemplateID & "'," & lGroupID & ")"
            
    Application.PublicDB.Execute xSQL
End Sub

Public Sub DeleteAssignment(ByVal xTemplateID As String, ByVal lGroupID As Long)
    Dim xSQL As String
    
    xSQL = "DELETE FROM tblTemplateAssignments WHERE fldTemplateID= '" & xTemplateID & _
           "' AND fldGroupID=" & lGroupID
            
    Application.PublicDB.Execute xSQL
End Sub

Public Sub DeleteAssignments(ByVal xTemplateID As String)
    Dim xSQL As String
    
    xSQL = "DELETE FROM tblTemplateAssignments WHERE fldTemplateID= '" & xTemplateID & "'"
            
    Application.PublicDB.Execute xSQL
End Sub

Public Function Exists(ByVal xTemplateID As String) As Boolean
    Dim xSQL As String
    
    xSQL = "SELECT fldFileName FROM tblTemplates WHERE " & _
           "fldFileName ='" & xTemplateID & "'"
            
    Exists = Application.PublicDB.OpenRecordset(xSQL).RecordCount > 0
End Function

Public Function MemberOf(ByVal xTemplateID As String, ByVal lGroupID As Long) As Boolean
    Dim xSQL As String
    
    xSQL = "SELECT fldTemplateID FROM tblTemplateAssignments WHERE " & _
           "fldTemplateID ='" & xTemplateID & "' AND fldGroupID=" & lGroupID
            
    MemberOf = Application.PublicDB.OpenRecordset(xSQL).RecordCount > 0
End Function
'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDef = New mpDB.CTemplateDef
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
End Sub

Private Sub UpdateRecordset()
    With m_oDef
        On Error Resume Next
        m_oRS!fldShortName = .ShortName
        m_oRS!fldDescription = .Description
        m_oRS!fldFileName = .FileName
        m_oRS!fldType = .TemplateType
        m_oRS!fldBoilerplateFile = .BoilerplateFile
        m_oRS!fldOptionsTable = .OptionsTable
        m_oRS!fldClassName = .ClassName
        m_oRS!fldMacro = .Macro
        m_oRS!fldEditMacro = .EditMacro
        m_oRS!fldSubFolder = .SubFolder
        m_oRS!fldForm = .Form
        m_oRS!fldFavorite = .Favorite
        m_oRS!fldDefaultDocIDStamp = .DefaultDocIDStamp
        m_oRS!fldWriteToBP = .WriteToBP
        m_oRS!fldProtectedSections = .ProtectedSections
        m_oRS!fldBaseTemplate = .BaseTemplate
        m_oRS!fldApplication = .Application
    End With
End Sub
Public Sub UpdateObject()
    With m_oDef
        .ShortName = Empty
        .Description = Empty
        .FileName = Empty
        .TemplateType = Empty
        .BoilerplateFile = Empty
        .OptionsTable = Empty
        .ClassName = Empty
        .Macro = Empty
        .EditMacro = Empty
        .SubFolder = Empty
        .Form = Empty
        .DefaultDocIDStamp = Empty
        .WriteToBP = Empty
        .ProtectedSections = Empty
        .BaseTemplate = Empty
        .Application = 0
        
        On Error Resume Next
        .ShortName = m_oRS!fldShortName
        .Description = m_oRS!fldDescription
        .FileName = m_oRS!fldFileName
        .TemplateType = m_oRS!fldType
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .OptionsTable = m_oRS!fldOptionsTable
        .ClassName = m_oRS!fldClassName
        .Macro = m_oRS!fldMacro
        .EditMacro = m_oRS!fldEditMacro
        .SubFolder = m_oRS!fldSubFolder
        .Form = m_oRS!fldForm
        .Favorite = m_oRS!fldFavorite
        .DefaultDocIDStamp = m_oRS!fldDefaultDocIDStamp
        .WriteToBP = m_oRS!fldWriteToBP
        .ProtectedSections = m_oRS!fldProtectedSections
        .MappedVariables = GetTemplateMappings() '*c Fix 3913
        .Application = m_oRS!fldApplication
'*****************************************************************
'multitype:
'       author defaults category is template name if
'       the category field is empty.  'category' is
'       a field new to v9.2.0 that allows multiple templates
'       to reference the author defaults definitions of a
'       similar template.  Eg. LetterEnglish and LetterFrench
'       can reference the same author default controls. using
'       template name as a backup keeps it compatible with
'       earlier versions
        Dim xCat As String
        xCat = m_oRS!fldBaseTemplate
        If Len(xCat) Then
            .BaseTemplate = m_oRS!fldBaseTemplate
            Dim oRSBase As DAO.Recordset
            Dim xSQLBase As String
            xSQLBase = "SELECT fldOptionsTable, fldBoilerplateFile FROM tblTemplates WHERE fldFileName = '" & xCat & "'"
            Set oRSBase = Application.PublicDB.OpenRecordset(xSQLBase, dbOpenForwardOnly)
            If oRSBase.BOF And oRSBase.EOF Then
                Err.Raise mpErrors.mpError_InvalidTemplateName, , _
                "The base template for '" & .FileName & "' is invalid."
            Else
                If .OptionsTable = Empty Then
                    .OptionsTable = oRSBase.Fields(0)
                End If

                If .BoilerplateFile = Empty Then
                    .BoilerplateFile = oRSBase.Fields(1)
                End If
            End If
            oRSBase.Close
            Set oRSBase = Nothing
        Else
            .BaseTemplate = m_oRS!fldFileName
        End If
'*****************************************************************
    End With
End Sub


Private Function GetTemplateMappings() As String()
'returns an array with variable mapping info
    Dim xSQL As String
    Dim aVars() As String
    Dim xSource As String
    Dim xTargetVar As String
    Dim xSourceVar As String
    Dim xDefault As String
    Dim i As Integer
    Dim oRS As DAO.Recordset
    
    ReDim aVars(0, 3)
    
    On Error GoTo 0 'ProcError
    If m_oDef.ClassName <> Empty Then
        xSQL = "SELECT * FROM tblTemplateMappings " & _
               "WHERE fldTarget = '" & m_oDef.ClassName & "' OR fldTarget = '" & m_oDef.FileName & "'"
    Else
        xSQL = "SELECT * FROM tblTemplateMappings " & _
               "WHERE fldTarget = '" & m_oDef.FileName & "'"
    End If
        
'   open the rs
    Set oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
'           fill array with mapping info
            ReDim aVars(.RecordCount - 1, 3)
            While Not .EOF
                On Error Resume Next
                xSource = oRS!fldSource
                xTargetVar = xNullToString(oRS!fldTargetVariable)
                xSourceVar = xNullToString(oRS!fldSourceVariable)
                xDefault = xNullToString(oRS!fldDefault)
                On Error GoTo ProcError
                aVars(i, 0) = xSource
                aVars(i, 1) = xTargetVar
                aVars(i, 2) = xSourceVar
                aVars(i, 3) = xDefault
                i = i + 1
                .MoveNext
            Wend
        End If
    End With
    GetTemplateMappings = aVars()
    Exit Function
ProcError:
    RaiseError "mpDB.CTemplateDefs.GetTemplateMappings"
    Exit Function
End Function


