VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COptionsControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   COptionsControls Collection Class
'   created 12/10/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of COptionsControls

'   Member of
'   Container for COptionsControl
'**********************************************************

'**********************************************************
Private m_oRS As DAO.Recordset
Private m_oCCtl As mpDB.COptionsControl
Private m_xTemplate As String
'**********************************************************


'**********************************************************
'   Properties
'**********************************************************
Public Property Let Template(xNew As String)
    Dim xDesc As String
    
    If Application.TemplateDefinitions(xNew) Is Nothing Then
        Err.Raise mpError_InvalidTemplateName
    End If
    m_xTemplate = xNew
    
'   get recordset containing custom
'   properties for supplied template
    Refresh xNew
    Exit Property
ProcError:
    If Err.Number = mpError_InvalidTemplateName Then
        xDesc = xNew & " is an invalid template."
    End If
    RaiseError "mpDB.COptionsControls.Template", _
              xDesc
End Property

Public Property Get Template() As String
    Template = m_xTemplate
End Property

'**********************************************************
'   Methods
'**********************************************************
'Private Sub Refresh(Optional ByVal xTemplate As String)
''get recordset of property defs from db
'    Dim xSQL As String
'
''   if Template is not supplied, check property
'    If Len(xTemplate) = 0 Then
'        xTemplate = Me.Template
'    End If
'
''   if still no Template, get all custom controls,
''   else get custom controls in specified Template
'    If Len(xTemplate) Then
'        xSQL = "SELECT * " & _
'               "FROM tblPreferencesControls " & _
'               "WHERE fldTemplate='" & xTemplate & "' " & _
'               "ORDER BY fldIndex"
'    Else
'        xSQL = "SELECT * " & _
'               "FROM tblPreferencesControls " & _
'               "ORDER BY fldIndex"
'    End If
'
''   open recordset
'    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
'
''   populate if possible
'    If Not (m_oRS.BOF And m_oRS.EOF) Then
'        m_oRS.MoveLast
'        m_oRS.MoveFirst
'    End If
'End Sub

Private Sub Refresh(Optional ByVal xTemplate As String)
'get recordset of property defs from db
    Dim xSQL As String
    
'   if Template is not supplied, check property
    On Error GoTo ProcError
    If Len(xTemplate) = 0 Then
        xTemplate = Me.Template
    End If
    
'   if still no Template, get all custom controls,
'   else get custom controls in specified Template
    If Len(xTemplate) Then
'*******************************************************************
'multitype:
'       attempt to get controls for template - see if any are defined
        Dim iNumCtls As Integer
        
        xSQL = "SELECT COUNT(fldTemplate) FROM tblPreferencesControls " & _
               "WHERE fldTemplate='" & xTemplate & "' "
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenForwardOnly)
        iNumCtls = m_oRS.Fields(0)
        m_oRS.Close
        
        If iNumCtls Then
'           controls exist, get them
            xSQL = "SELECT * " & _
                   "FROM tblPreferencesControls " & _
                   "WHERE fldTemplate='" & xTemplate & "' " & _
                   "ORDER BY fldIndex"
        Else
'           controls don't exist, get them from base template
            Dim xBase As String
            xBase = Application.TemplateDefinitions(xTemplate).BaseTemplate
            
            xSQL = "SELECT * FROM tblPreferencesControls " & _
                   "WHERE fldTemplate='" & xBase & "' " & _
                   "ORDER BY fldIndex"
        End If
    Else
        xSQL = "SELECT * FROM tblPreferencesControls " & _
               "ORDER BY fldIndex"
    End If

'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
'   populate if possible
    If Not (m_oRS.BOF And m_oRS.EOF) Then
        m_oRS.MoveLast
        m_oRS.MoveFirst
    Else
'       alert that controls must be created in db
        Dim xMsg As String
        xMsg = "No author defaults controls have been created for " & _
            xTemplate & ". Please either create controls in tblPreferencesControls, " & _
            "or specify a different template in tblTemplates.'Author Defaults Category'."
        MsgBox xMsg, vbExclamation, App.Title
'******************************************
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.COptionsControls.Refresh"
    Exit Sub
End Sub

Public Property Get Count() As Integer
    If Not (m_oRS Is Nothing) Then
        Count = m_oRS.RecordCount
    End If
End Property

Public Function Item(vID As Variant) As mpDB.COptionsControl
Attribute Item.VB_UserMemId = 0
'finds the record with ID = vID or Name = vID -
'returns the found record as a COptionsControl-
'raises error if no match - returns "Nothing" if
'no recordset exists

    Dim xCriteria As String
    Dim vBkmk As Variant
    
    On Error GoTo ProcError
    
    If m_oRS Is Nothing Then
        Exit Function
    End If
       
    With m_oRS
'       set criteria
        If IsNumeric(vID) Then
            On Error Resume Next
            .MoveFirst
            .AbsolutePosition = vID - 1
            If Err.Number Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            On Error GoTo ProcError
        Else
            xCriteria = "fldOptionsField = '" & vID & "'"
    
'           find
            .FindFirst xCriteria
            
            If .NoMatch Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            
        End If
        
'       requested item exists - update current object
        UpdateObject
                
'       return current object
        Set Item = m_oCCtl
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.COptionsControls.Item"
    Exit Function
End Function

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oCCtl = New mpDB.COptionsControl
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
    Set m_oCCtl = Nothing
End Sub

Private Sub UpdateObject()
    With m_oCCtl
        .Caption = Empty
        .ControlType = Empty
        .Index = Empty
        Set .List = Nothing
        .OptionsField = Empty
        .VerticalOffset = Empty
        .HorizontalOffset = Empty
        .Width = Empty
        .MaxValue = Empty
        .MinValue = Empty
        .Increment = Empty
        .AppendQuotes = Empty
        .MeasurementUnit = Empty '*c
        
        On Error Resume Next
        .ControlType = m_oRS!fldType
        .Caption = m_oRS!fldCaption
        .Index = m_oRS!fldIndex
        If Not IsNull(m_oRS!fldList) Then
            Set .List = Application.Lists(m_oRS!fldList)
        End If
        .OptionsField = m_oRS!fldOptionsField
        .VerticalOffset = m_oRS!fldVerticalOffset
        .HorizontalOffset = m_oRS!fldHorizontalOffset
        .Width = m_oRS!fldWidth
        .MaxValue = m_oRS!fldMaxValue
        .MinValue = m_oRS!fldMinValue
        .Increment = m_oRS!fldIncrement
        .AppendQuotes = m_oRS!fldAppendQuotes
        .MeasurementUnit = m_oRS!fldMeasurementUnit '*c
    End With
End Sub
