VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSegmentDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CSegmentDef Class
'   created 7/9/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define
'   a MacPac segment - i.e. a document or boilerplate
'   that can be inserted into another document

'   Member of CSegmentDef
'**********************************************************
Private m_lID As Long
Private m_xName As String
Private m_xDescription As String
Private m_xSourceTemplate As String
Private m_xDestTemplate As String
Private m_xBP As String
Private m_bAllowInStartSection As Boolean
Private m_bAllowInEndSection As Boolean
Private m_bAllowInCursorSection As Boolean
Private m_bAllowAtCursor As Boolean
Private m_bAllowAtEndOfDoc As Boolean
Private m_bAllowAtStartOfDoc As Boolean
Private m_bAllowExistingHeadFoots As Boolean
Private m_aVars() As String
Private m_xBaseStyle As String

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let SourceTemplate(xNew As String)
    m_xSourceTemplate = xNew
End Property

Public Property Get SourceTemplate() As String
    SourceTemplate = m_xSourceTemplate
End Property

Public Property Let DestTemplate(xNew As String)
    m_xDestTemplate = xNew
End Property

Public Property Get DestTemplate() As String
    DestTemplate = m_xDestTemplate
End Property

Public Property Let Boilerplate(xNew As String)
    m_xBP = xNew
End Property

Public Property Get Boilerplate() As String
    Boilerplate = m_xBP
End Property

Friend Property Let MappedVariables(aNew() As String)
    m_aVars = aNew
End Property

Public Property Get MappedVariables() As String()
    MappedVariables = m_aVars
End Property

Public Property Let AllowInStartSection(bNew As Boolean)
    m_bAllowInStartSection = bNew
End Property

Public Property Get AllowInStartSection() As Boolean
    AllowInStartSection = m_bAllowInStartSection
End Property

Public Property Let AllowInEndSection(bNew As Boolean)
    m_bAllowInEndSection = bNew
End Property

Public Property Get AllowInEndSection() As Boolean
    AllowInEndSection = m_bAllowInEndSection
End Property

Public Property Let AllowInCursorSection(bNew As Boolean)
    m_bAllowInCursorSection = bNew
End Property

Public Property Get AllowInCursorSection() As Boolean
    AllowInCursorSection = m_bAllowInCursorSection
End Property

Public Property Let AllowAtCursor(bNew As Boolean)
    m_bAllowAtCursor = bNew
End Property

Public Property Get AllowAtCursor() As Boolean
    AllowAtCursor = m_bAllowAtCursor
End Property

Public Property Let AllowAtEndOfDocument(bNew As Boolean)
    m_bAllowAtEndOfDoc = bNew
End Property

Public Property Get AllowAtEndOfDocument() As Boolean
    AllowAtEndOfDocument = m_bAllowAtEndOfDoc
End Property

Public Property Let AllowAtStartOfDocument(bNew As Boolean)
    m_bAllowAtStartOfDoc = bNew
End Property

Public Property Get AllowAtStartOfDocument() As Boolean
    AllowAtStartOfDocument = m_bAllowAtStartOfDoc
End Property

Public Property Let AllowExistingHeaderFooters(bNew As Boolean)
    m_bAllowExistingHeadFoots = bNew
End Property

Public Property Get AllowExistingHeaderFooters() As Boolean
    AllowExistingHeaderFooters = m_bAllowExistingHeadFoots
End Property

Public Property Get BaseStyle() As String
    BaseStyle = m_xBaseStyle
End Property
Public Property Let BaseStyle(xNew As String)
    m_xBaseStyle = xNew
End Property

