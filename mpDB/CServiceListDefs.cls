VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CServiceListDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CServiceListDefs Collection Class
'   created 5/27/00 by Daniel Fisherman-

'   Contains properties/methods that manage the
'   collection of Service List definitions -
'   collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for CServiceListDef
'**********************************************************


Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CServiceListDef
Private m_oArray As XArray
Private m_bDoRefreshArray As Boolean
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function ListSource() As XArray
    If m_oArray Is Nothing Then
        RefreshXArray
    ElseIf m_bDoRefreshArray Then
        RefreshXArray
    End If
    Set ListSource = m_oArray
End Function

Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CServiceListDef
Attribute Item.VB_UserMemId = 0
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where index = lID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       get item where name = vID
        xCrit = "fldID = " & lID
            
'       attempt to find the item
        m_oRS.FindFirst xCrit
        If m_oRS.NoMatch Then
'           if no match found, the
'           member does not exist
            Err.Raise mpError_InvalidMember
        Else
'           item found - update template object
            UpdateObject
        End If
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CServiceListDefs.Item"
End Function

Public Function Refresh(Optional ByVal lServiceType As Long)
    Dim xSQL As String
    
    On Error GoTo ProcError
        
    If lServiceType Then
'       get all service list types for specified service type
        xSQL = "SELECT * FROM tblServiceListAssignments INNER JOIN tblServiceListTypes ON " & _
            "tblServiceListAssignments.fldServiceListType = tblServiceListTypes.fldID "
        If lServiceType Then
            xSQL = xSQL & " WHERE fldServiceType = " & lServiceType
        End If
               
    '   open the rs
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
        With m_oRS
            If .RecordCount Then
                .MoveLast
                .MoveFirst
            Else
'               get catchall service types
                xSQL = "SELECT * FROM tblServiceListAssignments INNER JOIN tblServiceListTypes ON " & _
                    "tblServiceListAssignments.fldServiceListType = tblServiceListTypes.fldID "
                If lServiceType Then
                    xSQL = xSQL & " WHERE fldServiceType = 0"
                End If
                       
'               open the rs
                Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
                If m_oRS.RecordCount Then
                    m_oRS.MoveLast
                    m_oRS.MoveFirst
                End If
            End If
        End With
    Else
'       get all service list types
        xSQL = "SELECT * FROM tblServiceListTypes"
               
    '   open the rs
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
        With m_oRS
            If .RecordCount Then
                .MoveLast
                .MoveFirst
            End If
        End With
    End If
    
    m_bDoRefreshArray = True
    Exit Function
ProcError:
    RaiseError "mpDB.CServiceListDefs.Refresh"
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xID As String
    Dim xDescription As String
    
    On Error GoTo ProcError
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            Set m_oArray = New XArray
            m_oArray.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldName
                m_oArray.Value(i, 0) = xDescription
                m_oArray.Value(i, 1) = xID
            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
    m_bDoRefreshArray = False
    Exit Sub
ProcError:
    RaiseError "mpDB.CServiceListDefs.RefreshXArray"
    Exit Sub
End Sub

Private Sub UpdateObject()
    With m_oType
        .CellsPerRecipient = Empty
        .Description = Empty
        .ID = Empty
        .Name = Empty
        .BoilerplateFile = Empty
        .AllowEMail = Empty
        .AllowFax = Empty
        .AllowPhone = Empty
        .IncludeEMail = Empty
        .IncludeFax = Empty
        .IncludePhone = Empty
        .LeftIndent = Empty
        .RightIndent = Empty
        .WidthRule = Empty
        .SeparatePageText = Empty
        .SpaceBetweenRows = Empty
        .AllowPartyNames = Empty
        .AllowPartyTitle = Empty
        .AllowDPhrase = Empty
        '---9.7.1
        .AllowNonEF = Empty
        
        On Error Resume Next
        
        .CellsPerRecipient = m_oRS!fldCellsPerRecipient
        .Description = m_oRS!fldDescription
        .ID = m_oRS!fldID
        .Name = m_oRS!fldName
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .AllowEMail = m_oRS!fldAllowEMail
        .AllowFax = m_oRS!fldAllowFax
        .AllowPhone = m_oRS!fldAllowPhone
        .IncludeEMail = m_oRS!fldIncludeEMail
        .IncludeFax = m_oRS!fldIncludeFax
        .IncludePhone = m_oRS!fldIncludePhone
        .LeftIndent = m_oRS!fldLeftIndent
        .RightIndent = m_oRS!fldRightIndent
        .WidthRule = m_oRS!fldWidthRule
        .SeparatePageText = m_oRS!fldSeparatePageText
        .SpaceBetweenRows = m_oRS!fldSpaceBetweenRows
        .AllowPartyNames = m_oRS!fldAllowPartyNames
        .AllowPartyTitle = m_oRS!fldAllowPartyTitle
        .AllowDPhrase = m_oRS!fldAllowDPhrase
        '---9.7.1
        .AllowNonEF = m_oRS!fldAllowNonEF
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oType = New mpDB.CServiceListDef
End Sub


