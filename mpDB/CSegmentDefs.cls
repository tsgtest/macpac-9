VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSegmentDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CSegmentDefs Collection Class
'   created 5/16/00 by Daniel Fisherman-

'   Contains properties/methods that manage the
'   collection of Service Document definitions -
'   collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for CSegmentDef
'**********************************************************


Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CSegmentDef
Private m_oListSource As XArray
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CSegmentDef
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where index = lID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       get item where name = vID
        xCrit = "fldID = " & lID
            
'       attempt to find the item
        m_oRS.FindFirst xCrit
        If m_oRS.NoMatch Then
'           if no match found, the
'           member does not exist
'            Err.Raise mpError_InvalidMember
            Exit Function
        Else
'           item found - update template object
            UpdateObject
        End If
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.Item"
End Function

Public Function ItemAtIndex(iIndex As Integer) As mpDB.CSegmentDef
    On Error GoTo ProcError
    
'   get item where index = lID
    If (iIndex < 0) Or iIndex > Count - 1 Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       move the current rec to specified position
        On Error Resume Next
        m_oRS.AbsolutePosition = iIndex
        If Err.Number Then
            On Error GoTo ProcError
'           member does not exist
            Err.Raise mpError_InvalidMember
        Else
            On Error GoTo ProcError
'           item found - update template object
            UpdateObject
        End If
    End If
    Set ItemAtIndex = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.ItemAtIndex"
End Function

Public Function Refresh(ByVal xTemplate As String, Optional ByVal lGroup As Long)
'refreshes segments collection for specified template-
'optinal group id filters set by segment group as well

    If lGroup Then
        RefreshByGroup xTemplate, lGroup
    Else
        RefreshByTemplate xTemplate
    End If
    
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.Refresh"
End Function

Private Function RefreshByGroup(ByVal xTemplate As String, ByVal lGroup As Long)
    Dim xSQL As String
    
    On Error GoTo ProcError
'************************************************************
'multitype:
    Dim oRS As DAO.Recordset
    Dim iNumSegs As Integer
    
    DebugOutput "RefreshByGroup"
    
    xSQL = "SELECT COUNT(fldDestTemplate) FROM tblSegments " & _
           "WHERE fldDestTemplate='" & xTemplate & "'"
    Set oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenForwardOnly, dbReadOnly)
    iNumSegs = oRS.Fields(0)
    If iNumSegs = 0 Then
        With Application.TemplateDefinitions
            .Group = 0
            .Refresh
            xTemplate = .Item(xTemplate).BaseTemplate
        End With
    End If
    
'************************************************************

'   get segments allowed in specified template
    xSQL = "SELECT tblSegments.* FROM tblSegments INNER JOIN tblSegmentAssignments " & _
                "ON tblSegments.fldID = tblSegmentAssignments.fldSegmentID " & _
           "WHERE (tblSegments.fldDestTemplate='" & xTemplate & _
                "' OR tblSegments.fldDestTemplate='- All Templates -') " & _
                " AND tblSegmentAssignments.fldGroupID = " & lGroup
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        Else
'           get segments allowed in for all unspecified templates (ie default group)
            xSQL = "SELECT tblSegmentTypes.* " & _
                   "FROM tblSegmentTypes INNER JOIN tblSegmentAssignments " & _
                   "ON tblSegmentTypes.fldID = tblSegmentAssignments.fldSegmentID " & _
                   "WHERE tblSegmentAssignments.fldTemplateID='- Default Group -' " & _
                   "UNION " & _
                   "SELECT tblSegments.* FROM tblSegments " & _
                   "WHERE tblSegments.fldDestTemplate='- All Templates -'"

'           open the rs
            Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
            With m_oRS
                If .RecordCount Then
                    .MoveLast
                    .MoveFirst
                End If
            End With
        End If
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.Refresh"
End Function

Private Function RefreshByTemplate(ByVal xTemplate As String)
    Dim xSQL As String

    On Error GoTo ProcError
'************************************************************
'multitype:
    Dim oRS As DAO.Recordset
    Dim iNumSegs As Integer

    DebugOutput "RefreshByTemplate"

    xSQL = "SELECT COUNT(fldDestTemplate) FROM tblSegments " & _
           "WHERE fldDestTemplate='" & xTemplate & "'"
    Set oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenForwardOnly, dbReadOnly)
    iNumSegs = oRS.Fields(0)
    If iNumSegs = 0 Then
        With Application.TemplateDefinitions
            If Not .Exists(xTemplate) Then
'               template not in tblTemplates
                GoTo NonMPTemplate
            Else
                .Group = 0
                xTemplate = .Item(xTemplate).BaseTemplate
            End If
        End With
    End If

'************************************************************

'   get segments allowed in specified template
    xSQL = "SELECT tblSegments.* FROM tblSegments " & _
           "WHERE tblSegments.fldDestTemplate='" & xTemplate & "' " & _
           "UNION " & _
           "SELECT tblSegments.* FROM tblSegments " & _
           "WHERE tblSegments.fldDestTemplate='- All Templates -'"

'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        Else
'           get segments allowed in for all unspecified templates (ie default group)
'            xSQL = "SELECT tblSegments.* " & _
'                   "FROM tblSegments INNER JOIN tblSegmentAssignments " & _
'                   "ON tblSegments.fldID = tblSegmentAssignments.fldSegmentID " & _
'                   "WHERE tblSegmentAssignments.fldTemplateID='- Default Group -' " & _
'                   "UNION " & _
                   "SELECT tblSegments.* FROM tblSegments " & _
                   "WHERE tblSegments.fldDestTemplate='- All Templates -'"
NonMPTemplate:
            xSQL = "SELECT tblSegments.* FROM tblSegments WHERE tblSegments.fldDestTemplate='- All Templates -'"

'           open the rs
            Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
            With m_oRS
                If .RecordCount Then
                    .MoveLast
                    .MoveFirst
                End If
            End With
        End If
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.Refresh"
End Function

Public Property Get ListSource() As XArray
    If (m_oListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oListSource
End Property

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oType
        On Error Resume Next
        .AllowAtCursor = m_oRS!fldAllowAtCursor
        .AllowAtEndOfDocument = m_oRS!fldAllowEndOfDoc
        .AllowExistingHeaderFooters = m_oRS!fldAllowExistingHeadFoots
        .AllowInCursorSection = m_oRS!fldAllowInCursorSection
        .AllowInEndSection = m_oRS!fldAllowInEndSection
        .AllowInStartSection = m_oRS!fldAllowInStartSection
        .AllowAtStartOfDocument = m_oRS!fldAllowStartOfDoc
        .Boilerplate = m_oRS!fldBoilerplateName
        .ID = m_oRS!fldID
        .Name = m_oRS!fldName
        .SourceTemplate = m_oRS!fldSourceTemplate
        .DestTemplate = m_oRS!fldDestTemplate
        .MappedVariables = GetSegmentMappings(.ID)
        .Description = m_oRS!fldDescription
        .BaseStyle = m_oRS!fldBaseStyle
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oType = New mpDB.CSegmentDef
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xName As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            Set m_oListSource = New XArray
            m_oListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xName = !fldName
                m_oListSource.Value(i, 1) = xID
                m_oListSource.Value(i, 0) = xName
            
            Next i
        Else
            Set m_oListSource = New XArray
            m_oListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function GetSegmentMappings(ByVal lID As Long) As String()
'returns an array with variable mapping info
    Dim xSQL As String
    Dim aVars() As String
    Dim xField As String
    Dim xVar As String
    Dim i As Integer
    Dim oRS As DAO.Recordset
    
    ReDim aVars(0, 1)
    
    On Error GoTo ProcError
    xSQL = "SELECT fldPrefillVar, fldPrefillValueSource FROM tblSegmentMappings " & _
           "WHERE fldSegment = " & lID
        
'   open the rs
    Set oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
'           fill array with mapping info
            ReDim aVars(.RecordCount - 1, 1)
            While Not .EOF
                On Error Resume Next
                xField = oRS!fldPrefillVar
                xVar = oRS!fldPrefillValueSource
                On Error GoTo ProcError
                aVars(i, 0) = xField
                aVars(i, 1) = xVar
                i = i + 1
                .MoveNext
            Wend
        End If
    End With
    GetSegmentMappings = aVars
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.GetSegmentMappings"
    Exit Function
End Function

Public Sub Filter(ByVal xTemplate As String, ByVal xText As String, _
                Optional ByVal lGroup As Long, _
                Optional ByVal bSearchName As Boolean = True, _
                Optional ByVal bSearchDescription As Boolean = False)
    Dim xSQL As String

    On Error GoTo ProcError

'   get templates whose destination is either the
'   current template or all templates and is in the
'   specified group
    xSQL = "SELECT DISTINCT tblSegments.* " & _
           "FROM tblSegments RIGHT JOIN tblSegmentAssignments " & _
           "ON tblSegments.fldID = tblSegmentAssignments.fldSegmentID " & _
           "WHERE (tblSegments.fldDestTemplate = '" & _
           xTemplate & "' OR tblSegments.fldDestTemplate = '- All Templates -') "

    If Len(xText) And (bSearchName Or bSearchDescription) Then
        xSQL = xSQL & "AND "
        If bSearchName Then
            xSQL = xSQL & "(fldName LIKE ""*" & xText & "*"" "
            If bSearchDescription Then
                xSQL = xSQL & "OR fldName LIKE ""*" & xText & "*"" "
            End If
            xSQL = xSQL & ")"
        ElseIf bSearchDescription Then
            xSQL = xSQL & "fldDescription LIKE ""*" & xText & "*"" "
        End If
    End If

    If lGroup Then
'       limit to all templates in group
        xSQL = xSQL & "AND tblSegmentAssignments.fldGroupID = " & lGroup
    End If

'   open the rs
    Set m_oRS = Application.PublicDB _
        .OpenRecordset(xSQL, dbOpenSnapshot)

    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CTemplateDefs.Refresh"
    Exit Sub
End Sub




