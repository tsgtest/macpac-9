VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDepoSummaryDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Depo Summary Definitions Collection Class
'   created 5/16/00 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Depo defs - collection is a recordset, though
'   an XArray belongs to class for display purposes

'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_ID = 0
    xarCol_Description = 1
    xarCol_BoilerplateFile = 2
End Enum

Private m_oRS As DAO.Recordset
Private m_oDef As mpDB.CDepoSummaryDef
Private m_oArray As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************

Public Property Let ListSource(xarNew As XArray)
    Set m_oArray = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshListArray
    End If
    Set ListSource = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh()
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    xSQL = "SELECT  * FROM tblDepoSummaryTypes ORDER BY fldID"
    
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                    dbOpenSnapshot, dbReadOnly)
    
'   populate if depos exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If Not (m_oArray Is Nothing) Then
        RefreshListArray
    End If
End Sub

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(lID As Long) As mpDB.CDepoSummaryDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim oDef As mpDB.CDepoSummaryDef
    
    On Error GoTo ProcError
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Err.Raise mpError_InvalidDepoFormatType
        Else
'           found - update current object
            UpdateObject oDef
            
'           return current object
            Set Item = oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.DepoSummaryDefinition.Item"
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDef = New mpDB.CDepoSummaryDef
    Set m_oArray = New XArrayObject.XArray
    m_oArray.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
    Set m_oDef = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshListArray()
'fills array xArray with records
'in recordset rExisting

    Dim lRows As Long
    Dim i As Integer
    Dim xID As String
    Dim xDesc As String
    Dim vBkmk As Variant
    
    With m_oRS
        lRows = .RecordCount
    
        If lRows > 0 Then
            vBkmk = .Bookmark
            .MoveLast
            .MoveFirst
            lRows = .RecordCount
            m_oArray.ReDim 0, lRows - 1, 0, 2
            While Not .EOF
                xID = Empty
                xDesc = Empty
                
                On Error Resume Next
                xID = !fldID
                xDesc = !fldDescription
                
                m_oArray.Value(i, 0) = xID
                m_oArray.Value(i, 1) = xDesc
                .MoveNext
                i = i + 1
            Wend
            .Bookmark = vBkmk
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject(oDef As mpDB.CDepoSummaryDef)
    Set oDef = New mpDB.CDepoSummaryDef
    With oDef
        .ID = Empty
        .Description = Empty
        .BoilerplateFile = Empty
    End With
    
    With m_oRS
        On Error Resume Next
        oDef.ID = !fldID
        oDef.Description = !fldDescription
        oDef.BoilerplateFile = !fldBoilerplateFile
    End With
End Function



