VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCIFormats"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CCIFormats Collection Class
'   created 06/15/00 by Jeffrey Sweetland

'   Contains properties and methods that manage the
'   collection of CI Formats

'   Member of
'   Container for CCIFormat
'**********************************************************

'**********************************************************
Private m_oCIFormat As CCIFormat
Private m_oRS As DAO.Recordset
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Add() As CCIFormat
    On Error GoTo ProcError
    Set m_oCIFormat = New CCIFormat
    m_oCIFormat.IsNew = True
    Set Add = m_oCIFormat
    Exit Function
ProcError:
    RaiseError "mpDB.CCIFormats.Add", Err.Description
    Exit Function
End Function

Public Function Exists(ByVal lID As Long) As Boolean
'returns TRUE if the specified custom format already exists
    Dim xSQL As String
    
    xSQL = "SELECT fldID FROM tblCIFormats WHERE fldID=" & lID
            
    Exists = Application.PublicDB.OpenRecordset(xSQL).RecordCount > 0
    
    Exit Function
ProcError:
    RaiseError "mpDB.CCIFormats.Exists", Err.Description
    Exit Function
End Function

Public Sub Save(oCIFormat As CCIFormat)
    Dim xSQL As String
    
    On Error GoTo ProcError
    If oCIFormat Is Nothing Then
        Exit Sub
    End If
    
    With oCIFormat
'       validate
        If .IsNew And Me.Exists(.ID) Then
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save the control definition.  A control for a custom property with ID=" & _
                    .ID & " already exists."
        ElseIf .IncludeBCC + .IncludeCC + .IncludeTo + .IncludeFrom = 0 Then
            'one 'Include' is required
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save the control definition.  At least one 'Include' property must be set to True."
        ElseIf .Description = Empty Then
            'description is required
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save the control definition.  Description is required information."
        ElseIf .DefaultList = Empty Then
            'description is required
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save the control definition.  Default List is required information."
        End If
        
        If .IsNew Then
'           insert
            xSQL = "INSERT INTO tblCIFormats(fldDescription,fldIncludeTo,fldIncludeFrom," & _
                "fldIncludeCC,fldIncludeBCC,fldIncludePhone,fldPhoneLabel,fldIncludeFax, " & _
                "fldFaxLabel,fldIncludeEmail,fldEmailLabel,fldOnEmptyAction,fldPromptForPhones, " & _
                "fldDefaultList,fldToNamesOnly,fldFromNamesOnly,fldCCNamesOnly,fldBCCNamesOnly) " & _
                "VALUES (""" & _
                .Description & """," & _
                .IncludeTo & "," & _
                .IncludeFrom & "," & _
                .IncludeCC & "," & _
                .IncludeBCC & "," & _
                .IncludePhone & ",""" & _
                .PhoneLabel & """," & _
                .IncludeFax & ",""" & _
                .FaxLabel & """," & _
                .IncludeEMail & ",""" & _
                .EmailLabel & """," & _
                .OnEmptyAction & "," & _
                .PromptForPhones & "," & _
                .DefaultList & "," & _
                .ToNamesOnly & "," & _
                .FromNamesOnly & "," & _
                .CCNamesOnly & "," & _
                .BCCNamesOnly & ")"
        Else
'           save
            xSQL = "UPDATE tblCIFormats SET fldDescription=""" & _
                .Description & _
                """,fldIncludeTo=" & .IncludeTo & _
                ",fldIncludeFrom=" & .IncludeFrom & _
                ",fldIncludeCC=" & .IncludeCC & _
                ",fldIncludeBCC=" & .IncludeBCC & _
                ",fldIncludePhone=" & .IncludePhone & _
                ",fldPhoneLabel=""" & .PhoneLabel & _
                """,fldIncludeFax=" & .IncludeFax & _
                ",fldFaxLabel=""" & .FaxLabel & _
                """,fldIncludeEmail=" & .IncludeEMail & _
                ",fldEmailLabel=""" & .EmailLabel & _
                """,fldOnEmptyAction=" & .OnEmptyAction & _
                ",fldPromptForPhones=" & .PromptForPhones & _
                ",fldDefaultList=" & .DefaultList & _
                ",fldToNamesOnly=" & .ToNamesOnly & _
                ",fldFromNamesOnly=" & .FromNamesOnly & _
                ",fldCCNamesOnly=" & .CCNamesOnly & _
                ",fldBCCNamesOnly=" & .BCCNamesOnly & _
                " WHERE fldID=" & oCIFormat.ID
        End If
    End With
    
    With Application.PublicDB
        'execute query
        .Execute xSQL
        If .RecordsAffected = 0 Then
            'add, update failed - raise error
            Err.Raise 600, , _
                "Could not save the CI Format.  Execution of the sql statement failed."
        Else
            oCIFormat.IsNew = False
            Refresh
        End If
    End With
    
   Exit Sub
ProcError:
    RaiseError "mpDB.CCIFormats.Save", Err.Description
    Exit Sub
End Sub

Public Sub Delete(ByVal lID As Long)
    Dim oItem As CCIFormat
    
    On Error Resume Next
    Set oItem = Item(lID)
    On Error GoTo ProcError
    
    If Not oItem Is Nothing Then
        Dim xSQL As String
        
        'get delete statement
        xSQL = "DELETE FROM tblCIFormats WHERE fldID=" & lID
            
        With Application.PublicDB
            'do deletion
            .Execute xSQL
            
            'raise error if no records were deleted
            If .RecordsAffected = 0 Then
                Err.Raise 600, , _
                    "Could not delete the CI Format with id=" & CStr(lID) & "."
            End If
        End With
    Else
        'invalid CI Format - raise error
        Err.Raise mpError.mpError_InvalidParameter, , _
            "No CI Format with id=" & CStr(lID) & " exists."
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.CCIFormats.Delete", Err.Description
    Exit Sub
End Sub

Public Function Count() As Integer
    Count = m_oRS.RecordCount
End Function

Public Function Item(ByVal lID As Long) As CCIFormat
    Dim i As Integer
    Dim xCrit As String
    
    On Error GoTo ProcError
    
    With m_oRS
'       get item where name = vID
        xCrit = "fldID = " & lID
        
'       attempt to find the item
        .FindFirst xCrit
        If .NoMatch Then
'           if no match found, the member does not exist
            Err.Raise mpError_InvalidMember, , lID & " is not the ID of a valid CIFormat.  " & _
                    "Please check tblCIFormats in mpPublic.mdb."
        Else
'           item found - update CIFormat object
            UpdateObject
        End If
    End With
    
'   return CIFormat object
    m_oCIFormat.IsNew = False
    Set Item = m_oCIFormat
    
    Exit Function
ProcError:
    RaiseError "mpDB.CCIFormats.Item", Err.Description
    Exit Function
End Function

Public Function ItemAt(ByVal iIndex As Integer) As CCIFormat
    Dim i As Integer
    
    On Error GoTo ProcError
    
    i = iIndex - 1
    
    With m_oRS
        On Error Resume Next
        .AbsolutePosition = i
        If Err Then
            'return nothing
            Exit Function
        Else
'           item found - update CIFormat object
            UpdateObject
        End If
    End With
    
'   return CIFormat object
    m_oCIFormat.IsNew = False
    Set ItemAt = m_oCIFormat
    
    Exit Function
ProcError:
    RaiseError "mpDB.CCIFormats.Item", Err.Description
    Exit Function
End Function

Public Sub Refresh()
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get CIFormats
    xSQL = "SELECT * FROM tblCIFormats"
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CCIFormats.Refresh", Err.Description
    Exit Sub
End Sub


'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oCIFormat = New mpDB.CCIFormat
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
End Sub

Private Sub UpdateObject()
    With m_oCIFormat
        .ID = Empty
        .Description = Empty
        .IncludeTo = Empty
        .IncludeFrom = Empty
        .IncludeCC = Empty
        .IncludeBCC = Empty
        .IncludePhone = Empty
        .IncludeFax = Empty
        .IncludeEMail = Empty
        .DefaultList = Empty
        .OnEmptyAction = Empty
        .PromptForPhones = Empty
        .ToNamesOnly = Empty
        .FromNamesOnly = Empty
        .CCNamesOnly = Empty
        .BCCNamesOnly = Empty
        .PhoneLabel = Empty
        .FaxLabel = Empty
        .EmailLabel = Empty
        
        On Error Resume Next
        .ID = m_oRS!fldID
        .Description = m_oRS!fldDescription
        .IncludeTo = m_oRS!fldIncludeTo
        .IncludeFrom = m_oRS!fldIncludeFrom
        .IncludeCC = m_oRS!fldIncludeCC
        .IncludeBCC = m_oRS!fldIncludeBCC
        .IncludePhone = m_oRS!fldIncludePhone
        .IncludeFax = m_oRS!fldIncludeFax
        .IncludeEMail = m_oRS!fldIncludeEMail
        .DefaultList = m_oRS!fldDefaultList
        .OnEmptyAction = m_oRS!fldOnEmptyAction
        .PromptForPhones = m_oRS!fldPromptForPhones
        .ToNamesOnly = m_oRS!fldToNamesOnly
        .FromNamesOnly = m_oRS!fldFromNamesOnly
        .CCNamesOnly = m_oRS!fldCCNamesOnly
        .BCCNamesOnly = m_oRS!fldBCCNamesOnly
        .PhoneLabel = m_oRS!fldPhoneLabel
        .FaxLabel = m_oRS!fldFaxLabel
        .EmailLabel = m_oRS!fldEmailLabel
    End With
End Sub
