VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomDialogDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CCustomDialogs Collection Class
'   created 2/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of CCustomCustomDialogs

'   Member of
'   Container for
'**********************************************************

'**********************************************************
Private m_oCustomDialog As CCustomDialogDef
Private m_oRS As DAO.Recordset
Private m_xTemplate As String
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Add(ByVal xTemplate As String) As CCustomDialogDef
    On Error GoTo ProcError
    Set m_oCustomDialog = New CCustomDialogDef
    
    If xTemplate = Empty Then
        Err.Raise mpError.mpError_InvalidParameter, , _
            "Missing template specified for new Custom Dialog."
    ElseIf Not Application.TemplateDefinitions.Exists(xTemplate) Then
        Err.Raise mpError.mpError_InvalidTemplateName, , _
            "'" & xTemplate & "' is an invalid template."
    ElseIf Application.CustomDialogDefs.Exists(xTemplate) Then
        'custom dialog def already exists - can't add
        Err.Raise mpError.mpError_InvalidTemplateName, , _
            "'" & xTemplate & "' already has a dialog defined."
    End If
    
    'used by save method
    m_xTemplate = xTemplate
    
    m_oCustomDialog.IsNew = True
    Set Add = m_oCustomDialog
    Exit Function
ProcError:
    RaiseError "mpDB.CCustomDialogDefs.Add", Err.Description
    Exit Function
End Function

Public Sub Save(oDef As CCustomDialogDef)
    Dim xSQL As String
    On Error GoTo ProcError
    
    If oDef Is Nothing Then
        Err.Raise mpError.mpError_InvalidParameter, , _
            "Could not save custom dialog definition.  " & _
            "The supplied variable is 'Nothing'."
    End If
    
    With oDef
'       validate
        If .Caption = Empty Then
            'failed validation
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save custom dialog definition.  " & _
                "'Caption' is  required data."
        ElseIf .IsNew And Me.Exists(m_xTemplate) Then
            'custom dialog definition already exists
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save custom dialog definition.  A custom dialog for '" & _
                    m_xTemplate & "' has already been defined."
        End If
        
        If .IsNew Then
            'use default values for height and width to start with
            xSQL = "INSERT INTO tblCustomDialogs(fldTemplate," & _
                "fldCaption," & _
                IIf(.Tab1 <> Empty, "fldTab1,", "") & _
                IIf(.Tab2 <> Empty, "fldTab2,", "") & _
                IIf(.Tab3 <> Empty, "fldTab3,", "") & _
                "fldShowAuthorControl,fldShowLetterheadTab,fldCIFormat) VALUES " & _
                "(""" & m_xTemplate & """," & _
                IIf(.Caption <> Empty, """" & .Caption & """,", "") & _
                IIf(.Tab1 <> Empty, """" & .Tab1 & """,", "") & _
                IIf(.Tab2 <> Empty, """" & .Tab2 & """,", "") & _
                IIf(.Tab3 <> Empty, """" & .Tab3 & """,", "") & _
                .ShowAuthorControl & "," & _
                .ShowLetterheadTab & "," & _
                .CIFormat & ")"
        Else
            xSQL = "UPDATE tblCustomDialogs SET " & _
                IIf(.Caption <> Empty, "fldCaption=""" & .Caption & """,", "") & _
                IIf(.Tab1 <> Empty, "fldTab1=""" & .Tab1 & """,", "") & _
                IIf(.Tab2 <> Empty, "fldTab2=""" & .Tab2 & """,", "") & _
                IIf(.Tab3 <> Empty, "fldTab3=""" & .Tab3 & """,", "") & _
                "fldShowAuthorControl=" & .ShowAuthorControl & "," & _
                "fldShowLetterheadTab=" & .ShowLetterheadTab & "," & _
                "fldCIFormat=" & .CIFormat & "," & _
                "fldHeight=" & .Height & "," & _
                "fldWidth=" & .Width & _
                " WHERE fldTemplate=""" & m_xTemplate & """"

        End If
    End With
    
    With Application.PublicDB
        'execute query
        .Execute xSQL
        If .RecordsAffected = 0 Then
            'add, update failed - raise error
            Err.Raise 600, , "Could not save the custom dialog for '" & m_xTemplate & "'."
        Else
            oDef.IsNew = False
            Me.Refresh
        End If
    End With
    
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomDialogDefs.Save"
    Exit Sub
End Sub

Public Sub Delete(ByVal xTemplate As String)
    Dim oItem As CCustomDialogDef
    
    On Error Resume Next
    Set oItem = Item(xTemplate)
    On Error GoTo ProcError
    
    If Not oItem Is Nothing Then
        Dim xSQL As String
        
        'get delete statement for given template
        xSQL = "DELETE FROM tblCustomDialogs WHERE " & _
            "fldTemplate='" & xTemplate & "'"
            
        With Application.PublicDB
            'do deletion
            .Execute xSQL
            
            'raise error if no records were deleted
            If .RecordsAffected = 0 Then
                Err.Raise mpError.mpError_ItemDoesNotExist, , _
                    "Could not delete the custom dialog for template '" & xTemplate & "'."
            End If
        End With
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomDialogDefs.Delete", Err.Description
    Exit Sub
End Sub
Public Function Count() As Integer
    Count = m_oRS.RecordCount
End Function

Public Function Item(ByVal vID As Variant) As CCustomDialogDef
    Dim i As Integer
    Dim xCrit As String
    
    On Error GoTo ProcError
    
    With m_oRS
        If IsNumeric(vID) Then
'           get item where index = vID
            If (vID <= 0) Or (vID > .RecordCount) Then
                Err.Raise mpError_InvalidItemIndex
            Else
                .AbsolutePosition = vID - 1
                UpdateObject
            End If
        Else
'           get item where name = vID
            xCrit = "fldTemplate = '" & vID & "'"
            
'           attempt to find the item
            .FindFirst xCrit
            If .NoMatch Then
'               if no match found, the
'               member does not exist
                Err.Raise mpError_InvalidItemIndex
            Else
'               item found - update CustomDialog object
                UpdateObject
            End If
        End If
    End With
    
'   return CustomDialog object
    m_oCustomDialog.IsNew = False
    Set Item = m_oCustomDialog
    Exit Function
ProcError:
    RaiseError "mpDB.CCustomDialogDefs.Item"
    Exit Function
End Function

Public Sub Refresh()
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get CustomDialogs
    xSQL = "SELECT * FROM tblCustomDialogs"
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomDialogDefs.Refresh"
    Exit Sub
End Sub

Public Function Exists(ByVal xTemplateID As String) As Boolean
    Dim xSQL As String
    
    xSQL = "SELECT fldTemplate FROM tblCustomDialogs WHERE " & _
           "fldTemplate ='" & xTemplateID & "'"
            
    Exists = Application.PublicDB.OpenRecordset(xSQL).RecordCount > 0
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oCustomDialog = New mpDB.CCustomDialogDef
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
End Sub

Private Sub UpdateObject()
    With m_oCustomDialog
        .Form = Empty
        .Caption = Empty
        .Tab1 = Empty
        .Tab2 = Empty
        .Tab3 = Empty
        .ShowAuthorControl = Empty
        .ShowLetterheadTab = Empty
        .Height = Empty
        .Width = Empty
        .CIFormat = Empty
        
        On Error Resume Next
        .Caption = m_oRS!fldCaption
        .Tab1 = m_oRS!fldTab1
        .Tab2 = m_oRS!fldTab2
        .Tab3 = m_oRS!fldTab3
        .Form = m_oRS!fldForm
        .Height = m_oRS!fldHeight
        .Width = m_oRS!fldWidth
        .ShowAuthorControl = m_oRS!fldShowAuthorControl
        .ShowLetterheadTab = m_oRS!fldShowLetterheadTab
        .CIFormat = m_oRS!fldCIFormat
        
        'used by save method
        m_xTemplate = m_oRS!fldTemplate
    End With
End Sub

Private Sub UpdateRecordset()
    On Error GoTo ProcError
    With m_oCustomDialog
        If .IsNew Then
            If m_xTemplate = Empty Then
                Err.Raise mpError.mpError_InvalidParameter, , _
                    "Missing or invalid template specified for new Custom Dialog."
            Else
                m_oRS!fldTemplate = m_xTemplate
            End If
        End If
        
        m_oRS!fldCaption = .Caption
        m_oRS!fldTab1 = .Tab1
        m_oRS!fldTab2 = .Tab2
        m_oRS!fldTab3 = .Tab3
        
        'don't set fldForm until we offer other form types
        'm_oRS!fldForm = .Form
        
        m_oRS!fldHeight = .Height
        m_oRS!fldWidth = .Width
        m_oRS!fldShowAuthorControl = .ShowAuthorControl
        m_oRS!fldShowLetterheadTab = .ShowLetterheadTab
        m_oRS!fldCIFormat = .CIFormat
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomDialogDefs.UpdateRecordset"
    Exit Sub
End Sub



