VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingFavorite"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingType Class
'   created 1/20/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a Pleading Favorite

'   Member of CPleadingFavorites
'**********************************************************

Private m_lID As Long
Private m_lLevel0 As Long
Private m_lLevel1 As Long
Private m_lLevel2 As Long
Private m_lLevel3 As Long
Private m_xDesc As String

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Level0(lNew As Long)
    m_lLevel0 = lNew
End Property

Public Property Get Level0() As Long
    Level0 = m_lLevel0
End Property

Public Property Let Level1(lNew As Long)
    m_lLevel1 = lNew
End Property

Public Property Get Level1() As Long
    Level1 = m_lLevel1
End Property

Public Property Let Level2(lNew As Long)
    m_lLevel2 = lNew
End Property

Public Property Get Level2() As Long
    Level2 = m_lLevel2
End Property

Public Property Let Level3(lNew As Long)
    m_lLevel3 = lNew
End Property

Public Property Get Level3() As Long
    Level3 = m_lLevel3
End Property

Public Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property
