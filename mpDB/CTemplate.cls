VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplate Class
'   created 11/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac Template

'   Member of CTemplates
'   Container for
'**********************************************************
Public Enum mpTemplateTypes
    mpTemplateType_MacPac = 1
    mpTemplateType_Workgroup = 2
    mpTemplateType_User = 3
End Enum

Private m_lID As Long
Private m_xName As String
Private m_xFile As String
Private m_iType As mpTemplateTypes
Private m_lGroup As Long
Private m_oCustProps As CTemplatePropertyDefs

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let FileName(xNew As String)
    m_xFile = xNew
End Property

Public Property Get FileName() As String
    FileName = m_xFile
End Property

Public Property Get Directory() As String
'returns the path to the template -
'based on template type
    Dim xPath As String
    
    On Error GoTo ProcError
    
    Select Case TemplateType
        Case mpTemplateType_MacPac
            xPath = GetIni("General", _
                           "TemplatesDir", _
                           App.Path & "\MacPac.ini")
            
'           raise error if path is not valid
            If Dir(xPath, vbDirectory) = "" Then
                Err.Raise mpError_InvalidMacPacTemplatesPath
            End If
            
            If Right(xPath, 1) = "\" Then
                xPath = Left(xPath, Len(xPath) - 1)
            End If
            Directory = xPath
        Case mpTemplateType_Workgroup
            Directory = Word.Application.Options _
                .DefaultFilePath(wdWorkgroupTemplatesPath)
'           raise error if path is not valid
            If Dir(Directory, vbDirectory) = "" Then
                Err.Raise mpError_InvalidWorkgroupTemplatesPath
            End If
            Directory = xPath
        Case mpTemplateType_User
            Directory = Word.Application.Options _
                .DefaultFilePath(wdUserTemplatesPath)
'           raise error if path is not valid
            If Dir(xPath, vbDirectory) = "" Then
                Err.Raise mpError_InvalidUserTemplatesPath
            End If
            Directory = xPath
    End Select
    Exit Property
ProcError:
    Err.Raise Err.Number, "mpDB.CTemplate.Directory", Application.Error.Desc(Err.Number)
End Property

Public Property Let TemplateType(iNew As mpTemplateTypes)
    m_iType = iNew
End Property

Public Property Get TemplateType() As mpTemplateTypes
    TemplateType = m_iType
End Property

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Group(lNew As Long)
    m_lGroup = lNew
End Property

Public Property Get Group() As Long
    Group = m_lGroup
End Property

Public Property Get FullName() As String
'returns the file path and name of template
    FullName = Me.Directory & "\" & Me.FileName
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function CustomProperties() As CTemplatePropertyDefs
    If m_oCustProps Is Nothing Then
        Set m_oCustProps = New CTemplatePropertyDefs
    End If
End Function

Public Function Exists() As Boolean
    Exists = (Dir(Me.FullName) <> "")
End Function

Public Sub Retrieve()
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If Me.Exists Then
        Word.Documents.Open Me.FullName, , , False
    Else
        Err.Raise mpError_TemplateNotFound
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplate.Retrieve", _
              Application.Error.Desc(Err.Number)
    Exit Sub
End Sub

Public Sub CreateNewDocument()
'creates a new document based on the Template
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If Me.Exists Then
        Word.Documents.Add Me.FullName, False
    Else
        Err.Raise mpError_TemplateNotFound
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplate.CreateNewDocument", _
              Application.Error.Desc(Err.Number)
    Exit Sub
End Sub

Public Sub CreateNewTemplate()
'creates a new template based on the Template
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If Me.Exists Then
        Word.Documents.Add Me.FullName, True, , False
    Else
        Err.Raise mpError_TemplateNotFound
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplate.CreateNewTemplate", _
              Application.Error.Desc(Err.Number)
    Exit Sub
End Sub

Private Sub Class_Terminate()
    Set m_oCustProps = Nothing
End Sub
