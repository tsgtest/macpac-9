VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CListItems"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CListItems Collection Class
'   created 11/29/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CListItems - list items collection is an XArray

'   Member of CList
'   Container for CListItems
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_DisplayText = 0
    xarCol_ID = 1
    xarCol_SuppVal = 2
End Enum

Private m_xarLItems As XArrayObject.XArray
Private m_dbP
Private m_mpNumbers As New mpBase2.CNumbers
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Source(xarNew As XArray)
    Set m_xarLItems = xarNew
End Property

Public Property Get Source() As XArray
    Set Source = m_xarLItems
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Add(vID As Variant, xDisplayText As String, vSuppVal As Variant)
    Dim lUBound As Long
    
    With m_xarLItems
        lUBound = m_xarLItems.UpperBound(1)
        .Insert 1, lUBound + 1
        .Value(lUBound + 1, xarCol_DisplayText) = xDisplayText
        .Value(lUBound + 1, xarCol_ID) = vID
        .Value(lUBound + 1, xarCol_SuppVal) = vSuppVal
    End With
End Sub

Friend Sub Delete(lIndex As Long)
    m_xarLItems.Delete 1, lIndex
End Sub

Friend Sub DeleteAll()
    m_xarLItems.Clear
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = m_xarLItems.Count(1)
End Function

Public Function Item(Optional lIndex As Long, Optional vID As Variant) As CListItem
    Dim liP As CListItem
    Dim i As Integer

    On Error GoTo ProcError

    If IsMissing(vID) Then
        Set liP = New CListItem
        lIndex = Max(CDbl(lIndex), 0)
        liP.DisplayText = m_xarLItems.Value(lIndex, xarCol_DisplayText)
        liP.ID = m_xarLItems.Value(lIndex, xarCol_ID)
        liP.SuppValue = m_xarLItems.Value(lIndex, xarCol_SuppVal)
    Else
        For i = 0 To m_xarLItems.UpperBound(1) ' - 1
            If m_xarLItems.Value(i, xarCol_ID) = vID Then
                Set liP = New CListItem
                liP.DisplayText = m_xarLItems.Value(i, xarCol_DisplayText)
                liP.ID = m_xarLItems.Value(i, xarCol_ID)
                On Error Resume Next
                liP.SuppValue = m_xarLItems.Value(i, xarCol_SuppVal)
                On Error GoTo ProcError
                Exit For
            End If
        Next i
    End If

    Set Item = liP
    Exit Function
ProcError:
    Set liP = Nothing
    Err.Raise Err.Number, "mpDB.CListItems.Item"
End Function


'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_xarLItems = New XArrayObject.XArray
    m_xarLItems.ReDim 0, -1, 0, 2
End Sub

Private Sub Class_Terminate()
    Set m_xarLItems = Nothing
End Sub

