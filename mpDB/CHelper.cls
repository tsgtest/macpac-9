VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'---exposes internal functionality

Function TableToXArray(xDB As String, xTableName As String, _
    Optional xWhere As Variant, Optional xSort As Variant)
    
    Dim oDB As DAO.Database
    
    If InStr(xDB, "Public") <> 0 Then
        Set oDB = Application.PublicDB
    Else
        Set oDB = Application.PrivateDB
    End If
    
    Set TableToXArray = Application.xarGetTableXArray(oDB, xTableName, xWhere, xSort)

End Function

