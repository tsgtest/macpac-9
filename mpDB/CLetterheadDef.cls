VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLetterheadDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Letterhead Def Class
'   created 12/22/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define macpac letterhead

'   Member of
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xDesc As String
Private m_xBP As String
Private m_bUsesPage1Header As Boolean
Private m_bUsesPrimaryHeader As Boolean
Private m_bUsesPage1Footer As Boolean
Private m_bUsesPrimaryFooter As Boolean
Private m_sTopMargin As Single
Private m_sBottomMargin As Single
Private m_sLeftMargin As Single
Private m_sRightMargin As Single
Private m_sHeaderDistance As Single
Private m_sFooterDistance As Single
Private m_sHeaderSpacerHeight As Single
Private m_lTemplateToAttach As Long
Private m_bAllowAuthorEmail As Boolean
Private m_bAllowAuthorPhone As Boolean
Private m_bAllowAuthorName As Boolean
Private m_bAllowAuthorFax As Boolean
Private m_bAllowAuthorTitle As Boolean
Private m_bAllowAuthorAdmittedIn As Boolean
Private m_sPageHeight As Single
Private m_sPageWidth As Single
Private m_iAddressFormat As Integer
Private m_lOfficeID As Long
Private m_bAllowCustomDetail1 As String '9.7.1 #4028
Private m_bAllowCustomDetail2 As String '9.7.1 #4028
Private m_bAllowCustomDetail3 As String '9.7.1 #4028
'9.7.1 - #4202
Private m_xCustomText1 As String
Private m_xCustomText2 As String
Private m_xCustomText3 As String
Private m_bFormatFirmName As Boolean


'**********************************************************
'   Properties
'**********************************************************
Friend Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property
Public Property Get AllowCustomDetail1() As Boolean '9.7.1 #4028
    AllowCustomDetail1 = m_bAllowCustomDetail1
End Property
Friend Property Let AllowCustomDetail1(bNew As Boolean) '9.7.1 #4028
    m_bAllowCustomDetail1 = bNew
End Property
Public Property Get AllowCustomDetail2() As Boolean '9.7.1 #4028
    AllowCustomDetail2 = m_bAllowCustomDetail2
End Property
Friend Property Let AllowCustomDetail2(bNew As Boolean) '9.7.1 #4028
    m_bAllowCustomDetail2 = bNew
End Property
Public Property Get AllowCustomDetail3() As Boolean '9.7.1 #4028
    AllowCustomDetail3 = m_bAllowCustomDetail3
End Property
Friend Property Let AllowCustomDetail3(bNew As Boolean) '9.7.1 #4028
    m_bAllowCustomDetail3 = bNew
End Property
Friend Property Let OfficeID(lNew As Long)
    m_lOfficeID = lNew
End Property

Public Property Get OfficeID() As Long
    OfficeID = m_lOfficeID
End Property

Friend Property Let BoilerplateFile(xNew As String)
    m_xBP = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBP
End Property

'9.7.1 - #4202
Friend Property Let CustomText1(xNew As String)
    m_xCustomText1 = xNew
End Property
'9.7.1 - #4202
Public Property Get CustomText1() As String
    CustomText1 = m_xCustomText1
End Property
'9.7.1 - #4202
Friend Property Let CustomText2(xNew As String)
    m_xCustomText2 = xNew
End Property
'9.7.1 - #4202
Public Property Get CustomText2() As String
    CustomText2 = m_xCustomText2
End Property
'9.7.1 - #4202
Friend Property Let CustomText3(xNew As String)
    m_xCustomText3 = xNew
End Property
'9.7.1 - #4202
Public Property Get CustomText3() As String
    CustomText3 = m_xCustomText3
End Property

Friend Property Let UsesFirstPageHeader(bNew As Boolean)
    m_bUsesPage1Header = bNew
End Property

Public Property Get UsesFirstPageHeader() As Boolean
    UsesFirstPageHeader = m_bUsesPage1Header
End Property

Friend Property Let UsesPrimaryHeader(bNew As Boolean)
    m_bUsesPrimaryHeader = bNew
End Property

Public Property Get UsesPrimaryHeader() As Boolean
    UsesPrimaryHeader = m_bUsesPrimaryHeader
End Property

Friend Property Let UsesFirstPageFooter(bNew As Boolean)
    m_bUsesPage1Footer = bNew
End Property

Public Property Get UsesFirstPageFooter() As Boolean
    UsesFirstPageFooter = m_bUsesPage1Footer
End Property

Friend Property Let UsesPrimaryFooter(bNew As Boolean)
    m_bUsesPrimaryFooter = bNew
End Property

Public Property Get UsesPrimaryFooter() As Boolean
    UsesPrimaryFooter = m_bUsesPrimaryFooter
End Property

Friend Property Let TopMargin(sNew As Single)
    m_sTopMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTopMargin
End Property

Friend Property Let BottomMargin(sNew As Single)
    m_sBottomMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBottomMargin
End Property

Friend Property Let HeaderSpacerHeight(sNew As Single)
    m_sHeaderSpacerHeight = sNew
End Property

Public Property Get HeaderSpacerHeight() As Single
    HeaderSpacerHeight = m_sHeaderSpacerHeight
End Property

Friend Property Let AddressFormat(iNew As Integer)
    m_iAddressFormat = iNew
End Property

Public Property Get AddressFormat() As Integer
    AddressFormat = m_iAddressFormat
End Property

Friend Property Let LeftMargin(sNew As Single)
    m_sLeftMargin = sNew
End Property

Public Property Get LeftMargin() As Single
    LeftMargin = m_sLeftMargin
End Property

Friend Property Let RightMargin(sNew As Single)
    m_sRightMargin = sNew
End Property

Public Property Get RightMargin() As Single
    RightMargin = m_sRightMargin
End Property

Friend Property Let HeaderDistance(sNew As Single)
    m_sHeaderDistance = sNew
End Property

Public Property Get HeaderDistance() As Single
    HeaderDistance = m_sHeaderDistance
End Property

Friend Property Let FooterDistance(sNew As Single)
    m_sFooterDistance = sNew
End Property

Public Property Get FooterDistance() As Single
    FooterDistance = m_sFooterDistance
End Property

Friend Property Let PageHeight(sNew As Single)
    m_sPageHeight = sNew
End Property

Public Property Get PageHeight() As Single
    PageHeight = m_sPageHeight
End Property

Friend Property Let PageWidth(sNew As Single)
    m_sPageWidth = sNew
End Property

Public Property Get PageWidth() As Single
    PageWidth = m_sPageWidth
End Property

Friend Property Let AllowAuthorEMail(bNew As Boolean)
    m_bAllowAuthorEmail = bNew
End Property

Public Property Get AllowAuthorEMail() As Boolean
    AllowAuthorEMail = m_bAllowAuthorEmail
End Property

Friend Property Let AllowAuthorFax(bNew As Boolean)
    m_bAllowAuthorFax = bNew
End Property

Public Property Get AllowAuthorFax() As Boolean
    AllowAuthorFax = m_bAllowAuthorFax
End Property

Friend Property Let AllowAuthorTitle(bNew As Boolean)
    m_bAllowAuthorTitle = bNew
End Property

Public Property Get AllowAuthorTitle() As Boolean
    AllowAuthorTitle = m_bAllowAuthorTitle
End Property

Friend Property Let AllowAuthorAdmittedIn(bNew As Boolean)
    m_bAllowAuthorAdmittedIn = bNew
End Property

Public Property Get AllowAuthorAdmittedIn() As Boolean
    AllowAuthorAdmittedIn = m_bAllowAuthorAdmittedIn
End Property

Friend Property Let AllowAuthorPhone(bNew As Boolean)
    m_bAllowAuthorPhone = bNew
End Property

Public Property Get AllowAuthorPhone() As Boolean
    AllowAuthorPhone = m_bAllowAuthorPhone
End Property

Friend Property Let AllowAuthorName(bNew As Boolean)
    m_bAllowAuthorName = bNew
End Property

Public Property Get AllowAuthorName() As Boolean
    AllowAuthorName = m_bAllowAuthorName
End Property

Public Property Let FormatFirmName(bNew As Boolean)
    m_bFormatFirmName = bNew
End Property

Public Property Get FormatFirmName() As Boolean
    FormatFirmName = m_bFormatFirmName
End Property
'**********************************************************
'   Methods
'**********************************************************
