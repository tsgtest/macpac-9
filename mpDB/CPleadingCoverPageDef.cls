VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCoverPageDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CPleadingCoverPageDef Class
'   created 2/14/02 by Mike Conner

'   Contains properties and methods that
'   define a type of MacPac Pleading

'   Member of CPleadingCoverPageDefs
'   Container for
'**********************************************************

Private m_lID As Long
Private m_iLevel0 As Integer
Private m_iLevel1 As Integer
Private m_iLevel2 As Integer
Private m_iLevel3 As Integer
Private m_lDefPleadingPaper As Long
Private m_xBPFile As String
Private m_xCourtTitle As String
Private m_xCourtTitle1 As String
Private m_xCourtTitle2 As String
Private m_xCourtTitle3 As String
Private m_sPageHeight As Single
Private m_sPageWidth As Single
Private m_bUseBoilerplateFooterSetup As Boolean
Private m_bUnderlineDocumentTitle As Boolean
Private m_bytFirstNumberedPage As Byte
Private m_bytFirstPageNumber As Byte
Private m_bFormatFirmName As Boolean


Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Level0(iNew As Integer)
    m_iLevel0 = iNew
End Property

Public Property Get Level0() As Integer
    Level0 = m_iLevel0
End Property

Public Property Let Level1(iNew As Integer)
    m_iLevel1 = iNew
End Property

Public Property Get Level1() As Integer
    Level1 = m_iLevel1
End Property

Public Property Let Level2(iNew As Integer)
    m_iLevel2 = iNew
End Property

Public Property Get Level2() As Integer
    Level2 = m_iLevel2
End Property

Public Property Let Level3(iNew As Integer)
    m_iLevel3 = iNew
End Property

Public Property Get Level3() As Integer
    Level3 = m_iLevel3
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Public Property Let DefaultPleadingPaperType(lNew As Long)
    m_lDefPleadingPaper = lNew
End Property

Public Property Get DefaultPleadingPaperType() As Long
    DefaultPleadingPaperType = m_lDefPleadingPaper
End Property

Public Property Let CourtTitle(xNew As String)
    m_xCourtTitle = xNew
End Property

Public Property Get CourtTitle() As String
    CourtTitle = m_xCourtTitle
End Property

Public Property Let CourtTitle1(xNew As String)
    m_xCourtTitle1 = xNew
End Property

Public Property Get CourtTitle1() As String
    CourtTitle1 = m_xCourtTitle1
End Property

Public Property Let CourtTitle2(xNew As String)
    m_xCourtTitle2 = xNew
End Property

Public Property Get CourtTitle2() As String
    CourtTitle2 = m_xCourtTitle2
End Property

Public Property Let CourtTitle3(xNew As String)
    m_xCourtTitle3 = xNew
End Property

Public Property Get CourtTitle3() As String
    CourtTitle3 = m_xCourtTitle3
End Property

Public Property Let FirstNumberedPage(bytNew As Byte)
    m_bytFirstNumberedPage = bytNew
End Property

Public Property Get FirstNumberedPage() As Byte
    FirstNumberedPage = m_bytFirstNumberedPage
End Property

Public Property Let FirstPageNumber(bytNew As Byte)
    m_bytFirstPageNumber = bytNew
End Property

Public Property Get FirstPageNumber() As Byte
    FirstPageNumber = m_bytFirstPageNumber
End Property

Public Property Let UnderlineDocumentTitle(bNew As Boolean)
    m_bUnderlineDocumentTitle = bNew
End Property

Public Property Get UnderlineDocumentTitle() As Boolean
    UnderlineDocumentTitle = m_bUnderlineDocumentTitle
End Property

Public Property Let UseBoilerplateFooterSetup(bNew As Boolean)
    m_bUseBoilerplateFooterSetup = bNew
End Property

Public Property Get UseBoilerplateFooterSetup() As Boolean
    UseBoilerplateFooterSetup = m_bUseBoilerplateFooterSetup
End Property

Public Property Let PageHeight(sNew As Single)
    m_sPageHeight = sNew
End Property

Public Property Get PageHeight() As Single
    PageHeight = m_sPageHeight
End Property

Public Property Let PageWidth(sNew As Single)
    m_sPageWidth = sNew
End Property

Public Property Get PageWidth() As Single
    PageWidth = m_sPageWidth
End Property

Public Property Let FormatFirmName(bNew As Boolean)
    m_bFormatFirmName = bNew
End Property

Public Property Get FormatFirmName() As Boolean
    FormatFirmName = m_bFormatFirmName
End Property


