VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLabelDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Label Defs Collection Class
'   created 10/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Envelope defs - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************
Private m_oRS As DAO.Recordset
Private m_oDef As mpDB.CLabelDef
Private m_oArray As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_oArray = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh()
'gets the recordset of label definitions
    Dim xSQL As String

    xSQL = "SELECT  * FROM tblLabels"

'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                        dbOpenSnapshot, dbReadOnly)

'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With

''   update list xArray if already filled - else
''   wait until needed - ie wait until Get ListSource
'    If DoXArray Then
'        RefreshXArray
'    End If
End Sub

Public Function Active() As mpDB.CLabelDef
'returns the office object of the current record in the recordset
    Set Active = m_oDef
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(vIndex As Variant) As mpDB.CLabelDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = '" & vIndex & "'"
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           try matching LabelType field
            xCriteria = "fldLabelID = '" & vIndex & "'"

'           find
            .FindFirst xCriteria
            
            If .NoMatch Then
'               not found
                .Bookmark = vBkmk
                Set Item = Nothing
            Else
'               found - update current object
                UpdateObject
            
'               return current object
                Set Item = m_oDef
            End If
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CLabelDefs.Item"
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oDef = New mpDB.CLabelDef
    Set m_oArray = New XArrayObject.XArray
    m_oArray.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
    Set m_oDef = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xName As String
    Dim xID As String
    Dim xDescription As String

    With m_oRS
        lNumRows = .RecordCount

        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_oArray.ReDim 0, lNumRows - 1, 0, 2
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xName = !fldName
                xID = !fldID
                xDescription = !fldDescription
                m_oArray.Value(i, 0) = xID
                m_oArray.Value(i, 1) = xName
                m_oArray.Value(i, 2) = xDescription

            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject()
    Set m_oDef = New CLabelDef
    '9.7.1 #4024
    With m_oDef
        'Initialize properties
        .ID = Empty
        .LabelID = Empty
        .Name = Empty
        .Description = Empty
        .Tooltip = Empty
        .NumberDown = Empty
        .NumberAcross = Empty
        .SpacerCol = Empty
        .SpacerRow = Empty
        .HasGraphic = Empty
        .BoilerplateRange = Empty
        .BoilerplateFile = Empty
        .BoilerplateFile97 = Empty
        .BoilerplateRange97 = Empty
        .AllowDPhrases = Empty
        .AllowBarCode = Empty
        .AllowOfficeAddress = Empty
        .AllowBillingNumber = Empty
        .AllowAuthorInitials = Empty
        .LeftIndent = Empty
        .TopMargin = Empty
        .MaximumLines = Empty
        .BoilerplateFile = Empty
        .BoilerplateFile97 = Empty
        .ReturnAddressFormat = Empty
        .VerticalAlignment = Empty
        .IsCustom = Empty
        .IsDotMatrix = Empty
        .Height = Empty
        .Width = Empty
        .VerticalPitch = Empty
        .HorizontalPitch = Empty
        .SideMargin = Empty
        .PageHeight = Empty
        .PageWidth = Empty
        .BottomMargin = Empty
        .HeaderDistance = Empty
        .FooterDistance = Empty
        .PageOrientation = Empty
        .SplitDPhrases = Empty
        .DPhrase1Caption = Empty
        .DPhrase2Caption = Empty
        .LabelType = Empty
        .LabelVendor = Empty
    End With
    
    With m_oRS
        On Error Resume Next
        m_oDef.ID = !fldID
        m_oDef.LabelID = !fldLabelID
        m_oDef.Name = !fldName
        m_oDef.Description = !fldDescription
        m_oDef.Tooltip = !fldTooltipText
        m_oDef.NumberDown = !fldDown
        m_oDef.NumberAcross = !fldAcross
        m_oDef.SpacerCol = !fldSpacerColumn
        m_oDef.SpacerRow = !fldSpacerRow
        m_oDef.HasGraphic = !fldHasGraphic
        m_oDef.BoilerplateRange = !fldBPRange
        m_oDef.BoilerplateRange97 = !fldBPRange97
        m_oDef.BPGraphicRange = !fldBPGraphicRange
        m_oDef.AllowDPhrases = !fldAllowDPhrases
        m_oDef.AllowBarCode = !fldAllowBarCode
        m_oDef.AllowOfficeAddress = !fldAllowOfficeAddress
        m_oDef.AllowBillingNumber = !fldAllowBillingNumber
        m_oDef.AllowAuthorInitials = !fldAllowAuthorInitials
        m_oDef.LeftIndent = !fldLeftIndent
        m_oDef.TopMargin = !fldTopMargin
        m_oDef.MaximumLines = !fldMaxLines
        m_oDef.BoilerplateFile = !fldBPFile
        m_oDef.BoilerplateFile97 = !fldBPFile97
        m_oDef.ReturnAddressFormat = !fldReturnAddressFormat
        m_oDef.VerticalAlignment = !fldVerticalAlignment
        m_oDef.IsCustom = !fldIsCustom
        m_oDef.IsDotMatrix = !fldIsDotMatrix
        m_oDef.Height = !fldHeight
        m_oDef.Width = !fldWidth
        m_oDef.VerticalPitch = !fldVerticalPitch
        m_oDef.HorizontalPitch = !fldHorizontalPitch
        m_oDef.SideMargin = !fldSideMargin
        m_oDef.PageHeight = !fldPageHeight
        m_oDef.PageWidth = !fldPageWidth
        m_oDef.BottomMargin = !fldBottomMargin
        m_oDef.HeaderDistance = !fldHeaderDistance
        m_oDef.FooterDistance = !fldFooterDistance
        m_oDef.PageOrientation = !fldPageOrientation
        m_oDef.SplitDPhrases = !fldSplitDPhrases '9.7.1 #4024
        m_oDef.DPhrase1Caption = !fldDPhrase1Caption '9.7.1 #4024
        m_oDef.DPhrase2Caption = !fldDPhrase2Caption '9.7.1 #4024
        m_oDef.LabelType = !fldType
        m_oDef.LabelVendor = !fldVendor
    End With
End Function

