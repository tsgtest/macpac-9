VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentInsertDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CDocumentInsertDefs Collection Class
'   created 10/21/05 by Jeffrey Sweetland

'   Contains properties/methods that manage the
'   collection of Document Inser definitions -
'   collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for CSegmentDef
'**********************************************************


Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CDocumentInsertDef
Private m_oListSource As XArray
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CDocumentInsertDef
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where index = lID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       get item where name = vID
        xCrit = "fldID = " & lID
            
'       attempt to find the item
        m_oRS.FindFirst xCrit
        If m_oRS.NoMatch Then
'           if no match found, the
'           member does not exist
'            Err.Raise mpError_InvalidMember
            Exit Function
        Else
'           item found - update template object
            UpdateObject
        End If
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.Item"
End Function
Public Function ItemAtIndex(iIndex As Integer) As mpDB.CDocumentInsertDef
    On Error GoTo ProcError
    
'   get item where index = lID
    If (iIndex < 0) Or iIndex > Count - 1 Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       move the current rec to specified position
        On Error Resume Next
        m_oRS.AbsolutePosition = iIndex
        If Err.Number Then
            On Error GoTo ProcError
'           member does not exist
            Err.Raise mpError_InvalidMember
        Else
            On Error GoTo ProcError
'           item found - update template object
            UpdateObject
        End If
    End If
    Set ItemAtIndex = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CSegmentDefs.ItemAtIndex"
End Function
'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional xTemplateID As String, Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source- returns
'all letterhead types if no template id is supplied
    Dim xSQL As String

    If Len(xTemplateID) Then
'       get all types for specified template-
'******************************************************************************
        
        xSQL = "SELECT tblDocumentInserts.* " & _
               "FROM tblDocumentInserts INNER JOIN tblDocumentInsertAssignments " & _
                    "ON tblDocumentInserts.fldID = tblDocumentInsertAssignments." & _
                    "fldDocumentInsertID " & _
               "WHERE tblDocumentInsertAssignments.fldTemplateID='" & xTemplateID & "' " & _
               "ORDER BY tblDocumentInserts.fldName"
        
    Else
'       get all types
        xSQL = "SELECT * FROM tblDocumentInserts " & _
               "ORDER BY tblDocumentInserts.fldName"
    End If
   
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub
Public Property Get ListSource() As XArray
    If (m_oListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oListSource
End Property

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oType
        On Error Resume Next
        
        'Clear existing values
        .ID = Empty
        .Name = Empty
        .Subject = Empty
        .Boilerplate1 = Empty
        .Bookmark1 = Empty
        .Location1 = Empty
        .TargetBookmark1 = Empty
        .Boilerplate2 = Empty
        .Bookmark2 = Empty
        .Location2 = Empty
        .TargetBookmark2 = Empty
        .Boilerplate3 = Empty
        .Bookmark3 = Empty
        .Location3 = Empty
        .TargetBookmark3 = Empty
        .Boilerplate4 = Empty
        .Bookmark4 = Empty
        .Location4 = Empty
        .TargetBookmark4 = Empty
        
        ' Get current values
        .ID = m_oRS!fldID
        .Name = m_oRS!fldName
        .Subject = m_oRS!fldSubject
        .Boilerplate1 = m_oRS!fldBoilerplate1
        .Bookmark1 = m_oRS!fldBookmark1
        .Location1 = m_oRS!fldLocation1
        .TargetBookmark1 = m_oRS!fldTargetBookmark1
        .Boilerplate2 = m_oRS!fldBoilerplate2
        .Bookmark2 = m_oRS!fldBookmark2
        .Location2 = m_oRS!fldLocation2
        .TargetBookmark2 = m_oRS!fldTargetBookmark2
        .Boilerplate3 = m_oRS!fldBoilerplate3
        .Bookmark3 = m_oRS!fldBookmark3
        .Location3 = m_oRS!fldLocation3
        .TargetBookmark3 = m_oRS!fldTargetBookmark3
        .Boilerplate4 = m_oRS!fldBoilerplate4
        .Bookmark4 = m_oRS!fldBookmark4
        .Location4 = m_oRS!fldLocation4
        .TargetBookmark4 = m_oRS!fldTargetBookmark4
    End With
End Sub
Private Sub Class_Initialize()
    Set m_oType = New mpDB.CDocumentInsertDef
End Sub
Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xName As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            Set m_oListSource = New XArray
            m_oListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xName = !fldName
                m_oListSource.Value(i, 1) = xID
                m_oListSource.Value(i, 0) = xName
            
            Next i
        Else
            Set m_oListSource = New XArray
            m_oListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Sub Class_Terminate()
    Set m_oListSource = Nothing
End Sub
