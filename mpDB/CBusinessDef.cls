VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CDocumentLook Class
'   created 3/7/00 by Dan Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac Document Look - a set of
'   formats and content that has a name
'**********************************************************
Option Explicit

Private m_sLMargin As Single
Private m_sRMargin As Single
Private m_sTMargin As Single
Private m_sBMargin As Single
Private m_sPHeight As Single
Private m_sPWidth As Single
Private m_iCols As Byte
Private m_sColSpace As Single
Private m_bDiffPage1 As Boolean
Private m_bAllowDPhrases As Boolean
Private m_bAllowDocTitle As Boolean
Private m_lDefTitlePage As Long
Private m_xBP As String
Private m_xName As String
Private m_iOrient As Byte
Private m_lID As Long
Private m_bSplitDPhrases As Boolean '9.7.1 #4024
Private m_xDPhrase1Caption As String '9.7.1 #4024
Private m_xDefaultNumberingScheme As String
Private m_xDPhrase2Caption As String '9.7.1 #4024

'---9.5.0
Private m_bFullRowSignature As Boolean


'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property
Public Property Get SplitDPhrases() As Boolean '9.7.1 #4024
    SplitDPhrases = m_bSplitDPhrases
End Property
Public Property Let SplitDPhrases(bNew As Boolean) '9.7.1 #4024
    m_bSplitDPhrases = bNew
End Property
Public Property Get DPhrase1Caption() As String '9.7.1 #4024
    DPhrase1Caption = m_xDPhrase1Caption
End Property
Public Property Let DPhrase1Caption(xNew As String) '9.7.1 #4024
    m_xDPhrase1Caption = xNew
End Property
Public Property Let DefaultTitlePage(lNew As Long)
    m_lDefTitlePage = lNew
End Property

Public Property Get DefaultTitlePage() As Long
    DefaultTitlePage = m_lDefTitlePage
End Property
Public Property Get DPhrase2Caption() As String '9.7.1 #4024
    DPhrase2Caption = m_xDPhrase2Caption
End Property
Public Property Let DPhrase2Caption(xNew As String) '9.7.1 #4024
    m_xDPhrase2Caption = xNew
End Property
Public Property Let Boilerplate(xNew As String)
    m_xBP = xNew
End Property

Public Property Get Boilerplate() As String
    Boilerplate = m_xBP
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let DefaultNumberingScheme(xNew As String)
    m_xDefaultNumberingScheme = xNew
End Property

Public Property Get DefaultNumberingScheme() As String
    DefaultNumberingScheme = m_xDefaultNumberingScheme
End Property

Public Property Let Orientation(iNew As Byte)
    m_iOrient = iNew
End Property

Public Property Get Orientation() As Byte
    Orientation = m_iOrient
End Property

Public Property Let DifferentPage1HeaderFooter(bNew As Boolean)
    m_bDiffPage1 = bNew
End Property

Public Property Get DifferentPage1HeaderFooter() As Boolean
    DifferentPage1HeaderFooter = m_bDiffPage1
End Property

Public Property Let LeftMargin(sNew As Single)
    m_sLMargin = sNew
End Property

Public Property Get LeftMargin() As Single
    LeftMargin = m_sLMargin
End Property

Public Property Let RightMargin(sNew As Single)
    m_sRMargin = sNew
End Property

Public Property Get RightMargin() As Single
    RightMargin = m_sRMargin
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTMargin
End Property

Public Property Let BottomMargin(sNew As Single)
    m_sBMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBMargin
End Property

Public Property Let PageHeight(sNew As Single)
    m_sPHeight = sNew
End Property

Public Property Get PageHeight() As Single
    PageHeight = m_sPHeight
End Property

Public Property Let PageWidth(sNew As Single)
    m_sPWidth = sNew
End Property

Public Property Get PageWidth() As Single
    PageWidth = m_sPWidth
End Property

Public Property Let Columns(iNew As Byte)
    m_iCols = iNew
End Property

Public Property Get Columns() As Byte
    Columns = m_iCols
End Property

Public Property Let SpaceBetweenColumns(sNew As Single)
    m_sColSpace = sNew
End Property

Public Property Get SpaceBetweenColumns() As Single
    SpaceBetweenColumns = m_sColSpace
End Property

Public Property Let AllowDeliveryPhrases(bNew As Boolean)
    m_bAllowDPhrases = bNew
End Property

Public Property Get AllowDeliveryPhrases() As Boolean
    AllowDeliveryPhrases = m_bAllowDPhrases
End Property

Public Property Let AllowDocumentTitle(bNew As Boolean)
    m_bAllowDocTitle = bNew
End Property

Public Property Get AllowDocumentTitle() As Boolean
    AllowDocumentTitle = m_bAllowDocTitle
End Property
'---9.5.0
Public Property Let FullRowSignature(bNew As Boolean)
    m_bFullRowSignature = bNew
End Property
Public Property Get FullRowSignature() As Boolean
    FullRowSignature = m_bFullRowSignature
End Property
