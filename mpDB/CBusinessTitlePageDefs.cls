VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessTitlePageDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CBusinessTitlePageDefs Class
'   created 5/3/2006 by Charlie Homo-
'   charliehomo@pacbell.net

'   Contains properties and methods that
'   manage the MacPac Title Page Types
'**********************************************************
Option Explicit

Private m_oDef As mpDB.CBusinessTitlePageDef
Private m_oRS As DAO.Recordset
Private m_oArray As XArray

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oArray
End Property

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function

Public Function Item(vID As Variant) As CBusinessTitlePageDef
Attribute Item.VB_UserMemId = 0
'returns the title page whose id or name = vid
    Dim xCrit As String
    Dim vBkmk As Variant
    
        
    On Error GoTo ProcError
    
'   set criteria
    If IsNumeric(vID) Then
        xCrit = "fldID = " & vID
    Else
        xCrit = "fldName = '" & vID & "'"
    End If
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCrit
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CBusinessTitlePageDefs.Item"
    Exit Function
End Function

Public Sub Refresh()
    Dim xSQL As String
    
    On Error GoTo ProcError
    xSQL = "SELECT * FROM tblBusinessTitlePageTypes"
    
    Set m_oRS = Application.PublicDB _
        .OpenRecordset(xSQL, dbOpenSnapshot)
    
    With m_oRS
        If .RecordCount Then
'           populate
            .MoveLast
            .MoveFirst
'           refresh the list source
            RefreshXArray
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CBusinessTitlePageDefs.Refresh"
    Exit Sub
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xName As String
    
    On Error GoTo ProcError
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
'           populate if necessary
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            
'           create storage
            Set m_oArray = New XArray
            m_oArray.ReDim 0, lNumRows - 1, 0, 1
            
'           cycle through records, storing values in array
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                
                xID = Empty
                xName = Empty
                
                On Error Resume Next
                xID = !fldID
                xName = !fldName
                m_oArray.Value(i, 0) = xID
                m_oArray.Value(i, 1) = xName
                On Error GoTo ProcError
            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CBusinessTitlePageDefs.RefreshXArray"
    Exit Sub
End Sub

Private Sub UpdateObject()
    Set m_oDef = New CBusinessTitlePageDef
    With m_oDef
        .ID = Empty
        .Name = Empty
        .Boilerplate = Empty
        .LeftMargin = Empty
        .RightMargin = Empty
        .TopMargin = Empty
        .BottomMargin = Empty
        .VerticalAlignment = Empty
        .PageHeight = Empty
        .PageWidth = Empty
        .DateFormats = Empty
        .DefaultDate = Empty
        .Orientation = Empty
        .AllowDocumentTitle = Empty
        .AllowPartyName1 = Empty
        .AllowPartyName2 = Empty
        .AllowPartyName3 = Empty

        On Error Resume Next
        .ID = m_oRS!fldID
        .Name = m_oRS!fldName
        .Boilerplate = m_oRS!fldBoilerplate
        .LeftMargin = m_oRS!fldLeftMargin
        .RightMargin = m_oRS!fldRightMargin
        .TopMargin = m_oRS!fldTopMargin
        .BottomMargin = m_oRS!fldBottomMargin
        .VerticalAlignment = m_oRS!fldVerticalAlignment
        .PageHeight = m_oRS!fldPageHeight
        .PageWidth = m_oRS!fldPageWidth
        .DateFormats = m_oRS!fldDateFormats
        .DefaultDate = m_oRS!fldDefaultTitlePageDate
        .Orientation = m_oRS!fldOrientation
        .AllowDocumentTitle = m_oRS!fldAllowDocTitle
        .AllowPartyName1 = m_oRS!fldAllowPartyName1
        .AllowPartyName2 = m_oRS!fldAllowPartyName2
        .AllowPartyName3 = m_oRS!fldAllowPartyName3
    End With
End Sub

Private Sub Class_Terminate()
    Set m_oDef = Nothing
    Set m_oRS = Nothing
End Sub
