VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessSignatureDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CBusinessSignature Class
'   created 3/14/00 by Jeffrey Sweetland

'   Contains properties and methods that
'   define a type of MacPac Business Signature

'   Member of CBusinessSignatureDefs
'   Container for
'**********************************************************

Private m_lID As Long
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_xTableBookmark As String
Private m_xSignatureBlockBookmark As String
Private m_xSubEntityBookmark As String
Private m_bIncludesDate As Boolean
Private m_bAllowSubEntities As Boolean

Private m_bAllowEntityTitle As Boolean
Private m_bAllowPartyName As Boolean
Private m_bAllowPartyDescription As Boolean
Private m_bAllowSignerName As Boolean
Private m_bAllowSignerTitle As Boolean
Private m_bAllowIts As Boolean
Private m_bAllowSubPartyName As Boolean
Private m_bAllowSubPartyDescription As Boolean
Private m_bAllowSubSignerName As Boolean
Private m_bAllowSubSignerTitle As Boolean
Private m_bAllowSubIts As Boolean
Private m_lDateFormats As Long
Private m_xDefDate As String
'* 9.5.0
Private m_bFullRowFormat As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property
Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property
Public Property Get Description() As String
    Description = m_xDescription
End Property
Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property
Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property
Public Property Let TableBookmark(xNew As String)
    m_xTableBookmark = xNew
End Property
Public Property Get TableBookmark() As String
    TableBookmark = m_xTableBookmark
End Property
Public Property Let SignatureBlockBookmark(xNew As String)
    m_xSignatureBlockBookmark = xNew
End Property
Public Property Get SignatureBlockBookmark() As String
    SignatureBlockBookmark = m_xSignatureBlockBookmark
End Property
Public Property Let SubEntityBookmark(xNew As String)
    m_xSubEntityBookmark = xNew
End Property
Public Property Get SubEntityBookmark() As String
    SubEntityBookmark = m_xSubEntityBookmark
End Property
Public Property Let IncludesDate(bNew As Boolean)
    m_bIncludesDate = bNew
End Property
Public Property Get IncludesDate() As Boolean
    IncludesDate = m_bIncludesDate
End Property
Public Property Let DateFormats(lNew As Long)
    m_lDateFormats = lNew
End Property
Public Property Get DateFormats() As Long
    DateFormats = m_lDateFormats
End Property
Public Property Let AllowSubEntities(bNew As Boolean)
    m_bAllowSubEntities = bNew
End Property
Public Property Get AllowSubEntities() As Boolean
    AllowSubEntities = m_bAllowSubEntities
End Property
'* 9.5.0
Public Property Let FullRowFormat(bNew As Boolean)
    m_bFullRowFormat = bNew
End Property
Public Property Get FullRowFormat() As Boolean
    FullRowFormat = m_bFullRowFormat
End Property
'* End 9.5.0

'* 9.7.1

Public Property Let AllowEntityTitle(bNew As Boolean)
    m_bAllowEntityTitle = bNew
End Property
Public Property Get AllowEntityTitle() As Boolean
    AllowEntityTitle = m_bAllowEntityTitle
End Property

Public Property Let AllowPartyName(bNew As Boolean)
    m_bAllowPartyName = bNew
End Property
Public Property Get AllowPartyName() As Boolean
    AllowPartyName = m_bAllowPartyName
End Property

Public Property Let AllowPartyDescription(bNew As Boolean)
    m_bAllowPartyDescription = bNew
End Property
Public Property Get AllowPartyDescription() As Boolean
    AllowPartyDescription = m_bAllowPartyDescription
End Property

Public Property Let AllowSignerName(bNew As Boolean)
    m_bAllowSignerName = bNew
End Property
Public Property Get AllowSignerName() As Boolean
    AllowSignerName = m_bAllowSignerName
End Property

Public Property Let AllowSignerTitle(bNew As Boolean)
    m_bAllowSignerTitle = bNew
End Property
Public Property Get AllowSignerTitle() As Boolean
    AllowSignerTitle = m_bAllowSignerTitle
End Property

Public Property Let AllowIts(bNew As Boolean)
    m_bAllowIts = bNew
End Property
Public Property Get AllowIts() As Boolean
    AllowIts = m_bAllowIts
End Property

Public Property Let AllowSubPartyName(bNew As Boolean)
    m_bAllowSubPartyName = bNew
End Property
Public Property Get AllowSubPartyName() As Boolean
    AllowSubPartyName = m_bAllowSubPartyName
End Property

Public Property Let AllowSubPartyDescription(bNew As Boolean)
    m_bAllowSubPartyDescription = bNew
End Property
Public Property Get AllowSubPartyDescription() As Boolean
    AllowSubPartyDescription = m_bAllowSubPartyDescription
End Property

Public Property Let AllowSubSignerName(bNew As Boolean)
    m_bAllowSubSignerName = bNew
End Property
Public Property Get AllowSubSignerName() As Boolean
    AllowSubSignerName = m_bAllowSubSignerName
End Property

Public Property Let AllowSubSignerTitle(bNew As Boolean)
    m_bAllowSubSignerTitle = bNew
End Property
Public Property Get AllowSubSignerTitle() As Boolean
    AllowSubSignerTitle = m_bAllowSubSignerTitle
End Property

Public Property Let AllowSubIts(bNew As Boolean)
    m_bAllowSubIts = bNew
End Property
Public Property Get AllowSubIts() As Boolean
    AllowSubIts = m_bAllowSubIts
End Property

Friend Property Let DefaultDate(xNew As String)
    m_xDefDate = xNew
End Property

Public Property Get DefaultDate() As String
    DefaultDate = m_xDefDate
End Property

'**********************************************************
'   Methods
'**********************************************************

