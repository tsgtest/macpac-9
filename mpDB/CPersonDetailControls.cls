VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersonDetailControls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPersonDetailControls Collection Class
'   created 12/10/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CPersonDetailControls

'   Member of
'   Container for CPersonDetailControl
'**********************************************************

'**********************************************************
Private m_oRS As DAO.Recordset
'**********************************************************
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Friend Sub Refresh()
'get recordset of property defs from db
    Dim xSQL As String
    
'   get person detail controls
    xSQL = "SELECT * " & _
           "FROM tblCustomPeopleControls " & _
           "ORDER BY fldIndex"
           
'   open recordset
    Set m_oRS = Application.PublicDB _
        .OpenRecordset(xSQL, dbOpenSnapshot)
    
'   populate if possible
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
End Sub

Public Property Get Count() As Integer
    If Not (m_oRS Is Nothing) Then
        Count = m_oRS.RecordCount
    End If
End Property


Public Function Item(vID As Variant) As mpDB.CPersonDetailControl
'Public Function Item(lIndex As Long) As mpDB.CPersonDetailControl

'finds the record with ID = vID or Name = vID -
'returns the found record as a CPersonDetailControl-
'raises error if no match - returns "Nothing" if
'no recordset exists

    Dim xCriteria As String
    Dim oCtl As CPersonDetailControl
    Dim vBkmk As Variant
    
    On Error GoTo ProcError
    
    If m_oRS Is Nothing Then
        Exit Function
    End If
       
    With m_oRS
 '       set criteria
        If IsNumeric(vID) Then
            On Error Resume Next
            .MoveFirst
            .AbsolutePosition = vID - 1
            If Err.Number Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            On Error GoTo ProcError
        Else
            xCriteria = "fldName = '" & vID & "'"
'           find
            .FindFirst xCriteria
            
            If .NoMatch Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
        End If
    
'       requested item exists - update object
        Set oCtl = New CPersonDetailControl
        If Not oCtl Is Nothing Then _
                UpdateObject oCtl

'       return current object
        Set Item = oCtl
    
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CPersonDetailControls.Item"
    Exit Function
End Function

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Terminate()
    Set m_oRS = Nothing
End Sub

Private Sub UpdateObject(oCtl As mpDB.CPersonDetailControl)
    With oCtl
        .Caption = Empty
        .ControlType = Empty
        .Index = Empty
        Set .List = Nothing
        .LinkedProperty = Empty
        .VerticalOffset = Empty
        .HorizontalOffset = Empty
        .Width = Empty
        .MaxValue = Empty
        .MinValue = Empty
        .Increment = Empty
        .AppendQuotes = Empty
        .Lines = Empty
        
        On Error Resume Next
        .Caption = m_oRS!fldCaption
        .ControlType = m_oRS!fldType
        .Index = m_oRS!fldIndex
        If Not IsNull(m_oRS!fldList) Then
            Set .List = Application.Lists(m_oRS!fldList)
        End If
        .LinkedProperty = m_oRS!fldLinkedProperty
        .VerticalOffset = m_oRS!fldVerticalOffset
        .Lines = m_oRS!fldLines
        If Not IsNull(m_oRS!fldMaxLength) Then
            .MaxValue = CSng(m_oRS!fldMaxLength)
        End If
    End With
End Sub


