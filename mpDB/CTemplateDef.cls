VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplateDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplate Class
'   created 11/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac Template

'   Member of CTemplates
'   Container for
'**********************************************************
Public Enum mpTemplateTypes
    mpTemplateType_MacPac = 1
    mpTemplateType_Workgroup = 2
    mpTemplateType_User = 3
End Enum

Public Enum mpTemplateApplications
    mpTemplateApp_Word = 0
    mpTemplateApp_Excel = 1
    mpTemplateApp_Powerpoint = 2
    mpTemplateApp_Other = 3
End Enum

Private m_lID As Long
Private m_xName As String
Private m_xFile As String
Private m_xBaseTemplate As String
Private m_iType As mpTemplateTypes
Private m_lGroup As Long
Private m_xBPFile As String
Private m_xOptionsTable As String
Private m_xDescription As String
Private m_xShortName As String
Private m_xClassName As String
Private m_oCustProps As CCustomPropertyDefs
Private m_xMacro As String
Private m_xEditMacro As String
Private m_xSubFolder As String
Private m_xForm As String
Private m_xProtectedSections As String
Private m_lDefDocIDStamp As Long
Private m_bFavorite As Boolean
Private m_bWriteToBP As Boolean
Private m_bIsNew As Boolean
Private m_aVars() As String
Private m_iApplication As mpTemplateApplications



'**********************************************************
'   Properties
'**********************************************************
Friend Property Let IsNew(bNew As Boolean)
    m_bIsNew = bNew
End Property

Friend Property Get IsNew() As Boolean
    IsNew = m_bIsNew
End Property

Public Property Let FileName(xNew As String)
    m_xFile = xNew
End Property

Public Property Get FileName() As String
    FileName = m_xFile
End Property

Public Property Let BaseTemplate(xNew As String)
    m_xBaseTemplate = xNew
End Property

Public Property Get BaseTemplate() As String
    BaseTemplate = m_xBaseTemplate
End Property
Public Property Let MappedVariables(aNew() As String)
    m_aVars = aNew
End Property
Public Property Get MappedVariables() As String()
    MappedVariables = m_aVars
End Property
Public Property Let Macro(xNew As String)
    m_xMacro = xNew
End Property

Public Property Get Macro() As String
    Macro = m_xMacro
End Property

Public Property Let EditMacro(xNew As String)
    m_xEditMacro = xNew
End Property

Public Property Get EditMacro() As String
    EditMacro = m_xEditMacro
End Property

Public Property Let SubFolder(xNew As String)
    m_xSubFolder = xNew
End Property

Public Property Get SubFolder() As String
    SubFolder = m_xSubFolder
End Property

Public Property Let Form(xNew As String)
    m_xForm = xNew
End Property

Public Property Get Form() As String
    Form = m_xForm
End Property

Public Property Let ShortName(xNew As String)
    m_xShortName = xNew
End Property

Public Property Get ShortName() As String
    ShortName = m_xShortName
End Property

Public Property Let ClassName(xNew As String)
    m_xClassName = xNew
End Property

Public Property Get ClassName() As String
    ClassName = m_xClassName
End Property

Public Property Let ProtectedSections(xNew As String)
    m_xProtectedSections = xNew
End Property

Public Property Get ProtectedSections() As String
    ProtectedSections = m_xProtectedSections
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Public Property Let OptionsTable(xNew As String)
    m_xOptionsTable = xNew
End Property

Public Property Get OptionsTable() As String
    OptionsTable = m_xOptionsTable
End Property

Public Property Let DefaultDocIDStamp(lNew As Long)
    m_lDefDocIDStamp = lNew
End Property

Public Property Get DefaultDocIDStamp() As Long
    DefaultDocIDStamp = m_lDefDocIDStamp
End Property

Public Property Let Favorite(bNew As Boolean)
    m_bFavorite = bNew
End Property

Public Property Get Favorite() As Boolean
    Favorite = m_bFavorite
End Property

Public Property Let TemplateType(iNew As mpTemplateTypes)
    m_iType = iNew
End Property

Public Property Get TemplateType() As mpTemplateTypes
    TemplateType = m_iType
End Property

Public Property Get Group() As Long
    Group = m_lGroup
End Property

Public Property Let WriteToBP(bNew As Boolean)
    m_bWriteToBP = bNew
End Property

Public Property Get WriteToBP() As Boolean
    WriteToBP = m_bWriteToBP
End Property

Public Property Let Application(iNew As mpTemplateApplications)
    m_iApplication = iNew
End Property

Public Property Get Application() As mpTemplateApplications
    Application = m_iApplication
End Property
'**********************************************************
'   Methods
'**********************************************************
