VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomProperty"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CCustomProperty Class
'   created 11/27/99 by Daniel Fisherman
'   Contains properties and methods concerning
'   MacPac person's custom properties
'**********************************************************

Private m_xName As String
Private m_xValue As String
Private m_xField As Variant

'**********************************************************
'Properties
'**********************************************************
Public Property Let Field(xField As String)
    m_xField = xField
End Property

Public Property Get Field() As String
    Field = m_xField
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Value(xNew As String)
    m_xValue = xNew
End Property

Public Property Get Value() As String
Attribute Value.VB_UserMemId = 0
    Value = m_xValue
End Property

