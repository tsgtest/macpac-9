VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessTitlePageDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CBusinessTitlePageDef Class
'   created 5/3/2006 by Charlie Homo-
'   charliehomo@pacbell.net

'   Contains properties and methods that
'   define a Business Title Page - a set of
'   formats and content that has a name
'**********************************************************
Option Explicit

Private m_sLMargin As Single
Private m_sRMargin As Single
Private m_sTMargin As Single
Private m_sBMargin As Single
Private m_sPHeight As Single
Private m_sPWidth As Single
Private m_xBP As String
Private m_xName As String
Private m_iVertAlignment As Byte
Private m_lID As Long
Private m_xDefDate As String
Private m_lDateFormats As Long
Private m_bAllowDocTitle As Boolean
Private m_bAllowPartyName1 As Boolean
Private m_bAllowPartyName2 As Boolean
Private m_bAllowPartyName3 As Boolean
Private m_iOrient As Byte


'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let AllowDocumentTitle(bNew As Boolean)
    m_bAllowDocTitle = bNew
End Property

Public Property Get AllowDocumentTitle() As Boolean
    AllowDocumentTitle = m_bAllowDocTitle
End Property

Public Property Let AllowPartyName1(bNew As Boolean)
    m_bAllowPartyName1 = bNew
End Property

Public Property Get AllowPartyName1() As Boolean
    AllowPartyName1 = m_bAllowPartyName1
End Property

Public Property Let AllowPartyName2(bNew As Boolean)
    m_bAllowPartyName2 = bNew
End Property

Public Property Get AllowPartyName2() As Boolean
    AllowPartyName2 = m_bAllowPartyName2
End Property

Public Property Let AllowPartyName3(bNew As Boolean)
    m_bAllowPartyName3 = bNew
End Property

Public Property Get AllowPartyName3() As Boolean
    AllowPartyName3 = m_bAllowPartyName3
End Property

Public Property Let Orientation(iNew As Byte)
    m_iOrient = iNew
End Property

Public Property Get Orientation() As Byte
    Orientation = m_iOrient
End Property

Public Property Let DateFormats(lNew As Long)
    m_lDateFormats = lNew
End Property

Public Property Get DateFormats() As Long
    DateFormats = m_lDateFormats
End Property

Public Property Let DefaultDate(xNew As String)
    m_xDefDate = xNew
End Property

Public Property Get DefaultDate() As String
    DefaultDate = m_xDefDate
End Property

Public Property Let Boilerplate(xNew As String)
    m_xBP = xNew
End Property

Public Property Get Boilerplate() As String
    Boilerplate = m_xBP
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let VerticalAlignment(iNew As Byte)
    m_iVertAlignment = iNew
End Property

Public Property Get VerticalAlignment() As Byte
    VerticalAlignment = m_iVertAlignment
End Property

Public Property Let LeftMargin(sNew As Single)
    m_sLMargin = sNew
End Property

Public Property Get LeftMargin() As Single
    LeftMargin = m_sLMargin
End Property

Public Property Let RightMargin(sNew As Single)
    m_sRMargin = sNew
End Property

Public Property Get RightMargin() As Single
    RightMargin = m_sRMargin
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTMargin
End Property

Public Property Let BottomMargin(sNew As Single)
    m_sBMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBMargin
End Property

Public Property Let PageHeight(sNew As Single)
    m_sPHeight = sNew
End Property

Public Property Get PageHeight() As Single
    PageHeight = m_sPHeight
End Property

Public Property Let PageWidth(sNew As Single)
    m_sPWidth = sNew
End Property

Public Property Get PageWidth() As Single
    PageWidth = m_sPWidth
End Property
