VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCounselDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Pleading Counsel Types Collection Class
'   created 1/2/00 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Letterhead types - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_xarListSource As XArrayObject.XArray

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property



'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

   xSQL = "SELECT tblPleadingCounselTypes.* " & _
           "FROM tblPleadingCounselTypes " & ";"

'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub
Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function
Public Function Item(lID As Long) As mpDB.CPleadingCounselDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim opCounsel As CPleadingCounselDef
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
            Set opCounsel = New CPleadingCounselDef
'           found - update current object
            UpdateObject opCounsel
            
'           return current object
            Set Item = opCounsel
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingCounselDefs.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_oRS = Nothing
End Sub
Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub
Private Function UpdateObject(opCounsel As CPleadingCounselDef)
    
    With m_oRS
        On Error Resume Next
        opCounsel.ID = !fldID
        opCounsel.Description = Empty
        opCounsel.BoilerplateFile = Empty
        opCounsel.AllowAdditionalFirmAddress = Empty
        opCounsel.AllowAdditionalFirmPhone = Empty
        opCounsel.AllowAdditionalFirmFax = Empty
        opCounsel.AllowAdditionalFirmSlogan = Empty
        opCounsel.AllowSignerEMail = Empty
        opCounsel.AllowFirmFax = Empty
        opCounsel.AllowFirmSlogan = Empty
        opCounsel.ClientTitleDefault = Empty
        opCounsel.IncludeBarID = Empty
        opCounsel.BarIDPrefix = Empty
        opCounsel.BarIDSuffix = Empty
        opCounsel.DefaultPosition = Empty
        opCounsel.FullRowFormat = Empty
        opCounsel.RowsPerCounsel = Empty
        opCounsel.AttorneyRequired = Empty
        opCounsel.IncludeSignerEmail = Empty
        opCounsel.SignerEmailPrefix = Empty
        opCounsel.SignerEmailSuffix = Empty
        opCounsel.SignerNameCase = Empty
        opCounsel.AllowSignerEmailInput = Empty
        opCounsel.OfficeAddressFormat = Empty
        opCounsel.FormatFirmName = Empty
        
        opCounsel.ID = !fldID
        opCounsel.Description = !fldDescription
        opCounsel.BoilerplateFile = !fldBoilerplateFile
        opCounsel.AllowAdditionalFirmAddress = !fldAllowAdditionalFirmAddress
        opCounsel.AllowAdditionalFirmPhone = !fldAllowAdditionalFirmPhone
        opCounsel.AllowAdditionalFirmFax = !fldAllowAdditionalFirmFax
        opCounsel.AllowAdditionalFirmSlogan = !fldAllowAdditionalFirmSlogan
        opCounsel.AllowSignerEMail = !fldAllowSignerEMail
        opCounsel.AllowFirmFax = !fldAllowFirmFax
        opCounsel.AllowFirmSlogan = !fldAllowFirmSlogan
        opCounsel.ClientTitleDefault = !fldClientTitleDefault
        opCounsel.IncludeBarID = !fldIncludeBarID
        opCounsel.BarIDPrefix = !fldBarIDPrefix
        opCounsel.BarIDSuffix = !fldBarIDSuffix
        opCounsel.DefaultPosition = !fldDefaultPosition
        opCounsel.FullRowFormat = !fldFullRowFormat
        opCounsel.RowsPerCounsel = !fldRowsPerCounsel
        opCounsel.IncludeSignerEmail = !fldIncludeSignerEmail
        opCounsel.SignerEmailPrefix = !fldSignerEmailPrefix
        opCounsel.SignerEmailSuffix = !fldSignerEmailSuffix
        opCounsel.SignerNameCase = !fldSignerNameCase
        opCounsel.AllowSignerEmailInput = !fldAllowSignerEMailInput
        opCounsel.OfficeAddressFormat = !fldOfficeAddressFormat
        opCounsel.FormatFirmName = !fldFormatFirmName
        
        '---9.4.1
        Dim bValue As Boolean
        Err = 0
        bValue = !fldAttorneyRequired
        If Err.Number = 0 Then
            opCounsel.AttorneyRequired = bValue
        Else
            opCounsel.AttorneyRequired = True
        End If
    End With
End Function


