VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CCustomProperties Collection Class
'   created 11/27/99 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of Custom Properties of a CPerson -
'   member of CPerson
'**********************************************************

'**********************************************************
Private m_CustProp As CCustomProperty
Private m_oCustProps As Collection
Private m_debugID As Long
'**********************************************************

Public Function Add(xField As String, _
                    Name As String, _
                    Optional Value As String) As CCustomProperty
    Set m_CustProp = New CCustomProperty
    With m_CustProp
        .Field = xField
        .Name = Name
        .Value = Value
    End With
    m_oCustProps.Add m_CustProp, Name
    
    Set Add = m_CustProp
End Function

Friend Function Delete(vKey As Variant)
    m_oCustProps.Remove vKey
End Function

Public Function Count() As Integer
    Count = m_oCustProps.Count
End Function

Public Function Item(vKey As Variant) As CCustomProperty
Attribute Item.VB_UserMemId = 0
    On Error Resume Next
    Set Item = m_oCustProps.Item(vKey)
End Function

Private Sub Class_Initialize()
    #If compTrackObjects Then
        m_debugID = LogObjectCreate("CustomProperties")
    #End If
    Set m_oCustProps = New Collection
End Sub

Private Sub Class_Terminate()
    #If compTrackObjects Then
        g_colDebug.Remove CStr(m_debugID)
    #End If
    Set m_CustProp = Nothing
    Set m_oCustProps = Nothing
End Sub
