VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplateGroups"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplateGroups Collection Class
'   created 11/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CTemplateGroups - list items collection is an XArray

'   Member of CList
'   Container for CTemplateGroups
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_ID = 0
    xarCol_Name = 1
    xarCol_ParentID = 2
End Enum

Private m_oArray As XArrayObject.XArray
Private m_oGroup As CTemplateGroup
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Source(xarNew As XArray)
    Set m_oArray = xarNew
End Property

Public Property Get Source() As XArray
    Set Source = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Add(ByVal vID As Variant, ByVal xName As String, Optional ByVal lParentID As Long) As mpDB.CTemplateGroup
    Dim lUBound As Long
    
    On Error GoTo ProcError
    With m_oArray
        lUBound = m_oArray.UpperBound(1)
        .Insert 1, lUBound + 1
        .Value(lUBound + 1, xarCol_ID) = vID
        .Value(lUBound + 1, xarCol_Name) = xName
        .Value(lUBound + 1, xarCol_ParentID) = lParentID
    End With
    Set m_oGroup = New CTemplateGroup
    With m_oGroup
        .ID = vID
        .Name = xName
        If lParentID > 0 Then
            .ParentID = lParentID
        End If
    End With
    m_oGroup.IsNew = True
    Set Add = m_oGroup
    
    Exit Function
ProcError:
    RaiseError "mpDB.CTemplateGroups.Add", Err.Description
    Exit Function
End Function

Public Sub Save(oGroup As CTemplateGroup)
    Dim xSQL As String
    
    On Error GoTo ProcError
    With oGroup
        If .IsNew Then
            xSQL = "INSERT INTO tblTemplateGroups(fldID,fldName,fldParent) VALUES(" & _
                .ID & ",'" & .Name & "'," & .ParentID
        Else
            xSQL = "UPDATE tblTemplateGroups SET fldID=" & .ID & _
                ",fldName=" & .Name & "',fldParent=" & .ParentID
        End If
        
        With Application.PublicDB
            .Execute xSQL
            
            If .RecordsAffected = 0 Then
                Err.Raise 600, , "Could not save template group."
            Else
                oGroup.IsNew = False
            End If
        End With
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CTemplateGroups.Save"
    Exit Sub
End Sub

Public Sub Delete(lIndex As Long)
    On Error GoTo ProcError
    m_oArray.Delete 1, lIndex
'    Dim xSQL As String
'    xSQL = "DELETE * FROM tblTemplateGroups WHERE fldID = " & lID
'    With Application.PublicDB
'        'execute query
'        .Execute xSQL
'        If .RecordsAffected = 0 Then
'            'add, update failed - raise error
'            Err.Raise 600, , "Could not delete the template group."
'        Else
'            Me.Refresh
'        End If
'    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CTemplateGroups.Delete", Err.Description
    Exit Sub
End Sub

Friend Sub DeleteAll()
    m_oArray.Clear
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = m_oArray.Count(1)
End Function


Public Function ItemFromIndex(iID As Integer) As CTemplateGroup
    Dim i As Integer
    Set m_oGroup = New CTemplateGroup
    
    On Error GoTo ProcError
    
    m_oGroup.Name = m_oArray.Value(iID - 1, xarCol_Name)
    m_oGroup.ID = m_oArray.Value(iID - 1, xarCol_ID)
    
    If Len(m_oArray.Value(iID - 1, xarCol_ParentID)) Then
        m_oGroup.ParentID = m_oArray.Value(iID - 1, xarCol_ParentID)
    End If
    
    If m_oGroup.ID = Empty Then
        Err.Raise mpError_InvalidMember
        Exit Function
    End If
        
    m_oGroup.IsNew = False
    Set ItemFromIndex = m_oGroup
    Exit Function
ProcError:
    If Err.Number = 9 Then   'subscript out of range
        Err.Number = mpError_InvalidMember
    End If
    RaiseError "mpDB.CTemplateGroups.ItemFromIndex"
    Exit Function
End Function

Public Function Item(Optional vID As Variant) As CTemplateGroup
    Dim i As Integer
    Set m_oGroup = New CTemplateGroup
    
    On Error GoTo ProcError
    
    If IsNumeric(vID) Then
'       use ID
        For i = 0 To m_oArray.UpperBound(1) ' - 1
            If Val(m_oArray.Value(i, xarCol_ID)) = vID Then
                m_oGroup.Name = m_oArray.Value(i, xarCol_Name)
                m_oGroup.ID = m_oArray.Value(i, xarCol_ID)
                If Len(m_oArray.Value(i, xarCol_ParentID)) Then
                    m_oGroup.ParentID = m_oArray.Value(i, xarCol_ParentID)
                End If
                Exit For
            End If
        Next i
    Else
'       use name
        For i = 0 To m_oArray.UpperBound(1) ' - 1
            If m_oArray.Value(i, xarCol_Name) = vID Then
                m_oGroup.Name = m_oArray.Value(i, xarCol_Name)
                m_oGroup.ID = m_oArray.Value(i, xarCol_ID)
                If Len(m_oArray.Value(i, xarCol_ParentID)) Then
                    m_oGroup.ParentID = m_oArray.Value(i, xarCol_ParentID)
                End If
                Exit For
            End If
        Next i
    End If
    
    If m_oGroup.ID = Empty Then		'9.7.1030 #4642
        Set Item = Nothing
'        Err.Raise mpError_InvalidMember
        Exit Function
    End If
        
    m_oGroup.IsNew = False
    Set Item = m_oGroup
    Exit Function
ProcError:
    If Err.Number = 9 Then   'subscript out of range
        Err.Number = mpError_InvalidMember
    End If
    RaiseError "mpDB.CTemplateGroups.Item"
    Exit Function
End Function

Sub Refresh()
'fills xarray with template groups
    Set m_oArray = New XArrayObject.XArray
    m_oArray.ReDim 0, -1, 0, 1
    
'   get xarray from sql statement
    'GLOG : 5335 : CEH
    Set m_oArray = Application.xarGetTableXArray( _
        Application.PublicDB, "tblTemplateGroups", , "fldOrder")
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Refresh
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
End Sub


