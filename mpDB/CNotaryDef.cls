VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNotaryDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CNotaryDef Class
'   created 6/11/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define
'   a type of MacPac Notary Document

'   Member of CNotaryDefs
'   Container for COfficeAddressFormat
'**********************************************************

Private m_lID As Long
Private m_iLevel0 As Integer
Private m_xBPFile As String
Private m_bAllowGender As Boolean
Private m_bAllowPersonalProved As Boolean
Private m_bAllowAppearingPerson As Boolean
Private m_bAllowNotaryName As Boolean
Private m_bAllowCounty As Boolean
Private m_bAllowState As Boolean
Private m_bAllowCommission As Boolean
Private m_bAllowExecuteDate As Boolean
Private m_bAllowAppearanceDate As Boolean
Private m_bAllowCountyHeading As Boolean
Private m_bAllowDocumentTitle As Boolean
Private m_bAllowNumberOfPages As Boolean
Private m_bAllowDocumentDate As Boolean
Private m_bAllowOtherSigners As Boolean
Private m_bAllowCompanyName As Boolean
Private m_bAllowCompanyState As Boolean
Private m_bAllowSignerTitle As Boolean
Private m_xDlgCaption As String
Private m_lDateFormats As Long
Private m_xDesc As String
Private m_xDefExecuteDate As String
Private m_xDefAppearanceDate As String
Private m_sTMargin As Single
Private m_sBMargin As Single
Private m_sLMargin As Single
Private m_sRMargin As Single
Private m_xKnownByList As String
Private m_xCustom1Label As String
Private m_xCustom1Text As String
Private m_xCustom2Label As String
Private m_xCustom2Text As String
'CharlieCore 9.2.0 - BaseStyle
Private m_xBaseStyle As String

Friend Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let DateFormats(lNew As Long)
    m_lDateFormats = lNew
End Property

Public Property Get DateFormats() As Long
    DateFormats = m_lDateFormats
End Property

Friend Property Let Level0(iNew As Integer)
    m_iLevel0 = iNew
End Property

Public Property Get Level0() As Integer
    Level0 = m_iLevel0
End Property

Friend Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Friend Property Let KnownByList(xNew As String)
    m_xKnownByList = xNew
End Property

Public Property Get KnownByList() As String
    KnownByList = m_xKnownByList
End Property

Friend Property Let AllowGender(bNew As Boolean)
    m_bAllowGender = bNew
End Property

Public Property Get AllowGender() As Boolean
    AllowGender = m_bAllowGender
End Property

Friend Property Let AllowCountyHeading(bNew As Boolean)
    m_bAllowCountyHeading = bNew
End Property

Public Property Get AllowCountyHeading() As Boolean
    AllowCountyHeading = m_bAllowCountyHeading
End Property

Friend Property Let AllowPersonalProved(bNew As Boolean)
    m_bAllowPersonalProved = bNew
End Property

Public Property Get AllowPersonalProved() As Boolean
    AllowPersonalProved = m_bAllowPersonalProved
End Property

Friend Property Let AllowAppearingPerson(bNew As Boolean)
    m_bAllowAppearingPerson = bNew
End Property

Public Property Get AllowAppearingPerson() As Boolean
    AllowAppearingPerson = m_bAllowAppearingPerson
End Property

Friend Property Let AllowNotaryName(bNew As Boolean)
    m_bAllowNotaryName = bNew
End Property

Public Property Get AllowNotaryName() As Boolean
    AllowNotaryName = m_bAllowNotaryName
End Property

Friend Property Let AllowCompanyName(bNew As Boolean)
    m_bAllowCompanyName = bNew
End Property

Public Property Get AllowCompanyName() As Boolean
    AllowCompanyName = m_bAllowCompanyName
End Property

Friend Property Let AllowCompanyState(bNew As Boolean)
    m_bAllowCompanyState = bNew
End Property

Public Property Get AllowCompanyState() As Boolean
    AllowCompanyState = m_bAllowCompanyState
End Property

Friend Property Let AllowSignerTitle(bNew As Boolean)
    m_bAllowSignerTitle = bNew
End Property

Public Property Get AllowSignerTitle() As Boolean
    AllowSignerTitle = m_bAllowSignerTitle
End Property

Friend Property Let AllowCounty(bNew As Boolean)
    m_bAllowCounty = bNew
End Property

Public Property Get AllowCounty() As Boolean
    AllowCounty = m_bAllowCounty
End Property

Friend Property Let AllowState(bNew As Boolean)
    m_bAllowState = bNew
End Property

Public Property Get AllowState() As Boolean
    AllowState = m_bAllowState
End Property

Friend Property Let AllowCommission(bNew As Boolean)
    m_bAllowCommission = bNew
End Property

Public Property Get AllowCommission() As Boolean
    AllowCommission = m_bAllowCommission
End Property

Friend Property Let AllowAppearanceDate(bNew As Boolean)
    m_bAllowAppearanceDate = bNew
End Property

Public Property Get AllowAppearanceDate() As Boolean
    AllowAppearanceDate = m_bAllowAppearanceDate
End Property

Friend Property Let AllowExecuteDate(bNew As Boolean)
    m_bAllowExecuteDate = bNew
End Property

Public Property Get AllowExecuteDate() As Boolean
    AllowExecuteDate = m_bAllowExecuteDate
End Property

Friend Property Let AllowDocumentTitle(bNew As Boolean)
    m_bAllowDocumentTitle = bNew
End Property

Public Property Get AllowDocumentTitle() As Boolean
    AllowDocumentTitle = m_bAllowDocumentTitle
End Property

Friend Property Let AllowDocumentDate(bNew As Boolean)
    m_bAllowDocumentDate = bNew
End Property

Public Property Get AllowDocumentDate() As Boolean
    AllowDocumentDate = m_bAllowDocumentDate
End Property

Friend Property Let AllowNumberOfPages(bNew As Boolean)
    m_bAllowNumberOfPages = bNew
End Property

Public Property Get AllowNumberOfPages() As Boolean
    AllowNumberOfPages = m_bAllowNumberOfPages
End Property

Friend Property Let AllowOtherSigners(bNew As Boolean)
    m_bAllowOtherSigners = bNew
End Property

Public Property Get AllowOtherSigners() As Boolean
    AllowOtherSigners = m_bAllowOtherSigners
End Property

Friend Property Let DialogCaption(xNew As String)
    m_xDlgCaption = xNew
End Property

Public Property Get DialogCaption() As String
    DialogCaption = m_xDlgCaption
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Let DefaultAppearanceDateFormat(xNew As String)
    m_xDefAppearanceDate = xNew
End Property

Public Property Get DefaultAppearanceDateFormat() As String
    DefaultAppearanceDateFormat = m_xDefAppearanceDate
End Property

Public Property Let DefaultExecuteDateFormat(xNew As String)
    m_xDefExecuteDate = xNew
End Property

Public Property Get DefaultExecuteDateFormat() As String
    DefaultExecuteDateFormat = m_xDefExecuteDate
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTMargin
End Property

Public Property Let BottomMargin(sNew As Single)
    m_sBMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBMargin
End Property

Public Property Let LeftMargin(sNew As Single)
    m_sLMargin = sNew
End Property

Public Property Get LeftMargin() As Single
    LeftMargin = m_sLMargin
End Property

Public Property Let RightMargin(sNew As Single)
    m_sRMargin = sNew
End Property

Public Property Get RightMargin() As Single
    RightMargin = m_sRMargin
End Property

Public Property Get Custom1Label() As String
    Custom1Label = m_xCustom1Label
End Property

Public Property Let Custom1Label(xNew As String)
    m_xCustom1Label = xNew
End Property

Public Property Get Custom1DefaultText() As String
    Custom1DefaultText = m_xCustom1Text
End Property

Public Property Let Custom1DefaultText(xNew As String)
    m_xCustom1Text = xNew
End Property

Public Property Get Custom2Label() As String
    Custom2Label = m_xCustom2Label
End Property

Public Property Let Custom2Label(xNew As String)
    m_xCustom2Label = xNew
End Property

Public Property Get Custom2DefaultText() As String
    Custom2DefaultText = m_xCustom2Text
End Property

Public Property Let Custom2DefaultText(xNew As String)
    m_xCustom2Text = xNew
End Property

'CharlieCore 9.2.0 - BaseStyle
Public Property Get BaseStyle() As String
    BaseStyle = m_xBaseStyle
End Property
Public Property Let BaseStyle(xNew As String)
    m_xBaseStyle = xNew
End Property

