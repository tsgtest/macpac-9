VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCounselTypes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Pleading Caption Types Collection Class
'   created 1/2/00 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Letterhead types - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_opCounsel As mpDB.CPleadingCounselType
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(vTemplateID As Variant, Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    xSQL = "SELECT tblPleadingCounselTypes.* " & _
           "FROM tblPleadingCounselTypes INNER JOIN tblPleadingCounselAssignments " & _
                "ON tblPleadingCounselTypes.fldID = tblPleadingCounselAssignments." & _
                "fldPleadingCounselID WHERE tblPleadingCounselAssignments.fldPleadingTypeID=" & _
                "'" & vTemplateID & "'" & ";"
  
   
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Active() As mpDB.CPleadingCounselType
'returns the office object of the current record in the recordset
    Set Active = m_opCounsel
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(lIndex As Long) As mpDB.CPleadingCounselType
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lIndex
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_opCounsel
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingCounselTypes.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_opCounsel = New mpDB.CPleadingCounselType
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_opCounsel = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject()
    With m_oRS
        On Error Resume Next
        m_opCounsel.ID = !fldID
        m_opCounsel.Description = !fldDescription
        m_opCounsel.BoilerplateFile = !fldBoilerplateFile
        m_opCounsel.Bookmark = !fldBookmark
        m_opCounsel.AllowAdditionalFirmAddress = !AllowAdditionalFirmAddress
        m_opCounsel.AllowAdditionalFirmPhone = !AllowAdditionalFirmPhone
        m_opCounsel.AllowAdditionalFirmFax = !AllowAdditionalFirmFax
        m_opCounsel.AllowAdditionalFirmSlogan = !AllowAdditionalFirmSlogan
        m_opCounsel.AllowFirmSlogan = !AllowFirmSlogan
        
'        m_opCounsel.AllowCaseNumber = !fldAllowCaseNumber
'        m_opCounsel.AllowAdditionalCaseNumber1 = !fldAllowAdditionalCaseNumber1
'        m_opCounsel.AllowAppellateCaseNumber = !fldAllowAppellateCaseNumber
'        m_opCounsel.AllowAdditionalCaseNumber2 = !fldAllowAdditionalCaseNumber2
'        m_opCounsel.AllowChapterNumber = !fldAllowChapterNumber
'        m_opCounsel.AllowDocTitleCentered = !fldAllowDocTitleCentered
'        m_opCounsel.AllowDocTitleRight = !fldAllowDocTitleRight
'        m_opCounsel.CaptionSeparatorType = !fldAllowAuthorName
    End With
End Function







