VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDepoSummaryDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   Depo Summary Definition Class
'   created 5/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define an depo summary

'   Member of CDepoSummaryDefs
'**********************************************************
Option Explicit

'**********************************************************
Private m_lID As Long
Private m_xDesc As String
Private m_xBPFile As String

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

