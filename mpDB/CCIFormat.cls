VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCIFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CCIFormat Class
'   created 06/15/00 by Jeffrey Sweetland
'   Contains properties of the CIFormat database object
'   Corresponding to parameters of MPCI Retrieve method
'**********************************************************

Private m_lID As Long
Private m_xDescription As String
Private m_bIncludeTo As Boolean
Private m_bIncludeFrom As Boolean
Private m_bIncludeCC As Boolean
Private m_bIncludeBCC As Boolean
Private m_bIncludePhone As Boolean
Private m_bIncludeFax As Boolean
Private m_bIncludeEMail As Boolean
Private m_iOnEmptyAction As Integer
Private m_bPromptForPhones As Boolean
Private m_iDefaultList As Integer
Private m_bToNamesOnly As Boolean
Private m_bFromNamesOnly As Boolean
Private m_bCCNamesOnly As Boolean
Private m_bBCCNamesOnly As Boolean
Private m_bIsNew As Boolean
Private m_xPhoneLabel As String
Private m_xFaxLabel As String
Private m_xEmailLabel As String

'**********************************************************
'Properties
'**********************************************************
Friend Property Let IsNew(bNew As Boolean)
    m_bIsNew = bNew
End Property

Friend Property Get IsNew() As Boolean
    IsNew = m_bIsNew
End Property

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property
Public Property Get ID() As Long
    ID = m_lID
End Property
Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property
Public Property Get Description() As String
    Description = m_xDescription
End Property
Public Property Let IncludeTo(bNew As Boolean)
    m_bIncludeTo = bNew
End Property
Public Property Get IncludeTo() As Boolean
    IncludeTo = m_bIncludeTo
End Property
Public Property Let IncludeFrom(bNew As Boolean)
    m_bIncludeFrom = bNew
End Property
Public Property Get IncludeFrom() As Boolean
    IncludeFrom = m_bIncludeFrom
End Property

Public Property Let IncludeCC(bNew As Boolean)
    m_bIncludeCC = bNew
End Property
Public Property Get IncludeCC() As Boolean
    IncludeCC = m_bIncludeCC
End Property

Public Property Let IncludeBCC(bNew As Boolean)
    m_bIncludeBCC = bNew
End Property
Public Property Get IncludeBCC() As Boolean
    IncludeBCC = m_bIncludeBCC
End Property

Public Property Let IncludePhone(bNew As Boolean)
    m_bIncludePhone = bNew
End Property
Public Property Get IncludePhone() As Boolean
    IncludePhone = m_bIncludePhone
End Property

Public Property Let IncludeFax(bNew As Boolean)
    m_bIncludeFax = bNew
End Property
Public Property Get IncludeFax() As Boolean
    IncludeFax = m_bIncludeFax
End Property

Public Property Let IncludeEMail(bNew As Boolean)
    m_bIncludeEMail = bNew
End Property
Public Property Get IncludeEMail() As Boolean
    IncludeEMail = m_bIncludeEMail
End Property

Public Property Let PromptForPhones(bNew As Boolean)
    m_bPromptForPhones = bNew
End Property
Public Property Get PromptForPhones() As Boolean
    PromptForPhones = m_bPromptForPhones
End Property

Public Property Let ToNamesOnly(bNew As Boolean)
    m_bToNamesOnly = bNew
End Property
Public Property Get ToNamesOnly() As Boolean
    ToNamesOnly = m_bToNamesOnly
End Property

Public Property Let FromNamesOnly(bNew As Boolean)
    m_bFromNamesOnly = bNew
End Property
Public Property Get FromNamesOnly() As Boolean
    FromNamesOnly = m_bFromNamesOnly
End Property

Public Property Let CCNamesOnly(bNew As Boolean)
    m_bCCNamesOnly = bNew
End Property
Public Property Get CCNamesOnly() As Boolean
    CCNamesOnly = m_bCCNamesOnly
End Property

Public Property Let BCCNamesOnly(bNew As Boolean)
    m_bBCCNamesOnly = bNew
End Property
Public Property Get BCCNamesOnly() As Boolean
    BCCNamesOnly = m_bBCCNamesOnly
End Property

Public Property Let OnEmptyAction(iNew As Integer)
    m_iOnEmptyAction = iNew
End Property
Public Property Get OnEmptyAction() As Integer
    OnEmptyAction = m_iOnEmptyAction
End Property
Public Property Let DefaultList(iNew As Integer)
    m_iDefaultList = iNew
End Property
Public Property Get DefaultList() As Integer
    DefaultList = m_iDefaultList
End Property
Public Property Let PhoneLabel(xNew As String)
    m_xPhoneLabel = xNew
End Property
Public Property Get PhoneLabel() As String
    PhoneLabel = m_xPhoneLabel
End Property
Public Property Let FaxLabel(xNew As String)
    m_xFaxLabel = xNew
End Property
Public Property Get FaxLabel() As String
    FaxLabel = m_xFaxLabel
End Property
Public Property Let EmailLabel(xNew As String)
    m_xEmailLabel = xNew
End Property
Public Property Get EmailLabel() As String
    EmailLabel = m_xEmailLabel
End Property

