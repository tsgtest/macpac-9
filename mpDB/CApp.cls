VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CApp"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**********************************************************
'   CApp Collection Class
'   created 12/23/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   CApp object - this is a private class - all props/methods
'   that should be exposed need to be defined in CDatabase-
'   an object of this class is created globally by Sub Main

'   Container for CPersons, COffices, CLists, DAO.Database
'**********************************************************

'**********************************************************

Private m_oDBPublic As DAO.Database
Private m_oDBPrivate As DAO.Database
Private m_oTemplateDefs As mpDB.CTemplateDefs
Private m_oTemplateGroups As mpDB.CTemplateGroups
Private m_oLists As mpDB.CLists
Private m_Offices As mpDB.COffices
Private m_oEnvDefs As mpDB.CEnvelopeDefs
Private m_oLabelDefs As mpDB.CLabelDefs
Private m_oPeople As mpDB.CPersons
Private m_oPeopleFirm As mpDB.CPersons
Private m_oPeopleInput As mpDB.CPersons
Private m_oPeopleFavorites As mpDB.CPersons
Private m_oAttorneys As mpDB.CPersons
Private m_oError As mpError.CError
Private m_oCustProps As CCustomProperties
Private m_oCustControls As CCustomControls
Private m_oLHDefs As mpDB.CLetterheadDefs
Private m_oPleadingDefs As mpDB.CPleadingDefs
Private m_oPleadingFaves As mpDB.CPleadingFavorites
Private m_oPPaperDefs As mpDB.CPleadingPaperDefs
Private m_oPCounselDefs As mpDB.CPleadingCounselDefs
Private m_oPleadingSeparateStatementDefs As mpDB.CPleadingSeparateStatementDefs
Private m_oPCaptionDefs As mpDB.CPleadingCaptionDefs
Private m_oPSigDefs As mpDB.CPleadingSignatureDefs
Private m_oDocStampDefs As mpDB.CDocumentStampDefs
Private m_oCustomDialogDefs As mpDB.CCustomDialogDefs
Private m_oPCaptionBorderStyles As mpDB.CPleadingCaptionBorderStyles
Private m_oBusTypes As mpDB.CBusinessDefs
Private m_oBSigDefs As mpDB.CBusinessSignatureDefs
Private m_oFormats As mpDB.COfficeAddressFormats
Private m_oDepoSummaryDefs As mpDB.CDepoSummaryDefs
Private m_oSegmentDefs As mpDB.CSegmentDefs
Private m_oSegmentGroups As mpDB.CSegmentGroups
Private m_oCIFormats As mpDB.CCIFormats
Private m_oPleadingCoverPageDefs As mpDB.CPleadingCoverPageDefs
Private m_oDocInsertDefs As mpDB.CDocumentInsertDefs

Friend Function NewXArray() As XArray
    On Error GoTo ProcError
    Set NewXArray = New XArray
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.NewXArray"
    Exit Function
End Function

Friend Function CIFormats() As mpDB.CCIFormats
'create and fill if object doesn't already exist
    On Error GoTo ProcError
    If (m_oCIFormats Is Nothing) Then
        Set m_oCIFormats = New mpDB.CCIFormats
        m_oCIFormats.Refresh
    End If
    
    Set CIFormats = m_oCIFormats
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.CIFormats"
    Exit Function
End Function
Friend Function NotaryDefs(ByVal lID As Long) As mpDB.CNotaryDefs
'create and fill if object doesn't already exist
    Dim oDefs As mpDB.CNotaryDefs
    Set oDefs = New mpDB.CNotaryDefs
    oDefs.Refresh lID
    Set NotaryDefs = oDefs
End Function

Friend Function SegmentDefs(ByVal xTemplate As String) As mpDB.CSegmentDefs
'create and fill if object doesn't already exist
    Dim oDefs As mpDB.CSegmentDefs
    Set oDefs = New mpDB.CSegmentDefs
    oDefs.Refresh xTemplate
    Set SegmentDefs = oDefs
End Function
Friend Function SegmentDefinitions() As mpDB.CSegmentDefs
    If (m_oSegmentDefs Is Nothing) Then
        Set m_oSegmentDefs = New mpDB.CSegmentDefs
    End If
    Set SegmentDefinitions = m_oSegmentDefs
End Function
Friend Function SegmentGroups() As mpDB.CSegmentGroups
    If (m_oSegmentGroups Is Nothing) Then
        Set m_oSegmentGroups = New mpDB.CSegmentGroups
    End If
    Set SegmentGroups = m_oSegmentGroups
End Function
Friend Function ServiceDefs(ByVal lID As Long) As mpDB.CServiceDefs
'create and fill if object doesn't already exist
    Dim oDefs As mpDB.CServiceDefs
    Set oDefs = New mpDB.CServiceDefs
    oDefs.Refresh lID
    Set ServiceDefs = oDefs
End Function

Friend Function BusinessTitlePageDefs() As mpDB.CBusinessTitlePageDefs
'create and fill if object doesn't already exist
    Dim oDefs As mpDB.CBusinessTitlePageDefs
    Set oDefs = New mpDB.CBusinessTitlePageDefs
    oDefs.Refresh
    Set BusinessTitlePageDefs = oDefs
End Function

Friend Function ServiceListDefs(ByVal lServiceType As Long) As mpDB.CServiceListDefs
'create and fill if object doesn't already exist
    Dim oDefs As mpDB.CServiceListDefs
    Set oDefs = New mpDB.CServiceListDefs
    oDefs.Refresh lServiceType
    Set ServiceListDefs = oDefs
End Function

Friend Function VerificationDefs(ByVal lID As Long) As mpDB.CVerificationDefs
'create and fill if object doesn't already exist
    Dim oDefs As mpDB.CVerificationDefs
    Set oDefs = New mpDB.CVerificationDefs
    oDefs.Refresh lID
    Set VerificationDefs = oDefs
End Function

Friend Function DepoSummaryDefs() As mpDB.CDepoSummaryDefs
'create and fill if object doesn't already exist
    If (m_oDepoSummaryDefs Is Nothing) Then
        Set m_oDepoSummaryDefs = New mpDB.CDepoSummaryDefs
        m_oDepoSummaryDefs.Refresh
    End If
    
    Set DepoSummaryDefs = m_oDepoSummaryDefs
End Function

Friend Function OfficeAddressFormats() As mpDB.COfficeAddressFormats
    If (m_oFormats Is Nothing) Then
        Set m_oFormats = New mpDB.COfficeAddressFormats
        m_oFormats.Refresh
    End If
    
    Set OfficeAddressFormats = m_oFormats
End Function

Friend Function PleadingCaptionBorderStyles() As mpDB.CPleadingCaptionBorderStyles
'create and fill if object doesn't already exist

    'Always refresh -- these can be reset from pleading front end
    Set m_oPCaptionBorderStyles = New mpDB.CPleadingCaptionBorderStyles
    m_oPCaptionBorderStyles.Refresh
    
    Set PleadingCaptionBorderStyles = m_oPCaptionBorderStyles
End Function

Friend Function DocumentStampDefs(Optional ByVal vTypeID As Integer = 0, _
                                  Optional ByVal xTemplate As String, _
                                  Optional ByVal lLHID As Long = 0) As mpDB.CDocumentStampDefs
'create and fill if object doesn't already exist
    Dim oDocStampDefs As mpDB.CDocumentStampDefs
    On Error GoTo ProcError
    Set oDocStampDefs = New mpDB.CDocumentStampDefs
    oDocStampDefs.Refresh vTypeID, xTemplate, lLHID
    
    Set DocumentStampDefs = oDocStampDefs
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.DocumentStampDefs"
    Exit Function
End Function

Friend Function CustomDialogDefs() As mpDB.CCustomDialogDefs
'create and fill if object doesn't already exist
    If (m_oCustomDialogDefs Is Nothing) Then
        Set m_oCustomDialogDefs = New mpDB.CCustomDialogDefs
        m_oCustomDialogDefs.Refresh
    End If
    
    Set CustomDialogDefs = m_oCustomDialogDefs
End Function

Friend Function PleadingSignatureDefs() As mpDB.CPleadingSignatureDefs
'create and fill if object doesn't already exist
    If (m_oPSigDefs Is Nothing) Then
        Set m_oPSigDefs = New mpDB.CPleadingSignatureDefs
        m_oPSigDefs.Refresh
    End If
    
    Set PleadingSignatureDefs = m_oPSigDefs
End Function

Friend Function PleadingCoverPageDefs() As mpDB.CPleadingCoverPageDefs
    On Error GoTo ProcError
    If (m_oPleadingCoverPageDefs Is Nothing) Then
        Set m_oPleadingCoverPageDefs = New mpDB.CPleadingCoverPageDefs
        m_oPleadingCoverPageDefs.Refresh
    End If
    
    Set PleadingCoverPageDefs = m_oPleadingCoverPageDefs
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.PleadingCoverPageDefs"
    Exit Function
End Function

Friend Function BusinessDefs() As mpDB.CBusinessDefs
'create and fill if object doesn't already exist
    Dim oDocLooks As mpDB.CBusinessDefs
    
    If (m_oBusTypes Is Nothing) Then
        Set m_oBusTypes = New mpDB.CBusinessDefs
        m_oBusTypes.Refresh
    End If
    Set BusinessDefs = m_oBusTypes
End Function

Friend Function BusinessSignatureDefs() As mpDB.CBusinessSignatureDefs
'create and fill if object doesn't already exist
    If (m_oBSigDefs Is Nothing) Then
        Set m_oBSigDefs = New mpDB.CBusinessSignatureDefs
        m_oBSigDefs.Refresh
    End If
    
    Set BusinessSignatureDefs = m_oBSigDefs
End Function

Friend Function PleadingCaptionDefs() As mpDB.CPleadingCaptionDefs
'create and fill if object doesn't already exist
    If (m_oPCaptionDefs Is Nothing) Then
        Set m_oPCaptionDefs = New mpDB.CPleadingCaptionDefs
        m_oPCaptionDefs.Refresh
    End If
    
    Set PleadingCaptionDefs = m_oPCaptionDefs
End Function

Friend Function PleadingCounselDefs() As mpDB.CPleadingCounselDefs
'create and fill if object doesn't already exist
    If (m_oPCounselDefs Is Nothing) Then
        Set m_oPCounselDefs = New mpDB.CPleadingCounselDefs
        m_oPCounselDefs.Refresh
    End If
    
    Set PleadingCounselDefs = m_oPCounselDefs
End Function

Friend Function PleadingDefs() As mpDB.CPleadingDefs
    On Error GoTo ProcError
    If (m_oPleadingDefs Is Nothing) Then
        Set m_oPleadingDefs = New mpDB.CPleadingDefs
    End If
    
    Set PleadingDefs = m_oPleadingDefs
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.PleadingDefs"
    Exit Function
End Function

Friend Function PleadingPaperDefs(Optional xState As String = "") As mpDB.CPleadingPaperDefs
'create and fill if object doesn't already exist
    On Error GoTo ProcError
    If (m_oPPaperDefs Is Nothing) Or xState <> "" Then
        Set m_oPPaperDefs = New mpDB.CPleadingPaperDefs
        m_oPPaperDefs.Refresh xState
    End If
    
    Set PleadingPaperDefs = m_oPPaperDefs
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.PleadingPaperDefs"
    Exit Function
End Function

Friend Function PleadingFavorites() As mpDB.CPleadingFavorites
'create and fill if object doesn't already exist
    If (m_oPleadingFaves Is Nothing) Then
        Set m_oPleadingFaves = New mpDB.CPleadingFavorites
        m_oPleadingFaves.Refresh
    End If
    
    Set PleadingFavorites = m_oPleadingFaves
End Function

Public Function OptionsControls(ByVal xTemplate As String) As mpDB.COptionsControls
    Dim oOptCtls As mpDB.COptionsControls
    Set oOptCtls = New mpDB.COptionsControls
    oOptCtls.Template = xTemplate
    Set OptionsControls = oOptCtls
End Function

Public Function PersonDetailControls() As mpDB.CPersonDetailControls
    Dim oCtls As mpDB.CPersonDetailControls
    Set oCtls = New mpDB.CPersonDetailControls
    oCtls.Refresh
    Set PersonDetailControls = oCtls
End Function

Public Function CustomControls(ByVal xCategory As String) As mpDB.CCustomControls
    Dim oCustCtls As mpDB.CCustomControls
    Set oCustCtls = New mpDB.CCustomControls
    oCustCtls.Category = xCategory
    Set CustomControls = oCustCtls
End Function

Public Function CustomProperties(ByVal xCategory As String) As mpDB.CCustomPropertyDefs
    Dim oPropDefs As mpDB.CCustomPropertyDefs
    Set oPropDefs = New mpDB.CCustomPropertyDefs
    oPropDefs.Category = xCategory
    Set CustomProperties = oPropDefs
End Function

Public Function CustomPersonProperties() As mpDB.CCustomProperties
    If m_oCustProps Is Nothing Then
        GetCustomProperties
    End If
    Set CustomPersonProperties = m_oCustProps
End Function

Public Function Error() As mpError.CError
    Set Error = m_oError
End Function

Friend Sub AddTemplateFavorite(ByVal xID As String)
'adds template with id = xID to list of favorites
'    Dim oDef As CTemplateDef
    Dim xSQL As String
    
'   test for valid template
'    On Error Resume Next
'    Set oDef = Item(xID)
'    On Error GoTo ProcError
'    If oDef Is Nothing Then
'        Err.Raise mpError_InvalidMember, , xID & _
'            " is an invalid template."
'    End If
    
'   insert into tblFavoriteTemplates
    xSQL = "INSERT INTO tblFavoriteTemplates (fldTemplateID) VALUES('" & xID & "') "
    Application.PrivateDB.Execute xSQL
    Exit Sub
ProcError:
    RaiseError "mpDB.CApp.AddTemplateFavorite"
    Exit Sub
End Sub

Friend Sub RemoveTemplateFavorite(ByVal xID As String)
'removes template with id = xID from list of favorites
'    Dim oDef As CTemplateDef
    Dim xSQL As String
    
''   test for valid template
'    On Error Resume Next
'    Set oDef = Item(xID)
'    On Error GoTo ProcError
'    If oDef Is Nothing Then
'        Err.Raise mpError_InvalidMember, , xID & _
'            " is an invalid template."
'    End If
    
'   delete from  tblFavoriteTemplates
    xSQL = "DELETE '" & xID & "' FROM tblFavoriteTemplates"
    Application.PrivateDB.Execute xSQL
    Exit Sub
ProcError:
    RaiseError "mpDB.CApp.RemoveTemplateFavorite"
    Exit Sub
End Sub

Public Sub SetPrivateDBQuery(ByVal xSQL As String, ByVal xQueryDef As String)
'   resets SQL of private db
'   query def to specified SQL

    Dim oQDef As DAO.QueryDef
    
'   check for existence of query def
    On Error Resume Next
    Set oQDef = m_oDBPrivate.QueryDefs(xQueryDef)
    On Error GoTo 0
    
    If oQDef Is Nothing Then
'       query def does not exist - create new one
        Set oQDef = New DAO.QueryDef
        oQDef.Name = xQueryDef
        m_oDBPrivate.QueryDefs.Append oQDef
    End If
    
'   reset SQL of query to point to public db
    oQDef.SQL = xSQL
End Sub

Public Sub Initialize()
'initializes the app object - called by Sub Main
    Dim xPublicDB As String
    Dim xPeopleDB As String
    Dim oTableDef As DAO.TableDef
    Dim xPrivateDB As String
    Dim datLastCompact As Date
    Dim xCompactDate As String
    Dim xSQL As String
    Dim xConnect As String
    Dim oTD As TableDef
    Dim xManualEntryIDThreshold As String
    
    
    Set m_oError = New mpError.CError
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   get location of Public DB
    xPublicDB = mpBase2.PublicDB
    xPeopleDB = mpBase2.PeopleDB
    
'   check for missing INI keys
    If xPublicDB = "" Then
        Err.Raise mpError_InvalidPublicDB
    ElseIf xPeopleDB = "" Then
        Err.Raise mpError_invalidPeopleDB
    End If
    
'   get location of Private DB
    xPrivateDB = mpBase2.UserFilesDirectory & "mpPrivate.mdb"
    
    If Dir(xPublicDB, vbNormal) = "" Then
'       alert user and attempt to get local copy
        xPublicDB = GetLocalPublicDB()
    End If
    
    If xPublicDB = Empty Then
        Err.Raise mpError_UserOptsForNoPublicDB
    ElseIf Dir(xPublicDB, vbNormal) = "" Then
'       even local copy is bum - raise error
        Err.Raise mpError_InvalidPublicDB
    ElseIf Dir(xPrivateDB, vbNormal) = "" Then
        Err.Raise mpError_InvalidPrivateDB
    ElseIf Dir(xPeopleDB, vbNormal) = "" Then
        Err.Raise mpError_invalidPeopleDB
    End If
    
'   get mpPeople_ManualEntryIDThreshold
    
    xManualEntryIDThreshold = mpBase2.GetMacPacIniNumeric("General", "ManualEntryIDThreshold") '*c
    
    If xManualEntryIDThreshold <> "" Then
        g_lManualEntryIDThreshold = CLng(xManualEntryIDThreshold)
    Else
        g_lManualEntryIDThreshold = mpPeople_ManualEntryIDThreshold
    End If
    
    Err.Clear
    
'   get date of last compact of dbs
    xCompactDate = mpBase2.GetUserIniNumeric("General", "LastCompact") '*c
    
    If Len(xCompactDate) Then
        datLastCompact = CDate(xCompactDate)
        
'       get frequency of compacts
        Dim iCompactFreq As Integer
        
        On Error Resume Next
        iCompactFreq = mpBase2.GetMacPacIni("General", "CompactFreq")
        On Error GoTo ProcError

'       set frequency to 7 days if no frequency specified
        If iCompactFreq = 0 Then
            iCompactFreq = 7
        End If
        
        If (Now >= datLastCompact + iCompactFreq) Then
'           compact private db every week
            CompactDBs xPrivateDB
            
'           set current date as last compact date
            SetUserIni "General", "LastCompact", CDbl(Now)
        End If
    Else
'       db has not been compacted yet -
'       set last compact date to today
        SetUserIni "General", "LastCompact", CDbl(Now)
    End If
    
'   connect to MacPac dbs
    With DBEngine(0)
        Set m_oDBPublic = .OpenDatabase(xPublicDB, , True)
        Set m_oDBPrivate = .OpenDatabase(xPrivateDB, , True)

        '9.7.1 - #2253
        If m_oDBPublic Is Nothing Then
            CompactDBPublic xPublicDB
        End If
        
        If m_oDBPrivate Is Nothing Then
            CompactDBs xPrivateDB
        End If
        
'       set sqls of private db queries that
'       get data from public db
        If Not (m_oDBPublic Is Nothing) Then
'           reset SQL of private db qryPeoplePublicInt
'           to point to public db
            xSQL = "SELECT * FROM qryPeoplePublic IN '" & _
                   xPublicDB & "';"

            SetPrivateDBQuery xSQL, "qryPeoplePublicInt"
            
'           reset SQL of private db qryTemplates
'           to point to public db
            xSQL = "SELECT DISTINCT* FROM ((tblTemplates LEFT JOIN " & _
                   "tblCustomDialogs ON tblTemplates.fldFileName = " & _
                   "tblCustomDialogs.fldTemplate) LEFT JOIN " & _
                   "tblTemplateAssignments ON tblTemplates.fldFileName = " & _
                   "tblTemplateAssignments.fldTemplateID) IN '" & _
                   xPublicDB & "';"
                   
            SetPrivateDBQuery xSQL, "qryTemplates"
            
'           refresh link to people tables in mpPeople.mdb
            xConnect = ";DATABASE=" & xPeopleDB
            RefreshLinkedTable xConnect, "tblPeopleMP"
            RefreshLinkedTable xConnect, "tblPeopleHub"
            RefreshLinkedTable xConnect, "tblAttyLicenses"
        End If
    End With
    
'   ensure that people and offices have been setup
    If Me.People.Count = 0 Then
        Err.Raise mpError_NoPeople
    ElseIf Me.Offices.Count = 0 Then
        Err.Raise mpError_NoOffices
    End If
    
'   mark successful initialization
    g_bInit = True
    Exit Sub
    
ProcError:
    RaiseError "mpDB.CApp.Initialize"
    Exit Sub
End Sub

Private Sub RefreshLinkedTable(ByVal xConnect As String, ByVal xTable As String)
    Dim oTD As DAO.TableDef
    
    On Error GoTo ProcError
    
'   check for existing link
    On Error Resume Next
    Set oTD = m_oDBPublic.TableDefs(xTable)
    On Error GoTo ProcError
    
'   add link if missing
    If oTD Is Nothing Then
'       create a new link
        Set oTD = m_oDBPublic.CreateTableDef(xTable)
        On Error Resume Next
        oTD.Connect = xConnect
        oTD.SourceTableName = xTable
        m_oDBPublic.TableDefs.Append oTD
        If Err.Number Then
            Err.Raise mpError_CreateLinkFailed
        End If
    Else
'       refresh existing link
        On Error Resume Next
        oTD.Connect = xConnect
        oTD.RefreshLink
        On Error GoTo ProcError
        If Err.Number Then
            Err.Raise mpError_RefreshLinkFailed
        End If
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.CApp.RefreshLinkedTable"
    Exit Sub
End Sub

Private Function CompactDBs(ByVal xPrivateDB As String, Optional ByVal xPublicDB As String)
'compacts private and public dbs if possible
    On Error Resume Next
'   replace private db with compacted version
    DBEngine.CompactDatabase xPrivateDB, _
        xPrivateDB & ".New", , dbVersion40
    
    If Err.Number = 0 Then
'       Replace old version
        On Error GoTo ProcError
        Kill xPrivateDB
        Name xPrivateDB & ".New" As xPrivateDB
    End If
    
'   do public db only if specified - public db
'   is typically read only, so it won't need to be
'   compacted
    If Len(xPublicDB) Then
        On Error Resume Next
'       replace public db with compacted version
        DBEngine.CompactDatabase xPublicDB, _
            xPublicDB & ".New", , dbVersion40
        
        If Err.Number = 0 Then
'           Replace old version
            On Error GoTo ProcError
            Kill xPublicDB
            Name xPublicDB & ".New" As xPublicDB
        Else
            On Error GoTo ProcError
        End If
    End If
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.CompactDBs"
    Exit Function
End Function

Private Function CompactDBPublic(ByVal xPublicDB As String)
'compacts private and public dbs if possible
    On Error Resume Next
'   replace private db with compacted version
    DBEngine.CompactDatabase xPublicDB, _
        xPublicDB & ".New", , dbVersion40
    
    If Err.Number = 0 Then
'       Replace old version
        On Error GoTo ProcError
        Kill xPublicDB
        Name xPublicDB & ".New" As xPublicDB
    End If
    
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.CompactDBPublic"
    Exit Function
End Function

Private Function GetCustomProperties()
'add members to custom properties collection of CApp
    Dim xSQL As String
    Dim rs As DAO.Recordset
    
    Set m_oCustProps = New CCustomProperties
    
'   get recordset that defines custom properties
    xSQL = "SELECT * FROM tblPeopleCustomProperties"
    Set rs = Application.PublicDB.OpenRecordset( _
        "tblPeopleCustomProperties", dbOpenForwardOnly, dbReadOnly)

'   if there are custom properties, fill custom properties
'   collection of person with values in table
    With rs
        If Not (.BOF And .EOF) Then
'           there are custom properties -

'           cycle through records, adding to collection
            While Not .EOF
                m_oCustProps.Add !fldFieldName, _
                                 !fldName
                If Err.Number = 3265 Then
                    Err.Raise mpError_InvalidCustomPersonField
                End If
                .MoveNext
            Wend
        End If
    End With
    rs.Close
    Set rs = Nothing
End Function

Private Sub Class_Terminate()
' some objects may not exist, so just skip over
    On Error Resume Next
    Set m_oError = Nothing
    Set m_oDBPublic = Nothing
    Set m_oDBPrivate = Nothing
    Set m_oLists = Nothing
    Set m_Offices = Nothing
    Set m_oPeople = Nothing
    Set m_oPeopleFirm = Nothing
    Set m_oPeopleInput = Nothing
    Set m_oPeopleFavorites = Nothing
    Set m_oAttorneys = Nothing
    Set m_oCustProps = Nothing
End Sub

Friend Function PublicDB() As DAO.Database
    Set PublicDB = m_oDBPublic
End Function

Friend Function PrivateDB() As DAO.Database
    Set PrivateDB = m_oDBPrivate
End Function

Friend Function TemplateDefinitions() As mpDB.CTemplateDefs
    If (m_oTemplateDefs Is Nothing) Then
        Set m_oTemplateDefs = New mpDB.CTemplateDefs
    End If
    Set TemplateDefinitions = m_oTemplateDefs
End Function

Friend Function TemplateGroups() As mpDB.CTemplateGroups
    If (m_oTemplateGroups Is Nothing) Then
        Set m_oTemplateGroups = New mpDB.CTemplateGroups
    End If
    Set TemplateGroups = m_oTemplateGroups
End Function

Friend Function People() As mpDB.CPersons
'   used to write display favorites - create and
'   fill if object doesn't already exist
    If (m_oPeople Is Nothing) Then
        Set m_oPeople = New mpDB.CPersons
        m_oPeople.Refresh
    End If
    
    Set People = m_oPeople
End Function

Friend Function Attorneys() As mpDB.CPersons
'  used to display attorneys - create and
'   fill if object doesn't already exist
    If (m_oAttorneys Is Nothing) Then
        Set m_oAttorneys = New mpDB.CPersons
        With m_oAttorneys
            .PersonType = mpPersonType_AllAttorneys
            .Refresh
        End With
    End If
    Set Attorneys = m_oAttorneys
    Exit Function
ProcError:
    RaiseError "mpDB.CApp.Attorneys"
    Exit Function
End Function

Friend Function LetterheadDefs(Optional ByVal xTemplateID As String) As mpDB.CLetterheadDefs
'create and fill if object doesn't already exist
    Static vTemplatePrev As Variant
    Static bPrevSet As Boolean

'   refresh only if a different template's
'   letterheads are being requested
    If (vTemplatePrev <> xTemplateID) Or (Not bPrevSet) Then
        Set m_oLHDefs = New mpDB.CLetterheadDefs
        m_oLHDefs.Refresh xTemplateID
        vTemplatePrev = xTemplateID
        bPrevSet = True
    End If
    Set LetterheadDefs = m_oLHDefs
End Function
Friend Function DocumentInsertDefs(Optional ByVal xTemplateID As String) As mpDB.CDocumentInsertDefs
'create and fill if object doesn't already exist
    Static vTemplatePrev As Variant
    Static bPrevSet As Boolean

'   refresh only if a different template's
'   inserts are being requested
    If (vTemplatePrev <> xTemplateID) Or (Not bPrevSet) Then
        Set m_oDocInsertDefs = New mpDB.CDocumentInsertDefs
        m_oDocInsertDefs.Refresh xTemplateID
        vTemplatePrev = xTemplateID
        bPrevSet = True
    End If
    Set DocumentInsertDefs = m_oDocInsertDefs
End Function

Friend Function FirmPeople() As mpDB.CPersons
'  used to write changes to tblPeople - create and
'   fill if object doesn't already exist
    If (m_oPeopleFirm Is Nothing) Then
        Set m_oPeopleFirm = New mpDB.CPersons
        With m_oPeopleFirm
            .PersonType = mpPersonType_Firm
            .Refresh
        End With
    End If
    Set FirmPeople = m_oPeopleFirm
End Function

Friend Function InputPeople() As mpDB.CPersons
'   always refresh InputPeople
    If m_oPeopleInput Is Nothing Then
        Set m_oPeopleInput = New mpDB.CPersons
    With m_oPeopleInput
            .PersonType = mpPersonType_Input
            .Refresh
    End With
    End If
    
    Set InputPeople = m_oPeopleInput
End Function

Friend Function FavoritePeople() As mpDB.CPersons
'   always refresh favorites
    Set m_oPeopleFavorites = New mpDB.CPersons
    With m_oPeopleFavorites
        .PersonType = mpPersonType_Favorite
        .Refresh
    End With
    
    Set FavoritePeople = m_oPeopleFavorites
End Function

Friend Function Lists() As CLists
'create and fill if object doesn't already exist
    If (m_oLists Is Nothing) Then
        Set m_oLists = New mpDB.CLists
        m_oLists.Refresh
    End If
    
    Set Lists = m_oLists
End Function

Friend Function Offices() As mpDB.COffices
'create and fill if object doesn't already exist
    If (m_Offices Is Nothing) Then
        Set m_Offices = New mpDB.COffices
        m_Offices.Refresh
    End If
    
    Set Offices = m_Offices
End Function

Friend Function EnvelopeDefs() As mpDB.CEnvelopeDefs
'create and fill if object doesn't already exist
    If (m_oEnvDefs Is Nothing) Then
        Set m_oEnvDefs = New mpDB.CEnvelopeDefs
        m_oEnvDefs.Refresh
    End If
    
    Set EnvelopeDefs = m_oEnvDefs
End Function

Friend Function LabelDefs() As mpDB.CLabelDefs
'create and fill if object doesn't already exist
    If (m_oLabelDefs Is Nothing) Then
        Set m_oLabelDefs = New mpDB.CLabelDefs
        m_oLabelDefs.Refresh
    End If
    
    Set LabelDefs = m_oLabelDefs
End Function

Friend Function SetDefaultAuthor(lID As Long, xTemplate As String) As mpDB.CPerson
'sets person with ID = lID from Source
    Dim i As Integer
    Dim iSource As Integer
    Dim xSQL As String
    Dim rDefaultAuthors As DAO.Recordset
    Dim xControlName As String
    
    On Error GoTo bSetDefaultAuthor_Error
    
'   retrieve record from appropriate options table
    xSQL = "SELECT * FROM tblTemplateDefAuthors"
    xSQL = xSQL & " WHERE fldTemplate = '" & xTemplate & "'"
    
    Set rDefaultAuthors = g_App.PrivateDB.OpenRecordset(xSQL)
    
'   if record exists, update record,
'   else add new record
    With rDefaultAuthors
        If .RecordCount > 0 Then
            .Edit
        Else
            .AddNew
        End If
        
        On Error Resume Next
'       source can always be determined from ID - those
'       IDs at or above the threshold are manual entry
        If lID >= g_lManualEntryIDThreshold Then
            iSource = mpPeopleSourceList_PeopleInput
        Else
            iSource = mpPeopleSourceList_People
        End If
        
        .Fields(0) = xTemplate
        .Fields(1) = lID
        .Update
    End With
    Set SetDefaultAuthor = Application.People(lID)
    Exit Function
    
bSetDefaultAuthor_Error:
    Err.Raise Err.Number, _
              "mpDB.CApp.SetDefaultAuthor", _
              Application.Error.Desc(Err.Number)
    SetDefaultAuthor = Nothing
    Exit Function
End Function
Friend Function GetDefaultAuthor(xTemplate As String) As mpDB.CPerson
'returns the person listed as the default author for template xTemplate
    Dim xSQL As String
    Dim rs As DAO.Recordset
    
    xSQL = "SELECT fldDefaultAuthor FROM tblTemplateDefAuthors " & _
           "WHERE fldTemplate = '" & xTemplate & "'"
    Set rs = Application.PrivateDB.OpenRecordset( _
            xSQL, dbOpenForwardOnly, dbReadOnly)
    
    If rs.RecordCount Then
        Set GetDefaultAuthor = Application.People.Item(rs!fldDefaultAuthor)
    Else
        Set GetDefaultAuthor = Nothing
    End If
    rs.Close
End Function

Public Function lGetTableArray(dbMP As DAO.Database, _
                               xTableName As String, _
                               x_array() As String, _
                               Optional vWhere As Variant, _
                               Optional vSort As Variant) As Long
                     
'fills array x_Array with records in table xTable

    Dim xSQL As String
    Dim rDBTable As DAO.Recordset
    
    xSQL = "SELECT * FROM " & xTableName
    If Not IsMissing(vWhere) Then _
        xSQL = xSQL & " WHERE " & vWhere
    If Not IsMissing(vSort) Then _
        xSQL = xSQL & " ORDER BY " & vSort
        
    Set rDBTable = dbMP.OpenRecordset(xSQL)
    
    lGetTableArray = Me.lGetRecordsetArray(rDBTable, _
                                                   x_array())
End Function

Public Function lGetSQLArray(dbMP As DAO.Database, _
                     xSQL As String, _
                     x_array() As String) As Long
                     
'fills array x_array with
'records from SQL statement xSQL
    Dim rDBTable As DAO.Recordset
    
    Set rDBTable = dbMP.OpenRecordset(xSQL)
    
    lGetSQLArray = Me.lGetRecordsetArray(rDBTable, _
                                                 x_array())
    
End Function


Public Function lGetRecordsetArray(rExisting As DAO.Recordset, _
                            x_array() As String) As Long
'fills array x_Array with records
'in recordset rExisting

    Dim lNumFields As Long
    Dim lNumRows As Long
    Dim i As Integer
    Dim j As Integer
    
    With rExisting
        lNumFields = .Fields.Count
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            ReDim x_array(lNumRows - 1, lNumFields - 1)
            For i = 0 To lNumRows - 1
                For j = 0 To lNumFields - 1
                    .AbsolutePosition = i
                    x_array(i, j) = xNullToString(.Fields(j))
                Next j
            Next i
        Else
            ReDim x_array(0, lNumFields - 1)
        End If
    End With
    lGetRecordsetArray = lNumRows
End Function

Public Function xarGetTableXArray(dbMP As DAO.Database, _
                        xTableName As String, _
                        Optional vWhere As Variant, _
                        Optional vSort As Variant) As XArray
                     
'fills array xArray with
'records in table xTable

    Dim xSQL As String
    Dim rDBTable As DAO.Recordset
    
    xSQL = "SELECT * FROM " & xTableName
    If Not IsMissing(vWhere) Then _
        xSQL = xSQL & " WHERE " & vWhere
    If Not IsMissing(vSort) Then _
        xSQL = xSQL & " ORDER BY " & vSort
        
    Set rDBTable = dbMP.OpenRecordset(xSQL)
    
    Set xarGetTableXArray = Me.xarGetRecordsetArray(rDBTable)
End Function

Public Function xarGetSQLXArray(dbMP As DAO.Database, xSQL As String) As XArray
    Dim rs As DAO.Recordset
    Set rs = dbMP.OpenRecordset(xSQL)
    Set xarGetSQLXArray = Me.xarGetRecordsetArray(rs)
End Function


Public Function xarGetRecordsetArray(rExisting As DAO.Recordset) As XArray
'fills array xArray with records
'in recordset rExisting

    Dim lNumFields As Long
    Dim lNumRows As Long
    Dim i As Integer
    Dim j As Integer
    Dim xarTemp As XArray
    
    Set xarTemp = New XArrayObject.XArray
    
    With rExisting
        lNumFields = .Fields.Count
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            xarTemp.ReDim 0, lNumRows - 1, 0, lNumFields - 1
            For i = 0 To lNumRows - 1
                For j = 0 To lNumFields - 1
                    .AbsolutePosition = i
                    xarTemp.Value(i, j) = xNullToString(.Fields(j))
                Next j
            Next i
        Else
            xarTemp.ReDim 0, -1, 0, lNumFields - 1
        End If
    End With
    Set xarGetRecordsetArray = xarTemp
End Function

Public Function OptionsHaveChanged(ByVal xTemplate As String) As Boolean

End Function

Private Function GetLocalPublicDB() As String
'prompts user to use local copy of mpPublic.mdb
'if possible - returns local copy if user says ok
    Dim bLocalCopyExists As Boolean
    Dim xLocalCopy As String
    Dim iChoice As Integer
    Dim xMsg As String
    
'   check for copy of public db
'   in firm directory
    xLocalCopy = mpBase2.ApplicationDirectory & "\mpPublic.mdb"
    bLocalCopyExists = (Dir(xLocalCopy) <> "")
    If bLocalCopyExists Then
        xMsg = "Could not connect to the Public database.  " & _
               "Would you like to attempt to connect to a " & _
               "backup copy of the Public database?"
        iChoice = MsgBox(xMsg, vbQuestion + vbYesNo)
        
        If iChoice = vbYes Then
            GetLocalPublicDB = xLocalCopy
        End If
    End If
    Exit Function
End Function

Friend Function PleadingSeparateStatementDefs() As mpDB.CPleadingSeparateStatementDefs
    On Error GoTo ProcError
    If (m_oPleadingSeparateStatementDefs Is Nothing) Then
        Set m_oPleadingSeparateStatementDefs = New mpDB.CPleadingSeparateStatementDefs
        m_oPleadingSeparateStatementDefs.Refresh
    End If
    
    Set PleadingSeparateStatementDefs = m_oPleadingSeparateStatementDefs
    Exit Function
ProcError:
    RaiseError "mpDB.Capp.PleadingSeparateStatementDefs"
    Exit Function
End Function
