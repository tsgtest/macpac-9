VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentLookDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CDocumentLooks Class
'   created 3/7/00 by Dan Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage the MacPac Document Looks collection
'**********************************************************
Option Explicit

Enum mpDocumentLookTypes
    mpDocumentLookType_All = 1
    mpDocumentLookType_Private = 2
    mpDocumentLookType_Public = 3
End Enum

Private m_oDef As mpDB.CDocumentLookDef
Private m_oRS As DAO.Recordset
Private m_oArray As XArray
Private m_oDB As DAO.Database
Private m_lType As Long

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oArray
End Property

Public Function Add(ByVal xName As String) As mpDB.CDocumentLookDef
'creates a new document look
    Set m_oDef = New mpDB.CDocumentLookDef
    With m_oRS
'       add new record
        .AddNew
        
'       set name and default parameters
        !fldName = xName
        !fldAlignment = 0
        !fldBottomMargin = 0.5
        !fldColumns = 1
        !fldLine1Indent = 0.5
        !fldFontName = "Times New Roman"
        !fldFontSize = 12
        !fldLeftMargin = 0.5
        !fldLineSpacing = 0
        !fldPageNumAlignment = 1
        !fldPageNumPage1 = 0
        !fldPageNumFormat = 1
        !fldPageNumLocation = 1
        !fldPageNumTextAfter = "."
        !fldDiffPage1HF = -1
        !fldTopMargin = 0.5
        !fldRightMargin = 0.5
        !fldSpaceAfter = 0
        !fldSpaceBefore = 12
        !fldColSpace = 0.25
        
'       save record
        .Update
        
'       update object with values from new record
        .MoveLast
        UpdateObject
        
'       return object
        Set Add = m_oDef
    End With
    
'   refresh the collection of document looks
    Refresh mpDocumentLookType_Private
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function

Public Function Item(vID As Variant) As CDocumentLookDef
Attribute Item.VB_UserMemId = 0
'returns the document look whose id or name = vid
    Dim xCrit As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    If IsNumeric(vID) Then
        xCrit = "fldID = " & vID
    Else
        xCrit = "fldName = '" & vID & "'"
    End If
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCrit
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CDocumentLooks.Item"
    Exit Function
End Function

Public Sub Delete(lID As Long)
'delete the specified document look
    On Error GoTo ProcError
    Item lID
    m_oRS.Delete
    
'   refresh the collection of document looks
    Refresh mpDocumentLookType_Private
    Exit Sub
ProcError:
    RaiseError "mpDB.CDocumentLookDefs.Delete"
    Exit Sub
End Sub

Public Sub Save()
'save the current record - create
'a new one if the current object
'has no ID
    Dim xDesc As String
    
    With m_oRS
        If .Updatable Then
'           edit/add record
            If m_oDef.ID <> Empty Then
                .Edit
                UpdateRecordset
                .Update
            Else
                .AddNew
                UpdateRecordset
                .Update
                m_oDef.ID = !fldID
            End If
        Else
'           recordset is not updatable
            xDesc = "Public Document Look Definitions " & _
                    "can't be saved using this method."
            Err.Raise mpError_CollectionNotUpdatable
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CDocumentLookDefs.Save", xDesc
End Sub

Public Sub Refresh(iType As mpDocumentLookTypes)
'get doclookdefs recordset - different types -
'private ones come from mpPrivate.mdb.
    Dim xSQL As String
    
    On Error GoTo ProcError
    Select Case iType
        Case mpDocumentLookType_Private
            xSQL = "SELECT * FROM tblDocumentLookTypes"
            Set m_oDB = Application.PrivateDB
        Case mpDocumentLookType_Public
            xSQL = "SELECT * FROM tblDocumentLookTypes"
            Set m_oDB = Application.PublicDB
        Case mpDocumentLookType_All
            xSQL = "SELECT * FROM tblDocumentLookTypes UNION " & _
                   "SELECT * FROM qryDocumentLookTypes"
            Set m_oDB = Application.PrivateDB
    End Select
    
    Set m_oRS = m_oDB.OpenRecordset(xSQL, dbOpenDynaset)
    
    With m_oRS
        If .RecordCount Then
'           populate
            .MoveLast
            .MoveFirst
'           refresh the list source
            RefreshXArray
        End If
    End With
    m_lType = iType
    Exit Sub
ProcError:
    RaiseError "mpDB.CDocumentLooks.Refresh"
    Exit Sub
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xName As String
    
    On Error GoTo ProcError
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
'           populate if necessary
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            
'           create storage
            Set m_oArray = New XArray
            m_oArray.ReDim 0, lNumRows - 1, 0, 1
            
'           cycle through records, storing values in array
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                
                xID = Empty
                xName = Empty
                
                On Error Resume Next
                xID = !fldID
                xName = !fldName
                m_oArray.Value(i, 0) = xID
                m_oArray.Value(i, 1) = xName
                On Error GoTo ProcError
            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CDocumentLookDefs.RefreshXArray"
End Sub

Private Sub UpdateObject()
    Set m_oDef = New CDocumentLookDef
    With m_oDef
        .Alignment = Empty
        .BottomMargin = Empty
        .Columns = Empty
        .FirstLineIndent = Empty
        .FontName = Empty
        .FontSize = Empty
        .LeftMargin = Empty
        .LineSpacing = Empty
        .PageNumAlignment = Empty
        .PageNumFirstPage = Empty
        .PageNumFormat = Empty
        .PageNumLocation = Empty
        .PageNumTextAfter = Empty
        .PageNumTextBefore = Empty
        .DifferentPage1HeaderFooter = Empty
        .SpaceAfter = Empty
        .SpaceBefore = Empty
        .SpaceBetweenColumns = Empty
        .ID = Empty
        
        On Error Resume Next
        .ID = m_oRS!fldID
        .Alignment = m_oRS!fldAlignment
        .BottomMargin = m_oRS!fldBottomMargin
        .Columns = m_oRS!fldColumns
        .FirstLineIndent = m_oRS!fldLine1Indent
        .FontName = m_oRS!fldFontName
        .FontSize = m_oRS!fldFontSize
        .LeftMargin = m_oRS!fldLeftMargin
        .LineSpacing = m_oRS!fldLineSpacing
        .PageNumAlignment = m_oRS!fldPageNumAlignment
        .PageNumFirstPage = m_oRS!fldPageNumPage1
        .PageNumFormat = m_oRS!fldPageNumFormat
        .PageNumLocation = m_oRS!fldPageNumLocation
        .PageNumTextAfter = m_oRS!fldPageNumTextAfter
        .PageNumTextBefore = m_oRS!fldPageNUmTextBefore
        .DifferentPage1HeaderFooter = m_oRS!fldDiffPage1HF
        .TopMargin = m_oRS!fldTopMargin
        .RightMargin = m_oRS!fldRightMargin
        .SpaceAfter = m_oRS!fldSpaceAfter
        .SpaceBefore = m_oRS!fldSpaceBefore
        .SpaceBetweenColumns = m_oRS!fldColSpace
    End With
End Sub

Private Sub UpdateRecordset()
    With m_oDef
        m_oRS!fldAlignment = .Alignment
        m_oRS!fldBottomMargin = .BottomMargin
        m_oRS!fldColumns = .Columns
        m_oRS!fldLine1Indent = .FirstLineIndent
        m_oRS!fldFontName = .FontName
        m_oRS!fldFontSize = .FontSize
        m_oRS!fldLeftMargin = .LeftMargin
        m_oRS!fldLineSpacing = .LineSpacing
        m_oRS!fldPageNumAlignment = .PageNumAlignment
        m_oRS!fldPageNumPage1 = .PageNumFirstPage
        m_oRS!fldPageNumFormat = .PageNumFormat
        m_oRS!fldPageNumLocation = .PageNumLocation
        m_oRS!fldPageNumTextAfter = .PageNumTextAfter
        m_oRS!fldPageNUmTextBefore = .PageNumTextBefore
        m_oRS!fldDiffPage1HF = .DifferentPage1HeaderFooter
        m_oRS!fldTopMargin = .TopMargin
        m_oRS!fldRightMargin = .RightMargin
        m_oRS!fldSpaceAfter = .SpaceAfter
        m_oRS!fldSpaceBefore = .SpaceBefore
        m_oRS!fldColSpace = .SpaceBetweenColumns
    End With
End Sub

Private Sub Class_Terminate()
    Set m_oDef = Nothing
    Set m_oRS = Nothing
    Set m_oDB = Nothing
End Sub
