VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentLooks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CDocumentLooks Class
'   created 3/7/00 by Dan Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage the MacPac Document Looks collection
'**********************************************************
Option Explicit

Private m_oLook As mpDB.CDocumentLook
Private m_oRS As DAO.Recordset

Public Function Add() As mpDB.CDocumentLook
    Set m_oLook = New mpDB.CDocumentLook
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function

Public Function Item(vID As Variant) As CDocumentLook
    Dim xCrit As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    If IsNumeric(vID) Then
        xCrit = "fldID = " & vID
    Else
        xCrit = "fldName = '" & vID & "'"
    End If
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCrit
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oLook
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CDocumentLooks.Item"
    Exit Function
End Function

Public Sub Delete(lID As Long)

End Sub

Public Sub Save()

End Sub

Public Sub Refresh()
    Dim xSQL As String
    On Error GoTo ProcError
    xSQL = "SELECT * FROM tblDocumentLooks"
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CDocumentLooks.Refresh"
    Exit Sub
End Sub

Private Sub UpdateObject()
    Set m_oLook = New CDocumentLook
    With m_oLook
        .Alignment = Empty
        .BottomMargin = Empty
        .Columns = Empty
        .FirstLineIndent = Empty
        .FontName = Empty
        .FontSize = Empty
        .LeftMargin = Empty
        .LineSpacing = Empty
        .PageNumAlignment = Empty
        .PageNumFirstPage = Empty
        .PageNumFormat = Empty
        .PageNumLocation = Empty
        .PageNumTextAfter = Empty
        .PageNumTextBefore = Empty
        .DifferentPage1HeaderFooter = Empty
        
        .Alignment = m_oRS!fldAlignment
        .BottomMargin = m_oRS!fldBottomMargin
        .Columns = m_oRS!fldColumns
        .FirstLineIndent = m_oRS!fldLine1Indent
        .FontName = m_oRS!fldFontName
        .FontSize = m_oRS!fldFontSize
        .LeftMargin = m_oRS!fldLeftMargin
        .LineSpacing = m_oRS!fldLineSpacing
        .PageNumAlignment = m_oRS!fldPageNumAlignment
        .PageNumFirstPage = m_oRS!fldPageNumPage1
        .PageNumFormat = m_oRS!fldPageNumFormat
        .PageNumLocation = m_oRS!fldPageNumLocation
        .PageNumTextAfter = m_oRS!fldPageNumTextAfter
        .PageNumTextBefore = m_oRS!fldPageNUmTextBefore
        .DifferentPage1HeaderFooter = m_oRS!fldDiffPage1HF
    End With
End Sub

Private Sub UpdateRecordset()

End Sub

Private Sub Class_Terminate()
    Set m_oLook = Nothing
    Set m_oRS = Nothing
End Sub
