VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCounselType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Pleading Caption Type Class
'   created 1/2/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define macpac letterhead

'   Member of
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_xBookmark As String
Private m_bAllowFirmSlogan As Boolean
Private m_bAllowAdditionalFirmSlogan As Boolean
Private m_bAllowAdditionalFirmAddress As Boolean
Private m_bAllowAdditionalFirmPhone As Boolean
Private m_bAllowAdditionalFirmFax As Boolean



'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property

Public Property Let Bookmark(xNew As String)
    m_xBookmark = xNew
End Property

Public Property Get Bookmark() As String
    Bookmark = m_xBookmark
End Property

Public Property Let AllowAdditionalFirmPhone(bNew As Boolean)
    m_bAllowAdditionalFirmPhone = bNew
End Property

Public Property Get AllowAdditionalFirmPhone() As Boolean
    AllowAdditionalFirmPhone = m_bAllowAdditionalFirmPhone
End Property

Public Property Let AllowAdditionalFirmSlogan(bNew As Boolean)
    m_bAllowAdditionalFirmSlogan = bNew
End Property

Public Property Get AllowAdditionalFirmSlogan() As Boolean
    AllowAdditionalFirmSlogan = m_bAllowAdditionalFirmSlogan
End Property

Public Property Let AllowAdditionalFirmFax(bNew As Boolean)
    m_bAllowAdditionalFirmFax = bNew
End Property

Public Property Get AllowAdditionalFirmFax() As Boolean
    AllowAdditionalFirmFax = m_bAllowAdditionalFirmFax
End Property

Public Property Let AllowFirmSlogan(bNew As Boolean)
    m_bAllowFirmSlogan = bNew
End Property

Public Property Get AllowFirmSlogan() As Boolean
    AllowFirmSlogan = m_bAllowFirmSlogan
End Property

Public Property Let AllowAdditionalFirmAddress(bNew As Boolean)
    m_bAllowAdditionalFirmAddress = bNew
End Property

Public Property Get AllowAdditionalFirmAddress() As Boolean
    AllowAdditionalFirmAddress = m_bAllowAdditionalFirmAddress
End Property

