VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CServiceDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CServiceDef Class
'   created 5/16/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define
'   a type of MacPac Service Document (POS/COS)

'   Member of CServiceDefs
'   Container for COfficeAddressFormat
'**********************************************************

Private m_lID As Long
Private m_iLevel0 As Integer
Private m_xServiceType As String
Private m_xBPFile As String
Private m_xDefExecuteDate As String
Private m_xDefServiceDate As String
Private m_bytFooterTextMax As Byte
Private m_lDefaultPleadingPaperType As Long
Private m_bAllowServiceList As Boolean
Private m_bAllowServiceTitle As Boolean
Private m_bAllowPartyDescription As Boolean
Private m_bAllowPartyTitle As Boolean
Private m_bAllowCourtTitle As Boolean
Private m_bAllowCaseNumber As Boolean
Private m_bAllowManualInput As Boolean
Private m_bDefToManualInput As Boolean
Private m_bUpperCaseFirmName As Boolean
Private m_bAllowGender As Boolean
Private m_bAllowExecuteDate As Boolean
Private m_bAllowServiceDate As Boolean
Private m_bAllowManualCompany As Boolean
Private m_bAllowManualAddress As Boolean
Private m_bAllowManualCity As Boolean
Private m_bAllowManualState As Boolean
Private m_bAllowManualCounty As Boolean
Private m_bAllowDoNoIncludeServiceTitle As Boolean
Private m_xDlgCaption As String
Private m_xDesc As String
Private m_xDefaultFooterText As String
Private m_bAllowCounty As Boolean
Private m_lDateFormats As Long
Private m_xCustom1Label As String
Private m_xCustom1Text As String
Private m_xCustom2Label As String
Private m_xCustom2Text As String
Private m_xCustom3Label As String
Private m_xCustom3Text As String
Private m_xCustom4Label As String
Private m_xCustom4Text As String
Private m_lDefServiceListType As Long
Private m_oAddressFormat As mpDB.COfficeAddressFormat
Private m_bUseBoilerplateFooterSetup As Boolean
Private m_bytFirstNumberedPage As Byte
Private m_bytFirstPageNumber As Byte
Private m_xServiceTitles As String
Private m_xESignatureBoilerplate As String

'CharlieCore 9.2.0 - BaseStyle
Private m_xBaseStyle As String

Friend Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let DateFormats(lNew As Long)
    m_lDateFormats = lNew
End Property

Public Property Get DateFormats() As Long
    DateFormats = m_lDateFormats
End Property

Friend Property Let ServiceTitles(xNew As String)
    m_xServiceTitles = xNew
End Property

Public Property Get ServiceTitles() As String
    ServiceTitles = m_xServiceTitles
End Property

Public Property Let FirstNumberedPage(bytNew As Byte)
    m_bytFirstNumberedPage = bytNew
End Property

Public Property Get FirstNumberedPage() As Byte
    FirstNumberedPage = m_bytFirstNumberedPage
End Property

Public Property Let FirstPageNumber(bytNew As Byte)
    m_bytFirstPageNumber = bytNew
End Property

Public Property Get FirstPageNumber() As Byte
    FirstPageNumber = m_bytFirstPageNumber
End Property

Public Property Let UseBoilerplateFooterSetup(bNew As Boolean)
    m_bUseBoilerplateFooterSetup = bNew
End Property

Public Property Get UseBoilerplateFooterSetup() As Boolean
    UseBoilerplateFooterSetup = m_bUseBoilerplateFooterSetup
End Property

Friend Property Let DefaultServiceList(lNew As Long)
    m_lDefServiceListType = lNew
End Property

Public Property Get DefaultServiceList() As Long
    DefaultServiceList = m_lDefServiceListType
End Property

Friend Property Set AddressFormat(oNew As mpDB.COfficeAddressFormat)
    Set m_oAddressFormat = oNew
End Property

Public Property Get AddressFormat() As mpDB.COfficeAddressFormat
    Set AddressFormat = m_oAddressFormat
End Property

Friend Property Let Level0(iNew As Integer)
    m_iLevel0 = iNew
End Property

Public Property Get Level0() As Integer
    Level0 = m_iLevel0
End Property

Friend Property Let ServiceType(iNew As Integer)
    m_xServiceType = iNew
End Property

Public Property Get ServiceType() As Integer
    ServiceType = m_xServiceType
End Property

Friend Property Let FooterTextMaxChars(lNew As Long)
    m_bytFooterTextMax = lNew
End Property

Public Property Get FooterTextMaxChars() As Long
    FooterTextMaxChars = m_bytFooterTextMax
End Property

Friend Property Let UpperCaseFirmName(bNew As Boolean)
    m_bUpperCaseFirmName = bNew
End Property

Public Property Get UpperCaseFirmName() As Boolean
    UpperCaseFirmName = m_bUpperCaseFirmName
End Property

Friend Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Friend Property Let DefaultFooterText(xNew As String)
    m_xDefaultFooterText = xNew
End Property

Public Property Get DefaultFooterText() As String
    DefaultFooterText = m_xDefaultFooterText
End Property

Friend Property Let DefaultServiceDate(xNew As String)
    m_xDefServiceDate = xNew
End Property

Public Property Get DefaultServiceDate() As String
    DefaultServiceDate = m_xDefServiceDate
End Property

Friend Property Let DefaultExecuteDate(xNew As String)
    m_xDefExecuteDate = xNew
End Property

Public Property Get DefaultExecuteDate() As String
    DefaultExecuteDate = m_xDefExecuteDate
End Property

Friend Property Let AllowServiceTitle(bNew As Boolean)
    m_bAllowServiceTitle = bNew
End Property

Public Property Get AllowServiceTitle() As Boolean
    AllowServiceTitle = m_bAllowServiceTitle
End Property

Friend Property Let AllowPartyDescription(bNew As Boolean)
    m_bAllowPartyDescription = bNew
End Property

Public Property Get AllowPartyDescription() As Boolean
    AllowPartyDescription = m_bAllowPartyDescription
End Property

Friend Property Let AllowPartyTitle(bNew As Boolean)
    m_bAllowPartyTitle = bNew
End Property

Public Property Get AllowPartyTitle() As Boolean
    AllowPartyTitle = m_bAllowPartyTitle
End Property

Friend Property Let AllowCourtTitle(bNew As Boolean)
    m_bAllowCourtTitle = bNew
End Property

Public Property Get AllowCourtTitle() As Boolean
    AllowCourtTitle = m_bAllowCourtTitle
End Property

Friend Property Let AllowCaseNumber(bNew As Boolean)
    m_bAllowCaseNumber = bNew
End Property

Public Property Get AllowCaseNumber() As Boolean
    AllowCaseNumber = m_bAllowCaseNumber
End Property

Friend Property Let AllowServiceList(bNew As Boolean)
    m_bAllowServiceList = bNew
End Property

Public Property Get AllowServiceList() As Boolean
    AllowServiceList = m_bAllowServiceList
End Property

Friend Property Let AllowGender(bNew As Boolean)
    m_bAllowGender = bNew
End Property

Public Property Get AllowGender() As Boolean
    AllowGender = m_bAllowGender
End Property

Friend Property Let AllowServiceDate(bNew As Boolean)
    m_bAllowServiceDate = bNew
End Property

Public Property Get AllowServiceDate() As Boolean
    AllowServiceDate = m_bAllowServiceDate
End Property

Friend Property Let AllowExecuteDate(bNew As Boolean)
    m_bAllowExecuteDate = bNew
End Property

Public Property Get AllowExecuteDate() As Boolean
    AllowExecuteDate = m_bAllowExecuteDate
End Property

Friend Property Let AllowCounty(bNew As Boolean)
    m_bAllowCounty = bNew
End Property

Public Property Get AllowCounty() As Boolean
    AllowCounty = m_bAllowCounty
End Property

Friend Property Let AllowManualInput(bNew As Boolean)
    m_bAllowManualInput = bNew
End Property

Public Property Get AllowManualInput() As Boolean
    AllowManualInput = m_bAllowManualInput
End Property

Friend Property Let AllowManualCompany(bNew As Boolean)
    m_bAllowManualCompany = bNew
End Property

Public Property Get AllowManualCompany() As Boolean
    AllowManualCompany = m_bAllowManualCompany
End Property

Friend Property Let AllowManualAddress(bNew As Boolean)
    m_bAllowManualAddress = bNew
End Property

Public Property Get AllowManualAddress() As Boolean
    AllowManualAddress = m_bAllowManualAddress
End Property

Friend Property Let AllowDoNoIncludeServiceTitle(bNew As Boolean)
    m_bAllowDoNoIncludeServiceTitle = bNew
End Property

Public Property Get AllowDoNoIncludeServiceTitle() As Boolean
    AllowDoNoIncludeServiceTitle = m_bAllowDoNoIncludeServiceTitle
End Property

Friend Property Let AllowManualCity(bNew As Boolean)
    m_bAllowManualCity = bNew
End Property

Public Property Get AllowManualCity() As Boolean
    AllowManualCity = m_bAllowManualCity
End Property

Friend Property Let AllowManualState(bNew As Boolean)
    m_bAllowManualState = bNew
End Property

Public Property Get AllowManualState() As Boolean
    AllowManualState = m_bAllowManualState
End Property

Friend Property Let AllowManualCounty(bNew As Boolean)
    m_bAllowManualCounty = bNew
End Property

Public Property Get AllowManualCounty() As Boolean
    AllowManualCounty = m_bAllowManualCounty
End Property

Friend Property Let DefaultToManualInput(bNew As Boolean)
    m_bDefToManualInput = bNew
End Property

Public Property Get DefaultToManualInput() As Boolean
    If Me.AllowManualInput Then
        DefaultToManualInput = m_bDefToManualInput
    End If
End Property

Friend Property Let DialogCaption(xNew As String)
    m_xDlgCaption = xNew
End Property

Public Property Get DialogCaption() As String
    DialogCaption = m_xDlgCaption
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Private Sub Class_Terminate()
    Set m_oAddressFormat = Nothing
End Sub
Public Property Get Custom1Label() As String
    Custom1Label = m_xCustom1Label
End Property
Public Property Let Custom1Label(xNew As String)
    m_xCustom1Label = xNew
End Property
Public Property Get Custom1DefaultText() As String
    Custom1DefaultText = m_xCustom1Text
End Property
Public Property Let Custom1DefaultText(xNew As String)
    m_xCustom1Text = xNew
End Property
Public Property Get Custom2Label() As String
    Custom2Label = m_xCustom2Label
End Property
Public Property Let Custom2Label(xNew As String)
    m_xCustom2Label = xNew
End Property
Public Property Get Custom2DefaultText() As String
    Custom2DefaultText = m_xCustom2Text
End Property
Public Property Let Custom2DefaultText(xNew As String)
    m_xCustom2Text = xNew
End Property
Public Property Get Custom3Label() As String
    Custom3Label = m_xCustom3Label
End Property
Public Property Let Custom3Label(xNew As String)
    m_xCustom3Label = xNew
End Property
Public Property Get Custom3DefaultText() As String
    Custom3DefaultText = m_xCustom3Text
End Property
Public Property Let Custom3DefaultText(xNew As String)
    m_xCustom3Text = xNew
End Property
Public Property Get Custom4Label() As String
    Custom4Label = m_xCustom4Label
End Property
Public Property Let Custom4Label(xNew As String)
    m_xCustom4Label = xNew
End Property
Public Property Get Custom4DefaultText() As String
    Custom4DefaultText = m_xCustom4Text
End Property
Public Property Let Custom4DefaultText(xNew As String)
    m_xCustom4Text = xNew
End Property
Public Property Get DefaultPleadingPaperType() As Long
    DefaultPleadingPaperType = m_lDefaultPleadingPaperType
End Property
Public Property Let DefaultPleadingPaperType(lNew As Long)
    m_lDefaultPleadingPaperType = lNew
End Property
'CharlieCore 9.2.0 - BaseStyle
Public Property Get BaseStyle() As String
    BaseStyle = m_xBaseStyle
End Property
Public Property Let BaseStyle(xNew As String)
    m_xBaseStyle = xNew
End Property
Public Property Get ESignatureBoilerplate() As String
    ESignatureBoilerplate = m_xESignatureBoilerplate
End Property
Public Property Let ESignatureBoilerplate(xNew As String)
    m_xESignatureBoilerplate = xNew
End Property
