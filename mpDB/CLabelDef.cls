VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLabelDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Label Def Class
'   created 10/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define an office

'   Member of CEnvelopeDefs
'**********************************************************

'**********************************************************
Private m_vID As Variant
Private m_vLabelID As Variant
Private m_xName As String
Private m_xDescription As String
Private m_xTooltipText As String
Private m_iDown As Integer
Private m_iAcross As Integer
Private m_xBPRange As String
Private m_xBPRange97 As String
Private m_xBPFile As String
Private m_xBPFile97 As String
Private m_xBPGraphicRange As String
Private m_bSpacerRow As Boolean
Private m_bSpacerCol As Boolean
Private m_bHasGraphic As Boolean
Private m_bAllowDPhrases As Boolean
Private m_bAllowBarCode As Boolean
Private m_bAllowBillingNumber As Boolean
Private m_bAllowAuthorInitials As Boolean
Private m_bAllowOfficeAddress As Boolean
Private m_sLeftIndent As Single
Private m_sTopMargin As Single
Private m_lMaxLines As Integer
Private m_iVerticalAligment As Integer
Private m_sWidth As Single
Private m_sHeight As Single
Private m_sVerticalPitch As Single
Private m_sHorizontalPitch As Single
Private m_sSideMargin As Single
Private m_sPageWidth As Single
Private m_sPageHeight As Single
Private m_sBottomMargin As Single
Private m_sHeaderDistance As Single
Private m_sFooterDistance As Single
Private m_iPageOrientation As Single
Private m_bIsCustom As Boolean
Private m_bIsDotMatrix As Boolean
Private m_iReturnAddressFormat As Integer
Private m_bSplitDPhrases As Boolean '9.7.1 #4024
Private m_xDPhrase1Caption As String '9.7.1 #4024
Private m_xDPhrase2Caption As String '9.7.1 #4024
Private m_xLabelType As String
Private m_xLabelVendor As String


'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(vNew As Variant)
    m_vID = vNew
End Property

Public Property Get ID() As Variant
    ID = m_vID
End Property

Public Property Let LabelID(vNew As Variant)
    m_vLabelID = vNew
End Property

Public Property Get LabelID() As Variant
    LabelID = m_vLabelID
End Property
Public Property Get SplitDPhrases() As Boolean '9.7.1 #4024
    SplitDPhrases = m_bSplitDPhrases
End Property
Public Property Let SplitDPhrases(bNew As Boolean) '9.7.1 #4024
    m_bSplitDPhrases = bNew
End Property
Public Property Get DPhrase1Caption() As String '9.7.1 #4024
    DPhrase1Caption = m_xDPhrase1Caption
End Property
Public Property Let DPhrase1Caption(xNew As String) '9.7.1 #4024
    m_xDPhrase1Caption = xNew
End Property
Public Property Get DPhrase2Caption() As String '9.7.1 #4024
    DPhrase2Caption = m_xDPhrase2Caption
End Property
Public Property Let DPhrase2Caption(xNew As String) '9.7.1 #4024
    m_xDPhrase2Caption = xNew
End Property

Public Property Get LabelType() As String
    LabelType = m_xLabelType
End Property
Public Property Let LabelType(xNew As String)
    m_xLabelType = xNew
End Property
Public Property Get LabelVendor() As String
    LabelVendor = m_xLabelVendor
End Property
Public Property Let LabelVendor(xNew As String)
    m_xLabelVendor = xNew
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let Tooltip(xNew As String)
    m_xTooltipText = xNew
End Property

Public Property Get Tooltip() As String
    Tooltip = m_xTooltipText
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Public Property Let BoilerplateFile97(xNew As String)
    m_xBPFile97 = xNew
End Property

Public Property Get BoilerplateFile97() As String
    BoilerplateFile97 = m_xBPFile97
End Property

Public Property Let BoilerplateRange97(xNew As String)
    m_xBPRange97 = xNew
End Property

Public Property Get BoilerplateRange97() As String
    BoilerplateRange97 = m_xBPRange97
End Property

Public Property Let BoilerplateRange(xNew As String)
    m_xBPRange = xNew
End Property

Public Property Get BoilerplateRange() As String
    BoilerplateRange = m_xBPRange
End Property

Public Property Let BPGraphicRange(xNew As String)
    m_xBPGraphicRange = xNew
End Property

Public Property Get BPGraphicRange() As String
    BPGraphicRange = m_xBPGraphicRange
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTopMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTopMargin
End Property

Public Property Let Width(sNew As Single)
    m_sWidth = sNew
End Property

Public Property Get Width() As Single
    Width = m_sWidth
End Property

Public Property Let Height(sNew As Single)
    m_sHeight = sNew
End Property

Public Property Get Height() As Single
    Height = m_sHeight
End Property

Public Property Let LeftIndent(sNew As Single)
    m_sLeftIndent = sNew
End Property

Public Property Get LeftIndent() As Single
    LeftIndent = m_sLeftIndent
End Property

Public Property Let VerticalPitch(sNew As Single)
    m_sVerticalPitch = sNew
End Property

Public Property Get VerticalPitch() As Single
    VerticalPitch = m_sVerticalPitch
End Property

Public Property Let HorizontalPitch(sNew As Single)
    m_sHorizontalPitch = sNew
End Property

Public Property Get HorizontalPitch() As Single
    HorizontalPitch = m_sHorizontalPitch
End Property

Public Property Let NumberDown(iNew As Integer)
    m_iDown = iNew
End Property

Public Property Get NumberDown() As Integer
    NumberDown = m_iDown
End Property

Public Property Let VerticalAlignment(iNew As Integer)
    m_iVerticalAligment = iNew
End Property

Public Property Get VerticalAlignment() As Integer
    VerticalAlignment = m_iVerticalAligment
End Property

Public Property Let SideMargin(sNew As Single)
    m_sSideMargin = sNew
End Property

Public Property Get SideMargin() As Single
    SideMargin = m_sSideMargin
End Property

Public Property Let PageWidth(sNew As Single)
    m_sPageWidth = sNew
End Property

Public Property Get PageWidth() As Single
    PageWidth = m_sPageWidth
End Property

Public Property Let PageHeight(sNew As Single)
    m_sPageHeight = sNew
End Property

Public Property Get PageHeight() As Single
    PageHeight = m_sPageHeight
End Property

Public Property Let BottomMargin(sNew As Single)
    m_sBottomMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBottomMargin
End Property

Public Property Let HeaderDistance(sNew As Single)
    m_sHeaderDistance = sNew
End Property

Public Property Get HeaderDistance() As Single
    HeaderDistance = m_sHeaderDistance
End Property

Public Property Let FooterDistance(sNew As Single)
    m_sFooterDistance = sNew
End Property

Public Property Get FooterDistance() As Single
    FooterDistance = m_sFooterDistance
End Property

Public Property Let PageOrientation(iNew As Integer)
    m_iPageOrientation = iNew
End Property

Public Property Get PageOrientation() As Integer
    PageOrientation = m_iPageOrientation
End Property


Public Property Let NumberAcross(iNew As Integer)
    m_iAcross = iNew
End Property

Public Property Get NumberAcross() As Integer
    NumberAcross = m_iAcross
End Property

Public Property Let AllowDPhrases(bNew As Boolean)
    m_bAllowDPhrases = bNew
End Property

Public Property Get AllowDPhrases() As Boolean
    AllowDPhrases = m_bAllowDPhrases
End Property

Public Property Let AllowBarCode(bNew As Boolean)
    m_bAllowBarCode = bNew
End Property

Public Property Get AllowBarCode() As Boolean
    AllowBarCode = m_bAllowBarCode
End Property

Public Property Let HasGraphic(bNew As Boolean)
    m_bHasGraphic = bNew
End Property

Public Property Get HasGraphic() As Boolean
    HasGraphic = m_bHasGraphic
End Property

Public Property Let IsCustom(bNew As Boolean)
    m_bIsCustom = bNew
End Property

Public Property Get IsCustom() As Boolean
    IsCustom = m_bIsCustom
End Property

Public Property Let IsDotMatrix(bNew As Boolean)
    m_bIsDotMatrix = bNew
End Property

Public Property Get IsDotMatrix() As Boolean
    IsDotMatrix = m_bIsDotMatrix
End Property

Public Property Let ReturnAddressFormat(iNew As Integer)
    m_iReturnAddressFormat = iNew
End Property

Public Property Get ReturnAddressFormat() As Integer
    ReturnAddressFormat = m_iReturnAddressFormat
End Property

Public Property Let AllowBillingNumber(bNew As Boolean)
    m_bAllowBillingNumber = bNew
End Property

Public Property Get AllowBillingNumber() As Boolean
    AllowBillingNumber = m_bAllowBillingNumber
End Property

Public Property Let AllowAuthorInitials(bNew As Boolean)
    m_bAllowAuthorInitials = bNew
End Property

Public Property Get AllowAuthorInitials() As Boolean
    AllowAuthorInitials = m_bAllowAuthorInitials
End Property

Public Property Let AllowOfficeAddress(bNew As Boolean)
    m_bAllowOfficeAddress = bNew
End Property

Public Property Get AllowOfficeAddress() As Boolean
    AllowOfficeAddress = m_bAllowOfficeAddress
End Property

Public Property Let SpacerRow(bNew As Boolean)
    m_bSpacerRow = bNew
End Property

Public Property Get SpacerRow() As Boolean
    SpacerRow = m_bSpacerRow
End Property

Public Property Let SpacerCol(bNew As Boolean)
    m_bSpacerCol = bNew
End Property

Public Property Get SpacerCol() As Boolean
    SpacerCol = m_bSpacerCol
End Property

Public Property Let MaximumLines(lNew As Long)
    m_lMaxLines = lNew
End Property

Public Property Get MaximumLines() As Long
    MaximumLines = m_lMaxLines
End Property


'**********************************************************
'   Methods
'**********************************************************

Private Sub Class_Terminate()

End Sub



