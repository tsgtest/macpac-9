VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COfficeAddressFormats"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   COfficeAddressFormats Collection Class
'   created 2/2/2000 by Jeffrey Sweetland

'   Contains properties and methods that manage the
'   collection of COfficeAddressFormat

'   Member of
'   Container for COfficeAddressFormat
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_oDef As mpDB.COfficeAddressFormat
Private m_oArray As XArray
'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(vID As Variant) As mpDB.COfficeAddressFormat
Attribute Item.VB_UserMemId = 0
    Dim xCrit As String
    Dim vBkmk As Variant
    
    On Error GoTo ProcError
    
'   set criteria
    If IsNumeric(vID) Then
        xCrit = "fldID = " & vID
    Else
        xCrit = "fldName = '" & vID & "'"
    End If
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCrit
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
            Err.Raise mpError_InvalidMember
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.COfficeAddressFormats.Item"
    Exit Function
End Function
Public Sub Refresh(Optional ByVal bDoXarray As Boolean = True)
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get stamps
    xSQL = "SELECT * FROM tblOfficeAddressFormats " & _
           "ORDER BY fldName"
            
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    
'   populate the recordset
    With m_oRS
        If Not (.EOF And .BOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update xarray if specified
    If bDoXarray Then
        RefreshXArray
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.COfficeAddressFormats.Refresh"
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oDef
        .IncludeCountry = Empty
        .FirmName = Empty
        .FullStateName = Empty
        .ID = Empty
        .IncludeFax = Empty
        .IncludePhone = Empty
        .IncludeFaxCaption = Empty
        .IncludePhoneCaption = Empty
        .StandardAddress = Empty
        .Name = Empty
        .Separator = Empty
        .IncludeSlogan = Empty
        .TrailingSeparator = Empty
        .FaxCaptionSuffix = Empty
        .PhoneCaptionSuffix = Empty
        .Template = Empty
        
        On Error Resume Next
        
        .FirmName = m_oRS!fldFirmName
        .FullStateName = m_oRS!fldFullStateName
        .ID = m_oRS!fldID
        .IncludeFaxCaption = m_oRS!fldIncludeFaxCaption
        .IncludePhoneCaption = m_oRS!fldIncludePhoneCaption
        .IncludeFax = m_oRS!fldIncludeFax
        .IncludePhone = m_oRS!fldIncludePhone
        .StandardAddress = m_oRS!fldStandardAddress
        .Name = m_oRS!fldName
        .Separator = m_oRS!fldSeparator
        .IncludeSlogan = m_oRS!fldIncludeSlogan
        .TrailingSeparator = m_oRS!fldTrailingSeparator
        .FaxCaptionSuffix = m_oRS!fldFaxCaptionSuffix
        .PhoneCaptionSuffix = m_oRS!fldPhoneCaptionSuffix
        .IncludeCountry = m_oRS!fldIncludeCountry
        .Template = m_oRS!fldTemplate
        
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oDef = New mpDB.COfficeAddressFormat
    Set m_oArray = New XArrayObject.XArray
    m_oArray.ReDim 0, -1, 0, 1
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xName As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_oArray.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xName = !fldName
                m_oArray.Value(i, 0) = xID
                m_oArray.Value(i, 1) = xName
            
            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
End Sub



