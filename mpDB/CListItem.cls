VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CListItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CListItem Collection Class
'   created 11/29/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties/ methods of a ListItem

'   Member of CListItems
'**********************************************************

'**********************************************************
Private m_vID As Variant
Private m_xDisplayText As String
Private m_vSuppValue As Variant

Public Property Get ID() As Variant
    ID = m_vID
End Property

Public Property Let ID(vNew As Variant)
    m_vID = vNew
End Property

Public Property Get DisplayText() As String
    DisplayText = m_xDisplayText
End Property

Public Property Let DisplayText(xNew As String)
    m_xDisplayText = xNew
End Property

Public Property Get SuppValue() As Variant
    SuppValue = m_vSuppValue
End Property

Public Property Let SuppValue(vNew As Variant)
    m_vSuppValue = vNew
End Property


