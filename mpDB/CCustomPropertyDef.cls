VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomPropertyDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
  Option Explicit

'**********************************************************
'   CTemplateCustomProperty Class
'   created 10/22/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods of a
'   MacPac Template Custom Property

'   Member of App
'   Container for
'**********************************************************
Public Enum mpCustomPropertyTypes
    mpCustomPropertyType_Document = 1
    mpCustomPropertyType_Author = 2
    mpCustomPropertyType_AuthorPreference = 3
    mpCustomPropertyType_AuthorOffice = 4
End Enum

Public Enum mpCustomPropertyActions
    mpCustomPropertyAction_InsertText = 0
    mpCustomPropertyAction_InsertCheckbox = 1
    mpCustomPropertyAction_RunMacroOnly = 2
End Enum

Private m_iID As Integer
Private m_xCategory As String
Private m_bWholePara As Boolean
Private m_xName As String
Private m_xBookmark As String
Private m_xDelim As String
Private m_xLinkedProp As String
Private m_xMacro As String
Private m_iPropType As mpCustomPropertyTypes
Private m_bIndexed As Boolean
Private m_lAction As Long   'defined as long to allow future switch to tblActions with IDs
Private m_iUnderlineLen As Integer
Private m_bIsNew As Boolean

'**********************************************************
'   Properties
'**********************************************************
Friend Property Let IsNew(bNew As Boolean)
    m_bIsNew = bNew
End Property

Friend Property Get IsNew() As Boolean
    IsNew = m_bIsNew
End Property

Public Property Let ID(iNew As Integer)
    m_iID = iNew
End Property

Public Property Get ID() As Integer
    ID = m_iID
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Macro(xNew As String)
    m_xMacro = xNew
End Property

Public Property Get Macro() As String
    Macro = m_xMacro
End Property

Public Property Let Category(xNew As String)
    m_xCategory = xNew
End Property

Public Property Get Category() As String
    Category = m_xCategory
End Property

Public Property Let LinkedProperty(xNew As String)
    m_xLinkedProp = xNew
End Property

Public Property Get LinkedProperty() As String
    LinkedProperty = m_xLinkedProp
End Property

Public Property Let Bookmark(xNew As String)
    m_xBookmark = xNew
End Property

Public Property Get Bookmark() As String
Attribute Bookmark.VB_UserMemId = 0
    Bookmark = m_xBookmark
End Property

Public Property Let DelimiterReplacement(xNew As String)
    m_xDelim = xNew
End Property

Public Property Get DelimiterReplacement() As String
    DelimiterReplacement = m_xDelim
End Property

Public Property Get WholeParagraph() As Boolean
    WholeParagraph = m_bWholePara
End Property

Public Property Let WholeParagraph(bNew As Boolean)
    m_bWholePara = bNew
End Property

Public Property Let Action(lNew As mpCustomPropertyActions)
    m_lAction = lNew
End Property

Public Property Get Action() As mpCustomPropertyActions
    Action = m_lAction
End Property

Public Property Let UnderlineLength(iNew As Integer)
    m_iUnderlineLen = iNew
End Property

Public Property Get UnderlineLength() As Integer
    UnderlineLength = m_iUnderlineLen
End Property

Public Property Get PropertyType() As mpCustomPropertyTypes
    PropertyType = m_iPropType
End Property

Public Property Let PropertyType(iNew As mpCustomPropertyTypes)
    m_iPropType = iNew
End Property

Public Property Get Indexed() As Boolean
    Indexed = m_bIndexed
End Property

Public Property Let Indexed(bNew As Boolean)
    m_bIndexed = bNew
End Property

'**********************************************************
'   Methods
'**********************************************************
'**********************************************************
'   Class Events
'**********************************************************
Private Sub Class_Initialize()
End Sub

