VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCaptionTypes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Pleading Caption Types Collection Class
'   created 1/2/00 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Letterhead types - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_opCaption As mpDB.CPleadingCaptionType
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(vTemplateID As Variant, Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    xSQL = "SELECT tblPleadingCaptionTypes.* " & _
           "FROM tblPleadingCaptionTypes INNER JOIN tblPleadingCaptionAssignments " & _
                "ON tblPleadingCaptionTypes.fldID = tblPleadingCaptionAssignments." & _
                "fldPleadingCaptionID" ' & _
                ' " WHERE tblPleadingCaptionAssignments.fldPleadingTypeID=" "'" & vTemplateID & "'" & ";"
  
   
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Active() As mpDB.CPleadingCaptionType
'returns the office object of the current record in the recordset
    Set Active = m_opCaption
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(lIndex As Long) As mpDB.CPleadingCaptionType
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lIndex
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_opCaption
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingCaptionTypes.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_opCaption = New mpDB.CPleadingCaptionType
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_opCaption = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject()
    With m_oRS
        On Error Resume Next
        m_opCaption.ID = !fldID
        m_opCaption.Description = !fldDescription
        m_opCaption.BoilerplateFile = !fldBoilerplateFile
        m_opCaption.Bookmark = !fldBookmark
        m_opCaption.CaseNumber1Label = !fldCaseNumber1Label
        m_opCaption.CaseNumber2Label = !fldCaseNumber2Label
        m_opCaption.CaseNumber3Label = !fldCaseNumber3Label
        m_opCaption.CaseNumber4Label = !fldCaseNumber4Label
        m_opCaption.CaseNumber5Label = !fldCaseNumber5Label
        m_opCaption.CaseNumber6Label = !fldCaseNumber6Label
        m_opCaption.CaseNumber1PrefillText = !fldCaseNumber1PrefillText
        m_opCaption.CaseNumber2PrefillText = !fldCaseNumber2PrefillText
        m_opCaption.CaseNumber3PrefillText = !fldCaseNumber3PrefillText
        m_opCaption.CaseNumber4PrefillText = !fldCaseNumber4PrefillText
        m_opCaption.CaseNumber5PrefillText = !fldCaseNumber5PrefillText
        m_opCaption.CaseNumber6PrefillText = !fldCaseNumber6PrefillText
        m_opCaption.AllowDocTitleCentered = !fldAllowDocTitleCentered
        m_opCaption.AllowDocTitleRight = !fldAllowDocTitleRight
        m_opCaption.CaptionSeparatorType = !fldAllowAuthorName
    End With
End Function





