VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLetterHeadType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Letterhead Type Class
'   created 12/22/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define macpac letterhead

'   Member of
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_xHeaderBookmark As String
Private m_xHeaderBookmarkPrimary As String
Private m_xFooterBookmark As String
Private m_xFooterBookmarkPrimary As String
Private m_sTopMargin As Single
Private m_sBottomMargin As Single
Private m_sLeftMargin As Single
Private m_sRightMargin As Single
Private m_sHeaderDistance As Single
Private m_sFooterDistance As Single
Private m_sHeaderSpacerHeight As Single
Private m_lTemplateToAttach As Long
Private m_bAllowAuthorEmail As Boolean
Private m_bAllowAuthorPhone As Boolean
Private m_bAllowAuthorName As Boolean



'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property

Public Property Let HeaderBookmark(xNew As String)
    m_xHeaderBookmark = xNew
End Property

Public Property Get HeaderBookmark() As String
    HeaderBookmark = m_xHeaderBookmark
End Property

Public Property Let HeaderBookmarkPrimary(xNew As String)
    m_xHeaderBookmarkPrimary = xNew
End Property

Public Property Get HeaderBookmarkPrimary() As String
    HeaderBookmarkPrimary = m_xHeaderBookmarkPrimary
End Property

Public Property Let FooterBookmark(xNew As String)
    m_xFooterBookmark = xNew
End Property

Public Property Get FooterBookmark() As String
    FooterBookmark = m_xFooterBookmark
End Property

Public Property Let FooterBookmarkPrimary(xNew As String)
    m_xFooterBookmarkPrimary = xNew
End Property

Public Property Get FooterBookmarkPrimary() As String
    FooterBookmarkPrimary = m_xFooterBookmarkPrimary
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTopMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTopMargin
End Property

Public Property Let BottomMargin(sNew As Single)
    m_sBottomMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBottomMargin
End Property

Public Property Let LeftMargin(sNew As Single)
    m_sLeftMargin = sNew
End Property

Public Property Get LeftMargin() As Single
    LeftMargin = m_sLeftMargin
End Property

Public Property Let RightMargin(sNew As Single)
    m_sRightMargin = sNew
End Property

Public Property Get RightMargin() As Single
    RightMargin = m_sRightMargin
End Property

Public Property Let HeaderDistance(sNew As Single)
    m_sHeaderDistance = sNew
End Property

Public Property Get HeaderDistance() As Single
    HeaderDistance = m_sHeaderDistance
End Property

Public Property Let FooterDistance(sNew As Single)
    m_sFooterDistance = sNew
End Property

Public Property Get FooterDistance() As Single
    FooterDistance = m_sFooterDistance
End Property

Public Property Let HeaderSpacerHeight(sNew As Single)
    m_sHeaderSpacerHeight = sNew
End Property

Public Property Get HeaderSpacerHeight() As Single
    HeaderSpacerHeight = m_sHeaderSpacerHeight
End Property

Public Property Let TemplateToAttach(lNew As Long)
    m_lTemplateToAttach = lNew
End Property

Public Property Get TemplateToAttach() As Long
    TemplateToAttach = m_lTemplateToAttach
End Property

Public Property Let AllowAuthorEMail(bNew As Boolean)
    m_bAllowAuthorEmail = bNew
End Property

Public Property Get AllowAuthorEMail() As Boolean
    AllowAuthorEMail = m_bAllowAuthorEmail
End Property

Public Property Let AllowAuthorPhone(bNew As Boolean)
    m_bAllowAuthorPhone = bNew
End Property

Public Property Get AllowAuthorPhone() As Boolean
    AllowAuthorPhone = m_bAllowAuthorPhone
End Property

Public Property Let AllowAuthorName(bNew As Boolean)
    m_bAllowAuthorName = bNew
End Property

Public Property Get AllowAuthorName() As Boolean
    AllowAuthorName = m_bAllowAuthorName
End Property

'**********************************************************
'   Methods
'**********************************************************
