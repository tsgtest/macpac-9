VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLetterheadTypes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Letterhead Types Collection Class
'   created 12/22/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Letterhead types - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_ID = 0
    xarCol_Description = 1
    xarCol_BoilerplateFile = 2
    xarCol_HeaderBookmark = 3
    xarCol_HeaderBookmarkPrimary = 4
    xarCol_FooterBookmark = 5
    xarCol_FooterBookmarkPrimary = 6
    xarCol_TopMargin = 7
    xarCol_BottomMargin = 8
    xarCol_LeftMargin = 9
    xarCol_RightMargin = 10
    xarCol_HeaderDistance = 11
    xarCol_FooterDistance = 12
    xarCol_HeaderSpacerHeight = 13
    xarCol_TemplateToAttachTo = 14
    xarCol_AllowAuthorEmail = 15
    xarCol_AllowAuthorPhone = 16
    xarCol_AllowAuthorName = 17
End Enum

Private m_oRS As DAO.Recordset
Private m_Letterhead As mpDB.CLetterHeadType
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
        'Refresh lTemplateID, True
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional xTemplateID As String, Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source- returns
'all letterhead types if no template id is supplied
    Dim xSQL As String

    If Len(xTemplateID) Then
'       get all types for specified template
        xSQL = "SELECT tblLetterheadTypes.* " & _
               "FROM tblLetterheadTypes INNER JOIN tblLetterheadAssignments " & _
                    "ON tblLetterheadTypes.fldID = tblLetterheadAssignments." & _
                    "fldLetterheadID " & _
               "WHERE tblLetterheadAssignments.fldTemplateID=" & _
                    "'" & xTemplateID & "' " & _
               "ORDER BY tblLetterheadTypes.fldDescription"
    Else
'       get all types
        xSQL = "SELECT * FROM tblLetterheadTypes " & _
               "ORDER BY tblLetterheadTypes.fldDescription"
    End If
   
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Active() As mpDB.CLetterHeadType
'returns the office object of the current record in the recordset
    Set Active = m_Letterhead
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(lIndex As Long) As mpDB.CLetterHeadType
Attribute Item.VB_UserMemId = 0
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lIndex
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_Letterhead
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CLetterheadTypes.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_Letterhead = New mpDB.CLetterHeadType
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_Letterhead = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject()
    With m_oRS
        On Error Resume Next
        m_Letterhead.ID = !fldID
        m_Letterhead.Description = !fldDescription
        m_Letterhead.BoilerplateFile = !fldBoilerplateFile
        m_Letterhead.HeaderBookmark = !fldHeaderBookmark
        m_Letterhead.HeaderBookmarkPrimary = !fldHeaderBookmarkPrimary
        m_Letterhead.FooterBookmark = !fldFooterBookmark
        m_Letterhead.FooterBookmarkPrimary = !fldFooterBookmarkPrimary
        m_Letterhead.TopMargin = !fldTopMargin
        m_Letterhead.BottomMargin = !fldBottomMargin
        m_Letterhead.LeftMargin = !fldLeftMargin
        m_Letterhead.RightMargin = !fldRightMargin
        m_Letterhead.HeaderDistance = !fldHeaderDistance
        m_Letterhead.FooterDistance = !fldFooterDistance
        m_Letterhead.HeaderSpacerHeight = !fldHeaderSpacerHeight
        m_Letterhead.TemplateToAttach = !fldTemplateToAttach
        m_Letterhead.AllowAuthorEMail = !fldAllowAuthorEmail
        m_Letterhead.AllowAuthorPhone = !fldAllowAuthorPhone
        m_Letterhead.AllowAuthorName = !fldAllowAuthorName
    End With
End Function



