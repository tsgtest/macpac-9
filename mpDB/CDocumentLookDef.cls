VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentLookDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CDocumentLook Class
'   created 3/7/00 by Dan Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define a MacPac Document Look - a set of
'   formats and content that has a name
'**********************************************************
Option Explicit

Private m_sLMargin As Single
Private m_sRMargin As Single
Private m_sTMargin As Single
Private m_sBMargin As Single
Private m_iAlign As WdParagraphAlignment
Private m_sLine1Indent As Single
Private m_iLineSpacing As WdLineSpacing
Private m_sSpaceBefore As Single
Private m_sSpaceAfter As Single
Private m_xFont As String
Private m_iFontSize As Integer
Private m_iCols As Integer
Private m_sColSpace As Single
Private m_iNumFormat As mpPageNumberFormats
Private m_xBefore As String
Private m_xAfter As String
Private m_iLocation As mpPageNumberLocations
Private m_iPgNumAlign As mpPageNumberAlignments
Private m_bPage1 As Boolean
Private m_bDiffPage1 As Boolean
Private m_lID As Long

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let PageNumFormat(iNew As mpPageNumberFormats)
    m_iNumFormat = iNew
End Property

Public Property Get PageNumFormat() As mpPageNumberFormats
    PageNumFormat = m_iNumFormat
End Property

Public Property Let PageNumTextBefore(xNew As String)
    m_xBefore = xNew
End Property

Public Property Get PageNumTextBefore() As String
    PageNumTextBefore = m_xBefore
End Property

Public Property Let PageNumTextAfter(xNew As String)
    m_xAfter = xNew
End Property

Public Property Get PageNumTextAfter() As String
    PageNumTextAfter = m_xAfter
End Property

Public Property Let PageNumLocation(iNew As mpPageNumberLocations)
    m_iLocation = iNew
End Property

Public Property Get PageNumLocation() As mpPageNumberLocations
    PageNumLocation = m_iLocation
End Property

Public Property Let PageNumAlignment(iNew As mpPageNumberAlignments)
    m_iPgNumAlign = iNew
End Property

Public Property Get PageNumAlignment() As mpPageNumberAlignments
    PageNumAlignment = m_iPgNumAlign
End Property

Public Property Let PageNumFirstPage(bNew As Boolean)
    m_bPage1 = bNew
End Property

Public Property Get PageNumFirstPage() As Boolean
    PageNumFirstPage = m_bPage1
End Property

Public Property Let DifferentPage1HeaderFooter(bNew As Boolean)
    m_bDiffPage1 = bNew
End Property

Public Property Get DifferentPage1HeaderFooter() As Boolean
    DifferentPage1HeaderFooter = m_bDiffPage1
End Property

Public Property Let LeftMargin(sNew As Single)
    m_sLMargin = sNew
End Property

Public Property Get LeftMargin() As Single
    LeftMargin = m_sLMargin
End Property

Public Property Let RightMargin(sNew As Single)
    m_sRMargin = sNew
End Property

Public Property Get RightMargin() As Single
    RightMargin = m_sRMargin
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTMargin
End Property

Public Property Let BottomMargin(sNew As Single)
    m_sBMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBMargin
End Property

Public Property Let Alignment(iNew As WdParagraphAlignment)
    m_iAlign = iNew
End Property

Public Property Get Alignment() As WdParagraphAlignment
    Alignment = m_iAlign
End Property

Public Property Let FirstLineIndent(sNew As Single)
    m_sLine1Indent = sNew
End Property

Public Property Get FirstLineIndent() As Single
    FirstLineIndent = m_sLine1Indent
End Property

Public Property Let LineSpacing(iNew As WdLineSpacing)
    m_iLineSpacing = iNew
End Property

Public Property Get LineSpacing() As WdLineSpacing
    LineSpacing = m_iLineSpacing
End Property

Public Property Let SpaceBefore(sNew As Single)
    m_sSpaceBefore = sNew
End Property

Public Property Get SpaceBefore() As Single
    SpaceBefore = m_sSpaceBefore
End Property

Public Property Let SpaceAfter(sNew As Single)
    m_sSpaceAfter = sNew
End Property

Public Property Get SpaceAfter() As Single
    SpaceAfter = m_sSpaceAfter
End Property

Public Property Let FontName(xNew As String)
    m_xFont = xNew
End Property

Public Property Get FontName() As String
    FontName = m_xFont
End Property

Public Property Let FontSize(iNew As Integer)
    m_iFontSize = iNew
End Property

Public Property Get FontSize() As Integer
    FontSize = m_iFontSize
End Property

Public Property Let Columns(iNew As Integer)
    m_iCols = iNew
End Property

Public Property Get Columns() As Integer
    Columns = m_iCols
End Property

Public Property Let SpaceBetweenColumns(sNew As Single)
    m_sColSpace = sNew
End Property

Public Property Get SpaceBetweenColumns() As Single
    SpaceBetweenColumns = m_sColSpace
End Property


