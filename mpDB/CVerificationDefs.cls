VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVerificationDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CVerificationDefs Collection Class
'   created 8/8/00 by Ajax Green-

'   Contains properties/methods that manage the
'   collection of Verification Document definitions -
'   collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for CVerificationDef
'**********************************************************


Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CVerificationDef
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CVerificationDef
Attribute Item.VB_UserMemId = 0
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where index = lID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       get item where name = vID
        xCrit = "fldID = " & lID
            
'       attempt to find the item
        m_oRS.FindFirst xCrit
        If m_oRS.NoMatch Then
'           if no match found, the
'           member does not exist
            Err.Raise mpError_InvalidMember
        Else
'           item found - update template object
            UpdateObject
        End If
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CVerificationDefs.Item"
End Function

Public Function Refresh(Optional ByVal lLevel0 As Long)
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get templates
    xSQL = "SELECT * FROM tblVerificationTypes "
    If lLevel0 Then
        xSQL = xSQL & "WHERE fldLevel0 = " & lLevel0
    End If
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CVerificationDefs.Refresh"
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oType
        Set .AddressFormat = Nothing
        .AllowCity = Empty
        .AllowCorporatePosition = Empty
        .AllowCorporationName = Empty
        .AllowCounty = Empty
        .AllowDeceasedName = Empty
        .AllowGender = Empty
        .AllowNotaryName = Empty
        .AllowPartyName = Empty
        .AllowPartyTitle = Empty
        .AllowState = Empty
        .AllowExecuteDate = Empty
        .AllowAttorneyFor = Empty
        
        .BoilerplateFile = Empty
        .Custom1DefaultText = Empty
        .Custom1Label = Empty
        .Custom2DefaultText = Empty
        .Custom2Label = Empty
        .DateFormats = Empty
        .DefaultFooterText = Empty
        .DefaultPleadingPaperType = Empty
        .DefaultExecuteDate = Empty
        .Description = Empty
        .DialogCaption = Empty
        .FooterTextMaxChars = Empty
        .ID = Empty
        .Level0 = Empty
        .UpperCaseFirmName = Empty
        .FirstNumberedPage = Empty
        .FirstPageNumber = Empty
        .UseBoilerplateFooterSetup = Empty
'CharlieCore 9.2.0 - BaseStyle
        .BaseStyle = Empty
        
        On Error Resume Next
        Set .AddressFormat = Application.OfficeAddressFormats( _
            m_oRS!fldAddressFormat)
        
        .AllowCity = m_oRS!fldAllowCity
        .AllowCorporatePosition = m_oRS!fldAllowCorporatePosition
        .AllowCorporationName = m_oRS!fldAllowCorporationName
        .AllowCounty = m_oRS!fldAllowCounty
        .AllowDeceasedName = m_oRS!fldAllowDeceasedName
        .AllowGender = m_oRS!fldAllowGender
        .AllowNotaryName = m_oRS!fldAllowNotaryName
        .AllowPartyName = m_oRS!fldAllowPartyName
        .AllowPartyTitle = m_oRS!fldAllowPartyTitle
        .AllowState = m_oRS!fldAllowState
        .AllowExecuteDate = m_oRS!fldAllowExecuteDate
        .AllowAttorneyFor = m_oRS!fldAllowAttorneyFor
        
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .Custom1DefaultText = m_oRS!fldCustom1DefaultText
        .Custom1Label = m_oRS!fldCustom1Label
        .Custom2DefaultText = m_oRS!fldCustom2DefaultText
        .Custom2Label = m_oRS!fldCustom2Label
        .DateFormats = m_oRS!fldDateFormats
        .DefaultFooterText = m_oRS!fldDefaultFooterText
        .DefaultPleadingPaperType = m_oRS!fldPleadingPaper
        .DefaultExecuteDate = m_oRS!fldDefaultExecuteDate
        .Description = m_oRS!fldDescription
        .DialogCaption = m_oRS!fldDialogCaption
        .FooterTextMaxChars = m_oRS!fldFooterTextMax
        .ID = m_oRS!fldID
        .Level0 = m_oRS!fldLevel0
        .UpperCaseFirmName = m_oRS!fldUCaseFirmName
        .FirstNumberedPage = m_oRS!fldFirstNumberedPage
        .FirstPageNumber = m_oRS!fldFirstPageNumber
        .UseBoilerplateFooterSetup = m_oRS!fldUseBoilerplateFooterSetup
'CharlieCore 9.2.0 - BaseStyle
        .BaseStyle = m_oRS!fldBaseStyle
        
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oType = New mpDB.CVerificationDef
End Sub
