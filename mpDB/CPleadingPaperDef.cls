VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingPaperDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CPleadingPaperDef Class
'   created 12/15/99 by Jeffrey Sweetland

'   Contains properties and methods that
'   define a Pleading Paper definition
'   follows structure of tblPleadingPaperDefs

'   Member of CPleadingPaperDefs
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_xHeaderBookmark As String
Private m_xFooterBookmark As String
Private m_sTopMargin As Single
Private m_sBottomMargin As Single
Private m_sLeftMargin As Single
Private m_sRightMargin As Single
Private m_sHeaderDistance As Single
Private m_sFooterDistance As Single
Private m_sHeaderSpacerHeight As Single
Private m_xState As String
Private m_iLineSpacing
Private m_xPageIntroChar As String
Private m_xPageTrailingChar As String
Private m_bFirstPageNumber As Boolean
Private m_bUsesPage1Header As Boolean
Private m_bUsesPrimaryHeader As Boolean
Private m_bUsesPage1Footer As Boolean
Private m_bUsesPrimaryFooter As Boolean
Private m_iDefaultSidebarType As Integer
Private m_lOrientation As Long
Private m_sPageHeight As Single
Private m_sPageWidth As Single
Private m_bAllowSideBar As Boolean
Private m_bFormatFirmName As Boolean
Private m_lOfficeAddressFormat As Long

'---9.5.2
Private m_iPageNumberLocation As Integer

Public Enum mpPleadingPaperLineSpacing
    mpPleadingPaperLineSpacingSingle = 0
    mpPleadingPaperLineSpacingExactly = 4
End Enum

Public Enum mpPleadingSideBarTypes
    mpPleadingSideBarNone = 0
    mpPleadingSideBarPortrait = 1
    mpPleadingSideBarLandscape = 2
End Enum

'---9.5.2

Public Enum mpPleadingPaperPageNumberLocation
    mpPleadingPaperPageNumberBottom = 0
    mpPleadingPaperPageNumberTop = 1
    mpPleadingPaperPageNumberNone = 2
End Enum


Private Const mpHeaderBookmark As String = "zzmpPleadingPaperHeader"
Private Const mpFooterBookmark As String = "zzmpPleadingPaperFooter"

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let LineSpacing(iNew As mpPleadingPaperLineSpacing)
    m_iLineSpacing = iNew
End Property

Public Property Get LineSpacing() As mpPleadingPaperLineSpacing
    LineSpacing = m_iLineSpacing
End Property
'---9.5.2
Public Property Let PageNumberLocation(iNew As mpPleadingPaperPageNumberLocation)
    m_iPageNumberLocation = iNew
End Property

Public Property Get PageNumberLocation() As mpPleadingPaperPageNumberLocation
    PageNumberLocation = m_iPageNumberLocation
End Property

Public Property Let DefaultSidebarType(iNew As mpPleadingSideBarTypes)
    m_iDefaultSidebarType = iNew
End Property

Public Property Get DefaultSidebarType() As mpPleadingSideBarTypes
    DefaultSidebarType = m_iDefaultSidebarType
End Property

Public Property Get PageIntroChar() As String
    PageIntroChar = m_xPageIntroChar
End Property

Public Property Let PageIntroChar(xNew As String)
    m_xPageIntroChar = xNew
End Property

Public Property Get PageTrailingChar() As String
    PageTrailingChar = m_xPageTrailingChar
End Property

Public Property Let PageTrailingChar(xNew As String)
    m_xPageTrailingChar = xNew
End Property

'Public Property Let FirstPageNumbered(bNew As Boolean)
'    m_bFirstPageNumber = bNew
'End Property
'
'Public Property Get FirstPageNumbered() As Boolean
'    FirstPageNumbered = m_bFirstPageNumber
'End Property
'
Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Get HeaderBookmark() As String
    HeaderBookmark = mpHeaderBookmark
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property

Public Property Let BoilerplateFile(xNew As String)
    On Error GoTo ProcError
    If Dir(mpBase2.BoilerplateDirectory & xNew) = "" Then _
        Err.Raise mpError_InvalidBoilerplateFile
    m_xBoilerplateFile = xNew
    Exit Property
ProcError:
    Err.Raise Err.Number, "mpDB.CPleadingPaperDef", Application.Error.Desc(Err.Number)
End Property

Public Property Get FooterBookmark() As String
    FooterBookmark = mpFooterBookmark
End Property

Friend Property Let UsesFirstPageHeader(bNew As Boolean)
    m_bUsesPage1Header = bNew
End Property

Public Property Get UsesFirstPageHeader() As Boolean
    UsesFirstPageHeader = m_bUsesPage1Header
End Property

Friend Property Let UsesPrimaryHeader(bNew As Boolean)
    m_bUsesPrimaryHeader = bNew
End Property

Public Property Get UsesPrimaryHeader() As Boolean
    UsesPrimaryHeader = m_bUsesPrimaryHeader
End Property

Friend Property Let UsesFirstPageFooter(bNew As Boolean)
    m_bUsesPage1Footer = bNew
End Property

Public Property Get UsesFirstPageFooter() As Boolean
    UsesFirstPageFooter = m_bUsesPage1Footer
End Property

Friend Property Let UsesPrimaryFooter(bNew As Boolean)
    m_bUsesPrimaryFooter = bNew
End Property

Public Property Get UsesPrimaryFooter() As Boolean
    UsesPrimaryFooter = m_bUsesPrimaryFooter
End Property

Public Property Let State(xNew As String)
    m_xState = xNew
End Property

Public Property Get State() As String
    State = m_xState
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTopMargin
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTopMargin = sNew
End Property

Public Property Get BottomMargin() As Single
    BottomMargin = m_sBottomMargin
End Property

Public Property Let BottomMargin(sNew As Single)
    m_sBottomMargin = sNew
End Property

Public Property Get LeftMargin() As Single
    LeftMargin = m_sLeftMargin
End Property

Public Property Let LeftMargin(sNew As Single)
    m_sLeftMargin = sNew
End Property

Public Property Get RightMargin() As Single
    RightMargin = m_sRightMargin
End Property

Public Property Let RightMargin(sNew As Single)
    m_sRightMargin = sNew
End Property

Public Property Get HeaderDistance() As Single
    HeaderDistance = m_sHeaderDistance
End Property

Public Property Let HeaderDistance(sNew As Single)
    m_sHeaderDistance = sNew
End Property

Public Property Get FooterDistance() As Single
    FooterDistance = m_sFooterDistance
End Property

Public Property Let FooterDistance(sNew As Single)
    m_sFooterDistance = sNew
End Property

Friend Property Let HeaderSpacerHeight(sNew As Single)
    m_sHeaderSpacerHeight = sNew
End Property

Public Property Get HeaderSpacerHeight() As Single
    HeaderSpacerHeight = m_sHeaderSpacerHeight
End Property

Friend Property Let PageHeight(sNew As Single)
    m_sPageHeight = sNew
End Property

Public Property Get PageHeight() As Single
    PageHeight = m_sPageHeight
End Property

Friend Property Let PageWidth(sNew As Single)
    m_sPageWidth = sNew
End Property

Public Property Get PageWidth() As Single
    PageWidth = m_sPageWidth
End Property

Friend Property Let Orientation(lNew As Long)
    m_lOrientation = lNew
End Property

Public Property Get Orientation() As Long
    Orientation = m_lOrientation
End Property

Public Property Let AllowSideBar(bNew As Boolean)
    m_bAllowSideBar = bNew
End Property

Public Property Get AllowSideBar() As Boolean
    AllowSideBar = m_bAllowSideBar
End Property

Public Property Let FormatFirmName(bNew As Boolean)
    m_bFormatFirmName = bNew
End Property

Public Property Get FormatFirmName() As Boolean
    FormatFirmName = m_bFormatFirmName
End Property

Public Property Let OfficeAddressFormat(lNew As Long)
    m_lOfficeAddressFormat = lNew
End Property

Public Property Get OfficeAddressFormat() As Long
    OfficeAddressFormat = m_lOfficeAddressFormat
End Property



