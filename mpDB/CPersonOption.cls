VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CPersonOption"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   PersonOption Class
'   created 12/23/98 by Daniel Fisherman
'   Contains properties and methods that
'   define an option set item -

'   Member of CPersonOptions
'**********************************************************

Option Explicit

Private m_xName As String
Private m_vValue As Variant
Private m_opsParent As mpDB.CPersonOptions
'**********************************************************

'************************************************************************
'Properties
'************************************************************************
Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Value(vNew As Variant)
    m_vValue = vNew
End Property

Public Property Get Value() As Variant
Attribute Value.VB_UserMemId = 0
    Value = m_vValue
End Property

Public Property Let Parent(opisNew As mpDB.CPersonOptions)
    Set m_opsParent = opisNew
End Property

Public Property Get Parent() As mpDB.CPersonOptions
    Set Parent = m_opsParent
End Property

