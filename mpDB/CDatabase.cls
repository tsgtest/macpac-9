VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDatabase"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   THIS IS THE TOP LEVEL CLASS IN THE EXPOSED MODEL

'   CDatabase Class
'   created 12/28/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods
'   of the MacPac database backend - this class exposes
'   all the necessary properties and methods for use
'   as a MacPac db server component

'   Container for CApp
'**********************************************************

'**********************************************************
'  METHODS
'**********************************************************

Public Function Version() As String
    On Error Resume Next
    Version = App.Major & "." & App.Minor & "." & App.Revision
End Function

Public Property Get Initialized() As Boolean
    Initialized = g_bInit
End Property

Public Function Licenses(lPersonID As Long) As CLicenses
'returns the licenses for person with id = lPersonID
    Dim oLic As CLicenses
    
    On Error GoTo ProcError
    Set oLic = New CLicenses
    oLic.Refresh lPersonID
    Set Licenses = oLic
    Exit Function
    
ProcError:
    RaiseError "mpDB.CDatabase.Licenses"
    Exit Function
End Function

Public Function PleadingCoverPageDefs() As mpDB.CPleadingCoverPageDefs
    Set PleadingCoverPageDefs = Application.PleadingCoverPageDefs()
End Function

Public Function BusinessDefs() As mpDB.CBusinessDefs
    Set BusinessDefs = Application.BusinessDefs()
End Function

Public Function BusinessSignatureDefs() As mpDB.CBusinessSignatureDefs
    Set BusinessSignatureDefs = Application.BusinessSignatureDefs()
End Function

Public Function BusinessTitlePageDefs() As mpDB.CBusinessTitlePageDefs
    Set BusinessTitlePageDefs = Application.BusinessTitlePageDefs()
End Function
Public Function VerificationDefs(Optional ByVal ParentID As Long) As mpDB.CVerificationDefs
    Set VerificationDefs = Application.VerificationDefs(ParentID)
End Function

Public Function SegmentDefs(ByVal xTemplate As String) As mpDB.CSegmentDefs
    Set SegmentDefs = Application.SegmentDefs(xTemplate)
End Function

Public Function PleadingCaptionBorderStyles() As mpDB.CPleadingCaptionBorderStyles
    Set PleadingCaptionBorderStyles = _
        Application.PleadingCaptionBorderStyles()
End Function

Public Function OfficeAddressFormats() As mpDB.COfficeAddressFormats
    Set OfficeAddressFormats = Application.OfficeAddressFormats
End Function

Public Function CIFormats() As mpDB.CCIFormats
    Set CIFormats = Application.CIFormats
End Function

Public Function CustomDialogDefs() As mpDB.CCustomDialogDefs
    Set CustomDialogDefs = Application.CustomDialogDefs()
End Function

Public Function PleadingFavorites() As mpDB.CPleadingFavorites
    Set PleadingFavorites = Application.PleadingFavorites
End Function

Public Function PleadingSignatureDefs() As mpDB.CPleadingSignatureDefs
    Set PleadingSignatureDefs = Application.PleadingSignatureDefs()
End Function

Public Function PleadingCaptionDefs() As mpDB.CPleadingCaptionDefs
    Set PleadingCaptionDefs = Application.PleadingCaptionDefs()
End Function

Public Function PleadingCounselDefs() As mpDB.CPleadingCounselDefs
    Set PleadingCounselDefs = Application.PleadingCounselDefs()
End Function

Public Function PleadingPaperDefs(Optional ByVal xState As String = "") As mpDB.CPleadingPaperDefs
    Set PleadingPaperDefs = Application.PleadingPaperDefs(xState)
End Function

Public Function PleadingDefs() As mpDB.CPleadingDefs
    Set PleadingDefs = Application.PleadingDefs()
End Function

Public Function NotaryDefs(Optional ByVal ParentID As Long) As mpDB.CNotaryDefs
    Set NotaryDefs = Application.NotaryDefs(ParentID)
End Function

Public Function ServiceDefs(Optional ByVal ParentID As Long) As mpDB.CServiceDefs
    Set ServiceDefs = Application.ServiceDefs(ParentID)
End Function

Public Function ServiceListDefs(Optional lServiceType As Long) As mpDB.CServiceListDefs
    Set ServiceListDefs = Application.ServiceListDefs(lServiceType)
End Function

Public Function LetterheadDefs(Optional ByVal xTemplate As String) As mpDB.CLetterheadDefs
    Set LetterheadDefs = Application.LetterheadDefs(xTemplate)
End Function
Public Function DocumentInsertDefs(Optional ByVal xTemplate As String) As mpDB.CDocumentInsertDefs
    Set DocumentInsertDefs = Application.DocumentInsertDefs(xTemplate)
End Function
Public Function DocumentStampDefs(Optional ByVal iCategory As Integer = 0, _
                                  Optional ByVal xTemplate As String, _
                                  Optional ByVal lLHID As Long = 0) As mpDB.CDocumentStampDefs
    Set DocumentStampDefs = Application.DocumentStampDefs(iCategory, xTemplate, lLHID)
End Function

Public Function OptionsControls(ByVal xTemplate As String) As mpDB.COptionsControls
    Set OptionsControls = Application.OptionsControls(xTemplate)
End Function

Public Function PersonDetailControls() As mpDB.CPersonDetailControls
    Set PersonDetailControls = Application.PersonDetailControls()
End Function

Public Function CustomProperties(ByVal xCategory As String) As mpDB.CCustomPropertyDefs
    Set CustomProperties = Application.CustomProperties(xCategory)
End Function

Public Function CustomControls(ByVal xCategory As String) As mpDB.CCustomControls
    Set CustomControls = Application.CustomControls(xCategory)
End Function

Public Function DepoSummaryDefs() As mpDB.CDepoSummaryDefs
    Set DepoSummaryDefs = Application.DepoSummaryDefs()
End Function

Public Function CustomPersonProperties() As mpDB.CCustomProperties
    Set CustomPersonProperties = Application.CustomPersonProperties
End Function

Public Function SetDefaultAuthor(lID As Long, xTemplate As String) As mpDB.CPerson
    Set SetDefaultAuthor = Application.SetDefaultAuthor(lID, xTemplate)
End Function

Public Function GetDefaultAuthor(xTemplate As String) As mpDB.CPerson
    Set GetDefaultAuthor = Application.GetDefaultAuthor(xTemplate)
End Function

Public Function PublicDB() As DAO.Database
    Set PublicDB = Application.PublicDB
End Function

Public Function PrivateDB() As DAO.Database
    Set PrivateDB = Application.PrivateDB
End Function

Public Function People() As mpDB.CPersons
    Set People = Application.People
End Function

Public Function Attorneys() As mpDB.CPersons
    Set Attorneys = Application.Attorneys
End Function

Public Function TemplateDefinitions() As mpDB.CTemplateDefs
    Set TemplateDefinitions = Application.TemplateDefinitions
End Function

Public Function TemplateGroups() As mpDB.CTemplateGroups
    Set TemplateGroups = Application.TemplateGroups
End Function

Public Function SegmentDefinitions() As mpDB.CSegmentDefs
    Set SegmentDefinitions = Application.SegmentDefinitions
End Function

Public Function SegmentGroups() As mpDB.CSegmentGroups
    Set SegmentGroups = Application.SegmentGroups
End Function

Public Function FirmPeople() As mpDB.CPersons
    Set FirmPeople = Application.FirmPeople
End Function

Public Function FavoritePeople() As mpDB.CPersons
    Set FavoritePeople = Application.FavoritePeople
End Function

Public Function InputPeople() As mpDB.CPersons
    Set InputPeople = Application.InputPeople
End Function

Public Function Lists() As mpDB.CLists
    Set Lists = Application.Lists
End Function

Public Function Offices() As mpDB.COffices
    Set Offices = Application.Offices
End Function

Public Function EnvelopeDefs() As mpDB.CEnvelopeDefs
    Set EnvelopeDefs = Application.EnvelopeDefs
End Function

Public Function PleadingSeparateStatementDefs() As mpDB.CPleadingSeparateStatementDefs
    Set PleadingSeparateStatementDefs = Application.PleadingSeparateStatementDefs()
End Function

Public Function LabelDefs() As mpDB.CLabelDefs
    Set LabelDefs = Application.LabelDefs
End Function

Public Function NewXArray() As XArray
    Set NewXArray = Application.NewXArray()
End Function
'**********************************************************

Private Sub Class_Terminate()
    Set g_App = Nothing
End Sub
