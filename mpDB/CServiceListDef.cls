VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CServiceListDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CServiceDef Class
'   created 5/16/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that define
'   a type of MacPac Service Document (POS/COS)

'   Member of CServiceDefs
'   Container for COfficeAddressFormat
'**********************************************************

Enum mpServiceListWidthRules
    mpServiceListWidthRule_FitToMargin = 1
    mpServiceListWidthRule_FitToPage = 2
End Enum

Private m_lID As Long
Private m_xDesc As String
Private m_xName As String
Private m_xBPFile As String
Private m_bytCells  As Byte
Private m_bAllowPhone As Boolean
Private m_bAllowFax As Boolean
Private m_bAllowEMail As Boolean
Private m_bIncludePhone As Boolean
Private m_bIncludeFax As Boolean
Private m_bIncludeEMail As Boolean
Private m_iWidthRule As Byte
Private m_sLeftIndent As Single
Private m_sRightIndent As Single
Private m_xSeparatePageText As String
Private m_sSpaceBetweenRows As Single
Private m_bAllowPartyTitle As Boolean
Private m_bAllowPartyNames As Boolean
Private m_bAllowDPhrase As Boolean
'---9.7.1
Private m_bAllowNonEF As Boolean


Friend Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Friend Property Let SeparatePageText(xNew As String)
    m_xSeparatePageText = xNew
End Property

Public Property Get SeparatePageText() As String
    SeparatePageText = xSubstitute(m_xSeparatePageText, "<13>", Chr(13))
End Property

Friend Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Friend Property Let CellsPerRecipient(bytNew As Byte)
    m_bytCells = bytNew
End Property

Public Property Get CellsPerRecipient() As Byte
    CellsPerRecipient = m_bytCells
End Property

Public Property Let AllowPhone(bNew As Boolean)
    m_bAllowPhone = bNew
End Property

Public Property Get AllowPhone() As Boolean
    AllowPhone = m_bAllowPhone
End Property

Public Property Let AllowFax(bNew As Boolean)
    m_bAllowFax = bNew
End Property

Public Property Get AllowFax() As Boolean
    AllowFax = m_bAllowFax
End Property

Public Property Let AllowEMail(bNew As Boolean)
    m_bAllowEMail = bNew
End Property

Public Property Get AllowEMail() As Boolean
    AllowEMail = m_bAllowEMail
End Property

Public Property Let IncludePhone(bNew As Boolean)
    m_bIncludePhone = bNew
End Property

Public Property Get IncludePhone() As Boolean
    IncludePhone = m_bIncludePhone
End Property

Public Property Let IncludeFax(bNew As Boolean)
    m_bIncludeFax = bNew
End Property

Public Property Get IncludeFax() As Boolean
    IncludeFax = m_bIncludeFax
End Property

Public Property Let IncludeEMail(bNew As Boolean)
    m_bIncludeEMail = bNew
End Property

Public Property Get IncludeEMail() As Boolean
    IncludeEMail = m_bIncludeEMail
End Property

Public Property Let WidthRule(iNew As mpServiceListWidthRules)
    m_iWidthRule = iNew
End Property

Public Property Get WidthRule() As mpServiceListWidthRules
    WidthRule = m_iWidthRule
End Property

Public Property Let LeftIndent(sNew As Single)
    m_sLeftIndent = sNew
End Property

Public Property Get LeftIndent() As Single
    LeftIndent = m_sLeftIndent * 72
End Property

Public Property Let RightIndent(sNew As Single)
    m_sRightIndent = sNew
End Property

Public Property Get RightIndent() As Single
    RightIndent = m_sRightIndent * 72
End Property

Public Property Get SpaceBetweenRows() As Single
    SpaceBetweenRows = m_sSpaceBetweenRows
End Property
Public Property Let SpaceBetweenRows(sNew As Single)
    m_sSpaceBetweenRows = sNew
End Property

Public Property Let AllowPartyTitle(ByVal bNew As Boolean)
    m_bAllowPartyTitle = bNew
End Property
Public Property Get AllowPartyTitle() As Boolean
    AllowPartyTitle = m_bAllowPartyTitle
End Property
Public Property Let AllowPartyNames(ByVal bNew As Boolean)
        m_bAllowPartyNames = bNew
End Property
Public Property Get AllowPartyNames() As Boolean
    AllowPartyNames = m_bAllowPartyNames
End Property
Public Property Let AllowDPhrase(ByVal bNew As Boolean)
        m_bAllowDPhrase = bNew
End Property
Public Property Get AllowDPhrase() As Boolean
    AllowDPhrase = m_bAllowDPhrase
End Property
'---9.7.1
Public Property Let AllowNonEF(ByVal bNew As Boolean)
        m_bAllowNonEF = bNew
End Property
Public Property Get AllowNonEF() As Boolean
    AllowNonEF = m_bAllowNonEF
End Property

