VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingSeparateStatementDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Pleading Separate Statement Types Collection Class
'   created 7/6/04 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Letterhead types - collection is a recordset, though
'   an XArray belongs to class for display purposes

'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String
   
   xSQL = "SELECT tblSeparateStatementTypes.* " & _
           "FROM tblSeparateStatementTypes " & ";"
     
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CPleadingSeparateStatementDef
Attribute Item.VB_UserMemId = 0
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim opSepStatement As CPleadingSeparateStatementDef
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
            Set opSepStatement = New CPleadingSeparateStatementDef
'           found - update current object
            UpdateObject opSepStatement
            
'           return current object
            Set Item = opSepStatement
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingCaptionDefs.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject(opSepStatement As CPleadingSeparateStatementDef)
    With m_oRS
        On Error Resume Next
        
        opSepStatement.ID = Empty
        opSepStatement.Description = Empty
        opSepStatement.Title = Empty
        opSepStatement.BoilerplateFile = Empty
        opSepStatement.LeftColumnHeading = Empty
        opSepStatement.RightColumnHeading = Empty
        
        
        opSepStatement.ID = !fldID
        opSepStatement.Description = !fldDescription
        opSepStatement.Title = !fldTitle
        opSepStatement.BoilerplateFile = !fldBoilerplateFile
        opSepStatement.LeftColumnHeading = !fldLeftColumnHeading
        opSepStatement.RightColumnHeading = !fldRightColumnHeading
    End With
End Function
