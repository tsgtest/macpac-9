VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CList Collection Class
'   created 11/29/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties/ methods of a List

'   Member of CLists
'   Container for CListItems
'**********************************************************

Public Enum mpListFilterOperators
    mpListFilterOperator_Is = 1
    mpListFilterOperator_IsGreaterThan = 2
    mpListFilterOperator_IsLessThan = 3
    mpListFilterOperator_IsLike = 4
End Enum

'**********************************************************
Private m_oDB As DAO.Database
Private m_xSQL As String
Private m_xName As String
Private m_xFilterField As String
Private m_vFilterValue As Variant
Private m_iFilterOperator As Integer
Private m_xSortField As String
Private m_oItems As CListItems
Private m_bIsDBList As Boolean
Private m_bKeepFilter As Boolean

'**********************************************************
'   Properties
'**********************************************************
Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Private Property Let DB(dbNew As DAO.Database)
    Set m_oDB = dbNew
End Property

Private Property Get DB() As DAO.Database
    Set DB = m_oDB
End Property

Public Property Let IsDBList(bNew As Boolean)
    m_bIsDBList = bNew
End Property

Public Property Get IsDBList() As Boolean
    IsDBList = m_bIsDBList
End Property

Public Property Let ListItems(lisNew As CListItems)
    Set m_oItems = lisNew
End Property

Public Property Get ListItems() As CListItems
'   refresh list if necessary
    If (m_oItems Is Nothing) Then
        Set m_oItems = RefreshListItems()
    End If
    Set ListItems = m_oItems
End Property

Public Property Get FilterValue() As Variant
    FilterValue = m_vFilterValue
End Property

Public Property Let FilterValue(xNew As Variant)
    m_vFilterValue = xNew
End Property

Friend Property Get SQL() As String
    SQL = m_xSQL
End Property

Friend Property Let SQL(xNew As String)
    m_xSQL = xNew
End Property

Friend Property Let SortField(xNew As String)
    m_xSortField = xNew
End Property

Public Property Get SortField() As String
    SortField = m_xSortField
End Property

Public Property Get FilterField() As String
    FilterField = m_xFilterField
End Property

Public Property Let FilterField(xNew As String)
    m_xFilterField = xNew
End Property

Public Property Get KeepFilter() As Boolean
    KeepFilter = m_bKeepFilter
End Property

Public Property Let KeepFilter(bNew As Boolean)
    m_bKeepFilter = bNew
End Property

Friend Property Get FilterOperator() As Integer
    FilterOperator = m_iFilterOperator
End Property

Friend Property Let FilterOperator(iNew As Integer)
    m_iFilterOperator = iNew
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh()
    Set m_oItems = RefreshListItems()
End Sub

'**********************************************************
'   Class Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    Me.FilterOperator = mpListFilterOperator_Is
End Sub

Private Sub Class_Terminate()
    Set m_oDB = Nothing
    Set m_oItems = Nothing
End Sub

'**********************************************************
'   Internal Procs
'**********************************************************
Private Function RefreshListItems() As CListItems
' fills list items collection with records from db - uses
' xListSQL if supplied, else retrieves SQL from record in
' tblLists in Public DB. will append WHERE clause or modify
' WHERE clause if xParamValue is supplied, SQL is gotten
' from tblLists, and ParamField is specified in tblLists.
' xParamValue should be of the form "o XXX", where o is a
' valid SQL operator (eg = > < LIKE), and XXX is a value.
' Example: "='CA'"
    Dim oRS As DAO.Recordset
    Dim oItems As CListItems
    Dim vID As Variant
    Dim xDisplayText As String
    Dim vSuppValue As Variant
    Dim xSQL As String
    Dim xFilterValue As String
    Dim xOp As String
    Dim xWhere As String
    Dim xListSQL As String
    Dim xDesc As String
    Dim lErr As Long
       
    On Error GoTo ProcError
    
'   create WHERE clause if filter value and field are specified and the
'   list source is a SQL recordset
    If Me.IsDBList Then
'       start with a fresh collection
        Set oItems = New CListItems
        If Me.SQL = "" Then
'           get list definition from db
            xSQL = "SELECT * FROM tblLists WHERE " & _
                   "fldListName = '" & Me.Name & "'"
            
            Set oRS = Application.PublicDB.OpenRecordset(xSQL)
            With oRS
                If Not (.BOF And .EOF) Then
                    .MoveFirst
                    Me.SQL = !fldSQL
                    If Not IsNull(!fldFilterField) Then
                        Me.FilterField = !fldFilterField
                    End If
                End If
                .Close
            End With
        End If
        
        If (Me.FilterValue <> "") And (Me.FilterField <> "") Then
'           get operator
            Select Case Me.FilterOperator
                Case mpListFilterOperator_IsGreaterThan
                    xOp = " > "
                Case mpListFilterOperator_IsLessThan
                    xOp = " < "
                Case mpListFilterOperator_IsLike
                    xOp = " LIKE "
                Case Else
                    xOp = " = "
            End Select
            
'           modify filter value based on data type
            If Not Me.KeepFilter Then
                Select Case VarType(Me.FilterValue)
                    Case vbString
    '                   enclose in quotes, insert
    '                   wildcards if operator is LIKE
                        If xOp = " LIKE " Then
                            xFilterValue = "'*" & Me.FilterValue & "*'"
                        Else
                            xFilterValue = "'" & Me.FilterValue & "'"
                        End If
                    Case vbDate
    '                   enclose in #
                        xFilterValue = "#" & Me.FilterValue & "#"
                    Case Else
    '                   do nothing
                        xFilterValue = Me.FilterValue
                End Select
            Else
'               do nothing
                xFilterValue = Me.FilterValue
            End If
            
'           build WHERE clause-
            If InStr(Me.SQL, " WHERE ") = 0 Then
                xWhere = " WHERE " & Me.FilterField & xOp & xFilterValue
            Else
                xWhere = " AND (" & Me.FilterField & xOp & xFilterValue & ")"
            End If
        End If
        
        xListSQL = Me.SQL & xWhere
        
'       add sort order
        If Me.SortField <> Empty Then
            xListSQL = xListSQL & " ORDER BY " & Me.SortField
        End If
        
'       get recordset that is the source of the list
        On Error Resume Next
        Err.Clear
        Set oRS = Application.PublicDB.OpenRecordset(xListSQL, _
                                    dbOpenForwardOnly, _
                                    dbForwardOnly + dbReadOnly)
        If Err Then
            lErr = Err.Number
            xDesc = "Could not retrieve the items in the " & Me.Name & " list.  " & _
                "It is possible that the SQL definition for the list is invalid.  " & _
                "Please check tblLists/tblCustomLists in mpPublic.mdb."
            On Error GoTo ProcError
            Err.Raise mpError_InvalidList
        Else
            On Error GoTo ProcError
        End If
            
        With oRS
            Dim iNumFields As Integer
            iNumFields = .Fields.Count
'           fill listitems collection if recordset is not empty
            If Not (.BOF And .EOF) Then
                While Not .EOF
                    On Error Resume Next
                    xDisplayText = .Fields(0)
                    If iNumFields > 1 Then
                        vID = .Fields(1)
                    Else
'                       use display text as ID if ID field is empty
                        vID = .Fields(0)
                    End If
                    If iNumFields > 2 Then
                        vSuppValue = .Fields(2)
                    End If
                    On Error GoTo 0

'                   add record to collection class - assumes that id is field0,
'                   DisplayText is field(1), and SuppValue is field(2)
                    oItems.Add vID, xDisplayText, vSuppValue
                    .MoveNext
                Wend
            End If
            .Close
            Set RefreshListItems = oItems
        End With
    Else
        Set RefreshListItems = New CListItems
    End If
    Exit Function
ProcError:

    RaiseError "mpDB.CList.RefreshListItems", xDesc
    Exit Function
End Function

