VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCaptionBorderStyles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingCaptionBorderStyles Collection Class
'   created 2/29/00 by Jeffrey Sweetland

'   Contains properties and methods that manage the
'   collection of CPleadingCaptionBorderStyle

'   Member of
'   Container for CPleadingCaptionBorderStyle
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_oBorders As CPleadingCaptionBorderStyle
Private m_xarListSource As XArrayObject.XArray

'**********************************************************
'**********************************************************
'   Properties
'**********************************************************
Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property
'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    xSQL = "SELECT * " & _
           "FROM tblPleadingCaptionBorderStyles" & ";"
   
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub
Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function
Public Function Item(lIndex As Long) As mpDB.CPleadingCaptionBorderStyle
Attribute Item.VB_UserMemId = 0
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lIndex
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oBorders
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CPleadingCaptionBorderStyles.Item"
    Exit Function
End Function
'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oBorders = New mpDB.CPleadingCaptionBorderStyle
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub
Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_oBorders = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub
'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oBorders
        On Error Resume Next
        .ID = m_oRS!fldID
        .Description = m_oRS!fldDescription
        .CaptionBottomBorder = m_oRS!fldCaptionBottomBorder
        .CaptionTopBorder = m_oRS!fldCaptionTopBorder
        .CaptionRightBorder = m_oRS!fldCaptionRightBorder
        .CaptionTopBorderRow = m_oRS!fldCaptionTopBorderBorderRow
        .CaptionBottomBorderRow = m_oRS!fldCaptionBottomBorderBorderRow
        .CaptionHorizontalBorder = m_oRS!fldCaptionHorizontalBorder
        .TableBottomBorder = m_oRS!fldTableBottomBorder
        .TableHorizontalBorder = m_oRS!fldTableHorizontalBorder
        .TableTopBorder = m_oRS!fldTableTopBorder
        .CaptionRightBorderSpecialCharacters = m_oRS!fldRightBorderSpecialCharacters
        .CaptionRightBorderSpecialCharacterStart = m_oRS!fldRightBorderSpecialCharacterStart
        .CaptionRightBorderSpecialCharacterEnd = m_oRS!fldRightBorderSpecialCharacterEnd
        .CaptionRightBorderSpecialCharacterHorizontal = m_oRS!fldRightBorderSpecialCharacterHorizontal
        .CaptionRightBorderColumn = m_oRS!fldRightBorderColumn
        
    End With
End Sub
