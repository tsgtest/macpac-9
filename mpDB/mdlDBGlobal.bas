Attribute VB_Name = "mdlDBGlobal"
Option Explicit

Public Const mpFirmINI As String = "MacPac.ini"
Public Const mpUserINI As String = "User.ini"
Public Const mpOptions_FirmDefaultID As Long = -99999999
Public Const mpPeople_ManualEntryIDThreshold As Long = 1200000

Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)

'SQL
Public Const mpSQL_DPhrases As String = "SELECT DISTINCTROW fldDhrases FROM tblDPhrases"
Public Const mpSQL_Offices As String = "SELECT DISTINCTROW fldOfficeDisplayName, fldOfficeID FROM tblOffices ORDER BY fldOfficeDisplayName"
Public Const mpSQL_Letterhead As String = "SELECT DISTINCTROW fldType FROM tblLetterheads WHERE fldRestricted = "
Public Const mpSQL_Filters As String = "SELECT DISTINCTROW fldFilter, fldID FROM tblFiltersLists"

Public Const mpSQL_Attys_LastFirstMI As String = "SELECT DISTINCTROW fldSource, 0 as fldSigner, [fldLastName] & "", "" & [fldFirstName] & IIf(IsNull([fldMI]),"""","" "" & [fldMI]) AS fldFullName, fldID, fldBar_ID FROM qryPeoplePublic WHERE (([fldBar_ID] Is Not Null)) OR (((UCase([fldTitle]))=""ATTORNEY"")) OR (((UCase([fldTitle]))= ""PARTNER"")) OR (((UCase([fldTitle]))= ""ASSOCIATE""))"
Public Const mpSQL_Attys_FirstMILast As String = "SELECT DISTINCTROW fldSource, 0 as fldSigner, [fldFirstName] & "" "" & IIf([fldMI]<>"""",[fldMI] & "" "","""") & [fldLastName] AS fldFullName, fldID, fldBar_ID FROM qryPeoplePublic WHERE (([fldBar_ID] Is Not Null)) OR (((UCase([fldTitle]))=""ATTORNEY"")) OR (((UCase([fldTitle]))= ""PARTNER"")) OR (((UCase([fldTitle]))= ""ASSOCIATE""))"
Public Const mpSQL_Attys_FirstLast As String = "SELECT DISTINCTROW fldSource, 0 as fldSigner, [fldFirstName] & "" ""  & [fldLastName] AS fldFullName, fldID, fldBar_ID FROM qryPeoplePublic WHERE (([fldBar_ID] Is Not Null)) OR (((UCase([fldTitle]))=""ATTORNEY"")) OR (((UCase([fldTitle]))= ""PARTNER"")) OR (((UCase([fldTitle]))= ""ASSOCIATE""))"
Public Const mpSQL_Attys_LastFirst As String = "SELECT DISTINCTROW fldSource, 0 as fldSigner, [fldLastName]  & "", "" & [fldFirstName] AS fldFullName, fldID, fldBar_ID FROM qryPeoplePublic WHERE (([fldBar_ID] Is Not Null)) OR (((UCase([fldTitle]))=""ATTORNEY"")) OR (((UCase([fldTitle]))= ""PARTNER"")) OR (((UCase([fldTitle]))= ""ASSOCIATE""))"

Public m_INI As New mpBase2.CIni
Public g_App As mpDB.CApp
Public g_bInit As Boolean

Public g_lManualEntryIDThreshold As Long

Sub Main()
    On Error GoTo ProcError
    Set g_App = New mpDB.CApp
    g_App.Initialize
    Exit Sub
ProcError:
    RaiseError "mpDB.mdlDBGlobal.Main"
    Exit Sub
End Sub

Public Function Application() As mpDB.CApp
    If (g_App Is Nothing) Then
        Set g_App = New mpDB.CApp
        g_App.Initialize
    End If
    Set Application = g_App
End Function

Public Function CurErrDesc() As String
'returns the description of the current error
    CurErrDesc = Application.Error.Desc(Err.Number)
End Function

Public Function CurErrSource(ByVal xNewSource As String) As String
'returns the description of the current error
    CurErrSource = Application.Error.Source(xNewSource, Err.Source)
End Function

Public Function RaiseError(ByVal xSource As String, Optional ByVal xDescription As String)
    With Application.Error
'       raise the error
        Err.Raise Err.Number, _
              .Source(xSource, Err.Source), _
              .Desc(Err.Number, xDescription)
    End With
End Function

Public Sub DebugOutput(ByVal OutputString As String)
     
    Call OutputDebugString(OutputString)
     
End Sub



