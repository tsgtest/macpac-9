VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPersonDetailControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
  Option Explicit

'**********************************************************
'   CPersonDetailControl Class
'   created 2/10/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods of a
'   MacPac Person Detail Control - the
'   control associated with a field in the
'   person detail form

'   Member of App
'   Container for
'**********************************************************
Public Enum mpPersonDetailCtls
    mpPersonDetailCtl_TextBox = 1
    mpPersonDetailCtl_ListBox = 2
    mpPersonDetailCtl_ComboBox = 3
    mpPersonDetailCtl_CheckBox = 4
    mpPersonDetailCtl_Label = 5
    mpPersonDetailCtl_SpinText = 6
End Enum

Private m_lID As Long
Private m_xCaption As String
Private m_xLinkedProperty As String
Private m_xName As String
Private m_iControlType As mpPersonDetailCtls
Private m_iIndex As Integer
Private m_oList As CList
Private m_iVOffset As Integer
Private m_iHOffset As Integer
Private m_iWidth As Integer
Private m_sMin As Single
Private m_sMax As Single
Private m_sIncrement As Single
Private m_bAppendQuotes As Boolean
Private m_iLines As Integer



'**********************************************************
'   Properties
'**********************************************************
Friend Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let Caption(xNew As String)
    m_xCaption = xNew
End Property

Public Property Get Caption() As String
    Caption = m_xCaption
End Property

Friend Property Let LinkedProperty(xNew As String)
    m_xLinkedProperty = xNew
End Property

Public Property Get LinkedProperty() As String
    LinkedProperty = m_xLinkedProperty
End Property

Public Property Get ControlType() As mpPersonDetailCtls
    ControlType = m_iControlType
End Property

Friend Property Let ControlType(iNew As mpPersonDetailCtls)
    m_iControlType = iNew
End Property

Public Property Get Index() As Integer
    Index = m_iIndex
End Property

Friend Property Let Index(iNew As Integer)
    m_iIndex = iNew
End Property

Friend Property Set List(oNew As mpDB.CList)
    Set m_oList = oNew
End Property

Public Property Get List() As mpDB.CList
    Set List = m_oList
End Property

Public Property Get VerticalOffset() As Integer
    VerticalOffset = m_iVOffset
End Property

Public Property Let VerticalOffset(iNew As Integer)
    m_iVOffset = iNew
End Property

Public Property Get Lines() As Integer
    Lines = m_iLines
End Property

Public Property Let Lines(iNew As Integer)
    m_iLines = iNew
End Property


Public Property Get HorizontalOffset() As Integer
    HorizontalOffset = m_iHOffset
End Property

Public Property Let HorizontalOffset(iNew As Integer)
    m_iHOffset = iNew
End Property

Public Property Get Width() As Integer
    Width = m_iWidth
End Property

Public Property Let Width(iNew As Integer)
    m_iWidth = iNew
End Property

Public Property Get MinValue() As Single
    MinValue = m_sMin
End Property

Public Property Let MinValue(sNew As Single)
    m_sMin = sNew
End Property

Public Property Get MaxValue() As Single
    MaxValue = m_sMax
End Property

Public Property Let MaxValue(sNew As Single)
    m_sMax = sNew
End Property

Public Property Get Increment() As Single
    Increment = m_sIncrement
End Property

Public Property Let Increment(sNew As Single)
    m_sIncrement = sNew
End Property

Public Property Let AppendQuotes(bNew As Boolean)
    m_bAppendQuotes = bNew
End Property

Public Property Get AppendQuotes() As Boolean
    AppendQuotes = m_bAppendQuotes
End Property

'**********************************************************
'   Methods
'**********************************************************
'**********************************************************
'   Class Events
'**********************************************************

