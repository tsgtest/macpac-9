VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplates"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CTemplates Collection Class
'   created 11/20/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CListItems - list items collection is an XArray

'   Member of CList
'   Container for CListItems
'**********************************************************

'**********************************************************
Private Enum xarCols
    xarCol_ID = 0
    xarCol_Name = 1
    xarCol_FileName = 2
    xarCol_Type = 3
    xarCol_Group = 4
End Enum

Private m_oArray As XArrayObject.XArray
Private m_oTemplate As CTemplate
Private m_rsTemplates As DAO.Recordset
Private m_iGroup As Integer
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let Group(iNew As Integer)
    m_iGroup = iNew
    Me.Refresh
End Property

Public Property Get Group() As Integer
    Group = m_iGroup
End Property

Friend Property Let Source(xarNew As XArray)
    Set m_oArray = xarNew
End Property

Public Property Get Source() As XArray
    If m_oArray Is Nothing Then
        Me.Refresh
    End If
    
    Set Source = m_oArray
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Add(ByVal xName As String, _
               ByVal xFileName As String, _
               ByVal iType As mpTemplateTypes, _
               ByVal lGroup As Long)
               
    Dim lUBound As Long
    Dim oGroups As CTemplateGroups
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If m_oArray Is Nothing Then
        Me.Refresh
    End If
    
    Set oGroups = New CTemplateGroups
    
'   alert if no template groups
    If oGroups.Count = 0 Then
        Err.Raise mpError_NoTemplateGroups
        Exit Sub
    End If
    
'   test for existence of group
    If oGroups.Item(lGroup) Is Nothing Then
        Err.Raise mpError_TemplateGroupDoesNotExist
    ElseIf xName = Empty Then
'       invalid template name
        Err.Raise mpError_InvalidTemplateName
    ElseIf xFileName = Empty Then
'       invalid template file name
        Err.Raise mpError_InvalidTemplateFileName
    End If
    
    Dim xSQL As String
    With m_oArray
        lUBound = m_oArray.UpperBound(1)
        .Insert 1, lUBound + 1
        .Value(lUBound + 1, xarCol_Name) = xName
        .Value(lUBound + 1, xarCol_FileName) = xFileName
        .Value(lUBound + 1, xarCol_Type) = iType
        .Value(lUBound + 1, xarCol_Group) = lGroup
    End With

    Set oGroups = Nothing
    Exit Sub
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplates.Add", _
              Application.Error.Desc(Err.Number)
    Exit Sub
End Sub

Friend Sub Delete(lID As Long)
    Dim xSQL As String
    xSQL = "DELETE * FROM tblTemplates WHERE fldID = " & lID
    Application.PublicDB.Execute xSQL
    Me.Refresh
End Sub

Friend Sub DeleteAll()
    Dim xSQL As String
    xSQL = "DELETE * FROM tblTemplates"
    Application.PublicDB.Execute xSQL
    Me.Refresh
End Sub

Public Function Count() As Long
    If m_oArray Is Nothing Then
        Me.Refresh
    End If
    
    On Error Resume Next
    Count = m_oArray.Count(1)
End Function

Public Function Item(Optional ByVal vIndex As Variant, Optional ByVal vID As Variant) As CTemplate
Attribute Item.VB_Description = "Returns theCTemplate item with ID = vID or Index = lIndex"
Attribute Item.VB_UserMemId = 0
    Dim i As Integer
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If m_oArray Is Nothing Then
        Me.Refresh
    End If
    
    
    If Not IsMissing(vIndex) Then
'       use index
        vIndex = Max(CDbl(vIndex), 0)
        On Error Resume Next
        m_oTemplate.ID = m_oArray.Value(vIndex, xarCol_ID)
        On Error GoTo ProcError
        
        If m_oTemplate.ID = Empty Then
            Err.Raise mpError_InvalidMember
            Exit Function
        Else
            Set m_oTemplate = New CTemplate
        End If
        
        m_oTemplate.ID = m_oArray.Value(vIndex, xarCol_ID)
        m_oTemplate.Name = m_oArray.Value(vIndex, xarCol_Name)
        m_oTemplate.FileName = m_oArray.Value(vIndex, xarCol_FileName)
        m_oTemplate.TemplateType = m_oArray.Value(vIndex, xarCol_Type)
        m_oTemplate.Group = m_oArray.Value(vIndex, xarCol_Group)
    Else
        If IsNumeric(vID) Then
'           get item where id = vID
            For i = 1 To m_oArray.UpperBound(1) ' - 1
                If m_oArray.Value(i, xarCol_ID) = vID Then
                    Set m_oTemplate = New CTemplate
                    m_oTemplate.Name = m_oArray.Value(i, xarCol_Name)
                    m_oTemplate.ID = m_oArray.Value(i, xarCol_ID)
                    m_oTemplate.FileName = m_oArray.Value(i, xarCol_FileName)
                    m_oTemplate.TemplateType = m_oArray.Value(i, xarCol_Type)
                    m_oTemplate.Group = m_oArray.Value(i, xarCol_Group)
                    Exit For
                End If
            Next i
        Else
'           get item where name = vID
            For i = 1 To m_oArray.UpperBound(1) ' - 1
                If m_oArray.Value(i, xarCol_Name) = vID Then
                    Set m_oTemplate = New CTemplate
                    m_oTemplate.Name = m_oArray.Value(i, xarCol_Name)
                    m_oTemplate.ID = m_oArray.Value(i, xarCol_ID)
                    m_oTemplate.FileName = m_oArray.Value(i, xarCol_FileName)
                    m_oTemplate.TemplateType = m_oArray.Value(i, xarCol_Type)
                    m_oTemplate.Group = m_oArray.Value(i, xarCol_Group)
                    Exit For
                End If
            Next i
        End If
        
'       if no match found, the
'       member does not exist
        If (m_oTemplate Is Nothing) Then
            Err.Raise mpError_InvalidMember
        End If
    End If
    
    Set Item = m_oTemplate
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplates.Item", _
              Application.Error.Desc(Err.Number)
End Function

Public Sub Save(lError)

    If m_oArray Is Nothing Then
        Me.Refresh
    End If
End Sub

Public Sub Refresh()
    Dim xSQL As String
    Dim l As Long
    Dim lID As Long
    Dim xName As String
    Dim xFileName As String
    Dim lGroupID As Long
    Dim iType As mpTemplateTypes
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
        
    Set m_oArray = New XArrayObject.XArray
    m_oArray.ReDim 0, -1, 0, 4
    
    If Me.Group Then
'       get all templates in group
        xSQL = "SELECT * FROM tblTemplates WHERE fldGroupID = " & Me.Group
    Else
'       get all templates
        xSQL = "SELECT * FROM tblTemplates"
    End If
    
'   open the rs
    Set m_rsTemplates = Application.PublicDB.OpenRecordset(xSQL)
    
    With m_rsTemplates
        If .RecordCount Then
            .MoveLast
'           create space in xarray
            m_oArray.ReDim 1, .RecordCount, 0, 4
            .MoveFirst
            l = 1
'           cycle through records, filling array
            While Not .EOF
                On Error Resume Next
                xName = !fldName
                xFileName = !fldFileName
                lID = !fldID
                iType = !fldType
                lGroupID = !fldGroupID
                #If compHandleErrors Then
                    On Error GoTo ProcError
                #End If
                m_oArray.Value(l, xarCol_Name) = xName
                m_oArray.Value(l, xarCol_ID) = lID
                m_oArray.Value(l, xarCol_FileName) = xFileName
                m_oArray.Value(l, xarCol_Type) = iType
                m_oArray.Value(l, xarCol_Group) = lGroupID
                .MoveNext
                l = l + 1
            Wend
        End If
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpDB.CTemplates.Refresh"
    Exit Sub
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oTemplate = New mpDB.CTemplate
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
    Set m_rsTemplates = Nothing
End Sub
