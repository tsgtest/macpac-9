VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessSignatureDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Business Signature Types Collection Class
'   created 3/14/00 by Jeffrey Sweetland

'   Contains properties and methods that manage the
'   collection of Business Signature Types types -
'   collection is a recordset, though an XArray
'   belongs to class for display purposes

'   Member of CApp
'   Container for CBusinessSignatureDef
'**********************************************************

'**********************************************************
Enum mpTriState
    mpTrue = -1
    mpFalse = 0
    mpUndefined = 1
End Enum

Private m_oRS As DAO.Recordset
Private m_bFullRowFormat As mpTriState
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property
Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property
Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property
'---9.5.0
Public Property Let FullRowFormat(bNew As mpTriState)
    m_bFullRowFormat = bNew
    Refresh
End Property
Public Property Get FullRowFormat() As mpTriState
    FullRowFormat = m_bFullRowFormat
End Property
'---End 9.5.0

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String
   
   xSQL = "SELECT tblBusinessSignatureTypes.* " & _
           "FROM tblBusinessSignatureTypes"
     
'    If FullRowFormat = mpTrue Then
'        xSQL = xSQL & " WHERE fldFullRowFormat=-1"
'    ElseIf FullRowFormat = mpFalse Then
'        xSQL = xSQL & " WHERE fldFullRowFormat=0"
'    End If
    xSQL = xSQL & ";"
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function
Public Function Item(lIndex As Long) As mpDB.CBusinessSignatureDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim opSig As CBusinessSignatureDef
    
    On Error GoTo ProcError
    
'   set criteria
    xCriteria = "fldID = " & lIndex
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
            Set opSig = New CBusinessSignatureDef
'           found - update current object
            UpdateObject opSig
            
'           return current object
            Set Item = opSig
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CBusinessSignatureDefs.Item"
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject(opSig As CBusinessSignatureDef)
    With opSig
        On Error Resume Next
        .ID = m_oRS!fldID
        .Description = m_oRS!fldDescription
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .IncludesDate = m_oRS!fldIncludesDate
        .SignatureBlockBookmark = m_oRS!fldSignatureBlockBookmark
        .SubEntityBookmark = m_oRS!fldSubEntityBookmark
        .TableBookmark = m_oRS!fldTableBookmark
        .AllowSubEntities = m_oRS!fldAllowSubEntities
        .FullRowFormat = m_oRS!fldFullRowFormat '* 9.5.0
        .DateFormats = m_oRS!fldDateFormats
        .DefaultDate = m_oRS!fldDefaultSignatureDate
        .AllowEntityTitle = m_oRS!fldAllowEntityTitle
        .AllowPartyName = m_oRS!fldAllowPartyName
        .AllowPartyDescription = m_oRS!fldAllowPartyDescription
        .AllowSignerName = m_oRS!fldAllowSignerName
        .AllowSignerTitle = m_oRS!fldAllowSignerTitle
        .AllowIts = m_oRS!fldAllowIts
        .AllowSubPartyName = m_oRS!fldAllowSubPartyName
        .AllowSubPartyDescription = m_oRS!fldAllowSubPartyDescription
        .AllowSubSignerName = m_oRS!fldAllowSubSignerName
        .AllowSubSignerTitle = m_oRS!fldAllowSubSignerTitle
        .AllowSubIts = m_oRS!fldAllowSubIts
    End With
End Function
