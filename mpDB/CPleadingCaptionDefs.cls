VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCaptionDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
 Option Explicit

'**********************************************************
'   Pleading Caption Types Collection Class
'   created 1/2/00 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Letterhead types - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for COffice
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String
   
   xSQL = "SELECT tblPleadingCaptionTypes.* " & _
           "FROM tblPleadingCaptionTypes " & ";"
     
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function
Public Function Item(lID As Long) As mpDB.CPleadingCaptionDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim opCaption As CPleadingCaptionDef
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
            Set opCaption = New CPleadingCaptionDef
'           found - update current object
            UpdateObject opCaption
            
'           return current object
            Set Item = opCaption
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingCaptionDefs.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

Private Function UpdateObject(opCaption As CPleadingCaptionDef)
    With m_oRS
        On Error Resume Next
        
        opCaption.ID = Empty
        opCaption.Description = Empty
        opCaption.BoilerplateFile = Empty
        opCaption.LeftColBoilerplateBookmark = Empty
        opCaption.RightColBoilerplateBookmark = Empty
        opCaption.PartiesInCaption = Empty
        opCaption.DateTimeInfoString = Empty
        opCaption.DateTimeInfoSeparator = Empty
        opCaption.Party1TitleDefault = Empty
        opCaption.Party2TitleDefault = Empty
        opCaption.Party3TitleDefault = Empty
        opCaption.Party1Label = Empty
        opCaption.Party2Label = Empty
        opCaption.Party3Label = Empty
        opCaption.CaseNumber1PrefillText = Empty
        opCaption.CaseNumber2PrefillText = Empty
        opCaption.CaseNumber3PrefillText = Empty
        opCaption.CaseNumber4PrefillText = Empty
        opCaption.CaseNumber5PrefillText = Empty
        opCaption.CaseNumber6PrefillText = Empty
        opCaption.CaseNumber1Label = Empty
        opCaption.CaseNumber2Label = Empty
        opCaption.CaseNumber3Label = Empty
        opCaption.CaseNumber4Label = Empty
        opCaption.CaseNumber5Label = Empty
        opCaption.CaseNumber6Label = Empty
        opCaption.CaseNumber1Separator = Empty
        opCaption.CaseNumber2Separator = Empty
        opCaption.CaseNumber3Separator = Empty
        opCaption.CaseNumber4Separator = Empty
        opCaption.CaseNumber5Separator = Empty
        opCaption.CaseNumber6Separator = Empty
        opCaption.CaptionBorderStyle = Empty
        opCaption.IsCrossAction = Empty
        opCaption.IncludesCaseTitle = Empty
        opCaption.IncludesDateTimeInfo = Empty
        opCaption.IncludesPresidingJudge = Empty
        opCaption.IncludesOnAppealFromCourt = Empty
        opCaption.OnAppealFromPrefillText = Empty
        opCaption.AllowsAdditionalInformation = Empty
        opCaption.UnderlineDocumentTitle = Empty
        opCaption.UseBoilerplateBorders = Empty
        opCaption.RowsPerCaption = Empty
        opCaption.OnAppealFromJurisdictionsID = Empty
        opCaption.AllowRelatedActionInput = Empty
        opCaption.OfficeAddressFormat = Empty
        opCaption.FormatFirmName = Empty
        
        opCaption.ID = !fldID
        opCaption.Description = !fldDescription
        opCaption.BoilerplateFile = !fldBoilerplateFile
        opCaption.LeftColBoilerplateBookmark = !fldLeftColBoilerplateBookmark
        opCaption.RightColBoilerplateBookmark = !fldRightColBoilerplateBookmark
        opCaption.PartiesInCaption = !fldPartiesInCaption
        opCaption.DateTimeInfoString = !fldDateTimeInfoString
        opCaption.DateTimeInfoSeparator = xNullToString(!fldDateTimeInfoSeparator)
        opCaption.Party1TitleDefault = !fldParty1TitleDefault
        opCaption.Party2TitleDefault = !fldParty2TitleDefault
        opCaption.Party3TitleDefault = !fldParty3TitleDefault
        opCaption.Party1Label = !fldParty1Label
        opCaption.Party2Label = !fldParty2Label
        opCaption.Party3Label = !fldParty3Label
        opCaption.CaseNumber1PrefillText = !fldCaseNumber1PrefillText
        opCaption.CaseNumber2PrefillText = !fldCaseNumber2PrefillText
        opCaption.CaseNumber3PrefillText = !fldCaseNumber3PrefillText
        opCaption.CaseNumber4PrefillText = !fldCaseNumber4PrefillText
        opCaption.CaseNumber5PrefillText = !fldCaseNumber5PrefillText
        opCaption.CaseNumber6PrefillText = !fldCaseNumber6PrefillText
        opCaption.CaseNumber1Label = !fldCaseNumber1Label
        opCaption.CaseNumber2Label = !fldCaseNumber2Label
        opCaption.CaseNumber3Label = !fldCaseNumber3Label
        opCaption.CaseNumber4Label = !fldCaseNumber4Label
        opCaption.CaseNumber5Label = !fldCaseNumber5Label
        opCaption.CaseNumber6Label = !fldCaseNumber6Label
        opCaption.CaseNumber1Separator = !fldCaseNumber1Separator
        opCaption.CaseNumber2Separator = !fldCaseNumber2Separator
        opCaption.CaseNumber3Separator = !fldCaseNumber3Separator
        opCaption.CaseNumber4Separator = !fldCaseNumber4Separator
        opCaption.CaseNumber5Separator = !fldCaseNumber5Separator
        opCaption.CaseNumber6Separator = !fldCaseNumber6Separator
        opCaption.CaptionBorderStyle = !fldCaptionBorderStyle
        opCaption.IsCrossAction = !fldIsCrossAction
        opCaption.IncludesCaseTitle = !fldIncludesCaseTitle
        opCaption.IncludesDateTimeInfo = !fldIncludesDateTimeInfo
        opCaption.IncludesPresidingJudge = !fldIncludesPresidingJudge
        opCaption.IncludesOnAppealFromCourt = !fldIncludesOnAppealFromCourt
        opCaption.OnAppealFromPrefillText = !fldOnAppealFromPrefillText
        opCaption.AllowsAdditionalInformation = !fldAllowsAdditionalInformation
        opCaption.UnderlineDocumentTitle = !fldUnderlineDocumentTitle
        opCaption.UseBoilerplateBorders = !fldUseBoilerplateBorders
        opCaption.RowsPerCaption = !fldRowsPerCaption
        opCaption.OnAppealFromJurisdictionsID = !fldOnAppealFromJurisdictionsID
        opCaption.AllowRelatedActionInput = !fldAllowRelatedActionInput
        opCaption.OfficeAddressFormat = !fldOfficeAddressFormat
        opCaption.FormatFirmName = !fldFormatFirmName
        
    End With
End Function
