VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCounselDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Pleading Caption Type Class
'   created 1/2/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define macpac letterhead

'   Member of
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xDescription As String
Private m_xBoilerplateFile As String
Private m_bAllowFirmSlogan As Boolean
Private m_bAllowAdditionalFirmSlogan As Boolean
Private m_bAllowAdditionalFirmAddress As Boolean
Private m_bAllowAdditionalFirmPhone As Boolean
Private m_bAllowAdditionalFirmFax As Boolean
Private m_bAllowFirmFax As Boolean
Private m_bAllowSignerEmail As Boolean
Private m_bAllowSignerEmailInput As Boolean
Private m_bFullRowFormat As Boolean
Private m_bAttorneyRequired As Boolean
Private m_xClientTitleDefault
Private m_xBarIDPrefix As String
Private m_xBarIDSuffix As String
Private m_bIncludeBarID As Boolean
Private m_lRowsPerCounsel As Long
Private m_lDefaultPosition As Long
Private m_xSignerEmailPrefix As String
Private m_xSignerEmailSuffix As String
Private m_bIncludeSignerEmail As Boolean
Private m_lSignerNameCase As SignerNameCase
Private m_lOfficeAddressFormat As Long
Private m_bFormatFirmName As Boolean


Public Enum SignerNameCase
    SignerNameCase_None = 0
    SignerNameCase_Upper = 1
    SignerNameCase_Proper = 3
End Enum



'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let BoilerplateFile(xNew As String)
    m_xBoilerplateFile = xNew
End Property

Public Property Get FullRowFormat() As Boolean
    FullRowFormat = m_bFullRowFormat
End Property

Public Property Let FullRowFormat(bNew As Boolean)
    m_bFullRowFormat = bNew
End Property

Public Property Get AttorneyRequired() As Boolean
    AttorneyRequired = m_bAttorneyRequired
End Property

Public Property Let AttorneyRequired(bNew As Boolean)
    m_bAttorneyRequired = bNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBoilerplateFile
End Property
Public Property Let AllowAdditionalFirmPhone(bNew As Boolean)
    m_bAllowAdditionalFirmPhone = bNew
End Property
Public Property Get AllowAdditionalFirmPhone() As Boolean
    AllowAdditionalFirmPhone = m_bAllowAdditionalFirmPhone
End Property
Public Property Let AllowAdditionalFirmSlogan(bNew As Boolean)
    m_bAllowAdditionalFirmSlogan = bNew
End Property

Public Property Get AllowAdditionalFirmSlogan() As Boolean
    AllowAdditionalFirmSlogan = m_bAllowAdditionalFirmSlogan
End Property

Public Property Let AllowSignerEMail(bNew As Boolean)
    m_bAllowSignerEmail = bNew
End Property

Public Property Get AllowSignerEMail() As Boolean
    AllowSignerEMail = m_bAllowSignerEmail
End Property

Public Property Let AllowSignerEmailInput(bNew As Boolean)
    m_bAllowSignerEmailInput = bNew
End Property

Public Property Get AllowSignerEmailInput() As Boolean
    AllowSignerEmailInput = m_bAllowSignerEmailInput
End Property

Public Property Let AllowFirmFax(bNew As Boolean)
    m_bAllowFirmFax = bNew
End Property

Public Property Get AllowFirmFax() As Boolean
    AllowFirmFax = m_bAllowFirmFax
End Property

Public Property Let AllowAdditionalFirmFax(bNew As Boolean)
    m_bAllowAdditionalFirmFax = bNew
End Property

Public Property Get AllowAdditionalFirmFax() As Boolean
    AllowAdditionalFirmFax = m_bAllowAdditionalFirmFax
End Property

Public Property Let AllowFirmSlogan(bNew As Boolean)
    m_bAllowFirmSlogan = bNew
End Property

Public Property Get AllowFirmSlogan() As Boolean
    AllowFirmSlogan = m_bAllowFirmSlogan
End Property

Public Property Let AllowAdditionalFirmAddress(bNew As Boolean)
    m_bAllowAdditionalFirmAddress = bNew
End Property

Public Property Get AllowAdditionalFirmAddress() As Boolean
    AllowAdditionalFirmAddress = m_bAllowAdditionalFirmAddress
End Property

Public Property Get ClientTitleDefault() As String
    ClientTitleDefault = m_xClientTitleDefault
End Property

Public Property Let ClientTitleDefault(xNew As String)
    m_xClientTitleDefault = xNew
End Property

Public Property Get IncludeBarID() As Boolean
    IncludeBarID = m_bIncludeBarID
End Property

Public Property Let IncludeBarID(bNew As Boolean)
    m_bIncludeBarID = bNew
End Property

Public Property Let BarIDPrefix(xNew As String)
    m_xBarIDPrefix = xSubstitute(xNew, "|", "")
    m_xBarIDPrefix = xTranslateSpecialCharacter(m_xBarIDPrefix)
End Property

Public Property Get BarIDPrefix() As String
    BarIDPrefix = m_xBarIDPrefix
End Property

Public Property Let BarIDSuffix(xNew As String)
    m_xBarIDSuffix = xSubstitute(xNew, "|", "")
    m_xBarIDSuffix = xTranslateSpecialCharacter(m_xBarIDSuffix)
End Property

Public Property Get BarIDSuffix() As String
    BarIDSuffix = m_xBarIDSuffix
End Property

Public Property Get IncludeSignerEmail() As Boolean
    IncludeSignerEmail = m_bIncludeSignerEmail
End Property

Public Property Let IncludeSignerEmail(bNew As Boolean)
    m_bIncludeSignerEmail = bNew
End Property

Public Property Let SignerEmailPrefix(xNew As String)
    m_xSignerEmailPrefix = xSubstitute(xNew, "|", "")
    m_xSignerEmailPrefix = xTranslateSpecialCharacter(m_xSignerEmailPrefix)
End Property

Public Property Get SignerEmailPrefix() As String
    SignerEmailPrefix = m_xSignerEmailPrefix
End Property

Public Property Let SignerEmailSuffix(xNew As String)
    m_xSignerEmailSuffix = xSubstitute(xNew, "|", "")
    m_xSignerEmailSuffix = xTranslateSpecialCharacter(m_xSignerEmailSuffix)
End Property

Public Property Get SignerEmailSuffix() As String
    SignerEmailSuffix = m_xSignerEmailSuffix
End Property

Public Property Let RowsPerCounsel(lNew As Long)
    m_lRowsPerCounsel = lNew
End Property

Public Property Get RowsPerCounsel() As Long
'---defaulted to 1
    If m_lRowsPerCounsel = 0 Then m_lRowsPerCounsel = 1
    RowsPerCounsel = m_lRowsPerCounsel
End Property

Public Property Let DefaultPosition(lNew As Long)
    m_lDefaultPosition = lNew
End Property

Public Property Get DefaultPosition() As Long
'---defaulted to 1
    If m_lDefaultPosition = 0 Then m_lDefaultPosition = 1
    DefaultPosition = m_lDefaultPosition
End Property

Public Property Let SignerNameCase(ByVal NewValue As SignerNameCase)
    m_lSignerNameCase = NewValue
End Property

Public Property Get SignerNameCase() As SignerNameCase
    SignerNameCase = m_lSignerNameCase
End Property

Public Property Let OfficeAddressFormat(lNew As Long)
    m_lOfficeAddressFormat = lNew
End Property

Public Property Get OfficeAddressFormat() As Long
    OfficeAddressFormat = m_lOfficeAddressFormat
End Property

Public Property Let FormatFirmName(bNew As Boolean)
    m_bFormatFirmName = bNew
End Property

Public Property Get FormatFirmName() As Boolean
    FormatFirmName = m_bFormatFirmName
End Property




