VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingPaperDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   Pleading Paper Definitions Collection Class
'   created 10/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that manage the
'   collection of Pleading Paper defs - collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for CPleadingPaper
'**********************************************************

'**********************************************************
Private m_rsPleadingPaper As DAO.Recordset
Private m_xarListSource As XArrayObject.XArray
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property
Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property
Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property
'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional xFilterState As String = "", Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    On Error GoTo ProcError
    xSQL = "SELECT  * FROM tblPleadingPaperTypes"
    
    If xFilterState <> "" Then
        xSQL = xSQL & " WHERE fldParent = '" & xFilterState & "'"
    End If

    xSQL = xSQL & " ORDER BY fldDescription"
    
'   open recordset
    Set m_rsPleadingPaper = Application.PublicDB.OpenRecordset(xSQL, _
                                                            dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_rsPleadingPaper
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        Else
            Err.Raise mpError_InvalidPleadingState
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
    Exit Sub
ProcError:
    Err.Raise Err.Number, "mpDB.CPleadingPaperDefs", Application.Error.Desc(Err.Number)
End Sub
Public Function Count() As Long
    Count = m_rsPleadingPaper.RecordCount
End Function
Public Function Item(lID As Long) As mpDB.CPleadingPaperDef
Attribute Item.VB_UserMemId = 0
    Dim xCriteria As String
    Dim vBkmk As Variant
    Dim oPleadingPaperDef As CPleadingPaperDef
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_rsPleadingPaper
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            Set oPleadingPaperDef = New CPleadingPaperDef
            UpdateObject oPleadingPaperDef
            
'           return current object
            Set Item = oPleadingPaperDef
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, "mpDB.CPleadingPaperDef.Item"
    Exit Function
End Function
'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub
Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_rsPleadingPaper = Nothing
End Sub
Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xID As String
    Dim xDescription As String
    
    With m_rsPleadingPaper
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xDescription
                m_xarListSource.Value(i, 1) = xID
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub
Private Function UpdateObject(oPleadingPaperDef As CPleadingPaperDef)
    With m_rsPleadingPaper
        On Error Resume Next
        oPleadingPaperDef.ID = Empty
        oPleadingPaperDef.Description = Empty
        oPleadingPaperDef.BoilerplateFile = Empty
        oPleadingPaperDef.TopMargin = Empty
        oPleadingPaperDef.BottomMargin = Empty
        oPleadingPaperDef.LeftMargin = Empty
        oPleadingPaperDef.RightMargin = Empty
        oPleadingPaperDef.HeaderDistance = Empty
        oPleadingPaperDef.FooterDistance = Empty
        oPleadingPaperDef.State = Empty
        oPleadingPaperDef.LineSpacing = Empty
        oPleadingPaperDef.PageIntroChar = Empty
        oPleadingPaperDef.PageTrailingChar = Empty
        oPleadingPaperDef.DefaultSidebarType = Empty
        oPleadingPaperDef.UsesFirstPageFooter = Empty
        oPleadingPaperDef.UsesPrimaryFooter = Empty
        oPleadingPaperDef.UsesFirstPageHeader = !empty
        oPleadingPaperDef.UsesPrimaryHeader = Empty
        oPleadingPaperDef.PageHeight = Empty
        oPleadingPaperDef.PageWidth = Empty
        oPleadingPaperDef.AllowSideBar = Empty
        oPleadingPaperDef.FormatFirmName = Empty
        oPleadingPaperDef.Orientation = Empty
        oPleadingPaperDef.HeaderSpacerHeight = Empty
        '---9.5.2
        oPleadingPaperDef.PageNumberLocation = Empty
        oPleadingPaperDef.OfficeAddressFormat = Empty
        
        oPleadingPaperDef.ID = !fldID
        oPleadingPaperDef.Description = !fldDescription
        oPleadingPaperDef.BoilerplateFile = !fldBoilerplateFile
        oPleadingPaperDef.TopMargin = !fldTopMargin
        oPleadingPaperDef.BottomMargin = !fldBottomMargin
        oPleadingPaperDef.LeftMargin = !fldLeftMargin
        oPleadingPaperDef.RightMargin = !fldRightMargin
        oPleadingPaperDef.HeaderDistance = !fldHeaderDistance
        oPleadingPaperDef.FooterDistance = !fldFooterDistance
        oPleadingPaperDef.State = !fldParent
        oPleadingPaperDef.LineSpacing = !fldLineSpacing
        oPleadingPaperDef.PageIntroChar = xTrimTrailingChrs(!fldPageIntroChar, "|", True, True)
        oPleadingPaperDef.PageTrailingChar = xTrimTrailingChrs(!fldPageTrailingChar, "|", True, True)
        oPleadingPaperDef.DefaultSidebarType = !fldDefaultSidebarType
        
        oPleadingPaperDef.UsesFirstPageFooter = !fldUsesFirstPageFooter
        oPleadingPaperDef.UsesPrimaryFooter = !fldUsesPrimaryFooter
        oPleadingPaperDef.UsesFirstPageHeader = !fldUsesFirstPageHeader
        oPleadingPaperDef.UsesPrimaryHeader = !fldUsesPrimaryHeader
        oPleadingPaperDef.PageHeight = !fldPageHeight
        oPleadingPaperDef.PageWidth = !fldPageWidth
        oPleadingPaperDef.Orientation = !fldOrientation
        oPleadingPaperDef.AllowSideBar = !fldAllowSideBar
        oPleadingPaperDef.FormatFirmName = !fldFormatFirmName
        oPleadingPaperDef.HeaderSpacerHeight = !fldHeaderSpacerHeight

        '---9.5.2
        oPleadingPaperDef.PageNumberLocation = !fldPageNumberLocation
        oPleadingPaperDef.OfficeAddressFormat = !fldOfficeAddressFormat
        
    End With
End Function
