VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBusinessDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CBusinessDefs Class
'   created 3/21/00 by Dan Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage the MacPac Business Types
'**********************************************************
Option Explicit

Private m_oDef As mpDB.CBusinessDef
Private m_oRS As DAO.Recordset
Private m_oArray As XArray

Public Property Get ListSource() As XArray
    If (m_oArray Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_oArray
End Property

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function

Public Function Item(vID As Variant) As CBusinessDef
Attribute Item.VB_UserMemId = 0
'returns the document look whose id or name = vid
    Dim xCrit As String
    Dim vBkmk As Variant
    
        
    On Error GoTo ProcError
    
'   set criteria
    If IsNumeric(vID) Then
        xCrit = "fldID = " & vID
    Else
        xCrit = "fldName = '" & vID & "'"
    End If
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCrit
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CBusinessDefs.Item"
    Exit Function
End Function

Public Sub Refresh()
    Dim xSQL As String
    
    On Error GoTo ProcError
    '9.7.1 - #4168
    xSQL = "SELECT * FROM tblBusinessTypes ORDER BY fldName"
    
    
    Set m_oRS = Application.PublicDB _
        .OpenRecordset(xSQL, dbOpenSnapshot)
        
    With m_oRS
        If .RecordCount Then
'           populate
            .MoveLast
            .MoveFirst
'           refresh the list source
            RefreshXArray
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CBusinessDefs.Refresh"
    Exit Sub
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xName As String
    
    On Error GoTo ProcError
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
'           populate if necessary
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            
'           create storage
            Set m_oArray = New XArray
            m_oArray.ReDim 0, lNumRows - 1, 0, 1
            
'           cycle through records, storing values in array
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                
                xID = Empty
                xName = Empty
                
                On Error Resume Next
                xID = !fldID
                xName = !fldName
                m_oArray.Value(i, 0) = xID
                m_oArray.Value(i, 1) = xName
                On Error GoTo ProcError
            Next i
        Else
            m_oArray.ReDim 0, -1, 0, 2
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CBusinessDefs.RefreshXArray"
    Exit Sub
End Sub

Private Sub UpdateObject()
    Set m_oDef = New CBusinessDef
    With m_oDef
        .BottomMargin = Empty
        .Columns = Empty
        .LeftMargin = Empty
        .DifferentPage1HeaderFooter = Empty
        .SpaceBetweenColumns = Empty
        .TopMargin = Empty
        .RightMargin = Empty
        .ID = Empty
        .Boilerplate = Empty
        .Orientation = Empty
        .Name = Empty
        .AllowDeliveryPhrases = Empty
        .AllowDocumentTitle = Empty
        .PageHeight = Empty
        .PageWidth = Empty
        .SplitDPhrases = Empty '9.7.1 #4024
        .DefaultTitlePage = Empty
        .DefaultNumberingScheme = Empty
        .DPhrase1Caption = Empty '9.7.1 #4024
        .DPhrase2Caption = Empty '9.7.1 #4024
        
        On Error Resume Next
        .ID = m_oRS!fldID
        .Name = m_oRS!fldName
        .BottomMargin = m_oRS!fldBottomMargin
        .Columns = m_oRS!fldColumns
        .LeftMargin = m_oRS!fldLeftMargin
        .DifferentPage1HeaderFooter = m_oRS!fldDiffPage1HF
        .TopMargin = m_oRS!fldTopMargin
        .RightMargin = m_oRS!fldRightMargin
        .SpaceBetweenColumns = m_oRS!fldColSpace
        .Boilerplate = m_oRS!fldBoilerplate
        .Orientation = m_oRS!fldOrientation
        .AllowDeliveryPhrases = m_oRS!fldAllowDPhrases
        .AllowDocumentTitle = m_oRS!fldAllowDocTitle
        .PageHeight = m_oRS!fldPageHeight
        .PageWidth = m_oRS!fldPageWidth
        .FullRowSignature = m_oRS!fldFullRowSignature
        .SplitDPhrases = m_oRS!fldSplitDPhrases '9.7.1 #4024
        .DefaultTitlePage = m_oRS!fldDefTitlePage
        .DefaultNumberingScheme = m_oRS!fldDefaultNumberingScheme
        .DPhrase1Caption = m_oRS!fldDPhrase1Caption '9.7.1 #4024
        .DPhrase2Caption = m_oRS!fldDPhrase2Caption '9.7.1 #4024
    End With
End Sub

Private Sub Class_Terminate()
    Set m_oDef = Nothing
    Set m_oRS = Nothing
End Sub
