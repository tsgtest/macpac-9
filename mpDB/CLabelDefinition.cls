VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CLabelDefinition"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Envelope Definition Class
'   created 10/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define an office

'   Member of CEnvelopeDefinitions
'**********************************************************

'**********************************************************
Private m_lID As Long
Private m_xName As String
Private m_xDescription As String
Private m_iDown As Integer
Private m_iAcross As Integer
Private m_xBPRange As String
Private m_xBPGraphicRange As String
Private m_bSpacerRow As Boolean
Private m_bSpacerCol As Boolean
Private m_bAllowDPhrases As Boolean
Private m_bAllowBarCode As Boolean
Private m_bAllowBillingNumber As Boolean
Private m_bAllowOfficeAddress As Boolean
Private m_sLeftIndent As Single
Private m_sTopMargin As Single
Private m_lMaxLines As Integer


'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property
Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property

Public Property Get Description() As String
    Description = m_xDescription
End Property

Public Property Let BoilerplateRange(xNew As String)
    m_xBPRange = xNew
End Property

Public Property Get BoilerplateRange() As String
    BoilerplateRange = m_xBPRange
End Property

Public Property Let BPGraphicRange(xNew As String)
    m_xBPGraphicRange = xNew
End Property

Public Property Get BPGraphicRange() As String
    BPGraphicRange = m_xBPGraphicRange
End Property

Public Property Let TopMargin(sNew As Single)
    m_sTopMargin = sNew
End Property

Public Property Get TopMargin() As Single
    TopMargin = m_sTopMargin
End Property

Public Property Let LeftIndent(sNew As Single)
    m_sLeftIndent = sNew
End Property

Public Property Get LeftIndent() As Single
    LeftIndent = m_sLeftIndent
End Property

Public Property Let NumberDown(iNew As Integer)
    m_iDown = iNew
End Property

Public Property Get NumberDown() As Integer
    NumberDown = m_iDown
End Property

Public Property Let NumberAcross(iNew As Integer)
    m_iAcross = iNew
End Property

Public Property Get NumberAcross() As Integer
    NumberAcross = m_iAcross
End Property

Public Property Let AllowDPhrases(bNew As Boolean)
    m_bAllowDPhrases = bNew
End Property

Public Property Get AllowDPhrases() As Boolean
    AllowDPhrases = m_bAllowDPhrases
End Property

Public Property Let AllowBarCode(bNew As Boolean)
    m_bAllowBarCode = bNew
End Property

Public Property Get AllowBarCode() As Boolean
    AllowBarCode = m_bAllowBarCode
End Property

Public Property Let AllowBillingNumber(bNew As Boolean)
    m_bAllowBillingNumber = bNew
End Property

Public Property Get AllowBillingNumber() As Boolean
    AllowBillingNumber = m_bAllowBillingNumber
End Property

Public Property Let AllowOfficeAddress(bNew As Boolean)
    m_bAllowOfficeAddress = bNew
End Property

Public Property Get AllowOfficeAddress() As Boolean
    AllowOfficeAddress = m_bAllowOfficeAddress
End Property

Public Property Let SpacerRow(bNew As Boolean)
    m_bSpacerRow = bNew
End Property

Public Property Get SpacerRow() As Boolean
    SpacerRow = m_bSpacerRow
End Property

Public Property Let SpacerCol(bNew As Boolean)
    m_bSpacerCol = bNew
End Property

Public Property Get SpacerCol() As Boolean
    SpacerCol = m_bSpacerCol
End Property

Public Property Let MaximumLines(lNew As Long)
    m_lMaxLines = lNew
End Property

Public Property Get MaximumLines() As Long
    MaximumLines = m_lMaxLines
End Property


'**********************************************************
'   Methods
'**********************************************************

Private Sub Class_Terminate()

End Sub



