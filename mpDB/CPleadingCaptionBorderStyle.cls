VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingCaptionBorderStyle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_iTableTopBorder As Integer
Private m_iTableBottomBorder As Integer
Private m_iTableHorizontalBorder As Integer
Private m_iCaptionRightBorder As Integer
Private m_iCaptionBottomBorder As Integer
Private m_iCaptionTopBorder As Integer
Private m_iCaptionBottomBorderRow As Integer
Private m_iCaptionTopBorderRow As Integer
Private m_iCaptionHorizontalBorder As Integer
Private m_xCaptionRightBorderSpecialCharacters As String
Private m_xCaptionRightBorderSpecialCharacterStart As String
Private m_xCaptionRightBorderSpecialCharacterEnd As String
Private m_xCaptionRightBorderSpecialCharacterHorizontal As String
Private m_iCaptionRightBorderColumn As Integer
Private m_xDescription As String
Private m_iID As Integer
Public Property Let ID(iNew As Integer)
    m_iID = iNew
End Property
Public Property Get ID() As Integer
    ID = m_iID
End Property
Public Property Let Description(xNew As String)
    m_xDescription = xNew
End Property
Public Property Get Description() As String
    Description = m_xDescription
End Property
Public Property Let TableTopBorder(iNew As Integer)
    m_iTableTopBorder = iNew
End Property
Public Property Get TableTopBorder() As Integer
    TableTopBorder = m_iTableTopBorder
End Property
Public Property Let TableBottomBorder(iNew As Integer)
    m_iTableBottomBorder = iNew
End Property
Public Property Get TableBottomBorder() As Integer
    TableBottomBorder = m_iTableBottomBorder
End Property
Public Property Let TableHorizontalBorder(iNew As Integer)
    m_iTableHorizontalBorder = iNew
End Property
Public Property Get TableHorizontalBorder() As Integer
    TableHorizontalBorder = m_iTableHorizontalBorder
End Property
Public Property Let CaptionTopBorder(iNew As Integer)
    m_iCaptionTopBorder = iNew
End Property
Public Property Get CaptionTopBorder() As Integer
    CaptionTopBorder = m_iCaptionTopBorder
End Property
Public Property Let CaptionBottomBorder(iNew As Integer)
    m_iCaptionBottomBorder = iNew
End Property
Public Property Get CaptionBottomBorder() As Integer
    CaptionBottomBorder = m_iCaptionBottomBorder
End Property
Public Property Let CaptionTopBorderRow(iNew As Integer)
    m_iCaptionTopBorderRow = iNew
End Property
Public Property Get CaptionTopBorderRow() As Integer
    CaptionTopBorderRow = m_iCaptionTopBorderRow
End Property
Public Property Let CaptionBottomBorderRow(iNew As Integer)
    m_iCaptionBottomBorderRow = iNew
End Property
Public Property Get CaptionBottomBorderRow() As Integer
    CaptionBottomBorderRow = m_iCaptionBottomBorderRow
End Property
Public Property Let CaptionHorizontalBorder(iNew As Integer)
    m_iCaptionHorizontalBorder = iNew
End Property
Public Property Get CaptionHorizontalBorder() As Integer
    CaptionHorizontalBorder = m_iCaptionHorizontalBorder
End Property
Public Property Let CaptionRightBorder(iNew As Integer)
    m_iCaptionRightBorder = iNew
End Property
Public Property Get CaptionRightBorder() As Integer
    CaptionRightBorder = m_iCaptionRightBorder
End Property
Public Property Let CaptionRightBorderSpecialCharacters(xNew As String)
    m_xCaptionRightBorderSpecialCharacters = xNew
End Property
Public Property Get CaptionRightBorderSpecialCharacters() As String
    CaptionRightBorderSpecialCharacters = m_xCaptionRightBorderSpecialCharacters
End Property
Public Property Let CaptionRightBorderSpecialCharacterStart(xNew As String)
    m_xCaptionRightBorderSpecialCharacterStart = xNew
End Property
Public Property Get CaptionRightBorderSpecialCharacterStart() As String
    CaptionRightBorderSpecialCharacterStart = m_xCaptionRightBorderSpecialCharacterStart
End Property
Public Property Let CaptionRightBorderSpecialCharacterEnd(xNew As String)
    m_xCaptionRightBorderSpecialCharacterEnd = xNew
End Property
Public Property Get CaptionRightBorderSpecialCharacterEnd() As String
    CaptionRightBorderSpecialCharacterEnd = m_xCaptionRightBorderSpecialCharacterEnd
End Property
Public Property Let CaptionRightBorderSpecialCharacterHorizontal(xNew As String)
    m_xCaptionRightBorderSpecialCharacterHorizontal = xNew
End Property
Public Property Get CaptionRightBorderSpecialCharacterHorizontal() As String
    CaptionRightBorderSpecialCharacterHorizontal = m_xCaptionRightBorderSpecialCharacterHorizontal
End Property
Public Property Let CaptionRightBorderColumn(iNew As Integer)
    m_iCaptionRightBorderColumn = iNew
End Property
Public Property Get CaptionRightBorderColumn() As Integer
    CaptionRightBorderColumn = m_iCaptionRightBorderColumn
End Property

