VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentStampDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CDocumentStampDefs Collection Class
'   created 2/2/2000 by Jeffrey Sweetland

'   Contains properties and methods that manage the
'   collection of CDocumentStampDef

'   Member of
'   Container for CDocumentStampDef
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_oDef As mpDB.CDocumentStampDef
Private m_xarListSource As XArray
'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property
Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function
Public Function Active() As mpDB.CDocumentStampDef
'returns the office object of the current record in the recordset
    Set Active = m_oDef
End Function

Public Function Item(lIndex As Long) As mpDB.CDocumentStampDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    On Error GoTo ProcError
    
'   set criteria
    xCriteria = "fldID = " & lIndex
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_oDef
        End If
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CDocumentStampDefs.Item"
    Exit Function
End Function

Public Sub Refresh(ByVal iStampType As Integer, ByVal xTemplate As String, Optional ByVal lLetterheadID As Long, Optional bDoXarray As Boolean = True)
    Dim xSQL As String

    On Error GoTo ProcError

'*******************************************************************
'multitype:
    If xTemplate <> Empty Then
'       attempt to get document stamps for template - see if any are defined
    Dim iNumStamps As Integer
        
'       count number of document stamps of specified type assigned to template-
'       if there are assignments, we use those, else we get assignments from base template
        xSQL = "SELECT COUNT(tblDocumentStampAssignments.fldTemplateID) " & _
            "FROM (tblDocStampCategories INNER JOIN tblDocumentStamps ON " & _
            "tblDocStampCategories.fldID = tblDocumentStamps.fldStampCategory) " & _
            "INNER JOIN tblDocumentStampAssignments ON tblDocumentStamps.fldID = " & _
            "tblDocumentStampAssignments.fldStampID " & _
            "WHERE tblDocumentStampAssignments.fldTemplateID='" & xTemplate & "'"
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenForwardOnly)
        iNumStamps = m_oRS.Fields(0)
        m_oRS.Close
    
        If iNumStamps = 0 Then
'           stamps don't exist, get them from base template
            xTemplate = Application.TemplateDefinitions(xTemplate).BaseTemplate
        End If
    End If
'*******************************************************************

'   get stamps-
    If (lLetterheadID <> Empty) And (xTemplate <> Empty) Then
'       attempt to get stamps specific to the particular letterhead and template
        xSQL = "SELECT tblDocumentStamps.* " & _
               "FROM (tblDocStampCategories INNER JOIN tblDocumentStamps ON " & _
               "tblDocStampCategories.fldID = tblDocumentStamps.fldStampCategory) " & _
               "INNER JOIN tblDocumentStampAssignments ON tblDocumentStamps.fldID = " & _
               "tblDocumentStampAssignments.fldStampID " & _
               "WHERE tblDocumentStampAssignments.fldTemplateID='" & xTemplate & "' AND " & _
               "tblDocumentStampAssignments.fldLetterheadID = " & lLetterheadID

'       filter by category if specified
        If iStampType > 0 Then
            xSQL = xSQL & " and fldStampCategory = " & iStampType
        End If

'       put in alpha order
        xSQL = xSQL & " ORDER BY tblDocumentStamps.fldDescription"

'       open the rs
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)

'       there are specific records for this letterhead/template-
'       populate the recordset and exit
        With m_oRS
            If Not (.EOF And .BOF) Then
                .MoveLast
                .MoveFirst
                If bDoXarray Then
                    RefreshXArray
                End If
                Exit Sub
            End If
        End With
    End If

'   if we get here, there are either no stamps assigned
'   for the supplied letterhead/template type, or we're
'   not interested in getting letterhead specific stamps
    If xTemplate <> Empty Then
'       get stamps assigned to template
        xSQL = "SELECT tblDocumentStamps.* " & _
               "FROM (tblDocStampCategories INNER JOIN tblDocumentStamps ON " & _
               "tblDocStampCategories.fldID = tblDocumentStamps.fldStampCategory) " & _
               "INNER JOIN tblDocumentStampAssignments ON tblDocumentStamps.fldID = " & _
               "tblDocumentStampAssignments.fldStampID " & _
               "WHERE tblDocumentStampAssignments.fldTemplateID='" & xTemplate & "' AND " & _
               "IsNull(tblDocumentStampAssignments.fldLetterheadID)"

'       filter by category if specified
        If iStampType > 0 Then
            xSQL = xSQL & " and fldStampCategory = " & iStampType
        End If

'       put in alpha order
        xSQL = xSQL & " ORDER BY tblDocumentStamps.fldDescription"

'       open the rs
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)

        If (m_oRS.EOF And m_oRS.BOF) Then
'           no stamps have been assigned to the specified template -
'           get stamps that have been assigned to all templates
            xSQL = "SELECT tblDocumentStamps.* " & _
                   "FROM (tblDocStampCategories INNER JOIN tblDocumentStamps ON " & _
                   "tblDocStampCategories.fldID = tblDocumentStamps.fldStampCategory) " & _
                   "INNER JOIN tblDocumentStampAssignments ON tblDocumentStamps.fldID = " & _
                   "tblDocumentStampAssignments.fldStampID " & _
                   "WHERE tblDocumentStampAssignments.fldTemplateID='-All Templates-' "

'       filter by category if specified
        If iStampType > 0 Then
            xSQL = xSQL & " and fldStampCategory = " & iStampType
        End If

'       put in alpha order
        xSQL = xSQL & " ORDER BY tblDocumentStamps.fldDescription"
'           open the rs
            Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
        End If

    Else
'       get stamps regardless of assignments
        xSQL = "SELECT * FROM tblDocumentStamps"

'       filter by category if specified
        If iStampType > 0 Then
            xSQL = xSQL & " WHERE fldStampCategory = " & iStampType
        End If

'       put in alpha order
        xSQL = xSQL & " ORDER BY tblDocumentStamps.fldDescription"

'       open the rs
        Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    End If

'   populate the recordset
    With m_oRS
        If Not (.EOF And .BOF) Then
            .MoveLast
            .MoveFirst
        Else
            Err.Raise mpError_InvalidDocStampCategory
        End If
    End With

'   if no stamps found
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If bDoXarray Then
        RefreshXArray
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.CDocumentStampDefs.Refresh"
    Exit Sub
End Sub


'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oDef
        .ID = Empty
        .Description = Empty
        .BoilerplateFile = Empty
        .Category = Empty
        .InsertFirstPage = Empty
        .InsertPrimary = Empty
        .StampsAreIdentical = Empty
        .DateFormat = Empty
        .TimeFormat = Empty
        .DeleteExisting = Empty
        .AllowVariableText = Empty
        .OptionalControls = Empty
        
        .PrimaryHorizontalOffset = Empty
        .PrimaryHorizontalPosition = Empty
        .Location = Empty
        .PrimaryTextTemplate = Empty
        .PrimaryAltTextTemplate = Empty
        .PrimaryVerticalOffset = Empty
        .PrimaryVerticalPosition = Empty
        .PrimaryVerticalRoot = Empty
        .PrimaryWidth = Empty
        .Height = Empty
        .Page1HorizontalOffset = Empty
        .Page1HorizontalPosition = Empty
        .Page1TextTemplate = Empty
        .Page1AltTextTemplate = Empty
        .Page1VerticalOffset = Empty
        .Page1VerticalPosition = Empty
        .Page1VerticalRoot = Empty
        .Page1SameAsPrimary = Empty
        .Page1Width = Empty
    End With
        
    On Error Resume Next
    With m_oRS
        m_oDef.ID = !fldID
        m_oDef.Description = !fldDescription
        m_oDef.BoilerplateFile = !fldBoilerplateFile
        m_oDef.Category = !fldStampCategory
        m_oDef.Location = !fldLocation
        m_oDef.InsertFirstPage = !fldInsertFirstPage
        m_oDef.InsertPrimary = !fldInsertPrimary
        m_oDef.StampsAreIdentical = !fldIdentical
        m_oDef.DateFormat = !fldDateFormat
        m_oDef.TimeFormat = !fldTimeFormat
        m_oDef.DeleteExisting = !fldDeleteExisting
        m_oDef.AllowVariableText = !fldAllowVariableText
        
'       set values for primary stamp
        m_oDef.PrimaryHorizontalOffset = !fldHOffset2
        m_oDef.PrimaryHorizontalPosition = !fldHPos2
        m_oDef.PrimaryTextTemplate = !fldTextTemplate2
        m_oDef.PrimaryAltTextTemplate = !fldAltTextTemplate2
        m_oDef.PrimaryVerticalOffset = !fldVOffset2
        m_oDef.PrimaryVerticalPosition = !fldVPos2
        m_oDef.PrimaryVerticalRoot = !fldVRoot2
        m_oDef.PrimaryWidth = !fldWidth2
        m_oDef.Height = !fldHeight
        m_oDef.ForcePrimaryPageAnchorSpace = !fldForceAnchorSpace2
        m_oDef.AdditionalPrimaryPageSpace = !fldAdditionalSpace2
        m_oDef.OptionalControls = !fldOptionalControls
        
        If m_oDef.StampsAreIdentical Then
'           set all values equal to primary stamp
            m_oDef.Page1TextTemplate = !fldTextTemplate2
            m_oDef.Page1AltTextTemplate = !fldAltTextTemplate2
            m_oDef.Page1VerticalOffset = !fldVOffset2
            m_oDef.Page1VerticalPosition = !fldVPos2
            m_oDef.Page1VerticalRoot = !fldVRoot2
            m_oDef.Page1HorizontalOffset = !fldHOffset2
            m_oDef.Page1HorizontalPosition = !fldHPos2
            m_oDef.Page1Width = !fldWidth2
            m_oDef.ForceFirstPageAnchorSpace = !fldForceAnchorSpace2
            m_oDef.AdditionalFirstPageSpace = !fldAdditionalSpace2
        Else
'           set all values equal to page 1 values in record
            m_oDef.Page1TextTemplate = !fldTextTemplate1
            m_oDef.Page1AltTextTemplate = !fldAltTextTemplate1
            m_oDef.Page1VerticalOffset = !fldVOffset1
            m_oDef.Page1VerticalPosition = !fldVPos1
            m_oDef.Page1VerticalRoot = !fldVRoot1
            m_oDef.Page1HorizontalOffset = !fldHOffset1
            m_oDef.Page1HorizontalPosition = !fldHPos1
            m_oDef.Page1Width = !fldWidth1
            m_oDef.ForceFirstPageAnchorSpace = !fldForceAnchorSpace1
            m_oDef.AdditionalFirstPageSpace = !fldAdditionalSpace1
        End If
    End With
End Sub
Private Sub Class_Initialize()
    Set m_oDef = New mpDB.CDocumentStampDef
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub

