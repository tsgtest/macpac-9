VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingSignatureDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingTypes Collection Class
'   created 1/21/00 by Mike Conner-
'   barbies@ix.netcom.net

'   Contains properties and methods that manage the
'   collection of CPleadingType

'   Member of
'   Container for CPleadingType
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
'Private m_oPSignature As mpDB.CPleadingSignatureDef
Private m_opSignature As mpDB.CPleadingSignatureDef
Private m_xarListSource As XArrayObject.XArray

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

Public Property Let ListSource(xarNew As XArray)
    Set m_xarListSource = xarNew
End Property

Public Property Get ListSource() As XArray
    If (m_xarListSource Is Nothing) Then
        RefreshXArray
    End If
    Set ListSource = m_xarListSource
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub Refresh(Optional DoXArray As Boolean = True)
'opens the appropriate recordset as the collection
'source and fills an xarray as a display source
    Dim xSQL As String

    xSQL = "SELECT tblPleadingSignatureTypes.* " & _
           "FROM tblPleadingSignatureTypes" & ";"
   
'   open recordset
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, _
                dbOpenSnapshot, dbReadOnly)
    
'   populate if offices exist
    With m_oRS
        If Not (.BOF And .EOF) Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
'   update list xArray if already filled - else
'   wait until needed - ie wait until Get ListSource
    If DoXArray Then
        RefreshXArray
    End If
End Sub

Public Function Active() As mpDB.CPleadingSignatureDef
'returns the office object of the current record in the recordset
    Set Active = m_opSignature
End Function

Public Function Count() As Long
    Count = m_oRS.RecordCount
End Function


Public Function Item(lID As Long) As mpDB.CPleadingSignatureDef
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
'   set criteria
    xCriteria = "fldID = " & lID
    
    With m_oRS
'       store bookmark for possible future reset
        vBkmk = .Bookmark
        
'       find
        .FindFirst xCriteria
        
        If .NoMatch Then
'           not found
            .Bookmark = vBkmk
            Set Item = Nothing
        Else
'           found - update current object
            UpdateObject
            
'           return current object
            Set Item = m_opSignature
        End If
    End With
    
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingSignatureDefs.Item", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_opSignature = New mpDB.CPleadingSignatureDef
    Set m_xarListSource = New XArrayObject.XArray
    m_xarListSource.ReDim 0, -1, 0, 1
End Sub

Private Sub Class_Terminate()
    Set m_xarListSource = Nothing
    Set m_opSignature = Nothing
    Set m_oRS = Nothing
End Sub

Private Sub RefreshXArray()
'fills array xArray with records
'in recordset rExisting

    Dim lNumRows As Long
    Dim i As Integer
    Dim xSize As String
    Dim xID As String
    Dim xDescription As String
    
    With m_oRS
        lNumRows = .RecordCount
    
        If lNumRows > 0 Then
            .MoveLast
            .MoveFirst
            lNumRows = .RecordCount
            m_xarListSource.ReDim 0, lNumRows - 1, 0, 1
            For i = 0 To lNumRows - 1
                .AbsolutePosition = i
                On Error Resume Next
                xID = !fldID
                xDescription = !fldDescription
                m_xarListSource.Value(i, 0) = xID
                m_xarListSource.Value(i, 1) = xDescription
            
            Next i
        Else
            m_xarListSource.ReDim 0, -1, 0, 2
        End If
    End With
End Sub


'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_opSignature
        .BoilerplateFile = Empty
        .ClosingParagraph = Empty
        .ClosingParagraphBookmark = Empty
        .Description = Empty
        .ID = Empty
        .SignatureBlockBookmarkSecondary = Empty
        .DisplayName = Empty
        .DisplayJudgeName = Empty
        .DisplaySignerName = Empty
        .FullRowFormat = Empty
        .ShowFirmID = Empty
        .ShowFirmAddress = Empty
        .IncludeBarID = Empty
        .BarIDPrefix = Empty
        .BarIDSuffix = Empty
        .AllowFirmSlogan = Empty
        .AllowAddressTab = Empty
        .AllowFirmCity = Empty
        .AllowFirmFax = Empty
        .AllowFirmPhone = Empty
        .AllowFirmState = Empty
        .AllowSignerEMail = Empty
        .AllowOneOfSignerText = Empty
        .RowsPerSignature = Empty
        .DefaultPosition = Empty
        .AllowClosingParagraph = Empty
        .ClosingParagraphBoilerplateFile = Empty
        .DatedPrefix = Empty
        .DateFormat = Empty
        .OfficeAddressFormat = Empty
        .SignerNameCase = Empty
        .IncludeSignerEmail = Empty
        .SignerEmailPrefix = Empty
        .SignerEmailSuffix = Empty
        .AllowSignerEmailInput = Empty
        .FormatFirmName = Empty
        .NonTableFormat = Empty
        
        On Error Resume Next
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .ClosingParagraph = m_oRS!fldClosingParagraph
        .ClosingParagraphBookmark = m_oRS!fldClosingParagraphBookmark
        .Description = m_oRS!fldDescription
        .ID = m_oRS!fldID
        .SignatureBlockBookmarkSecondary = m_oRS!fldSignatureBlockBookmarkSecondary
        .DisplayName = m_oRS!fldDisplayName
        .DisplayJudgeName = m_oRS!fldDisplayJudgeName
        .DisplaySignerName = m_oRS!fldDisplaySignerName
        .FullRowFormat = m_oRS!fldFullRowFormat
        .ShowFirmID = m_oRS!fldDisplayFirmID
        .AllowFirmSlogan = m_oRS!fldAllowFirmSlogan
        .ShowFirmAddress = m_oRS!fldDisplayFirmAddress
        .IncludeBarID = m_oRS!fldIncludeBarID
        .BarIDPrefix = m_oRS!fldBarIDPrefix
        .BarIDSuffix = m_oRS!fldBarIDSuffix
        .AllowAddressTab = m_oRS!fldAllowAddressTab
        .AllowFirmCity = m_oRS!fldAllowFirmCity
        .AllowFirmFax = m_oRS!fldAllowFirmFax
        .AllowFirmPhone = m_oRS!fldAllowFirmPhone
        .AllowFirmState = m_oRS!fldAllowFirmState
        .AllowSignerEMail = m_oRS!fldAllowSignerEMail
        .AllowOneOfSignerText = m_oRS!fldAllowOneOfSignerText
        .RowsPerSignature = m_oRS!fldRowsPerSignature
        .DefaultPosition = m_oRS!fldDefaultPosition
        .AllowClosingParagraph = m_oRS!fldAllowClosingParagraph
        .ClosingParagraphBoilerplateFile = m_oRS!fldClosingParagraphBoilerplateFile
        .DatedPrefix = m_oRS!fldDatedPrefix
        .DateFormat = m_oRS!fldDateFormat
        .OfficeAddressFormat = m_oRS!fldOfficeAddressFormat
        .SignerNameCase = m_oRS!fldSignerNameCase

        .IncludeSignerEmail = m_oRS!fldIncludeSignerEmail
        .SignerEmailPrefix = m_oRS!fldSignerEmailPrefix
        .SignerEmailSuffix = m_oRS!fldSignerEmailSuffix
        .AllowSignerEmailInput = m_oRS!fldAllowSignerEMailInput
        .FormatFirmName = m_oRS!fldFormatFirmName
        .NonTableFormat = m_oRS!fldNonTableFormat
    
    End With
End Sub

