VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CNotaryDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CNotaryDefs Collection Class
'   created 6/11/00 by Daniel Fisherman-

'   Contains properties/methods that manage the
'   collection of Notary Document definitions -
'   collection is a recordset, though
'   an XArray belongs to class for display purposes

'   Member of CApp
'   Container for CNotaryDef
'**********************************************************


Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CNotaryDef
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CNotaryDef
Attribute Item.VB_UserMemId = 0
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where index = lID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
'       get item where name = vID
        xCrit = "fldID = " & lID
            
'       attempt to find the item
        m_oRS.FindFirst xCrit
        If m_oRS.NoMatch Then
'           if no match found, the
'           member does not exist
            Err.Raise mpError_InvalidMember
        Else
'           item found - update template object
            UpdateObject
        End If
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    RaiseError "mpDB.CNotaryDefs.Item"
End Function

Public Function Refresh(Optional ByVal lLevel0 As Long)
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get templates
    xSQL = "SELECT * FROM tblNotaryTypes "
    If lLevel0 Then
        xSQL = xSQL & "WHERE fldLevel0 = " & lLevel0
    End If
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CNotaryDefs.Refresh"
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oType
        .BoilerplateFile = Empty
        .Description = Empty
        .ID = Empty
        .Level0 = Empty
        .DialogCaption = Empty
        .AllowGender = Empty
        .AllowAppearingPerson = Empty
        .AllowCounty = Empty
        .AllowNotaryName = Empty
        .AllowPersonalProved = Empty
        .AllowState = Empty
        .AllowCommission = Empty
        .DateFormats = Empty
        .DefaultAppearanceDateFormat = Empty
        .DefaultExecuteDateFormat = Empty
        .Custom1Label = Empty
        .Custom1DefaultText = Empty
        .Custom2Label = Empty
        .Custom2DefaultText = Empty
        .TopMargin = Empty
        .BottomMargin = Empty
        .LeftMargin = Empty
        .RightMargin = Empty
        .AllowAppearanceDate = Empty
        .AllowExecuteDate = Empty
        .AllowCountyHeading = Empty
        .KnownByList = Empty
        .AllowDocumentDate = Empty
        .AllowDocumentTitle = Empty
        .AllowNumberOfPages = Empty
        .AllowOtherSigners = Empty
        .AllowCompanyName = Empty
        .AllowCompanyState = Empty
        .AllowSignerTitle = Empty
'CharlieCore 9.2.0 - BaseStyle
        .BaseStyle = Empty
        
        On Error Resume Next
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .Description = m_oRS!fldDescription
        .DialogCaption = m_oRS!fldDialogCaption
        .ID = m_oRS!fldID
        .Level0 = m_oRS!fldLevel0
        .AllowGender = m_oRS!fldAllowGender
        .AllowAppearingPerson = m_oRS!fldAllowAppearingPerson
        .AllowCounty = m_oRS!fldAllowCounty
        .AllowCommission = m_oRS!fldAllowCommission
        .AllowNotaryName = m_oRS!fldAllowNotaryName
        .AllowPersonalProved = m_oRS!fldAllowPersonalProved
        .AllowState = m_oRS!fldAllowState
        .AllowCountyHeading = m_oRS!fldAllowCountyHeading
        .DefaultAppearanceDateFormat = m_oRS!fldDefaultAppearanceDateFormat
        .DefaultExecuteDateFormat = m_oRS!fldDefaultExecuteDateFormat
        .Custom1Label = m_oRS!fldCustom1Label
        .Custom1DefaultText = m_oRS!fldCustom1DefaultText
        .Custom2Label = m_oRS!fldCustom2Label
        .Custom2DefaultText = m_oRS!fldCustom2DefaultText
        .DateFormats = m_oRS!fldDateFormats
        .TopMargin = m_oRS!fldTMargin
        .BottomMargin = m_oRS!fldBMargin
        .LeftMargin = m_oRS!fldLMargin
        .RightMargin = m_oRS!fldRMargin
        .AllowAppearanceDate = m_oRS!fldAllowAppearanceDate
        .AllowExecuteDate = m_oRS!fldAllowExecuteDate
        .KnownByList = m_oRS!fldKnownByList
        .AllowDocumentDate = m_oRS!fldAllowDocumentDate
        .AllowDocumentTitle = m_oRS!fldAllowDocumentTitle
        .AllowNumberOfPages = m_oRS!fldAllowNumPages
        .AllowOtherSigners = m_oRS!fldAllowOtherSigners
        .AllowCompanyName = m_oRS!fldAllowCompanyName
        .AllowCompanyState = m_oRS!fldAllowCompanyState
        .AllowSignerTitle = m_oRS!fldAllowSignerTitle
'CharlieCore 9.2.0 - BaseStyle
        .BaseStyle = m_oRS!fldBaseStyle
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oType = New mpDB.CNotaryDef
End Sub
