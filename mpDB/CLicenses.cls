VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLicenses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CLicenses Class
'   created 2/23/00 by Dan Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   manage the collection of Attorney License

'   Member of CPerson
'**********************************************************

'**********************************************************
Private Enum mpLicCols
    mpLicCols_ID = 0
    mpLicCols_Desc = 1
    mpLicCols_Number = 2
    mpLicCols_Default = 3
End Enum

Private m_oArray As XArray
Private m_lPersonID As Long

Friend Property Let PersonID(lNew As Long)
    m_lPersonID = lNew
End Property

Friend Property Get PersonID() As Long
    PersonID = m_lPersonID
End Property

Public Property Get Default() As CLicense
'returns the default CLicense object of
'the licenses collection
    Dim i As Integer
'   cycle through all array elements
    For i = 0 To m_oArray.Count(1) - 1
'       return the first array element whose
'       'Default' column is TRUE
        If m_oArray(i, mpLicCols_Default) Then
            Set Default = Item(, i + 1)
            Exit For
        End If
    Next i
    Exit Property
ProcError:
    RaiseError "mpDB.CLicenses.Default"
    Exit Property
End Property

Public Property Get ListSource() As XArray
'returns the collection as an XArray-
'used to present the list in tdbl
    Set ListSource = m_oArray
End Property

Public Property Let ListSource(oNew As XArray)
    Set m_oArray = oNew
End Property

Public Function Add(ByVal xDescription As String, _
        ByVal xNumber As String, ByVal bDefault As String) As CLicense
    Dim iNewIndex As Integer
    
    With m_oArray
        iNewIndex = .Count(1)
        .Insert 1, iNewIndex
        .Value(iNewIndex, mpLicCols_Desc) = xDescription
        .Value(iNewIndex, mpLicCols_Number) = xNumber
        .Value(iNewIndex, mpLicCols_Default) = bDefault
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CLicenses.Add"
    Exit Function
End Function

Public Sub Delete(ByVal lID As Long)
    Dim i As Integer
    On Error GoTo ProcError
'   cycle through all array elements
    For i = 0 To m_oArray.Count(1) - 1
'       delete the first array element
'       whose 'ID' column = lID
        If m_oArray(i, mpLicCols_ID) = lID Then
            m_oArray.Delete 1, i
            Exit For
        End If
    Next i
    Exit Sub
ProcError:
    RaiseError "mpDB.CLicenses.Delete"
    Exit Sub
End Sub

Public Sub DeleteAll()
    On Error GoTo ProcError
    With m_oArray
        .ReDim .LowerBound(1), -1, .LowerBound(2), .UpperBound(2)
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CLicenses.DeleteAll"
    Exit Sub
End Sub

Friend Sub Save()
'saves current array items as db records-
'deletes existing records beforehand
    Dim xSQL As String
    Dim xDesc As String
    Dim xNum As String
    Dim bDefault As Boolean
    Dim i As Integer
    
    On Error GoTo ProcError
    xSQL = "DELETE * FROM tblAttyLicenses " & _
           "WHERE fldPerson = " & m_lPersonID
    
'   delete all current licenses from DB
    If m_lPersonID >= g_lManualEntryIDThreshold Then
'       person is manual entry - do private db
        Application.PrivateDB.Execute xSQL
    Else
'       person is public entry - do public db
        Application.PublicDB.Execute xSQL
    End If
    
'   add a new license for each item in array
    With m_oArray
        For i = 0 To .Count(1) - 1
'           get values to insert in new db record
            xDesc = .Value(i, mpLicCols_Desc)
            xNum = .Value(i, mpLicCols_Number)
            bDefault = .Value(i, mpLicCols_Default)
            
'           create insert sql
            xSQL = "INSERT INTO tblAttyLicenses " & _
                   "(fldPerson, fldDescription, fldLicense, fldDefault) " & _
                   "VALUES (""" & m_lPersonID & """,""" & xDesc & """,""" & _
                   xNum & """," & bDefault & ")"
    
'           delete all current licenses from DB
            If m_lPersonID >= g_lManualEntryIDThreshold Then
'               person is manual entry - do private db
                Application.PrivateDB.Execute xSQL
            Else
'               person is public entry - do public db
                Application.PublicDB.Execute xSQL
            End If
        Next i
    End With
    Refresh m_lPersonID
    Exit Sub
ProcError:
    RaiseError "mpDB.CLicenses.Save"
    Exit Sub
End Sub

Public Function Count() As Integer
    On Error Resume Next
    Count = m_oArray.Count(1)
End Function

Public Function Item(Optional ByVal lID As Long, _
                     Optional ByVal iIndex As Integer) As CLicense
Attribute Item.VB_UserMemId = 0
'returns the CLicense object with the specified ID,
'or the object that occupies index iIndex in the collection
    Dim lNum As Long
    Dim xCrit As String
    Dim vBkmk As Variant
    Dim i As Integer
    Dim oLic As CLicense
    
    lNum = Me.Count
    If lID Then
'       find record with specified ID
        For i = 0 To m_oArray.Count(1) - 1
'           delete the first array element
'           whose 'ID' column = lID
            If m_oArray(i, mpLicCols_ID) = lID Then
                Set oLic = New CLicense
                oLic.Default = m_oArray(i, mpLicCols_Default)
                oLic.Description = m_oArray(i, mpLicCols_Desc)
                oLic.ID = m_oArray(i, mpLicCols_ID)
                oLic.Number = m_oArray(i, mpLicCols_Number)
                Exit For
            End If
        Next i
    Else
'       get record with specified index
        On Error Resume Next
        Set oLic = New CLicense
        oLic.Default = m_oArray(iIndex - 1, mpLicCols_Default)
        If Err.Number Then
            On Error GoTo ProcError
            Err.Raise mpError_InvalidItemIndex
        Else
            On Error GoTo ProcError
        End If
        oLic.Description = m_oArray(iIndex - 1, mpLicCols_Desc)
        oLic.Number = m_oArray(iIndex - 1, mpLicCols_Number)
        Dim xID As String
        xID = m_oArray(iIndex - 1, mpLicCols_ID)
        If Len(xID) Then
            oLic.ID = m_oArray(iIndex - 1, mpLicCols_ID)
        End If
    End If
    Set Item = oLic
    
    Exit Function
ProcError:
    RaiseError "mpDB.CLicenses.Item"
    Exit Function
End Function

Public Sub Refresh(ByVal lPersonID As Long)
'fills m_oArray with the license records
'for person with id lPersonID
    Dim xSQL As String
    Dim lNum As Long
    Dim lID  As Long
    Dim xDesc As String
    Dim xLicense As String
    Dim bDefault As String
    Dim l As Long
    Dim oRS As DAO.Recordset
    
    m_lPersonID = lPersonID
    
    On Error GoTo ProcError

'   clear previous array
    Set m_oArray = New XArray
    m_oArray.ReDim 0, -1, 0, 3
    
'   build sql line
    xSQL = "SELECT * " & _
           "FROM tblAttyLicenses " & _
           "WHERE fldPerson = " & lPersonID
           
'   get recordset
    If lPersonID >= g_lManualEntryIDThreshold Then
'       person is manual entry - get from private db
        Set oRS = Application.PrivateDB.OpenRecordset( _
                    xSQL, dbOpenDynaset)
    Else
'       person is public entry - get from public db
        Set oRS = Application.PublicDB.OpenRecordset( _
                    xSQL, dbOpenDynaset)
    End If
    
'   fill array
    With oRS
        If Not (.BOF And .EOF) Then
'           populate recordset
            .MoveLast
            .MoveFirst
            
'           fill xarray
            RefreshListSource oRS
        End If
    End With
    oRS.Close
    Set oRS = Nothing
    Exit Sub
    
ProcError:
    RaiseError "mpDB.CLicenses.Refresh"
    Exit Sub
End Sub

Private Function RefreshListSource(oRS As DAO.Recordset)
'fills the records into an XArray
    Dim l As Long
    Dim lID As Long
    Dim xDesc As String
    Dim xLicense As String
    Dim bDefault As Boolean
    
    On Error GoTo ProcError
    
    With oRS
        .MoveFirst
        
'       count and create storage space
        Set m_oArray = New XArray
        m_oArray.ReDim 0, .RecordCount - 1, 0, 3
        
        While Not .EOF
'           clear previous values
            lID = Empty
            xDesc = Empty
            xLicense = Empty
            bDefault = Empty
            
'           store new values in buffer
            On Error Resume Next
            lID = !fldID
            xDesc = !fldDescription
            xLicense = !fldLicense
            bDefault = !fldDefault
            On Error GoTo ProcError
        
'           add to array
            m_oArray.Value(l, mpLicCols_ID) = lID
            m_oArray.Value(l, mpLicCols_Desc) = xDesc
            m_oArray.Value(l, mpLicCols_Number) = xLicense
            m_oArray.Value(l, mpLicCols_Default) = bDefault
            
'           move to next record
            l = l + 1
            .MoveNext
        Wend
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.CLicenses.RefreshListSource"
    Exit Function
End Function

Private Sub Class_Initialize()
    Set m_oArray = New XArray
    m_oArray.ReDim 0, -1, 0, 3
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
End Sub
