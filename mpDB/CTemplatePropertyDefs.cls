VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CTemplatePropertyDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CTemplateCustomPropertyDefs Collection Class
'   created 12/10/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CTemplateCustomPropertyDefs

'   Member of
'   Container for CTemplateCustomPropertyDef
'**********************************************************

'**********************************************************
Private m_oRS As DAO.Recordset
Private m_oProperty As CTemplatePropertyDef
Private m_xCategory As String
'**********************************************************


'**********************************************************
'   Properties
'**********************************************************
Public Property Let Category(xNew As String)
    m_xCategory = xNew
    
'   get recordset containing custom
'   properties for supplied template
    Refresh xNew
End Property

Public Property Get Category() As String
    Category = m_xCategory
End Property

'**********************************************************
'   Methods
'**********************************************************
Private Sub Refresh(ByVal xCategory As String)
'get recordset of property defs from db
    Dim xSQL As String
    xSQL = "SELECT * FROM tblCustomProperties " & _
           "WHERE fldCategory = '" & xCategory & "'"
           
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    If Not (m_oRS.BOF And m_oRS.EOF) Then
        m_oRS.MoveLast
        m_oRS.MoveFirst
    End If
End Sub

Public Property Get Count() As Integer
    If Not (m_oRS Is Nothing) Then
        Count = m_oRS.RecordCount
    End If
End Property

Public Function Item(vID As Variant) As CTemplatePropertyDef
Attribute Item.VB_UserMemId = 0
'finds the record with ID = vID or Name = vID -
'returns the found record as a CTemplatePropertyDef-
'raises error if no match - returns "Nothing" if
'no recordset exists - this will be the case if
'no template has been specified (ie me.template = empty)
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    If m_oRS Is Nothing Then
        Exit Function
    End If
    
    With m_oRS
'       set criteria
        If IsNumeric(vID) Then
            On Error Resume Next
'MsgBox "Bad - fix this - absolute position does not work"
            .MoveFirst
            .AbsolutePosition = vID - 1
            If Err.Number Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            On Error GoTo ProcError
        Else
            xCriteria = "fldName = '" & vID & "'"
    
'           find
            .FindFirst xCriteria
            
            If .NoMatch Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            
        End If
        
'       requested item exists - update current object
        UpdateObject
                
'       return current object
        Set Item = m_oProperty
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CTemplatePropertyDefs.Item"
    Exit Function
End Function

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oProperty = New mpDB.CTemplatePropertyDef
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
    Set m_oProperty = Nothing
End Sub

Private Sub UpdateObject()
    With m_oProperty
        .Name = Empty
        .Bookmark = Empty
        .DelimiterReplacement = Empty
        .ID = Empty
        .Name = Empty
        .LinkedProperty = Empty
        .WholeParagraph = Empty
        .PropertyType = Empty
        .Macro = Empty
        .Indexed = Empty
        
        On Error Resume Next
        .Name = m_oRS!fldName
        .Bookmark = m_oRS!fldBookmark
        .DelimiterReplacement = m_oRS!fldDelimiterReplacement
        .ID = m_oRS!fldID
        .LinkedProperty = m_oRS!fldLinkedProperty
        .WholeParagraph = m_oRS!fldWholeParagraph
        .PropertyType = m_oRS!fldType
        .Macro = m_oRS!fldMacro
        .Indexed = m_oRS!fldIndexed
    End With
End Sub
