VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingFavorites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingFavorites Collection Class
'   created 1/11/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CPleadingFavorite

'   Member of
'   Container for CPleadingFavorite
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_oFave As mpDB.CPleadingFavorite
Private m_oArray As XArray
'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Add(ByVal xDescription, _
                    ByVal lLevel0 As Long, _
                    ByVal lLevel1 As Long, _
                    ByVal lLevel2 As Long, _
                    ByVal lLevel3 As Long) As mpDB.CPleadingFavorite
'adds a pleading favorites
    
    On Error GoTo ProcError
    
'   create/fill a favorite object
    Set m_oFave = New CPleadingFavorite
    With m_oFave
        .Description = xDescription
        .Level0 = lLevel0
        .Level1 = lLevel1
        .Level2 = lLevel2
        .Level3 = lLevel3
    End With
    
'   create a record in the db
    m_oRS.AddNew
    
'   save to db
    UpdateRecordset
    
    Set Add = m_oFave
    Exit Function
ProcError:
    RaiseError "MPDB.CPleadingFavorites.Add"
End Function
                    
Public Sub Delete(ByVal lID As Long)
'deletes a pleading favorite
    Dim oItem As mpDB.CPleadingFavorite
    On Error GoTo ProcError
    Set oItem = Item(lID)
    If Not (oItem Is Nothing) Then
        m_oRS.Delete
    End If
    Exit Sub
ProcError:
    RaiseError "MPDB.CPleadingFavorites.Delete"
End Sub
                    
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CPleadingFavorite
Attribute Item.VB_UserMemId = 0
    Dim xCrit As String
    Dim vBookmark As Variant
    
    On Error GoTo ProcError
    xCrit = "fldID = " & lID
    
    With m_oRS
        .FindFirst xCrit
        If Not .NoMatch Then
            UpdateObject
        End If
    End With
    Set Item = m_oFave
    Exit Function
ProcError:
    RaiseError "MPDB.CPleadingFavorites.Item"
End Function

Public Function Refresh()
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get templates
    xSQL = "SELECT * FROM tblFavoritePleadings ORDER BY fldDescription"
           
'   open the rs
    Set m_oRS = Application.PrivateDB.OpenRecordset(xSQL, dbOpenDynaset)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    
    RefreshList
    
    Exit Function
ProcError:
    RaiseError "MPDB.CPleadingFavorites.Refresh"
    Exit Function
End Function

Public Function List() As XArray
    Set List = m_oArray
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub RefreshList()
    Dim i As Integer
    Dim xDesc As String
    Dim lID As Long
    
    Set m_oArray = New XArray
    
    With m_oRS
        If Not (.BOF And .EOF) Then
            m_oArray.ReDim 0, .RecordCount - 1, 0, 1
            .MoveFirst
            While Not .EOF
                xDesc = Empty
                lID = Empty
                
                On Error Resume Next
                xDesc = !fldDescription
                lID = !fldID
                On Error GoTo 0
                
                m_oArray.Value(i, 0) = xDesc
                m_oArray.Value(i, 1) = lID
                .MoveNext
                i = i + 1
            Wend
        Else
            m_oArray.ReDim 0, -1, 0, 1
        End If
    End With
End Sub

Private Sub UpdateObject()
    With m_oFave
        .ID = Empty
        .Level0 = Empty
        .Level1 = Empty
        .Level2 = Empty
        .Level3 = Empty
        .Description = Empty
        
        On Error Resume Next
        .ID = m_oRS!fldID
        .Level0 = m_oRS!fldLevel0
        .Level1 = m_oRS!fldLevel1
        .Level2 = m_oRS!fldLevel2
        .Level3 = m_oRS!fldLevel3
        .Description = m_oRS!fldDescription
    End With
End Sub

Private Sub UpdateRecordset()
    With m_oFave
        On Error Resume Next
        m_oRS!fldLevel0 = .Level0
        m_oRS!fldLevel1 = .Level1
        m_oRS!fldLevel2 = .Level2
        m_oRS!fldLevel3 = .Level3
        m_oRS!fldDescription = .Description
    End With
    On Error GoTo ProcError
    m_oRS.Update
    Exit Sub
ProcError:
    RaiseError "MPDB.CPleadingFavorites.UpdateRecordset", _
              "Could not update favorite. " & vbCr & _
              Application.Error.Desc(Err.Number)
End Sub

Private Sub Class_Initialize()
    Set m_oFave = New mpDB.CPleadingFavorite
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
    Set m_oFave = Nothing
End Sub
