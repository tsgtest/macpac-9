VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COffice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Office Class
'   created 12/23/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define an office

'   Member of COffices, CPerson
'**********************************************************

'**********************************************************
Public Enum mpOfficeAddressFormats
    mpOfficeAddressFormat_Default = 1
    mpOfficeAddressFormat_CountryIfForeign = 2
End Enum

Private m_lID As Long
Private m_xName As String
Private m_xFirmName As String
Private m_xFirmNameUpperCase As String
Private m_xSlogan As String
Private m_xDisplayName As String
Private m_xAddress1 As String
Private m_xAddress2 As String
Private m_xAddress3 As String
Private m_xPreCity As String
Private m_xCity As String
Private m_xPostCity As String
Private m_xState As String
Private m_xProvince As String
Private m_xCountry As String
Private m_xStateAbbr As String
Private m_xZip As String
Private m_xPhone1 As String
Private m_xPhone2 As String
Private m_xPhone3 As String
Private m_xFax1 As String
Private m_xFax2 As String
Private m_xCountyID As String
Private m_xDistrictID As String
Private m_xDefaultPleading As String
Private m_xTemplate As String
Private m_xPhoneCaption As String
Private m_xFaxCaption As String
Private m_xFirmURL As String
Private m_xFirmID As String
Private m_xCustom1 As String
Private m_xCustom2 As String
Private m_xCustom3 As String
Private m_xCustom4 As String
Private m_xCustom5 As String
Private m_Options As mpDB.CPersonOptions
Private m_xARFNFormat As XArray
Private m_xFirmNameFormatted As String


'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Slogan(xNew As String)
    m_xSlogan = xNew
End Property

Public Property Get Slogan() As String
    Slogan = m_xSlogan
End Property

Public Property Let FirmName(xNew As String)
    m_xFirmName = xNew
End Property

Public Property Get FirmName() As String
    FirmName = m_xFirmName
End Property

Public Property Let FirmNameUpperCase(xNew As String)
    m_xFirmNameUpperCase = xNew
End Property

Public Property Get FirmNameUpperCase() As String
    FirmNameUpperCase = m_xFirmNameUpperCase
End Property
'---9.7.1 - 4035
Public Property Let FirmNameFormatted(xNew As String)
    m_xFirmNameFormatted = xNew
End Property

Public Property Get FirmNameFormatted() As String
    FirmNameFormatted = m_xFirmNameFormatted
End Property

Public Property Let DisplayName(xNew As String)
    m_xDisplayName = xNew
End Property

Public Property Get DisplayName() As String
    DisplayName = m_xDisplayName
End Property

Public Property Let Address1(xNew As String)
    m_xAddress1 = xNew
End Property

Public Property Get Address1() As String
    Address1 = m_xAddress1
End Property

Public Property Let Address2(xNew As String)
    m_xAddress2 = xNew
End Property

Public Property Get Address2() As String
    Address2 = m_xAddress2
End Property

Public Property Let Address3(xNew As String)
    m_xAddress3 = xNew
End Property

Public Property Get Address3() As String
    Address3 = m_xAddress3
End Property

Public Property Let PreCity(xNew As String)
    m_xPreCity = xNew
End Property

Public Property Get PreCity() As String
    PreCity = m_xPreCity
End Property

Public Property Let City(xNew As String)
    m_xCity = xNew
End Property

Public Property Get City() As String
    City = m_xCity
End Property

Public Property Let PostCity(xNew As String)
    m_xPostCity = xNew
End Property

Public Property Get PostCity() As String
    PostCity = m_xPostCity
End Property

Public Property Let State(xNew As String)
    m_xState = xNew
End Property

Public Property Get State() As String
    State = m_xState
End Property

Public Property Let StateAbbr(xNew As String)
    m_xStateAbbr = xNew
End Property

Public Property Get StateAbbr() As String
    StateAbbr = m_xStateAbbr
End Property

Public Property Let Province(xNew As String)
    m_xProvince = xNew
End Property

Public Property Get Province() As String
    Province = m_xProvince
End Property

Public Property Let Country(xNew As String)
    m_xCountry = xNew
End Property

Public Property Get Country() As String
    Country = m_xCountry
End Property

Public Property Let Zip(xNew As String)
    m_xZip = xNew
End Property

Public Property Get Zip() As String
    Zip = m_xZip
End Property

Public Property Let Phone1(xNew As String)
    m_xPhone1 = xNew
End Property

Public Property Get Phone1() As String
    Phone1 = m_xPhone1
End Property

Public Property Let Phone2(xNew As String)
    m_xPhone2 = xNew
End Property

Public Property Get Phone2() As String
    Phone2 = m_xPhone2
End Property

Public Property Let Phone3(xNew As String)
    m_xPhone3 = xNew
End Property

Public Property Get Phone3() As String
    Phone3 = m_xPhone3
End Property

Public Property Let Fax1(xNew As String)
    m_xFax1 = xNew
End Property

Public Property Get Fax1() As String
    Fax1 = m_xFax1
End Property

Public Property Let Fax2(xNew As String)
    m_xFax2 = xNew
End Property

Public Property Get Fax2() As String
    Fax2 = m_xFax2
End Property

Public Property Let CountyID(xNew As String)
    m_xCountyID = xNew
End Property

Public Property Get CountyID() As String
    CountyID = m_xCountyID
End Property

Public Property Let DistrictID(xNew As String)
    m_xDistrictID = xNew
End Property

Public Property Get DistrictID() As String
    DistrictID = m_xDistrictID
End Property

Public Property Let DefaultPleading(xNew As String)
    m_xDefaultPleading = xNew
End Property

Public Property Get DefaultPleading() As String
    DefaultPleading = m_xDefaultPleading
End Property

Public Property Let PhoneCaption(xNew As String)
    m_xPhoneCaption = xNew
End Property

Public Property Get PhoneCaption() As String
    PhoneCaption = m_xPhoneCaption
End Property

Public Property Let FaxCaption(xNew As String)
    m_xFaxCaption = xNew
End Property

Public Property Get FaxCaption() As String
    FaxCaption = m_xFaxCaption
End Property

Public Property Get FirmURL() As String
    FirmURL = m_xFirmURL
End Property

Public Property Let FirmURL(xNew As String)
    m_xFirmURL = xNew
End Property

Public Property Get FirmID() As String
    FirmID = m_xFirmID
End Property

Public Property Let FirmID(xNew As String)
    m_xFirmID = xNew
End Property

Public Property Let Template(xNew As String)
    m_xTemplate = xNew
End Property

Public Property Get Template() As String
    Template = m_xTemplate
End Property

Public Property Let Custom1(xNew As String)
    m_xCustom1 = xNew
End Property

Public Property Get Custom1() As String
    Custom1 = m_xCustom1
End Property

Public Property Let Custom2(xNew As String)
    m_xCustom2 = xNew
End Property

Public Property Get Custom2() As String
    Custom2 = m_xCustom2
End Property

Public Property Let Custom3(xNew As String)
    m_xCustom3 = xNew
End Property

Public Property Get Custom3() As String
    Custom3 = m_xCustom3
End Property

Public Property Let Custom4(xNew As String)
    m_xCustom4 = xNew
End Property

Public Property Get Custom4() As String
    Custom4 = m_xCustom4
End Property

Public Property Let Custom5(xNew As String)
    m_xCustom5 = xNew
End Property

Public Property Get Custom5() As String
    Custom5 = m_xCustom5
End Property

Public Property Get FirmNameFormats() As XArray
    '---9.7.1 - 4035
    '---parse formatted firm name string, construct XArray col 1 = attribute, col 2 = string
    
    Set m_xARFNFormat = New XArray
    m_xARFNFormat.ReDim 0, 11, 0, 1
    
    With m_xARFNFormat
        .Value(0, 0) = "<B>"
        .Value(0, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<B>", "</B>")
        
        .Value(1, 0) = "<U>"
        .Value(1, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<U>", "</U>")
        
        .Value(2, 0) = "<I>"
        .Value(2, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<I>", "</I>")
        
        .Value(3, 0) = "<C>"
        .Value(3, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<C>", "</C>")
        
        .Value(4, 0) = "<SC>"
        .Value(4, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<SC>", "</SC>")
        
        .Value(5, 0) = "<11>"
        .Value(5, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<11>", "<11>")
        
        .Value(6, 0) = "<9>"
        .Value(6, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<9>", "<9>")
        
        .Value(7, 0) = "<13>"
        .Value(7, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<13>", "<13>")
        
        .Value(8, 0) = "<160>"
        .Value(8, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<160>", "<160>")
        
        
        '---handle font size token - values can be 1 to 50
        Dim i As Integer
        Dim xFS As String
        xFS = "FontSize"
        For i = 1 To 50
            If InStr(Me.FirmNameFormatted, "<P" & i) Then
                xFS = "FontSize=" & i
            End If
        Next
        
        .Value(9, 0) = xFS
        .Value(9, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<P", "</P")
        
        '---handle font increase token - values can be 1 to 10
        xFS = "FontSize"
        For i = 1 To 10
            If InStr(Me.FirmNameFormatted, "<+P" & i) Then
                xFS = "FontSizeIncrease=" & i
            End If
        Next
        
        .Value(10, 0) = xFS
        .Value(10, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<+P", "</+P")
    
        '---handle font reduction token - values can be 1 to 10
        xFS = "FontSize"
        For i = 1 To 10
            If InStr(Me.FirmNameFormatted, "<-P" & i) Then
                xFS = "FontSizeDecrease=" & i
            End If
        Next
        
        .Value(11, 0) = xFS
        .Value(11, 1) = ParseFirmNameFormats(Me.FirmNameFormatted, "<-P", "</-P")
    
    End With
    Set FirmNameFormats = m_xARFNFormat

End Property
'**********************************************************
'   Methods
'**********************************************************
Public Function Options(xTemplate As String) As mpDB.CPersonOptions
'returns a PersonOptions object containing
'the options for template xTemplate
    If (m_Options Is Nothing) Then
'       no options currently retrieved for this person -
'       create object and get options
        Set m_Options = New mpDB.CPersonOptions
        With m_Options
            .Template = xTemplate
            .Parent = Me
            .Refresh
        End With
    ElseIf xTemplate <> m_Options.Template Then
'       request has been made to switch to options for
'       different template - create a new collection
'       and retrieve appropriate options
        Set m_Options = New mpDB.CPersonOptions
        With m_Options
            .Template = xTemplate
            .Parent = Me
            .Refresh
        End With
    End If
    
'   return current options collection
    Set Options = m_Options
End Function

Public Function AddressOLD(ByVal Separator As String, _
                        Optional ByVal iFormat As mpOfficeAddressFormats = mpOfficeAddressFormat_Default, _
                        Optional ByVal xTemplate As String) As String
'returns the address for this office
'in the specified format
   Dim xAddrTmp As String
    
    If Len(xTemplate) Then
    
    Else
        Select Case iFormat
            Case mpOfficeAddressFormat_Default
                xAddrTmp = GetDefAddr(Separator)
            Case mpOfficeAddressFormat_CountryIfForeign
                xAddrTmp = GetAddrWithCountry(Separator)
        End Select
    End If
    AddressOLD = xAddrTmp
End Function

Public Function Address(oFormat As mpDB.COfficeAddressFormat) As String
'returns the address in the specified format
    Dim xAddrTmp As String
    Dim xDefOfficeCountry As String
    Dim xSep As String
    
    On Error GoTo ProcError
'   get xSep
    xSep = oFormat.Separator
        
    With Me
'       get firm name as specified
        Select Case oFormat.FirmName
            Case mpAddressFirmName_ProperCase
                xAddrTmp = xAddrTmp & .FirmName & xSep
            Case mpAddressFirmName_UpperCase
                xAddrTmp = xAddrTmp & .FirmNameUpperCase & xSep
        End Select
        
'       get slogan if specified
        If oFormat.IncludeSlogan Then
            xAddrTmp = xAddrTmp & .Slogan & xSep
        End If
        
'       get the main part of the address
        xAddrTmp = xAddrTmp & GetCoreAddress(oFormat, xSep)
        
'       add phone if specified
        If oFormat.IncludePhone Then
            If oFormat.IncludePhoneCaption Then
                xAddrTmp = xAddrTmp & Me.PhoneCaption & _
                    oFormat.PhoneCaptionSuffix & .Phone1 & xSep
            Else
                xAddrTmp = xAddrTmp & .Phone1 & xSep
            End If
        End If
                    
'       add fax if specified
        If oFormat.IncludeFax Then
            If oFormat.IncludeFaxCaption Then
                xAddrTmp = xAddrTmp & Me.FaxCaption & _
                        oFormat.FaxCaptionSuffix & .Fax1 & xSep
            Else
                xAddrTmp = xAddrTmp & .Fax1 & xSep
            End If
        End If
                        
'       remove final separator if it's trailing
        If Right(xAddrTmp, Len(xSep)) = xSep Then
            xAddrTmp = Left(xAddrTmp, Len(xAddrTmp) - Len(xSep))
        End If
        
'       add trailing xSep
        xAddrTmp = xAddrTmp & oFormat.TrailingSeparator
        
        Address = xAddrTmp
    End With
    Exit Function
ProcError:
    RaiseError "mpDB.COffice.Address"
End Function

Private Function GetCoreAddress(oFormat As mpDB.COfficeAddressFormat, _
                                ByVal xSep As String) As String
'returns the address of the office in the format specified by
'the office .template property and the OfficeAddressFormat object
    Dim xTemplate As String
    Dim xTemp As String
    Dim xDefOfficeCountry As String
    
    On Error GoTo ProcError
    
    If oFormat.Template <> "" Then
        xTemplate = oFormat.Template
    Else
        xTemplate = Me.Template
    End If
    
    xTemp = xSubstitute(xTemplate, "<Address1>", Me.Address1)
    xTemp = xSubstitute(xTemp, "<Address2>", Me.Address2)
    xTemp = xSubstitute(xTemp, "<Address3>", Me.Address3)
    xTemp = xSubstitute(xTemp, "<PreCity>", Me.PreCity)
    xTemp = xSubstitute(xTemp, "<City>", Me.City)
    xTemp = xSubstitute(xTemp, "<PostCity>", Me.PostCity)
    '9.7.1020 #4534
    xTemp = xSubstitute(xTemp, "<Phone1>", Me.Phone1)
    xTemp = xSubstitute(xTemp, "<Phone2>", Me.Phone2)
    xTemp = xSubstitute(xTemp, "<Phone3>", Me.Phone3)
    xTemp = xSubstitute(xTemp, "<Fax1>", Me.Fax1)
    xTemp = xSubstitute(xTemp, "<Fax2>", Me.Fax2)
    xTemp = xSubstitute(xTemp, "<Custom1>", Me.Custom1)
    xTemp = xSubstitute(xTemp, "<Custom2>", Me.Custom2)
    xTemp = xSubstitute(xTemp, "<Custom3>", Me.Custom3)
    xTemp = xSubstitute(xTemp, "<Custom4>", Me.Custom4)
    xTemp = xSubstitute(xTemp, "<Custom5>", Me.Custom5)
    
'   add state or state abbreviation based on format spec
    If oFormat.FullStateName Then
        xTemp = xSubstitute(xTemp, "<State>", Me.State)
    Else
        xTemp = xSubstitute(xTemp, "<State>", Me.StateAbbr)
    End If
    
    xTemp = xSubstitute(xTemp, "<Zip>", Me.Zip)
    
'   get country if specified
    Select Case oFormat.IncludeCountry
        Case mpAddressCountry_Include
            xTemp = xSubstitute(xTemp, "<Country>", Me.Country)
        Case mpAddressCountry_None
            xTemp = xSubstitute(xTemp, "<Country>", "")
        Case mpAddressCountry_IfForeign
'           get country of default office
            xDefOfficeCountry = Application.Offices.Default.Country

'           if not same country as this office, get country
            If xDefOfficeCountry <> Me.Country Then
                xTemp = xSubstitute(xTemp, "<Country>", Me.Country)
            Else
'               try and remove extraneous chars before country placeholder
                xTemp = xSubstitute(xTemp, ", <Country>", "")
                xTemp = xSubstitute(xTemp, "  <Country>", "")
                xTemp = xSubstitute(xTemp, " <Country>", "")
                xTemp = xSubstitute(xTemp, "<Country>", "")
            End If
    End Select

'   remove extraneous pipes that may exist due to empty address info
    xTemp = xSubstitute(xTemp, "|||", "|")
    xTemp = xSubstitute(xTemp, "||", "|")
    
'   add actual separator
    xTemp = xSubstitute(xTemp, "|", xSep)
    
'   ensure separator after address
    If Right(xTemp, Len(xSep)) <> xSep Then
        xTemp = xTemp & xSep
    End If
    
    GetCoreAddress = xTemp
    Exit Function
ProcError:
    RaiseError "mpDB.COffice.GetCoreAddress"
    Exit Function
End Function
Private Function GetAddrWithCountry(Separator As String) As String
'returns the address with country if the office is
'outside of the default office's country
    Dim xAddrTmp As String
    Dim xState As String
    Dim bHasCityStateLine As Boolean
    Dim xDefaultOfficeCountry As String
    
    With Me
'       get three address lines
        If .Address1 <> "" Then _
            xAddrTmp = xAddrTmp & .Address1 & Separator
        If .Address2 <> "" Then _
            xAddrTmp = xAddrTmp & .Address2 & Separator
        If .Address3 <> "" Then _
            xAddrTmp = xAddrTmp & .Address3 & Separator
            
'       get "pre-city city post-city, state  zip"
        If .PreCity <> "" Then
            xAddrTmp = xAddrTmp & .PreCity
            bHasCityStateLine = True
        End If
            
        If .City <> "" Then
            If bHasCityStateLine Then _
                xAddrTmp = xAddrTmp & " "
            
            xAddrTmp = xAddrTmp & .City
            bHasCityStateLine = True
        End If
            
        If .PostCity <> "" Then
            If bHasCityStateLine Then _
                xAddrTmp = xAddrTmp & " "
            
            xAddrTmp = xAddrTmp & .PostCity
            bHasCityStateLine = True
        End If
            
        If .StateAbbr <> "" Then
            xState = .StateAbbr
            If bHasCityStateLine Then
                xAddrTmp = xAddrTmp & ", "
            End If
            xAddrTmp = xAddrTmp & .StateAbbr
            bHasCityStateLine = True
        ElseIf .State <> "" Then
            xState = .State
            If bHasCityStateLine Then
                xAddrTmp = xAddrTmp & ", "
            End If
            xAddrTmp = xAddrTmp & .State
            bHasCityStateLine = True
        End If
        
        If .Zip <> "" Then
            If bHasCityStateLine Then
                xAddrTmp = xAddrTmp & "  "
            End If
            xAddrTmp = xAddrTmp & .Zip
            bHasCityStateLine = True
        End If
        
'       get country of default office
        xDefaultOfficeCountry = Application.Offices.Default.Country

'       if not same county as this office, get country
        If xDefaultOfficeCountry <> .Country Then
            If bHasCityStateLine Then
                xAddrTmp = xAddrTmp & ", "
            End If
            xAddrTmp = xAddrTmp & .Country
        End If
        
        If Separator = Chr(11) Then
            xAddrTmp = xAddrTmp & vbCr
        Else
            xAddrTmp = xAddrTmp
        End If
        
        GetAddrWithCountry = xAddrTmp
    End With
End Function

Private Function GetDefAddr(Separator As String) As String
    Dim xAddrTmp As String
    Dim xState As String
    Dim bHasCityStateLine As Boolean
    
    With Me
'       get three address lines
        If .Address1 <> "" Then _
            xAddrTmp = xAddrTmp & .Address1 & Separator
        If .Address2 <> "" Then _
            xAddrTmp = xAddrTmp & .Address2 & Separator
        If .Address3 <> "" Then _
            xAddrTmp = xAddrTmp & .Address3 & Separator
            
'       get "pre-city city post-city, state  zip"
        If .PreCity <> "" Then
            xAddrTmp = xAddrTmp & .PreCity
            bHasCityStateLine = True
        End If
            
        If .City <> "" Then
            If bHasCityStateLine Then _
                xAddrTmp = xAddrTmp & " "
            
            xAddrTmp = xAddrTmp & .City
            bHasCityStateLine = True
        End If
            
        If .PostCity <> "" Then
            If bHasCityStateLine Then _
                xAddrTmp = xAddrTmp & " "
            
            xAddrTmp = xAddrTmp & .PostCity
            bHasCityStateLine = True
        End If
            
        If .StateAbbr <> "" Then
            xState = .StateAbbr
            If bHasCityStateLine Then
                xAddrTmp = xAddrTmp & ", "
            End If
            xAddrTmp = xAddrTmp & .StateAbbr
            bHasCityStateLine = True
        ElseIf .State <> "" Then
            xState = .State
            If bHasCityStateLine Then
                xAddrTmp = xAddrTmp & ", "
            End If
            xAddrTmp = xAddrTmp & .State
            bHasCityStateLine = True
        End If
        
        If .Zip <> "" Then
            If bHasCityStateLine Then
                xAddrTmp = xAddrTmp & "  "
            End If
            xAddrTmp = xAddrTmp & .Zip
            bHasCityStateLine = True
        End If
        
        If Separator = Chr(11) Then
            xAddrTmp = xAddrTmp & vbCr
        Else
            xAddrTmp = xAddrTmp
        End If
        
        GetDefAddr = xAddrTmp
    End With
End Function

Private Function ParseFirmNameFormats(xSource As String, xStartToken As String, xFinishToken As String) As String
    '---9.7.1 - 4035 returns string enclosed in formatting token
    Dim iStartPos As Integer
    Dim iEndPos As Integer
    Dim iLength As Integer
    Dim i As Integer
    Dim xTemp As String
    
'''    xSource = "<B>Your Firm <I>Name</I>, <P8>LLP</P8></B>"
'''    xStartToken = "<P8>"
'''    xFinishToken = "</P8>"
    
    '---construct font size token
    If xStartToken = "<P" Then
        For i = 1 To 50
            If InStr(xSource, "<P" & i & ">") Then
                xStartToken = "<P" & i & ">"
                xFinishToken = "</P" & i & ">"
            End If
        Next
    End If
    
'   get increase/decrease font tokens
    If xStartToken = "<+P" Then
        For i = 1 To 10
            If InStr(xSource, "<+P" & i & ">") Then
                xStartToken = "<+P" & i & ">"
                xFinishToken = "</+P" & i & ">"
            End If
        Next
    End If

    If xStartToken = "<-P" Then
        For i = 1 To 10
            If InStr(xSource, "<-P" & i & ">") Then
                xStartToken = "<-P" & i & ">"
                xFinishToken = "</-P" & i & ">"
            End If
        Next
    End If

    iStartPos = InStr(xSource, xStartToken)
    iEndPos = InStr(xSource, xFinishToken)
    
    If iStartPos < iEndPos Then
        iLength = iEndPos - iStartPos
        If iLength = 0 Then Exit Function
        xTemp = Mid(xSource, iStartPos + Len(xStartToken), iLength - Len(xStartToken))
    Else
        If iStartPos + iEndPos = 0 Then Exit Function
        xTemp = Left(xSource, iStartPos - 1)
    End If
    
    
    '---Clean up nested tokens - font size first
    For i = 1 To 50
        xTemp = mpBase2.xSubstitute(xTemp, "<P" & i & ">", "")
        xTemp = mpBase2.xSubstitute(xTemp, "</P" & i & ">", "")
    Next
    
'   Clean up nested tokens - font increase/reduction
    For i = 1 To 10
        xTemp = mpBase2.xSubstitute(xTemp, "<+P" & i & ">", "")
        xTemp = mpBase2.xSubstitute(xTemp, "</+P" & i & ">", "")
        xTemp = mpBase2.xSubstitute(xTemp, "<-P" & i & ">", "")
        xTemp = mpBase2.xSubstitute(xTemp, "</-P" & i & ">", "")
    Next
    
    xTemp = mpBase2.xSubstitute(xTemp, "<B>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "</B>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<SC>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "</SC>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<U>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "</U>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<C>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "</C>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<I>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "</I>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<LC>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "</LC>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<11>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<13>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<9>", "")
    xTemp = mpBase2.xSubstitute(xTemp, "<160>", "")
    
    ParseFirmNameFormats = xTemp

End Function
Private Sub Class_Terminate()
    Set m_Options = Nothing
End Sub
