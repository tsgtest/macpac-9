VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEnvelopeDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   Envelope Definition Class
'   created 10/15/99 by Mike Conner-
'   barbies@ix.netcom.com

'   Contains properties and methods that
'   define an Envelope

'   Member of CEnvelopeDefs
'**********************************************************
Option Explicit

'**********************************************************
Private m_lID As Long
Private m_xSize As String
Private m_xDesc As String
Private m_sAddrLMin As Single
Private m_sAddrLMax As Single
Private m_sAddrTopMin As Single
Private m_sAddrTopMax As Single
Private m_sAddrLDef As Single
Private m_sAddrTDef As Single
Private m_bAllowDPhrases As Boolean
Private m_bAllowBarCode As Boolean
Private m_bAllowBillNo As Boolean
Private m_bAllowReturnAddress As Boolean
Private m_bAllowAuthorInitials As Boolean
Private m_iMaxLines As Integer
Private m_xBPFile As String
Private m_iReturnAddressFormat As Integer
Private m_bSplitDPhrases As Boolean '9.7.1 #4024
Private m_xDPhrase1Caption As String '9.7.1 #4024
Private m_xDPhrase2Caption As String '9.7.1 #4024


'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property

Public Property Get ID() As Long
    ID = m_lID
End Property

Friend Property Let Size(xNew As String)
    m_xSize = xNew
End Property

Public Property Get Size() As String
    Size = m_xSize
End Property
Public Property Get SplitDPhrases() As Boolean '9.7.1 #4024
    SplitDPhrases = m_bSplitDPhrases
End Property
Public Property Let SplitDPhrases(bNew As Boolean) '9.7.1 #4024
    m_bSplitDPhrases = bNew
End Property
Public Property Get DPhrase1Caption() As String '9.7.1 #4024
    DPhrase1Caption = m_xDPhrase1Caption
End Property
Public Property Let DPhrase1Caption(xNew As String) '9.7.1 #4024
    m_xDPhrase1Caption = xNew
End Property
Public Property Get DPhrase2Caption() As String '9.7.1 #4024
    DPhrase2Caption = m_xDPhrase2Caption
End Property
Public Property Let DPhrase2Caption(xNew As String) '9.7.1 #4024
    m_xDPhrase2Caption = xNew
End Property

Friend Property Let BoilerplateFile(xNew As String)
    m_xBPFile = xNew
End Property

Public Property Get BoilerplateFile() As String
    BoilerplateFile = m_xBPFile
End Property

Friend Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Friend Property Let ReturnAddressFormat(iNew As Integer)
    m_iReturnAddressFormat = iNew
End Property

Public Property Get ReturnAddressFormat() As Integer
    ReturnAddressFormat = m_iReturnAddressFormat
End Property

Friend Property Let AddressLeftMinimum(sNew As Single)
    m_sAddrLMin = sNew
End Property

Public Property Get AddressLeftMinimum() As Single
    AddressLeftMinimum = m_sAddrLMin
End Property

Friend Property Let AddressLeftMaximum(sNew As Single)
    m_sAddrLMax = sNew
End Property

Public Property Get AddressLeftMaximum() As Single
    AddressLeftMaximum = m_sAddrLMax
End Property

Friend Property Let AddressLeftDefault(sNew As Single)
    m_sAddrLDef = sNew
End Property

Public Property Get AddressLeftDefault() As Single
    AddressLeftDefault = m_sAddrLDef
End Property

Friend Property Let AddressTopDefault(sNew As Single)
    m_sAddrTDef = sNew
End Property

Public Property Get AddressTopDefault() As Single
    AddressTopDefault = m_sAddrTDef
End Property

Friend Property Let AddressTopMinimum(sNew As Single)
    m_sAddrTopMin = sNew
End Property

Public Property Get AddressTopMinimum() As Single
    AddressTopMinimum = m_sAddrTopMin
End Property

Friend Property Let AddressTopMaximum(sNew As Single)
    m_sAddrTopMax = sNew
End Property

Public Property Get AddressTopMaximum() As Single
    AddressTopMaximum = m_sAddrTopMax
End Property

Friend Property Let AllowDPhrases(bNew As Boolean)
    m_bAllowDPhrases = bNew
End Property

Public Property Get AllowDPhrases() As Boolean
    AllowDPhrases = m_bAllowDPhrases
End Property

Friend Property Let AllowBarCode(bNew As Boolean)
    m_bAllowBarCode = bNew
End Property

Public Property Get AllowBarCode() As Boolean
    AllowBarCode = m_bAllowBarCode
End Property

Friend Property Let AllowReturnAddress(bNew As Boolean)
    m_bAllowReturnAddress = bNew
End Property

Public Property Get AllowReturnAddress() As Boolean
    AllowReturnAddress = m_bAllowReturnAddress
End Property

Friend Property Let AllowBillingNumber(bNew As Boolean)
    m_bAllowBillNo = bNew
End Property

Public Property Get AllowBillingNumber() As Boolean
    AllowBillingNumber = m_bAllowBillNo
End Property

Friend Property Let AllowAuthorInitials(bNew As Boolean)
    m_bAllowAuthorInitials = bNew
End Property

Public Property Get AllowAuthorInitials() As Boolean
    AllowAuthorInitials = m_bAllowAuthorInitials
End Property

Friend Property Let MaximumLines(lNew As Long)
    m_iMaxLines = lNew
End Property

Public Property Get MaximumLines() As Long
    MaximumLines = m_iMaxLines
End Property

'**********************************************************
'   Methods
'**********************************************************
