VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomPropertyDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CTemplateCustomPropertyDefs Collection Class
'   created 12/10/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CTemplateCustomPropertyDefs

'   Member of
'   Container for CTemplateCustomPropertyDef
'**********************************************************

'**********************************************************
Private m_oRS As DAO.Recordset
Private m_oProperty As CCustomPropertyDef
Private m_xCategory As String
'**********************************************************


'**********************************************************
'   Properties
'**********************************************************
Public Property Let Category(xNew As String)
    m_xCategory = xNew
    
'   get recordset containing custom
'   properties for supplied template
    Refresh xNew
End Property

Public Property Get Category() As String
    Category = m_xCategory
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Add() As CCustomPropertyDef
    On Error GoTo ProcError
    
    Set m_oProperty = New CCustomPropertyDef
    m_oProperty.IsNew = True
    Set Add = m_oProperty
    Exit Function
ProcError:
    RaiseError "mpDB.CCustomPropertyDefs.Add", Err.Description
    Exit Function
End Function

Public Function Exists(ByVal xName As String, ByVal xCategory As String) As Boolean
'returns TRUE if the specified custom property already exists
    Dim xSQL As String
    
    xSQL = "SELECT fldID FROM tblCustomProperties WHERE " & _
           "fldName =""" & xName & """ AND fldCategory = """ & xCategory & """"
            
    Exists = Application.PublicDB.OpenRecordset(xSQL).RecordCount > 0
    
    Exit Function
ProcError:
    RaiseError "mpDB.CCustomPropertyDefs.Exists", Err.Description
    Exit Function
End Function

Public Sub Save(oDef As CCustomPropertyDef)
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    If oDef Is Nothing Then
        Err.Raise mpError.mpError_InvalidParameter, , _
            "Could not save custom property definition.  " & _
            "The supplied variable is 'Nothing'."
    End If
    
    With oDef
        'validate
        If .Category = Empty Or .Name = Empty Then
            'failed validation
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save custom dialog definition.  " & _
                "'Category' and 'Name' are  required data."
        ElseIf .PropertyType = mpCustomPropertyType_Author And _
            .LinkedProperty = Empty Then
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Linked property is required when property type is Author."
        ElseIf .PropertyType = mpCustomPropertyType_AuthorOffice And _
            .LinkedProperty = Empty Then
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Linked property is required when property " & _
                "type is Author Office."
        ElseIf .IsNew And Me.Exists(.Name, .Category) Then
            'custom dialog definition already exists
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save custom property definition.  " & _
                "A custom property with name '" & _
                .Name & "' already exists for category '" & .Category & "'."
        End If
        
        If .IsNew Then
            xSQL = "INSERT INTO tblCustomProperties(fldName,fldCategory,fldAction," & _
                "fldType,fldLinkedProperty,fldBookmark,fldWholeParagraph,fldIndexed," & _
                "fldUnderlineLength,fldDelimiterReplacement" & _
                IIf(.Macro <> Empty, ",fldMacro", "") & ") VALUES (""" & _
                .Name & """,""" & _
                .Category & """," & _
                .Action & "," & _
                .PropertyType & ",""" & _
                .LinkedProperty & """,""" & _
                .Bookmark & """," & _
                .WholeParagraph & "," & _
                .Indexed & "," & _
                .UnderlineLength & ",""" & _
                .DelimiterReplacement & """" & _
                IIf(.Macro <> Empty, ",""" & .Macro & """)", ")")
        Else
            xSQL = "UPDATE tblCustomProperties SET " & _
                   "fldName=""" & .Name & _
                   """,fldCategory=""" & .Category & _
                   """,fldAction=" & .Action & _
                   ",fldType=" & .PropertyType & _
                   ",fldLinkedProperty=""" & .LinkedProperty & _
                   """,fldBookmark=""" & .Bookmark & _
                   """,fldWholeParagraph=" & .WholeParagraph & _
                   ",fldIndexed=" & .Indexed & _
                   ",fldUnderlineLength=" & .UnderlineLength & _
                   ",fldDelimiterReplacement=""" & .DelimiterReplacement & _
                   IIf(.Macro <> Empty, """,fldMacro=""" & .Macro & """", """") & " WHERE fldID= " & .ID
        End If
    End With
        
    With Application.PublicDB
        'execute query
        .Execute xSQL
        
        If .RecordsAffected = 0 Then
            'add, update failed - raise error
            Err.Raise 600, , "Could not save the custom property '" & _
                oDef.Category & "!" & oDef.Name & "'."
        Else
            oDef.IsNew = False
            Refresh Me.Category
        End If
    End With
        
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomPropertyDefs.Save", Err.Description
    Exit Sub
End Sub

Public Sub Delete(ByVal lID As Long)
'deletes the custom property with ID = lID
    Dim xSQL As String
    On Error GoTo ProcError
    xSQL = "DELETE FROM tblCustomProperties WHERE fldID=" & lID
    
    With Application.PublicDB
        'execute query
        .Execute xSQL
        
        If .RecordsAffected = 0 Then
            'delete failed - raise error
            Err.Raise 600, , _
                "Could not delete the custom property with ID='" & CStr(lID) & "'."
        End If
    End With
    Refresh Me.Category
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomPropertyDefs.Delete", Err.Description
    Exit Sub
End Sub

Public Sub DeleteAll()
'deletes all custom properties for the specified category
    Dim xSQL As String
    On Error GoTo ProcError
    xSQL = "DELETE FROM tblCustomProperties WHERE fldCategory='" & Me.Category & "'"
    
    With Application.PublicDB
        'execute query
        .Execute xSQL
        
        If .RecordsAffected > 0 Then
            Refresh Me.Category
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomPropertyDefs.DeleteAll", Err.Description
    Exit Sub
End Sub

Private Sub Refresh(ByVal xCategory As String)
'get recordset of property defs from db
    Dim xSQL As String
    
'***********************************************
'multitype:

    Dim xBase As String
    Dim xTemplate As String
    Dim iNumProps As Integer
    
    Dim oTemplateDef As mpDB.CTemplateDef

'   get template definition for this category - category
'   does not have to be template, so oTemplateDef can
'   end up being Nothing
    On Error Resume Next
    Set oTemplateDef = Application.TemplateDefinitions.Item(xCategory)
    On Error GoTo ProcError
    
'   xbase will be non-empty only if xCategory is a template name
    If Not oTemplateDef Is Nothing Then
        xBase = Application.TemplateDefinitions(xCategory).BaseTemplate
    End If
    
'   count num cust props
    xSQL = "SELECT COUNT(fldCategory) FROM tblCustomProperties " & _
           "WHERE fldCategory = '" & xCategory & "'"
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenForwardOnly, dbReadOnly)
    
'   get number of properties for this category
    iNumProps = m_oRS.Fields(0)
    
'   if there aren't any, and there is a base template specified,
'   use the base templates custom properties
    If iNumProps = 0 And Len(xBase) Then
        xCategory = xBase
    End If
    
'   open the recordset of custom properties
    xSQL = "SELECT * FROM tblCustomProperties " & _
           "WHERE fldCategory = '" & xCategory & "'"
           
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    If Not (m_oRS.BOF And m_oRS.EOF) Then
        m_oRS.MoveLast
        m_oRS.MoveFirst
    End If
'***********************************************
    Exit Sub
ProcError:
    RaiseError "mpDB.CCustomPropertyDefs.Refresh"
    Exit Sub
End Sub

Public Property Get Count() As Integer
    If Not (m_oRS Is Nothing) Then
        Count = m_oRS.RecordCount
    End If
End Property

Public Function Item(vID As Variant) As CCustomPropertyDef
'finds the record with ID = vID or Name = vID -
'returns the found record as a CCustomPropertyDef-
'raises error if no match - returns "Nothing" if
'no recordset exists - this will be the case if
'no template has been specified (ie me.template = empty)
    Dim xCriteria As String
    Dim vBkmk As Variant
    
    On Error GoTo ProcError
    
    If m_oRS Is Nothing Then
        Exit Function
    End If
    
    With m_oRS
'       set criteria
        If IsNumeric(vID) Then
            On Error Resume Next
            .MoveFirst
            .AbsolutePosition = vID - 1
            If Err.Number Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            On Error GoTo ProcError
        Else
            xCriteria = "fldName = """ & vID & """"
    
'           find
            .FindFirst xCriteria
            
            If .NoMatch Then
'               not found
                Err.Raise mpError_InvalidMember
            End If
            
        End If
        
'       requested item exists - update current object
        UpdateObject
                
'       return current object
        m_oProperty.IsNew = False
        Set Item = m_oProperty
    End With
    
    Exit Function
    
ProcError:
    RaiseError "mpDB.CCustomPropertyDefs.Item"
    Exit Function
End Function

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_oProperty = New mpDB.CCustomPropertyDef
End Sub

Private Sub Class_Terminate()
    Set m_oRS = Nothing
    Set m_oProperty = Nothing
End Sub

Private Sub UpdateObject()
    With m_oProperty
        .Name = Empty
        .Bookmark = Empty
        .DelimiterReplacement = Empty
        .ID = Empty
        .Name = Empty
        .LinkedProperty = Empty
        .WholeParagraph = Empty
        .PropertyType = Empty
        .Macro = Empty
        .Indexed = Empty
        .Action = Empty
        .UnderlineLength = Empty
        .Category = Empty
        
        On Error Resume Next
        .Name = m_oRS!fldName
        .Bookmark = m_oRS!fldBookmark
        .DelimiterReplacement = m_oRS!fldDelimiterReplacement
        .ID = m_oRS!fldID
        .LinkedProperty = m_oRS!fldLinkedProperty
        .WholeParagraph = m_oRS!fldWholeParagraph
        .PropertyType = m_oRS!fldType
        .Macro = m_oRS!fldMacro
        .Indexed = m_oRS!fldIndexed
        .Action = m_oRS!fldAction
        .UnderlineLength = m_oRS!fldUnderlineLength
        .Category = m_oRS!fldCategory
    End With
End Sub
