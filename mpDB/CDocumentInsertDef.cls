VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDocumentInsertDef"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'**********************************************************
'   CDocumentInsertDef Class
'   created 10/21/05 by Jeffrey Sweetland

'   Contains properties and methods that define
'   a MacPac Document Insert
'
'   Member of CDocumentInsertDefs
'**********************************************************
Private m_lID As Long
Private m_xName As String
Private m_xSubject As String
Private m_xBoilerplate1 As String
Private m_xBookmark1 As String
Private m_iLocation1 As Integer
Private m_xTargetBookmark1 As String
Private m_xBoilerplate2 As String
Private m_xBookmark2 As String
Private m_iLocation2 As Integer
Private m_xTargetBookmark2 As String
Private m_xBoilerplate3 As String
Private m_xBookmark3 As String
Private m_iLocation3 As Integer
Private m_xTargetBookmark3 As String
Private m_xBoilerplate4 As String
Private m_xBookmark4 As String
Private m_iLocation4 As Integer
Private m_xTargetBookmark4 As String

Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property
Public Property Get ID() As Long
    ID = m_lID
End Property
Public Property Let Name(xNew As String)
    m_xName = xNew
End Property
Public Property Get Name() As String
    Name = m_xName
End Property
Public Property Let Subject(xNew As String)
    m_xSubject = xNew
End Property
Public Property Get Subject() As String
    Subject = m_xSubject
End Property
Public Property Let Boilerplate1(xNew As String)
    m_xBoilerplate1 = xNew
End Property
Public Property Get Boilerplate1() As String
    Boilerplate1 = m_xBoilerplate1
End Property
Public Property Let Bookmark1(xNew As String)
    m_xBookmark1 = xNew
End Property
Public Property Get Bookmark1() As String
    Bookmark1 = m_xBookmark1
End Property
Public Property Let Location1(iNew As Integer)
    m_iLocation1 = iNew
End Property
Public Property Get Location1() As Integer
    Location1 = m_iLocation1
End Property
Public Property Let TargetBookmark1(xNew As String)
    m_xTargetBookmark1 = xNew
End Property
Public Property Get TargetBookmark1() As String
    TargetBookmark1 = m_xTargetBookmark1
End Property
Public Property Let Boilerplate2(xNew As String)
    m_xBoilerplate2 = xNew
End Property
Public Property Get Boilerplate2() As String
    Boilerplate2 = m_xBoilerplate2
End Property
Public Property Let Bookmark2(xNew As String)
    m_xBookmark2 = xNew
End Property
Public Property Get Bookmark2() As String
    Bookmark2 = m_xBookmark2
End Property
Public Property Let Location2(iNew As Integer)
    m_iLocation2 = iNew
End Property
Public Property Get Location2() As Integer
    Location2 = m_iLocation2
End Property
Public Property Let TargetBookmark2(xNew As String)
    m_xTargetBookmark2 = xNew
End Property
Public Property Get TargetBookmark2() As String
    TargetBookmark2 = m_xTargetBookmark2
End Property
Public Property Let Boilerplate3(xNew As String)
    m_xBoilerplate3 = xNew
End Property
Public Property Get Boilerplate3() As String
    Boilerplate3 = m_xBoilerplate3
End Property
Public Property Let Bookmark3(xNew As String)
    m_xBookmark3 = xNew
End Property
Public Property Get Bookmark3() As String
    Bookmark3 = m_xBookmark3
End Property
Public Property Let Location3(iNew As Integer)
    m_iLocation3 = iNew
End Property
Public Property Get Location3() As Integer
    Location3 = m_iLocation3
End Property
Public Property Let TargetBookmark3(xNew As String)
    m_xTargetBookmark3 = xNew
End Property
Public Property Get TargetBookmark3() As String
    TargetBookmark3 = m_xTargetBookmark3
End Property
Public Property Let Boilerplate4(xNew As String)
    m_xBoilerplate4 = xNew
End Property
Public Property Get Boilerplate4() As String
    Boilerplate4 = m_xBoilerplate4
End Property
Public Property Let Bookmark4(xNew As String)
    m_xBookmark4 = xNew
End Property
Public Property Get Bookmark4() As String
    Bookmark4 = m_xBookmark4
End Property
Public Property Let Location4(iNew As Integer)
    m_iLocation4 = iNew
End Property
Public Property Get Location4() As Integer
    Location4 = m_iLocation4
End Property
Public Property Let TargetBookmark4(xNew As String)
    m_xTargetBookmark4 = xNew
End Property
Public Property Get TargetBookmark4() As String
    TargetBookmark4 = m_xTargetBookmark4
End Property

