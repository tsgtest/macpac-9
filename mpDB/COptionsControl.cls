VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COptionsControl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
  Option Explicit

'**********************************************************
'   COptionsControl Class
'   created 12/28/99 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods of a
'   MacPac Template Options Control - the
'   control associated with a field in a
'   user options table

'   Member of App
'   Container for
'**********************************************************
Public Enum mpOptionsControls
    mpOptionsControl_TextBox = 1
    mpOptionsControl_ListBox = 2
    mpOptionsControl_ComboBox = 3
    mpOptionsControl_CheckBox = 4
    mpOptionsControl_Label = 5
    mpOptionsControl_SpinText = 6
End Enum

Public Enum mpOptionsMeasurementUnits
    mpOptionsMeasurementUnit_None = -1
    mpOptionsMeasurementUnit_Inches = 0
    mpOptionsMeasurementUnit_Centimeters = 1
    mpOptionsMeasurementUnit_Millimeters = 2
    mpOptionsMeasurementUnit_Points = 3
    mpOptionsMeasurementUnit_Picas = 4
End Enum

Private m_iMeasurementUnit As mpOptionsMeasurementUnits
Private m_xCaption As String
Private m_xOptionsField As String
Private m_xName As String
Private m_iControlType As mpOptionsControls
Private m_iIndex As Integer
Private m_oList As CList
Private m_iVOffset As Integer
Private m_iHOffset As Integer
Private m_iWidth As Integer
Private m_sMin As Single
Private m_sMax As Single
Private m_sIncrement As Single
Private m_bAppendQuotes As Boolean

'**********************************************************
'   Properties
'**********************************************************
Friend Property Let Caption(xNew As String)
    m_xCaption = xNew
End Property

Public Property Get Caption() As String
    Caption = m_xCaption
End Property

Friend Property Let OptionsField(xNew As String)
    m_xOptionsField = xNew
End Property

Public Property Get OptionsField() As String
    OptionsField = m_xOptionsField
End Property

Public Property Let MeasurementUnit(iUnit As mpOptionsMeasurementUnits)
    m_iMeasurementUnit = iUnit
End Property

Public Property Get MeasurementUnit() As mpOptionsMeasurementUnits
    MeasurementUnit = m_iMeasurementUnit
End Property

Public Property Get ControlType() As mpOptionsControls
    ControlType = m_iControlType
End Property

Friend Property Let ControlType(iNew As mpOptionsControls)
    m_iControlType = iNew
End Property

Public Property Get Index() As Integer
    Index = m_iIndex
End Property

Friend Property Let Index(iNew As Integer)
    m_iIndex = iNew
End Property

Friend Property Set List(oNew As mpDB.CList)
    Set m_oList = oNew
End Property

Public Property Get List() As mpDB.CList
    Set List = m_oList
End Property

Public Property Get VerticalOffset() As Integer
    VerticalOffset = m_iVOffset
End Property

Public Property Let VerticalOffset(iNew As Integer)
    m_iVOffset = iNew
End Property

Public Property Get HorizontalOffset() As Integer
    HorizontalOffset = m_iHOffset
End Property

Public Property Let HorizontalOffset(iNew As Integer)
    m_iHOffset = iNew
End Property

Public Property Get Width() As Integer
    Width = m_iWidth
End Property

Public Property Let Width(iNew As Integer)
    m_iWidth = iNew
End Property

Public Property Get MinValue() As Single
    MinValue = m_sMin
End Property

Public Property Let MinValue(sNew As Single)
    m_sMin = sNew
End Property

Public Property Get MaxValue() As Single
    MaxValue = m_sMax
End Property

Public Property Let MaxValue(sNew As Single)
    m_sMax = sNew
End Property

Public Property Get Increment() As Single
    Increment = m_sIncrement
End Property

Public Property Let Increment(sNew As Single)
    m_sIncrement = sNew
End Property

Public Property Let AppendQuotes(bNew As Boolean)
    m_bAppendQuotes = bNew
End Property

Public Property Get AppendQuotes() As Boolean
    AppendQuotes = m_bAppendQuotes
End Property
'**********************************************************
'   Methods
'**********************************************************
'**********************************************************
'   Class Events
'**********************************************************
