VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COfficeAddressFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   COfficeAddressFormat Class
'   created 5/19/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that
'   define the format of an Office Address

'   Member of COfficeAddressFormats
'   Container for
'**********************************************************
Enum mpAddressCountry
    mpAddressCountry_None = 1
    mpAddressCountry_Include = 2
    mpAddressCountry_IfForeign = 3
End Enum

Enum mpAddressFirmName
    mpAddressFirmName_None = 1
    mpAddressFirmName_ProperCase = 2
    mpAddressFirmName_UpperCase = 3
End Enum

Private m_lID As Long
Private m_xName As String
Private m_bFullStateName As Boolean
Private m_iFirmName As mpAddressFirmName
Private m_bIncludeSlogan As Boolean
Private m_iIncludeCountry As mpAddressCountry
Private m_bIncludePhone As Boolean
Private m_bIncludeFax As Boolean
Private m_bIncludePhoneCaption As Boolean
Private m_bIncludeFaxCaption As Boolean
Private m_xFaxCaptionSuffix As String
Private m_xPhoneCaptionSuffix As String
Private m_xSeparator As String
Private m_xTrailingSeparator As String
Private m_xCountryPrefix As String
Private m_bStandardAddress As Boolean
Private m_xTemplate As String



Public Property Let ID(lNew As Long)
    m_lID = lNew
End Property
Public Property Get ID() As Long
    ID = m_lID
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property
Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let FullStateName(bNew As Boolean)
    m_bFullStateName = bNew
End Property
Public Property Get FullStateName() As Boolean
    FullStateName = m_bFullStateName
End Property

Public Property Let Template(xNew As String)
    m_xTemplate = xNew
End Property

Public Property Get Template() As String
    Template = m_xTemplate
End Property

Public Property Let FirmName(iNew As mpAddressFirmName)
    m_iFirmName = iNew
End Property
Public Property Get FirmName() As mpAddressFirmName
    FirmName = m_iFirmName
End Property

Public Property Let IncludeSlogan(bNew As Boolean)
    m_bIncludeSlogan = bNew
End Property
Public Property Get IncludeSlogan() As Boolean
    IncludeSlogan = m_bIncludeSlogan
End Property

Public Property Let IncludeCountry(iNew As mpAddressCountry)
    m_iIncludeCountry = iNew
End Property
Public Property Get IncludeCountry() As mpAddressCountry
    IncludeCountry = m_iIncludeCountry
End Property

Public Property Let StandardAddress(bNew As Boolean)
    m_bStandardAddress = bNew
End Property
Public Property Get StandardAddress() As Boolean
    StandardAddress = m_bStandardAddress
End Property

Public Property Let IncludePhone(bNew As Boolean)
    m_bIncludePhone = bNew
End Property
Public Property Get IncludePhone() As Boolean
    IncludePhone = m_bIncludePhone
End Property

Public Property Let IncludeFax(bNew As Boolean)
    m_bIncludeFax = bNew
End Property
Public Property Get IncludeFax() As Boolean
    IncludeFax = m_bIncludeFax
End Property

Public Property Let IncludePhoneCaption(bNew As Boolean)
    m_bIncludePhoneCaption = bNew
End Property

Public Property Get IncludePhoneCaption() As Boolean
    IncludePhoneCaption = m_bIncludePhoneCaption
End Property

Public Property Let IncludeFaxCaption(bNew As Boolean)
    m_bIncludeFaxCaption = bNew
End Property

Public Property Get IncludeFaxCaption() As Boolean
    IncludeFaxCaption = m_bIncludeFaxCaption
End Property

Public Property Let PhoneCaptionSuffix(xNew As String)
    m_xPhoneCaptionSuffix = xNew
End Property
Public Property Get PhoneCaptionSuffix() As String
    If IsNumeric(m_xPhoneCaptionSuffix) Then
        PhoneCaptionSuffix = Chr(m_xPhoneCaptionSuffix)
    Else
'       replace tokens
        PhoneCaptionSuffix = xReplaceTokens(m_xPhoneCaptionSuffix)
    End If
End Property

Public Property Let FaxCaptionSuffix(xNew As String)
    m_xFaxCaptionSuffix = xNew
End Property
Public Property Get FaxCaptionSuffix() As String
    If IsNumeric(m_xFaxCaptionSuffix) Then
        FaxCaptionSuffix = Chr(m_xFaxCaptionSuffix)
    Else
'       replace tokens
        FaxCaptionSuffix = xReplaceTokens(m_xFaxCaptionSuffix)
    End If
End Property

Public Property Let Separator(xNew As String)
    m_xSeparator = xNew
End Property
Public Property Get Separator() As String
    If IsNumeric(m_xSeparator) Then
        Separator = Chr(m_xSeparator)
    Else
'       replace tokens
        Separator = xReplaceTokens(m_xSeparator)
    End If
End Property

Public Property Let TrailingSeparator(xNew As String)
    m_xTrailingSeparator = xNew
End Property
Public Property Get TrailingSeparator() As String
    If IsNumeric(m_xTrailingSeparator) Then
        TrailingSeparator = Chr(m_xTrailingSeparator)
    Else
'       replace tokens
        TrailingSeparator = xReplaceTokens(m_xTrailingSeparator)
    End If
End Property
