VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPleadingDefs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CPleadingDefs Collection Class
'   created 1/11/00 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CPleadingDef

'   Member of
'   Container for CPleadingDef
'**********************************************************

'**********************************************************

Private m_oRS As DAO.Recordset
Private m_oType As mpDB.CPleadingDef

Private Const mpStyleValueUndefined As Long = -777777

'**********************************************************
'   Properties
'**********************************************************

'**********************************************************
'   Methods
'**********************************************************
Public Function Count()
    On Error Resume Next
    Count = m_oRS.RecordCount
End Function

Public Function Item(lID As Long) As mpDB.CPleadingDef
Attribute Item.VB_UserMemId = 0
    On Error GoTo ProcError
    
'   get item where index = vID
    If (lID <= 0) Then
        Err.Raise mpError_InvalidItemIndex
    Else
        m_oRS.AbsolutePosition = lID - 1
        UpdateObject
    End If
    Set Item = m_oType
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingDefs.Item", _
              Application.Error.Desc(Err.Number)
End Function

Public Function ItemFromCourt(ByVal lLevel0 As Long, _
                              ByVal iLevel1 As Integer, _
                              ByVal iLevel2 As Integer, _
                              ByVal ilevel3 As Integer) As mpDB.CPleadingDef
Attribute ItemFromCourt.VB_Description = "returns the CPleadingType object that matches the supplied levels 0-3.  Returns NOTHING if there is no match."
'returns the first court that matches the supplied court levels -
'if the supplied levels don't match, we look for a more general entry
    Dim xCrit As String
    
    On Error GoTo ProcError
    
'   get item where levels match
    xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
            "fldLevel1 = " & iLevel1 & " AND " & _
            "fldLevel2 = " & iLevel2 & " AND " & _
            "fldLevel3 = " & ilevel3

    With m_oRS
'       attempt to find the item
        .FindFirst xCrit
        If .NoMatch Then
'           if no match found, make it less specific
            xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
                    " fldLevel1 = " & iLevel1 & " AND " & _
                    " fldLevel2 = " & iLevel2 & " AND " & _
                    " fldLevel3 Is Null"

            .FindFirst xCrit
        End If
        
        If .NoMatch Then
'           if no match found, make it less specific
            xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
                    " fldLevel1 = " & iLevel1 & " AND " & _
                    " fldLevel2 Is Null AND " & _
                    " fldLevel3 Is Null"
                    
            .FindFirst xCrit
        End If
        
        If .NoMatch Then
'           if no match found, make it less specific
            xCrit = "fldLevel0 = " & lLevel0 & " AND " & _
                    " fldLevel1 Is Null AND " & _
                    " fldLevel2 Is Null AND " & _
                    " fldLevel3 Is Null"
            .FindFirst xCrit
        End If
        
        If .NoMatch Then
'           return nothing
            Set ItemFromCourt = Nothing
        Else
'           item found - update template object
            UpdateObject
'           return template object
            Set ItemFromCourt = m_oType
        End If
    End With
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CPleadingDefs.ItemFromCourt", _
              Application.Error.Desc(Err.Number)
End Function

Private Function Refresh()
    Dim xSQL As String
    
    On Error GoTo ProcError
        
'   get templates
    xSQL = "SELECT * FROM tblPleadingTypes"
           
'   open the rs
    Set m_oRS = Application.PublicDB.OpenRecordset(xSQL, dbOpenSnapshot)
    With m_oRS
        If .RecordCount Then
            .MoveLast
            .MoveFirst
        End If
    End With
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              "mpDB.CTemplates.Refresh", _
              Application.Error.Desc(Err.Number)
    Exit Function
End Function

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub UpdateObject()
    With m_oType
        .BoilerplateFile = Empty
        .FirstNumberedPage = Empty
        .FirstPageNumber = Empty
        .ID = Empty
        .Level0 = Empty
        .Level1 = Empty
        .Level2 = Empty
        .Level3 = Empty
        .DefaultCaptionType = Empty
        .DefaultCaptionType2 = Empty
        .DefaultCounselType = Empty
        .DefaultPleadingPaperType = Empty
        .DefaultSignatureType = Empty
        .CourtTitle = Empty
        .CourtTitle1 = Empty
        .CourtTitle2 = Empty
        .CourtTitle3 = Empty
        .FooterTextMaxChars = Empty
        .FooterTextIncludesCaseNo = Empty
        .AllowsBlueBack = Empty
        .AllowsCoverPage = Empty
        .BlueBackBoilerplateFile = Empty
        .HasCoverPageSection = Empty
        .UseBoilerplateFooterSetup = Empty
        .UnderlineDocumentTitle = Empty
        .AllowCoCounsel = Empty
        .AllowCrossAction = Empty
        .FooterCaseNumberSeparator = Empty
        .AllowsFontChange = Empty
        .IsMultiParty = Empty
        .NormalStyleLineSpacingRule = mpStyleValueUndefined
        .NormalStyleLineSpacing = mpStyleValueUndefined
        .BodyTextStyleLineSpacingRule = mpStyleValueUndefined
        .BodyTextStyleLineSpacing = mpStyleValueUndefined
        .BodyTextStyleSpaceAfter = mpStyleValueUndefined
        .BodyTextStyleFirstLineIndent = mpStyleValueUndefined
        .DefaultNumberingScheme = Empty
        .NoticeAttorney = Empty
        
        On Error Resume Next
        .BoilerplateFile = m_oRS!fldBoilerplateFile
        .FirstNumberedPage = m_oRS!fldFirstNumberedPage
        .FirstPageNumber = m_oRS!fldFirstPageNumber
        .UseBoilerplateFooterSetup = m_oRS!fldUseBoilerplateFooterSetup
        .ID = m_oRS!fldID
        .Level0 = m_oRS!fldLevel0
        .Level1 = m_oRS!fldLevel1
        .Level2 = m_oRS!fldLevel2
        .Level3 = m_oRS!fldLevel3
        .DefaultCaptionType = m_oRS!fldDefCaptionType
        .DefaultCaptionType2 = m_oRS!fldDefCaptionType2
        .DefaultCounselType = m_oRS!fldDefCounselType
        .DefaultPleadingPaperType = m_oRS!fldDefPleadingPaperType
        .DefaultSignatureType = m_oRS!fldDefSignatureType
        .CourtTitle = m_oRS!fldCourtTitle
        .CourtTitle1 = m_oRS!fldCourtTitle1
        .CourtTitle2 = m_oRS!fldCourtTitle2
        .CourtTitle3 = m_oRS!fldCourtTitle3
        .RequiresFooter = m_oRS!fldRequireFooterText
        .RequiresDocTitle = m_oRS!fldRequireDocTitle
        .FooterTextMaxChars = m_oRS!fldFooterTextMaxChars
        .FooterTextIncludesCaseNo = m_oRS!fldFooterTextIncludesCaseNo
        .AllowsBlueBack = m_oRS!fldAllowsBlueBack
        .AllowsCoverPage = m_oRS!fldAllowsCoverPage
        .CoverPageBoilerplateFile = m_oRS!fldCoverPageBoilerplateFile
        .BlueBackBoilerplateFile = m_oRS!fldBlueBackBoilerplateFile
        .HasCoverPageSection = m_oRS!fldHasCoverPageSection
        .UnderlineDocumentTitle = m_oRS!fldUnderlineDocumentTitle
        .AllowCoCounsel = m_oRS!fldAllowCoCounsel
        .AllowCrossAction = m_oRS!fldAllowCrossAction
        .FooterCaseNumberSeparator = m_oRS!fldFooterCaseNumberSeparator
        .AllowsFontChange = m_oRS!fldAllowsFontChange
        .IsMultiParty = m_oRS!fldIsMultiParty
        .NormalStyleLineSpacingRule = m_oRS!fldNormalStyleLineSpacingRule
        .NormalStyleLineSpacing = m_oRS!fldNormalStyleLineSpacing
        .BodyTextStyleLineSpacingRule = m_oRS!fldBodyTextStyleLineSpacingRule
        .BodyTextStyleLineSpacing = m_oRS!fldBodyTextStyleLineSpacing
        .BodyTextStyleSpaceAfter = m_oRS!fldBodyTextStyleSpaceAfter
        .BodyTextStyleFirstLineIndent = m_oRS!fldBodyTextStyleFirstLineIndent
        .DefaultNumberingScheme = m_oRS!fldDefaultNumberingScheme
        .NoticeAttorney = m_oRS!fldNoticeAttorney
    End With
End Sub

Private Sub Class_Initialize()
    Set m_oType = New mpDB.CPleadingDef
    Refresh
End Sub
