VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLists"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CLists Collection Class
'   created 11/29/98 by Daniel Fisherman-
'   momshead@earthlink.net

'   Contains properties and methods that manage the
'   collection of CLists

'   Member of CApp
'   Container for CList
'**********************************************************

'**********************************************************
Private m_colLists As Collection
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Friend Property Get Parent() As mpDB.CApp
    Set Parent = Application
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Add(ByVal xName As String, _
                    Optional ByVal xSQL As String, _
                    Optional ByVal xFilterField As String, _
                    Optional ByVal xFilterOp As Integer, _
                    Optional ByVal xFilterValue As String, _
                    Optional ByVal xSortField) As mpDB.CList
'adds a list to the collection -
'xFilterField and xFilterOpValue
'are ignored if xSQL is empty
    Dim oList As CList
    
    On Error GoTo ProcError
    Set oList = New mpDB.CList
    
    On Error Resume Next
    With oList
        .Name = xName
        If xSQL <> "" Then
            .SQL = xSQL
            .FilterField = xFilterField
            .FilterValue = xFilterValue
            .FilterOperator = xFilterOp
            .SortField = xSortField
            .IsDBList = xSQL <> Empty
        End If
    End With
    m_colLists.Add oList, oList.Name
    Set Add = oList
    Exit Function
ProcError:
    RaiseError "mpDB.CLists.Add"
    Exit Function
End Function

Public Sub Save(ByVal oList As mpDB.CList)
    Dim xName As String
    
    On Error GoTo ProcError
    
    'can't save a list that is
    'not a db list - raise error
    If oList.SQL = "" Then
        Err.Raise mpError.mpError_InvalidList, , _
            "Can't save to the database a list that has an empty sql property."
    End If
    
    'get name -  check for existence
    xName = oList.Name
    
    'if it exists, update, else add new
    If Me.Exists(xName) Then
        UpdateList oList
    Else
        AddListToDB oList
    End If
    Exit Sub
ProcError:
    RaiseError "mpDB.CLists.Save"
    Exit Sub
End Sub

Private Sub AddListToDB(oList As mpDB.CList)
    Dim xSQL As String
    Dim oDB As DAO.Database
    
    On Error GoTo ProcError
    Set oDB = g_App.PublicDB
    
    If Me.Exists(oList.Name) Then
        'list already exists - we can't add to db - alert
        Err.Raise mpError.mpError_InvalidList, , _
            "A list with the name '" & oList.Name & _
            "' already exists."
    End If

    With oList
        xSQL = "INSERT INTO tblCustomLists (fldListName,fldSQL" & _
            IIf(.FilterField <> Empty, ",fldFilterField", "") & _
            IIf(.SortField <> Empty, ",fldSortField)", ")") & _
            " VALUES('" & .Name & "','" & .SQL & "'" & _
            IIf(.FilterField <> Empty, ",'" & .FilterField & "'", "") & _
            IIf(.SortField <> Empty, ",'" & .SortField & "')", ")")
        oDB.Execute xSQL
    
        If oDB.RecordsAffected = 0 Then
            'something is wrong
            Err.Raise mpError.mpError_InvalidParameter, , _
                "Could not save updated list '" & .Name & "'." & _
                "The sql statement '" & xSQL & "' failed."
        End If
    End With
    
    Exit Sub
ProcError:
    RaiseError "mpDB.CLists.AddListToDB"
    Exit Sub
End Sub

Private Sub UpdateList(oList As mpDB.CList)
    Dim xSQL As String
    Dim oDB As DAO.Database
    
    On Error GoTo ProcError
    
    Set oDB = g_App.PublicDB
    
    With oList
        'try updating record in custom list table -
        'it might not be there, though
            xSQL = "UPDATE tblCustomLists SET " & _
                "fldSQL='" & .SQL & _
                "',fldFilterField=" & IIf(.FilterField <> Empty, "'" & .FilterField & "'", "NULL") & _
                ",fldSortField=" & IIf(.SortField <> Empty, "'" & .SortField & "'", "NULL") & _
                " WHERE fldListName='" & .Name & "'"
        oDB.Execute xSQL
        
        If oDB.RecordsAffected = 0 Then
            'record is not in the custom lists table-
            'try the built in lists table
            xSQL = "UPDATE tblLists SET " & _
                "fldSQL='" & .SQL & _
                "',fldFilterField=" & IIf(.FilterField <> Empty, "'" & .FilterField & "'", "NULL") & _
                ",fldSortField=" & IIf(.SortField <> Empty, "'" & .SortField & "'", "NULL") & _
                " WHERE fldListName='" & .Name & "'"
                
            oDB.Execute xSQL
            
            If oDB.RecordsAffected = 0 Then
                'something is wrong
                Err.Raise mpError.mpError_InvalidParameter, , _
                    "Could not save updated list '" & .Name & "'." & _
                    "The sql statement '" & xSQL & "' failed."
            End If
        End If
    End With
    
    Exit Sub
ProcError:
    RaiseError "mpDB.CLists.UpdateList"
    Exit Sub
End Sub

'Public Sub CreateTable(ByVal xName As String, ByVal xDescriptionColumnName As String, Optional ByVal xIDColumnName As String)
''Creates a one column or two column table that could
''be used as the storage for a list
'    Dim xSQL As String
'
'    On Error GoTo ProcError
'    If xIDColumnName = Empty Then
'        'create a one column table
'        xSQL = "CREATE TABLE tbl" & xName & " (" & xDescriptionColumnName
'
'    Else
'        'create a two column table with primary key on col 2
'        xSQL = "CREATE TABLE tbl" & xName & " (" & xDescriptionColumnName & _
'                " CHAR NOT NULL," & xIDColumnName
'    End If
'
'    'add primary key on last column
'    xSQL = xSQL & " CHAR NOT NULL CONSTRAINT PrimaryKey PRIMARY KEY)"
    
Public Sub CreateTable(ByVal xName As String, ByVal xDescriptionColumnName As String, Optional ByVal xIDColumnName As String)
    'Creates a one column or two column table that could
    'be used as the storage for a list
    Dim xSQL As String

    On Error GoTo ProcError
    If xIDColumnName = Empty Then
    'create a one column table
    xSQL = "CREATE TABLE tbl" & xName & " (" & xDescriptionColumnName
    
    Else
    'create a two column table with primary key on col 2
    xSQL = "CREATE TABLE tbl" & xName & " (" & xDescriptionColumnName & _
    " VARCHAR NOT NULL," & xIDColumnName
    End If
    
    'add primary key on last column
    xSQL = xSQL & " VARCHAR NOT NULL CONSTRAINT PrimaryKey PRIMARY KEY)"
    
    g_App.PublicDB.Execute xSQL
    Exit Sub
ProcError:
    RaiseError "mpDB.CLists.CreateTable"
    Exit Sub
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = m_colLists.Count
End Function

Public Function Exists(ByVal xName As String) As Boolean
    Dim xSQL As String
    Dim oRS As DAO.Recordset
    
    On Error GoTo ProcError
    If xName = Empty Then
        Exit Function
    End If
    
    xSQL = "SELECT fldListName from tblCustomLists WHERE fldListName = '" & _
        xName & "' UNION SELECT fldListName FROM tblLists WHERE fldListName = '" & _
        xName & "'"

    Set oRS = g_App.PublicDB.OpenRecordset(xSQL, dbOpenForwardOnly)
    Exists = (oRS.RecordCount <> 0)
    Exit Function
ProcError:
    RaiseError "mpDB.CLists.Exists"
    Exit Function
End Function

Public Function Item(ByVal vID As Variant) As CList
Attribute Item.VB_UserMemId = 0
    Dim xDesc As String
    
    On Error Resume Next
    Set Item = m_colLists.Item(vID)
    On Error GoTo ProcError
    If Item Is Nothing Then
        xDesc = "'" & vID & "' is an invalid list.  Please check tblLists/tblCustomLists in mpPublic.mdb."
        Err.Raise mpError_InvalidMember
    End If
    Exit Function
ProcError:
    RaiseError "mpDB.CLists.Item", xDesc
    Exit Function
End Function

Friend Sub Refresh()
' adds all lists from the MacPac backend to the collection of lists
    Dim rs As DAO.Recordset
    Dim xSQL As String
    Dim oList As mpDB.CList
    
    On Error GoTo ProcError
    
    Set m_colLists = New Collection
    
'   cycle through all records in tblLists that should be
'   added at initialization and create list
    xSQL = "SELECT * FROM tblLists UNION ALL " & _
           "SELECT * FROM tblCustomLists " & _
           "ORDER BY fldListName"
    
    Set rs = Application.PublicDB.OpenRecordset(xSQL)
    With rs
        If Not (.BOF And .EOF) Then
            .MoveFirst
            While Not .EOF
                Set oList = Me.Add(rs!fldListName, _
                                   rs!fldSQL, _
                                   xNullToString(rs!fldFilterField), , , _
                                   xNullToString(rs!fldSortField))
                oList.IsDBList = True
                .MoveNext
            Wend
        End If
        .Close
    End With
    
    Exit Sub
    
ProcError:
    RaiseError "mpDB.CLists.Refresh"
    Exit Sub
End Sub

'**********************************************************
'   Internal procedures
'**********************************************************
Private Sub Class_Initialize()
    Set m_colLists = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_colLists = Nothing
End Sub


