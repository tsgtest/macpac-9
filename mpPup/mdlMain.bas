Attribute VB_Name = "mdlMain"
Public Enum Errors
    Err_MissingDB = vbError + 512 + 1
    Err_TableDeleteFailed = vbError + 512 + 2
    Err_QueryExecutionFailed = vbError + 512 + 3
End Enum

Const SPACER As String = "     "
Private lRet As Long

Public Sub Main()
    Execute
End Sub

Public Function Execute() As Long
    Dim oDB As DAO.Database
    Dim oTD As DAO.TableDef
    Dim lErr As Long
    Dim xErrDesc As String
    Dim iChannel As Integer
    Dim xPath As String
    
    On Error GoTo ProcError
'   create log
    iChannel = FreeFile()
    Open App.Path & "\" & "mpPup.log" For Append As #iChannel
    
'   write start time
    Print #iChannel, vbCrLf & Now & SPACER & "Start"
    
    xPath = App.Path & "\mpPeople.mdb"
    
    If Dir(xPath) = Empty Then
        Print #iChannel, Now & SPACER & "Could not connect to " & App.Path & "\mpPeople.mdb" & ". " & _
            "Ensure that it is located in the same directory as mpPup.exe."
        Exit Function
    End If

    'backup db
    FileCopy xPath, xPath & ".bak"
    Print #iChannel, Now & SPACER & "Backed up database mpPeople.mdb to mpPeople.mdb.bak."

'   open people.mdb
    On Error Resume Next
    Set oDB = DBEngine(0).OpenDatabase(xPath)
    On Error GoTo ProcError
    
    If oDB Is Nothing Then
        Err.Raise Err_MissingDB, , "Could not connect to " & App.Path & "\mpPeople.mdb" & ". " & _
            "Ensure that it is located in the same directory as mpPup.exe."
    End If
    
    With oDB
'       delete hub table - we'll recreate it below
        On Error Resume Next
        .TableDefs.Delete "tblPeopleHub"
        Set oTD = .TableDefs("tblPeopleHub")
        On Error GoTo ProcError
        If Not oTD Is Nothing Then
            Err.Raise Err_TableDeleteFailed, , "Could not delete tblPeopleHub."
        End If

'       create new hub table
        Err.Clear
        On Error Resume Next
        .Execute .QueryDefs("qryGetHubPeople").SQL
        If Err.Number > 0 Then
            lErr = Err.Number
            xErrDesc = Err.Description
            On Error GoTo ProcError
            Err.Raise Err_QueryExecutionFailed, , _
                "Could not execute qryGetHubPeople.  " & _
                "#" & lErr & "  " & xErrDesc
        End If
        Print #iChannel, Now & SPACER & "Updated table 'tblPeopleHub'"
        
        On Error Resume Next
        Err.Clear
        .Execute .QueryDefs("qryAddNewPeopleMPItems").SQL
        If Err.Number > 0 Then
            lErr = Err.Number
            xErrDesc = Err.Description
            On Error GoTo ProcError
            Err.Raise Err_QueryExecutionFailed, , _
                "Could not execute qryAddNewPeopleMPItems.  " & _
                "#" & lErr & "  " & xErrDesc
        End If
        Print #iChannel, Now & SPACER & "Added missing record IDs to 'tblPeopleMP'"
        
'       update licenses if specified in command line
        If InStr(UCase(Command()), UCase("-a")) > 0 Then
            On Error Resume Next
           
            Err.Clear
           .Execute .QueryDefs("qryDeleteAttyLicenseRecords").SQL
            If Err.Number > 0 Then
                lErr = Err.Number
                xErrDesc = Err.Description
                On Error GoTo ProcError
                Err.Raise Err_QueryExecutionFailed, , _
                    "Could not execute qryDeleteAttyLicenseRecords.  " & _
                    "#" & lErr & "  " & xErrDesc
            End If
            
            On Error Resume Next
            
            Err.Clear
            .Execute .QueryDefs("qryGetHubPeopleLicenses").SQL
            On Error GoTo ProcError
            If Err.Number > 0 Then
                lErr = Err.Number
                xErrDesc = Err.Description
                Err.Raise Err_QueryExecutionFailed, , _
                    "Could not execute qryGetHubPeopleLicenses.  " & _
                    "#" & lErr & "  " & xErrDesc
            End If
            Print #iChannel, Now & SPACER & "Updated attorney licenses"
        End If
        
        'delete backup - if we got here, we were successful
        Kill xPath & ".bak"
        Print #iChannel, Now & SPACER & "Delete backup database 'mpPeople.mdb.bak'"
        
        'compact db
        oDB.Close
        Set oDB = Nothing
        
        Err.Clear
        DBEngine.CompactDatabase xPath, xPath & ".new"
        If Err.Number = 0 Then
            Print #iChannel, Now & SPACER & "Compacted mpPeople.mdb"
            Kill xPath
            FileCopy xPath & ".new", xPath
            Kill xPath & ".new"
        Else
            Print #iChannel, Now & SPACER & "WARNING: Could not compact mpPeople.mdb."
        End If
        
        Print #iChannel, Now & SPACER & "Update completed successfully."
    End With
    Exit Function
ProcError:
    If Not (oDB Is Nothing) Then
        oDB.Close
    End If
    Set oDB = Nothing
    Print #iChannel, Now & SPACER & "ERROR: " & Err.Number & " - " & Err.Description
    
    'restore backups - something's wrong
    FileCopy xPath & ".bak", xPath
    Kill xPath & ".bak"
    Print #iChannel, Now & SPACER & "Restored original copy of database 'mpPeople.mdb'"
    Print #iChannel, Now & SPACER & "END"
End Function




Public Function ExecuteOLD() As Long
    Dim oDB As DAO.Database
    Dim oTD As DAO.TableDef
    Dim lErr As Long
    Dim xErrDesc As String
    Dim iChannel As Integer
    Dim xPath As String
    
    On Error GoTo ProcError
    
'   create log
    iChannel = FreeFile()
    Open App.Path & "\" & "mpPup.log" For Append As #iChannel
    
'   write start time
    Print #iChannel, vbCrLf & Now & SPACER & "Start"
    
'   open people.mdb
    xPath = App.Path & "\mpPeople.mdb"
    On Error Resume Next
    Set oDB = DBEngine(0).OpenDatabase(xPath)
    On Error GoTo ProcError
    
    If oDB Is Nothing Then
        Err.Raise Err_MissingDB, , "Could not connect to " & App.Path & "\mpPeople.mdb" & ". " & _
            "Ensure that it is located in the same directory as mpPup.exe."
    End If
    
    With oDB
''       back up hub table
'        On Error Resume Next
'        Set oTD = .TableDefs("tblPeopleHubBak")
'        On Error GoTo ProcError
'
'        'delete old version if necessary
'        If Not (oTD Is Nothing) Then
'            .TableDefs.Delete "tblPeopleHubBak"
'        End If
'
'        'create new version
'        .Execute "SELECT tblPeopleHub.* INTO tblPeopleHubBak FROM tblPeopleHub"
'
'        If Err.Number > 0 Then
'            'exit - we need backups before we begin
'            Err.Raise Err_QueryExecutionFailed, _
'                "mpPup.mdlMain.Execute", "Could not create backup of tblPeopleHub."
'        End If
'
''       back up mp table
'        On Error Resume Next
'        Set oTD = .TableDefs("tblPeopleMPBak")
'        On Error GoTo ProcError
'
'        'delete old version if necessary
'        If Not (oTD Is Nothing) Then
'            .TableDefs.Delete "tblPeopleMPBak"
'        End If
'
'        'create new version
'        .Execute "SELECT tblPeopleMP.* INTO tblPeopleMPBak FROM tblPeopleMP"
'
'        If Err.Number > 0 Then
'            'exit - we need backups before we begin
'            Err.Raise Err_QueryExecutionFailed, _
'                "mpPup.mdlMain.Execute", "Could not create backup of tblPeopleMP."
'        End If
'
'       delete hub table - we'll recreate it below
        On Error Resume Next
        .TableDefs.Delete "tblPeopleHub"
        Set oTD = .TableDefs("tblPeopleHub")
        On Error GoTo ProcError
        If Not oTD Is Nothing Then
            Err.Raise Err_TableDeleteFailed, , "Could not delete tblPeopleHub."
        End If

'       create new hub table
        Err.Clear
        On Error Resume Next
        .Execute .QueryDefs("qryGetHubPeople").SQL
        If Err.Number > 0 Then
            lErr = Err.Number
            xErrDesc = Err.Description
            On Error GoTo ProcError
            Err.Raise Err_QueryExecutionFailed, , _
                "Could not execute qryGetHubPeople.  " & _
                "#" & lErr & "  " & xErrDesc
        End If
        
        
        On Error Resume Next
        .Execute .QueryDefs("qryAddNewPeopleMPItems").SQL
        If Err.Number > 0 Then
            lErr = Err.Number
            xErrDesc = Err.Description
            On Error GoTo ProcError
            Err.Raise Err_QueryExecutionFailed, , _
                "Could not execute qryAddNewPeopleMPItems.  " & _
                "#" & lErr & "  " & xErrDesc
        End If
        
'       update licenses if specified in command line
        If InStr(UCase(Command()), UCase("-a")) > 0 Then
            On Error Resume Next
           
           .Execute .QueryDefs("qryDeleteAttyLicenseRecords").SQL
            If Err.Number > 0 Then
                lErr = Err.Number
                xErrDesc = Err.Description
                On Error GoTo ProcError
                Err.Raise Err_QueryExecutionFailed, , _
                    "Could not execute qryDeleteAttyLicenseRecords.  " & _
                    "#" & lErr & "  " & xErrDesc
            End If
            
            On Error Resume Next
            
            .Execute .QueryDefs("qryGetHubPeopleLicenses").SQL
            On Error GoTo ProcError
            If Err.Number > 0 Then
                lErr = Err.Number
                xErrDesc = Err.Description
                Err.Raise Err_QueryExecutionFailed, , _
                    "Could not execute qryGetHubPeopleLicenses.  " & _
                    "#" & lErr & "  " & xErrDesc
            End If
        End If
        
'       mark last update
        On Error Resume Next
'        Me.LastUpdate = Now
        On Error GoTo ProcError
        
        Print #iChannel, Now & SPACER & "Update completed successfully."
    End With
    
    Exit Function
ProcError:
    'restore backups

''   restore hub table
'    With oDB
'        'create new version
'        On Error Resume Next
'        .Execute "SELECT tblPeopleHubBak.* INTO tblPeopleHub FROM tblPeopleHubBak"
'
'        If Err.Number > 0 Then
'            'try deleting the existing table
'            .TableDefs.Delete "tblPeopleHub"
'        End If
'
'        'if we got here, we've restored hub - delete the backup
'        .Execute "DROP TABLE tblPeopleHubBak"
'
'        'restore mp table
'        On Error Resume Next
'        Set oTD = .TableDefs("tblPeopleMP")
'        On Error GoTo ProcError
'
'        'delete old version if necessary
'        On Error Resume Next
'        .TableDefs.Delete "tblPeopleMP"
'
'        'create new version
'        .Execute "SELECT tblPeopleMPBak.* INTO tblPeopleMP FROM tblPeopleMPBak"
'    End With
'
    Print #iChannel, Now & SPACER & "ERROR: " & Err.Number & " - " & Err.Description
    Print #iChannel, Now & SPACER & "END"
End Function





