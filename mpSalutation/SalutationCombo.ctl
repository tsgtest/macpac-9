VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.UserControl SalutationCombo 
   BackStyle       =   0  'Transparent
   ClientHeight    =   528
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3588
   LockControls    =   -1  'True
   ScaleHeight     =   528
   ScaleWidth      =   3588
   Begin TrueDBList60.TDBCombo cmbSalutation 
      Height          =   600
      Left            =   0
      OleObjectBlob   =   "SalutationCombo.ctx":0000
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   3585
   End
   Begin VB.TextBox txtSalutation 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3555
   End
End
Attribute VB_Name = "SalutationCombo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
'******************************************************************
'MacPac SalutationCombo Control
'Created 1/3/02 by Daniel Fisherman

'Properties:
'Show Dropdown
'    boolean - shows either the dropdown or textbox
'Text
'   string - gets/sets the text in the control
'Dropdown Value
'   string - gets/sets the bound text of the dropdown
'Height
'   single - gets/sets the height of the control
'Width
'   single - gets/sets the width of the control
'List
'   string, get/sets the dropdown list
'ListRows
'   integer - gets/sets the number of rows shown by the dropdown
'Appearance
'   enum - gets/sets 3D appearance of control
'Schema
'   variant - gets/sets the array that provides the schema for list items
'DefaultListIndex
'   integer - gets/sets the index of the default dropdown selection
'ParsedContactData - gets/sets the parsed data used by control
'to generate a salutation list as a pipe delimited string.
'Used for MacPac reuse to get data for subsequent document creation
'
'Methods:
'AddContacts - appends each contact in the supplied mpCI.Contacts
'   collection to the existing mpCI.Contacts collection for the control
'RefreshList - refreshes the list to display the latest parsed contact data
'ClearList - clears the salutation list and source array
'******************************************************************

Private m_iSelectionType As salSelectionType
Private m_bAllowDropdown As Boolean
Private m_xText As String
Private m_xDropdownValue As String
Private m_xList As String
Private m_xSchema As Variant
Private m_xMultiRecipientSeparator As String
Private m_bMultiRecipientFormats As Boolean
Private m_iListRows As Integer
Private m_iDefaultListIndex As Integer
Private m_oDataArr As XArray

Enum salAppearance
    salAppearance_Flat = 0
    salAppearance_3D = 1
End Enum

Enum salSelectionType
    salSelectionType_Content = 0
    salSelectionType_EndOfSalutation = 1
    salSelectionType_StartOfSalutation = 2
End Enum

Enum salErrors
    salError_InvalidList = vbError + 513
    salError_InvalidSchematic
    salError_InvalidDefaultListIndex
    salError_InvalidCIConfiguration
    salError_ContactsCollectionIsNothing
    salError_DropdownNotAllowed
End Enum

Public Property Set Font(oNew As StdFont)
    Set UserControl.cmbSalutation.Font = oNew
    Set UserControl.txtSalutation.Font = oNew
End Property

Public Property Get Font() As StdFont
    Set Font = UserControl.cmbSalutation.Font
End Property

Public Property Let AllowDropdown(ByVal bNew As Boolean)
Attribute AllowDropdown.VB_Description = "Sets/Returns whether the dropdown can be shown  in the control. If the dropdown is not available, only a text box will display."
'Sets whether the dropdown can be shown in the control.
'If the dropdown is not available, only a text box will display.
    On Error GoTo ProcError
    
    m_bAllowDropdown = bNew
    PropertyChanged "AllowDropdown"
    
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.AllowDropdown"
    Exit Property
End Property

Public Property Get AllowDropdown() As Boolean
'Returns whether the dropdown can be shown in the control.
    On Error GoTo ProcError
    AllowDropdown = m_bAllowDropdown
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.AllowDropdown"
    Exit Property
End Property

Public Property Let SelectionType(ByVal bNew As salSelectionType)
Attribute SelectionType.VB_Description = "Sets/Returns if the last character or the character before the trailing ':' or ',' will be selected when the control gets focus."
'Sets if the last character or the character before the
'trailing ':' or ',' will be selected when the control gets focus.
    On Error GoTo ProcError
    
    m_iSelectionType = bNew
    PropertyChanged "SelectSalutationEnd"
    
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.SelectionType"
    Exit Property
End Property

Public Property Get SelectionType() As salSelectionType
    On Error GoTo ProcError
    SelectionType = m_iSelectionType
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.SelectionType"
    Exit Property
End Property

Public Property Let Text(ByVal xNew As String)
Attribute Text.VB_Description = "Sets/Returns the salutation  text of the control."
    On Error GoTo ProcError
'   set text for both combo and text box
    UserControl.txtSalutation.Text = xNew
    UserControl.cmbSalutation.Text = xNew
    
    PropertyChanged "Text"
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.Text"
    Exit Property
End Property

Public Property Get Text() As String
    On Error GoTo ProcError
    Text = UserControl.txtSalutation.Text
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.Text"
    Exit Property
End Property

Public Property Let List(ByVal xNew As String)
Attribute List.VB_Description = "Sets/Returns the actual list of dropdown items as a delimited string.  Delimiter is '|'."
'sets the list to the items supplied as a pipe delimited string
    Dim iCurListIndex As Integer
    
    On Error GoTo ProcError
    
    'show dropdown only if list has items and it's allowed
    ShowDropdown (xNew <> Empty) And Me.AllowDropdown
        
    If Not Me.AllowDropdown Then
        Exit Property
    End If
    
    With UserControl.cmbSalutation
'       get current list selection
        iCurListIndex = Me.ListIndex
    
'       set list
        If xNew <> Empty Then
            .Array = mpbase2.xarStringToxArray(xNew, 1, "|")
            .ReBind
'           clear selection, then select list index specified above
            .SelectedItem = -1
            If iCurListIndex = -1 Then
                .SelectedItem = Me.DefaultListIndex
            Else
                .SelectedItem = iCurListIndex
            End If
            
            ResizeTDBCombo UserControl.cmbSalutation, Me.ListRows
            cmbSalutation_Change
        End If
    End With
    
    m_xList = xNew
    PropertyChanged "List"
    
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.List"
    Exit Property
End Property

Public Property Get ParsedContactData() As String
Attribute ParsedContactData.VB_Description = "gets/sets the parsed data used by control\r\nto generate a salutation listas a pipe delimited string.  Used for MacPac reuse to get data for subsequent document creation"
'gets the data used to generate the salutation list as a
'pipe delimited string.  use with MacPac reuse to store
'data for subsequent document creation
    Dim iNumRows As Integer
    Dim iSelRow As Integer
    Dim oArr As XArray
    
    If Not UserControl.cmbSalutation.Visible Then
        Exit Property
    End If
    
    On Error Resume Next
    iNumRows = m_oDataArr.Count(1)
    On Error GoTo ProcError
    
    If iNumRows > 0 Then
        On Error Resume Next
        iSelRow = UserControl.cmbSalutation.SelectedItem
        On Error GoTo ProcError
        ParsedContactData = iSelRow & "|" & m_oDataArr.Count(1) & "|" & mpbase2.XArrayToString(m_oDataArr)
    End If
    Exit Property
ProcError:
    RaiseError "mpSal.SalutationCombo.ParsedContactData"
    Exit Property
End Property

Public Property Let ParsedContactData(xNew As String)
'sets the parsed data used by control
'to generate a salutation list - data must be in the form
'X|Y|Prefix|FirstName|MiddleName|LastName|Suffix|Salutation|Custom1|Custom2 where
'X is the selected row, Y is the number of rows the array should have, and custom1, 2 are the
'names of custom fields for which there are schematic tokens.  The first
'row must be this heading.

    Dim iPos As Integer
    Dim iNumRows As Integer
    Dim xData As String
    Dim iNumCols As Integer
    Dim iNumSep As Integer
    Dim iSelRow As Integer
    
    On Error GoTo ProcError
    
    If xNew = Empty Then
        Exit Property
    End If
    
    'get selected row
    iPos = InStr(xNew, "|")
    iSelRow = Left(xNew, iPos - 1)
    
    'get number of rows
    xData = Mid(xNew, iPos + 1)
    iPos = InStr(xData, "|")
    iNumRows = Left(xData, iPos - 1)

    'get array of salutations
    xData = Mid(xData, iPos + 1)
    iNumSep = mpbase2.lCountChrs(xData, "|")
    iNumCols = (iNumSep + 1) / iNumRows
    
    Set m_oDataArr = mpbase2.xarStringToxArray(xData, iNumCols)
    
    '#3860 - 9.6.2
    If Not (UCase(xData) Like "*|GOESBY|*") Then
        Dim i As Integer
        m_oDataArr.Insert 2, 6
        m_oDataArr(0, 6) = "GoesBy"
    End If
    
    RefreshList iSelRow, True
    Exit Property
ProcError:
    RaiseError "mpSal.SalutationCombo.ParsedContactData"
    Exit Property
End Property

Public Property Get List() As String
'returns as a string the list that is being displayed in the dropdown
    On Error GoTo ProcError
    List = m_xList
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.List"
    Exit Property
End Property

Public Property Let Schema(ByVal xNew As String)
Attribute Schema.VB_Description = "Sets/Returns the token strings used to generate the actual salutation list displayed in the dropdown"
'sets the schematics used to create a list of salutations -
'the schematics contain tokens that are filled in with
'data from CI.
    Dim iNumLBrackets As Integer
    Dim iNumRBrackets As Integer
    
    On Error GoTo ProcError
    
'   check for equal numbers of < and >
    iNumLBrackets = mpbase2.lCountChrs(xNew, "<")
    iNumRBrackets = mpbase2.lCountChrs(xNew, ">")
    
    If iNumLBrackets > iNumRBrackets Then
'       not equal - alert and exit
        Err.Raise salError_InvalidSchematic, , _
            "Could not set schema property.  The number of '<' " & _
            "exceeds the number of '>' in '" & xNew & "'."
    ElseIf iNumLBrackets < iNumRBrackets Then
'       not equal - alert and exit
        Err.Raise salError_InvalidSchematic, , _
            "Could not set schema property.  The number of '>' " & _
            "exceeds the number of '<' in '" & xNew & "'."
    End If
    
    m_xSchema = xNew
    PropertyChanged "Schema"
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.Schema"
    Exit Property
End Property

Public Property Get Schema() As String
    On Error GoTo ProcError
    Schema = m_xSchema
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.Schema"
    Exit Property
End Property

Public Property Let MultiRecipientSeparator(ByVal xNew As String)
    m_xMultiRecipientSeparator = xNew
    PropertyChanged "MultiRecipientSeparator"
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.MultiRecipientSeparator"
    Exit Property
End Property

Public Property Get MultiRecipientSeparator() As String
    On Error GoTo ProcError
    MultiRecipientSeparator = m_xMultiRecipientSeparator
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.MultiRecipientSeparator"
    Exit Property
End Property

Public Property Let MultiRecipientFormats(ByVal bNew As Boolean)
    m_bMultiRecipientFormats = bNew
    PropertyChanged "MultiRecipientFormats"
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.MultiRecipientFormats"
    Exit Property
End Property

Public Property Get MultiRecipientFormats() As Boolean
    On Error GoTo ProcError
    MultiRecipientFormats = m_bMultiRecipientFormats
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.MultiRecipientFormats"
    Exit Property
End Property


Public Property Let DefaultListIndex(ByVal iNew As Integer)
Attribute DefaultListIndex.VB_Description = "Sets/Returns the list index for the default selection in the dropdown."
'sets the default list item selected
'in the dropdown when it is populated
    Dim iNumRows As Integer
    
    On Error GoTo ProcError
    
'   get number of rows in list
    With UserControl.cmbSalutation
        If Not (.Array Is Nothing) Then
            iNumRows = .Array.Count(1)
        End If
    End With
    
'   raise error if value is less than -1
    If iNew < -1 Then
        Err.Raise salError_InvalidDefaultListIndex, , _
            "'" & iNew & "' is an invalid dropdown index."
    End If
    
    m_iDefaultListIndex = iNew
    
    PropertyChanged "DefaultListIndex"
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.DefaultListIndex"
    Exit Property
End Property

Public Property Get DefaultListIndex() As Integer
    On Error GoTo ProcError
    DefaultListIndex = m_iDefaultListIndex
    Exit Property
ProcError:
    RaiseError "mpSal.SalutationCombo.DefaultListIndex"
    Exit Property
End Property

Public Property Get ListIndex() As Integer
'returns the current list index
    On Error GoTo ProcError
    With UserControl.cmbSalutation
        If IsNull(.SelectedItem) Or (UserControl.cmbSalutation.Array Is Nothing) Then
            ListIndex = -1
        Else
            ListIndex = .SelectedItem
        End If
    End With
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.ListIndex"
    Exit Property
End Property

Friend Property Let ListRows(ByVal iNew As Integer)
'sets the maximum number of rows that the dropdown w
    On Error GoTo ProcError
    m_iListRows = iNew
    
    ResizeTDBCombo UserControl.cmbSalutation, iNew
    PropertyChanged "ListRows"
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.ListRows"
    Exit Property
End Property

Public Property Get ListRows() As Integer
Attribute ListRows.VB_Description = "Sets/Returns the number of rows displayed by the dropdown"
    On Error GoTo ProcError
    ListRows = m_iListRows
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.ListRows"
    Exit Property
End Property

Public Property Let Appearance(ByVal iNew As salAppearance)
Attribute Appearance.VB_Description = "Sets/Returns the 3D appearance of the control"
'sets appearance of control as either 3D or flat
    On Error GoTo ProcError
    With UserControl
        .cmbSalutation.Appearance = iNew
        .txtSalutation.Appearance = iNew
    End With
    
    PropertyChanged "Appearance"
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.Appearance"
    Exit Property
End Property

Public Property Get Appearance() As salAppearance
'Returns the 3D appearance of the control
    On Error GoTo ProcError
    Appearance = UserControl.cmbSalutation.Appearance
    Exit Property
ProcError:
    RaiseError "mpSAL.SalutationCombo.Appearance"
    Exit Property
End Property

Public Sub AddContacts(oContacts As mpCI.Contacts)
'parses the supplied contacts into the contact data array
    On Error GoTo ProcError
    ParseContacts oContacts
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.AddContacts"
    Exit Sub
End Sub

Public Sub AddContacts20(oContacts As CIO.CContacts)
'parses the supplied contacts into the contact data array
    On Error GoTo ProcError
    ParseContacts20 oContacts
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.AddContacts20"
    Exit Sub
End Sub

Public Sub ShowDropdown(bShow As Boolean)
'shows dropdown if specified and allowed - else show textbox
    On Error GoTo ProcError
    
    If bShow And (Not Me.AllowDropdown) Then
        'something's wrong
        Err.Raise salError_DropdownNotAllowed, , _
            "Could not show the salutation dropdown.  The control " & _
            "is configured to be a textbox only."
    End If
    
    With UserControl
        .cmbSalutation.Visible = bShow And Me.AllowDropdown
        .txtSalutation.Visible = Not .cmbSalutation.Visible
    End With
    Exit Sub
ProcError:
    RaiseError "mpSal.SalutationCombo.ShowDropdown"
    Exit Sub
End Sub

Public Sub ClearList()
Attribute ClearList.VB_Description = "clears the salutation list and sets control to a textbox"
'clears the salutation list and source array
    On Error GoTo ProcError
    AssignEmptyArray
    RefreshList
    Exit Sub
ProcError:
    RaiseError "mpSal.SalutationCombo.ClearList"
    Exit Sub
End Sub

Public Sub RefreshList(Optional ByVal iSelectedRow As Integer = 0, _
                       Optional bExisting As Boolean = False)
'populates the dropdown with salutations based on the
'parsed contacts array and the current schema - uses the schema
'defined in tblSalutations if no schema has been set

    Dim xSchema() As String
    Dim xSchematic As String
    Dim xList As String
    Dim i As Integer
    
    On Error GoTo ProcError
    
    If (m_oDataArr.Count(1) < 2) Or (Not Me.AllowDropdown) Then
        'there are no contacts to show or the list is not allowed, exit
        ShowDropdown False
        Exit Sub
    End If
    
    'get currently selected row if
    'no row specified for selection
    If iSelectedRow = 0 And Not IsNull(UserControl.cmbSalutation.SelectedItem) Then
        iSelectedRow = UserControl.cmbSalutation.SelectedItem
    End If
    
    If Me.Schema = Empty Then
'       GetSchema did not produce any schema - alert and exit
        Err.Raise salError_InvalidSchematic, , _
            "Cannot set salutations.  The .schema property must be set first."
    End If
    
    If InStr(Me.Schema, "|") > 0 Then
'       there are more than 1 - convert schema string to array
        mpbase2.xStringToArray Me.Schema, xSchema(), 1, "|"
    
        For i = LBound(xSchema) To UBound(xSchema)
'           get schematic
            xSchematic = xSchema(i, 0)

'           get the salutation based on schematic
            xList = xList & GetSalutation(xSchematic, bExisting) & "|"
        Next i
        
'       trim trailing pipe
        xList = Left(xList, Len(xList) - 1)
    Else
'       only 1 schematic exists
        xList = xList & GetSalutation(Me.Schema, bExisting)
    End If
        
'   set list
    Me.List = xList
    
'   select the specified row
    On Error Resume Next
    UserControl.cmbSalutation.SelectedItem = iSelectedRow
    
'   resize dropdown
    ResizeTDBCombo UserControl.cmbSalutation, Me.ListRows
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.RefreshList"
    Exit Sub
End Sub

Private Function GetSalutationPrefix(ByVal xSchematic As String) As String
'returns the prefix specified by the schematic -
'the prefix is a token enclosed in <<xxx>> that
'begins the schematic
    Dim iPos As Integer
    
    On Error GoTo ProcError
    If Left(xSchematic, 2) = "<<" Then
'       prefix exists - parse
        iPos = InStr(xSchematic, ">>")
        
        If iPos = 0 Then
'           invalid entry - alert
            Err.Raise salError_InvalidSchematic, , _
                "'" & xSchematic & "' is an invalid salutation schematic."
        Else
'           parse prefix, remove <<>>
            GetSalutationPrefix = Mid(Left(xSchematic, iPos - 1), 3)
        End If
    End If
    Exit Function
ProcError:
    RaiseError "mpSAL.SalutationCombo.GetSalutationPrefix"
    Exit Function
End Function

Private Function GetSalutationSuffix(ByVal xSchematic As String) As String
'returns the suffix specified by the schematic -
'the prefix is a token enclosed in <<xxx>> that
'ends the schematic
    Dim iPos As Integer

    On Error GoTo ProcError
    If Right(xSchematic, 2) = ">>" Then
'       prefix exists - parse
        iPos = InStrRev(xSchematic, "<<")
        
        If iPos = 0 Then
'           invalid entry - alert
            Err.Raise salError_InvalidSchematic, , _
                "'" & xSchematic & "' is an invalid salutation schematic."
        Else
'           parse out suffix, remove <<>>
            GetSalutationSuffix = Mid(xSchematic, iPos + 2)
            GetSalutationSuffix = Left(GetSalutationSuffix, Len(GetSalutationSuffix) - 2)
        End If
    End If
    Exit Function
ProcError:
    RaiseError "mpSAL.SalutationCombo.GetSalutationPrefix"
    Exit Function
End Function

Private Function GetSalutation(ByVal xSchematic As String, _
                               Optional bExisting As Boolean = False) As String
'returns a string with the instantiated salutation
    Dim xTemp As String
    Dim xSal As String
    Dim oContact As mpCI.Contact
    Dim xPrefix As String
    Dim xSuffix As String
    Dim xSalForOne As String
    Dim iPos As Integer
    Dim i As Integer
    
'   parse any prefix and suffix - these are not
'   contact prefix and suffix, but the prefix and
'   suffix to the entire salutation string
    xPrefix = GetSalutationPrefix(xSchematic)
    
    If "<<" & xPrefix & ">>" = xSchematic Then
'       the prefix is the entire schematic,
'       the prefix is the salutation
        GetSalutation = xPrefix
        Exit Function
    End If
    
    xSuffix = GetSalutationSuffix(xSchematic)
    
'   remove prefix and suffix from schematic
    xSchematic = Replace(xSchematic, "<<" & xPrefix & ">>", "")
    xSchematic = Replace(xSchematic, "<<" & xSuffix & ">>", "")
    
'   cycle through each row in
'   array, adding to salutation
    On Error GoTo ProcError
    For i = 1 To m_oDataArr.UpperBound(1)
        'get the salutation for the contact
        xSalForOne = GetSalutationForOneContact(i, xSchematic, bExisting)
        
        'add salutation and comma separator to salutation string
        xSal = xSal & ", " & xSalForOne
    Next i
    
'   check for existence of a separator
    iPos = InStrRev(xSal, ", ")
    
    If iPos > 0 Then
        If m_oDataArr.Count(1) - 1 = 2 Then
'           append " and " to last separator
            xSal = Left(xSal, iPos - 1) & " " & Me.MultiRecipientSeparator _
                        & " " & Mid(xSal, iPos + 2)
        ElseIf m_oDataArr.Count(1) - 1 > 2 Then
'           append ", and " to last separator
            If Me.MultiRecipientFormats Then
            
                xTemp = Left(xSal, iPos - 1) & " " & Me.MultiRecipientSeparator _
                        & " " & Mid(xSal, iPos + 2)
                xTemp = Replace(xTemp, "  ", " ")
                xTemp = xPrefix & Trim$(Mid(xTemp, 2)) & xSuffix
                
                xTemp = xTemp & "|" & Replace(xTemp, " " & Me.MultiRecipientSeparator & " ", _
                                              ", " & Me.MultiRecipientSeparator & " ")
                        
                GetSalutation = xTemp
                Exit Function
            Else
                xSal = Left(xSal, iPos - 1) & ", " & Me.MultiRecipientSeparator _
                        & " " & Mid(xSal, iPos + 2)
            End If
        End If
    End If
    
'   clean extra space
    xSal = Replace(xSal, "  ", " ")

'   trim trailing comma and return
    GetSalutation = xPrefix & Trim$(Mid(xSal, 2)) & xSuffix
    Exit Function
ProcError:
    RaiseError "mpSAL.SalutationCombo.GetSalutation"
    Exit Function
End Function

Private Function GetSalutationForOneContact(ByVal iRowIndex As Integer, _
                                            ByVal xSchematic As String, _
                                            Optional bExisting As Boolean = False) As String
    Dim oCustFld As Object
    Dim iPos As Integer
    Dim iPos2 As Integer
    Dim xSal As String
    Dim xToken As String
    
    On Error GoTo ProcError
    'start with schematic only
    xSal = xSchematic
    
'   replace tokens with data
    With m_oDataArr
        xSal = Replace(xSal, "<Prefix>", .Value(iRowIndex, 0), , , vbTextCompare)
        xSal = Replace(xSal, "<FirstName>", .Value(iRowIndex, 1), , , vbTextCompare)
        xSal = Replace(xSal, "<GoesBy>", .Value(iRowIndex, 6), , , vbTextCompare)
        xSal = Replace(xSal, "<LastName>", .Value(iRowIndex, 3), , , vbTextCompare)
        xSal = Replace(xSal, "<MiddleName>", .Value(iRowIndex, 2), , , vbTextCompare)
        xSal = Replace(xSal, "<Suffix>", .Value(iRowIndex, 4), , , vbTextCompare)
        xSal = Replace(xSal, "<Salutation>", .Value(iRowIndex, 5), , , vbTextCompare)
    End With

'   check for remaining tokens
    iPos = InStr(xSal, "<")
    iPos2 = InStr(xSal, ">")

'   additional tokens exist - cycle through
    While iPos > 0 And iPos2 > 0
        If iPos2 < iPos Then
'           invalid entry - alert
            Err.Raise salError_InvalidSchematic, , _
                "'" & xSal & "' is an invalid salutation schematic."
        End If
        
'       get token text - this is custom field name
        xToken = Mid(xSal, iPos + 1, iPos2 - iPos - 1)
        
        On Error Resume Next
        'check if token is a field name
        Dim iLastColIndex As Integer
        Dim i As Integer
        Dim xCustValue As String
        Dim bFound As Boolean
        Dim xTemp As String
        
        bFound = False
        xCustValue = Empty
        
        iLastColIndex = m_oDataArr.UpperBound(2)
        
        'cycle through custom field columns, looking for the
        'column that contains the values for the specified token
        For i = 7 To iLastColIndex
            If UCase$(m_oDataArr.Value(0, i)) = UCase$(xToken) Then
                'column was found for token - get value from row
                'that contains the contact info
                xCustValue = m_oDataArr.Value(iRowIndex, i)
                bFound = True
                Exit For
            End If
        Next i
        
        If (Not bFound) And (Not bExisting) Then
            xTemp = mpbase2.GetMacPacIni("General", "PromptForMissingCustomSalutation")
            If UCase(xTemp) <> "FALSE" Then
'           ci is not retrieving the custom field
'           requested in the salutation schematic
            On Error GoTo ProcError
            Err.Raise salError_InvalidCIConfiguration, , _
                "Contact Integration has not been configured to " & _
                "retrieve the custom field '" & xToken & _
                "' requested by the salutation control.  Please " & _
                "contact your administrator."
            End If
        End If
        
'       substitute value for token
        xSal = Trim$(Replace(xSal, "<" & xToken & ">", xCustValue))

'       check for remaining tokens
        iPos = InStr(xSal, "<")
        iPos2 = InStr(xSal, ">")
    Wend
    GetSalutationForOneContact = xSal
    Exit Function
ProcError:
    RaiseError "mpSal.SalutationCombo.GetSalutationForOneContact"
    Exit Function
End Function

Private Sub CreateParsedContactsArray(oCustFlds As Object)
'creates and returns an xarray that can store parsed contact info
    Dim oCustFld As Object
    Dim i As Integer
    Dim j As Integer

    'create initial array dimensions - 1st row
    'in array will be the colummn headings
    With m_oDataArr
        .ReDim 0, 0, 0, 6
        .Value(0, 0) = "Prefix"
        .Value(0, 1) = "FirstName"
        .Value(0, 2) = "MiddleName"
        .Value(0, 3) = "LastName"
        .Value(0, 4) = "Suffix"
        .Value(0, 5) = "Salutation"
        .Value(0, 6) = "GoesBy"
        
        'cycle through custom fields, getting the
        'number of custom fields that are tokens
        For j = 1 To oCustFlds.Count
            'create space for custom field
            .ReDim 0, 0, 0, .UpperBound(2) + 1
            
            'get custom field
            Set oCustFld = oCustFlds.Item(j)
            
            'set column heading = the custom field name
            .Value(0, .UpperBound(2)) = oCustFld.Name
        Next j
    
'        'cycle and add for a total of 20 custom fields,
'        'this deals with contacts from various backends with
'        'non-matching custom fields
'        For i = 1 To 20 - oCustFlds.Count
'            'create space for custom field
'            .ReDim 0, 0, 0, .UpperBound(2) + 1
'
'            'set column heading = the custom field name
'            .Value(0, .UpperBound(2)) = ""
'        Next i
        
    End With
    Exit Sub
ProcError:
    RaiseError "mpSal.SalutationCombo.CreateParsedContactsArray"
    Exit Sub
End Sub

'#4716
Private Sub ParseContacts(oContacts As mpCI.Contacts)
'builds an array of parsed salutation info from supplied contacts
    Dim oContact As mpCI.Contact
    Dim iUpper As Integer
    Dim iCol As Integer
    Dim xFldName As String
    Dim xValue As String
    
    'exit if there are no contacts
    If oContacts Is Nothing Then
        Exit Sub
    ElseIf oContacts.Count = 0 Then
        Exit Sub
    End If
    
    If m_oDataArr.UpperBound(2) = 0 Then
        'dimension m_m_oDataArr with enough cols to
        'hold standard salutation elements and custom fields
        CreateParsedContactsArray oContacts.Item(1).CustomFields
    End If
    
'   cycle through each contact in
'   collection, adding to array
    On Error GoTo ProcError
    For Each oContact In oContacts
        If oContact.ContactType = ciContactType_To Then
            iUpper = m_oDataArr.UpperBound(1) + 1
            m_oDataArr.Insert 1, iUpper
'           replace tokens with data
            With oContact
                m_oDataArr.Value(iUpper, 0) = .Prefix
                m_oDataArr.Value(iUpper, 1) = .FirstName
                m_oDataArr.Value(iUpper, 2) = .MiddleName
                m_oDataArr.Value(iUpper, 3) = .LastName
                m_oDataArr.Value(iUpper, 4) = .Suffix
                m_oDataArr.Value(iUpper, 5) = .Salutation
                m_oDataArr.Value(iUpper, 6) = .GoesBy
            End With
            
            'cycle through columns, getting name of
            'custom field.  then populate cells with
            'value of custom field specified by name
            For iCol = 7 To m_oDataArr.UpperBound(2)
                xFldName = m_oDataArr.Value(0, iCol)
                On Error Resume Next
                xValue = oContact.CustomFields(xFldName)
                If xValue = "" Then
                    m_oDataArr.Value(iUpper, iCol) = ""
                Else
                    m_oDataArr.Value(iUpper, iCol) = xValue
                End If
            Next iCol
        End If
    Next oContact
    
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.ParseContacts"
    Exit Sub
End Sub

'#4716
Private Sub ParseContacts20(oContacts As CIO.CContacts)
'builds an array of parsed salutation info from supplied contacts
    Dim oContact As CIO.CContact
    Dim iUpper As Integer
    Dim iCol As Integer
    Dim xFldName As String
    Dim xValue As String
    
    'exit if there are no contacts
    If oContacts Is Nothing Then
        Exit Sub
    ElseIf oContacts.Count = 0 Then
        Exit Sub
    End If
    If m_oDataArr.UpperBound(2) = 0 Then
        'dimension m_oDataArr with enough cols to
        'hold standard salutation elements and custom fields
        CreateParsedContactsArray oContacts.Item(1).CustomFields
    End If
    
'   cycle through each contact in
'   collection, adding to array
    On Error GoTo ProcError
    For Each oContact In oContacts
        If oContact.ContactType = CIO.ciContactType_To Then
            iUpper = m_oDataArr.UpperBound(1) + 1
            m_oDataArr.Insert 1, iUpper
'           replace tokens with data
            With oContact
                m_oDataArr.Value(iUpper, 0) = .Prefix
                m_oDataArr.Value(iUpper, 1) = .FirstName
                m_oDataArr.Value(iUpper, 2) = .MiddleName
                m_oDataArr.Value(iUpper, 3) = .LastName
                m_oDataArr.Value(iUpper, 4) = .Suffix
                m_oDataArr.Value(iUpper, 5) = .Salutation
                m_oDataArr.Value(iUpper, 6) = .GoesBy
            End With
            
            'cycle through columns, getting name of
            'custom field.  then populate cells with
            'value of custom field specified by name
            For iCol = 7 To m_oDataArr.UpperBound(2)
                xFldName = m_oDataArr.Value(0, iCol)
                On Error Resume Next
                xValue = oContact.CustomFields(xFldName)
                If xValue = "" Then
                    m_oDataArr.Value(iUpper, iCol) = ""
                Else
                    m_oDataArr.Value(iUpper, iCol) = xValue
                End If
            Next iCol
        End If
    Next oContact
    
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.ParseContacts20"
    Exit Sub
End Sub

Private Sub cmbSalutation_Change()
    With UserControl.cmbSalutation
        UserControl.txtSalutation.Text = .Text
        If .Visible Then
            DoEvents
            'change to text box if text is not a choice in the list
            If .Text <> .BoundText Then
                ShowDropdown False
            End If
        End If
    End With
End Sub

Private Sub cmbSalutation_GotFocus()
    Dim xChr As String
    
    On Error GoTo ProcError
    With UserControl.cmbSalutation
        Select Case Me.SelectionType
            Case salSelectionType_EndOfSalutation
'               select directly before the ':' or ','
'               if those are the traling chars
                xChr = Right(.Text, 1)
                .SelLength = 0
                If xChr = ":" Or xChr = "," Then
                    .SelStart = Len(.Text) - 1
                Else
                    .SelStart = Len(.Text)
                End If
            Case salSelectionType_Content
                .SelStart = 0
                .SelLength = Len(.Text)
            Case Else
                .SelStart = 0
                .SelLength = 0
        End Select
    End With
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.cmbSalutation_GotFocus"
    Exit Sub
End Sub

Private Sub cmbSalutation_Open()
    If Me.List = Empty Then
        UserControl.cmbSalutation.CloseCombo
    End If
End Sub

Private Sub txtSalutation_GotFocus()
    Dim xChr As String
    
    On Error GoTo ProcError
    With UserControl.txtSalutation
        Select Case Me.SelectionType
            Case salSelectionType_EndOfSalutation
'               select directly before the ':' or ','
'               if those are the traling chars
                xChr = Right(.Text, 1)
                .SelLength = 0
                If xChr = ":" Or xChr = "," Then
                    .SelStart = Len(.Text) - 1
                Else
                    .SelStart = Len(.Text)
                End If
            Case salSelectionType_Content
                .SelStart = 0
                .SelLength = Len(.Text)
            Case Else
                .SelStart = 0
                .SelLength = 0
        End Select
    End With
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.txtSalutation_GotFocus"
    Exit Sub
End Sub

Private Sub cmbSalutation_ItemChange()
    UserControl.txtSalutation.Text = UserControl.cmbSalutation.Text
End Sub

Private Sub ReadProperties(oPropBag As PropertyBag)
'reads properties from property bag
    On Error GoTo ProcError
    With oPropBag
        Me.AllowDropdown = .ReadProperty("AllowDropdown", True)
        Me.SelectionType = .ReadProperty("SelectionType", True)
        Me.DefaultListIndex = .ReadProperty("DefaultListIndex", 0)
        Me.Appearance = .ReadProperty("Appearance", salAppearance.salAppearance_Flat)
        Me.List = .ReadProperty("List", "")
        Me.ListRows = .ReadProperty("ListRows", 8)
        Me.Schema = .ReadProperty("Schema", "")
        Me.MultiRecipientSeparator = .ReadProperty("MultiRecipientSeparator", "and")
        Me.MultiRecipientFormats = .ReadProperty("MultiRecipientFormats", False)
        Me.Text = .ReadProperty("Text", "")
        
        Dim oFont As StdFont
        Set oFont = New StdFont
        oFont.Bold = .ReadProperty("FontBold", False)
        oFont.Italic = .ReadProperty("FontItalic", False)
        oFont.Underline = .ReadProperty("FontUnderline", False)
        oFont.Size = .ReadProperty("FontSize", 8)
        oFont.Name = .ReadProperty("FontName", "Arial")
        Set Me.Font = oFont
    End With

    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.ReadProperties"
    Exit Sub
End Sub

Private Sub WriteProperties(oPropBag As PropertyBag)
'writes properties to property bag
    On Error GoTo ProcError
    With oPropBag
        .WriteProperty "AllowDropdown", Me.AllowDropdown, True
        .WriteProperty "SelectionType", Me.SelectionType, True
        .WriteProperty "Appearance", Me.Appearance, salAppearance.salAppearance_Flat
        .WriteProperty "List", Me.List, ""
        .WriteProperty "ListRows", Me.ListRows, 8
        .WriteProperty "Schema", Me.Schema, ""
        .WriteProperty "MultiRecipientSeparator", Me.MultiRecipientSeparator, "and"
        .WriteProperty "MultiRecipientFormats", Me.MultiRecipientFormats, False
        .WriteProperty "Schema", Me.Schema, ""
        .WriteProperty "Text", Me.Text, ""
        .WriteProperty "DefaultListIndex", Me.DefaultListIndex, 0
        .WriteProperty "FontName", Me.Font.Name, "Arial"
        .WriteProperty "FontSize", Me.Font.Size, 9
        .WriteProperty "FontBold", Me.Font.Bold, False
        .WriteProperty "FontItalic", Me.Font.Italic, False
        .WriteProperty "FontUnderline", Me.Font.Underline, False
    End With
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.WriteProperties"
    Exit Sub
End Sub

Private Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
    Dim xARP As XArray
    Dim iRows As Integer
    
    On Error GoTo ProcError
'    On Error Resume Next
    With tdbCBX
        Set xARP = tdbCBX.Array
        If xARP Is Nothing Then
            .DropdownHeight = 0
        Else
            iRows = Min(xARP.Count(1), CDbl(iMaxRows))
            .DropdownHeight = (iRows * .RowHeight) + 50
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.ResizeTDBCombo"
    Exit Sub
End Sub

Private Sub RaiseError(Optional ByVal xNewSource As String)
'raises the current error - sets source if source
'has been supplied and is not already set

    If xNewSource <> Empty Then
'       source supplied
        If InStr(Err.Source, ".") = 0 Then
'           source not set
            Err.Source = xNewSource
        End If
    End If
    
'   raises modified error
    Err.Raise Err.Number, Err.Source, Err.Description
End Sub

Private Sub UserControl_InitProperties()
    Me.AllowDropdown = True
    Me.ListRows = 8
    Me.Appearance = salAppearance_Flat
    Me.SelectionType = True
    
    Dim oFont As StdFont
    Set oFont = New StdFont
    With oFont
        .Name = "Arial"
        .Size = 8
    End With
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    ReadProperties PropBag
End Sub

Private Sub UserControl_Resize()
    On Error GoTo ProcError
    With UserControl
'       limit height
        .Height = 340
        .cmbSalutation.Height = 340
        .cmbSalutation.Width = .Width + 20
        .txtSalutation.Height = 300
        .txtSalutation.Width = .Width - 20
        .txtSalutation.Left = .cmbSalutation.Left + 20
        .txtSalutation.Top = .cmbSalutation.Top + 20
    End With
    Exit Sub
ProcError:
    RaiseError "mpSAL.SalutationCombo.UserControl_Resize"
    Exit Sub
End Sub

Private Sub UserControl_Show()
    On Error GoTo ProcError
    'create parsed contact data array
    If m_oDataArr Is Nothing Then
        AssignEmptyArray
    End If
    Exit Sub
ProcError:
    RaiseError "mpSal.SalutationCombo.UserControl_Show"
    Exit Sub
End Sub

Private Sub AssignEmptyArray()
    On Error GoTo ProcError
    Set m_oDataArr = New XArray
    m_oDataArr.ReDim 0, -1, 0, 0
    Exit Sub
ProcError:
    RaiseError "mpSal.SalutationCombo.AssignEmptyArray"
    Exit Sub
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    WriteProperties PropBag
End Sub

'Private Function xarStringToxArray(ByVal xString As String, _
'                        Optional iNumCols As Integer = 1, _
'                        Optional xSep As String = "|") As XArray
'
'    Dim iNumSeps As Long
'    Dim iNumEntries As Long
'    Dim iSepPos As Long
'    Dim xEntry As String
'    Dim i As Long
'    Dim j As Long
'    Dim xARP As XArray
'
''   get ubound of arrP - count delimiter
''   then divide by iNumCols
'    iNumSeps = lCountChrs(xString, xSep)
'    iNumEntries = (iNumSeps + 1) / iNumCols
'
'    Set xARP = New XArrayObject.XArray
'
'
'    xARP.Insert 0, iNumEntries - 1, 0, iNumCols - 1
'
'    For i = 0 To iNumEntries - 1
'        For j = 0 To iNumCols - 1
''           get next entry & store
'            iSepPos = InStr(xString, xSep)
'            If iSepPos Then
'                xEntry = Left(xString, iSepPos - 1)
'            Else
'                xEntry = xString
'            End If
'            xARP.Value(i, j) = xEntry
'
''           remove entry from xstring
'            If iSepPos Then _
'                xString = Mid(xString, iSepPos + Len(xSep))
'        Next j
'    Next i
'
'    Set xarStringToxArray = xARP
'End Function

